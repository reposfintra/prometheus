/*
 * NegociosApplusService.java
 * Created on 2 de febrero de 2009, 17:33
 */
package com.tsp.operation.model.services;
import com.tsp.operation.model.DAOS.NegociosApplusDAO;
import java.util.ArrayList;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import java.util.ResourceBundle;
import com.tsp.operation.model.beans.Imagen;
import com.tsp.operation.model.beans.NegocioApplus;
import com.tsp.operation.model.beans.Material;
import com.tsp.operation.model.beans.BeanGeneral;//20100219
import java.util.Map;
/**
 * @author  Fintra
 */
public class NegociosApplusService {
    /** Creates a new instance of NegociosApplusService */
    NegociosApplusDAO negociosApplusDAO;
    private List      archivitos;
    private BeanGeneral data_recep_obra;//20100219
    
    public NegociosApplusService() {
        negociosApplusDAO= new NegociosApplusDAO();
    }
    public NegociosApplusService(String dataBaseName) {
        negociosApplusDAO= new NegociosApplusDAO(dataBaseName);
    }
    
    public ArrayList getNegociosApplus(String estado,String contratista,String numosxi,String factconformed,String loginx) throws Exception{
                     //ystem.out.println("estado en service"+estado);
        return negociosApplusDAO.getNegociosApplus(estado,contratista,numosxi,factconformed,loginx);
    }
    public String CambiarEstado(String[] ofertas,String nuevoestado,String observacion,String esquema_comision,String svx,String contratista_consultar,String fact_conformed,String cuoticas,String fecfaccli,String fact_conformada_consultar,String esquema_financiacion,String userx) throws Exception{
        return negociosApplusDAO.CambiarEstado(ofertas ,nuevoestado,observacion,esquema_comision,svx,contratista_consultar,fact_conformed,cuoticas,fecfaccli,fact_conformada_consultar,esquema_financiacion,userx);
    }

    public ArrayList getEstadosApplus() throws Exception{
        return negociosApplusDAO.getEstadosApplus();
    }

    public void insertImagen( ByteArrayInputStream bfin, int longitud,  Dictionary fields,String idxx4,String tipito) throws Exception{
        try{
            this.negociosApplusDAO.insertImagen(bfin,longitud,fields,idxx4,tipito);
        }catch(Exception e){  throw new Exception( e.getMessage()); }
    }
    
    public void insertRegistroArchivo( String ruta,  Dictionary fields,String idxx4,String tipito, String bd) throws Exception{
        try{
            this.negociosApplusDAO.insertRegistroArchivo(ruta,fields,idxx4,tipito, bd);
        }catch(Exception e){  throw new Exception( e.getMessage()); }
    }

    public String[] getArchivo(String numosx, String loginx,String tipito)
    {
        String respuesta[]=new String[0] ;
        //respuesta[0]="";
        try
        {
            //ImagenService imagenSvc = new ImagenService();
            java.util.List lista= searchArchivos(numosx, loginx,tipito);

            //ystem.out.println("lista"+lista);
            Imagen img;
            if(lista != null && lista.size() > 0){
                respuesta=new String[lista.size()];
                for (int k=0;k<lista.size();k++){
                    img = (Imagen)lista.get(k);
                    respuesta[k] = img.getActividad()+"__"+img.getNameImagen();
                }
            }
            img = null;
            lista = null;

        }
        catch(Exception e)
        {
            System.out.println("errorrr::"+e.toString()+"__"+e.getMessage());
        }
        return respuesta;
    }
    
    public List getNombresArchivos(String numosx,String loginx,String tipito)
    {
       java.util.List lista =new ArrayList<>();
        try
        {             
            //Realizamos las busqueda por negocio
            String rutaOrigen  = this.negociosApplusDAO.getPathDocument(numosx.split("-")[0]);
            lista.addAll(negociosApplusDAO.searchNombresArchivos(rutaOrigen,numosx.split("-")[0]));
            
            //Realizamos las busqueda por numero_solicitud
            if(numosx.split("-").length >1){
                rutaOrigen  = this.negociosApplusDAO.getPathDocument(numosx.split("-")[1]);
                lista.addAll(negociosApplusDAO.searchNombresArchivos(rutaOrigen,numosx.split("-")[1]));
            }
            
            //lista= negociosApplusDAO.searchNombresArchivos(numosx, loginx,tipito);          
        }
        catch(Exception e)
        {
            System.out.println("errorrr::"+e.toString()+"__"+e.getMessage());
        }
        return lista;
    }

    public List searchArchivos(String documento,String loginx,String tipito) throws Exception{
        try{
             ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
             String rutaOrigen  = this.negociosApplusDAO.getPathDocument(documento);
             String rutaDestino  = rb.getString("ruta")+ "/images/multiservicios/"+loginx+"/";            
             //ystem.out.println("en service doc:"+documento+" y ruta:"+ruta);
             return  this.negociosApplusDAO.searcArchivos(documento,(""+rutaOrigen ),(""+rutaDestino ),tipito);
        }catch(Exception e){ throw new Exception( e.getMessage());}
    }
    
    public boolean almacenarArchivoEnCarpetaUsuario(String documento,String loginx,String nomarchivo) throws Exception{
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        try{            
             String rutaOrigen  = this.negociosApplusDAO.getPathDocument(documento.split("-")[0]);
             String rutaDestino  = rb.getString("ruta")+ "/images/multiservicios/"+loginx+"/";            
         
             return  this.negociosApplusDAO.copiarArchivo(documento.split("-")[0],(""+rutaOrigen ),(""+rutaDestino ),nomarchivo);           
        }catch(Exception e){ 
            try {
                String rutaOrigen  = this.negociosApplusDAO.getPathDocument(documento.split("-")[1]);
                String rutaDestino  = rb.getString("ruta")+ "/images/multiservicios/"+loginx+"/";            
                return  this.negociosApplusDAO.copiarArchivo(documento.split("-")[1],(""+rutaOrigen ),(""+rutaDestino ),nomarchivo);
                
            } catch (Exception ex) {
                throw new Exception( ex.getMessage());
            }
        }
    }

    public String[][] obtenerNombresArchivos(String codigoNegocio) {
        return this.negociosApplusDAO.obtenerNombresArchivos(codigoNegocio);
    }
    
    public List<Map<String, String>> getCategoriaArchivos(String codigoNegocio) {
        return negociosApplusDAO.obtenerCategoriaArchivos(codigoNegocio);
    }

    public String obtenerNombreArchivo(int id) {
        return negociosApplusDAO.obtenerNombreArchivo(id);
    }

    public List<Map<String, String>> getArchivosCredito(String idCategoria,String codigoNegocio, String numeroSolicitud, String tipo, String tipoPersona) {
        return negociosApplusDAO.obtenerArchivosCrédito(idCategoria, codigoNegocio, numeroSolicitud, tipo, tipoPersona);
    }   

    public boolean esListadoViejo(String codigoNegocio, String tipito) {
        return negociosApplusDAO.esListadoViejo(codigoNegocio, tipito);
    }
    
    public ArrayList getContratistas() {
       java.util.ArrayList lista =null;
        try {
            lista= negociosApplusDAO.getContratistas();
        } catch(Exception e) {
            System.out.println("errorrr::"+e.toString()+"__"+e.getMessage());
        }
        return lista;
    }

    public String[] getExPrefacturaEca(String id_orde)  throws Exception{
        return this.negociosApplusDAO.getExPrefacturaEca(id_orde);

    }

    public String[] getExPrefacturaContratista(String id_orde,String id_accio)  throws Exception{
        return this.negociosApplusDAO.getExPrefacturaContratista(id_orde,id_accio);

    }

    public String getEsquemaComision (String id_orden) throws Exception{
        return this.negociosApplusDAO.getEsquemaComision(id_orden);
    }

    public List  obtainCxpsContratista()throws Exception{
        return negociosApplusDAO.obtainCxpsContratista();
    }

    public String updateAccord (String factura_contratista,String factura_formula_prov) throws Exception{
        return this.negociosApplusDAO.updateAccord(factura_contratista, factura_formula_prov);
    }

    public ArrayList obtainAccords(String[] negocios) throws Exception{
        return this.negociosApplusDAO.obtainAccords(negocios);
    }

    public String validarCambioEstado(String[] acciones,String nuevo_estado) throws Exception{
        return this.negociosApplusDAO.validarCambioEstado(acciones,nuevo_estado);
    }

    public ArrayList getEstadosApplusUser(String userx) throws Exception{
        return negociosApplusDAO.getEstadosApplusUser(userx);
    }

    public ArrayList getNegociosApplus2(String estado,String contratista,String numosxi,String factconformed,String loginx,String id_solici,
            String nicc ,String nomclie,String id_cliente) throws Exception{
        return negociosApplusDAO.getNegociosApplus2(estado,contratista,numosxi,factconformed,loginx,id_solici,nicc ,nomclie, id_cliente);
    }

    public ArrayList getEstadosApplus2() throws Exception{
        return negociosApplusDAO.getEstadosApplus2();
    }

    public NegocioApplus obtainAccione(String solicitud_consultable,String id_accion,String loginx) throws Exception{
        return negociosApplusDAO.obtainAccione(solicitud_consultable,id_accion,loginx);
    }
    public java.util.TreeMap getClientesEca()throws Exception    {
        return  negociosApplusDAO.getClientesEca();
    }

    public String getClientesEca(String cl,String nomselect,String def)throws Exception    {
        return  negociosApplusDAO.getClientesEca(cl, nomselect,def);
    }

    public String marcarTipoTrabajo(String solicitud_consultable,String id_accion,String loginx,String tipotraba,String observaci,String extipotraba,String fecvisitaplan,String fecvisitareal) throws Exception{
        return this.negociosApplusDAO.marcarTipoTrabajo(solicitud_consultable,id_accion,loginx,tipotraba,observaci,extipotraba,fecvisitaplan,fecvisitareal);//091030
    }

    public String asignar(String loginx, String fecof,String solicitudes[]) throws Exception{
        return this.negociosApplusDAO.asignar(loginx,fecof,solicitudes);
    }

    public String validacionSeguridadSolicitud(String id_solicitudx,String id_accionx,String loginxx,String pasoadar){
        return this.negociosApplusDAO.validacionSeguridadSolicitud( id_solicitudx, id_accionx, loginxx, pasoadar);
    }

    public String getIdClie(String nic) throws Exception{
        return this.negociosApplusDAO.getIdClie( nic );
    }

    public NegocioApplus obtainAlcanc(String solicitud_consultable,String id_accion,String loginx) throws Exception{//091016
        return negociosApplusDAO.obtainAlcanc(solicitud_consultable,id_accion,loginx);
    }

    public String marcarAvance(String solicitud_consultable,String id_accion,String loginx,String alcanci,String adicioni,String trabaji)  throws Exception{
        return negociosApplusDAO.marcarAvance(solicitud_consultable,id_accion,loginx,alcanci,adicioni,trabaji);
    }

public String modificarProducto(String exproducto,String consec,String descr,double pre,String tipo,String medida,String loginx,String cat)  throws Exception{
        return negociosApplusDAO.modificarProducto(exproducto, consec, descr, pre, tipo, medida, loginx, cat);//091222
    }

    public Material getMaterial(String pk_material) throws Exception{
        return negociosApplusDAO.getMaterial(pk_material);
    }

    public String asignarEstado(String loginx, String estado_asignable,String acciones[]) throws Exception{
        return this.negociosApplusDAO.asignarEstado(loginx,estado_asignable,acciones);
    }

    public BeanGeneral obtainDatRecepObra(String solicitud) throws Exception{
        data_recep_obra=this.negociosApplusDAO.getDatRecepObra(solicitud);;
        return data_recep_obra;
    }

    public BeanGeneral getDatRecepObra() {
        return data_recep_obra;
    }

    public String aceptarRecepObra(String solicitud,String[] observaciones,String[] acciones,String[] fecFin,String[] fecInterventor,String loginx) throws Exception{
        return this.negociosApplusDAO.aceptarRecepObra(solicitud,observaciones,acciones,fecFin,fecInterventor,loginx);
    }
}
