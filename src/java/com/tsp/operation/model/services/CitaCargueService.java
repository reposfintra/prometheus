/*
 * CumplidoService.java
 *
 * Created on 20 de septiembre de 2005, 02:17 PM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  KREALES
 */
public class CitaCargueService {
    CitaCargueDAO citaDao;
    CitaCargue citaCargue;
    CitaCargue citaDescargue;
    /** Creates a new instance of CumplidoService */
    public CitaCargueService() {
        citaDao = new CitaCargueDAO();
    }
    /**
     * Getter for property cita.
     * @return Value of property cita.
     */
    public com.tsp.operation.model.beans.CitaCargue getCita() {
        return citaDao.getCita();
    }
    
    /**
     * Setter for property cita.
     * @param cita New value of property cita.
     */
    public void setCita(com.tsp.operation.model.beans.CitaCargue cita) {
        citaDao.setCita(cita);
    }
    
    /**
     * Metodo insertCitaCargue ,Metodo que actualiza un registro en la tabla de citas de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @version : 1.0
     */
    public void insertCitaCargue()throws SQLException{
        citaDao.insertCitaCargue();
    }
     /**
     * Metodo buscarCitaCargue , Metodo que busca una cita de cargue.
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String placa
     * @parameter : String numpla
     * @parameter : String tipo  si es cargue C si es descargue D
     * @version : 1.0
     */
    
    public void buscarCitaCargue(String placa,String numpla,String tipo)throws SQLException{
        citaDao.buscarCitaCargue(placa, numpla, tipo);
    }
        /**
     * Metodo insertCitaCargue, Metodo que inserta un nuevo registro en la tabla de citas de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @retorna Un string con el comando slq para utilzarlo en una transaccion
     * @version : 1.0
     */
    
    
    public String insertCitaCargueNueva()throws SQLException{
        return citaDao.insertCitaCargueNueva();
    }
    
    /**
     * Getter for property citaCargue.
     * @return Value of property citaCargue.
     */
    public com.tsp.operation.model.beans.CitaCargue getCitaCargue() {
        return citaCargue;
    }
    
    /**
     * Setter for property citaCargue.
     * @param citaCargue New value of property citaCargue.
     */
    public void setCitaCargue(com.tsp.operation.model.beans.CitaCargue citaCargue) {
        this.citaCargue = citaCargue;
    }
    
    /**
     * Getter for property citaDescargue.
     * @return Value of property citaDescargue.
     */
    public com.tsp.operation.model.beans.CitaCargue getCitaDescargue() {
        return citaDescargue;
    }
    
    /**
     * Setter for property citaDescargue.
     * @param citaDescargue New value of property citaDescargue.
     */
    public void setCitaDescargue(com.tsp.operation.model.beans.CitaCargue citaDescargue) {
        this.citaDescargue = citaDescargue;
    }
    /**
     * Metodo reporteCitaCargue , Metodo que busca una lista con informacion acerca de
     * indicadores de cumplimiento de citas de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @parameter : String fecfin
     * @version : 1.0
     */
    public void reporteCitaCargue(String fecini,String fecfin)throws SQLException{
        citaDao.reporteCitaCargue(fecini, fecfin);
    }
    
    /**
     * Getter for property descargue.
     * @return Value of property descargue.
     */
    public java.util.Vector getDescargue() {
        return citaDao.getDescargue();
    }    
    
    /**
     * Setter for property descargue.
     * @param descargue New value of property descargue.
     */
    public void setDescargue(java.util.Vector descargue) {
        citaDao.setDescargue(descargue);
    }
    
    /**
     * Getter for property cargue.
     * @return Value of property cargue.
     */
    public java.util.Vector getCargue() {
        return citaDao.getCargue();
    }
    
    /**
     * Setter for property cargue.
     * @param cargue New value of property cargue.
     */
    public void setCargue(java.util.Vector cargue) {
        citaDao.setCargue(cargue);
    }
    /**
     * Metodo causasCitaCargue , Metodo que busca una lista con informacion acerca de
     * indicadores de utilizacion de las causas de cita de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String tipo C para cargue, D para descargue
     * @parameter : String fecini
     * @parameter : String fecfin
     * @version : 1.0
     */
    public void causasCitaCargue(String tipo,String fecini,String fecfin)throws SQLException{
        citaDao.causasCitaCargue(tipo,fecini,fecfin);
    }
    
     /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.util.Vector getCausas() {
        return citaDao.getCausas();
    }
    
    /**
     * Setter for property causas.
     * @param causas New value of property causas.
     */
    public void setCausas(java.util.Vector causas) {
        citaDao.setCausas(causas);
    }
     /**
     * Metodo reporteCitaCarguePlaca , Metodo que busca una lista con informacion acerca de
     * las placas que incumplieron la cita de cargue
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @parameter : String fecfin
     * @version : 1.0
     */
    public void reporteCitaCarguePlaca(String fecini,String fecfin)throws SQLException{
        citaDao.reporteCitaCarguePlaca(fecini, fecfin);
    }
     /**
     * Getter for property placas.
     * @return Value of property placas.
     */
    public java.util.Vector getPlacas() {
        return citaDao.getPlacas();
    }    
     
    /**
     * Setter for property placas.
     * @param placas New value of property placas.
     */
    public void setPlacas(java.util.Vector placas) {
        citaDao.setPlacas(placas);
    }    
      /**
     * Metodo reportePlacasCitaCargue , Metodo que busca una lista con informacion acerca de
     * las placas con fecha planeada de cargue en la mina, a partir de una fecha dada hasta hoy
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @version : 1.0
     */
    public void reportePlacasCitaCargue(String fecini)throws SQLException{
        citaDao.reportePlacasCitaCargue(fecini);
    }
    /**
     * Metodo cargarPropiedades, Metodo que guarda en un archivo
     * tipo properties los valores guardados en una variable properties
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @version : 1.0
     */
    public void setUltimafechaCreacion() throws IOException{
        citaDao.setUltimafechaCreacion();
    }
    /**
     * Metodo cantCargues , Metodo que busca la cantidad de cargues o descargues planeado
     * y automaticos
     * @autor : Ing. Karen Reales
     * @throws : SQLException
     * @parameter : String fecini
     * @parameter : String fecfin
     * @parameter : String tipo
     * @version : 1.0
     */
    public void cantCargues(String fecini,String fecfin, String tipo)throws SQLException{
    
        citaDao.cantCargues(fecini, fecfin, tipo);
    }
    
}
