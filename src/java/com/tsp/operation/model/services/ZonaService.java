/*
 * ZonaService.java
 *
 * Created on 13 de Julio de 2005, 09:00 AM
 */

package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  HENRY
 */
public class ZonaService {
    
    private ZonaDAO zonaDao; 
    private TreeMap zonas;
    
    /** Creates a new instance of ZonaService */
    public ZonaService() {
        zonaDao = new ZonaDAO(); 
    }
     
   public void insertarZona(Zona zona)throws SQLException {
       try{
           zonaDao.setZona(zona);
           zonaDao.insertarZona();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
   public boolean existeZona(String cod) throws SQLException {
       return zonaDao.existeZona(cod); 
   }         
   /*public boolean existZonaNom(String nom) throws SQLException {
       return zonaDao.existZonaNom(nom); 
   } */  
   public List obtenerZonas( ) throws SQLException {
       List zonas = null;
       zonas = zonaDao.obtenerZonas();
       return zonas;
   }   
   public void listarZonas () throws SQLException {
       try{
           zonaDao.listarZonas();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   
   public void listarZonasxUsuario ( String usuario)throws SQLException{
       try{
           zonaDao.listarZonasxUsuario(usuario);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public void zonas () throws SQLException {
       try{
           zonaDao.zonas();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
   }
   public Zona obtenerZona()throws SQLException{
       return zonaDao.obtenerZona();
   }   
   public Vector obtZonas()throws SQLException{
       return zonaDao.obtZonas();
   }   
   public void buscarZona(String cod) throws SQLException {
       try{
           zonaDao.buscarZona(cod);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void buscarZonaNombre(String nom) throws SQLException {
       try{
           zonaDao.buscarZonaNombre(nom);     
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
   }
   public void modificarZona(Zona zona)throws SQLException {
       try{
           zonaDao.setZona(zona);
           zonaDao.modificarZona();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }   
   public void eliminarZona(Zona zona)throws SQLException {
       try{
           zonaDao.setZona(zona);
           zonaDao.eliminarZona();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
   }
 /**
   * Metodo: cambioZonaPlanilla, instancia del metodo correspondiente en el DAO
   * @autor : Ing. Sandra Escalante
   * @see cambioZonaPlanilla
   * @param : numero de la planilla, codigo y nombre de la zona
   * @version : 1.0
   */
   public void cambioZonaPlanilla (String planilla, String zona, String nomzona, String distrito, String user, String base, String uzona) throws SQLException {
       zonaDao.cambioZonaPlanilla(planilla, zona, nomzona, distrito,user,base,uzona);
   }
   
   /**
    * Getter for property zonas.
    * @return Value of property zonas.
    */
   public java.util.TreeMap getZonas() {
       return zonas;
   }   
   
   /**
    * Setter for property zonas.
    * @param zonas New value of property zonas.
    */
   public void setZonas(java.util.TreeMap zonas) {
       this.zonas = zonas;
   }
   
   /**
   * Metodo Carga el treemap de zonas del sistema
   * @autor Ing. Andr�s Maturana
   * @version 1.0
   */
   public void loadZonas() throws Exception{
       this.zonas = new TreeMap();
       this.listarZonas();
       List zonas = this.obtenerZonas();
       for( int i = 0; i<zonas.size(); i++){
           Zona z = (Zona) zonas.get(i);
           this.zonas.put(z.getNomZona(), z.getCodZona());
       }
   }
   
    /**
   * Metodo: listarZonasFronterisas, busca las zona fronteriza
   * @autor : Ing. Diogenes Bastidas
   * @param : 
   * @version : 1.0
   */
   
   public void listarZonasFronterisas ()throws SQLException{
        zonaDao.listarZonasFronterisas();
   }
   
   /**
   * Metodo: listarZonasNoFronterisas, busca las zona no fronteriza
   * @autor : Ing. Diogenes Bastidas
   * @param : 
   * @version : 1.0
   */
   
   public void listarZonasNoFronterisas ()throws SQLException{
       zonaDao.listarZonasNoFronterisas();
   }
   
   /**
    * Getter for property zonasFronterisas.
    * @return Value of property zonasFronterisas.
    */
   public List getZonasFronterisas() {
       return zonaDao.getZonasFronterisas();
   }
   
   /**
    * Getter for property zonasNoFronterisas.
    * @return Value of property zonasNoFronterisas.
    */
   public List getZonasNoFronterisas() {
       return zonaDao.getZonasNoFronterisas();
   }
}
