/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.CorficolombianaDAO;
import java.util.ArrayList;

/**
 *
 * @author maltamiranda
 */
public class CorficolombianaService {
    ArrayList resultado;

    public ArrayList getResultado() {
        return resultado;
    }

    public void setResultado(ArrayList resultado) {
        this.resultado = resultado;
    }

    public ArrayList getReportes() throws Exception
    {   CorficolombianaDAO cd= new CorficolombianaDAO();
        return cd.getReportes();
    }

    public void getConsulta(String id_reporte,String tipo, String periodo, String parametro)throws Exception{
        CorficolombianaDAO cd= new CorficolombianaDAO();
        this. setResultado(cd.getConsulta(id_reporte, tipo, periodo,parametro));
        //return cd.getConsulta(id_reporte, tipo, periodo);
    }

}
