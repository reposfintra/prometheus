/*
 * ReporteBancoTransferenciaService.java
 *
 * Created on 25 de octubre de 2006, 09:14 AM
 */

package com.tsp.operation.model.services;

import java.util.*;
import com.tsp.operation.model.DAOS.*;
/**
 *
 * @author  EQUIPO12
 */
public class ReporteBancoTransferenciaService {
    private ReporteBancoTransferenciaDAO ReporteBTDAO;   
    private List listaNit;
    private List listaReporte;
    
    
    /** Creates a new instance of ReporteBancoTransferenciaService */
    public ReporteBancoTransferenciaService() {
        ReporteBTDAO = new ReporteBancoTransferenciaDAO();
    }
    public ReporteBancoTransferenciaService(String dataBaseName) {
        ReporteBTDAO = new ReporteBancoTransferenciaDAO(dataBaseName);
    }
    
    
     public void buscarNit() throws Exception {
        try{
            this.listaNit = ReporteBTDAO.buscarNit(); 
        }catch(Exception e){
            throw new Exception("Error en buscarNit ReporteBancoTransferenciaService]...\n"+e.getMessage());
        }
    }
     
      public List buscarPlanillas(String nit) throws Exception {
        try{
            return ReporteBTDAO.buscarPlanillas(nit);
            
        }catch(Exception e){
            throw new Exception("Error en buscarNit ReporteBancoTransferenciaService]...\n"+e.getMessage());
        }
    }
     
     /**
      * Getter for property listaNit.
      * @return Value of property listaNit.
      */
     public java.util.List getListaNit() {
         return listaNit;
     }
     
     /**
      * Setter for property listaNit.
      * @param listaNit New value of property listaNit.
      */
     public void setListaNit(java.util.List listaNit) {
         this.listaNit = listaNit;
     }
     
     /**
      * Getter for property listaReporte.
      * @return Value of property listaReporte.
      */
     public java.util.List getListaReporte() {
         return listaReporte;
     }     
   
     /**
      * Setter for property listaReporte.
      * @param listaReporte New value of property listaReporte.
      */
     public void setListaReporte(java.util.List listaReporte) {
         this.listaReporte = listaReporte;
     }     
     
}
