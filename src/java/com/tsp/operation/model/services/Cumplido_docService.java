/*
 * Cumplido_docService.java
 *
 * Created on 5 de octubre de 2005, 04:50 PM
 */

package com.tsp.operation.model.services;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  R.SALAZAR
 */
public class Cumplido_docService {
    Cumplido_docDAO cumpdocdao;
    
    /** Creates a new instance of Cumplido_docService */
    public Cumplido_docService() {
        cumpdocdao = new Cumplido_docDAO();
    }
     /**
     * Metodo: getCumplido_docs, permite retornar un vector de registros de documentos cumplidos.
     * @autor : Ing. Jose de la rosa
     * @param :
     * @version : 1.0
     */
    public Vector getCumplido_docs () {
        return cumpdocdao.getCumplido_docs ();
    }
    
     /**
     * Metodo: setCumplido_docs, permite obtener un vector de registros de documentos cumplidos.
     * @autor : Ing. Jose de la rosa
     * @param : vector
     * @version : 1.0
     */
    public void setCumplido_docs (Vector cumpl) {
        cumpdocdao.setCumplido_docs (cumpl);
    }   
    
    /**
     * Metodo: searchCumplido_doc, permite buscar una documento cumplido dado unos parametros
     * @autor : Ing. Jose de la rosa
     * @param : distrito, numero de la remesa, numero de la planilla,tipo de documento, documento, tipo de documento relacionado y documento relacionado.
     * @see searchCumplido_doc - Cumplido_docDAO
     * @version : 1.0
     */
    public boolean searchCumplido_doc (String dstrct,String numrem,String numpla,String tipo_doc,String documento,String tipo_doc_rel,String documento_rel)throws SQLException{
        
        try{
            return cumpdocdao.searchCumplido_doc( dstrct, numrem, numpla, tipo_doc, documento, tipo_doc_rel, documento_rel);
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    public void InsertarCumplido_doc(Cumplido_doc var)throws SQLException{
        
        try{
            cumpdocdao.insertarCumplidos(var);
        }
        catch(SQLException e){
           throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Obtiene los clientes asignados a un usuario
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param login Login del usuario
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public String clientesAsignados(String login) throws SQLException{
        return cumpdocdao.clientesAdignados(login);
    }
    
    /**
     * Obtiene los documentos cumplidos en un per�do dado.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param fechai Fecha inicial del per�odo
     * @param fechaf Fecha final del per�odo
     * @throws <code>SQLException</code>
     * @version 1.0
     */
    public void documentosCumplidos(String fechai, String fechaf) throws SQLException{
        this.cumpdocdao.documentosCumplidos(fechai, fechaf);
    }
}
