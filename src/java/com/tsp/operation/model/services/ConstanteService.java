/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.services;


import com.tsp.operation.model.beans.*;
import java.util.*;
import java.util.Date;

import java.sql.*;
import com.tsp.operation.model.DAOS.*;

import com.tsp.util.Util;
import com.tsp.util.LogWriter;
import java.text.*;










/**
 *
 * @author Alvaro
 */
public class ConstanteService {

    private ConstanteDAO constanteDao;



    /** Creates a new instance of ConsorcioService */
    public ConstanteService() {
        constanteDao = new ConstanteDAO();
    }
    public ConstanteService(String dataBaseName) {
        constanteDao = new ConstanteDAO(dataBaseName);
    }



    /** Crea una lista de ofertas pendientes de facturar a los clientes */
    public Constante getConstante(String dstrct, String codigo, String dataBaseName) throws SQLException{
        return constanteDao.getConstante(dstrct, codigo, dataBaseName);
    }


    public String  getDescripcion(String dstrct, String codigo, String dataBaseName) throws SQLException{
        return constanteDao.getDescripcion(dstrct, codigo, dataBaseName);
    }


    public String  getValor(String dstrct, String codigo, String dataBaseName) throws SQLException{
        return constanteDao.getValor(dstrct, codigo, dataBaseName);
    }


    public String getCuentaHCIngreso(String codigo) throws SQLException {
        return constanteDao.getCuentaHCIngreso(codigo);
    }

}

