/*
 * InteresesFenalcoService.java
 *
 * Created on 1 de agosto de 2008, 8:56
 */

package com.tsp.operation.model.services;

import com.tsp.operation.model.DAOS.InteresesFenalcoDAO;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 * @author  navi
 */
public class InteresesFenalcoService {

    private InteresesFenalcoDAO interesesFenalcoDAO;

    public InteresesFenalcoService() {
        interesesFenalcoDAO= new InteresesFenalcoDAO();
    }
    public InteresesFenalcoService(String dataBaseName) {
        interesesFenalcoDAO= new InteresesFenalcoDAO(dataBaseName);
    }

    public String InsertIngFenalcoNegRemix(String cod_neg,String cod_cli,String usuario) throws SQLException{
        return  interesesFenalcoDAO.InsertIngFenalcoNegRemix(cod_neg, cod_cli , usuario );
    }

    public ArrayList<String> insertarIngFenalco(String cod_neg, String cod_cli, String usuario, String documentType, String cmc) throws Exception {
        return interesesFenalcoDAO.insertarIngFenalco(cod_neg, cod_cli, usuario, documentType, cmc);
    }

}
