/*
* Nombre        NegocioTrazabilidadDAO.java
* Descripci�n   Clase para el acceso a los datos de negocios_trazabilidad
* Autor         Iris vargas
* Fecha         25 de noviembre de 2011, 12:08 PM
* Versi�n       1.0
* Coyright      Geotech S.A.
*/

package com.tsp.operation.model.services;

import com.google.gson.JsonObject;
import com.tsp.operation.model.DAOS.NegocioTrazabilidadDAO;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.operation.model.beans.NegocioTrazabilidad;
import java.util.ArrayList;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.NegocioAnalista;
import java.sql.SQLException;


/**
 * Clase para el acceso a los datos de negocios_trazabilidad
 * @author Iris Vargas
 */
public class NegocioTrazabilidadService {
    
    NegocioTrazabilidadDAO dao;
    

    public NegocioTrazabilidadService() {
        dao = new NegocioTrazabilidadDAO();
    }
    public NegocioTrazabilidadService(String dataBaseName) {
        dao = new NegocioTrazabilidadDAO(dataBaseName);
    }
    
    

    /**
     * Sql par insertar registro en la tabla negocios_trazabilidad
     * @return sql de insersion de negocios_trazabilidad
     * @param negtraza objeto NegocioTrazabilidad
     * @throws Exception cuando hay error
     */
    public String insertNegocioTrazabilidad(NegocioTrazabilidad  negtraza){
        return dao.insertNegocioTrazabilidad(negtraza);
    }

     /**
     * Sql para actualizar la actividad de un negocio
     * @return sql de insersion de negocios_trazabilidad
     * @param negocio   codigo de negocio
     * @param actividad  actividad a actualizar
     * @throws Exception cuando hay error
     */
    public String updateActividad(String negocio, String actividad){
        return dao.updateActividad(negocio, actividad);
    }

    /**
     * Sql para actualizar el estado de un negocio
     * @return Sql para actualizar el estado de un negocio
     * @param negocio   codigo de negocio
     * @param estado  estado a actualizar
     * @throws Exception cuando hay error
     */
    public String updateEstadoNeg(String negocio, String estado){
        return dao.updateEstadoNeg(negocio, estado);
    }

    /**
     * Sql para actualizar el estado de un formulario
     * @return sql que actualiza el estado de un formulario
     * @param form   numero de formulario
     * @param estado  estado a actualizar
     * @throws Exception cuando hay error
     */
    public String updateEstadoForm(String negocio, String estado){
        return dao.updateEstadoForm(negocio, estado);
    }

    /**
     * Sql para buscar la trazabilidad de una solicitud
     * @return lista de las trazas de la solicitud
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public ArrayList< NegocioTrazabilidad> buscarNegocioTrazabilidad(int num_solicitud) throws Exception{
      return dao.buscarNegocioTrazabilidad(num_solicitud);
     }

      /**
     * Sql para buscar la trazabilidad de una solicitud y actividad
     * @return la traza de la actividad
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public  NegocioTrazabilidad buscarTraza(int num_solicitud, String actividad) throws Exception{
         return dao.buscarTraza(num_solicitud, actividad);
     }

     /**
     * Genera una lista de ocupaciones
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList listadotablagen(String tabla, String ref) throws Exception{
        return dao.listadotablagen(tabla, ref);
    }
    
 
    /**
     * Sql para buscar la trazabilidad de una solicitud rechazada
     * @return lista de las trazas de la solicitud
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public ArrayList< NegocioTrazabilidad> buscarNegocioTrazabilidadRechazado(int num_solicitud) throws Exception{
      return dao.buscarNegocioTrazabilidadRechazado(num_solicitud);
     }




        /**
     * Sql para buscar la trazabilidad de una solicitud rechazada yque tiene como causal datacredito
     * @return lista de las trazas de la solicitud
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public ArrayList< NegocioTrazabilidad> buscarNegocioTrazabilidadDatacredito(int num_solicitud) throws Exception{
      return dao.buscarNegocioTrazabilidadDatacredito(num_solicitud);
     }
     
     
      /**
     * Sql para buscar si un negocio tiene analista asignado
     * @return devuelve true o false segun el caso
     * @param cod_neg codigo del negocio
     * @throws Exception cuando hay error
     */
    public boolean tieneAnalista(String cod_neg) throws Exception {
       return  dao.tieneAnalista(cod_neg);
    }

    /**
     * Sql para buscar los usarios con el perfil analista
     * @return devuelve listado de analistas
     * @throws Exception cuando hay error
     */
     public ArrayList< String> buscarAnalistas() throws Exception{
         return dao.buscarAnalistas();
     }

     /**
     * Sql para buscar la ultima secuencia de la asignacion a analista
     * @return secuencia
     * @param numero_solicitud ultiliza la solicitud para obtener el producto de la misma
     * @throws Exception cuando hay error
     */
    public int secUltimoAnalistaProducto(String numero_solicitud) throws Exception {
        return dao.secUltimoAnalistaProducto(numero_solicitud);
    }

    /**
     * Sql para insertar registro en la tabla negocios_analista
     * @return sql de insersion de negocios_analista
     * @param neg_analista objeto NegocioTrazabilidad
     * @throws Exception cuando hay error
     */
    public String insertNegocioAnalista(NegocioAnalista neg_analista){
        return dao.insertNegocioAnalista(neg_analista);
    }

   /**
     * Sql para insertar registro en la tabla negocios_analista
     * @return sql de insersion de negocios_analista
     * @param neg_analista objeto NegocioTrazabilidad
     * @throws Exception cuando hay error
     */
    public NegocioTrazabilidad trazabilidad_radicacion(String cod_neg,String num_solicitud, Usuario usuario){
        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                                negtraza.setActividad("RAD");
                                negtraza.setNumeroSolicitud(num_solicitud);
                                negtraza.setUsuario(usuario.getLogin());
                                negtraza.setCausal("");
                                negtraza.setComentarios("No Aplica Radicacion");
                                negtraza.setDstrct(usuario.getDstrct());
                                negtraza.setCodNeg(cod_neg);
                                negtraza.setConcepto("CONTINUA");
        return negtraza;
    }
      /**
     * Sql buscar ultima activida del negocio.
     * @return beans con traza del negocio
     * @param numero solicitud
     * @throws Exception cuando hay error
     */
   public  NegocioTrazabilidad buscarUltimaActivida(int num_solicitud) throws Exception{
         return dao.buscarUltimaTraza(num_solicitud);
     }
   
    /**
     * Sql buscar la penultima actividad del negocio.
     * @return beans con traza del negocio
     * @param numero solicitud
     * @throws Exception cuando hay error
     */
   public  NegocioTrazabilidad buscarDevolverSolTraza(int num_solicitud) throws Exception{
         return dao.buscarDevolverSolTraza(num_solicitud);
     }
   
    public ArrayList buscarNegocioRelAut(String cod_cliente, String cod_neg) throws Exception {
        return dao.buscarNegocioRelAut(cod_cliente, cod_neg);
    }
    
   public String actualizarNegocioRel(String negocio, String neg_rel,String neg_type){
        return dao.actualizarNegocioRel(negocio, neg_rel, neg_type);
   }
   
   public String actualizarCicloNegocioRel(String negocio, String neg_rel){
          return dao.actualizarCicloNegocioRel(negocio, neg_rel);
   }
   
    public String  buscarNegociosStandBy(String unidad_negocio){
        return dao.buscarNegociosStandBy(unidad_negocio);
    }
    
    public String  buscarConfigCausalesStandBy(){
        return dao.buscarConfigCausalesStandBy();
    }
    
    public String insertCausalStandBy(String nombre, String plazo, Usuario usuario) {
         return dao.insertCausalStandBy(nombre, plazo, usuario);
    }
    
    public String actualizarCausalStandBy(String codigo, String plazo, Usuario usuario) {
         return dao.actualizarCausalStandBy(codigo, plazo, usuario);
    }
    
    public String activaInactivaCausalStandBy(String codigo, String estado, Usuario usuario) {
        return dao.activaInactivaCausalStandBy(codigo, estado, usuario);
    }
    
    public String getCausalStandBy(String codigo) {
        return dao.getCausalStandBy(codigo);
    }
    
    public String getMailUser(String usuario) {
        return dao.getMailUser(usuario);
    }   
    
    public String getCorreosToSendStandBy(String type) {
        return dao.getCorreosToSendStandBy(type);
    }
    
    public JsonObject getInfoCuentaEnvioCorreo() {
        return dao.getInfoCuentaEnvioCorreo();
    }
    
    public String insertErrorCorreoStandBy(String proceso, String descripcion, String detalle_error, String usuario) {
        return dao.insertErrorCorreoStandBy(proceso, descripcion, detalle_error, usuario);
    }
    
    public String getUnidadConvenio(int id_convenio) {
        return dao.getUnidadConvenio(id_convenio);
    }



    public ArrayList<CmbGeneralScBeans> GetComboGenerico(String Query, String IdCmb, String DescripcionCmb, String wuereCmb)throws Exception{
       return dao.GetComboGenerico(Query, IdCmb, DescripcionCmb, wuereCmb);
    }
    
    public String getNombreCliente(String negocio) {
        return dao.getNombreCliente(negocio);
    }

    public String getUnidadNegocio(int id_convenio) {
        return dao.getUnidadNegocio(id_convenio);
    }

      /**
     * Sql para buscar la trazabilidad de una solicitud y actividad
     * @return la traza de la actividad
     * @param num_solicitud
     * @throws Exception cuando hay error
     */
     public  NegocioTrazabilidad buscarTrazaNegocio(int num_solicitud) throws Exception{
         return dao.buscarTrazaNegocio(num_solicitud);
     }


      /**
     * Sql para actualizar la actividad de un negocio
     * @return sql de insersion de negocios_trazabilidad
     * @param negocio   codigo de negocio
     * @param estado_neg  actividad a actualizar
     
     */
    public String updateActividadnegocio(String negocio, String estado_neg, String actividad){
        return dao.updateActividadnegocio(negocio,estado_neg,actividad);
    }
    
    public String obtenerCausalUltimoStandBy(String numeroSolicitud, String codigoNegocio, String actividad) throws SQLException {
        return dao.obtenerCausalUltimoStandBy(numeroSolicitud, codigoNegocio, actividad);
    }
}
