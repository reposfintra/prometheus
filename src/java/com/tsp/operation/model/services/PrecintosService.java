/********************************************************************
 *  Nombre Clase.................   PrecintosDAO.java
 *  Descripci�n..................   Service del DAO de la tabla precintos
 *  Autor........................   Ing. Tito Andr�s Maturana
 *                                  Ing. Leonardo Parodi
 *  Fecha........................   22.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.*;


public class PrecintosService {
    private PrecintosDAO dao;
    
    /** Creates a new instance of PrecintosService */
    public PrecintosService() {
        dao = new PrecintosDAO();
    }    
   
    
    /**
     * Obtiene informaci�n de un n�mero de planilla, tales como: remes, fecha,
     * ruta (origen-destino) y cliente
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param numpla N�mero de la planilla
     * @returns <code>InfoPlanilla</code> con informaci�n de la planilla
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @see com.tsp.operation.model.DAOS.PrecintosDAO#informacionPlanilla(String)
     * @version 1.0
     */
    public InfoPlanilla informacionPlanilla(String numpla) throws SQLException{
        return dao.informacionPlanilla(numpla);
    }    
   
     /**
    * Metodo IngresarPrecintos , Metodo que ingresa una serie de Precintos
    * @autor : Ing. Leonardo Parody
    * @param : List Precintos
    * @version : 1.0
    */
     public void IngresarPrecintos(Precinto precintos) throws SQLException{
         try{
             dao.setPrecinto(precintos);
             dao.InsertarPrecinto();
         }
         catch(SQLException e){
           throw new SQLException(e.getMessage());
         }
     }
     /**
    * Metodo ExisteSerie , Metodo Verifica si una serie ya existe
    * @autor : Ing. Leonardo Parody
    * @param : long inicio
    * @param : long fin
    * @version : 1.0
    */
     public List ExisteSerie(long inicio, long fin, String tipo_documento) throws SQLException{
         try{
             return dao.ExisteSerie(inicio, fin, tipo_documento);
         }
         catch(SQLException e){
           throw new SQLException(e.getMessage());
         }
     }
    
   /**
    * Metodo AnularPrecinto , Metodo que Anula Precintos
    * @autor : Ing. Leonardo Parody
    * @param : long inicio
    * @param : long fin
    * @version : 1.0
    */
     public void EliminarPrecinto(String precinto, String tipo_documento) throws SQLException{
         try{
             dao.EliminarPrecinto(precinto, tipo_documento);
         }
         catch(SQLException e){
           throw new SQLException(e.getMessage());
         }
     }
    
     /**
    * Metodo SerieAsignada , Metodo Verifica si una serie ya esta asignada
    * @autor : Ing. Leonardo Parody
    * @param : long inicio
    * @param : long fin
    * @version : 1.0
    */
     public List SerieAsignada(long inicio, long fin, String tipo_documento) throws SQLException{
         try{
             return dao.SerieAsignada(inicio, fin, tipo_documento);
         }
         catch(SQLException e){
           throw new SQLException(e.getMessage());
         }
     }
    
    /**
     * Busca un numero de precinto o sticker para ver si existe
     * @autor Ing. Karen Reales
     * @param agencia C�digo de la agencia
     * @param numero
     * @returns true o false
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public boolean existe(String agencia, String numero, String tipo)throws SQLException{
        return dao.existe(agencia, numero, tipo);
    }
     /**
     * Actualiza el precinto con el numero de planilla que lo utilizo y la fecha de utilizacion
     * @autor Ing. Karen Reales
     * @returns String con el comando para inserccion
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public String utilizar(Precinto p)throws SQLException{
        dao.setPrecinto(p);
        return dao.utilizar();
    }    
    
 
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
     /**
     * Obtiene los stickers utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @return <code>Vector</code> con los precintos no utilizados
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @see com.tsp.operation.model.DAOS.PrecintosDAO#stickersUtilizados(String, String, String)
     * @version 1.0
     */
    public void stickersUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        dao.stickersUtilizados(agencia, fechaI + " 00:00", fechaF + " 23:59");
    }
    /**
     * Obtiene los precintos no utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @returns <code>Vector</code> con los precintos no utilizados
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @see com.tsp.operation.model.DAOS.PrecintosDAO#precintosNoUtilizados(String, String, String
     * @version 1.0
     */
    public void precintosNoUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        dao.precintosNoUtilizados(agencia, fechaI + " 00:00", fechaF + " 23:59");
    }
     /**
     * Obtiene los precintos utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @returns <code>Vector</code> con los precintos no utilizados
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @see com.tsp.operation.model.DAOS.PrecintosDAO#precintosUtilizados(String, String, String)
     * @version 1.0
     */
    public void precintosUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        dao.precintosUtilizados(agencia, fechaI + " 00:00", fechaF + " 23:59");
    }
    /**
     * Obtiene los stickers no utilizados por una o todas las agencias
     * en un per�odo determinado
     * @autor Ing. Tito Andr�s Maturana
     * @param agencia C�digo de la agencia
     * @param fechaI Fecha de inicio del reporte
     * @param fechaF Fecha final del reporte
     * @return <code>Vector</code> con los precintos no utilizados
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @see com.tsp.operation.model.DAOS.PrecintosDAO#stickersNoUtilizados(String, String, String
     * @version 1.0
     */
    public void stickersNoUtilizados(String agencia, String fechaI, String fechaF)throws SQLException{
        dao.stickersNoUtilizados(agencia, fechaI + " 00:00", fechaF + " 23:59");
    }
     /**
     * Actualiza el precinto dejando vacio el campo de planilla de todos los precintos
     * marcados con ese numero de planilla.
     * @autor Ing. Karen Reales
     * @params  Numero de la planilla
     * @returns String con el comando para inserccion
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public String desmarcar(Precinto p)throws SQLException{
        dao.setPrecinto(p);
        return dao.desmarcar();
    }
     /**
     * Busca un numero de precinto o sticker para ver si ha sido utilizado
     * por otra planilla que no sea la que se esta modificando
     * @autor Ing. Karen Reales
     * @param agencia C�digo de la agencia
     * @param numero
     * @param numero de la planilla
     * @returns true o false
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public boolean existe(String agencia, String numero, String tipo,String numpla)throws SQLException{
        return dao.existe(agencia,numero,tipo,numpla);
    }
    
    /**
     * Consulta la agencia, el usuario que creo la serie determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param tipo_doc Precinto - Sticker
     * @param inicio Inicio de la serie
     * @param fin Fin de la serie
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void consultar(String agencia, String tipo_doc, long inicio, long fin)throws SQLException{
        dao.consultar(agencia, tipo_doc, inicio, fin);
    }
       /**
     * Actualiza el precinto con el numero de planilla que lo utilizo y la fecha de utilizacion
     * @autor Ing. Karen Reales
     * @returns String con el comando para inserccion
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public String reutilizar(Precinto p)throws SQLException{
        dao.setPrecinto(p);
        return dao.reutilizar();
    }
    
         /**
     * Busca un numero de precinto entre las ordenes de carga no anuladas realizadas en una 
     *agencia para verificar si existe o no
     * @autor Ing. Karen Reales
     * @param agencia C�digo de la agencia
     * @param numero de precinto
     * @returns true o false
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public boolean existeOCarga(String agencia, String numero,String dstrct,String ocarga)throws SQLException{
        return dao.existeOCarga(agencia, numero,dstrct,ocarga);
    }
    
      /**
     * Consulta la agencia, el usuario que creo la serie determinada
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param agencia C�digo de la agencia
     * @param tipo_doc Precinto - Sticker
     * @param inicio Inicio de la serie
     * @param fin Fin de la serie
     * @throws SQLException Si se presenta un error en la conexi�n a la base de datos
     * @version 1.0
     */
    public void consultar(String agencia, String tipo_doc)throws SQLException{
        dao.consultar(agencia, tipo_doc);
    }
    
}
