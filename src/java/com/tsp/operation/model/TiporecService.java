/*
 * TiporecService.java
 *
 * Created on 18 de junio de 2005, 09:18 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Jm
 */
public class TiporecService {
    TiporecDAO tpDAO;
    /** Creates a new instance of TiporecService */
    public TiporecService() {
        tpDAO = new TiporecDAO();
    }
    public java.util.Vector getTiposrec() {
        return tpDAO.getTiposr();
    }
    
    /**
     * Setter for property tiposr.
     * @param tiposr New value of property tiposr.
     */
    public void setTiposrec(java.util.Vector tiposr) {
        tpDAO.setTiposr(tiposr);
    }
    
    /**
     * Getter for property tp.
     * @return Value of property tp.
     */
    public Tiporec getTprec() {
        return tpDAO.getTp();
    }
    
    /**
     * Setter for property tp.
     * @param tp New value of property tp.
     */
    public void setTprec(Tiporec tp) {
        tpDAO.setTp(tp);
    }
    
    public void list(String codtipo)throws SQLException{
        tpDAO.list(codtipo);
    }
}
