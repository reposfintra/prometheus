/*
 * HNovedadesClipper.java
 *
 * Created on 8 de abril de 2005, 04:08 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  KREALES
 */
public class HNovedadesClipper extends Thread{
    
    private Model  model;
    private String Fecha;
    private String agencia;
    private String corte;
    private String base;
    private  static final String SQL_NOVEDADES ="select  a.*," +
    "	coalesce(e.horaent,'') as horaent,   " +
    "	coalesce(f.horasal,'') as horasal,    " +
    "  	coalesce(g.fecivia,'') as fecivia,   " +
    "	coalesce(g.horivia,'') as horivia,   " +
    "   	coalesce(h.fececar,'') as fececar,  " +
    "    	coalesce(h.horecar,'') as horecar,     " +
    " 	coalesce(i.fecy,'') as fecy,  " +
    " 	coalesce(i.hory,'') as hory,     " +
    "	coalesce(j.fecedes,'')as fecedes,  " +
    "     	coalesce(j.horedes,'')as horedes,     " +
    "  	coalesce(k.fecsdes,'')as fecsdes,    " +
    "   	coalesce(k.horsdes,'')as horsdes,   " +
    "    	coalesce(l.fecemin,'')as fecemin,   " +
    "    	coalesce(l.horemin,'')as horemin,    " +
    "   	coalesce(m.fecsmin,'') as fecsmin,    " +
    "  	coalesce(m.horsmin,'') as horsmin " +
    " from  (select p.reg_status as estado,      " +
    "        p.base,               " +
    " 	TO_CHAR(p.creation_date,'YYYYMMDD') as creation_date_pla,   " +
    "        p.numpla as planilla,          " +
    "      	p.creation_date,          " +
    "        p.plaveh as placa,         " +
    "     	p.platlr as trailer,           " +
    "        p.pesoreal as pesonetocargue,   " +
    "        p.fecdsp," +
    "        TO_CHAR(p.feccum,'YYYYMMDD') as FECCUM,     " +
    "        TO_CHAR(p.feccum,'HH24MI') as horacum,                 " +
    "        p.cedcon as cedcond,                    " +
    "	n.nombre as nomcond,       " +
    "        p.nitpro as cedprop,                    " +
    "	coalesce(np.nombre,'') as nomprop,       " +
    "       	p.despachador as usuario,               " +
    "	r.std_job_no as stdjob,          " +
    "      	substr(r.remision,3,7)as remision,                " +
    "  	TO_CHAR(NOW(),'YYYYMMDD') as CORTE," +
    "	b.*," +
    "	coalesce(c.bombacpm,'')as bombacpm,    " +
    "	coalesce(c.sucursal,'')as bombacpmsucursal,   " +
    "	coalesce(d.sucursal,'')as bombefecsucursal,  " +
    "	coalesce(z.bombpeaje,'')as bombpeaje,  " +
    "	coalesce(z.sucursal,'')as bombpeajesucursal,     " +
    "	coalesce(c.vlracpm,'0')as vlracpm,   " +
    "	coalesce(d.bombefec,'')as bombefec,    " +
    "	coalesce(d.vlrefec,'0')as vlrefec" +
    "    	from    (	select * " +
    "		  	from 	planilla  " +
    "			where 	feccum between ? and ?  " +
    "             		and  base = ?             " +
    "			and  pesoreal>0                " +
    "			      )p" +
    " 	LEFT OUTER JOIN Nit n ON (n.cedula = p.cedcon )" +
    "	LEFT OUTER JOIN Nit np ON (np.cedula = p.nitpro )          " +
    "	inner join plarem pr on (pr.numpla=p.numpla)    " +
    "        inner join remesa r on (r.numrem=pr.numrem)" +
    "	left OUTER join     (	select 	planilla,            " +
    "				vlr as vlracpm," +
    "			        proveedor_anticipo as bombpeaje,         " +
    "				sucursal     " +
    "			from    movpla          " +
    "			where   concept_code = '03')z  on (p.numpla= z.planilla) " +
    " left OUTER join  (	select planilla,               " +
    "				vlr as vlracpm,         " +
    "				proveedor_anticipo as bombacpm,         " +
    "				sucursal      " +
    "			from    movpla  " +
    "			where   concept_code = '02')c on (p.numpla = c.planilla) " +
    "	left  OUTER join    (	select 	planilla,               " +
    "				vlr as vlrefec,        " +
    "				proveedor_anticipo as bombefec,         " +
    "				sucursal      " +
    "		from    movpla  " +
    "			where   concept_code = '01')d on (p.numpla = d.planilla)" +
    "		inner join   (	select  pla,            " +
    "				full_weight as pllenod,            " +
    "				empty_weight as pvacd,          " +
    "				acpm_gln as galacpm,         " +
    "				ticket_a as peajea,             " +
    "				ticket_b as peajeb,     " +
    "  				ticket_c as peajec,           " +
    "				full_weight_m as pesollmin,               " +
    "				empty_weight_m as pesovacmin,	     " +
    "     				(full_weight_m-empty_weight_m) as pesonetom     " +
    "			from plaaux     )b on (p.numpla = b.pla)" +
    " )a" +
    "	left OUTER join    (	select TO_CHAR(date_time_traffic,'HH24MI')as horaent,        " +
    "			       pla     " +
    "			from   planilla_tiempo    " +
    "			where  time_code ='EPUE')e on (a.planilla = e.pla) " +
    "	left OUTER join     (	select TO_CHAR(date_time_traffic,'HH24MI')as horasal,          " +
    "				pla     from planilla_tiempo    " +
    "			where time_code ='SPUE')f on (a.planilla = f.pla) " +
    "	left OUTER join     (	select TO_CHAR(date_time_traffic,'YYYYMMDD')as fecivia,                " +
    "			       TO_CHAR(date_time_traffic,'HH24MI')as horivia,     " +
    "			       pla     " +
    "			from 	planilla_tiempo    " +
    "			where 	time_code ='IVIA')g on (a.planilla = g.pla) " +
    "	left OUTER join     (	select  TO_CHAR(date_time_traffic,'YYYYMMDD')as fececar," +
    "		                TO_CHAR(date_time_traffic,'HH24MI')as horecar,         " +
    "				pla     " +
    "			from 	planilla_tiempo    " +
    "			where time_code ='ECAR')h on (a.planilla = h.pla) " +
    "	left OUTER join     (select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecy,           " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as hory,             " +
    "				pla     " +
    "			from 	planilla_tiempo    where time_code ='LLY')i on (a.planilla = i.pla)" +
    "	left OUTER join      (select TO_CHAR(date_time_traffic,'YYYYMMDD')as fecedes,                " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as horedes,          " +
    "				pla     " +
    "			  from 	planilla_tiempo    " +
    "			  where time_code ='EDES')j on (a.planilla = j.pla) " +
    "	 left OUTER join     (select TO_CHAR(date_time_traffic,'YYYYMMDD')as fecsdes,                " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as horsdes,          " +
    "				pla     " +
    "			from planilla_tiempo    " +
    "			where time_code ='SDES')k on (a.planilla = k.pla) " +
    "	left OUTER join     (select TO_CHAR(date_time_traffic,'YYYYMMDD')as fecemin,               " +
    "			      TO_CHAR(date_time_traffic,'HH24MI')as horemin,          " +
    "			      pla     " +
    "			from  planilla_tiempo    " +
    "			where time_code ='EMIN')l on (a.planilla = l.pla)" +
    "	left OUTER join     (select TO_CHAR(date_time_traffic,'YYYYMMDD')as fecsmin,                " +
    "			      TO_CHAR(date_time_traffic,'HH24MI')as horsmin,          " +
    "			      pla     " +
    "			from  planilla_tiempo    " +
    "			where time_code ='SDES') m on (a.planilla = m.pla)  order by a.creation_date_pla ";
    
    private String usuario;
    /** Creates a new instance of HNovedadesClipper */
    public HNovedadesClipper() {
    }
    public void start(Model model, String Fecha, String agencia,String corte,String base, String u)throws SQLException {
        try{
            model.LogProcesosSvc.InsertProceso("Creacion Archivos Novedades Clipper", this.hashCode(), "Creacion Archivos Novedades Clipper ", "");
        }catch(Exception e){
            ////System.out.println("Error insertando el log");
        }
        this.model = model;
        this.Fecha = Fecha;
        this.agencia = agencia;
        this.corte = corte;
        this.base = base;
        this.usuario =u;
        super.start();
    }
    public synchronized void run(){
        try {
            
            
            Connection con=null;
            PreparedStatement st = null;
            ResultSet rs = null;
            PoolManager poolManager = null;
            
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            int year=0;
            int month=0;
            int date=0;
            try{
                year=Integer.parseInt(corte.substring(0,4));
                month=Integer.parseInt(corte.substring(5,7))-1;
                date= Integer.parseInt(corte.substring(8,10));
            }catch(Exception e){
                ////System.out.println("Error desarmando la fecha");
            }
            Calendar fecha_cortI = Calendar.getInstance();
            fecha_cortI.set(year,month,date-1,0,00,0);
            //fecha_cortI.set(year,10,1,0,00,0);
            java.util.Date fecha_corti = fecha_cortI.getTime();
            String fecIni =s.format(fecha_corti);
            
            Calendar fecha_cortF = Calendar.getInstance();
            fecha_cortF.set(year,month,date+1,23,59,0);
            //fecha_cortF.set(year,10,30,23,59,0);
            java.util.Date fecha_cortf = fecha_cortF.getTime();
            String fecFin =s.format(fecha_cortf);
            
            try {
                ////System.out.println("Entro al hilo...");
                poolManager = PoolManager.getInstance();
                con = poolManager.getConnection("fintra");
                if(con!=null){
                    ////System.out.println("genero la consulta ");
                    
                    st = con.prepareStatement(SQL_NOVEDADES);
                    st.setString(1,fecIni);
                    st.setString(2,fecFin);
                    st.setString(3,base);
                    ////System.out.println("VOY A IMPRIMIR EL SQL...");
                    ////System.out.println(st.toString());
                    rs = st.executeQuery();
                    
                    ////System.out.println("Voy a empezar a escribir el archivo");
                    ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                    String path = rb.getString("ruta");
                    
                    File file = new File(path + "/exportar/migracion/" + usuario + "/");
                    file.mkdirs();
                    String NombreArchivo = path + "/exportar/migracion/" + usuario + "/cc" + base.toUpperCase()+Fecha.replaceAll("-","")+".txt";
                    PrintStream archivo = new PrintStream(NombreArchivo);
                    
                    ////System.out.println("RUTA " + NombreArchivo);
                    
                    char comilla = '"';
                    while(rs.next()){
                        ////System.out.println("Encontro datos...");
                        archivo.println( comilla+rs.getString("estado").toUpperCase()+comilla+"," +
                        ""+rs.getString("creation_date_pla")+"," +//CREATION_DATE
                        ""+rs.getString("remision")+"," +
                        ""+rs.getString("stdjob")+"," +
                        comilla+""+rs.getString("placa").toUpperCase()+comilla+"," +
                        comilla+""+rs.getString("trailer").toUpperCase()+comilla+"," +
                        ""+rs.getFloat("pllenod")+"," +
                        ""+rs.getFloat("pvacd")+"," +
                        ""+rs.getFloat("pesonetocargue")+"," +
                        ""+rs.getString("FECCUM")+"," +//FECCUM
                        ""+rs.getString("cedcond")+"," +
                        comilla+""+rs.getString("nomcond").toUpperCase()+comilla+"," +
                        ""+rs.getFloat("galacpm")+"," +
                        ""+rs.getFloat("vlracpm")+"," +
                        ""+rs.getString("bombacpm")+"," +
                        comilla+""+rs.getString("bombacpmsucursal").toUpperCase()+comilla+"," +
                        ""+rs.getFloat("vlrefec")+"," +
                        ""+rs.getString("bombefec")+"," +
                        comilla+""+rs.getString("bombefecsucursal").toUpperCase()+comilla+"," +
                        ""+rs.getInt("peajea")+"," +
                        ""+rs.getInt("peajeb")+"," +
                        ""+rs.getInt("peajec")+"," +
                        ""+rs.getString("bombpeaje")+"," +
                        comilla+""+rs.getString("bombpeajesucursal").toUpperCase()+comilla+"," +
                        ""+rs.getString("cedprop")+"," +
                        comilla+""+rs.getString("nomprop").toUpperCase()+comilla+"," +
                        ""+rs.getString("horaent")+"," +//EPUE
                        ""+rs.getString("horacum")+"," +//FECCUM
                        ""+rs.getString("fecsmin")+"," +//SDES
                        ""+rs.getString("horsmin")+"," +//SDES
                        comilla+""+rs.getString("usuario").toUpperCase()+comilla+"," +
                        ""+rs.getString("fecivia")+"," +//IVIA
                        ""+rs.getString("horivia")+"," +//IVIA
                        ""+rs.getString("fececar")+"," +//ECAR
                        ""+rs.getString("horecar")+"," +//ECAR
                        ""+rs.getFloat("pesollmin")+"," +
                        ""+rs.getFloat("pesovacmin")+"," +
                        ""+rs.getFloat("pesonetom")+"," +
                        ""+rs.getString("fecy")+"," +//LLY
                        ""+rs.getString("hory")+"," +//LLY
                        ""+rs.getString("fecedes")+"," +//EDES
                        ""+rs.getString("horedes")+"," +//EDES
                        ""+rs.getString("fecsdes")+"," +//SDES
                        ""+rs.getString("horsdes")+"," +//SDES
                        ""+rs.getString("fecemin")+"," +//EMIN
                        ""+rs.getString("horemin")//EMIN
                        );
                    }
                    archivo.close();
                    
                }
                
            }catch(Exception e){
                throw new Exception("ERROR ESCRIBIENDO EL ARCHIVO DE NOVEDADES " + e.getMessage() );
            }
            finally{
                if (st != null){
                    try{
                        st.close();
                    }
                    catch(Exception e){
                        throw new Exception("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                    }
                }
                
                if (con != null){
                    poolManager.freeConnection("fintra", con);
                }
            }
            model.LogProcesosSvc.finallyProceso("Creacion Archivos Novedades Clipper", this.hashCode(),"","PROCESO FINALIZADO CON EXITO." );
        }
        catch(Exception e){
            ////System.out.println("Ocurrio un error "+e.toString());
            try{
                model.LogProcesosSvc.finallyProceso("Creacion Archivos Novedades Clipper", this.hashCode(),"","ERROR :" + e.getMessage());
            }
            catch(Exception f){
                
            }
            //throw new Exception("ERROR ESCRIBIENDO EL ARCHIVO DE NOVEDADES " + e.getMessage());
        }
    }
}
