/*
 * veh_externosService.java
 *
 * Created on 30 de julio de 2005, 02:16 PM
 */

package com.tsp.operation.model;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;

/**
 *
 * @author  jescandon
 */
public class veh_externosService {
    private veh_externosDAO    VEDataAccess;
    private List               ListaVE;
    private veh_externos       Datos;
    
    /** Creates a new instance of veh_externosService */
    public veh_externosService() {
        VEDataAccess = new veh_externosDAO();
    }
    
    public void INSERT(String placa, String fecha_disp, int tiempovigencia, String agencia_disp, String base, String usuario, String distrito ) throws Exception {
        try{
            VEDataAccess.INSERT_VE(placa, fecha_disp, tiempovigencia, agencia_disp, base, usuario, distrito);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [veh_externosService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATE( int tiempovigencia, String agencia_disp, String base, String usuario, String placa, java.sql.Timestamp  fecha_disp, String vplaca , String vfecha_disp) throws Exception {
        try{
            VEDataAccess.UPDATE_VE( tiempovigencia, agencia_disp, base, usuario, placa,fecha_disp, vplaca, vfecha_disp);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [veh_externosService]...\n"+e.getMessage());
        }
    }
    
    public void UPDATEESTADO(String estado, String usuario, String placa , java.sql.Timestamp fecha_disp) throws Exception {
        try{
            VEDataAccess.UPDATEE_VE(estado, usuario, placa, fecha_disp);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATEESTADO [veh_externosService]...\n"+e.getMessage());
        }
    }
    
    
    public void LIST( String agencia_disp ) throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaVE = VEDataAccess.LIST_VE(agencia_disp);
        }
        catch(Exception e){
            throw new Exception("Error en LIST [veh_externosService]...\n"+e.getMessage());
        }
    }
    
    public void LISTALL() throws Exception {
        try{
            this.ReiniciarLista();
            this.ListaVE = VEDataAccess.LIST_ALL();
        }
        catch(Exception e){
            throw new Exception("Error en LISTALL [veh_externosService]...\n"+e.getMessage());
        }
    }
    
    public void SEARCH(String placa, java.sql.Timestamp fecha_disp )throws Exception {
        try{
            this.ReiniciarDato();
            this.Datos = VEDataAccess.SEARCH_VE(placa, fecha_disp);
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [veh_externosService]...\n"+e.getMessage());
        }
    }
    
    public boolean EXISTE(String placa, java.sql.Timestamp fecha_disp )throws Exception {
        try{
            return VEDataAccess.EXISTE_VE(placa, fecha_disp);
        }
        catch(Exception e){
            throw new Exception("Error en EXISTE [veh_externosService]...\n"+e.getMessage());
        }
    }
    
     public void DELETE (String placa, java.sql.Timestamp fecha_disp ) throws Exception {        
        try{
            VEDataAccess.DELETE_VE(placa, fecha_disp);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [veh_externosService]...\n"+e.getMessage());        
        }        
    }

      public int VEXAGENCIA(String Agencia)throws Exception {
        try{
            return VEDataAccess.LIST_VExAgencias(Agencia);
        }
        catch(Exception e){
            throw new Exception("Error en VEXAGENCIA [veh_externosService]...\n"+e.getMessage());
        }
    }
    
    public void ReiniciarDato(){
        this.Datos = null;
    }
    public void ReiniciarLista(){
        this.ListaVE = null;
    }
    
    public veh_externos getDato(){
        return this.Datos;
    }
    
    public List getList(){
        return this.ListaVE;
    }
    
}
