/*
 * RemPlaService.java
 *
 * Created on 23 de noviembre de 2004, 10:29 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class RemPlaService {
    
    /** Creates a new instance of RemPlaService */
    private RemPlaDAO rempla;
    
    public RemPlaService() {
        rempla = new RemPlaDAO();
    }
    
    public String insertRemesa(RemPla rem, String base)throws SQLException{
        try{
           rempla.setRemPla(rem);
           return rempla.insertRemPla(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
     public RemPla getRemPla()throws SQLException{
        
        return rempla.getRemPla();
        
    }
    public void buscaRemPla(String pla, String rem)throws SQLException{
        try{
            rempla.searchRempla(pla, rem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaRemPla(String pla)throws SQLException{
        try{
            rempla.searchRempla(pla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaRemPlaR(String rem)throws SQLException{
        try{
            rempla.searchRemplaRem(rem);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String anularPlanilla(RemPla rpla)throws SQLException{
        String sql = "";
        try{
            rempla.setRemPla(rpla);
            sql =rempla.anularRemPla();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        return sql;
    }

    
    public void anularRemPlaR(RemPla rpla)throws SQLException{
        try{
            rempla.setRemPla(rpla);
            rempla.anularRemPlaR();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public String cumplirRemPla(RemPla rpla)throws SQLException{
        try{
            rempla.setRemPla(rpla);
            return rempla.cumplirRemPla();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public boolean estaRempla(String numpla, String numrem)throws SQLException{
        return rempla.estaRempla(numpla, numrem);
    }
    public boolean estaRemplaImp(String numpla, String numrem)throws SQLException{
        return rempla.estaRemplaImp(numpla, numrem);
    }
    public boolean estaRemplaImpNoCump(String planilla, String remesa )throws SQLException{
        return rempla.estaRemplaImpNoCump(planilla, remesa);
    }
    public String updateRemPla(RemPla rp)throws SQLException{
        rempla.setRemPla(rp);
        return rempla.updateRemPla();
    }

    
      /**
     *Metodo para anular todas las remesas relacionadas a una planilla
     *@autor: Karen Reales
     *@param: numpla es el numreo de la planilla con la cual se relaciona la remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String anularListaPlanilla(String numpla)throws SQLException{
        return rempla.anularListaPlanilla(numpla);
    }
        
        
}
