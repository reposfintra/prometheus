/*
 * ReporteRegistroTiempoXLS.java
 *
 * Created on 3 de diciembre de 2004, 12:39
 */

package com.tsp.operation.model;
import java.util.*;
import java.io.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author Mario Fontalvo
 */
public class HReportePlacaPesos extends Thread{
    
    private Vector reporte;
    private String fec1;
    private String fec2;
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReportePlacaPesos() {
    }
    public void start(Vector reporte, String fec1,String fec2) {
        this.reporte        = reporte;
        this.fec1 = fec1;
        this.fec2= fec2;
        super.start();
    }
    
    public synchronized void run(){
        try{
            fec1 = fec1.replaceAll("-","");
            String nombreArch= "ReportePlacaPesos["+fec1+"]["+fec2+"].xls";
            String       Hoja  = "ViajesPlaca";
            String       Ruta  = "/exportar/masivo/Cortes/ConciliacionPlaca/" + nombreArch;
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFCell     cell  = null;
            
            for (int col=0; col<40 ; col++)
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.ORANGE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /****************************************************/
            
            sheet.createFreezePane(0,5);
            
            
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<7;j++) {	
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1); 
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte Pesos Placas");
            
            /*************************************************************************************/
            
            /***** RECORRER LAS PLANILLAS ******/
            int Fila = 4;
            
            row  = sheet.createRow((short)(Fila));
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cedula Propietario");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Nombre Propietario");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Placa");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha Viaje");
            
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Peso lleno Mina");
            
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Peso lleno Carbosan");
            
            
            for (int i  = 0; i<reporte.size();i++){
                
                Planilla pla = (Planilla) reporte.elementAt(i);
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pla.getNitpro());
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pla.getNomCond());
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pla.getPlaveh());
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pla.getFecdsp());
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pla.getPesoM());
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo4);
                cell.setCellValue(pla.getPesoP());
                
                
            }// end while de planillas
            
            /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
        }catch(Exception e){
            ////System.out.println(e.getMessage());
        }finally{
            super.destroy();
        }
    }
}
