/*
 * peajeService.java
 *
 * Created on 3 de diciembre de 2004, 05:57 PM
 */

package com.tsp.operation.model;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  KREALES
 */
public class PeajeService {
    
    private PeajeDAO peaje;
    
    /** Creates a new instance of peajeService */
    public PeajeService() {
        peaje= new PeajeDAO();
    }
     public List getPeajes(String dstrct )throws SQLException{
        
        List peajes=null;
        
        peaje.searchPeajes(dstrct);
        peajes= peaje.getList();
        
        return peajes;
        
    }
    
   
    public Peajes get( )throws SQLException{
        
        return peaje.getPeaje();
        
    }
     public boolean exist(String tiket )throws SQLException{
        
        return peaje.exist(tiket);
        
    }
      public boolean existList(String dstr )throws SQLException{
        
        return peaje.existList(dstr);
        
    }
    
    
    public void buscar(String tiket)throws SQLException{
        try{
            peaje.search(tiket);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void insert(Peajes p, String base)throws SQLException{
        try{
            peaje.set(p);
            peaje.insert(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void anular(Peajes p)throws SQLException{
        try{
            peaje.set(p);
            peaje.anular();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void modificar(Peajes p)throws SQLException{
        try{
            peaje.set(p);
            peaje.update();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
}
