/*
 * CarroceriaService.java
 *
 * Created on 9 de noviembre de 2004, 02:53 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  AMENDEZ
 */
public class CarroceriaService {
    
    private CarroceriaDAO carrDataAccess;
    /** Creates a new instance of CarroceriaService */
    public CarroceriaService() {
        carrDataAccess = new CarroceriaDAO();
    }
    
    public TreeMap listar() throws SQLException{
        carrDataAccess.listar();
        
        return carrDataAccess.getCbxCarr();
    }
    public String getNombreCarroceria(String codigo) throws SQLException{
        return carrDataAccess.getNombreCarroceria(codigo);
    }
    
}
