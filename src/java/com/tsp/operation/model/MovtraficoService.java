/*
 * MovtraficoService.java
 *
 * Created on 18 de junio de 2005, 08:38 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Jm
 */
public class MovtraficoService {
    MovtraficoDAO mvDAO;
    /** Creates a new instance of MovtraficoService */
    public MovtraficoService() {
        mvDAO = new MovtraficoDAO();
    }
    public java.util.Vector getMov() {
        return mvDAO.getMovimientos();
    }
    
    /**
     * Setter for property movimientos.
     * @param movimientos New value of property movimientos.
     */
    public void setMov(java.util.Vector movimientos) {
        mvDAO.setMovimientos(movimientos);
    }
    
    /**
     * Getter for property mv.
     * @return Value of property mv.
     */
    public com.tsp.operation.model.beans.Movtrafico getMovimientos() {
        return mvDAO.getMv();
    }
    
    /**
     * Setter for property mv.
     * @param mv New value of property mv.
     */
    public void setMovimientos(com.tsp.operation.model.beans.Movtrafico mv) {
        mvDAO.setMv(mv);
    }
    public void listME(String p)throws SQLException{
        mvDAO.listMovEntrega(p);
    }
    public void listMT(String p)throws SQLException{
        mvDAO.listMov(p);
    }

}
