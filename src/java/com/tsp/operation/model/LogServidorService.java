/*
 * LogServidorService.java
 *
 * Created on 18 de febrero de 2005, 03:36 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  KREALES
 */
public class LogServidorService {
    
    private Vector archivo=null;
    private String nombreArchivo = "";
    /** Creates a new instance of LogServidorService */
    public LogServidorService() {
    }
    public Vector getVector(){
        return archivo;
    }
    public String getNombre(){
        return nombreArchivo;
    }
    public void leerArchivo() throws IOException{
        
        File f = new File("/exportar/masivo/Sincronizacion/log_Sincronizacion_Servidor.txt");
        if(f.exists()){
            Vector derecho = new Vector();
            BufferedReader in = new BufferedReader(new FileReader("/exportar/masivo/Sincronizacion/log_Sincronizacion_Servidor.txt"));
            String row="";
            int sw=1;
            while(sw==1){
                try {
                    row = in.readLine();
                    if(row==null)
                        sw=0;
                    else{
                        derecho.add(row);
                    }
                }catch(IOException e){
                    e.printStackTrace();
                }
                
            }
            if(derecho.size()>0){
                archivo = new Vector();
                archivo.add(derecho.elementAt(0));
                for(int i=derecho.size()-1; i>0; i--){
                    archivo.add(derecho.elementAt(i));
                }
            }
        }
    }
    public void leerArchivo(String nombre) throws IOException{
        nombreArchivo = nombre;
        File f = new File("/exportar/masivo/Sincronizacion/servidor/Journal/"+nombre);
        if(f.exists()){
            archivo= new Vector();
            BufferedReader in = new BufferedReader(new FileReader("/exportar/masivo/Sincronizacion/servidor/Journal/"+nombre));
            String row="";
            int sw=1;
            while(sw==1){
                try {
                    row = in.readLine();
                    if(row==null)
                        sw=0;
                    else{
                        archivo.add(row);
                    }
                }catch(IOException e){
                    e.printStackTrace();
                }
                
            }
            
        }
    }
    
    
}


