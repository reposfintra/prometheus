/*
 * veh_externosDAO.java
 *
 * Created on 30 de julio de 2005, 12:40 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;

/**
 *
 * @author  jescandon
 */
public class veh_externosDAO {
    private static String SQLInsert     =   "   INSERT INTO VEH_EXTERNOS                                                   "+
                                            "   ( PLACA, FECHA_DISP, TIEMPO_VIGENCIA, AGENCIA_DISP, BASE, CREATION_USER, dstrct  ) "+
                                            "   VALUES ( ?, ?, ?, ?, ?, ?, ? )                                                ";
    
    
    private static String SQLUpdate     =   "   UPDATE VEH_EXTERNOS SET                                                                                     "+
                                            "   TIEMPO_VIGENCIA = ?, AGENCIA_DISP = ?,                                                                      "+
                                            "   BASE = ?, USER_UPDATE = ?, PLACA = ?, FECHA_DISP = ? WHERE PLACA = ?  AND FECHA_DISP = ?                    ";
    
    private static String SQLUpdateE    =  "   UPDATE VEH_EXTERNOS SET                                      "+
                                           "   REG_STATUS = ?, USER_UPDATE = ?, LAST_UPDATE = ?            "+
    "   WHERE PLACA = ? AND FECHA_DISP = ?                          ";
    
    private static String SQLDelete  =  "   DELETE FROM VEH_EXTERNOS                                    "+
    "   WHERE PLACA = ? AND FECHA_DISP = ?                          ";
    
    private static String SQLList =     "   SELECT PLACA , FECHA_DISP, TIEMPO_VIGENCIA, AGENCIA_DISP,   "+
    "   BASE, REG_STATUS FROM VEH_EXTERNOS WHERE AGENCIA_DISP = ?   ";
    
    private static String SQLExist =    "   SELECT PLACA , FECHA_DISP, TIEMPO_VIGENCIA, AGENCIA_DISP,               "+
    "   BASE, REG_STATUS FROM VEH_EXTERNOS WHERE PLACA = ? AND FECHA_DISP = ?   ";
    
    
    private static String SQLListALL =    "   SELECT PLACA , FECHA_DISP, TIEMPO_VIGENCIA, AGENCIA_DISP,   "+
    "   BASE, REG_STATUS FROM VEH_EXTERNOS                          ";
    
    private static String SQLVehExAgencias =  " SELECT COUNT(placa) FROM VEH_EXTERNOS WHERE AGENCIA_DISP = ?     ";
   
    /** Creates a new instance of veh_externosDAO */
    public veh_externosDAO() {
    }
    
    public void INSERT_VE(String placa, String fecha_disp, int tiempovigencia, String agencia_disp, String base, String usuario, String distrito ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLInsert);
            st.setString(1,  placa.toUpperCase());
            st.setString(2,  fecha_disp);
            st.setInt(3,  tiempovigencia);
            st.setString(4,  agencia_disp);
            st.setString(5,  base);
            st.setString(6,  usuario);
            st.setString(7, distrito);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_VH [veh_externosDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public void UPDATE_VE( int tiempovigencia, String agencia_disp, String base, String usuario, String placa , java.sql.Timestamp fecha_disp, String vplaca, String vfecha_disp ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLUpdate);
            st.setInt(1,  tiempovigencia);
            st.setString(2, agencia_disp );
            st.setString(3,  base);
            st.setString(4,  usuario);
            st.setString(5,  placa );
            st.setTimestamp(6,  fecha_disp);
            st.setString(7,  vplaca);
            st.setString(8,  vfecha_disp);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATE_VH [veh_externosDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public void UPDATEE_VE(String estado, String usuario, String placa , java.sql.Timestamp fecha_disp) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        try{
            
            st= con.prepareStatement(this.SQLUpdateE);
            st.setString(1, estado);
            st.setString(2, usuario);
            st.setTimestamp(3, Util.ConvertiraTimestamp(Util.getFechaActual_String(6)));
            st.setString(4, placa);
            st.setTimestamp(5, fecha_disp);
            st.executeUpdate();
        }catch(Exception e){
            throw new SQLException("Error en rutina UPDATEE_VE [veh_externosDAO]... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            poolManager.freeConnection("fintra",con);
        }
    }
    
    public veh_externos SEARCH_VE(String placa, java.sql.Timestamp fecha_disp ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        veh_externos datos = null;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLExist);
            st.setString(1, placa);
            st.setTimestamp(2, fecha_disp);
            rs = st.executeQuery();
            while(rs.next()){
                datos = veh_externos.load(rs);
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina SEARCH_VH [veh_externosDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return datos;
    }
    
    
    public boolean EXISTE_VE(String placa, java.sql.Timestamp fecha_disp ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag = false;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLExist);
            st.setString(1, placa);
            st.setTimestamp(2, fecha_disp);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina EXISTE_VH [veh_externosDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return flag;
    }
    
    public  void DELETE_VE(String placa, java.sql.Timestamp fecha_disp ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLDelete);
            st.setString(1,  placa);
            st.setTimestamp(2,  fecha_disp);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETE_VH [veh_externosDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public List LIST_VE(String agencia_disp) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        String consulta      = agencia_disp.equalsIgnoreCase("ALL")?this.SQLListALL:this.SQLList;
        try{
            st = con.prepareStatement(consulta);
            if (!agencia_disp.equalsIgnoreCase("ALL"))
                st.setString(1, agencia_disp);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(veh_externos.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST_VE [veh_externosDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    public List LIST_ALL() throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLListALL);
            ////System.out.println(st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(veh_externos.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST_ALL [veh_externosDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    public int LIST_VExAgencias(String Agencia) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        int vehA             = 0;
        try{
            st = con.prepareStatement(this.SQLVehExAgencias);
            st.setString(1, Agencia);
            ////System.out.println(st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    vehA = rs.getInt(1);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST_ALL [veh_externosDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return vehA;
    }
}
