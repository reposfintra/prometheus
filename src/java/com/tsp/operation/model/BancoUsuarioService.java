/*
 * BancoUsuarioService.java
 *
 * Created on 10 de junio de 2005, 04:17 PM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.*;
import java.sql.*;
/**
 *
 * @author  kreales
 */
public class BancoUsuarioService {
    
    BancoUsuarioDAO banco;
    TreeMap listBUsuarios;
    TreeMap listBUsuariosOtros;
    /** Creates a new instance of BancoUsuarioService */
    public BancoUsuarioService() {
        banco = new BancoUsuarioDAO();
    }
    public BancoUsuarioService(String dataBaseName) {
        banco = new BancoUsuarioDAO(dataBaseName);
    }
    
    public java.util.TreeMap getListaBancos(String user) throws SQLException{
        banco.buscarBancos(user);
        return banco.getListaBancos();
    }
    
    /**
     * Este metodo hace casi lo mismo que getListaBancos( String user )
     * Pero con la diferencia que crea objetos bancos y los devuelve
     * en un java.util.Vector.
     */
    public Vector obtenerBancosDeUsuario(String user)throws SQLException{
        return banco.obtenerBancosDeUsuario(user);
    }
    
    public void eliminarBancoDeUsuario(Banco b, String usuario) throws SQLException {
        banco.eliminarBancoDeUsuario(b, usuario);
    }
    
    public void eliminarBancosDeUsuario( String usuario) throws SQLException {
        banco.eliminarBancosDeUsuario(usuario);
    }
    
    public void agregarBancosDeUsuario(String [] nuevos, String usuario)throws SQLException{
        banco.agregarBancosDeUsuario(nuevos, usuario);
    }
    
    /**
     * Getter for property listBUsuarios.
     * @return Value of property listBUsuarios.
     */
    public java.util.TreeMap getListBUsuarios() {
        return listBUsuarios;
    }
    
    /**
     * Setter for property listBUsuarios.
     * @param listBUsuarios New value of property listBUsuarios.
     */
    public void setListBUsuarios(java.util.TreeMap listBUsuarios) {
        this.listBUsuarios = listBUsuarios;
    }
    
    /**
     * Metodo que busca los bancos dado un usuario.
     * @param: id del usuario
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void buscarBancos(String user)throws SQLException{
        banco.buscarBancos(user);
        listBUsuarios = banco.getListaBancos();
    }
    /**
     * Metodo que busca los bancos dado un usuario, pero como en el programa
     *de reanticipos se necesita mostrar los bancos del usuario en sesion entonces
     *por eso se crea en otra variable los bancos de otros usuarios.
     * @param: id del usuario
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void buscarBancosOtros(String user)throws SQLException{
        banco.buscarBancos(user);
        listBUsuariosOtros = banco.getListaBancos();
    }
    
    /**
     * Getter for property listBUsuariosOtros.
     * @return Value of property listBUsuariosOtros.
     */
    public java.util.TreeMap getListBUsuariosOtros() {
        return listBUsuariosOtros;
    }
    
    /**
     * Setter for property listBUsuariosOtros.
     * @param listBUsuariosOtros New value of property listBUsuariosOtros.
     */
    public void setListBUsuariosOtros(java.util.TreeMap listBUsuariosOtros) {
        this.listBUsuariosOtros = listBUsuariosOtros;
    }
    
}
