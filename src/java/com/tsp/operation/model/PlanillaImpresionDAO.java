
/*********************************************************************
 * Nombre      ............... PlanillaImpresionDAO.java
 * Descripcion ............... Opciones de impresion de planillas
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 16 - 2004
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 *********************************************************************/



package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
import java.text.*;
import java.math.*;
import com.tsp.operation.model.DAOS.MainDAO;



public class PlanillaImpresionDAO  extends MainDAO {
    
    private Vector plas;
    /** Creates a new instance of PlanillaImpresionDAO */
    private final String SQL_BUSCAR_CIA =
    "  SELECT DSTRCT, NOMBRE_ASEGURADORA, POLIZA_ASEGURADORA, FECHA_VEN_POLIZA, CODIGO_REGIONAL, CODIGO_EMPRESA, RANGO_INICIAL, RANGO_FINAL, RESOLUCION "+
    "  FROM CIA WHERE DSTRCT = ?  ";
    
    private final String SQL_BUSCAR_PNI =
    //mfontalvo
    " SELECT  " +
    "          a.numpla,                               " +
    "          to_char(a.fecdsp,'YYYY-MM-dd') as fecdsp,  " +
    "          a.oripla,                               " +
    "          get_nombreciudad( a.oripla) AS nomciu,  " +
    "          a.despla,                               " +
    "          get_nombreciudad( a.despla) AS nomciu,  " +
    "          a.ruta_pla,                             " +
    "          a.plaveh,                               " +
    "          k.descripcion,                          " +
    "          d.modelo,                               " +
    "          d.serial,                               " +
    "          l.descripcion,                          " +
    "          d.dimcarroceria,                        " +
    "          d.noejes,                               " +
    "          a.pesoreal,                             " +
    "          e.cedula,                               " +
    "          e.nombre,                               " +
    "          e.direccion,                            " +
    "          e.telefono,                             " +
    "         get_nombreciudad( e.codciu) AS nomciu," +
    "          g.cedula,                               " +
    "          g.nombre,                               " +
    "          g.direccion,                            " +
    "          g.telefono,                             " +
    "          get_nombreciudad( g.codciu) AS nomciu,  " +
    "          i.cedula,                               " +
    "          i.nombre,                               " +
    "          i.direccion,                            " +
    "          i.telefono,                             " +
    "          get_nombreciudad( i.codciu) AS nomciu,  " +
    "          a.printer_date,                         " +
    "          m.vlr,                                  " +
    "          m.vlr_for,                              " +
    "          m.currency,                             " +
    "          a.despachador,                          " +
    "          a.reg_status                            " +
    "         FROM planilla a                                             " +
    "               LEFT OUTER JOIN placa d on (d.placa  = a.plaveh)      " +
    "               LEFT OUTER JOIN nit e on (e.cedula   = a.nitpro)      " +
    "               LEFT OUTER JOIN nit g on (g.cedula   = a.nitpro )     " +
    "               LEFT OUTER JOIN nit i on (i.cedula   = a.cedcon)      " +
    "               LEFT OUTER JOIN marca_placa k on (k.codigo = d.marca) " +
    "               LEFT OUTER JOIN color_placa l on (l.codigo= d.color)  " +
    "           LEFT OUTER JOIN (select sum(vlr) as vlr, sum(vlr_for) as vlr_for, currency, planilla from movpla where concept_code = '01'  and reg_status= '' group by planilla, currency) m on (m.planilla = numpla)   " +
    "         WHERE                                      " +
    "               a.printer_date = '0099-01-01 00:00'  " +
    "               and a.despachador  = ?               " +
    "               and a.reg_status <> 'A'   " +
    "               AND A.BASE = 'COL'           " +
    "         ORDER BY a.agcpla, a.numpla;               ";
    
    private final String SQL_BUSCAR_PLANILLA_NOIMPRESA=
    //mfontalvo
    "SELECT " +
    "           a.numpla,                              " +
    "     	to_char(a.fecdsp,'YYYY-MM-dd') as fecdsp,  " +
    "     	a.oripla,                              " +
    "     	b.nomciu,                              " +
    "      	a.despla,                              " +
    "     	c.nomciu,                              " +
    "     	a.ruta_pla,                            " +
    "     	a.plaveh,                              " +
    "     	k.descripcion,                         " +
    "     	d.modelo,                              " +
    "     	d.serial,                              " +
    "     	l.descripcion," +
    "   	d.dimcarroceria,                       " +
    "     	d.noejes,                              " +
    "     	a.pesoreal,                            " +
    "           e.cedula,                              " +
    "     	e.nombre,                              " +
    "     	e.direccion,                           " +
    "     	e.telefono,                            " +
    "     	f.nomciu,                              " +
    "     	g.cedula,                              " +
    "     	g.nombre,                              " +
    "     	g.direccion,                           " +
    "     	g.telefono,                            " +
    "     	h.nomciu,                              " +
    "     	i.cedula,                              " +
    "    	i.nombre,                              " +
    "     	i.direccion,                           " +
    "     	i.telefono,                            " +
    "     	j.nomciu,                              " +
    "     	a.printer_date,                        " +
    "           m.vlr,                                 " +
    "     	m.vlr_for,                             " +
    "     	m.currency,                            " +
    "           a.despachador,                         " +
    "           a.reg_status                           " +
    "     FROM planilla a " +
    "           LEFT OUTER JOIN ciudad b on ( b.codciu = a.oripla   )   " +
    "           LEFT OUTER JOIN ciudad c on ( c.codciu = a.despla )  	" +
    "           LEFT OUTER JOIN placa d on (d.placa  = a.plaveh)" +
    "           LEFT OUTER JOIN nit e on (e.cedula   = a.nitpro)" +
    "           LEFT OUTER JOIN ciudad f on (f.codciu   = e.codciu)  " +
    "           LEFT OUTER JOIN nit g on (g.cedula   = a.nitpro )  " +
    "           LEFT OUTER JOIN ciudad h on (  h.codciu = g.codciu)" +
    "           LEFT OUTER JOIN nit i on (i.cedula = a.cedcon)   " +
    "           LEFT OUTER JOIN ciudad j on (j.codciu = i.codciu )     " +
    "           LEFT OUTER JOIN marca_placa k on (k.codigo = d.marca)      " +
    "           LEFT OUTER JOIN color_placa l on (l.codigo= d.color) " +
    "       LEFT OUTER JOIN (select sum(vlr) as vlr, sum(vlr_for) as vlr_for, currency, planilla from movpla where concept_code = '01' and planilla = ? and reg_status= '' group by planilla, currency) m on (m.planilla = numpla)  " +
    "     WHERE a.numpla      =       ?  " +
    "           and a.reg_status <> 'A' " +
    "     ORDER BY a.agcpla, a.numpla              ;";
    
    /* kreales
    "SELECT  	a.numpla, " +
    "	a.fecdsp," +
    "	a.oripla," +
    "	d.nomciu," +
    "	a.despla," +
    "	e.nomciu," +
    "	a.ruta_pla," +
    "	a.plaveh," +
    "	c.MARCA ," +
    "	c.MODELO, " +
    "	c.SERIAL, " +
    "	c.COLOR, " +
    "	c.DIMCARROCERIA, " +
    "	c.NOEJES," +
    "	a.pesoreal," +
    "   h.CEDULA, " +
    "	h.NOMBRE, " +
    "	h.DIRECCION, " +
    "	h.TELEFONO, " +
    "	i.NOMCIU," +
    "	f.CEDULA, " +
    "	f.NOMBRE, " +
    "	f.DIRECCION, " +
    "	f.TELEFONO, " +
    "	g.NOMCIU," +
    "	a.printer_date," +
    "   j.VLR, " +
    "	j.VLR_FOR , " +
    "	j.CURRENCY " +
    "FROM    planilla a," +
    "	nit b," +
    "	placa c," +
    "	ciudad d," +
    "	ciudad e," +
    "	nit f," +
    "	ciudad g," +
    "	nit h," +
    "	ciudad i," +
    "	movpla j " +
    "WHERE   a.printer_date = '0099-01-01 00:00' and" +
    "	b.cedula = a.cedcon and" +
    "	a.plaveh = c.placa  and" +
    "	a.ORIPLA = d.codciu and" +
    "	a.despla = e.codciu and" +
    "	a.cedcon = f.cedula and" +
    "	f.codciu = g.codciu and" +
    "	a.nitpro = h.cedula and" +
    "	f.codciu = i.codciu and" +
    "	j.planilla = a.numpla and" +
    "   A.AGCPLA = ?        AND   " +
    "   A.NUMPLA LIKE  ?   AND   " +
    "   A.PRINTER_DATE LIKE  ? ";
     */
    /*
     mfontlavo
    "  SELECT                                                                                                               " +
    "     A.NUMPLA, A.FECPLA,                                                                                               " +
    "     A.ORIPLA, B.NOMCIU,                                                                                               " +
    "     A.DESPLA, D.NOMCIU, A.RUTA_PLA,                                                                                   " +
    "     A.PLAVEH, F.MARCA , F.MODELO, F.SERIAL, F.COLOR, F.DIMCARROCERIA, F.NOEJES, S.EMPTY_WEIGHT,                       " +
    "     I.CEDULA, I.NOMBRE, I.DIRECCION, I.TELEFONO, J.NOMCIU,                                                            " +
    "     M.CEDULA, M.NOMBRE, M.DIRECCION, M.TELEFONO, N.NOMCIU,                                                            " +
    "     Q.CEDULA, Q.NOMBRE, Q.DIRECCION, Q.TELEFONO, R.NOMCIU,                                                            " +
    "     A.PRINTER_DATE, U.VLR, U.VLR_FOR , U.CURRENCY                                                                     " +
    "   FROM                                                                                                                " +
    "     PLANILLA AS A                                                                                                     " +
    "     LEFT JOIN CIUDAD AS B ON (A.ORIPLA = B.CODCIU)                                                                    " +
    "     LEFT JOIN CIUDAD AS D ON (A.DESPLA = D.CODCIU)                                                                    " +
    "     LEFT JOIN PLACA  AS F ON (A.PLAVEH = F.PLACA )                                                                    " +
    "     LEFT JOIN (                                                                                                       " +
    "          PLACA AS H LEFT JOIN (NIT AS I LEFT JOIN CIUDAD AS J ON (I.CODCIU = J.CODCIU)) ON (H.PROPIETARIO = I.CEDULA) " +
    "     ) ON (A.PLAVEH = H.PLACA)                                                                                         " +
    "     LEFT JOIN (PLACA AS L LEFT JOIN (NIT AS M LEFT JOIN CIUDAD AS N ON (M.CODCIU = N.CODCIU)) ON (L.TENEDOR     = M.CEDULA)) ON (A.PLAVEH = L.PLACA)" +
    "     LEFT JOIN (PLACA AS P LEFT JOIN (NIT AS Q LEFT JOIN CIUDAD AS R ON (Q.CODCIU = R.CODCIU)) ON (P.CONDUCTOR   = Q.CEDULA)) ON (A.PLAVEH = P.PLACA) " +
    "     LEFT JOIN MOVPLA AS U ON (A.NUMPLA = U.PLANILLA AND U.DOCUMENT_TYPE = '001' AND U.CONCEPT_CODE = '01')            " +
    "     LEFT JOIN PLAAUX AS S ON (A.NUMPLA =  S.PLA )                                                                     " +
    "   WHERE                                                                                                               " +
    "     A.AGCPLA = ?         AND  " +
    "     A.NUMPLA LIKE ( ? )  AND  " +
    "     A.PRINTER_DATE LIKE ( ? ) " +
    " ORDER BY                      " +
    "       A.NUMPLA                " ;*/
    
    
    private final String SQL_BUSCAR_REMESAS =
    "SELECT                       " +
    "    b.numrem,                " +
    "    b.docuinterno,           " +
    "    b.std_job_no,            " +
    "    b.cliente,     c.nomcli, " +
    "    c.texto_oc               " +
    "FROM                         " +
    "    plarem  AS a,            " +
    "    remesa  AS b,            " +
    "    cliente AS c             " +
    "WHERE                        " +
    "        a.numpla = ?         " +
    "    and b.numrem = a.numrem  " +
    "    and c.codcli = b.cliente ";
    
    
    private final String SQL_BUSCAR_REMDES =
    " SELECT B.CODIGO, B.NOMBRE, B.CIUDAD, C.NOMCIU                          "+
    " FROM                                                                   "+
    "       REMESADEST AS A LEFT JOIN (REMIDEST AS B LEFT JOIN CIUDAD AS C ON (B.CIUDAD = C.CODCIU) ) ON (A.CODIGO = B.CODIGO) "+
    " WHERE                                                                  "+
    "        A.TIPO   = ?        AND                                         "+
    "        A.NUMREM = ?                                                    ";
    
    private final String SQL_BUSCAR_DESCUENTOS =
    " select               " +
    "    a.concept_code,   " +
    "    b.concept_desc,   " +
    "    a.vlr,            " +
    "    a.currency,       " +
    "    'V'               " +
    " from                 " +
    "    movpla a,         " +
    "    tblcon b          " +
    " where                " +
    "    a.planilla      = ?               " +
    " and b.concept_code = a.concept_code  " +
    " and a.reg_status   = ''              " +
    " and document_type= '001'             " +
    " and a.concept_code not in ('01')     " +
    " order by b.concept_desc              " ;
    
    
    /*
    " SELECT                                                                                                   "+
    "   A.CONCEPT_CODE, B.CONCEPT_DESC, A.VLR::NUMERIC(20,2), A.CURRENCY, A.TYPE                               "+
    " FROM                                                                                                     "+
    "   TBLDES AS A LEFT JOIN TBLCON AS B ON (A.CONCEPT_CODE = B.CONCEPT_CODE AND A.DSTRCT = B.DSTRCT)         "+
    " WHERE                                                                                                    "+
    "   A.STD_JOB_NO LIKE ?                                                                                    ";*/
    private final String SQL_ACTUALIZAR_FECHA_IMPRESION =
    " UPDATE  planilla     "+
    " SET PRINTER_DATE = ? "+
    " WHERE NUMPLA = ?     ";
    
    //variables de exportacion a la ms230828
    private final String CONST_BASE = "spo";
    
    private final String SQL_BUSCAR_PLANILLA =
    " SELECT  A.CIA,A.STD_JOB_NO, B.NUMPLA, B.UNIT_COST,B.PLATLR,B.FECPLA, B.AGCPLA, B.PLAVEH, B.PESOREAL::NUMERIC(20,2), B.CEDCON, A.REMISION  "+
    " FROM  remesa AS A, planilla AS B, plarem AS C            " +
    " WHERE                                                    " +
    "   A.NUMREM = C.NUMREM AND                                " +
    "	B.NUMPLA = C.NUMPLA AND                                " +
    //"	A.STAREM = 'A'      AND                                " +
    "	A.FECREM = ?                                           " +
    "  AND A.BASE = B.BASE AND B.BASE = C.BASE AND A.BASE = ?  " ;
    private final String SQL_BUSCAR_ANTICIPOS =
    " SELECT A.CONCEPT_CODE, B.CODIGO_MIGRACION, A.VLR::NUMERIC(20,2), A.VLR_FOR::NUMERIC(20,2), A.CURRENCY " +
    " FROM                                                                    " +
    "	MOVPLA AS A,  TBLCON AS B                                             " +
    " WHERE                                                                   " +
    " 	A.CONCEPT_CODE  = B.CONCEPT_CODE AND                                  " +
    " 	A.DOCUMENT_TYPE = '001'           AND                                 " +
    "	A.PLANILLA      = ?                                                   " +
    "   order by CONCEPT_CODE desc" ;
    
    
    //"  AND A.BASE = B.BASE AND A.BASE = ?                                     " ;
    private final String SQL_CM_P_ACPM =
    " SELECT                             " +
    "	B.CODIGO_MIGRACION               " +
    " FROM                               " +
    "	PLAAUX AS A, PROVEEDOR_ACPM AS B " +
    " WHERE                              " +
    "	A.ACPM_SUPPLIER   = B.NIT AND    " +
    "	A.PLA = ?                        " ;
    //"  AND A.BASE = B.BASE AND A.BASE = ?" ;
    
    
    
    private final String SQL_CM_P_ANTICIPO =
    " SELECT                                 " +
    "	B.CODIGO_MIGRACION                   " +
    " FROM                                   " +
    "	MOVPLA AS A, PROVEEDOR_ANTICIPO AS B " +
    " WHERE                                  " +
    "	A.PROVEEDOR_ANTICIPO = B.NIT AND     " +
    "   A.DOCUMENT_TYPE  = '001'     AND     " +
    "   A.CONCEPT_CODE   = ?	 AND         " +
    "   A.PLANILLA = ?                       " ;
    //"  AND A.BASE = B.BASE AND A.BASE = ?    " ;
    
    
    
    private final String SQL_DESC_PAPEL =
    "SELECT VLR::NUMERIC(20,2) FROM TBLDES WHERE STD_JOB_NO = ? AND CONCEPT_CODE in( 'DESC','DESC ') "; //AND BASE = ?";
    
    
    //CODIGO DE KAREN REALES
    private final String SQL_DESC_SJ =" SELECT A.CONCEPT_CODE, B.CODIGO_MIGRACION, A.VLR::NUMERIC(20,2) as valor, A.VLR_FOR::NUMERIC(20,2), A.CURRENCY, a.creation_date  " +
    "     FROM                                                                     " +
    "     MOVPLA AS A,  " +
    " TBLCON AS B," +
    " TBLDES AS C" +
    " WHERE                                                                    " +
    "      A.CONCEPT_CODE  = B.CONCEPT_CODE AND" +
    " A.concept_code = C.concept_code AND" +
    "      A.DOCUMENT_TYPE = '001'           AND                                  " +
    "     A.PLANILLA      =?        AND" +
    " c.std_job_no = ?" +
    "   and a.vlr>0" +
    " order by CONCEPT_CODE desc ;";
    ;//BUSCO TODOS LOS DESCUENTOS QUE SE DEBAN APLICAR AL STANDARD
    
    
    private String SQL = "SELECT A.CIA, " +
    "           A.STD_JOB_NO, " +
    "           A.qty_packed, " +
    "           coalesce(A.facturacial,'')AS facturacial, " +
    "           coalesce(A.docuinterno,'')AS docuinterno, " +
    "           A.REMISION,  " +
    "           b.numpla as NUMPLA,  " +
    "           TO_NUMBER(COSTO.unit_cost,'999999999999.99') AS UNIT_COST, " +
    "           B.PLATLR, " +
    "           TO_CHAR(B.corte,'YYYYMMDD')as FECPLA, " +
    "           i.codigo as agcpla, " +
    "           B.PLAVEH, " +
    "           B.PESOREAL::NUMERIC(20,2),  " +
    "           B.CEDCON, " +
    "           coalesce(A.REMITENTE,'') as remitente, " +
    "           coalesce(B.precinto,'') as precinto, " +
    "           coalesce(A.DESTINATARIO,'') as destinatario, " +
    "           b.despachador," +
    "           coalesce(u.nit,'7777777')AS nit " +
    "     FROM  (select * from planilla where feccum between  ? and ? and   base =? and  reg_status <> 'A')AS b" +
    "           INNER JOIN plarem AS C   on (c.numpla=b.numpla)" +
    "           INNER JOIN remesa AS A ON(c.numrem=a.numrem) LEFT JOIN (                select  *  from    STDJOBCOSTFULL                ) COSTO ON (COSTO.SJ = A.STD_JOB_NO )  " +
    "           INNER JOIN ciu_ref as i on (i.codciu = B.AGCPLA)" +
    "           LEFT OUTER JOIN usuarios u ON (u.idusuario = b.despachador) " +
    "         order by a.remision ;";
    
    private String SQL2 = "	SELECT A.CIA, " +
    "               A.STD_JOB_NO, " +
    "               A.qty_packed, " +
    "               TO_NUMBER(COSTO.unit_cost,'999999999999.99') AS UNIT_COST, " +
    "               coalesce(A.facturacial,'')AS facturacial, " +
    "               coalesce(A.docuinterno,'')AS docuinterno, " +
    "               A.REMISION, " +
    "               b.numpla as NUMPLA, " +
    "               B.UNIT_COST, " +
    "               B.PLATLR, " +
    "               TO_CHAR(B.corte,'YYYYMMDD')as FECPLA, " +
    "               i.codigo as agcpla, " +
    "               B.PLAVEH, " +
    "               B.PESOREAL::NUMERIC(20,2), " +
    "               B.CEDCON, " +
    "               coalesce(B.precinto,'') AS precinto, " +
    "               b.despachador," +
    "               coalesce(u.nit,'7777777')AS nit" +
    "	FROM (SELECT * FROM planilla WHERE feccum  between ? AND ? AND reg_status = '' and base = ?) AS B " +
    "                  INNER JOIN plarem C ON (c.numpla = b.numpla) " +
    "                  INNER JOIN remesa A ON (a.numrem = c.numrem) " +
    "                  LEFT JOIN stdjobcostfull COSTO ON (COSTO.SJ = A.STD_JOB_NO ) " +
    "                  LEFT OUTER JOIN ciu_ref i ON (b.agcpla = i.codciu) " +
    "                  LEFT OUTER JOIN usuarios u ON (u.idusuario = b.despachador) " +
    "	ORDER BY a.remision ;" ;
    
    //Karen 25-03-2006
    private String SQL_CARGAGRAL ="SELECT 	A.CIA, " +
    "           A.STD_JOB_NO, " +
    "           A.qty_packed ::NUMERIC(20,2), " +
    "           coalesce(A.facturacial,'')AS facturacial, " +
    "           coalesce(A.docuinterno,'')AS docuinterno, " +
    "           A.numrem as remision,  " +
    "           B.NUMPLA,  " +
    "           B.UNIT_COST, " +
    "           B.PLATLR, " +
    "           TO_CHAR(B.FECPLA,'YYYYMMDD')as FECPLA, " +
    "           B.PLAVEH, " +
    "           B.PESOREAL::NUMERIC(20,2) as cantPlanilla,  " +
    "           B.CEDCON, " +
    "           coalesce(A.REMITENTE,'') as remitente, " +
    "           coalesce(B.precinto,'') as precinto, " +
    "           coalesce(A.DESTINATARIO,'') as destinatario, " +
    "           coalesce(D.fecsal,'')as fecsal, " +
    "           coalesce(D.horsal,'')as horsal, " +
    "           coalesce(E.fececar,'')as fececar, " +
    "           coalesce(E.horecar,'')as horecar, " +
    "           coalesce(F.horscar,'')as horscar, " +
    "           coalesce(G.fecedes,'')as fecedes, " +
    "           coalesce(G.horedes,'')as horedes, " +
    "           coalesce(H.horsdes,'')as horsdes, " +
    "           A.unit_packed, " +
    "           b.contenedores, " +
    "           b.cf_code, " +
    "           a.pesoreal::NUMERIC(20,2) as cantRemesa," +
    "           b.despachador," +
    "           coalesce(u.nit,'7777777')AS nit," +
    "           c.porcent" +
    "     FROM  (Select * from planilla where fecdsp between ? and ? AND BASE = ?  ) AS B" +
    "           INNER JOIN plarem C ON (c.numpla = b.numpla)" +
    "           INNER JOIN remesa A ON (a.numrem = c.numrem  and a.base = 'COL')       " +
    "           LEFT OUTER JOIN usuarios u ON (u.idusuario = b.despachador) " +
    "           LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecsal,  " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as horsal,  " +
    "				pla " +
    "			from    planilla_tiempo " +
    "			where   time_code ='SALI'  ) D ON (b.numpla = d.pla) " +
    "           LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fececar,  " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as horecar,  " +
    "				pla " +
    "			from planilla_tiempo " +
    "			where time_code ='ECAR'  )E ON (b.numpla = E.pla) " +
    "           LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecscar,  " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as horscar,  " +
    "				pla " +
    "			from 	planilla_tiempo " +
    "			where 	time_code ='SCAR'       ) F ON (b.numpla = F.pla) " +
    "           LEFT JOIN (	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecedes,  " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as horedes,  " +
    "				pla " +
    "			from planilla_tiempo " +
    "			where time_code ='EDES' ) G ON (b.numpla = G.pla) " +
    "           LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecsdes,  " +
    "				TO_CHAR(date_time_traffic,'HH24MI')as horsdes,  " +
    "				pla " +
    "			from 	planilla_tiempo " +
    "			where 	time_code ='SDES' ) H ON (B.numpla = H.pla) " +
    "order by a.remision ;  ";
    
    //kreales 25-03-2006
    //consulta para buscar remesas nuevas de planillas viejas.
    private String SQL_CARGAGRAL_REM ="SELECT 	A.CIA,  " +
    "               A.STD_JOB_NO,  " +
    "               A.qty_packed ::NUMERIC(20,2),  " +
    "               coalesce(A.facturacial,'')AS facturacial,  " +
    "               coalesce(A.docuinterno,'')AS docuinterno,  " +
    "               A.numrem as remision,   " +
    "               B.NUMPLA,   " +
    "               B.UNIT_COST,  " +
    "               B.PLATLR,  " +
    "               TO_CHAR(B.FECPLA,'YYYYMMDD')as FECPLA,  " +
    "               B.PLAVEH,  " +
    "               B.PESOREAL::NUMERIC(20,2) as cantPlanilla,   " +
    "               B.CEDCON,  " +
    "               coalesce(A.REMITENTE,'') as remitente,  " +
    "               coalesce(B.precinto,'') as precinto,  " +
    "               coalesce(A.DESTINATARIO,'') as destinatario,  " +
    "               coalesce(D.fecsal,'')as fecsal,  " +
    "               coalesce(D.horsal,'')as horsal,  " +
    "               coalesce(E.fececar,'')as fececar,  " +
    "               coalesce(E.horecar,'')as horecar,  " +
    "               coalesce(F.horscar,'')as horscar,  " +
    "               coalesce(G.fecedes,'')as fecedes,  " +
    "               coalesce(G.horedes,'')as horedes,  " +
    "               coalesce(H.horsdes,'')as horsdes,  " +
    "               A.unit_packed,  " +
    "               b.contenedores,  " +
    "               b.cf_code,  " +
    "               a.pesoreal::NUMERIC(20,2) as cantRemesa, " +
    "               b.despachador, " +
    "               coalesce(u.nit,'7777777')AS nit, " +
    "               c.porcent " +
    "         FROM  (Select * " +
    "		from planilla " +
    "		where numpla in (select numpla " +
    "				 from (select numrem from remesa where fecrem = ? and base = ?)remesa" +
    "				 inner join plarem on (plarem.numrem=remesa.numrem)" +
    "				 )" +
    "		and fecdsp <? AND BASE = ?  ) AS B " +
    "               INNER JOIN plarem C ON (c.numpla = b.numpla) " +
    "               INNER JOIN remesa A ON (a.numrem = c.numrem  and a.base = 'COL')        " +
    "               LEFT OUTER JOIN usuarios u ON (u.idusuario = b.despachador)  " +
    "               LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecsal,   " +
    "    				TO_CHAR(date_time_traffic,'HH24MI')as horsal,   " +
    "    				pla  " +
    "    			from    planilla_tiempo  " +
    "    			where   time_code ='SALI'  ) D ON (b.numpla = d.pla)  " +
    "               LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fececar,   " +
    "    				TO_CHAR(date_time_traffic,'HH24MI')as horecar,   " +
    "    				pla  " +
    "    			from planilla_tiempo  " +
    "    			where time_code ='ECAR'  )E ON (b.numpla = E.pla)  " +
    "               LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecscar,   " +
    "   				TO_CHAR(date_time_traffic,'HH24MI')as horscar,   " +
    "    				pla  " +
    "    			from 	planilla_tiempo  " +
    "    			where 	time_code ='SCAR'       ) F ON (b.numpla = F.pla)  " +
    "               LEFT JOIN (	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecedes,   " +
    "   				TO_CHAR(date_time_traffic,'HH24MI')as horedes,   " +
    "    				pla  " +
    "    			from planilla_tiempo  " +
    "    			where time_code ='EDES' ) G ON (b.numpla = G.pla)  " +
    "               LEFT JOIN ( 	select 	TO_CHAR(date_time_traffic,'YYYYMMDD')as fecsdes,   " +
    "    				TO_CHAR(date_time_traffic,'HH24MI')as horsdes,   " +
    "    				pla  " +
    "    			from 	planilla_tiempo  " +
    "    			where 	time_code ='SDES' ) H ON (B.numpla = H.pla)  " +
    "    order by a.remision ";
    
    
    //JEscandon 06.01.2006
    /*CONSULTAS GENRADAS PARA CAGAR EL OBJETO DATOSPLANILLASIMP PARA GENERAR PLANILLA EN ARCHIVO PDF*/
    private String SQL_DATOSCIA = " SELECT DSTRCT, NOMBRE_ASEGURADORA, POLIZA_ASEGURADORA, " +
    " to_char(FECHA_VEN_POLIZA,'DD/MM/YY') as FECHA_VEN_POLIZA, CODIGO_REGIONAL, " +
    " CODIGO_EMPRESA, RANGO_INICIAL, RANGO_FINAL, RESOLUCION  "+
    " FROM CIA WHERE DSTRCT = ?  ";
    
    
    
    //jescandon 27.01.06
    private String SQL_DATOSREMESA =  " SELECT  b.numrem,                 "+
    "  b.docuinterno,                   "+
    "  b.std_job_no,                    "+
    "  b.cliente,     c.nomcli,         "+
    "  c.texto_oc, b.observacion        "+
    "  FROM                             "+
    "  plarem  AS a,                    "+
    "  remesa  AS b,                    "+
    "  cliente AS c                     "+
    "  WHERE                            "+
    "  a.numpla = ?                     "+
    "  and b.numrem = a.numrem          "+
    "  and c.codcli = b.cliente         "+
    "  AND b.reg_status = ''            ";
    
    
    /*private String SQL_DATOSPLANILLA = "    SELECT                                  "+
    "    a.numpla,                               "+
    "    TO_char(a.fecdsp,'DD/MM/YY') as fecdsp, "+
    "    a.oripla,                               "+
    "    b.nomciu as origen,                     "+
    "    a.despla,                               "+
    "    c.nomciu as destino,                    "+
    "    a.ruta_pla,                             "+
    "    a.plaveh,                               "+
    "    k.descripcion as marca,                 "+
    "    d.modelo,                               "+
    "    d.serial,                               "+
    "    l.descripcion as color,                 "+
    "    n.descripcion as carroceria,            "+
    "    d.noejes,                               "+
    "    d.tara as pesovacio,                    "+
    "    d.venseguroobliga as vensoat,           "+
    "    a.platlr,                               "+
    "    a.pesoreal,                             "+
    "    e.cedula as cedprop,                    "+
    "    e.nombre as nomprop,                    "+
    "    e.direccion as dirprop,                 "+
    "    e.telefono as telprop,                  "+
    "    f.nomciu as ciuprop,                    "+
    "    g.cedula as cedten,                     "+
    "    g.nombre as nomten,                     "+
    "    g.direccion as dirten,                  "+
    "    g.telefono as telten,                   "+
    "    h.nomciu as ciuten,                     "+
    "    i.cedula as cedcond,                    "+
    "    i.nombre as nomcond,                    "+
    "    i.direccion as dircond,                 "+
    "    o.categoriapase as catpase,             "+
    "    j.nomciu as ciucond,                    "+
    "    a.printer_date,                         "+
    "    m.vlr as anticipo,                      "+
    "    m.vlr_for,                              "+
    "    m.currency as moneda,                   "+
    "    a.despachador,                          "+
    "    a.contenedores                          "+
    "    FROM planilla a                         "+
    "    LEFT OUTER JOIN ciudad b on ( b.codciu = a.oripla   )       "+
    "    LEFT OUTER JOIN ciudad c on ( c.codciu = a.despla )         "+
    "    LEFT OUTER JOIN placa d on (d.placa  = a.plaveh)            "+
    "    LEFT OUTER JOIN nit e on (e.cedula   = a.nitpro)            "+
    "    LEFT OUTER JOIN ciudad f on (f.codciu   = e.codciu)         "+
    "    LEFT OUTER JOIN nit g on (g.cedula   = a.nitpro )           "+
    "    LEFT OUTER JOIN ciudad h on (  h.codciu = g.codciu)         "+
    "    LEFT OUTER JOIN nit i on (i.cedula = a.cedcon)              "+
    "    LEFT OUTER JOIN conductor o on (a.cedcon = o.cedula)    	"+
    "    LEFT OUTER JOIN ciudad j on (j.codciu = i.codciu )          "+
    "    LEFT OUTER JOIN marca_placa k on (k.codigo = d.marca)       "+
    "    LEFT OUTER JOIN color_placa l on (l.codigo= d.color)        "+
    "    LEFT OUTER JOIN carroceria n on (n.codigo= d.carroceria)  	"+
    "    LEFT OUTER JOIN (select sum(vlr) as vlr, sum(vlr_for) as vlr_for, currency, planilla from movpla where concept_code = '01' and planilla = ? and reg_status= '' group by planilla, currency) m on (m.planilla = numpla)   "+
    "   WHERE a.base = 'COL'                         "+
    "    and a.numpla       = ?                      "+
    "    ORDER BY a.agcpla, a.numpla                 ";*/
    
    
    private String SQL_DATOSPLANILLA = "    SELECT                                  "+
    "    a.numpla,                               "+
    "    TO_char(a.fecdsp,'DD/MM/YY') as fecdsp, "+
    "    a.oripla,                               "+
    "    b.nomciu as origen,                     "+
    "    a.despla,                               "+
    "    c.nomciu as destino,                    "+
    "    a.ruta_pla,                             "+
    "    a.plaveh,                               "+
    "    k.descripcion as marca,                 "+
    "    d.modelo,                               "+
    "    d.serial,                               "+
    "    l.descripcion as color,                 "+
    "    n.descripcion as carroceria,            "+
    "    d.noejes,                               "+
    "    d.tara as pesovacio,                    "+
    "    d.venseguroobliga as vensoat,           "+
    "    a.platlr,                               "+
    "    a.pesoreal,                             "+
    "    e.cedula as cedprop,                    "+
    "    e.nombre as nomprop,                    "+
    "    e.direccion as dirprop,                 "+
    "    e.telefono as telprop,                  "+
    "    f.nomciu as ciuprop,                    "+
    "    g.cedula as cedten,                     "+
    "    g.nombre as nomten,                     "+
    "    g.direccion as dirten,                  "+
    "    g.telefono as telten,                   "+
    "    h.nomciu as ciuten,                     "+
    "    i.cedula as cedcond,                    "+
    "    i.nombre as nomcond,                    "+
    "    i.direccion as dircond,                 "+
    "    o.categoriapase as catpase,             "+
    "    j.nomciu as ciucond,                    "+
    "    a.printer_date,                         "+
    "    m.vlr as anticipo,                      "+
    "    m.vlr_for,                              "+
    "    m.currency as moneda,                   "+
    "    a.despachador,                          "+
    "    a.contenedores,                         "+
    "    a.reg_status                            "+
    "    FROM planilla a                         "+
    "    LEFT OUTER JOIN ciudad b on ( b.codciu = a.oripla   )       "+
    "    LEFT OUTER JOIN ciudad c on ( c.codciu = a.despla )         "+
    "    LEFT OUTER JOIN placa d on (d.placa  = a.plaveh)            "+
    "    LEFT OUTER JOIN nit e on (e.cedula   = a.nitpro)            "+
    "    LEFT OUTER JOIN ciudad f on (f.codciu   = e.codciu)         "+
    "    LEFT OUTER JOIN nit g on (g.cedula   = a.nitpro )           "+
    "    LEFT OUTER JOIN ciudad h on (  h.codciu = g.codciu)         "+
    "    LEFT OUTER JOIN nit i on (i.cedula = a.cedcon)              "+
    "    LEFT OUTER JOIN conductor o on (a.cedcon = o.cedula)    	"+
    "    LEFT OUTER JOIN ciudad j on (j.codciu = i.codciu )          "+
    "    LEFT OUTER JOIN marca_placa k on (k.codigo = d.marca)       "+
    "    LEFT OUTER JOIN color_placa l on (l.codigo= d.color)        "+
    "    LEFT OUTER JOIN carroceria n on (n.codigo= d.carroceria)  	"+
    "    LEFT OUTER JOIN (select sum(vlr) as vlr, sum(vlr_for) as vlr_for, currency, planilla from movpla where concept_code = '01' and planilla = ? and reg_status= '' group by planilla, currency) m on (m.planilla = numpla)   "+
    "   WHERE a.base = 'COL'                         "+
    "    and a.numpla       = ?                      "+
    "    ORDER BY a.agcpla, a.numpla                 ";
    
    public static String DOCUMENTOS_GENERALES=         "   select tipo_doc,    "+
    "   documento           "+
    "   from   remesa_docto" +
    "   where numrem = ?    "+
    "   and tipo_doc_rel = ''";
    
    /* FIN CONSULTAS JEscandon */
    
    public static String SQL_SOPORTES_CLIENTES = " SELECT a.cliente, a.soporte, b.descripcion " +
    " FROM soporte_clientes a, tablagen b " +
    " WHERE a.cliente = ? and b.table_type = 'SOP' and b.table_code = a.soporte ";
    
    
    
    public PlanillaImpresionDAO() {
        super("PlanillaImpresionDAO.xml");
    }
    
    
    /**
     * Procedimiento que consulta los datos generales
     * de la compa�ia
     * @params Distrito Cia a Buscar
     * @return Objeto CIA que almacena los datos de la Compa�ia
     * @throws Exception.
     */
    
    
    CIA  BuscarCIA(String Distrito)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        CIA               datos    = null;
        
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_CIA");
            st.setString(1, Distrito);
            rs = st.executeQuery();
            if(rs.next()){
                datos = CIA.load(rs);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarCIA [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_BUSCAR_CIA");
        }
        return datos;
    }
    
    /**
     * Funcion que busca todas las planillas no impresa o la
     * planilla setea  en el parametro
     * @autor  mfontalvo
     * @params NumeroPlanilla  planilla a imprimir
     * @params usuario  despachador
     * @params tipo  tipo de Busqueda (especifica o general)
     * @return Lista de Planillas a imprimir
     * @throws Exception.
     */
    
    
    List BuscarPlanillasNoImp(String NumeroPlanilla, String usuario,String tipo )throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        try {
            if(tipo.equals("1")){
                st = this.crearPreparedStatement("SQL_BUSCAR_PLANILLA_NOIMPRESA");
                st.setString(1, NumeroPlanilla);
            }
            else if(tipo.equals("2")){
                st = this.crearPreparedStatement("SQL_BUSCAR_PNI");
                st.setString(1,usuario);
            }
            rs = st.executeQuery();
            
            while(rs.next()){
                DatosPlanillaImp datos = DatosPlanillaImp.load(rs);
                
                // Buscando remesas de la planilla
                datos.setRemesas      ( BuscarRemesas( datos.getNumeroPlanilla()));
                // Buscando descuentos de la planilla
                datos.setDescuentos   ( BuscarDescuentos(datos.getNumeroPlanilla()));
                
                
                // mfontalvo
                // 2005-12-26
                // Soportes asigandos a cada remesa - re asignados a la planilla
                TreeMap Soportes = new TreeMap();
                // Observaciones de las remesas
                TreeMap obs      = new TreeMap();
                List remesas = datos.getRemesas();
                for (int i=0; i<remesas.size();i++){
                    TreeMap SoporteRem = ((DatosRemesaImp) remesas.get(i)).getSoportes();
                    Iterator keySoporteRem = SoporteRem.keySet().iterator();
                    while (keySoporteRem.hasNext()){
                        String key =(String) keySoporteRem.next();
                        Soportes.put(key, key);
                    }
                    obs.put( ((DatosRemesaImp) remesas.get(i)).getObservaciones(),
                    ((DatosRemesaImp) remesas.get(i)).getObservaciones() );
                }
                datos.setSoportes(Soportes);
                datos.setObservaciones( obs.values().toString().replaceAll("\\[|\\]",""));
                ////////////////////////////////////////////////////////////////
                
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarPlanillasNoImp [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            
            
            // deconectar de la base de datos dependiendo del query
            if(tipo.equals("1"))
                this.desconectar("SQL_BUSCAR_PLANILLA_NOIMPRESA");
            else if(tipo.equals("2"))
                this.desconectar("SQL_BUSCAR_PNI");
            
            
            
        }
        return Lista;
    }
    
    
    /**
     * Funcion que devuelve el listado de remesas asociadas a una planilla
     * @autor: mfontalvo
     * @params NumeroPlanilla Planilla a Consultar
     * @return List de remesas relacionadas a la planilla
     * @throws Exception.
     */
    
    
    List BuscarRemesas( String NumeroPlanilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_REMESAS");
            st.setString(1, NumeroPlanilla);
            rs = st.executeQuery();
            int limiteRemesas = 5;
            
            while(rs.next()){
                DatosRemesaImp datos = DatosRemesaImp.load(rs);
                datos.setSoportes(Util.ArrayToTreeMap( rs.getString("soportes").split("~") ));
                Lista.add(datos);
                if (Lista.size()>limiteRemesas)
                    break;
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarRemesas [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_BUSCAR_REMESAS");
        }
        return Lista;
    }
    
    
    
    
    /**
     * Funcion que devuelve el listado de descuentos asociados a una planilla
     * segun sea los parametros
     * @autor: mfontalvo
     * @params Planilla Planilla a Consultar
     * @return List de descuentos asociados a una planillas
     * @throws Exception.
     */
    
    List BuscarDescuentos( String Planilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_DESCUENTOS");
            st.setString(1, Planilla);
            rs = st.executeQuery();
            while(rs.next()){
                DatosDescuentosImp datos = DatosDescuentosImp.load(rs);
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDescuentos [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_BUSCAR_DESCUENTOS");
        }
        return Lista;
    }
    
    
    /**
     * Funcion que devuelve el listado de remietentes o destinatarios
     * segun sea los parametros
     * @autor: mfontalvo
     * @params NumeroRemesa Remesa Consultar
     * @params Tipo Tipo { remitentes o destinatarios }
     * @return List de remitentes o destinatarios relacionados a una remesa
     * @throws Exception.
     */
    
    List BuscarRemDes(String NumeroRemesa, String Tipo)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_REMDES");
            st.setString(1, Tipo);
            st.setString(2, NumeroRemesa);
            rs = st.executeQuery();
            while(rs.next()){
                DatosRemDesImp datos = DatosRemDesImp.load(rs);
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarRemDes [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_BUSCAR_REMDES");
        }
        return Lista;
    }
    
    
    
    
    /**
     * Procedimiento de busqueda de documentos internos de una remesa
     * @autor: mfontalvo
     * @fecha: 2005-11-08
     * @params Remesa Remesa a Consultar
     * @return String de documentos internos
     * @throws Exception.
     */
    
    public String BuscarDocumentosInternos(String Remesa)throws SQLException{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            doc      = "";
        
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_DOCUMENTOS_INTERNOS");
            st.setString(1, Remesa);
            rs = st.executeQuery();
            while(rs.next()){
                doc += rs.getString(1) + ",";
            }
            if (doc.equals("")) doc = " ";
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BuscarDocumentosInternos [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_BUSCAR_DOCUMENTOS_INTERNOS");
        }
        return doc;
    }
    
    
    /**
     * Procedimiento de busqueda de soportes asignados a un cliente
     * @autor: mfontalvo
     * @fecha: 2005-12-26
     * @params Cliente  Cliente a  consultar
     * @return TreeMap de soportes de un cliente.
     * @throws Exception.
     */
    
    TreeMap BuscarSoportesClientes(String Cliente)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        TreeMap           Soportes = new TreeMap();
        
        try {
            st = this.crearPreparedStatement("SQL_SOPORTES_CLIENTES");
            st.setString(1, Cliente);
            rs = st.executeQuery();
            while(rs.next()){
                Soportes.put( rs.getString(1) + ": " + rs.getString(2), rs.getString(3));
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarSoportesClientes [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_SOPORTES_CLIENTES");
        }
        return Soportes;
    }
    
    /**
     * Procedimiento de busqueda de la fecha de cargue de la planilla
     * @autor: mfontalvo
     * @fecha: 2005-11-08
     * @params Planilla planilla a consultar
     * @return String Fecha de carge de una planilla.
     * @throws Exception.
     */
    
    
    String BuscarFechaCarguePlanilla(String Planilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            fecha      = "";
        
        try {
            st = this.crearPreparedStatement("SQL_FECHA_CARGUE");
            st.setString(1, Planilla);
            rs = st.executeQuery();
            if(rs.next()){
                fecha += rs.getString(1) ;
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDocumentosInternos [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_FECHA_CARGUE");
        }
        return fecha;
    }
    
    
    /*FUNCIONES PARA CARGAR EL OBJETO DATOS PLANILLA IMP */
    /**
     * Metodo BuscarDatosCIA, buscar los datos correspondientes a la compa�ia
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see : String Distrito, DatosPlanillaImpPDF datos
     * @version : 1.0
     */
    public DatosPlanillaImpPDF BuscarDatosCIA( String Distrito )throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        DatosPlanillaImpPDF datos = new DatosPlanillaImpPDF();
        
        try {
            st = this.crearPreparedStatement("SQL_BUSCAR_CIA");
            st.setString(1, Distrito);
            rs = st.executeQuery();
            while(rs.next()){
                DatosPlanillaImpPDF.loadDatosCIA(rs, datos );
                //datos.setRemesas(BuscarRemesas(conPostgres, datos.getNumeroPlanilla()));
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDatosCIA [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_BUSCAR_CIA");
        }
        return datos;
    }
    
    /**
     * Metodo BuscarDatosPlanilla, buscar los datos correspondientes a la planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see : String Numpla, DatosPlanillaImpPDF datos
     * @version : 1.0
     */
    public void BuscarDatosPlanilla( String Numpla, DatosPlanillaImpPDF datos )throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        
        try {
            st = this.crearPreparedStatement("SQL_DATOSPLANILLA");
            st.setString(1, Numpla);
            st.setString(2, Numpla);
            rs = st.executeQuery();
            while(rs.next()){
                DatosPlanillaImpPDF.loadDatosPlanilla(rs, datos );
                datos.setRemesas(ListaRemesas(datos.getNumeroPlanilla()));
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDatosPlanilla [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_DATOSPLANILLA");
        }
    }
    
    
    /**
     * Metodo ListaRemesas, buscar las remesas asociadas a una planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String NumeroPlanilla El nomero de la planilla
     * @version : 1.0
     */
    List ListaRemesas(String NumeroPlanilla )throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        try {
            st = this.crearPreparedStatement("SQL_DATOSREMESA");
            st.setString(1, NumeroPlanilla);
            rs = st.executeQuery();
            while(rs.next()){
                DatosRemesaImp rem = DatosRemesaImp.loadRemesas(rs);
                rem.setDescuentos(BuscarDescuentos(NumeroPlanilla));
                rem.setDocumentoInterno(BuscarDocumentosInternos(rem.getNumeroRemesa()));
                rem.setSoportes( BuscarSoportesClientes(rem.getCodigoCliente()));
                TreeMap soportes = rem.getSoportes();
                Set key = soportes.keySet();
                Iterator it = key.iterator();
                String soporte = "";
                while(it.hasNext()){
                    soporte += (String)soportes.get(it.next());
                    ////System.out.println("------" + soporte);
                }
                Lista.add(rem);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina ListaRemesas [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_DATOSREMESA");
        }
        return Lista;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /* Funcion que busca todas las planillas para
     * EXPORTA DATOS AL ARCHIVO DE TEXTO MS230828
     * @autor  mfontalvo
     * @params Fecha ............ busca por fecha de despacho
     * @return Lista de Planillas a exportar
     * @throws Exception.
     */
    
    List BuscarPlanillas(String Fecha)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_PLANILLA);
            st.setString(1, Fecha);
            st.setString(2, this.CONST_BASE);
            rs = st.executeQuery();
            while(rs.next()){
                DatosPlanilla datos = DatosPlanilla.load2(rs);
                datos.setCia(rs.getString("cia"));
                datos.setAnticipos(BuscarAnticipos(datos.getNumeroPlanilla(),datos.getSJ()));
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarPlanillas  (MS230828)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;
    }
    
    
    
     /* Funcion que busca los anticipos de una planilla
      * @autor  mfontalvo
      * @params NumeroPlanilla ............ Planilla a buscar
      * @params concept_code............... Concept Code
      * @return String de anticipos de la planilla
      * @throws Exception.
      */
    
    
    String BuscarCMAnticipos(String NumeroPlanilla, String concept_code)throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        String            CM = "";
        
        PoolManager poolManager = PoolManager.getInstance();
        try {
            st = this.crearPreparedStatement("SQL_CM_P_ANTICIPO");
            st.setString(1, concept_code);
            st.setString(2, NumeroPlanilla);
            rs = st.executeQuery();
            if(rs.next()){
                CM = rs.getString(1);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarCMAnticipos  (MS230828)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_CM_P_ANTICIPO");
        }
        return CM;
    }
    
     /* Funcion que busca los anticipos de peaje de una planilla
      * @autor  mfontalvo
      * @params NumeroPlanilla ............ Planilla a buscar
      * @return String de anticipos de peaje de la planilla
      * @throws Exception.
      */
    String BuscarCMPeajes(String NumeroPlanilla)throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        String            CM = "";
        
        PoolManager poolManager = PoolManager.getInstance();
        try {
            st = this.crearPreparedStatement("SQL_CM_P_PEAJE");
            st.setString(1, NumeroPlanilla);
            rs = st.executeQuery();
            if(rs.next()){
                CM = rs.getString(1);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarCMPeajes  (MS230828)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_CM_P_PEAJE");
            
            
        }
        return CM;
    }
     /* Funcion que busca los Acpm de una planilla
      * @autor  mfontalvo
      * @params NumeroPlanilla ............ Planilla a buscar
      * @return String de anticipos de Acpm de la planilla
      * @throws Exception.
      */
    String BuscarCMAcpm(String NumeroPlanilla)throws Exception{
        PreparedStatement st = null;
        ResultSet         rs = null;
        String            CM = "";
        
        PoolManager poolManager = PoolManager.getInstance();
        
        try {
            st = this.crearPreparedStatement("SQL_CM_P_ACPM");
            st.setString(1, NumeroPlanilla);
            //st.setString(2, this.CONST_BASE);
            rs = st.executeQuery();
            if(rs.next()){
                CM = rs.getString(1);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarCMAcpm  (MS230828)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_CM_P_ACPM");
        }
        return CM;
    }
    
    /**
     * Procedimiento para buscar los datos que se usaran para la
     * migracion hacia el archivo MST610
     * @autor KReales
     * @param Fecha de migracion y base
     * @throws Exception.
     */
    public void searchPlanillas(String Fecha,String base)throws Exception{
        
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        plas = new Vector();
        ////System.out.println("La base es: "+base);
        try {
            
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            
            int year=Integer.parseInt(Fecha.substring(0,4));
            int month=Integer.parseInt(Fecha.substring(5,7))-1;
            int date= Integer.parseInt(Fecha.substring(8,10));
            
            Calendar fecha_cortI = Calendar.getInstance();
            fecha_cortI.set(year,month,date,7,0,0);
            java.util.Date fecha_corti = fecha_cortI.getTime();
            String fecIni =s.format(fecha_corti);
            
            Calendar fecha_cortF = Calendar.getInstance();
            fecha_cortF.set(year,month,date+1,7,0,0);
            java.util.Date fecha_cortf = fecha_cortF.getTime();
            String fecFin =s.format(fecha_cortf);
            
            if(base.equals("spo")){
                s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                fecIni =s.format(fecha_corti);
                fecFin =s.format(fecha_cortf);
                st = this.crearPreparedStatement("SQL");
                st.setString(1, fecIni);
                st.setString(2, fecFin);
                st.setString(3, base);
                
            }else{
                fecha_cortI = Calendar.getInstance();
                fecha_cortI.set(year,month,date,7,0,0);
                fecha_corti = fecha_cortI.getTime();
                fecIni =s.format(fecha_corti);
                
                fecha_cortF = Calendar.getInstance();
                fecha_cortF.set(year,month,date+1,7,0,0);
                fecha_cortf = fecha_cortF.getTime();
                fecFin =s.format(fecha_cortf);
                
                
                s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                fecIni =s.format(fecha_corti);
                fecFin =s.format(fecha_cortf);
                
                st = this.crearPreparedStatement("SQL2");
                st.setString(1, fecIni);
                st.setString(2, fecFin);
                st.setString(3, base);
                
            }
            //st.setString(2, fecFin+"%");
            ////System.out.println("Este es mi query "+st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                DatosPlanilla datos = new DatosPlanilla();
                datos.setNumeroRemision(rs.getString("remision"));
                datos.setFechaPlanilla(rs.getString("fecpla"));
                datos.setSJ(rs.getString("STD_JOB_NO"));
                datos.setPlacaVehiculo(rs.getString("PLAVEH"));
                datos.setPesoReal(rs.getString("PESOREAL"));
                datos.setCedulaConductor(rs.getString("CEDCON"));
                datos.setNumeroPlanilla(rs.getString("numpla"));
                datos.setCia(rs.getString("cia"));
                datos.setFlete(rs.getString("unit_cost"));
                
                datos.setCantidReal("");
                datos.setDocInterno("");
                datos.setFacComercial("");
                datos.setCantidReal("");
                datos.setContenedor("");
                datos.setPrecinto("");
                datos.setUnidades("");
                datos.setTrailer("");
                datos.setFechaSalida("");
                datos.setHoraISalida("");
                datos.setHoraFSalida("");
                datos.setFechaCargue("");
                datos.setHoraICargue("");
                datos.setHoraFCargue("");
                datos.setFechaDescargue("");
                datos.setHoraIDescargue("");
                datos.setHoraFDescargue("");
                datos.setRemitente("");
                datos.setDestinatario("");
                
                
                datos.setCodigoAgenciaPlanilla(rs.getString("AGCPLA"));
                datos.setBase(base);
                datos.setAnticipos(BuscarAnticipos(datos.getNumeroPlanilla(),datos.getSJ()));
                // datos.setNumeroPlanilla("");
                datos.setDespachador(rs.getString("despachador"));
                datos.setCedula(rs.getString("nit"));
                plas.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina searchPlanillas  (MST620)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL");
            this.desconectar("SQL2");
        }
    }
    public void searchPlanillas_CGAGEN(String Fecha,String base)throws Exception{
        
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        plas = new Vector();
        ////System.out.println("La base es: "+base);
        PoolManager poolManager = PoolManager.getInstance();
        try {
            
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
            
            int year=Integer.parseInt(Fecha.substring(0,4));
            int month=Integer.parseInt(Fecha.substring(5,7))-1;
            int date= Integer.parseInt(Fecha.substring(8,10));
            
            Calendar fecha_cortI = Calendar.getInstance();
            fecha_cortI.set(year,month,date,0,0,0);
            java.util.Date fecha_corti = fecha_cortI.getTime();
            String fecIni =s.format(fecha_corti);
            
            Calendar fecha_cortF = Calendar.getInstance();
            fecha_cortF.set(year,month,date,23,59,0);
            java.util.Date fecha_cortf = fecha_cortF.getTime();
            
            
            
            //s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            fecIni =s.format(fecha_corti);
            String fecFin =s.format(fecha_cortf);
            
            st =this.crearPreparedStatement("SQL_CARGAGRAL");
            st.setString(1, fecIni);
            st.setString(2, fecFin);
            st.setString(3, base);
            
            ////System.out.println("Este es mi query CGAGEN"+st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                DatosPlanilla datos = new DatosPlanilla();
                datos.setNumeroRemision(rs.getString("remision"));
                datos.setFechaPlanilla(rs.getString("fecpla"));
                datos.setSJ(rs.getString("STD_JOB_NO"));
                datos.setPlacaVehiculo(rs.getString("PLAVEH"));
                datos.setPesoReal(rs.getString("cantremesa"));
                datos.setCedulaConductor(rs.getString("CEDCON"));
                datos.setNumeroPlanilla(rs.getString("numpla"));
                datos.setCia(rs.getString("cia"));
                datos.setFlete(rs.getString("unit_cost"));
                datos.setDocInterno(rs.getString("docuinterno"));
                datos.setFacComercial(rs.getString("facturacial"));
                datos.setCantidReal(rs.getString("qty_packed"));
                datos.setContenedor(rs.getString("contenedores"));
                datos.setPrecinto(rs.getString("precinto"));
                datos.setUnidades(rs.getString("unit_packed"));
                datos.setTrailer(rs.getString("PLATLR"));
                datos.setFechaSalida(rs.getString("fecsal"));
                datos.setHoraISalida(rs.getString("horsal"));
                datos.setHoraFSalida(rs.getString("horsal"));
                datos.setFechaCargue(rs.getString("fececar"));
                datos.setHoraICargue(rs.getString("horecar"));
                datos.setHoraFCargue(rs.getString("horscar"));
                datos.setFechaDescargue(rs.getString("fecedes"));
                datos.setHoraIDescargue(rs.getString("horedes"));
                datos.setHoraFDescargue(rs.getString("horsdes"));
                datos.setRemitente(rs.getString("REMITENTE"));
                datos.setDestinatario(rs.getString("DESTINATARIO"));
                //    datos.setCodigoAgenciaPlanilla(rs.getString("AGCPLA"));
                datos.setBase(base);
                datos.setAnticipos(BuscarDescuentosCGAGEN(datos.getNumeroPlanilla(),datos.getSJ()));
                if(rs.getBoolean("tieneCf")){
                    datos.setCf_code(rs.getString("cf_code"));
                }
                else{
                    datos.setCf_code(buscarCf(datos.getSJ()));
                }
                datos.setCantPlanilla(rs.getString("cantPlanilla"));
                datos.setCantRemesa(rs.getString("cantRemesa"));
                datos.setDespachador(rs.getString("despachador"));
                datos.setCedula(rs.getString("nit"));
                datos.setPorcent(rs.getInt("Porcent"));
                
                plas.add(datos);
            }
            
            s = new SimpleDateFormat("yyyy-MM-dd");
            fecIni =s.format(fecha_corti);
            
            st = this.crearPreparedStatement("SQL_CARGAGRAL_REM");
            st.setString(1, fecIni);
            st.setString(2, base);
            st.setString(3, fecIni);
            st.setString(4, base);
            
            ////System.out.println("Este es mi query CGAGEN PARA REMESAS"+st.toString());
            rs = st.executeQuery();
            while(rs.next()){
                DatosPlanilla datos = new DatosPlanilla();
                datos.setNumeroRemision(rs.getString("remision"));
                datos.setFechaPlanilla(rs.getString("fecpla"));
                datos.setSJ(rs.getString("STD_JOB_NO"));
                datos.setPlacaVehiculo(rs.getString("PLAVEH"));
                datos.setPesoReal(rs.getString("cantremesa"));
                datos.setCedulaConductor(rs.getString("CEDCON"));
                datos.setNumeroPlanilla(rs.getString("numpla"));
                datos.setCia(rs.getString("cia"));
                datos.setFlete(rs.getString("unit_cost"));
                datos.setDocInterno(rs.getString("docuinterno"));
                datos.setFacComercial(rs.getString("facturacial"));
                datos.setCantidReal(rs.getString("qty_packed"));
                datos.setContenedor(rs.getString("contenedores"));
                datos.setPrecinto(rs.getString("precinto"));
                datos.setUnidades(rs.getString("unit_packed"));
                datos.setTrailer(rs.getString("PLATLR"));
                datos.setFechaSalida(rs.getString("fecsal"));
                datos.setHoraISalida(rs.getString("horsal"));
                datos.setHoraFSalida(rs.getString("horsal"));
                datos.setFechaCargue(rs.getString("fececar"));
                datos.setHoraICargue(rs.getString("horecar"));
                datos.setHoraFCargue(rs.getString("horscar"));
                datos.setFechaDescargue(rs.getString("fecedes"));
                datos.setHoraIDescargue(rs.getString("horedes"));
                datos.setHoraFDescargue(rs.getString("horsdes"));
                datos.setRemitente(rs.getString("REMITENTE"));
                datos.setDestinatario(rs.getString("DESTINATARIO"));
                //    datos.setCodigoAgenciaPlanilla(rs.getString("AGCPLA"));
                datos.setBase(base);
                datos.setAnticipos(BuscarDescuentosCGAGEN(datos.getNumeroPlanilla(),datos.getSJ()));
                if(rs.getBoolean("tieneCf")){
                    datos.setCf_code(rs.getString("cf_code"));
                }
                else{
                    datos.setCf_code(buscarCf(datos.getSJ()));
                }
                datos.setCantPlanilla(rs.getString("cantPlanilla"));
                datos.setCantRemesa(rs.getString("cantRemesa"));
                datos.setDespachador(rs.getString("despachador"));
                datos.setCedula(rs.getString("nit"));
                datos.setPorcent(rs.getInt("Porcent"));
                plas.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina searchPlanillas  (MST620)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_CARGAGRAL");
            this.desconectar("SQL_CARGAGRAL_REM");
        }
    }
    
    List BuscarAnticipos2(String NumeroPlanilla, String SJ)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            // descuentos de papeleria
            st = conPostgres.prepareStatement(SQL_DESC_SJ);
            st.setString(1, NumeroPlanilla);
            st.setString(2, SJ);
            ////System.out.println("Consulta para descuentos: "+st.toString());
            //st.setString(2, this.CONST_BASE);
            rs = st.executeQuery();
            while(rs.next()){
                DatosDescuentosImp datos = new DatosDescuentosImp();
                datos.setCodigo(rs.getString(3));
                datos.setValor(rs.getString(1));
                datos.setTipo("Sj");
                Lista.add(datos);
            }
            
            // otros anticipos
            st = conPostgres.prepareStatement(SQL_BUSCAR_ANTICIPOS);
            st.setString(1, NumeroPlanilla);
            ////System.out.println("Consulta para anticipo: "+st.toString());
            //st.setString(2, this.CONST_BASE);
            float cant = 0;
            Float red = new Float(0.5);
            int sw=0;
            
            rs = st.executeQuery();
            while(rs.next()){
                DatosDescuentosImp datos = DatosDescuentosImp.load2(rs);
                
                //proveedor anticipo
                if(datos.getCodigo().equals("09")){
                    cant = Float.parseFloat(datos.getValor());
                }
                else if(datos.getCodigo().equals("01")){
                    datos.setProveedorMigracion(BuscarCMAnticipos(NumeroPlanilla,"01"));
                    cant = cant + Float.parseFloat(datos.getValor());
                    double cant1  = this.redondear(cant,0);
                    ////System.out.println(cant1);
                    datos.setValor(""+cant1);
                    Lista.add(datos);
                    sw=1;
                }
                //proveedor acpm
                else if(datos.getCodigo().equals("02")){
                    datos.setProveedorMigracion(BuscarCMAcpm(NumeroPlanilla));
                    Lista.add(datos);
                }
                //proveesor peaje
                else if(datos.getCodigo().equals("03")){
                    datos.setProveedorMigracion(BuscarCMPeajes(NumeroPlanilla));
                    Lista.add(datos);
                }
                /*else {
                    datos.setCodigo("DESC");
                    Lista.add(datos);
                }*/
            }
            if(sw==0 && cant!=0){
                ////System.out.println("Encontro q no hay efectivo pero si 09 y la cant no es 0");
                st = conPostgres.prepareStatement(SQL_BUSCAR_ANTICIPOS);
                st.setString(1, NumeroPlanilla);
                rs = st.executeQuery();
                while(rs.next()){
                    DatosDescuentosImp datos = DatosDescuentosImp.load2(rs);
                    if(datos.getCodigo().equals("09")){
                        datos.setProveedorMigracion(BuscarCMAnticipos(NumeroPlanilla,"09"));
                        Lista.add(datos);
                    }
                }
            }
            
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarAnticipos  (MS230828)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;
    }
    
    
    
    public java.util.Vector getPlas() {
        return plas;
    }
    
    public  double redondear(double valor, int escala){
        double rval = 0;
        BigDecimal bd = null;
        bd = BigDecimal.valueOf(valor).setScale(escala, RoundingMode.HALF_UP);
        //JOptionPane.showMessageDialog(null, );
        rval = bd.doubleValue();
        
        return rval;
    }
    
    //juam
    /*Busca Planillas no Impresas de acuerdo a una Remesa*/
    List BuscarPlanillasNoImpRemesa( String NumeroRemesa )throws Exception{
        Connection        con      = null;
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        try {
            con = this.conectar("SQL_BUSCAR_PNI_REMESA");
            String sql = this.obtenerSQL("SQL_BUSCAR_PNI_REMESA").replaceAll("#REMESAS#", NumeroRemesa);
            st = con.prepareStatement( sql );
            rs = st.executeQuery();
            while(rs.next()){
                DatosPlanillaImp datos = DatosPlanillaImp.load(rs);
                
                // Buscando remesas de la planilla
                datos.setRemesas      ( BuscarRemesas( datos.getNumeroPlanilla()));
                // Buscando descuentos de la planilla
                datos.setDescuentos   ( BuscarDescuentos(datos.getNumeroPlanilla()));
                
                
                // mfontalvo
                // 2005-12-26
                // Soportes asigandos a cada remesa - re asignados a la planilla
                TreeMap Soportes = new TreeMap();
                // Observaciones de las remesas
                TreeMap obs      = new TreeMap();
                List remesas = datos.getRemesas();
                for (int i=0; i<remesas.size();i++){
                    TreeMap SoporteRem = ((DatosRemesaImp) remesas.get(i)).getSoportes();
                    Iterator keySoporteRem = SoporteRem.keySet().iterator();
                    while (keySoporteRem.hasNext()){
                        String key =(String) keySoporteRem.next();
                        Soportes.put(key, key);
                    }
                    obs.put( ((DatosRemesaImp) remesas.get(i)).getObservaciones(),
                    ((DatosRemesaImp) remesas.get(i)).getObservaciones() );
                }
                datos.setSoportes(Soportes);
                datos.setObservaciones( obs.values().toString().replaceAll("\\[|\\]",""));
                ////////////////////////////////////////////////////////////////
                
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarPlanillasNoImpRemesa [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar( "SQL_BUSCAR_PNI_REMESA");
            
        }
        return Lista;
    }
    
    /**
     *Funcion que true o false si la planilla esta en MIMS
     *@autor: Karen Reales
     *@param: planilla, numero de la planilla que se va a buscar
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String estaEnMimsPla(String planilla)throws SQLException{
        
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String sw="";
        PoolManager poolManager = PoolManager.getInstance();
        try {
            
            st = crearPreparedStatement("SQL_estaEnMimsPla");
            st.setString(1, planilla);
            rs = st.executeQuery();
            if(rs.next()){
                ////System.out.println("Encontre datos de requisicion");
                ////System.out.println("El numero es: "+rs.getString("PR"));
                sw=rs.getString("PR");
                
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error buscando la planilla en MIMS... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            this.desconectar("SQL_estaEnMimsPla");
        }
        return sw;
    }
    /**
     *Funcion que retorna true o false si la remesa esta en MIMS
     *@autor: Karen Reales
     *@param: remesa es el numero de la remesa a buscar
     *@param: distrito es el distrito de la remesa a buscar
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public boolean estaEnMimsRem(String remesa, String distrito)throws SQLException{
        
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        boolean sw=false;
        plas = new Vector();
        PoolManager poolManager = PoolManager.getInstance();
        try {
            st = crearPreparedStatement("SQL_estaEnMimsRem");
            st.setString(1, remesa);
            st.setString(2, distrito);
            ////System.out.println("Busco la remision : "+remesa + " distrito "+distrito);
            rs = st.executeQuery();
            if(rs.next()){
                sw= true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error buscando la REMESA en MIMS... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_estaEnMimsRem");
        }
        return sw;
    }
    
    /**
     * Metodo BuscarDescuentos, lista todas los descuestos para una planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see : Connection conPostgres, String Planilla
     * @version : 1.0
     */
    List BuscarDescuentos(Connection conPostgres, String Planilla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        boolean           liberar = false;
        
        PoolManager poolManager = null;
        if (conPostgres == null){
            poolManager = PoolManager.getInstance();
            conPostgres  = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_BUSCAR_DESCUENTOS);
            st.setString(1, Planilla);
            rs = st.executeQuery();
            while(rs.next()){
                DatosDescuentosImp datos = DatosDescuentosImp.load(rs);
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDescuentos [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            if(liberar)  poolManager.freeConnection("fintra",conPostgres);
        }
        return Lista;
    }
    
    
    /**
     * Metodo BuscarDocInternos, buscar los documentos internos de una remesa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see : Connection conPostgres, String Remesa
     * @version : 1.0
     */
    String BuscarDocInternos(Connection conPostgres, String numrem)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        boolean           liberar = false;
        String documentos = "";
        PoolManager poolManager = null;
        if (conPostgres == null){
            poolManager = PoolManager.getInstance();
            conPostgres  = poolManager.getConnection("fintra");
            liberar     = true;
        }
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(DOCUMENTOS_GENERALES);
            st.setString(1, numrem);
            rs = st.executeQuery();
            while(rs.next()){
                documentos  += rs.getString("documento")  + "\n";
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina documentos generales [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            if(liberar)  poolManager.freeConnection("fintra",conPostgres);
        }
        return documentos;
    }
    
    /**
     * Funcion que retorna un vector de remesas relacionados a una planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public Vector buscarRemesas(String numpla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String documentos = "";
        Vector remesas = new Vector();
        try {
            st = crearPreparedStatement("SQL_buscarRemesas");
            st.setString(1, numpla);
            rs = st.executeQuery();
            while(rs.next()){
                RemPla pr= new RemPla();
                pr.setRemesa(rs.getString("numrem"));
                pr.setPorcent(rs.getFloat("porcent"));
                pr.setPlanilla(rs.getString("numpla"));
                pr.setAccount_code_c(rs.getString("account_code_c"));
                remesas.add(pr);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarDescuentos [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_buscarRemesas");
        }
        return remesas;
    }
    /**
     * Funcion para buscar una +cf dado un standard.
     * @autor : Ing. Karen Reales
     * @param : String numero del standard
     * @return : String con +cf del standard
     * @version : 1.0
     */
    public String buscarCf(String stdjob)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String cf = "";
        try {
            st = crearPreparedStatement("SQL_buscarcf");
            st.setString(1, stdjob);
            rs = st.executeQuery();
            if(rs.next()){
                cf = rs.getString("cf_code");
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarCf de un standard [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_buscarcf");
        }
        return cf;
    }
    
    /**
     * Funcion para actualizar el registro en el movpla con la fecha de migracion
     * @autor : Ing. Karen Reales
     * @param : String numero de la planilla
     * @version : 1.0
     */
    public void actualizarMigracion(String numpla)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        
        try {
            st = crearPreparedStatement("SQL_ACTUALIZAR_MIGRACION");
            st.setString(1, numpla);
            st.executeUpdate();
            
        }
        catch(Exception e) {
            throw new Exception("Error en rutina actualizarMigracion [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_ACTUALIZAR_MIGRACION");
        }
        
    }
    
    List BuscarDescuentosCGAGEN(String NumeroPlanilla, String SJ)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        PoolManager poolManager = PoolManager.getInstance();
        try {
            Connection con = this.conectar("SQL_DESC_SJ");
            // descuentos de papeleria
            st = con.prepareStatement(this.obtenerSQL("SQL_DESC_SJ"));
            st.setString(1, NumeroPlanilla);
            st.setString(2, SJ);
            //st.setString(2, this.CONST_BASE);
            rs = st.executeQuery();
            while(rs.next()){
                DatosDescuentosImp datos = new DatosDescuentosImp();
                datos.setCodigo(rs.getString("CONCEPT_CODE"));
                datos.setValor(rs.getString("Valor"));
                datos.setTipo("Sj");
                datos.setCreacion_movpla(rs.getString("creation_date"));
                Lista.add(datos);
            }
            
            // otros anticipos
            st = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_ANTICIPOS_CGAGEN"));
            st.setString(1, NumeroPlanilla);
            //st.setString(2, this.CONST_BASE);
            float cant = 0;
            Float red = new Float(0.5);
            int sw=0;
            
            rs = st.executeQuery();
            if(rs.next()){
                DatosDescuentosImp datos = new DatosDescuentosImp();
                String cod = rs.getString("currency")!=null?rs.getString("currency"):"PES";
                cod = cod.equals("PES")?"ANTIC":"ANTIB";
                datos.setCodigo           (rs.getString("CONCEPT_CODE"));
                datos.setConceptoMigracion(cod);
                datos.setValor            (rs.getString("vlr"));
                datos.setProveedorMigracion(cod);
                datos.setTipo("CGAGEN");
                datos.setCreacion_movpla(rs.getString("creation_date"));
                datos.setDocumento(rs.getString("document"));
                cant = Float.parseFloat(datos.getValor());
                double cant1  = this.redondear(cant,0);
                datos.setValor(""+cant1);
                Lista.add(datos);
            }
            
            // BOLETAS DE COMBUSTIBLE
            st = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_BOLETAS"));
            st.setString(1, NumeroPlanilla);
            //st.setString(2, this.CONST_BASE);
            
            rs = st.executeQuery();
            if(rs.next()){
                DatosDescuentosImp datos = new DatosDescuentosImp();
                String cod = rs.getString("currency")!=null?rs.getString("currency"):"PES";
                cod = "TERCM";
                datos.setCodigo           (rs.getString("CONCEPT_CODE"));
                datos.setConceptoMigracion(cod);
                datos.setValor            (rs.getString("vlr"));
                datos.setProveedorMigracion(cod);
                datos.setTipo("CGAGEN");
                datos.setDocumento("");
                datos.setCreacion_movpla(rs.getString("creation_date"));
                cant = Float.parseFloat(datos.getValor());
                double cant1  = this.redondear(cant,0);
                datos.setValor(""+cant1);
                Lista.add(datos);
            }
            
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarAnticipos  (MST620 CARGA GENERAL)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_DESC_SJ");
        }
        return Lista;
    }
    
    /**
     * Funcion para actualizar el registro en el movpla con la fecha de migracion
     * @autor : Ing. Karen Reales
     * @param : String numero de la planilla
     * @version : 1.0
     */
    public String actualizarMigracion(String numpla, String creacion, String concept_code, String cheque)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String sql="";
        try {
            Connection con = this.conectar("SQL_ACTUALIZAR_MIGRACION");
            if(con!=null){
                if(concept_code.equals("01")&& !cheque.equals("")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_MIGRACION"));
                    st.setString(1, numpla);
                    st.setString(2, creacion);
                    st.setString(3, cheque);
                    //st.executeUpdate();
                    sql = st.toString();
                }
                else if(concept_code.equals("01") && cheque.equals("")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_MIGRACION_SINCHEQUE"));
                    st.setString(1, numpla);
                    st.setString(2, creacion);
                    //st.executeUpdate();
                    sql = st.toString();
                    
                    
                }
                else if(!concept_code.equals("01")){
                    st = con.prepareStatement(this.obtenerSQL("SQL_ACTUALIZAR_OTROSCONCEPTOS"));
                    st.setString(1, numpla);
                    st.setString(2, creacion);
                    st.setString(3,concept_code);
                    //st.executeUpdate();
                    sql = st.toString();
                    
                    
                }
            }
            
        }
        catch(Exception e) {
            throw new Exception("Error en rutina actualizarMigracion [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_ACTUALIZAR_MIGRACION");
        }
        return sql;
        
    }
    
     /* Funcion que busca los anticipos de una planilla
      * @autor  mfontalvo
      * @params NumeroPlanilla ............ Planilla a buscar
      * @params Sj ........................ Estandar de la planilla
      * @return Lista de anticipos de la planilla
      * @throws Exception.
      * @see BuscarCMAnticipos, BuscarCMAcpm, BuscarCMPeajes
      */
    
    
    List BuscarAnticipos(String NumeroPlanilla, String SJ)throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        try {
            // descuentos de papeleria
            Connection con  = this.conectar("SQL_DESC_PAPEL");
            String SQL_DESC     = this.obtenerSQL("SQL_DESC_PAPEL");
            st = con.prepareStatement(SQL_DESC);
            st.setString(1, SJ);
            rs = st.executeQuery();
            while(rs.next()){
                DatosDescuentosImp datos = new DatosDescuentosImp();
                datos.setDocumento("");
                datos.setCreacion_movpla("0099-01-01 00:00");
                datos.setCodigo(rs.getString("concept_code"));
                datos.setValor(rs.getString("vlr"));
                datos.setTipo("Sj");
                Lista.add(datos);
                
            }
            // otros anticipos
            String SQL_BUSCAR    =  this.obtenerSQL("SQL_BUSCAR_ANTICIPOS");
            st = con.prepareStatement(SQL_BUSCAR);
            st.setString(1, NumeroPlanilla);
            ////System.out.println("Consulta para anticipo: "+st.toString());
            //st.setString(2, this.CONST_BASE);
            float cant = 0;
            Float red = new Float(0.5);
            int sw=0;
            rs = st.executeQuery();
            while(rs.next()){
                DatosDescuentosImp datos = DatosDescuentosImp.load2(rs);
                datos.setDocumento("");
                datos.setCreacion_movpla("0099-01-01 00:00");
                //proveedor anticipo
                if(datos.getCodigo().equals("09")){
                    cant = Float.parseFloat(datos.getValor());
                }
                else if(datos.getCodigo().equals("01")){
                    datos.setProveedorMigracion(BuscarCMAnticipos(NumeroPlanilla,"01"));
                    cant = cant + Float.parseFloat(datos.getValor());
                    double cant1  = this.redondear(cant,0);
                    ////System.out.println(cant1);
                    datos.setValor(""+cant1);
                    Lista.add(datos);
                    sw=1;
                }
                //proveedor acpm
                else if(datos.getCodigo().equals("02")){
                    datos.setProveedorMigracion(BuscarCMAcpm(NumeroPlanilla));
                    Lista.add(datos);
                }
                //proveesor peaje
                else if(datos.getCodigo().equals("03")){
                    datos.setProveedorMigracion(BuscarCMPeajes(NumeroPlanilla));
                    Lista.add(datos);
                }
                
                /*else {
                    datos.setCodigo("DESC");
                    Lista.add(datos);
                }*/
            }
            if(sw==0 && cant!=0){
                ////System.out.println("Encontro q no hay efectivo pero si 09 y la cant no es 0");
                st = con.prepareStatement(SQL_BUSCAR);
                st.setString(1, NumeroPlanilla);
                rs = st.executeQuery();
                while(rs.next()){
                    DatosDescuentosImp datos = DatosDescuentosImp.load2(rs);
                    datos.setDocumento("");
                    datos.setCreacion_movpla("0099-01-01 00:00");
                    if(datos.getCodigo().equals("09")){
                        datos.setProveedorMigracion(BuscarCMAnticipos(NumeroPlanilla,"09"));
                        Lista.add(datos);
                    }
                }
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarAnticipos  (MS230828)  [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            this.desconectar("SQL_DESC_PAPEL");
        }
        return Lista;
    }
    
    
    void ActualizarFechaImpresion(String Planilla) throws SQLException {
        PreparedStatement st          = null;
        try {
            st = this.crearPreparedStatement("SQL_ACTUALIZAR_FECHA_IMPRESION");
            st.setString(1 ,  Planilla);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina ActualizarFechaImpresion [PlanillaImperesionDao]... \n"+e.getMessage());
        }
        finally{
            if(st!=null)  st.close();
            this.desconectar("SQL_ACTUALIZAR_FECHA_IMPRESION");
        }
    }
    
    /**
     * Funcion que busca todas las planillas no impresa o la
     * planilla setea  en el parametro
     * @autor  Ing. Juan M. Escand�n
     * @params NumeroPlanilla  planilla a imprimir
     * @params usuario  despachador
     * @params tipo  tipo de Busqueda (especifica o general)
     * @return Lista de Planillas a imprimir
     * @throws Exception.
     */
    
    
    List BuscarPlaNoImpPlanViaje(String NumeroPlanilla, String usuario,String tipo )throws Exception{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        List              Lista    = new LinkedList();
        
        try {
            if(tipo.equals("1")){
                st = this.crearPreparedStatement("SQL_BUSCAR_PLANILLA_NOIMPRESA_PV");
                st.setString(1, NumeroPlanilla);
            }
            else if(tipo.equals("2")){
                st = this.crearPreparedStatement("SQL_BUSCAR_PNI");
                st.setString(1,usuario);
            }
            rs = st.executeQuery();
            
            while(rs.next()){
                DatosPlanillaImp datos = DatosPlanillaImp.load(rs);
                datos.setPlanviaje( ( rs.getString("planviaje")  != null )?rs.getString("planviaje"):"" );
                // Buscando remesas de la planilla
                datos.setRemesas      ( BuscarRemesas( datos.getNumeroPlanilla()));
                // Buscando descuentos de la planilla
                datos.setDescuentos   ( BuscarDescuentos(datos.getNumeroPlanilla()));
                
                
                // mfontalvo
                // 2005-12-26
                // Soportes asigandos a cada remesa - re asignados a la planilla
                TreeMap Soportes = new TreeMap();
                // Observaciones de las remesas
                TreeMap obs      = new TreeMap();
                List remesas = datos.getRemesas();
                for (int i=0; i<remesas.size();i++){
                    TreeMap SoporteRem = ((DatosRemesaImp) remesas.get(i)).getSoportes();
                    Iterator keySoporteRem = SoporteRem.keySet().iterator();
                    while (keySoporteRem.hasNext()){
                        String key =(String) keySoporteRem.next();
                        Soportes.put(key, key);
                    }
                    obs.put( ((DatosRemesaImp) remesas.get(i)).getObservaciones(),
                    ((DatosRemesaImp) remesas.get(i)).getObservaciones() );
                }
                datos.setSoportes(Soportes);
                datos.setObservaciones( obs.values().toString().replaceAll("\\[|\\]",""));
                ////////////////////////////////////////////////////////////////
                
                Lista.add(datos);
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarPlanillasNoImp [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            
            
            // deconectar de la base de datos dependiendo del query
            if(tipo.equals("1"))
                this.desconectar("SQL_BUSCAR_PLANILLA_NOIMPRESA_PV");
            else if(tipo.equals("2"))
                this.desconectar("SQL_BUSCAR_PNI");
            
            
            
        }
        return Lista;
    }
}
