/*
 * InformePredoDAO.java
 *
 * Created on 18 de julio de 2005, 10:04 AM
 */

package com.tsp.operation.model;

/**
 *
 * @author  kreales
 */
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class InformePredoService {
    
    InformePredoDAO repDao;
    /** Creates a new instance of InformePredoDAO */
    public InformePredoService() {
        repDao =  new InformePredoDAO();
    }
    
    public void llenarInforme(String fechai, String fechaf)throws SQLException{
        
        repDao.llenarInforme(fechai, fechaf);
        
    }
    public void LlenarAgasoc( )throws SQLException{
        
        repDao.LlenarAgasoc();
    }
    /**
     * Getter for property datos.
     * @return Value of property datos.
     */
    public java.util.Vector getDatos() {
        return repDao.getDatos();
    }
    
    public void llenarInforme(String fechai, String fechaf, String agencia,String tipo)throws SQLException{
        
        repDao.llenarInforme(fechai, fechaf, agencia,tipo);
        
    }
    
    public java.util.Vector getAgencias() {
        return repDao.getAgencias();
    }
    public Vector ListarAgencias(String fec1,String fec2 )throws SQLException{
          return repDao.ListarAgencias(fec1,fec2 );    
    }
    
    public void ListarxAgencia(String agencia, String fec1,String fec2 )throws SQLException{
        repDao.ListarxAgencia(agencia, fec1,fec2);
    }
    public void buscarHistorial(String placa)throws SQLException{
        repDao.buscarHistorial(placa);
    }
     public java.util.Vector getHistorial() {
         return repDao.getHistorial();
     }     
    
     /**
      * Setter for property historial.
      * @param historial New value of property historial.
      */
     public void setHistorial(java.util.Vector historial) {
         repDao.setHistorial(historial);
     }
     public void llenarInforme()throws SQLException{
         repDao.llenarInforme();
     }
         public void llenarInforme(String fechai, String fechaf, String agencia,String tipo,String cliente, String zona)throws SQLException{
        
        repDao.llenarInforme(fechai, fechaf, agencia,tipo,cliente,zona);
        
    }

    
}
