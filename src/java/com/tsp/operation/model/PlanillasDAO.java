
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.*;



public class PlanillasDAO {
    
    
    //____________________________________________________________________________________________
    //                                          ATRIBUTOS
    //____________________________________________________________________________________________
    
    
    
    private static final String QUERY_NAME_CLIENTE  =" SELECT NOMCLI       FROM CLIENTE   WHERE CODCLI=?";
    private static final String QUERY_NAME_NIT      =" SELECT nombre       FROM NIT       WHERE cedula=?";
    private static final String QUERY_NAME_PROVEEDOR=" SELECT payment_name FROM PROVEEDOR WHERE dstrct=? AND nit=?";
    private static final String QUERY_NAME_CIUDAD   =" SELECT nomciu       FROM CIUDAD    WHERE codciu=?";
    private static final String QUERY_VALOR_PEAJE   =" SELECT value        FROM PEAJES    WHERE dstrct=? AND ticket_id=?";
    private static final String QUERY_NAME_AGENCIA  =" SELECT nombre       FROM AGENCIA   WHERE dstrct=? AND id_mims=?";
    
    
    
    
    private static final String QUERY_DATOS_OC=
    "SELECT         " +
    "             p.cia                   AS   DISTRITO,             " +
    "             p.numpla                AS   PLANILLA,             " +
    "             fechaposllegada       AS   FECHA_PLANEACION,     " +
    "             fecdsp                AS   FECHA_DESPCHO,        " +
    "             fecpla                AS   FECHA_PLANILLA,       " +
    "             printer_date          AS   FECHA_IMPRESION,      " +
    "             tipoviaje             AS   TIPO,                 " +
    "             agcpla                AS   CODIGO_AGENCIA,       " +
    "             oripla                AS   CODIGO_ORIGEN,        " +
    "             despla                AS   DESTINO,              " +
    "             plaveh                AS   PLACA,                " +
    "             cedcon                AS   CEDULA_CONDUCTOR,     " +
    "             nitpro                AS   CEDULA_PROPIETARIO,   " +
    "             pesoreal              AS   CANTIDAD_CARGA,       " +
    "             CASE WHEN movpla.ajuste is not null then vlrpla + movpla.ajuste else  vlrpla end              AS   VALOR1,               " +
    "             vlrpla2               AS   VALOR2,               " +
    "             unidcam               AS   MONEDA,               " +
    "             unit_vlr              AS   UNIDAD,               " +
    "             despachador           AS   CODIGO_DESPACHADOR,   " +
    "             platlr                AS   TRAILER,  " +
    "             precinto, " +
    "             contenedores," +
    "	     movpla.ajuste," +
    "        pr.numrem," +
    "        P.unit_cost," +
    "        p.reg_status," +
    "        case when c.cod_doc is not null then 'CUMPLIDA' else '' END as cumplido," +
    "	     case when d.numpla is not null then 'CON DISCREPANCIA' ELSE 'SIN DISCREPANCIA' END as discrepancia," +
    "        coalesce(to_char(doc.creation_date,'yyyy-MM-dd HH24:MI'),'0099-01-01') as fec_anul, 	" +
    "        c.cantidad AS cantidadC," +
    "        c.descripcion AS unidadC"+
    "     FROM                                                     " +
    "           (select * from planilla where  numpla       =  ? AND CIA = ?) p left outer join (select sum(vlr)as ajuste, planilla from movpla where concept_code  = '09' and planilla = ? group by planilla )movpla on (movpla.planilla = p.numpla )" +
    "           left join (SELECT " +
    "   			      cod_doc," +
    "   			      cantidad,      " +
    "   			      descripcion " +
    "   			FROM  " +
    "   			      cumplido" +
    "   			      left join tablagen t on (table_type='TUNIDAD' and table_code=unidad)" +
    "   			WHERE" +
    "   			      tipo_doc='001' and cod_doc=?) c on (c.cod_doc= p.numpla )  " +
    "	        left outer join (select numpla from discrepancia where numpla = ?)d on (d.numpla= p.numpla )  " +
    "           inner join plarem pr on ( pr.numpla = p.numpla  )" +
    "           left outer join (select creation_date,documento from documento_anulado)doc on (doc.documento= p.numpla )                                   " ;
    
    
    
    private static final String QUERY_LISTADO_ENCABEZADO="SELECT  "+
    "        a.cia,       "+
    "        a.numpla,    "+
    "        a.fecpla,    "+
    "        e.nomcli,    "+
    "        d.sj_desc,   "+
    "        d.sj         "+
    "FROM                 "+
    "       planilla       a, "+
    "       plarem         b, "+
    "       remesa         c, "+
    "       stdjobdetsel    d, "+
    "       CLIENTE         e  "+
    "WHERE               "+
    "         a.fecpla  between ?  and ?  "+
    "  and    a.reg_status=''             "+
    "  and    a.cia = ?                    ";
    
    
    
    private static final String QUERY_LISTADO_CONDICION="  and    b.cia = a.cia      "+
    "  and    b.reg_status=''          "+
    "  and    b.numpla = a.numpla      "+
    "  and    c.reg_status=''          "+
    "  and    c.cia = b.cia            "+
    "  and    c.numrem = b.numrem      "+
    "  and    d.reg_status=''          "+
    "  and    d.dstrct = c.cia         "+
    "  and    d.sj     = c.std_job_no  "+
    "  and    e.codcli = c.cliente     ";
    
    
    
    private static final  String QUERY_TIEMPOS_OC="SELECT         "+
    "      A.time_code          AS CODIGO,     "+
    "      A.secuence           AS SECUENCIA,  "+
    "      B.time_description   AS DESCRIPCION,"+
    "      A.date_time_traffic  AS FECHA       "+
    " FROM "+
    "     PLANILLA_TIEMPO    A,  "+
    "     TBLTIEMPO          B   "+
    " WHERE                      "+
    "         A.reg_status = ''  "+
    "     AND A.dstrct     = ?   "+
    "     AND A.pla        = ?   "+
    "     AND A.sj         = ?   "+
    "     AND B.reg_status = ''  "+
    "     AND B.dstrct     = A.dstrct    "+
    "     AND B.sj         = A.sj        "+
    "     AND B.cf_code    = A.cf_code   "+
    "     AND B.time_code  = A.time_code ";
    
    
    
    private static final String QUERY_TIEMPO_PUESTOS= " SELECT "+
    "      TIME_CODE_1        AS CODIGO1,    "+
    "      TIME_CODE_2        AS CODIGO2     "+
    "FROM  TIEMPO                            "+
    "WHERE                                   "+
    "         reg_status  = ''               "+
    "     AND DSTRCT      =  ?               "+
    "     AND SJ          =  ?               "+
    "     AND TIME_CODE_1 =  ?               ";
    
    
    
    
    private static final String QUERY_FECHA_CODIGO_TIEMPO="SELECT  date_time_traffic "+
    "FROM    PLANILLA_TIEMPO   "+
    "WHERE   DSTRCT      = ?   "+
    "    AND PLA         = ?   "+
    "    AND SJ          = ?   "+
    "    AND TIME_CODE   = ?   ";
    
    
    //Diogenes 11-04-2006
     private static final String QUERY_CONSULTAR_OC=
    "SELECT         " +
    "             p.cia                 AS   DISTRITO,             " +
    "             p.numpla              AS   PLANILLA,             " +
    "             fechaposllegada       AS   FECHA_PLANEACION,     " +
    "             fecdsp                AS   FECHA_DESPCHO,        " +
    "             fecpla                AS   FECHA_PLANILLA,       " +
    "             printer_date          AS   FECHA_IMPRESION,      " +
    "             tipoviaje             AS   TIPO,                 " +
    "             agcpla                AS   CODIGO_AGENCIA,       " +
    "             plaveh                AS   PLACA,                " +
    "             cedcon                AS   CEDULA_CONDUCTOR,     " +
    "             nitpro                AS   CEDULA_PROPIETARIO,   " +
    "             pesoreal              AS   CANTIDAD_CARGA,       " +
    "             vlrpla             AS   VALOR1,               " +
    "             vlrpla2               AS   VALOR2,               " +
    "             unidcam               AS   MONEDA,               " +
    "             unit_vlr              AS   UNIDAD,               " +
    "             despachador           AS   CODIGO_DESPACHADOR,   " +
    "             platlr                AS   TRAILER,  " +
    "             precinto, " +
    "             contenedores," +
    "        P.unit_cost," +
    "        p.reg_status," +
    "        case when c.cod_doc is not null then 'CUMPLIDA' else '' END as cumplido," +
    "	     case when d.numpla is not null then 'CON DISCREPANCIA' ELSE 'SIN DISCREPANCIA' END as discrepancia," +
    "        coalesce(to_char(doc.creation_date,'yyyy-MM-dd HH24:MI'),'0099-01-01') as fec_anul, 	" +
    "        c.cantidad AS cantidadC," +
    "        c.descripcion AS unidadC," +
    "        c.creation_user As usuarioC," +
    "        p.feccum  AS FechaCumplido," +
    "        get_nombreciudad(agcpla) AS agenciapla," +
    "        get_nombreciudad(oripla) AS origen,    " +
    "        get_nombreciudad(despla) AS destino,   " +
    "        get_nombrenit(cedcon) AS nom_conductor,	" +
    "        get_nombrenit(nitpro) AS nom_propietario," +
    "        p.cmc   "+
    "     FROM                                                     " +
    "           (select * from planilla where  numpla       =  ? AND CIA = ?) p" +
    "           left join (SELECT " +
    "   			      cod_doc," +
    "   			      cantidad,      " +
    "   			      descripcion," +
    "                                 cumplido.creation_user " +
    "   			FROM  " +
    "   			      cumplido" +
    "   			      left join tablagen t on (table_type='TUNIDAD' and table_code=unidad)" +
    "   			WHERE" +
    "   			      tipo_doc='001' and cod_doc=?) c on (c.cod_doc= p.numpla )  " +
    "	        left outer join (select numpla from discrepancia where distrito = ? AND numpla = ?)d on (d.numpla= p.numpla )  " +
    "           left outer join (select dstrct,creation_date,documento,tipodoc from documento_anulado)doc on ( doc.dstrct = p.cia and doc.documento= p.numpla and doc.tipodoc='001' )                                   " ;
    
    
    
    //____________________________________________________________________________________________
    //                                          METODOS
    //____________________________________________________________________________________________
    
    
    
    public PlanillasDAO(){
    }
    
    
    List queryConsultaListadoOC(int tipo,String distrito,String fecha1, String fecha2) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        List listaPlanilla=null;
        try{
            String condicion=(tipo==3)?" and  b.feccum='0099-01-01 00:00:00' ":"and  b.feccum <>'0099-01-01 00:00:00'";
            st= conPostgres.prepareStatement(QUERY_LISTADO_ENCABEZADO + condicion + QUERY_LISTADO_CONDICION);
            st.setString(1,fecha1);
            st.setString(2,fecha2);
            st.setString(3,distrito);
            rs=st.executeQuery();
            if(rs!=null){
                int sw=0;
                while(rs.next()){
                    if(sw==0){
                        listaPlanilla = new LinkedList();
                        sw=1;
                    }
                    Planillas planilla = new Planillas();
                    planilla.setDistrito(rs.getString(1));
                    planilla.setPlanilla(rs.getString(2) );
                    planilla.setFechaDespacho(rs.getString(3) );
                    planilla.setCliente(rs.getString(4) );
                    planilla.setSJDescripcion("["+rs.getString(6)+"]&nbsp " + rs.getString(5) );
                    listaPlanilla.add(planilla);
                }
            }
        }catch(Exception e){
            throw new SQLException("No se pudo consultar planillas " +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return listaPlanilla;
    }
    
    
    
    Planillas queryConsultaOC(String distrito,String oc,String base, String ot) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        Planillas planilla=null;
        try{
            st= conPostgres.prepareStatement(QUERY_DATOS_OC);
            st.setString(1,oc);
            st.setString(2,distrito);
            st.setString(3,oc);
            st.setString(4,oc);
            st.setString(5,oc);
            ////System.out.println("Busca planilla "+st);
            rs=st.executeQuery();
            if(rs!=null){
                Model model = new Model();
                while(rs.next()){
                    planilla = new Planillas();
                    planilla.setDistrito(rs.getString(1) );
                    planilla.setPlanilla(rs.getString(2) );
                    planilla.setFechaPlaneacion(rs.getString(3));
                    planilla.setFechaDespacho(rs.getString(4));
                    planilla.setFechaCreacion(rs.getString(5));
                    planilla.setFechaImpresion(rs.getString(6));
                    planilla.setTipo(rs.getString(7));
                    planilla.setAgenciaDespacho(rs.getString(8));
                    planilla.setOrigen(BuscarNombre(rs.getString(1),rs.getString(9),4 ) );
                    planilla.setDestino(BuscarNombre(rs.getString(1),rs.getString(10),4 ) );
                    planilla.setPlaca(rs.getString(11));
                    planilla.setCedulaConductor(rs.getString(12));
                    planilla.setCedulaPropietario(rs.getString(13));
                    planilla.setConductor( BuscarNombre(rs.getString(1), rs.getString(12), 1 ) );
                    planilla.setPropietario( BuscarNombre(rs.getString(1), rs.getString(13), 1 )   );
                    planilla.setCarga(rs.getDouble(14));
                    double valor=(rs.getDouble(15)==0)?rs.getDouble(16):rs.getDouble(15);
                    planilla.setValor(valor);
                    planilla.setMoneda( rs.getString(17));
                    planilla.setUnidad( rs.getString(18));
                    planilla.setCedulaDespachador( rs.getString(19));
                    planilla.setDespachador( rs.getString("CODIGO_DESPACHADOR"));
                    planilla.setPrecintos(rs.getString("precinto"));
                    planilla.setContenedores(rs.getString("contenedores"));
                    planilla.setValorFlete(rs.getDouble("unit_cost"));
                    planilla.setTrailer(rs.getString("trailer"));
                    planilla.setEstado(rs.getString("reg_status").equals("A")?"ANULADA":"ACTIVA");
                    planilla.setCumplido("");
                    planilla.setCantCumplida(rs.getString("cantidadC"));
                    planilla.setUnidadCumplida(rs.getString("unidadC"));
                    if(!rs.getString("cumplido").equals("")){
                        planilla.setCumplido("  "+rs.getString("cumplido")+"-"+rs.getString("discrepancia"));
                    }
                    planilla.setFecha_anul(rs.getString("fec_anul").equals("0099-01-01")?"No anulada":rs.getString("fec_anul"));
                    //DATOS ESPECIFICOS PARA DESPACHO NORMAL
                    
                    
                    //inicializo los datos de la remesa para q no salgan null
                    planilla.setFechaCumplido( "" );
                    planilla.setAgenciaCumplido( "");
                    planilla.setRemesa( "" );
                    planilla.setSJ( "" );
                    planilla.setSJDescripcion( "");
                    planilla.setCliente( "");
                    planilla.setNitCliente( "" );
                    
                    if(ot.equals("NINGUNA")){
                        ot = rs.getString("numrem");
                    }
                    
                    //--- Datos de la remesa
                    DatosRem rem =  model.RemisionSvc.searchDatosRem(planilla.getDistrito(),planilla.getPlanilla(),ot);
                    if(rem!=null){
                        planilla.setFechaCumplido( rem.getFechaCumplido() );
                        planilla.setAgenciaCumplido( rem.getAgenciaCumplido());
                        planilla.setRemesa( rem.getRemesa() );
                        planilla.setRemision(rem.getNoRemision());
                        planilla.setSJ( rem.getStj() );
                        planilla.setSJDescripcion( rem.getDescripcion());
                        planilla.setCliente( rem.getCliente());
                        planilla.setNitCliente( BuscarNombre(rs.getString(1), rem.getCliente(), 6 ) );
                    }
                    
                    //--- Valores Anticipos
                    ValorAnticipo anticipoACPM  = model.RemisionSvc.searchAnticipo( planilla.getDistrito(), planilla.getAgenciaDespacho(), planilla.getPlanilla(), "02");
                    ValorAnticipo anticipoPeaje = model.RemisionSvc.searchAnticipo( planilla.getDistrito(), planilla.getAgenciaDespacho(), planilla.getPlanilla(), "03");
                    double valorACPM     = 0;
                    double valorPeaje    = 0;
                    if(anticipoACPM!=null)   valorACPM =(anticipoACPM.getVlr()==0)?anticipoACPM.getVlrFor():anticipoACPM.getVlr();
                    if(anticipoPeaje!=null)  valorPeaje=(anticipoPeaje.getVlr()==0)?anticipoPeaje.getVlrFor():anticipoPeaje.getVlr();
                    planilla.setValorACPM( valorACPM );
                    planilla.setValorPeajes( valorPeaje );
                    
                    //--- Datos Auxiliares
                    Plaaux  aux = model.RemisionSvc.searchAux( planilla.getDistrito(), planilla.getPlanilla());
                    if(aux!=null){
                        planilla.setGalonesACPM((double)aux.getAcpm_gln());
                        planilla.setProveedorACPM( BuscarNombre(planilla.getDistrito(),aux.getAcpm_supplier(),1));
                        planilla.setNitProveedorACPM(aux.getAcpm_supplier());
                        double cantPeaje = aux.getTicket_a() + aux.getTicket_b() + aux.getTicket_c();
                        planilla.setCantidadPeajes((int)cantPeaje);
                        planilla.setValorPeajes(valorPeaje);
                        planilla.setNitProveedorPeajes( aux.getTicket_supplier());
                        planilla.setProveedorPeajes(BuscarNombre(planilla.getDistrito(),aux.getTicket_supplier(),1));
                    }
                    
                    //--- Movimientos de la planilla
                    List movimientos = model.RemisionSvc.searchMovimiento(planilla.getPlanilla(), planilla.getDistrito(), planilla.getAgenciaDespacho());
                    if(movimientos!=null || movimientos.size()>0){
                        Iterator itM = movimientos.iterator();
                        while(itM.hasNext()){
                            AnticipoPlanilla mov = (AnticipoPlanilla) itM.next();
                            /*Banco banco = model.RemisionSvc.searchBancos(planilla.getDistrito(), mov.getAgencia(), mov.getMoneda(),base);
                            ////System.out.print("--->" + planilla.getDistrito() + mov.getAgencia() + mov.getMoneda());
                            mov.setAgencia(BuscarNombre( planilla.getDistrito(), mov.getAgencia(),4 ));
                            if(banco!=null){
                                mov.setBanco( banco.getBanco());
                                mov.setAgenciaBanco( banco.getNombre_Agencia());
                                mov.setCuenta( banco.getCuenta());
                            }*/
                        }
                    }
                    planilla.setListaMovimiento(movimientos);
                    
                    //--- Tiempos de Viajes
                    planilla.setListaTiempo(this.BuscarTiempos(planilla.getDistrito(),planilla.getPlanilla(),planilla.getSJ()));
                    
                    //--- Mas datos
                    planilla.setAgenciaDespacho(BuscarNombre(rs.getString(1),rs.getString(8),4) );
                    
                    
                    break;
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de la planilla "+oc +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return planilla;
    }
    
    
    
    String BuscarNombre(String distrito,String nit, int tipo)throws SQLException {
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        String nombre="";
        try{
            switch (tipo){
                case 1://conductor
                    st= conPostgres.prepareStatement(QUERY_NAME_NIT);
                    st.setString(1,nit);
                    break;
                case 2://propietario y proveedor acpm
                    st= conPostgres.prepareStatement(QUERY_NAME_PROVEEDOR);
                    st.setString(1,distrito);
                    st.setString(2,nit);
                    
                    break;
                case 4://ciudades
                    st= conPostgres.prepareStatement(QUERY_NAME_CIUDAD);
                    st.setString(1,nit);
                    break;
                case 5://agencias
                    st= conPostgres.prepareStatement(QUERY_NAME_AGENCIA);
                    st.setString(1,distrito);
                    st.setString(2,nit);
                    break;
                case 6://cliente
                    st= conPostgres.prepareStatement(QUERY_NAME_CLIENTE);
                    st.setString(1,nit);
                    break;
            }
            rs=st.executeQuery();
            while(rs.next())
                nombre=rs.getString(1);
        }catch(Exception e){
            throw new SQLException("Error intentando buscar el nombre correspondiente al NIT " + nit +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return nombre;
    }
    
    
    
    
    
    
    //____________________________________________________________________________________________
    //                                          METODOS
    //____________________________________________________________________________________________
    
    
    
    
    //Busca los tiempos asociados a la planilla
    List BuscarTiempos(String distrito, String oc, String sj) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        List lista=null;
        try{
            st= conPostgres.prepareStatement(QUERY_TIEMPOS_OC);
            st.setString(1, distrito);
            st.setString(2, oc);
            st.setString(3, sj);
            rs=st.executeQuery();
            if(rs!=null){
                lista = new LinkedList();
                while(rs.next()){
                    TiempoPlanilla  tiempo = new TiempoPlanilla();
                    tiempo.setCodigo(rs.getString(1));
                    tiempo.setSecuencia(rs.getInt(2));
                    tiempo.setDescripcion(rs.getString(3));
                    tiempo.setFecha(rs.getString(4));
                    tiempo.setCodigo1("");
                    tiempo.setCodigo2("");
                    tiempo.setDiferencia("");
                    PreparedStatement st2=null;
                    st2= conPostgres.prepareStatement(QUERY_TIEMPO_PUESTOS);
                    st2.setString(1, distrito);
                    st2.setString(2, sj);
                    st2.setString(3, rs.getString(1));
                    ResultSet rs2=null;
                    rs2=st2.executeQuery();
                    int ss=0;
                    if(rs2!=null)
                        while(rs2.next()){
                            ss++;
                            tiempo.setCodigo1(rs2.getString(1));
                            tiempo.setFecha_time1(BuscarFechaDelTiempo(distrito,oc,sj,rs2.getString(1)));
                            tiempo.setCodigo2(rs2.getString(2));
                            tiempo.setFecha_time2(BuscarFechaDelTiempo(distrito,oc,sj,rs2.getString(2)));
                        }
                    // if(ss>0)
                    //  tiempo.setDiferencia(tiempo.BuscarDiferenciaFecha(tiempo.getFecha_time1(),tiempo.getFecha_time2()));
                    
                    lista.add(tiempo);
                }
            }
            
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de Tiempos para la planilla "+oc +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return lista;
    }
    
    
    
    
    
    
    
    //Busca la fecha del codigo de Tiempo
    String BuscarFechaDelTiempo(String distrito, String oc, String sj,String tiempo) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        String fecha="";
        PreparedStatement st=null;
        ResultSet rs=null;
        try{
            st= conPostgres.prepareStatement(QUERY_FECHA_CODIGO_TIEMPO);
            st.setString(1, distrito);
            st.setString(2, oc);
            st.setString(3, sj);
            st.setString(4, tiempo);
            rs=st.executeQuery();
            if(rs!=null)
                while(rs.next())
                    fecha=rs.getString(1);
        }
        catch(SQLException e){
            throw new SQLException("Error en la busqueda diferencia de Tiempo" +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return fecha;
    }
    
    
     
    /**
     * Metodo consultarOC, busca la informacion correspondiente a una planilla,
     * @param: distrito, oc, base
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
    
   public Planillas consultarOC(String distrito,String oc,String base) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st=null;
        ResultSet rs=null;
        Planillas planilla=null;
        try{
            st= conPostgres.prepareStatement(QUERY_CONSULTAR_OC);
            st.setString(1,oc);
            st.setString(2,distrito);
            st.setString(3,oc);
            
            st.setString(4,distrito);
            st.setString(5,oc);
            rs=st.executeQuery();
            if(rs!=null){
                Model model = new Model();
                while(rs.next()){
                    planilla = new Planillas();
                    planilla.setDistrito(rs.getString("distrito") );
                    planilla.setPlanilla(rs.getString("planilla") );
                    planilla.setFechaPlaneacion(rs.getString("fecha_planeacion"));
                    planilla.setFechaDespacho(rs.getString("fecha_despcho"));
                    planilla.setFechaCreacion(rs.getString("fecha_planilla"));
                    planilla.setFechaImpresion(rs.getString("fecha_impresion"));
                    planilla.setTipo(rs.getString("tipo"));
                    planilla.setCod_agencia_despacho(rs.getString("codigo_agencia"));
                    planilla.setAgenciaDespacho(rs.getString("agenciapla"));
                    planilla.setOrigen(rs.getString("origen") );
                    planilla.setDestino(rs.getString("destino"));
                    planilla.setPlaca(rs.getString("placa"));
                    planilla.setCedulaConductor(rs.getString("cedula_conductor"));
                    planilla.setCedulaPropietario(rs.getString("cedula_propietario"));
                    planilla.setConductor( rs.getString("nom_conductor") );
                    planilla.setPropietario( rs.getString("nom_propietario")   );
                    planilla.setCarga(rs.getDouble("cantidad_carga"));
                    double valor=rs.getDouble("valor1");
                    planilla.setValorMe(rs.getDouble("valor2"));
                    planilla.setValor(valor);
                    planilla.setMoneda( rs.getString("moneda"));
                    planilla.setUnidad( rs.getString("unidad"));
                    planilla.setDespachador( rs.getString("codigo_despachador"));
                    planilla.setPrecintos(rs.getString("precinto"));
                    planilla.setContenedores(rs.getString("contenedores"));
                    planilla.setValorFlete(rs.getDouble("unit_cost"));
                    planilla.setTrailer(rs.getString("trailer"));
                    planilla.setEstado(rs.getString("reg_status").equals("A")?"ANULADA":"ACTIVA");
                    
                    planilla.setCantCumplida(rs.getString("cantidadC"));
                    planilla.setUnidadCumplida(rs.getString("unidadC"));
                    planilla.setCumplido(rs.getString("cumplido"));
                    planilla.setDiscrepancia(rs.getString("discrepancia"));
                    
                    planilla.setFecha_anul(rs.getString("fec_anul").equals("0099-01-01")?"No anulada":rs.getString("fec_anul"));
                    planilla.setFechaCumplido(rs.getString("fechacumplido").substring(0,16));
                    planilla.setUsuario_cumplio(rs.getString("usuarioc"));
                    planilla.setCmc(rs.getString("cmc"));
                    
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en la busqueda de la planilla "+oc +" --> "+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return planilla;
    }
    
    
    
}// FIN DAO
