
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.beans.*;

public class ConductorService {
    
   private ConductorDAO  ConductorDataAccess;
   private Conductor     conductor;
   private Nit           nit;
   private List          ciudades;
   private List          departamentos;
   private List          paises;
   
   
 // Inicializamos las variables  
    public ConductorService() {
        ConductorDataAccess = new ConductorDAO();
        conductor = null;
        nit       = null;
        ciudades  = null;
        departamentos=null;
        paises    = null;
    }
    
  // resetiamos las variables  
    public  void reset(){
        ConductorDataAccess.reset();
    }
    //__________________________________________________
   // SEARCH
    
  
   //--- buscamos un conductor especifico en la tabla conductor 
    public void searchConductor(String id) throws Exception{
        try{
           conductor = ConductorDataAccess.buscarConductor(id);  
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
   //--- devolvemos el conductor
    public Conductor getConductor(){
        return this.conductor;
    }
 
    //-- 
    public void setConductor(Conductor object){
        this.conductor=object;
    }
    
    public void setNit(Nit object){
        this.nit=object;
    }
    
   //--- buscamos un conductor especifico en la tabla conductor 
    public void searchNit(String id) throws Exception{
        try{
           nit = ConductorDataAccess.buscarNit(id);  
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
   //--- devolvemos el conductor
    public Nit getNit(){
        return this.nit;
    }
 
   
   //--- buscamos las ciudades 
    public List searchCiudades() throws Exception {
        try{
           ciudades = ConductorDataAccess.ciudades();  
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        } 
        return ciudades;
    }
    
   //--- devolvemos la lista de ciudades 
    public  List getCiudades(){
        return this.ciudades;
    }
    
    
    
    
    //--- buscamos los departamentos 
    public List searchDepartamentos() throws Exception {
        try{
           departamentos = ConductorDataAccess.departamentos();  
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        } 
        return departamentos;
    }
    
   //--- devolvemos la lista de departamentos 
    public List getDepartamentos(){
        return this.departamentos;
    }
    
    
    
    
    
    //--- buscamos los paises 
    public List searchPaises() throws Exception {
        try{
           paises = ConductorDataAccess.paises();  
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        } 
        return paises;
    }
    
   //--- devolvemos la lista de paises 
    public List getPaises(){
        return this.paises;
    }
    
    
    
    
    
    
    
    
    
    
    
   //__________________________________________________
   // DELETE (esto es solo cambiar el reg_status)
  
    
    public void delete(String id)throws Exception{
        try{
           ConductorDataAccess.eliminar(id);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    }
    
    
    
    
   //__________________________________________________
   // INSERT
   
    
   // insercion en la tabla conductor
    
    /*public void insertConductor(Conductor conductor,String usuario)throws Exception{
        try{
           ConductorDataAccess.insertConductor(conductor,usuario);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    } */
    
    
   // inserción en la tabla nit
    public void insertNit(Nit nit,String usuario)throws Exception{
        try{
           ConductorDataAccess.insertNit(nit,usuario);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    }
    
    
    
    
    //__________________________________________________
   // UPDATE
   
    
   // modificamos en la tabla conductor
    
    /*public void updateConductor(Conductor conductor,String usuario)throws Exception{
        try{
           ConductorDataAccess.updateConductor(conductor,usuario);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    } */
    
    
   // modificamos en la tabla nit
    public void updateNit(Nit nit,String usuario)throws Exception{
        try{
           ConductorDataAccess.updateNit(nit,usuario);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    }
    
    
    
    
    
   // vector para la jsp
    public String getVector()throws Exception{
       String vector="";
       try{
          List listaCiudades = this.searchCiudades();
          Iterator it = listaCiudades.iterator();
          if(it!=null){
             vector="var datos=[";
             int sw=0;
             while(it.hasNext()){
                CodeValue objeto=(CodeValue) it.next();
                if(sw>0) vector+=",";
                vector+="['" + objeto.getCodigo() + "-" +objeto.getDescripcion().toUpperCase()+"-" + objeto.getReferencia()+"']";
                sw++;
             }
            vector+="]";
          }
       }catch(Exception e){
          throw new Exception( e.getMessage() );
       }
       return vector;
     }

    
    public Conductor getCond() throws SQLException{
        
        return ConductorDataAccess.getConductor();
    }
    
    /*public void consultaConductor(String cedula) throws SQLException{
        try{
            ConductorDataAccess.consultaConductor(cedula);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }*/
    public Vector getConductores()throws SQLException{
        return ConductorDataAccess.getConductores();
    }
    public void buscaConductor(String cedula) throws SQLException{
        try{
            ConductorDataAccess.searchConductor(cedula);
            conductor =ConductorDataAccess.getConductor(); 
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean conductorExist(String cedula) throws SQLException{
        
        boolean sw = false;
        try{
            sw = ConductorDataAccess.nitExist(cedula);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        return sw;
    }
    public void consultaConductor(String cedula) throws SQLException{
        try{
            ConductorDataAccess.consultaConductor(cedula);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public boolean nitVetado(String cedula) throws SQLException{
        return ConductorDataAccess.nitVetado(cedula);
    }
    /*****Modificacions Diogenes 121005***/
     public void buscar_Conductor(String id) throws Exception{
        try{
           ConductorDataAccess.buscar_Conductor(id); 
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    public Conductor obtConductor(){
        return ConductorDataAccess.getConductor(); 
    }
    
    public boolean existerConductorAnul(String id)throws Exception{
        return ConductorDataAccess.existerConductorAnul(id);
    }
    
    public void updateConductor(Conductor conductor)throws Exception{
        try{
           ConductorDataAccess.updateConductor(conductor);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    } 
    public void insertConductor(Conductor conductor)throws Exception{
        try{
           ConductorDataAccess.insertConductor(conductor);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    } 
    
    public void  obtConductoresPorNombre(String id)throws Exception{
         ConductorDataAccess.obtConductoresPorNombre(id);
     }
    
    public boolean existeConductor(String id) throws SQLException{
       return ConductorDataAccess.existeConductor(id);
    }
    /*****Modificacions Diogenes 121005***/
    /**
     * Metodo buscar_ConductorCGA, busca un conductor sin tener en cuenta su estado
     * param: ducumento del conductor
     * @see   : buscar_ConductorCGA - ConductorDAO()
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */ 
     public void buscar_ConductorCGA(String id) throws Exception{
        try{
           ConductorDataAccess.buscar_ConductorCGA(id); 
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }    
  
    /**
     * Metodo existerConductorAnulCGA, returna true si existe el conductor
     * anulado, sino false 
     * param: documento conductor
     * @see   : existerConductorAnulCGA - ConductorDAO()
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existerConductorAnulCGA(String id)throws Exception{
        return ConductorDataAccess.existerConductorAnulCGA(id);
    }
    /**
     * Metodo updateConductorCGA, modifica un registro a la tabla conductor,
     * param: objeto tipo conductor
     * @see   : updateConductorCGA - ConductorDAO()
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void updateConductorCGA(Conductor conductor)throws Exception{
        try{
           ConductorDataAccess.updateConductorCGA(conductor);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    } 
    /**
     * Metodo insertConductorCGA, inserta un registro a la tabla conductor,
     * param: objeto de tipo conductor
     * @see   : insertConductorCGA - ConductorDAO()
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void insertConductorCGA(Conductor conductor)throws Exception{
        try{
           ConductorDataAccess.insertConductorCGA(conductor);  
        }catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    }
    
    /**
     * Metodo cambiarEstadoConductor, cambia el estado, de un conductor
     * Activo o Inactivo
     * param: documento conductor, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: cambiarEstadoConductor - ConductorDAO
     * @version : 1.0
     */
    public void cambiarEstadoConductor(String doc, String estado )throws Exception {
        ConductorDataAccess.cambiarEstadoConductor(doc, estado);
    }
    /**
     * Metodo cambiarEstadoConductor, cambia el estado, de un conductor
     * Activo o Inactivo
     * param: documento conductor, estado
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: cambiarEstadoConductor - ConductorDAO
     * @version : 1.0
     */
    public void cambiarEstadoNit(String doc, String estado )throws Exception {
        ConductorDataAccess.cambiarEstadoNit(doc, estado); 
    }
    /**
     * Metodo buscarConductorCGA, busca el conductor 
     * param: documento conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: buscarConductorCGA - ConductorDAO
     * @version : 1.0
     */
     public void buscarConductorCGA(String id)throws Exception{
         ConductorDataAccess.buscarConductorCGA(id);
     }
     
      /**
     * Metodo existeConductorCGA, busca un conductor retirna true si esta de lo contrario false
     * param: documento del conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeConductorCGA(String id)throws Exception{
        try{
            return ConductorDataAccess.existeConductorCGA(id);
        }
        catch(Exception e){
           throw new Exception( e.getMessage() );
        } 
    }
    /* Metodo obtConductorPorCedula, 
     * descripcion: metodo service que llama al metodo obtConductorPorCedula del DAo
     *              para obtener el nombre del conductor por una cedula
     * param: cedula del conductor
     * @see   :obtConductorPorCedula - ConductorDAO()
     * @autor : Ing. Andres Martinez
     * @version : 1.0
     */ 
    public String obtConductorPorCedula(String id)throws Exception{
        return ConductorDataAccess.obtConductorPorCedula(id);
    }
    
    public Vector getPropietarios()throws SQLException{        
        return ConductorDataAccess.getPropietarios();        
 }
    
    /**
     * Metodo buscarConductorCGA, busca el conductor 
     * param: documento conductor
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: buscarConductorCGA - ConductorDAO
     * @version : 1.0
     */
     public void buscarNit(String id)throws Exception{
         ConductorDataAccess.buscarNitCGA(id);  
     }
    
}// FIN SERVICE
