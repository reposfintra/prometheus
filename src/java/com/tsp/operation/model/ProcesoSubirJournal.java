/*
 * ProcesoSubirJournal.java
 *
 * Created on 21 de febrero de 2005, 10:44 AM
 */

package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.text.*;

/**
 *
 * @author  KREALES
 */
public class ProcesoSubirJournal extends Thread{
    
    /** Creates a new instance of ProcesoSubirJournal */
    private Model  model;
    private String nombre_archivo;
    private PrintStream log;
    /** Creates a new instance of HPlaca */
    public void start(Model model, String nombre) {
        this.model = model;
        this.nombre_archivo = nombre;
        super.start();
    }
    
    public synchronized void run() {
        
        try{
            log = new PrintStream(new FileOutputStream("/exportar/masivo/log_hilo.txt"));
            log.println("Inicio el proceso....");
            ////System.out.println("Inicio el proceso....");
            Vector list = new Vector();
            File lista = new File("/exportar/masivo/Sincronizacion/servidor/recibido/");
            File[] vec=lista.listFiles();
            int sw=0;
            for(int i = 0; i<vec.length; i++){
                if(i==0)
                    log.println("Empiezo a recorrer los archivos de la carpeta....");
                File archivo  = vec[i];
                log.println("Nombre del archivo: "+archivo.getName());
                if(archivo.getName().equalsIgnoreCase(nombre_archivo)){
                    ////System.out.println("Este archivo es el q se va a subir: "+archivo.getName());
                    log.println("Este archivo es el q se va a subir: "+archivo.getName());
                    sw=1;
                    leerArchivo(archivo.getName());
                    log.println("Termine de subir el archivo: "+archivo.getName());
                    ////System.out.println("Termine de subir el archivo: "+archivo.getName());
                }
                else if(sw==1){
                    log.println("Estos es el archivo q voy a subir despues del original: "+archivo.getName());
                    leerArchivo(archivo.getName());
                }
            }
            log.println("FIN DEL PROCESO....");
            ////System.out.println("FIN DEL PROCESO....");
        }catch(Exception e){
            log.println("HA OCURRIDO UN ERROR....");
        }
    }
    
    public void leerArchivo(String nombre){
        
        Connection con=null;
        Statement st = null,st1 = null;
        ResultSet rs = null, rs1=null,rs2=null,rs3=null,rs4=null;
        PoolManager poolManager = null;
        
        poolManager = PoolManager.getInstance();
        con = poolManager.getConnection("fintra");
        try{
            if(con!=null){
                st = con.createStatement();
                boolean autocommit = con.getAutoCommit();
                BufferedReader in = new BufferedReader(new FileReader("/exportar/masivo/Sincronizacion/servidor/recibido/"+nombre));
                String row="";
                int sw=1;
                int i =0;
                while(sw==1){
                    try {
                        row = in.readLine();
                        if(row==null)
                            sw=0;
                        else{
                            ////System.out.println(row);
                            st.executeUpdate(row);
                        }
                    }catch(Exception e){
                        //////System.out.println( e.getMessage());
                        continue;
                        
                    }
                    
                }
            }
        }catch(Exception e){
            
        }finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(Exception e){
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    
    
}
