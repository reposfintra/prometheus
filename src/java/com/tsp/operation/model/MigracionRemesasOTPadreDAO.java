/*
 * MigracionRemesasAnuladasDAO.java
 *
 * Created on 21 de julio de 2005, 01:25 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  Henry
 */
public class MigracionRemesasOTPadreDAO {
	
	
    private Vector remesasOTPadre;
    private static final String SQL_LISTAR_REMESAS_PADRES = "SELECT DISTINCT REMESA_PADRE, DESCRIPTION, U.NIT AS CREATION_USER, "+
                                                            "REMESA FROM REMESA_PADRE RP, USUARIOS U, mov_OT_PADRE WHERE CODE=REMESA_PADRE "+
                                                            "AND TIP_REMESA=? AND RP.CREATION_USER=U.IDUSUARIO";
    private String dataBaseName;
    /** Creates a new instance of MigracionRemesasAnuladasDAO */
    public MigracionRemesasOTPadreDAO() {
    }
    public MigracionRemesasOTPadreDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    
    public void obtenerRemesasPadres() throws java.sql.SQLException{    
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;        
        PoolManager poolManager = null;        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            remesasOTPadre = new Vector();
            if(con!=null){
                st = con.prepareStatement(this.SQL_LISTAR_REMESAS_PADRES);
                st.setString(1,"P");
                rs = st.executeQuery();                                
                while(rs.next()){      
                    Vector datos = new Vector();
                    datos.addElement(rs.getString(1));                    
                    datos.addElement(rs.getString(2));
                    datos.addElement(rs.getString(3));
                    datos.addElement(rs.getString(4));
                    this.remesasOTPadre.addElement(datos);
                }                
            }    
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }  
    }       
    public Vector getRemesasPadres() {
        return this.remesasOTPadre;
    }
}
