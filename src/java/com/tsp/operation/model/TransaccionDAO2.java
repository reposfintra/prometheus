/*
 * Transaccion.java
 *
 * Created on 21 de enero de 2005, 04:08 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  KREALES
 */
public class TransaccionDAO2  extends MainDAO {
    
    /** Creates a new instance of Transaccion */
    
    
    
    //Connection con=null;
    //Statement st = null;
    ResultSet rs = null;
    int swCon    = 0;
    private  String sql      = "";
    
    public TransaccionDAO2()  {
        super("TransaccionDAO.xml");        
    }
    public TransaccionDAO2(String dataBaseName)  {
        super("TransaccionDAO.xml", dataBaseName);        
    }
  /*  public Statement getSt(){
        return st;
    }*/
    
    //@todo Metodo nuevo public Connection conectar() throws SQLException
    /*public Connection conectar() throws SQLException {
      Connection conn = this.conectarBDJNDI("sot");
      conn.setAutoCommit(false);
      return conn;
    }*/
    
  /*  public void crearStatement()throws SQLException{        
            con = this.conectarBDJNDI("sot");
            st = con.createStatement();
    */
    
    public void execute(String SQL)throws SQLException{
        Connection con = null;
        Statement st   = null;
        try{
            //con = this.conectarBDJNDI("fintra");
            con = this.conectar("default");
            st = con.createStatement();            
            if(st!=null){
                st.addBatch(SQL);
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
        }
        catch(SQLException e){
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
            
        }
        finally{
            if (st  != null){ try{ st.close(); st=null;   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar("default"); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
            
    }
    
    
     
    /* public void cerrarTodo() throws Exception {
        if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        if (con != null && !con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
    }*/
    
    //hosorio 24-Ago-2007 
/*    public void crearStatementJNDI()throws SQLException{        
        
        con = this.conectarBDJNDI("sot");
        st = con.createStatement();
    }
    //hosorio 24-Ago-2007
    public void executeJNDI()throws SQLException{
        try{            
            if(st!=null){
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
        }
        catch(SQLException e){            
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());            
        }        
        
    }
    //hosorio 24-Ago-2007
    public void cerrarTodoJNDI() throws Exception {
        if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        this.desconectar(con); 
    }*/
      /**
     * Getter for property sql.
     * @return Value of property sql.
     */
    public java.lang.String getSql() {
        return sql;
    }    
    
    /**
     * Setter for property sql.
     * @param sql New value of property sql.
     */
    public void setSql(java.lang.String sql) {
        this.sql = sql;
    }   
}
