/*
 * TBLTIEMPOService.java
 *
 * Created on 19 de noviembre de 2004, 09:14 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class TbltiempoService {
    
    private TbltiempoDAO tbltiempo;
    /** Creates a new instance of TBLTIEMPOService */
    public TbltiempoService() {
        
        tbltiempo= new TbltiempoDAO();
    }
    
    public Vector getTiempos()throws SQLException{
        return tbltiempo.getTiempos();
    }
    public void buscaTiemposSal(String base)throws SQLException{
        tbltiempo.searchTblTiemposSal(base);
    }
    public void buscaTiempos(String sj)throws SQLException{
        tbltiempo.searchTblTiempos(sj);
        
    }
    public List getTblTiempos(String sj )throws SQLException{
        
        List tiempos=null;
        
        tbltiempo.searchTblTiempos(sj);
        tiempos = tbltiempo.list();
        
        return tiempos;
        
    }
    
    public List getTblTiemposAll(String sj )throws SQLException{
        
        List tiempos=null;
        
        tbltiempo.searchTblTiemposAll(sj);
        tiempos = tbltiempo.list();
        
        return tiempos;
        
    }
    
    
    public int cantidadTiempos(String sj)throws SQLException{
        
        return tbltiempo.cantidadTiempo(sj) ;
        
    }
     public List getTblTiemposSalida(String base)throws SQLException{
        
        List tiempos=null;
        
        tbltiempo.searchTblTiemposSal(base);
        tiempos = tbltiempo.list();
        
        return tiempos;
        
    }
    
    public int cantidadTiempos()throws SQLException{
        
        return tbltiempo.cantidadTiempo() ;
        
    }
    public void searchTblTiemposAll(String sj )throws SQLException{
        tbltiempo.searchTblTiemposAll(sj);
    }
    public void searchTblTiemposEnt(String standard, String base )throws SQLException{
        tbltiempo.searchTblTiemposEnt(standard, base);
    }
}
