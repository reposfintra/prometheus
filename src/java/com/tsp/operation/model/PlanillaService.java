package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.LogErrores;

public class PlanillaService{
    
    private PlanillaDAO planilla;
    private Informe inf ;
    private InfoPlanilla infoPlanilla;
    private String nomPropie;
    private int total = 0;
    
    PlanillaService(){
        planilla = new PlanillaDAO();
    }
    PlanillaService(String dataBaseName){
        planilla = new PlanillaDAO(dataBaseName);
    }
    
    public Vector getPlasIncompletas(String usuario )throws SQLException{
        
        //List planillas=null;
        
        planilla.searchPlasIncompletas(usuario);
        return planilla.getPlanillasIncompletas();
        
        
    }
    
    public List getPlanillas(String usuario )throws SQLException{
        
        planilla.searchPlanillas(usuario);
        return planilla.getPlanillas();
        
        
    }
    
    public List getPColpapel(String usuario )throws SQLException{
        
        planilla.searchPColpapel(usuario);
        return planilla.getPlanillas();
        
        
    }
    public boolean existPlasIncompletas(String usuario)throws SQLException{
        
        return planilla.existPlasIncompletas(usuario);
    }
    
    public boolean existPlanillas(String usuario)throws SQLException{
        
        return planilla.existPlanillas(usuario);
    }
    public Plaaux getPlaaux()throws SQLException{
        
        return planilla.getPlaaux();
        
    }
    
    public Planilla getPlanilla()throws SQLException{
        
        return planilla.getPla();
        
    }
    public void bucaPlaaux(String pla)throws SQLException{
        try{
            planilla.searchPla(pla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void bucaPlanilla(String pla)throws SQLException{
        try{
            planilla.searchPlanilla(pla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaPlanilla(String pla)throws SQLException{
        try{
            planilla.searchPlanilla2(pla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void buscaFecha(String plaveh)throws SQLException{
        try{
            planilla.buscaFecha(plaveh);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void consultaPlanilla(String placa, String fechaini, String fechafin, String cumplido, String anulada, String nit, String cedcon, String base, String despachador, String ori, String des, String agencia )throws SQLException{
        
        planilla.consultaPlanilla(placa,  fechaini,  fechafin, cumplido,  anulada,  nit, cedcon,  base, despachador, ori, des, agencia );
    }
    public Vector getPlas()throws SQLException{
        
        return planilla.getPlanillasVec();
        
    }
    
    public void bucarColpapel(String pla)throws SQLException{
        try{
            planilla.searchPlanillaC(pla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public int cantPlasIncompletas(String usuario)throws SQLException{
        
        return planilla.cantPlasIncompletas(usuario);
    }
    
    public String getRutas(String numpla)throws SQLException{
        
        return planilla.searchRuta(numpla);
    }
    public String insertPlanilla(Planilla pla, String base)throws SQLException{
        try{
            planilla.setPlanilla(pla);
            return planilla.insertPla(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public String updatePlanilla(Planilla pla)throws SQLException{
        try{
            planilla.setPlanilla(pla);
            return planilla.updatePla();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String anularPlanilla(Planilla pla)throws SQLException{
        try{
            planilla.setPlanilla(pla);
            return planilla.anularPla();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public List buscarRemesas(String numpla)throws SQLException{
        planilla.searchRemesas(numpla);
        return planilla.getRemesas();
    }
    
    public boolean validarCantidad(float can, float cantcum)throws SQLException{
        
        //RETORNA FALSE SI LA CANTIDAD CUMPLIDA ES MAYOR QUE LA CANTIDAD EN PLANILLA
        if(cantcum>can)
            return false;
        
        else
            return true;
    }
    public void searchSaldo(String codpla)throws SQLException{
        
        planilla.searchSaldo(codpla);
        
    }
    
    public void buscarPlanillaAnticipo(String codpla)throws SQLException{
        planilla.searchPlanillaAnticipo(codpla);
    }
    
    public void llenarInforme(String fechai, String fechaf,String cliente,String ruta,String base)throws SQLException{
        planilla.llenarInforme(fechai, fechaf,cliente,ruta,base);
    }
    public Vector getInformes(){
        return planilla.getInformes();
    }
    
    public void llenarClientes(String fechai, String fechaf,String base)throws SQLException{
        planilla.llenarClientes(fechai, fechaf, base);
    }
    public void llenarRutas(String fechai, String fechaf, String cliente,String base)throws SQLException{
        planilla.llenarRutas(fechai, fechaf, cliente,base);
    }
    public Vector getClientes(){
        
        return planilla.getClientes();
    }
    public Vector getRutas(){
        
        return planilla.getRutas();
    }
    public void setInforme(Informe inf){
        
        this.inf = inf;
    }
    public Informe getInforme(){
        return this.inf;
    }
    public int buscarTotalEquipo(String fechai, String fechaf, String cliente, String ruta,String base)throws SQLException{
        return planilla.buscarTotalEquipo(fechai, fechaf, cliente, ruta,base);
    }
    
    public void llenarInformeEgeso(String fechai, String fechaf,String base)throws SQLException{
        planilla.llenarInformeEgeso(fechai, fechaf,base);
    }
    public void llenarInformeProveedores(String fechai, String fechaf, String base)throws SQLException{
        planilla.llenarInformeProveedores(fechai, fechaf, base);
    }
    public Vector getProveedores(){
        
        return planilla.getProveedores();
    }
    
    public boolean estaTagTraffic(String trafico)throws SQLException{
        return planilla.estaTagTraffic(trafico);
        
    }
    public String buscarTrafficCode(String placa)throws SQLException{
        return planilla.buscarTrafficCode(placa);
    }
    public String ActualizarTrafficTag(String base)throws SQLException{
        return planilla.ActualizarTrafficTag(base);
    }
    public void setPlanilla(Planilla p){
        planilla.setPlanilla(p);
    }
    public boolean estaTagTrafficPlaca(String trafic, String placa)throws SQLException{
        return planilla.estaTagTrafficPlaca(trafic, placa);
    }
    
    public void llenarCorte(String base)throws SQLException{
        planilla.llenarCorte(base);
    }
    
    public String buscarFecCorte(String base)throws SQLException{
        return planilla.buscarFecCorte(base);
    }
    public void updateCorte(String corte, String numpla)throws SQLException{
        planilla.updateCorte(corte, numpla);
    }
    
    //INFORMES TIPO PRODECO DE 7AM A 7PM
    public void llenarInforme2(String fechai, String fechaf,String cliente,String ruta, String base)throws SQLException{
        planilla.llenarInforme2(fechai, fechaf, cliente, ruta, base);
    }
    public void llenarClientes2(String fechai, String fechaf, String base)throws SQLException{
        planilla.llenarClientes2(fechai, fechaf, base);
    }
    public void llenarRutas2(String fechai, String fechaf, String cliente, String base)throws SQLException{
        planilla.llenarRutas2(fechai, fechaf, cliente, base);
    }
    public int buscarTotalEquipo2(String fechai, String fechaf, String cliente, String ruta, String base)throws SQLException{
        return planilla.buscarTotalEquipo2(fechai, fechaf, cliente, ruta, base);
    }
    public void llenarInformeEgeso2(String fechai, String fechaf, String base)throws SQLException{
        planilla.llenarInformeEgeso2(fechai, fechaf, base);
    }
    public void llenarInformeProveedores2(String fechai, String fechaf, String base)throws SQLException{
        planilla.llenarInformeProveedores2(fechai, fechaf, base);
    }
    //Codigo agregado por Jesu Cuesta
    public void reporteViajes(String fech1,String fech2 )throws SQLException{
        planilla.reporteViajes(fech1, fech2);
    }
    public String getPlacaPlanilla(String pla) throws SQLException{
        return planilla.getPlacaPlanilla(pla);
    }
    public java.util.Vector getReporte() {
        return planilla.getReporte();
    }
    public void reporteViajesPesos(String fech1,String fech2,String base )throws SQLException{
        planilla.reporteViajesPesos(fech1, fech2, base);
    }
    public void buscarTransitoRemision(String remision, String base)throws SQLException{
        planilla.buscarTransitoRemision(remision, base);
    }
    public void buscarTransitoPlaca(String placa, String base)throws SQLException{
        planilla.buscarTransitoPlaca(placa, base);
        
    }
    public Planilla obtenerPlanillaPorNumero(String numpla) throws SQLException {
        return planilla.obtenerPlanillaPorNumero(numpla);
    }
    public String searchPlaTiempo(String codpla, String codtiempo )throws SQLException{
        return planilla.searchPlaTiempo(codpla, codtiempo);
    }
    public Vector obtenerPlanillas() throws SQLException {
        return planilla.obtenerPlanillas();
    }
    
    public Vector obtenerPlanillas(String desde, String hasta) throws SQLException {
        return planilla.obtenerPlanillas(desde,hasta);
    }
    
    
    //planeacion
    
    public Planilla getPla() {
        return planilla.getPl();
    }
    
    /**
     * Setter for property pl.
     * @param pl New value of property pl.
     */
    public void setPl(Planilla pl) {
        planilla.setPl(pl);
    }
    
    /**
     * Getter for property planillas.
     * @return Value of property planillas.
     */
    public java.util.Vector getPlanillas2() {
        return planilla.getPlanillas2();
    }
    
    /**
     * Setter for property planillas.
     * @param planillas New value of property planillas.
     */
    public void setPlanillas(java.util.Vector planillas) {
        planilla.setPlanillas(planillas);
    }
    public void list(String distrito, String fecha_ini, String fecha_final)throws SQLException{
        planilla.list(distrito,fecha_ini,fecha_final);
    }
    public int cantPlanilla(String codpla )throws SQLException{
        return planilla.cantPlanilla(codpla);
    }
    
    /**
     * @autor Tito Andr�s
     * @param numpla N�mero de la planilla
     */
    public InfoPlanilla obtenerInformacionPlanilla(String numpla) throws SQLException{
        return planilla.obtenerInformacionPlanilla(numpla);
    }
    
    /*public TreeMap tramosPlanilla(String numpla){
        TreeMap tramos = new TreeMap();
     
        try{
            String ruta_pla = this.planilla.obtenerInformacionTramo(numpla);
     
            for(int i=0;i<ruta_pla.length()-1;i+=2){
                if( i<=ruta_pla.length()-4 ){
                    String ruta = ruta_pla;
                    String tramo = ruta.substring(i,i+4);
                    String ciu1 = tramo.substring(0,2);
                    String ciu2 = tramo.substring(2,4);
                    String nom_ciu1 = this.planilla.obtenerNombreCiudad(ciu1);
                    String nom_ciu2 = this.planilla.obtenerNombreCiudad(ciu2);
                    tramos.put(nom_ciu1 + " - " + nom_ciu2, ciu1+ciu2);
                }
            }
        }
        catch(Exception exc){
            tramos.put("No se ha regisrado la ruta de la planilla","NA");
        }
     
        return tramos;
    }*/
    
    /**
     * Obtiene los tramos registrados en la ruta de una planilla.
     * @autor Ing. Tito Andr�s Maturana
     * @see com.tsp.operation.model.PlanillaDAO#obtenerNombreCiudad(String)
     * @see com.tsp.operation.model.PlanillaDAO#obtenerInformacionTramo(String)
     * @param numpla El n�mero de la planilla.
     * @return Un objeto de la clase <code>TreeMap</code> con los tramos de la ruta.
     * @version 1.1
     */
    public Vector tramosPlanilla(String numpla){
        Vector tramos = new Vector();
        
        try{
            InfoPlanilla info = this.obtenerInformacionPlanilla(numpla);
            
            String ruta_pla = this.planilla.obtenerInformacionTramo(numpla);
            Vector tr = new Vector();
            for(int i=0;i<ruta_pla.length()-1;i+=2){
                if( i<=ruta_pla.length()-4 ){
                    String ruta = ruta_pla;
                    String tramo = ruta.substring(i,i+4);
                    String ciu1 = tramo.substring(0,2);
                    String ciu2 = tramo.substring(2,4);
                    String nom_ciu1 = this.planilla.obtenerNombreCiudad(ciu1);
                    String nom_ciu2 = this.planilla.obtenerNombreCiudad(ciu2);
                    
                    tramo = nom_ciu1 + " - " + nom_ciu2 + " - " + (ciu1 + ciu2);
                    tr.add(tramo);
                    //tramos.put(nom_ciu1 + " - " + nom_ciu2, ciu1+ciu2);
                }
            }
            
            Object[] tr_aux = tr.toArray();
            String ultdest = "";
            for( int i=0; i<tr_aux.length; i++){
                String trm = (String) tr_aux[i];
                String[] ciuds = trm.split(" - ");
                
                ////System.out.println("**************** tramo: " + trm);
                
                for( int j=i+1; j<tr_aux.length; j++){
                    
                    String trm_aux = (String) tr_aux[j];
                    String[] caux = trm_aux.split(" - ");
                    
                    //////System.out.println("--------> tramo: " + trm_aux);
                    //////System.out.println("--------> " + i + ": caux[0]: " + caux[0] + ", ori: " + info.getOrigen() + ", igual? " + ( caux[0].compareTo(info.getOrigen())==0 ));
                    //////System.out.println("--------> " + j + ": caux[0]: " + caux[0] + ", ciuds[1]: " + ciuds[1] + ", igual? " + ( caux[0].compareTo(ultdest)==0 ) );
                    if( caux[0].compareTo(info.getOrigen())==0 ){
                        String aux = (String) tr_aux[i];
                        tr_aux[i] = tr_aux[j];
                        tr_aux[j] = aux;
                        //////System.out.println("** entro:" + tr_aux[i] + ", pos: " + i);
                    } else if ( caux[0].compareTo(ultdest)==0 && ciuds[0].compareTo(info.getOrigen())!=0 ){
                        String aux = (String) tr_aux[i];
                        tr_aux[i] = tr_aux[j];
                        tr_aux[j] = aux;
                        //////System.out.println("** entro:" + tr_aux[i] + ", pos: " + i);
                    }
                }
                
                ultdest = ciuds[1];
                
            }
            
            for( int i=0; i<tr_aux.length; i++){
                String trm = (String) tr_aux[i];
                String[] ciuds = trm.split(" - ");
                Vector vec = new Vector();
                vec.add(ciuds[0] + " - " + ciuds[1]);
                vec.add(ciuds[2]);
                tramos.add(vec);
            }
        }
        catch(Exception exc){
            Vector vec = new Vector();
            vec.add("No se ha regisrado la ruta de la planilla");
            vec.add("NA");
            tramos.add(vec);
        }
        
        return tramos;
    }
    
   
    public void listaPlanillas(String numpla )throws SQLException{
        planilla.listaPlanillas(numpla);
    }
    
    /**
     * Metodo: buscarPlanillaxNumero, instancia del metodo correspondiente en el DAO
     * @autor : Ing. Sandra Escalante
     * @see buscarPlanillaxNumero
     * @param : numero de la planilla
     * @version : 1.0
     */
     public void buscarPlanillaxNumero(String numero) throws SQLException{
        planilla.buscarPlanillaxNumero(numero);
    }
   
    public void getInfoPlanilla(String numpla )throws SQLException{
        planilla.getInfoPlanilla(numpla);
    }
    /* Modificacion Jose 18*/
    /**
     * Getter for property infoPlanilla.
     * @return Value of property infoPlanilla.
     */
    public com.tsp.operation.model.beans.InfoPlanilla getInfoPlanilla () {
        return infoPlanilla;
    }
    
    public void confirmacionTrailer(Planilla p) throws SQLException{
        planilla.confirmacionTrailer(p);
    }
    
    //METODO QUE PERMITE GUARDAR LAS PLANILLAS DESPACHADAS EN LAS TABLAS Ingreso trafico y trafico
    public String insertTrafico() throws SQLException{
        return planilla.insertTrafico();
    }
    //METODO QUE PERMITE ACTUALIZAR LAS TABLAS DE INGRESO_TRAFICO Y TRAFICO EN EL MOMENTO EN QUE SE
    //MODIFICA UNA PLANILLA.
    public void updateTrafico(String numpla) throws SQLException{
        planilla.updateTrafico(numpla);
    }
    //METODO QUE PERMITE ACTUALIZAR LAS TABLAS DE INGRESO_TRAFICO Y TRAFICO EN EL MOMENTO EN QUE SE
    //ANULA UNA PLANILLA.
    public void anulaTrafico(String numpla) throws SQLException{
        planilla.anulaTrafico(numpla);
    }
    
    /**
     * Obtiene el nombre dle propietario de una planilla dada.
     * @autor Tito Andr�s 27.09.2005
     * @param numpla N�mero de la planilla
     * @throws SQLException
     */
    public String obtenerNombrePropietario(String numpla )throws SQLException{
        return planilla.obtenerNombrePropietario(numpla);
    }    
 
    public void consultaPlanillaUsuario(String usuario, String placa, String fecini, String fecfin, String oripla, String despla, String agenpla, String conductor) throws SQLException{
        try{
            planilla.consultaPlanillaUsuario(usuario, placa, fecini, fecfin, oripla, despla, agenpla, conductor);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public Vector ReporteVector() throws Exception{
        
        return planilla.getReporte();
        
    }
    public void setPlaaux(Plaaux p){
        
        planilla.setPlaaux(p);
    }
    
    //Jose 111005
    public List reportePlanillasXLS() throws SQLException{
        try{
            return planilla.reportePlanillasXLS();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public List getListPlanillas()throws SQLException{
        return planilla.getPlanillas();
        
    }
    
    //JOSE 211005
    //Jose
    public void consultaPlanillaRemesa(String plan) throws SQLException{
        try{
            planilla.consultaPlanillaRemesa(plan);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void consultaPlanillaRemesaDiscrepancia(String planilla, String remesa, String tipo_doc, String documento, int num_discre )throws SQLException{
        try{
            this.planilla.consultaPlanillaRemesaDiscrepancia(planilla, remesa, tipo_doc, documento, num_discre);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
   //Tito Andres 31.10.2005
    /**
     * Genera el informe de facturaci�n para Carb�n.
     * @autor Ing. Tito Andr�s Maturana
     * @param fechai Fecha inicial del informe
     * @param fechaf Fecha final del infome
     * @param ruta Ruta a la que pertenece el Est�ndar
     * @throws SQLException si ocurre un error en la conexi�n con la Base de Datos
     * @version 1.1
     */
    public Vector obtenerInformeFacturacion(String fechai, String fechaf, String ruta) throws SQLException{
        
        Vector informe = new Vector();
        
        if( ruta.matches("127%") ){
            informe =  this.planilla.obtenerInformeFacturacion(fechai + " 7:00:00.0", com.tsp.util.Util.fechaFinal(fechaf + " 6:59:00", 1), "%");            
        } else {
            informe =  this.planilla.obtenerInformeFacturacion(fechai + " 7:00:00.0", com.tsp.util.Util.fechaFinal(fechaf + " 6:59:00", 1), ruta + "%");
        }
        
        informe = this.organizarInformeFacturacionxDia(informe);
        
        return informe;
    }
    public Vector organizarInformeFacturacionxDia(Vector informe) throws SQLException{
        Vector informe_2 = new Vector();
        
        for( int i= 0; i< informe.size(); i++){
            InformeFact info = (InformeFact) informe.elementAt(i);
            Vector linea = info.getRemisiones();
            
            String diafin = "";
            if( linea.size()>0 ){
                RemisionIFact aux = new RemisionIFact();
                aux = (RemisionIFact) linea.elementAt(0);
                String[] fecha = aux.getFecha().split(" ");
                String fecha_rem = aux.getFecha();
                diafin = com.tsp.util.Util.fechaFinal(fecha[0] + " 6:59:00", 1);
            }
            
            //////System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ruta: " + info.getStd_job());
            
            double tonelaje_dia = 0;
            Vector remisiones = new Vector();
            Vector obj = new Vector();
            Vector nlinea = new Vector();
            
            for( int j=0; j<linea.size(); j++){
                RemisionIFact remision = new RemisionIFact();
                remision = (RemisionIFact) linea.elementAt(j);
                
                String[] fecha = remision.getFecha().split(" ");
                String fecha_rem = remision.getFecha();
                //////System.out.println("���������������������������������������������> remision: " + remision.getRemision() + ", fecha:" + remision.getFecha());
                //
                
                boolean cond = fecha_rem.compareTo(diafin)<=0;
                if( !cond ){
                    obj.add(remisiones);
                    obj.add(String.valueOf(tonelaje_dia));
                    //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> remisiones: " + remisiones.size());
                    //////System.out.println("������������������������������������> tonelaje dia: " + tonelaje_dia);
                    nlinea.add(obj);
                    obj = new Vector();
                    remisiones = new Vector();
                    tonelaje_dia = 0;
                    diafin = com.tsp.util.Util.fechaFinal(fecha[0] + " 6:59:00", 1);
                    remisiones.add(remision);
                    tonelaje_dia += remision.getTonelaje().doubleValue();
                    //////System.out.println("*******************> fecha_rem: " + remision.getFecha());
                    
                    if( j==(linea.size() - 1)){
                        obj.add(remisiones);
                        obj.add(String.valueOf(tonelaje_dia));
                        //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> remisiones: " + remisiones.size());
                        nlinea.add(obj);
                        //////System.out.println("***************>>>>>>>>>>> fecha_rem: " + remision.getFecha());
                    }
                    
                }
                else if( j==(linea.size() - 1)){
                    remisiones.add(remision);
                    tonelaje_dia += remision.getTonelaje().doubleValue();
                    obj.add(remisiones);
                    obj.add(String.valueOf(tonelaje_dia));
                    //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> remisiones: " + remisiones.size());
                    nlinea.add(obj);
                    //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> fecha_rem: " + remision.getFecha());
                }
                else{
                    remisiones.add(remision);
                    tonelaje_dia += remision.getTonelaje().doubleValue();
                    //////System.out.println("-------------------> fecha_rem: " + remision.getFecha());
                }
                //////System.out.println("����������������������������������������������������> fecha_rem:" + fecha_rem + ", dia_fin:" + diafin + ", cond" + cond);
            }
            
            info.setRemisiones(nlinea);
            informe_2.add(info);
        }
        
        return informe_2;
    }
    
    public Vector organizarInformeFacturacionxDia_127012(Vector informe) throws SQLException{
        Vector informe_2 = new Vector();
        
        for( int i= 0; i< informe.size(); i++){
            InformeFact info = (InformeFact) informe.elementAt(i);
            Vector linea = info.getRemisiones();
            
            String diafin = "";
            if( linea.size()>0 ){
                RemisionIFact aux = new RemisionIFact();
                aux = (RemisionIFact) linea.elementAt(0);
                String[] fecha = aux.getFecha().split(" ");
                String fecha_rem = aux.getFecha();
                diafin = fecha[0] + " 23:59:59";
            }
            
            //////System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ruta: " + info.getStd_job());
            
            double tonelaje_dia = 0;
            Vector remisiones = new Vector();
            Vector obj = new Vector();
            Vector nlinea = new Vector();
            
            for( int j=0; j<linea.size(); j++){
                RemisionIFact remision = new RemisionIFact();
                remision = (RemisionIFact) linea.elementAt(j);
                
                String[] fecha = remision.getFecha().split(" ");
                String fecha_rem = remision.getFecha();
                //////System.out.println("���������������������������������������������> remision: " + remision.getRemision() + ", fecha:" + remision.getFecha());
                //
                
                boolean cond = fecha_rem.compareTo(diafin)<=0;
                if( !cond ){
                    obj.add(remisiones);
                    obj.add(String.valueOf(tonelaje_dia));
                    //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> remisiones: " + remisiones.size());
                    //////System.out.println("������������������������������������> tonelaje dia: " + tonelaje_dia);
                    nlinea.add(obj);
                    obj = new Vector();
                    remisiones = new Vector();
                    tonelaje_dia = 0;
                    diafin = fecha[0] + " 23:59:59";
                    remisiones.add(remision);
                    tonelaje_dia += remision.getTonelaje().doubleValue();
                    //////System.out.println("*******************> fecha_rem: " + remision.getFecha());
                    
                    if( j==(linea.size() - 1)){
                        obj.add(remisiones);
                        obj.add(String.valueOf(tonelaje_dia));
                        //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> remisiones: " + remisiones.size());
                        nlinea.add(obj);
                        //////System.out.println("***************>>>>>>>>>>> fecha_rem: " + remision.getFecha());
                    }
                    
                }
                else if( j==(linea.size() - 1)){
                    remisiones.add(remision);
                    tonelaje_dia += remision.getTonelaje().doubleValue();
                    obj.add(remisiones);
                    obj.add(String.valueOf(tonelaje_dia));
                    //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> remisiones: " + remisiones.size());
                    nlinea.add(obj);
                    //////System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> fecha_rem: " + remision.getFecha());
                }
                else{
                    remisiones.add(remision);
                    tonelaje_dia += remision.getTonelaje().doubleValue();
                    //////System.out.println("-------------------> fecha_rem: " + remision.getFecha());
                }
                //////System.out.println("����������������������������������������������������> fecha_rem:" + fecha_rem + ", dia_fin:" + diafin + ", cond" + cond);
            }
            
            info.setRemisiones(nlinea);
            informe_2.add(info);
        }
        
        return informe_2;
    }
    
    //Tito Andr�s Maturana 04.11.2005
    /**
     * Genera el informe de anticipos
     * @autor Tito Andr�s Maturana
     * @param fechai Fecha inicial del reporte.
     * @param fechaf Fecha final del reporte.
     * @throws SQLException
     * @version 1.0
     */
    public Vector generarInformeAnticipos(String fechai, String fechaf)
    throws SQLException{
        
        return this.planilla.obtenerInformeAnticipos(fechai, fechaf);
    }
    
    /**
     * Metodo: reporteProdFlotaXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @see reporteProdFlotaPlacasXLS - planillaDAO
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasXLS(String fini, String ffin) throws SQLException{
        try{
            return planilla.reporteProdFlotaPlacasXLS(fini,ffin);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: reporteProdFlotaXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y un nit del proveedor
     * @see reporteProdFlotaPlacasDiasXLS - planillaDAO
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasDiasXLS(String fini, String ffin, String placa,String nit) throws SQLException{
        try{
            return planilla.reporteProdFlotaPlacasDiasXLS(fini,ffin,placa,nit);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: reporteProdStandarJobXLS, permite listar los detalles de los standar job
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @see reporteProdStandarJobXLS - planillaDAO
     * @version : 1.0
     */
    public List reporteProdStandarJobXLS() throws SQLException{
        try{
            return planilla.reporteProdStandarJobXLS();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    /**
     * Metodo: nombrePlacaTrayler, retorna el trayler de la placa dada
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y un nit del proveedor
     * @version : 1.0
     */
    public String nombrePlacaTrayler(String fini, String ffin, String placa, String nit) throws SQLException{
        try{
            return planilla.nombrePlacaTrayler(fini,ffin,placa,nit);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: reporteLufkinXLS, permite listar un reporte de produccion de flota por trayler
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @see reporteProdStandarJobXLS - planillaDAO
     * @version : 1.0
     */
    public List reporteLufkinXLS(String fini, String ffin) throws SQLException{
        try{
            return planilla.reporteLufkinXLS(fini,ffin);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: reporteProdFlotaPlacasStandarXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @see reporteProdStandarJobXLS - planillaDAO
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasStandarXLS(String fini, String ffin, String standar) throws SQLException{
        try{
            return planilla.reporteProdFlotaPlacasStandarXLS(fini,ffin,standar);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
     /* Metodo: reporteFlotaDirectaXLS, permite listar un reporte de produccion de flota directa
      * @autor : Ing. Jose de la rosa
      * @param : fecha inicial y fecha final
      * @version : 1.0
      */
    public List reporteFlotaDirectaXLS(String fini, String ffin) throws SQLException{
        try{
            return planilla.reporteFlotaDirectaXLS(fini,ffin);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: reporteFlotaIntermediaXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public List reporteFlotaIntermediaXLS(String fini, String ffin) throws SQLException{
        try{
            return planilla.reporteFlotaIntermediaXLS(fini,ffin);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Genera el resumen del informe de anticipos
     * @autor Tito Andr�s Maturana
     * @param fechai Fecha inicial del reporte.
     * @param fechaf Fecha final del reporte.
     * @throws SQLException
     * @version 1.0
     */
    public Vector obtenerResumenInformeAnticipos(String fechai, String fechaf) throws SQLException{
        return this.planilla.obtenerResumenInformeAnticipos(fechai, fechaf);
    }
    
    /*JEscandon 13.12.05*/
    /**
     * Genera Lista de recursos disponibles con ENTREGA ENTRE HOY  Y HOY - 2 DIAS
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @see list1 - PlanillaDAO
     * @throws SQLException
     * @version 1.0
     */
    public void list1(String distrito, String fecha_ini, String fecha_final)throws SQLException{
        planilla.list1(distrito,fecha_ini,fecha_final);
    }
    
    /**
     * Genera Lista de recursos disponibles que TIENE CUALQUIER MOVIMIENTO ENTRE HOY Y HACE 2 DIAS
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @see list2 - PlanillaDAO
     * @throws SQLException
     * @version 1.0
     */
    public void list2(String distrito, String fecha_ini, String fecha_final)throws SQLException{
        planilla.list2(distrito,fecha_ini,fecha_final);
    }
    
    /**
     * Genera Lista de recursos disponibles que NO TIENE MOVIMIENTO Y LA FECHA DE POSLLEGADA ESTA ENTRE HOY Y HACE 2 DIAS
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @see list3 - PlanillaDAO
     * @throws SQLException
     * @version 1.0
     */
    public void list3(String distrito, String fecha_ini, String fecha_final)throws SQLException{
        planilla.list3(distrito,fecha_ini,fecha_final);
    }
    //Jose 15.12.05
    /* Metodo: obtenerDespacho, permite listar los campos asociados al trailer
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicio y fecha fin
     * @see obtenerCamposTrailer - planillaDAO
     * @version : 1.0
     */
    public List obtenerDespacho(String fechini, String fechfin) throws SQLException{
        try{
            return planilla.obtenerDespacho(fechini,fechfin);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo: tipoViaje, permite obtener dada una planilla si el tipo de
     *                       viaje es nacional o internacional
     * @autor : Ing. Jose de la rosa
     * @param : numero planilla.
     * @see obtenerCamposTrailer - planillaDAO
     * @version : 1.0
     */
    public String tipoViaje(String numpla) throws SQLException{
        try{
            return planilla.tipoViaje(numpla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    } /*Fin*/
        
    /**
     * Metodo: obtenerValorStandarPlanilla, permite obtener unos valores de la planilla
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial y fecha final
     * @version : 1.0
     */
    public Planilla obtenerValorStandarPlanilla(String numpla) throws SQLException {
        try{
            return planilla.obtenerValorStandarPlanilla(numpla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    //David 23.12.05
    /**
     * Este m�todo genera un boolean true si existe el numero de de planilla y false si no existe
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params : String numpla
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public boolean existPlanilla(String numpla)throws SQLException{
        return planilla.existPlanilla(numpla);
    }
    /**
     * Metodo que devuelve un string con un comando sql de update
     * para actualizar los datos de trafico e ingrso trafico en el campo cliente
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    
    public void actualizarTrafico() throws SQLException{
        planilla.actualizarTrafico();
        
    }
    /**
     * Metodo que devuelve un string con los nombres de los clientes dada una planilla
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public String buscarClientes(String numpla) throws SQLException{
        return planilla.buscarClientes(numpla);
    }
    /**
     * Metodo que devuelve la conversion de un valor de una moneda 1 a una moneda2 buscando
     *en la tabla de tasas.
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public double buscarValor(String moneda1, String moneda2, String fecha, float valor ) throws SQLException {
        return planilla.buscarValor(moneda1, moneda2, fecha, valor);
    }
    
    //JEscandon 26.01.06
    /**
     * Este m�todo genera un boolean true si existe una planilla, teneniendo en cuenta la Base = COL o false de lo contrario
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params : String numpla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public boolean existPlanillaBaseCol(String numpla)throws SQLException{
        return planilla.existPlanillaBaseCol(numpla);
    }
     
   /**
     * Genera la informaci�n para el Reporte de Extrafletes y Costos Reembolsables
     * @autor Ing. Tito Andr�s Maturana D.
     * @param fechai Fecha inicial del per�odo
     * @param fechai Fecha final del per�odo
     * @param codcli C�digo del cliente
     * @param tipo Indica si es extraflete o costo reembolsable
     * @param doc Tipo de documento
     * @throws SQLException
     * @version 1.0.
     **/
    public void reporteExtrafletes(String fechai, String fechaf, String codcli, String tipo, String doc) throws SQLException{
        planilla.reporteExtrafletes(fechai, fechaf, codcli, tipo, doc);
    }
     /**
     * Funcion que retorna la cantidad de planillas realizadas a una placa desde una
     * fecha dada hasta la fecha actual.
     * @autor Ing. Karen Reales
     * @param fechai Fecha inicial del per�odo
     ** @param placa Placa del vehiculo
     * @throws SQLException
     * @version 1.0.
     **/
    public int cantPlanillasPlaca(String fechai,String placa) throws SQLException{
        return planilla.cantPlanillasPlaca(fechai,placa);
    }
    
     /**
     * Metodo: buscarPlanillasinDocumetos, Metodo que busca las planillas creadas por un usuario
     *         que no tienen documentos completos. No teniene en cuenta las de carbon
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : usuario
     * @version : 1.0
     */
    public void buscarPlanillasinDocumentos(String usuario, String fecha1, String fecha2) throws SQLException{
        planilla.buscarPlanillasinDocumentos(usuario,fecha1,fecha2);
    }

    /**
     * Getter for property plasindoc.
     * @return Value of property plasindoc.
     */
    
    public java.util.Vector getPlasindoc() {
        return planilla.getPlasindoc();
    }
    //Modificacion Jose 10-02-200
    public void obtenerNombrePropietario2 (String numpla )throws SQLException{
        nomPropie=planilla.obtenerNombrePropietario (numpla);
    }
    //Modificacion Jose 10-02-200
    public void obtenerInformacionPlanilla2 (String numpla) throws SQLException{        
        infoPlanilla=planilla.obtenerInformacionPlanilla (numpla);
    }
     /**
     * Getter for property nomPropie.
     * @return Value of property nomPropie.
     */
    public java.lang.String getNomPropie () {
        return nomPropie;
    }
    /*JEscandon 13.12.05*/
     /**
     * Genera Lista de recursos disponibles
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @see list1 - PlanillaDAO
     * @throws SQLException
     * @version 1.0
     */
    public void listRecursosDisp(String distrito, String fecha_ini, String fecha_final)throws SQLException{
        planilla.ListaRecursosDisp(distrito,fecha_ini,fecha_final);
    }    
    /******* PlanillaService **********/
    /**
     * Metodo: reporteViajesXLS, permite listar los viajes del reporte de planeaci�n
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y el estandar
     * @version : 1.0
     */
    public List reporteViajesXLS (String fini, String ffin, String estandar) throws SQLException{
        try{
            return planilla.reporteViajesXLS (fini, ffin, estandar);
        }
        catch(SQLException e){
            throw new SQLException (e.getMessage ());
        }
    }
    /**
     * Getter for property planillas.
     * @return Value of property planillas.
     */
    public java.util.Vector getPlanillasVehExternos() {
        return planilla.getPlanillas3();
    }
    
    
     /*JEscandon 07-03-06*/
     /**
     * Genera Lista de veh - externos que estan disponibles para un periodo determinado
     * @autor Ing. Juan Manuel Escandon Perez
     * @param Distrito, fecha inicial, fecha final
     * @see ListaVehExternos - PlanillaDAO
     * @throws SQLException
     * @version 1.0
     */
    public void ListaVehExternos(String distrito, String fecha_ini, String fecha_final)throws SQLException{
        planilla.ListaVehExternos(distrito,fecha_ini,fecha_final);
    }   
       /**
     *Metodo: buscarTotalEquipo2, Metodo que retorna la cantidad total de
     *         equipos despachados de carbon en unas fechas dadas 
     *@autor : Ing. Karen Reales
     *@param : fecha inicio , fecha fin, base
     *@return: Numero de viajes
     *@version : 1.0
     */
    public int buscarTotalEquipo2(String fechai, String fechaf, String base)throws SQLException{
        return planilla.buscarTotalEquipo2(fechai,fechaf,base);
    }
     public void setPlanillasVec(Vector plas){
        planilla.setPlanillasVec(plas);
    }
        /**
     *Metodo: estaEnTrafico, Metodo que retorna true o false si la placa
     * que se esta registrando ya esta en transito.
     *@autor : Ing. Karen Reales
     *@param : placa
     *@return: True o False
     *@version : 1.0
     */
    public boolean estaEnTrafico(String placa)throws SQLException{
        return planilla.estaEnTrafico(placa);
    }
     /**
     * Setter for property informes.
     */
    public void setInformes(Vector informes){
        planilla.setInformes(informes);
    }
    /** fecha: 2006-05-19
     * Metodo: reporteProdFlotaPlacasDiasSTDXLS, permite listar un reporte de produccion de flota
     * @autor : Ing. Jose de la rosa
     * @param : fecha inicial, fecha final, placa y un nit del proveedor
     * @see reporteProdFlotaPlacasDiasXLS - planillaDAO
     * @version : 1.0
     */
    public List reporteProdFlotaPlacasDiasSTDXLS(String fini, String ffin, String placa,String nit, String standar) throws SQLException{
        try{
            return planilla.reporteProdFlotaPlacasDiasSTDXLS(fini,ffin,placa,nit,standar);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
       /**
     * Este m�todo genera un boolean true si la planilla tiene numero de factura
     * @throws SQLException si un error de acceso a la base de datos ocurre.
     * @params : String numpla
     * @autor : Ing. Karen Reales
     * @version : 1.0
     */
    public boolean tieneFactura(String codpla )throws SQLException{
        return planilla.tieneFactura(codpla);
    }
     /**fecha:  12/06/2006
     * Metodo: getPlanillasSinPlanviaje, obtiene las planillas que no tiene plan de viaje
     *         que pertenecen a la agencia dada, retorna las planillas con fecha diez dias
     *         antes de la fecha actual
     * @autor : Osvaldo P�rez Ferrer
     * @param : codigo de la agencia
     * @version : 1.0
     */
     public Vector getPlanillasSinPlanviaje(String agencia) throws SQLException{
         try{
           return planilla.getPlanillasSinPlanviaje(agencia);  
         }
         catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
     }
     public List obtenerCamposTrailer(String trailer, String numpla) throws SQLException{
        try{
            return this.planilla.obtenerCamposTrailer(trailer, numpla);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
     /**
     *Metodo que permite obtener una remesa dado el numero de la remesa
     *@autor: David Pi�a
     *@param numpla N�mero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getPlanillaIC( String numpla ) throws SQLException {
        planilla.getPlanillaIC( numpla );
    }
      /**
     * M�todo que obtiene el reporte de las planillas vacias
     * @autor.......Jose De La Rosa
     * @throws......SQLException
     * @version.....1.0.
     **/
    public Vector planillasVacias ( String fecini, String fecfin ) throws SQLException{
        return planilla.planillasVacias ( fecini, fecfin );
    }
       /**
     *Metodo que anula un registro en ingreso_trafico
     *@autor: Jose de la rosa
     *@param: Numero de la planilla
     *@return: Comando SQL
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String eliminarTrafico (String numpla) throws SQLException{
        return this.planilla.eliminarTrafico(numpla);
    }   
        /**
     *Metodo que permite obtener el plan de viaje de una planilla
     *@autor: David Pi�a
     *@param fechainicio La fecha inicio de la planilla
     *@param fechafin La fecha fin de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getPlanDeViaje( String fechainicio, String fechafin ) throws SQLException {
        planilla.getPlanDeViaje( fechainicio, fechafin );
    }
    /**
     *Metodo que permite obtener la hoja de reporte de una planilla
     *@autor: David Pi�a
     *@param fechainicio La fecha inicio de la planilla
     *@param fechafin La fecha fin de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getHojadeReporte( String fechainicio, String fechafin ) throws SQLException {
        planilla.getHojadeReporte( fechainicio, fechafin );
    }
    
        public Vector getResumenPro(){
        return planilla.getResumenPro();
    }

    public void generarResumenPro( String fechai, String fechaf, String ruta ) throws SQLException {
        planilla.generarResumenPro(fechai + " 7:00:00.0", com.tsp.util.Util.fechaFinal(fechaf + " 6:59:00", 1), ruta);
    }
    
    public void generarReporteDiario( String distrito, String desde, String hasta, String plaveh, String cedcon, String despachador, String agencias, String cliente, String usuario ) throws SQLException {
        planilla.generarReporteDiario( distrito, desde, hasta, plaveh, cedcon, despachador, agencias, cliente, usuario );
    }
    
         /**
     * .
     * @autor Jose De La Rosa 30.09.2005
     * @throws SQLException
     *Moddificado por Luigi el 2002-02-28
     */
    public void obtenerPLacaConductor(String placa, String fecini, String fecfin, String oripla, String despla, String agenpla, String conductor) throws SQLException {
        try{
            planilla.obtenerPLacaConductor(placa, fecini, fecfin, oripla, despla, agenpla, conductor);
            this.total = planilla.getTotal();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Getter for property total.
     * @return Value of property total.
     */
    public int getTotal() {
        return total;
    }    
    
    /**
     * Setter for property total.
     * @param total New value of property total.
     */
    public void setTotal(int total) {
        this.total = total;
    }
    public List buscarRemesas(String numpla, String numrem)throws SQLException{
        planilla.searchRemesas(numpla,numrem);
         return planilla.getRemesas();
    }
    
    /**
     * Metodo para extraer los datos de distribucion de la planilla en 
     * base a lo porcentajes de la remesa
     * @autor mfontalvo
     * @param planilla, numero de la planilla
     * @throws Exception 
     * @return Vector, remesas relacionadas
     */
     public Vector obtenerDatosRPL(String numpla )throws Exception{
         return planilla.obtenerDatosRPL(numpla);
     }


}