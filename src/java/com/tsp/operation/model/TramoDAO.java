/*
 * Nombre        TramoDAO.java
 * Autor         Ing. Jesus Cuestas
 * Modificado    Ing Sandra Escalante
 * Fecha         30 de junio de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


public class TramoDAO{
    
    private Tramo tramo;
    Tramo tr;
    Vector tramos;
    boolean flag;
    //trafico
    TreeMap ltramos;
    
    private static String SQL_TRAMO =    "   SELECT  MAX(tiempo) AS tiempo,  " +
    "                                               origen, " +
    "                                               destino " +
    "                                       FROM    via " +
    "                                       WHERE   origen =? " +
    "                                               AND destino = ?  " +
    "                                       GROUP BY origen, destino";
    
    //TMaturana, modificado x sescalante 01.02.06
    private static String SQL_ORIGENES = "  SELECT  origin, " +
    "                                               get_nombreciudad(origin) AS nombre " +
    "                                       FROM    tramo";
    
    private static String SQL_DESTINOS ="   SELECT  destination, " +
    "                                               get_nombreciudad(destination) " +
    "                                       FROM    tramo " +
    "                                       WHERE   origin = ?";
    
    private static String SQL_BUSCAR = "   SELECT  t.*," +
    "                                               a.nomciu AS origen, " +
    "                                               b.nomciu as destino " +
    "                                       FROM    tramo t, " +
    "                                               ciudad a, " +
    "                                               ciudad b " +
    "                                       WHERE   t.origin = ? " +
    "                                               AND t.destination = ?" +
    "                                               AND a.codciu = t.origin " +
    "                                               AND b.codciu = t.destination";
    
    private static String SQL_MODIFICAR = " UPDATE  tramo " +
    "                                       SET     ticket_id1=?," +
    "                                               ticket_id2=?," +
    "                                               ticket_id3=?," +
    "                                               ticket_id4=?," +
    "                                               ticket_id5=?," +
    "                                               ticket_id6=?," +
    "                                               cantidad_maximo_ticket_1 = ?, " +
    "                                               cantidad_maximo_ticket_2 = ?," +
    "                                               cantidad_maximo_ticket_3 = ?," +
    "                                               cantidad_maximo_ticket_4 = ?,   " +
    "                                               cantidad_maximo_ticket_5 = ?," +
    "                                               cantidad_maximo_ticket_6 = ?," +
    "                                               acpm = ? " +
    "                                       WHERE   origin = ? " +
    "                                               AND destination = ?";
    
    private static String SQL_INSERTAR = "   INSERT INTO tramo ( reg_status,     dstrct,             origin, " +
    "                                               destination,    acpm,               unidad_consumo, " +
    "                                               km,             unidad_distancia,   tiempo, "+
    "                                               unidad_duracion,last_update,        user_update, " +
    "                                               creation_date,  creation_user,      paiso," +
    "                                               paisd,          base    ) " +
    "                           VALUES  (   '',?,?,?,?,?,?,?,?,?, 'now()',?, 'now()',?,?,?,?    )";
    
    private static String SQL_ANULAR = "    UPDATE  tramo " +
    "                                           SET     reg_status='A', " +
    "                                                   last_update = 'now()', " +
    "                                                   user_update = ? " +
    "                                           WHERE   dstrct = ? " +
    "                                                   AND origin = ? " +
    "                                                   AND destination = ?";
    
    private  static String SQL_LISTAR = "SELECT t.*,   " +
    "                                           po.country_name  AS po,  " +
    "                                           pd.country_name AS pd, " +
    "                           		get_nombreciudad( t.destination ) AS nomdest,  " +
    "                                           get_nombreciudad( t.origin ) AS nomorigen,   " +
    "                                           u.descripcion AS uconsumo,  " +
    "                                   	ud.descripcion AS udistancia,  " +
    "                                   	ut.descripcion AS uduracion " +
    "                                    FROM   tramo t " +
    "                                                   LEFT JOIN   pais pd     ON ( t.paisd = pd.country_code )                  " +
    "                      				LEFT JOIN   pais po     ON ( t.paiso = po.country_code ) " +
    "                   				LEFT JOIN   tablagen u    ON ( u.table_code = t.unidad_consumo and u.table_type = 'TUNIDAD' )  " +
    "                   				LEFT JOIN   tablagen ud   ON ( ud.table_code = t.unidad_distancia and u.table_type = 'TUNIDAD' )  " +
    "                   				LEFT JOIN   tablagen ut   ON ( ut.table_code = t.unidad_duracion  and u.table_type = 'TUNIDAD' )  " +
    "                                   WHERE   t.reg_status != 'A' " +
    "                                   ORDER BY  t.origin";
    
    //Sandra 15-03-2006
    private static String SQL_CONSULTA = "  SELECT  t.*," +
    "                                               cd.nomciu AS nomdest,   " +
    "                                               co.nomciu AS nomorigen,    " +
    "                                               po.country_name AS npaiso,   " +
    "                                               pd.country_name AS npaisd,  " +
    "                                               u.descripcion AS uconsumo,  " +
    "                                               ud.descripcion AS udistancia,                                                   " +
    "                                               ut.descripcion AS uduracion  " +
    "                                           FROM    tramo t  " +
    "                                                           LEFT JOIN  pais pd ON   (t.paisd = pd.country_code)                   " +
    "                                                           LEFT JOIN  pais po ON   (t.paiso = po.country_code)  " +
    "                                                           LEFT JOIN  ciudad cd ON (cd.codciu = t.destination)                   " +
    "                                                           LEFT JOIN  ciudad co ON (co.codciu = t.origin)    " +
    "                   				LEFT JOIN   tablagen u    ON ( u.table_code = t.unidad_consumo and u.table_type = 'TUNIDAD' )  " +
    "                   				LEFT JOIN   tablagen ud   ON ( ud.table_code = t.unidad_distancia and u.table_type = 'TUNIDAD' )  " +
    "                   				LEFT JOIN   tablagen ut   ON ( ut.table_code = t.unidad_duracion  and u.table_type = 'TUNIDAD' )  " +
    "                                           WHERE   t.dstrct like ?  " +
    "                                                   AND ( pd.country_code LIKE ? OR pd.country_name LIKE ? )" +
    "                                                   AND ( po.country_code LIKE ? OR po.country_name LIKE ? )	" +
    "                                                   AND ( co.codciu LIKE ? OR co.nomciu LIKE ? )" +
    "                                                   AND ( cd.codciu LIKE ? OR cd.nomciu LIKE ? )" +
    "                                                   AND t.acpm like ?  " +
    "                                                   AND t.km like ?  " +
    "                                                   AND t.tiempo like ?  " +
    "                                                   AND t.reg_status != 'A'  " +
    "                                           ORDER BY t.origin";   
    
    private static String SQL_BUSCAR2 = "   SELECT  t.*,      " +
    "                                               get_nombreciudad( t.destination ) AS ndestino,  " +
    "                                               get_nombreciudad( t.origin ) AS norigen,   " +
    "                                               po.country_name  AS npaiso,  " +
    "                                               pd.country_name  AS npaisd " +
    "                                       FROM    tramo t" +
    "                                                       LEFT JOIN  pais pd ON   (t.paisd = pd.country_code) " +
    "                                                       LEFT JOIN  pais po ON   (t.paiso = po.country_code) " +
    "                                       WHERE   t.reg_status != 'A'  " +
    "                                               AND t.dstrct = ?  " +
    "                                               AND t.origin = ?  " +
    "                                               AND t.destination = ?";
    
    private static String SQL_MODIFICAR2 = "UPDATE  tramo " +
    "                                       SET     acpm = ?, " +
    "                                               unidad_consumo = ?, " +
    "                                               km= ?, " +
    "                                               unidad_distancia = ?, " +
    "                                               tiempo = ?, " +
    "                                               unidad_duracion = ?, " +
    "                                               last_update = 'now()', " +
    "                                               user_update = ?, " +
    "                                               reg_status='' " +
    "                                       WHERE   dstrct = ? " +
    "                                               AND origin = ? " +
    "                                               AND destination = ?";
    
    private static String SQL_VERIFICAR = " SELECT  origin, " +
    "                                               destination " +
    "                                       FROM    tramo " +
    "                                       WHERE   dstrct = ? " +
    "                                               AND origin = ? " +
    "                                               AND destination = ? " +
    "                                               AND reg_status != 'A' ";
    
    private static String SQL_LISTAR2 = "SELECT get_nombreciudad( t.destination ) AS ndestino,  " +
    "                                           get_nombreciudad( t.origin ) AS norigen,   " +   
    "                                           t.origin," +
    "                                           t.destination" +
    "                                    FROM   tramo t " +
    "                                    WHERE  t.dstrct = ?" +
    "                                           AND t.reg_status != 'A'" +
    "                                    ORDER BY norigen";
    
    private static String SQL_LISTAR3 = "SELECT get_nombreciudad( t.destination ) AS ndestino,  " +
    "                                           get_nombreciudad( t.origin ) AS norigen,   " +   
    "                                           t.origin," +
    "                                           t.destination" +
    "                                    FROM   tramo t " +
    "                                    WHERE  t.origin = ?" +
    "                                           AND t.dstrct =?" +
    "                                           AND t.reg_status != 'A'" +
    "                                   ORDER BY norigen";
    
    private static String SQL_VERIFTANUL = " SELECT origin, " +
    "                                               destination " +
    "                                       FROM    tramo " +
    "                                       WHERE   " +
    "                                               dstrct = ? " +
    "                                               AND origin = ? " +
    "                                               AND destination = ? " +
    "                                               AND reg_status = 'A' ";
    
    private static String SQL_UPDATEEXT = 
        "UPDATE tramo SET " +
        "   origin = ?, " +
        "   destination = ? " +
        "WHERE " +
        "   origin = ? " +
        "   AND destination = ? ";
    
    /** Creates a new instance of TramoDAO */
    TramoDAO(){       
    }
    
    /**
     * Getter for property tramo.
     * @return Value of property tramo.
     */
    public Tramo getTramo(){
        return tramo;
    }
    
    /**
     * Setter for property tramo.
     * @param Ubicaciones New value of property tramo.
     */
    public void setTramo(Tramo t){
        tramo = t;
    }
    
    /**
     * Metodo <tt>searchTramo</tt>, obtiene un tramo dado origen y destino     
     * @param origen y destino (String)
     * @version : 1.0
     */
    public void searchTramo(String ori, String dest)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCAR);
                st.setString(1,ori);
                st.setString(2,dest);
                rs = st.executeQuery();
                
                if(rs.next())
                    tramo=Tramo.load(rs);
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TRAMOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo <tt>updateTramo</tt>, actualiza la informacion del tramo     
     * @version : 1.0
     */
    public void updateTramo()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_MODIFICAR);
                st.setString(1,tramo.getTicket_id1());
                st.setString(2,tramo.getTicket_id2());
                st.setString(3,tramo.getTicket_id3());
                st.setString(4,tramo.getTicket_id4());
                st.setString(5,tramo.getTicket_id5());
                st.setString(6,tramo.getTicket_id6());
                st.setFloat(7,tramo.getCantidad_maximo_ticket1());
                st.setFloat(8,tramo.getCantidad_maximo_ticket2());
                st.setFloat(9,tramo.getCantidad_maximo_ticket3());
                st.setFloat(10,tramo.getCantidad_maximo_ticket4());
                st.setFloat(11,tramo.getCantidad_maximo_ticket5());
                st.setFloat(12,tramo.getCantidad_maximo_ticket6());
                st.setFloat(13, tramo.getAcpm());
                st.setString(14, tramo.getOrigin());
                st.setString(15, tramo.getDestination());
                ////System.out.println(st.toString());
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    /**
     * Getter for property tr.
     * @return Value of property tr.
     */
    public Tramo getTr() {
        return tr;
    }
    
    /**
     * Setter for property tr.
     * @param tr New value of property tr.
     */
    public void setTr(Tramo tr) {
        this.tr = tr;
    }
    
    /**
     * Getter for property tramos.
     * @return Value of property tramos.
     */
    public java.util.Vector getTramos() {
        return tramos;
    }
    
    /**
     * Setter for property tramos.
     * @param tramos New value of property tramos.
     */
    public void setTramos(java.util.Vector tramos) {
        this.tramos = tramos;
    }
    
    /**
     * Metodo <tt>list</tt>, obtiene los tramos con origen y destino dado      
     * @param origen y destino (String) 
     * @version : 1.0
     */
    public void list(String o, String d)throws SQLException{        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        tr  = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_TRAMO);
                st.setString(1, o);
                st.setString(2, d);
                
                rs= st.executeQuery();
                if(rs.next()){
                    tr = new Tramo();
                    tr.setOrigen(rs.getString("origen"));
                    tr.setDestino(rs.getString("destino"));
                    tr.setTiempo(rs.getFloat("tiempo"));                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA OBTENCION DE TRAMOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo <tt>agregarTramo</tt>, ingresar un nuevo tramo
     * @autor : Ing. Jesus Cuestas          
     * @version : 1.0
     */
    public void agregarTramo()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_INSERTAR);
                st.setString(1, tramo.getDstrct());
                st.setString(2, tramo.getOrigen());
                st.setString(3, tramo.getDestino());
                st.setFloat(4, tramo.getConsumo_combustible());
                st.setString(5, tramo.getUnidad_consumo());
                st.setFloat(6, tramo.getDistancia());
                st.setString(7, tramo.getUnidad_distancia());
                st.setFloat(8, tramo.getDuracion());
                st.setString(9, tramo.getUnidad_duracion());
                st.setString(10, tramo.getUsuario());
                st.setString(11, tramo.getUsuario());
                st.setString(12, tramo.getPaiso());
                st.setString(13, tramo.getPaisd());
                st.setString(14, tramo.getBase());
                ////System.out.println("AG TRAMO: "+st);
                st.executeUpdate();
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCION DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo <tt>anularTramo</tt>, anular un tramo (reg_status=A)
     * @autor : Ing. Jesus Cuestas          
     * @version : 1.0
     */
    public void anularTramo()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement( SQL_ANULAR );
                st.setString(1, tramo.getUsuario());
                st.setString(2, tramo.getDstrct());
                st.setString(3, tramo.getOrigen());
                st.setString(4, tramo.getDestino());
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo <tt>listarTramos</tt>, obtiene los tramos del sistema
     * @autor : Ing. Jesus Cuestas          
     * @version : 1.0
     */
    public void listarTramos()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR);
                //////System.out.println(""+st);
                rs= st.executeQuery();
                
                tramos = new Vector();
                while(rs.next()){
                    tramo = new Tramo();                    
                    tramo.setDstrct(rs.getString("dstrct"));
                    tramo.setNorigen(rs.getString("nomorigen"));
                    tramo.setNdestino(rs.getString("nomdest"));
                    tramo.setOrigen(rs.getString("origin"));
                    tramo.setDestino(rs.getString("destination"));
                    tramo.setConsumo_combustible(rs.getFloat("acpm"));
                    tramo.setUnidad_consumo(rs.getString("uconsumo"));
                    tramo.setDistancia(rs.getFloat("km"));
                    tramo.setUnidad_distancia(rs.getString("udistancia"));
                    tramo.setDuracion(rs.getFloat("tiempo"));
                    tramo.setUnidad_duracion(rs.getString("uduracion"));
                    tramo.setPaiso(rs.getString("po"));
                    tramo.setPaisd(rs.getString("pd"));
                    tramos.add(tramo);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo <tt>consultarTramo</tt>, obtiene los tramos dados unos parametros
     * @autor : Ing. Jesus Cuestas          
     * @modificado por: Sandra Escalante 15-03-2006
     * @version : 1.0
     */    
    public void consultarTramo()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_CONSULTA);
                st.setString(1, tramo.getDstrct()+"%");
                st.setString(2, tramo.getPaisd()+"%");
                st.setString(3, tramo.getPaisd()+"%");
                st.setString(4, tramo.getPaiso()+"%");
                st.setString(5, tramo.getPaiso()+"%");
                st.setString(6, tramo.getOrigen()+"%");
                st.setString(7, tramo.getOrigen()+"%");
                st.setString(8, tramo.getDestino()+"%");
                st.setString(9, tramo.getDestino()+"%");
                st.setString(10, tramo.getScombustible()+"%");
                st.setString(11, tramo.getSdistancia()+"%");
                st.setString(12, tramo.getSduracion()+"%");
               //////System.out.println("ST....." + st);
                rs= st.executeQuery();
                tramos = new Vector();
                while(rs.next()){
                    tramo = new Tramo();                    
                    tramo.setDstrct(rs.getString("dstrct"));
                    tramo.setNorigen(rs.getString("nomorigen"));
                    tramo.setNdestino(rs.getString("nomdest"));
                    tramo.setOrigen(rs.getString("origin"));
                    tramo.setDestino(rs.getString("destination"));
                    tramo.setConsumo_combustible(rs.getFloat("acpm"));
                    tramo.setUnidad_consumo(rs.getString("uconsumo"));
                    tramo.setDistancia(rs.getFloat("km"));
                    tramo.setUnidad_distancia(rs.getString("udistancia"));
                    tramo.setDuracion(rs.getFloat("tiempo"));
                    tramo.setUnidad_duracion(rs.getString("uduracion"));
                    tramo.setPaiso(rs.getString("npaiso"));
                    tramo.setPaisd(rs.getString("npaisd"));
                    tramos.add(tramo);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Metodo <tt>buscarTramo</tt>, obtiene un tramos dado unos parametros
     * @autor : Ing. Jesus Cuestas          
     * @param distrito, origen y destino (String)
     * @version : 1.0
     */
    public void buscarTramo(String cia, String origen, String destino)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_BUSCAR2);
                ////System.out.println(st);
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);
                rs= st.executeQuery();
                if(rs.next()){
                    tramo = new Tramo();
                    tramo.setDstrct(rs.getString("dstrct"));                    
                    tramo.setOrigen(rs.getString("origin"));
                    tramo.setNorigen(rs.getString("norigen"));
                    tramo.setNdestino(rs.getString("ndestino"));
                    tramo.setDestino(rs.getString("destination"));
                    tramo.setConsumo_combustible(rs.getFloat("acpm"));
                    tramo.setUnidad_consumo(rs.getString("unidad_consumo"));
                    tramo.setDistancia(rs.getFloat("km"));
                    tramo.setUnidad_distancia(rs.getString("unidad_distancia"));
                    tramo.setDuracion(rs.getFloat("tiempo"));
                    tramo.setUnidad_duracion(rs.getString("unidad_duracion"));
                    tramo.setPaisd(rs.getString("npaisd"));
                    tramo.setPaiso(rs.getString("npaiso"));
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA (BUSCAR2) DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo <tt>modificarTramo</tt>, actualiza la informacion del tramo
     * @autor : Ing. Jesus Cuestas          
     * @version : 1.0
     */
    public void modificarTramo()throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_MODIFICAR2);
                st.setFloat(1, tramo.getConsumo_combustible());
                st.setString(2, tramo.getUnidad_consumo());
                st.setFloat(3, tramo.getDistancia());
                st.setString(4, tramo.getUnidad_distancia());
                st.setFloat(5, tramo.getDuracion());
                st.setString(6, tramo.getUnidad_duracion());
                st.setString(7, tramo.getUsuario());
                st.setString(8, tramo.getDstrct());
                st.setString(9, tramo.getOrigen());
                st.setString(10, tramo.getDestino());                
                st.executeUpdate();
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA MODIFICACION DEL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }

    /**
     * Metodo <tt>existeTramo</tt>, verifica la existencia de un tramo
     * @autor : Ing. Jesus Cuestas          
     * @param disrito, origen, destino (String)
     * @version : 1.0
     */
    public boolean existeTramo(String cia, String origen, String destino)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;//no existe
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_VERIFICAR);
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);                
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;//si existe
                }
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICANDO EL TRAMO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Metodo <tt>listTramos</tt>, obtiene la lista de los tramos segun distrito
     * @autor : Ing. Jesus Cuestas          
     * @param distrito
     * @version : 1.0
     */
    public void listTramos(String cia)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ltramos = null;
        ltramos = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR2);
                st.setString(1,cia);
                ////System.out.println(""+st);
                rs = st.executeQuery();
               
                while(rs.next()){                                            
                    ltramos.put(rs.getString("norigen")+" - "+rs.getString("ndestino"), 
                        rs.getString("origin")+rs.getString("destination"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA LISTA (LISTA2) DE TRAMOS POR DISTRITO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    /**
     * Metodo <tt>listTramos</tt>, obtiene una lista de los tramos dado unos
     *  parametros
     * @autor : Ing. Jesus Cuestas          
     * @param origen, distrito y mensaje (String)
     * @version : 1.0
     */
    public void listTramos(String origen, String cia,String mensaje)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ltramos = null;
        ltramos = new TreeMap();
        PoolManager poolManager = null;
        int sw=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(SQL_LISTAR3);
                st.setString(1, origen);
                st.setString(2, cia);
                rs = st.executeQuery();               
                
                while(rs.next()){
                    ltramos.put(rs.getString("norigen")+" - "+rs.getString("ndestino"), 
                        rs.getString("origin")+rs.getString("destination"));
                    sw = 1;
                }
                
                if (sw==1){
                    ltramos.put(" Seleccione",""); 
                }
                else {
                    ltramos.put(" No se encuentran tramos con origen seleccionado...", "");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS TRAMOS ( LISTAR3 ) POR ORIGEN Y DISTRITO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * Getter for property ltramos.
     * @return Value of property ltramos.
     */
    public java.util.TreeMap getLtramos() {
        return ltramos;
    }
    
    /**
     * Setter for property ltramos.
     * @param ltramos New value of property ltramos.
     */
    public void setLtramos(java.util.TreeMap ltramos) {
        this.ltramos = ltramos;
    }
    
    /**
     * Getter for property flag.
     * @return Value of property flag.
     */
    public boolean isFlag() {
        return flag;
    }
    
    /**
     * Setter for property flag.
     * @param flag New value of property flag.
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
            
    /**
     * Metodo <tt>listarOrigenes</tt>, obtiene los origenes de los tramos
     * @autor : Ing. Tito Maturana
     * @version : 1.0
     */
    public TreeMap listarOrigenes()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        TreeMap lista = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_ORIGENES);
                rs = st.executeQuery();
                int sw=0;
                lista.put(" Seleccione","");
                while(rs.next()){
                    lista.put(rs.getString(2), rs.getString(1));                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LOS ORIGENES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        return lista;
    }
    
    /**
     * Metodo <tt>listarDestinos</tt>, obtiene los destinos de los tramos
     *  dado un origen
     * @autor : Ing. Tito Maturana
     * @param origen (String)
     * @version : 1.0
     */
    public TreeMap listarDestinos(String orig)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ltramos = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_DESTINOS);
                st.setString(1, orig);
                rs = st.executeQuery();
                ltramos.put(" Seleccione","");
                while(rs.next()){
                    ltramos.put(rs.getString(2), rs.getString(1));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LOS DESTINOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return ltramos;
    }
    
    /**
     * Metodo <tt>existeTramoAnulado</tt>, verifica la existencia de un tramo
     *  anulado
     * @autor : Ing. Sandra Escalante
     * @param distrito, origen, destino (String)
     * @version : 1.0
     */
    public boolean existeTramoAnulado(String cia, String origen, String destino)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw = false;//no existe
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_VERIFTANUL);
                st.setString(1,cia);
                st.setString(2,origen);
                st.setString(3,destino);                
                ////System.out.println("TR ANULADO: "+st);
                rs = st.executeQuery();
                if(rs.next()){
                    sw = true;//si existe
                }
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR VERIFICANDO LA EXISTENCIA DE UN TRAMO ANULADO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
     
    /**
     * Actualiza el origen o el destino de un tramo.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param nor C�digo de la nueva ciudad de origen.
     * @param ndes C�digo de la nueva ciudad destino.
     * @param or C�digo de la ciudad de origen.
     * @param des C�digo de la ciudad destino.
     * @throws SQLException si ocurre una excepci�n en la conexi�n con la Base de Datos
     * @version 1.0
     */
    public void updateTramoExtremo(String nor, String ndes, String or, String des)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(this.SQL_UPDATEEXT);
                st.setString(1, nor);
                st.setString(2, ndes);
                st.setString(3, or);
                st.setString(4, des);
                
                st.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR EN [updateTramoExtremo] " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
}





