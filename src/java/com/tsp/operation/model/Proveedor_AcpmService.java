package com.tsp.operation.model;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;

public class Proveedor_AcpmService{
    
    private Proveedor_AcpmDAO proveedor;
    
    Proveedor_AcpmService(){
        
        proveedor = new Proveedor_AcpmDAO();
    }
    public List getProveedoresACPM(String dstrct )throws SQLException{
        
        List proveedores=null;
        
        proveedor.searchProveedorACPM(dstrct);
        proveedores = proveedor.getProveedorACPM();
        
        return proveedores;
        
    }
    
    public boolean existProveedoresAcpm(String dstrct )throws SQLException{
        
        return proveedor.existProveedorAcpm(dstrct);
        
    }
    public boolean existProveedor(String nit )throws SQLException{
        
        return proveedor.existProveedor(nit);
        
    }
    
    public boolean existSucursal(String nit, String sucursal )throws SQLException{
        
        return proveedor.existSucursal(nit,sucursal);
        
    }
    
    public Proveedor_Acpm getProveedor( )throws SQLException{
        
        return proveedor.getProveedor();
        
    }
    
    public String getCodigo( String nit)throws SQLException{
        
        return proveedor.searchCodigo(nit);
        
    }
    
    
    public void buscaProveedor(String nit, String codigo)throws SQLException{
        try{
            proveedor.searchProveedor(nit,codigo);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public void consultaProveedor(String ciudad, String nombre, String nit, String tipo_servicio)throws SQLException{
        try{
            proveedor.consultar(ciudad, nombre, nit, tipo_servicio);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
    public Vector getProveedores(){
        return proveedor.getProveedores();
    }
    
    public void insertProveedor(Proveedor_Acpm pacpm, String base)throws SQLException{
        try{
            proveedor.setProveedor(pacpm);
            proveedor.insertProveedor(base);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void updateProveedor(Proveedor_Acpm pacpm)throws SQLException{
        try{
            proveedor.setProveedor(pacpm);
            proveedor.updateProveedor();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    public void anularProveedor(Proveedor_Acpm pacpm)throws SQLException{
        try{
            proveedor.setProveedor(pacpm);
            proveedor.anularProveedor();
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
}





