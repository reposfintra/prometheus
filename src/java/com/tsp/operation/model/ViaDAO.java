/*
 * ViaDAO.java
 *
 * Created on 9 de diciembre de 2004, 03:37 PM
 */

package com.tsp.operation.model;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.beans.Via;

public class ViaDAO {
    
    private Vector vecVias;
    
   
    private static String SQL_DESTINOS ="   SELECT  DISTINCT(destino), " +
                                        "           get_nombreciudad(destino) " +
                                        "   FROM    via " +
                                        "   WHERE   origen = ? ";//AMATURANA 14.11.2006
    
     private final static String SQL_VIAS_REL_POSGRE =
    "SELECT (origen || destino || secuencia) as codvia, descripcion " +
    "  FROM via " +
    " WHERE origen || destino = SUBSTR(?, 1, 4)" +
    "  and rec_status!='A'";    
    
    private final static String SQL_VIAS_REL =
    "SELECT TABLE_CODE, TABLE_DESC FROM MSF010 WHERE TABLE_TYPE = '+RP' AND TABLE_CODE4 = SUBSTR(?, 1, 4)";
    
    private final static String SQL_VIAS_DESC_POSGRE =
    "SELECT descripcion FROM via WHERE origen || destino || secuencia = ?";
    
    private final static String SQL_VIAS_DESC =
    "SELECT TABLE_DESC FROM MSF010 WHERE TABLE_TYPE = '+RP' AND TABLE_CODE = ?";
    
    private final static String SQL_VIAS_POSGRE =
    "SELECT (origen || destino || secuencia) as codvia, descripcion " +
    "FROM via";
    
    private final static String SQL_VIAS =
    "SELECT TABLE_CODE, TABLE_DESC FROM MSF010 WHERE TABLE_TYPE = '+RP'";
    
    private final static String SQL_VIAS_TREE = "SELECT secuencia,descripcion FROM via WHERE origen = ? AND destino = ?";
    
    private static String SQL_GET_NOMCIUDAD = "SELECT NOMCIU FROM CIUDAD WHERE CODCIU=?";
    
    //private static String SQL_GET_CODZONA = "SELECT ZONA FROM CIUDAD WHERE CODCIU=?";
    
    private final static String SQL_VIAS_CIU =
        "SELECT " +
        "       origen, " +
        "       COALESCE(get_nombreciudad(origen),'') AS nomorig, " +
        "       destino, " +
        "       COALESCE(get_nombreciudad(destino),'') AS nomdes, " +
        "       secuencia, " +
        "       descripcion, " +
        "       via " +
        "FROM   via " +
        "WHERE  via LIKE ? " +
        "       AND rec_status != 'A'";
    /*  Added by Andr�s Maturana */
   
    
    private final static String SQL_UPDATE = 
        "UPDATE via SET " +
        "   via = ?, " +
        "   tiempo = ?, " +
        "   lastupdate = 'now()' " +
        "WHERE" +
        "   origen = ?" +
        "   AND destino = ?" +
        "   AND secuencia = ?";
    
    private final static String SQL_UPDATE_DUR = 
        "UPDATE via SET " +
        "   duracion = ? " +
        "WHERE" +
        "   origen = ?" +
        "   AND destino = ?" +
        "   AND secuencia = ?";
    
    private final static String SQL_OBTENER = 
        "SELECT " +
        "   origen, " +
        "   destino, " +
        "   secuencia, " +
        "   COALESCE(tiempo, 0.0) AS tiempo " +
        "FROM via " +
        "WHERE " +
        "   origen = ? " +
        "   AND destino = ?";
    private TreeMap cbxVias;
    
    /** Creates a new instance of ViaDAO */
    public ViaDAO() {
    }
    
    public void setCbxVias(TreeMap cbxVias){
        this.cbxVias = cbxVias;
    }
    
    public TreeMap getCbxVias(){
        return cbxVias;
    }
    
    public void getViasRel(String codvia) throws SQLException {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        TreeMap vias = null;
        cbxVias = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("mim");
            psttm = con.prepareStatement(SQL_VIAS_REL);
            psttm.setString(1,  codvia);
            rs = psttm.executeQuery();
            cbxVias = new TreeMap();
            while (rs.next()){
                cbxVias.put(rs.getString(2), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE VIAS RELACIONADAS: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("mim", con);
            }
        }
    }
    
    /**
     *Metodo para obtener la vias relacionadas a una ruta desde postgres
     */
    public void getViasRelPg(String codvia) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        TreeMap vias = null;
        cbxVias = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(SQL_VIAS_REL_POSGRE);
            psttm.setString(1,  codvia);
            rs = psttm.executeQuery();
            cbxVias = new TreeMap();
            while (rs.next()){
                cbxVias.put(rs.getString(2), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE VIAS RELACIONADAS EN POSTGRES: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void getVias() throws SQLException{
        Connection con = null;
        Statement sttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        TreeMap vias = null;
        cbxVias = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("mim");
            sttm = con.createStatement();
            rs = sttm.executeQuery(SQL_VIAS);
            cbxVias = new TreeMap();
            cbxVias.put("", "");
            while (rs.next()){
                cbxVias.put(rs.getString(2), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE VIAS: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){
                try{
                    sttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("mim", con);
            }
        }
    }
    
    /**
     *Metodo para obtener el listado de todas las vias desde postgres
     */
    public void getViasPg() throws SQLException{
        Connection con = null;
        Statement sttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        TreeMap vias = null;
        cbxVias = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            sttm = con.createStatement();
            rs = sttm.executeQuery(SQL_VIAS_POSGRE);
            cbxVias = new TreeMap();
            cbxVias.put("", "");
            while (rs.next()){
                cbxVias.put(rs.getString(2), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO EL LISTADO DE  LAS VIAS EN POSTGRES: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (sttm != null){
                try{
                    sttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public String getDescripcion(String via) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String descripcion = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("mim");
            psttm = con.prepareStatement(SQL_VIAS_DESC);
            psttm.setString(1, via);
            rs = psttm.executeQuery();
            if (rs.next()){
                descripcion =  rs.getString(1);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE DESCRIPCION VIA" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("mim", con);
            }
        }
        return descripcion;
    }
    /**
     * M�todo que retorna la ruta de una via
     * @autor.......kreales
     * @param.......el codigo de la via
     * @throws......SQLException
     * @version.....1.0.
     * @return.......String de la ruta
     **/
    public String getRuta(String via) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String descripcion = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement("Select via from via where origen||destino||secuencia = ?");
            psttm.setString(1, via);
            rs = psttm.executeQuery();
            if (rs.next()){
                descripcion =  rs.getString(1);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CONSULTA DE DESCRIPCION VIA EN POSTGRES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return descripcion;
    }
    /**
     * M�todo que retorna la ruta de una via
     * @autor.......Jose de la rosa
     * @param.......el origen, destino
     * @throws......SQLException
     * @version.....1.0.
     * @return
     **/
    public void getViasTreeMaps(String origen, String destino) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        TreeMap vias = null;
        cbxVias = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            psttm = con.prepareStatement(SQL_VIAS_TREE);
            psttm.setString(1,  origen);
            psttm.setString(2,  destino);
            rs = psttm.executeQuery();
            cbxVias = new TreeMap();
            while (rs.next()){
                cbxVias.put(rs.getString(2), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA DE VIAS: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null){
                try{
                    psttm.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    /**
     * M�todo que buscar y llena un TreeMap con las ciudades que comprenden una via
     * @autor:     Henry A. Osorio Gonzalez
     * @param:     String origen.  Origen de la Ruta
     * @param:     String destino. Destino de la Ruta
     * @param:     String freq.    Frecuencia de la Ruta
     * @throws:    SQLException
     * @version:   1.0.
     **/
    public void getViasXRutaFrecuencia(String ruta_pla) throws SQLException {
        String via = this.getRuta(ruta_pla);
        Via objetoVia;
        vecVias = new Vector();
        int cont = 0;
        if (via!=null) {
            for(int i=0;i<via.length();i++){
                cont++;
                if(cont==2){
                    objetoVia = new Via();
                    String codciu = via.substring(i-1,i+1);
                    objetoVia.setCodigo(codciu);
                    String nomciu = getNombreCiudad(codciu);                   
                    if(nomciu==null){
                        objetoVia.setCiudad(codciu+" [No existe]");
                    }else{
                        objetoVia.setCiudad(nomciu);
                        //fila.add(codciu);
                    }
                    vecVias.addElement(objetoVia);
                    cont = 0;
                }
            }
        }
    }
    
    /**
     * M�todo que retorna el nombre de un puesto de control
     * @autor.......Armando Oviedo
     * @param.......c�digo pc
     * @throws......SQLException
     * @version.....1.0.
     * @return.......Nombre del puesto de control
     **/
    public String getNombreCiudad(String codciu) throws SQLException{
        Connection con = null;
        PoolManager pm = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String nombre=null;
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(SQL_GET_NOMCIUDAD);
            ps.setString(1, codciu);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }
        }catch(SQLException ex){
            throw new SQLException("ERROR AL BUSCAR EL NOMBRE DE LA CIUDAD, "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
        return nombre;
    }
    
    
    /**
     * Getter for property vecVias.
     * @return Value of property vecVias.
     */
    public java.util.Vector getVecVias() {
        return vecVias;
    }
   
    
    /**
     * Obtiene todas las vias donde el c�digo de la ciudad se encuentra registrado en el campo via.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param codciu C�digo de la ciudad
     * @throws SQLException
     * @version 1.0
     */
    public void viasCiudad(String codciu) throws SQLException{
        Connection con = null;
        PoolManager pm = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        this.vecVias = new Vector();
        
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(this.SQL_VIAS_CIU);
            ps.setString(1, "%" + codciu + "%");
            rs = ps.executeQuery();
            
            while(rs.next()){
                Hashtable ht = new Hashtable();
                ht.put("orig", rs.getString("origen"));
                ht.put("dest", rs.getString("destino"));
                ht.put("norig", rs.getString("nomorig"));
                ht.put("ndest", rs.getString("nomdes"));
                ht.put("sec", rs.getString("secuencia"));
                ht.put("via", rs.getString("via"));
                ht.put("desc", rs.getString("descripcion"));
                
                this.vecVias.add(ht);
            }
       
        }catch(SQLException ex){
            throw new SQLException("ERROR EN [viasCiudad] "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }

    /**
     * Actualiza el tiempo y la via
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ori C�digo de la ciudad origen
     * @param des C�digo de la ciudad destino
     * @param sec C�digo de la secuencia
     * @param via Via
     * @param tiempo Tiempo
     * @throws SQLException
     * @version 1.0
     */    
    public void updateVia(String ori, String des, String sec, String via,  float tiempo) throws SQLException{
        Connection con = null;
        PoolManager pm = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        this.vecVias = new Vector();
        
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(this.SQL_UPDATE);
            ps.setString(1, via);
            ps.setFloat(2, tiempo);
            ps.setString(3, ori);
            ps.setString(4, des);
            ps.setString(5, sec);
            
            ps.executeUpdate();
       
        }catch(SQLException ex){
            throw new SQLException("ERROR EN [updateVia] "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Actualiza la m�xima duraci�n
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ori C�digo de la ciudad origen
     * @param des C�digo de la ciudad destino
     * @param sec C�digo de la secuencia
     * @param dur Duracion
     * @throws SQLException
     * @version 1.0
     */
    public void updateDuracion(String ori, String des, String sec, float dur) throws SQLException{
        Connection con = null;
        PoolManager pm = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        this.vecVias = new Vector();
        
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(this.SQL_UPDATE_DUR);
            ps.setFloat(1, dur);
            ps.setString(2, ori);
            ps.setString(3, des);
            ps.setString(4, sec);
            
            ps.executeUpdate();
       
        }catch(SQLException ex){
            throw new SQLException("ERROR EN [updateDuracion] "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }

    /**
     * Obtiene todas las v�as con el mismo origen y destino
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ori C�digo de la ciudad origen
     * @param des C�digo de la ciudad destino
     * @throws SQLException
     * @version 1.0
     */      
    public void obtenerVias(String ori, String dest) throws SQLException{
        Connection con = null;
        PoolManager pm = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        this.vecVias = new Vector();
        
        try{
            pm = PoolManager.getInstance();
            con = pm.getConnection("fintra");
            ps = con.prepareStatement(this.SQL_OBTENER);
            ps.setString(1, ori);
            ps.setString(2, dest);
            
            rs = ps.executeQuery();
            
            while(rs.next()){
                Hashtable ht = new Hashtable();
                ht.put("orig", rs.getString("origen"));
                ht.put("dest", rs.getString("destino"));
                ht.put("sec", rs.getString("secuencia"));
                ht.put("time", String.valueOf(rs.getFloat("tiempo")));
                
                this.vecVias.add(ht);
            }
       
        }catch(SQLException ex){
            throw new SQLException("ERROR EN [obtenerVias] "+ex.getMessage()+", "+ex.getErrorCode());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                pm.freeConnection("fintra", con );
            }
        }
    }
    
    /**
     * Metodo <tt>listarDestinos</tt>, obtiene los destinos de las v�as
     *  dado un origen
     * @autor Ing. Andr�s Maturana
     * @param origen (String)
     * @version : 1.0
     */
    public void listarDestinos(String orig)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.cbxVias = new TreeMap();
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            
            if(con!=null){
                st = con.prepareStatement(this.SQL_DESTINOS);
                st.setString(1, orig);
                rs = st.executeQuery();
                this.cbxVias.put(" Seleccione","");
                while(rs.next()){
                    this.cbxVias.put(rs.getString(2), rs.getString(1));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE LOS DESTINOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
}
