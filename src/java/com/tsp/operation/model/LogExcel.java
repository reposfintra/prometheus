/*
 * LogExcel.java
 *
 * Created on 24 de julio de 2005, 09:25 PM
 */

package com.tsp.operation.model;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


/**
 *
 * @author  Jm
 */
public class LogExcel extends Thread{
    //private String Usuario;
    //private Model  model;
    //private String fileLog;
    private List Lista;
    private String Directorio;
    private String Archivo;
    private int registrosInsertados;
    private int registrosNoinsertados;
    private int       Cont;
    private int       Fila;
    
    private HSSFWorkbook  workbook;
    private HSSFSheet     sheet;
    private HSSFCellStyle style;
    private HSSFFont      font;
    private HSSFPalette   palette;
    private HSSFRow      row   = null;
    private HSSFCell     cell  = null;
    private Util u = new Util();
    
    private String usuario;
    /** Creates a new instance of LogExcel */
    public LogExcel() {
    }
    
    public void star(List LRegistros, int regI, int regNoI, String us) throws Exception{
        //sandrameg 190905
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        this.usuario = us;
        this.Directorio = path + "/exportar/migracion/" + usuario + "/";
        this.Archivo = "LogRD["+u.getFechaActual_String(4)+"].xls";////System.out.println("RUTA: " + Directorio + Archivo);
        this.Lista = LRegistros;
        this.registrosInsertados = regI;
        this.registrosNoinsertados = regNoI;
        
        super.start();
    }
    
    public void addCell(int Fila, int Columna, String Valor, HSSFCellStyle Style) throws Exception{
        try{
            HSSFRow  r = this.sheet.createRow((int)(Fila));
            HSSFCell c = r.createCell((short)(Columna));
            c.setCellValue(Valor);
            if(Style!=null) c.setCellStyle(Style);
        } catch(Exception e){ ////System.out.println("ERROR: RUTINA addCell(String). "+e.getMessage());
        }
    }
    
    public void addCell(int Fila, int Columna, double Valor, HSSFCellStyle Style) throws Exception{
        try{
            HSSFRow  r = this.sheet.createRow((int)(Fila));
            HSSFCell c = r.createCell((short)(Columna));
            c.setCellValue(Valor);
            if(Style!=null) c.setCellStyle(Style);
        } catch(Exception e){ ////System.out.println("ERROR: RUTINA addCell(double). "+e.getMessage());
        }
    }
    
    public void NuevoExcel(boolean Cabecera) throws Exception{
        try{
            if(Cabecera){
                Fila = 3;
                workbook = new HSSFWorkbook();
                sheet   = workbook.createSheet();
                
                /****Estilo de la cabecera****/
                /*style   = workbook.createCellStyle();
                font    = workbook.createFont();
                font.setFontHeightInPoints((short)12);        //tamano letra
                font.setFontName("Arial");                    //Tipo letra
                font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); //letra negrita
                font.setColor(HSSFColor.BLACK.index);
                style.setFont(font);
                style.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));*/
                
                for (int col=0; col<40 ; col++)
                    sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );
                
                
                /** ENCABEZADO GENERAL *******************************/
                HSSFFont  fuente1 = workbook.createFont();
                fuente1.setFontName("verdana");
                fuente1.setFontHeightInPoints((short)(16)) ;
                fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                fuente1.setColor((short)(0x1));
                
                HSSFCellStyle estilo1 = workbook.createCellStyle();
                estilo1.setFont(fuente1);
                estilo1.setFillForegroundColor(HSSFColor.ORANGE.index);
                estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
                
                /** TEXTO EN EL ENCABEAZADO *************************/
                HSSFFont  fuente2 = workbook.createFont();
                fuente2.setFontName("verdana");
                fuente2.setFontHeightInPoints((short)(11)) ;
                fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                fuente2.setColor((short)(0x0));
                
                HSSFCellStyle estilo2 = workbook.createCellStyle();
                estilo2.setFont(fuente2);
                estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
                estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
                
                
                /** ENCABEZADO DE LAS COLUMNAS***********************/
                HSSFFont  fuente3 = workbook.createFont();
                fuente3.setFontName("verdana");
                fuente3.setFontHeightInPoints((short)(11)) ;
                fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                fuente3.setColor((short)(0x1));
                
                HSSFCellStyle estilo3 = workbook.createCellStyle();
                estilo3.setFont(fuente3);
                estilo3.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
                estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
                estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
                estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
                estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
                estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
                estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
                estilo3.setRightBorderColor(HSSFColor.BLACK.index);
                estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
                estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
                
                
                
                /*****************************/
                /* ESTADISCTICAS*/
                row  = sheet.createRow((short)(0));
                
                for (int j=0;j<7;j++) {
                    cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
                }
                
                row  = sheet.createRow((short)(1));
                row  = sheet.createRow((short)(2));
                row  = sheet.createRow((short)(3));
                row  = sheet.createRow((short)(4));
                
                
                
                row  = sheet.getRow((short)(0));
                cell = row.getCell((short)(0));
                cell.setCellValue("TRANSPORTE SANCHEZ POLO");
                
                
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo2);
                cell.setCellValue("REPORTE RECURSOS DISPONIBLES");
                
                
                addCell(2,0,"TOTAL RECURSOS DISPONIBLE",estilo3);
                addCell(3,0,this.registrosInsertados+this.registrosNoinsertados,style);
                sheet.setColumnWidth((short)0, (short) 9000);
                
                addCell(2,1,"TOTAL INSERTADOS",estilo3);
                addCell(3,1,this.registrosInsertados,style);
                sheet.setColumnWidth((short)1, (short) 9000);
                
                addCell(2,2,"TOTAL NO INSERTADOS",estilo3);
                addCell(3,2,this.registrosNoinsertados,style);
                sheet.setColumnWidth((short)2, (short) 9000);
                
                ////System.out.println("Generando cabecera del archivo...");
                addCell(4,0,"TIPO",estilo3);
                addCell(4,1,"DESCRIPCION",estilo3);
                addCell(4,2,"NUMERO DE PLANILLA",estilo3);
                addCell(4,3,"REGISTRO",estilo3);
                addCell(4,4,"COLUMNA(S)",estilo3);
                addCell(4,5,"RECURSO",estilo3);
                addCell(4,6,"ARCHIVO",estilo3);
                ////System.out.println("Generando nombre de columnas del archivo...");
                
            }
            else {
                CerrarExcel();
                workbook = new HSSFWorkbook();
                sheet    = workbook.createSheet();
            }
        } catch(Exception e){ ////System.out.println("ERROR: RUTINA NuevoExcel. "+e.getMessage());
        }
    }
    
    public void CerrarExcel() throws Exception{
        try{
            FileOutputStream fileOut = new FileOutputStream(this.Directorio+this.Archivo);
            workbook.write( fileOut );
            fileOut.close();
        } catch(Exception e){ ////System.out.println("ERROR: RUTINA CerrarExcel. "+e.getMessage());
        }
    }
    
    public void ImprimirInconsistencias() throws Exception{
        try{
            sheet.setZoom(3,4); //zoom de la hoja del 75% para mejorara la visualización de la hoja
            //sheet.createFreezePane(0,4);
            //sheet.groupColumn((short)1,(short)5);
            //sheet.setColumnGroupCollapsed((short)2, true);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = workbook.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = workbook.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            //    estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /****************************************************/
            Fila=5;
            if( this.Lista.size() > 0 ){
                Iterator It = Lista.iterator();
                while(It.hasNext()) {
                    Inconsistencia datos = (Inconsistencia) It.next();
                    
                    this.addCell(Fila,0,datos.getTipo(), estilo4);
                    sheet.setColumnWidth((short)0, (short) 9000); //aumento al ancho de las columnas que contienen los valores
                    this.addCell(Fila,1,datos.getDescripcion(), estilo4);
                    sheet.setColumnWidth((short)1, (short) 9000);
                    this.addCell(Fila,2,datos.getNumpla(), estilo4);
                    sheet.setColumnWidth((short)2, (short) 9000);
                    this.addCell(Fila,3,datos.getRegistro(), estilo4);
                    sheet.setColumnWidth((short)3, (short) 4500);
                    this.addCell(Fila,4,datos.getColumna(), estilo4);
                    sheet.setColumnWidth((short)4, (short) 4500);
                    this.addCell(Fila,5,datos.getPlaca(), estilo4);
                    sheet.setColumnWidth((short)5, (short) 4500);
                    this.addCell(Fila,6,datos.getArchivo(), estilo4);
                    sheet.setColumnWidth((short)5, (short) 4500);
                    Fila++;
                }
                
                
            }
            
        } catch(Exception e){ ////System.out.println("ERROR: RUTINA ImprimirIncosistencia. "+e.getMessage());
        }
    }
    
    public void run(){
        try{
            this.NuevoExcel(true);
            this.ImprimirInconsistencias();
            this.CerrarExcel();
            ////System.out.println("Termine Exitosamente...");
            //this.model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.Usu,"EXITOSO");
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
    
