
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.beans.*;


public class MigracionConductorPropietarioService {
    
   private MigracionConductorPropietarioDAO  MigracionDataAccess;   
   
 // Inicializamos las variables  
    public MigracionConductorPropietarioService() {
       MigracionDataAccess = new MigracionConductorPropietarioDAO();
    }
    
    
    
  //--- verificamos  en el archivo  760
    public boolean exist760(String cedula)throws Exception{
         boolean estado=false; 
         try{
             estado = MigracionDataAccess.exist760(cedula);
         }catch(Exception e){
            throw new SQLException("Error el la existencia de la cedula: "+e.getMessage());   
         }
         return estado;
    }
        
    
  //--- verificamos en el archivo  810     
    public boolean exist810(String cedula) throws SQLException{
       boolean estado=false;
       try{
          estado= MigracionDataAccess.exist810(cedula);
       }catch(Exception e){
          throw new SQLException("Error el la existencia de la cedula: "+e.getMessage());   
       }
       return estado;
    }
    
    
    //--- verificamos en el archivo  200     
    public boolean exist200(String cedula) throws SQLException{
       boolean estado=false;
       try{
          estado= MigracionDataAccess.exist200(cedula);
       }catch(Exception e){
          throw new SQLException("Error el la existencia de la cedula: "+e.getMessage());   
       }
       return estado;
    }
    
    
   //--- buscamos el propietario de la placa
    public String getPropietario(String placa) throws SQLException{
        String propietario="";
        try{
           propietario=MigracionDataAccess.searchPropietario(placa);
        }catch(Exception e){
           throw new SQLException("error en la busqueda del propietario: "+e.getMessage());     
        }
        return propietario;
    }
    
    
     //--- buscamos  la CC del propietario de la placa
    public String getCCPropietario(String remision) throws SQLException{
        String propietario="";
        try{
           propietario=MigracionDataAccess.searchCCPropietario(remision);
        }catch(Exception e){
           throw new SQLException("error en la busqueda del propietario: "+e.getMessage());     
        }
        return propietario;
    }
    
   
}
