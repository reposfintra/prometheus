/*
 * AnticiposDAO.java
 *
 * Created on 20 de abril de 2005, 09:14 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  kreales
 */
public class AnticiposDAO extends MainDAO {
    
    private List antList;
    private Anticipos ant;
    private Vector anticipos;
    private Vector anticiposProv;
    private Proveedores provee;
    
    
    
    /** Creates a new instance of AnticiposDAO */
    public AnticiposDAO() {
        super("AnticiposDAO.xml");
    }
    public AnticiposDAO(String dataBaseName) {
        super("AnticiposDAO.xml", dataBaseName);
    }
    public Vector getAnticipos(){
        return anticipos;
    }
    public Anticipos getAnticipo(){
        return ant;
    }
    
    public void setAnticipos(Anticipos a){
        this.ant = a;
    }
    public void insertAnticipo(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("select * from tblcon where concept_code=? ");
                st.setString(1,ant.getAnticipo_code());
                rs = st.executeQuery();
                if(!rs.next()){
                    st = con.prepareStatement("insert into tblcon(  dstrct, concept_code,concept_desc, creation_date, creation_user, base, codigo_migracion, ind_application) values (?,?,?,'now()',?,?,?,?)");
                    st.setString(1, ant.getDstrct());
                    st.setString(2, ant.getAnticipo_code());
                    st.setString(3, ant.getAnticipo_desc());
                    st.setString(4, ant.getCreation_user());
                    st.setString(5, base);
                    st.setString(6, ant.getCodmigra());
                    st.setString(7, ant.getIndicador());
                    st.executeUpdate();
                }
                st = con.prepareStatement("insert into tbldes(  dstrct, concept_code,std_job_no, creation_date, creation_user,  type, vlr,currency,base) values (?,?,?,'now()',?,?,?,?,?)");
                st.setString(1, ant.getDstrct());
                st.setString(2, ant.getAnticipo_code());
                st.setString(3, ant.getSj());
                st.setString(4, ant.getCreation_user());
                st.setString(5, ant.getTipo_s());
                st.setFloat(6, ant.getValor());
                st.setString(7, ant.getMoneda());
                st.setString(8, base);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS DESCUENTOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public void updateAnticipos()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                
                st = con.prepareStatement("update tblcon set concept_desc=?, ind_application=?, codigo_migracion =?, last_update='now()' where dstrct=? and  concept_code=?");
                st.setString(1, ant.getAnticipo_desc());
                st.setString(2, ant.getIndicador());
                st.setString(3, ant.getCodmigra());
                st.setString(4, ant.getDstrct());
                st.setString(5, ant.getAnticipo_code());
                st.executeUpdate();
                
                st = con.prepareStatement("update tbldes set type=?, vlr=?,currency=? where dstrct=? and concept_code=? and std_job_no=?");
                st.setString(1, ant.getTipo_s());
                st.setFloat(2, ant.getValor());
                st.setString(3, ant.getMoneda());
                st.setString(4, ant.getDstrct());
                st.setString(5, ant.getAnticipo_code());
                st.setString(6, ant.getSj());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LOS ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public void anularAnticipo()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("update tbldes set reg_status ='A' where dstrct=? and  concept_code=? and std_job_no=?");
                st.setString(1, ant.getDstrct());
                st.setString(2, ant.getAnticipo_code());
                st.setString(3, ant.getSj());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LOS PROVEEDORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    
    public void vecAnticipos(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("SELECT d.*, t.concept_desc,t.ind_application,t.codigo_migracion,coalesce(s.std_job_desc,'Ninguno')as std_job_desc FROM tbldes d  LEFT OUTER JOIN stdjob s  ON (d.std_job_no =s.std_job_no), tblcon t where d.base =? and d.concept_code = t.concept_code and d.reg_status=''");
                st.setString(1, base);
                rs = st.executeQuery();
                anticipos = new Vector();
                while(rs.next()){
                    ant = new Anticipos();
                    ant.setAnticipo_code(rs.getString("concept_code"));
                    ant.setAnticipo_desc(rs.getString("concept_desc"));
                    ant.setDstrct(rs.getString("dstrct"));
                    ant.setTipo_s(rs.getString("type"));
                    ant.setSj_nombre(rs.getString("std_job_desc"));
                    ant.setSj(rs.getString("std_job_no"));
                    
                    anticipos.add(ant);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR FORMANDO EL VECTOR DE LOS ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
  
  /**
     *Metodo que busca la lista de anticipos a aplicar en el despacho, que
     *son aplicados a un proveedor.
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchAnticiposProveedor(Usuario u)throws SQLException{
        
        PreparedStatement st = null, st2=null;
        ResultSet rs = null, rs2 = null;
        
        try {
            Connection con = this.conectar("SQL_BUSCAR_DESC_PROV");
            st2 = con.prepareStatement(this.obtenerSQL("SQL_LISTA_PROV"));
            
            st = con.prepareStatement(this.obtenerSQL("SQL_BUSCAR_DESC_PROV"));
            st.setString(1,u.getId_agencia());
            rs = st.executeQuery();
            anticiposProv = new Vector();
            while(rs.next()){
                ant = new Anticipos();
                ant.setAnticipo_code(rs.getString("concept_code"));
                ant.setAnticipo_desc(rs.getString("concept_desc"));
                ant.setDstrct(rs.getString("dstrct"));
                ant.setTipo_s(rs.getString("tipo"));
                ant.setValor(rs.getFloat("vlr"));
                ant.setModif(rs.getString("modif"));
                ant.setProveedor("890103161/TRANSPORTE SANCHEZ POLO");
                ant.setChequeado(false);
                
                st2.setString(1,u.getId_agencia());
                st2.setString(2,rs.getString("concept_code"));
                rs2 = st2.executeQuery();
                Vector proveedores = new Vector();
                while(rs2.next()){
                   Proveedores provee = new Proveedores(); 
                   provee.setNit(rs2.getString("nit_prov"));
                   provee.setSucursal(rs2.getString("sucursal_prov"));
                   provee.setNombre(rs2.getString("nombre"));
                   proveedores.add(provee);
                }
                ant.setProveedores(proveedores);
                
                anticiposProv.add(ant);
                
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR FORMANDO EL VECTOR DE LOS ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_BUSCAR_DESC_PROV");
        }
        
    }
    //Karen 30/Septiembre 2006
     public void searchAnticipos(String dst, String codAnt, String stdjob)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ant = null;
        try {
                st = this.crearPreparedStatement("SQL_SEARCH_ANTICIPO");
                st.setString(1, dst);
                st.setString(2, codAnt);
                rs = st.executeQuery();
                if(rs.next()){
                    ant = new Anticipos();
                    ant.setAnticipo_code(rs.getString("concept_code"));
                    ant.setAnticipo_desc(rs.getString("concept_desc"));
                    ant.setDstrct(rs.getString("dstrct"));
                    ant.setTipo_s(rs.getString("tipo"));
                    ant.setSj(stdjob);
                    ant.setValor(rs.getFloat("vlr"));
                    ant.setMoneda(rs.getString("currency"));
                    ant.setCodmigra(rs.getString("codigo_migracion"));
                    ant.setIndicador(rs.getString("ind_application"));
                }
                
            
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO UN ANTICIPO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            this.desconectar("SQL_SEARCH_ANTICIPO");
        }
        
    }
    public void insertProveedorAnticipos(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.getDatabaseName());
            if(con!=null){
                st = con.prepareStatement("insert into anticipos_proveedor( dstrct,  anticipo_code,  nit_prov, sucursal_prov, creation_date,creation_user, base) values (?,?,?,?,'now',?,?)");
                st.setString(1, provee.getDstrct());
                st.setString(2, provee.getAnticipo());
                st.setString(3, provee.getNit());
                st.setString(4, provee.getSucursal());
                st.setString(5, provee.getCreation_user());
                st.setString(6,base);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS PROVEEDORES - ANTICIPO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.getDatabaseName(), con);
            }
        }
        
    }
    public Proveedores getProveedore(){
        
        return provee;
    }
    public void setProveedores(Proveedores p){
        this.provee =p;
    }
    
    /**
     * Getter for property anticiposProv.
     * @return Value of property anticiposProv.
     */
    public java.util.Vector getAnticiposProv() {
        return anticiposProv;
    }
    
    /**
     * Setter for property anticiposProv.
     * @param anticiposProv New value of property anticiposProv.
     */
    public void setAnticiposProv(java.util.Vector anticiposProv) {
        this.anticiposProv = anticiposProv;
    }
    /**
     *Metodo que busca la lista de anticipos a aplicar en el despacho, que
     *son aplicados a un proveedor y que se aplicaron a una planilla.
     *@autor: Karen Reales
     *@param : Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchAnticiposProveedor(String planilla)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            st = crearPreparedStatement("SQL_BUSCAR_DESC_PROV_PLA");
            st.setString(1, planilla);
            rs = st.executeQuery();
            anticiposProv = new Vector();
            while(rs.next()){
                ant = new Anticipos();
                ant.setAnticipo_code(rs.getString("concept_code"));
                ant.setAnticipo_desc(rs.getString("concept_desc"));
                ant.setDstrct(rs.getString("dstrct"));
                ant.setTipo_s(rs.getString("tipo"));
                ant.setValor(rs.getFloat("vlr"));
                ant.setModif(rs.getString("modif"));
                ant.setCreation_date(rs.getString("Creation_date"));
                ant.setProveedor(rs.getString("proveedor_anticipo")+"/"+rs.getString("sucursal"));
                ant.setChequeado(!rs.getString("fecha_cheque").equals("0099-01-01 00:00:00"));
                anticiposProv.add(ant);
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR FORMANDO EL VECTOR DE LOS ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_BUSCAR_DESC_PROV_PLA");
        }
        
    }
   
     /**
     *Metodo que busca la lista de anticipos a aplicar en el despacho.
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void vecAnticipos(String base,String sj)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            //PRIMERO BUSCO EL STANDARD JOB PARA SABER SI ES UN STANDARD LOCAL.
            boolean isLocal=true;
            st = crearPreparedStatement("SQL_BUSCAR_STANDARD");
            st.setString(1, sj);
            rs = st.executeQuery();
            if(rs.next()){
                if(rs.getString("ISLOCAL").equals("TRUE")){
                    isLocal =false;
                }
            }
            //BUSCO EL STANDARD EN TBLDES
            int sw=0;
            st = crearPreparedStatement("SQL_BUSCAR_DESC");
            st.setString(1, sj);
            rs = st.executeQuery();
            anticipos = new Vector();
            while(rs.next()){
                sw=1;
                ant = new Anticipos();
                ant.setAnticipo_code(rs.getString("concept_code"));
                ant.setAnticipo_desc(rs.getString("concept_desc"));
                ant.setDstrct(rs.getString("dstrct"));
                ant.setTipo_s(rs.getString("type"));
                ant.setValor(rs.getFloat("vlr"));
                ant.setModif(rs.getString("modif"));
                ant.setMoneda(rs.getString("currency"));
                ant.setChequeado(true);
                if(rs.getString("concept_code").equals("ESTA "))
                    ant.setChequeado(isLocal);
                ant.setProveedor(rs.getString("proveedor"));
                anticipos.add(ant);
            }
            //SI NO ESTA EN TBLDES SE BUSCA GENERALMENTE.
            if(sw==0){
                st = crearPreparedStatement("SQL_BUSCAR_DESC_GEN");
                rs = st.executeQuery();
                anticipos = new Vector();
                while(rs.next()){
                    ant = new Anticipos();
                    ant.setAnticipo_code(rs.getString("concept_code"));
                    ant.setAnticipo_desc(rs.getString("concept_desc"));
                    ant.setDstrct(rs.getString("dstrct"));
                    ant.setTipo_s(rs.getString("tipo"));
                    ant.setValor(rs.getFloat("vlr"));
                    ant.setModif(rs.getString("modif"));
                    ant.setMoneda(rs.getString("currency"));
                    ant.setChequeado(true);
                    if(rs.getString("concept_code").equals("ESTA "))
                        ant.setChequeado(isLocal);
                    ant.setProveedor("");
                    anticipos.add(ant);
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR FORMANDO EL VECTOR DE LOS ANTICIPOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            this.desconectar("SQL_BUSCAR_STANDARD");
            this.desconectar("SQL_BUSCAR_DESC");
            this.desconectar("SQL_BUSCAR_DESC_GEN");
        }
        
    }
    
}
