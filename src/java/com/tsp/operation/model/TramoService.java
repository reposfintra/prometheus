/*
 * Nombre        TramoService.java
 * Autor         Ing. Jesus Cuestas
 * Modificado    Ing Sandra Escalante
 * Fecha         30 de junio de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;


public class TramoService{
    
    private TramoDAO tramo;
    
    //tmaturana 27.01.06
    private TreeMap origenes = new TreeMap();
    private TreeMap destinos = new TreeMap();
    
    /** Creates a new instance of TramoService */
    TramoService(){
        tramo = new TramoDAO();
    }
    
    /**
     * Setter for property tramo.
     * @param Ubicaciones New value of property tramo.
     */
    public void setTramo(Tramo t)throws SQLException{
        tramo.setTramo(t);
    }
    
    /**
     * Metodo <tt>update</tt>, actualiza la infomacion indicada de un tramo
     *  en el sistema
     * @see updateTramo()
     * @version : 1.0
     */
    public void update()throws SQLException{
        tramo.updateTramo();
    }
    
    /**
     * Getter for property tramo.
     * @return Value of property tramo.
     */
    public Tramo getTramo()throws SQLException{
        return tramo.getTramo();
    }
    
    /**
     * Metodo <tt>buscarTramo</tt>, obteniene una lista de tramos
     *  dado origen y destino
     * @see searchTramo ( origen, destino )
     * @version : 1.0
     */
    public void buscarTramo(String ori, String dest)throws SQLException{
        try{
            tramo.searchTramo(ori,dest);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Getter for property tramos.
     * @return Value of property tramos.
     */
    public java.util.Vector getTramos() {
        return tramo.getTramos();
    }
    
    /**
     * Setter for property tramos.
     * @param tramos New value of property tramos.
     */
    public void setTramos(java.util.Vector tramos) {
        tramo.setTramos(tramos);
    }
    
    /**
     * Metodo <tt>listo</tt>, obteniene una lista de tramos
     *  dado origen y destino
     * @see list ( origen, destino )
     * @version : 1.0
     */
    public void list(String o, String d)throws SQLException{
        tramo.list(o,d);
    }
    
    /**
     * Metodo <tt>agregarTramo</tt>, ingresa un nuevo tramo al sistema
     * @autor : Ing. Jesus Cuestas
     * @see agregarTramo ( )
     * @version : 1.0
     */
    public void agregarTramo()throws SQLException{
        tramo.agregarTramo();
    }
    
    /**
     * Metodo <tt>anularTramo</tt>, anula (reg_status=A) un tramo
     * @autor : Ing. Jesus Cuestas
     * @see anularTramo ( )
     * @version : 1.0
     */
    public void anularTramo()throws SQLException{
        tramo.anularTramo();
    }
    
    /**
     * Metodo <tt>listarTramos</tt>, obtiene la lista de los tramos registrados
     * en el sistema
     * @autor : Ing. Jesus Cuestas
     * @see listarTramos ( )
     * @version : 1.0
     */
    public void listarTramos()throws SQLException{
        tramo.listarTramos();
    }
    
    /**
     * Metodo <tt>buscarTramo</tt>, obtiene un tramo dado uno parametros
     * @autor : Ing. Jesus Cuestas
     * @see buscarTramo ( distrito, origen, destino )
     * @version : 1.0
     */
    public void buscarTramo(String cia, String origen, String destino)throws SQLException{
        tramo.buscarTramo(cia, origen, destino);
    }
    
    /**
     * Metodo <tt>modificarTramo</tt>, actualiza datos de un tramo
     * @autor : Ing. Jesus Cuestas
     * @see modificarTramo ()
     * @version : 1.0
     */
    public void modificarTramo()throws SQLException{
        tramo.modificarTramo();
    }
    
    /**
     * Metodo <tt>existeTramo</tt>, verifica la existencia de un tramo
     * @autor : Ing. Jesus Cuestas
     * @see existeTramo ( distrito, origen, destino )
     * @version : 1.0
     */
    public boolean existeTramo(String cia, String origen, String destino)throws SQLException{
        return tramo.existeTramo(cia, origen, destino);
    }
    
    /**
     * Metodo <tt>consultarTramo</tt>, obtiene la lista de los tramos dado unos
     * parametros
     * @autor : Ing. Jesus Cuestas
     * @see consultarTramo ( )
     * @version : 1.0
     */
    public void consultarTramo()throws SQLException{
        tramo.consultarTramo();
    }
    
    /**
     * Metodo <tt>listTramos</tt>, obtiene la lista de los tramos por distrito
     * @autor : Ing. Jesus Cuestas
     * @see listTramos ( distrito )
     * @version : 1.0
     */
    public void listTramos(String cia)throws SQLException{
        tramo.listTramos(cia);
    }
    
    /**
     * Metodo <tt>listarTramos</tt>, obtiene la lista de los tramos dado unos parametros
     * @autor : Ing. Jesus Cuestas
     * @see listTramos ( origen, distrito, mensaje )
     * @version : 1.0
     */
    public void listTramos(String origen,String cia,String mensaje)throws SQLException{
        tramo.listTramos(origen,cia,mensaje);
    }
    
    /**
     * Getter for property tramos.
     * @return Value of property tramos.
     */
    public java.util.TreeMap getLtramos() {
        return tramo.getLtramos();
    }
    
    public boolean getFlag() {
        return tramo.isFlag();
    }
    
    /**
     * Getter for property tramo.
     * @return Value of property tramo.
     */
    public Tramo getTr() {
        return tramo.getTr();
    }
    
    /**
     * Metodo <tt>listarOrigenes</tt>, obtiene la lista de los origenes
     * de los tramos registrados en el sistema
     * @autor : Ing. Tito Maturana
     * @see listarOrigenes ()
     * @version : 1.0
     */
    public TreeMap listarOrigenes() throws java.sql.SQLException{
        return this.tramo.listarOrigenes();
    }
    
    /**
     * Metodo <tt>listarDestinos</tt>, obtiene la lista de los destinos de los tramos
     *  dado un origen
     * @autor : Ing. Tito Maturana
     * @see listarDestinos ( origen )
     * @version : 1.0
     */
    public TreeMap listarDestinos(String orig) throws java.sql.SQLException{
        return this.tramo.listarDestinos(orig);
    }
    
    /**
     * Metodo <tt>obtenerTramo</tt>,  obtiene el origen y destino de un tramo     
     * @see searchTramo ( tramo )
     * @version : 1.0
     */
    public String obtenerTramo(String tr) throws java.sql.SQLException{
        String tramo = "";
        try{
            String orig = tr.charAt(0) + "" + tr.charAt(1);
            String dest = tr.charAt(2) + "" + tr.charAt(3);
            this.tramo.searchTramo(orig,  dest);
            tramo = this.tramo.getTramo().getOriginn() + " - " + this.tramo.getTramo().getDestinationn();
        }catch(Exception exc){
            tramo = "No Aplica.";
        }
        return tramo;
    }
    
    /**
     * Getter for property origines.
     * @return Value of property origines.
     */
    public java.util.TreeMap getOrigenes() {
        return origenes;
    }
    
    /**
     * Setter for property origines.
     * @param origines New value of property origines.
     */
    public void setOrigenes(java.util.TreeMap origenes) {
        this.origenes = origenes;
    }
    
    /**
     * Getter for property destinos.
     * @return Value of property destinos.
     */
    public java.util.TreeMap getDestinos() {
        return destinos;
    }
    
    /**
     * Setter for property destinos.
     * @param destinos New value of property destinos.
     */
    public void setDestinos(java.util.TreeMap destinos) {
        this.destinos = destinos;
    }
    
    /**
     * Carga la propiedad <code>origenes</code> con los c�digos y nombres
     * de los origenes en los posibles tramos.
     * @autor Ing. Tito Andr�s Maturana
     * @throws SQLException si ocurre una excepci�n en la conexi�n con la Base de Datos
     * @version 1.0
     */
    public void loadOrigenes() throws SQLException{
        this.origenes = this.listarOrigenes();
    }
    
    /**
     * Carga la propiedad <code>destinos</code> con los c�digos y nombres
     * de los destinos en los posibles tramos.
     * @autor Ing. Tito Andr�s Maturana
     * @param orig C�digo de la ciudad de origen.
     * @throws SQLException si ocurre una excepci�n en la conexi�n con la Base de Datos
     * @version 1.0
     */
    public void loadDestinos(String orig) throws SQLException{
        this.destinos = this.listarDestinos(orig);
    }
    
    /**
     * Metodo <tt>existeTramoAnulado</tt>, verifica la existencia de un tramo anulado     
     * @autor : Ing. Sandra Escalante
     * @see existeTramoAnulado ( distrito, origen, destino )
     * @version : 1.0
     */
    public boolean existeTramoAnulado(String cia, String origen, String destino)throws SQLException{
        return tramo.existeTramoAnulado(cia, origen, destino);
    }
      /**
     * Actualiza el origen o el destino de un tramo.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param nor C�digo de la nueva ciudad de origen.
     * @param ndes C�digo de la nueva ciudad destino.
     * @param or C�digo de la ciudad de origen.
     * @param des C�digo de la ciudad destino.
     * @throws SQLException si ocurre una excepci�n en la conexi�n con la Base de Datos
     * @version 1.0
     */
    public void updateTramoExtremo(String nor, String ndes, String or, String des)throws SQLException{
        this.tramo.updateTramoExtremo(nor, ndes, or, des);
    }
}





