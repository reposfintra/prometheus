/*
 * ProveedoresDAO.java
 *
 * Created on 19 de abril de 2005, 03:25 PM
 */

package com.tsp.operation.model;


import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  kreales
 */
public class ProveedoresDAO {
    
    private List proveeList;
    private Proveedores provee;
    private Vector proveedores;
    private Vector anticipos;
    private String dataBaseName;
    /** Creates a new instance of ProveedoresDAO */
    public ProveedoresDAO() {
    }
    public ProveedoresDAO(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
    public Vector getAnticipos(){
        return anticipos;
    }
    public List getProveedorACPM(){
        
        return proveeList;
    }
    public Vector getProveedores(){
        
        return proveedores;
    }
    public Proveedores getProveedore(){
        
        return provee;
    }
    public void setProveedores(Proveedores p){
        this.provee =p;
    }
    
    
    public void insertProveedor(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("insert into proveedores(dstrct, city_code, nit,sucursal, valor_servicio,  moneda, codigo_migracion, porcentaje,  creation_date,  creation_user , base) values (?,?,?,?,?,?,?,?,'now()',?,?)");
                st.setString(1, provee.getDstrct());
                st.setString(2, provee.getCity_code());
                st.setString(3, provee.getNit());
                st.setString(4, provee.getSucursal());
                st.setFloat(5, provee.getTotale());
                st.setString(6, provee.getMoneda());
                st.setString(7,provee.getmigracion());
                st.setFloat(8, provee.getPorcentaje());
                st.setString(9, provee.getCreation_user());
                st.setString(10,base);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LOS PROVEEDORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
    }
   
    public void updateProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("update proveedores set   city_code=?,   valor_servicio=?,  moneda=?, codigo_migracion=?, porcentaje=?,  last_update='now()',  user_update=? where dstrct=? and nit=? and sucursal=?");
                st.setString(1, provee.getCity_code());
                st.setFloat(2, provee.getTotale());
                st.setString(3, provee.getMoneda());
                st.setString(4,provee.getmigracion());
                st.setFloat(5, provee.getPorcentaje());
                st.setString(6, provee.getCreation_user());
                st.setString(7, provee.getDstrct());
                st.setString(8, provee.getNit());
                st.setString(9, provee.getSucursal());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LOS PROVEEDORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
    }
    
    public boolean existNit(String nit)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sql=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select * from nit where cedula =?");
                st.setString(1, nit);
                rs = st.executeQuery();
                if(rs.next()){
                    sql = true;
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE NIT " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        return sql;
        
    }
    public void anularProveedor()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("update proveedores set reg_status='A' where dstrct=? and nit_prov=? and sucursal_prov=?");
                st.setString(1, provee.getDstrct());
                st.setString(2, provee.getNit());
                st.setString(3, provee.getSucursal());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LOS PROVEEDORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
    }
    public void vecProveedor(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre from proveedores p, nit n where p.base =? and p.reg_status='' and n.cedula = p.nit");
                st.setString(1, base);
                rs = st.executeQuery();
                proveedores = new Vector();
                while(rs.next()){
                    provee = new Proveedores();
                    provee.setCantacpm(rs.getFloat("valor_servicio"));
                    provee.setNit(rs.getString("nit"));
                    provee.setNombre(rs.getString("nombre"));
                    provee.setSucursal(rs.getString("sucursal"));
                    provee.setMoneda(rs.getString("moneda"));
                    provee.setCity_code(rs.getString("city_code"));
                    provee.setmigracion(rs.getString("codigo_migracion"));
                    provee.setDstrct(rs.getString("dstrct"));
                    provee.setPorcentaje(rs.getFloat("porcentaje"));
                    proveedores.add(provee);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PROVEEDORES " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
    }
    
    public void searchProveedor(String nit, String sucursal)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        provee=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select p.*, n.nombre from proveedores p, nit n where p.reg_status='' and n.cedula = p.nit and p.nit = ? and p.sucursal = ?");
                st.setString(1, nit);
                st.setString(2, sucursal);
                ////System.out.println(""+st.toString());
                rs = st.executeQuery();
                if(rs.next()){
                    provee = new Proveedores();
                    provee.setTotale(rs.getFloat("valor_servicio"));
                    provee.setNit(rs.getString("nit"));
                    provee.setNombre(rs.getString("nombre"));
                    provee.setSucursal(rs.getString("sucursal"));
                    provee.setMoneda(rs.getString("moneda"));
                    provee.setCity_code(rs.getString("city_code"));
                    provee.setmigracion(rs.getString("codigo_migracion"));
                    provee.setDstrct(rs.getString("dstrct"));
                    provee.setPorcentaje(rs.getFloat("porcentaje"));
                    provee.setCreation_user(rs.getString("Creation_user"));
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
    }
    public void searchAnticipos(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        provee=null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("SELECT d.*, t.concept_desc,t.ind_application,t.codigo_migracion,coalesce(s.std_job_desc,'Ninguno')as std_job_desc FROM tbldes d  LEFT OUTER JOIN stdjob s  ON (d.std_job_no =s.std_job_no), tblcon t where d.base =? and d.concept_code = t.concept_code and d.reg_status=''");
                st.setString(1, base);
                rs = st.executeQuery();
                anticipos = new Vector();
                while(rs.next()){
                    Anticipos ant = new Anticipos();
                    ant.setAnticipo_code(rs.getString("concept_code"));
                    ant.setAnticipo_desc(rs.getString("concept_desc"));
                    ant.setDstrct(rs.getString("dstrct"));
                    ant.setTipo_s(rs.getString("type"));
                    ant.setSj_nombre(rs.getString("std_job_desc"));
                    ant.setSj(rs.getString("std_job_no"));
                    anticipos.add(ant);
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE ANTICIPOS DE UN PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
    }
    
    
    public boolean estaAnticipoProvee(String nit, String sucursal, String  codigo)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select * from anticipos_proveedor where nit_prov=? and sucursal_prov=? and anticipo_code=? and reg_status=''");
                st.setString(1,nit);
                st.setString(2,sucursal);
                st.setString(3,codigo);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE ANTICIPOS-PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        return sw;
        
    }
    
    public void listaAnticipoProvee(String  codigo)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("select a.*, n.nombre from anticipos_proveedor a, nit n where a.anticipo_code=? and a.reg_status='' and n.cedula = a.nit_prov");
                st.setString(1,codigo);
                rs = st.executeQuery();
                proveedores = new Vector();
                while(rs.next()){
                   provee = new Proveedores(); 
                   provee.setNit(rs.getString("nit_prov"));
                   provee.setSucursal(rs.getString("sucursal_prov"));
                   provee.setNombre(rs.getString("nombre"));
                   proveedores.add(provee);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA LISTA DE ANTICIPOS-PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
        
    }
    
    public void deleteAnticipoProvee(String  codigo, String nit, String sucursal)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("update anticipos_proveedor set reg_status='A' where anticipo_code=? and nit_prov =? and  sucursal_prov =?");
                st.setString(1,codigo);
                st.setString(2,nit);
                st.setString(3,sucursal);
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE ANTICIPOS-PROVEEDOR " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
        
    }
    
    
    
    
}
