/*
 * AnticiposService.java
 *
 * Created on 20 de abril de 2005, 10:35 AM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  kreales
 */
public class AnticiposService {
    
    AnticiposDAO ant;
    /** Creates a new instance of AnticiposService */
    public AnticiposService() {
        ant = new AnticiposDAO();
    }
    public AnticiposService(String dataBaseName) {
        ant = new AnticiposDAO(dataBaseName);
    }
    
    public void insertAnticipo(String base)throws SQLException{
        ant.insertAnticipo(base);
        
    }
    public void updateAnticipo()throws SQLException{
        ant.updateAnticipos();
    }
    public void anularAnticipo()throws SQLException{
        ant.anularAnticipo();
    }
    public void vecAnticipos(String base)throws SQLException{
        ant.vecAnticipos(base);
    }
    public void searchAnticipos(String dst, String codAnt, String sj)throws SQLException{
        ant.searchAnticipos(dst, codAnt,sj);
    }
    public void setAnticipos(Anticipos a){
        ant.setAnticipos(a);
    }
    public Vector getAnticipos(){
        return ant.getAnticipos();
    }
    public Anticipos getAnticipo(){
        return ant.getAnticipo();
    }
    public void insertProveedorAnticipos(String base)throws SQLException{
        ant.insertProveedorAnticipos(base);
        
    }
    public Proveedores getProveedore(){
        
        return ant.getProveedore();
    }
    public void setProveedores(Proveedores p){
        ant.setProveedores(p);
    }
    public void vecAnticipos(String base,String sj)throws SQLException{
        ant.vecAnticipos(base,sj);
    }
    /**
     * Getter for property anticiposProv.
     * @return Value of property anticiposProv.
     */
    public java.util.Vector getAnticiposProv() {
        return ant.getAnticiposProv();
    }
    
    /**
     * Setter for property anticiposProv.
     * @param anticiposProv New value of property anticiposProv.
     */
    public void setAnticiposProv(java.util.Vector anticiposProv) {
        ant.setAnticiposProv(anticiposProv);
    }
    
      /**
     *Metodo que busca la lista de anticipos a aplicar en el despacho, que
     *son aplicados a un proveedor.
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchAnticiposProveedor(Usuario u)throws SQLException{
        ant.searchAnticiposProveedor(u);
    }
    /**
     *Metodo que busca la lista de anticipos a aplicar en el despacho, que
     *son aplicados a un proveedor y que se aplicaron a una planilla.
     *@autor: Karen Reales
     *@param : Numero de la planilla
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void searchAnticiposProveedor(String planilla)throws SQLException{
        ant.searchAnticiposProveedor(planilla);
    }    
}
