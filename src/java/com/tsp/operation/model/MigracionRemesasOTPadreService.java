/*
 * MigracionRemesasAnuladasService.java
 *
 * Created on 21 de julio de 2005, 12:27 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  Henry
 */
public class MigracionRemesasOTPadreService {
    
    private MigracionRemesasOTPadreDAO dao;
    
    /** Creates a new instance of MigracionRemesasAnuladasService */
    public MigracionRemesasOTPadreService() {
    	dao = new MigracionRemesasOTPadreDAO();
    }
    public MigracionRemesasOTPadreService(String dataBaseName) {
    	dao = new MigracionRemesasOTPadreDAO(dataBaseName);
    }
    public Vector obtenerRemesasOTPadre() throws SQLException{    	 
    	this.dao.obtenerRemesasPadres(); 
        return dao.getRemesasPadres();        
    }    
        
}
