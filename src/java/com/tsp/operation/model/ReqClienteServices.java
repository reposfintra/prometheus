/*
 * ReqClienteServices.java
 *
 * Created on 27 de mayo de 2005, 04:47 PM
 */

//import Model.Services.*;

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  mcelin
 */
public class ReqClienteServices {
    
    private ReqClienteDao reqcliented;
    
    /** Creates a new instance of ReqClienteServices */
    public ReqClienteServices() {
        reqcliented = new ReqClienteDao();
    }
    //nueva
    public void EliminarReq_venN(String fechaI, String fechaF )throws SQLException{
        try{
            reqcliented.EliminarReq_venN(fechaI, fechaF);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public Vector listarPto_VentaA(String ano1, String mes1, int dia1, String ano2, String mes2, int dia2, String dstrct, String fin_proc  ) throws SQLException {
        try{
            return reqcliented.listarPto_VentaA( ano1, mes1, dia1, ano2, mes2, dia2, dstrct,fin_proc );
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public Vector listarPto_Venta(String ano1, String mes1, int dia1, String ano2, String mes2, int dia2, String dstrct ) throws SQLException {
        try{
            return reqcliented.listarPto_Venta( ano1, mes1, dia1, ano2, mes2, dia2, dstrct );
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public Vector listarPto_VentaAct( String dstrct,String fecI_pro, String fecF_pro  ) throws SQLException {
        try{
            return reqcliented.listarPto_VentaAct(dstrct, fecI_pro, fecF_pro );
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void EliminarReq_venAct(String fecha, String stdjob, String dstrct )throws SQLException {
        try{
            reqcliented.EliminarReq_venAct(fecha, stdjob, dstrct );
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
  
    public void EliminarReq_fronAct(String numpla, String stdjob )throws SQLException {
        try{
            reqcliented.EliminarReq_fronAct(numpla, stdjob );
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public String Buscar_agasocXcod( String codciu )throws SQLException{
        try{
            return reqcliented.Buscar_agasocXcod(codciu);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String Buscar_agasocXnom( String nomciu )throws SQLException{
        try{
            return reqcliented.Buscar_agasocXnom(nomciu);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String Buscar_tiporec( String trec, String dstrct)throws SQLException{
        try{
            return reqcliented.Buscar_tiporec(trec, dstrct);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String Buscar_Tiempo( String origen, String destino )throws SQLException{
        try{
            return reqcliented.Buscar_Tiempo(origen, destino);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public String Buscar_Fechadespacho( String numpla, String t )throws SQLException{
        try{
            return reqcliented.Buscar_Fechadespacho(numpla,t);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public Vector listarReqFrontera( String dstrct )throws SQLException{
        try{
            return reqcliented.listarReqFrontera( dstrct);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public Vector listarReqFronRegistrados(String fecini, String fecfin )throws SQLException{
        try{
            return reqcliented.listarReqFronRegistrados(fecini,fecfin);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public Vector listarReqFronAct(String fecini, String fecfin , String dstrct )throws SQLException{
        try{
            return reqcliented.listarReqFronAct(fecini,fecfin, dstrct);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    public void llenarReqcliente( ReqCliente reqcliente )throws SQLException{
        try{
            reqcliented.llenarReqcliente(reqcliente);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public void ModificarReqcliente( ReqCliente reqcliente )throws SQLException{
        try{
            reqcliented.ModificarReqcliente(reqcliente);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /////alpall
    /********************************** METODOS NUEVOS *************************/
    
    /**
     * Obtiene todos los requerimientos del cliente que no han sido asignados
     * @return Vector un vector con los requerimientos que aun no han sido asignados
     */
    public Vector obtenerRequerimientosNoAsignados() throws SQLException {
        return reqcliented.obtenerRequerimientosEnEstado("");
    }
    
    
    //alejo
    public ReqCliente obtenerRequerimiento( String dstrct_code, String num_sec, String std_job_no,
    String fecha_dispo, String numpla ) throws SQLException {
        return reqcliented.obtenerRequerimiento(dstrct_code, num_sec, std_job_no, fecha_dispo, numpla);
    }
    //Diogenes 16.11.05
    /**
     * Metodo viajeInternacional, busca el origen y destino en la tabla ciudad
     *       returna true si tienen pais diferente
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : codigo de origen, codigo destino
     * @see :
     * @version : 1.0
     */
    public boolean viajeInternacional( String origen, String destino )throws SQLException{
        try{
            return reqcliented.viajeInternacional(origen, destino);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo buscarFronteraAsoc, busca la frontera asociada al codigo de la ciudad
     *       returna el codigo del la  frontera
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : codigo de la ciudad
     * @see:
     * @version : 1.0
     */
    public String buscarFronteraAsoc( String codciu )throws SQLException{
        try{
            return reqcliented.buscarFronteraAsoc(codciu);
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo crearRequerimientoInternacional
     * @autor : Karen Reales
     * @param : Requerimiento a internacionalizar
     * @return: Vector de Requerimientos
     * @version : 1.0
     */
    
    public Vector crearRequerimientoInternacional(ReqCliente ri )throws SQLException{
        return reqcliented.crearRequerimientoInternacional(ri);
    }
    /**
     * Metodo actualizaRequerimientoInternacional
     * @autor : Karen Reales
     * @param : Requerimiento a internacionalizar
     * @return: Vector de Requerimientos
     * @version : 1.0
     */
    
    public void actualizarRequerimientoInternacional(ReqCliente requerimiento )throws SQLException{
        reqcliented.actualizarRequerimientoInternacional(requerimiento);
    }
    
    /**
     * Metodo  buscarTipoClaseRecurso, busca el tipo y la calse del recurso
     * @param: placa
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Hashtable buscarTipoClaseRecurso(String recurso) throws SQLException {
        try{
            return reqcliented.buscarTipoClaseRecurso(recurso);
        }catch (SQLException e){
            throw new SQLException(e.getMessage());
        }        
    }
    public void EliminarReq_fronN( )throws SQLException{
        try{
            reqcliented.EliminarReq_fronN();
        }
        catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo actualizarPlanillasPlaneacion, actuliza la planillas de planeacion
     * @autor : Diogenes Bastidas
     * @param : distrito 
     * @return:
     * @version : 1.0
     */
    
    public void actualizarPlanillasPlaneacion( String dstrct )throws SQLException{
        Vector vec = reqcliented.listarRemesasPlaneacion(dstrct);
        String planillas = "";
        try{
            for(int i=0; i< vec.size(); i++){
                ReqFrontera reqfron = (ReqFrontera) vec.get(i);
                if( reqcliented.existePlanillaDestinoFinal(reqfron.getnumrem(),reqfron.getdesrem()) ){
                    reqcliented.actualizarPlanillaPlaneacion(reqfron.getnumrem());
                }else{
                    reqcliented.buscarPlanillasAct(reqfron.getnumrem());
                }
            }
        }catch (SQLException e){
            throw new SQLException(e.getMessage());
        }
        
    }
    
}
