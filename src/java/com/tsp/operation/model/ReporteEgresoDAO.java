/*
 * ReporteEgresoDAO.java
 *
 * Created on 13 de septiembre de 2005, 10:15 AM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;
/**
 *
 * @author  Administrador
 */
public class ReporteEgresoDAO extends MainDAO{
    
    public static String SQLUPDATECXP      		 = " UPDATE fin.cxp_doc  SET  cheque = ?, user_update = ? WHERE cheque =  ? AND dstrct = ? AND proveedor = ? ;";// AND tipo_documento = ? AND documento = ? ;  ";
    
    public static String SQL_UPDATE_CORRIDA       	 = " UPDATE fin.corridas SET cheque = ?, user_update = ? WHERE cheque = ?  AND dstrct = ?  ;"; // AND tipo_documento = ? AND documento = ? ;  ";
    
    private static String SQLLIST = "   SELECT MV.DOCUMENT_NO AS NUMEROCHK,                                         "+
    "   P.OC AS PLANILLA,                                                                                           "+
    "   P.VLR AS VALORCHK,                                                                                          "+
    "   P.VLR_FOR AS VALORFOR,                                                                                      "+
    "   MV.PAYMENT_NAME AS BENEFICIARIO,                                                                            "+
    "   MV.FECHA_CHEQUE AS FECHACHK,                                                                                "+
    "   MV.CREATION_USER AS DESPACHADOR,                                                                            "+
    "   MV.BRANCH_CODE AS BANCO,                                                                                    "+
    "   MV.BANK_ACCOUNT_NO AS BANCOCUENTA,                                                                          "+
    "   MV.AGENCY_ID AS AGENCIA,                                                                                    "+
    "   MV.REG_STATUS,                                                                                              "+
    "   MV.NIT_PROVEEDOR,                                                                                           "+
    "   MV.FECHA_REPORTE,                                                                                           "+
    "   a.nommoneda AS MONEDA,                                                                                      "+
    "   get_nombrenit( MV.NIT_PROVEEDOR )  AS NOMBREP                                                               "+
    "   FROM                                                                                                        "+
    "   EGRESO MV                                                                                                   "+
    "       left outer join EGRESODET P on (MV.DOCUMENT_NO = P.DOCUMENT_NO and MV.BRANCH_CODE = P.BRANCH_CODE and   "+
    "       MV.BANK_ACCOUNT_NO=p.BANK_ACCOUNT_NO )                                                                  "+
    "       LEFT JOIN monedas a ON ( a.codmoneda = mv.currency )                                                    "+
    "   WHERE                                                                                                       "+
    "   AGENCY_ID = ? AND FECHA_CHEQUE BETWEEN ? AND ?                                                              "+
    "   AND MV.CREATION_USER LIKE ?                                                                                 "+
    "   AND MV.CONCEPT_CODE = '01'                                                                                  "+
    "   AND MV.BRANCH_CODE LIKE ?                                                                                   "+//AMATURANA
    "   AND MV.BANK_ACCOUNT_NO LIKE ?                                                                               "+//AMATURANA
    "   AND MV.NIT_PROVEEDOR LIKE ?                                                                                 "+//Jescandon
    "   ORDER BY MV.DOCUMENT_NO, MV.FECHA_CHEQUE     ";  


    public static String SQLSEARCHCAMBIOCHK =       "       SELECT * FROM CAMBIO_CHEQUE WHERE DOCUMENT_NO = ? AND new_document_no = ? ;";
   
    
    private static String SQLCHK   =	"       SELECT                                                                  "+
    "           E.DOCUMENT_NO  AS NUMEROCHK,                                        "+
    "           E.VLR          AS VALORCHK,                                         "+
    "           E.PAYMENT_NAME AS BENEFICIARIO,                                     "+
    "           E.FECHA_CHEQUE AS FECHACHK,                                         "+
    "           ED.TIPO_DOCUMENTO,                                                  "+
    "           E.nit_proveedor,                                                    "+
    "           E.DSTRCT                                                            "+
    "       FROM                                                                    "+
    "       EGRESO E                                                                "+
    "       LEFT JOIN EGRESODET ED ON ( ED.DSTRCT = E.DSTRCT AND ED.DOCUMENT_NO = E.DOCUMENT_NO AND ED.BRANCH_CODE = E.BRANCH_CODE AND ED.BANK_ACCOUNT_NO = E.BANK_ACCOUNT_NO  )   "+         
    "       WHERE E.FECHA_CHEQUE = ?                                                "+
    "    AND  E.DOCUMENT_NO <> ''                                                   "+
    "    AND  E.BRANCH_CODE = ?                                                     "+
    "    AND  E.BANK_ACCOUNT_NO = ?                                                 "+
    "   GROUP BY                                                                    "+
    "            E.DOCUMENT_NO,                                                     "+
    "            E.PAYMENT_NAME,                                                    "+
    "            E.FECHA_CHEQUE,                                                    "+
    "            E.VLR,                                                             "+
    "            ED.TIPO_DOCUMENTO,                                                 "+
    "            E.nit_proveedor,                                                   "+
    "            E.DSTRCT                                                           "+
    "    ORDER BY                                                                   "+
    "            E.DOCUMENT_NO,                                                     "+
    "            E.PAYMENT_NAME,                                                    "+
    "            E.FECHA_CHEQUE                                                     ";
    
    public static String SQLITEM =  "    SELECT        ED.DOCUMENT_NO AS NUMEROCHK,         "+
    "   ED.OC AS PLANILLA,                                  "+
    "   ED.VLR AS VALORCHK,                                 "+
    "   ED.ITEM_NO AS ITEM,                                 "+
    "   ED.CURRENCY AS MONEDA,                              "+
    "   ED.BRANCH_CODE AS BANCO,                            "+
    "   ED.BANK_ACCOUNT_NO AS SUCURSAL                      "+
    "   FROM                                                "+
    "   EGRESODET ED                                        "+
    "   WHERE                                               "+
    "   DOCUMENT_NO = ?                                     "+
    "   AND DOCUMENT_NO <> '' AND ITEM_NO <> ''     "+
    "   ORDER BY DOCUMENT_NO                                                ";
    
    
    public static String SQLUPDATEEGRESO = " UPDATE EGRESO SET DOCUMENT_NO = ? , LAST_UPDATE = 'now()' , USER_UPDATE = ? WHERE DOCUMENT_NO = ? AND branch_code = ? AND bank_account_no = ?; ";
    
    public static String SQLUPDATEEGRESODET = " UPDATE EGRESODET SET DOCUMENT_NO = ? , LAST_UPDATE = 'now()' , USER_UPDATE = ? WHERE DOCUMENT_NO = ? AND branch_code = ? AND bank_account_no = ?; ";
    
    public static String SQLUPDATEANLUACION = " UPDATE ANULACION_EGRESO SET DOCUMENT_NO = ? , LAST_UPDATE = 'now()' , USER_UPDATE = ? WHERE DOCUMENT_NO = ? AND branch_code = ? AND bank_account_no = ?; ";
    
    public static String SQLUPDATEMOVPPLA = " UPDATE MOVPLA SET DOCUMENT = ? , LAST_UPDATE = 'now()' , USER_UPDATE = ? WHERE DOCUMENT = ? AND branch_code = ? AND bank_account_no = ?;";
            
    
    private static String SQLBANCO  =   "   select 	branch_code,                        "+
    "   bank_account_no                             "+
    "   from movpla                                 "+
    "   where agency_id = ?                         "+
    "   and date_doc                                "+
    "   between ? and  ?                            "+
    "   and branch_code <> ''                       "+
    "   and bank_account_no <> ''                   "+
    "   and branch_code LIKE ?                       "+//AMATURANA
    "   and bank_account_no LIKE ?                   "+//AMATURANA
    "   group by branch_code ,                      "+
    "   bank_account_no                             ";
    
    private static String SQLLISTMIGRACION =        "       SELECT C.DSTRCT, C.BRANCH_CODE, C.BANK_ACCOUNT_NO,              "+
    "       C.USUARIO_MIGRACION, C.FECHA_MIGRACION, C.DOCUMENT_NO,          "+
    "       C.NEW_DOCUMENT_NO, MV.VLR, MV.AGENCY_ID, MV.PLANILLA  FROM    "+
    "       CAMBIO_CHEQUE C                                                 "+
    "       left outer join MOVPLA MV on (C.NEW_DOCUMENT_NO = MV.DOCUMENT)  "+
    "       WHERE C.FECHA_MIGRACION = '0099-01-01'                          ";
    
    private static String SQLUPDATEMIGRACION =      "       UPDATE CAMBIO_CHEQUE SET USUARIO_MIGRACION = ? ,                "+
    "       FECHA_MIGRACION = 'now()'                                       "+
    "       WHERE DOCUMENT_NO = ?                                           ";
    
    
    public static String SQLINSERTCAMBIOCHK =       "       INSERT INTO CAMBIO_CHEQUE                                                               "+
    "       ( BRANCH_CODE, BANK_ACCOUNT_NO, DOCUMENT_NO, NEW_DOCUMENT_NO, DSTRCT, CREATION_USER )   "+
    "       VALUES                                                                                  "+
    "       ( ?, ?, ?, ?, ?, ? );                                                                    ";
    
    public static String SQLUPDATECAMBIOCHK =       "       UPDATE                                                                                  "+
    "       CAMBIO_CHEQUE                                                                           "+
    "       SET  NEW_DOCUMENT_NO = ?, USER_UPDATE = ?, LAST_UPDATE = 'now()' "+
    "       WHERE NEW_DOCUMENT_NO = ? AND BRANCH_CODE = ? AND BANK_ACCOUNT_NO = ? AND  DSTRCT = ?;        ";
    
    public static String SQLBUSCAR  = " SELECT DOCUMENT_NO FROM EGRESO WHERE DOCUMENT_NO = ? AND DOCUMENT_NO NOT IN ( #datos# ) AND BRANCH_CODE = ? AND BANK_ACCOUNT_NO = ?; ";
    
    public static String SQLSEARCH  = " SELECT * FROM EGRESO WHERE DOCUMENT_NO = ?;  ";
    
    public static String SQLDELETECC = "    DELETE FROM CAMBIO_CHEQUE WHERE NEW_DOCUMENT_NO  = ?; ";
    
    /** Creates a new instance of ReporteEgresoDAO */
    public ReporteEgresoDAO() {
        super("ReporteEgresoDAO.xml");
    }
    public ReporteEgresoDAO(String dataBaseName) {
        super("ReporteEgresoDAO.xml", dataBaseName);
    }
    public void updateEgreso( String chk, String banco, String sucursal, String usuario ) throws Exception{
        PreparedStatement st = null;
        String query         = "SQL_UPDATEEGRESO";
        try{
            st = this.crearPreparedStatement(query);
            st.setString(1, usuario );
            st.setString(2, chk );
            st.setString(3, banco );
            st.setString(4, sucursal );
            
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina updateEgreso [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
    }





public List LISTREPORTEEGRESO( String agencia, String fechaInicial, String fechaFinal, String usuario, String banco, String sucu, String proveedor ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLLIST);
            st.setString(1, agencia);
            st.setString(2, fechaInicial);
            st.setString(3, fechaFinal);
            st.setString(4, usuario );
            st.setString(5, "%" + banco + "%");//AMATURANA
            st.setString(6, "%" + sucu + "%" );//AMATURANA
            st.setString(7, "%" + proveedor + "%" );//Jescandon
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(ReporteEgreso.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTREPORTEEGRESO [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return lista;
    }


 public List egresosAnulados( String dstrct, String banco, String sucursal, String fechainicio , String fechafin, String proveedor, String usuario, String agencia ) throws Exception{
     PreparedStatement st = null;
     ResultSet rs         = null;
     String query         = "SQL_EGRESOS_ANULADOS";
     List lista           = new LinkedList();
     try{
         st = this.crearPreparedStatement(query);
         st.setString(1, dstrct );
         st.setString(2, "%"+banco+"%" );
         st.setString(3, "%"+sucursal+"%" );
         st.setString(4, fechainicio );
         st.setString(5, fechafin );
         st.setString(6, agencia );
         st.setString(7, "%"+usuario+"%" );
         st.setString(8, "%"+proveedor+"%" );
         rs=st.executeQuery();
         if(rs!=null){
             while(rs.next()){
                 lista.add(ReporteEgreso.loadAnulados(rs));
             }
         }
         
     }catch(Exception e){
         throw new SQLException("Error en rutina egresosAnulados [ReporteEgresoDAO].... \n"+ e.getMessage());
     }
     finally{
         if(st!=null) st.close();
         this.desconectar(query);
     }
     return lista;
 }



    public String UPDATE( String ndoc,  String user, String doc, String banco, String sucursal, String distrito, String tipodoc, ReporteEgreso re ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        String sql = "";
        
        if (con == null)
            throw new SQLException("Sin conexion");
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        try{
            st = con.createStatement();
            
            
            /*ACTULIZACION REGISTROS*/
            
            ps = con.prepareStatement(this.SQLUPDATEANLUACION);
            ps.setString(1, ndoc+"A");
            ps.setString(2, user);
            ps.setString(3, doc);
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            
            sql += ps.toString();
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESO);
            ps.setString(1, ndoc+"A");
            ps.setString(2, user);
            ps.setString(3, doc);
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            
            sql += ps.toString();
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESODET);
            ps.setString(1, ndoc+"A");
            ps.setString(2, user);
            ps.setString(3, doc);
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            
            sql += ps.toString();
            
            //Tipo de dato <> 10 Actualiza en la tabla movpla
            if(!tipodoc.equals("010")){
                
                ps = con.prepareStatement(this.SQLUPDATEMOVPPLA);
                ps.setString(1, ndoc+"A");
                ps.setString(2, user);
                ps.setString(3, doc);
                ps.setString(4, banco);
                ps.setString(5, sucursal);
                
                sql += ps.toString();
                
            }
            
            //Actualiza en cxp
            else{
                
                ps = con.prepareStatement(this.SQLUPDATECXP );
                ps.setString(1, ndoc+"A");
                ps.setString(2, user);
                ps.setString(3, doc);
                ps.setString(4, re.getDistrito()  );
                ps.setString(5, re.getProveedor() );
                //ps.setString(6, re.getTipo_doc()  );
                //ps.setString(7, re.getDocumento() );
                
                sql += ps.toString();
                
                ps = con.prepareStatement(this.SQL_UPDATE_CORRIDA );
                ps.setString(1, ndoc+"A");
                ps.setString(2, user);
                ps.setString(3, doc);
                ps.setString(4, re.getDistrito()  );
                //ps.setString(5, re.getTipo_doc()  );
                //ps.setString(6, re.getDocumento() );
                sql += ps.toString();
                
            }
            
            
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
        return sql;
        
    }
    
    
    
    
    public String UPDATEREGA( String ndoc,  String user, String doc, String banco, String sucursal, String distrito, String tipodoc, ReporteEgreso re ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        String    sql        = "";
        try{
            st = con.createStatement();
            
            ps = con.prepareStatement(this.SQLUPDATEANLUACION);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            
            sql += ps.toString();
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESO);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            
            sql += ps.toString();
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESODET);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            
            sql += ps.toString();
            
            //Tipo de dato <> 10 Actualiza en la tabla movpla
            if(!tipodoc.equals("010")){
                
                ps = con.prepareStatement(this.SQLUPDATEMOVPPLA);
                ps.setString(1, ndoc);
                ps.setString(2, user);
                ps.setString(3, ndoc+"A");
                ps.setString(4, banco);
                ps.setString(5, sucursal);
                
                sql += ps.toString();
                
            }
            
            //Actualiza en la tabla cxp
            else{
                
                ps = con.prepareStatement(this.SQLUPDATECXP);
                ps.setString(1, ndoc);
                ps.setString(2, user);
                ps.setString(3, ndoc+"A");
                ps.setString(4, re.getDistrito()  );
                ps.setString(5, re.getProveedor() );
                //ps.setString(6, re.getTipo_doc()  );
               // ps.setString(7, re.getDocumento() );
                
                sql += ps.toString();
                
                ps = con.prepareStatement(this.SQL_UPDATE_CORRIDA);
                ps.setString(1, ndoc);
                ps.setString(2, user);
                ps.setString(3, ndoc+"A");
                ps.setString(4, re.getDistrito()  );
                //ps.setString(5, re.getTipo_doc()  );
                //ps.setString(6, re.getDocumento() );
                
                sql += ps.toString();                
                
            }
            
            /*CODIGO ACTUALIZACION EN MIGRACION CAMBIO CHEQUE*/
            if(!this.SEARCHCAMBIOCHK(doc, ndoc )){
                ps = con.prepareStatement(this.SQLINSERTCAMBIOCHK);
                ps.setString(1, banco);
                ps.setString(2, sucursal);
                ps.setString(3, doc);
                ps.setString(4, ndoc);
                ps.setString(5, distrito);
                ps.setString(6, user);
                
                sql += ps.toString();
            }
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
        return sql;
    }
    
    public List LISTREPORTEEGRESO( String agencia, String fechaInicial, String fechaFinal, String usuario ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLLIST);
            st.setString(1, agencia);
            st.setString(2, fechaInicial);
            st.setString(3, fechaFinal);
            st.setString(4, usuario );
            ////System.out.println("LISTA REPORTE EGRESO " + st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(ReporteEgreso.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTREPORTEEGRESO [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return lista;
    }
    
    public List LISTBANCO( String agencia, String fechaInicial, String fechaFinal ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLBANCO);
            st.setString(1, agencia);
            st.setString(2, fechaInicial);
            st.setString(3, fechaFinal);
            ////System.out.println("Lista Bancos " + st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Banco.loadReporteEgreso(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTBANCO [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return lista;
    }
    
    
    
    public List LISTCHK( String fechaInicial, String banco, String sucursal ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLCHK);
            st.setString(1, fechaInicial);
            st.setString(2, banco);
            st.setString(3, sucursal);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(ReporteEgreso.load2(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return lista;
    }
    
    public void UPDATE( String ndoc,  String user, String doc, String banco, String sucursal, String distrito ) throws Exception {
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        try{
            con.setAutoCommit(false);
            st = con.createStatement();
            
            
            /*ACTULIZACION REGISTROS*/
            
            ps = con.prepareStatement(this.SQLUPDATEANLUACION);
            ps.setString(1, ndoc+"A");
            ps.setString(2, user);
            ps.setString(3, doc);
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            //////System.out.println("ANULACION 1" + ps.toString());
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESO);
            ps.setString(1, ndoc+"A");
            ps.setString(2, user);
            ps.setString(3, doc);
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESODET);
            ps.setString(1, ndoc+"A");
            ps.setString(2, user);
            ps.setString(3, doc);
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEMOVPPLA);
            ps.setString(1, ndoc+"A");
            ps.setString(2, user);
            ps.setString(3, doc);
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            //////System.out.println("MOVPLA 1" + ps.toString());
            st.addBatch(ps.toString());
            
            //JEscandon Modificacion 06.01.2006
            
            ps = con.prepareStatement(this.SQLUPDATEANLUACION);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESO);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESODET);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEMOVPPLA);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            
            String docaux = doc.substring(0,doc.length()-1);
            
            
            
            ps = con.prepareStatement(this.SQLDELETECC);
            ps.setString(1, ndoc);
            
            st.addBatch(ps.toString());
            
            if (!this.SEARCHCAMBIOCHK(docaux)){
                
                ps = con.prepareStatement(this.SQLINSERTCAMBIOCHK);
                
                ps.setString(1, banco);
                ps.setString(2, sucursal);
                ps.setString(3, docaux);
                ps.setString(4, ndoc);
                ps.setString(5, distrito);
                ps.setString(6, user);
                st.addBatch(ps.toString());
                
            }else{
                ps = con.prepareStatement(this.SQLUPDATECAMBIOCHK);
                
                ps.setString(1, ndoc);
                ps.setString(2, user);
                ps.setString(3, docaux);
                ps.setString(4, banco);
                ps.setString(5, sucursal);
                ps.setString(6, distrito);
                
                st.addBatch(ps.toString());
            }
            
            
            
            st.executeBatch();
            
            con.commit();
            con.setAutoCommit(true);
            
        }catch(SQLException e){
            
            if(con!=null)
                con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
    }
    
    
    
    
    public void UPDATEREGA( String ndoc,  String user, String doc, String banco, String sucursal, String distrito ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        Statement st = null;
        PreparedStatement ps = null;
        ResultSet rs         = null;
        try{
            con.setAutoCommit(false);
            st = con.createStatement();
            
            
            ps = con.prepareStatement(this.SQLUPDATEANLUACION);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESO);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEEGRESODET);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            ps = con.prepareStatement(this.SQLUPDATEMOVPPLA);
            ps.setString(1, ndoc);
            ps.setString(2, user);
            ps.setString(3, ndoc+"A");
            ps.setString(4, banco);
            ps.setString(5, sucursal);
            st.addBatch(ps.toString());
            
            
            String docaux = doc.substring(0,doc.length()-1);
            
            
            ps = con.prepareStatement(this.SQLDELETECC);
            ps.setString(1, ndoc);
            
            st.addBatch(ps.toString());
            
            if (!this.SEARCHCAMBIOCHK(docaux)){
                
                ps = con.prepareStatement(this.SQLINSERTCAMBIOCHK);
                
                ps.setString(1, banco);
                ps.setString(2, sucursal);
                ps.setString(3, docaux);
                ps.setString(4, ndoc);
                ps.setString(5, distrito);
                ps.setString(6, user);
                st.addBatch(ps.toString());
                
            }else{
                ps = con.prepareStatement(this.SQLUPDATECAMBIOCHK);
                
                ps.setString(1, ndoc);
                ps.setString(2, user);
                ps.setString(3, docaux);
                ps.setString(4, banco);
                ps.setString(5, sucursal);
                ps.setString(6, distrito);
                
                st.addBatch(ps.toString());
            }
            
            st.executeBatch();
            
            con.commit();
            con.setAutoCommit(true);
            
        }catch(SQLException e){
            
            if(con!=null)
                con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
    }
    
    public void UPDATEEGRESO( String ndoc, String user, String doc ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.SQLUPDATEEGRESO);
            st.setString(1, ndoc);
            st.setString(2, user);
            st.setString(3, doc);
            ////System.out.println("UPDATE " + st);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
    }
    
    public void UPDATEEGRESODET( String ndoc, String user, String doc ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.SQLUPDATEEGRESODET);
            st.setString(1, ndoc);
            st.setString(2, user);
            st.setString(3, doc);
            ////System.out.println("UPDATE " + st);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
    }
    
    public void UPDATEANULACIOM( String ndoc,  String user, String doc ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.SQLUPDATEANLUACION);
            st.setString(1, ndoc);
            st.setString(2, user);
            st.setString(3, doc);
            ////System.out.println("UPDATE " + st);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
    }
    
    public void UPDATEMOVPLA( String ndoc,  String user, String doc ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.SQLUPDATEMOVPPLA);
            st.setString(1, ndoc);
            st.setString(2, user);
            st.setString(3, doc);
            ////System.out.println("UPDATE " + st);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
    }
    
    public boolean SEARCH( String doc, String listaO, String banco, String sucursal ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag = false;
        try{
            st = con.prepareStatement(this.SQLBUSCAR.replace("#datos#",listaO));
            st.setString(1, doc);
            st.setString(2, banco);
            st.setString(3, sucursal);
            //st.setString(2, listaO);
            rs = st.executeQuery();
            if(rs.next())
                flag = true;
            
            
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return flag;
    }
    
    public ReporteEgreso SEARCHRE( String doc ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        ReporteEgreso re = new ReporteEgreso();
        re.setFlag(false);
        try{
            st = con.prepareStatement(this.SQLSEARCH);
            st.setString(1, doc);
            rs = st.executeQuery();
            while(rs.next()){
                re.setFechaChk(rs.getString("FECHA_CHEQUE"));
                re.setFlag(true);
                break;
            }            
            
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return re;
    }
    
    public ReporteEgreso SearchItem( String cheque ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        ReporteEgreso re = new ReporteEgreso();
        try{
            st = con.prepareStatement(this.SQLITEM);
            st.setString(1, cheque);
            ////System.out.println("C " + st);
            rs=st.executeQuery();
            while(rs.next()){
                re = ReporteEgreso.loadItem(rs);
                break;
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return re;
    }
    
    
    public boolean SEARCHCAMBIOCHK( String cheque ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag = false;
        try{
            st = con.prepareStatement(this.SQLSEARCHCAMBIOCHK);
            st.setString(1, cheque);
            ////System.out.println("SOUT  " + st);
            rs = st.executeQuery();
            if(rs.next())
                flag = true;
            
            
        }catch(Exception e){
            throw new SQLException("Error en rutina SEARCHCAMBIOCHK  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return flag;
    }
    public List LISTCHKNOMIGRADOS() throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = this.crearPreparedStatement("SQL_MIGRACION_CHK");
            ////System.out.println("LISTCHKNOMIGRADOS " + st.toString());
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    Cambio_cheque chk = new Cambio_cheque();
                    chk = Cambio_cheque.load(rs);
                    chk = this.CHKMIGRADOS(chk);
                    lista.add(chk);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTCHKNOMIGRADOS [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_MIGRACION_CHK");
        }
        return lista;
    }
    
    
    /**
     * Funcion que los cheques que no han sido migrados
     * @autor  jescandon
     * @return Lista de cheques no migrados
     * @throws Exception.
     */
    public Cambio_cheque CHKMIGRADOS( Cambio_cheque chk ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = this.crearPreparedStatement("SQL_MIGRACION_CHK2");
            st.setString(1, chk.getDocument_no() );
            ////System.out.println("LISTCHKNOMIGRADOS2 " + st.toString());
            
            rs = st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    chk.setValorchk(rs.getDouble("VLR"));
                    chk.setAgencia(rs.getString("AGENCY_ID"));
                    chk.setPlanilla(rs.getString("PLANILLA"));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTCHKNOMIGRADOS2 [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("SQL_MIGRACION_CHK2");
        }
        return chk;
    }
    
    
    
    /**
     * Funcion que busca informaci�n complementaria de los cheques.
     * @autor  jescandon
     * @params Objeto Cambio_cheque
     * @return  Objeto Cambio_cheque
     * @throws Exception.
     */
    
    public void UPDATEMIGRACIONCHK( String user, String doc ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        try{
            st = con.prepareStatement(this.SQLUPDATEMIGRACION);
            st.setString(1, user);
            st.setString(2, doc);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina  UPDATEMIGRACIONCHK [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        
    }
    
    public void DELETE(String cheque ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection(this.getDatabaseName());
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLDELETECC);
            st.setString(1,  cheque);
            ////System.out.println("DELETE " + st);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETE [ReporteEgresoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection(this.getDatabaseName(),conPostgres);
        }
    }
    
    
    public boolean SEARCHCAMBIOCHK( String document_no , String new_document_no ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag = false;
        try{
            st = con.prepareStatement(this.SQLSEARCHCAMBIOCHK);
            st.setString(1, document_no);
            st.setString(2, new_document_no);
            rs = st.executeQuery();
            if(rs.next())
                flag = true;
            
            
        }catch(Exception e){
            throw new SQLException("Error en rutina SEARCHCAMBIOCHK  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return flag;
    }
    
    
        
    public List LISTBANCO( String agencia, String fechaInicial, String fechaFinal, String banco, String sucu ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLBANCO);
            st.setString(1, agencia);
            st.setString(2, fechaInicial);
            st.setString(3, fechaFinal);
            st.setString(4, "%" + banco + "%");//AMATURANA
            st.setString(5, "%" + sucu + "%");//AMATURANA
            ////System.out.println("Lista Bancos " + st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(Banco.loadReporteEgreso(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTBANCO [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return lista;
    }
    
    
    public List LISTREPORTEEGRESO( String agencia, String fechaInicial, String fechaFinal, String usuario, String banco, String sucu ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection(this.getDatabaseName());
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLLIST);
            st.setString(1, agencia);
            st.setString(2, fechaInicial);
            st.setString(3, fechaFinal);
            st.setString(4, usuario );
            st.setString(5, "%" + banco + "%");//AMATURANA
            st.setString(6, "%" + sucu + "%" );//AMATURANA
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(ReporteEgreso.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTREPORTEEGRESO [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection(this.getDatabaseName(),con);
        }
        return lista;
    }
    
    

   public boolean existe_Egreso( String banco, String sucursal, String documento ) throws Exception{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        List lista              = new LinkedList();
        Connection con          = null;
        String sql              = "SQL_SEARCH_CHK";
        try{
            
            String aux = this.obtenerSQL(sql);
            aux        = aux.replaceAll("#CHK#", documento );
            
            con = this.conectar(sql);
            st = con.prepareStatement(aux);
            
            st.setString(1, banco);
            st.setString(2, sucursal);
            rs  =   st.executeQuery();
            
            return rs.next();
            
        }catch(Exception e){
            throw new SQLException("Error en rutina existe_Egreso  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(sql);
        }
    }    
    
    public List list_Reenumeracion( String banco, String sucursal, String query ) throws Exception{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        List lista              = new LinkedList();
        Connection con          = null;
        String sql              = "SQL_LIST_REENUMERACION";
        try{
            String aux = this.obtenerSQL(sql);
            aux        = aux.replaceAll("#REPLACE#", query );

            con = this.conectar(sql);
            st = con.prepareStatement(aux);

            st.setString(1, banco);
            st.setString(2, sucursal);
            System.out.println("LSTA " + st );
            rs  =   st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(ReporteEgreso.load2(rs));
                }
            }

        }catch(Exception e){
            throw new SQLException("Error en rutina ListReenumeracion  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(sql);
        }
        return lista;
    }
    
    public boolean existe_Chk_RangoSerie( String rangoi, String rangof, String documento ) throws Exception{
        PreparedStatement st    = null;
        ResultSet rs            = null;
        List lista              = new LinkedList();
        String sql              = "SQL_RANGO";
        Connection con          = null;
        boolean flag            = false;
        try{
            
            String aux = this.obtenerSQL(sql);
            aux        = aux.replace("#RI#",     rangoi );
            aux        = aux.replace("#RF#",     rangof );
            aux        = aux.replace("#CHK#",    documento );
            
            con = this.conectar(sql);
            st  = con.prepareStatement(aux);
            
            rs  =   st.executeQuery();
            if( rs.next() ){
                flag    = rs.getBoolean(1);
            }
            
        }catch(Exception e){
            throw new SQLException("Error en rutina existe_RangoSerie  [ReporteEgresoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(sql);
        }
        return flag;
    }
    
    
      /**
     * Lista los bancos de una agencia
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param agencia C�digo de la agencia
     */     
    public List listBancoPorAgencia( String dstrct, String agencia, String fechaInicial, String fechaFinal, String usuario, String banco, String sucu, String proveedor ) throws Exception{
       PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LIST_BANCOS";
        List lista = new LinkedList();
        Connection con = null;
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
        try{
            String sql =  this.obtenerSQL(query);
            con = this.conectar(query);
            
            if( banco.length()!=0 && sucu.length()!=0 ){
                sql = sql.replaceAll("#BANCO", "AND MV.BRANCH_CODE = ?");
                sql = sql.replaceAll("#SUCURSAL", "AND MV.BANK_ACCOUNT_NO = ? ");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, banco);//AMATURANA
                st.setString(3, sucu);//AMATURANA
                st.setString(4, fechaInicial);
                st.setString(5, fechaFinal);
                st.setString(6, fechaInicial + " 00:00:00");
                st.setString(7, fechaFinal + " 23:59:59");
                st.setString(8, agencia + "%");
                st.setString(9, usuario );
                st.setString(10, "%" + proveedor + "%" );//Jescandon
            } else if( banco.length()!=0 ){
                sql = sql.replaceAll("#BANCO", "AND MV.BRANCH_CODE = ?");
                sql = sql.replaceAll("#SUCURSAL", "");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, banco);//AMATURANA
                st.setString(3, fechaInicial);
                st.setString(4, fechaFinal);
                st.setString(5, fechaInicial + " 00:00:00");
                st.setString(6, fechaFinal + " 23:59:59");
                st.setString(7, agencia + "%");
                st.setString(8, usuario );
                st.setString(9, "%" + proveedor + "%" );//Jescandon          
            } else {
                sql = sql.replaceAll("#BANCO", "");
                sql = sql.replaceAll("#SUCURSAL", "");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, fechaInicial);
                st.setString(3, fechaFinal);
                st.setString(4, fechaInicial + " 00:00:00");
                st.setString(5, fechaFinal + " 23:59:59");
                st.setString(6, agencia + "%");
                st.setString(7, usuario );
                st.setString(8, "%" + proveedor + "%" );//Jescandon              
            }
            
            logger.info("?listBancoPorAgencia: " + st);
            rs = st.executeQuery();
            
            if(rs!=null){
                while(rs.next()){
                    lista.add(Banco.loadReporteEgreso(rs));
                }
            }
        } catch(Exception e){
            throw new SQLException("Error en rutina listBancoPorAgencia [ReporteEgresoDAO].... \n"+ e.getMessage());
        } finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        
        return lista;
    }
    public List LISTREPORTEEGRESO( String dstrct, String agencia, String fechaInicial, String fechaFinal, String usuario, String banco, String sucu, String proveedor ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LIST";
        List lista = new LinkedList();
        Connection con = null;
        org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(this.getClass());
        
        try{
            String sql =  this.obtenerSQL(query);
            con = this.conectar(query);
            
            if( banco.length()!=0 && sucu.length()!=0 ){
                sql = sql.replaceAll("#BANCO", "AND MV.BRANCH_CODE = ?");
                sql = sql.replaceAll("#SUCURSAL", "AND MV.BANK_ACCOUNT_NO = ? ");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, banco);//AMATURANA
                st.setString(3, sucu);//AMATURANA
                st.setString(4, fechaInicial);
                st.setString(5, fechaFinal);
                st.setString(6, fechaInicial + " 00:00:00");
                st.setString(7, fechaFinal + " 23:59:59");
                st.setString(8, agencia + "%");
                st.setString(9, usuario );
                st.setString(10, "%" + proveedor + "%" );//Jescandon
            } else if( banco.length()!=0 ){
                sql = sql.replaceAll("#BANCO", "AND MV.BRANCH_CODE = ?");
                sql = sql.replaceAll("#SUCURSAL", "");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, banco);//AMATURANA
                st.setString(3, fechaInicial);
                st.setString(4, fechaFinal);
                st.setString(5, fechaInicial + " 00:00:00");
                st.setString(6, fechaFinal + " 23:59:59");
                st.setString(7, agencia + "%");
                st.setString(8, usuario );
                st.setString(9, "%" + proveedor + "%" );//Jescandon          
            } else {
                sql = sql.replaceAll("#BANCO", "");
                sql = sql.replaceAll("#SUCURSAL", "");
                st = con.prepareStatement(sql);
                st.setString(1, dstrct);
                st.setString(2, fechaInicial);
                st.setString(3, fechaFinal);
                st.setString(4, fechaInicial + " 00:00:00");
                st.setString(5, fechaFinal + " 23:59:59");
                st.setString(6, agencia + "%");
                st.setString(7, usuario );
                st.setString(8, "%" + proveedor + "%" );//Jescandon              
            }
            
            logger.info("?LISTREPORTEEGRESO: " + st);
            rs = st.executeQuery();
            
            if(rs!=null){
                while(rs.next()){
                    String fecha_anul = rs.getString("fecha_anulo");
                    String fecha_cheque = rs.getString("FECHACHK");
                    
                    logger.info("? fecha_anul: " + fecha_anul + ", fecha_cheque: " + fecha_cheque + ", fechaI: " + fechaInicial.replaceAll("-", ""));
                    if( !fecha_anul.equals("0099-01-01") 
                        && Integer.parseInt(fecha_cheque.replaceAll("-", "")) < Integer.parseInt(fechaInicial.replaceAll("-", "")) 
                        && Integer.parseInt(fecha_anul) >= Integer.parseInt(fechaInicial.replaceAll("-", "")) ){
                            lista.add(ReporteEgreso.loadAnulados(rs));
                    } else {
                        lista.add(ReporteEgreso.load(rs));
                        if( rs.getString("REG_STATUS").equals("A") ){
                            lista.add(ReporteEgreso.loadAnulados(rs));
                        }
                    }
                }
            }
        } catch(Exception e){
            e.printStackTrace();
            throw new SQLException("Error en rutina LISTREPORTEEGRESO [ReporteEgresoDAO].... \n"+ e.getMessage());
        } finally{
            if(st!=null) st.close();
            this.desconectar(query);
        }
        
        return lista;
    }
}

//entregado a tito 14 Marzo 2007
