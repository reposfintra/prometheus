/*
 * peajeService.java
 *
 * Created on 3 de diciembre de 2004, 05:57 PM
 */

package com.tsp.operation.model;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  KREALES
 */
public class AutorizacionFleteService {
    
    private AutorizacionFleteDAO autfleteDAO;
    
    /** Creates a new instance of peajeService */
    public AutorizacionFleteService() {
        autfleteDAO= new AutorizacionFleteDAO();
    }
    public com.tsp.operation.model.beans.AutorizacionFlete getAflete() {
        return autfleteDAO.getAflete();
    }
    
    /**
     * Setter for property aflete.
     * @param aflete New value of property aflete.
     */
    public void setAflete(com.tsp.operation.model.beans.AutorizacionFlete aflete) {
        autfleteDAO.setAflete(aflete);
    }
    public String insert()throws SQLException{
        return autfleteDAO.insert();
    }
    public String insertSendMail()throws SQLException{
        return autfleteDAO.insertSendMail();
    }
    public void selecionar(String planilla)throws SQLException{
        autfleteDAO.selecionar(planilla);
    }
    public String getMaxPlanilla()throws SQLException{
        return autfleteDAO.getMaxPlanilla();
    }
    public String getSolicitud()throws SQLException{
        return autfleteDAO.getSolicitud();
    }
    public void cumplirSolicitud(String solicitud)throws SQLException{
        autfleteDAO.cumplirSolicitud(solicitud);
    }
     public boolean estaCumplida(String solicitud)throws SQLException{
         return autfleteDAO.estaCumplida(solicitud);
     }
     public void insertControlActualizacion(String comando,String usuario)throws SQLException{
         autfleteDAO.insertControlActualizacion(comando, usuario);
     }
}
