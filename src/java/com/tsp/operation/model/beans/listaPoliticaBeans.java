/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author geotech
 */
public class listaPoliticaBeans {

    private int idPolitica;
    private String politica;
    private String centralRiesgo;
    private String convenio;
    private int idConvenio;
    private int edadIni;
    private int edadFin;
    private double montoIni;
    private double montoFin;
    private boolean monto;
    private boolean edad;
    
    
    public listaPoliticaBeans() {
    }

    /**
     * @return the idPolitica
     */
    public int getIdPolitica() {
        return idPolitica;
    }

    /**
     * @param idPolitica the idPolitica to set
     */
    public void setIdPolitica(int idPolitica) {
        this.idPolitica = idPolitica;
    }

    /**
     * @return the politica
     */
    public String getPolitica() {
        return politica;
    }

    /**
     * @param politica the politica to set
     */
    public void setPolitica(String politica) {
        this.politica = politica;
    }


    /**
     * @return the centralRiesgo
     */
    public String getCentralRiesgo() {
        return centralRiesgo;
    }

    /**
     * @param centralRiesgo the centralRiesgo to set
     */
    public void setCentralRiesgo(String centralRiesgo) {
        this.centralRiesgo = centralRiesgo;
    }

    /**
     * @return the convenio
     */
    public String getConvenio() {
        return convenio;
    }

    /**
     * @param convenio the convenio to set
     */
    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    /**
     * @return the idConvenio
     */
    public int getIdConvenio() {
        return idConvenio;
    }

    /**
     * @param idConvenio the idConvenio to set
     */
    public void setIdConvenio(int idConvenio) {
        this.idConvenio = idConvenio;
    }

    /**
     * @return the edadIni
     */
    public int getEdadIni() {
        return edadIni;
    }

    /**
     * @param edadIni the edadIni to set
     */
    public void setEdadIni(int edadIni) {
        this.edadIni = edadIni;
    }

    /**
     * @return the edadFin
     */
    public int getEdadFin() {
        return edadFin;
    }

    /**
     * @param edadFin the edadFin to set
     */
    public void setEdadFin(int edadFin) {
        this.edadFin = edadFin;
    }

    /**
     * @return the montoIni
     */
    public double getMontoIni() {
        return montoIni;
    }

    /**
     * @param montoIni the montoIni to set
     */
    public void setMontoIni(double montoIni) {
        this.montoIni = montoIni;
    }

    /**
     * @return the montoFin
     */
    public double getMontoFin() {
        return montoFin;
    }

    /**
     * @param montoFin the montoFin to set
     */
    public void setMontoFin(double montoFin) {
        this.montoFin = montoFin;
    }

    /**
     * @return the monto
     */
    public boolean isMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(boolean monto) {
        this.monto = monto;
    }

    /**
     * @return the edad
     */
    public boolean isEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(boolean edad) {
        this.edad = edad;
    }




}
