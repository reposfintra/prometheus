/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;



/**
 *
 * @author Alvaro
 */
public class EstadoFinanciero implements Serializable {

    private double secuencia;
    private String indicador_tercero;
    private String tipo_registro;
    private String descripcion;
    private String cuenta;
    private String tercero;
    private String formula;


   public void setEstado(Double secuencia,String indicador_tercero,String tipo_registro,
                         String descripcion, String cuenta,String tercero, String formula){

       this.secuencia = secuencia;
       this.indicador_tercero = indicador_tercero;
       this.tipo_registro = tipo_registro;
       this.descripcion = descripcion;
       this.cuenta = cuenta;
       this.tercero = tercero;
       this.formula = formula;

   }

    /**
     * @return the secuencia
     */
    public double getSecuencia() {
        return secuencia;
    }

    /**
     * @param secuencia the secuencia to set
     */
    public void setSecuencia(double secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * @return the indicador_tercero
     */
    public String getIndicador_tercero() {
        return indicador_tercero;
    }

    /**
     * @param indicador_tercero the indicador_tercero to set
     */
    public void setIndicador_tercero(String indicador_tercero) {
        this.indicador_tercero = indicador_tercero;
    }

    /**
     * @return the tipo_registro
     */
    public String getTipo_registro() {
        return tipo_registro;
    }

    /**
     * @param tipo_registro the tipo_registro to set
     */
    public void setTipo_registro(String tipo_registro) {
        this.tipo_registro = tipo_registro;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the tercero
     */
    public String getTercero() {
        return tercero;
    }

    /**
     * @param tercero the tercero to set
     */
    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    /**
     * @return the formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * @param formula the formula to set
     */
    public void setFormula(String formula) {
        this.formula = formula;
    }


}
