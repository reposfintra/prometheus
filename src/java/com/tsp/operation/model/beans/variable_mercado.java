/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author user
 */
public class variable_mercado {
    private int id;
    private String codigo;
    private String descripcion;
    private String ciudad;
    private String codCentralRiesgo;    
    private int obligatorio;
    private int tipo_dato;
    private String nombre;  

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    

    public int getTipo_dato() {
        return tipo_dato;
    }

    public void setTipo_dato(int tipo_dato) {
        this.tipo_dato = tipo_dato;
    }

        
    public int getObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(int obligatorio) {
        this.obligatorio = obligatorio;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the codCentralRiesgo
     */
    public String getCodCentralRiesgo() {
        return codCentralRiesgo;
    }

    /**
     * @param codCentralRiesgo the codCentralRiesgo to set
     */
    public void setCodCentralRiesgo(String codCentralRiesgo) {
        this.codCentralRiesgo = codCentralRiesgo;
    }
}
