 /***************************************
    * Nombre Clase ............. Programa.java
    * Descripci�n  .. . . . . .  Encapsula datos de programas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  14/12/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.beans;


import java.util.*;


public class Programa {
   
    private String distrito;
    private String programa;
    private String descripcion;
    private String programador;
    private String equipo;
    private String sitio; 
    private String fecha;
    private String usuario;
    private List   listArchivos;  
    private List   listRevisiones;  

    
    public Programa() {
         reset();  
    }
    
    
    
    /**
     * M�todos que setea las variables
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void reset(){
        listArchivos   = new LinkedList();
        listRevisiones = new LinkedList();
        String vacio   = "";
        distrito       = vacio;
        programa       = vacio;
        descripcion    = vacio;
        programador    = vacio;
        equipo         = vacio;
        sitio          = vacio; 
        fecha          = vacio;
        usuario        = vacio;
    }
    
    
    
    
    // SET :
    /**
     * M�todos que guardan valores
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    
    
    
   
    /**
     * M�todos que setea el valor del programa
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setPrograma    (String val){
        this.programa    = val;
    }   
    
    
    
    
    /**
     * M�todos que setea el valor del programa
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setDistrito    (String val){
        this.distrito   = val;
    }   
    
    
    
    
    /**
     * M�todos que setea el valor de la descripcion del programa
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/
    public void setDescripcion (String val){
        this.descripcion = val;
    }
    
    /**
     * M�todos que setea el valor del programador
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/
    public void setProgramador(String val){
        this.programador  = val;
    }
    
    
    /**
     * M�todos que setea el valor del equipo
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/
    
    public void setEquipo(String val){
        this.equipo = val;
    }
    
    
    /**
     * M�todos que setea el valor del sitio
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/
    public void setSitio(String val){
        this.sitio  = val;
    }
    
    /**
     * M�todos que setea el valor de la fecha
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/
    public void setFecha(String val){
        this.fecha  = val;
    }
    
    /**
     * M�todos que setea el valor del usuario
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public void setUser(String val){
        this.usuario  = val;
    }
    
    
    /**
     * M�todos que setea la lista de archivos
     * @autor.......fvillacob 
     * @parameter ... List val
     * @version.....1.0.     
     **/
    public void setListFile(List val){
        this.listArchivos   = val;
    }
    
    
    /**
     * M�todos que setea la lista de revisiones
     * @autor.......fvillacob 
     * @parameter ... List val
     * @version.....1.0.     
     **/
    public void setListRevisiones(List val){
        this.listRevisiones  = val;
    }
    
    
        
    
    
    
    
    // GET :
    /**
     * M�todos que devuelven valores
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    
    
    
     /**
     * M�todos que devuelve el valor del programa
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getPrograma    (){
        return this.programa    ;
    }   
    
    
    
    /**
     * M�todos que devuelve el valor del distrito
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getDistrito  (){
        return this.distrito ;
    }   
    
    
    
    
    /**
     * M�todos que devuelve el valor de la descripcion
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getDescripcion (){
        return this.descripcion ;
    }
    
    /**
     * M�todos que devuelve el valor del programador
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getProgramador(){
        return this.programador  ;
    }
    
    
    /**
     * M�todos que devuelve el valor del equipo
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/    
    public String getEquipo(){
        return this.equipo ;
    }
    
    
    /**
     * M�todos que devuelve el valor del sitio
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getSitio(){
        return this.sitio  ;
    }
    
    
    /**
     * M�todos que devuelve el valor de la fecha
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getFecha(){
        return this.fecha  ;
    }
    
    
    /**
     * M�todos que devuelve el valor del usuario
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getUser(){
        return this.usuario  ;
    }
    
    
    /**
     * M�todos que devuelve la lista de files
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public List getListFile(){
       return this.listArchivos  ;
    }
    
    
    /**
     * M�todos que devuelve la lista de revisiones
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public List getListRevisiones(){
       return  this.listRevisiones  ;
    }
    
    
    
}
