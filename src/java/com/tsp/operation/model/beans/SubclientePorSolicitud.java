/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;


import java.text.*;



/**
 *
 * @author Alvaro
 */
public class SubclientePorSolicitud {

    private String id_cliente;
    private String id_solicitud;
    private int    parcial;
    private String num_os;
    private String id_subcliente;
    private String nombre;
    private String nic;
    private String nit;
    private double porc_cuota_inicial;
    private String estudio_economico;
    private String esoficial;

    private double intereses;                   // Inicializado en cero y despues se actualiza
    private double valor_con_financiacion;      // Inicializado en cero y despues se actualiza
    private double valor_cuota;                 // Inicializado en cero y despues se actualiza
    private int    periodo;
    private String fecha_financiacion;

    private double puntos;                      // Se actualiza con objeto PuntosFinanciacion
    private String tipo;                        // Se actualiza con objeto PuntosFinanciacion
    private String clase_dtf;                   // Se actualiza con objeto PuntosFinanciacion

    private double dtf_semana;
    private double porcentaje_interes;


    private double valor_sin_iva;                                               // valor solicitud sin iva
    private double porcentaje_base;                                             // porcentaje distribucion para valor solicitud
    private double subtotal_base_1;                                             // subtotal base sin_iva;
    private int    meses_incremento;                                            // meses mora;
    private double porcentaje_incremento;                                       // porcentaje mora;
    private double porcentaje_extemporaneo;                                     // porcentaje final mora;
    private double valor_extemporaneo_1;                                        // valor mora de subtotal base sin iva;
    private double subtotal_base_2;                                             // subtotal base con valor mora sin iva;
    private double valor_cuota_inicial;
    private double subtotal_base_3;                                             // sutotal base con valor mora sin iva sin cuota inicial;

    private double valor_base_iva;                                              // valor del iva
    private double valor_iva;                                                   // valor iva distribuido por subcliente
    private double valor_extemporaneo_2;                                        // valor mora del iva
    private double subtotal_iva;                                                // valor iva con mora;
    private double subtotal_a_financiar;
    private double valor_extemporaneo;                                          // valor total mora;

    private String prefacturar;
    private String simbolo_variable;
    private String observacion;
    private String fecha_factura;





    /** Creates a new instance of OfertaEca */
    public SubclientePorSolicitud() {
    }


    public static SubclientePorSolicitud load(java.sql.ResultSet rs)throws java.sql.SQLException{

        SubclientePorSolicitud subclientePorSolicitud = new SubclientePorSolicitud();

        subclientePorSolicitud.setId_cliente(rs.getString("id_cliente") );
        subclientePorSolicitud.setId_solicitud( rs.getString("id_solicitud") );
        subclientePorSolicitud.setParcial( rs.getInt("parcial") );
        subclientePorSolicitud.setNum_os( rs.getString("num_os") );
        subclientePorSolicitud.setId_subcliente(rs.getString("id_subcliente") );
        subclientePorSolicitud.setNombre(rs.getString("nombre") );
        subclientePorSolicitud.setNic(rs.getString("nic") );
        subclientePorSolicitud.setNit(rs.getString("nit") );
        subclientePorSolicitud.setPorc_cuota_inicial(rs.getDouble("porc_cuota_inicial") );
        subclientePorSolicitud.setValor_cuota_inicial(rs.getDouble("valor_cuota_inicial") );
        subclientePorSolicitud.setPeriodo(rs.getInt("periodo") );
        subclientePorSolicitud.setEstudio_economico(rs.getString("estudio_economico") );
        subclientePorSolicitud.setPorcentaje_base(rs.getDouble("porcentaje_base") );
        subclientePorSolicitud.setEsoficial(rs.getString("esoficial") );
        subclientePorSolicitud.setFecha_financiacion(rs.getString("fecha_financiacion").substring(0, 10) );
        subclientePorSolicitud.setMeses_incremento( rs.getInt("meses_incremento") );

        subclientePorSolicitud.setValor_sin_iva(0.00);
        subclientePorSolicitud.setSubtotal_base_1(0.00);
        subclientePorSolicitud.setPorcentaje_incremento(0.00);
        subclientePorSolicitud.setPorcentaje_extemporaneo(0.00);
        subclientePorSolicitud.setValor_extemporaneo_1(0.00);
        subclientePorSolicitud.setSubtotal_base_2(0.00);
        subclientePorSolicitud.setSubtotal_base_3(0.00);


        subclientePorSolicitud.setValor_base_iva(0.00);
        subclientePorSolicitud.setValor_iva(0.00);
        subclientePorSolicitud.setValor_extemporaneo_2(0.00);
        subclientePorSolicitud.setSubtotal_iva(0.00);

        subclientePorSolicitud.setSubtotal_a_financiar(0.00);
        subclientePorSolicitud.setValor_extemporaneo(0.00);

        subclientePorSolicitud.setIntereses(0.00);
        subclientePorSolicitud.setValor_con_financiacion(0.00);
        subclientePorSolicitud.setValor_cuota(0.00);

        subclientePorSolicitud.setPuntos(0.00);
        subclientePorSolicitud.setTipo("");
        subclientePorSolicitud.setClase_dtf("");
        subclientePorSolicitud.setDtf_semana(rs.getDouble("dtf_semana"));
        subclientePorSolicitud.setPorcentaje_interes(0.00);
        subclientePorSolicitud.setPrefacturar("");
        subclientePorSolicitud.setSimbolo_variable(rs.getString("simbolo_variable"));
        subclientePorSolicitud.setObservacion(rs.getString("observacion"));

        if (rs.getString("fecha_factura").equalsIgnoreCase("0099-01-01") ) {
            java.util.Date date = new java.util.Date();
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
            String fecha_hoy = sdf.format(date);
            subclientePorSolicitud.setFecha_factura(fecha_hoy);
        }
        else {
            subclientePorSolicitud.setObservacion(rs.getString("fecha_factura"));
        }


        return subclientePorSolicitud;

    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the id_subcliente
     */
    public String getId_subcliente() {
        return id_subcliente;
    }

    /**
     * @param id_subcliente the id_subcliente to set
     */
    public void setId_subcliente(String id_subcliente) {
        this.id_subcliente = id_subcliente;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nic
     */
    public String getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(String nic) {
        this.nic = nic;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the porc_cuota_inicial
     */
    public double getPorc_cuota_inicial() {
        return porc_cuota_inicial;
    }

    /**
     * @param porc_cuota_inicial the porc_cuota_inicial to set
     */
    public void setPorc_cuota_inicial(double porc_cuota_inicial) {
        this.porc_cuota_inicial = porc_cuota_inicial;
    }

    /**
     * @return the estudio_economico
     */
    public String getEstudio_economico() {
        return estudio_economico;
    }

    /**
     * @param estudio_economico the estudio_economico to set
     */
    public void setEstudio_economico(String estudio_economico) {
        this.estudio_economico = estudio_economico;
    }

    /**
     * @return the esoficial
     */
    public String getEsoficial() {
        return esoficial;
    }

    /**
     * @param esoficial the esoficial to set
     */
    public void setEsoficial(String esoficial) {
        this.esoficial = esoficial;
    }

    /**
     * @return the intereses
     */
    public double getIntereses() {
        return intereses;
    }

    /**
     * @param intereses the intereses to set
     */
    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }

    /**
     * @return the valor_con_financiacion
     */
    public double getValor_con_financiacion() {
        return valor_con_financiacion;
    }

    /**
     * @param valor_con_financiacion the valor_con_financiacion to set
     */
    public void setValor_con_financiacion(double valor_con_financiacion) {
        this.valor_con_financiacion = valor_con_financiacion;
    }

    /**
     * @return the valor_cuota
     */
    public double getValor_cuota() {
        return valor_cuota;
    }

    /**
     * @param valor_cuota the valor_cuota to set
     */
    public void setValor_cuota(double valor_cuota) {
        this.valor_cuota = valor_cuota;
    }

    /**
     * @return the periodo
     */
    public int getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the fecha_financiacion
     */
    public String getFecha_financiacion() {
        return fecha_financiacion;
    }

    /**
     * @param fecha_financiacion the fecha_financiacion to set
     */
    public void setFecha_financiacion(String fecha_financiacion) {
        this.fecha_financiacion = fecha_financiacion;
    }

    /**
     * @return the puntos
     */
    public double getPuntos() {
        return puntos;
    }

    /**
     * @param puntos the puntos to set
     */
    public void setPuntos(double puntos) {
        this.puntos = puntos;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the clase_dtf
     */
    public String getClase_dtf() {
        return clase_dtf;
    }

    /**
     * @param clase_dtf the clase_dtf to set
     */
    public void setClase_dtf(String clase_dtf) {
        this.clase_dtf = clase_dtf;
    }

    /**
     * @return the valor_sin_iva
     */
    public double getValor_sin_iva() {
        return valor_sin_iva;
    }

    /**
     * @param valor_sin_iva the valor_sin_iva to set
     */
    public void setValor_sin_iva(double valor_sin_iva) {
        this.valor_sin_iva = valor_sin_iva;
    }

    /**
     * @return the porcentaje_base
     */
    public double getPorcentaje_base() {
        return porcentaje_base;
    }

    /**
     * @param porcentaje_base the porcentaje_base to set
     */
    public void setPorcentaje_base(double porcentaje_base) {
        this.porcentaje_base = porcentaje_base;
    }

    /**
     * @return the subtotal_base_1
     */
    public double getSubtotal_base_1() {
        return subtotal_base_1;
    }

    /**
     * @param subtotal_base_1 the subtotal_base_1 to set
     */
    public void setSubtotal_base_1(double subtotal_base_1) {
        this.subtotal_base_1 = subtotal_base_1;
    }

    /**
     * @return the meses_incremento
     */
    public int getMeses_incremento() {
        return meses_incremento;
    }

    /**
     * @param meses_incremento the meses_incremento to set
     */
    public void setMeses_incremento(int meses_incremento) {
        this.meses_incremento = meses_incremento;
    }

    /**
     * @return the porcentaje_incremento
     */
    public double getPorcentaje_incremento() {
        return porcentaje_incremento;
    }

    /**
     * @param porcentaje_incremento the porcentaje_incremento to set
     */
    public void setPorcentaje_incremento(double porcentaje_incremento) {
        this.porcentaje_incremento = porcentaje_incremento;
    }

    /**
     * @return the porcentaje_extemporaneo
     */
    public double getPorcentaje_extemporaneo() {
        return porcentaje_extemporaneo;
    }

    /**
     * @param porcentaje_extemporaneo the porcentaje_extemporaneo to set
     */
    public void setPorcentaje_extemporaneo(double porcentaje_extemporaneo) {
        this.porcentaje_extemporaneo = porcentaje_extemporaneo;
    }

    /**
     * @return the valor_extemporaneo_1
     */
    public double getValor_extemporaneo_1() {
        return valor_extemporaneo_1;
    }

    /**
     * @param valor_extemporaneo_1 the valor_extemporaneo_1 to set
     */
    public void setValor_extemporaneo_1(double valor_extemporaneo_1) {
        this.valor_extemporaneo_1 = valor_extemporaneo_1;
    }

    /**
     * @return the subtotal_base_2
     */
    public double getSubtotal_base_2() {
        return subtotal_base_2;
    }

    /**
     * @param subtotal_base_2 the subtotal_base_2 to set
     */
    public void setSubtotal_base_2(double subtotal_base_2) {
        this.subtotal_base_2 = subtotal_base_2;
    }

    /**
     * @return the valor_cuota_inicial
     */
    public double getValor_cuota_inicial() {
        return valor_cuota_inicial;
    }

    /**
     * @param valor_cuota_inicial the valor_cuota_inicial to set
     */
    public void setValor_cuota_inicial(double valor_cuota_inicial) {
        this.valor_cuota_inicial = valor_cuota_inicial;
    }

    /**
     * @return the subtotal_base_3
     */
    public double getSubtotal_base_3() {
        return subtotal_base_3;
    }

    /**
     * @param subtotal_base_3 the subtotal_base_3 to set
     */
    public void setSubtotal_base_3(double subtotal_base_3) {
        this.subtotal_base_3 = subtotal_base_3;
    }

    /**
     * @return the valor_iva
     */
    public double getValor_iva() {
        return valor_iva;
    }

    /**
     * @param valor_iva the valor_iva to set
     */
    public void setValor_iva(double valor_iva) {
        this.valor_iva = valor_iva;
    }

    /**
     * @return the valor_extemporaneo_2
     */
    public double getValor_extemporaneo_2() {
        return valor_extemporaneo_2;
    }

    /**
     * @param valor_extemporaneo_2 the valor_extemporaneo_2 to set
     */
    public void setValor_extemporaneo_2(double valor_extemporaneo_2) {
        this.valor_extemporaneo_2 = valor_extemporaneo_2;
    }

    /**
     * @return the subtotal_iva
     */
    public double getSubtotal_iva() {
        return subtotal_iva;
    }

    /**
     * @param subtotal_iva the subtotal_iva to set
     */
    public void setSubtotal_iva(double subtotal_iva) {
        this.subtotal_iva = subtotal_iva;
    }

    /**
     * @return the subtotal_a_financiar
     */
    public double getSubtotal_a_financiar() {
        return subtotal_a_financiar;
    }

    /**
     * @param subtotal_a_financiar the subtotal_a_financiar to set
     */
    public void setSubtotal_a_financiar(double subtotal_a_financiar) {
        this.subtotal_a_financiar = subtotal_a_financiar;
    }

    /**
     * @return the valor_extemporaneo
     */
    public double getValor_extemporaneo() {
        return valor_extemporaneo;
    }

    /**
     * @param valor_extemporaneo the valor_extemporaneo to set
     */
    public void setValor_extemporaneo(double valor_extemporaneo) {
        this.valor_extemporaneo = valor_extemporaneo;
    }

    /**
     * @return the valor_base_iva
     */
    public double getValor_base_iva() {
        return valor_base_iva;
    }

    /**
     * @param valor_base_iva the valor_base_iva to set
     */
    public void setValor_base_iva(double valor_base_iva) {
        this.valor_base_iva = valor_base_iva;
    }

    /**
     * @return the dtf_semanal
     */
    public double getDtf_semana() {
        return dtf_semana;
    }

    /**
     * @param dtf_semanal the dtf_semanal to set
     */
    public void setDtf_semana(double dtf_semana) {
        this.dtf_semana = dtf_semana;
    }

    /**
     * @return the porcentaje_interes
     */
    public double getPorcentaje_interes() {
        return porcentaje_interes;
    }

    /**
     * @param porcentaje_interes the porcentaje_interes to set
     */
    public void setPorcentaje_interes(double porcentaje_interes) {
        this.porcentaje_interes = porcentaje_interes;
    }

    /**
     * @return the prefacturar
     */
    public String getPrefacturar() {
        return prefacturar;
    }

    /**
     * @param prefacturar the prefacturar to set
     */
    public void setPrefacturar(String prefacturar) {
        this.prefacturar = prefacturar;
    }

    /**
     * @return the simbolo_variable
     */
    public String getSimbolo_variable() {
        return simbolo_variable;
    }

    /**
     * @param simbolo_variable the simbolo_variable to set
     */
    public void setSimbolo_variable(String simbolo_variable) {
        this.simbolo_variable = simbolo_variable;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the fecha_factura
     */
    public String getFecha_factura() {
        return fecha_factura;
    }

    /**
     * @param fecha_factura the fecha_factura to set
     */
    public void setFecha_factura(String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }



}