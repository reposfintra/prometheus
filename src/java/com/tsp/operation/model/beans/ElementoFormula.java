/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;


import java.io.Serializable;



public class ElementoFormula implements Serializable {

    private String elemento ;
    private int posicionInicial;
    private int posicionFinal;
    private int longitud;
    private String operando;
    private int numeroOperando;
    private String operandoTraducido;

    public ElementoFormula (){
        this.elemento = "";
        this.posicionInicial = 0;
        this.posicionFinal = 0;
        this.longitud = 0;
        this.operando = "";
        this.numeroOperando = 0;
        this.operandoTraducido = "";

    }


    public ElementoFormula (String formula,int posicionInicial, int posicionFinal ){
        elemento = formula.substring(posicionInicial, posicionFinal+1);
        this.posicionInicial = posicionInicial;
        this.posicionFinal = posicionFinal+1;
        longitud = posicionFinal - posicionInicial + 1 ;
        operando = formula.substring(posicionInicial + 1, posicionFinal);
        operandoTraducido = "";

        try {
           numeroOperando = Integer.parseInt(operando.substring(2).trim() );
        }
        catch (NumberFormatException nfe) {
            numeroOperando = 1;
            System.out.println("Error en formula");
        }


    }

    @Override
    public String toString(){
        String cadena = "";
        cadena = "Elemento: " + elemento + "  Posicion inicial: " + posicionInicial;
        cadena = cadena + "  Posicion final: " + posicionFinal + "  Longitud: " + longitud;
        cadena = cadena + "  Operando: " + operando + "  Numero gran total: " + numeroOperando;
        cadena = cadena + "  Operando traducido: " + operandoTraducido;
        return cadena;
    }


        /**
         * @return the elemento
         */
        public String getElemento() {
            return elemento;
        }

        /**
         * @param elemento the elemento to set
         */
        public void setElemento(String elemento) {
            this.elemento = elemento;
        }

        /**
         * @return the posicionInicial
         */
        public int getPosicionInicial() {
            return posicionInicial;
        }

        /**
         * @param posicionInicial the posicionInicial to set
         */
        public void setPosicionInicial(int posicionInicial) {
            this.posicionInicial = posicionInicial;
        }

        /**
         * @return the posicionFinal
         */
        public int getPosicionFinal() {
            return posicionFinal;
        }

        /**
         * @param posicionFinal the posicionFinal to set
         */
        public void setPosicionFinal(int posicionFinal) {
            this.posicionFinal = posicionFinal;
        }

        /**
         * @return the longitud
         */
        public int getLongitud() {
            return longitud;
        }

        /**
         * @param longitud the longitud to set
         */
        public void setLongitud(int longitud) {
            this.longitud = longitud;
        }

        /**
         * @return the operando
         */
        public String getOperando() {
            return operando;
        }

        /**
         * @param operando the operando to set
         */
        public void setOperando(String operando) {
            this.operando = operando;
        }

        /**
         * @return the numeroOperando
         */
        public int getNumeroOperando() {
            return numeroOperando;
        }

        /**
         * @param numeroOperando the numeroOperando to set
         */
        public void setNumeroOperando(int numeroOperando) {
            this.numeroOperando = numeroOperando;
        }

    /**
     * @return the operandoTraducido
     */
    public String getOperandoTraducido() {
        return operandoTraducido;
    }

    /**
     * @param operandoTraducido the operandoTraducido to set
     */
    public void setOperandoTraducido(String operandoTraducido) {
        this.operandoTraducido = operandoTraducido;
    }
}

