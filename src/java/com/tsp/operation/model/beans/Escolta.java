/*
 * Escolta.java
 *
 * Created on 9 de agosto de 2005, 13:41
 */

package com.tsp.operation.model.beans;
/**
 *
 * @author  Henry
 */
public class Escolta {
    
    private String placa;
    private String propietario;
    private String conductor;
    private String cedconductor;
    private boolean seleccionado;
    private String razon_exclusion = "";
    private String user_update;
    private String ciudad;
    private String origen;
    private String destino;
    private String numpla;
    
    /** Creates a new instance of Escolta */
    public Escolta() {
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property propietrario.
     * @return Value of property propietrario.
     */
    public java.lang.String getPropietario() {
        return propietario;
    }
    
    /**
     * Setter for property propietrario.
     * @param propietrario New value of property propietrario.
     */
    public void setPropietario(java.lang.String propietrario) {
        this.propietario = propietrario;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property cedconductor.
     * @return Value of property cedconductor.
     */
    public java.lang.String getCedconductor() {
        return cedconductor;
    }
    
    /**
     * Setter for property cedconductor.
     * @param cedconductor New value of property cedconductor.
     */
    public void setCedconductor(java.lang.String cedconductor) {
        this.cedconductor = cedconductor;
    }
    
    /**
     * Getter for property seleccionado.
     * @return Value of property seleccionado.
     */
    public boolean isSeleccionado() {
        return seleccionado;
    }
    
    /**
     * Setter for property seleccionado.
     * @param seleccionado New value of property seleccionado.
     */
    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }
    
    /**
     * Getter for property razon_exclusion.
     * @return Value of property razon_exclusion.
     */
    public java.lang.String getRazon_exclusion() {
        return razon_exclusion;
    }
    
    /**
     * Setter for property razon_exclusion.
     * @param razon_exclusion New value of property razon_exclusion.
     */
    public void setRazon_exclusion(java.lang.String razon_exclusion) {
        this.razon_exclusion = razon_exclusion;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
}

