/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author Alvaro
 */
public class EstadoFinanciero1 implements Serializable{



    private double secuencia;
    private String indicador_tercero;
    private String tipo_registro;
    private String descripcion;
    private String cuenta;
    private String tercero;
    private double vlr_mes01;
    private double vlr_mes02;
    private double vlr_mes03;
    private double vlr_mes04;
    private double vlr_mes05;
    private double vlr_mes06;
    private double vlr_mes07;
    private double vlr_mes08;
    private double vlr_mes09;
    private double vlr_mes10;
    private double vlr_mes11;
    private double vlr_mes12;
    private double vlr_mes13;
    
    private String formula;




    public EstadoFinanciero1() {

    }
    

    public static EstadoFinanciero1 load(java.sql.ResultSet rs)throws java.sql.SQLException{

        EstadoFinanciero1 estadoFinanciero1 = new EstadoFinanciero1();


        estadoFinanciero1.setSecuencia(rs.getDouble("secuencia"));
        estadoFinanciero1.setIndicador_tercero( rs.getString("indicador_tercero") ) ;
        estadoFinanciero1.setTipo_registro( rs.getString("tipo_registro") ) ;
        estadoFinanciero1.setDescripcion( rs.getString("descripcion") ) ;
        estadoFinanciero1.setCuenta( rs.getString("cuenta") ) ;
        estadoFinanciero1.setTercero( rs.getString("tercero") ) ;

        estadoFinanciero1.setVlr_mes01( rs.getDouble("vlr_mes01") ) ;
        estadoFinanciero1.setVlr_mes02( rs.getDouble("vlr_mes02") ) ;
        estadoFinanciero1.setVlr_mes03( rs.getDouble("vlr_mes03") ) ;
        estadoFinanciero1.setVlr_mes04( rs.getDouble("vlr_mes04") ) ;
        estadoFinanciero1.setVlr_mes05( rs.getDouble("vlr_mes05") ) ;
        estadoFinanciero1.setVlr_mes06( rs.getDouble("vlr_mes06") ) ;
        estadoFinanciero1.setVlr_mes07( rs.getDouble("vlr_mes07") ) ;
        estadoFinanciero1.setVlr_mes08( rs.getDouble("vlr_mes08") ) ;
        estadoFinanciero1.setVlr_mes09( rs.getDouble("vlr_mes09") ) ;
        estadoFinanciero1.setVlr_mes10( rs.getDouble("vlr_mes10") ) ;
        estadoFinanciero1.setVlr_mes11( rs.getDouble("vlr_mes11") ) ;
        estadoFinanciero1.setVlr_mes12( rs.getDouble("vlr_mes12") ) ;
        estadoFinanciero1.setVlr_mes13( rs.getDouble("vlr_mes13") ) ;
        
        estadoFinanciero1.setFormula( rs.getString("formula") );
        


        return estadoFinanciero1;

    }




    /**
     * @return the indicador_tercero
     */
    public String getIndicador_tercero() {
        return indicador_tercero;
    }

    /**
     * @param indicador_tercero the indicador_tercero to set
     */
    public void setIndicador_tercero(String indicador_tercero) {
        this.indicador_tercero = indicador_tercero;
    }

    /**
     * @return the tipo_registro
     */
    public String getTipo_registro() {
        return tipo_registro;
    }

    /**
     * @param tipo_registro the tipo_registro to set
     */
    public void setTipo_registro(String tipo_registro) {
        this.tipo_registro = tipo_registro;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the tercero
     */
    public String getTercero() {
        return tercero;
    }

    /**
     * @param tercero the tercero to set
     */
    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    /**
     * @return the vlr_mes01
     */
    public double getVlr_mes01() {
        return vlr_mes01;
    }

    /**
     * @param vlr_mes01 the vlr_mes01 to set
     */
    public void setVlr_mes01(double vlr_mes01) {
        this.vlr_mes01 = vlr_mes01;
    }

    /**
     * @return the vlr_mes02
     */
    public double getVlr_mes02() {
        return vlr_mes02;
    }

    /**
     * @param vlr_mes02 the vlr_mes02 to set
     */
    public void setVlr_mes02(double vlr_mes02) {
        this.vlr_mes02 = vlr_mes02;
    }

    /**
     * @return the vlr_mes03
     */
    public double getVlr_mes03() {
        return vlr_mes03;
    }

    /**
     * @param vlr_mes03 the vlr_mes03 to set
     */
    public void setVlr_mes03(double vlr_mes03) {
        this.vlr_mes03 = vlr_mes03;
    }

    /**
     * @return the vlr_mes04
     */
    public double getVlr_mes04() {
        return vlr_mes04;
    }

    /**
     * @param vlr_mes04 the vlr_mes04 to set
     */
    public void setVlr_mes04(double vlr_mes04) {
        this.vlr_mes04 = vlr_mes04;
    }

    /**
     * @return the vlr_mes05
     */
    public double getVlr_mes05() {
        return vlr_mes05;
    }

    /**
     * @param vlr_mes05 the vlr_mes05 to set
     */
    public void setVlr_mes05(double vlr_mes05) {
        this.vlr_mes05 = vlr_mes05;
    }

    /**
     * @return the vlr_mes06
     */
    public double getVlr_mes06() {
        return vlr_mes06;
    }

    /**
     * @param vlr_mes06 the vlr_mes06 to set
     */
    public void setVlr_mes06(double vlr_mes06) {
        this.vlr_mes06 = vlr_mes06;
    }

    /**
     * @return the vlr_mes07
     */
    public double getVlr_mes07() {
        return vlr_mes07;
    }

    /**
     * @param vlr_mes07 the vlr_mes07 to set
     */
    public void setVlr_mes07(double vlr_mes07) {
        this.vlr_mes07 = vlr_mes07;
    }

    /**
     * @return the vlr_mes08
     */
    public double getVlr_mes08() {
        return vlr_mes08;
    }

    /**
     * @param vlr_mes08 the vlr_mes08 to set
     */
    public void setVlr_mes08(double vlr_mes08) {
        this.vlr_mes08 = vlr_mes08;
    }

    /**
     * @return the vlr_mes09
     */
    public double getVlr_mes09() {
        return vlr_mes09;
    }

    /**
     * @param vlr_mes09 the vlr_mes09 to set
     */
    public void setVlr_mes09(double vlr_mes09) {
        this.vlr_mes09 = vlr_mes09;
    }

    /**
     * @return the vlr_mes10
     */
    public double getVlr_mes10() {
        return vlr_mes10;
    }

    /**
     * @param vlr_mes10 the vlr_mes10 to set
     */
    public void setVlr_mes10(double vlr_mes10) {
        this.vlr_mes10 = vlr_mes10;
    }

    /**
     * @return the vlr_mes11
     */
    public double getVlr_mes11() {
        return vlr_mes11;
    }

    /**
     * @param vlr_mes11 the vlr_mes11 to set
     */
    public void setVlr_mes11(double vlr_mes11) {
        this.vlr_mes11 = vlr_mes11;
    }

    /**
     * @return the vlr_mes12
     */
    public double getVlr_mes12() {
        return vlr_mes12;
    }

    /**
     * @param vlr_mes12 the vlr_mes12 to set
     */
    public void setVlr_mes12(double vlr_mes12) {
        this.vlr_mes12 = vlr_mes12;
    }

    
    /**
     * @return the vlr_mes13
     */
    public double getVlr_mes13() {
        return vlr_mes13;
    }

    /**
     * @param vlr_mes13 the vlr_mes13 to set
     */
    public void setVlr_mes13(double vlr_mes13) {
        this.vlr_mes13 = vlr_mes13;
    }    
    
    
    
    
    /**
     * @return the secuencia
     */
    public double getSecuencia() {
        return secuencia;
    }

    /**
     * @param secuencia the secuencia to set
     */
    public void setSecuencia(double secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * @return the formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * @param formula the formula to set
     */
    public void setFormula(String formula) {
        this.formula = formula;
    }




}
