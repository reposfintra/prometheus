 /*
 * RepMovTrafico.java
 *
 * Created on 8 de septiembre de 2005, 06:10 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Armando Oviedo
 */

import java.util.*;
import java.sql.*;

public class RepMovTrafico{
    
    private String regstatus;
    private String dstrct;
    private String numpla;
    private String observacion;
    private String tipo_procedencia;
    private String ubicacion_procedencia;
    private String tipo_reporte;
    private String lastupdate;    
    private String userupdate;
    private String creationdate;
    private String creationuser;  
    private String base;
    private String fechareporte;
    private String zona;
    private String codUbicacion;
    private String ub_ultreporte="";
    private String fec_ultreporte="";
    private String fec_rep_pla="";
    private String causa="";
    private String clasificacion="";
    private boolean ultimoRep = false;
    //Tito Andr�s Maturana 28.12.2005
    private String fechareporte_planeado;
    //Diogenes 2/1/06
    private String fecha_prox_reporte;
    //jose 2006/03/24
    private String nombre_reporte;
    
    private String nombre_procedencia;
    private String desctipo;
    private String descubicacion;
    private double tiempo;
    
    private String destino;
    
     private boolean isUltimo = false;
    /** Creates a new instance of RepMovTrafico */
    public RepMovTrafico() {
    }
    
    public static RepMovTrafico load(ResultSet rs) throws SQLException{
        RepMovTrafico rmt = new RepMovTrafico();
        rmt.setReg_Status(rs.getString("reg_status"));
        rmt.setDstrct(rs.getString("dstrct"));
        rmt.setNumpla(rs.getString("numpla"));
        rmt.setCreation_date(rs.getString("creation_date"));        
        rmt.setObservacion(rs.getString("observacion"));
        rmt.setTipo_procedencia(rs.getString("tipo_procedencia"));
        rmt.setUbicacion_procedencia(rs.getString("ubicacion_procedencia"));
        rmt.setTipo_reporte(rs.getString("tipo_reporte"));
        rmt.setLast_update(rs.getString("last_update"));
        rmt.setCreation_user(rs.getString("creation_user"));
        rmt.setUpdate_user(rs.getString("user_update"));                
        rmt.setBase(rs.getString("base"));
        rmt.setFechareporte(rs.getString("fechareporte"));
        rmt.setZona(rs.getString("zona"));
        rmt.setCodUbicacion(rmt.getUbicacion_procedencia());
        rmt.setFec_rep_pla(rs.getString("fechareporte_planeado"));
        return rmt;
    }
    
    public void setReg_Status(String regstatus){
        this.regstatus = regstatus;
    }
    public void setDstrct(String dstrct){
        this.dstrct = dstrct;
    }
    public void setNumpla(String numpla){
        this.numpla = numpla;
    }
    public void setCreation_date(String creationdate){
        this.creationdate = creationdate;
    }
    public void setObservacion(String observacion){
        this.observacion = observacion;
    }
    public void setTipo_procedencia(String tipo_procedencia){
        this.tipo_procedencia = tipo_procedencia;
    }
    public void setUbicacion_procedencia(String ubicacion_procedencia){
        this.ubicacion_procedencia = ubicacion_procedencia;
    }
    public void setTipo_reporte(String tipo_reporte){
        this.tipo_reporte = tipo_reporte;
    }
    public void setCreation_user(String creationuser){
        this.creationuser = creationuser;
    }
    public void setUpdate_user(String user_update){
        this.userupdate = user_update;
    }   
    public void setLast_update(String lastupdate){
        this.lastupdate = lastupdate;
    }
    public void setBase(String base){
        this.base = base;
    }
    public void setFechareporte(String fechareporte){
        this.fechareporte = fechareporte;
    }
    public void setZona(String zona){
        this.zona = zona;
    }
    public void setCodUbicacion(String CodUbicacion){
        this.codUbicacion = CodUbicacion;
    }
    public String getReg_status(){
        return this.regstatus;
    }
    public String getDstrct(){
        return this.dstrct;
    }
    public String getNumpla(){
        return this.numpla;
    }
    public String getCreation_date(){
        return creationdate;
    }
    public String getObservacion(){
        return observacion;
    }
    public String getTipo_procedencia(){
        return this.tipo_procedencia;
    }
    public String getUbicacion_procedencia(){
        return this.ubicacion_procedencia;
    }
    public String getTipo_reporte(){
        return this.tipo_reporte;
    }
    public String getCreation_user(){
        return this.creationuser;
    }
    public String getUpdate_user(){
        return this.userupdate;
    }    
    public String getLast_update(){
        return this.lastupdate;
    }
    public String getBase(){
        return this.base;
    }
    public String getFechaReporte(){
        return this.fechareporte;
    }
    public String getZona(){
        return this.zona;
    }
    public String getCodUbicacion(){
        return this.codUbicacion;
    }
    
    /**
     * Getter for property fec_ultreporte.
     * @return Value of property fec_ultreporte.
     */
    public java.lang.String getFec_ultreporte() {
        return fec_ultreporte;
    }
    
    /**
     * Setter for property fec_ultreporte.
     * @param fec_ultreporte New value of property fec_ultreporte.
     */
    public void setFec_ultreporte(java.lang.String fec_ultreporte) {
        this.fec_ultreporte = fec_ultreporte;
    }
    
    /**
     * Getter for property ub_ultreporte.
     * @return Value of property ub_ultreporte.
     */
    public java.lang.String getUb_ultreporte() {
        return ub_ultreporte;
    }
    
    /**
     * Setter for property ub_ultreporte.
     * @param ub_ultreporte New value of property ub_ultreporte.
     */
    public void setUb_ultreporte(java.lang.String ub_ultreporte) {
        this.ub_ultreporte = ub_ultreporte;
    }
    
    /**
     * Getter for property fec_rep_pla.
     * @return Value of property fec_rep_pla.
     */
    public java.lang.String getFec_rep_pla() {
        return fec_rep_pla;
    }
    
    /**
     * Setter for property fec_rep_pla.
     * @param fec_rep_pla New value of property fec_rep_pla.
     */
    public void setFec_rep_pla(java.lang.String fec_rep_pla) {
        this.fec_rep_pla = fec_rep_pla;
    }
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa() {
        return causa;
    }
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.lang.String causa) {
        this.causa = causa;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.lang.String getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(java.lang.String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property ultimoRep.
     * @return Value of property ultimoRep.
     */
    public boolean isUltimoRep() {
        return ultimoRep;
    }
    
    /**
     * Setter for property ultimoRep.
     * @param ultimoRep New value of property ultimoRep.
     */
    public void setUltimoRep(boolean ultimoRep) {
        this.ultimoRep = ultimoRep;
    }
    
    /**
     * Getter for property fechareporte_planeado.
     * @return Value of property fechareporte_planeado.
     */
    public java.lang.String getFechareporte_planeado () {
        return fechareporte_planeado;
    }
    
    /**
     * Setter for property fechareporte_planeado.
     * @param fechareporte_planeado New value of property fechareporte_planeado.
     */
    public void setFechareporte_planeado (java.lang.String fechareporte_planeado) {
        this.fechareporte_planeado = fechareporte_planeado;
    }
    
    /**
     * Getter for property fecha_prox_reporte.
     * @return Value of property fecha_prox_reporte.
     */
    public java.lang.String getFecha_prox_reporte () {
        return fecha_prox_reporte;
    }
    
    /**
     * Setter for property fecha_prox_reporte.
     * @param fecha_prox_reporte New value of property fecha_prox_reporte.
     */
    public void setFecha_prox_reporte (java.lang.String fecha_prox_reporte) {
        this.fecha_prox_reporte = fecha_prox_reporte;
    }
    
    /**
     * Getter for property nombre_reporte.
     * @return Value of property nombre_reporte.
     */
    public java.lang.String getNombre_reporte () {
        return nombre_reporte;
    }
    
    /**
     * Setter for property nombre_reporte.
     * @param nombre_reporte New value of property nombre_reporte.
     */
    public void setNombre_reporte (java.lang.String nombre_reporte) {
        this.nombre_reporte = nombre_reporte;
    }
    
    /**
     * Getter for property nombre_procedencia.
     * @return Value of property nombre_procedencia.
     */
    public java.lang.String getNombre_procedencia() {
        return nombre_procedencia;
    }
    
    /**
     * Setter for property nombre_procedencia.
     * @param nombre_procedencia New value of property nombre_procedencia.
     */
    public void setNombre_procedencia(java.lang.String nombre_procedencia) {
        this.nombre_procedencia = nombre_procedencia;
    }
    
    /**
     * Getter for property desctipo.
     * @return Value of property desctipo.
     */
    public java.lang.String getDesctipo() {
        return desctipo;
    }
    
    /**
     * Setter for property desctipo.
     * @param desctipo New value of property desctipo.
     */
    public void setDesctipo(java.lang.String desctipo) {
        this.desctipo = desctipo;
    }
    
    /**
     * Getter for property descubicacion.
     * @return Value of property descubicacion.
     */
    public java.lang.String getDescubicacion() {
        return descubicacion;
    }
    
    /**
     * Setter for property descubicacion.
     * @param descubicacion New value of property descubicacion.
     */
    public void setDescubicacion(java.lang.String descubicacion) {
        this.descubicacion = descubicacion;
    }
    
    /**
     * Getter for property isUltimo.
     * @return Value of property isUltimo.
     */
    public boolean IsUltimo() {
        return isUltimo;
    }
    
    /**
     * Setter for property isUltimo.
     * @param isUltimo New value of property isUltimo.
     */
    public void setIsUltimo(boolean isUltimo) {
        this.isUltimo = isUltimo;
    }
    
    /**
     * Getter for property tiempo.
     * @return Value of property tiempo.
     */
    public double getTiempo() {
        return tiempo;
    }
    
    /**
     * Setter for property tiempo.
     * @param tiempo New value of property tiempo.
     */
    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
}
