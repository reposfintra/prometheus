/*
 * SerieDocAnulado.java
 *
 * Created on 19 de marzo de 2007, 10:42 AM
 */
/*
* Nombre        SerieDocAnulado.java
* Descripci�n   Bean para el acceso a los datos del archivo de serie de documentos anulados
* Autor         Ing. Andr�s Maturana D.
* Fecha         19 de marzo de 2007
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.beans;

import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

/**
 *
 * @author  Andres
 */
public class SerieDocAnulado {
    
    private String dstrct;
    private String reg_status;
    private String tipo_documento;
    private String no_documento;
    private String referencia_1;
    private String referencia_2;
    private String referencia_3;
    private String causa_anulacion;
    private String last_update;
    private String user_update;
    private String creation_user;
    private String creation_date;
    private String base;
    private String tipo_documento_desc;
    private String causa_anulacion_desc;
    private String observacion;
    
    /** Creates a new instance of SerieDocAnulado */
    public SerieDocAnulado() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property causa_anulacion.
     * @return Value of property causa_anulacion.
     */
    public java.lang.String getCausa_anulacion() {
        return causa_anulacion;
    }
    
    /**
     * Setter for property causa_anulacion.
     * @param causa_anulacion New value of property causa_anulacion.
     */
    public void setCausa_anulacion(java.lang.String causa_anulacion) {
        this.causa_anulacion = causa_anulacion;
    }
    
    /**
     * Getter for property causa_anulacion_desc.
     * @return Value of property causa_anulacion_desc.
     */
    public java.lang.String getCausa_anulacion_desc() {
        return causa_anulacion_desc;
    }
    
    /**
     * Setter for property causa_anulacion_desc.
     * @param causa_anulacion_desc New value of property causa_anulacion_desc.
     */
    public void setCausa_anulacion_desc(java.lang.String causa_anulacion_desc) {
        this.causa_anulacion_desc = causa_anulacion_desc;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property no_documento.
     * @return Value of property no_documento.
     */
    public java.lang.String getNo_documento() {
        return no_documento;
    }
    
    /**
     * Setter for property no_documento.
     * @param no_documento New value of property no_documento.
     */
    public void setNo_documento(java.lang.String no_documento) {
        this.no_documento = no_documento;
    }
    
    /**
     * Getter for property referencia_1.
     * @return Value of property referencia_1.
     */
    public java.lang.String getReferencia_1() {
        return referencia_1;
    }
    
    /**
     * Setter for property referencia_1.
     * @param referencia_1 New value of property referencia_1.
     */
    public void setReferencia_1(java.lang.String referencia_1) {
        this.referencia_1 = referencia_1;
    }
    
    /**
     * Getter for property referencia_2.
     * @return Value of property referencia_2.
     */
    public java.lang.String getReferencia_2() {
        return referencia_2;
    }
    
    /**
     * Setter for property referencia_2.
     * @param referencia_2 New value of property referencia_2.
     */
    public void setReferencia_2(java.lang.String referencia_2) {
        this.referencia_2 = referencia_2;
    }
    
    /**
     * Getter for property referencia_3.
     * @return Value of property referencia_3.
     */
    public java.lang.String getReferencia_3() {
        return referencia_3;
    }
    
    /**
     * Setter for property referencia_3.
     * @param referencia_3 New value of property referencia_3.
     */
    public void setReferencia_3(java.lang.String referencia_3) {
        this.referencia_3 = referencia_3;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property tipo_documento_desc.
     * @return Value of property tipo_documento_desc.
     */
    public java.lang.String getTipo_documento_desc() {
        return tipo_documento_desc;
    }
    
    /**
     * Setter for property tipo_documento_desc.
     * @param tipo_documento_desc New value of property tipo_documento_desc.
     */
    public void setTipo_documento_desc(java.lang.String tipo_documento_desc) {
        this.tipo_documento_desc = tipo_documento_desc;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Carga los campos enviados a trav�s de un request
     */
    public void loadRequest(HttpServletRequest req ){
        this.setDstrct(this.validarRequest(req.getParameter("dstrct")));
        this.setTipo_documento(this.validarRequest(req.getParameter("tipo_doc")));
        this.setNo_documento(this.validarRequest(req.getParameter("nodoc")));
        this.setReferencia_1(this.validarRequest(req.getParameter("ref1")));
        this.setReferencia_2(this.validarRequest(req.getParameter("ref2")));
        this.setReferencia_3(this.validarRequest(req.getParameter("ref3")));
        this.setCausa_anulacion(this.validarRequest(req.getParameter("causa_anul")));
        this.setObservacion(this.validarRequest(req.getParameter("observacion")));
    }
    
    /**
     * Valida los campos enviados a trav�s de un request
     */
    private String validarRequest(String str){
        if( str==null )
            return "";
        else
            return str;
    }
    
    /**
     * Carga los campos enviados a trav�s de un <code>ResultSet</code>
     */
    public void load( ResultSet rs ) throws SQLException {
        this.setDstrct(rs.getString("dstrct"));
        this.setTipo_documento(rs.getString("tipo_documento"));
        this.setNo_documento(rs.getString("no_documento"));
        this.setReferencia_1(rs.getString("referencia_1"));
        this.setReferencia_2(rs.getString("referencia_2"));
        this.setReferencia_3(rs.getString("referencia_3"));
        this.setCausa_anulacion(rs.getString("causa_anul"));
        this.setObservacion(rs.getString("observacion"));
        this.setCreation_date(rs.getString("creation_date_ftd"));
        this.setLast_update(rs.getString("last_update_ftd"));
        this.setCreation_user(rs.getString("creation_user"));
        this.setUser_update(rs.getString("user_update"));
        this.setBase(rs.getString("base"));
    }
    
}
