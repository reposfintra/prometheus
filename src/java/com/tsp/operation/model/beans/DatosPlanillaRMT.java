/*
 * DatosPlanillaRMT.java
 *
 * Created on 14 de septiembre de 2005, 11:47 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.model.beans;

import java.util.*;

/**
 *
 * @author Armando Oviedo
 */
public class DatosPlanillaRMT {
    
    private String numpla;
    private String oripla;
    private String despla;
    private String plaveh;
    private String cedcon;
    private String rutapla;
    private String nombrecond;
    private String nomciuori;
    private String nomciudest;
    private String caravana;
    private String fecha_prox_rep;
    private String stdjob;
    private String frontera;
    private String clientes="";
    private Vector destinatarios;
    private String pto_control_proxreporte="";
    private String pto_control_ultreporte="";
    private String causas="";
    private String via="";
    private String tipo_procedencia="";
    private String nom_ciudad="";
    private String cod_ciudad="";
    private String reg_status="";
    private String nom_propietario;
    private String user_update;
    private String ced_propietario;
    private String zona;//David Pina Lopez
    private String tipo_reporte;//David Pina Lopez
    private String eintermedias;//Karen
    private String neintermedias;//Karen
    
    /** Creates a new instance of DatosPlanillaRMT */
    public DatosPlanillaRMT() {        
    }
    public void setNumpla(String numpla){
        this.numpla = numpla;        
    }
    public void setOripla(String oripla){
        this.oripla = oripla;        
    }
    public void setDespla(String despla){
        this.despla = despla;        
    }
    public void setPlaveh(String plaveh){
        this.plaveh = plaveh;
    }
    public void setCedcon(String cedcon){
        this.cedcon = cedcon;
    }
    public void setRutapla(String ruta_pla){
        this.rutapla = ruta_pla;
    }
    public void setNombrecond(String nombre){
        this.nombrecond = nombre;
    }
    public void setNomciuori(String nomciuori){
        this.nomciuori = nomciuori;
    }
    public void setNomciudest(String nomciudest){
        this.nomciudest = nomciudest;
    }
    public String getNumpla(){
        return this.numpla;
    }
    public String getOripla(){
        return this.oripla;        
    }
    public String getDespla(){
        return this.despla;
    }
    public String getPlaveh(){
        return this.plaveh;
    }
    public String getCedcon(){
        return this.cedcon;
    }
    public String getRutapla(){
        return this.rutapla;
    }
    public String getNombrecond(){
        return this.nombrecond;
    }
    public String getNomciuori(){
        return this.nomciuori;
    }
    public String getNomciudest(){
        return this.nomciudest;
    }
    
    /**
     * Getter for property caravana.
     * @return Value of property caravana.
     */
    public java.lang.String getCaravana() {
        return caravana;
    }
    
    /**
     * Setter for property caravana.
     * @param caravana New value of property caravana.
     */
    public void setCaravana(java.lang.String caravana) {
        this.caravana = caravana;
    }
    
    /**
     * Getter for property fecha_prox_rep.
     * @return Value of property fecha_prox_rep.
     */
    public java.lang.String getFecha_prox_rep() {
        return fecha_prox_rep;
    }
    
    /**
     * Setter for property fecha_prox_rep.
     * @param fecha_prox_rep New value of property fecha_prox_rep.
     */
    public void setFecha_prox_rep(java.lang.String fecha_prox_rep) {
        this.fecha_prox_rep = fecha_prox_rep;
    }
    
    /**
     * Getter for property stdjob.
     * @return Value of property stdjob.
     */
    public java.lang.String getStdjob() {
        return stdjob;
    }
    
    /**
     * Setter for property stdjob.
     * @param stdjob New value of property stdjob.
     */
    public void setStdjob(java.lang.String stdjob) {
        this.stdjob = stdjob;
    }
    
    /**
     * Getter for property frontera.
     * @return Value of property frontera.
     */
    public java.lang.String getFrontera() {
        return frontera;
    }
    
    /**
     * Setter for property frontera.
     * @param frontera New value of property frontera.
     */
    public void setFrontera(java.lang.String frontera) {
        this.frontera = frontera;
    }
    
    /**
     * Getter for property clientes.
     * @return Value of property clientes.
     */
    public java.lang.String getClientes() {
        return clientes;
    }
    
    /**
     * Setter for property clientes.
     * @param clientes New value of property clientes.
     */
    public void setClientes(java.lang.String clientes) {
        this.clientes = clientes;
    }
    
    /**
     * Getter for property destinatarios.
     * @return Value of property destinatarios.
     */
    public java.util.Vector getDestinatarios() {
        return destinatarios;
    }
    
    /**
     * Setter for property destinatarios.
     * @param destinatarios New value of property destinatarios.
     */
    public void setDestinatarios(java.util.Vector destinatarios) {
        this.destinatarios = destinatarios;
    }
    
    /**
     * Getter for property pto_control_proxreporte.
     * @return Value of property pto_control_proxreporte.
     */
    public java.lang.String getPto_control_proxreporte() {
        return pto_control_proxreporte;
    }
    
    /**
     * Setter for property pto_control_proxreporte.
     * @param pto_control_proxreporte New value of property pto_control_proxreporte.
     */
    public void setPto_control_proxreporte(java.lang.String pto_control_proxreporte) {
        this.pto_control_proxreporte = pto_control_proxreporte;
    }
    
    /**
     * Getter for property causas.
     * @return Value of property causas.
     */
    public java.lang.String getCausas() {
        return causas;
    }
    
    /**
     * Setter for property causas.
     * @param causas New value of property causas.
     */
    public void setCausas(java.lang.String causas) {
        this.causas = causas;
    }
    
    /**
     * Getter for property via.
     * @return Value of property via.
     */
    public java.lang.String getVia () {
        return via;
    }
    
    /**
     * Setter for property via.
     * @param via New value of property via.
     */
    public void setVia (java.lang.String via) {
        this.via = via;
    }
    
    /**
     * Getter for property pto_control_ultreporte.
     * @return Value of property pto_control_ultreporte.
     */
    public java.lang.String getPto_control_ultreporte () {
        return pto_control_ultreporte;
    }
    
    /**
     * Setter for property pto_control_ultreporte.
     * @param pto_control_ultreporte New value of property pto_control_ultreporte.
     */
    public void setPto_control_ultreporte (java.lang.String pto_control_ultreporte) {
        this.pto_control_ultreporte = pto_control_ultreporte;
    }
    
    /**
     * Getter for property tipo_procedencia.
     * @return Value of property tipo_procedencia.
     */
    public java.lang.String getTipo_procedencia () {
        return tipo_procedencia;
    }
    
    /**
     * Setter for property tipo_procedencia.
     * @param tipo_procedencia New value of property tipo_procedencia.
     */
    public void setTipo_procedencia (java.lang.String tipo_procedencia) {
        this.tipo_procedencia = tipo_procedencia;
    }
    
    /**
     * Getter for property nom_ciudad.
     * @return Value of property nom_ciudad.
     */
    public java.lang.String getNom_ciudad () {
        return nom_ciudad;
    }    
    
    /**
     * Setter for property nom_ciudad.
     * @param nom_ciudad New value of property nom_ciudad.
     */
    public void setNom_ciudad (java.lang.String nom_ciudad) {
        this.nom_ciudad = nom_ciudad;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status () {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status (java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property cod_ciudad.
     * @return Value of property cod_ciudad.
     */
    public java.lang.String getCod_ciudad () {
        return cod_ciudad;
    }
    
    /**
     * Setter for property cod_ciudad.
     * @param cod_ciudad New value of property cod_ciudad.
     */
    public void setCod_ciudad (java.lang.String cod_ciudad) {
        this.cod_ciudad = cod_ciudad;
    }
    
    /**
     * Getter for property nom_propietario.
     * @return Value of property nom_propietario.
     */
    public java.lang.String getNom_propietario () {
        return nom_propietario;
    }
    
    /**
     * Setter for property nom_propietario.
     * @param nom_propietario New value of property nom_propietario.
     */
    public void setNom_propietario (java.lang.String nom_propietario) {
        this.nom_propietario = nom_propietario;
    }
    
    /**
     * Getter for property ced_propietario.
     * @return Value of property ced_propietario.
     */
    public java.lang.String getCed_propietario () {
        return ced_propietario;
    }
    
    /**
     * Setter for property ced_propietario.
     * @param ced_propietario New value of property ced_propietario.
     */
    public void setCed_propietario (java.lang.String ced_propietario) {
        this.ced_propietario = ced_propietario;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update () {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }
    
    /**
     * Getter for property tipo_reporte.
     * @return Value of property tipo_reporte.
     */
    public java.lang.String getTipo_reporte() {
        return tipo_reporte;
    }
    
    /**
     * Setter for property tipo_reporte.
     * @param tipo_reporte New value of property tipo_reporte.
     */
    public void setTipo_reporte(java.lang.String tipo_reporte) {
        this.tipo_reporte = tipo_reporte;
    }
    
    /**
     * Getter for property eintermedias.
     * @return Value of property eintermedias.
     */
    public java.lang.String getEintermedias() {
        return eintermedias;
    }
    
    /**
     * Setter for property eintermedias.
     * @param eintermedias New value of property eintermedias.
     */
    public void setEintermedias(java.lang.String eintermedias) {
        this.eintermedias = eintermedias;
    }
    
    /**
     * Getter for property neintermedias.
     * @return Value of property neintermedias.
     */
    public java.lang.String getNeintermedias() {
        return neintermedias;
    }
    
    /**
     * Setter for property neintermedias.
     * @param neintermedias New value of property neintermedias.
     */
    public void setNeintermedias(java.lang.String neintermedias) {
        this.neintermedias = neintermedias;
    }
    
}
