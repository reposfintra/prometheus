/***********************************************************************************
 * Nombre clase : ............... FlotaUtilizada.java                              *
 * Descripcion :................. Clase que maneja los atributos del objeto        *
 *                                FlotaUtilizada los cuales representan los        *
 *                                campos de la tabla flota_utilizada               *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 9 de diciembre de 2005, 08:00 AM                 *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.beans;

public class FlotaUtilizada {
    
    private String tipo;
    private String placa;
    private String numpla;
    private String fecha;
    private String origen;
    private String destino;
    private String ori_dest; //Agencia Asociada
    private String reg_status;
    private String creation_user;
    private String creation_date;
    private String dstrct;
    private String last_update;
    private String user_update;
    private String base;
    private String planilla_Salida;
    private String fecha_salida;
    private String placa_salida;
    private String cliente;
    private String clienteSalida;
    private String recurso;
    public FlotaUtilizada() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property ori_dest.
     * @return Value of property ori_dest.
     */
    public java.lang.String getOri_dest() {
        return ori_dest;
    }
    
    /**
     * Setter for property ori_dest.
     * @param ori_dest New value of property ori_dest.
     */
    public void setOri_dest(java.lang.String ori_dest) {
        this.ori_dest = ori_dest;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property planilla_Salida.
     * @return Value of property planilla_Salida.
     */
    public java.lang.String getPlanilla_Salida() {
        return planilla_Salida;
    }
    
    /**
     * Setter for property planilla_Salida.
     * @param planilla_Salida New value of property planilla_Salida.
     */
    public void setPlanilla_Salida(java.lang.String planilla_Salida) {
        this.planilla_Salida = planilla_Salida;
    }
    
    /**
     * Getter for property fecha_salida.
     * @return Value of property fecha_salida.
     */
    public java.lang.String getFecha_salida() {
        return fecha_salida;
    }
    
    /**
     * Setter for property fecha_salida.
     * @param fecha_salida New value of property fecha_salida.
     */
    public void setFecha_salida(java.lang.String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }
    
    /**
     * Getter for property placa_salida.
     * @return Value of property placa_salida.
     */
    public java.lang.String getPlaca_salida() {
        return placa_salida;
    }
    
    /**
     * Setter for property placa_salida.
     * @param placa_salida New value of property placa_salida.
     */
    public void setPlaca_salida(java.lang.String placa_salida) {
        this.placa_salida = placa_salida;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property clienteSalida.
     * @return Value of property clienteSalida.
     */
    public java.lang.String getClienteSalida() {
        return clienteSalida;
    }
    
    /**
     * Setter for property clienteSalida.
     * @param clienteSalida New value of property clienteSalida.
     */
    public void setClienteSalida(java.lang.String clienteSalida) {
        this.clienteSalida = clienteSalida;
    }
    
    /**
     * Getter for property recurso.
     * @return Value of property recurso.
     */
    public java.lang.String getRecurso() {
        return recurso;
    }
    
    /**
     * Setter for property recurso.
     * @param recurso New value of property recurso.
     */
    public void setRecurso(java.lang.String recurso) {
        this.recurso = recurso;
    }
    
}
