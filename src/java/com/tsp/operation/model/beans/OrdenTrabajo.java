package com.tsp.operation.model.beans;

/**
 * @author PBASSIL
 */
public class OrdenTrabajo {

    private String id_multiservicio;
    private String id_solicitud;
    private String observaciones;
    private String nic;
    private String last_update;
    private String creation_user;
    private String user_update;
    private String reg_status;

    public OrdenTrabajo() {
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getId_multiservicio() {
        return id_multiservicio;
    }

    public void setId_multiservicio(String id_multiservicio) {
        this.id_multiservicio = id_multiservicio;
    }

    public String getId_solicitud() {
        return id_solicitud;
    }

    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }
}