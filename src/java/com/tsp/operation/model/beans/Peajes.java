/*
 * Peajes.java
 *
 * Created on 3 de diciembre de 2004, 03:51 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  KREALES
 */
public class Peajes {
    
    private String dstrct;
    private String ticket_id;
    private String description;
    private float value;
    private String current_type;
    private String creation_user;
    
    /** Creates a new instance of Peajes */
    public static Peajes load(ResultSet rs)throws SQLException {
        Peajes p = new Peajes();
        p.setDecripcion(rs.getString("description"));
        p.setDstrct(rs.getString("Dstrct"));
        p.setMoneda(rs.getString("current_type"));
        p.setTiket_id(rs.getString("ticket_id"));
        p.setUser(rs.getString("creation_user"));
        p.setValue(rs.getFloat("Value"));
        return p;
    }
    public void setDstrct(String dstrct){
        this.dstrct=dstrct;
    }
    public String getDstrct(){
        return this.dstrct;
            
    }
    public void setTiket_id(String ticket_id){
        this.ticket_id=ticket_id;
    }
    public String getTiket_id(){
        return this.ticket_id;
            
    }
    public void setDecripcion(String descripcion){
        this.description=descripcion;
    }
    public String getDescripcion(){
        return this.description;
            
    }
     public void setUser(String user){
        this.creation_user=user;
    
    }
    public String getUser(){
        return creation_user;
    
    }
    public void setMoneda(String moneda){
        this.current_type=moneda;
    
    }
    public String getMoneda(){
        return current_type;
    
    }
    public void setValue(float valor){
        this.value=valor;
    
    }
    public float getValor(){
        return value;
    
    }
    
}
