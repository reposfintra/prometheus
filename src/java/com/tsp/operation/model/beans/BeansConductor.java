/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author user
 */
public class BeansConductor {
                
    private String clasificacion;
    private String nit;
    private String tipo_doc;
    private String fecha_nacimiento;
    private String nombre;
    private String banco;
    private String sucursal;
    private String cedula_titular_cuenta;
    private String nombre_titular_cuenta;
    private String tipo_cuenta;
    private String no_cuenta;   
    private String veto;
    private String veto_causal;
    private String ciudad;
    private String barrio;
    private String direccion;
    private String telefono;
    private String celular;
    private String email;

    public BeansConductor() {
    }

    public BeansConductor(String clasificacion, String nit, String tipo_doc, String fecha_nacimiento, String nombre, String banco, String sucursal, String cedula_titular_cuenta, String nombre_titular_cuenta, String tipo_cuenta, String no_cuenta, String veto, String veto_causal, String ciudad, String barrio, String direccion, String telefono, String celular, String email) {
        this.clasificacion = clasificacion;
        this.nit = nit;
        this.tipo_doc = tipo_doc;
        this.fecha_nacimiento = fecha_nacimiento;
        this.nombre = nombre;
        this.banco = banco;
        this.sucursal = sucursal;
        this.cedula_titular_cuenta = cedula_titular_cuenta;
        this.nombre_titular_cuenta = nombre_titular_cuenta;
        this.tipo_cuenta = tipo_cuenta;
        this.no_cuenta = no_cuenta;
        this.veto = veto;
        this.veto_causal = veto_causal;
        this.ciudad = ciudad;
        this.barrio = barrio;
        this.direccion = direccion;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
    }
    
    

    /**
     * @return the clasificacion
     */
    public String getClasificacion() {
        return clasificacion;
    }

    /**
     * @param clasificacion the clasificacion to set
     */
    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the tipo_doc
     */
    public String getTipo_doc() {
        return tipo_doc;
    }

    /**
     * @param tipo_doc the tipo_doc to set
     */
    public void setTipo_doc(String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    /**
     * @return the fecha_nacimiento
     */
    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    /**
     * @param fecha_nacimiento the fecha_nacimiento to set
     */
    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * @return the sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the cedula_titular_cuenta
     */
    public String getCedula_titular_cuenta() {
        return cedula_titular_cuenta;
    }

    /**
     * @param cedula_titular_cuenta the cedula_titular_cuenta to set
     */
    public void setCedula_titular_cuenta(String cedula_titular_cuenta) {
        this.cedula_titular_cuenta = cedula_titular_cuenta;
    }

    /**
     * @return the nombre_titular_cuenta
     */
    public String getNombre_titular_cuenta() {
        return nombre_titular_cuenta;
    }

    /**
     * @param nombre_titular_cuenta the nombre_titular_cuenta to set
     */
    public void setNombre_titular_cuenta(String nombre_titular_cuenta) {
        this.nombre_titular_cuenta = nombre_titular_cuenta;
    }

    /**
     * @return the tipo_cuenta
     */
    public String getTipo_cuenta() {
        return tipo_cuenta;
    }

    /**
     * @param tipo_cuenta the tipo_cuenta to set
     */
    public void setTipo_cuenta(String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    /**
     * @return the no_cuenta
     */
    public String getNo_cuenta() {
        return no_cuenta;
    }

    /**
     * @param no_cuenta the no_cuenta to set
     */
    public void setNo_cuenta(String no_cuenta) {
        this.no_cuenta = no_cuenta;
    }

    /**
     * @return the veto
     */
    public String getVeto() {
        return veto;
    }

    /**
     * @param veto the veto to set
     */
    public void setVeto(String veto) {
        this.veto = veto;
    }

    /**
     * @return the veto_causal
     */
    public String getVeto_causal() {
        return veto_causal;
    }

    /**
     * @param veto_causal the veto_causal to set
     */
    public void setVeto_causal(String veto_causal) {
        this.veto_causal = veto_causal;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * @param barrio the barrio to set
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * @param celular the celular to set
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    
}
