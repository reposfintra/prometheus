/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 * Bean para manejar lotes subida archivo recaudo Asobancaria<br/>
 * 30/03/2015
 * @author mcastillo
 */
public class RecaudoAsobancariaLote {
    
    private String cod_servicio_rec;
    private int numero_lote;
    private int total_registros;
    private int cod_convenio;
    private double total_recaudado;
    private double total_recaudado_cheque;
    
    public RecaudoAsobancariaLote (){
    }

    /**
     * @return the cod_servicio_rec
     */
    public String getCod_servicio_rec() {
        return cod_servicio_rec;
    }

    /**
     * @param cod_servicio_rec the cod_servicio_rec to set
     */
    public void setCod_servicio_rec(String cod_servicio_rec) {
        this.cod_servicio_rec = cod_servicio_rec;
    }

    /**
     * @return the numero_lote
     */
    public int getNumero_lote() {
        return numero_lote;
    }

    /**
     * @param numero_lote the numero_lote to set
     */
    public void setNumero_lote(int numero_lote) {
        this.numero_lote = numero_lote;
    }

    /**
     * @return the total_registros
     */
    public int getTotal_registros() {
        return total_registros;
    }

    /**
     * @param total_registros the total_registros to set
     */
    public void setTotal_registros(int total_registros) {
        this.total_registros = total_registros;
    }

    /**
     * @return the total_recaudado
     */
    public double getTotal_recaudado() {
        return total_recaudado;
    }

    /**
     * @param total_recaudado the total_recaudado to set
     */
    public void setTotal_recaudado(double total_recaudado) {
        this.total_recaudado = total_recaudado;
    }

    public int getCod_convenio() {
        return cod_convenio;
    }

    public void setCod_convenio(int cod_convenio) {
        this.cod_convenio = cod_convenio;
    }

    public double getTotal_recaudado_cheque() {
        return total_recaudado_cheque;
    }

    public void setTotal_recaudado_cheque(double total_recaudado_cheque) {
        this.total_recaudado_cheque = total_recaudado_cheque;
    }
    
    
}
