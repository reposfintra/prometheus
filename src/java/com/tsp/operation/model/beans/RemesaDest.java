/*
 * RemesaDest.java
 *
 * Created on 7 de diciembre de 2004, 04:57 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
import java.io.Serializable;
import java.io.*;
import java.sql.*;
import java.util.*;

public class RemesaDest {
    
    private String dstrct;
    private String nombre;
    private String codigo;
    private String numrem;
    private String tipo;
    private java.util.Date last_update;
    private String user_update;
    
    /** Creates a new instance of RemesaDest */
    public static  RemesaDest load(ResultSet rs)throws SQLException  {
        RemesaDest rd = new RemesaDest();
        rd.setCodigo(rs.getString("codigo"));
        rd.setDstrct(rs.getString("dstrct"));
        rd.setFecha(rs.getTimestamp("last_update"));
        rd.setNumrem(rs.getString("numrem"));
        rd.setTipo(rs.getString("tipo"));
        rd.setUsuario(rs.getString("user_update"));
        return rd;
    }
    public String getDstrct(){
        return dstrct;
    }
    public void setDstrct(String dstrct){
        this.dstrct=dstrct;
    }
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getCodigo(){
        return codigo;
    }
    public void setCodigo(String codigo){
        this.codigo=codigo;
    }
    public String getNumrem(){
        return numrem;
    }
    public void setNumrem(String numrem){
        this.numrem=numrem;
    }
    public String getTipo(){
        return tipo;
    }
    public void setTipo(String tipo){
        this.tipo=tipo;
    }
    public  java.util.Date getFecha(){
        return last_update;
    }
    public void  setFecha(java.util.Date last_update){
        this.last_update=last_update;
    }
    public String getUsuario(){
        return user_update;
    }
    public void setUsuario(String usuario){
        this.user_update=usuario;
    }
}
