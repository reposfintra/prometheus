/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class RecaudoProceso {

    private String simbolo_var;
    private Double imp_recaudo;


    public RecaudoProceso () {
       inicializar() ;
   }



    public void inicializar() {

        setSimbolo_var("");
        setImp_recaudo((Double) 0.00);
    }





    public static RecaudoProceso load(java.sql.ResultSet rs)throws java.sql.SQLException{

        RecaudoProceso recaudoProceso = new RecaudoProceso();


        recaudoProceso.setSimbolo_var(rs.getString("simbolo_var") );
        recaudoProceso.setImp_recaudo(rs.getDouble("valor_recaudo") );

        return recaudoProceso;
    }




















    /**
     * @return the simbolo_var
     */
    public String getSimbolo_var() {
        return simbolo_var;
    }

    /**
     * @param simbolo_var the simbolo_var to set
     */
    public void setSimbolo_var(String simbolo_var) {
        this.simbolo_var = simbolo_var;
    }

    /**
     * @return the imp_recaudo
     */
    public Double getImp_recaudo() {
        return imp_recaudo;
    }

    /**
     * @param imp_recaudo the imp_recaudo to set
     */
    public void setImp_recaudo(Double imp_recaudo) {
        this.imp_recaudo = imp_recaudo;
    }













}
