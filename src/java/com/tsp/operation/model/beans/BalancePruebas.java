/*
 *  Nombre clase    :  BalancePrueba.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 20 de junio de 2006, 11:43 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;

import java.util.*;
import java.sql.*;

public class BalancePruebas extends BPCuentas{
              
    
    /*Datos cabecera*/
    private String Pagina;
    private String FechaActual;
    private String Periodo;
    
    private List CuentasNumericas;
    private List CuentasAlpha;
    
    private BPCuentasNumericas  bpn;
    private BPCuentasAlpha      bpa;
    
   
    
    /** Crea una nueva instancia de  BalancePrueba */
    public BalancePruebas() {
        CuentasNumericas    = new LinkedList();
        CuentasAlpha        = new LinkedList();
    }
    
   
    
    /**
     * Getter for property FechaActual.
     * @return Value of property FechaActual.
     */
    public java.lang.String getFechaActual() {
        return FechaActual;
    }
    
    /**
     * Setter for property FechaActual.
     * @param FechaActual New value of property FechaActual.
     */
    public void setFechaActual(java.lang.String FechaActual) {
        this.FechaActual = FechaActual;
    }
    
    /**
     * Getter for property Pagina.
     * @return Value of property Pagina.
     */
    public java.lang.String getPagina() {
        return Pagina;
    }
    
    /**
     * Setter for property Pagina.
     * @param Pagina New value of property Pagina.
     */
    public void setPagina(java.lang.String Pagina) {
        this.Pagina = Pagina;
    }
    
    /**
     * Getter for property Periodo.
     * @return Value of property Periodo.
     */
    public java.lang.String getPeriodo() {
        return Periodo;
    }
    
    /**
     * Setter for property Periodo.
     * @param Periodo New value of property Periodo.
     */
    public void setPeriodo(java.lang.String Periodo) {
        this.Periodo = Periodo;
    }
    
    
    public void Calcular( int mes ){
        
        if (CuentasNumericas!=null && CuentasNumericas.size()>0){
            for (int i = 0; i < CuentasNumericas.size();i++){
                BPCuentasNumericas bpn = (BPCuentasNumericas) CuentasNumericas.get(i);
                this.setVMovCredito( mes,   bpn.getVMovCredito(mes));
                this.setVMovDebito(  mes ,  bpn.getVMovDebito(mes));
            }
            CalcularSaldos(mes);
        }
        
         if (CuentasAlpha!=null && CuentasAlpha.size()>0){
            for (int i = 0; i < CuentasAlpha.size();i++){
                BPCuentasAlpha bpa = (BPCuentasAlpha) CuentasAlpha.get(i);
                this.setVMovCredito( mes,   bpa.getVMovCredito(mes));
                this.setVMovDebito(  mes ,  bpa.getVMovDebito(mes));
            }
            CalcularSaldos(mes);
        }
        
    }
    
    /**
     * Getter for property bpn.
     * @return Value of property bpn.
     */
    public com.tsp.operation.model.beans.BPCuentasNumericas getBpn() {
        return bpn;
    }
    
    /**
     * Setter for property bpn.
     * @param bpn New value of property bpn.
     */
    public void setBpn(com.tsp.operation.model.beans.BPCuentasNumericas bpn) {
        this.bpn = bpn;
    }
    
    /**
     * Getter for property bpa.
     * @return Value of property bpa.
     */
    public com.tsp.operation.model.beans.BPCuentasAlpha getBpa() {
        return bpa;
    }
    
    /**
     * Setter for property bpa.
     * @param bpa New value of property bpa.
     */
    public void setBpa(com.tsp.operation.model.beans.BPCuentasAlpha bpa) {
        this.bpa = bpa;
    }
    
    /**
     * Getter for property CuentasNumericas.
     * @return Value of property CuentasNumericas.
     */
    public java.util.List getCuentasNumericas() {
        return CuentasNumericas;
    }
    
    /**
     * Setter for property CuentasNumericas.
     * @param CuentasNumericas New value of property CuentasNumericas.
     */
    public void setCuentasNumericas(java.util.List CuentasNumericas) {
        this.CuentasNumericas = CuentasNumericas;
    }
    
    /**
     * Getter for property CuentasAlpha.
     * @return Value of property CuentasAlpha.
     */
    public java.util.List getCuentasAlpha() {
        return CuentasAlpha;
    }
    
    /**
     * Setter for property CuentasAlpha.
     * @param CuentasAlpha New value of property CuentasAlpha.
     */
    public void setCuentasAlpha(java.util.List CuentasAlpha) {
        this.CuentasAlpha = CuentasAlpha;
    }
    
}
