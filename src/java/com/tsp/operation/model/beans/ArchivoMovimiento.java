  /***************************************
    * Nombre Clase ............. ArchivoMovimiento.java
    * Descripci�n  .. . . . . .  Obtiene datos para generar el archivo movimiento
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  14/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.beans;


import com.tsp.operation.model.Model;
import com.tsp.util.Utility;
import java.util.Date;
import java.text.*;

public class ArchivoMovimiento {
    
    
    private String  distrito;
    private String  banco;
    private String  sucursal;
    private String  corrida;
    private String  cheque;
    private String  idMims;
    private String  moneda;
    
    
    
    private String secuencia; 
    private String secuenciaProveedor; 
    private String fecha; 
    
    private String  valor; 
    private String observacion; 
    private String secuenciaNotaAdicional; 
    //proveedor:
    private String nitProveedor; 
    private String nombre;
    private String codigoBancoProveedor; 
    private String tipoCuentaProveedor; 
    private String noCuentaProveedor; 
    private String tipoTransaccionProveedor; 
    
    private String nombreCuenta; 
    private String cedulaCuenta; 
    
    //RRARP
    private String treg="6";
    private String   BANCOLOMBIA_PROPOSITO         = "PAGFINTRA"; 
    private String indicador_pago   = " ";
    
    private String   BANCOLOMBIA                   = "07";
    private String   TABLA_BANCOLOMBIA_BANCO       = "BANCOLOMBI";   // BANCOS ESTIPULADOS POR BANCOLOMBIA
    private String   TABLA_BANCOLOMBIA_CIUDAD      = "CIUBANCOLO";   // CODIGO DE CIUDAD ESTIPULADOS POR BANCOLOMBIA
           
    private String   BANCOLOMBIA_TIPO_REGISTRO     = "1";
    private String   BANCOLOMBIA_TIPO_REGISTRO_DET = "6";
    private String   BANCOLOMBIA_TIPO_TRANSACTION  = "220";     
    private String   BANCOLOMBIA_CTA_AHORR0        = "37";
    private String   BANCOLOMBIA_CTA_CORRIENTE     = "27";
    private String   BANCOLOMBIA_CTA_EFECTIVO      = "26";
    
    private String  tipo_transaction;
    //endrarp
   
    //cliente:
    private String nitCliente;
    private String noCuentaCliente; 
    private String tipoCuentaCliente; 
    private String tipoOperacionCliente; 
    
    //Datos necesarios para generar el archivo BBVA     - TMOLINA 2008-08-27
    private String tipoID;
    private String documento;
    private String formaDePago;
    private String codBanco;
    private String noCuentaBBVA;
    private String tipCuenta;
    private String noCuenta;
    private String valorEntero;
    private String valorDecimal;
    private String codOficinaPagadora;
    private String nombreBeneficiario;
    private String direccion1;
    private String direccion2;
    private String email;
    private String concepto1;
    private String   BANCOLOMBIA_CPAG_TIPO_TRANSACTION= "320";
    
    private String tipoCuentaOrigen;




    
    
    
    public ArchivoMovimiento() {}
    
    
    
    
    
    
  /**
   * M�todo que formatea la longitus de los campos para el archivo
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/  
public void formatearLongitud()throws Exception{   
       try{ 
            String space     = " ";
            String cero      = "0";
            String derecha   = "R";
            String izquierda = "L";

            tipo_transaction = equivalenciaTipoTransaction( BANCOLOMBIA, this.tipoCuentaProveedor );
            
            
            this.treg                     = rellenar( this.treg,                   space,  1,           derecha);
            this.cedulaCuenta             = rellenar( this.cedulaCuenta ,          space,  15,        derecha  );
            this.nombreCuenta             = rellenar( this.nombreCuenta,           space,  30,        derecha  );
            this.codigoBancoProveedor     = rellenar( this.codigoBancoProveedor,   cero ,  9,       izquierda  );
            this.noCuentaProveedor        = rellenar( this.noCuentaProveedor,      space,  17,        derecha  );
            this.indicador_pago           = rellenar( this.indicador_pago,         space,  1,       izquierda  );
            this.tipo_transaction         = rellenar( this.tipo_transaction,       space,  2,       izquierda  );
            this.valor                    = rellenar( this.valor + "00",           cero,   17,      izquierda  );
            this.BANCOLOMBIA_PROPOSITO    = rellenar( this.BANCOLOMBIA_PROPOSITO , space,  9,        derecha  );
            this.cheque                   = rellenar( this.cheque,                 space, 12,         derecha  );
       }catch(Exception e){
           throw new Exception( " formatearLongitud " + e.getMessage());
       }
 }

    
    
 
  /**
   * M�todo que devuelve la linea
   * @autor.......fvillacob
   * @throws......Exception
   * @version.....1.0.
   **/  
  public String getColumnas()throws Exception{

      String space = " ";
      String derecha = "R";
      
      return 
            this.treg               +     
            this.cedulaCuenta       +      
            this.nombreCuenta       +     
            this.codigoBancoProveedor+
            this.noCuentaProveedor  +     
            this.indicador_pago     +     
            this.tipo_transaction   + 
            this.valor              +     
            this.getFecha("yyyyMMdd")+
            this.BANCOLOMBIA_PROPOSITO +
            this.cheque             +            //Referencia       
            rellenar(space, space, 1, derecha) + // 12 Tipo de documento de identificaci�n
            rellenar(space, space, 5, derecha) + // 13 Oficina de entrega
            rellenar(space, space, 15, derecha) + // 14 N�mero de Fax
            rellenar(space, space, 80, derecha) + // 15 E-mail beneficiario
            rellenar(space, space, 15, derecha) + // 16 N�mero identificaci�n del autorizado
            rellenar(space, space, 27, derecha);   // 17 Filler
     
  }


  
   public String head(int t,String d,double c, String secuencia,Model model)throws Exception
   {
       String space     = " ";
       String cero      = "0";
       String derecha   = "R";
       String izquierda = "L";
       obtenerTipoTransaccion();
       
        BeanGeneral dataTransferencia = model.TransferenciaSvc.getDataTransferencia(this.tipoCuentaOrigen);
        obtenerTipoProposito(dataTransferencia.getValor_05());
        String tipoCuenta = (dataTransferencia.getValor_03().equals("CC") ||dataTransferencia.getValor_03().equals("CPAG")) ? "D" : "S";
     
         return 
            "1"         +
            rellenar(dataTransferencia.getValor_01(),               cero,      15,       izquierda )   +
            "I"         +
            rellenar(space,                      space,     15,       derecha   )   +
            dataTransferencia.getValor_04()                                            +
            rellenar(dataTransferencia.getValor_05() ,               space,     10,       derecha   )   +
            this.getFecha("yyyyMMdd")                                               +
            rellenar(secuencia,                  space,       2 ,       derecha )   +  
            this.getFecha("yyyyMMdd")                                               +
            rellenar(String.valueOf(t),         cero,       6 ,       izquierda )   +  
            rellenar(String.valueOf(d )     ,     cero,     17,       izquierda )   +  
            rellenar(String.valueOf((long)c)    , cero,     17,       izquierda )   +
            rellenar(dataTransferencia.getValor_02(),              cero,      11,       izquierda )   +
            tipoCuenta         +
            rellenar(space,                    space,       149 ,       derecha );
   }



   public String getFecha(String formato)throws Exception{        
       String Fecha = "";
       try{
            SimpleDateFormat FMT = null;
            FMT = new SimpleDateFormat(formato);      
            Fecha = FMT.format(new Date());
            
       }catch(Exception e){
           throw new Exception("getFecha " + e.getMessage());
       }
       return Fecha.toUpperCase() ;
    }
  
  
    
/**
* M�todo que rellena los campos
* @autor.......fvillacob
* @throws......Exception
* @version.....1.0.
**/  
public static String rellenar(String cadena, String caracter, int tope, String posicion)throws Exception{
   try{
       int lon = cadena.length();
       if(tope>lon){
         for(int i=lon;i<tope;i++){
             if(posicion.equals("R"))    cadena += caracter;
             else                        cadena  = caracter + cadena;
         }
       }
       else
           cadena = Utility.Trunc(cadena, tope );
   }catch(Exception e){
       throw new Exception(e.getMessage());
   }
   return cadena;
}





    
    
    /**
     * Getter for property codigoBancoProveedor.
     * @return Value of property codigoBancoProveedor.
     */
    public java.lang.String getCodigoBancoProveedor() {
        return codigoBancoProveedor;
    }    
    
    /**
     * Setter for property codigoBancoProveedor.
     * @param codigoBancoProveedor New value of property codigoBancoProveedor.
     */
    public void setCodigoBancoProveedor(java.lang.String codigoBancoProveedor) {
        this.codigoBancoProveedor = codigoBancoProveedor;
    }    
    
    
    
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }    
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }    
    
    
    
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNitProveedor() {
        return nitProveedor;
    }    
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNitProveedor(java.lang.String nit) {
        this.nitProveedor = nit;
    }
    
    
    
    
    /**
     * Getter for property nitCliente.
     * @return Value of property nitCliente.
     */
    public java.lang.String getNitCliente() {
        return nitCliente;
    }
    
    /**
     * Setter for property nitCliente.
     * @param nitCliente New value of property nitCliente.
     */
    public void setNitCliente(java.lang.String nitCliente) {
        this.nitCliente = nitCliente;
    }
    
    
    
    
    
    /**
     * Getter for property noCuentaCliente.
     * @return Value of property noCuentaCliente.
     */
    public java.lang.String getNoCuentaCliente() {
        return noCuentaCliente;
    }
    
    /**
     * Setter for property noCuentaCliente.
     * @param noCuentaCliente New value of property noCuentaCliente.
     */
    public void setNoCuentaCliente(java.lang.String noCuentaCliente) {
        this.noCuentaCliente = noCuentaCliente;
    }
    
    
    
    
    /**
     * Getter for property noCuentaProveedor.
     * @return Value of property noCuentaProveedor.
     */
    public java.lang.String getNoCuentaProveedor() {
        return noCuentaProveedor;
    }
    
    /**
     * Setter for property noCuentaProveedor.
     * @param noCuentaProveedor New value of property noCuentaProveedor.
     */
    public void setNoCuentaProveedor(java.lang.String noCuentaProveedor) {
        this.noCuentaProveedor = noCuentaProveedor;
    }
    
    
    
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    
    
    
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    
    
    
    
    /**
     * Getter for property secuencia.
     * @return Value of property secuencia.
     */
    public java.lang.String getSecuencia() {
        return secuencia;
    }
    
    /**
     * Setter for property secuencia.
     * @param secuencia New value of property secuencia.
     */
    public void setSecuencia(java.lang.String secuencia) {
        this.secuencia = secuencia;
    }
    
    
    
    
    
    /**
     * Getter for property secuenciaNotaAdicional.
     * @return Value of property secuenciaNotaAdicional.
     */
    public java.lang.String getSecuenciaNotaAdicional() {
        return secuenciaNotaAdicional;
    }
    
    /**
     * Setter for property secuenciaNotaAdicional.
     * @param secuenciaNotaAdicional New value of property secuenciaNotaAdicional.
     */
    public void setSecuenciaNotaAdicional(java.lang.String secuenciaNotaAdicional) {
        this.secuenciaNotaAdicional = secuenciaNotaAdicional;
    }
    
    
    
    
    
    /**
     * Getter for property secuenciaProveedor.
     * @return Value of property secuenciaProveedor.
     */
    public java.lang.String getSecuenciaProveedor() {
        return secuenciaProveedor;
    }
    
    /**
     * Setter for property secuenciaProveedor.
     * @param secuenciaProveedor New value of property secuenciaProveedor.
     */
    public void setSecuenciaProveedor(java.lang.String secuenciaProveedor) {
        this.secuenciaProveedor = secuenciaProveedor;
    }
    
    
    
    
    
    /**
     * Getter for property tipoCuentaCliente.
     * @return Value of property tipoCuentaCliente.
     */
    public java.lang.String getTipoCuentaCliente() {
        return tipoCuentaCliente;
    }
    
    /**
     * Setter for property tipoCuentaCliente.
     * @param tipoCuentaCliente New value of property tipoCuentaCliente.
     */
    public void setTipoCuentaCliente(java.lang.String tipoCuentaCliente) {
        this.tipoCuentaCliente = tipoCuentaCliente;
    }
    
    
    
    
    
    /**
     * Getter for property tipoCuentaProveedor.
     * @return Value of property tipoCuentaProveedor.
     */
    public java.lang.String getTipoCuentaProveedor() {
        return tipoCuentaProveedor;
    }
    
    /**
     * Setter for property tipoCuentaProveedor.
     * @param tipoCuentaProveedor New value of property tipoCuentaProveedor.
     */
    public void setTipoCuentaProveedor(java.lang.String tipoCuentaProveedor) {
        this.tipoCuentaProveedor = tipoCuentaProveedor;
    }
    
    
    
    
    
    
    /**
     * Getter for property tipoOperacion.
     * @return Value of property tipoOperacion.
     */
    public java.lang.String getTipoOperacionCliente() {
        return tipoOperacionCliente;
    }
    
    /**
     * Setter for property tipoOperacion.
     * @param tipoOperacion New value of property tipoOperacion.
     */
    public void setTipoOperacionCliente(java.lang.String tipoOperacion) {
        this.tipoOperacionCliente = tipoOperacion;
    }
    
    
    
    
    
    /**
     * Getter for property tipoTransaccionProveedor.
     * @return Value of property tipoTransaccionProveedor.
     */
    public java.lang.String getTipoTransaccionProveedor() {
        return tipoTransaccionProveedor;
    }
    
    /**
     * Setter for property tipoTransaccionProveedor.
     * @param tipoTransaccionProveedor New value of property tipoTransaccionProveedor.
     */
    public void setTipoTransaccionProveedor(java.lang.String tipoTransaccionProveedor) {
        this.tipoTransaccionProveedor = tipoTransaccionProveedor;
    }
    
    
    
    
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public String getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(String valor) {
        this.valor = valor;
    }
    
    
    
    
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }    
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }   
    
    
    
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }    
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }    
    
    
    
    
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    
    
    
    
    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }
    
    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }
    
    
    
    
    
    /**
     * Getter for property cheque.
     * @return Value of property cheque.
     */
    public java.lang.String getCheque() {
        return cheque;
    }
    
    /**
     * Setter for property cheque.
     * @param cheque New value of property cheque.
     */
    public void setCheque(java.lang.String cheque) {
        this.cheque = cheque;
    }
    
    
    
    
    
    /**
     * Getter for property idMims.
     * @return Value of property idMims.
     */
    public java.lang.String getIdMims() {
        return idMims;
    }
    
    /**
     * Setter for property idMims.
     * @param idMims New value of property idMims.
     */
    public void setIdMims(java.lang.String idMims) {
        this.idMims = idMims;
    }
    
    
    
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    
    
    
    
    
    /**
     * Getter for property nombreCuenta.
     * @return Value of property nombreCuenta.
     */
    public java.lang.String getNombreCuenta() {
        return nombreCuenta;
    }
    
    /**
     * Setter for property nombreCuenta.
     * @param nombreCuenta New value of property nombreCuenta.
     */
    public void setNombreCuenta(java.lang.String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }
    
    
    
    
    
    
    /**
     * Getter for property cedulaCuenta.
     * @return Value of property cedulaCuenta.
     */
    public java.lang.String getCedulaCuenta() {
        return cedulaCuenta;
    }
    
    /**
     * Setter for property cedulaCuenta.
     * @param cedulaCuenta New value of property cedulaCuenta.
     */
    public void setCedulaCuenta(java.lang.String cedulaCuenta) {
        this.cedulaCuenta = cedulaCuenta;
    }
    
    public String equivalenciaTipoTransaction(String codeBanco, String tipo){
        String equivale = tipo;
        
        // BANCOLOMBIA
     
               if( tipo.equals( "CA"  ))  equivale = BANCOLOMBIA_CTA_AHORR0;
               if( tipo.equals( "CC"  ))  equivale = BANCOLOMBIA_CTA_CORRIENTE;                               
        
        return equivale;
    }
    
    /**
     * Getter for property tipoID.
     * @return Value of property tipoID.
     */
    public java.lang.String getTipoID() {
        return tipoID;
    }    
    
    /**
     * Setter for property tipoID.
     * @param tipoID New value of property tipoID.
     */
    public void setTipoID(java.lang.String tipoID) {
        this.tipoID = tipoID;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }    
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property formaDePago.
     * @return Value of property formaDePago.
     */
    public java.lang.String getFormaDePago() {
        return formaDePago;
    }    
    
    /**
     * Setter for property formaDePago.
     * @param formaDePago New value of property formaDePago.
     */
    public void setFormaDePago(java.lang.String formaDePago) {
        this.formaDePago = formaDePago;
    }
    
   /**
     * Getter for property codBanco.
     * @return Value of property codBanco.
     */
    public java.lang.String getCodBanco() {
        return codBanco;
    }    
    
    /**
     * Setter for property codBanco.
     * @param codBanco New value of property codBanco.
     */
    public void setCodBanco(java.lang.String codBanco) {
        this.codBanco = codBanco;
    } 
    
    /**
     * Getter for property noCuentaBBVA.
     * @return Value of property noCuentaBBVA.
     */
    public java.lang.String getNoCuentaBBVA() {
        return noCuentaBBVA;
    }    
    
    /**
     * Setter for property noCuentaBBVA.
     * @param noCuentaBBVA New value of property noCuentaBBVA.
     */
    public void setNoCuentaBBVA(java.lang.String noCuentaBBVA) {
        this.noCuentaBBVA = noCuentaBBVA;
    } 
    
    /**
     * Getter for property tipCuenta.
     * @return Value of property tipCuenta.
     */
    public java.lang.String getTipCuenta() {
        return tipCuenta;
    }    
    
    /**
     * Setter for property tipCuenta.
     * @param tipCuenta New value of property tipCuenta.
     */
    public void setTipCuenta(java.lang.String tipCuenta) {
        this.tipCuenta = tipCuenta;
    } 
    
    /**
     * Getter for property noCuenta.
     * @return Value of property noCuenta.
     */
    public java.lang.String getNoCuenta() {
        return noCuenta;
    }    
    
    /**
     * Setter for property noCuenta.
     * @param noCuenta New value of property noCuenta.
     */
    public void setNoCuenta(java.lang.String noCuenta) {
        this.noCuenta = noCuenta;
    } 
    
   /**
     * Getter for property valorEntero.
     * @return Value of property valorEntero.
     */
    public java.lang.String getValEntera() {
        return valorEntero;
    }    
    
    /**
     * Setter for property valorEntero.
     * @param valorEntero New value of property valorEntero.
     */
    public void setValEntera(java.lang.String valorEntero) {
        this.valorEntero = valorEntero;
    } 
    
    /**
     * Getter for property valorDecimal.
     * @return Value of property valorDecimal.
     */
    public java.lang.String getValDecimal() {
        return valorDecimal;
    }    
    
    /**
     * Setter for property valorDecimal.
     * @param valorDecimal New value of property valorDecimal.
     */
    public void setValDecimal(java.lang.String valorDecimal) {
        this.valorDecimal = valorDecimal;
    }  
    
    /**
     * Getter for property codOficinaPagadora.
     * @return Value of property codOficinaPagadora.
     */
    public java.lang.String getCodOficinaPagadora() {
        return codOficinaPagadora;
    }    
    
    /**
     * Setter for property codOficinaPagadora.
     * @param codOficinaPagadora New value of property codOficinaPagadora.
     */
    public void setCodOficinaPagadora(java.lang.String codOficinaPagadora) {
        this.codOficinaPagadora = codOficinaPagadora;
    }  
    
    /**
     * Getter for property nombreBeneficiario.
     * @return Value of property nombreBeneficiario.
     */
    public java.lang.String getNomBeneficiario() {
        return nombreBeneficiario;
    }    
    
    /**
     * Setter for property nombreBeneficiario.
     * @param nombreBeneficiario New value of property nombreBeneficiario.
     */
    public void setNomBeneficiario(java.lang.String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }  
    
    /**
     * Getter for property direccion1.
     * @return Value of property direccion1.
     */
    public java.lang.String getDireccion1() {
        return direccion1;
    }    
    
    /**
     * Setter for property direccion1.
     * @param direccion1 New value of property direccion1.
     */
    public void setDireccion1(java.lang.String direccion1) {
        this.direccion1 = direccion1;
    }  
    
     /**
     * Getter for property direccion2.
     * @return Value of property direccion2.
     */
    public java.lang.String getDireccion2() {
        return direccion2;
    }    
    
    /**
     * Setter for property direccion2.
     * @param direccion2 New value of property direccion2.
     */
    public void setDireccion2(java.lang.String direccion2) {
        this.direccion2 = direccion2;
    }  
    
    /**
     * Getter for property email.
     * @return Value of property email.
     */
    public java.lang.String getEmail() {
        return email;
    }    
    
    /**
     * Setter for property email.
     * @param email New value of property email.
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    } 
    
     /**
     * Getter for property concepto1.
     * @return Value of property concepto1.
     */
    public java.lang.String getConcepto1l() {
        return concepto1;
    }    
    
    /**
     * Setter for property concepto1.
     * @param concepto1 New value of property concepto1.
     */
    public void setConcepto1(java.lang.String concepto1) {
        this.concepto1 = concepto1;
    }
    
      
    /**
     * Get the value of tipoCuentaOrigen
     *
     * @return the value of tipoCuentaOrigen
     */
    public String getTipoCuentaOrigen() {
        return tipoCuentaOrigen;
    }

    /**
     * Set the value of tipoCuentaOrigen
     *
     * @param tipoCuentaOrigen new value of tipoCuentaOrigen
     */
    public void setTipoCuentaOrigen(String tipoCuentaOrigen) {
        this.tipoCuentaOrigen = tipoCuentaOrigen;
    }
    
    
    
    /**
     * Obtiene el tipo de tranasaccion
     * @param codeBanco
     * @param listaAnticipos 
     */
    public void obtenerTipoTransaccion(){
            if(tipoCuentaOrigen.equals("CPAG")){
                BANCOLOMBIA_TIPO_TRANSACTION = BANCOLOMBIA_CPAG_TIPO_TRANSACTION;
            }
    }

    
    
    /**
   * M�todo que devuelve la linea para el archivo BBVA
   * @autor.......tmolina
   * @throws......Exception
   * @version.....1.0.
   **/  
  public String getColumnasBBVA(){     //2008-08-28
     
     return this.tipoID             +
             this.documento          +
             this.formaDePago        +
             this.codBanco           +
             this.noCuentaBBVA       +
             this.tipCuenta          +
             this.noCuenta           +
             this.valorEntero        +
             this.valorDecimal       +
             this.fecha              +
             this.codOficinaPagadora +
             this.nombreBeneficiario +
             this.direccion1         +
             this.direccion2         +
             this.email              +
             this.concepto1;
  }
  
  /**
   * M�todo que formatea la longitus de los campos para el archivo al banco BBVA
   * @autor.......tmolina
   * @throws......Exception
   * @version.....1.0.
   **/  
  public void formatearLongitudBBVA()throws Exception{   
       try{ 
            String space     = "&nbsp";
            String cero      = "0";
            String derecha   = "R";
            String izquierda = "L";
            
            this.documento      = this.rellenar(this.documento, cero, 16, izquierda);
            this.noCuenta       = this.rellenar(this.noCuenta, space, 17, derecha);
            this.valorEntero    = this.rellenar(this.valorEntero, cero, 13, izquierda);
            this.valorDecimal   = this.rellenar(this.valorDecimal, cero, 2, izquierda);
            this.nombreBeneficiario = this.rellenar(this.nombreBeneficiario, space, 36, derecha);
            this.direccion1     = this.rellenar(this.direccion1, space, 36, derecha);
            this.direccion2     = this.rellenar(this.direccion2, space, 36, derecha);
            this.email          = this.rellenar(this.email, space, 48, derecha);
            this.concepto1      = this.rellenar(this.concepto1, space, 40, derecha);
            
       }catch(Exception e){
           throw new Exception( " formatearLongitudBBVA " + e.getMessage());
       }
   }
  
      /**
     * Obtiene el tipo de tranasaccion 
     * @param proposito
     */
    public void obtenerTipoProposito(String proposito) {
        BANCOLOMBIA_PROPOSITO = proposito;
    }
  
  }
