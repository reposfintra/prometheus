/*
 * ConfigReporte.java
 *
 * Created on 31 de agosto de 2005, 04:51 PM
 */

package com.tsp.operation.model.beans;

import java.util.Vector;

/**
 *
 * @author  Alejandro
 */
public class ConfigReporte {
    
    private String nombreCliente;
    private String codigoCliente;
    private Vector reportes;
    
    /** Creates a new instance of ConfigReporte */
    public ConfigReporte() {
        reportes = new Vector();
    }
    
    public boolean equals(Object otro){
        if ( otro instanceof ConfigReporte ){
            return ((ConfigReporte)otro).codigoCliente.equals(codigoCliente);
        }
        else if ( otro instanceof String ){
            return this.codigoCliente.equals(otro);
        }
        return false;
    }
    
    /**
     * Getter for property codigoCliente.
     * @return Value of property codigoCliente.
     */
    public java.lang.String getCodigoCliente() {
        return codigoCliente;
    }
    
    /**
     * Setter for property codigoCliente.
     * @param codigoCliente New value of property codigoCliente.
     */
    public void setCodigoCliente(java.lang.String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
    
    /**
     * Getter for property nombreCliente.
     * @return Value of property nombreCliente.
     */
    public java.lang.String getNombreCliente() {
        return nombreCliente;
    }
    
    /**
     * Setter for property nombreCliente.
     * @param nombreCliente New value of property nombreCliente.
     */
    public void setNombreCliente(java.lang.String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }
    
    /**
     * Getter for property reportes.
     * @return Value of property reportes.
     */
    public java.util.Vector getReportes() {
        return reportes;
    }
    
    /**
     * Setter for property reportes.
     * @param reportes New value of property reportes.
     */
    public void setReportes(java.util.Vector reportes) {
        this.reportes = reportes;
    }
    
    public void agregarReporte(String codigo, String nombre, String campos, String camposVisibles){
        reportes.addElement(new Reporte(codigo, nombre, campos, camposVisibles));
    }
    
    public class Reporte {
        
        private String codigo;
        private String nombre;
        private String camposVisibles;
        private String campos;
        
        public Reporte(String codigo, String nombre, String campos, String camposVisibles){
            this.codigo = codigo;
            this.nombre = nombre;
            this.camposVisibles = camposVisibles;
            this.campos = campos;            
        }
        
        /**
         * Getter for property campos.
         * @return Value of property campos.
         */
        public java.lang.String getCampos() {
            StringBuffer sb = new StringBuffer();
            String cmps [] = campos.split(",");
            int caracteresAEliminarAlFinal = 0;
            for( int i=0,c=0; i<cmps.length; i++,c++ ){
                sb.append(cmps[i]);
                sb.append(",");
                caracteresAEliminarAlFinal = 1;
                if ( c >= 5 ){
                    sb.append(" ");
                    c = 0;
                    caracteresAEliminarAlFinal = 2;
                }
            }
            sb.delete(sb.length()-caracteresAEliminarAlFinal, sb.length());
            return sb.toString();
        }
        
        /**
         * Setter for property campos.
         * @param campos New value of property campos.
         */
        public void setCampos(java.lang.String campos) {
            this.campos = campos;
        }
        
        /**
         * Getter for property codigo.
         * @return Value of property codigo.
         */
        public java.lang.String getCodigo() {
            return codigo;
        }
        
        /**
         * Setter for property codigo.
         * @param codigo New value of property codigo.
         */
        public void setCodigo(java.lang.String codigo) {
            this.codigo = codigo;
        }
        
        /**
         * Getter for property nombre.
         * @return Value of property nombre.
         */
        public java.lang.String getNombre() {
            return nombre;
        }
        
        /**
         * Setter for property nombre.
         * @param nombre New value of property nombre.
         */
        public void setNombre(java.lang.String nombre) {
            this.nombre = nombre;
        }
        
        /**
         * Getter for property camposVisibles.
         * @return Value of property camposVisibles.
         */
        public java.lang.String getCamposVisibles() {
            return camposVisibles;
        }
        
        /**
         * Setter for property camposVisibles.
         * @param camposVisibles New value of property camposVisibles.
         */
        public void setCamposVisibles(java.lang.String camposVisibles) {
            this.camposVisibles = camposVisibles;
        }
        
    }
    
    
    
}
