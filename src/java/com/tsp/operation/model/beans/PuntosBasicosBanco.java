package com.tsp.operation.model.beans;

/**
 * Bean para la tabla fin.punto_basico_banco
 * @author darrieta - geotech
 */
public class PuntosBasicosBanco {
    
    private String banco;
    private String lineaCredito;
    private double puntosBasicos;
    private String dstrct;
    private String nombreBanco;
    private String descripcionLinea;

    /**
     * Get the value of descripcionLinea
     *
     * @return the value of descripcionLinea
     */
    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    /**
     * Set the value of descripcionLinea
     *
     * @param descripcionLinea new value of descripcionLinea
     */
    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }

    /**
     * Get the value of nombreBanco
     *
     * @return the value of nombreBanco
     */
    public String getNombreBanco() {
        return nombreBanco;
    }

    /**
     * Set the value of nombreBanco
     *
     * @param nombreBanco new value of nombreBanco
     */
    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }


    /**
     * Get the value of dstrct
     *
     * @return the value of dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * Set the value of dstrct
     *
     * @param dstrct new value of dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }


    /**
     * Get the value of puntos_basicos
     *
     * @return the value of puntos_basicos
     */
    public double getPuntosBasicos() {
        return puntosBasicos;
    }

    /**
     * Set the value of puntos_basicos
     *
     * @param puntos_basicos new value of puntos_basicos
     */
    public void setPuntosBasicos(double puntos_basicos) {
        this.puntosBasicos = puntos_basicos;
    }

    /**
     * Get the value of linea_credito
     *
     * @return the value of linea_credito
     */
    public String getLineaCredito() {
        return lineaCredito;
    }

    /**
     * Set the value of linea_credito
     *
     * @param linea_credito new value of linea_credito
     */
    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }


    /**
     * Get the value of banco
     *
     * @return the value of banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Set the value of banco
     *
     * @param banco new value of banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    
    
}
