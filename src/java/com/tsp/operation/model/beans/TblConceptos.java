/**
 * Nombre        TblConceptos.java
 * Descripci�n   Datos de la tabla de tblcon.
 * Autor         Mario Fontalvo Solano
 * Fecha         8 de marzo de 2006, 03:07 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.beans;


public class TblConceptos {
    
    private String reg_status;
    private String dstrct;
    private String concept_code;
    private String concept_desc;
    private String ind_application;
    private String account;
    private String codigo_migracion;
    private String visible;
    private String modif;
    private int ind_signo;
    private String concept_class;
    private String class_desc;
    private String base;
    
    private String creation_date;
    private String creation_user;
    private String last_update;
    private String user_update;
    
    
    private String tipo;
    private double vlr;
    private String currency;
    private String descuento_fijo;
    private String tipo_cuenta;
    private String asocia_cheque;
    private String ingreso_costo;
    
    /** Crea una nueva instancia de  TConceptos */
    //Inicializacion de variables
    public TblConceptos() {
        ind_application  = "V";
        codigo_migracion = "";
        visible          = "N";
        modif            = "N";
        ind_signo        = 1;
        concept_class    = "";
        tipo_cuenta      = "E";
        asocia_cheque    = "N";
        tipo             = "V";
        vlr              = 0;
        currency         = "";
        descuento_fijo   = "N"; 
        tipo_cuenta      = "";
        asocia_cheque    = "";
        ingreso_costo    = "C";
    }
    
    /**
     * Getter for property account.
     * @return Value of property account.
     */
    public java.lang.String getAccount() {
        return account;
    }
    
    /**
     * Setter for property account.
     * @param account New value of property account.
     */
    public void setAccount(java.lang.String account) {
        this.account = account;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property codigo_migracion.
     * @return Value of property codigo_migracion.
     */
    public java.lang.String getCodigo_migracion() {
        return codigo_migracion;
    }
    
    /**
     * Setter for property codigo_migracion.
     * @param codigo_migracion New value of property codigo_migracion.
     */
    public void setCodigo_migracion(java.lang.String codigo_migracion) {
        this.codigo_migracion = codigo_migracion;
    }
    
    /**
     * Getter for property concept_class.
     * @return Value of property concept_class.
     */
    public java.lang.String getConcept_class() {
        return concept_class;
    }
    
    /**
     * Setter for property concept_class.
     * @param concept_class New value of property concept_class.
     */
    public void setConcept_class(java.lang.String concept_class) {
        this.concept_class = concept_class;
    }
    
    /**
     * Getter for property concept_code.
     * @return Value of property concept_code.
     */
    public java.lang.String getConcept_code() {
        return concept_code;
    }
    
    /**
     * Setter for property concept_code.
     * @param concept_code New value of property concept_code.
     */
    public void setConcept_code(java.lang.String concept_code) {
        this.concept_code = concept_code;
    }
    
    /**
     * Getter for property concept_desc.
     * @return Value of property concept_desc.
     */
    public java.lang.String getConcept_desc() {
        return concept_desc;
    }
    
    /**
     * Setter for property concept_desc.
     * @param concept_desc New value of property concept_desc.
     */
    public void setConcept_desc(java.lang.String concept_desc) {
        this.concept_desc = concept_desc;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property ind_application.
     * @return Value of property ind_application.
     */
    public java.lang.String getInd_application() {
        return ind_application;
    }
    
    /**
     * Setter for property ind_application.
     * @param ind_application New value of property ind_application.
     */
    public void setInd_application(java.lang.String ind_application) {
        this.ind_application = ind_application;
    }
    
    /**
     * Getter for property ind_signo.
     * @return Value of property ind_signo.
     */
    public int getInd_signo() {
        return ind_signo;
    }
    
    /**
     * Setter for property ind_signo.
     * @param ind_signo New value of property ind_signo.
     */
    public void setInd_signo(int ind_signo) {
        this.ind_signo = ind_signo;
    }
    
    /**
     * Getter for property modif.
     * @return Value of property modif.
     */
    public java.lang.String getModif() {
        return modif;
    }
    
    /**
     * Setter for property modif.
     * @param modif New value of property modif.
     */
    public void setModif(java.lang.String modif) {
        this.modif = modif;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property visible.
     * @return Value of property visible.
     */
    public java.lang.String getVisible() {
        return visible;
    }
    
    /**
     * Setter for property visible.
     * @param visible New value of property visible.
     */
    public void setVisible(java.lang.String visible) {
        this.visible = visible;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property class_desc.
     * @return Value of property class_desc.
     */
    public java.lang.String getClass_desc() {
        return class_desc;
    }
    
    /**
     * Setter for property class_desc.
     * @param class_desc New value of property class_desc.
     */
    public void setClass_desc(java.lang.String class_desc) {
        this.class_desc = class_desc;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public double getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }
    
    /**
     * Getter for property currency.
     * @return Value of property currency.
     */
    public java.lang.String getCurrency() {
        return currency;
    }
    
    /**
     * Setter for property currency.
     * @param currency New value of property currency.
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }
    
    /**
     * Getter for property descuento_fijo.
     * @return Value of property descuento_fijo.
     */
    public java.lang.String getDescuento_fijo() {
        return descuento_fijo;
    }
    
    /**
     * Setter for property descuento_fijo.
     * @param descuento_fijo New value of property descuento_fijo.
     */
    public void setDescuento_fijo(java.lang.String descuento_fijo) {
        this.descuento_fijo = descuento_fijo;
    }
    
    /**
     * Getter for property tipo_cuenta.
     * @return Value of property tipo_cuenta.
     */
    public java.lang.String getTipo_cuenta() {
        return tipo_cuenta;
    }
    
    /**
     * Setter for property tipo_cuenta.
     * @param tipo_cuenta New value of property tipo_cuenta.
     */
    public void setTipo_cuenta(java.lang.String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }
    
    /**
     * Getter for property asocia_cheque.
     * @return Value of property asocia_cheque.
     */
    public java.lang.String getAsocia_cheque() {
        return asocia_cheque;
    }
    
    /**
     * Setter for property asocia_cheque.
     * @param asocia_cheque New value of property asocia_cheque.
     */
    public void setAsocia_cheque(java.lang.String asocia_cheque) {
        this.asocia_cheque = asocia_cheque;
    }
    
    /**
     * Getter for property ingreso_costo.
     * @return Value of property ingreso_costo.
     */
    public java.lang.String getIngreso_costo() {
        return ingreso_costo;
    }
    
    /**
     * Setter for property ingreso_costo.
     * @param ingreso_costo New value of property ingreso_costo.
     */
    public void setIngreso_costo(java.lang.String ingreso_costo) {
        this.ingreso_costo = ingreso_costo;
    }
    
}
