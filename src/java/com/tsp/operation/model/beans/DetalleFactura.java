/****************************************************************************************
 * Nombre clase : ............... FlotaUtilizada.java                                   *
 * Descripcion :................. Bean utilizado para el reporte de extractos.          * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                          *
 * Fecha :....................... 01 de enero de 2006, 08:00 AM                         *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ****************************************************************************************/

package com.tsp.operation.model.beans;
import java.io.Serializable;
import java.text.DecimalFormat;


public class DetalleFactura implements Serializable{
    
    private String facturaNo = ""; //EXT_INV_NO
    private String itemNo = "";//INV_ITEM_NO
    private String itemDesc = "";//INV_ITEM_DESC
    private double itemValor = 0;//LOC_VAL_INVD
    private String moneda;//CURRENCY_TYPE
    //private double impuesto = 0;//impuesto Item
    
    private double iva  = 0;//Retencion
    private double riva = 0;//Retencion
    private double rica = 0;//Retencion
    private double rfte = 0;//Retencion
    private double neto = 0;//Retencion
    
    private double totalFacturaProveedor = 0;
    private String totalFactProvString   = "";
    private String concepto;
    
    

    
    /** Creates a new instance of DetalleFactura */
    public DetalleFactura() {
    }
    
    /**
     * Getter for property facturaNo.
     * @return Value of property facturaNo.
     */
    public java.lang.String getFacturaNo() {
        return facturaNo;
    }
    
    /**
     * Setter for property facturaNo.
     * @param facturaNo New value of property facturaNo.
     */
    public void setFacturaNo(java.lang.String facturaNo) {
        this.facturaNo = facturaNo;
    }
    

    
    /**
     * Getter for property itemDesc.
     * @return Value of property itemDesc.
     */
    public java.lang.String getItemDesc() {
        return itemDesc;
    }
    
    /**
     * Setter for property itemDesc.
     * @param itemDesc New value of property itemDesc.
     */
    public void setItemDesc(java.lang.String itemDesc) {
        this.itemDesc = itemDesc;
    }
    
    /**
     * Getter for property itemNo.
     * @return Value of property itemNo.
     */
    public java.lang.String getItemNo() {
        return itemNo;
    }
    
    /**
     * Setter for property itemNo.
     * @param itemNo New value of property itemNo.
     */
    public void setItemNo(java.lang.String itemNo) {
        this.itemNo = itemNo;
    }
    
    /**
     * Getter for property itemValor.
     * @return Value of property itemValor.
     */
    public double getItemValor() {
        return itemValor;
    }
    
    /**
     * Setter for property itemValor.
     * @param itemValor New value of property itemValor.
     */
    public void setItemValor(double itemValor) {
        this.itemValor = itemValor;
    }
    
    /**
     * Getter for property modena.
     * @return Value of property modena.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property modena.
     * @param modena New value of property modena.
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
  
    
    /**
     * Getter for property riva.
     * @return Value of property riva.
     */
    public double getRiva() {
        return riva;
    }
    
    /**
     * Setter for property riva.
     * @param riva New value of property riva.
     */
    public void setRiva(double riva) {
        this.riva = riva;
    }
    
    /**
     * Getter for property rica.
     * @return Value of property rica.
     */
    public double getRica() {
        return rica;
    }
    
    /**
     * Setter for property rica.
     * @param rica New value of property rica.
     */
    public void setRica(double rica) {
        this.rica = rica;
    }
    
    /**
     * Getter for property rfte.
     * @return Value of property rfte.
     */
    public double getRfte() {
        return rfte;
    }
    
    /**
     * Setter for property rfte.
     * @param rfte New value of property rfte.
     */
    public void setRfte(double rfte) {
        this.rfte = rfte;
    }
    
    /**
     * Getter for property iva.
     * @return Value of property iva.
     */
    public double getIva() {
        return iva;
    }
    
    /**
     * Setter for property iva.
     * @param iva New value of property iva.
     */
    public void setIva(double iva) {
        this.iva = iva;
    }
    
    /**
     * Getter for property neto.
     * @return Value of property neto.
     */
    public double getNeto() {
        return neto;
    }
    
    /**
     * Setter for property neto.
     * @param neto New value of property neto.
     */
    public void setNeto(double neto) {
        this.neto = neto;
    }
    
    /**
     * Getter for property totalFacturaProveedor.
     * @return Value of property totalFacturaProveedor.
     */
    public double getTotalFacturaProveedor() {
        return totalFacturaProveedor;
    }
    
    /**
     * Setter for property totalFacturaProveedor.
     * @param totalFacturaProveedor New value of property totalFacturaProveedor.
     */
    public void setTotalFacturaProveedor(double totalFacturaProveedor) {        
        DecimalFormat form = new DecimalFormat("#,##0.00;-#,##0.00");        
        this.totalFacturaProveedor = totalFacturaProveedor;
        if (totalFacturaProveedor!=0)
            this.totalFactProvString = form.format(totalFacturaProveedor);
        else
            this.totalFactProvString = "";
    }
    
    /**
     * Getter for property totalFactProvString.
     * @return Value of property totalFactProvString.
     */
    public java.lang.String getTotalFactProvString() {
        return totalFactProvString;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
}
