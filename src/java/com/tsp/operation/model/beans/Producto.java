/************************************************************************
 * Nombre clase: Producto.java                                          *
 * Descripci�n: Clase que maneja los atributos del objeto               *
 *              de los productos.                                       *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: Created on 17 de octubre de 2005, 10:38 AM                    *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Producto {
    private String codigo;
    private String descripcion;
    private String cliente;
    private String unidad;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String distrito;
    private String base;    
    private boolean utilizado;
    private String nota_debito;
    private String discrepancia;
    private String numpla;
    private String numrem;
    private String ubicacion;
    /** Creates a new instance of Producto */
    public Producto() {
    }

    public java.lang.String getBase() {
        return base;
    }

    public void setBase(java.lang.String base) {
        this.base = base;
    }

    public java.lang.String getCodigo() {
        return codigo;
    }

    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }

    public java.lang.String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }

    public java.lang.String getDistrito() {
        return distrito;
    }

    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }

    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public java.lang.String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public java.lang.String getUnidad() {
        return unidad;
    }

    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }

    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }

    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }

    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property utilizado.
     * @return Value of property utilizado.
     */
    public boolean isUtilizado() {
        return utilizado;
    }
    
    /**
     * Setter for property utilizado.
     * @param utilizado New value of property utilizado.
     */
    public void setUtilizado(boolean utilizado) {
        this.utilizado = utilizado;
    }
    
    /**
     * Getter for property nota_debito.
     * @return Value of property nota_debito.
     */
    public java.lang.String getNota_debito() {
        return nota_debito;
    }
    
    /**
     * Setter for property nota_debito.
     * @param nota_debito New value of property nota_debito.
     */
    public void setNota_debito(java.lang.String nota_debito) {
        this.nota_debito = nota_debito;
    }
    
    /**
     * Getter for property discrepancia.
     * @return Value of property discrepancia.
     */
    public java.lang.String getDiscrepancia() {
        return discrepancia;
    }
    
    /**
     * Setter for property discrepancia.
     * @param discrepancia New value of property discrepancia.
     */
    public void setDiscrepancia(java.lang.String discrepancia) {
        this.discrepancia = discrepancia;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property ubicacion.
     * @return Value of property ubicacion.
     */
    public java.lang.String getUbicacion() {
        return ubicacion;
    }
    
    /**
     * Setter for property ubicacion.
     * @param ubicacion New value of property ubicacion.
     */
    public void setUbicacion(java.lang.String ubicacion) {
        this.ubicacion = ubicacion;
    }

    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente () {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente (java.lang.String cliente) {
        this.cliente = cliente;
    }

}
