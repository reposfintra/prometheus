/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class FacturaFintra {


    private String   dstrct;
    private String   tipo_documento;
    private String   documento;
    private int      item;
    private String   descripcion;
    private double   valor_factura;
    private double   valor_saldo;
    private double   valor_item;
    private double   valor_ingreso;
    private String   tipo_operacion;
    private String   numero_operacion;
    private String   tipo_factura;
    private String   factura;
    private String   clase;
    private String   dstrct_egreso;
    private String   branch_code_egreso;
    private String   bank_account_no_egreso;
    private String   document_no_egreso;
    private String   item_no_egreso;


    public FacturaFintra ()   {
    }


    public static FacturaFintra load(java.sql.ResultSet rs)throws java.sql.SQLException{

        FacturaFintra facturaFintra = new FacturaFintra();

        facturaFintra.setDstrct( rs.getString("Dstrct") ) ;
        facturaFintra.setTipo_documento( rs.getString("tipo_documento") ) ;
        facturaFintra.setDocumento( rs.getString("documento") ) ;
        facturaFintra.setItem( rs.getInt("item") ) ;
        facturaFintra.setDescripcion( rs.getString("descripcion") ) ;

        facturaFintra.setValor_factura( rs.getDouble("valor_factura") ) ;
        facturaFintra.setValor_saldo (rs.getDouble("valor_saldo") ) ;

        facturaFintra.setValor_item( rs.getDouble("valor_item") ) ;
        facturaFintra.setValor_ingreso( rs.getDouble("valor_ingreso") ) ;
        facturaFintra.setTipo_operacion( rs.getString("tipo_operacion") ) ;
        facturaFintra.setNumero_operacion( rs.getString("numero_operacion") ) ;
        facturaFintra.setTipo_factura( rs.getString("tipo_factura") ) ;
        facturaFintra.setFactura( rs.getString("factura") ) ;
        facturaFintra.setClase( rs.getString("clase") ) ;
        facturaFintra.setDstrct_egreso( rs.getString("dstrct_egreso") ) ;
        facturaFintra.setBranch_code_egreso( rs.getString("branch_code_egreso") ) ;
        facturaFintra.setBank_account_no_egreso( rs.getString("bank_account_no_egreso") ) ;
        facturaFintra.setDocument_no_egreso( rs.getString("document_no_egreso") ) ;
        facturaFintra.setItem_no_egreso( rs.getString("item_no_egreso") ) ;

        return facturaFintra;
    }



    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the item
     */
    public int getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(int item) {
        this.item = item;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the valor_item
     */
    public double getValor_item() {
        return valor_item;
    }

    /**
     * @param valor_item the valor_item to set
     */
    public void setValor_item(double valor_item) {
        this.valor_item = valor_item;
    }

    /**
     * @return the valor_ingreso
     */
    public double getValor_ingreso() {
        return valor_ingreso;
    }

    /**
     * @param valor_ingreso the valor_ingreso to set
     */
    public void setValor_ingreso(double valor_ingreso) {
        this.valor_ingreso = valor_ingreso;
    }

    /**
     * @return the tipo_operacion
     */
    public String getTipo_operacion() {
        return tipo_operacion;
    }

    /**
     * @param tipo_operacion the tipo_operacion to set
     */
    public void setTipo_operacion(String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    /**
     * @return the numero_operacion
     */
    public String getNumero_operacion() {
        return numero_operacion;
    }

    /**
     * @param numero_operacion the numero_operacion to set
     */
    public void setNumero_operacion(String numero_operacion) {
        this.numero_operacion = numero_operacion;
    }

    /**
     * @return the tipo_factura
     */
    public String getTipo_factura() {
        return tipo_factura;
    }

    /**
     * @param tipo_factura the tipo_factura to set
     */
    public void setTipo_factura(String tipo_factura) {
        this.tipo_factura = tipo_factura;
    }

    /**
     * @return the factura
     */
    public String getFactura() {
        return factura;
    }

    /**
     * @param factura the factura to set
     */
    public void setFactura(String factura) {
        this.factura = factura;
    }

    /**
     * @return the clase
     */
    public String getClase() {
        return clase;
    }

    /**
     * @param clase the clase to set
     */
    public void setClase(String clase) {
        this.clase = clase;
    }

    /**
     * @return the dstrct_egreso
     */
    public String getDstrct_egreso() {
        return dstrct_egreso;
    }

    /**
     * @param dstrct_egreso the dstrct_egreso to set
     */
    public void setDstrct_egreso(String dstrct_egreso) {
        this.dstrct_egreso = dstrct_egreso;
    }

    /**
     * @return the branch_code_egreso
     */
    public String getBranch_code_egreso() {
        return branch_code_egreso;
    }

    /**
     * @param branch_code_egreso the branch_code_egreso to set
     */
    public void setBranch_code_egreso(String branch_code_egreso) {
        this.branch_code_egreso = branch_code_egreso;
    }

    /**
     * @return the bank_account_no_egreso
     */
    public String getBank_account_no_egreso() {
        return bank_account_no_egreso;
    }

    /**
     * @param bank_account_no_egreso the bank_account_no_egreso to set
     */
    public void setBank_account_no_egreso(String bank_account_no_egreso) {
        this.bank_account_no_egreso = bank_account_no_egreso;
    }

    /**
     * @return the document_no_egreso
     */
    public String getDocument_no_egreso() {
        return document_no_egreso;
    }

    /**
     * @param document_no_egreso the document_no_egreso to set
     */
    public void setDocument_no_egreso(String document_no_egreso) {
        this.document_no_egreso = document_no_egreso;
    }

    /**
     * @return the item_no_egreso
     */
    public String getItem_no_egreso() {
        return item_no_egreso;
    }

    /**
     * @param item_no_egreso the item_no_egreso to set
     */
    public void setItem_no_egreso(String item_no_egreso) {
        this.item_no_egreso = item_no_egreso;
    }

    /**
     * @return the valor_factura
     */
    public double getValor_factura() {
        return valor_factura;
    }

    /**
     * @param valor_factura the valor_factura to set
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }

    /**
     * @return the valor_saldo
     */
    public double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo(double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }





}
