/**************************************************************************
 * Nombre: ......................InfoActividad.java                       *
 * Descripci�n: .................Beans de acuerdo especial.               *
 * Autor:........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:........................31 de agosto de 2005, 10:43 AM        *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;


public class InfoActividad implements Serializable {
    private String dstrct;
    private String codactividad;
    private String codcliente;
    private String fecini;
    private String fecfin;
    private double duracion;
    private String documento;
    private double tiempodemora;
    private String causademora;
    private String resdemora;
    private String feccierre;
    private String observacion;
    private String estado;
    private String base;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String cantrealizada;
    private String cantplaneada;
    private String numpla;
    private String numrem;
    
    //nuevo 19-12-2005
    
    private String referencia1;
    private String referencia2;
    private int refnumerica1;
    private int refnumerica2;
    
    
    /** Creates a new instance of InfoActividad */
    public static InfoActividad load(ResultSet rs)throws SQLException{ 
        InfoActividad infact = new InfoActividad();
        infact.setDstrct( rs.getString("dstrct"));
        infact.setCodActividad( rs.getString("cod_actividad"));
        infact.setCodCliente(rs.getString("codcli"));
        infact.setFecini((!rs.getString("fecinicio").equals("0099-01-01 00:00:00"))?rs.getString("fecinicio"):"");
        infact.setFecfin((!rs.getString("fecfin").equals("0099-01-01 00:00:00"))?rs.getString("fecfin"):"");
        infact.setDuracion(rs.getDouble("duracion"));
        infact.setDocumento(rs.getString("documento"));
        infact.setTiempoDemora(rs.getDouble("tiempodemora"));
        infact.setCausaDemora(rs.getString("causademora"));
        infact.setResdemora(rs.getString("resdemora"));
        infact.setCantplaneada(rs.getString("cantplaneada"));
        infact.setCantrealizada(rs.getString("cantrealizada"));
        
        infact.setReferencia1(rs.getString("referencia1"));
        infact.setReferencia2(rs.getString("referencia2"));
        infact.setRefnumerica1(rs.getInt("refnumerica1"));
        infact.setRefnumerica2(rs.getInt("refnumerica2"));
        infact.setFeccierre((!rs.getString("feccierre").equals("0099-01-01 00:00:00"))?rs.getString("feccierre"):"");
        infact.setObservacion(rs.getString("observacion"));        
        return infact;
    }
    
    public void setDstrct(String D){
        this.dstrct = D;
    }
    
    public String getDstrct(){
        return dstrct;
    }
    
    public void setCodActividad(String codact){
        this.codactividad = codact;
    }
    
    public String getCodActividad(){
        return codactividad;
    }
 
    public void setCodCliente(String codcli){
        this.codcliente = codcli;
    }
    
    public String getCodCliente(){
        return codcliente;
    }
    
   
    
    public void setFecini(String fecini){
        this.fecini = fecini;
    }
    
    public String getFecini (){
        return fecini;
    }
    public void setFecfin(String fecfin){
        this.fecfin=fecfin;
    }
    public String getFecfin(){
        return fecfin;
    }
    public void setDuracion(double dur){
        this.duracion=dur;
    }
    public double getDuracion(){
        return duracion;
    }
    public void setDocumento(String doc){
        this.documento=doc;
    }
    public String getDocumento(){
        return documento;
    }
    public void setTiempoDemora(double td){
        this.tiempodemora = td;
    }
    public double getTiempoDemora(){
        return tiempodemora;
    }
    public void setCausaDemora(String cd){
        this.causademora = cd;        
    }
    public String getCausaDemora(){
        return causademora;
    }
    public void setResdemora(String rs){
        this.resdemora = rs;        
    }
    public String getResdemora(){
        return resdemora;
    }
    public void setFeccierre(String fec){
        this.feccierre=fec;
    }
    public String getFeccierre(){
        return feccierre;
    }
    public void setObservacion(String a){
        this.observacion=a;
    }
    public String getObservacion(){
        return observacion;
    }
    public void setEstado(String e){
        this.estado=e;
    }
    public String getEstado(){
        return estado;
    }
    public void setBase(String base){
        this.base = base;
    }
    public String getBase(){
        return base;
    }
    public void setCreation_user(String cu){
        this.creation_user = cu;
    }
    public String getCreation_user(){
        return creation_user;
    }
    public void setCreation_date(String fec){ 
        this.creation_date = fec;
    }
    public String getCreation_date(){
        return creation_date;
    }
    public void setLast_update(String fec){ 
        this.last_update = fec;
    }
    public String getLast_update(){
        return last_update;
    }
    public void setUser_update(String us){
        this.user_update = us;
    }
    public String getUser_update(){
        return user_update;
    }
    
    /**
     * Getter for property cantrealizada.
     * @return Value of property cantrealizada.
     */
    public java.lang.String getCantrealizada() {
        return cantrealizada;
    }
    
    /**
     * Setter for property cantrealizada.
     * @param cantrealizada New value of property cantrealizada.
     */
    public void setCantrealizada(java.lang.String cantrealizada) {
        this.cantrealizada = cantrealizada;
    }
    
    /**
     * Getter for property cantplaneada.
     * @return Value of property cantplaneada.
     */
    public java.lang.String getCantplaneada() {
        return cantplaneada;
    }
    
    /**
     * Setter for property cantplaneada.
     * @param cantplaneada New value of property cantplaneada.
     */
    public void setCantplaneada(java.lang.String cantplaneada) {
        this.cantplaneada = cantplaneada;
    }
    
    /**
     * Getter for property referencia1.
     * @return Value of property referencia1.
     */
    public java.lang.String getReferencia1() {
            return referencia1;
    }
    
    /**
     * Setter for property referencia1.
     * @param referencia1 New value of property referencia1.
     */
    public void setReferencia1(java.lang.String referencia1) {
            this.referencia1 = referencia1;
    }
    
    /**
     * Getter for property referencia2.
     * @return Value of property referencia2.
     */
    public java.lang.String getReferencia2() {
            return referencia2;
    }
    
    /**
     * Setter for property referencia2.
     * @param referencia2 New value of property referencia2.
     */
    public void setReferencia2(java.lang.String referencia2) {
            this.referencia2 = referencia2;
    }
    
    /**
     * Getter for property refnumerica1.
     * @return Value of property refnumerica1.
     */
    public int getRefnumerica1() {
        return refnumerica1;
    }    
    
    /**
     * Setter for property refnumerica1.
     * @param refnumerica1 New value of property refnumerica1.
     */
    public void setRefnumerica1(int refnumerica1) {
        this.refnumerica1 = refnumerica1;
    }
    
    /**
     * Getter for property refnumerica2.
     * @return Value of property refnumerica2.
     */
    public int getRefnumerica2() {
        return refnumerica2;
    }
    
    /**
     * Setter for property refnumerica2.
     * @param refnumerica2 New value of property refnumerica2.
     */
    public void setRefnumerica2(int refnumerica2) {
        this.refnumerica2 = refnumerica2;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
}

