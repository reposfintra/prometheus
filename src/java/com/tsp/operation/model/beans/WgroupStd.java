/********************************************************************
 *      Nombre Clase.................   Documento.java    
 *      Descripci�n..................   Bean del archivo wgroup_standard    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   17.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.model.beans;

/**
 *
 * @author  Andres
 */
public class WgroupStd {
    
    private String work_group;
    private String std_job_no;
    
    private String reg_status;
    private String base;
    private String dstrct;
    private String creation_user;
    
    /** Creates a new instance of WgroupStd */
    public WgroupStd() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property work_group.
     * @return Value of property work_group.
     */
    public java.lang.String getWork_group() {
        return work_group;
    }
    
    /**
     * Setter for property work_group.
     * @param work_group New value of property work_group.
     */
    public void setWork_group(java.lang.String work_group) {
        this.work_group = work_group;
    }
    
}
