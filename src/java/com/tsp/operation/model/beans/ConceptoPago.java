/******************************************************************
 * Nombre ......................ConceptoPago.java
 * Descripci�n..................Conceptos de pago
 * Autor........................Armando Oviedo
 * Fecha........................10/10/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*;

public class ConceptoPago {
    
    private String codigo;
    private String descripcion;
    private String controlparam;
    private String codpadre;
    private int orden;
    private int nivel;
    private String id_folder;
    private String referencia1;
    private String referencia2;
    private String referencia3;
    private String referencia4;
    private String reg_status;
    private String dstrct;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String base;
    private String nompadre;
    
    /** Creates a new instance of ConceptoPago */
    public ConceptoPago() {
    }
    
    /** 
     * M�todo utilizado para cargar un resultSet en el bean ConceptoPago
     * @author..........Armando Oviedo
     * @version.........1.0
     * @param...........ResultSet rs
     * @return..........ConceptoPago cp
     */
    public static ConceptoPago load(ResultSet rs) throws SQLException{
        ConceptoPago cp = new ConceptoPago();
        cp.setBase(rs.getString("base"));
        cp.setCodigo(rs.getString("codigo"));
        cp.setCodpadre(rs.getString("codpadre"));
        cp.setControlparam(rs.getString("controlparam"));
        cp.setCreation_date("creation_date");
        cp.setCreation_user(rs.getString("creation_user"));
        cp.setDescripcion(rs.getString("descripcion"));
        cp.setDstrct(rs.getString("dstrct"));
        cp.setId_folder(rs.getString("idfolder"));
        cp.setLast_update(rs.getString("last_update"));
        cp.setOrden(rs.getInt("orden"));
        cp.setNivel(rs.getInt("nivel"));
        cp.setReferencia1(rs.getString("referencia1"));
        cp.setReferencia2(rs.getString("referencia2"));
        cp.setReferencia3(rs.getString("referencia3"));
        cp.setReferencia4(rs.getString("referencia4"));
        cp.setReg_status(rs.getString("reg_status"));
        cp.setUser_update(rs.getString("user_update"));        
        return cp;
    }
    
    /** 
     * M�todo utilizado para cargar un resultSet en el bean ConceptoPago
     * @author..........Armando Oviedo
     * @version.........1.0
     * @param...........ResultSet rs
     * @return..........ConceptoPago cp
     */
    public static ConceptoPago load2(ResultSet rs) throws SQLException{
        ConceptoPago cp = new ConceptoPago();
        cp.setCodigo(rs.getString("codigo"));
        cp.setNivel(rs.getInt("nivel"));
        cp.setId_folder(rs.getString("idfolder"));
        cp.setDescripcion(rs.getString("descripcion"));
        cp.setCodpadre(rs.getString("codpadre"));
        cp.setNompadre(rs.getString("nompadre"));
        cp.setControlparam(rs.getString("controlparam"));
        cp.setReferencia1(rs.getString("referencia1"));
        cp.setReferencia2(rs.getString("referencia2"));
        return cp;
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property controlparam.
     * @return Value of property controlparam.
     */
    public java.lang.String getControlparam() {
        return controlparam;
    }
    
    /**
     * Setter for property controlparam.
     * @param controlparam New value of property controlparam.
     */
    public void setControlparam(java.lang.String controlparam) {
        this.controlparam = controlparam;
    }
    
    /**
     * Getter for property codpadre.
     * @return Value of property codpadre.
     */
    public java.lang.String getCodpadre() {
        return codpadre;
    }
    
    /**
     * Setter for property codpadre.
     * @param codpadre New value of property codpadre.
     */
    public void setCodpadre(java.lang.String codpadre) {
        this.codpadre = codpadre;
    }
    
    /**
     * Getter for property orden.
     * @return Value of property orden.
     */
    public int getOrden() {
        return orden;
    }
    
    /**
     * Setter for property orden.
     * @param orden New value of property orden.
     */
    public void setOrden(int orden) {
        this.orden = orden;
    }
    
    /**
     * Getter for property nivel.
     * @return Value of property nivel.
     */
    public int getNivel() {
        return nivel;
    }
    
    /**
     * Setter for property nivel.
     * @param nivel New value of property nivel.
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    /**
     * Getter for property idfolder.
     * @return Value of property idfolder.
     */
    public java.lang.String getId_folder() {
        return id_folder;
    }
    
    /**
     * Setter for property idfolder.
     * @param idfolder New value of property idfolder.
     */
    public void setId_folder(java.lang.String id_folder) {
        this.id_folder = id_folder;
    }
    
    /**
     * Getter for property referencia1.
     * @return Value of property referencia1.
     */
    public java.lang.String getReferencia1() {
        return referencia1;
    }
    
    /**
     * Setter for property referencia1.
     * @param referencia1 New value of property referencia1.
     */
    public void setReferencia1(java.lang.String referencia1) {
        this.referencia1 = referencia1;
    }
    
    /**
     * Getter for property referencia2.
     * @return Value of property referencia2.
     */
    public java.lang.String getReferencia2() {
        return referencia2;
    }
    
    /**
     * Setter for property referencia2.
     * @param referencia2 New value of property referencia2.
     */
    public void setReferencia2(java.lang.String referencia2) {
        this.referencia2 = referencia2;
    }
    
    /**
     * Getter for property referencia3.
     * @return Value of property referencia3.
     */
    public java.lang.String getReferencia3() {
        return referencia3;
    }
    
    /**
     * Setter for property referencia3.
     * @param referencia3 New value of property referencia3.
     */
    public void setReferencia3(java.lang.String referencia3) {
        this.referencia3 = referencia3;
    }
    
    /**
     * Getter for property referencia4.
     * @return Value of property referencia4.
     */
    public java.lang.String getReferencia4() {
        return referencia4;
    }
    
    /**
     * Setter for property referencia4.
     * @param referencia4 New value of property referencia4.
     */
    public void setReferencia4(java.lang.String referencia4) {
        this.referencia4 = referencia4;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property nompadre.
     * @return Value of property nompadre.
     */
    public java.lang.String getNompadre() {
        return nompadre;
    }
    
    /**
     * Setter for property nompadre.
     * @param nompadre New value of property nompadre.
     */
    public void setNompadre(java.lang.String nompadre) {
        this.nompadre = nompadre;
    }
    
}
    
    
    
    