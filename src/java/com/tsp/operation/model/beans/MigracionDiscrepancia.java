/*
 * MigrarDiscrepancia.java
 *
 * Created on 14 de octubre de 2005, 08:48 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  dbastidas
 */
public class MigracionDiscrepancia {
    private String OC;
    private String item;
    private String raised_by;
    private String raised_date;
    private String dr_medium_ind;
    private String supp_contated;
    private String supp_contact;
    private String discrep_qty;
    private String discrep_oum;
    private String qi_code;
    private String dr_desc;
    private String dr_ref;
    private String discrp_type1;
    private String discrp_type2;
    private String discrp_type3;
    private String discrp_type4;
    private String discrp_type5;
    private String discrp_type6;
    private String hold_pmt_ind;
    private String upd_stats_ind;
    private String cr_replace_ind;
    private String replace_qty;
    private String credit_value;
    //Osvaldo
    private String documento;
    
    /** Creates a new instance of MigrarDiscrepancia */
    public MigracionDiscrepancia() {
    }
    
    /**
     * Getter for property OC.
     * @return Value of property OC.
     */
    public java.lang.String getOC() {
        return OC;
    }
    
    /**
     * Setter for property OC.
     * @param OC New value of property OC.
     */
    public void setOC(java.lang.String OC) {
        this.OC = OC;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property raised_by.
     * @return Value of property raised_by.
     */
    public java.lang.String getRaised_by() {
        return raised_by;
    }
    
    /**
     * Setter for property raised_by.
     * @param raised_by New value of property raised_by.
     */
    public void setRaised_by(java.lang.String raised_by) {
        this.raised_by = raised_by;
    }
    
    /**
     * Getter for property raised_date.
     * @return Value of property raised_date.
     */
    public java.lang.String getRaised_date() {
        return raised_date;
    }
    
    /**
     * Setter for property raised_date.
     * @param raised_date New value of property raised_date.
     */
    public void setRaised_date(java.lang.String raised_date) {
        this.raised_date = raised_date;
    }
    
    /**
     * Getter for property dr_medium_ind.
     * @return Value of property dr_medium_ind.
     */
    public java.lang.String getDr_medium_ind() {
        return dr_medium_ind;
    }
    
    /**
     * Setter for property dr_medium_ind.
     * @param dr_medium_ind New value of property dr_medium_ind.
     */
    public void setDr_medium_ind(java.lang.String dr_medium_ind) {
        this.dr_medium_ind = dr_medium_ind;
    }
    
    /**
     * Getter for property supp_contated.
     * @return Value of property supp_contated.
     */
    public java.lang.String getSupp_contated() {
        return supp_contated;
    }
    
    /**
     * Setter for property supp_contated.
     * @param supp_contated New value of property supp_contated.
     */
    public void setSupp_contated(java.lang.String supp_contated) {
        this.supp_contated = supp_contated;
    }
    
    /**
     * Getter for property supp_cantact.
     * @return Value of property supp_cantact.
     */
    public java.lang.String getSupp_contact() {
        return supp_contact;
    }
    
    /**
     * Setter for property supp_cantact.
     * @param supp_cantact New value of property supp_cantact.
     */
    public void setSupp_contact(java.lang.String supp_contact) {
        this.supp_contact = supp_contact;
    }
    
    /**
     * Getter for property discrep_qty.
     * @return Value of property discrep_qty.
     */
    public java.lang.String getDiscrep_qty() {
        return discrep_qty;
    }
    
    /**
     * Setter for property discrep_qty.
     * @param discrep_qty New value of property discrep_qty.
     */
    public void setDiscrep_qty(java.lang.String discrep_qty) {
        this.discrep_qty = discrep_qty;
    }
    
    /**
     * Getter for property discrep_oum.
     * @return Value of property discrep_oum.
     */
    public java.lang.String getDiscrep_oum() {
        return discrep_oum;
    }
    
    /**
     * Setter for property discrep_oum.
     * @param discrep_oum New value of property discrep_oum.
     */
    public void setDiscrep_oum(java.lang.String discrep_oum) {
        this.discrep_oum = discrep_oum;
    }
    
    /**
     * Getter for property qi_code.
     * @return Value of property qi_code.
     */
    public java.lang.String getQi_code() {
        return qi_code;
    }
    
    /**
     * Setter for property qi_code.
     * @param qi_code New value of property qi_code.
     */
    public void setQi_code(java.lang.String qi_code) {
        this.qi_code = qi_code;
    }
    
    /**
     * Getter for property dr_code.
     * @return Value of property dr_code.
     */
    public java.lang.String getDr_desc() {
        return dr_desc;
    }
    
    /**
     * Setter for property dr_code.
     * @param dr_code New value of property dr_code.
     */
    public void setDr_desc(java.lang.String dr_desc) {
        this.dr_desc = dr_desc;
    }
    
    /**
     * Getter for property dr_ref.
     * @return Value of property dr_ref.
     */
    public java.lang.String getDr_ref() {
        return dr_ref;
    }
    
    /**
     * Setter for property dr_ref.
     * @param dr_ref New value of property dr_ref.
     */
    public void setDr_ref(java.lang.String dr_ref) {
        this.dr_ref = dr_ref;
    }
    
    /**
     * Getter for property discrp_type1.
     * @return Value of property discrp_type1.
     */
    public java.lang.String getDiscrp_type1() {
        return discrp_type1;
    }
    
    /**
     * Setter for property discrp_type1.
     * @param discrp_type1 New value of property discrp_type1.
     */
    public void setDiscrp_type1(java.lang.String discrp_type1) {
        this.discrp_type1 = discrp_type1;
    }
    
    /**
     * Getter for property discrp_type2.
     * @return Value of property discrp_type2.
     */
    public java.lang.String getDiscrp_type2() {
        return discrp_type2;
    }
    
    /**
     * Setter for property discrp_type2.
     * @param discrp_type2 New value of property discrp_type2.
     */
    public void setDiscrp_type2(java.lang.String discrp_type2) {
        this.discrp_type2 = discrp_type2;
    }
    
    /**
     * Getter for property discrp_type3.
     * @return Value of property discrp_type3.
     */
    public java.lang.String getDiscrp_type3() {
        return discrp_type3;
    }
    
    /**
     * Setter for property discrp_type3.
     * @param discrp_type3 New value of property discrp_type3.
     */
    public void setDiscrp_type3(java.lang.String discrp_type3) {
        this.discrp_type3 = discrp_type3;
    }
    
    /**
     * Getter for property discrp_type4.
     * @return Value of property discrp_type4.
     */
    public java.lang.String getDiscrp_type4() {
        return discrp_type4;
    }
    
    /**
     * Setter for property discrp_type4.
     * @param discrp_type4 New value of property discrp_type4.
     */
    public void setDiscrp_type4(java.lang.String discrp_type4) {
        this.discrp_type4 = discrp_type4;
    }
    
    /**
     * Getter for property discrp_type5.
     * @return Value of property discrp_type5.
     */
    public java.lang.String getDiscrp_type5() {
        return discrp_type5;
    }
    
    /**
     * Setter for property discrp_type5.
     * @param discrp_type5 New value of property discrp_type5.
     */
    public void setDiscrp_type5(java.lang.String discrp_type5) {
        this.discrp_type5 = discrp_type5;
    }
    
    /**
     * Getter for property discrp_type6.
     * @return Value of property discrp_type6.
     */
    public java.lang.String getDiscrp_type6() {
        return discrp_type6;
    }
    
    /**
     * Setter for property discrp_type6.
     * @param discrp_type6 New value of property discrp_type6.
     */
    public void setDiscrp_type6(java.lang.String discrp_type6) {
        this.discrp_type6 = discrp_type6;
    }
    
    /**
     * Getter for property hold_pmt_ind.
     * @return Value of property hold_pmt_ind.
     */
    public java.lang.String getHold_pmt_ind() {
        return hold_pmt_ind;
    }
    
    /**
     * Setter for property hold_pmt_ind.
     * @param hold_pmt_ind New value of property hold_pmt_ind.
     */
    public void setHold_pmt_ind(java.lang.String hold_pmt_ind) {
        this.hold_pmt_ind = hold_pmt_ind;
    }
    
    /**
     * Getter for property upd_stats_ind.
     * @return Value of property upd_stats_ind.
     */
    public java.lang.String getUpd_stats_ind() {
        return upd_stats_ind;
    }
    
    /**
     * Setter for property upd_stats_ind.
     * @param upd_stats_ind New value of property upd_stats_ind.
     */
    public void setUpd_stats_ind(java.lang.String upd_stats_ind) {
        this.upd_stats_ind = upd_stats_ind;
    }
    
    /**
     * Getter for property cr_replace_ind.
     * @return Value of property cr_replace_ind.
     */
    public java.lang.String getCr_replace_ind() {
        return cr_replace_ind;
    }
    
    /**
     * Setter for property cr_replace_ind.
     * @param cr_replace_ind New value of property cr_replace_ind.
     */
    public void setCr_replace_ind(java.lang.String cr_replace_ind) {
        this.cr_replace_ind = cr_replace_ind;
    }
    
    /**
     * Getter for property replace_qty.
     * @return Value of property replace_qty.
     */
    public java.lang.String getReplace_qty() {
        return replace_qty;
    }
    
    /**
     * Setter for property replace_qty.
     * @param replace_qty New value of property replace_qty.
     */
    public void setReplace_qty(java.lang.String replace_qty) {
        this.replace_qty = replace_qty;
    }
    
    /**
     * Getter for property credit_value.
     * @return Value of property credit_value.
     */
    public java.lang.String getCredit_value() {
        return credit_value;
    }
    
    /**
     * Setter for property credit_value.
     * @param credit_value New value of property credit_value.
     */
    public void setCredit_value(java.lang.String credit_value) {
        this.credit_value = credit_value;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
}
