/***********************************************
 * Nombre: Ingreso_especial.java
 * Descripci�n: Beans de flota directa.
 * Autor: Ing. Jose de la rosa
 * Fecha: 6 de diciembre de 2005, 08:57 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ***********************************************/

package com.tsp.operation.model.beans;

public class Ingreso_especial {
    
    private String tipo_acuerdo;
    private String codigo_concepto;
    private String clase_equipo;
    private float porcentaje_ingreso;
    private int numero;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String base;
    private String distrito;
    private String rec_status;
    
    /** Creates a new instance of Ingreso_especial */
    public Ingreso_especial () {
        tipo_acuerdo = "";
        codigo_concepto = "";
        clase_equipo = "";
        porcentaje_ingreso = 0;
        numero = 0;
        usuario_creacion = "";
        fecha_creacion = "";
        usuario_modificacion = "";
        ultima_modificacion = "";
        base = "";
        distrito= "";
        rec_status = "";
        
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property clase_equipo.
     * @return Value of property clase_equipo.
     */
    public java.lang.String getClase_equipo () {
        return clase_equipo;
    }
    
    /**
     * Setter for property clase_equipo.
     * @param clase_equipo New value of property clase_equipo.
     */
    public void setClase_equipo (java.lang.String clase_equipo) {
        this.clase_equipo = clase_equipo;
    }
    
    /**
     * Getter for property codigo_concepto.
     * @return Value of property codigo_concepto.
     */
    public java.lang.String getCodigo_concepto () {
        return codigo_concepto;
    }
    
    /**
     * Setter for property codigo_concepto.
     * @param codigo_concepto New value of property codigo_concepto.
     */
    public void setCodigo_concepto (java.lang.String codigo_concepto) {
        this.codigo_concepto = codigo_concepto;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion () {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion (java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property porcentaje_ingreso.
     * @return Value of property porcentaje_ingreso.
     */
    public float getPorcentaje_ingreso () {
        return porcentaje_ingreso;
    }
    
    /**
     * Setter for property porcentaje_ingreso.
     * @param porcentaje_ingreso New value of property porcentaje_ingreso.
     */
    public void setPorcentaje_ingreso (float porcentaje_ingreso) {
        this.porcentaje_ingreso = porcentaje_ingreso;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status () {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status (java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property ultima_modificacion.
     * @return Value of property ultima_modificacion.
     */
    public java.lang.String getUltima_modificacion () {
        return ultima_modificacion;
    }
    
    /**
     * Setter for property ultima_modificacion.
     * @param ultima_modificacion New value of property ultima_modificacion.
     */
    public void setUltima_modificacion (java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion () {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion (java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion () {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property numero.
     * @return Value of property numero.
     */
    public int getNumero () {
        return numero;
    }
    
    /**
     * Setter for property numero.
     * @param numero New value of property numero.
     */
    public void setNumero (int numero) {
        this.numero = numero;
    }
    
    /**
     * Getter for property tipo_acuerdo.
     * @return Value of property tipo_acuerdo.
     */
    public java.lang.String getTipo_acuerdo () {
        return tipo_acuerdo;
    }
    
    /**
     * Setter for property tipo_acuerdo.
     * @param tipo_acuerdo New value of property tipo_acuerdo.
     */
    public void setTipo_acuerdo (java.lang.String tipo_acuerdo) {
        this.tipo_acuerdo = tipo_acuerdo;
    }
    
}
