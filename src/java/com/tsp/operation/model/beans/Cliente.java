/*
 * Cliente.java
 *
 * Created on 20 de abril de 2005, 09:08 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Henry
 */
public class Cliente {

    private String estado;
    private String codcli;
    private String nomcli;
    private String notas;
    private String agduenia;
    private String reg_status;
    private String creation_date;
    private String last_update;
    private String base;
    private String texto_oc;
    private float rentabilidad;
    private String agente;
    private String email;
    private String unidad;
    private String nit;
    private String digito_verificacion;
    //Diogenes
    private String branch_code;
    private String bank_account_no;
    private String sec_standard;

    private String cmc;

    private String rif;
    private String agenciaFacturacion;

    private String telefono;
    private String celContacto;
    private String direccion;
    private String barrio;
    private String celular;

      private String creation_user;

    
    /**********Datos eca del cliente********************
     * jpinedo
     */
    
    private String nomcontacto;
    private String telcontacto;
    private String cargocontacto;
    private String nomrepresentante;
    private String telrepresentante;
    private String celrepresentante;
    private String ciudad;
    private String tipo;
    private String sector;
    private String id_ejecutivo;

    private boolean  oficial;
        private String [] nics;

    private String  id_padre;
    private String  edif;
    private String  emailrepresentantelega;
    private String  nic;
    private String  Clasificacion;
    private String  iddepartamento;
    
    

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getNomcontacto() {
        return nomcontacto;
    }

    public void setNomcontacto(String nomcontacto) {
        this.nomcontacto = nomcontacto;
    }

    public String getTelcontacto() {
        return telcontacto;
    }

    public void setTelcontacto(String telcontacto) {
        this.telcontacto = telcontacto;
    }

    public String getCargocontacto() {
        return cargocontacto;
    }

    public void setCargocontacto(String cargocontacto) {
        this.cargocontacto = cargocontacto;
    }

    public String getNomrepresentante() {
        return nomrepresentante;
    }

    public void setNomrepresentante(String nomrepresentante) {
        this.nomrepresentante = nomrepresentante;
    }

    public String getTelrepresentante() {
        return telrepresentante;
    }

    public void setTelrepresentante(String telrepresentante) {
        this.telrepresentante = telrepresentante;
    }

    public String getCelrepresentante() {
        return celrepresentante;
    }

    public void setCelrepresentante(String celrepresentante) {
        this.celrepresentante = celrepresentante;
    }

    public String getId_ejecutivo() {
        return id_ejecutivo;
    }

    public void setId_ejecutivo(String id_ejecutivo) {
        this.id_ejecutivo = id_ejecutivo;
    }

    public String getIddepartamento() {
        return iddepartamento;
    }

    public void setIddepartamento(String iddepartamento) {
        this.iddepartamento = iddepartamento;
    }

    public String getClasificacion() {
        return Clasificacion;
    }

    public void setClasificacion(String Clasificacion) {
        this.Clasificacion = Clasificacion;
    }
    

    

    

   


















    /** Creates a new instance of Cliente */
    public Cliente() {
    }
    public Cliente(boolean b) {
        this.estado ="";
        this.codcli ="";
        this.nomcli ="";
        this.notas ="";
        this.agduenia ="";
        this.reg_status ="";
        this.creation_date ="";
        this.last_update ="";
        this.base ="";
        this.texto_oc ="";
        this.rentabilidad = 0;

        this.telefono="";
        this.direccion="";

    }
    //** Metodos Set **\\
    public void setEstado(String est){
        this.estado = est;
    }
    public void setCodcli(String cod){
        this.codcli = cod;
    }
    public void setNomcli(String nom){
        this.nomcli = nom;
    }
    public void setNotas(String notas){
        this.notas = notas;
    }
    public void setAgduenia(String ag) {
        this.agduenia = ag;
    }
    public void setReg_status(String reg){
        this.reg_status = reg;
    }
    public void setCreation_date(String date){
        this.creation_date = date;
    }
    public void setLast_update(String last){
        this.last_update = last;
    }
    public void setBase(String base) {
        this.base = base;
    }
    public void setTexto_oc(String texto_oc) {
        this.texto_oc = texto_oc;
    }
    public void setRentabilidad(float rent) {
        this.rentabilidad = rent;
    }


    public void setNomContacto(String nombre) {
        this.nomcontacto = nombre;
    }
    public void setTelContacto(String tel) {
        this.telcontacto = tel;
    }

        public void setCargoContacto(String c) {
        this.cargocontacto = c;
    }


    public void setNomRepresentante(String n) {
        this.nomrepresentante = n;
    }

    public void setTelRepresentante(String n) {
        this.telrepresentante = n;
    }

   public void setCelRepresentante(String n) {
        this.celrepresentante = n;
    }


      public void setSector(String n) {
        this.sector = n;
    }

   public void setTipo(String n) {
        this.tipo = n;
    }

   public void setCiudad(String n) {
        this.ciudad = n;
    }

   public void setOficial(boolean oficial) {
        this.oficial = oficial;
    }

    public void setNics(String[] nics) {
        this.nics = nics;
    }


       public void setId_padre(String id_padre) {
        this.id_padre = id_padre;
    }

    public void setIdEjecutivo(String id) {
        this.id_ejecutivo = id;
    }

        public void setCreationUser(String creation_user) {
        this.creation_user = creation_user;
    }




    //** Metodos Get **\\

        public String getCreationUser()
        {
        return this.creation_user;
        }

        public String getNomContacto()
        {
        return this.nomcontacto;
        }
        public String getTelContacto()
        {
            return this.telcontacto;
        }
        public String getCargoContacto()
        {
            return this.cargocontacto;
        }
         public String getNomRepresentante()
        {
        return this.nomrepresentante;
        }
         public String getTelRepresentante()
        {
        return this.telrepresentante;
        }

        public String getCelRepresentante()
        {
        return this.celrepresentante;
        }

        public String getTipo()
        {
        return this.tipo;
        }
        public String getCiudad()
        {
        return this.ciudad;
        }

        public String getSector()
        {
        return this.sector;
        }

        public String getIdEjecutivo()
        {
        return this.id_ejecutivo;
        }

        
        public boolean  isOficial()
        {
        return this.oficial;
        }

    public String[] getNics() {
        return nics;
    }



        public String getId_padre() {
        return id_padre;
    }







        


public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    




    public String getEstado(){
        return this.estado;
    }
    public String getCodcli(){
        return this.codcli;
    }
    public String getNomcli(){
        return this.nomcli;
    }
    public String getNotas(){
        return this.notas;
    }
    public String getAgduenia(){
        return this.agduenia;
    }
    public String getReg_status(){
        return this.reg_status;
    }
    public String getCreation_date(){
        return this.creation_date;
    }
    public String getLast_update(){
        return this.last_update;
    }
    public String getBase(){
        return this.base;
    }
    public String getTexto_oc(){
        return this.texto_oc;
    }
    public float getRentabilidad(){
        return this.rentabilidad;
    }

    /**
     * Getter for property agente.
     * @return Value of property agente.
     */
    public java.lang.String getAgente() {
        return agente;
    }

    /**
     * Setter for property agente.
     * @param agente New value of property agente.
     */
    public void setAgente(java.lang.String agente) {
        this.agente = agente;
    }

    /**
     * Getter for property email.
     * @return Value of property email.
     */
    public java.lang.String getEmail() {
        return email;
    }

    /**
     * Setter for property email.
     * @param email New value of property email.
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }

    /*
     * Metodo traido del bean de la aplicación SOT
     */

    public static Cliente load(java.sql.ResultSet rs)throws java.sql.SQLException{
        Cliente cliente = new Cliente();

        cliente.setCodcli( rs.getString("codcli") );
        cliente.setNomcli(rs.getString("nomcli") );
        cliente.setNotas( rs.getString("notas") );

        return cliente;
    }

    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }

    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }

    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }

    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }

    /**
     * Getter for property branch_code.
     * @return Value of property branch_code.
     */
    public java.lang.String getBranch_code() {
        return branch_code;
    }

    /**
     * Setter for property branch_code.
     * @param branch_code New value of property branch_code.
     */
    public void setBranch_code(java.lang.String branch_code) {
        this.branch_code = branch_code;
    }

    /**
     * Getter for property bank_account_no.
     * @return Value of property bank_account_no.
     */
    public java.lang.String getBank_account_no() {
        return bank_account_no;
    }

    /**
     * Setter for property bank_account_no.
     * @param bank_account_no New value of property bank_account_no.
     */
    public void setBank_account_no(java.lang.String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }
    /**
     * Getter for property bank_account_no.
     * @return Value of property bank_account_no.
     */
    public java.lang.String getSec_standard() {
        return sec_standard;
    }

    /**
     * Setter for property bank_account_no.
     * @param bank_account_no New value of property bank_account_no.
     */
    public void setSec_standard(java.lang.String sec_standard) {
        this.sec_standard = sec_standard;
    }

    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }

    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }

    /**
     * Getter for property rif.
     * @return Value of property rif.
     */
    public java.lang.String getRif() {
        return rif;
    }

    /**
     * Setter for property rif.
     * @param rif New value of property rif.
     */
    public void setRif(java.lang.String rif) {
        this.rif = rif;
    }

    /**
     * Getter for property agenciaFacturacion.
     * @return Value of property agenciaFacturacion.
     */
    public java.lang.String getAgenciaFacturacion() {
        return agenciaFacturacion;
    }

    /**
     * Setter for property agenciaFacturacion.
     * @param agenciaFacturacion New value of property agenciaFacturacion.
     */
    public void setAgenciaFacturacion(java.lang.String agenciaFacturacion) {
        this.agenciaFacturacion = agenciaFacturacion;
    }

    /**
     * devuelve el campo direccion
     * @return cadena con la direccion
     * @since 2010-07-22
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * coloca el valor del campo direccion
     * @param direccion el valor a colocar
     * @since 2010-07-22
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * devuelve el campo telefono
     * @return cadena con el telefono
     * @since 2010-07-22
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * coloca el valor del campo telefono
     * @param telefono el valor a colocar
     * @since 2010-07-22
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEdif() {
        return edif;
    }

    public void setEdif(String edif) {
        this.edif = edif;
    }

    public String getCelContacto() {
        return celContacto;
    }

    public void setCelContacto(String celContacto) {
        this.celContacto = celContacto;
    } 

    /**
     * @return the digito_verificacion
     */
    public String getDigito_verificacion() {
        return digito_verificacion;
    }

    /**
     * @param digito_verificacion the digito_verificacion to set
     */
    public void setDigito_verificacion(String digito_verificacion) {
        this.digito_verificacion = digito_verificacion;
    }
    
     public String getEmailrepresentantelega() {
        return emailrepresentantelega;
    }

    public void setEmailrepresentantelega(String emailrepresentantelega) {
        this.emailrepresentantelega = emailrepresentantelega;
    }

    /*public void setCod_neg(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Object getCod_neg() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/

    public Object getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }
    
    public Object getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
}

