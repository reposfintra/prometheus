package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Proyecto implements Serializable {
	
	private String dstrct;
	private String project;
	private String description;
	private String capture_code;
	private java.util.Date creation_date;
	private String creation_user;
	
	public static Proyecto load(ResultSet rs)throws SQLException{
		
		Proyecto proyecto = new Proyecto();
		
		proyecto.setDstrct(rs.getString(2));
		proyecto.setProject(rs.getString(3));
		proyecto.setDescription(rs.getString(4));
		proyecto.setCapture_code(rs.getString(5));
		proyecto.setCreation_date(rs.getTimestamp(8));
		proyecto.setCreation_user(rs.getString(9));
		
		return proyecto;
	}
	
	//============================================
	//		Metodos de acceso a propiedades
	//============================================
	
	
	public void setDstrct(String dstrct){
			
		this.dstrct=dstrct;
	}
	
	public String getDstrct(){
	
		return dstrct;
	}
	
	public void setProject(String project){
		
		this.project=project;
	}
	
	public String getProject(){
		
		return project;
	}
	
	public void setDescription(String description){
		
		this.description=description;
	}
	
	public String getDescription(){
		
		return description;
	}
	
	public void setCapture_code(String capture_code){
		
		this.capture_code=capture_code;
	}
	
	public String getCapture_code(){
		
		return capture_code;
	}
	
	public void setCreation_date(java.util.Date creation_date){
		
		this.creation_date=creation_date;
	}
	
	public java.util.Date getCreation_date(){
		
		return creation_date;
	}
	
	public void setCreation_user(String creation_user){
		
		this. creation_user=creation_user;
	}
	
	public String getCreation_user(){
		
		return creation_user;
	}
}
	
	
	
	
	
