
package com.tsp.operation.model.beans;

public class ListadoOC {
    private String oc;
    private String fecha;
    private String cliente;
    private String sj;
    
  //Metodos
    public ListadoOC() {}
    
    //set
    public void setOC(String oc){
       this.oc=oc; 
    }
  
    public void setFecha(String fecha){
        this.fecha=fecha;
    }
    
    public void setCliente(String cliente){
        this.cliente=cliente;
    }
    
    public void setSJ(String sj){
        this.sj=sj;
    }
   
    //get 
    public String getOC(){
        return this.oc;
    }
    
    public String getFecha(){
        return this.fecha;
    }
    
    public String getCliente(){
        return this.cliente;
    }
    
    public String getSJ(){
        return this.sj;
    }

}// end clase
