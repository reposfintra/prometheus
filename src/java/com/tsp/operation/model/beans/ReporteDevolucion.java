/******************************************************************
* Nombre                        ReporteDevolucion.java
* Descripci�n                   Clase bean para el reporte de devolucion
* Autor                         ricardo rosero
* Fecha                         19/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import com.tsp.operation.model.beans.*;

/**
 * @author  Ricardo Rosero
 */
public class ReporteDevolucion implements java.io.Serializable {
    //atributos
    private String numrem;
    private String orirem;
    private String desrem;
    private String cliente;
    private String numpla;
    private String fec_devolucion;
    private String documento;
    private String tipo_doc;
    private String ubicacion;
    private String codprod;
    private String producto;
    private String tipoemp;
    private int cantdev;
    private String nombredev;
    private String motivodev;
    private String respondev;
    private String notasxom;
    private String fechdevbod;
    private String fecrechazo;
    private String numrechazo;
    private String nombrerep;
    private String fechareporte;
    private String plaveh;
    private String fec_cumplido;
    
    /** Creates a new instance of ReporteDevolucion */
    public ReporteDevolucion load(ResultSet rs) throws SQLException {
        ReporteDevolucion rd = new ReporteDevolucion();
        
        rd.setNumrem(rs.getString("numrem"));
        rd.setOrirem(rs.getString("orirem"));
        rd.setDesrem(rs.getString("desrem"));
        rd.setCliente(rs.getString("cliente"));
        rd.setNumpla(rs.getString("numpla"));
        String f1 = rs.getString("fec_devolucion").substring(0,10);
        rd.setFec_devolucion(f1);
        rd.setDocumento(rs.getString("documento"));
        rd.setTipo_doc(rs.getString("tipo_doc"));
        rd.setUbicacion(rs.getString("ubicacion"));
        rd.setCodprod(rs.getString("codprod"));
        rd.setProducto(rs.getString("producto"));
        rd.setTipoemp(rs.getString("tipoemp"));
        rd.setCantdev(rs.getInt("cantdev"));
        rd.setNombredev(rs.getString("nombredev"));
        rd.setMotivodev(rs.getString("motivodev"));
        rd.setRespondev(rs.getString("respondev"));
        rd.setNotasxom(rs.getString("notasxom"));
        String f2 = rs.getString("fechdevbod").substring(0,10);
        rd.setFechdevbod(f2);
        String f3 = rs.getString("fecrechazo").substring(0,10);
        rd.setFecrechazo(f3);
        rd.setNumrechazo(rs.getString("numrechazo"));
        rd.setNombrerep(rs.getString("nombrerep"));
        String f5 = rs.getString("fechareporte").substring(0,10);
        rd.setFechareporte(f5);
        rd.setPlaveh(rs.getString("plaveh"));
        String f4 = rs.getString("fec_cumplido").substring(0,10);
        rd.setFec_cumplido(f4);
        //////System.out.println("............ " + rd.getNumpla());
        return rd;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }    
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }    
    
    /**
     * Getter for property codprod.
     * @return Value of property codprod.
     */
    public java.lang.String getCodprod() {
        return codprod;
    }
    
    /**
     * Setter for property codprod.
     * @param codprod New value of property codprod.
     */
    public void setCodprod(java.lang.String codprod) {
        this.codprod = codprod;
    }
    
    /**
     * Getter for property desrem.
     * @return Value of property desrem.
     */
    public java.lang.String getDesrem() {
        return desrem;
    }
    
    /**
     * Setter for property desrem.
     * @param desrem New value of property desrem.
     */
    public void setDesrem(java.lang.String desrem) {
        this.desrem = desrem;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property fec_cumplido.
     * @return Value of property fec_cumplido.
     */
    public java.lang.String getFec_cumplido() {
        return fec_cumplido;
    }
    
    /**
     * Setter for property fec_cumplido.
     * @param fec_cumplido New value of property fec_cumplido.
     */
    public void setFec_cumplido(java.lang.String fec_cumplido) {
        this.fec_cumplido = fec_cumplido;
    }
    
    /**
     * Getter for property fec_devolucion.
     * @return Value of property fec_devolucion.
     */
    public java.lang.String getFec_devolucion() {
        return fec_devolucion;
    }
    
    /**
     * Setter for property fec_devolucion.
     * @param fec_devolucion New value of property fec_devolucion.
     */
    public void setFec_devolucion(java.lang.String fec_devolucion) {
        this.fec_devolucion = fec_devolucion;
    }
    
    /**
     * Getter for property fechareporte.
     * @return Value of property fechareporte.
     */
    public java.lang.String getFechareporte() {
        return fechareporte;
    }
    
    /**
     * Setter for property fechareporte.
     * @param fechareporte New value of property fechareporte.
     */
    public void setFechareporte(java.lang.String fechareporte) {
        this.fechareporte = fechareporte;
    }
    
    /**
     * Getter for property fechdevbod.
     * @return Value of property fechdevbod.
     */
    public java.lang.String getFechdevbod() {
        return fechdevbod;
    }
    
    /**
     * Setter for property fechdevbod.
     * @param fechdevbod New value of property fechdevbod.
     */
    public void setFechdevbod(java.lang.String fechdevbod) {
        this.fechdevbod = fechdevbod;
    }
    
    /**
     * Getter for property fecrechazo.
     * @return Value of property fecrechazo.
     */
    public java.lang.String getFecrechazo() {
        return fecrechazo;
    }
    
    /**
     * Setter for property fecrechazo.
     * @param fecrechazo New value of property fecrechazo.
     */
    public void setFecrechazo(java.lang.String fecrechazo) {
        this.fecrechazo = fecrechazo;
    }
    
    /**
     * Getter for property motivodev.
     * @return Value of property motivodev.
     */
    public java.lang.String getMotivodev() {
        return motivodev;
    }
    
    /**
     * Setter for property motivodev.
     * @param motivodev New value of property motivodev.
     */
    public void setMotivodev(java.lang.String motivodev) {
        this.motivodev = motivodev;
    }
    
    /**
     * Getter for property nombredev.
     * @return Value of property nombredev.
     */
    public java.lang.String getNombredev() {
        return nombredev;
    }
    
    /**
     * Setter for property nombredev.
     * @param nombredev New value of property nombredev.
     */
    public void setNombredev(java.lang.String nombredev) {
        this.nombredev = nombredev;
    }
    
    /**
     * Getter for property nombrerep.
     * @return Value of property nombrerep.
     */
    public java.lang.String getNombrerep() {
        return nombrerep;
    }
    
    /**
     * Setter for property nombrerep.
     * @param nombrerep New value of property nombrerep.
     */
    public void setNombrerep(java.lang.String nombrerep) {
        this.nombrerep = nombrerep;
    }
    
    /**
     * Getter for property notasxom.
     * @return Value of property notasxom.
     */
    public java.lang.String getNotasxom() {
        return notasxom;
    }
    
    /**
     * Setter for property notasxom.
     * @param notasxom New value of property notasxom.
     */
    public void setNotasxom(java.lang.String notasxom) {
        this.notasxom = notasxom;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numrechazo.
     * @return Value of property numrechazo.
     */
    public java.lang.String getNumrechazo() {
        return numrechazo;
    }
    
    /**
     * Setter for property numrechazo.
     * @param numrechazo New value of property numrechazo.
     */
    public void setNumrechazo(java.lang.String numrechazo) {
        this.numrechazo = numrechazo;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property orirem.
     * @return Value of property orirem.
     */
    public java.lang.String getOrirem() {
        return orirem;
    }
    
    /**
     * Setter for property orirem.
     * @param orirem New value of property orirem.
     */
    public void setOrirem(java.lang.String orirem) {
        this.orirem = orirem;
    }
    
    /**
     * Getter for property plaveh.
     * @return Value of property plaveh.
     */
    public java.lang.String getPlaveh() {
        return plaveh;
    }
    
    /**
     * Setter for property plaveh.
     * @param plaveh New value of property plaveh.
     */
    public void setPlaveh(java.lang.String plaveh) {
        this.plaveh = plaveh;
    }
    
    /**
     * Getter for property producto.
     * @return Value of property producto.
     */
    public java.lang.String getProducto() {
        return producto;
    }
    
    /**
     * Setter for property producto.
     * @param producto New value of property producto.
     */
    public void setProducto(java.lang.String producto) {
        this.producto = producto;
    }
    
    /**
     * Getter for property respondev.
     * @return Value of property respondev.
     */
    public java.lang.String getRespondev() {
        return respondev;
    }
    
    /**
     * Setter for property respondev.
     * @param respondev New value of property respondev.
     */
    public void setRespondev(java.lang.String respondev) {
        this.respondev = respondev;
    }
    
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc() {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc(java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    /**
     * Getter for property tipoemp.
     * @return Value of property tipoemp.
     */
    public java.lang.String getTipoemp() {
        return tipoemp;
    }
    
    /**
     * Setter for property tipoemp.
     * @param tipoemp New value of property tipoemp.
     */
    public void setTipoemp(java.lang.String tipoemp) {
        this.tipoemp = tipoemp;
    }
    
    /**
     * Getter for property ubicacion.
     * @return Value of property ubicacion.
     */
    public java.lang.String getUbicacion() {
        return ubicacion;
    }
    
    /**
     * Setter for property ubicacion.
     * @param ubicacion New value of property ubicacion.
     */
    public void setUbicacion(java.lang.String ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    /**
     * Getter for property cantdev.
     * @return Value of property cantdev.
     */
    public int getCantdev() {
        return cantdev;
    }
    
    /**
     * Setter for property cantdev.
     * @param cantdev New value of property cantdev.
     */
    public void setCantdev(int cantdev) {
        this.cantdev = cantdev;
    }
    
}
