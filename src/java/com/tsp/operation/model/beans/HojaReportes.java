/******************************************************************************** 
 * Nombre clase :                   HojaReportes.java                           *
 * Descripcion :                    Estructura del Reporte Factura Destinatario *
 * Autor :                          LREALES                                     *
 * Fecha :                          03 de abril de 2006, 02:25 PM               *
 * Version :                        1.0                                         *
 * Copyright :                      Fintravalores S.A.                     *
 ********************************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class HojaReportes {
    
    // 1. Datos Basicos
    private String tipo = "";
    private String numero = "";
    private String destinatario = "";
    private String tipo_documento = "";
    private String documento = "";
    private String tipo_doc_rel = "";    
    private String doc_rel = "";
    private String fecha_cum = "";
    private String cantidad_cum = "";
    private String discrepancia  = "";
    private String observacion = "";
    
    public static HojaReportes load ( ResultSet rs ) throws SQLException {
    
        HojaReportes hojaReportes = new HojaReportes();
        
        hojaReportes.setTipo(rs.getString("tipo"));
        hojaReportes.setNumero(rs.getString("numero"));
        hojaReportes.setDestinatario(rs.getString("destinatario"));
        hojaReportes.setTipo_documento(rs.getString("tipo_documento"));
        hojaReportes.setDocumento(rs.getString("documento"));
        hojaReportes.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
        hojaReportes.setDoc_rel(rs.getString("doc_rel"));
        hojaReportes.setFecha_cum(rs.getString("fecha_cum"));
        hojaReportes.setCantidad_cum(rs.getString("cantidad_cum"));
        hojaReportes.setDiscrepancia(rs.getString("discrepancia"));
        hojaReportes.setObservacion(rs.getString("observacion"));
        
        return hojaReportes;
    
    }
          
    /**
     * 1. Datos Basicos:
     * Almacena datos basicos de la planilla
     * @autor........  LREALES
     * @param........  String val : valores 
     * @version ..... 1.0      
     */ 
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property numero.
     * @return Value of property numero.
     */
    public java.lang.String getNumero() {
        return numero;
    }
    
    /**
     * Setter for property numero.
     * @param numero New value of property numero.
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }
    
    /**
     * Getter for property destinatario.
     * @return Value of property destinatario.
     */
    public java.lang.String getDestinatario() {
        return destinatario;
    }
    
    /**
     * Setter for property destinatario.
     * @param destinatario New value of property destinatario.
     */
    public void setDestinatario(java.lang.String destinatario) {
        this.destinatario = destinatario;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property tipo_doc_rel.
     * @return Value of property tipo_doc_rel.
     */
    public java.lang.String getTipo_doc_rel() {
        return tipo_doc_rel;
    }
    
    /**
     * Setter for property tipo_doc_rel.
     * @param tipo_doc_rel New value of property tipo_doc_rel.
     */
    public void setTipo_doc_rel(java.lang.String tipo_doc_rel) {
        this.tipo_doc_rel = tipo_doc_rel;
    }
    
    /**
     * Getter for property doc_rel.
     * @return Value of property doc_rel.
     */
    public java.lang.String getDoc_rel() {
        return doc_rel;
    }
    
    /**
     * Setter for property doc_rel.
     * @param doc_rel New value of property doc_rel.
     */
    public void setDoc_rel(java.lang.String doc_rel) {
        this.doc_rel = doc_rel;
    }
    
    /**
     * Getter for property fecha_cum.
     * @return Value of property fecha_cum.
     */
    public java.lang.String getFecha_cum() {
        return fecha_cum;
    }
    
    /**
     * Setter for property fecha_cum.
     * @param fecha_cum New value of property fecha_cum.
     */
    public void setFecha_cum(java.lang.String fecha_cum) {
        this.fecha_cum = fecha_cum;
    }
    
    /**
     * Getter for property cantidad_cum.
     * @return Value of property cantidad_cum.
     */
    public java.lang.String getCantidad_cum() {
        return cantidad_cum;
    }
    
    /**
     * Setter for property cantidad_cum.
     * @param cantidad_cum New value of property cantidad_cum.
     */
    public void setCantidad_cum(java.lang.String cantidad_cum) {
        this.cantidad_cum = cantidad_cum;
    }
    
    /**
     * Getter for property discrepancia.
     * @return Value of property discrepancia.
     */
    public java.lang.String getDiscrepancia() {
        return discrepancia;
    }
    
    /**
     * Setter for property discrepancia.
     * @param discrepancia New value of property discrepancia.
     */
    public void setDiscrepancia(java.lang.String discrepancia) {
        this.discrepancia = discrepancia;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
} 