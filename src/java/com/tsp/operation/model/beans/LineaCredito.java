/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 * Bean para la tabla fin.linea_credito
 * 20/05/2014
 * @author lcanchila
 */
public class LineaCredito {
    
    private int id;
    private String linea;
    private String dstrct;
    private String hc;
    

    /**
     * @return the linea
     */
    public String getLinea() {
        return linea;
    }

    /**
     * @param linea the linea to set
     */
    public void setLinea(String linea) {
        this.linea = linea;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the hc
     */
    public String getHc() {
        return hc;
    }

    /**
     * @param hc the hc to set
     */
    public void setHc(String hc) {
        this.hc = hc;
    }
    
}
