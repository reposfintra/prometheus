/*
 * CIA.java
 *
 * Created on 16 de diciembre de 2004, 14:29
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
/**
 *
 * @author  Mario Fontalvo
 */
public class CIA implements Serializable{
    private String Distrito;
    private String NombreAseguradora;
    private String PolizaAseguradora;
    private String FechaVencimientoPoliza;
    private String CodigoRegional;
    private String CodigoEmpresa;
    private String RangoInicial;
    private String RangoFinal;
    private String Resolucion;
    
    private String moneda;
    private String base;
    
    
    /** Creates a new instance of CIA */
    public CIA() {
    }
    
    public static CIA load (ResultSet rs)throws Exception{
        CIA datos = new CIA();
        datos.setDistrito               (rs.getString(1));
        datos.setNombreAseguradora      (rs.getString(2));
        datos.setPolizaAseguradora      (rs.getString(3));
        datos.setFechaVencimientoPoliza (rs.getString(4));
        datos.setCodigoRegional         (rs.getString(5));
        datos.setCodigoEmpresa          (rs.getString(6));
        datos.setRangoInicial           (rs.getString(7));
        datos.setRangoFinal             (rs.getString(8));
        datos.setResolucion             (rs.getString(9));    
        return datos;
    }
    // setter
    public void setDistrito(String valor){
        this.Distrito = valor;
    }
    public void setNombreAseguradora(String valor){
        this.NombreAseguradora = valor;
    }
    public void setPolizaAseguradora(String valor){
        this.PolizaAseguradora = valor;
    }
    public void setFechaVencimientoPoliza(String valor){
        this.FechaVencimientoPoliza = valor;
    }
    public void setCodigoRegional(String valor){
        this.CodigoRegional = valor;
    }
    public void setCodigoEmpresa(String valor){
        this.CodigoEmpresa = valor;
    }
    public void setRangoInicial(String valor){
        this.RangoInicial = valor;
    }
    public void setRangoFinal(String valor){
        this.RangoFinal = valor;
    }
    public void setResolucion(String valor){
        this.Resolucion = valor;
    }    
    
    // getter
    public String getDistrito(){
        return this.Distrito;
    }
    public String getNombreAseguradora(){
        return this.NombreAseguradora;
    }
    public String getPolizaAseguradora(){
        return this.PolizaAseguradora;
    }
    public String getFechaVencimientoPoliza(){
        return this.FechaVencimientoPoliza;
    }
    public String getCodigoRegional(){
        return this.CodigoRegional;
    }
    public String getCodigoEmpresa(){
        return this.CodigoEmpresa;
    }
    public String getRangoInicial(){
        return this.RangoInicial;
    }
    public String getRangoFinal(){
        return this.RangoFinal;
    }
    public String getResolucion(){
        return this.Resolucion;
    }    
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
}
