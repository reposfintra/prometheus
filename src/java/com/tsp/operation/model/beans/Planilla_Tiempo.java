/*
 * Planilla_Tiempo.java
 *
 * Created on 24 de noviembre de 2004, 10:47 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  KREALES
 */
public class Planilla_Tiempo {
    
    private String dstrct;
    private String pla;
    private String sj;
    private String cf_code;
    private String time_code;
    private int secuence;
    private String date_time_traffic;
    private String creation_user;
    
    public void setDstrct(String dstrct){
        this.dstrct=dstrct;
    }
    public String getDstrct(){
        return dstrct;
    }
    
    public void setPla(String pla){
        this.pla=pla;
    }
    public String getPla(){
        return pla;
    }
    public void setSj(String sj){
        this.sj=sj;
    }
    public String getSj(){
        return sj;
    }
    public void setCf_code(String cf_code){
        this.cf_code=cf_code;
    }
    public String getCf_code(){
        return cf_code;
    }

    public void setTime_code(String time_code){
        this.time_code=time_code;
    }
    public String getTime_code(){
        return time_code;
    }
    
    public void setSecuence(int secuence){
        this.secuence=secuence;
    }
    public int getSecuence(){
        return secuence;
    }
    public void setDate_time_traffic(String date_time_traffic){
        this.date_time_traffic=date_time_traffic;
    }
    public String getDate_time_traffic(){
        return date_time_traffic;
    }
    
    public void setCreation_user(String creation_user){
        this.creation_user=creation_user;
    }
    public String getCreation_user(){
        return creation_user;
    }
    
}
