
package com.tsp.operation.model.beans;


public class AnticipoPlanilla {
    
// Atributos  
    
    private String agencia;
    private String tipoDocumento;
    private String documento;
    private String item;
    private String concepto;
    private double valor;
    private String moneda;   
    private String banco;
    private String agenciaBanco;
    private String cuenta;
    private String fecha;
    private String descripcion;
    
    private String date_doc; // fecaha del movimiento
    private String planilla;
    private String reg_status;
    private double valor_for;
   
 // Metodos  
    
    //set
    public AnticipoPlanilla() {}
    
    public void setAgencia(String agencia){
        this.agencia=agencia;
    }
    
    public void setAgenciaBanco(String agencia){
        this.agenciaBanco=agencia;
    }
    
     public void setBanco(String banco){
        this.banco=banco;
    }
    
    public void setTipoDocumento(String tipo){
        this.tipoDocumento=tipo;
    }
    
    public void setDocumento(String doc){
        this.documento=doc;
    }
    
    public void setItem(String item){
        this.item=item;
    }
    
    public void setConcepto(String concepto){
        this.concepto=concepto;
    }
    
    public void setFecha(String fecha){
        this.fecha=fecha;
    }
    
    public void setValor(double valor){
        this.valor=valor;
    }
    
    public void setMoneda(String moneda){
        this.moneda=moneda;
    }
    
    public void setCuenta(String cuenta){
        this.cuenta=cuenta;
    }
    
  //get
    public String getAgencia(){
        return this.agencia;
    }
    
     public String getAgenciaBanco(){
        return this.agenciaBanco;
    }
    
     public String  getBanco(){
        return this.banco;
    }
    
    public String getTipoDocumento(){
        return this.tipoDocumento;
    }
    
    public String getDocumento(){
        return this.documento;
    }
    
    public String getItem(){
        return this.item;
    }
    
    public String getConcepto(){
        return this.concepto;
    }
    
    public String getFecha(){
        return this.fecha;
    }
    
    public double getValor(){
        return this.valor;
    }
    
    public String getMoneda(){
        return this.moneda;
    }
    
     public String getCuenta(){
        return this.cuenta;
    }
     
     /**
      * Getter for property descripcion.
      * @return Value of property descripcion.
      */
     public java.lang.String getDescripcion() {
         return descripcion;
     }
     
     /**
      * Setter for property descripcion.
      * @param descripcion New value of property descripcion.
      */
     public void setDescripcion(java.lang.String descripcion) {
         this.descripcion = descripcion;
     }
     
     /**
      * Getter for property date_doc.
      * @return Value of property date_doc.
      */
     public java.lang.String getDate_doc() {
         return date_doc;
     }
     
     /**
      * Setter for property date_doc.
      * @param date_doc New value of property date_doc.
      */
     public void setDate_doc(java.lang.String date_doc) {
         this.date_doc = date_doc;
     }
     
     /**
      * Getter for property planiilla.
      * @return Value of property planiilla.
      */
     public java.lang.String getPlanilla() {
         return planilla;
     }
     
     /**
      * Setter for property planiilla.
      * @param planiilla New value of property planiilla.
      */
     public void setPlanilla(java.lang.String planilla) {
         this.planilla = planilla;
     }
     
     /**
      * Getter for property reg_status.
      * @return Value of property reg_status.
      */
     public java.lang.String getReg_status() {
         return reg_status;
     }
     
     /**
      * Setter for property reg_status.
      * @param reg_status New value of property reg_status.
      */
     public void setReg_status(java.lang.String reg_status) {
         this.reg_status = reg_status;
     }
     
     /**
      * Getter for property valor_for.
      * @return Value of property valor_for.
      */
     public double getValor_for() {
         return valor_for;
     }
     
     /**
      * Setter for property valor_for.
      * @param valor_for New value of property valor_for.
      */
     public void setValor_for(double valor_for) {
         this.valor_for = valor_for;
     }
     
}
