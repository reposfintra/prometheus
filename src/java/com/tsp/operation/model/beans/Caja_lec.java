/*
 *  Nombre clase    :  Caja_lec.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 4 de agosto de 2006, 10:03 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;

public class Caja_lec {
    
    private String num_caja;    
    private String descripcion;    
    private String codigo_cliente;
    private String dato;
    private String reg_status;
    
    /** Crea una nueva instancia de  Caja_lec */
    public Caja_lec() {
        num_caja        = "";
        descripcion     = "";
        codigo_cliente  = "";
        dato            = "";
        reg_status      = "";
    }
    
    
    public static Caja_lec load(ResultSet rs)throws Exception{
        Caja_lec datos = new Caja_lec();
        datos.setNum_caja(rs.getString("table_code"));
        datos.setCodigo_cliente(rs.getString("referencia"));
        datos.setDescripcion(rs.getString("descripcion"));
        datos.setDato(rs.getString("dato"));
        datos.setReg_status(rs.getString("reg_status"));
        return datos;
    }
    
    /**
     * Getter for property codigo_cliente.
     * @return Value of property codigo_cliente.
     */
    public java.lang.String getCodigo_cliente() {
        return codigo_cliente;
    }    
    
    /**
     * Setter for property codigo_cliente.
     * @param codigo_cliente New value of property codigo_cliente.
     */
    public void setCodigo_cliente(java.lang.String codigo_cliente) {
        this.codigo_cliente = codigo_cliente;
    }    
    
    /**
     * Getter for property dato.
     * @return Value of property dato.
     */
    public java.lang.String getDato() {
        return dato;
    }
    
    /**
     * Setter for property dato.
     * @param dato New value of property dato.
     */
    public void setDato(java.lang.String dato) {
        this.dato = dato;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property num_caja.
     * @return Value of property num_caja.
     */
    public java.lang.String getNum_caja() {
        return num_caja;
    }
    
    /**
     * Setter for property num_caja.
     * @param num_caja New value of property num_caja.
     */
    public void setNum_caja(java.lang.String num_caja) {
        this.num_caja = num_caja;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
}
