/***********************************************************************************
 * Nombre clase : ............... HojaVida.java                                    *
 * Descripcion :................. Clase que representa con sus atributos una hoja  *
 *                                de vida                                          *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 03 de enero de 2005                              *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.beans;

public class HojaVida extends Object  {
    
    private Conductor    conductor;
    private Placa        placa;
    private Proveedor    prop;
    private Tenedor      tenedor;
    private Identidad    identidad;
    private String       fotoVehiculo;
    private String       fotoConductor;
    //nuevo diogenes 07-03-2006
    private String       huella_der;
    private String       huella_izq;
    private String       leyenda1;
    private String       leyenda2;
    //2006-04-18
    private String       firma;
    private String       agenUsuarioCrea;
    private String       agenUsuarioMod;
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public com.tsp.operation.model.beans.Conductor getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(com.tsp.operation.model.beans.Conductor conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public com.tsp.operation.model.beans.Placa getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(com.tsp.operation.model.beans.Placa placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property popietario.
     * @return Value of property popietario.
     */
    public com.tsp.operation.model.beans.Proveedor getPropietario() {
        return prop;
    }
    
    /**
     * Setter for property popietario.
     * @param popietario New value of property popietario.
     */
    public void setPropietario(com.tsp.operation.model.beans.Proveedor propietario) {
        this.prop = propietario;
    }
    
    /**
     * Getter for property tenedor.
     * @return Value of property tenedor.
     */
    public com.tsp.operation.model.beans.Tenedor getTenedor() {
        return tenedor;
    }
    
    /**
     * Setter for property tenedor.
     * @param tenedor New value of property tenedor.
     */
    public void setTenedor(com.tsp.operation.model.beans.Tenedor tenedor) {
        this.tenedor = tenedor;
    }
    
    /**
     * Getter for property identidad.
     * @return Value of property identidad.
     */
    public com.tsp.operation.model.beans.Identidad getIdentidad() {
        return identidad;
    }
    
    /**
     * Setter for property identidad.
     * @param identidad New value of property identidad.
     */
    public void setIdentidad(com.tsp.operation.model.beans.Identidad identidad) {
        this.identidad = identidad;
    }
    
    /**
     * Getter for property fotoVehiculo.
     * @return Value of property fotoVehiculo.
     */
    public java.lang.String getFotoVehiculo() {
        return fotoVehiculo;
    }
    
    /**
     * Setter for property fotoVehiculo.
     * @param fotoVehiculo New value of property fotoVehiculo.
     */
    public void setFotoVehiculo(java.lang.String fotoVehiculo) {
        this.fotoVehiculo = fotoVehiculo;
    }
    
    /**
     * Getter for property fotoConductor.
     * @return Value of property fotoConductor.
     */
    public java.lang.String getFotoConductor() {
        return fotoConductor;
    }
    
    /**
     * Setter for property fotoConductor.
     * @param fotoConductor New value of property fotoConductor.
     */
    public void setFotoConductor(java.lang.String fotoConductor) {
        this.fotoConductor = fotoConductor;
    }
    
    /**
     * Getter for property huella_der.
     * @return Value of property huella_der.
     */
    public java.lang.String getHuella_der() {
        return huella_der;
    }
    
    /**
     * Setter for property huella_der.
     * @param huella_der New value of property huella_der.
     */
    public void setHuella_der(java.lang.String huella_der) {
        this.huella_der = huella_der;
    }
    
    /**
     * Getter for property huella_izq.
     * @return Value of property huella_izq.
     */
    public java.lang.String getHuella_izq() {
        return huella_izq;
    }
    
    /**
     * Setter for property huella_izq.
     * @param huella_izq New value of property huella_izq.
     */
    public void setHuella_izq(java.lang.String huella_izq) {
        this.huella_izq = huella_izq;
    }
    
    /**
     * Getter for property leyenda1.
     * @return Value of property leyenda1.
     */
    public java.lang.String getLeyenda1() {
        return leyenda1;
    }
    
    /**
     * Setter for property leyenda1.
     * @param leyenda1 New value of property leyenda1.
     */
    public void setLeyenda1(java.lang.String leyenda1) {
        this.leyenda1 = leyenda1;
    }
    
    /**
     * Getter for property leyenda2.
     * @return Value of property leyenda2.
     */
    public java.lang.String getLeyenda2() {
        return leyenda2;
    }
    
    /**
     * Setter for property leyenda2.
     * @param leyenda2 New value of property leyenda2.
     */
    public void setLeyenda2(java.lang.String leyenda2) {
        this.leyenda2 = leyenda2;
    }
    
    /**
     * Getter for property firma.
     * @return Value of property firma.
     */
    public java.lang.String getFirma() {
        return firma;
    }
    
    /**
     * Setter for property firma.
     * @param firma New value of property firma.
     */
    public void setFirma(java.lang.String firma) {
        this.firma = firma;
    }
    
    /**
     * Getter for property agenUsuarioCrea.
     * @return Value of property agenUsuarioCrea.
     */
    public java.lang.String getAgenUsuarioCrea() {
        return agenUsuarioCrea;
    }
    
    /**
     * Setter for property agenUsuarioCrea.
     * @param agenUsuarioCrea New value of property agenUsuarioCrea.
     */
    public void setAgenUsuarioCrea(java.lang.String agenUsuarioCrea) {
        this.agenUsuarioCrea = agenUsuarioCrea;
    }
    
    /**
     * Getter for property agenUsuarioMod.
     * @return Value of property agenUsuarioMod.
     */
    public java.lang.String getAgenUsuarioMod() {
        return agenUsuarioMod;
    }
    
    /**
     * Setter for property agenUsuarioMod.
     * @param agenUsuarioMod New value of property agenUsuarioMod.
     */
    public void setAgenUsuarioMod(java.lang.String agenUsuarioMod) {
        this.agenUsuarioMod = agenUsuarioMod;
    }
    
}
