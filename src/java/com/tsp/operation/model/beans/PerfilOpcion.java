/*
 * Nombre        PerfilOpcion.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  Sandrameg
 */
public class PerfilOpcion implements Serializable {
    
    private String id_perfil;
    private int id_opcion; 
    private String estado;
    
    /** Creates a new instance of perfilOpcion */
    public PerfilOpcion load ( ResultSet rs ) throws SQLException {
        PerfilOpcion perfilOpcion = new PerfilOpcion();
        perfilOpcion.setId_opcion(rs.getInt ("id_opcion"));
        perfilOpcion.setId_perfil(rs.getString("id_perfil"));
        
        return perfilOpcion;
    }
    
    /**
     * Getter for property id_opcion.
     * @return Value of property id_opcion.
     */
    public int getId_opcion() {
        return id_opcion;
    }
    
    /**
     * Setter for property id_opcion.
     * @param id_opcion New value of property id_opcion.
     */
    public void setId_opcion(int id_opcion) {
        this.id_opcion = id_opcion;
    }
    
    /**
     * Getter for property id_perfil.
     * @return Value of property id_perfil.
     */
    public java.lang.String getId_perfil() {
        return id_perfil;
    }
    
    /**
     * Setter for property id_perfil.
     * @param id_perfil New value of property id_perfil.
     */
    public void setId_perfil(java.lang.String id_perfil) {
        this.id_perfil = id_perfil;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
}   