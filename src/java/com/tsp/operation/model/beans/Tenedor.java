/**************************************************************************
 * Nombre:        Propietario.java                   
 * Descripci�n:   Beans de acuerdo especial.             
 * Autor:         Ing. Henry Osorio
 * Fecha:         14 de junio de 2005, 10:01 PM     
 * Versi�n:       Java  1.0                          
 * Copyright:     Fintravalores S.A. S.A.       
 **************************************************************************/

package com.tsp.operation.model.beans;


public class Tenedor {    
   
    //hojavida
    private String nombre;
    private String cedula;
    private String expedicionced;
    private String direccion;
    private String ciudad;    
    private String telefono;     
    private String celular;         
    private String atitulo;
    
    private String nom1;
    private String nom2;
    private String ape1;
    private String ape2;
    
    private String pais;
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula() {
        return cedula;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula(java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Getter for property expedicionced.
     * @return Value of property expedicionced.
     */
    public java.lang.String getExpedicionced() {
        return expedicionced;
    }
    
    /**
     * Setter for property expedicionced.
     * @param expedicionced New value of property expedicionced.
     */
    public void setExpedicionced(java.lang.String expedicionced) {
        this.expedicionced = expedicionced;
    }
    
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public java.lang.String getTelefono() {
        return telefono;
    }
    
    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }
    
    /**
     * Getter for property celular.
     * @return Value of property celular.
     */
    public java.lang.String getCelular() {
        return celular;
    }
    
    /**
     * Setter for property celular.
     * @param celular New value of property celular.
     */
    public void setCelular(java.lang.String celular) {
        this.celular = celular;
    }
    
    /**
     * Getter for property atitulo.
     * @return Value of property atitulo.
     */
    public java.lang.String getAtitulo() {
        return atitulo;
    }
    
    /**
     * Setter for property atitulo.
     * @param atitulo New value of property atitulo.
     */
    public void setAtitulo(java.lang.String atitulo) {
        this.atitulo = atitulo;
    }
    
    /**
     * Getter for property nom1.
     * @return Value of property nom1.
     */
    public java.lang.String getNom1() {
        return nom1;
    }
    
    /**
     * Setter for property nom1.
     * @param nom1 New value of property nom1.
     */
    public void setNom1(java.lang.String nom1) {
        this.nom1 = nom1;
    }
    
    /**
     * Getter for property nom2.
     * @return Value of property nom2.
     */
    public java.lang.String getNom2() {
        return nom2;
    }
    
    /**
     * Setter for property nom2.
     * @param nom2 New value of property nom2.
     */
    public void setNom2(java.lang.String nom2) {
        this.nom2 = nom2;
    }
    
    /**
     * Getter for property ape1.
     * @return Value of property ape1.
     */
    public java.lang.String getApe1() {
        return ape1;
    }
    
    /**
     * Setter for property ape1.
     * @param ape1 New value of property ape1.
     */
    public void setApe1(java.lang.String ape1) {
        this.ape1 = ape1;
    }
    
    /**
     * Getter for property ape2.
     * @return Value of property ape2.
     */
    public java.lang.String getApe2() {
        return ape2;
    }
    
    public void setApe2(java.lang.String ape2) {
        this.ape2 = ape2;
    }
    
    /**
     * Getter for property pais.
     * @return Value of property pais.
     */
    public java.lang.String getPais() {
        return pais;
    }
    
    /**
     * Setter for property pais.
     * @param pais New value of property pais.
     */
    public void setPais(java.lang.String pais) {
        this.pais = pais;
    }
    
}