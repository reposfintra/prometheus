/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class BeansMultiservicio {
    String parametro1;
    String parametro2;
    String parametro3;
    String parametro4;
    String parametro5;
    String parametro6;
    String parametro7;
    String parametro8;
    String parametro9;
    String parametro10;
    int parametro11;
    int parametro12;
    int parametro13;
    int parametro14;
    int parametro15;
    double parametro16;
    double parametro17;
    double parametro18;
    double parametro19;
    double parametro20;

    public String getParametro1() {
        return parametro1;
    }

    public void setParametro1(String parametro1) {
        this.parametro1 = parametro1;
    }

    public String getParametro2() {
        return parametro2;
    }

    public void setParametro2(String parametro2) {
        this.parametro2 = parametro2;
    }

    public String getParametro3() {
        return parametro3;
    }

    public void setParametro3(String parametro3) {
        this.parametro3 = parametro3;
    }

    public String getParametro4() {
        return parametro4;
    }

    public void setParametro4(String parametro4) {
        this.parametro4 = parametro4;
    }

    public String getParametro5() {
        return parametro5;
    }

    public void setParametro5(String parametro5) {
        this.parametro5 = parametro5;
    }

    public String getParametro6() {
        return parametro6;
    }

    public void setParametro6(String parametro6) {
        this.parametro6 = parametro6;
    }

    public String getParametro7() {
        return parametro7;
    }

    public void setParametro7(String parametro7) {
        this.parametro7 = parametro7;
    }

    public String getParametro8() {
        return parametro8;
    }

    public void setParametro8(String parametro8) {
        this.parametro8 = parametro8;
    }

    public String getParametro9() {
        return parametro9;
    }

    public void setParametro9(String parametro9) {
        this.parametro9 = parametro9;
    }

    public String getParametro10() {
        return parametro10;
    }

    public void setParametro10(String parametro10) {
        this.parametro10 = parametro10;
    }

    public int getParametro11() {
        return parametro11;
    }

    public void setParametro11(int parametro11) {
        this.parametro11 = parametro11;
    }

    public int getParametro12() {
        return parametro12;
    }

    public void setParametro12(int parametro12) {
        this.parametro12 = parametro12;
    }

    public int getParametro13() {
        return parametro13;
    }

    public void setParametro13(int parametro13) {
        this.parametro13 = parametro13;
    }

    public int getParametro14() {
        return parametro14;
    }

    public void setParametro14(int parametro14) {
        this.parametro14 = parametro14;
    }

    public int getParametro15() {
        return parametro15;
    }

    public void setParametro15(int parametro15) {
        this.parametro15 = parametro15;
    }

    public double getParametro16() {
        return parametro16;
    }

    public void setParametro16(double parametro16) {
        this.parametro16 = parametro16;
    }

    public double getParametro17() {
        return parametro17;
    }

    public void setParametro17(double parametro17) {
        this.parametro17 = parametro17;
    }

    public double getParametro18() {
        return parametro18;
    }

    public void setParametro18(double parametro18) {
        this.parametro18 = parametro18;
    }

    public double getParametro19() {
        return parametro19;
    }

    public void setParametro19(double parametro19) {
        this.parametro19 = parametro19;
    }

    public double getParametro20() {
        return parametro20;
    }

    public void setParametro20(double parametro20) {
        this.parametro20 = parametro20;
    }
    
    
}
