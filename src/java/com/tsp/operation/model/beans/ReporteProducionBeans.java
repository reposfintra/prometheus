/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mmedina
 */
public class ReporteProducionBeans {
    private String periodo_anticipo;
    private String periodo_contabilizacion;
    private String descripcion;
    private double valor;
    private double valor_descuento;
    private double valor_neto;
    private double valor_combancaria;
    private double vlr_consignacion;
    private int rowspan;
    private String display;
    private AtributosRowsProd attr;

    public ReporteProducionBeans() {
    }

    /**
     * @return the periodo_anticipo
     */
    public String getPeriodo_anticipo() {
        return periodo_anticipo;
    }

    /**
     * @param periodo_anticipo the periodo_anticipo to set
     */
    public void setPeriodo_anticipo(String periodo_anticipo) {
        this.periodo_anticipo = periodo_anticipo;
    }

    /**
     * @return the periodo_contabilizacion
     */
    public String getPeriodo_contabilizacion() {
        return periodo_contabilizacion;
    }

    /**
     * @param periodo_contabilizacion the periodo_contabilizacion to set
     */
    public void setPeriodo_contabilizacion(String periodo_contabilizacion) {
        this.periodo_contabilizacion = periodo_contabilizacion;
    }

    /**
     * @return the valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * @return the valor_descuento
     */
    public double getValor_descuento() {
        return valor_descuento;
    }

    /**
     * @param valor_descuento the valor_descuento to set
     */
    public void setValor_descuento(double valor_descuento) {
        this.valor_descuento = valor_descuento;
    }

    /**
     * @return the valor_neto
     */
    public double getValor_neto() {
        return valor_neto;
    }

    /**
     * @param valor_neto the valor_neto to set
     */
    public void setValor_neto(double valor_neto) {
        this.valor_neto = valor_neto;
    }

    /**
     * @return the valor_combancaria
     */
    public double getValor_combancaria() {
        return valor_combancaria;
    }

    /**
     * @param valor_combancaria the valor_combancaria to set
     */
    public void setValor_combancaria(double valor_combancaria) {
        this.valor_combancaria = valor_combancaria;
    }

    /**
     * @return the vlr_consignacion
     */
    public double getVlr_consignacion() {
        return vlr_consignacion;
    }

    /**
     * @param vlr_consignacion the vlr_consignacion to set
     */
    public void setVlr_consignacion(double vlr_consignacion) {
        this.vlr_consignacion = vlr_consignacion;
    }

    /**
     * @return the arp
     */
    public AtributosRowsProd getArp() {
        return attr;
    }

    /**
     * @param arp the arp to set
     */
    public void setArp(AtributosRowsProd arp) {
        this.attr = arp;
    }

    /**
     * @return the rowspan
     */
    public int getRowspan() {
        return rowspan;
    }

    /**
     * @param rowspan the rowspan to set
     */
    public void setRowspan(int rowspan) {
        this.rowspan = rowspan;
    }

    /**
     * @return the display
     */
    public String getDisplay() {
        return display;
    }

    /**
     * @param display the display to set
     */
    public void setDisplay(String display) {
        this.display = display;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
     
}
