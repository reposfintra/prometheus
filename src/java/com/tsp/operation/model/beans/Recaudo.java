/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class Recaudo {

    private String periodo_recaudo;
    private String nom_empresa;
    private String nom_unicom;
    private String cod_unicom;
    private String gestor;
    private String nom_cli;
    private String nic;
    private String nis_rad;
    private int    num_acu;
    private String simbolo_var;
    private String f_fact;
    private String f_puesta;
    private String co_concepto;
    private String desc_concepto;
    private Double imp_facturado_concepto;
    private Double imp_pagado_concepto;
    private Double imp_recaudo;


    public Recaudo () {
       inicializar() ;

    }



    public void inicializar() {

        periodo_recaudo = "";
        nom_empresa = "";
        nom_unicom = "";
        cod_unicom = "";
        gestor = "";
        nom_cli = "";
        nic = "";
        nis_rad = "";
        num_acu = 0;
        simbolo_var = "";
        f_fact = "";
        f_puesta = "";
        co_concepto = "";
        desc_concepto = "";
        imp_facturado_concepto = 0.00;
        imp_pagado_concepto = 0.00;
        imp_recaudo = 0.00;

    }


    public static Recaudo load(java.sql.ResultSet rs)throws java.sql.SQLException{

        Recaudo recaudo = new Recaudo();

        recaudo.setPeriodo_recaudo(rs.getString("periodo_recaudo") ) ;
        recaudo.setNom_empresa(rs.getString("nom_empresa") ) ;
        recaudo.setNom_unicom(rs.getString("nom_unicom") ) ;
        recaudo.setCod_unicom(rs.getString("cod_unicom") ) ;
        recaudo.setGestor(rs.getString("gestor") ) ;
        recaudo.setNom_cli(rs.getString("nom_cli") ) ;
        recaudo.setNic(rs.getString("nic") ) ;
        recaudo.setNis_rad(rs.getString("nis_rad") ) ;
        recaudo.setNum_acu(rs.getInt("num_acu") ) ;
        recaudo.setSimbolo_var(rs.getString("simbolo_var") ) ;
        recaudo.setF_fact(rs.getString("f_fact") ) ;
        recaudo.setF_puesta(rs.getString("f_puesta") ) ;
        recaudo.setCo_concepto(rs.getString("co_concepto") ) ;
        recaudo.setDesc_concepto(rs.getString("desc_concepto") ) ;
        recaudo.setImp_facturado_concepto(rs.getDouble("imp_facturado_concepto") ) ;
        recaudo.setImp_pagado_concepto(rs.getDouble("imp_pagado_concepto") ) ;
        recaudo.setImp_recaudo(rs.getDouble("imp_recaudo") ) ;

        return recaudo;
    }





    /**
     * @return the periodo_recaudo
     */
    public String getPeriodo_recaudo() {
        return periodo_recaudo;
    }

    /**
     * @param periodo_recaudo the periodo_recaudo to set
     */
    public void setPeriodo_recaudo(String periodo_recaudo) {
        this.periodo_recaudo = periodo_recaudo;
    }

    /**
     * @return the nom_empresa
     */
    public String getNom_empresa() {
        return nom_empresa;
    }

    /**
     * @param nom_empresa the nom_empresa to set
     */
    public void setNom_empresa(String nom_empresa) {
        this.nom_empresa = nom_empresa;
    }

    /**
     * @return the nom_unicom
     */
    public String getNom_unicom() {
        return nom_unicom;
    }

    /**
     * @param nom_unicom the nom_unicom to set
     */
    public void setNom_unicom(String nom_unicom) {
        this.nom_unicom = nom_unicom;
    }

    /**
     * @return the cod_unicom
     */
    public String getCod_unicom() {
        return cod_unicom;
    }

    /**
     * @param cod_unicom the cod_unicom to set
     */
    public void setCod_unicom(String cod_unicom) {
        this.cod_unicom = cod_unicom;
    }

    /**
     * @return the gestor
     */
    public String getGestor() {
        return gestor;
    }

    /**
     * @param gestor the gestor to set
     */
    public void setGestor(String gestor) {
        this.gestor = gestor;
    }

    /**
     * @return the nom_cli
     */
    public String getNom_cli() {
        return nom_cli;
    }

    /**
     * @param nom_cli the nom_cli to set
     */
    public void setNom_cli(String nom_cli) {
        this.nom_cli = nom_cli;
    }

    /**
     * @return the nic
     */
    public String getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(String nic) {
        this.nic = nic;
    }

    /**
     * @return the nis_rad
     */
    public String getNis_rad() {
        return nis_rad;
    }

    /**
     * @param nis_rad the nis_rad to set
     */
    public void setNis_rad(String nis_rad) {
        this.nis_rad = nis_rad;
    }

    /**
     * @return the num_acu
     */
    public int getNum_acu() {
        return num_acu;
    }

    /**
     * @param num_acu the num_acu to set
     */
    public void setNum_acu(int num_acu) {
        this.num_acu = num_acu;
    }

    /**
     * @return the simbolo_var
     */
    public String getSimbolo_var() {
        return simbolo_var;
    }

    /**
     * @param simbolo_var the simbolo_var to set
     */
    public void setSimbolo_var(String simbolo_var) {
        this.simbolo_var = simbolo_var;
    }

    /**
     * @return the f_fact
     */
    public String getF_fact() {
        return f_fact;
    }

    /**
     * @param f_fact the f_fact to set
     */
    public void setF_fact(String f_fact) {
        this.f_fact = f_fact;
    }

    /**
     * @return the f_puesta
     */
    public String getF_puesta() {
        return f_puesta;
    }

    /**
     * @param f_puesta the f_puesta to set
     */
    public void setF_puesta(String f_puesta) {
        this.f_puesta = f_puesta;
    }

    /**
     * @return the co_concepto
     */
    public String getCo_concepto() {
        return co_concepto;
    }

    /**
     * @param co_concepto the co_concepto to set
     */
    public void setCo_concepto(String co_concepto) {
        this.co_concepto = co_concepto;
    }

    /**
     * @return the desc_concepto
     */
    public String getDesc_concepto() {
        return desc_concepto;
    }

    /**
     * @param desc_concepto the desc_concepto to set
     */
    public void setDesc_concepto(String desc_concepto) {
        this.desc_concepto = desc_concepto;
    }

    /**
     * @return the imp_facturado_concepto
     */
    public Double getImp_facturado_concepto() {
        return imp_facturado_concepto;
    }

    /**
     * @param imp_facturado_concepto the imp_facturado_concepto to set
     */
    public void setImp_facturado_concepto(Double imp_facturado_concepto) {
        this.imp_facturado_concepto = imp_facturado_concepto;
    }

    /**
     * @return the imp_pagado_concepto
     */
    public Double getImp_pagado_concepto() {
        return imp_pagado_concepto;
    }

    /**
     * @param imp_pagado_concepto the imp_pagado_concepto to set
     */
    public void setImp_pagado_concepto(Double imp_pagado_concepto) {
        this.imp_pagado_concepto = imp_pagado_concepto;
    }

    /**
     * @return the imp_recaudo
     */
    public Double getImp_recaudo() {
        return imp_recaudo;
    }

    /**
     * @param imp_recaudo the imp_recaudo to set
     */
    public void setImp_recaudo(Double imp_recaudo) {
        this.imp_recaudo = imp_recaudo;
    }

}
