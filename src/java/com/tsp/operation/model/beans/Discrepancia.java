/************************************************************************
 * Nombre clase: DiscrepanciaDAO.java                                   *
 * Descripci�n: Clase que maneja las consultas de las discrepancia y    *
 *              los productos de las discrepancias.                     *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: Created on 23 de septiembre de 2005, 08:43 AM                 *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/
package com.tsp.operation.model.beans;

/**
 *
 * @author  Jose
 */
import java.io.Serializable;

public class Discrepancia extends Object implements Serializable{
    
    private double valor;
    private String remb_conductor;
    private String autorizado;
    private String fecha_autoriza;
    private String usuario_cierre;
    private String fecha_cierre;
    private String usuario_migracion;
    private String fecha_migracion;
    private String observacion;
    private String referencia;
    private String cod_planilla;
    private String tipo_registro;
    private String nota_contable;
    private double cantidad_autorizada;
    private double valor_autorizado;
    //nuevos cambios
    private String placa;
    private String nom_documento;
    private String nom_destinatario;
    private String nom_conductor;
    private String fecha_devolucion;
    private String numero_rechazo;
    private String reportado;
    private String retencion;
    private String ubicacion;
    private String contacto;
    private int nro_discrepancia;
    private String nro_planilla;
    private String nro_remesa;
    private String tipo_doc;
    private String documento;
    private String tipo_doc_rel;
    private String documento_rel;
    private String cliente;
    private String cod_producto;
    private String descripcion;
    private String unidad;
    private String cod_discrepancia;
    private double cantidad;
    private String causa_dev;
    private String responsable;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String distrito;
    private String base;
    //Diogenes 04-01-2006
    private String cod_Cliente;
    private double cantidad_nueva;
    private String nuevo_numpla;
    private String codubicacion;
    
    //jose 03-01-2006
    private String nota_debito;
    private String nota_credito;
    private String usuario_nota_debito;
    private String usuario_nota_credito;
    private String fecha_nota_debito;
    private String fecha_nota_credito;
    private String observacion_cierre;
    private String grupo;
    
    /** Creates a new instance of Discrepancia */
    public Discrepancia () {
    }
    
    public java.lang.String getAutorizado () {
        return autorizado;
    }
    
    public void setAutorizado (java.lang.String autorizado) {
        this.autorizado = autorizado;
    }
    
    public double getCantidad () {
        return cantidad;
    }
    
    public void setCantidad (double cantidad) {
        this.cantidad = cantidad;
    }
    
    public double getCantidad_autorizada () {
        return cantidad_autorizada;
    }
    
    public void setCantidad_autorizada (double cantidad_autorizada) {
        this.cantidad_autorizada = cantidad_autorizada;
    }
    
    public java.lang.String getCod_discrepancia () {
        return cod_discrepancia;
    }
    
    public void setCod_discrepancia (java.lang.String cod_discrepancia) {
        this.cod_discrepancia = cod_discrepancia;
    }
    
    public java.lang.String getCod_planilla () {
        return cod_planilla;
    }
    
    public void setCod_planilla (java.lang.String cod_planilla) {
        this.cod_planilla = cod_planilla;
    }
    
    public java.lang.String getFecha_autoriza () {
        return fecha_autoriza;
    }
    
    public void setFecha_autoriza (java.lang.String fecha_autoriza) {
        this.fecha_autoriza = fecha_autoriza;
    }
    
    public java.lang.String getObservacion () {
        return observacion;
    }
    
    public void setObservacion (java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    public java.lang.String getRemb_conductor () {
        return remb_conductor;
    }
    
    public void setRemb_conductor (java.lang.String remb_conductor) {
        this.remb_conductor = remb_conductor;
    }
    
    public java.lang.String getTipo_registro () {
        return tipo_registro;
    }
    
    public void setTipo_registro (java.lang.String tipo_registro) {
        this.tipo_registro = tipo_registro;
    }
    
    public double getValor () {
        return valor;
    }
    
    public void setValor (double valor) {
        this.valor = valor;
    }
    
    public double getValor_autorizado () {
        return valor_autorizado;
    }
    
    public void setValor_autorizado (double valor_autorizado) {
        this.valor_autorizado = valor_autorizado;
    }
    
    public java.lang.String getBase () {
        return base;
    }
    
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    public java.lang.String getUltima_modificacion () {
        return ultima_modificacion;
    }
    
    public void setUltima_modificacion (java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    public java.lang.String getUsuario_creacion () {
        return usuario_creacion;
    }
    
    public void setUsuario_creacion (java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    public java.lang.String getUsuario_modificacion () {
        return usuario_modificacion;
    }
    
    public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    public java.lang.String getFecha_creacion () {
        return fecha_creacion;
    }
    
    public void setFecha_creacion (java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    public java.lang.String getContacto () {
        return contacto;
    }
    
    public void setContacto (java.lang.String contacto) {
        this.contacto = contacto;
    }
    
    public java.lang.String getResponsable () {
        return responsable;
    }
    
    public void setResponsable (java.lang.String responsable) {
        this.responsable = responsable;
    }
    
    public java.lang.String getUnidad () {
        return unidad;
    }
    
    public void setUnidad (java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    public java.lang.String getReferencia () {
        return referencia;
    }
    
    public void setReferencia (java.lang.String referencia) {
        this.referencia = referencia;
    }
    
    public java.lang.String getFecha_devolucion () {
        return fecha_devolucion;
    }
    
    public void setFecha_devolucion (java.lang.String fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }
    
    public String getNumero_rechazo () {
        return numero_rechazo;
    }
    
    public void setNumero_rechazo (String numero_rechazo) {
        this.numero_rechazo = numero_rechazo;
    }
    
    public java.lang.String getReportado () {
        return reportado;
    }
    
    public void setReportado (java.lang.String reportado) {
        this.reportado = reportado;
    }
    
    public java.lang.String getUbicacion () {
        return ubicacion;
    }
    
    public void setUbicacion (java.lang.String ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    public int getNro_discrepancia () {
        return nro_discrepancia;
    }
    
    public void setNro_discrepancia (int nro_discrepancia) {
        this.nro_discrepancia = nro_discrepancia;
    }
    
    public java.lang.String getNro_planilla () {
        return nro_planilla;
    }
    
    public void setNro_planilla (java.lang.String nro_planilla) {
        this.nro_planilla = nro_planilla;
    }
    
    public java.lang.String getNro_remesa () {
        return nro_remesa;
    }
    
    public void setNro_remesa (java.lang.String nro_remesa) {
        this.nro_remesa = nro_remesa;
    }
    
    public java.lang.String getTipo_doc () {
        return tipo_doc;
    }
    
    public void setTipo_doc (java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    public java.lang.String getDocumento () {
        return documento;
    }
    
    public void setDocumento (java.lang.String documento) {
        this.documento = documento;
    }
    
    public java.lang.String getCliente () {
        return cliente;
    }
    
    public void setCliente (java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    public java.lang.String getCod_producto () {
        return cod_producto;
    }
    
    public void setCod_producto (java.lang.String cod_producto) {
        this.cod_producto = cod_producto;
    }
    
    public java.lang.String getDescripcion () {
        return descripcion;
    }
    
    public void setDescripcion (java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    public java.lang.String getCausa_dev () {
        return causa_dev;
    }
    
    public void setCausa_dev (java.lang.String causa_dev) {
        this.causa_dev = causa_dev;
    }
    
    public java.lang.String getRetencion () {
        return retencion;
    }
    
    public void setRetencion (java.lang.String retencion) {
        this.retencion = retencion;
    }
    
    public java.lang.String getPlaca () {
        return placa;
    }
    
    public void setPlaca (java.lang.String placa) {
        this.placa = placa;
    }
    
    public java.lang.String getNom_conductor () {
        return nom_conductor;
    }
    
    public void setNom_conductor (java.lang.String nom_conductor) {
        this.nom_conductor = nom_conductor;
    }
    
    public java.lang.String getNom_documento () {
        return nom_documento;
    }
    
    public void setNom_documento (java.lang.String nom_documento) {
        this.nom_documento = nom_documento;
    }
    
    public java.lang.String getFecha_cierre () {
        return fecha_cierre;
    }
    
    public void setFecha_cierre (java.lang.String fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }
    
    public java.lang.String getFecha_migracion () {
        return fecha_migracion;
    }
    
    public void setFecha_migracion (java.lang.String fecha_migracion) {
        this.fecha_migracion = fecha_migracion;
    }
    
    public java.lang.String getUsuario_cierre () {
        return usuario_cierre;
    }
    
    public void setUsuario_cierre (java.lang.String usuario_cierre) {
        this.usuario_cierre = usuario_cierre;
    }
    
    public java.lang.String getUsuario_migracion () {
        return usuario_migracion;
    }
    
    public void setUsuario_migracion (java.lang.String usuario_migracion) {
        this.usuario_migracion = usuario_migracion;
    }
    
    public java.lang.String getNota_contable () {
        return nota_contable;
    }
    
    public void setNota_contable (java.lang.String nota_contable) {
        this.nota_contable = nota_contable;
    }

    public java.lang.String getDocumento_rel () {
        return documento_rel;
    }

    public void setDocumento_rel (java.lang.String documento_rel) {
        this.documento_rel = documento_rel;
    }

    public java.lang.String getTipo_doc_rel () {
        return tipo_doc_rel;
    }

    public void setTipo_doc_rel (java.lang.String tipo_doc_rel) {
        this.tipo_doc_rel = tipo_doc_rel;
    }
    
    /**
     * Getter for property cod_Cliente.
     * @return Value of property cod_Cliente.
     */
    public java.lang.String getCod_Cliente() {
        return cod_Cliente;
    }
    
    /**
     * Setter for property cod_Cliente.
     * @param cod_Cliente New value of property cod_Cliente.
     */
    public void setCod_Cliente(java.lang.String cod_Cliente) {
        this.cod_Cliente = cod_Cliente;
    }
    
    /**
     * Getter for property cantidad_nueva.
     * @return Value of property cantidad_nueva.
     */
    public double getCantidad_nueva() {
        return cantidad_nueva;
    }
    
    /**
     * Setter for property cantidad_nueva.
     * @param cantidad_nueva New value of property cantidad_nueva.
     */
    public void setCantidad_nueva(double cantidad_nueva) {
        this.cantidad_nueva = cantidad_nueva;
    }
    
    /**
     * Getter for property nuevo_numpla.
     * @return Value of property nuevo_numpla.
     */
    public java.lang.String getNuevo_numpla() {
        return nuevo_numpla;
    }
    
    /**
     * Setter for property nuevo_numpla.
     * @param nuevo_numpla New value of property nuevo_numpla.
     */
    public void setNuevo_numpla(java.lang.String nuevo_numpla) {
        this.nuevo_numpla = nuevo_numpla;
    }
    
    /**
     * Getter for property codubicacion.
     * @return Value of property codubicacion.
     */
    public java.lang.String getCodubicacion () {
        return codubicacion;
    }
    
    /**
     * Setter for property codubicacion.
     * @param codubicacion New value of property codubicacion.
     */
    public void setCodubicacion (java.lang.String codubicacion) {
        this.codubicacion = codubicacion;
    }
    
    /**
     * Getter for property nota_debito.
     * @return Value of property nota_debito.
     */
    public java.lang.String getNota_debito () {
        return nota_debito;
    }
    
    /**
     * Setter for property nota_debito.
     * @param nota_debito New value of property nota_debito.
     */
    public void setNota_debito (java.lang.String nota_debito) {
        this.nota_debito = nota_debito;
    }
    
    /**
     * Getter for property nota_credito.
     * @return Value of property nota_credito.
     */
    public java.lang.String getNota_credito () {
        return nota_credito;
    }
    
    /**
     * Setter for property nota_credito.
     * @param nota_credito New value of property nota_credito.
     */
    public void setNota_credito (java.lang.String nota_credito) {
        this.nota_credito = nota_credito;
    }
    
    /**
     * Getter for property usuario_nota_debito.
     * @return Value of property usuario_nota_debito.
     */
    public java.lang.String getUsuario_nota_debito () {
        return usuario_nota_debito;
    }
    
    /**
     * Setter for property usuario_nota_debito.
     * @param usuario_nota_debito New value of property usuario_nota_debito.
     */
    public void setUsuario_nota_debito (java.lang.String usuario_nota_debito) {
        this.usuario_nota_debito = usuario_nota_debito;
    }
    
    /**
     * Getter for property usuario_nota_credito.
     * @return Value of property usuario_nota_credito.
     */
    public java.lang.String getUsuario_nota_credito () {
        return usuario_nota_credito;
    }
    
    /**
     * Setter for property usuario_nota_credito.
     * @param usuario_nota_credito New value of property usuario_nota_credito.
     */
    public void setUsuario_nota_credito (java.lang.String usuario_nota_credito) {
        this.usuario_nota_credito = usuario_nota_credito;
    }
    
    /**
     * Getter for property fecha_nota_debito.
     * @return Value of property fecha_nota_debito.
     */
    public java.lang.String getFecha_nota_debito () {
        return fecha_nota_debito;
    }
    
    /**
     * Setter for property fecha_nota_debito.
     * @param fecha_nota_debito New value of property fecha_nota_debito.
     */
    public void setFecha_nota_debito (java.lang.String fecha_nota_debito) {
        this.fecha_nota_debito = fecha_nota_debito;
    }
    
    /**
     * Getter for property fecha_nota_credito.
     * @return Value of property fecha_nota_credito.
     */
    public java.lang.String getFecha_nota_credito () {
        return fecha_nota_credito;
    }
    
    /**
     * Setter for property fecha_nota_credito.
     * @param fecha_nota_credito New value of property fecha_nota_credito.
     */
    public void setFecha_nota_credito (java.lang.String fecha_nota_credito) {
        this.fecha_nota_credito = fecha_nota_credito;
    }
    
    /**
     * Getter for property observacion_cierre.
     * @return Value of property observacion_cierre.
     */
    public java.lang.String getObservacion_cierre () {
        return observacion_cierre;
    }
    
    /**
     * Setter for property observacion_cierre.
     * @param observacion_cierre New value of property observacion_cierre.
     */
    public void setObservacion_cierre (java.lang.String observacion_cierre) {
        this.observacion_cierre = observacion_cierre;
    }
    
    /**
     * Getter for property grupo.
     * @return Value of property grupo.
     */
    public java.lang.String getGrupo() {
        return grupo;
    }
    
    /**
     * Setter for property grupo.
     * @param grupo New value of property grupo.
     */
    public void setGrupo(java.lang.String grupo) {
        this.grupo = grupo;
    }
    
    /**
     * Getter for property nom_destinatario.
     * @return Value of property nom_destinatario.
     */
    public java.lang.String getNom_destinatario () {
        return nom_destinatario;
    }
    
    /**
     * Setter for property nom_destinatario.
     * @param nom_destinatario New value of property nom_destinatario.
     */
    public void setNom_destinatario (java.lang.String nom_destinatario) {
        this.nom_destinatario = nom_destinatario;
    }
    
}
