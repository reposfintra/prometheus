/*
 * RemDest.java
 *
 * Created on 7 de diciembre de 2004, 03:45 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
import java.io.Serializable;
import java.io.*;
import java.sql.*;
import java.util.*;

public class RemiDest implements Serializable{
    
    private String codigo;
    private String nombre;
    private String tipo; 
    private String codcli;
    //Henry
    private String direccion;
    private String ciudad;    
    private String creation_date;
    private String cration_user;
    private String user_update;
    private String last_update;
    private String base;
    private String dstrct;
    private String estado;
    private String nomcli;
    private String reg_status;
    //Osvaldo
    private String referencia;
    
    //Ivan Dario Gomez 30 Oct 2006
    private String nombre_contacto;
    private String telefono;
    
    /** Creates a new instance of Class */
    public static RemiDest load(ResultSet rs)throws SQLException {
        RemiDest rem = new RemiDest ();
        rem.setCodcli(rs.getString("codcli"));
        rem.setCodigo(rs.getString("codigo"));
        rem.setNombre(rs.getString("nombre"));
        rem.setTipo(rs.getString("tipo"));
        return rem;
    }
    
    public String getCodigo(){
        return codigo;
    }
    public void setCodigo(String codigo){
        this.codigo=codigo;
    }
    public String getNombre(){
        return nombre;
    }
     public void setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getTipo(){
        return tipo;
    } 
    public void setTipo(String tipo){
        this.tipo=tipo;
    } 
    public  String getCodcli(){
        return codcli;
    }
    public  void setCodcli(String codcli){
        this.codcli=codcli;
    }
    
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property cration_user.
     * @return Value of property cration_user.
     */
    public java.lang.String getCration_user() {
        return cration_user;
    }
    
    /**
     * Setter for property cration_user.
     * @param cration_user New value of property cration_user.
     */
    public void setCration_user(java.lang.String cration_user) {
        this.cration_user = cration_user;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property nomcli.
     * @return Value of property nomcli.
     */
    public java.lang.String getNomcli() {
        return nomcli;
    }
    
    /**
     * Setter for property nomcli.
     * @param nomcli New value of property nomcli.
     */
    public void setNomcli(java.lang.String nomcli) {
        this.nomcli = nomcli;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property referencia.
     * @return Value of property referencia.
     */
    public java.lang.String getReferencia() {
        return referencia;
    }
    
    /**
     * Setter for property referencia.
     * @param referencia New value of property referencia.
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }
    
    /**
     * Getter for property nombre_contacto.
     * @return Value of property nombre_contacto.
     */
    public java.lang.String getNombre_contacto() {
        return nombre_contacto;
    }
    
    /**
     * Setter for property nombre_contacto.
     * @param nombre_contacto New value of property nombre_contacto.
     */
    public void setNombre_contacto(java.lang.String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }
    
    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public java.lang.String getTelefono() {
        return telefono;
    }
    
    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }
    
}
