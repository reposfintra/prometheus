/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class DetallePrefactura {

    private  String id_contratista;
    private  String id_orden;
    private  String id_accion;
    private  String consecutivo;
    private  String acciones;
    private  String prefactura;
    private  String fecha_prefactura;
    private  double vlr_mat;
    private  double vlr_mob;
    private  double vlr_otr;
    private  double total_prev1;
    private  double bonificacion;
    private  double vlr_iva;
    private  double vlr_rmat;
    private  double vlr_rmob;
    private  double vlr_rotr;
    private  double vlr_retencion;
    private  double valor_factoring;
    private  String codigo_iva;
    private  String codigo_rmat;
    private  String codigo_rmob;
    private  String codigo_rotr;
    private  double porcentaje_iva;
    private  double porcentaje_rmat;
    private  double porcentaje_rmob;
    private  double porcentaje_rotr;
    private  String simbolo_variable;
    private  String factura_conformada;
    private  String id_cliente;
    private  int cuotas_reales;
    private  String tipo_dtf;
    private  String nombre_cliente;
    private  String direccion;
    private  String ciudad ;
    private  double porcentaje_factoring;
    private  double porcentaje_formula;
    private  double valor_formula;
    private String num_os;
    private double vlr_admnistracion;
    private double vlr_imprevisto;
    private double vlr_utilidad;
    private double vlr_base_iva;

    private String tipo_identificacion;
    private  double valor_formula_provintegral;
    private String f_facturado_cliente;

    public static DetallePrefactura load(java.sql.ResultSet rs)throws java.sql.SQLException{

        DetallePrefactura detallePrefactura = new DetallePrefactura();

        detallePrefactura.setId_contratista( rs.getString("id_contratista") );
        detallePrefactura.setId_orden(rs.getString("id_orden") );
        detallePrefactura.setId_accion( rs.getString("id_accion") );
        detallePrefactura.setConsecutivo( rs.getString("consecutivo") );
        detallePrefactura.setAcciones( rs.getString("acciones") );
        detallePrefactura.setPrefactura( rs.getString("prefactura") );
        detallePrefactura.setFecha_prefactura( rs.getString("fecha_prefactura") );
        detallePrefactura.setVlr_mat(rs.getDouble("vlr_mat"));
        detallePrefactura.setVlr_mob(rs.getDouble("vlr_mob"));
        detallePrefactura.setVlr_otr(rs.getDouble("vlr_otr"));
        detallePrefactura.setTotal_prev1( rs.getDouble("total_prev1") );
        detallePrefactura.setBonificacion(rs.getDouble("bonificacion"));
        detallePrefactura.setVlr_iva(rs.getDouble("vlr_iva"));
        detallePrefactura.setVlr_rmat(rs.getDouble("vlr_rmat"));
        detallePrefactura.setVlr_rmob(rs.getDouble("vlr_rmob"));
        detallePrefactura.setVlr_rotr(rs.getDouble("vlr_rotr"));
        detallePrefactura.setVlr_retencion(rs.getDouble("vlr_retencion"));
        detallePrefactura.setValor_factoring(rs.getDouble("vlr_factoring"));
        detallePrefactura.setCodigo_iva(rs.getString("codigo_iva"));
        detallePrefactura.setCodigo_rmat(rs.getString("codigo_rmat"));
        detallePrefactura.setCodigo_rmob(rs.getString("codigo_rmob"));
        detallePrefactura.setCodigo_rotr(rs.getString("codigo_rotr"));
        detallePrefactura.setPorcentaje_iva(rs.getDouble("porcentaje_iva"));
        detallePrefactura.setPorcentaje_rmat(rs.getDouble("porcentaje_rmat"));
        detallePrefactura.setPorcentaje_rmob(rs.getDouble("porcentaje_rmob"));
        detallePrefactura.setPorcentaje_rotr(rs.getDouble("porcentaje_rotr"));
        detallePrefactura.setSimbolo_variable(rs.getString("simbolo_variable"));
        detallePrefactura.setFactura_conformada(rs.getString("factura_conformada"));
        detallePrefactura.setId_cliente( rs.getString("id_cliente") );
        detallePrefactura.setCuotas_reales(rs.getInt("cuotas_reales"));
        detallePrefactura.setTipo_dtf(rs.getString("tipo_dtf"));
        detallePrefactura.setNombre_cliente( rs.getString("nombre_cliente") );
        detallePrefactura.setDireccion( rs.getString("direccion") );
        detallePrefactura.setCiudad( rs.getString("ciudad") );
        detallePrefactura.setPorcentaje_factoring(rs.getString("porcentaje_factoring"));
        detallePrefactura.setPorcentaje_formula(rs.getDouble("porcentaje_formula"));
        detallePrefactura.setValor_formula(rs.getDouble("vlr_formula"));
        detallePrefactura.setNumOs(rs.getString("numos"));
        detallePrefactura.setVlr_admnistracion(rs.getDouble("vlr_administracion"));
        detallePrefactura.setVlr_imprevisto(rs.getDouble("vlr_imprevisto"));
        detallePrefactura.setVlr_utilidad(rs.getDouble("vlr_utilidad"));
        detallePrefactura.setVlr_base_iva(rs.getDouble("vlr_base_iva"));
        detallePrefactura.setTipoIdentificacion(rs.getString("tipo_identificacion"));
        
        detallePrefactura.setValor_formula_provintegral(rs.getDouble("vlr_formula_provintegral"));
        
        detallePrefactura.setF_facturado_cliente(rs.getString("f_facturado_cliente"));
        
        return detallePrefactura;
    }


    /** Creates a new instance of Prefactura */
    public DetallePrefactura() {
    }

    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the id_orden
     */
    public String getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(String id_orden) {
        this.id_orden = id_orden;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the consecutivo
     */
    public String getConsecutivo() {
        return consecutivo;
    }

    /**
     * @param consecutivo the consecutivo to set
     */
    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    /**
     * @return the acciones
     */
    public String getAcciones() {
        return acciones;
    }

    /**
     * @param acciones the acciones to set
     */
    public void setAcciones(String acciones) {
        this.acciones = acciones;
    }

    /**
     * @return the prefactura
     */
    public String getPrefactura() {
        return prefactura;
    }

    /**
     * @param prefactura the prefactura to set
     */
    public void setPrefactura(String prefactura) {
        this.prefactura = prefactura;
    }

    /**
     * @return the fecha_prefactura
     */
    public String getFecha_prefactura() {
        return fecha_prefactura;
    }

    /**
     * @param fecha_prefactura the fecha_prefactura to set
     */
    public void setFecha_prefactura(String fecha_prefactura) {
        this.fecha_prefactura = fecha_prefactura;
    }

    /**
     * @return the vlr_mat
     */
    public double getVlr_mat() {
        return vlr_mat;
    }

    /**
     * @param vlr_mat the vlr_mat to set
     */
    public void setVlr_mat(double vlr_mat) {
        this.vlr_mat = vlr_mat;
    }

    /**
     * @return the vlr_mob
     */
    public double getVlr_mob() {
        return vlr_mob;
    }

    /**
     * @param vlr_mob the vlr_mob to set
     */
    public void setVlr_mob(double vlr_mob) {
        this.vlr_mob = vlr_mob;
    }

    /**
     * @return the vlr_otr
     */
    public double getVlr_otr() {
        return vlr_otr;
    }

    /**
     * @param vlr_otr the vlr_otr to set
     */
    public void setVlr_otr(double vlr_otr) {
        this.vlr_otr = vlr_otr;
    }

    /**
     * @return the total_prev1
     */
    public double getTotal_prev1() {
        return total_prev1;
    }

    /**
     * @param total_prev1 the total_prev1 to set
     */
    public void setTotal_prev1(double total_prev1) {
        this.total_prev1 = total_prev1;
    }

    /**
     * @return the bonificacion
     */
    public double getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the vlr_iva
     */
    public double getVlr_iva() {
        return vlr_iva;
    }

    /**
     * @param vlr_iva the vlr_iva to set
     */
    public void setVlr_iva(double vlr_iva) {
        this.vlr_iva = vlr_iva;
    }

    /**
     * @return the vlr_rmat
     */
    public double getVlr_rmat() {
        return vlr_rmat;
    }

    /**
     * @param vlr_rmat the vlr_rmat to set
     */
    public void setVlr_rmat(double vlr_rmat) {
        this.vlr_rmat = vlr_rmat;
    }

    /**
     * @return the vlr_rmob
     */
    public double getVlr_rmob() {
        return vlr_rmob;
    }

    /**
     * @param vlr_rmob the vlr_rmob to set
     */
    public void setVlr_rmob(double vlr_rmob) {
        this.vlr_rmob = vlr_rmob;
    }

    /**
     * @return the vlr_rotr
     */
    public double getVlr_rotr() {
        return vlr_rotr;
    }

    /**
     * @param vlr_rotr the vlr_rotr to set
     */
    public void setVlr_rotr(double vlr_rotr) {
        this.vlr_rotr = vlr_rotr;
    }

    /**
     * @return the vlr_retencion
     */
    public double getVlr_retencion() {
        return vlr_retencion;
    }

    /**
     * @param vlr_retencion the vlr_retencion to set
     */
    public void setVlr_retencion(double vlr_retencion) {
        this.vlr_retencion = vlr_retencion;
    }

    /**
     * @return the valor_factoring
     */
    public double getValor_factoring() {
        return valor_factoring;
    }

    /**
     * @param valor_factoring the valor_factoring to set
     */
    public void setValor_factoring(double valor_factoring) {
        this.valor_factoring = valor_factoring;
    }

    /**
     * @return the codigo_iva
     */
    public String getCodigo_iva() {
        return codigo_iva;
    }

    /**
     * @param codigo_iva the codigo_iva to set
     */
    public void setCodigo_iva(String codigo_iva) {
        this.codigo_iva = codigo_iva;
    }

    /**
     * @return the codigo_rmat
     */
    public String getCodigo_rmat() {
        return codigo_rmat;
    }

    /**
     * @param codigo_rmat the codigo_rmat to set
     */
    public void setCodigo_rmat(String codigo_rmat) {
        this.codigo_rmat = codigo_rmat;
    }

    /**
     * @return the codigo_rmob
     */
    public String getCodigo_rmob() {
        return codigo_rmob;
    }

    /**
     * @param codigo_rmob the codigo_rmob to set
     */
    public void setCodigo_rmob(String codigo_rmob) {
        this.codigo_rmob = codigo_rmob;
    }

    /**
     * @return the codigo_rotr
     */
    public String getCodigo_rotr() {
        return codigo_rotr;
    }

    /**
     * @param codigo_rotr the codigo_rotr to set
     */
    public void setCodigo_rotr(String codigo_rotr) {
        this.codigo_rotr = codigo_rotr;
    }

    /**
     * @return the porcentaje_iva
     */
    public double getPorcentaje_iva() {
        return porcentaje_iva;
    }

    /**
     * @param porcentaje_iva the porcentaje_iva to set
     */
    public void setPorcentaje_iva(double porcentaje_iva) {
        this.porcentaje_iva = porcentaje_iva;
    }

    /**
     * @return the porcentaje_rmat
     */
    public double getPorcentaje_rmat() {
        return porcentaje_rmat;
    }

    /**
     * @param porcentaje_rmat the porcentaje_rmat to set
     */
    public void setPorcentaje_rmat(double porcentaje_rmat) {
        this.porcentaje_rmat = porcentaje_rmat;
    }

    /**
     * @return the porcentaje_rmob
     */
    public double getPorcentaje_rmob() {
        return porcentaje_rmob;
    }

    /**
     * @param porcentaje_rmob the porcentaje_rmob to set
     */
    public void setPorcentaje_rmob(double porcentaje_rmob) {
        this.porcentaje_rmob = porcentaje_rmob;
    }

    /**
     * @return the porcentaje_rotr
     */
    public double getPorcentaje_rotr() {
        return porcentaje_rotr;
    }

    /**
     * @param porcentaje_rotr the porcentaje_rotr to set
     */
    public void setPorcentaje_rotr(double porcentaje_rotr) {
        this.porcentaje_rotr = porcentaje_rotr;
    }

    /**
     * @return the simbolo_variable
     */
    public String getSimbolo_variable() {
        return simbolo_variable;
    }

    /**
     * @param simbolo_variable the simbolo_variable to set
     */
    public void setSimbolo_variable(String simbolo_variable) {
        this.simbolo_variable = simbolo_variable;
    }

    /**
     * @return the factura_conformada
     */
    public String getFactura_conformada() {
        return factura_conformada;
    }

    /**
     * @param factura_conformada the factura_conformada to set
     */
    public void setFactura_conformada(String factura_conformada) {
        this.factura_conformada = factura_conformada;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the cuotas_reales
     */
    public int getCuotas_reales() {
        return cuotas_reales;
    }

    /**
     * @param cuotas_reales the cuotas_reales to set
     */
    public void setCuotas_reales(int cuotas_reales) {
        this.cuotas_reales = cuotas_reales;
    }

    /**
     * @return the tipo_dtf
     */
    public String getTipo_dtf() {
        return tipo_dtf;
    }

    /**
     * @param tipo_dtf the tipo_dtf to set
     */
    public void setTipo_dtf(String tipo_dtf) {
        this.tipo_dtf = tipo_dtf;
    }

    /**
     * @return the nombre_cliente
     */
    public String getNombre_cliente() {
        return nombre_cliente;
    }

    /**
     * @param nombre_cliente the nombre_cliente to set
     */
    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the porcentaje_factoring
     */
    public double getPorcentaje_factoring() {
        return porcentaje_factoring;
    }

    /**
     * @param porcentaje_factoring the porcentaje_factoring to set
     */
    public void setPorcentaje_factoring(String porcentaje_factoring) {
        double numero=Double.valueOf(porcentaje_factoring).doubleValue();
        this.porcentaje_factoring = numero;
    }

    /**
     * @return the porcentaje_formula
     */
    public double getPorcentaje_formula() {
        return porcentaje_formula;
    }

    /**
     * @param porcentaje_formula the porcentaje_formula to set
     */
    public void setPorcentaje_formula(double porcentaje_formula) {
        this.porcentaje_formula = porcentaje_formula;
    }

    /**
     * @return the valor_formula
     */
    public double getValor_formula() {
        return valor_formula;
    }

    /**
     * @param valor_formula the valor_formula to set
     */
    public void setValor_formula(double valor_formula) {
        this.valor_formula = valor_formula;
    }


    public String getNumOs() {
        return num_os;
    }

    public void setNumOs(String num_os1) {
        this.num_os= num_os1;
    }

    /**
     * @return the vlr_admnistracion
     */
    public double getVlr_admnistracion() {
        return vlr_admnistracion;
    }

    /**
     * @param vlr_admnistracion the vlr_admnistracion to set
     */
    public void setVlr_admnistracion(double vlr_admnistracion) {
        this.vlr_admnistracion = vlr_admnistracion;
    }

    /**
     * @return the vlr_imprevisto
     */
    public double getVlr_imprevisto() {
        return vlr_imprevisto;
    }

    /**
     * @param vlr_imprevisto the vlr_imprevisto to set
     */
    public void setVlr_imprevisto(double vlr_imprevisto) {
        this.vlr_imprevisto = vlr_imprevisto;
    }

    /**
     * @return the vlr_utilidad
     */
    public double getVlr_utilidad() {
        return vlr_utilidad;
    }

    /**
     * @param vlr_utilidad the vlr_utilidad to set
     */
    public void setVlr_utilidad(double vlr_utilidad) {
        this.vlr_utilidad = vlr_utilidad;
    }

    /**
     * @return the vlr_base_iva
     */
    public double getVlr_base_iva() {
        return vlr_base_iva;
    }

    /**
     * @param vlr_base_iva the vlr_base_iva to set
     */
    public void setVlr_base_iva(double vlr_base_iva) {
        this.vlr_base_iva = vlr_base_iva;
    }

    public String getTipoIdentificacion() {
        return tipo_identificacion;
    }
    
    public void setTipoIdentificacion(String x1) {
        tipo_identificacion=x1;
    }
    
    
    public double getValor_formula_provintegral() {
        return valor_formula_provintegral;
    }    
    public void setValor_formula_provintegral(double x1) {
        valor_formula_provintegral=x1;
    }
    
    
    public String getF_facturado_cliente() {
        return f_facturado_cliente;
    }    
    public void setF_facturado_cliente(String x1) {
        f_facturado_cliente=x1;
    }
}
