/*Created on 28 de junio de 2005, 02:37 PM*/

package com.tsp.operation.model.beans;

/**@author  fvillacob*/

import java.util.*;
import com.tsp.util.Utility;


public class OTs {
    
    private int    id;
    private String ot;
    private String desc;
    private String origen;
    private String destino;
    private String cliente;
    private String clienteName;
    private String tipo;
    private String fechaDespacho;
    private String agencia;
    private double diasViaje;
    private String ultimoReporte;
    private double valor;
    private String padre;
    private String sj;
    private String fechaentrega;
    private String facturaComercial;
    
    private List viajes;
    
    public OTs() {
        id               = 0;
        ot               = " ";
        desc             = " "; 
        origen           = " "; 
        destino          = " ";
        cliente          = " "; 
        clienteName      = " ";
        tipo             = " ";
        fechaDespacho    = " ";
        agencia          = " ";
        diasViaje        = 0; 
        ultimoReporte    = " ";
        viajes           = null;
        valor            = 0;
        padre            = " ";
        sj               = " ";
        facturaComercial = " ";
    }
    
    
    
    // SETS:
    
    public void setID            ( int no       ) { this.id               = no;     }
    public void setOT            ( String no    ) { this.ot               = no;     }
    public void setDesc          ( String des   ) { this.desc             = des;    }
    public void setOrigen        ( String ori   ) { this.origen           = ori;    }
    public void setDestino       ( String des   ) { this.destino          = des;    }
    public void setClienteId     ( String id    ) { this.cliente          = id;     }
    public void setClienteName   ( String name  ) { this.clienteName      = name;   }
    public void setTipoViaje     ( String type  ) { this.tipo             = type;   }
    public void setDespacho      ( String fecha ) { this.fechaDespacho    = fecha;  }
    public void setAgencia       ( String agency) { this.agencia          = agency; }
    public void setDias          ( double total ) { this.diasViaje        = total;  }
    public void setUltimoReporte ( String report) { this.ultimoReporte    = report; }
    public void setViajes        ( List ocs     ) { this.viajes           = ocs;    }    
    public void setValor         ( double val   ) { this.valor            = val;    }
    public void setPadre         ( String father) { this.padre            = father; }
    public void setSJ            ( String std   ) { this.sj               = std;    }    
    public void setFactComercial ( String fact  ) { this.facturaComercial = fact;   }
    
    
    
    
    // GETS:
    
    public int    getID            ( ) { return this.id            ; }
    public String getOT            ( ) { return this.ot            ; }
    public String getDesc          ( ) { return this.desc          ; }
    public String getOrigen        ( ) { return this.origen        ; }
    public String getDestino       ( ) { return this.destino       ; }
    public String getClienteId     ( ) { return this.cliente       ; }
    public String getClienteName   ( ) { return this.clienteName   ; }
    public String getTipoViaje     ( ) { return this.tipo          ; }
    public String getDespacho      ( ) { return this.fechaDespacho ; }
    public String getAgencia       ( ) { return this.agencia       ; }
    public double getDias          ( ) { return this.diasViaje     ; }
    public String getUltimoReporte ( ) { return this.ultimoReporte ; }
    public List   getViajes        ( ) { return this.viajes        ; }
    public double getValor         ( ) { return this.valor         ; }
    public String getPadre         ( ) { return this.padre         ; }
    public String getSJ            ( ) { return this.sj            ; }
    public String getFactComercial ( ) { return this.facturaComercial; }
    
    
    
    
    
    public void diferenciaFecha(String cadena) throws Exception{
      try{  
         double cantDias = 0;
         
          String[] vector = cadena.split(" ");
          String dias   = "0";
          String resto  = "00:00:00";
          String HH     = "0";
          String MM     = "0";
          String SS     = "0";
          
          if(vector.length>1){
             for(int i=0;i<=vector.length-1;i++){
               if(i==0) dias  = vector[i];
               if(i==2) resto = vector[i];
             }
          }
          else{
              dias  = "0";
              resto = cadena;
          }
         
          
          try{            
            double dia  = Double.parseDouble(dias);
            cantDias    = dia;
          }catch(Exception e){}
          
          try{
              String[] vec = resto.split(":");
              HH =  vec[0];
              MM =  vec[1];
              SS =  vec[2].substring(0,2);             
              double hora = Double.parseDouble(HH)/24;
              double min  = (Double.parseDouble(MM)/60)/24;
              double seg  = ((Double.parseDouble(SS)/60)/60)/24;
              cantDias   +=  hora + min + seg;
          }catch(Exception e){}
         
          try{
             String punto    = String.valueOf(cantDias);
             int a           = punto.indexOf(".");
             if (a >-1) {
               String numero  = punto.substring(0, a);
               String decimal = punto.substring(a+1, a+3);
               String conv    = numero +"."+  decimal;
               cantDias       = Double.parseDouble(conv);
             }             
          }catch(Exception e){}
          
         this.diasViaje = cantDias;
      }
      catch(Exception e){ throw new Exception( " Convirtiendo "+ cadena + " ->"+ e.getMessage());}
    }

    /**
     * Getter for property fechaentrega.
     * @return Value of property fechaentrega.
     */
    public java.lang.String getFechaentrega() {
        return fechaentrega;
    }    
    
    /**
     * Setter for property fechaentrega.
     * @param fechaentrega New value of property fechaentrega.
     */
    public void setFechaentrega(java.lang.String fechaentrega) {
        this.fechaentrega = fechaentrega;
    }
    
}
