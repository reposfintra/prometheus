/*
 * Tiempo.java
 *
 * Created on 26 de enero de 2005, 03:04 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
public class Tiempo {
    
    private String dstrct;
    private String sj;
    private String cf_code;
    private String time_code_1;
    private String time_code_2;
    private String time_codeUpdate1;
    private String time_codeUpdate2;
    private String secuence;
    private String creation_date;
    private String creation_user;
    private String reporte;
    /** Creates a new instance of Tiempo */
    public Tiempo() {
    }
    
    public String getreporte(){
        return this.reporte;
    }
    public String getdstrct(){
        return this.dstrct;
    }
    public String getsj(){
        return this.sj;
    }
    public String getcf_code(){
        return this.cf_code;
    }
    public String gettime_code_1(){
        return this.time_code_1;
    }
    public String gettime_code_2(){
        return this.time_code_2;
    }
    public String gettime_codeUpdate1(){
        return this.time_codeUpdate1;
    }
    public String gettime_codeUpdate2(){
        return this.time_codeUpdate2;
    }
    public String getsecuence(){
        return this.secuence;
    }
    public String getcreation_date(){
        return this.creation_date;
    }
    public String getcreation_user(){
        return this.creation_user;
    }
    public void setreporte(String reporte){
        this.reporte=reporte;
    }
    public void setdstrct(String dstrct){
        this.dstrct=dstrct;
    }
    public void setsj(String sj){
        this.sj=sj;
    }
    public void setcf_code(String cf_code){
        this.cf_code=cf_code;
    }
    public void settime_code_1(String time_code_1){
        this.time_code_1=time_code_1;
    }
    public void  settime_code_2(String time_code_2){
        this.time_code_2=time_code_2;
    }
    public void settime_codeUpdate1(String time_code_1){
        this.time_codeUpdate1=time_code_1;
    }
    public void  settime_Update2(String time_code_2){
        this.time_codeUpdate2=time_code_2;
    }
    public void setsecuence(String secuence){
        this.secuence=secuence;
    }
    public void setcreation_date(String creation_date){
        this.creation_date=creation_date;
    }
    public void setcreation_user(String creation_user){
        this.creation_user=creation_user;
    }
    
    
    
}
