/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author Harold Cuello G.
 */
public class SeguimientoCarteraBeans {

    public SeguimientoCarteraBeans() {
    }
    
    private int id;
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
   
    //-------------------------------------------------------------
    private String vencimiento_mayor;

    public void setVencimientoMayor(String vencimiento_mayor) {
        this.vencimiento_mayor = vencimiento_mayor;
    }

    public String getVencimientoMayor() {
        return vencimiento_mayor;
    }
    
    //-------------------------------------------------------------
    private String valor_asignado;

    public void setValorAsignado(String valor_asignado) {
        this.valor_asignado = valor_asignado;
    }

    public String getValorAsignado() {
        return valor_asignado;
    }
    
    //-------------------------------------------------------------
    private String perc_valor_asignado;

    public void setPercValorAsignado(String perc_valor_asignado) {
        this.perc_valor_asignado = perc_valor_asignado;
    }

    public String getPercValorAsignado() {
        return perc_valor_asignado;
    }
    
     //-------------------------------------------------------------
    private String cantidad_asignada;

    public void setCantidadAsignada(String cantidad_asignada) {
        this.cantidad_asignada = cantidad_asignada;
    }

    public String getCantidadAsignada() {
        return cantidad_asignada;
    }   

    //-------------------------------------------------------------
    private String perc_cantidad_asignada;

    public void setPercCantAsignada(String perc_cantidad_asignada) {
        this.perc_cantidad_asignada = perc_cantidad_asignada;
    }

    public String getPercCantAsignada() {
        return perc_cantidad_asignada;
    }
    
    
    

    //-------------------------------------------------------------
    private String tramo_anterior;

    public void setTramoAnterior(String tramo_anterior) {
        this.tramo_anterior = tramo_anterior;
    }

    public String getTramoAnterior() {
        return tramo_anterior;
    }
    
     //-------------------------------------------------------------
    private String sumatramo_anterior;

    public void setSumaTramoAnterior(String sumatramo_anterior) {
        this.sumatramo_anterior = sumatramo_anterior;
    }

    public String getSumaTramoAnterior() {
        return sumatramo_anterior;
    }   

    //-------------------------------------------------------------
    private String canttramo_anterior;

    public void setCantTramoAnterior(String canttramo_anterior) {
        this.canttramo_anterior = canttramo_anterior;
    }

    public String getCantTramoAnterior() {
        return canttramo_anterior;
    }
    

    
    
    //-------------------------------------------------------------
    private String status;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
    
    //-------------------------------------------------------------
    private String valor_saldo;

    public void setValorSaldo(String valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    public String getValorSaldo() {
        return valor_saldo;
    }
   
    //----------------------------------------
    private String valor_a_pagar;

    public void setValoraPagar(String valor_a_pagar) {
        this.valor_a_pagar = valor_a_pagar;
    }

    public String getValoraPagar() {
        return valor_a_pagar;
    }
    
    
    
    
    
    //-------------------------------------------------------------
    private String debido_cobrar;

    public void setDebidoCobrar(String debido_cobrar) {
        this.debido_cobrar = debido_cobrar;
    }

    public String getDebidoCobrar() {
        return debido_cobrar;
    }
    
    //-------------------------------------------------------------
    private String recaudoxcuota;

    public void setRecaudoxCuota(String recaudoxcuota) {
        this.recaudoxcuota = recaudoxcuota;
    }

    public String getRecaudoxCuota() {
        return recaudoxcuota;
    }
    
    //-------------------------------------------------------------
    private String cumplimiento;

    public void setCumplimiento(String cumplimiento) {
        this.cumplimiento = cumplimiento;
    }

    public String getCumplimiento() {
        return cumplimiento;
    }
    
    
    
    //-------------------------------------------------------------
    private String cedula;

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCedula() {
        return cedula;
    }
    
    //-------------------------------------------------------------
    private String nombre_cliente;

    public void setNombreCliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getNombreCliente() {
        return nombre_cliente;
    }

    //-------------------------------------------------------------
    private String direccion;

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccion() {
        return direccion;
    }
    
    //-------------------------------------------------------------
    private String ciudad;

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }
    
    //-------------------------------------------------------------
    private String telefono;

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono() {
        return telefono;
    }
    
    //-------------------------------------------------------------
    private String negocio;

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getNegocio() {
        return negocio;
    }
    
    //-------------------------------------------------------------
    private String convenio;
    /**
     * @param convenio the Convenio to set
     */
    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    /**
     * @return the convenio
     */
    public String getConvenio() {
        return convenio;
    }        
    
    //-------------------------------------------------------------
    private String diapago;
    /**
     * @param diapago the diapago to set
     */
    public void setDiaPago(String diapago) {
        this.diapago = diapago;
    }

    /**
     * @return the convenio
     */
    public String getDiaPago() {
        return diapago;
    }     
    
    //-------------------------------------------------------------
    private String telcontacto;

    public void setTelContacto(String telcontacto) {
        this.telcontacto = telcontacto;
    }

    public String getTelContacto() {
        return telcontacto;
    }


    //-------------------------------------------------------------
    private String cuota;

    public void setCuota(String cuota) {
        this.cuota = cuota;
    }

    public String getCuota() {
        return cuota;
    }

    
    //-------------------------------------------------------------
    private String fecha_vencimiento;

    public void setFechaVencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getFechaVencimiento() {
        return fecha_vencimiento;
    }

    //-------------------------------------------------------------
    private String dias_vencidos;

    public void setDiasVencidos(String dias_vencidos) {
        this.dias_vencidos = dias_vencidos;
    }

    public String getDiasVencidos() {
        return dias_vencidos;
    }
    
    //-------------------------------------------------------------
    private String ultimo_pago;

    public void setUltimoPago(String ultimo_pago) {
        this.ultimo_pago = ultimo_pago;
    }

    public String getUltimoPago() {
        return ultimo_pago;
    }    

    
    //-------------------------------------------------------------
    private String documento;

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getDocumento() {
        return documento;
    }    

            
    //-------------------------------------------------------------
    private String valor_factura;

    public void setValorFactura(String valor_factura) {
        this.valor_factura = valor_factura;
    }

    public String getValorFactura() {
        return valor_factura;
    }    
    
    
    //-------------------------------------------------------------
    private String ingreso;

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getIngreso() {
        return ingreso;
    }    

    //-------------------------------------------------------------
    private String branch_code;

    public void setBranchCode(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getBranchCode() {
        return branch_code;
    }    

    //-------------------------------------------------------------
    private String bank_account_no;

    public void setBankAccountNo(String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }

    public String getBankAccountNo() {
        return bank_account_no;
    }    

    //-------------------------------------------------------------
    private String fecha_ingreso;

    public void setFechaIngreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getFechaIngreso() {
        return fecha_ingreso;
    }    

    //-------------------------------------------------------------
    private String fecha_consignacion;

    public void setFechaConsignacion(String fecha_consignacion) {
        this.fecha_consignacion = fecha_consignacion;
    }

    public String getFechaConsignacion() {
        return fecha_consignacion;
    }    

    //-------------------------------------------------------------
    private String descripcion_ingreso;

    public void setDescripcionIngreso(String descripcion_ingreso) {
        this.descripcion_ingreso = descripcion_ingreso;
    }

    public String getDescripcionIngreso() {
        return descripcion_ingreso;
    }    

    //-------------------------------------------------------------
    private String valor_ingreso;

    public void setValorIngreso(String valor_ingreso) {
        this.valor_ingreso = valor_ingreso;
    }

    public String getValorIngreso() {
        return valor_ingreso;
    }    


    //-------------------------------------------------------------
    private String MaxRegTramo;

    public void setMaxRegTramo(String MaxRegTramo) {
        this.MaxRegTramo = MaxRegTramo;
    }

    public String getMaxRegTramo() {
        return MaxRegTramo;
    }  
    
    
    //-------------------------------------------------------------
    private String MarcaX;

    public void setMarcaX(String MarcaX) {
        this.MarcaX = MarcaX;
    }

    public String getMarcaX() {
        return MarcaX;
    }      
    
    
    
   //----------------------------------------------------------------
    private String dias_ven_hoy;

    /**
     * @return the dias_ven_hoy
     */
    public String getDias_ven_hoy() {
        return dias_ven_hoy;
    }

    /**
     * @param dias_ven_hoy the dias_ven_hoy to set
     */
    public void setDias_ven_hoy(String dias_ven_hoy) {
        this.dias_ven_hoy = dias_ven_hoy;
    }
    
    private String cuenta;

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
    
    private float interes_mora;
    private float  gasto_cobranza;

    /**
     * @return the interes_mora
     */
    public float getInteres_mora() {
        return interes_mora;
    }

    /**
     * @param interes_mora the interes_mora to set
     */
    public void setInteres_mora(float interes_mora) {
        this.interes_mora = interes_mora;
    }

    /**
     * @return the gasto_cobranza
     */
    public float getGasto_cobranza() {
        return gasto_cobranza;
    }

    /**
     * @param gasto_cobranza the gasto_cobranza to set
     */
    public void setGasto_cobranza(float gasto_cobranza) {
        this.gasto_cobranza = gasto_cobranza;
    }
    
    
    private String agente;

    /**
     * @return the agente
     */
    public String getAgente() {
        return agente;
    }

    /**
     * @param agente the agente to set
     */
    public void setAgente(String agente) {
        this.agente = agente;
    }
    
    private String periodo;

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
    
    private String status_vencimiento;

    /**
     * @return the status_vencimiento
     */
    public String getStatus_vencimiento() {
        return status_vencimiento;
    }

    /**
     * @param status_vencimiento the status_vencimiento to set
     */
    public void setStatus_vencimiento(String status_vencimiento) {
        this.status_vencimiento = status_vencimiento;
    }
    
    private String recaudoxcuotaFiducia;
    private String recaudoxcuotaFenal;

    /**
     * @return the recaudoxcuotaFiducia
     */
    public String getRecaudoxcuotaFiducia() {
        return recaudoxcuotaFiducia;
    }

    /**
     * @param recaudoxcuotaFiducia the recaudoxcuotaFiducia to set
     */
    public void setRecaudoxcuotaFiducia(String recaudoxcuotaFiducia) {
        this.recaudoxcuotaFiducia = recaudoxcuotaFiducia;
    }

    /**
     * @return the recaudoxcuotaFenal
     */
    public String getRecaudoxcuotaFenal() {
        return recaudoxcuotaFenal;
    }

    /**
     * @param recaudoxcuotaFenal the recaudoxcuotaFenal to set
     */
    public void setRecaudoxcuotaFenal(String recaudoxcuotaFenal) {
        this.recaudoxcuotaFenal = recaudoxcuotaFenal;
    }
    
    private String undNegocio;

    /**
     * @return the undNegocio
     */
    public String getUndNegocio() {
        return undNegocio;
    }

    /**
     * @param undNegocio the undNegocio to set
     */
    public void setUndNegocio(String undNegocio) {
        this.undNegocio = undNegocio;
    }
    
    private String periodoFoto;

    /**
     * @return the periodoFoto
     */
    public String getPeriodoFoto() {
        return periodoFoto;
    }

    /**
     * @param periodoFoto the periodoFoto to set
     */
    public void setPeriodoFoto(String periodoFoto) {
        this.periodoFoto = periodoFoto;
    }
    
    private String fecha_ultimo_compromiso;

    public void setFechaUltimoCompromiso(String fecha_ultimo_compromiso) {
        this.fecha_ultimo_compromiso = fecha_ultimo_compromiso;
    }

    public String getFechaUltimoCompromiso() {
        return fecha_ultimo_compromiso;
    }
    
    //-------------------------------------------------------------
    private String barrio;

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getBarrio() {
        return barrio;
    }
    
     //-------------------------------------------------------------
    private String codDpto;

    public void setCodDpto(String codDpto) {
        this.codDpto = codDpto;
    }

    public String getCodDpto() {
        return codDpto;
    }
    
    //-------------------------------------------------------------
    private String codCiudad;

    public void setCodCiudad(String codCiudad) {
        this.codCiudad = codCiudad;
    }

    public String getCodCiudad() {
        return codCiudad;
    }
    
    //-------------------------------------------------------------
    private String reestructuracion;

    public void setReestructuracion(String reestructuracion) {
        this.reestructuracion = reestructuracion;
    }

    public String getReestructuracion() {
        return reestructuracion;
    }
    
    //-------------------------------------------------------------
    private String juridica;

    public void setJuridica(String juridica) {
        this.juridica = juridica;
    }

    public String getJuridica() {
        return juridica;
    }
    
    //-------------------------------------------------------------
    private String pagaduria;
    /**
     * @return the pagaduria
     */
    public String getPagaduria() {
        return pagaduria;
    }

    /**
     * @param pagaduria the pagaduria to set
     */
    public void setPagaduria(String pagaduria) {
        this.pagaduria = pagaduria;
    }
    
    private String refinanciacion;

    public String getRefinanciacion() {
        return refinanciacion;
    }

    public void setRefinanciacion(String refinanciacion) {
        this.refinanciacion = refinanciacion;
    }
    
    
    
    
}