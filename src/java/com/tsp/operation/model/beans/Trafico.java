/*
 * Trafico.java
 *
 * Created on 5 de septiembre de 2005, 10:49 AM
 */


package com.tsp.operation.model.beans;

/**
 *
 * @author  usuario
 */

import java.io.*;
import java.sql.*;
import java.util.*;

public class Trafico implements Serializable{

    private String rec_status="";
    private String dstrct="";
    private String planilla="";
    private String placa="";
    private String cedcon="";
    private String cedprod="";
    private String origen="";
    private String destino="";
    private String zona="";
    private String escolta="";
    private String caravana="";
    private String fecha_despacho="";
    private String pto_control_ultreporte="";
    private String fecha_ult_reporte="";
    private String pto_control_proxreporte="";
    private String fecha_prox_reporte="";
    private String ult_observacion="";
    private String last_update="";
    private String user_update="";
    private String creation_date="";
    private String creation_user="";
    private String base="";
    
   /** Creates a new instance of Trafico */
    public Trafico() {
    }
    
    public static Trafico load(ResultSet rs)throws SQLException {
        Trafico trafico = new Trafico();
        trafico.setRec_status(rs.getString("reg_status"));
        trafico.setDstrct(rs.getString("dstrct"));
        trafico.setPlanilla(rs.getString("planilla"));
        trafico.setPlaca(rs.getString("placa"));
        trafico.setCedcon(rs.getString("cedcon"));
        trafico.setCedprod(rs.getString("cedprop"));
        trafico.setOrigen(rs.getString("origen"));
        trafico.setDestino(rs.getString("destino"));
        trafico.setZona(rs.getString("zona"));
        trafico.setEscolta(rs.getString("escolta"));
        trafico.setCaravana(rs.getString("caravana"));
        trafico.setFecha_despacho(rs.getString("fecha_despacho"));
        trafico.setPto_control_ultreporte(rs.getString("pto_control_ultreporte"));
        trafico.setFecha_ult_reporte(rs.getString("fecha_ult_reporte"));
        trafico.setPto_control_proxreporte(rs.getString("pto_control_proxreporte"));
        trafico.setFecha_prox_reporte(rs.getString("fecha_prox_reporte"));
        trafico.setUlt_observacion(rs.getString("ult_observacion"));
        trafico.setLast_update(rs.getString("last_update"));
        trafico.setUser_update(rs.getString("user_update"));
        trafico.setCreation_date(rs.getString("creation_date"));
        trafico.setCreation_user(rs.getString("creation_user"));
        trafico.setBase(rs.getString("base")); 
        return trafico;
    }
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    
    
    public java.lang.String getBase() {
        return base;
    }    
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }    
    
    /**
     * Getter for property caravana.
     * @return Value of property caravana.
     */
    public java.lang.String getCaravana() {
        return caravana;
    }
    
    /**
     * Setter for property caravana.
     * @param caravana New value of property caravana.
     */
    public void setCaravana(java.lang.String caravana) {
        this.caravana = caravana;
    }
    
    /**
     * Getter for property cedcon.
     * @return Value of property cedcon.
     */
    public java.lang.String getCedcon() {
        return cedcon;
    }
    
    /**
     * Setter for property cedcon.
     * @param cedcon New value of property cedcon.
     */
    public void setCedcon(java.lang.String cedcon) {
        this.cedcon = cedcon;
    }
    
    /**
     * Getter for property cedprod.
     * @return Value of property cedprod.
     */
    public java.lang.String getCedprod() {
        return cedprod;
    }
    
    /**
     * Setter for property cedprod.
     * @param cedprod New value of property cedprod.
     */
    public void setCedprod(java.lang.String cedprod) {
        this.cedprod = cedprod;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property escolta.
     * @return Value of property escolta.
     */
    public java.lang.String getEscolta() {
        return escolta;
    }
    
    /**
     * Setter for property escolta.
     * @param escolta New value of property escolta.
     */
    public void setEscolta(java.lang.String escolta) {
        this.escolta = escolta;
    }
    
    /**
     * Getter for property fecha_despacho.
     * @return Value of property fecha_despacho.
     */
    public java.lang.String getFecha_despacho() {
        return fecha_despacho;
    }
    
    /**
     * Setter for property fecha_despacho.
     * @param fecha_despacho New value of property fecha_despacho.
     */
    public void setFecha_despacho(java.lang.String fecha_despacho) {
        this.fecha_despacho = fecha_despacho;
    }
    
    /**
     * Getter for property fecha_prox_reporte.
     * @return Value of property fecha_prox_reporte.
     */
    public java.lang.String getFecha_prox_reporte() {
        return fecha_prox_reporte;
    }
    
    /**
     * Setter for property fecha_prox_reporte.
     * @param fecha_prox_reporte New value of property fecha_prox_reporte.
     */
    public void setFecha_prox_reporte(java.lang.String fecha_prox_reporte) {
        this.fecha_prox_reporte = fecha_prox_reporte;
    }
    
    /**
     * Getter for property fecha_ultima_reporte.
     * @return Value of property fecha_ultima_reporte.
     */
    public java.lang.String getFecha_ult_reporte() {
        return fecha_ult_reporte;
    }
    
    /**
     * Setter for property fecha_ultima_reporte.
     * @param fecha_ultima_reporte New value of property fecha_ultima_reporte.
     */
    public void setFecha_ult_reporte(java.lang.String fecha_ult_reporte) {
        this.fecha_ult_reporte = fecha_ult_reporte;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property pto_control_proxreporte.
     * @return Value of property pto_control_proxreporte.
     */
    public java.lang.String getPto_control_proxreporte() {
        return pto_control_proxreporte;
    }
    
    /**
     * Setter for property pto_control_proxreporte.
     * @param pto_control_proxreporte New value of property pto_control_proxreporte.
     */
    public void setPto_control_proxreporte(java.lang.String pto_control_proxreporte) {
        this.pto_control_proxreporte = pto_control_proxreporte;
    }
    
    /**
     * Getter for property pto_control_ultreporte.
     * @return Value of property pto_control_ultreporte.
     */
    public java.lang.String getPto_control_ultreporte() {
        return pto_control_ultreporte;
    }
    
    /**
     * Setter for property pto_control_ultreporte.
     * @param pto_control_ultreporte New value of property pto_control_ultreporte.
     */
    public void setPto_control_ultreporte(java.lang.String pto_control_ultreporte) {
        this.pto_control_ultreporte = pto_control_ultreporte;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status() {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property ult_observacion.
     * @return Value of property ult_observacion.
     */
    public java.lang.String getUlt_observacion() {
        return ult_observacion;
    }
    
    /**
     * Setter for property ult_observacion.
     * @param ult_observacion New value of property ult_observacion.
     */
    public void setUlt_observacion(java.lang.String ult_observacion) {
        this.ult_observacion = ult_observacion;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }
    
}
