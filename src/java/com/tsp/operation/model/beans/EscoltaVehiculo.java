/*
 * EscoltaVehiculo.java
 *
 * Created on 30 de julio de 2005, 04:30 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Henry
 */
public class EscoltaVehiculo {
        
    private int numcaravana;
    private String numpla;    
    private String placa;
    private String coddestino;
    private String fecasignacion;
    private String regstatus;   
    private String last_update;   
    private String user_update;   
    private String creation_date;   
    private String creation_user;   
    private String base;
    private String razon_exclusion ="";
    private String origen;
    private String destino;

    private String d_o;
    private String factura;
    private String fecha_asignacion_factura;
    private String usuario_asignacion_factura;
    private String fecha_generacion_factura;
    private String usuario_generacion_factura;
    private String reembolsable_cliente;
    private String reembolsable_proveedor;
    private double tarifa;
    private String escolta;
    
    
    /** Creates a new instance of EscoltaVehiculo */
    public EscoltaVehiculo() {
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property coddestino.
     * @return Value of property coddestino.
     */
    public java.lang.String getCoddestino() {
        return coddestino;
    }
    
    /**
     * Setter for property coddestino.
     * @param coddestino New value of property coddestino.
     */
    public void setCoddestino(java.lang.String coddestino) {
        this.coddestino = coddestino;
    }
    
    /**
     * Getter for property fecasignacion.
     * @return Value of property fecasignacion.
     */
    public java.lang.String getFecasignacion() {
        return fecasignacion;
    }
    
    /**
     * Setter for property fecasignacion.
     * @param fecasignacion New value of property fecasignacion.
     */
    public void setFecasignacion(java.lang.String fecasignacion) {
        this.fecasignacion = fecasignacion;
    }
    
    /**
     * Getter for property regstatus.
     * @return Value of property regstatus.
     */
    public java.lang.String getRegstatus() {
        return regstatus;
    }
    
    /**
     * Setter for property regstatus.
     * @param regstatus New value of property regstatus.
     */
    public void setRegstatus(java.lang.String regstatus) {
        this.regstatus = regstatus;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property razon_exclusion.
     * @return Value of property razon_exclusion.
     */
    public java.lang.String getRazon_exclusion() {
        return razon_exclusion;
    }
    
    /**
     * Setter for property razon_exclusion.
     * @param razon_exclusion New value of property razon_exclusion.
     */
    public void setRazon_exclusion(java.lang.String razon_exclusion) {
        this.razon_exclusion = razon_exclusion;
    }
    
    /**
     * Getter for property numcaravana.
     * @return Value of property numcaravana.
     */
    public int getNumcaravana() {
            return numcaravana;
    }
    
    /**
     * Setter for property numcaravana.
     * @param numcaravana New value of property numcaravana.
     */
    public void setNumcaravana(int numcaravana) {
            this.numcaravana = numcaravana;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property d_o.
     * @return Value of property d_o.
     */
    public java.lang.String getD_o() {
        return d_o;
    }
    
    /**
     * Setter for property d_o.
     * @param d_o New value of property d_o.
     */
    public void setD_o(java.lang.String d_o) {
        this.d_o = d_o;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property fecha_asignacion_factura.
     * @return Value of property fecha_asignacion_factura.
     */
    public java.lang.String getFecha_asignacion_factura() {
        return fecha_asignacion_factura;
    }
    
    /**
     * Setter for property fecha_asignacion_factura.
     * @param fecha_asignacion_factura New value of property fecha_asignacion_factura.
     */
    public void setFecha_asignacion_factura(java.lang.String fecha_asignacion_factura) {
        this.fecha_asignacion_factura = fecha_asignacion_factura;
    }
    
    /**
     * Getter for property usuario_asignacion_factura.
     * @return Value of property usuario_asignacion_factura.
     */
    public java.lang.String getUsuario_asignacion_factura() {
        return usuario_asignacion_factura;
    }
    
    /**
     * Setter for property usuario_asignacion_factura.
     * @param usuario_asignacion_factura New value of property usuario_asignacion_factura.
     */
    public void setUsuario_asignacion_factura(java.lang.String usuario_asignacion_factura) {
        this.usuario_asignacion_factura = usuario_asignacion_factura;
    }
    
    /**
     * Getter for property fecha_generacion_factura.
     * @return Value of property fecha_generacion_factura.
     */
    public java.lang.String getFecha_generacion_factura() {
        return fecha_generacion_factura;
    }
    
    /**
     * Setter for property fecha_generacion_factura.
     * @param fecha_generacion_factura New value of property fecha_generacion_factura.
     */
    public void setFecha_generacion_factura(java.lang.String fecha_generacion_factura) {
        this.fecha_generacion_factura = fecha_generacion_factura;
    }
    
    /**
     * Getter for property usuario_generacion_factura.
     * @return Value of property usuario_generacion_factura.
     */
    public java.lang.String getUsuario_generacion_factura() {
        return usuario_generacion_factura;
    }
    
    /**
     * Setter for property usuario_generacion_factura.
     * @param usuario_generacion_factura New value of property usuario_generacion_factura.
     */
    public void setUsuario_generacion_factura(java.lang.String usuario_generacion_factura) {
        this.usuario_generacion_factura = usuario_generacion_factura;
    }
    
    /**
     * Getter for property reembolsable_cliente.
     * @return Value of property reembolsable_cliente.
     */
    public java.lang.String getReembolsable_cliente() {
        return reembolsable_cliente;
    }
    
    /**
     * Setter for property reembolsable_cliente.
     * @param reembolsable_cliente New value of property reembolsable_cliente.
     */
    public void setReembolsable_cliente(java.lang.String reembolsable_cliente) {
        this.reembolsable_cliente = reembolsable_cliente;
    }
    
    /**
     * Getter for property reembolsable_proveedor.
     * @return Value of property reembolsable_proveedor.
     */
    public java.lang.String getReembolsable_proveedor() {
        return reembolsable_proveedor;
    }
    
    /**
     * Setter for property reembolsable_proveedor.
     * @param reembolsable_proveedor New value of property reembolsable_proveedor.
     */
    public void setReembolsable_proveedor(java.lang.String reembolsable_proveedor) {
        this.reembolsable_proveedor = reembolsable_proveedor;
    }
    
    /**
     * Getter for property tarifa.
     * @return Value of property tarifa.
     */
    public double getTarifa() {
        return tarifa;
    }
    
    /**
     * Setter for property tarifa.
     * @param tarifa New value of property tarifa.
     */
    public void setTarifa(double tarifa) {
        this.tarifa = tarifa;
    }
    
    /**
     * Getter for property escolta.
     * @return Value of property escolta.
     */
    public java.lang.String getEscolta() {
        return escolta;
    }
    
    /**
     * Setter for property escolta.
     * @param escolta New value of property escolta.
     */
    public void setEscolta(java.lang.String escolta) {
        this.escolta = escolta;
    }
    
}
