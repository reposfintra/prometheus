/*
 * PlanViaje.java
 *
 * Created on 18 de noviembre de 2004, 22:51
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;

/**
 * @author Armando
 */
public class PlanViaje implements Serializable {
    private String planilla = "";
    private String placa = "";
    private String producto = "";
    private String trailer = "";
    private String contenedor = "";
    private String codtipocarga = "";
    private String tipocarga = "";
    private String fecha = "";
    private String destinatario = "";
    private String ruta = "";
    private String cedcon = "";
    private String nomcon = "";
    private String dircon = "";
    private String phonecon = "";
    private String ciucon = "";
    private String al1 = "";
    private String at1 = "";
    private String al2 = "";
    private String at2 = "";
    private String al3 = "";
    private String at3 = "";
    private String pl1 = "";
    private String pt1 = "";
    private String pl2 = "";
    private String pt2 = "";
    private String pl3 = "";
    private String pt3 = "";
    private String radio = "";
    private String celular = "";
    private String avantel = "";
    private String telefono = "";
    private String cazador = "";
    private String movil = "";
    private String otro = "";
    private String qlo = "";
    private String qto = "";
    private String qld = "";
    private String qtd = "";
    private String ql1 = "";
    private String qt1 = "";
    private String ql2 = "";
    private String qt2 = "";
    private String ql3 = "";
    private String qt3 = "";
    private String nomfam = "";
    private String phonefam = "";
    private String nitpro = "";
    private String nompro = "";
    private String dirpro = "";
    private String phonepro = "";
    private String ciupro = "";
    private String comentario1 = "";
    private String comentario2 = "";
    private String comentario3 = "";
    private String tl1 = "";
    private String tt1 = "";
    private String tl2 = "";
    private String tt2 = "";
    private String tl3 = "";
    private String tt3 = "";
    private String usuario = "";
    private String last_mod_date = "";
    private String last_mod_user = ""; 
    private String last_mod_time = "";
    private String creation_date = "";
    private String retorno = "";
    private String plareal = "";
    private String cia = "";

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getContenedor() {
        return contenedor;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getCedcon() {
        return cedcon;
    }

    public void setCedcon(String cedcon) {
        int i = 0;
        if (cedcon != null) {
            if (cedcon.startsWith("0")){
                while ( cedcon.substring(i, (i+1)).equals("0") ){
                    i++;
                }
                this.cedcon = cedcon.substring(i, cedcon.length());
            }
            else
                this.cedcon = cedcon;
        }
        else
            this.cedcon = "";
        
    }

    public String getAl1() {
        return al1;
    }

    public void setAl1(String al1) {
        this.al1 = al1;
    }

    public String getAt1() {
        return at1;
    }

    public void setAt1(String at1) {
        this.at1 = at1;
    }

    public String getAl2() {
        return al2;
    }

    public void setAl2(String al2) {
        this.al2 = al2;
    }

    public String getAt2() {
        return at2;
    }

    public void setAt2(String at2) {
        this.at2 = at2;
    }

    public String getAl3() {
        return al3;
    }

    public void setAl3(String al3) {
        this.al3 = al3;
    }

    public String getAt3() {
        return at3;
    }

    public void setAt3(String at3) {
        this.at3 = at3;
    }

    public String getPl1() {
        return pl1;
    }

    public void setPl1(String pl1) {
        this.pl1 = pl1;
    }

    public String getPt1() {
        return pt1;
    }

    public void setPt1(String pt1) {
        this.pt1 = pt1;
    }

    public String getPl2() {
        return pl2;
    }

    public void setPl2(String pl2) {
        this.pl2 = pl2;
    }

    public String getPt2() {
        return pt2;
    }

    public void setPt2(String pt2) {
        this.pt2 = pt2;
    }

    public String getPl3() {
        return pl3;
    }

    public void setPl3(String pl3) {
        this.pl3 = pl3;
    }

    public String getPt3() {
        return pt3;
    }

    public void setPt3(String pt3) {
        this.pt3 = pt3;
    }

    public String getRadio() {
        return radio;
    }

    public void setRadio(String radio) {
        this.radio = radio;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getAvantel() {
        return avantel;
    }

    public void setAvantel(String avantel) {
        this.avantel = avantel;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCazador() {
        return cazador;
    }

    public void setCazador(String cazador) {
        this.cazador = cazador;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getOtro() {
        return otro;
    }

    public void setOtro(String otro) {
        this.otro = otro;
    }

    public String getQlo() {
        return qlo;
    }

    public void setQlo(String qlo) {
        this.qlo = qlo;
    }

    public String getQto() {
        return qto;
    }

    public void setQto(String qto) {
        this.qto = qto;
    }

    public String getQld() {
        return qld;
    }

    public void setQld(String qld) {
        this.qld = qld;
    }

    public String getQtd() {
        return qtd;
    }

    public void setQtd(String qtd) {
        this.qtd = qtd;
    }

    public String getQl1() {
        return ql1;
    }

    public void setQl1(String ql1) {
        this.ql1 = ql1;
    }

    public String getQt1() {
        return qt1;
    }

    public void setQt1(String qt1) {
        this.qt1 = qt1;
    }

    public String getQl2() {
        return ql2;
    }

    public void setQl2(String ql2) {
        this.ql2 = ql2;
    }

    public String getQt2() {
        return qt2;
    }

    public void setQt2(String qt2) {
        this.qt2 = qt2;
    }

    public String getQl3() {
        return ql3;
    }

    public void setQl3(String ql3) {
        this.ql3 = ql3;
    }

    public String getQt3() {
        return qt3;
    }

    public void setQt3(String qt3) {
        this.qt3 = qt3;
    }

    public String getNomfam() {
        return nomfam;
    }

    public void setNomfam(String nomfam) {
        this.nomfam = nomfam;
    }

    public String getPhonefam() {
        return phonefam;
    }

    public void setPhonefam(String phonefam) {
        this.phonefam = phonefam;
    }

    public String getNitpro() {
        return nitpro;
    }

    public void setNitpro(String nitpro) {
        int i = 0;
        if (nitpro != null){ 
            if (nitpro.startsWith("0")){
                while ( nitpro.substring(i, (i+1)).equals("0") ){
                    i++;
                }
                this.nitpro = nitpro.substring(i, nitpro.length());
            }
            else
                this.nitpro = nitpro;
        }
        else
            this.nitpro = "";
    }

    public String getComentario1() {
        return comentario1;
    }

    public void setComentario1(String comentario1) {
        this.comentario1 = comentario1;
    }

    public String getComentario2() {
        return comentario2;
    }

    public void setComentario2(String comentario2) {
        this.comentario2 = comentario2;
    }

    public String getComentario3() {
        return comentario3;
    }

    public void setComentario3(String comentario3) {
        this.comentario3 = comentario3;
    }

    public String getTl1() {
        return tl1;
    }

    public void setTl1(String tl1) {
        this.tl1 = tl1;
    }

    public String getTt1() {
        return tt1;
    }

    public void setTt1(String tt1) {
        this.tt1 = tt1;
    }

    public String getTl2() {
        return tl2;
    }

    public void setTl2(String tl2) {
        this.tl2 = tl2;
    }

    public String getTt2() {
        return tt2;
    }

    public void setTt2(String tt2) {
        this.tt2 = tt2;
    }

    public String getTl3() {
        return tl3;
    }

    public void setTl3(String tl3) {
        this.tl3 = tl3;
    }

    public String getTt3() {
        return tt3;
    }

    public void setTt3(String tt3) {
        this.tt3 = tt3;
    }

    public String getLast_mod_date() {
        return last_mod_date;
    }

    public void setLast_mod_date(String last_mod_date) {
        this.last_mod_date = last_mod_date;
    }

    public String getLast_mod_user() {
        return last_mod_user;
    }

    public void setLast_mod_user(String last_mod_user) {
        this.last_mod_user = last_mod_user;
    }

    /**
     * Getter for property dircon.
     * @return Value of property dircon.
     */
    public java.lang.String getDircon() {
        return dircon;
    }    
    
    /**
     * Setter for property dircon.
     * @param dircon New value of property dircon.
     */
    public void setDircon(java.lang.String dircon) {
        this.dircon = dircon;
    }
    
    /**
     * Getter for property nomcon.
     * @return Value of property nomcon.
     */
    public java.lang.String getNomcon() {
        return nomcon;
    }
    
    /**
     * Setter for property nomcon.
     * @param nomcon New value of property nomcon.
     */
    public void setNomcon(java.lang.String nomcon) {
        this.nomcon = nomcon;
    }
    
    /**
     * Getter for property phonecon.
     * @return Value of property phonecon.
     */
    public java.lang.String getPhonecon() {
        return phonecon;
    }
    
    /**
     * Setter for property phonecon.
     * @param phonecon New value of property phonecon.
     */
    public void setPhonecon(java.lang.String phonecon) {
        this.phonecon = phonecon;
    }
    
    /**
     * Getter for property ciucon.
     * @return Value of property ciucon.
     */
    public java.lang.String getCiucon() {
        return ciucon;
    }
    
    /**
     * Setter for property ciucon.
     * @param ciucon New value of property ciucon.
     */
    public void setCiucon(java.lang.String ciucon) {
        this.ciucon = ciucon;
    }
    
    /**
     * Getter for property phonepro.
     * @return Value of property phonepro.
     */
    public java.lang.String getPhonepro() {
        return phonepro;
    }
    
    /**
     * Setter for property phonepro.
     * @param phonepro New value of property phonepro.
     */
    public void setPhonepro(java.lang.String phonepro) {
        this.phonepro = phonepro;
    }
    
    /**
     * Getter for property dirpro.
     * @return Value of property dirpro.
     */
    public java.lang.String getDirpro() {
        return dirpro;
    }
    
    /**
     * Setter for property dirpro.
     * @param dirpro New value of property dirpro.
     */
    public void setDirpro(java.lang.String dirpro) {
        this.dirpro = dirpro;
    }
    
    /**
     * Getter for property ciupro.
     * @return Value of property ciupro.
     */
    public java.lang.String getCiupro() {
        return ciupro;
    }
    
    /**
     * Setter for property ciupro.
     * @param ciupro New value of property ciupro.
     */
    public void setCiupro(java.lang.String ciupro) {
        this.ciupro = ciupro;
    }
    
    /**
     * Getter for property nompro.
     * @return Value of property nompro.
     */
    public java.lang.String getNompro() {
        return nompro;
    }
    
    /**
     * Setter for property nompro.
     * @param nompro New value of property nompro.
     */
    public void setNompro(java.lang.String nompro) {
        this.nompro = nompro;
    }
    
    /**
     * Getter for property last_mod_time.
     * @return Value of property last_mod_time.
     */
    public java.lang.String getLast_mod_time() {
        return last_mod_time;
    }
    
    /**
     * Setter for property last_mod_time.
     * @param last_mod_time New value of property last_mod_time.
     */
    public void setLast_mod_time(java.lang.String last_mod_time) {
        this.last_mod_time = last_mod_time;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property codtipocarga.
     * @return Value of property codtipocarga.
     */
    public java.lang.String getCodtipocarga() {
        return codtipocarga;
    }
    
    /**
     * Setter for property codtipocarga.
     * @param codtipocarga New value of property codtipocarga.
     */
    public void setCodtipocarga(java.lang.String codtipocarga) {
        this.codtipocarga = codtipocarga;
    }
    
    /**
     * Getter for property tipocarga.
     * @return Value of property tipocarga.
     */
    public java.lang.String getTipocarga() {
        return tipocarga;
    }
    
    /**
     * Setter for property tipocarga.
     * @param tipocarga New value of property tipocarga.
     */
    public void setTipocarga(java.lang.String tipocarga) {
        this.tipocarga = tipocarga;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property retorno.
     * @return Value of property retorno.
     */
    public java.lang.String getRetorno() {
        return retorno;
    }
    
    /**
     * Setter for property retorno.
     * @param retorno New value of property retorno.
     */
    public void setRetorno(java.lang.String retorno) {
        this.retorno = retorno;
    }
    
    public String toString(){
        return planilla + ", " + placa + ", " + producto + ", " + trailer + ", " + contenedor + ", " +
               codtipocarga + ", " + tipocarga + ", " + fecha + ", " + destinatario + ", " + ruta + ", " +
               cedcon + ", " + nomcon + ", " + dircon + ", " + phonecon + ", " + ciucon + ", " + 
               al1 + ", " + at1 + ", " + al2 + ", " + at2 + ", " + al3 + ", " + at3 + ", " + pl1 + ", " + 
               pt1 + ", " + pl2 + ", " + pt2 + ", " + pl3 + ", " + pt3 + ", " + radio + ", " + celular + ", " +
               avantel + ", " + telefono + ", " + cazador + ", " + movil + ", " + otro + ", " + qlo + ", " +
               qto + ", " + qld + ", " + qtd + ", " + ql1 + ", " + qt1 + ", " + ql2 + ", " + qt2 + ", "+ ql3 + ", " +
               qt3 + ", " + nomfam + ", " + phonefam + ", " + nitpro + ", " + nompro + ", " + dirpro + ", " +
               phonepro + ", " + ciupro + ", " + comentario1 + ", " + comentario2 + ", " + comentario3 + ", " +
               tl1 + ", " + tt1 + ", " + tl2 + ", " + tt2 + ", " + tl3 + ", " +  tt3 + ", " + usuario + ", " + 
               last_mod_date + ", " + last_mod_user + ", " + last_mod_time + ", " + creation_date + ", " + retorno;
      
    }
    
    /**
     * Getter for property plareal.
     * @return Value of property plareal.
     */
    public java.lang.String getPlareal() {
        return plareal;
    }
    
    /**
     * Setter for property plareal.
     * @param plareal New value of property plareal.
     */
    public void setPlareal(java.lang.String plareal) {
        this.plareal = plareal;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
}
