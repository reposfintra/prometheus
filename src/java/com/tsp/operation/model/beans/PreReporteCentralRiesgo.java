/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author geotech
 */
public class PreReporteCentralRiesgo {
    
   private double obligacionPendiente;
   private String negocio;
   private String nomCliente;
   private String identificacion;
   private String numCuotas;
   private double ObligacionTotal;
   private int cuotaMinVencida;
   private int cuotaMaxVencida;
   private int estado;
   private String fechaApertura;
   private String fechaVenci;
   private int cuotasEnMora;
   private int cuotasCanceladas;
   
    
     public PreReporteCentralRiesgo() {
    }

    /**
     * @return the obligacionPendiente
     */
    public double getObligacionPendiente() {
        return obligacionPendiente;
    }

    /**
     * @param obligacionPendiente the obligacionPendiente to set
     */
    public void setObligacionPendiente(double obligacionPendiente) {
        this.obligacionPendiente = obligacionPendiente;
    }

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    /**
     * @return the nomCliente
     */
    public String getNomCliente() {
        return nomCliente;
    }

    /**
     * @param nomCliente the nomCliente to set
     */
    public void setNomCliente(String nomCliente) {
        this.nomCliente = nomCliente;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the numCuotas
     */
    public String getNumCuotas() {
        return numCuotas;
    }

    /**
     * @param numCuotas the numCuotas to set
     */
    public void setNumCuotas(String numCuotas) {
        this.numCuotas = numCuotas;
    }

    /**
     * @return the ObligacionTotal
     */
    public double getObligacionTotal() {
        return ObligacionTotal;
    }

    /**
     * @param ObligacionTotal the ObligacionTotal to set
     */
    public void setObligacionTotal(double ObligacionTotal) {
        this.ObligacionTotal = ObligacionTotal;
    }

    /**
     * @return the cuotaMinVencida
     */
    public int getCuotaMinVencida() {
        return cuotaMinVencida;
    }

    /**
     * @param cuotaMinVencida the cuotaMinVencida to set
     */
    public void setCuotaMinVencida(int cuotaMinVencida) {
        this.cuotaMinVencida = cuotaMinVencida;
    }

    /**
     * @return the cuotaMaxVencida
     */
    public int getCuotaMaxVencida() {
        return cuotaMaxVencida;
    }

    /**
     * @param cuotaMaxVencida the cuotaMaxVencida to set
     */
    public void setCuotaMaxVencida(int cuotaMaxVencida) {
        this.cuotaMaxVencida = cuotaMaxVencida;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    /**
     * @return the fechaApertura
     */
    public String getFechaApertura() {
        return fechaApertura;
    }

    /**
     * @param fechaApertura the fechaApertura to set
     */
    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    /**
     * @return the fechaVenci
     */
    public String getFechaVenci() {
        return fechaVenci;
    }

    /**
     * @param fechaVenci the fechaVenci to set
     */
    public void setFechaVenci(String fechaVenci) {
        this.fechaVenci = fechaVenci;
    }

    /**
     * @return the cuotasEnMora
     */
    public int getCuotasEnMora() {
        return cuotasEnMora;
    }

    /**
     * @param cuotasEnMora the cuotasEnMora to set
     */
    public void setCuotasEnMora(int cuotasEnMora) {
        this.cuotasEnMora = cuotasEnMora;
    }

    /**
     * @return the cuotasCanceladas
     */
    public int getCuotasCanceladas() {
        return cuotasCanceladas;
    }

    /**
     * @param cuotasCanceladas the cuotasCanceladas to set
     */
    public void setCuotasCanceladas(int cuotasCanceladas) {
        this.cuotasCanceladas = cuotasCanceladas;
    }

    
}
