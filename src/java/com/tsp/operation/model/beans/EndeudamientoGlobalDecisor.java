/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 *
 * @author egonzalez
 */
public class EndeudamientoGlobalDecisor {
    
    private String calificacion;
    private Double saldoPendiente;
    private String tipoCredito;
    private String moneda;
    private Integer numeroCreditos;
    private Timestamp fechaReporte;
    private String entidad;
    private String garantia;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private int numConsumo;
    private double totalConsumo;
    private int numMicrocredito;
    private double totalMicrocredito;
    private int numComercial;
    private double totalComercial;
    private int numHipotecario;
    private double totalHipotecario;
    private String nitEmpresa;

    public EndeudamientoGlobalDecisor() {
    }

    /**
     * @return the calificacion
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     * @param calificacion the calificacion to set
     */
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * @return the saldoPendiente
     */
    public Double getSaldoPendiente() {
        return saldoPendiente;
    }

    /**
     * @param saldoPendiente the saldoPendiente to set
     */
    public void setSaldoPendiente(Double saldoPendiente) {
        this.saldoPendiente = saldoPendiente;
    }

    /**
     * @return the tipoCredito
     */
    public String getTipoCredito() {
        return tipoCredito;
    }

    /**
     * @param tipoCredito the tipoCredito to set
     */
    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the numeroCreditos
     */
    public Integer getNumeroCreditos() {
        return numeroCreditos;
    }

    /**
     * @param numeroCreditos the numeroCreditos to set
     */
    public void setNumeroCreditos(Integer numeroCreditos) {
        this.numeroCreditos = numeroCreditos;
    }

    /**
     * @return the fechaReporte
     */
    public Timestamp getFechaReporte() {
        return fechaReporte;
    }

    /**
     * @param fechaReporte the fechaReporte to set
     */
    public void setFechaReporte(Timestamp fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    /**
     * @return the entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * @param entidad the entidad to set
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * @return the garantia
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * @param garantia the garantia to set
     */
    public void setGarantia(String garantia) {
        this.garantia = garantia;
    }

    /**
     * @return the tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * @param creationUser the creationUser to set
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * @return the userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * @param userUpdate the userUpdate to set
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * @return the numConsumo
     */
    public int getNumConsumo() {
        return numConsumo;
    }

    /**
     * @param numConsumo the numConsumo to set
     */
    public void setNumConsumo(int numConsumo) {
        this.numConsumo = numConsumo;
    }

    /**
     * @return the totalConsumo
     */
    public double getTotalConsumo() {
        return totalConsumo;
    }

    /**
     * @param totalConsumo the totalConsumo to set
     */
    public void setTotalConsumo(double totalConsumo) {
        this.totalConsumo = totalConsumo;
    }

    /**
     * @return the numMicrocredito
     */
    public int getNumMicrocredito() {
        return numMicrocredito;
    }

    /**
     * @param numMicrocredito the numMicrocredito to set
     */
    public void setNumMicrocredito(int numMicrocredito) {
        this.numMicrocredito = numMicrocredito;
    }

    /**
     * @return the totalMicrocredito
     */
    public double getTotalMicrocredito() {
        return totalMicrocredito;
    }

    /**
     * @param totalMicrocredito the totalMicrocredito to set
     */
    public void setTotalMicrocredito(double totalMicrocredito) {
        this.totalMicrocredito = totalMicrocredito;
    }

    /**
     * @return the numComercial
     */
    public int getNumComercial() {
        return numComercial;
    }

    /**
     * @param numComercial the numComercial to set
     */
    public void setNumComercial(int numComercial) {
        this.numComercial = numComercial;
    }

    /**
     * @return the totalComercial
     */
    public double getTotalComercial() {
        return totalComercial;
    }

    /**
     * @param totalComercial the totalComercial to set
     */
    public void setTotalComercial(double totalComercial) {
        this.totalComercial = totalComercial;
    }

    /**
     * @return the numHipotecario
     */
    public int getNumHipotecario() {
        return numHipotecario;
    }

    /**
     * @param numHipotecario the numHipotecario to set
     */
    public void setNumHipotecario(int numHipotecario) {
        this.numHipotecario = numHipotecario;
    }

    /**
     * @return the totalHipotecario
     */
    public double getTotalHipotecario() {
        return totalHipotecario;
    }

    /**
     * @param totalHipotecario the totalHipotecario to set
     */
    public void setTotalHipotecario(double totalHipotecario) {
        this.totalHipotecario = totalHipotecario;
    }

    /**
     * @return the nitEmpresa
     */
    public String getNitEmpresa() {
        return nitEmpresa;
    }

    /**
     * @param nitEmpresa the nitEmpresa to set
     */
    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }
    
    
    
}
