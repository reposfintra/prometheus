/******************************************************************
* Nombre ......................TblGeneralProg.java
* Descripci�n..................Clase bean para tablagen_prog (antes tbl_general_prog)
* Autor........................ricardo rosero
* Fecha........................23/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
* Modificado...................29/12/2005
*******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import com.tsp.operation.model.beans.*;

public class TblGeneralProg implements java.io.Serializable {
    // Atributos
    private String table_type;
    private String program;
    private String table_code;
    private String uc;
    private String um;
    private String fc;
    private String fm;
    private String estado;
    private String distrito;
    private String base;

    /**
    * load
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ ResulSet rs
    * @throws ....... SQLException
    * @version ...... 1.0
    */
    public static TblGeneralProg load(ResultSet rs) throws SQLException {
        TblGeneralProg tbl = new TblGeneralProg();

        tbl.setEstado(rs.getString("reg_status"));
        tbl.setDistrito(rs.getString("dstrct"));
        tbl.settable_type(rs.getString("table_type"));
        tbl.settable_code(rs.getString("table_code"));
        tbl.setprogram(rs.getString("program"));
        String fecha2 =rs.getString("last_update").substring(0,10);
        tbl.setFm(fecha2);
        tbl.setUm(rs.getString("user_update"));
        String fecha1 =rs.getString("creation_date").substring(0,10);
        tbl.setFC(fecha1);
        tbl.setUc(rs.getString("creation_user"));
        tbl.setBase(rs.getString("base"));
        
        return tbl;
    }

    /**
    * setFC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */
    public void setFC(String dato){
        this.fc = dato;
    }

    /**
    * getFC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */
    public String getFC(){
        return this.fc;
    }

    /**
    * settable_type
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */
    public void settable_type(String dato){
        this.table_type = dato;
    }

    /**
    * gettable_type
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */
    public String gettable_type(){
        return this.table_type;
    }

    /**
    * setprogram
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */
    public void setprogram(String dato){
        this.program = dato;
    }

    /**
    * getprogram
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */
    public String getprogram(){
        return this.program;
    }

    /**
     * Getter for property fm.
     * @return Value of property fm.
     */
    public java.lang.String getFm() {
        return fm;
    }

    /**
     * Setter for property fm.
     * @param fm New value of property fm.
     */
    public void setFm(java.lang.String fm) {
        this.fm = fm;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }

    /**
     * Getter for property table_code.
     * @return Value of property table_code.
     */
    public java.lang.String gettable_code() {
        return table_code;
    }

    /**
     * Setter for property table_code.
     * @param table_code New value of property table_code.
     */
    public void settable_code(java.lang.String table_code) {
        this.table_code = table_code;
    }

    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }

    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }

    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }

    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }

    /**
     * Getter for property uc.
     * @return Value of property uc.
     */
    public java.lang.String getUc() {
        return uc;
    }

    /**
     * Setter for property uc.
     * @param uc New value of property uc.
     */
    public void setUc(java.lang.String uc) {
        this.uc = uc;
    }

    /**
     * Getter for property um.
     * @return Value of property um.
     */
    public java.lang.String getUm() {
        return um;
    }

    /**
     * Setter for property um.
     * @param um New value of property um.
     */
    public void setUm(java.lang.String um) {
        this.um = um;
    }

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }

}
