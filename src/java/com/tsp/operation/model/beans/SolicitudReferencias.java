package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudReferencias<br/>
 * 1/03/2011
 * @author ivargas-Geotech
 */
public class SolicitudReferencias {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String tipo;
    private String telefono2;
    private String extension;
    private String primerApellido;
    private String segundoApellido;
    private String primerNombre;
    private String segundoNombre;
    private String secuencia;
    private String direccion;
    private String ciudad;
    private String departamento;
    private String telefono;
    private String celular;
    private String email;
    private String tipoReferencia;
    private String nombre;
    private String tiempoConocido;
    private String parentesco;    
    private String referenciado = "";
    private String referenciaComercial;

    public SolicitudReferencias load(ResultSet rs) throws SQLException {
        SolicitudReferencias referencia = new SolicitudReferencias();
        referencia.setNumeroSolicitud(rs.getString("numero_solicitud"));
        referencia.setTipo(rs.getString("tipo"));
        referencia.setTipoReferencia(rs.getString("tipo_referencia"));
        referencia.setSecuencia(rs.getString("secuencia"));
        referencia.setPrimerApellido(rs.getString("primer_apellido"));
        referencia.setSegundoApellido(rs.getString("segundo_apellido"));
        referencia.setPrimerNombre(rs.getString("primer_nombre"));
        referencia.setSegundoNombre(rs.getString("segundo_nombre"));
        referencia.setNombre(rs.getString("nombre"));
        referencia.setTelefono(rs.getString("telefono1"));
        referencia.setTelefono2(rs.getString("telefono2"));
        referencia.setExtension(rs.getString("extension"));
        referencia.setCelular(rs.getString("celular"));
        referencia.setCiudad(rs.getString("ciudad"));
        referencia.setDepartamento(rs.getString("departamento"));
        referencia.setTiempoConocido(rs.getString("tiempo_conocido"));
        referencia.setParentesco(rs.getString("parentesco"));
        referencia.setEmail(rs.getString("email"));
        referencia.setDireccion(rs.getString("direccion"));
        referencia.setReferenciado((rs.getString("persona_referencia") != null) ? rs.getString("persona_referencia") : "");
        referencia.setReferenciaComercial(rs.getString("referencia_comercial"));
        
        return referencia;
    }

    /**
     * obtiene el valor de extension
     *
     * @return el valor de extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * cambia el valor de extension
     *
     * @param extension nuevo valor extension
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * obtiene el valor de nombre
     *
     * @return el valor de nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * cambia el valor de nombre
     *
     * @param nombre nuevo valor nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * obtiene el valor de parentesco
     *
     * @return el valor de parentesco
     */
    public String getParentesco() {
        return parentesco;
    }

    /**
     * cambia el valor de parentesco
     *
     * @param parentesco nuevo valor parentesco
     */
    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    /**
     * obtiene el valor de tiempoConocido
     *
     * @return el valor de tiempoConocido
     */
    public String getTiempoConocido() {
        return tiempoConocido;
    }

    /**
     * cambia el valor de tiempoConocido
     *
     * @param tiempoConocido nuevo valor tiempoConocido
     */
    public void setTiempoConocido(String tiempoConocido) {
        this.tiempoConocido = tiempoConocido;
    }

    /**
     * obtiene el valor de tipoReferencia
     *
     * @return el valor de tipoReferencia
     */
    public String getTipoReferencia() {
        return tipoReferencia;
    }

    /**
     * cambia el valor de tipoReferencia
     *
     * @param tipoReferencia nuevo valor tipoReferencia
     */
    public void setTipoReferencia(String tipoReferencia) {
        this.tipoReferencia = tipoReferencia;
    }

    /**
     * obtiene el valor de secuencia
     *
     * @return el valor de secuencia
     */
    public String getSecuencia() {
        return secuencia;
    }

    /**
     * cambia el valor de secuencia
     *
     * @param secuencia nuevo valor secuencia
     */
    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * obtiene el valor de telefono
     *
     * @return el valor de telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * cambia el valor de telefono
     *
     * @param telefono nuevo valor telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * obtiene el valor de celular
     *
     * @return el valor de celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * cambia el valor de celular
     *
     * @param celular nuevo valor celular
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * obtiene el valor de ciudad
     *
     * @return el valor de ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * cambia el valor de ciudad
     *
     * @param ciudad nuevo valor ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * obtiene el valor de departamento
     *
     * @return el valor de departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * cambia el valor de departamento
     *
     * @param departamento nuevo valor departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * obtiene el valor de direccion
     *
     * @return el valor de tdireccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * cambia el valor de direccion
     *
     * @param direccion nuevo valor direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * obtiene el valor de email
     *
     * @return el valor de email
     */
    public String getEmail() {
        return email;
    }

    /**
     * cambia el valor de email
     *
     * @param email nuevo valor email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * obtiene el valor de  primerApellido
     *
     * @return el valor de primerApellido
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * cambia el valor de primerApellido
     *
     * @param primerApellido nuevo valor primerApellido
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     * obtiene el valor de  primerNombre
     *
     * @return el valor de primerNombre
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     * cambia el valor de primerNombre
     *
     * @param primerNombre nuevo valor primerNombre
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     * obtiene el valor de  segundoApellido
     *
     * @return el valor de segundoApellido
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     * cambia el valor de segundoApellido
     *
     * @param segundoApellido nuevo valor segundoApellido
     */
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    /**
     * obtiene el valor de  segundoNombre
     *
     * @return el valor de segundoNombre
     */
    public String getSegundoNombre() {
        return segundoNombre;
    }

    /**
     * cambia el valor de segundoNombre
     *
     * @param segundoNombre nuevo valor segundoNombre
     */
    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    /**
     * obtiene el valor de  telefono2
     *
     * @return el valor de telefono2
     */
    public String getTelefono2() {
        return telefono2;
    }

    /**
     * cambia el valor de telefono2
     *
     * @param telefono2 nuevo valor telefono2
     */
    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    /**
     * obtiene el valor de tipo
     *
     * @return el valor de tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * cambia el valor de tipo
     *
     * @param tipo nuevo valor tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getReferenciado() {
        return referenciado;
    }

    public void setReferenciado(String referenciado) {
        this.referenciado = referenciado;
    }

    public String getReferenciaComercial() {
        return referenciaComercial;
    }

    public void setReferenciaComercial(String referenciaComercial) {
        this.referenciaComercial = referenciaComercial;
    }  
}