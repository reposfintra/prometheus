package com.tsp.operation.model.beans;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla fin.credito_bancario_detalle
 * 25/11/2011
 * @author darrieta
 */
public class CreditoBancarioDetalle {
    
    private String dstrct;
    private String nit_banco;
    private String documento;
    private int id;
    private String fecha_inicial;
    private String fecha_final;
    private double dtf;
    private double saldo_inicial;
    private double capital_inicial;
    private double intereses;
    private double ajuste_intereses;
    private double pago_capital;
    private double pago_intereses;
    private String nombreBanco;
    private String creation_user;
    private double tasaEA;
    private double tasaDiaria;
    private double interes_acumulado;
    private String doc_intereses = "";
    private String doc_pago = "";

    /**
     * Get the value of doc_pago
     *
     * @return the value of doc_pago
     */
    public String getDoc_pago() {
        return doc_pago;
    }

    /**
     * Set the value of doc_pago
     *
     * @param doc_pago new value of doc_pago
     */
    public void setDoc_pago(String doc_pago) {
        this.doc_pago = doc_pago;
    }

    /**
     * Get the value of doc_intereses
     *
     * @return the value of doc_intereses
     */
    public String getDoc_intereses() {
        return doc_intereses;
    }

    /**
     * Set the value of doc_intereses
     *
     * @param doc_intereses new value of doc_intereses
     */
    public void setDoc_intereses(String doc_intereses) {
        this.doc_intereses = doc_intereses;
    }


    /**
     * Get the value of interes_acumulado
     *
     * @return the value of interes_acumulado
     */
    public double getInteres_acumulado() {
        return interes_acumulado;
    }

    /**
     * Set the value of interes_acumulado
     *
     * @param interes_acumulado new value of interes_acumulado
     */
    public void setInteres_acumulado(double interes_acumulado) {
        this.interes_acumulado = interes_acumulado;
    }

    /**
     * Get the value of tasaDiaria
     *
     * @return the value of tasaDiaria
     */
    public double getTasaDiaria() {
        return tasaDiaria;
    }

    /**
     * Set the value of tasaDiaria
     *
     * @param tasaDiaria new value of tasaDiaria
     */
    public void setTasaDiaria(double tasaDiaria) {
        this.tasaDiaria = tasaDiaria;
    }

    /**
     * Get the value of tasaEA
     *
     * @return the value of tasaEA
     */
    public double getTasaEA() {
        return tasaEA;
    }

    /**
     * Set the value of tasaEA
     *
     * @param tasaEA new value of tasaEA
     */
    public void setTasaEA(double tasaEA) {
        this.tasaEA = tasaEA;
    }


    /**
     * Get the value of creation_user
     *
     * @return the value of creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * Set the value of creation_user
     *
     * @param creation_user new value of creation_user
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }


    /**
     * Get the value of nombreBanco
     *
     * @return the value of nombreBanco
     */
    public String getNombreBanco() {
        return nombreBanco;
    }

    /**
     * Set the value of nombreBanco
     *
     * @param nombreBanco new value of nombreBanco
     */
    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }


    /**
     * Get the value of pago_intereses
     *
     * @return the value of pago_intereses
     */
    public double getPago_intereses() {
        return pago_intereses;
    }

    /**
     * Set the value of pago_intereses
     *
     * @param pago_intereses new value of pago_intereses
     */
    public void setPago_intereses(double pago_intereses) {
        this.pago_intereses = pago_intereses;
    }


    /**
     * Get the value of pago_capital
     *
     * @return the value of pago_capital
     */
    public double getPago_capital() {
        return pago_capital;
    }

    /**
     * Set the value of pago_capital
     *
     * @param pago_capital new value of pago_capital
     */
    public void setPago_capital(double pago_capital) {
        this.pago_capital = pago_capital;
    }


    /**
     * Get the value of ajuste_intereses
     *
     * @return the value of ajuste_intereses
     */
    public double getAjuste_intereses() {
        return ajuste_intereses;
    }

    /**
     * Set the value of ajuste_intereses
     *
     * @param ajuste_intereses new value of ajuste_intereses
     */
    public void setAjuste_intereses(double ajuste_intereses) {
        this.ajuste_intereses = ajuste_intereses;
    }

    /**
     * Get the value of intereses
     *
     * @return the value of intereses
     */
    public double getIntereses() {
        return intereses;
    }

    /**
     * Set the value of intereses
     *
     * @param intereses new value of intereses
     */
    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }


    /**
     * Get the value of capital_inicial
     *
     * @return the value of capital_inicial
     */
    public double getCapital_inicial() {
        return capital_inicial;
    }

    /**
     * Set the value of capital_inicial
     *
     * @param capital_inicial new value of capital_inicial
     */
    public void setCapital_inicial(double capital_inicial) {
        this.capital_inicial = capital_inicial;
    }


    /**
     * Get the value of saldo_inicial
     *
     * @return the value of saldo_inicial
     */
    public double getSaldo_inicial() {
        return saldo_inicial;
    }

    /**
     * Set the value of saldo_inicial
     *
     * @param saldo_inicial new value of saldo_inicial
     */
    public void setSaldo_inicial(double saldo_inicial) {
        this.saldo_inicial = saldo_inicial;
    }

    /**
     * Get the value of dtf
     *
     * @return the value of dtf
     */
    public double getDtf() {
        return dtf;
    }

    /**
     * Set the value of dtf
     *
     * @param dtf new value of dtf
     */
    public void setDtf(double dtf) {
        this.dtf = dtf;
    }


    /**
     * Get the value of fecha_final
     *
     * @return the value of fecha_final
     */
    public String getFecha_final() {
        return fecha_final;
    }

    /**
     * Set the value of fecha_final
     *
     * @param fecha_final new value of fecha_final
     */
    public void setFecha_final(String fecha_final) {
        this.fecha_final = fecha_final;
    }


    /**
     * Get the value of fecha_inicial
     *
     * @return the value of fecha_inicial
     */
    public String getFecha_inicial() {
        return fecha_inicial;
    }

    /**
     * Set the value of fecha_inicial
     *
     * @param fecha_inicial new value of fecha_inicial
     */
    public void setFecha_inicial(String fecha_inicial) {
        this.fecha_inicial = fecha_inicial;
    }


    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the value of documento
     *
     * @return the value of documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * Set the value of documento
     *
     * @param documento new value of documento
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * Get the value of nit_banco
     *
     * @return the value of nit_banco
     */
    public String getNit_banco() {
        return nit_banco;
    }

    /**
     * Set the value of nit_banco
     *
     * @param nit_banco new value of nit_banco
     */
    public void setNit_banco(String nit_banco) {
        this.nit_banco = nit_banco;
    }

    /**
     * Get the value of dstrct
     *
     * @return the value of dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * Set the value of dstrct
     *
     * @param dstrct new value of dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public static CreditoBancarioDetalle Load(ResultSet rs) throws SQLException{
        
        CreditoBancarioDetalle detalle = new CreditoBancarioDetalle();
        detalle.setId(rs.getInt("id"));
        detalle.setNit_banco(rs.getString("nit_banco"));
        detalle.setDocumento(rs.getString("documento"));
        detalle.setDtf(rs.getDouble("dtf"));
        detalle.setFecha_inicial(rs.getString("fecha_inicial"));
        detalle.setFecha_final(rs.getString("fecha_final"));
        detalle.setSaldo_inicial(rs.getDouble("saldo_inicial"));
        detalle.setCapital_inicial(rs.getDouble("capital_inicial"));
        detalle.setIntereses(rs.getDouble("intereses"));
        detalle.setAjuste_intereses(rs.getDouble("ajuste_intereses"));
        detalle.setPago_capital(rs.getDouble("pago_capital"));
        detalle.setPago_intereses(rs.getDouble("pago_intereses"));
        detalle.setInteres_acumulado(rs.getDouble("interes_acumulado"));
        detalle.setDoc_intereses(rs.getString("doc_intereses"));
        detalle.setDoc_pago(rs.getString("doc_pago"));

        return detalle;
    }
}
