/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class ConfiguracionDescuentosObligaciones {
    
    private int id;
    private String concepto;
    private String descripcion;
    private int descuento;
    private int porcentaje_cta_inicial;
    private String aplica_inicial;
    private int periodo;
    private String tipo_negocio;
    private int id_unidad_negocio;
    private String creation_date;
    private String creation_user;
    private String last_update;
    private String user_update;

    public ConfiguracionDescuentosObligaciones() {
    }
    
    

    public ConfiguracionDescuentosObligaciones(int id, String concepto, String descripcion, int descuento, int porcentaje_cta_inicial, String aplica_inicial, int periodo, String tipo_negocio, int id_unidad_negocio, String creation_date, String creation_user, String last_update, String user_update) {
        this.id = id;
        this.concepto = concepto;
        this.descripcion = descripcion;
        this.descuento = descuento;
        this.porcentaje_cta_inicial = porcentaje_cta_inicial;
        this.aplica_inicial = aplica_inicial;
        this.periodo = periodo;
        this.tipo_negocio = tipo_negocio;
        this.id_unidad_negocio = id_unidad_negocio;
        this.creation_date = creation_date;
        this.creation_user = creation_user;
        this.last_update = last_update;
        this.user_update = user_update;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the concepto
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * @param concepto the concepto to set
     */
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the descuento
     */
    public int getDescuento() {
        return descuento;
    }

    /**
     * @param descuento the descuento to set
     */
    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    /**
     * @return the porcentaje_cta_inicial
     */
    public int getPorcentaje_cta_inicial() {
        return porcentaje_cta_inicial;
    }

    /**
     * @param porcentaje_cta_inicial the porcentaje_cta_inicial to set
     */
    public void setPorcentaje_cta_inicial(int porcentaje_cta_inicial) {
        this.porcentaje_cta_inicial = porcentaje_cta_inicial;
    }

    /**
     * @return the aplica_inicial
     */
    public String getAplica_inicial() {
        return aplica_inicial;
    }

    /**
     * @param aplica_inicial the aplica_inicial to set
     */
    public void setAplica_inicial(String aplica_inicial) {
        this.aplica_inicial = aplica_inicial;
    }

    /**
     * @return the periodo
     */
    public int getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the tipo_negocio
     */
    public String getTipo_negocio() {
        return tipo_negocio;
    }

    /**
     * @param tipo_negocio the tipo_negocio to set
     */
    public void setTipo_negocio(String tipo_negocio) {
        this.tipo_negocio = tipo_negocio;
    }

    /**
     * @return the id_unidad_negocio
     */
    public int getId_unidad_negocio() {
        return id_unidad_negocio;
    }

    /**
     * @param id_unidad_negocio the id_unidad_negocio to set
     */
    public void setId_unidad_negocio(int id_unidad_negocio) {
        this.id_unidad_negocio = id_unidad_negocio;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }
    
    
    
}
