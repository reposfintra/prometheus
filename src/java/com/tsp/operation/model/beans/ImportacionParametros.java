/*************************************************************
 * Nombre      ............... ImportacionParametros.java
 * Descripcion ............... Clase general de importaciones
 * Autor       ............... mfontalvo
 * Fecha       ............... Octubre - 05 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.operation.model.beans;


import java.util.*;
import java.sql.*;


/**
 * Clase que almacena las propiedades generales de una tabla de importacion
 */
public class ImportacionParametros {
    
    
    
    /**
     * Nombre del formato
     */
    private String formato;
    /**
     * Nombre de la tabla de importacion
     */    
    private String tabla;
    /**
     * Descripcion de la tabla de importacion
     */    
    private String descripcion;
    /**
     * parametro de construccion de la tabla en la base de datos, los valores que puede
     * enviar son los siguientes
     *
     *  -   A: nombre del archivo
     *  -   T: nombre de la tabla
     */    
    private String ctab;
    /**
     * fecha de concatenacion al nombre de la tabla de construccion en la base de datos,
     * los valores que puede tomar este campo son los siguientes
     *
     *  -   A: fecha del Archivo
     *  -   S: fecha actual
     *  -   N: no aplicar fecha
     */    
    private String fcon;
    /**
     * Parametro para la asociacion del nombre del archivo con una tabla en la base de
     * datos, los valores que puede tomar son los siguientes
     *
     *  -   I: inicia por el nombre del archivo
     *  -   C: nombre completo
     */    
    private String bpna;
    /**
     * Parametro que indica la manera en que se van a actualizacion de datos en la base
     * de datos, los valores que puede tomar son los siguientes
     *
     *   -   A: adicionar datos al momento de subirlos
     *   -   E: eliminar los datos actuales antes de subir el archivo
     */    
    private String adat;
    /**
     * Indica si el archivo que imporaction a aprocesar contiene titulos, los valores que
     * puede tener este campos son los siguientes:
     *
     *   - S: contiene titulo el archivo de importacion
     *   - N: no contiene titulo el archivo de importacion
     */    
    private String titulo = "S";
    /**
     * Indica si la tabla de importacion asociada al archivo posee o no una clase especial
     * para su proceso enca caso de no estar definida, se procedera con el proceso
     * general de importacion
     */    
    private String evento;
    
    /**
     * Contiene la definicion de los campos de la tabla de importacion, con sus
     * respectivos parametros de importacion independientes
     */    
    private List   lcampos;
    /**
     * Consulta sql generada por la estructura de la tabla de importacion
     */    
    private String Consulta;
    
    /**
     * Crea una nueva instacia de la clase ImportacionParametros
     */
    
    private String insert;
    private String update;
    
    public ImportacionParametros() {      
        formato = "";
        tabla = "";
        descripcion = "";
        ctab = "";
        fcon = "";
        bpna = "";
        adat = "";
        lcampos = null;
        insert  = "N";
        update  = "N";
    }
    
    
    /**
     * Metodo estatico para setear las propiedades del objeto de acuedo a un resultset
     * @autor mfontalvo
     * @param rs ResultSet que contiene datos de la propiedades del bean
     * @return retorna un objeto ImportacionParametro deacuerdo a los valores pasados por rs
     * @throws SQLException .
     */    
    public static ImportacionParametros load (ResultSet rs) throws SQLException {
        ImportacionParametros dt = new ImportacionParametros();
        dt.setFormato     ( rs.getString("formato"    ) );
        dt.setTabla       ( rs.getString("tabla"      ) );
        dt.setDescripcion ( rs.getString("descripcion") );
        dt.setCTAB        ( rs.getString("ctab"       ) );
        dt.setFCON        ( rs.getString("fcon"       ) );
        dt.setBPNA        ( rs.getString("bpna"       ) );
        dt.setADAT        ( rs.getString("adat"       ) );
        dt.setTitulo      ( rs.getString("titulo"     ) );
        dt.setEvento      ( rs.getString("evento"     ) );
        dt.setInsert      ( rs.getString("insert"     ) );
        dt.setUpdate      ( rs.getString("update"     ) );
        return dt;
    }    
    
    /**
     * Metodo que modifica el valor de la propiedad Tabla
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad Tabla
     */    
    public void setTabla (String newValue){
        tabla = newValue;
    }
    /**
     * Metodo que modifica el valor de la propiedad Descripcion
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad Descripcion
     */    
    public void setDescripcion (String newValue){
        descripcion = newValue;
    }
    /**
     * Metodo que modifica el valor de la propiedad CTAB
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad CTAB
     */    
    public void setCTAB (String newValue){
        ctab = newValue;
    }
    /**
     * Metodo que modifica el valor de la propiedad FCON
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad FCON
     */    
    public void setFCON (String newValue){
        fcon = newValue;
    }
    /**
     * Metodo que modifica el valor de la propiedad BPNA
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad BPNA
     */    
    public void setBPNA (String newValue){
        bpna = newValue;
    }
    /**
     * Metodo que modifica el valor de la propiedad ADAT
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad ADAT
     */    
    public void setADAT (String newValue){
        adat = newValue;
    }
    /**
     * Metodo que modifica el valor de la propiedad ListaCampos
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad Lista Campos
     */    
    public void setListaCampos (List newValue){
        lcampos = newValue;
    }
    
    
    /**
     * Obtiene el valor de la propiedad Tabla
     * @autor mfontalvo
     * @return Devuelve el valor de la propiedad Tabla
     */    
    public String getTabla (){
        return tabla;
    }
    /**
     * Obtiene el valor de la propiedad Descripcion
     * @autor mfontalvo
     * @return Devuelve el valor de la propiedad Descripcion
     */    
    public String getDescripcion (){
        return descripcion ;
    }
    /**
     * Obtiene el valor de la propiedad CTAB
     * @autor mfontalvo
     * @return Devuelve el valor de la propiedad CTAB
     */    
    public String getCTAB (){
        return ctab ;
    }
    /**
     * Obtiene el valor de la propiedad FCON
     * @autor mfontalvo
     * @return Devuelve el valor de la propiedad FCON
     */    
    public String getFCON (){
        return fcon ;
    }
    /**
     * Obtiene el valor de la propiedad BPNA
     * @autor mfontalvo
     * @return Devuelve el valor de la propiedad BPNA
     */    
    public String getBPNA (){
        return bpna ;
    }
    /**
     * Obtiene el valor de la propiedad ADAT
     * @autor mfontalvo
     * @return Devuelve el valor de la propiedad ADAT
     */    
    public String getADAT (){
        return adat ;
    }
    /**
     * Obtiene el valor de la propiedad ListaCampos
     * @autor mfontalvo
     * @return Devuelve el valor de la propiedad ListaCampos
     */    
    public List getListaCampos (){
        return lcampos;
    }    
    
    /**
     * Obtiene el valor de la propiedad titulo.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @return valor de la propiedad titulo.
     */
    public java.lang.String getTitulo() {
        return titulo;
    }    
    
    
    /**
     * Cambia el valor de la propiedad titulo.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @param titulo nuevo valor de la propiedad.
     */
    public void setTitulo(java.lang.String titulo) {
        this.titulo = titulo;
    }    
    
    /**
     * Obtiene el valor de la propiedad Consulta.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @return valor de la propiedad titulo Consulta.
     */
    public java.lang.String getConsulta() {
        return Consulta;
    }    
    
    /**
     * Cambia el valor de la propiedad Consulta.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @param Consulta nuevo valor de la propiedad.
     */
    public void setConsulta(java.lang.String Consulta) {
        this.Consulta = Consulta;
    }    
    
    /**
     * Obtiene el valor de la propiedad evento.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @return valor de la propiedad titulo evento.
     */
    public java.lang.String getEvento() {
        return evento;
    }    
    
    /**
     * Cambia el valor de la propiedad evento.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @param evento nuevo valor de la propiedad.
     */
    public void setEvento(java.lang.String evento) {
        this.evento = evento;
    }
    
    /**
     * Getter for property formato.
     * @return Value of property formato.
     */
    public java.lang.String getFormato() {
        return formato;
    }
    
    /**
     * Setter for property formato.
     * @param formato New value of property formato.
     */
    public void setFormato(java.lang.String formato) {
        this.formato = formato;
    }
    
    /**
     * Getter for property insert.
     * @return Value of property insert.
     */
    public java.lang.String getInsert() {
        return insert;
    }
    
    /**
     * Setter for property insert.
     * @param insert New value of property insert.
     */
    public void setInsert(java.lang.String insert) {
        this.insert = insert;
    }
    
    /**
     * Getter for property update.
     * @return Value of property update.
     */
    public java.lang.String getUpdate() {
        return update;
    }
    
    /**
     * Setter for property update.
     * @param update New value of property update.
     */
    public void setUpdate(java.lang.String update) {
        this.update = update;
    }
    
}
