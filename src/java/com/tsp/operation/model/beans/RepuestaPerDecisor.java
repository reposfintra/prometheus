/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class RepuestaPerDecisor {

private String Var_Cuotas_Mensuales;
private String Var_Cartera_Castigada;
private String Var_Cartera_Recuperada;
private String Var_Cartera_Recuperada_Telco;
private String Var_Dudoso_Recaudo;
private String Var_Dudoso_Recaudo_Telco;
private String Var_Moras_30_Act;
private String Var_Moras_30_Act_Telco;
private String Var_Moras_60_Act;
private String Var_Moras_60_Act_Telco;
private String Var_Moras_90_Act;
private String Var_Moras_90_Act_Telco;
private String Var_Moras_120_O_Mas_Act;
private String Var_Moras_120_O_Mas_Act_Telco;
private String Var_Moras_30_His_Ult_12_M;
private String Var_Moras_60_His_Ult_12_M;
private String Var_Moras_90_His_Ult_12_M;
private String Var_Moras_120_His_Ult_12_M;
private String Var_Moras_30_His_Ult_6_M;
private String Var_Moras_60_His_Ult_6_M;
private String Var_Moras_90_His_Ult_6_M;
private String Var_Moras_120_His_Ult_6_M;
private String Var_Mora_Act_Telco_90_O_MAS;
private String Var_Factor_Financiacion;
private String Var_Porcentaje_Financiacion;
private String Var_Dictamen_Buro_Aprobado;
private String Var_Dictamen_Buro_Estudio;
private String Var_Dictamen_Buro_Rechazado;
private String Var_Codigo_Causal;
private String Var_Bandera;
private String Var_Factor_Gastos_Familiares;
private String Var_Gastos_Familiares;
private String Var_Total_Egresos;
private String Var_Disponible;
private String Var_Tasa_Interes;
private String Var_Mensaje_1;
private String Var_Mensaje_2;
private String Var_Mensaje_3;
private String Var_Mensaje_4;
private String Var_Monto_Preaprobado;
private String Var_Cartera_Castigada_Telco;
private String Var_Descripcion_Causal;

    public RepuestaPerDecisor() {
    }

    /**
     * @return the Var_Cuotas_Mensuales
     */
    public String getVar_Cuotas_Mensuales() {
        return Var_Cuotas_Mensuales;
    }

    /**
     * @param Var_Cuotas_Mensuales the Var_Cuotas_Mensuales to set
     */
    public void setVar_Cuotas_Mensuales(String Var_Cuotas_Mensuales) {
        this.Var_Cuotas_Mensuales = Var_Cuotas_Mensuales;
    }

    /**
     * @return the Var_Cartera_Castigada
     */
    public String getVar_Cartera_Castigada() {
        return Var_Cartera_Castigada;
    }

    /**
     * @param Var_Cartera_Castigada the Var_Cartera_Castigada to set
     */
    public void setVar_Cartera_Castigada(String Var_Cartera_Castigada) {
        this.Var_Cartera_Castigada = Var_Cartera_Castigada;
    }

    /**
     * @return the Var_Cartera_Recuperada
     */
    public String getVar_Cartera_Recuperada() {
        return Var_Cartera_Recuperada;
    }

    /**
     * @param Var_Cartera_Recuperada the Var_Cartera_Recuperada to set
     */
    public void setVar_Cartera_Recuperada(String Var_Cartera_Recuperada) {
        this.Var_Cartera_Recuperada = Var_Cartera_Recuperada;
    }

    /**
     * @return the Var_Cartera_Recuperada_Telco
     */
    public String getVar_Cartera_Recuperada_Telco() {
        return Var_Cartera_Recuperada_Telco;
    }

    /**
     * @param Var_Cartera_Recuperada_Telco the Var_Cartera_Recuperada_Telco to set
     */
    public void setVar_Cartera_Recuperada_Telco(String Var_Cartera_Recuperada_Telco) {
        this.Var_Cartera_Recuperada_Telco = Var_Cartera_Recuperada_Telco;
    }

    /**
     * @return the Var_Dudoso_Recaudo
     */
    public String getVar_Dudoso_Recaudo() {
        return Var_Dudoso_Recaudo;
    }

    /**
     * @param Var_Dudoso_Recaudo the Var_Dudoso_Recaudo to set
     */
    public void setVar_Dudoso_Recaudo(String Var_Dudoso_Recaudo) {
        this.Var_Dudoso_Recaudo = Var_Dudoso_Recaudo;
    }

    /**
     * @return the Var_Dudoso_Recaudo_Telco
     */
    public String getVar_Dudoso_Recaudo_Telco() {
        return Var_Dudoso_Recaudo_Telco;
    }

    /**
     * @param Var_Dudoso_Recaudo_Telco the Var_Dudoso_Recaudo_Telco to set
     */
    public void setVar_Dudoso_Recaudo_Telco(String Var_Dudoso_Recaudo_Telco) {
        this.Var_Dudoso_Recaudo_Telco = Var_Dudoso_Recaudo_Telco;
    }

    /**
     * @return the Var_Moras_30_Act
     */
    public String getVar_Moras_30_Act() {
        return Var_Moras_30_Act;
    }

    /**
     * @param Var_Moras_30_Act the Var_Moras_30_Act to set
     */
    public void setVar_Moras_30_Act(String Var_Moras_30_Act) {
        this.Var_Moras_30_Act = Var_Moras_30_Act;
    }

    /**
     * @return the Var_Moras_30_Act_Telco
     */
    public String getVar_Moras_30_Act_Telco() {
        return Var_Moras_30_Act_Telco;
    }

    /**
     * @param Var_Moras_30_Act_Telco the Var_Moras_30_Act_Telco to set
     */
    public void setVar_Moras_30_Act_Telco(String Var_Moras_30_Act_Telco) {
        this.Var_Moras_30_Act_Telco = Var_Moras_30_Act_Telco;
    }

    /**
     * @return the Var_Moras_60_Act
     */
    public String getVar_Moras_60_Act() {
        return Var_Moras_60_Act;
    }

    /**
     * @param Var_Moras_60_Act the Var_Moras_60_Act to set
     */
    public void setVar_Moras_60_Act(String Var_Moras_60_Act) {
        this.Var_Moras_60_Act = Var_Moras_60_Act;
    }

    /**
     * @return the Var_Moras_60_Act_Telco
     */
    public String getVar_Moras_60_Act_Telco() {
        return Var_Moras_60_Act_Telco;
    }

    /**
     * @param Var_Moras_60_Act_Telco the Var_Moras_60_Act_Telco to set
     */
    public void setVar_Moras_60_Act_Telco(String Var_Moras_60_Act_Telco) {
        this.Var_Moras_60_Act_Telco = Var_Moras_60_Act_Telco;
    }

    /**
     * @return the Var_Moras_90_Act
     */
    public String getVar_Moras_90_Act() {
        return Var_Moras_90_Act;
    }

    /**
     * @param Var_Moras_90_Act the Var_Moras_90_Act to set
     */
    public void setVar_Moras_90_Act(String Var_Moras_90_Act) {
        this.Var_Moras_90_Act = Var_Moras_90_Act;
    }

    /**
     * @return the Var_Moras_90_Act_Telco
     */
    public String getVar_Moras_90_Act_Telco() {
        return Var_Moras_90_Act_Telco;
    }

    /**
     * @param Var_Moras_90_Act_Telco the Var_Moras_90_Act_Telco to set
     */
    public void setVar_Moras_90_Act_Telco(String Var_Moras_90_Act_Telco) {
        this.Var_Moras_90_Act_Telco = Var_Moras_90_Act_Telco;
    }

    /**
     * @return the Var_Moras_120_O_Mas_Act
     */
    public String getVar_Moras_120_O_Mas_Act() {
        return Var_Moras_120_O_Mas_Act;
    }

    /**
     * @param Var_Moras_120_O_Mas_Act the Var_Moras_120_O_Mas_Act to set
     */
    public void setVar_Moras_120_O_Mas_Act(String Var_Moras_120_O_Mas_Act) {
        this.Var_Moras_120_O_Mas_Act = Var_Moras_120_O_Mas_Act;
    }

    /**
     * @return the Var_Moras_120_O_Mas_Act_Telco
     */
    public String getVar_Moras_120_O_Mas_Act_Telco() {
        return Var_Moras_120_O_Mas_Act_Telco;
    }

    /**
     * @param Var_Moras_120_O_Mas_Act_Telco the Var_Moras_120_O_Mas_Act_Telco to set
     */
    public void setVar_Moras_120_O_Mas_Act_Telco(String Var_Moras_120_O_Mas_Act_Telco) {
        this.Var_Moras_120_O_Mas_Act_Telco = Var_Moras_120_O_Mas_Act_Telco;
    }

    /**
     * @return the Var_Moras_30_His_Ult_12_M
     */
    public String getVar_Moras_30_His_Ult_12_M() {
        return Var_Moras_30_His_Ult_12_M;
    }

    /**
     * @param Var_Moras_30_His_Ult_12_M the Var_Moras_30_His_Ult_12_M to set
     */
    public void setVar_Moras_30_His_Ult_12_M(String Var_Moras_30_His_Ult_12_M) {
        this.Var_Moras_30_His_Ult_12_M = Var_Moras_30_His_Ult_12_M;
    }

    /**
     * @return the Var_Moras_60_His_Ult_12_M
     */
    public String getVar_Moras_60_His_Ult_12_M() {
        return Var_Moras_60_His_Ult_12_M;
    }

    /**
     * @param Var_Moras_60_His_Ult_12_M the Var_Moras_60_His_Ult_12_M to set
     */
    public void setVar_Moras_60_His_Ult_12_M(String Var_Moras_60_His_Ult_12_M) {
        this.Var_Moras_60_His_Ult_12_M = Var_Moras_60_His_Ult_12_M;
    }

    /**
     * @return the Var_Moras_90_His_Ult_12_M
     */
    public String getVar_Moras_90_His_Ult_12_M() {
        return Var_Moras_90_His_Ult_12_M;
    }

    /**
     * @param Var_Moras_90_His_Ult_12_M the Var_Moras_90_His_Ult_12_M to set
     */
    public void setVar_Moras_90_His_Ult_12_M(String Var_Moras_90_His_Ult_12_M) {
        this.Var_Moras_90_His_Ult_12_M = Var_Moras_90_His_Ult_12_M;
    }

    /**
     * @return the Var_Moras_120_His_Ult_12_M
     */
    public String getVar_Moras_120_His_Ult_12_M() {
        return Var_Moras_120_His_Ult_12_M;
    }

    /**
     * @param Var_Moras_120_His_Ult_12_M the Var_Moras_120_His_Ult_12_M to set
     */
    public void setVar_Moras_120_His_Ult_12_M(String Var_Moras_120_His_Ult_12_M) {
        this.Var_Moras_120_His_Ult_12_M = Var_Moras_120_His_Ult_12_M;
    }

    /**
     * @return the Var_Moras_30_His_Ult_6_M
     */
    public String getVar_Moras_30_His_Ult_6_M() {
        return Var_Moras_30_His_Ult_6_M;
    }

    /**
     * @param Var_Moras_30_His_Ult_6_M the Var_Moras_30_His_Ult_6_M to set
     */
    public void setVar_Moras_30_His_Ult_6_M(String Var_Moras_30_His_Ult_6_M) {
        this.Var_Moras_30_His_Ult_6_M = Var_Moras_30_His_Ult_6_M;
    }

    /**
     * @return the Var_Moras_60_His_Ult_6_M
     */
    public String getVar_Moras_60_His_Ult_6_M() {
        return Var_Moras_60_His_Ult_6_M;
    }

    /**
     * @param Var_Moras_60_His_Ult_6_M the Var_Moras_60_His_Ult_6_M to set
     */
    public void setVar_Moras_60_His_Ult_6_M(String Var_Moras_60_His_Ult_6_M) {
        this.Var_Moras_60_His_Ult_6_M = Var_Moras_60_His_Ult_6_M;
    }

    /**
     * @return the Var_Moras_90_His_Ult_6_M
     */
    public String getVar_Moras_90_His_Ult_6_M() {
        return Var_Moras_90_His_Ult_6_M;
    }

    /**
     * @param Var_Moras_90_His_Ult_6_M the Var_Moras_90_His_Ult_6_M to set
     */
    public void setVar_Moras_90_His_Ult_6_M(String Var_Moras_90_His_Ult_6_M) {
        this.Var_Moras_90_His_Ult_6_M = Var_Moras_90_His_Ult_6_M;
    }

    /**
     * @return the Var_Moras_120_His_Ult_6_M
     */
    public String getVar_Moras_120_His_Ult_6_M() {
        return Var_Moras_120_His_Ult_6_M;
    }

    /**
     * @param Var_Moras_120_His_Ult_6_M the Var_Moras_120_His_Ult_6_M to set
     */
    public void setVar_Moras_120_His_Ult_6_M(String Var_Moras_120_His_Ult_6_M) {
        this.Var_Moras_120_His_Ult_6_M = Var_Moras_120_His_Ult_6_M;
    }

    /**
     * @return the Var_Mora_Act_Telco_90_O_MAS
     */
    public String getVar_Mora_Act_Telco_90_O_MAS() {
        return Var_Mora_Act_Telco_90_O_MAS;
    }

    /**
     * @param Var_Mora_Act_Telco_90_O_MAS the Var_Mora_Act_Telco_90_O_MAS to set
     */
    public void setVar_Mora_Act_Telco_90_O_MAS(String Var_Mora_Act_Telco_90_O_MAS) {
        this.Var_Mora_Act_Telco_90_O_MAS = Var_Mora_Act_Telco_90_O_MAS;
    }

    /**
     * @return the Var_Factor_Financiacion
     */
    public String getVar_Factor_Financiacion() {
        return Var_Factor_Financiacion;
    }

    /**
     * @param Var_Factor_Financiacion the Var_Factor_Financiacion to set
     */
    public void setVar_Factor_Financiacion(String Var_Factor_Financiacion) {
        this.Var_Factor_Financiacion = Var_Factor_Financiacion;
    }

    /**
     * @return the Var_Porcentaje_Financiacion
     */
    public String getVar_Porcentaje_Financiacion() {
        return Var_Porcentaje_Financiacion;
    }

    /**
     * @param Var_Porcentaje_Financiacion the Var_Porcentaje_Financiacion to set
     */
    public void setVar_Porcentaje_Financiacion(String Var_Porcentaje_Financiacion) {
        this.Var_Porcentaje_Financiacion = Var_Porcentaje_Financiacion;
    }

    /**
     * @return the Var_Dictamen_Buro_Aprobado
     */
    public String getVar_Dictamen_Buro_Aprobado() {
        return Var_Dictamen_Buro_Aprobado;
    }

    /**
     * @param Var_Dictamen_Buro_Aprobado the Var_Dictamen_Buro_Aprobado to set
     */
    public void setVar_Dictamen_Buro_Aprobado(String Var_Dictamen_Buro_Aprobado) {
        this.Var_Dictamen_Buro_Aprobado = Var_Dictamen_Buro_Aprobado;
    }

    /**
     * @return the Var_Dictamen_Buro_Estudio
     */
    public String getVar_Dictamen_Buro_Estudio() {
        return Var_Dictamen_Buro_Estudio;
    }

    /**
     * @param Var_Dictamen_Buro_Estudio the Var_Dictamen_Buro_Estudio to set
     */
    public void setVar_Dictamen_Buro_Estudio(String Var_Dictamen_Buro_Estudio) {
        this.Var_Dictamen_Buro_Estudio = Var_Dictamen_Buro_Estudio;
    }

    /**
     * @return the Var_Dictamen_Buro_Rechazado
     */
    public String getVar_Dictamen_Buro_Rechazado() {
        return Var_Dictamen_Buro_Rechazado;
    }

    /**
     * @param Var_Dictamen_Buro_Rechazado the Var_Dictamen_Buro_Rechazado to set
     */
    public void setVar_Dictamen_Buro_Rechazado(String Var_Dictamen_Buro_Rechazado) {
        this.Var_Dictamen_Buro_Rechazado = Var_Dictamen_Buro_Rechazado;
    }

    /**
     * @return the Var_Codigo_Causal
     */
    public String getVar_Codigo_Causal() {
        return Var_Codigo_Causal;
    }

    /**
     * @param Var_Codigo_Causal the Var_Codigo_Causal to set
     */
    public void setVar_Codigo_Causal(String Var_Codigo_Causal) {
        this.Var_Codigo_Causal = Var_Codigo_Causal;
    }

    /**
     * @return the Var_Bandera
     */
    public String getVar_Bandera() {
        return Var_Bandera;
    }

    /**
     * @param Var_Bandera the Var_Bandera to set
     */
    public void setVar_Bandera(String Var_Bandera) {
        this.Var_Bandera = Var_Bandera;
    }

    /**
     * @return the Var_Factor_Gastos_Familiares
     */
    public String getVar_Factor_Gastos_Familiares() {
        return Var_Factor_Gastos_Familiares;
    }

    /**
     * @param Var_Factor_Gastos_Familiares the Var_Factor_Gastos_Familiares to set
     */
    public void setVar_Factor_Gastos_Familiares(String Var_Factor_Gastos_Familiares) {
        this.Var_Factor_Gastos_Familiares = Var_Factor_Gastos_Familiares;
    }

    /**
     * @return the Var_Gastos_Familiares
     */
    public String getVar_Gastos_Familiares() {
        return Var_Gastos_Familiares;
    }

    /**
     * @param Var_Gastos_Familiares the Var_Gastos_Familiares to set
     */
    public void setVar_Gastos_Familiares(String Var_Gastos_Familiares) {
        this.Var_Gastos_Familiares = Var_Gastos_Familiares;
    }

    /**
     * @return the Var_Total_Egresos
     */
    public String getVar_Total_Egresos() {
        return Var_Total_Egresos;
    }

    /**
     * @param Var_Total_Egresos the Var_Total_Egresos to set
     */
    public void setVar_Total_Egresos(String Var_Total_Egresos) {
        this.Var_Total_Egresos = Var_Total_Egresos;
    }

    /**
     * @return the Var_Disponible
     */
    public String getVar_Disponible() {
        return Var_Disponible;
    }

    /**
     * @param Var_Disponible the Var_Disponible to set
     */
    public void setVar_Disponible(String Var_Disponible) {
        this.Var_Disponible = Var_Disponible;
    }

    /**
     * @return the Var_Tasa_Interes
     */
    public String getVar_Tasa_Interes() {
        return Var_Tasa_Interes;
    }

    /**
     * @param Var_Tasa_Interes the Var_Tasa_Interes to set
     */
    public void setVar_Tasa_Interes(String Var_Tasa_Interes) {
        this.Var_Tasa_Interes = Var_Tasa_Interes;
    }

    /**
     * @return the Var_Mensaje_1
     */
    public String getVar_Mensaje_1() {
        return Var_Mensaje_1;
    }

    /**
     * @param Var_Mensaje_1 the Var_Mensaje_1 to set
     */
    public void setVar_Mensaje_1(String Var_Mensaje_1) {
        this.Var_Mensaje_1 = Var_Mensaje_1;
    }

    /**
     * @return the Var_Mensaje_2
     */
    public String getVar_Mensaje_2() {
        return Var_Mensaje_2;
    }

    /**
     * @param Var_Mensaje_2 the Var_Mensaje_2 to set
     */
    public void setVar_Mensaje_2(String Var_Mensaje_2) {
        this.Var_Mensaje_2 = Var_Mensaje_2;
    }

    /**
     * @return the Var_Mensaje_3
     */
    public String getVar_Mensaje_3() {
        return Var_Mensaje_3;
    }

    /**
     * @param Var_Mensaje_3 the Var_Mensaje_3 to set
     */
    public void setVar_Mensaje_3(String Var_Mensaje_3) {
        this.Var_Mensaje_3 = Var_Mensaje_3;
    }

    /**
     * @return the Var_Mensaje_4
     */
    public String getVar_Mensaje_4() {
        return Var_Mensaje_4;
    }

    /**
     * @param Var_Mensaje_4 the Var_Mensaje_4 to set
     */
    public void setVar_Mensaje_4(String Var_Mensaje_4) {
        this.Var_Mensaje_4 = Var_Mensaje_4;
    }

    /**
     * @return the Var_Monto_Preaprobado
     */
    public String getVar_Monto_Preaprobado() {
        return Var_Monto_Preaprobado;
    }

    /**
     * @param Var_Monto_Preaprobado the Var_Monto_Preaprobado to set
     */
    public void setVar_Monto_Preaprobado(String Var_Monto_Preaprobado) {
        this.Var_Monto_Preaprobado = Var_Monto_Preaprobado;
    }

    /**
     * @return the Var_Cartera_Castigada_Telco
     */
    public String getVar_Cartera_Castigada_Telco() {
        return Var_Cartera_Castigada_Telco;
    }

    /**
     * @param Var_Cartera_Castigada_Telco the Var_Cartera_Castigada_Telco to set
     */
    public void setVar_Cartera_Castigada_Telco(String Var_Cartera_Castigada_Telco) {
        this.Var_Cartera_Castigada_Telco = Var_Cartera_Castigada_Telco;
    }

    /**
     * @return the Var_Descripcion_Causal
     */
    public String getVar_Descripcion_Causal() {
        return Var_Descripcion_Causal;
    }

    /**
     * @param Var_Descripcion_Causal the Var_Descripcion_Causal to set
     */
    public void setVar_Descripcion_Causal(String Var_Descripcion_Causal) {
        this.Var_Descripcion_Causal = Var_Descripcion_Causal;
    }
   
    
}
