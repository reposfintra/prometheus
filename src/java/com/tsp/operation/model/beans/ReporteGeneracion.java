/********************************************************************
 *      Nombre Clase.................   ReporteGeneracion.java
 *      Descripci�n..................   Bean del reporte de retroactivos de generaci�n
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   29.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;

public class ReporteGeneracion {
    
    private String usuario;
    private String planilla;
    private String pc;
    private String fecha;
    private String zona;
    private String grabado;
    private String login;
    private String nom_zona;
    private String nom_pc;
        
    
    /** Creates a new instance of ReporteGeneracion */
    public ReporteGeneracion() {
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property grabado.
     * @return Value of property grabado.
     */
    public java.lang.String getGrabado() {
        return grabado;
    }
    
    /**
     * Setter for property grabado.
     * @param grabado New value of property grabado.
     */
    public void setGrabado(java.lang.String grabado) {
        this.grabado = grabado;
    }
    
    /**
     * Getter for property pc.
     * @return Value of property pc.
     */
    public java.lang.String getPc() {
        return pc;
    }
    
    /**
     * Setter for property pc.
     * @param pc New value of property pc.
     */
    public void setPc(java.lang.String pc) {
        this.pc = pc;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }
    
    /**
     * Getter for property login.
     * @return Value of property login.
     */
    public java.lang.String getLogin() {
        return login;
    }
    
    /**
     * Setter for property login.
     * @param login New value of property login.
     */
    public void setLogin(java.lang.String login) {
        this.login = login;
    }
    
    /**
     * Getter for property nom_pc.
     * @return Value of property nom_pc.
     */
    public java.lang.String getNom_pc() {
        return nom_pc;
    }
    
    /**
     * Setter for property nom_pc.
     * @param nom_pc New value of property nom_pc.
     */
    public void setNom_pc(java.lang.String nom_pc) {
        this.nom_pc = nom_pc;
    }
    
    /**
     * Getter for property nom_zona.
     * @return Value of property nom_zona.
     */
    public java.lang.String getNom_zona() {
        return nom_zona;
    }
    
    /**
     * Setter for property nom_zona.
     * @param nom_zona New value of property nom_zona.
     */
    public void setNom_zona(java.lang.String nom_zona) {
        this.nom_zona = nom_zona;
    }
    
    /**
     * Obtinene el valor de las propiedades de un objeto <code>ResultSet</code> y las setea
     * @autor Ing. Tito Andr�s Maturana
     * @param rs Objeto <code>ResultSet</code>
     * @throws SQLException
     */
    public void Load(ResultSet rs) throws SQLException {
        this.fecha = rs.getString("fechareporte");
        this.grabado = rs.getString("grabado");;
        this.login = rs.getString("usuario");
        this.nom_pc = rs.getString("nompc");
        this.nom_zona = rs.getString("nomzona");
        this.pc = rs.getString("ubicacion_procedencia");
        this.planilla = rs.getString("numpla");
        this.usuario = rs.getString("nomusuario");
        this.zona = rs.getString("zona");
    }
    
}
