/*
 * JXLExcel.java
 *
 * Created on 22 de septiembre de 2005, 05:22 PM
 */




package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;


import jxl.*;
import jxl.write.*;
import jxl.write.WritableFont.*;
import jxl.format.*;
import jxl.biff.*;


/**
 *
 * @author  mfontalvo
 */
public class JXLWrite {
    
    
    WritableWorkbook wb;
    WritableSheet sheet;
    int numhojas  = 0;
    
    
    public static jxl.format.Colour Colores;
    
    
    
    /** Creates a new instance of JXLExcel */
    public JXLWrite() {
    }
    
    public JXLWrite(String file) throws Exception {
        nuevoLibro(file);
    }
    
    public void nuevoLibro(String file) throws Exception{
        wb = Workbook.createWorkbook(new File(file));
    }
    
    public void cerrarLibro() throws Exception{
        if (wb==null)
            throw new Exception("No se pudo cerrar el libro, primero deber� crear el libro");
        wb.write();
        wb.close();
        wb = null;
    }
    
    public void obtenerHoja(String hoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");
        
        sheet = wb.getSheet(hoja);
        if (sheet == null) sheet = wb.createSheet(hoja, numhojas++);
        
    }
    
    public WritableSheet obtenerHoja()throws Exception{
        return sheet;
    }
    
    public Cell obtenerCelda(int fila, int columna)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        
        Cell cell   = sheet.getCell(columna, fila);
        return cell;
    }
    
    public void adicionarCelda(int fila, int columna, String value, WritableCellFormat formato)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        if (formato!=null)
            sheet.addCell( new jxl.write.Label(columna, fila, value, formato) );
        else
            sheet.addCell( new jxl.write.Label(columna, fila, value) );
    }
    
    public void adicionarCelda(int fila, int columna, double value, WritableCellFormat formato)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        if (formato!=null)
            sheet.addCell( new jxl.write.Number(columna, fila, value , formato));
        else
            sheet.addCell( new jxl.write.Number(columna, fila, value));
    }
    
    public void adicionarCelda(int fila, int columna, Date value, WritableCellFormat formato)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        if (formato!=null)
            sheet.addCell( new jxl.write.DateTime(columna, fila, value, formato) );
        else
            sheet.addCell( new jxl.write.DateTime(columna, fila, value) );
    }
    
    public void adicionarFormula(int fila, int columna, String formula, WritableCellFormat formato)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        if (formato!=null)
            sheet.addCell( new jxl.write.Formula(columna, fila, formula, formato) );
        else
            sheet.addCell( new jxl.write.Formula(columna, fila, formula) );
       
    } 
        
    
    
    
    public void combinarCeldas(int filaInicial, int columnaInicial, int filaFinal , int columnaFinal)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        sheet.mergeCells(columnaInicial , filaInicial , columnaFinal , filaFinal);
    }
    
    public void cambiarMagnificacion(int porcentaje)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo modificar la magnificacion, primero deber� crear la hoja");
        
        sheet.getSettings().setZoomFactor(porcentaje);
    }
    
    public void cambiarAnchoColumna (int columna, int ancho) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo cambiar el ancho de la columna, primero deber� crear la hoja");
        sheet.setColumnView( columna,  ancho);
        
    }   
    
    public void cambiarAltoFila (int fila, int ancho) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo cambiar alto de la fila, primero deber� crear la hoja");
        sheet.setRowView( fila,  ancho);
        
    }     
    
    
    public void crearPanel (int filas, int columnas)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear el panel, primero deber� crear la hoja");
        sheet.getSettings().setHorizontalFreeze(columnas);
        sheet.getSettings().setVerticalFreeze(filas);
    }
    
    
    public WritableCellFormat nuevoEstilo(String name, int size, boolean bold, boolean italic, Object formato, Object color,  Object fondo, Object align)throws Exception{
        WritableFont fuente = new WritableFont(
        WritableFont.createFont(name) ,
        size,
        (bold ? WritableFont.BOLD: WritableFont.NO_BOLD ) ,
        italic,
        UnderlineStyle.NO_UNDERLINE);
        
        if (color!=null) fuente.setColour((jxl.format.Colour) color);
        
        WritableCellFormat celda = (formato!=null ?
        new WritableCellFormat(fuente, (DisplayFormat) formato):
            new WritableCellFormat(fuente)         );
            
            if (fondo!=null) celda.setBackground( (jxl.format.Colour) fondo);
            if (align!=null) celda.setAlignment ( (jxl.write.Alignment) align);
            
            return celda;
    }
    
    
}
