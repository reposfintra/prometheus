/********************************************************************
 *      Nombre Clase.................   Documento.java    
 *      Descripci�n..................   Bean de la tabla tbldoc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   11.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.Util;
import java.sql.*;


public class Documento {
        private String c_document_type;
        private String c_ndocument_type;
        private String c_document_name;
        private String c_banco;
        private String c_ref_1;
        private String c_sigla;
        
        private String usuario_creacion;
        private String fecha_creacion;
        private String usuario_modificacion;
        private String ultima_modificacion;
        private String distrito;
        private String estado;
        private String base;

        
        /** Creates a new instance of Documento */
        public Documento() {
                this.c_banco = "";
                this.c_document_type = "";
                this.c_ndocument_type = this.c_document_type;
                this.c_document_name = "";
                this.c_ref_1 = "";
                this.c_sigla = "";
        }
        
        /**
         * Toma el valor de cada uno de los campos y los asigna.
         * @autor Tito Andr�s Maturana
         * @param rs ResultSet de la consulta.
         * @throws SQLException
         * @version 1.0
         */  
        public void Load(ResultSet rs) throws SQLException{
                this.c_banco = rs.getString("banco");
                this.c_document_type = rs.getString("document_type");
                this.c_ndocument_type = this.c_document_type;
                this.c_document_name = rs.getString("document_name");
                this.c_ref_1 = rs.getString("ref_1");
                this.c_sigla = rs.getString("sigla");
                this.estado = rs.getString("reg_status");                
                this.distrito = rs.getString("dstrct"); 
                this.base = rs.getString("base");
                this.fecha_creacion = rs.getString("creation_date");
                this.usuario_creacion = rs.getString("creation_user");
                this.ultima_modificacion = rs.getString("last_update");
                this.usuario_modificacion = rs.getString("user_update");
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }
        
        /**
         * Getter for property c_banco.
         * @return Value of property c_banco.
         */
        public java.lang.String getC_banco() {
                return c_banco;
        }
        
        /**
         * Setter for property c_banco.
         * @param c_banco New value of property c_banco.
         */
        public void setC_banco(java.lang.String c_banco) {
                this.c_banco = c_banco;
        }
        
        /**
         * Getter for property c_document_name.
         * @return Value of property c_document_name.
         */
        public java.lang.String getC_document_name() {
                return c_document_name;
        }
        
        /**
         * Setter for property c_document_name.
         * @param c_document_name New value of property c_document_name.
         */
        public void setC_document_name(java.lang.String c_document_name) {
                this.c_document_name = c_document_name;
        }
        
        /**
         * Getter for property c_document_type.
         * @return Value of property c_document_type.
         */
        public java.lang.String getC_document_type() {
                return c_document_type;
        }
        
        /**
         * Setter for property c_document_type.
         * @param c_document_type New value of property c_document_type.
         */
        public void setC_document_type(java.lang.String c_document_type) {
                this.c_document_type = c_document_type;
        }
        
        /**
         * Getter for property c_ref_1.
         * @return Value of property c_ref_1.
         */
        public java.lang.String getC_ref_1() {
                return c_ref_1;
        }
        
        /**
         * Setter for property c_ref_1.
         * @param c_ref_1 New value of property c_ref_1.
         */
        public void setC_ref_1(java.lang.String c_ref_1) {
                this.c_ref_1 = c_ref_1;
        }
        
        /**
         * Getter for property c_sigla.
         * @return Value of property c_sigla.
         */
        public java.lang.String getC_sigla() {
                return c_sigla;
        }
        
        /**
         * Setter for property c_sigla.
         * @param c_sigla New value of property c_sigla.
         */
        public void setC_sigla(java.lang.String c_sigla) {
                this.c_sigla = c_sigla;
        }
        
        /**
         * Getter for property distrito.
         * @return Value of property distrito.
         */
        public java.lang.String getDistrito() {
                return distrito;
        }
        
        /**
         * Setter for property distrito.
         * @param distrito New value of property distrito.
         */
        public void setDistrito(java.lang.String distrito) {
                this.distrito = distrito;
        }
        
        /**
         * Getter for property estado.
         * @return Value of property estado.
         */
        public java.lang.String getEstado() {
                return estado;
        }
        
        /**
         * Setter for property estado.
         * @param estado New value of property estado.
         */
        public void setEstado(java.lang.String estado) {
                this.estado = estado;
        }
        
        /**
         * Getter for property fecha_creacion.
         * @return Value of property fecha_creacion.
         */
        public java.lang.String getFecha_creacion() {
                return fecha_creacion;
        }
        
        /**
         * Setter for property fecha_creacion.
         * @param fecha_creacion New value of property fecha_creacion.
         */
        public void setFecha_creacion(java.lang.String fecha_creacion) {
                this.fecha_creacion = fecha_creacion;
        }
        
        /**
         * Getter for property ultima_modificacion.
         * @return Value of property ultima_modificacion.
         */
        public java.lang.String getUltima_modificacion() {
                return ultima_modificacion;
        }
        
        /**
         * Setter for property ultima_modificacion.
         * @param ultima_modificacion New value of property ultima_modificacion.
         */
        public void setUltima_modificacion(java.lang.String ultima_modificacion) {
                this.ultima_modificacion = ultima_modificacion;
        }
        
        /**
         * Getter for property usuario_creacion.
         * @return Value of property usuario_creacion.
         */
        public java.lang.String getUsuario_creacion() {
                return usuario_creacion;
        }
        
        /**
         * Setter for property usuario_creacion.
         * @param usuario_creacion New value of property usuario_creacion.
         */
        public void setUsuario_creacion(java.lang.String usuario_creacion) {
                this.usuario_creacion = usuario_creacion;
        }
        
        /**
         * Getter for property usuario_modificacion.
         * @return Value of property usuario_modificacion.
         */
        public java.lang.String getUsuario_modificacion() {
                return usuario_modificacion;
        }
        
        /**
         * Setter for property usuario_modificacion.
         * @param usuario_modificacion New value of property usuario_modificacion.
         */
        public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
                this.usuario_modificacion = usuario_modificacion;
        }
        
        /**
         * Getter for property c_ndocument_type.
         * @return Value of property c_ndocument_type.
         */
        public java.lang.String getC_ndocument_type() {
                return c_ndocument_type;
        }
        
        /**
         * Setter for property c_ndocument_type.
         * @param c_ndocument_type New value of property c_ndocument_type.
         */
        public void setC_ndocument_type(java.lang.String c_ndocument_type) {
                this.c_ndocument_type = c_ndocument_type;
        }
        
}
