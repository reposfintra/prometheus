/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 *
 * @author egonzalez
 */
public class CuentaAhorroDecisor {
    
  private Boolean bloqueada;
  private String entidad; 
  private String numero;
  private Timestamp fechaApertura; 
  private Short situacionTitular; 
  private String oficina; 
  private String ciudad; 
  private String codigoDaneCiudad;
  private String tipoIdentificacion; 
  private String identificacion; 
  private String sector; 
  private String creationUser;
  private String userUpdate;
  private String nitEmpresa;
  private String estado;
  private Timestamp ultimaActualizacion;

    public CuentaAhorroDecisor() {
    }

    /**
     * @return the bloqueada
     */
    public Boolean getBloqueada() {
        return bloqueada;
    }

    /**
     * @param bloqueada the bloqueada to set
     */
    public void setBloqueada(Boolean bloqueada) {
        this.bloqueada = bloqueada;
    }

    /**
     * @return the entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * @param entidad the entidad to set
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the fechaApertura
     */
    public Timestamp getFechaApertura() {
        return fechaApertura;
    }

    /**
     * @param fechaApertura the fechaApertura to set
     */
    public void setFechaApertura(Timestamp fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    /**
     * @return the situacionTitular
     */
    public Short getSituacionTitular() {
        return situacionTitular;
    }

    /**
     * @param situacionTitular the situacionTitular to set
     */
    public void setSituacionTitular(Short situacionTitular) {
        this.situacionTitular = situacionTitular;
    }

    /**
     * @return the oficina
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * @param oficina the oficina to set
     */
    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the codigoDaneCiudad
     */
    public String getCodigoDaneCiudad() {
        return codigoDaneCiudad;
    }

    /**
     * @param codigoDaneCiudad the codigoDaneCiudad to set
     */
    public void setCodigoDaneCiudad(String codigoDaneCiudad) {
        this.codigoDaneCiudad = codigoDaneCiudad;
    }

    /**
     * @return the tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * @param creationUser the creationUser to set
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * @return the userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * @param userUpdate the userUpdate to set
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * @return the nitEmpresa
     */
    public String getNitEmpresa() {
        return nitEmpresa;
    }

    /**
     * @param nitEmpresa the nitEmpresa to set
     */
    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the ultimaActualizacion
     */
    public Timestamp getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    /**
     * @param ultimaActualizacion the ultimaActualizacion to set
     */
    public void setUltimaActualizacion(Timestamp ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }
    
    
  
}
