/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author desarrollo
 */
public class ProcesoMeta {
    private int id;
    private String reg_status;
    private int id_tabla_rel;
    private String nombre;
    private String descripcion;
    private String descripcionTablaRel; 
    private String tipo;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the id_tabla_rel
     */
    public int getId_tabla_rel() {
        return id_tabla_rel;
    }

    /**
     * @param id_tabla_rel the id_tabla_rel to set
     */
    public void setId_tabla_rel(int id_tabla_rel) {
        this.id_tabla_rel = id_tabla_rel;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the descripcionTablaRel
     */
    public String getDescripcionTablaRel() {
        return descripcionTablaRel;
    }

    /**
     * @param descripcionTablaRel the descripcionTablaRel to set
     */
    public void setDescripcionTablaRel(String descripcionTablaRel) {
        this.descripcionTablaRel = descripcionTablaRel;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
}
