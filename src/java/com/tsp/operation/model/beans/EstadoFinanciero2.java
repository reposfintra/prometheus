/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author Alvaro
 */
public class EstadoFinanciero2 extends EstadoFinanciero implements Serializable{


    private double valor_periodo1;
    private double valor_periodo2;
    private double valor_periodo3;


    public EstadoFinanciero2() {

    }


    public static EstadoFinanciero2 load(java.sql.ResultSet rs)throws java.sql.SQLException{

        EstadoFinanciero2 estadoFinanciero2 = new EstadoFinanciero2();


        estadoFinanciero2.setSecuencia(rs.getDouble("secuencia"));
        estadoFinanciero2.setIndicador_tercero( rs.getString("indicador_tercero") ) ;
        estadoFinanciero2.setTipo_registro( rs.getString("tipo_registro") ) ;
        estadoFinanciero2.setDescripcion( rs.getString("descripcion") ) ;
        estadoFinanciero2.setCuenta( rs.getString("cuenta") ) ;
        estadoFinanciero2.setTercero( rs.getString("tercero") ) ;

        estadoFinanciero2.setValor_periodo1( rs.getDouble("valor_periodo1") ) ;
        estadoFinanciero2.setValor_periodo2( rs.getDouble("valor_periodo2") ) ;
        estadoFinanciero2.setValor_periodo3( rs.getDouble("valor_periodo3") ) ;
        estadoFinanciero2.setFormula( rs.getString("formula") );

        return estadoFinanciero2;

    }


    /**
     * @return the valor_periodo1
     */
    public double getValor_periodo1() {
        return valor_periodo1;
    }

    /**
     * @param valor_periodo1 the valor_periodo1 to set
     */
    public void setValor_periodo1(double valor_periodo1) {
        this.valor_periodo1 = valor_periodo1;
    }

    /**
     * @return the valor_periodo2
     */
    public double getValor_periodo2() {
        return valor_periodo2;
    }

    /**
     * @param valor_periodo2 the valor_periodo2 to set
     */
    public void setValor_periodo2(double valor_periodo2) {
        this.valor_periodo2 = valor_periodo2;
    }

    /**
     * @return the valor_periodo3
     */
    public double getValor_periodo3() {
        return valor_periodo3;
    }

    /**
     * @param valor_periodo3 the valor_periodo3 to set
     */
    public void setValor_periodo3(double valor_periodo3) {
        this.valor_periodo3 = valor_periodo3;
    }




}
