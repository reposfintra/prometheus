/*
 * Nombre        ReporteRemesasPorFacturar.java
 * Autor         Ing. Lissett Reales.
 * Fecha         21 de julio de 2006, 07:42 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class ReporteRemesasPorFacturar implements Serializable {
    
    //String ind;//                
    private String ot;
    private String valor_ot;
    private String age_ori;
    private String fec_desp;
    private String age_dest;                
    private String fec_trafico;
    private String entregada;              
    private String age_cump;
    private String fec_cump;
    private String cumplida;
    private String oc;
    private String cliente;                
    private String cod_cliente;
    private String ruta;
    private String age_duenia;
    private String doc_int;
    private String std_job;                
    private String descripcion_ot;                
    private String aux_integral;
    private String fec_envio_log;
    private String fec_envio_fis;
    private String fec_recibo_log;
    private String fec_recibo_fis;
    
    private String dias;            
    private String anio_cump;
    private String mes_cump;
    private String dia_cump;
    private String anio_reporte;
    private String mes_reporte;
    private String dia_reporte;
    private String valor_oc;
    private String cant_oc;
    private String placa;
    private String wo_type;              
    private String anio_desp;
    private String mes_desp;
    private String cumplidor;
    private String observacion;
    private String vencimiento;
    
    // MODIFICADO 22 NOVIEMBRE 2006 - LREALES
    private String age_facturacion;
    //
    
    private String esVacio;
    
    private String tipo_entrega;
    private String ot_padre;
    private String asociadas_ot_padre;
    private String estado_ot;
    
    //AMATURANA 31.03.2007
    private String fiduciaria;
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt() {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt(java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property valor_ot.
     * @return Value of property valor_ot.
     */
    public java.lang.String getValor_ot() {
        return valor_ot;
    }
    
    /**
     * Setter for property valor_ot.
     * @param valor_ot New value of property valor_ot.
     */
    public void setValor_ot(java.lang.String valor_ot) {
        this.valor_ot = valor_ot;
    }
    
    /**
     * Getter for property age_ori.
     * @return Value of property age_ori.
     */
    public java.lang.String getAge_ori() {
        return age_ori;
    }
    
    /**
     * Setter for property age_ori.
     * @param age_ori New value of property age_ori.
     */
    public void setAge_ori(java.lang.String age_ori) {
        this.age_ori = age_ori;
    }
    
    /**
     * Getter for property fec_desp.
     * @return Value of property fec_desp.
     */
    public java.lang.String getFec_desp() {
        return fec_desp;
    }
    
    /**
     * Setter for property fec_desp.
     * @param fec_desp New value of property fec_desp.
     */
    public void setFec_desp(java.lang.String fec_desp) {
        this.fec_desp = fec_desp;
    }
    
    /**
     * Getter for property age_dest.
     * @return Value of property age_dest.
     */
    public java.lang.String getAge_dest() {
        return age_dest;
    }
    
    /**
     * Setter for property age_dest.
     * @param age_dest New value of property age_dest.
     */
    public void setAge_dest(java.lang.String age_dest) {
        this.age_dest = age_dest;
    }
    
    /**
     * Getter for property fec_trafico.
     * @return Value of property fec_trafico.
     */
    public java.lang.String getFec_trafico() {
        return fec_trafico;
    }
    
    /**
     * Setter for property fec_trafico.
     * @param fec_trafico New value of property fec_trafico.
     */
    public void setFec_trafico(java.lang.String fec_trafico) {
        this.fec_trafico = fec_trafico;
    }
    
    /**
     * Getter for property entregada.
     * @return Value of property entregada.
     */
    public java.lang.String getEntregada() {
        return entregada;
    }
    
    /**
     * Setter for property entregada.
     * @param entregada New value of property entregada.
     */
    public void setEntregada(java.lang.String entregada) {
        this.entregada = entregada;
    }
    
    /**
     * Getter for property age_cump.
     * @return Value of property age_cump.
     */
    public java.lang.String getAge_cump() {
        return age_cump;
    }
    
    /**
     * Setter for property age_cump.
     * @param age_cump New value of property age_cump.
     */
    public void setAge_cump(java.lang.String age_cump) {
        this.age_cump = age_cump;
    }
    
    /**
     * Getter for property fec_cump.
     * @return Value of property fec_cump.
     */
    public java.lang.String getFec_cump() {
        return fec_cump;
    }
    
    /**
     * Setter for property fec_cump.
     * @param fec_cump New value of property fec_cump.
     */
    public void setFec_cump(java.lang.String fec_cump) {
        this.fec_cump = fec_cump;
    }
    
    /**
     * Getter for property cumplida.
     * @return Value of property cumplida.
     */
    public java.lang.String getCumplida() {
        return cumplida;
    }
    
    /**
     * Setter for property cumplida.
     * @param cumplida New value of property cumplida.
     */
    public void setCumplida(java.lang.String cumplida) {
        this.cumplida = cumplida;
    }
    
    /**
     * Getter for property oc.
     * @return Value of property oc.
     */
    public java.lang.String getOc() {
        return oc;
    }
    
    /**
     * Setter for property oc.
     * @param oc New value of property oc.
     */
    public void setOc(java.lang.String oc) {
        this.oc = oc;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property cod_cliente.
     * @return Value of property cod_cliente.
     */
    public java.lang.String getCod_cliente() {
        return cod_cliente;
    }
    
    /**
     * Setter for property cod_cliente.
     * @param cod_cliente New value of property cod_cliente.
     */
    public void setCod_cliente(java.lang.String cod_cliente) {
        this.cod_cliente = cod_cliente;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property age_duenia.
     * @return Value of property age_duenia.
     */
    public java.lang.String getAge_duenia() {
        return age_duenia;
    }
    
    /**
     * Setter for property age_duenia.
     * @param age_duenia New value of property age_duenia.
     */
    public void setAge_duenia(java.lang.String age_duenia) {
        this.age_duenia = age_duenia;
    }
    
    /**
     * Getter for property doc_int.
     * @return Value of property doc_int.
     */
    public java.lang.String getDoc_int() {
        return doc_int;
    }
    
    /**
     * Setter for property doc_int.
     * @param doc_int New value of property doc_int.
     */
    public void setDoc_int(java.lang.String doc_int) {
        this.doc_int = doc_int;
    }
    
    /**
     * Getter for property std_job.
     * @return Value of property std_job.
     */
    public java.lang.String getStd_job() {
        return std_job;
    }
    
    /**
     * Setter for property std_job.
     * @param std_job New value of property std_job.
     */
    public void setStd_job(java.lang.String std_job) {
        this.std_job = std_job;
    }
    
    /**
     * Getter for property descripcion_ot.
     * @return Value of property descripcion_ot.
     */
    public java.lang.String getDescripcion_ot() {
        return descripcion_ot;
    }
    
    /**
     * Setter for property descripcion_ot.
     * @param descripcion_ot New value of property descripcion_ot.
     */
    public void setDescripcion_ot(java.lang.String descripcion_ot) {
        this.descripcion_ot = descripcion_ot;
    }
    
    /**
     * Getter for property aux_integral.
     * @return Value of property aux_integral.
     */
    public java.lang.String getAux_integral() {
        return aux_integral;
    }
    
    /**
     * Setter for property aux_integral.
     * @param aux_integral New value of property aux_integral.
     */
    public void setAux_integral(java.lang.String aux_integral) {
        this.aux_integral = aux_integral;
    }
    
    /**
     * Getter for property dias.
     * @return Value of property dias.
     */
    public java.lang.String getDias() {
        return dias;
    }
    
    /**
     * Setter for property dias.
     * @param dias New value of property dias.
     */
    public void setDias(java.lang.String dias) {
        this.dias = dias;
    }
    
    /**
     * Getter for property anio_cump.
     * @return Value of property anio_cump.
     */
    public java.lang.String getAnio_cump() {
        return anio_cump;
    }
    
    /**
     * Setter for property anio_cump.
     * @param anio_cump New value of property anio_cump.
     */
    public void setAnio_cump(java.lang.String anio_cump) {
        this.anio_cump = anio_cump;
    }
    
    /**
     * Getter for property mes_cump.
     * @return Value of property mes_cump.
     */
    public java.lang.String getMes_cump() {
        return mes_cump;
    }
    
    /**
     * Setter for property mes_cump.
     * @param mes_cump New value of property mes_cump.
     */
    public void setMes_cump(java.lang.String mes_cump) {
        this.mes_cump = mes_cump;
    }
    
    /**
     * Getter for property dia_cump.
     * @return Value of property dia_cump.
     */
    public java.lang.String getDia_cump() {
        return dia_cump;
    }
    
    /**
     * Setter for property dia_cump.
     * @param dia_cump New value of property dia_cump.
     */
    public void setDia_cump(java.lang.String dia_cump) {
        this.dia_cump = dia_cump;
    }
    
    /**
     * Getter for property anio_reporte.
     * @return Value of property anio_reporte.
     */
    public java.lang.String getAnio_reporte() {
        return anio_reporte;
    }
    
    /**
     * Setter for property anio_reporte.
     * @param anio_reporte New value of property anio_reporte.
     */
    public void setAnio_reporte(java.lang.String anio_reporte) {
        this.anio_reporte = anio_reporte;
    }
    
    /**
     * Getter for property mes_reporte.
     * @return Value of property mes_reporte.
     */
    public java.lang.String getMes_reporte() {
        return mes_reporte;
    }
    
    /**
     * Setter for property mes_reporte.
     * @param mes_reporte New value of property mes_reporte.
     */
    public void setMes_reporte(java.lang.String mes_reporte) {
        this.mes_reporte = mes_reporte;
    }
    
    /**
     * Getter for property dia_reporte.
     * @return Value of property dia_reporte.
     */
    public java.lang.String getDia_reporte() {
        return dia_reporte;
    }
    
    /**
     * Setter for property dia_reporte.
     * @param dia_reporte New value of property dia_reporte.
     */
    public void setDia_reporte(java.lang.String dia_reporte) {
        this.dia_reporte = dia_reporte;
    }
    
    /**
     * Getter for property valor_oc.
     * @return Value of property valor_oc.
     */
    public java.lang.String getValor_oc() {
        return valor_oc;
    }
    
    /**
     * Setter for property valor_oc.
     * @param valor_oc New value of property valor_oc.
     */
    public void setValor_oc(java.lang.String valor_oc) {
        this.valor_oc = valor_oc;
    }
    
    /**
     * Getter for property cant_oc.
     * @return Value of property cant_oc.
     */
    public java.lang.String getCant_oc() {
        return cant_oc;
    }
    
    /**
     * Setter for property cant_oc.
     * @param cant_oc New value of property cant_oc.
     */
    public void setCant_oc(java.lang.String cant_oc) {
        this.cant_oc = cant_oc;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property wo_type.
     * @return Value of property wo_type.
     */
    public java.lang.String getWo_type() {
        return wo_type;
    }
    
    /**
     * Setter for property wo_type.
     * @param wo_type New value of property wo_type.
     */
    public void setWo_type(java.lang.String wo_type) {
        this.wo_type = wo_type;
    }
    
    /**
     * Getter for property anio_desp.
     * @return Value of property anio_desp.
     */
    public java.lang.String getAnio_desp() {
        return anio_desp;
    }
    
    /**
     * Setter for property anio_desp.
     * @param anio_desp New value of property anio_desp.
     */
    public void setAnio_desp(java.lang.String anio_desp) {
        this.anio_desp = anio_desp;
    }
    
    /**
     * Getter for property mes_desp.
     * @return Value of property mes_desp.
     */
    public java.lang.String getMes_desp() {
        return mes_desp;
    }
    
    /**
     * Setter for property mes_desp.
     * @param mes_desp New value of property mes_desp.
     */
    public void setMes_desp(java.lang.String mes_desp) {
        this.mes_desp = mes_desp;
    }
    
    /**
     * Getter for property cumplidor.
     * @return Value of property cumplidor.
     */
    public java.lang.String getCumplidor() {
        return cumplidor;
    }
    
    /**
     * Setter for property cumplidor.
     * @param cumplidor New value of property cumplidor.
     */
    public void setCumplidor(java.lang.String cumplidor) {
        this.cumplidor = cumplidor;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property vencimiento.
     * @return Value of property vencimiento.
     */
    public java.lang.String getVencimiento() {
        return vencimiento;
    }
    
    /**
     * Setter for property vencimiento.
     * @param vencimiento New value of property vencimiento.
     */
    public void setVencimiento(java.lang.String vencimiento) {
        this.vencimiento = vencimiento;
    }
    
    
    // MODIFICADO 22 NOVIEMBRE 2006 - LREALES
    
    /**
     * Getter for property age_facturacion.
     * @return Value of property age_facturacion.
     */
    public java.lang.String getAge_facturacion() {
        return age_facturacion;
    }
    
    /**
     * Setter for property age_facturacion.
     * @param age_facturacion New value of property age_facturacion.
     */
    public void setAge_facturacion(java.lang.String age_facturacion) {
        this.age_facturacion = age_facturacion;
    }
    
    /**
     * Getter for property esVacio.
     * @return Value of property esVacio.
     */
    public java.lang.String getEsVacio() {
        return esVacio;
    }
    
    /**
     * Setter for property esVacio.
     * @param esVacio New value of property esVacio.
     */
    public void setEsVacio(java.lang.String esVacio) {
        this.esVacio = esVacio;
    }
    
    /**
     * Getter for property fec_envio_log.
     * @return Value of property fec_envio_log.
     */
    public java.lang.String getFec_envio_log() {
        return fec_envio_log;
    }
    
    /**
     * Setter for property fec_envio_log.
     * @param fec_envio_log New value of property fec_envio_log.
     */
    public void setFec_envio_log(java.lang.String fec_envio_log) {
        this.fec_envio_log = fec_envio_log;
    }
    
    /**
     * Getter for property fec_envio_fis.
     * @return Value of property fec_envio_fis.
     */
    public java.lang.String getFec_envio_fis() {
        return fec_envio_fis;
    }
    
    /**
     * Setter for property fec_envio_fis.
     * @param fec_envio_fis New value of property fec_envio_fis.
     */
    public void setFec_envio_fis(java.lang.String fec_envio_fis) {
        this.fec_envio_fis = fec_envio_fis;
    }
    
    /**
     * Getter for property fec_recibo_log.
     * @return Value of property fec_recibo_log.
     */
    public java.lang.String getFec_recibo_log() {
        return fec_recibo_log;
    }
    
    /**
     * Setter for property fec_recibo_log.
     * @param fec_recibo_log New value of property fec_recibo_log.
     */
    public void setFec_recibo_log(java.lang.String fec_recibo_log) {
        this.fec_recibo_log = fec_recibo_log;
    }
    
    /**
     * Getter for property fec_recibo_fis.
     * @return Value of property fec_recibo_fis.
     */
    public java.lang.String getFec_recibo_fis() {
        return fec_recibo_fis;
    }
    
    /**
     * Setter for property fec_recibo_fis.
     * @param fec_recibo_fis New value of property fec_recibo_fis.
     */
    public void setFec_recibo_fis(java.lang.String fec_recibo_fis) {
        this.fec_recibo_fis = fec_recibo_fis;
    }
    
    /**
     * Getter for property ot_padre.
     * @return Value of property ot_padre.
     */
    public java.lang.String getOt_padre() {
        return ot_padre;
    }
    
    /**
     * Setter for property ot_padre.
     * @param ot_padre New value of property ot_padre.
     */
    public void setOt_padre(java.lang.String ot_padre) {
        this.ot_padre = ot_padre;
    }
    
    /**
     * Getter for property asociadas_ot_padre.
     * @return Value of property asociadas_ot_padre.
     */
    public java.lang.String getAsociadas_ot_padre() {
        return asociadas_ot_padre;
    }
    
    /**
     * Setter for property asociadas_ot_padre.
     * @param asociadas_ot_padre New value of property asociadas_ot_padre.
     */
    public void setAsociadas_ot_padre(java.lang.String asociadas_ot_padre) {
        this.asociadas_ot_padre = asociadas_ot_padre;
    }
    
    /**
     * Getter for property tipo_entrega.
     * @return Value of property tipo_entrega.
     */
    public java.lang.String getTipo_entrega() {
        return tipo_entrega;
    }
    
    /**
     * Setter for property tipo_entrega.
     * @param tipo_entrega New value of property tipo_entrega.
     */
    public void setTipo_entrega(java.lang.String tipo_entrega) {
        this.tipo_entrega = tipo_entrega;
    }
    
    /**
     * Getter for property estado_ot.
     * @return Value of property estado_ot.
     */
    public java.lang.String getEstado_ot() {
        return estado_ot;
    }
    
    /**
     * Setter for property estado_ot.
     * @param estado_ot New value of property estado_ot.
     */
    public void setEstado_ot(java.lang.String estado_ot) {
        this.estado_ot = estado_ot;
    }
    
    /**
     * Getter for property fiduciaria.
     * @return Value of property fiduciaria.
     */
    public java.lang.String getFiduciaria() {
        return fiduciaria;
    }
    
    /**
     * Setter for property fiduciaria.
     * @param fiduciaria New value of property fiduciaria.
     */
    public void setFiduciaria(java.lang.String fiduciaria) {
        this.fiduciaria = fiduciaria;
    }
    
    //
    
}