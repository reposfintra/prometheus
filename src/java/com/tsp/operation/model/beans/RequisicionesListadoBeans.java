/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author Harold Cuello G.
 */
public class RequisicionesListadoBeans {

    public RequisicionesListadoBeans() {
    }
    
    //-------------------------------------------------------------
    private int id;
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    //-------------------------------------------------------------
    private String id_empresa;
    
    public void setIdEmpresa(String id_empresa) {
        this.id_empresa = id_empresa;
    }

    public String getIdEmpresa() {
        return id_empresa;
    }
    
    //-------------------------------------------------------------
    private String dsc_empresa;

    public void setDscEmpresa(String dsc_empresa) {
        this.dsc_empresa = dsc_empresa;
    }

    public String getDscEmpresa() {
        return dsc_empresa;
    }
    
    //-------------------------------------------------------------
    private int id_tipo_requisicion;

    public void setIdTipoRequisicion(int id_tipo_requisicion) {
        this.id_tipo_requisicion = id_tipo_requisicion;
    }

    public int getIdTipoRequisicion() {
        return id_tipo_requisicion;
    }
    
    //-------------------------------------------------------------
    private String dsc_tipo_requisicion;

    public void setDscTipoRequisicion(String dsc_tipo_requisicion) {
        this.dsc_tipo_requisicion = dsc_tipo_requisicion;
    }

    public String getsetDscTipoRequisicion() {
        return dsc_tipo_requisicion;
    }

    //-------------------------------------------------------------
    private int id_proceso_sgc;
    
    public void setIdProcesoSgc(int id_proceso_sgc) {
        this.id_proceso_sgc = id_proceso_sgc;
    }

    public int getIdProcesoSgc() {
        return id_proceso_sgc;
    }
    
    //-------------------------------------------------------------
    private String dsc_proceso_sgc;

    public void setDscProcesoSgc(String dsc_proceso_sgc) {
        this.dsc_proceso_sgc = dsc_proceso_sgc;
    }

    public String getDscProcesoSgc() {
        return dsc_proceso_sgc;
    }
    
    //-------------------------------------------------------------
    private String radicado;
    
    public void setRadicado(String radicado) {
        this.radicado = radicado;
    }

    public String getRadicado() {
        return radicado;
    }
    
    //-------------------------------------------------------------
    private String fch_radicacion;
    
    public void setFchRadicacion(String fch_radicacion) {
        this.fch_radicacion = fch_radicacion;
    }

    public String getFchRadicacion() {
        return fch_radicacion;
    }
    
    //-------------------------------------------------------------
    private String usuario;
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }
    
    //-------------------------------------------------------------
    private String asunto;
    
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getAsunto() {
        return asunto;
    }
    
    //-------------------------------------------------------------
    private String descripcion;
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    //-------------------------------------------------------------
    private int id_estado;
    
    public void setIdEstado(int id_estado) {
        this.id_estado = id_estado;
    }

    public int getIdEstado() {
        return id_estado;
    }
    
    //-------------------------------------------------------------
    private String estado;
    
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    //-------------------------------------------------------------
    private String acciones;
    
    public void setAcciones(String acciones) {
        this.acciones = acciones;
    }

    public String getAcciones() {
        return acciones;
    }
    
    //-------------------------------------------------------------
    private String fch_cierre;

    public void setFchCierre(String fch_cierre) {
        this.fch_cierre = fch_cierre;
    }

    public String getFchCierre() {
        return fch_cierre;
    }
    
    //-------------------------------------------------------------
    private String atiende;

    public void setAtiende(String atiende) {
        this.atiende = atiende;
    }

    public String getAtiende() {
        return atiende;
    }
    
    //-------------------------------------------------------------
    private int id_prioridad;

    public void setIdPrioridad(int id_prioridad) {
        this.id_prioridad = id_prioridad;
    }

    public int getIdPrioridad() {
        return id_prioridad;
    }
    
    //-------------------------------------------------------------
    private String dsc_prioridad;
    
    public void setDscPrioridad(String dsc_prioridad) {
        this.dsc_prioridad = dsc_prioridad;
    }

    public String getDscPrioridad() {
        return dsc_prioridad;
    }
    
    //-------------------------------------------------------------
    private int autorizado;
    
    public void setAutorizado(int autorizado) {
        this.autorizado = autorizado;
    }

    public int getAutorizado() {
        return autorizado;
    }
    
    //-------------------------------------------------------------
    private String autoriza;
    
    public void setAutoriza(String autoriza) {
        this.autoriza = autoriza;
    }

    public String getAutoriza() {
        return autoriza;
    }
    
    //-------------------------------------------------------------
    private int id_tipo_tarea;

    public void setIdTipoTarea(int id_tipo_tarea) {
        this.id_tipo_tarea = id_tipo_tarea;
    }

    public int getIdTipoTarea() {
        return id_tipo_tarea;
    }
    
    //-------------------------------------------------------------
    private String dsc_tipotarea;
    
    public void setDscTipoTarea(String dsc_tipotarea) {
        this.dsc_tipotarea = dsc_tipotarea;
    }

    public String getDscTipoTarea() {
        return dsc_tipotarea;
    }    

    //-------------------------------------------------------------
    private String fch_rqinicio;
    
    public void setRqFchInicio(String fch_rqinicio) {
        this.fch_rqinicio = fch_rqinicio;
    }

    public String getRqFchInicio() {
        return fch_rqinicio;
    }
    
    //-------------------------------------------------------------
    private String fch_rqfin;
    
    public void setRqFchFin(String fch_rqfin) {
        this.fch_rqfin = fch_rqfin;
    }

    public String getRqFchFin() {
        return fch_rqfin;
    }

    //-------------------------------------------------------------
    private String fch_inicioactividad;
    
    public void setRqInicioActividad(String fch_inicioactividad) {
        this.fch_inicioactividad = fch_inicioactividad;
    }

    public String getRqInicioActividad() {
        return fch_inicioactividad;
    }    
    
    
    //-------------------------------------------------------------
    private float horas_trabajo;

    public void setHorasTrabajo(float horas_trabajo) {
        this.horas_trabajo = horas_trabajo;
    }

    public float getHorasTrabajo() {
        return horas_trabajo;
    }
    
    //-------------------------------------------------------------
    private String moderador_por;
    
    public void setModeradoPor(String moderador_por) {
        this.moderador_por = moderador_por;
    }

    public String getModeradoPor() {
        return moderador_por;
    }     

    //-------------------------------------------------------------
    private int id_priorizacion;

    public void setIdPriorizacion(int id_priorizacion) {
        this.id_priorizacion = id_priorizacion;
    }

    public int getIdPriorizacion() {
        return id_priorizacion;
    }    
    
    private int id_task;
    
    public void setIdTask(int id_task) {
        this.id_task = id_task;
    }

    public int getIdTask() {
        return id_task;
    }
    
    //-------------------------------------------------------------
    private String empresa_task;
    
    public void setIdEmpresaTask(String empresa_task) {
        this.empresa_task = empresa_task;
    }

    public String getIdEmpresaTask() {
        return empresa_task;
    }

    //-------------------------------------------------------------
    private int idusuario_task;
    
    public void setIdUsuarioTask(int idusuario_task) {
        this.idusuario_task = idusuario_task;
    }

    public int getIdUsuarioTask() {
        return idusuario_task;
    }    
    
    //-------------------------------------------------------------
    private String usuario_task;
    
    public void setUsuarioTask(String idusuario_task) {
        this.usuario_task = usuario_task;
    }

    public String getUsuarioTask() {
        return usuario_task;
    }    

    //-------------------------------------------------------------
    private int idtipotarea_task;
    
    public void setIdTipoTask(int idtipotarea_task) {
        this.idtipotarea_task = idtipotarea_task;
    }

    public int getIdTipoTask() {
        return idtipotarea_task;
    }    
    
    //-------------------------------------------------------------
    private String tipo_task;
    
    public void setTipoTask(String tipo_task) {
        this.tipo_task = tipo_task;
    }

    public String getTipoTask() {
        return tipo_task;
    }    
    
    //-------------------------------------------------------------
    private String descripcion_task;
    
    public void setDescripcionTask(String descripcion_task) {
        this.descripcion_task = descripcion_task;
    }

    public String getDescripcionTask() {
        return descripcion_task;
    }    
    
    //-------------------------------------------------------------
    private String fch_inicio_task;
    
    public void setFchInicioTask(String fch_inicio_task) {
        this.fch_inicio_task = fch_inicio_task;
    }

    public String getFchInicioTask() {
        return fch_inicio_task;
    }
    
    //-------------------------------------------------------------
    private String fch_fin_task;
    
    public void setFchFinTask(String fch_fin_task) {
        this.fch_fin_task = fch_fin_task;
    }

    public String getFchFinTask() {
        return fch_fin_task;
    }    
    
    //-------------------------------------------------------------
    private String horas_estim_task;
    
    public void setHourTask(String horas_estim_task) {
        this.horas_estim_task = horas_estim_task;
    }

    public String getHourTask() {
        return horas_estim_task;
    }       
    
    //-------------------------------------------------------------
    private String fch_terminacion;
    
    public void setFchTerminacion(String fch_terminacion) {
        this.fch_terminacion = fch_terminacion;
    }

    public String getFchTerminacion() {
        return fch_terminacion;
    }      

    //-------------------------------------------------------------
    private String hora_reproceso_task;
    
    public void setHoraReprocesoTask(String hora_reproceso_task) {
        this.hora_reproceso_task = hora_reproceso_task;
    }

    public String getHoraReprocesoTask() {
        return hora_reproceso_task;
    }    
    
 
    //-------------------------------------------------------------
    private int estado_tarea_task;
    
    public void setIdEstadoTask(int estado_tarea_task) {
        this.estado_tarea_task = estado_tarea_task;
    }

    public int getIdEstadoTask() {
        return estado_tarea_task;
    }   
    
    //-------------------------------------------------------------
    private String estado_desctarea_task;
    
    public void setDescEstadoTask(String estado_desctarea_task) {
        this.estado_desctarea_task = estado_desctarea_task;
    }

    public String getDescEstadoTask() {
        return estado_desctarea_task;
    }
    
    private int cod_tarearq;
    
    public void setCodTaskRq(int cod_tarearq) {
        this.cod_tarearq = cod_tarearq;
    }

    public int getCodTaskRq() {
        return cod_tarearq;
    }      

    //-------------------------------------------------------------
    private int IndiceiEstadistica;
    
    public void setIndiceiEstadistica(int IndiceiEstadistica) {
        this.IndiceiEstadistica = IndiceiEstadistica;
    }
    
    public int getIndiceiEstadistica() {
        return IndiceiEstadistica;
    }

    private String DescYearEne;
    
    public void setDescYearEne(String DescYearEne) {
        this.DescYearEne = DescYearEne;
    }
    
    public String getDescYearEne() {
        return DescYearEne;
    }
    
    private String DescYearFeb;
    
    public void setDescYearFeb(String DescYearFeb) {
        this.DescYearFeb = DescYearFeb;
    }
    
    public String getDescYearFeb() {
        return DescYearFeb;
    }
    
    private String DescYearMar;
    
    public void setDescYearMar(String DescYearMar) {
        this.DescYearMar = DescYearMar;
    }
    
    public String getDescYearMar() {
        return DescYearMar;
    }
    
    private String DescYearAbr;
    
    public void setDescYearAbr(String DescYearAbr) {
        this.DescYearAbr = DescYearAbr;
    }
    
    public String getDescYearAbr() {
        return DescYearAbr;
    }
    
    private String DescYearMay;
    
    public void setDescYearMay(String DescYearMay) {
        this.DescYearMay = DescYearMay;
    }
    
    public String getDescYearMay() {
        return DescYearMay;
    }
    
    private String DescYearJun;
    
    public void setDescYearJun(String DescYearJun) {
        this.DescYearJun = DescYearJun;
    }
    
    public String getDescYearJun() {
        return DescYearJun;
    }
    
    private String DescYearJul;
    
    public void setDescYearJul(String DescYearJul) {
        this.DescYearJul = DescYearJul;
    }
    
    public String getDescYearJul() {
        return DescYearJul;
    }
    
    private String DescYearAgo;
    
    public void setDescYearAgo(String DescYearAgo) {
        this.DescYearAgo = DescYearAgo;
    }
    
    public String getDescYearAgo() {
        return DescYearAgo;
    }
    
    private String DescYearSep;
    
    public void setDescYearSep(String DescYearSep) {
        this.DescYearSep = DescYearSep;
    }
    
    public String getDescYearSep() {
        return DescYearSep;
    }
    
    private String DescYearOct;
    
    public void setDescYearOct(String DescYearOct) {
        this.DescYearOct = DescYearOct;
    }
    
    public String getDescYearOct() {
        return DescYearOct;
    }
    
    private String DescYearNov;
    
    public void setDescYearNov(String DescYearNov) {
        this.DescYearNov = DescYearNov;
    }
    
    public String getDescYearNov() {
        return DescYearNov;
    }
    
    private String DescYearDic;
    
    public void setDescYearDic(String DescYearDic) {
        this.DescYearDic = DescYearDic;
    }
    
    public String getDescYearDic() {
        return DescYearDic;
    }
    
    private String DescAcumulado;
    
    public void setDescAcumulado(String DescAcumulado) {
        this.DescAcumulado = DescAcumulado;
    }
    
    public String getDescAcumulado() {
        return DescAcumulado;
    }

    
    //-------------------------------------------------------------
    @Override
    public String toString() {
        return "RequisicionesListadoBeans{" + "id=" + id + ", id_empresa=" + id_empresa + ", dsc_empresa=" + dsc_empresa + ", id_tipo_requisicion=" + id_tipo_requisicion + ", dsc_tipo_requisicion=" + dsc_tipo_requisicion + ", id_proceso_sgc=" + id_proceso_sgc + ", dsc_proceso_sgc=" + dsc_proceso_sgc + ", radicado=" + radicado + ", fch_radicacion=" + fch_radicacion + ", usuario=" + usuario + ", asunto=" + asunto + ", descripcion=" + descripcion + ", id_estado=" + id_estado + ", estado=" + estado + ", acciones=" + acciones + ", fch_cierre=" + fch_cierre + ", atiende=" + atiende + ", id_prioridad=" + id_prioridad + ", dsc_prioridad=" + dsc_prioridad + ", autorizado=" + autorizado + ", autoriza=" + autoriza + ", id_tipo_tarea=" + id_tipo_tarea + ", dsc_tipotarea=" + dsc_tipotarea + ", fch_rqinicio=" + fch_rqinicio + ", fch_rqfin=" + fch_rqfin + ", fch_inicioactividad=" + fch_inicioactividad + ", horas_trabajo=" + horas_trabajo + ", moderador_por=" + moderador_por + ", id_priorizacion=" + id_priorizacion + ", id_task=" + id_task + ", empresa_task=" + empresa_task + ", idusuario_task=" + idusuario_task + ", usuario_task=" + usuario_task + ", idtipotarea_task=" + idtipotarea_task + ", tipo_task=" + tipo_task + ", descripcion_task=" + descripcion_task + ", fch_inicio_task=" + fch_inicio_task + ", fch_fin_task=" + fch_fin_task + ", horas_estim_task=" + horas_estim_task + ", fch_terminacion=" + fch_terminacion + ", hora_reproceso_task=" + hora_reproceso_task + ", estado_tarea_task=" + estado_tarea_task + ", estado_desctarea_task=" + estado_desctarea_task + ", cod_tarearq=" + cod_tarearq + '}';
    }
    
}
