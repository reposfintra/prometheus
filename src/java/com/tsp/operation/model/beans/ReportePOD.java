/**************************************************************************
 * Nombre clase: ReportePOD.java                               *
 * Descripci�n: Clase que maneja los GET y los SET del reporte POD    *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 2 de septiembre de 2006, 09:21 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/



package com.tsp.operation.model.beans;


public class ReportePOD {
    private String cliente;
    private String planilla;
    private String remesa;
    private String fechaRemesa;
    private String placa;
    private String ruta;
    private String destinatario;
    private String faccial;
    private String fechaLlegada;
    private String nota_entrega;
    private String num_viaje; 
    /** Creates a new instance of ReportePOD */
    public ReportePOD() {
      cliente      =" ";
      planilla     =" ";
      remesa       =" ";
      fechaRemesa  =" ";
      placa        =" ";
      ruta         =" ";
      destinatario =" ";
      faccial      =" ";
      fechaLlegada =" ";
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property destinatario.
     * @return Value of property destinatario.
     */
    public java.lang.String getDestinatario() {
        return destinatario;
    }
    
    /**
     * Setter for property destinatario.
     * @param destinatario New value of property destinatario.
     */
    public void setDestinatario(java.lang.String destinatario) {
        this.destinatario = destinatario;
    }
    
    /**
     * Getter for property faccial.
     * @return Value of property faccial.
     */
    public java.lang.String getFaccial() {
        return faccial;
    }
    
    /**
     * Setter for property faccial.
     * @param faccial New value of property faccial.
     */
    public void setFaccial(java.lang.String faccial) {
        this.faccial = faccial;
    }
    
    /**
     * Getter for property fechaLlegada.
     * @return Value of property fechaLlegada.
     */
    public java.lang.String getFechaLlegada() {
        return fechaLlegada;
    }
    
    /**
     * Setter for property fechaLlegada.
     * @param fechaLlegada New value of property fechaLlegada.
     */
    public void setFechaLlegada(java.lang.String fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }
    
    /**
     * Getter for property fechaRemesa.
     * @return Value of property fechaRemesa.
     */
    public java.lang.String getFechaRemesa() {
        return fechaRemesa;
    }
    
    /**
     * Setter for property fechaRemesa.
     * @param fechaRemesa New value of property fechaRemesa.
     */
    public void setFechaRemesa(java.lang.String fechaRemesa) {
        this.fechaRemesa = fechaRemesa;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property nota_entrega.
     * @return Value of property nota_entrega.
     */
    public java.lang.String getNota_entrega() {
        return nota_entrega;
    }
    
    /**
     * Setter for property nota_entrega.
     * @param nota_entrega New value of property nota_entrega.
     */
    public void setNota_entrega(java.lang.String nota_entrega) {
        this.nota_entrega = nota_entrega;
    }
    
    /**
     * Getter for property num_viaje.
     * @return Value of property num_viaje.
     */
    public java.lang.String getNum_viaje() {
        return num_viaje;
    }
    
    /**
     * Setter for property num_viaje.
     * @param num_viaje New value of property num_viaje.
     */
    public void setNum_viaje(java.lang.String num_viaje) {
        this.num_viaje = num_viaje;
    }
    
}
