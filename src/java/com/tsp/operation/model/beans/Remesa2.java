/*
 * Remesa2.java
 *
 * Created on 24 de abril de 2007, 11:17 AM
 */

package com.tsp.operation.model.beans;

import java.util.*;
import java.sql.*;
import com.tsp.util.Util;

public class Remesa2 implements Cloneable{

    private String starem = "";
    private String numrem = "";
    private String fecrem = "";
    private String fechacargue = "";
    private String agcrem = "";
    private String orirem = "";
    private String desrem = "";
    private String cliente = "";
    private String destinatario = "";
    private String unidcam = "";
    private String docuinterno = "";
    private String facturacial = "";
    private String tipoviaje = "";
    private String cia = "";
    private String lastupdate = "";
    private String usuario = "";
    private double vlrrem = 0;
    private double vlrrem2 = 0;
    private String remitente = "";
    private String ultreporte = "";
    private String observacion = "";
    private String demoras = "";
    private double pesoreal = 0;
    private String crossdocking = "";
    private String estado = "";
    private String unit_of_work = "";
    private String ot_padre = "";
    private String ot_rela = "";
    private String tieneplanilla = "";
    private String fecremori = "";
    private String plan_str_date = "";
    private String std_job_no = "";
    private String descripcion = "";
    private String reg_status = "";
    private String printer_date = "";
    private String derechos_cedido = "";
    private String remision = "";
    private String creation_date = "";
    private String base = "";
    private String codtipocarga = "";
    private double qty_value = 0;
    private double qty_packed = 0;
    private double qty_value_received = 0;
    private double qty_packed_received = 0;
    private String unit_packed = "";
    private String corte = "";
    private String currency = "";
    private String n_facturable = "";
    private String aduana = "";
    private String documento = "";
    private String transaccion = "";
    private String fecha_contabilizacion = "";
    private String tipo_doc_fac = "";
    private String doc_fac = "";
    private String periodo = "";
    private String cadena = "";
    private String cod_pagador = "";
    private String cmc = "";
    private String tiene_movimiento = "";
    
    
    
    private String nomori = "";
    private String nomdes = "";
    private String nomcli = "";
    private String nomagc = "";
    private String std_job_desc = "";

    private Vector remitentes;
    private Vector destinatarios;
    
    private TreeMap remitentes2;
    private TreeMap destinatarios2;
    
    
    private double tarifa = 0;
    private String cod_unidad = "";
    private String desc_unidad = "";
    private double tasa;    
    private String concepto = "";
    
    
    private double vlrrem_aj    = 0;
    private double vlrrem2_aj   = 0;
    private double pesoreal_aj  = 0;
    private double qty_value_aj = 0;
    private double tasa_aj      = 0;
    

    /** Creates a new instance of Remesa2 */
    public Remesa2() {
    }
    
    public Object clone() {
        try{
            return super.clone();
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }    
    
    public static Remesa2 load(ResultSet rs) throws Exception{
        Remesa2 r = new Remesa2();

        r.setStarem ( Util.coalesce( rs.getString("starem") ,"") );
        r.setNumrem ( Util.coalesce( rs.getString("numrem") ,"") );
        r.setFecrem ( Util.coalesce( rs.getString("fecrem") ,"") );
        r.setFechacargue ( Util.coalesce( rs.getString("fechacargue") ,"") );
        r.setAgcrem ( Util.coalesce( rs.getString("agcrem") ,"") );
        r.setOrirem ( Util.coalesce( rs.getString("orirem") ,"") );
        r.setDesrem ( Util.coalesce( rs.getString("desrem") ,"") );
        r.setCliente ( Util.coalesce( rs.getString("cliente") ,"") );
        r.setDestinatario ( Util.coalesce( rs.getString("destinatario") ,"") );
        r.setUnidcam ( Util.coalesce( rs.getString("unidcam") ,"") );
        r.setDocuinterno ( Util.coalesce( rs.getString("docuinterno") ,"") );
        r.setFacturacial ( Util.coalesce( rs.getString("facturacial") ,"") );
        r.setTipoviaje ( Util.coalesce( rs.getString("tipoviaje") ,"") );
        r.setCia ( Util.coalesce( rs.getString("cia") ,"") );
        r.setLastupdate ( Util.coalesce( rs.getString("lastupdate") ,"") );
        r.setUsuario ( Util.coalesce( rs.getString("usuario") ,"") );
        r.setVlrrem (  rs.getDouble("vlrrem")  );
        r.setVlrrem2 ( rs.getDouble("vlrrem2") );
        r.setRemitente ( Util.coalesce( rs.getString("remitente") ,"") );
        r.setUltreporte ( Util.coalesce( rs.getString("ultreporte") ,"") );
        r.setObservacion ( Util.coalesce( rs.getString("observacion") ,"") );
        r.setDemoras ( Util.coalesce( rs.getString("demoras") ,"") );
        r.setPesoreal (  rs.getDouble("pesoreal") );
        r.setCrossdocking ( Util.coalesce( rs.getString("crossdocking") ,"") );
        r.setEstado ( Util.coalesce( rs.getString("estado") ,"") );
        r.setUnit_of_work ( Util.coalesce( rs.getString("unit_of_work") ,"") );
        r.setOt_padre ( Util.coalesce( rs.getString("ot_padre") ,"") );
        r.setOt_rela ( Util.coalesce( rs.getString("ot_rela") ,"") );
        r.setTieneplanilla ( Util.coalesce( rs.getString("tieneplanilla") ,"") );
        r.setFecremori ( Util.coalesce( rs.getString("fecremori") ,"") );
        r.setPlan_str_date ( Util.coalesce( rs.getString("plan_str_date") ,"") );
        r.setStd_job_no ( Util.coalesce( rs.getString("std_job_no") ,"") );
        r.setDescripcion ( Util.coalesce( rs.getString("descripcion") ,"") );
        r.setReg_status ( Util.coalesce( rs.getString("reg_status") ,"") );
        r.setPrinter_date ( Util.coalesce( rs.getString("printer_date") ,"") );
        r.setDerechos_cedido ( Util.coalesce( rs.getString("derechos_cedido") ,"") );
        r.setRemision ( Util.coalesce( rs.getString("remision") ,"") );
        r.setCreation_date ( Util.coalesce( rs.getString("creation_date") ,"") );
        r.setBase ( Util.coalesce( rs.getString("base") ,"") );
        r.setCodtipocarga ( Util.coalesce( rs.getString("codtipocarga") ,"") );
        r.setQty_value ( rs.getDouble("qty_value")  );
        r.setQty_packed ( rs.getDouble("qty_packed")  );
        r.setQty_value_received ( rs.getDouble("qty_value_received") );
        r.setQty_packed_received ( rs.getDouble("qty_packed_received")  );
        r.setUnit_packed ( Util.coalesce( rs.getString("unit_packed") ,"") );
        r.setCorte ( Util.coalesce( rs.getString("corte") ,"") );
        r.setCurrency ( Util.coalesce( rs.getString("currency") ,"") );
        r.setN_facturable ( Util.coalesce( rs.getString("n_facturable") ,"") );
        r.setAduana ( Util.coalesce( rs.getString("aduana") ,"") );
        r.setDocumento ( Util.coalesce( rs.getString("documento") ,"") );
        r.setTransaccion ( Util.coalesce( rs.getString("transaccion") ,"") );
        r.setFecha_contabilizacion ( Util.coalesce( rs.getString("fecha_contabilizacion") ,"") );
        r.setTipo_doc_fac ( Util.coalesce( rs.getString("tipo_doc_fac") ,"") );
        r.setDoc_fac ( Util.coalesce( rs.getString("doc_fac") ,"") );
        r.setPeriodo ( Util.coalesce( rs.getString("periodo") ,"") );
        r.setCadena ( Util.coalesce( rs.getString("cadena") ,"") );
        r.setCod_pagador ( Util.coalesce( rs.getString("cod_pagador") ,"") );
        r.setCmc ( Util.coalesce( rs.getString("cmc") ,"") );
        //r.setTiene_movimiento ( Util.coalesce( rs.getString("tiene_movimiento") ,"") );
        r.setNomori           ( Util.coalesce( rs.getString("nomori") ,"") );
        r.setNomdes           ( Util.coalesce( rs.getString("nomdes") ,"") );
        r.setNomcli           ( Util.coalesce( rs.getString("nomcli") ,"") );
        r.setNomagc           ( Util.coalesce( rs.getString("nomagc") ,"") );
        r.setStd_job_desc     ( Util.coalesce( rs.getString("std_job_desc") ,"") );
        r.setTarifa           ( rs.getDouble("tarifa") );
        r.setCod_unidad       ( Util.coalesce( rs.getString("cod_unidad") ,"") );
        r.setDesc_unidad      ( Util.coalesce( rs.getString("desc_unidad") ,"") );
        r.setTasa             ( (r.getVlrrem()!=0? r.getVlrrem2() / r.getVlrrem() : 0 ) );
        return r;
    }
    
    /**
     * Getter for property aduana.
     * @return Value of property aduana.
     */
    public java.lang.String getAduana() {
        return aduana;
    }
    
    /**
     * Setter for property aduana.
     * @param aduana New value of property aduana.
     */
    public void setAduana(java.lang.String aduana) {
        this.aduana = aduana;
    }
    
    /**
     * Getter for property agcrem.
     * @return Value of property agcrem.
     */
    public java.lang.String getAgcrem() {
        return agcrem;
    }
    
    /**
     * Setter for property agcrem.
     * @param agcrem New value of property agcrem.
     */
    public void setAgcrem(java.lang.String agcrem) {
        this.agcrem = agcrem;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property cadena.
     * @return Value of property cadena.
     */
    public java.lang.String getCadena() {
        return cadena;
    }
    
    /**
     * Setter for property cadena.
     * @param cadena New value of property cadena.
     */
    public void setCadena(java.lang.String cadena) {
        this.cadena = cadena;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }
    
    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }
    
    /**
     * Getter for property cod_pagador.
     * @return Value of property cod_pagador.
     */
    public java.lang.String getCod_pagador() {
        return cod_pagador;
    }
    
    /**
     * Setter for property cod_pagador.
     * @param cod_pagador New value of property cod_pagador.
     */
    public void setCod_pagador(java.lang.String cod_pagador) {
        this.cod_pagador = cod_pagador;
    }
    
    /**
     * Getter for property codtipocarga.
     * @return Value of property codtipocarga.
     */
    public java.lang.String getCodtipocarga() {
        return codtipocarga;
    }
    
    /**
     * Setter for property codtipocarga.
     * @param codtipocarga New value of property codtipocarga.
     */
    public void setCodtipocarga(java.lang.String codtipocarga) {
        this.codtipocarga = codtipocarga;
    }
    
    /**
     * Getter for property corte.
     * @return Value of property corte.
     */
    public java.lang.String getCorte() {
        return corte;
    }
    
    /**
     * Setter for property corte.
     * @param corte New value of property corte.
     */
    public void setCorte(java.lang.String corte) {
        this.corte = corte;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property crossdocking.
     * @return Value of property crossdocking.
     */
    public java.lang.String getCrossdocking() {
        return crossdocking;
    }
    
    /**
     * Setter for property crossdocking.
     * @param crossdocking New value of property crossdocking.
     */
    public void setCrossdocking(java.lang.String crossdocking) {
        this.crossdocking = crossdocking;
    }
    
    /**
     * Getter for property currency.
     * @return Value of property currency.
     */
    public java.lang.String getCurrency() {
        return currency;
    }
    
    /**
     * Setter for property currency.
     * @param currency New value of property currency.
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }
    
    /**
     * Getter for property demoras.
     * @return Value of property demoras.
     */
    public java.lang.String getDemoras() {
        return demoras;
    }
    
    /**
     * Setter for property demoras.
     * @param demoras New value of property demoras.
     */
    public void setDemoras(java.lang.String demoras) {
        this.demoras = demoras;
    }
    
    /**
     * Getter for property derechos_cedido.
     * @return Value of property derechos_cedido.
     */
    public java.lang.String getDerechos_cedido() {
        return derechos_cedido;
    }
    
    /**
     * Setter for property derechos_cedido.
     * @param derechos_cedido New value of property derechos_cedido.
     */
    public void setDerechos_cedido(java.lang.String derechos_cedido) {
        this.derechos_cedido = derechos_cedido;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property desrem.
     * @return Value of property desrem.
     */
    public java.lang.String getDesrem() {
        return desrem;
    }
    
    /**
     * Setter for property desrem.
     * @param desrem New value of property desrem.
     */
    public void setDesrem(java.lang.String desrem) {
        this.desrem = desrem;
    }
    
    /**
     * Getter for property destinatario.
     * @return Value of property destinatario.
     */
    public java.lang.String getDestinatario() {
        return destinatario;
    }
    
    /**
     * Setter for property destinatario.
     * @param destinatario New value of property destinatario.
     */
    public void setDestinatario(java.lang.String destinatario) {
        this.destinatario = destinatario;
    }
    
    /**
     * Getter for property destinatarios.
     * @return Value of property destinatarios.
     */
    public java.util.Vector getDestinatarios() {
        return destinatarios;
    }
    
    /**
     * Setter for property destinatarios.
     * @param destinatarios New value of property destinatarios.
     */
    public void setDestinatarios(java.util.Vector destinatarios) {
        this.destinatarios = destinatarios;
    }
    
    /**
     * Getter for property doc_fac.
     * @return Value of property doc_fac.
     */
    public java.lang.String getDoc_fac() {
        return doc_fac;
    }
    
    /**
     * Setter for property doc_fac.
     * @param doc_fac New value of property doc_fac.
     */
    public void setDoc_fac(java.lang.String doc_fac) {
        this.doc_fac = doc_fac;
    }
    
    /**
     * Getter for property docuinterno.
     * @return Value of property docuinterno.
     */
    public java.lang.String getDocuinterno() {
        return docuinterno;
    }
    
    /**
     * Setter for property docuinterno.
     * @param docuinterno New value of property docuinterno.
     */
    public void setDocuinterno(java.lang.String docuinterno) {
        this.docuinterno = docuinterno;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property facturacial.
     * @return Value of property facturacial.
     */
    public java.lang.String getFacturacial() {
        return facturacial;
    }
    
    /**
     * Setter for property facturacial.
     * @param facturacial New value of property facturacial.
     */
    public void setFacturacial(java.lang.String facturacial) {
        this.facturacial = facturacial;
    }
    
    /**
     * Getter for property fecha_contabilizacion.
     * @return Value of property fecha_contabilizacion.
     */
    public java.lang.String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }
    
    /**
     * Setter for property fecha_contabilizacion.
     * @param fecha_contabilizacion New value of property fecha_contabilizacion.
     */
    public void setFecha_contabilizacion(java.lang.String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }
    
    /**
     * Getter for property fechacargue.
     * @return Value of property fechacargue.
     */
    public java.lang.String getFechacargue() {
        return fechacargue;
    }
    
    /**
     * Setter for property fechacargue.
     * @param fechacargue New value of property fechacargue.
     */
    public void setFechacargue(java.lang.String fechacargue) {
        this.fechacargue = fechacargue;
    }
    
    /**
     * Getter for property fecrem.
     * @return Value of property fecrem.
     */
    public java.lang.String getFecrem() {
        return fecrem;
    }
    
    /**
     * Setter for property fecrem.
     * @param fecrem New value of property fecrem.
     */
    public void setFecrem(java.lang.String fecrem) {
        this.fecrem = fecrem;
    }
    
    /**
     * Getter for property fecremori.
     * @return Value of property fecremori.
     */
    public java.lang.String getFecremori() {
        return fecremori;
    }
    
    /**
     * Setter for property fecremori.
     * @param fecremori New value of property fecremori.
     */
    public void setFecremori(java.lang.String fecremori) {
        this.fecremori = fecremori;
    }
    
    /**
     * Getter for property lastupdate.
     * @return Value of property lastupdate.
     */
    public java.lang.String getLastupdate() {
        return lastupdate;
    }
    
    /**
     * Setter for property lastupdate.
     * @param lastupdate New value of property lastupdate.
     */
    public void setLastupdate(java.lang.String lastupdate) {
        this.lastupdate = lastupdate;
    }
    
    /**
     * Getter for property n_facturable.
     * @return Value of property n_facturable.
     */
    public java.lang.String getN_facturable() {
        return n_facturable;
    }
    
    /**
     * Setter for property n_facturable.
     * @param n_facturable New value of property n_facturable.
     */
    public void setN_facturable(java.lang.String n_facturable) {
        this.n_facturable = n_facturable;
    }
    
    /**
     * Getter for property nomagc.
     * @return Value of property nomagc.
     */
    public java.lang.String getNomagc() {
        return nomagc;
    }
    
    /**
     * Setter for property nomagc.
     * @param nomagc New value of property nomagc.
     */
    public void setNomagc(java.lang.String nomagc) {
        this.nomagc = nomagc;
    }
    
    /**
     * Getter for property nomcli.
     * @return Value of property nomcli.
     */
    public java.lang.String getNomcli() {
        return nomcli;
    }
    
    /**
     * Setter for property nomcli.
     * @param nomcli New value of property nomcli.
     */
    public void setNomcli(java.lang.String nomcli) {
        this.nomcli = nomcli;
    }
    
    /**
     * Getter for property nomdes.
     * @return Value of property nomdes.
     */
    public java.lang.String getNomdes() {
        return nomdes;
    }
    
    /**
     * Setter for property nomdes.
     * @param nomdes New value of property nomdes.
     */
    public void setNomdes(java.lang.String nomdes) {
        this.nomdes = nomdes;
    }
    
    /**
     * Getter for property nomori.
     * @return Value of property nomori.
     */
    public java.lang.String getNomori() {
        return nomori;
    }
    
    /**
     * Setter for property nomori.
     * @param nomori New value of property nomori.
     */
    public void setNomori(java.lang.String nomori) {
        this.nomori = nomori;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property orirem.
     * @return Value of property orirem.
     */
    public java.lang.String getOrirem() {
        return orirem;
    }
    
    /**
     * Setter for property orirem.
     * @param orirem New value of property orirem.
     */
    public void setOrirem(java.lang.String orirem) {
        this.orirem = orirem;
    }
    
    /**
     * Getter for property ot_padre.
     * @return Value of property ot_padre.
     */
    public java.lang.String getOt_padre() {
        return ot_padre;
    }
    
    /**
     * Setter for property ot_padre.
     * @param ot_padre New value of property ot_padre.
     */
    public void setOt_padre(java.lang.String ot_padre) {
        this.ot_padre = ot_padre;
    }
    
    /**
     * Getter for property ot_rela.
     * @return Value of property ot_rela.
     */
    public java.lang.String getOt_rela() {
        return ot_rela;
    }
    
    /**
     * Setter for property ot_rela.
     * @param ot_rela New value of property ot_rela.
     */
    public void setOt_rela(java.lang.String ot_rela) {
        this.ot_rela = ot_rela;
    }
    
    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }
    
    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }
    
    /**
     * Getter for property pesoreal.
     * @return Value of property pesoreal.
     */
    public double getPesoreal() {
        return pesoreal;
    }
    
    /**
     * Setter for property pesoreal.
     * @param pesoreal New value of property pesoreal.
     */
    public void setPesoreal(double pesoreal) {
        this.pesoreal = pesoreal;
    }
    
    /**
     * Getter for property plan_str_date.
     * @return Value of property plan_str_date.
     */
    public java.lang.String getPlan_str_date() {
        return plan_str_date;
    }
    
    /**
     * Setter for property plan_str_date.
     * @param plan_str_date New value of property plan_str_date.
     */
    public void setPlan_str_date(java.lang.String plan_str_date) {
        this.plan_str_date = plan_str_date;
    }
    
    /**
     * Getter for property printer_date.
     * @return Value of property printer_date.
     */
    public java.lang.String getPrinter_date() {
        return printer_date;
    }
    
    /**
     * Setter for property printer_date.
     * @param printer_date New value of property printer_date.
     */
    public void setPrinter_date(java.lang.String printer_date) {
        this.printer_date = printer_date;
    }
    
    /**
     * Getter for property qty_packed.
     * @return Value of property qty_packed.
     */
    public double getQty_packed() {
        return qty_packed;
    }
    
    /**
     * Setter for property qty_packed.
     * @param qty_packed New value of property qty_packed.
     */
    public void setQty_packed(double qty_packed) {
        this.qty_packed = qty_packed;
    }
    
    /**
     * Getter for property qty_packed_received.
     * @return Value of property qty_packed_received.
     */
    public double getQty_packed_received() {
        return qty_packed_received;
    }
    
    /**
     * Setter for property qty_packed_received.
     * @param qty_packed_received New value of property qty_packed_received.
     */
    public void setQty_packed_received(double qty_packed_received) {
        this.qty_packed_received = qty_packed_received;
    }
    
    /**
     * Getter for property qty_value.
     * @return Value of property qty_value.
     */
    public double getQty_value() {
        return qty_value;
    }
    
    /**
     * Setter for property qty_value.
     * @param qty_value New value of property qty_value.
     */
    public void setQty_value(double qty_value) {
        this.qty_value = qty_value;
    }
    
    /**
     * Getter for property qty_value_received.
     * @return Value of property qty_value_received.
     */
    public double getQty_value_received() {
        return qty_value_received;
    }
    
    /**
     * Setter for property qty_value_received.
     * @param qty_value_received New value of property qty_value_received.
     */
    public void setQty_value_received(double qty_value_received) {
        this.qty_value_received = qty_value_received;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
    /**
     * Getter for property remitente.
     * @return Value of property remitente.
     */
    public java.lang.String getRemitente() {
        return remitente;
    }
    
    /**
     * Setter for property remitente.
     * @param remitente New value of property remitente.
     */
    public void setRemitente(java.lang.String remitente) {
        this.remitente = remitente;
    }
    
    /**
     * Getter for property remitentes.
     * @return Value of property remitentes.
     */
    public java.util.Vector getRemitentes() {
        return remitentes;
    }
    
    /**
     * Setter for property remitentes.
     * @param remitentes New value of property remitentes.
     */
    public void setRemitentes(java.util.Vector remitentes) {
        this.remitentes = remitentes;
    }
    
    /**
     * Getter for property starem.
     * @return Value of property starem.
     */
    public java.lang.String getStarem() {
        return starem;
    }
    
    /**
     * Setter for property starem.
     * @param starem New value of property starem.
     */
    public void setStarem(java.lang.String starem) {
        this.starem = starem;
    }
    
    /**
     * Getter for property std_job_desc.
     * @return Value of property std_job_desc.
     */
    public java.lang.String getStd_job_desc() {
        return std_job_desc;
    }
    
    /**
     * Setter for property std_job_desc.
     * @param std_job_desc New value of property std_job_desc.
     */
    public void setStd_job_desc(java.lang.String std_job_desc) {
        this.std_job_desc = std_job_desc;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property tiene_movimiento.
     * @return Value of property tiene_movimiento.
     */
    public java.lang.String getTiene_movimiento() {
        return tiene_movimiento;
    }
    
    /**
     * Setter for property tiene_movimiento.
     * @param tiene_movimiento New value of property tiene_movimiento.
     */
    public void setTiene_movimiento(java.lang.String tiene_movimiento) {
        this.tiene_movimiento = tiene_movimiento;
    }
    
    /**
     * Getter for property tieneplanilla.
     * @return Value of property tieneplanilla.
     */
    public java.lang.String getTieneplanilla() {
        return tieneplanilla;
    }
    
    /**
     * Setter for property tieneplanilla.
     * @param tieneplanilla New value of property tieneplanilla.
     */
    public void setTieneplanilla(java.lang.String tieneplanilla) {
        this.tieneplanilla = tieneplanilla;
    }
    
    /**
     * Getter for property tipo_doc_fac.
     * @return Value of property tipo_doc_fac.
     */
    public java.lang.String getTipo_doc_fac() {
        return tipo_doc_fac;
    }
    
    /**
     * Setter for property tipo_doc_fac.
     * @param tipo_doc_fac New value of property tipo_doc_fac.
     */
    public void setTipo_doc_fac(java.lang.String tipo_doc_fac) {
        this.tipo_doc_fac = tipo_doc_fac;
    }
    
    /**
     * Getter for property tipoviaje.
     * @return Value of property tipoviaje.
     */
    public java.lang.String getTipoviaje() {
        return tipoviaje;
    }
    
    /**
     * Setter for property tipoviaje.
     * @param tipoviaje New value of property tipoviaje.
     */
    public void setTipoviaje(java.lang.String tipoviaje) {
        this.tipoviaje = tipoviaje;
    }
    
    /**
     * Getter for property transaccion.
     * @return Value of property transaccion.
     */
    public java.lang.String getTransaccion() {
        return transaccion;
    }
    
    /**
     * Setter for property transaccion.
     * @param transaccion New value of property transaccion.
     */
    public void setTransaccion(java.lang.String transaccion) {
        this.transaccion = transaccion;
    }
    
    /**
     * Getter for property ultreporte.
     * @return Value of property ultreporte.
     */
    public java.lang.String getUltreporte() {
        return ultreporte;
    }
    
    /**
     * Setter for property ultreporte.
     * @param ultreporte New value of property ultreporte.
     */
    public void setUltreporte(java.lang.String ultreporte) {
        this.ultreporte = ultreporte;
    }
    
    /**
     * Getter for property unidcam.
     * @return Value of property unidcam.
     */
    public java.lang.String getUnidcam() {
        return unidcam;
    }
    
    /**
     * Setter for property unidcam.
     * @param unidcam New value of property unidcam.
     */
    public void setUnidcam(java.lang.String unidcam) {
        this.unidcam = unidcam;
    }
    
    /**
     * Getter for property unit_of_work.
     * @return Value of property unit_of_work.
     */
    public java.lang.String getUnit_of_work() {
        return unit_of_work;
    }
    
    /**
     * Setter for property unit_of_work.
     * @param unit_of_work New value of property unit_of_work.
     */
    public void setUnit_of_work(java.lang.String unit_of_work) {
        this.unit_of_work = unit_of_work;
    }
    
    /**
     * Getter for property unit_packed.
     * @return Value of property unit_packed.
     */
    public java.lang.String getUnit_packed() {
        return unit_packed;
    }
    
    /**
     * Setter for property unit_packed.
     * @param unit_packed New value of property unit_packed.
     */
    public void setUnit_packed(java.lang.String unit_packed) {
        this.unit_packed = unit_packed;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property vlrrem.
     * @return Value of property vlrrem.
     */
    public double getVlrrem() {
        return vlrrem;
    }
    
    /**
     * Setter for property vlrrem.
     * @param vlrrem New value of property vlrrem.
     */
    public void setVlrrem(double vlrrem) {
        this.vlrrem = vlrrem;
    }
    
    /**
     * Getter for property vlrrem2.
     * @return Value of property vlrrem2.
     */
    public double getVlrrem2() {
        return vlrrem2;
    }
    
    /**
     * Setter for property vlrrem2.
     * @param vlrrem2 New value of property vlrrem2.
     */
    public void setVlrrem2(double vlrrem2) {
        this.vlrrem2 = vlrrem2;
    }
    
    /**
     * Getter for property tarifa.
     * @return Value of property tarifa.
     */
    public double getTarifa() {
        return tarifa;
    }
    
    /**
     * Setter for property tarifa.
     * @param tarifa New value of property tarifa.
     */
    public void setTarifa(double tarifa) {
        this.tarifa = tarifa;
    }
    
    /**
     * Getter for property cod_unidad.
     * @return Value of property cod_unidad.
     */
    public java.lang.String getCod_unidad() {
        return cod_unidad;
    }
    
    /**
     * Setter for property cod_unidad.
     * @param cod_unidad New value of property cod_unidad.
     */
    public void setCod_unidad(java.lang.String cod_unidad) {
        this.cod_unidad = cod_unidad;
    }
    
    /**
     * Getter for property desc_unidad.
     * @return Value of property desc_unidad.
     */
    public java.lang.String getDesc_unidad() {
        return desc_unidad;
    }
    
    /**
     * Setter for property desc_unidad.
     * @param desc_unidad New value of property desc_unidad.
     */
    public void setDesc_unidad(java.lang.String desc_unidad) {
        this.desc_unidad = desc_unidad;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property vlrrem_aj.
     * @return Value of property vlrrem_aj.
     */
    public double getVlrrem_aj() {
        return vlrrem_aj;
    }
    
    /**
     * Setter for property vlrrem_aj.
     * @param vlrrem_aj New value of property vlrrem_aj.
     */
    public void setVlrrem_aj(double vlrrem_aj) {
        this.vlrrem_aj = vlrrem_aj;
    }
    
    /**
     * Getter for property vlrrem2_aj.
     * @return Value of property vlrrem2_aj.
     */
    public double getVlrrem2_aj() {
        return vlrrem2_aj;
    }
    
    /**
     * Setter for property vlrrem2_aj.
     * @param vlrrem2_aj New value of property vlrrem2_aj.
     */
    public void setVlrrem2_aj(double vlrrem2_aj) {
        this.vlrrem2_aj = vlrrem2_aj;
    }
    
    /**
     * Getter for property pesoreal_aj.
     * @return Value of property pesoreal_aj.
     */
    public double getPesoreal_aj() {
        return pesoreal_aj;
    }
    
    /**
     * Setter for property pesoreal_aj.
     * @param pesoreal_aj New value of property pesoreal_aj.
     */
    public void setPesoreal_aj(double pesoreal_aj) {
        this.pesoreal_aj = pesoreal_aj;
    }
    
    /**
     * Getter for property qty_value_aj.
     * @return Value of property qty_value_aj.
     */
    public double getQty_value_aj() {
        return qty_value_aj;
    }
    
    /**
     * Setter for property qty_value_aj.
     * @param qty_value_aj New value of property qty_value_aj.
     */
    public void setQty_value_aj(double qty_value_aj) {
        this.qty_value_aj = qty_value_aj;
    }
    
    /**
     * Getter for property tasa_aj.
     * @return Value of property tasa_aj.
     */
    public double getTasa_aj() {
        return tasa_aj;
    }
    
    /**
     * Setter for property tasa_aj.
     * @param tasa_aj New value of property tasa_aj.
     */
    public void setTasa_aj(double tasa_aj) {
        this.tasa_aj = tasa_aj;
    }
    
    /**
     * Getter for property remitentes2.
     * @return Value of property remitentes2.
     */
    public java.util.TreeMap getRemitentes2() {
        return remitentes2;
    }
    
    /**
     * Setter for property remitentes2.
     * @param remitentes2 New value of property remitentes2.
     */
    public void setRemitentes2(java.util.TreeMap remitentes2) {
        this.remitentes2 = remitentes2;
    }
    
    /**
     * Getter for property destinatarios2.
     * @return Value of property destinatarios2.
     */
    public java.util.TreeMap getDestinatarios2() {
        return destinatarios2;
    }
    
    /**
     * Setter for property destinatarios2.
     * @param destinatarios2 New value of property destinatarios2.
     */
    public void setDestinatarios2(java.util.TreeMap destinatarios2) {
        this.destinatarios2 = destinatarios2;
    }
    
}
