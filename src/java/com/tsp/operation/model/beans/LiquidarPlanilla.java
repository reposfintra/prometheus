   /**********************************************************************
    * Nombre Clase ............. LiquidarPlanilla.java
    * Descripci�n  .. . . . . .  Permite mantener valores de la liquidaci�n de la planilla
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/04/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *********************************************************************/


package com.tsp.operation.model.beans;


import  java.util.*;
import  com.tsp.operation.model.DAOS.LiquidacionDAO;




public class LiquidarPlanilla {
    
    
    
    /* _______________________________________________________________________________________________________
     *  ATRIBUTOS :
     * _______________________________________________________________________________________________________ */
    
    
    private  String            distrito;                // Distrito de la planilla
    private  String            planilla;                // La planilla a Liquidar
    private  List              movimientos;             // Lista de Movimientos de la planilla  (movpla)  de tipo  ItemLiquidacion.
    private  double            saldo;                   // Valor del saldo  de la liquidacion
    private  double            vlrMaximoReanticipos;    // Valor maximo de reanticipos a entregar
    private  String            moneda;                  // La moneda, default PESO
    private  double            valorPlanilla;           // Valor de la planilla, utilizado para calcular vlrMaximoReanticipos
    
    
    private  String            tipoMoneda;              /*  Determina a que moneda se realiza la conversi�n
                                                         *   MPL  :  Moneda de la planilla
                                                         *   MBA  :  Moneda Banco
                                                         *   MPE  :  Moneda Peso
                                                         *   default : MPE
                                                         */
    
    
    private  double            valorTotalPlanilla;      // Valor Total de la planilla = Vlr planilla + Ajustes
    private  double            valorMaxAnticipo;        // Valor maximo de Anticipos
    private  double            pocentaje;               // Porcentaje de Anticipos
    
    
    private  LiquidacionDAO    LiquidacionDataAccess;   // Permite realizar las consultas a la base de datos.
    
    
    
     private String  VENEZUELA_PLACA  = "VE";
     private String  BOLIVAR          = "BOL";
    
    
    /* _______________________________________________________________________________________________________
     *  M�TODOS :
     * _______________________________________________________________________________________________________ */
    
    
    
    
    
    public LiquidarPlanilla() {
        LiquidacionDataAccess  = new LiquidacionDAO();
        reset();
    }
    
    
    
    
    /* M�todos que setea vacio los atributos del objeto
     * @autor.......fvillacob   
     * @version.....1.0.     
     **/ 
    private void reset(){
        planilla             = "";
        distrito             = "";        
        saldo                = 0;
        vlrMaximoReanticipos = 0;
        valorPlanilla        = 0;        
        valorTotalPlanilla   = 0; 
        valorMaxAnticipo     = 0;
        pocentaje            = 0; 
        moneda               = "PES";
        tipoMoneda           = "MPE";
        movimientos          = new LinkedList();
    }
    
    
    
    
    
    /* M�todos que ejecuta el proceso de liquidar la planilla
     * @autor         fvillacob  
     * @parameter     String dstrct, String oc, String codigoTipoMoneda      
     * @throws        Exception
     * @version       1.0.     
     **/ 
    public void Liquidar(String dstrct, String oc, String codigoTipoMoneda)throws Exception{
        reset();
        try{
            this.planilla   = oc;
            this.distrito   = dstrct;
            this.tipoMoneda = codigoTipoMoneda;
            
            Hashtable  datosPlanilla = this.LiquidacionDataAccess.getDatosPlanilla( distrito, planilla );
            
            if ( datosPlanilla!=null  ){  
                
                
                
                // DATOS :     

                //--- 1. Datos de la planilla
                        String  origen                  = (String) datosPlanilla.get("origen");         
                        String  propietario             = (String) datosPlanilla.get("propietario");
                        String  monedaPlanilla          = (String) datosPlanilla.get("moneda");
                        ItemLiquidacion itemPlanilla    =  formarItemPlanilla ( datosPlanilla );


                //--- 2. Datos agencia relacionada para el calculo de retefuente.  
                        Hashtable  agenciaRelacionada  = this.LiquidacionDataAccess.getAgenciaRelacionada(  origen  );
                        String     codigoAgencia       = resetNull( (String) agenciaRelacionada.get("agencia") );
                        String     codigoRica          = resetNull( (String) agenciaRelacionada.get("rica") ); 


                //--- 3 Datos propietario, para aplicar los impuestos:
                        Hashtable  datosPropietario    = this.LiquidacionDataAccess.getDatosPropietario( distrito, propietario );
                        String     aplicaRFTE          = resetNull( (String) datosPropietario.get("retefuente") );
                        String     aplicaRICA          = resetNull( (String) datosPropietario.get("reteica") );
                        String     agenteRetenedor     = resetNull( (String) datosPropietario.get("agente_retenedor") );                
                        String     bancoPropietario    = resetNull( (String) datosPropietario.get("banco") );
                        String     sucursalBanco       = resetNull( (String) datosPropietario.get("sucursal") );
                        String     monedaPropietario   =  this.LiquidacionDataAccess.getMonedaBanco( distrito , bancoPropietario, sucursalBanco );




               // MOVIMIENTOS :                 

               //--- 1. Formamos la lista:
                        
                        String std        =  this.LiquidacionDataAccess.getStandar( this.distrito, this.planilla  );  // Seleccionamos el standar de la planilla                        
                        
                        List  listMovpla  =  this.LiquidacionDataAccess.getMovimientos( distrito, planilla );         // lista de movimientos de movpla                        
                        List  listTbldes  =  this.LiquidacionDataAccess.agregarDescuentos_Carbon( std, distrito );    // lista de movimientos de Tbldes dependiendo si el estandar es de carbon                        
                        List  items       =  new LinkedList();
                              items.add( itemPlanilla  );                                                             // adicionamos el item de la planilla
                              items.addAll( listMovpla );                                                             // adicionamos mov de  movpla
                              items.addAll( listTbldes );                                                             // adicionamos mov de Tbldes



              //--- 2. Determinamos a que Moneda realizamos la conversi�n dependiendo el tipo seleccionado:
                       String peso             = "PES";
                       this.moneda             = obtenerMonedaSeleccionada( codigoTipoMoneda, monedaPlanilla, monedaPropietario,  peso  ) ;


             //--- 3. cambio de moneda: pasamos a la moneda seleccionada
                       items =  cambiarMoneda(items, this.moneda  );


             //--- 4. calculamos el valor para los item de indicador tipo porcentaje:
                      items  = calcularPorcentajeItems(items);

                      
                      
             //--- 5. Agrupamos items:
                      items = agruparItem(items);
                      
                      

             //--- 6. aplicamos impuestos: 
                    
                String             placa       =  (String) datosPlanilla.get("placa"); 
                String             pais_placa  =  (String) datosPlanilla.get("pais_placa");
               
                
                if(  !pais_placa.equals( VENEZUELA_PLACA )  && !monedaPropietario.equals( BOLIVAR )     )   // Restringir impuesto Venezuela: cuando pais placa sea venezuela y banco venezuela, No aplicar Descuento.                 
                     if( ! agenteRetenedor.toUpperCase().equals("S") ){                   //  Si NO Es agente Retenedor

                         String tipoImpRFTE   = "RFTE";
                         String codigoImpRFTE = "RT01";
                         String tipoImpRICA   = "RICA";
                         
                      // Buscamos el Equivalente del Rica: 
                         String nuevoCodeRica =  this.LiquidacionDataAccess.codigoImp_tablaGen(codigoRica);

                         Hashtable IMP_RFTE   =  this.LiquidacionDataAccess.getImpuesto( this.distrito, codigoImpRFTE,    tipoImpRFTE );
                         Hashtable IMP_RICA   =  this.LiquidacionDataAccess.getImpuesto( this.distrito, nuevoCodeRica,    tipoImpRICA );


                         for(int i=0;i<items.size();i++){
                              ItemLiquidacion item = (ItemLiquidacion)items.get(i);

                              if(  item.getAsignador().toUpperCase().equals("V")  ){   // Si aplica  a valor

                                   if (  aplicaRFTE.toUpperCase().equals("S")  ){      // Retefuente
                                         item.impuestos.put("RFTE", IMP_RFTE  );
                                         double vlr  =  ( item.getValor()  * Double.parseDouble( resetValorNull( (String)IMP_RFTE.get("porcentaje")) ) ) /100;
                                         item.setReteFuente( vlr );
                                   }

                                   if ( aplicaRICA.toUpperCase().equals("S") ){       // ReteIca
                                        item.impuestos.put("RICA", IMP_RICA  );
                                        double vlr  =  ( item.getValor()  * Double.parseDouble( resetValorNull( (String)IMP_RICA.get("porcentaje")) )  ) /100;
                                        item.setReteIca( vlr );
                                   }

                              }

                         }

                     }


           //--- 7. Calculamos valores :                 
                     this.movimientos = items;         // Asignamos la lista ya procesada y calculada de los movimientos         
                     setearValorPlanilla();
                     calcularSaldo();
                     calcularValorMaximoReanticipos(std);

            }
            
            
            
            
        }catch(Exception e){
            throw new Exception( " LiquidarPlanilla -> Error en liquidar la planilla " + oc +" :" + e.getMessage());
        }
    }
    
    
    
    
    
    
    
    
    /* M�todos que permite obtener el tipo de moneda segun el parametro enviado
     * @autor         fvillacob  
     * @parameter     String tipo, String monedaPlanilla, String monedaBanco, String peso
     * @version       1.0.     
     **/ 
    private String obtenerMonedaSeleccionada(String tipo, String monedaPlanilla, String monedaBanco, String peso){
         String money = this.moneda;
           if(tipo.equals("MPL"))    money = monedaPlanilla;
           if(tipo.equals("MBA"))    money = monedaBanco;
           if(tipo.equals("MPE"))    money = peso;
         return  money;
    }
    
    
    
    
    
    /* M�todos que permite cambiar valores a la moneda seleccionada
     * @autor         fvillacob  
     * @parameter     List items, String monedaConversi�n 
     * @version       1.0.     
     **/ 
    private List cambiarMoneda(List items, String monedaConversi�n)throws Exception{
        try{        
                for(int i=0;i<items.size();i++){
                      ItemLiquidacion item = (ItemLiquidacion)items.get(i);
                      if( item.getIndicador().toUpperCase().equals("P")  ||  item.getMoneda().trim().equals("") )
                          item.setMoneda( monedaConversi�n  );
                      else if ( ! item.getMoneda().equals(  monedaConversi�n  )   ){
                            double newVlr = convertirMoneda( this.distrito, item.getMoneda(), item.getValor(),  monedaConversi�n  ); 
                            item.setValor ( newVlr );
                            item.setMoneda( monedaConversi�n );
                      }
                 } 
        }catch(Exception e){
            throw new Exception( " cambiarMoneda " + e.getMessage());
        }
        return items;
    }
    
    
    
    
    
    
    /* M�todos que permite formar el  item de la lista de movimientos a partir de los datos de la planilla
     * @autor         fvillacob  
     * @parameter     Hashtable  datosPlanilla 
     * @version       1.0.     
     **/ 
    private ItemLiquidacion  formarItemPlanilla(Hashtable  datosPlanilla) throws Exception{
        ItemLiquidacion  item = new ItemLiquidacion();
        try{
        
           String  moneda      =  (String) datosPlanilla.get("moneda"); 
           String  base        =  (String) datosPlanilla.get("base"); 
           String  estado      =  (String) datosPlanilla.get("estado"); 
           
           
      // 1. Obtenemos la cantidad cumplida  de la planilla segun la base
           double cantidad    =  1;           
           if (!base.toUpperCase().equals("COL"))
                cantidad = this.LiquidacionDataAccess.getCantidadCumplidaCarbon(distrito, planilla);
           else
                cantidad = this.LiquidacionDataAccess.getCantidadCumplida( distrito, planilla );
           
           
       // 2. Define el valor de la OC, si esta cumplida se recalcula asi vlr = valor Unitario * cantidad
           double  vlrPlanilla  =  Double.parseDouble( (String)datosPlanilla.get("valor") );
           double  vlrUnitario  =  Double.parseDouble( (String)datosPlanilla.get("valorUnitario") );
           if(estado.toUpperCase().equals("C")){
                 vlrPlanilla  =  vlrUnitario * cantidad;
           }  
           
           
      // 3. Formamos el item de la planilla:     
           item.setDescripcion("TRANSPORTE PLANILLA");
           item.setConcepto   ("00");
           item.setValor      ( vlrPlanilla );
           item.setMoneda     ( moneda  );
           item.setIndicador  ( "V" );
           item.setAsignador  ( "V" );
           
        }catch(Exception e){
            throw new Exception( " formarItemPlanilla "+ e.getMessage() );
        }
        return item;
    }
    
    
    
    
    
    
    /**
     * M�todo que permite agrupar los items de movimientos por concept_code
     * @autor    fvillacob
     * @param    List lista (lista de movimientos )
     * @throws   Exception
     * @version  1.0.  
     **/
    private List agruparItem(List lista)throws Exception{
        List aux = new LinkedList();
        try{
            
            List listConceptos =   buscarDistintosConcepto( lista );
            
            for(int i=0;i<listConceptos.size();i++){
                ItemLiquidacion  item   = (ItemLiquidacion)listConceptos.get(i);
                double           valor  = sumarValores( lista, item.getConcepto()  );
                
             // Si el valor del acumulado es diferente de cero lo adicionamos a la lista final
                if ( valor != 0 ){
                     item.setValor   ( valor );
                     aux.add( item );
                }                
            }
            
        }catch(Exception e){
            throw new Exception( " agruparItem "+ e.getMessage() );
        }        
        return aux;
    }
    
    
    
    
    
     /**
     * M�todo buscarDistintosConcepto, metodo que retorna una lista con los distintos conceptos del item 
     * @param     List lista de Movimientos
     * @throws    Exception
     * @version   1.0.
     **/ 
    private List buscarDistintosConcepto(List lista)throws Exception{
        List listConcepto = new LinkedList();        
        try{
            for(int i=0;i<lista.size();i++){
                ItemLiquidacion item = (ItemLiquidacion)lista.get(i);
                int sw = 0;
                
                for(int j=0;j<listConcepto.size();j++){
                    ItemLiquidacion copy = (ItemLiquidacion)listConcepto.get(j);
                    if( copy.getConcepto().equals( item.getConcepto()  )){
                        sw = 1;
                        break;
                    }
                }
                
                if(sw==0)
                   listConcepto.add(item);                
            }
        }catch(Exception e){
            throw new Exception( " buscarDistintosConcepto "+ e.getMessage() );
        } 
        return listConcepto;
    }
    
    
    
    
    /**
     * M�todo que suma valores del mismo concepto 
     * @param     List lista de Movimientos
     * @throws    Exception
     * @version   1.0.
     **/ 
    private double sumarValores(List lista, String concepto )throws Exception{
        double     vlr     = 0;
        try{            
            for(int i=0;i<lista.size();i++){
                ItemLiquidacion item = (ItemLiquidacion)lista.get(i);
                if ( item.getConcepto().equals( concepto )  )
                     vlr      += item.getValor();
            }            
        }catch(Exception e){
            throw new Exception( " sumarValores "+ e.getMessage() );
        } 
        return vlr;
    }
    
    
    
    
    
    
    /**
     * M�todo calcularPorcentajeItems, calcula el valor del movimiento de planilla Indicador P con respecto a la 
       sumatoria de los movimientos Asigando V
     * @autor    fvillacob
     * @param    List lista (lista de movimientos )
     * @throws   Exception
     * @version  1.0.  
     **/
    private List calcularPorcentajeItems(List lista) throws Exception{
        try{
            
            if( lista !=null  && lista.size()>0 ){
                
              //1. Sumatoria de los items ASIGNADOR = V
                double vlrocTotal  = 0;
                for(int i=0;i<lista.size();i++){
                    ItemLiquidacion  item = (ItemLiquidacion) lista.get(i);
                    if( item.getAsignador().toUpperCase().equals("V") )
                        vlrocTotal  += item.getValor();
                }
                
              // 2. calculamos valor item indicador P  = ( sumatoria items asignador V * valor item P ) /100
                for(int i=0;i<lista.size();i++){
                    ItemLiquidacion  item = (ItemLiquidacion) lista.get(i);
                    if( item.getIndicador().toUpperCase().equals("P") )
                        item.setValor( ( vlrocTotal  * item.getValor() )/100 );
                }
            }
        }catch(Exception e){
            throw new Exception( " calcularPorcentajeItems "+ e.getMessage());
        }
        return lista;
    }
    
    
    
    
    
    /* M�todos que calcula el valor del saldo de la liquidacion
     * @autor         fvillacob       
     * @throws        Exception
     * @version       1.0.     
     **/ 
    private void calcularSaldo()throws Exception{
        double  vlr =0;
        try{
            for(int i=0;i<this.movimientos.size();i++){
                  ItemLiquidacion  item = (ItemLiquidacion) this.movimientos.get(i);
                  item.setSaldo(  item.getValor()  - item.getReteFuente()  - item.getReteIca()  );
                  vlr += item.getSaldo();
            }
        }catch(Exception e){
            throw new Exception( " calcularSaldo "+ e.getMessage());
        }
        this.saldo = vlr;
    }
    
    
    
    
    
     /* M�todos que permite obtener el valor de la planilla de la lista, ya que el item correspondiente a la planilla
     * es de concepto '00'
     * @autor         fvillacob  
     * @parameter     Hashtable  datosPlanilla 
     * @version       1.0.     
     **/ 
    private void setearValorPlanilla(){
        for(int i=0;i<this.movimientos.size();i++){
            ItemLiquidacion item = (ItemLiquidacion)this.movimientos.get(i);
            if(item.getConcepto().equals("00")){
                this.valorPlanilla  = item.getValor();
                break;
            }
        }
    }
    
    
    
    
    /* M�todos que calcula el valor maximo de reanticipos  permitido
     * @autor         fvillacob       
     * @throws        Exception
     * @version       1.0.     
     **/ 
    private void calcularValorMaximoReanticipos(String std)throws Exception{
        try{
          
        // 1. Porcentaje de Anticipos:
              this.pocentaje                   =  this.LiquidacionDataAccess.getPorcentajeAnticipo( this.distrito, std ); ;
              
              
        // 2. Calculo de valores de Movimientos:
              double vlrAnticipos              = 0;
              double vlrAjustes                = 0;              
              double vlrExtrafletes            = 0;
              
              for(int i=0;i<this.movimientos.size();i++){
                    ItemLiquidacion item = (ItemLiquidacion)this.movimientos.get(i); 
                        double  vlr        = item.getValor();
                        String concep_code = item.getConcepto();
                        if ( concep_code.equals("01") )                                            // Anticipos, Reanticipos
                             vlrAnticipos += vlr;
                        else if ( concep_code.equals("09") )                                       // Ajustes
                             vlrAjustes   += vlr;
                        else if( !concep_code.equals("00") && item.getAsignador().equals("V")  )   // ExtraFletes : Los que aplique a valor.
                             vlrExtrafletes  += vlr;
              }

        // 3. Valor Total Planilla:
              this.valorTotalPlanilla          =  this.valorPlanilla  +  vlrExtrafletes  +  vlrAjustes;   
           
        // 4. Valor Maximo de Anticipos:
              this.valorMaxAnticipo            =  ( this.valorTotalPlanilla *  this.pocentaje ) /100;
              
        // 5. Saldo de Reanticipos:
              double saldoReanticiposPermitido =  this.valorMaxAnticipo  +  vlrAnticipos;  // Se coloca signo + porque bienen con signo -
              
        // 6. Valor Maximo de Reanticipos :    
              if  (  saldoReanticiposPermitido > saldo )
                     this.vlrMaximoReanticipos = saldo;
              else
                     this.vlrMaximoReanticipos = saldoReanticiposPermitido;
              
          
        }catch(Exception e){
            throw new Exception( " LiquidarPlanilla -> calcularValorMaximoReanticipos :" + e.getMessage());
        } 
              
    }
    
    
    
    
    
    
    /* M�todos que convirte  valores de una moneda a otra.
     * @autor         fvillacob       
     * @throws        Exception
     * @version       1.0.     
     **/ 
    public double convertirMoneda(String distrito, String monedaOrigen, double valor , String monedaDestino)throws Exception{
        String peso = "PES";
        double vlr  = valor;
        try{
            
            if ( monedaOrigen.equals(peso)  ){
                 double tasa  =  this.LiquidacionDataAccess.getTasa( distrito, monedaOrigen, monedaDestino);
                 vlr *=  tasa;
            }
            else{
                double tasa  =  this.LiquidacionDataAccess.getTasa( distrito, peso , monedaOrigen );    
                vlr /= tasa;
                if ( !monedaDestino.equals(peso)  )
                     vlr =  convertirMoneda(distrito, peso, vlr, monedaDestino  );                 
            }
            
        }catch(Exception e){
            throw new Exception( " convertirMoneda " +e.getMessage() );
        }
        return vlr;
    }
    
    
    
    
    
    private String resetNull(String val){
        return  (val==null)?"":val;
    }
     
    
    
    private String resetValorNull(String val){
        return  (val==null)?"0":val;
    }
    
    
    
    
    
// SET  and  GET :
/**
 * Bloque de m�todos que setean y retornan  valores de los atributos del objeto.
 * @autor.......fvillacob  
 * @version.....1.0.     
 **/ 
    
    
    
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }    
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    } 
    
    
    
    
    /**
     * Getter for property saldo.
     * @return Value of property saldo.
     */
    public double getSaldo() {
        return saldo;
    }    
    
    /**
     * Setter for property saldo.
     * @param saldo New value of property saldo.
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    
    
    
    /**
     * Getter for property vlrMaximoReanticipos.
     * @return Value of property vlrMaximoReanticipos.
     */
    public double getVlrMaximoReanticipos() {
        return vlrMaximoReanticipos;
    }
    
    /**
     * Setter for property vlrMaximoReanticipos.
     * @param vlrMaximoReanticipos New value of property vlrMaximoReanticipos.
     */
    public void setVlrMaximoReanticipos(double vlrMaximoReanticipos) {
        this.vlrMaximoReanticipos = vlrMaximoReanticipos;
    }
    
    
    
    
    /**
     * Getter for property movimientos.
     * @return Value of property movimientos.
     */
    public java.util.List getMovimientos() {
        return movimientos;
    }
    
    /**
     * Setter for property movimientos.
     * @param movimientos New value of property movimientos.
     */
    public void setMovimientos(java.util.List movimientos) {
        this.movimientos = movimientos;
    }
    
    
    
    
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }    
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }  
    
    
    
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }    
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    
    
    
    /**
     * Getter for property valorPlanilla.
     * @return Value of property valorPlanilla.
     */
    public double getValorPlanilla() {
        return valorPlanilla;
    }
    
    /**
     * Setter for property valorPlanilla.
     * @param valorPlanilla New value of property valorPlanilla.
     */
    public void setValorPlanilla(double valorPlanilla) {
        this.valorPlanilla = valorPlanilla;
    }
    
    
    
    
    /**
     * Getter for property valorTotalPlanilla.
     * @return Value of property valorTotalPlanilla.
     */
    public double getValorTotalPlanilla() {
        return valorTotalPlanilla;
    }    
    
    /**
     * Setter for property valorTotalPlanilla.
     * @param valorTotalPlanilla New value of property valorTotalPlanilla.
     */
    public void setValorTotalPlanilla(double valorTotalPlanilla) {
        this.valorTotalPlanilla = valorTotalPlanilla;
    }    
    
    
    
    
    /**
     * Getter for property valorMaxAnticipo.
     * @return Value of property valorMaxAnticipo.
     */
    public double getValorMaxAnticipo() {
        return valorMaxAnticipo;
    }    
    
    /**
     * Setter for property valorMaxAnticipo.
     * @param valorMaxAnticipo New value of property valorMaxAnticipo.
     */
    public void setValorMaxAnticipo(double valorMaxAnticipo) {
        this.valorMaxAnticipo = valorMaxAnticipo;
    }    
    
    
    
    
    /**
     * Getter for property pocentaje.
     * @return Value of property pocentaje.
     */
    public double getPocentaje() {
        return pocentaje;
    }
    
    /**
     * Setter for property pocentaje.
     * @param pocentaje New value of property pocentaje.
     */
    public void setPocentaje(double pocentaje) {
        this.pocentaje = pocentaje;
    }
    
    
    
    
    
    
    
    
     public static void main(String[] args)throws Exception{
        try{
            LiquidarPlanilla  x = new LiquidarPlanilla();
            x.Liquidar("FINV", "596638", "MPE");
            
            List lista  =  x.getMovimientos();
            for(int i=0;i<lista.size();i++){
                    ItemLiquidacion item = (ItemLiquidacion)lista.get(i);   
                    //System.out.println( item.getDescripcion() +" | "+  item.getValor() +" | "+ item.getReteFuente() +" | "+ item.getReteIca() );
                    
            }
            
            //System.out.println("PLANILLA              : " + x.getPlanilla()              );
            //System.out.println("VALOR PLANILLA        : " + x.getValorPlanilla()         );
            //System.out.println("VALOR TOTAL PLANILLA  : " + x.getValorTotalPlanilla()    );
            //System.out.println("PORCENTAJE            : " + x.getPocentaje()             );
            //System.out.println("VALOR MAX ANTICIPOS   : " + x.getValorMaxAnticipo()      );            
            //System.out.println("VALOR MAX REANTICIPOS : " + x.getVlrMaximoReanticipos()  );
            //System.out.println("SALDO                 : " + x.getSaldo()                 );            
            //System.out.println("MONEDA                : " + x.getMoneda()                );
            
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
     
     
     
    
}
