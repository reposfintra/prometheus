/******************************************************************
* Nombre ......................VehNoRetornado.java
* Descripci�n..................Clase bean para Veh�culos no retornados
* Autor........................Armando Oviedo
* Fecha........................18/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*;

public class VehNoRetornado {
    
    String regStatus;
    String dstrct;
    String lastUpdate;
    String userUpdate;
    String creationDate;
    String creationUser;
    String base;
    String placa;
    String causa;
    String fecha;
    
    
    /** Creates a new instance of VehNoRetornado */
    public VehNoRetornado() {
    }
    
    /**
     * M�todo que retorna un objeto VehNoRetornado cargado previamente de un resultset
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......ResultSet rs
     * @return.......objeto VehNoRetornado
     **/ 
    public VehNoRetornado loadResultSet(ResultSet rs) throws SQLException{
        VehNoRetornado vnr = new VehNoRetornado();
        vnr.setBase(rs.getString("base"));
        vnr.setPlaca(rs.getString("placa"));        
        vnr.setCausa(rs.getString("causa"));
        vnr.setCreationDate(rs.getString("creation_date"));
        vnr.setCreationUser(rs.getString("creation_user"));
        vnr.setFecha(rs.getString("fecha"));
        vnr.setDstrct(rs.getString("dstrct"));
        vnr.setLastUpdate(rs.getString("last_update"));
        vnr.setRegStatus(rs.getString("reg_status"));
        vnr.setUserUpdate(rs.getString("user_update"));
        return vnr;
    }
    public void setRegStatus(String regStatus){
        this.regStatus = regStatus;
    }
    public void setDstrct(String dstrct){
        this.dstrct = dstrct;
    }
    public void setLastUpdate(String lastUpdate){
        this.lastUpdate = lastUpdate;
    }
    public void setUserUpdate(String userUpdate){
        this.userUpdate = userUpdate;
    }
    public void setCreationDate(String creationDate){
        this.creationDate = creationDate;
    }
    public void setCreationUser(String creationUser){
        this.creationUser = creationUser;
    }
    public void setBase(String base){
        this.base = base;
    }
    public void setPlaca(String placa){
        this.placa = placa;
    }
    public void setCausa(String causa){
        this.causa = causa;
    }
    public void setFecha(String fecha){
        this.fecha = fecha;
    }    
    public String getRegStatus(){
        return this.regStatus;
    }
    public String getDstrct(){
        return this.dstrct;
    }
    public String getLastUpdate(){
        return this.lastUpdate;
    }
    public String getUserUpdate(){
        return this.userUpdate;
    }
    public String getCreationDate(){
        return this.creationDate;
    }
    public String getCreationUser(){
        return this.creationUser;
    }
    public String getBase(){
        return this.base;
    }
    public String getPlaca(){
        return this.placa;
    }
    public String getCausa(){
        return this.causa;
    }
    public String getFecha(){
        return this.fecha;
    }    
}