/********************************************************************
 *      Nombre Clase.................   AdminHistorico.java
 *      Descripción..................   Bean de la tabla admin_historicos
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versión......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

public class TablaItem{ 

    private String descripcion;
    private String unidad;
    private double cantidad;
    private double valor;
    private double valorTotal;
    
    public TablaItem(String d, String u, double c, double v, double vt){
        descripcion = d;
        unidad = u;
        cantidad = c;
        valor = v; 
        valorTotal = vt;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    
}

