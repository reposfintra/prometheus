/*************************************************************************************
 * Nombre clase :                   ReporteCumplido.java                             *
 * Descripcion :                    Clase que maneja los atributos relacionados con  *
 *                                  el reporte de cumplidos                          *
 * Autor :                          Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                          30 de enero de 2006, 09:20 AM                    *
 * Version :                        1.0                                              *
 * Copyright :                      Fintravalores S.A.                          *
 ************************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class ReporteCumplido {
    private String planilla;
    private String fecha_despacho;
    private String agc_planilla;
    private String origen_planilla;
    private String destino_planilla;
    private String fec_cumplido_planilla;
    private String fecha_entrega;
    private String dif__cantidad_planilla;
    private double cantidad_origen;
    private double cantidad_cumplida_pla;
    private double diferencia_cantidad;
    private String agencia_cumplido_pla;
    private String usuario_cumplido_pla;
    private String numdiscrepancia;
    private String remesa;
    private String fecha_remesa;
    private String agencia_remesa;
    private String origen_remesa;
    private String destino_remesa;
    private String cliente;
    private String fec_cumplido_remesa;
    private double cantidad_remesa;
    private double cantidad_cumplida_rem;
    private double dif_cantidad_remesa;
    private String agencia_cumplido_rem;
    private String usuario_cumplido_rem;
    private String documentos_remesa;
    private String documentos_cumplidos;
    private String agencia_cumplido;
    private String diferencia_fechas;
    
    private String documentosxcumplir;
    private String discrepancias;
    
    private String placa = "";
    private String agefac = "";
    private String tienedisc = "";
    
    
    /** Creates a new instance of ReporteCumplido */
    public ReporteCumplido() {
    }
    
    /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo ReporteCumplido
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static ReporteCumplido load(ResultSet rs)throws Exception{
        ReporteCumplido datos = new ReporteCumplido();
        datos.setPlanilla(rs.getString("planilla"));
        datos.setFecha_despacho(rs.getString("fecdesp"));
        datos.setAgc_planilla(rs.getString("agepla"));
        datos.setOrigen_planilla(rs.getString("oripla"));
        datos.setDestino_planilla(rs.getString("despla"));
        datos.setFec_cumplido_planilla(rs.getString("feccpd"));
        datos.setUsuario_cumplido_pla(rs.getString("ucpla"));
        datos.setAgencia_cumplido(rs.getString("agecum"));
        datos.setCantidad_origen(rs.getDouble("cantori"));
        datos.setCantidad_cumplida_pla(rs.getDouble("cantcump"));
        datos.setDiferencia_cantidad(rs.getDouble("difcant"));
        datos.setFecha_entrega(rs.getString("fecentpla"));
        datos.setDiferencia_fecha(rs.getString("difFechas"));
        datos.setRemesa(rs.getString("remesa"));
        datos.setFecha_remesa(rs.getString("fecrem"));
        datos.setAgencia_remesa(rs.getString("agerem"));
        datos.setOrigen_remesa(rs.getString("orirem"));
        datos.setDestino_remesa(rs.getString("desrem"));
        datos.setCliente(rs.getString("cliente"));
        datos.setCantidad_remesa(rs.getDouble("cantrem"));
        datos.setDocumentos_cumplidos(rs.getString("docint"));
        datos.setCantidad_cumplida_rem(rs.getDouble("cantremcum"));
        datos.setDif_cantidad_remesa(rs.getDouble("difcantidadrem"));
        datos.setFec_cumplido_remesa(rs.getString("feccumrem"));
        datos.setAgencia_cumplido_rem(rs.getString("agecumprem"));
        datos.setUsuario_cumplido_rem(rs.getString("ucrem"));
        
        
           String disc = rs.getString("discrepacia")!=null?rs.getString("discrepacia"):"";
        datos.setDiscrepancias( disc );
        
        datos.setPlaca( rs.getString("placa")!=null?rs.getString("placa"):"" );
        datos.setAgefac( rs.getString("agefac")!=null?rs.getString("agefac"):"" );
        datos.setTienedisc( disc.equals("")?"No":"Si" );
        return datos;
    }
    
    /**
     * Getter for property agc_planilla.
     * @return Value of property agc_planilla.
     */
    public java.lang.String getAgc_planilla() {
        return agc_planilla;
    }
    
    /**
     * Setter for property agc_planilla.
     * @param agc_planilla New value of property agc_planilla.
     */
    public void setAgc_planilla(java.lang.String agc_planilla) {
        this.agc_planilla = agc_planilla;
    }
    
    /**
     * Getter for property agencia_cumplido_pla.
     * @return Value of property agencia_cumplido_pla.
     */
    public java.lang.String getAgencia_cumplido_pla() {
        return agencia_cumplido_pla;
    }
    
    /**
     * Setter for property agencia_cumplido_pla.
     * @param agencia_cumplido_pla New value of property agencia_cumplido_pla.
     */
    public void setAgencia_cumplido_pla(java.lang.String agencia_cumplido_pla) {
        this.agencia_cumplido_pla = agencia_cumplido_pla;
    }
    
    /**
     * Getter for property agencia_cumplido_rem.
     * @return Value of property agencia_cumplido_rem.
     */
    public java.lang.String getAgencia_cumplido_rem() {
        return agencia_cumplido_rem;
    }
    
    /**
     * Setter for property agencia_cumplido_rem.
     * @param agencia_cumplido_rem New value of property agencia_cumplido_rem.
     */
    public void setAgencia_cumplido_rem(java.lang.String agencia_cumplido_rem) {
        this.agencia_cumplido_rem = agencia_cumplido_rem;
    }
    
    /**
     * Getter for property agencia_remesa.
     * @return Value of property agencia_remesa.
     */
    public java.lang.String getAgencia_remesa() {
        return agencia_remesa;
    }
    
    /**
     * Setter for property agencia_remesa.
     * @param agencia_remesa New value of property agencia_remesa.
     */
    public void setAgencia_remesa(java.lang.String agencia_remesa) {
        this.agencia_remesa = agencia_remesa;
    }
    
    /**
     * Getter for property cantidad_cumplida_pla.
     * @return Value of property cantidad_cumplida_pla.
     */
    public double getCantidad_cumplida_pla() {
        return cantidad_cumplida_pla;
    }
    
    /**
     * Setter for property cantidad_cumplida_pla.
     * @param cantidad_cumplida_pla New value of property cantidad_cumplida_pla.
     */
    public void setCantidad_cumplida_pla(double cantidad_cumplida_pla) {
        this.cantidad_cumplida_pla = cantidad_cumplida_pla;
    }
    
    /**
     * Getter for property cantidad_cumplida_rem.
     * @return Value of property cantidad_cumplida_rem.
     */
    public double getCantidad_cumplida_rem() {
        return cantidad_cumplida_rem;
    }
    
    /**
     * Setter for property cantidad_cumplida_rem.
     * @param cantidad_cumplida_rem New value of property cantidad_cumplida_rem.
     */
    public void setCantidad_cumplida_rem(double cantidad_cumplida_rem) {
        this.cantidad_cumplida_rem = cantidad_cumplida_rem;
    }
    
    /**
     * Getter for property cantidad_origen.
     * @return Value of property cantidad_origen.
     */
    public double getCantidad_origen() {
        return cantidad_origen;
    }
    
    /**
     * Setter for property cantidad_origen.
     * @param cantidad_origen New value of property cantidad_origen.
     */
    public void setCantidad_origen(double cantidad_origen) {
        this.cantidad_origen = cantidad_origen;
    }
    
    /**
     * Getter for property cantidad_remesa.
     * @return Value of property cantidad_remesa.
     */
    public double getCantidad_remesa() {
        return cantidad_remesa;
    }
    
    /**
     * Setter for property cantidad_remesa.
     * @param cantidad_remesa New value of property cantidad_remesa.
     */
    public void setCantidad_remesa(double cantidad_remesa) {
        this.cantidad_remesa = cantidad_remesa;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property destino_planilla.
     * @return Value of property destino_planilla.
     */
    public java.lang.String getDestino_planilla() {
        return destino_planilla;
    }
    
    /**
     * Setter for property destino_planilla.
     * @param destino_planilla New value of property destino_planilla.
     */
    public void setDestino_planilla(java.lang.String destino_planilla) {
        this.destino_planilla = destino_planilla;
    }
    
    /**
     * Getter for property destino_remesa.
     * @return Value of property destino_remesa.
     */
    public java.lang.String getDestino_remesa() {
        return destino_remesa;
    }
    
    /**
     * Setter for property destino_remesa.
     * @param destino_remesa New value of property destino_remesa.
     */
    public void setDestino_remesa(java.lang.String destino_remesa) {
        this.destino_remesa = destino_remesa;
    }
    
    /**
     * Getter for property dif__cantidad_planilla.
     * @return Value of property dif__cantidad_planilla.
     */
    public java.lang.String getDif__cantidad_planilla() {
        return dif__cantidad_planilla;
    }
    
    /**
     * Setter for property dif__cantidad_planilla.
     * @param dif__cantidad_planilla New value of property dif__cantidad_planilla.
     */
    public void setDif__cantidad_planilla(java.lang.String dif__cantidad_planilla) {
        this.dif__cantidad_planilla = dif__cantidad_planilla;
    }
    
    /**
     * Getter for property dif_cantidad_remesa.
     * @return Value of property dif_cantidad_remesa.
     */
    public double getDif_cantidad_remesa() {
        return dif_cantidad_remesa;
    }
    
    /**
     * Setter for property dif_cantidad_remesa.
     * @param dif_cantidad_remesa New value of property dif_cantidad_remesa.
     */
    public void setDif_cantidad_remesa(double dif_cantidad_remesa) {
        this.dif_cantidad_remesa = dif_cantidad_remesa;
    }
    
    /**
     * Getter for property diferencia_cantidad.
     * @return Value of property diferencia_cantidad.
     */
    public double getDiferencia_cantidad() {
        return diferencia_cantidad;
    }
    
    /**
     * Setter for property diferencia_cantidad.
     * @param diferencia_cantidad New value of property diferencia_cantidad.
     */
    public void setDiferencia_cantidad(double diferencia_cantidad) {
        this.diferencia_cantidad = diferencia_cantidad;
    }
    
    /**
     * Getter for property documentos_cumplidos.
     * @return Value of property documentos_cumplidos.
     */
    public java.lang.String getDocumentos_cumplidos() {
        return documentos_cumplidos;
    }
    
    /**
     * Setter for property documentos_cumplidos.
     * @param documentos_cumplidos New value of property documentos_cumplidos.
     */
    public void setDocumentos_cumplidos(java.lang.String documentos_cumplidos) {
        this.documentos_cumplidos = documentos_cumplidos;
    }
    
    /**
     * Getter for property documentos_remesa.
     * @return Value of property documentos_remesa.
     */
    public java.lang.String getDocumentos_remesa() {
        return documentos_remesa;
    }
    
    /**
     * Setter for property documentos_remesa.
     * @param documentos_remesa New value of property documentos_remesa.
     */
    public void setDocumentos_remesa(java.lang.String documentos_remesa) {
        this.documentos_remesa = documentos_remesa;
    }
    
    /**
     * Getter for property fec_cumplido_planilla.
     * @return Value of property fec_cumplido_planilla.
     */
    public java.lang.String getFec_cumplido_planilla() {
        return fec_cumplido_planilla;
    }
    
    /**
     * Setter for property fec_cumplido_planilla.
     * @param fec_cumplido_planilla New value of property fec_cumplido_planilla.
     */
    public void setFec_cumplido_planilla(java.lang.String fec_cumplido_planilla) {
        this.fec_cumplido_planilla = fec_cumplido_planilla;
    }
    
    /**
     * Getter for property fec_cumplido_remesa.
     * @return Value of property fec_cumplido_remesa.
     */
    public java.lang.String getFec_cumplido_remesa() {
        return fec_cumplido_remesa;
    }
    
    /**
     * Setter for property fec_cumplido_remesa.
     * @param fec_cumplido_remesa New value of property fec_cumplido_remesa.
     */
    public void setFec_cumplido_remesa(java.lang.String fec_cumplido_remesa) {
        this.fec_cumplido_remesa = fec_cumplido_remesa;
    }
    
    /**
     * Getter for property fecha_despacho.
     * @return Value of property fecha_despacho.
     */
    public java.lang.String getFecha_despacho() {
        return fecha_despacho;
    }
    
    /**
     * Setter for property fecha_despacho.
     * @param fecha_despacho New value of property fecha_despacho.
     */
    public void setFecha_despacho(java.lang.String fecha_despacho) {
        this.fecha_despacho = fecha_despacho;
    }
    
    /**
     * Getter for property fecha_entrega.
     * @return Value of property fecha_entrega.
     */
    public java.lang.String getFecha_entrega() {
        return fecha_entrega;
    }
    
    /**
     * Setter for property fecha_entrega.
     * @param fecha_entrega New value of property fecha_entrega.
     */
    public void setFecha_entrega(java.lang.String fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }
    
    /**
     * Getter for property fecha_remesa.
     * @return Value of property fecha_remesa.
     */
    public java.lang.String getFecha_remesa() {
        return fecha_remesa;
    }
    
    /**
     * Setter for property fecha_remesa.
     * @param fecha_remesa New value of property fecha_remesa.
     */
    public void setFecha_remesa(java.lang.String fecha_remesa) {
        this.fecha_remesa = fecha_remesa;
    }
    
    /**
     * Getter for property numdiscrepancia.
     * @return Value of property numdiscrepancia.
     */
    public java.lang.String getNumdiscrepancia() {
        return numdiscrepancia;
    }
    
    /**
     * Setter for property numdiscrepancia.
     * @param numdiscrepancia New value of property numdiscrepancia.
     */
    public void setNumdiscrepancia(java.lang.String numdiscrepancia) {
        this.numdiscrepancia = numdiscrepancia;
    }
    
    /**
     * Getter for property origen_planilla.
     * @return Value of property origen_planilla.
     */
    public java.lang.String getOrigen_planilla() {
        return origen_planilla;
    }
    
    /**
     * Setter for property origen_planilla.
     * @param origen_planilla New value of property origen_planilla.
     */
    public void setOrigen_planilla(java.lang.String origen_planilla) {
        this.origen_planilla = origen_planilla;
    }
    
    /**
     * Getter for property origen_remesa.
     * @return Value of property origen_remesa.
     */
    public java.lang.String getOrigen_remesa() {
        return origen_remesa;
    }
    
    /**
     * Setter for property origen_remesa.
     * @param origen_remesa New value of property origen_remesa.
     */
    public void setOrigen_remesa(java.lang.String origen_remesa) {
        this.origen_remesa = origen_remesa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property usuario_cumplido_pla.
     * @return Value of property usuario_cumplido_pla.
     */
    public java.lang.String getUsuario_cumplido_pla() {
        return usuario_cumplido_pla;
    }
    
    /**
     * Setter for property usuario_cumplido_pla.
     * @param usuario_cumplido_pla New value of property usuario_cumplido_pla.
     */
    public void setUsuario_cumplido_pla(java.lang.String usuario_cumplido_pla) {
        this.usuario_cumplido_pla = usuario_cumplido_pla;
    }
    
    /**
     * Getter for property usuario_cumplido_rem.
     * @return Value of property usuario_cumplido_rem.
     */
    public java.lang.String getUsuario_cumplido_rem() {
        return usuario_cumplido_rem;
    }
    
    /**
     * Setter for property usuario_cumplido_rem.
     * @param usuario_cumplido_rem New value of property usuario_cumplido_rem.
     */
    public void setUsuario_cumplido_rem(java.lang.String usuario_cumplido_rem) {
        this.usuario_cumplido_rem = usuario_cumplido_rem;
    }
    
    /**
     * Getter for property agencia_cumplido.
     * @return Value of property agencia_cumplido.
     */
    public java.lang.String getAgencia_cumplido() {
        return agencia_cumplido;
    }
    
    /**
     * Setter for property agencia_cumplido.
     * @param agencia_cumplido New value of property agencia_cumplido.
     */
    public void setAgencia_cumplido(java.lang.String agencia_cumplido) {
        this.agencia_cumplido = agencia_cumplido;
    }
    
    /**
     * Getter for property documentosxcumplir.
     * @return Value of property documentosxcumplir.
     */
    public java.lang.String getDocumentosxcumplir() {
        return documentosxcumplir;
    }
    
    /**
     * Setter for property documentosxcumplir.
     * @param documentosxcumplir New value of property documentosxcumplir.
     */
    public void setDocumentosxcumplir(java.lang.String documentosxcumplir) {
        this.documentosxcumplir = documentosxcumplir;
    }
    
    /**
     * Getter for property discrepancias.
     * @return Value of property discrepancias.
     */
    public java.lang.String getDiscrepancias() {
        return discrepancias;
    }
    
    /**
     * Setter for property discrepancias.
     * @param discrepancias New value of property discrepancias.
     */
    public void setDiscrepancias(java.lang.String discrepancias) {
        this.discrepancias = discrepancias;
    }
    
    /**
     * Getter for property diferencia_fecha.
     * @return Value of property diferencia_fecha.
     */
    public java.lang.String getDiferencia_fecha() {
        return diferencia_fechas;
    }
    
    /**
     * Setter for property diferencia_fecha.
     * @param diferencia_fecha New value of property diferencia_fecha.
     */
    public void setDiferencia_fecha(java.lang.String diferencia_fecha) {
        this.diferencia_fechas = diferencia_fecha;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property agefac.
     * @return Value of property agefac.
     */
    public java.lang.String getAgefac() {
        return agefac;
    }
    
    /**
     * Setter for property agefac.
     * @param agefac New value of property agefac.
     */
    public void setAgefac(java.lang.String agefac) {
        this.agefac = agefac;
    }
    
    /**
     * Getter for property tienedisc.
     * @return Value of property tienedisc.
     */
    public java.lang.String getTienedisc() {
        return tienedisc;
    }
    
    /**
     * Setter for property tienedisc.
     * @param tienedisc New value of property tienedisc.
     */
    public void setTienedisc(java.lang.String tienedisc) {
        this.tienedisc = tienedisc;
    }
    
}
