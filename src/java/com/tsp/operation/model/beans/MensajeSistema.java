package com.tsp.operation.model.beans;
/** * * @author imorales */
public class MensajeSistema {//variables del sistema 20100722
    private int frecuenciaRelojServer;//frecuencia con la que se consultar? el reloj en el server dada por ejecuciones de reloj
    private int frecuenciaConsultaBd;
    private int frecuenciaRelojJs;//frecuencia con la que se refrescar?  el reloj con javascript dada por milisegundos
    private String mensaje;
    private String estado_mensaje;
    private String tipo_mensaje;
    private int altura;
    private int ancho;
    private String css;
    private String showProgress;
    private String mostrar_segundos;
    public MensajeSistema() {     }
    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String x) {//
        this.mensaje = x;
    }
    public String getEstadoMensaje() {
        return estado_mensaje;
    }
    public void setEstadoMensaje(String x) {//
        this.estado_mensaje = x;
    }
    public String getTipoMensaje() {
        return tipo_mensaje;
    }
    public void setTipoMensaje(String x) {//
        this.tipo_mensaje = x;
    }
    public int getFrecuenciaRelojServer() {
        return frecuenciaRelojServer;
    }
    public void setFrecuenciaRelojServer(int x1) {//
        this.frecuenciaRelojServer = x1;
    }
    public int getFrecuenciaRelojJs() {
        return frecuenciaRelojJs;
    }
    public void setFrecuenciaRelojJs(int x1) {//
        this.frecuenciaRelojJs = x1;
    }    
    public int getFrecuenciaConsultaBd() {
        return frecuenciaConsultaBd;
    }
    public void setFrecuenciaConsultaBd(int x1) {//
        this.frecuenciaConsultaBd = x1;
    }
    public int getAltura() {
        return altura;
    }
    public void setAltura(int x1) {//
        this.altura = x1;
    }
    public int getAncho() {
        return ancho;
    }
    public void setAncho(int x1) {//
        this.ancho = x1;
    }
    public void setCss(String x) {//
        this.css = x;
    }
    public String getCss() {
        return css;
    }
    public void setShowProgress(String x) {//
        this.showProgress = x;
    }
    public String getShowProgress() {
        return showProgress;
    }
    public String getMostrarSegundos() {
        return mostrar_segundos;
    }
    public void setMostrarSegundos(String x) {//
        this.mostrar_segundos = x;
    }    
}
