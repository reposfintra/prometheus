/*
 *  Nombre clase    :  BalancePrueba.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 20 de junio de 2006, 11:43 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;

import java.util.*;
import java.sql.*;

public class BPUnidadesNegocio extends BPCuentas{
      
    private List Cuentas;               
    
    /** Crea una nueva instancia de  BalancePrueba */
    public BPUnidadesNegocio() {
        Cuentas = new LinkedList();
    }
    
   
    
    public void addCuentas(BPCuentas c){
        Cuentas.add(c);
    }
    
    public void Calcular( int mes ){
        
        if (Cuentas!=null && Cuentas.size()>0){
            for (int i = 0; i < Cuentas.size();i++){
                BPCuentas bpc = (BPCuentas) Cuentas.get(i);
                this.setVMovCredito( mes,   bpc.getVMovCredito(mes));
                this.setVMovDebito(  mes ,  bpc.getVMovDebito(mes));                
            }
            CalcularSaldos(mes);
        }
        
    }
    
    /**
     * Getter for property Cuentas.
     * @return Value of property Cuentas.
     */
    public java.util.List getCuentas() {
        return Cuentas;
    }
    
    /**
     * Setter for property Cuentas.
     * @param Cuentas New value of property Cuentas.
     */
    public void setCuentas(java.util.List Cuentas) {
        this.Cuentas = Cuentas;
    }
    
}
