package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla Destino_Credito<br/>
 * 9/10/2020
 * @author jzapata
 */
public class Destino_Credito {

    private String id;
    private String reg_status;
    private String cod_destino_credito;
    private String destino_credito;
    private String creation_date;
    private String creation_user;	
    private String last_update;
    private String user_update;

    public Destino_Credito load(ResultSet rs) throws SQLException {
        Destino_Credito destcre = new Destino_Credito();
        destcre.setDestino_credito(rs.getString("destino_credito"));
        destcre.setCod_destino_credito(rs.getString("cod_destino_credito"));
        destcre.setReg_status(rs.getString("reg_status"));
        return destcre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getCod_destino_credito() {
        return cod_destino_credito;
    }

    public void setCod_destino_credito(String cod_destino_credito) {
        this.cod_destino_credito = cod_destino_credito;
    }

    public String getDestino_credito() {
        return destino_credito;
    }

    public void setDestino_credito(String destino_credito) {
        this.destino_credito = destino_credito;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

}
