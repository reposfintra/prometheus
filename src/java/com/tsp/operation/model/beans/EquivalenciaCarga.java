/*
 * EquivalenciaCarga.java
 *
 * Created on 12 de enero de 2007, 09:26 AM
 */

package com.tsp.operation.model.beans;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  EQUIPO26
 */
public class EquivalenciaCarga {
    
    public String codigo_carga;
    public String descripcion_carga;
    public String codigo_legal;
    public String tipo_carga;
    public double valor_mercancia;
    public String unidad_mercancia;
    public String descripcion_corta;
    public String fecha;
    public String distrito;
    public String usuario;
    public String base;
    
    /** Creates a new instance of EquivalenciaCarga */
    public EquivalenciaCarga() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property codigo_carga.
     * @return Value of property codigo_carga.
     */
    public java.lang.String getCodigo_carga() {
        return codigo_carga;
    }
    
    /**
     * Setter for property codigo_carga.
     * @param codigo_carga New value of property codigo_carga.
     */
    public void setCodigo_carga(java.lang.String codigo_carga) {
        this.codigo_carga = codigo_carga;
    }
    
    /**
     * Getter for property codigo_legal.
     * @return Value of property codigo_legal.
     */
    public java.lang.String getCodigo_legal() {
        return codigo_legal;
    }
    
    /**
     * Setter for property codigo_legal.
     * @param codigo_legal New value of property codigo_legal.
     */
    public void setCodigo_legal(java.lang.String codigo_legal) {
        this.codigo_legal = codigo_legal;
    }
    
    /**
     * Getter for property descripcion_carga.
     * @return Value of property descripcion_carga.
     */
    public java.lang.String getDescripcion_carga() {
        return descripcion_carga;
    }
    
    /**
     * Setter for property descripcion_carga.
     * @param descripcion_carga New value of property descripcion_carga.
     */
    public void setDescripcion_carga(java.lang.String descripcion_carga) {
        this.descripcion_carga = descripcion_carga;
    }
    
    /**
     * Getter for property descripcion_corta.
     * @return Value of property descripcion_corta.
     */
    public java.lang.String getDescripcion_corta() {
        return descripcion_corta;
    }
    
    /**
     * Setter for property descripcion_corta.
     * @param descripcion_corta New value of property descripcion_corta.
     */
    public void setDescripcion_corta(java.lang.String descripcion_corta) {
        this.descripcion_corta = descripcion_corta;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property tipo_carga.
     * @return Value of property tipo_carga.
     */
    public java.lang.String getTipo_carga() {
        return tipo_carga;
    }
    
    /**
     * Setter for property tipo_carga.
     * @param tipo_carga New value of property tipo_carga.
     */
    public void setTipo_carga(java.lang.String tipo_carga) {
        this.tipo_carga = tipo_carga;
    }
    
    /**
     * Getter for property unidad_mercancia.
     * @return Value of property unidad_mercancia.
     */
    public java.lang.String getUnidad_mercancia() {
        return unidad_mercancia;
    }
    
    /**
     * Setter for property unidad_mercancia.
     * @param unidad_mercancia New value of property unidad_mercancia.
     */
    public void setUnidad_mercancia(java.lang.String unidad_mercancia) {
        this.unidad_mercancia = unidad_mercancia;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property valor_mercancia.
     * @return Value of property valor_mercancia.
     */
    public double getValor_mercancia() {
        return valor_mercancia;
    }
    
    /**
     * Setter for property valor_mercancia.
     * @param valor_mercancia New value of property valor_mercancia.
     */
    public void setValor_mercancia(double valor_mercancia) {
        this.valor_mercancia = valor_mercancia;
    }
    
}
