/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */

public class FacturaApplus {

  private String  id_contratista ;
  private String  prefactura;
  private String  fact_conformada;

  private double  vlr_mat;
  private double  vlr_mob;
  private double  vlr_otr;

  private double  vlr_mat_mob_otr;

  private double  vlr_administracion;
  private double  vlr_imprevisto;

  private double  vlr_utilidad;
  private double  vlr_bonificacion;
  private double  vlr_iva;
  private double  vlr_rmat;
  private double  vlr_rmob;
  private double  vlr_rotr;

  private double  vlr_retencion;

  private double  vlr_factoring;
  private double  vlr_formula;
  private double  vlr_base_iva;


    /** Creates a new instance of FacturaGeneral */
    public FacturaApplus() {
    }

    public static FacturaApplus load(java.sql.ResultSet rs)throws java.sql.SQLException{

        FacturaApplus facturaApplus = new FacturaApplus();

        facturaApplus.setId_contratista( rs.getString("id_contratista") );
        facturaApplus.setPrefactura( rs.getString("prefactura") );
        facturaApplus.setFact_conformada(rs.getString("fact_conformada") );
        facturaApplus.setVlr_mat(rs.getDouble("vlr_mat"));
        facturaApplus.setVlr_mob(rs.getDouble("vlr_mob"));
        facturaApplus.setVlr_otr(rs.getDouble("vlr_otr"));
        facturaApplus.setVlr_administracion(rs.getDouble("vlr_administracion"));
        facturaApplus.setVlr_imprevisto(rs.getDouble("vlr_imprevisto"));
        facturaApplus.setVlr_utilidad(rs.getDouble("vlr_utilidad"));
        facturaApplus.setVlr_Bonificacion(rs.getDouble("vlr_bonificacion"));
        facturaApplus.setVlr_iva(rs.getDouble("vlr_iva"));
        facturaApplus.setVlr_rmat(rs.getDouble("vlr_rmat"));
        facturaApplus.setVlr_rmob(rs.getDouble("vlr_rmob"));
        facturaApplus.setVlr_rotr(rs.getDouble("vlr_rotr"));
        facturaApplus.setVlr_factoring(rs.getDouble("vlr_factoring"));
        facturaApplus.setVlr_formula(rs.getDouble("vlr_formula"));
        facturaApplus.setVlr_base_iva(rs.getDouble("vlr_base_iva"));

        return facturaApplus;
    }

    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the prefactura
     */
    public String getPrefactura() {
        return prefactura;
    }

    /**
     * @param prefactura the prefactura to set
     */
    public void setPrefactura(String prefactura) {
        this.prefactura = prefactura;
    }

    /**
     * @return the vlr_mat
     */
    public double getVlr_mat() {
        return vlr_mat;
    }

    /**
     * @param vlr_mat the vlr_mat to set
     */
    public void setVlr_mat(double vlr_mat) {
        this.vlr_mat = vlr_mat;
    }

    /**
     * @return the vlr_mob
     */
    public double getVlr_mob() {
        return vlr_mob;
    }

    /**
     * @param vlr_mob the vlr_mob to set
     */
    public void setVlr_mob(double vlr_mob) {
        this.vlr_mob = vlr_mob;
    }

    /**
     * @return the vlr_otr
     */
    public double getVlr_otr() {
        return vlr_otr;
    }

    /**
     * @param vlr_otr the vlr_otr to set
     */
    public void setVlr_otr(double vlr_otr) {
        this.vlr_otr = vlr_otr;
    }

    /**
     * @return the vlr_administracion
     */
    public double getVlr_administracion() {
        return vlr_administracion;
    }

    /**
     * @param vlr_administracion the vlr_administracion to set
     */
    public void setVlr_administracion(double vlr_administracion) {
        this.vlr_administracion = vlr_administracion;
    }

    /**
     * @return the vlr_imprevisto
     */
    public double getVlr_imprevisto() {
        return vlr_imprevisto;
    }

    /**
     * @param vlr_imprevisto the vlr_imprevisto to set
     */
    public void setVlr_imprevisto(double vlr_imprevisto) {
        this.vlr_imprevisto = vlr_imprevisto;
    }

    /**
     * @return the vlr_utilidad
     */
    public double getVlr_utilidad() {
        return vlr_utilidad;
    }

    /**
     * @param vlr_utilidad the vlr_utilidad to set
     */
    public void setVlr_utilidad(double vlr_utilidad) {
        this.vlr_utilidad = vlr_utilidad;
    }

    /**
     * @return the bonificacion
     */
    public double getVlr_Bonificacion() {
        return vlr_bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setVlr_Bonificacion(double bonificacion) {
        this.vlr_bonificacion = bonificacion;
    }

    /**
     * @return the vlr_iva
     */
    public double getVlr_iva() {
        return vlr_iva;
    }

    /**
     * @param vlr_iva the vlr_iva to set
     */
    public void setVlr_iva(double vlr_iva) {
        this.vlr_iva = vlr_iva;
    }

    /**
     * @return the vlr_rmat
     */
    public double getVlr_rmat() {
        return vlr_rmat;
    }

    /**
     * @param vlr_rmat the vlr_rmat to set
     */
    public void setVlr_rmat(double vlr_rmat) {
        this.vlr_rmat = vlr_rmat;
    }

    /**
     * @return the vlr_rmob
     */
    public double getVlr_rmob() {
        return vlr_rmob;
    }

    /**
     * @param vlr_rmob the vlr_rmob to set
     */
    public void setVlr_rmob(double vlr_rmob) {
        this.vlr_rmob = vlr_rmob;
    }

    /**
     * @return the vlr_rotr
     */
    public double getVlr_rotr() {
        return vlr_rotr;
    }

    /**
     * @param vlr_rotr the vlr_rotr to set
     */
    public void setVlr_rotr(double vlr_rotr) {
        this.vlr_rotr = vlr_rotr;
    }

    /**
     * @return the vlr_factoring
     */
    public double getVlr_factoring() {
        return vlr_factoring;
    }

    /**
     * @param vlr_factoring the vlr_factoring to set
     */
    public void setVlr_factoring(double vlr_factoring) {
        this.vlr_factoring = vlr_factoring;
    }

    /**
     * @return the vlr_formula
     */
    public double getVlr_formula() {
        return vlr_formula;
    }

    /**
     * @param vlr_formula the vlr_formula to set
     */
    public void setVlr_formula(double vlr_formula) {
        this.vlr_formula = vlr_formula;
    }

    /**
     * @return the vlr_base_iva
     */
    public double getVlr_base_iva() {
        return vlr_base_iva;
    }

    /**
     * @param vlr_base_iva the vlr_base_iva to set
     */
    public void setVlr_base_iva(double vlr_base_iva) {
        this.vlr_base_iva = vlr_base_iva;
    }

    /**
     * @return the fact_conformada
     */
    public String getFact_conformada() {
        return fact_conformada;
    }

    /**
     * @param fact_conformada the fact_conformada to set
     */
    public void setFact_conformada(String fact_conformada) {
        this.fact_conformada = fact_conformada;
    }


}
