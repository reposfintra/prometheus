/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.util.Date;

/**
 *
 * @author egonzalez
 */
public class LiquidacionFenalcoBeans {
    
  private int id;
  private int id_rop;
  private int dias;
  private String  fecha ;
  private String item ;
  private double saldo_inicial;
  private double capital;
  private double interes;
  private double custodia;
  private double seguro;
  private double remesa;
  private double valor;
  private double saldo_final;
  private String creation_user;
  private Date creation_date;

    public LiquidacionFenalcoBeans() {
    }

    public LiquidacionFenalcoBeans(int id, int id_rop, int dias, String fecha, String item, double saldo_inicial, double capital, double interes, double custodia, double seguro, double remesa, double valor_cuota, double saldo_final, String creation_user, Date creation_date) {
        this.id = id;
        this.id_rop = id_rop;
        this.dias = dias;
        this.fecha = fecha;
        this.item = item;
        this.saldo_inicial = saldo_inicial;
        this.capital = capital;
        this.interes = interes;
        this.custodia = custodia;
        this.seguro = seguro;
        this.remesa = remesa;
        this.valor = valor_cuota;
        this.saldo_final = saldo_final;
        this.creation_user = creation_user;
        this.creation_date = creation_date;
    }


    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_rop
     */
    public int getId_rop() {
        return id_rop;
    }

    /**
     * @param id_rop the id_rop to set
     */
    public void setId_rop(int id_rop) {
        this.id_rop = id_rop;
    }
    
    
    /**
     * @return the dias
     */
    public int getDias() {
        return dias;
    }

    /**
     * @param dias the dias to set
     */
    public void setDias(int dias) {
        this.dias = dias;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the cuota
     */
    public String getItem() {
        return item;
    }

    /**
     * @param item
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * @return the saldo_inicial
     */
    public double getSaldo_inicial() {
        return saldo_inicial;
    }

    /**
     * @param saldo_inicial the saldo_inicial to set
     */
    public void setSaldo_inicial(double saldo_inicial) {
        this.saldo_inicial = saldo_inicial;
    }

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * @return the custodia
     */
    public double getCustodia() {
        return custodia;
    }

    /**
     * @param custodia the custodia to set
     */
    public void setCustodia(double custodia) {
        this.custodia = custodia;
    }

    /**
     * @return the seguro
     */
    public double getSeguro() {
        return seguro;
    }

    /**
     * @param seguro the seguro to set
     */
    public void setSeguro(double seguro) {
        this.seguro = seguro;
    }

    /**
     * @return the remesa
     */
    public double getRemesa() {
        return remesa;
    }

    /**
     * @param remesa the remesa to set
     */
    public void setRemesa(double remesa) {
        this.remesa = remesa;
    }

    /**
     * @return the valor_cuota
     */
    public double getValor_cuota() {
        return valor;
    }

    /**
     * @param valor_cuota the valor_cuota to set
     */
    public void setValor_cuota(double valor_cuota) {
        this.valor = valor_cuota;
    }

    /**
     * @return the saldo_final
     */
    public double getSaldo_final() {
        return saldo_final;
    }

    /**
     * @param saldo_final the saldo_final to set
     */
    public void setSaldo_final(double saldo_final) {
        this.saldo_final = saldo_final;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the creation_date
     */
    public Date getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    @Override
    public String toString() {
        return "LiquidacionFenalcoBeans{" + "id=" + id + ", id_rop=" + id_rop + ", fecha=" + fecha + ", Item=" + 
                item + ", saldo_inicial=" + saldo_inicial + ", capital=" + capital + ", interes=" + interes + ", custodia=" +
                custodia + ", seguro=" + seguro + ", remesa=" + remesa + ", valor_cuota=" + valor + ", saldo_final=" +
                saldo_final + ", creation_user=" + creation_user + ", creation_date=" + creation_date + '}';
    }

  
  
    
}
