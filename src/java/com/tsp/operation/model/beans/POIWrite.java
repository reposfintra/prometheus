/*
 * POIExcel.java
 *
 * Created on 21 de septiembre de 2005, 04:22 PM
 */


package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;



/**
 *
 * @author  mfontalvo
 */
public class POIWrite {
    
    private String       fileName;
    private HSSFWorkbook wb;
    private HSSFSheet    sheet;
    
    public static final int NONE = -1;

    
    /** Creates a new instance of POIExcel */

    public void newSheet(String hoja)
    {   sheet=wb.createSheet(hoja);
    }
    
    public POIWrite(String file) throws Exception{
        nuevoLibro(file);
    }
    
    public POIWrite() {
    }

    
    public void nuevoLibro (String file) throws Exception{
        wb = new HSSFWorkbook();
        fileName = file;

        
    }
    
    public void cerrarLibro () throws Exception{
        if (wb==null)
            throw new Exception("No se pudo cerrar el libro, primero deber� crear el libro");
        
        FileOutputStream fileOut = new FileOutputStream(fileName);
        wb.write(fileOut);
        fileOut.close(); 
        wb = null;
    }
    
    public void obtenerHoja(String hoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");
        
        sheet = wb.getSheet(hoja); 
        if (sheet == null) sheet = wb.createSheet(hoja); 
       
    }    
    
    public HSSFCell obtenerCelda(int fila, int columna)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row  = sheet.getRow(fila);
        if (row==null) row = sheet.createRow( (short) fila);
        
        HSSFCell cell   = row.getCell((short) columna );
        if (cell == null) cell = row.createCell( (short) columna);
        
        return cell;
    }
    
    public void adicionarCelda(int fila, int columna, String value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow(fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    }
    
    public void adicionarCelda(int fila, int columna, double value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow(fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    }    
    
    public void adicionarCelda(int fila, int columna, Date value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow(fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    } 
    
    public void combinarCeldas(int filaInicial, int columnaInicial, int filaFinal , int columnaFinal)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
                 
        sheet.addMergedRegion(new Region(filaInicial,(short)columnaInicial ,filaFinal,(short)columnaFinal));
     
    }
    
    public void cambiarMagnificacion (int x, int y)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo modificar la magnificacion, primero deber� crear la hoja");
                 
        sheet.setZoom(x,y);   
    }
    
    public void cambiarAnchoColumna (int columna, int ancho) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo cambiar el ancho de la columna, primero deber� crear la hoja");
        sheet.setColumnWidth((short) columna, (short) ancho);
        
    }
    
//    public void cambiarAltoFila (int fila, int alto) throws Exception{
//        if (sheet==null)
//            throw new Exception("No se pudo cambiar alto de la fila, primero deber� crear la hoja");
//        sheet.setRowWidth((short) fila, (short) alto);
//        
//    }     
    
    public void crearPanel (int colSplit, int rowSplit, int leftmostColumn, int topRow)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear el panel, primero deber� crear la hoja");
        sheet.createFreezePane(colSplit, rowSplit , leftmostColumn ,topRow );
    }

    
    public HSSFCellStyle nuevoEstilo (String name, int size, boolean bold, boolean italic , String formato ,int color, int fondo, int align)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear el estilo, primero deber� crear el libro");
        
        HSSFCellStyle style   = wb.createCellStyle();
        HSSFFont      font    = wb.createFont();
        
        font.setFontHeightInPoints((short) size);
        font.setFontName(name);                                 
        if (bold)   font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setItalic(italic);
        if (color!=NONE) font.setColor((short) color);
        
        
        style.setFont(font);
        if (!formato.equals(""))
            style.setDataFormat(wb.createDataFormat().getFormat(formato)); 
        
        if (fondo!=NONE) {
            style.setFillPattern((short) style.SOLID_FOREGROUND);
            style.setFillForegroundColor((short)fondo);
        }
        
        if (align!=NONE) style.setAlignment((short)align);
        return style;
        
    }
    
    
    
    public HSSFCellStyle nuevoEstilo (String name, int size, boolean bold, boolean italic , String formato ,int color, int fondo, int align, int border)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear  el estilo, primero deber� crear el libro");
        
        HSSFCellStyle style   = wb.createCellStyle();
        HSSFFont      font    = wb.createFont();
        
        font.setFontHeightInPoints((short) size);
        font.setFontName(name);                                 
        if (bold)   font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setItalic(italic);
        if (color!=NONE) font.setColor((short) color);
        
        
        style.setFont(font);
        if (!formato.equals(""))
            style.setDataFormat(wb.createDataFormat().getFormat(formato)); 
        
        if (fondo!=NONE) {
            style.setFillPattern((short) style.SOLID_FOREGROUND);
            style.setFillForegroundColor((short)fondo);
        }
        
        if (align!=NONE) style.setAlignment((short)align);
        
        if (border>0){
            style.setBorderBottom((short)border);
            style.setBottomBorderColor(HSSFColor.BLACK.index);
            
            style.setBorderLeft((short)border);
            style.setLeftBorderColor(HSSFColor.BLACK.index);
            
            style.setBorderRight((short)border);
            style.setRightBorderColor(HSSFColor.BLACK.index);
            
            style.setBorderTop((short)border);
            style.setTopBorderColor(HSSFColor.BLACK.index);
        }      
        return style;
        
    }    
    
    
    /**
     * Metodo para obtenecer un nuevo color personalizado
     * @autor mfontalvo
     * params rgb, codigo del color
     */
    public HSSFColor obtenerColor (int r, int g, int b ) throws Exception{
        HSSFColor newColor = null;
        if (wb==null)
            throw new Exception("No se pudo crear el color, primero deber� crear el libro");
        try {
        java.awt.Color color = new java.awt.Color(r, g, b);
        newColor =
        wb.getCustomPalette().findColor(
            (byte)color.getRed(),
            (byte)color.getGreen(),
            (byte)color.getBlue()
        );
        
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newColor;
        
    }
    
    
    /**
     * Metodo para agregar formulas a una celda, 'Formulas sencillas'
     * @autor mfontalvo
     * @param fila fila de la hoja actual
     * @param columna columna de la hoja actual
     * @param formula formula que desea agregar
     * @param style estilo de la celda
     * @throws Exception.
     */
    public void adicionarFormula(int fila, int columna, String formula, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        HSSFRow row   = sheet.createRow( (short) fila);
        HSSFCell cell = row.createCell ( (short) columna);

        cell.setCellFormula(formula);

        if (style!=null) cell.setCellStyle(style);
    }     

}

