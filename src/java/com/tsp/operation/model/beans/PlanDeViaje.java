 /***************************************
    * Nombre Clase ............. PlanDeViaje.java
    * Descripci�n  .. . . . . .  Estructura del Plan de Viaje
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.beans;


import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;


public class PlanDeViaje {
    
    
    
 // 1. Datos Basicos
    private String planilla      = "";
    private String distrito      = "";
    private String placa         = "";
    private String destinatario  = "";
    private String fechaDespacho = "";
    private String tipoCarga     = "";    
    private String productos     = "";
    private String placaUnidad   = "";
    private String contenedores  = "";
    private String ruta          = "";
    private String retorno       = "N";
    private String leido         = "NO";
    
    private String caravana      = "";
    private String trailer       = "";
    private String usuario       = "";
    private String agencia       = "";
    private String fechaCreacion = "";
    
    private String origen        = "";
    private String destino       = "";    
    
    private String fecha         = "";
    private String escolta       = "";
    private String estado        = "";
    private List   vias ;
    private String oricode       = "";
    private String descode       = "";  
    
    
    
    private String proximoPC     = "";
    private String proximoZona   = "";
    private String proximoRuta   = "";
 
    
    private String zona            = "";
    private String nombreZona      = "";
    private String pcTrafico       = "";
    private String nombrepcTrafico = "";
   
    
    
 // 2. Datos Conductor
    private String cedulaCond    = "";
    private String nameCond      = "";
    private String ciudadCond    = "";
    private String direccionCond = "";
    private String telefonoCond  = "";
    
    
 
    
  // 5. Equipos de Comunicacion
    private String radio       = "";
    private String avantel     = "";
    private String cazador     = "";
    private String celular     = "";
    private String telefono    = "";
    private String movil       = "";
    private String otros       = "";
    
    
 
    
 // 8. Femiliar
    private String nameFamiliar    = "";
    private String telFamiliar     = "";
    
 // 9. Propietario
    private String cedulaProp     = "";
    private String nameProp       = "";
    private String ciudadProp     = "";
    private String direccionProp  = "";
    private String telefonoProp   = "";
  
    
 // Comentarios
    private String comentario1   = "";
    private String comentario2   = "";
    private String comentario3   = "";
    
  
    
 // Datos Ubicaciones de la Via  
    private List listAlimentacion;
    private List listParqueadero;
    private List listTanqueo;
    private List listPernotadero;
    
    
    
 // Datos Ubicacion Plan de Via  
    private List listAlimentacionPlan;
    private List listParqueaderoPlan;
    private List listTanqueoPlan;
    private List listPernotaderoPlan;
      
    
    
    public PlanDeViaje() {
        listAlimentacion      = new LinkedList();
        listParqueadero       = new LinkedList();
        listTanqueo           = new LinkedList();
        listPernotadero       = new LinkedList();
        listAlimentacionPlan  = new LinkedList();
        listParqueaderoPlan   = new LinkedList();
        listTanqueoPlan       = new LinkedList();
        listPernotaderoPlan   = new LinkedList();
    }
    
    
    
    
    
   // SET:
    
   // 1. Datos Basicos: 
    
    
        /**
         * Almacena  datos basicos de la planilla
         * @autor........  Fernel Villacob
         * @param........  String val: valores 
         * @version ..... 1.0      
         */   
        
         public void setPlanilla ( String val)  { 
             this.planilla      = val; 
         }   
                  
         public void setListAlimentacion(List list){
             this.listAlimentacion = list;
         }
         
         public void setListParqueadero(List list){
             this.listParqueadero  = list;
         }
         
         public void setListPernotacion(List list){
             this.listPernotadero  = list;
         }
         
         public void setListTanqueo(List list){
             this.listTanqueo      = list;
         }          
          
         public void setListAlimentacionPlan(List list){
             this.listAlimentacionPlan = list;
         }
         
         public void setListParqueaderoPlan(List list){
             this.listParqueaderoPlan  = list;
         }
         
         public void setListPernotacionPlan(List list){
             this.listPernotaderoPlan  = list;
         }
         
         public void setListTanqueoPlan(List list){
             this.listTanqueoPlan      = list;
         }
          
         public void setZona  ( String val)  {  
             this.zona              = val; 
         } 
         
         public void setNombreZona ( String val) {   
             this.nombreZona        = val; 
         }
         
         public void setPCTrafico  ( String val){   
             this.pcTrafico   = val; 
         } 
         
         
         public void setNombrePCTrafico ( String val)  {   
             this.nombrepcTrafico   = val; 
         } 
        
         
         public void setProximoPC  ( String val)  {   
             this.proximoPC    = val;  
         }
         
         public void setProximoZona ( String val)  {   
             this.proximoZona       = val;  
         }
         
         public void setProximoRuta ( String val)  {   
             this.proximoRuta   = val;  
         }
        
         
         public void setFechaPlanViaje ( String val)  { 
             this.fecha      = val; 
         }
         
         public void setOrigenCode( String val)  { 
             this.oricode      = val; 
         }
         
         public void setDestinoCode ( String val)  { 
             this.descode     = val; 
         }
                  
         public void setEstado ( String val)  { 
             this.estado     = val; 
         }
            
         public void setEscolta ( String val)  { 
             this.escolta      = val; 
         }
         
         public void setDistrito ( String val)  { 
             this.distrito      = val; 
         } 
         
         public void setPlaca( String val)  { 
             this.placa         = val; 
         } 
         
         public void setDestinatario    ( String val)  { 
             this.destinatario  = val; 
         }     
         
         public void setFechaDespacho   ( String val)  { 
             this.fechaDespacho = val; 
         }    
         
         public void setTipoCarga       ( String val)  { 
             this.tipoCarga     = val; 
         }    
         
         public void setProducto        ( String val)  { 
             this.productos     = val; 
         }    
         
         public void setPlacaUnidad     ( String val)  { 
             this.placaUnidad   = val; 
         }
         
         public void setContenedores    ( String val)  { 
             this.contenedores  = val; 
         }    
         
         public void setRuta ( String val)  { 
             this.ruta          = val; 
         }   
         
         public void setRetorno ( String val)  { 
             this.retorno       = val; 
         }
         
         public void setleido( String val)  {
             this.leido         = val; 
         }
         
         public void setCaravana ( String val)  { 
             this.caravana      = val; 
         }
         
         public void setTrailer ( String val)  { 
             this.trailer       = val; 
         }
         
         public void setUsuario ( String val)  {
             this.usuario       = val; 
         }
         
         public void setAgencia ( String val)  { 
             this.agencia       = val; 
         }
         
         public void setFechaCreacion   ( String val)  { 
             this.fechaCreacion = val; 
         }
                  
         public void setOrigen ( String val)  { 
             this.origen        = val; 
         }
         
         public void setDestino ( String val)  { 
             this.destino       = val; 
         }
         
         
         public  void setVias(List list){
             this.vias = list;
         }
         
   
    // 2.  Datos Conductor:
         
         /**
         * Almacena  datos del conductor
         * @autor........  Fernel Villacob
         * @param........  String val: valores 
         * @version ..... 1.0      
         */   
         
         public void setCedulaConductor ( String val)  { 
             this.cedulaCond    = val; 
         } 
         
         public void setNameConductor   ( String val)  { 
             this.nameCond      = val; 
         }
         
         public void setCiudadConductor ( String val)  { 
             this.ciudadCond    = val; 
         }
         
         public void setDirConductor    ( String val)  { 
             this.direccionCond = val; 
         }
         
         public void setTelConductor    ( String val)  { 
             this.telefonoCond  = val;
         }
         
        
   
         
         
     // 5.  Equipos de Comunicacion: 
         
         /**
         * Almacena  datos de Equipos de Comunicacion
         * @autor........  Fernel Villacob
         * @param........  String val: valores 
         * @version ..... 1.0      
         */   
         
         public void setRadio    ( String val)  { 
             this.radio      = val;
         }
         
         public void setAvantel  ( String val)  { 
             this.avantel    = val; 
         }
         
         public void setCazador  ( String val)  { 
             this.cazador    = val; 
         }     
         
         public void setCelular  ( String val)  { 
             this.celular    = val; 
         }
         
         public void setTelefono ( String val)  {
             this.telefono   = val; 
         }
         
         public void setMovil    ( String val)  { 
             this.movil      = val; 
         }
         
         public void setOtros    ( String val)  {
             this.otros      = val; 
         }
         
         
   
         
         
   // 8. Femiliar
         /**
         * Almacena  datos de un familiar
         * @autor........  Fernel Villacob
         * @param........  String val: valores 
         * @version ..... 1.0      
         */   
         
         public void setNameFamiliar      ( String val)  { 
             this.nameFamiliar  = val; 
         }
         
         public void setTelFamiliar       ( String val)  { 
             this.telFamiliar   = val; 
         }      
         
         
  // 9. Propietario
         /**
         * Almacena  datos basicos del propietario
         * @autor........  Fernel Villacob
         * @param........  String val: valores 
         * @version ..... 1.0      
         */   
         public void setCedulaPropietario ( String val)  { 
             this.cedulaProp    = val; 
         } 
         
         public void setNamePropietario   ( String val)  { 
             this.nameProp      = val; 
         }
         
         public void setCiudadPropietario ( String val)  { 
             this.ciudadProp    = val; 
         }
         
         public void setDirPropietario    ( String val)  { 
             this.direccionProp = val;
         }
         
         public void setTelPropietario    ( String val)  { 
             this.telefonoProp  = val; 
         }   
         
        
  // 10. Comentarios    
         /**
         * Almacena  comentarios adicionales
         * @autor........  Fernel Villacob
         * @param........  String val: valores 
         * @version ..... 1.0      
         */   
         
         public void setComentario1     ( String val)  { 
             this.comentario1     = val; 
         } 
         
         public void setComentario2     ( String val)  { 
             this.comentario2     = val; 
         }
         
         public void setComentario3     ( String val)  { 
             this.comentario3     = val; 
         }
         
         
         
      
         
         
         
         
         
         
         
         
         
         
         
         
 // GET:
    
  // 1. Datos Basicos: 
  /**
  * Retorna datos basicos de la planilla
  * @autor........ Fernel Villacob
  * @version ..... 1.0 
  */  
         
         public String getPlanilla( )  { 
            return planilla ; 
         }   
         
         
         public List getListAlimentacion(){
             return this.listAlimentacion;
         }
         
         public List getListParqueadero(){
             return this.listParqueadero;
         }
         
         public List getListPernotacion(){
             return this.listPernotadero;
         }
         
         public List getListTanqueo(){
             return this.listTanqueo ;
         }
                  
         
         public List getListAlimentacionPlan(){
             return this.listAlimentacionPlan;
         }
         public List getListParqueaderoPlan(){
             return this.listParqueaderoPlan;
         }
         public List getListPernotacionPlan(){
             return this.listPernotaderoPlan;
         }
         public List getListTanqueoPlan(){
             return this.listTanqueoPlan;
         }
         
         public String getZona ( )  {   
             return this.zona ; 
         } 
         
         
         public String getNombreZona ( )  {   
             return this.nombreZona  ; 
         }
         
         public String getPCTrafico  ( )  {   
             return this.pcTrafico  ; 
         } 
         
         public String getNombrePCTrafico ( )  {   
             return this.nombrepcTrafico   ; 
         }          
         
         public String getProximoPC ( ) {
             return  this.proximoPC   ;  
         }
         
         public String getProximoZona ( ){ 
             return  this.proximoZona ;  
         }
         
         public String getProximoRuta ( ) { 
             return  this.proximoRuta ;  
         }
         
        
         
         public String getOrigenCode( )  { 
             return this.oricode  ; 
         }
         
         public String getDestinoCode ( ) { 
            return  this.descode ; 
         }
         
         
         public String getEstado ( )  { 
            return  this.estado ; 
         }
         
         
        public String getEscolta ()  { 
            return this.escolta ; 
        }
         
        public String getFechaPlanViaje ( )  { 
            return  this.fecha ; 
        }
        
        public String getDistrito( )  { 
            return distrito; 
        } 
        
        public String getPlaca( )  { 
            return placa; 
        } 
        
        public String getDestinatario( )  {
            return destinatario; 
        }     
        
        public String getFechaDespacho( )  { 
            return fechaDespacho; 
        }    
        
        public String getTipoCarga( )  { 
            return tipoCarga; 
        }    
        
        public String getProducto( )  { 
            return productos; 
        }    
        
        public String getPlacaUnidad( )  { 
            return placaUnidad ; 
        }
        
        public String getContenedores( )  {
            return contenedores; 
        }    
        
        public String getRuta( )  { 
            return ruta ; 
        }    
        
        public String getRetorno( )  { 
            return retorno ; 
        }
        
        public String getleido( )  { 
            return leido ; 
        }
        
        
        public  List getVias(){
             return this.vias ;
        }
        
        
        public String getCaravana ( )  { 
            return this.caravana ; 
        }
        
        public String getTrailer ( )  { 
            return this.trailer ; 
        }
        
        public String getUsuario ( )  { 
            return this.usuario ; 
        }
        
        public String getAgencia ( )  { 
            return this.agencia ; 
        }
        
        
        public String getFechaCreacion   ( )  {
            return this.fechaCreacion ; 
        }
   
        public String getOrigen ( )  { 
            return this.origen  ; 
        }
        
        
        public String getDestino ( )  {
            return this.destino       ; 
        }
      
   
    // 2.  Datos Conductor:
        public String getCedulaConductor ( )  { 
            return cedulaCond    ; 
        } 
        
        public String getNameConductor   ( )  { 
            return nameCond      ; 
        }
        
        public String getCiudadConductor ( )  {
            return ciudadCond    ; 
        }
        
        public String getDirConductor    ( )  { 
            return direccionCond ; 
        }
        
        public String getTelConductor    ( )  {
            return telefonoCond  ; 
        }
        
        
   
         
     // 5.  Equipos de Comunicacion:  
         public String getRadio    ( )  { 
             return radio      ; 
         }
         
         public String getAvantel  ( )  { 
             return avantel    ; 
         }
         
         public String getCazador  ( )  { 
             return cazador    ; 
         }  
         
         public String getCelular  ( )  { 
             return celular    ; 
         }
         
         public String getTelefono ( )  { 
             return telefono   ; 
         }
         
         public String getMovil    ( )  {
             return movil      ; 
         }
         
         public String getOtros    ( )  { 
             return otros      ; 
         }
         
             
   // 8. Femiliar
         public String getNameFamiliar      ( )  { 
             return nameFamiliar  ; 
         }
         
         public String getTelFamiliar       ( )  { 
             return telFamiliar   ; 
         }      
         
  // 9. Propietario
         public String getCedulaPropietario ( )  { 
             return cedulaProp    ; 
         } 
         
         public String getNamePropietario   ( )  { 
             return nameProp      ; 
         }
         
         public String getCiudadPropietario ( )  {
             return ciudadProp    ; 
         }
         
         public String getDirPropietario    ( )  { 
             return direccionProp ; 
         }
         
         public String getTelPropietario    ( )  { 
             return telefonoProp  ; 
         }   
        
         
  // 10. Comentarios   
         
         public String getComentario1     ( )  { 
             return comentario1     ; 
         } 
         
         public String getComentario2     ( )  { 
             return comentario2     ; 
         }
         
         public String getComentario3     ( )  { 
             return comentario3     ; 
         }
         
 
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
   /**
     * Metodo que setea el objeto con los parametros del formulario enviado por el request
     * @autor: ....... Fernel Villacob
     * @param ........ HttpServletRequest request
     * @version ...... 1.0
     */  
         
    public void LoadFormRequest( HttpServletRequest request )throws Exception{
        try{
        // Datos Basicos
            this.planilla       = request.getParameter("planilla");
            this.distrito       = request.getParameter("distrito");
            this.placa          = request.getParameter("placa");
            this.destinatario   = request.getParameter("destinatario");
            this.fechaDespacho  = request.getParameter("fechaDespacho");
            this.tipoCarga      = request.getParameter("tipoCarga");    
            this.productos      = request.getParameter("productos");
            this.placaUnidad    = request.getParameter("placaUnidad");
            this.contenedores   = request.getParameter("contenedores");
        //    this.ruta           = request.getParameter("ruta");
            this.retorno        = request.getParameter("hRetorno");            
         //   this.caravana       = request.getParameter("caravana");
            this.trailer        = request.getParameter("trailer");
            this.usuario        = request.getParameter("usuarioCreacion");
            this.fechaCreacion  = request.getParameter("fechaCreacion");
            this.fecha          = request.getParameter("fechaPlan");
            this.origen         = request.getParameter("origen");  
            this.destino        = request.getParameter("destino");  
            this.escolta        = request.getParameter("escolta");  
            this.estado         = reset(request.getParameter("statusPlanilla"));             
                        
            this.proximoRuta    = request.getParameter("ruta");
            this.proximoPC      = reset(request.getParameter("proximopc")); 
            this.proximoZona    = reset(request.getParameter("zona"));
            
            // conservar valores
            this.ruta           = request.getParameter("viaOriginal");  
            this.pcTrafico      = request.getParameter("pcOriginal"); 
            this.oricode        = request.getParameter("oriOriginal");             
            this.zona           = request.getParameter("zonaOriginal"); 
            this.nombreZona     = request.getParameter("zonaNameOriginal"); 
            
         // 2. Datos Conductor
            this.cedulaCond     = request.getParameter("cedulaCond");
            this.nameCond       = request.getParameter("nameCond");
            this.ciudadCond     = request.getParameter("ciudadCond");
            this.direccionCond  = request.getParameter("dirCond");
            this.telefonoCond   = request.getParameter("telCond");
            
        // 3. Equipos de Comunicacion
            this.radio          = request.getParameter("radio");
            this.avantel        = request.getParameter("avantel");
            this.cazador        = request.getParameter("cazador");
            this.celular        = request.getParameter("celular");
            this.telefono       = request.getParameter("telefono");
            this.movil          = request.getParameter("movil");
            this.otros          = request.getParameter("otros");
              
       //4. Ubicaciones     
            String[] alimentaciones   = request.getParameterValues("alimentacionPlan");
            String[] pernotaciones    = request.getParameterValues("pernotacionPlan");
            String[] parqueaderos     = request.getParameterValues("parqueaderoPlan");
            String[] tanqueos         = request.getParameterValues("tanqueoPlan");
                        
            this.listAlimentacionPlan = ubicaciones(alimentaciones);
            this.listPernotaderoPlan  = ubicaciones(pernotaciones);
            this.listParqueaderoPlan  = ubicaciones(parqueaderos);            
            this.listTanqueoPlan      = ubicaciones(tanqueos);
            
                        
         // 5. Femiliar
            this.nameFamiliar   = request.getParameter("nameFamiliar");
            this.telFamiliar    = request.getParameter("telFamiliar");

         // 6. Propietario
            this.cedulaProp     = request.getParameter("cedulaProp");
            this.nameProp       = request.getParameter("nameProp");
            this.ciudadProp     = request.getParameter("ciudadProp");
            this.direccionProp  = request.getParameter("dirProp");
            this.telefonoProp   = request.getParameter("telProp");

         // 7. Comentarios
            this.comentario1    = request.getParameter("comentario1");
            this.comentario2    = request.getParameter("comentario2");
            this.comentario3    = request.getParameter("comentario3");
            
        }catch(Exception e){
            throw new Exception( "LoadFormRequest: "+ e.getMessage() );
        }
    }
       
    
    
    
    
    /**
     * Metodo que devuelve una lista de ubicaciones segun el vector
     * @autor: ....... Fernel Villacob
     * @param ........ String[] vector
     * @version ...... 1.0
     */ 
    public List  ubicaciones(String[] vector)throws Exception{        
        List lista = new LinkedList();        
        try{
            if(vector!=null){
                for(int i=0; i< vector.length;i++){
                     String code = vector[i];
                     Ubicacion ub = new Ubicacion();
                              ub.setCod_ubicacion(code);
                              ub.setDescripcion(code);
                    lista.add(ub);
               } 
            }
        }catch(Exception e){
            throw new Exception("ubicaciones: "+e.getMessage());
        }
       return lista;
    }
    
    
    
    
    /**
     * Metodo que carga la lista de ubicaciones
     * @autor: ....... Fernel Villacob
     * @param ........ PlanDeViaje plan
     * @version ...... 1.0
     */ 
    public void loadUbicaciones(PlanDeViaje plan)throws Exception{ 
        try{            
              this.listAlimentacion = plan.getListAlimentacion();
              this.listParqueadero  = plan.getListParqueadero();
              this.listPernotadero  = plan.getListPernotacion();
              this.listTanqueo      = plan.getListTanqueo();
        }catch(Exception e){
            throw new Exception("ubicaciones: "+e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * Metodo que carga el objeto, Se llena de la consulta realizada sobre la tabla de planilla y demas para formar el plan de viaje
     * @autor: ....... Fernel Villacob
     * @param ........ ResultSet rs
     * @version ...... 1.0
     */ 
    public void loadFormat(ResultSet rs) throws Exception{
        try{
                this.planilla      =  reset( rs.getString("oc")            );
                this.distrito      =  reset( rs.getString("distrito")      );
                this.placa         =  reset( rs.getString("placa")         );
                this.destinatario  =  reset( rs.getString("destinatario")  );
                this.fechaDespacho =  reset( rs.getString("fechaDespacho") );
                this.placaUnidad   =  reset( rs.getString("placaUnidad")   );
                this.contenedores  =  reset( rs.getString("contenedores")  );
                this.origen        =  reset( rs.getString("origen")        );
                this.destino       =  reset( rs.getString("destino")       );                
                this.ruta          =  reset( rs.getString("ruta")          );
                this.tipoCarga     =  reset( rs.getString("desctipocarga") );                
                this.oricode       =  this.origen;
                this.descode       =  this.destino;                
                
                this.trailer       =  reset( rs.getString("trailer")       );
                this.caravana      =  reset( rs.getString("numcaravana")   );
                this.estado        =  reset( rs.getString("estado")        );
             
                this.cedulaCond    =  reset( rs.getString("cedulaCond")    );
                this.nameCond      =  reset( rs.getString("nameCond")      );
                this.ciudadCond    =  reset( rs.getString("ciudadCond")    );
                this.direccionCond =  reset( rs.getString("dirCond")       );
                this.telefonoCond  =  reset( rs.getString("telCond")       );
                this.celular       =  reset( rs.getString("celCond")       );
                
             
                this.cedulaProp    =  reset( rs.getString("propietario")   );
                this.nameProp      =  reset( rs.getString("nameProp")      );
                this.ciudadProp    =  reset( rs.getString("ciudadProp")    );
                this.direccionProp =  reset( rs.getString("dirProp")       );
                this.telefonoProp  =  reset( rs.getString("telProp")       );
            
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
    
    
    /**
     * Metodo que setea valores nulos
     * @autor: ....... Fernel Villacob
     * @param ........ String val
     * @version ...... 1.0
     */ 
    public String reset(String val){
        if( val==null  )
              val="";
        return val;
    }
    
    
    
    
    
    /**
     * Metodo que carga el objeto: Se llena de la consulta realizada sobre la tabla de plan_viaje
     * @autor: ....... Fernel Villacob
     * @param ........ String val
     * @version ...... 1.0
     */
     public void loadSearch(ResultSet rs) throws Exception{
        try{
                this.planilla      = reset( rs.getString("planilla")   );
                this.distrito      = reset( rs.getString("dstrct")     ); 
                this.fecha         = reset( rs.getString("fecha")      );
                this.cedulaCond    = reset( rs.getString("cedcon")     );                
                this.radio         = reset( rs.getString("radio")      );
                this.celular       = reset( rs.getString("celular")    );
                this.avantel       = reset( rs.getString("avantel")    );
                this.telefono      = reset( rs.getString("telefono")   );
                this.cazador       = reset( rs.getString("cazador")    );                
                this.movil         = reset( rs.getString("movil")      );
                this.otros         = reset( rs.getString("otro")       );                
                this.nameFamiliar  = reset( rs.getString("nomfam")     );
                this.telFamiliar   = reset( rs.getString("telfam")     );
                this.cedulaProp    = reset( rs.getString("nitpro")     );  
                this.comentario1   = reset( rs.getString("comentario") );
                this.retorno       = reset( rs.getString("retorno")    );
                this.leido         = reset( rs.getString("leido")      );
                this.productos     = reset( rs.getString("productos")  );
                this.agencia       = reset( rs.getString("agencia")    );
                this.usuario       = reset( rs.getString("usuario")    );
                this.fechaCreacion = reset( rs.getString("creation_date"));
        }catch(Exception e){
             throw new Exception( e.getMessage() );
        }
    }
    
    
    
     
     
     
      /**
     * Metodo que complemeta el objeto  obtenido de la tabla Plan de Viaje con datos de la Planilla
     * @autor: ....... Fernel Villacob
     * @param ........ PlanDeViaje plan
     * @version ...... 1.0
     */
      public void complemet(PlanDeViaje plan)throws Exception{
          try{
                  this.zona             = plan.getZona();
                  this.nombreZona       = plan.getNombreZona();
                  this.pcTrafico        = plan.getPCTrafico();
                  this.nombrepcTrafico  = plan.getNombrePCTrafico();
                  this.oricode          = plan.getOrigenCode();
                  this.descode          = plan.getDestinoCode();
                  this.vias             = plan.getVias();
                  this.placa            = plan.getPlaca();
                  this.escolta          = plan.getEscolta();
                  this.trailer          = plan.getTrailer();
                  this.contenedores     = plan.getContenedores();
                  this.fechaDespacho    = plan.getFechaDespacho();
                  this.destinatario     = plan.getDestinatario();
                  this.ruta             = plan.getRuta();
                  this.origen           = plan.getOrigen();
                  this.destino          = plan.getDestino();
                  this.nameCond         = plan.getNameConductor();
                  this.direccionCond    = plan.getDirConductor();
                  this.telefonoCond     = plan.getTelConductor();
                  this.ciudadCond       = plan.getCiudadConductor();          
                  this.nameProp         = plan.getNamePropietario();
                  this.direccionProp    = plan.getDirPropietario();
                  this.telefonoProp     = plan.getTelPropietario();
                  this.ciudadProp       = plan.getCiudadPropietario();          
                  this.tipoCarga        = plan.getTipoCarga();
                  this.caravana         = plan.getCaravana();
                  this.placaUnidad      = plan.getPlacaUnidad();
                  this.celular          = plan.getCelular();
                  this.telefono         = plan.getTelConductor();
                  this.estado           = plan.getEstado();
                  this.fecha            = plan.getFechaPlanViaje();
                  
                  loadUbicaciones(plan);
          }catch(Exception e){
              throw new Exception(e.getMessage());
          }
      } 
     
    
}
