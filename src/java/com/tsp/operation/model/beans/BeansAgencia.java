/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author user
 */
public class BeansAgencia {
    private Integer id;
    private String cod_agencia;
    private String nombre_agencia;
    private String id_transportadora;
    private String cod_transportadora;
    private String user_trans;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    /**
     * @return the cod_agencia
     */
    public String getCod_agencia() {
        return cod_agencia;
    }

    /**
     * @param cod_agencia the cod_agencia to set
     */
    public void setCod_agencia(String cod_agencia) {
        this.cod_agencia = cod_agencia;
    }

    /**
     * @return the nombre_agencia
     */
    public String getNombre_agencia() {
        return nombre_agencia;
    }

    /**
     * @param nombre_agencia the nombre_agencia to set
     */
    public void setNombre_agencia(String nombre_agencia) {
        this.nombre_agencia = nombre_agencia;
    }

    /**
     * @return the id_transportadora
     */
    public String getId_transportadora() {
        return id_transportadora;
    }

    /**
     * @param id_transportadora the id_transportadora to set
     */
    public void setId_transportadora(String id_transportadora) {
        this.id_transportadora = id_transportadora;
    }

    /**
     * @return the cod_transportadora
     */
    public String getCod_transportadora() {
        return cod_transportadora;
    }

    /**
     * @param cod_transportadora the cod_transportadora to set
     */
    public void setCod_transportadora(String cod_transportadora) {
        this.cod_transportadora = cod_transportadora;
    }

    /**
     * @return the user_trans
     */
    public String getUser_trans() {
        return user_trans;
    }

    /**
     * @param user_trans the user_trans to set
     */
    public void setUser_trans(String user_trans) {
        this.user_trans = user_trans;
    }
    
    
}
