/*
* Nombre        GrupoEquipo.java
* Descripci�n   Javabean que representa un grupo de equipo en SOT.
* Autor         Ivan Herazo
* Fecha         29 de marzo de 2005, 05:41 PM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;

/**
 * Javabean que representa un grupo de equipo en SOT.
 * @author  Ivan Herazo
 */
public class GrupoEquipo implements Serializable
{
  private String grupoTrabajo;
  private String descGrupoTrabajo;
  
  /** Crea una instancia de esta clase */
  public GrupoEquipo()
  {
    this.grupoTrabajo = "";
    this.descGrupoTrabajo = "";
  }
  
  /**
   * Getter for property grupoTrabajo.
   * @return Value of property grupoTrabajo.
   */
  public String getGrupoTrabajo() {
    return this.grupoTrabajo;
  }
  
  /**
   * Setter for property grupoTrabajo.
   * @param grupoTrabajo New value of property grupoTrabajo.
   */
  public void setGrupoTrabajo(String grupoTrabajo) {
    this.grupoTrabajo = grupoTrabajo;
  }
  
  /**
   * Getter for property descGrupoEquipo.
   * @return Value of property descGrupoEquipo.
   */
  public String getDescGrupoTrabajo() {
    return this.descGrupoTrabajo;
  }
  
  /**
   * Setter for property descGrupoEquipo.
   * @param descGrupoEquipo New value of property descGrupoEquipo.
   */
  public void setDescGrupoTrabajo(String descGrupoTrabajo) {
    this.descGrupoTrabajo = descGrupoTrabajo;
  }
  
  /**
   * Metodo factor�a que carga en memoria los datos de un registro
   * extra�do de la base de datos.
   * @param rset Registro a cargar.
   * @return Objeto con los datos cargados en memoria.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   */
  public static GrupoEquipo load(final ResultSet rset) throws SQLException
  {
    String value = null;
    GrupoEquipo grupoEq = new GrupoEquipo();
    value = rset.getString("work_group");
    if( !rset.wasNull() ) grupoEq.setGrupoTrabajo(value);

    value = rset.getString("work_grp_desc");
    if( !rset.wasNull() ) grupoEq.setDescGrupoTrabajo(value);
    return grupoEq;
  }
}
