package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla negocios_trazabilidad<br/>
 * 25/11/2011
 * @author ivargas-Geotech
 */
public class NegocioTrazabilidad {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String actividad;
    private String usuario;
    private String fecha;
    private String codNeg;
    private String comentarios;
    private String concepto;
    private String causal;
    private String comentario_standby;

    public NegocioTrazabilidad load(ResultSet rs) throws SQLException {
        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
        negtraza.setNumeroSolicitud(rs.getString("numero_solicitud")!=null?rs.getString("numero_solicitud"):"");
        negtraza.setActividad(rs.getString("actividad")!=null?rs.getString("actividad"):"");
        negtraza.setUsuario(rs.getString("usuario")!=null?rs.getString("usuario"):"");
        negtraza.setFecha(rs.getString("fecha")!=null?rs.getString("fecha"):"");
        negtraza.setCodNeg(rs.getString("cod_neg")!=null?rs.getString("cod_neg"):"");
        negtraza.setComentarios(rs.getString("comentarios")!=null?rs.getString("comentarios"):"");
        negtraza.setComentario_standby(rs.getString("comentario_stby")!=null?rs.getString("comentario_stby"):"");
        negtraza.setConcepto(rs.getString("concepto")!=null?rs.getString("concepto"):"");
        negtraza.setCausal(rs.getString("causal")!=null?rs.getString("causal"):"");
        return negtraza;
    }



    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getCausal() {
        return causal;
    }

    public void setCausal(String causal) {
        this.causal = causal;
    }

    public String getCodNeg() {
        return codNeg;
    }

    public void setCodNeg(String codNeg) {
        this.codNeg = codNeg;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the comentario_standby
     */
    public String getComentario_standby() {
        return comentario_standby;
    }

    /**
     * @param comentario_standby the comentario_standby to set
     */
    public void setComentario_standby(String comentario_standby) {
        this.comentario_standby = comentario_standby;
    }
}
