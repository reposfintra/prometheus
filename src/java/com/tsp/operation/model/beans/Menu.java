/*
 * Nombre        Menu.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         26 de abril de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Administrador
 */
public class Menu implements Serializable {
    
    private int idop;
    private int nivel;
    private int idpadre;
    private String descripcion;
    private int submenu_programa;
    private String url;
    private String nombre;    
    private String user_update;
    private String creation_user;
    private int orden;    
    //
    private int padre;
    private String pnombre;
    private String porden;
        
    /** Creates a new instance of Menu */
    public static Menu load ( ResultSet rs ) throws SQLException {
        
        Menu menu= new Menu();
        
        menu.setIdopcion(rs.getInt("id_opcion"));
        menu.setNivel(rs.getInt("nivel"));
        menu.setIdpadre(rs.getInt("id_padre"));
        menu.setDescripcion(rs.getString("descripcion"));        
        menu.setSubmenu(rs.getInt("submenu_programa"));
        menu.setUrl(rs.getString("url"));
        menu.setNombre(rs.getString("nombre"));
        menu.setOrden(rs.getInt("orden"));
        menu.setCreado_por(rs.getString("creation_user"));
        return menu;
    }
    
    /**
     * Setter for property orden.
     * @param p New value of property orden
     */
    public void setOrden (int o){
        this.orden = o;
    }
    
    /**
     * Setter for property idop.
     * @param p New value of property idop.
     */
    public void setIdopcion ( int idopcion ){
        this.idop = idopcion;
    }
    
    /**
     * Setter for property nivel.
     * @param p New value of property nivel.
     */
    public void setNivel ( int nivel ){
        this.nivel = nivel;
    }
    
    /**
     * Setter for property idpadre.
     * @param p New value of property idpadre.
     */
    public void setIdpadre (int idpadre ){
        this.idpadre = idpadre;
    }
    
    /**
     * Setter for property descripcion.
     * @param p New value of property descripcion.
     */
    public void setDescripcion (String descripcion ){
        this.descripcion = descripcion;
    }
    
    /**
     * Setter for property submenu_programa.
     * @param p New value of property submenu_programa.
     */
    public void setSubmenu (int sm){
        this.submenu_programa = sm;
    }
    
    /**
     * Setter for property url.
     * @param p New value of property url.
     */
    public void setUrl ( String url ){
        this.url = url;
    }
    
    /**
     * Setter for property nombre.
     * @param p New value of property nombre.
     */
    public void setNombre (String nom ){
        this.nombre = nom;
    }
    
    /**
     * Setter for property creation_user.
     * @param p New value of property creation_user.
     */
    public void setCreado_por ( String creado_por ){
        this.creation_user = creado_por;
    }
    
    /**
     * Setter for property user_update.
     * @param p New value of property user_update.
     */
    public void setUser_update ( String uu ){
        this.user_update = uu;
    }
    
    /**
     * Getter for property orden.
     * @return Value of property orden.
     */
    public int getOrden (){
        return orden;
    }
    
    /**
     * Getter for property idop.
     * @return Value of property idop.
     */
    public int getIdopcion ( ){
        return idop;
    }
    
    /**
     * Getter for property nivel.
     * @return Value of property nivel.
     */
    public int getNivel ( ){
        return nivel;
    }
    
    /**
     * Getter for property idpadre  .
     * @return Value of property idpadre.
     */
    public int getIdpadre ( ){
        return idpadre;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public String getDescripcion ( ){
        return descripcion;
    }
    
    /**
     * Getter for property submenu_programa.
     * @return Value of property submenu_programa.
     */
    public int getSubmenu (){
        return submenu_programa;
    }
    
    /**
     * Getter for property url.
     * @return Value of property url.
     */
    public String getUrl ( ){
        return url;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public String getNombre ( ){
        return nombre;
    }
    
    /**
     * Getter for property getCreado_por.
     * @return Value of property getCreado_por.
     */
    public String getCreado_por ( ){
        return creation_user;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public String getUser_update ( ){
        return user_update;
    }
    
    /**
     * Getter for property pnombre.
     * @return Value of property pnombre.
     */
    public java.lang.String getPnombre() {
        return pnombre;
    }
    
    /**
     * Setter for property pnombre.
     * @param pnombre New value of property pnombre.
     */
    public void setPnombre(java.lang.String pnombre) {
        this.pnombre = pnombre;
    }
    
    /**
     * Getter for property porden.
     * @return Value of property porden.
     */
    public java.lang.String getPorden() {
        return porden;
    }
    
    /**
     * Setter for property porden.
     * @param porden New value of property porden.
     */
    public void setPorden(java.lang.String porden) {
        this.porden = porden;
    }
    
    /**
     * Getter for property padre.
     * @return Value of property padre.
     */
    public int getPadre() {
        return padre;
    }
    
    /**
     * Setter for property padre.
     * @param padre New value of property padre.
     */
    public void setPadre(int padre) {
        this.padre = padre;
    }
    
}    
