   /***************************************
    * Nombre Clase ............. ItemLiquidacion.java
    * Descripci�n  .. . . . . .  Permite Guardar inf. de los movimientos de la planilla
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/04/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.beans;


import java.io.*;
import java.util.Hashtable;



public class ItemLiquidacion {
    
    
    private String  item;
    private String  concepto;    
    private String  descripcion;    
    private double  valor;
    private String  moneda;    
    private String  indicador;
    private String  asignador;
    
    private double  reteFuente;
    private double  reteIca;
    private double  saldo;
    
    public  Hashtable impuestos;
    
    
    public ItemLiquidacion() {
        item           = "";
        concepto       = ""; 
        descripcion    = "";
        valor          = 0; 
        moneda         = "";    
        indicador      = "";
        asignador      = "";
        reteFuente     = 0;
        reteIca        = 0;
        saldo          = 0;
        impuestos      = new Hashtable();
    }
    
    
    
    
    
    // SET AND GET :
    /**
     * Getter for property asignador.
     * @return Value of property asignador.
     */
    public java.lang.String getAsignador() {
        return asignador;
    }    
    
    /**
     * Setter for property asignador.
     * @param asignador New value of property asignador.
     */
    public void setAsignador(java.lang.String asignador) {
        this.asignador = asignador;
    }    
    
    
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }    
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    } 
    
    
    
    /**
     * Getter for property indicador.
     * @return Value of property indicador.
     */
    public java.lang.String getIndicador() {
        return indicador;
    }
    
    /**
     * Setter for property indicador.
     * @param indicador New value of property indicador.
     */
    public void setIndicador(java.lang.String indicador) {
        this.indicador = indicador;
    }
    
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    
    
    /**
     * Getter for property reteFuente.
     * @return Value of property reteFuente.
     */
    public double getReteFuente() {
        return reteFuente;
    }    
    
    /**
     * Setter for property reteFuente.
     * @param reteFuente New value of property reteFuente.
     */
    public void setReteFuente(double reteFuente) {
        this.reteFuente = reteFuente;
    }    
    
    
    
    /**
     * Getter for property reteIca.
     * @return Value of property reteIca.
     */
    public double getReteIca() {
        return reteIca;
    }
    
    /**
     * Setter for property reteIca.
     * @param reteIca New value of property reteIca.
     */
    public void setReteIca(double reteIca) {
        this.reteIca = reteIca;
    }
    
    
    
    /**
     * Getter for property saldo.
     * @return Value of property saldo.
     */
    public double getSaldo() {
        return saldo;
    }    
    
    /**
     * Setter for property saldo.
     * @param saldo New value of property saldo.
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }    
    
    
    
    /**
     * Getter for property impuestos.
     * @return Value of property impuestos.
     */
    public java.util.Hashtable getImpuestos() {
        return impuestos;
    }    
   
    /**
     * Setter for property impuestos.
     * @param impuestos New value of property impuestos.
     */
    public void setImpuestos(java.util.Hashtable impuestos) {
        this.impuestos = impuestos;
    }
    
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
}
