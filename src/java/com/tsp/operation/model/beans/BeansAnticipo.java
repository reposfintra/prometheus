/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author user
 */
public class BeansAnticipo {
    
    private String codigo_empresa;
    private String codigo_agencia;
    private String placa;
    private String tipo_doc_conductor;
    private String cedula_conductor;
    private String tipo_doc_intermediario;
    private String cedula_intermediario;
    private String codigo_producto;
    private String origen;
    private String destino;
    private String planilla;
    private Float valor_planilla;
    private Float valor_neto_anticipo;
    private Float porc_comision_intermediario;
    private Float valor_comision_intermediario;
    private String fecha_envio_fintra;
    private String fecha_creacion_anticipo;
    private String banco;
    private String sucursal;
    private String cedula_titular_cuenta;
    private String nombre_titular_cuenta;
    private String tipo_cuenta;
    private String no_cuenta;

    /**
     * @return the codigo_empresa
     */
    public String getCodigo_empresa() {
        return codigo_empresa;
    }

    /**
     * @param codigo_empresa the codigo_empresa to set
     */
    public void setCodigo_empresa(String codigo_empresa) {
        this.codigo_empresa = codigo_empresa;
    }

    /**
     * @return the codigo_agencia
     */
    public String getCodigo_agencia() {
        return codigo_agencia;
    }

    /**
     * @param codigo_agencia the codigo_agencia to set
     */
    public void setCodigo_agencia(String codigo_agencia) {
        this.codigo_agencia = codigo_agencia;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the tipo_doc_conductor
     */
    public String getTipo_doc_conductor() {
        return tipo_doc_conductor;
    }

    /**
     * @param tipo_doc_conductor the tipo_doc_conductor to set
     */
    public void setTipo_doc_conductor(String tipo_doc_conductor) {
        this.tipo_doc_conductor = tipo_doc_conductor;
    }

    /**
     * @return the cedula_conductor
     */
    public String getCedula_conductor() {
        return cedula_conductor;
    }

    /**
     * @param cedula_conductor the cedula_conductor to set
     */
    public void setCedula_conductor(String cedula_conductor) {
        this.cedula_conductor = cedula_conductor;
    }

    /**
     * @return the tipo_doc_intermediario
     */
    public String getTipo_doc_intermediario() {
        return tipo_doc_intermediario;
    }

    /**
     * @param tipo_doc_intermediario the tipo_doc_intermediario to set
     */
    public void setTipo_doc_intermediario(String tipo_doc_intermediario) {
        this.tipo_doc_intermediario = tipo_doc_intermediario;
    }

    /**
     * @return the cedula_intermediario
     */
    public String getCedula_intermediario() {
        return cedula_intermediario;
    }

    /**
     * @param cedula_intermediario the cedula_intermediario to set
     */
    public void setCedula_intermediario(String cedula_intermediario) {
        this.cedula_intermediario = cedula_intermediario;
    }

    /**
     * @return the codigo_producto
     */
    public String getCodigo_producto() {
        return codigo_producto;
    }

    /**
     * @param codigo_producto the codigo_producto to set
     */
    public void setCodigo_producto(String codigo_producto) {
        this.codigo_producto = codigo_producto;
    }

    /**
     * @return the origen
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * @param origen the origen to set
     */
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    }

    /**
     * @return the planilla
     */
    public String getPlanilla() {
        return planilla;
    }

    /**
     * @param planilla the planilla to set
     */
    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    /**
     * @return the valor_planilla
     */
    public Float getValor_planilla() {
        return valor_planilla;
    }

    /**
     * @param valor_planilla the valor_planilla to set
     */
    public void setValor_planilla(Float valor_planilla) {
        this.valor_planilla = valor_planilla;
    }

    /**
     * @return the valor_neto_anticipo
     */
    public Float getValor_neto_anticipo() {
        return valor_neto_anticipo;
    }

    /**
     * @param valor_neto_anticipo the valor_neto_anticipo to set
     */
    public void setValor_neto_anticipo(Float valor_neto_anticipo) {
        this.valor_neto_anticipo = valor_neto_anticipo;
    }

    /**
     * @return the porc_comision_intermediario
     */
    public Float getPorc_comision_intermediario() {
        return porc_comision_intermediario;
    }

    /**
     * @param porc_comision_intermediario the porc_comision_intermediario to set
     */
    public void setPorc_comision_intermediario(Float porc_comision_intermediario) {
        this.porc_comision_intermediario = porc_comision_intermediario;
    }

    /**
     * @return the valor_comision_intermediario
     */
    public Float getValor_comision_intermediario() {
        return valor_comision_intermediario;
    }

    /**
     * @param valor_comision_intermediario the valor_comision_intermediario to set
     */
    public void setValor_comision_intermediario(Float valor_comision_intermediario) {
        this.valor_comision_intermediario = valor_comision_intermediario;
    }

    /**
     * @return the fecha_envio_fintra
     */
    public String getFecha_envio_fintra() {
        return fecha_envio_fintra;
    }

    /**
     * @param fecha_envio_fintra the fecha_envio_fintra to set
     */
    public void setFecha_envio_fintra(String fecha_envio_fintra) {
        this.fecha_envio_fintra = fecha_envio_fintra;
    }

    /**
     * @return the fecha_creacion_anticipo
     */
    public String getFecha_creacion_anticipo() {
        return fecha_creacion_anticipo;
    }

    /**
     * @param fecha_creacion_anticipo the fecha_creacion_anticipo to set
     */
    public void setFecha_creacion_anticipo(String fecha_creacion_anticipo) {
        this.fecha_creacion_anticipo = fecha_creacion_anticipo;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getCedula_titular_cuenta() {
        return cedula_titular_cuenta;
    }

    public void setCedula_titular_cuenta(String cedula_titular_cuenta) {
        this.cedula_titular_cuenta = cedula_titular_cuenta;
    }

    public String getNombre_titular_cuenta() {
        return nombre_titular_cuenta;
    }

    public void setNombre_titular_cuenta(String nombre_titular_cuenta) {
        this.nombre_titular_cuenta = nombre_titular_cuenta;
    }

    public String getTipo_cuenta() {
        return tipo_cuenta;
    }

    public void setTipo_cuenta(String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    public String getNo_cuenta() {
        return no_cuenta;
    }

    public void setNo_cuenta(String no_cuenta) {
        this.no_cuenta = no_cuenta;
    }
    
    

}
