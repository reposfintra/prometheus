/*
 * Auto_Anticipo.java
 *
 * Created on 18 de junio de 2005, 11:57 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Sandrameg
 */
public class Auto_Anticipo {
    
    public String placa;
    public double efectivo;
    public double acpm;
    public int ticket_a;
    public int ticket_b;
    public int ticket_c;
    public String creation_user;
    public String user_update;
    public String base;
    public String sj;
    //////gets
    public String getPlaca(){
        return placa;
    }
    
    public double getEfectivo(){
        return efectivo;
    }
    
    public double getacpm(){
        return acpm;
    }
    
    public int getTicket_a(){
        return ticket_a;
    }
    
    public int getTicket_b(){
        return ticket_b;
    }
    
    public int getTicket_c(){
        return ticket_c;
    }
    
    public String getCreation_user(){
        return creation_user;
    }
    
    public String getUser_update(){
        return user_update;
    }
    
    ///////sets
    public void setPlaca(String pla){
        this.placa = pla;
    }
    
    public void setEfectivo (double ef){
        this.efectivo = ef;
    }
    
    public void setacpm(double acpm){
        this.acpm = acpm;
    }
    
    public void setTicket_a(int ta){
        this.ticket_a = ta;
    }
    
    public void setTicket_b(int tb){
        this.ticket_b = tb;
    }

    public void setTicket_c(int tc){
        this.ticket_c = tc;
    }
    
    public void setCreation_user(String user){
        this.creation_user = user;
    }
    
    public void setUser_update(String user){
        this.user_update = user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property sj.
     * @return Value of property sj.
     */
    public java.lang.String getSj() {
        return sj;
    }
    
    /**
     * Setter for property sj.
     * @param sj New value of property sj.
     */
    public void setSj(java.lang.String sj) {
        this.sj = sj;
    }
    
}
