/**************************************************************************
 * Nombre:        Referencias.java                     
 * Descripci�n:   Beans de acuerdo especial.       
 * Autor:         Ing. Henry Osorio
 * Fecha:         24 de septiembre de 2005, 13:24       
 * Versi�n:       Java  1.0                             
 * Copyright:     Fintravalores S.A. S.A.          
 **************************************************************************/
package com.tsp.operation.model.beans;

public class Referencia {
    private String dstrct_code;
    private String tiporef;
    private String fecref;
    private String clase;
    private String nomempresa;
    private String telefono;
    private String ciudadtel;
    private String direccion;
    private String ciudaddir;    
    private String contacto;    
    private String cargo_contacto;
    private String relacion;
    private String referencia;  
    private String usrconfirma;
    private String fecconfir;
    private String reg_status;
    private String creation_user;
    private String creation_date;
    private String last_update;
    private String user_update;
    private String base;
    
    private String ref_confirmacion;
    private String otra_relacion;
    
    /** Creates a new instance of Referencias */
    public Referencia() {
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
   
    
    /**
     * Getter for property tiporef.
     * @return Value of property tiporef.
     */
    public java.lang.String getTiporef() {
        return tiporef;
    }
    
    /**
     * Setter for property tiporef.
     * @param tiporef New value of property tiporef.
     */
    public void setTiporef(java.lang.String tiporef) {
        this.tiporef = tiporef;
    }
    
    /**
     * Getter for property clase.
     * @return Value of property clase.
     */
    public java.lang.String getClase() {
        return clase;
    }
    
    /**
     * Setter for property clase.
     * @param clase New value of property clase.
     */
    public void setClase(java.lang.String clase) {
        this.clase = clase;
    }
    
   
    
    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public java.lang.String getTelefono() {
        return telefono;
    }
    
    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }
    
  
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
   
    /**
     * Getter for property contacto.
     * @return Value of property contacto.
     */
    public java.lang.String getContacto() {
        return contacto;
    }
    
    /**
     * Setter for property contacto.
     * @param contacto New value of property contacto.
     */
    public void setContacto(java.lang.String contacto) {
        this.contacto = contacto;
    }
    
  
    /**
     * Getter for property usrconfirma.
     * @return Value of property usrconfirma.
     */
    public java.lang.String getUsrconfirma() {
        return usrconfirma;
    }
    
    /**
     * Setter for property usrconfirma.
     * @param usrconfirma New value of property usrconfirma.
     */
    public void setUsrconfirma(java.lang.String usrconfirma) {
        this.usrconfirma = usrconfirma;
    }
    
    /**
  
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property dstrct_code.
     * @return Value of property dstrct_code.
     */
    public java.lang.String getDstrct_code() {
        return dstrct_code;
    }
    
    /**
     * Setter for property dstrct_code.
     * @param dstrct_code New value of property dstrct_code.
     */
    public void setDstrct_code(java.lang.String dstrct_code) {
        this.dstrct_code = dstrct_code;
    }
    
    /**
     * Getter for property ciudadtel.
     * @return Value of property ciudadtel.
     */
    public java.lang.String getCiudadtel() {
        return ciudadtel;
    }
    
    /**
     * Setter for property ciudadtel.
     * @param ciudadtel New value of property ciudadtel.
     */
    public void setCiudadtel(java.lang.String ciudadtel) {
        this.ciudadtel = ciudadtel;
    }
    
    /**
     * Getter for property ciudaddir.
     * @return Value of property ciudaddir.
     */
    public java.lang.String getCiudaddir() {
        return ciudaddir;
    }
    
    /**
     * Setter for property ciudaddir.
     * @param ciudaddir New value of property ciudaddir.
     */
    public void setCiudaddir(java.lang.String ciudaddir) {
        this.ciudaddir = ciudaddir;
    }
    
    /**
     * Getter for property relacion.
     * @return Value of property relacion.
     */
    public java.lang.String getRelacion() {
        return relacion;
    }
    
    /**
     * Setter for property relacion.
     * @param relacion New value of property relacion.
     */
    public void setRelacion(java.lang.String relacion) {
        this.relacion = relacion;
    }
    
    /**
     * Getter for property referencia.
     * @return Value of property referencia.
     */
    public java.lang.String getReferencia() {
        return referencia;
    }
    
    /**
     * Setter for property referencia.
     * @param referencia New value of property referencia.
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }
    
    /**
     * Getter for property cargo_contacto.
     * @return Value of property cargo_contacto.
     */
    public java.lang.String getCargo_contacto() {
        return cargo_contacto;
    }
    
    /**
     * Setter for property cargo_contacto.
     * @param cargo_contacto New value of property cargo_contacto.
     */
    public void setCargo_contacto(java.lang.String cargo_contacto) {
        this.cargo_contacto = cargo_contacto;
    }
    
    /**
     * Getter for property fecref.
     * @return Value of property fecref.
     */
    public java.lang.String getFecref() {
        return fecref;
    }
    
    /**
     * Setter for property fecref.
     * @param fecref New value of property fecref.
     */
    public void setFecref(java.lang.String fecref) {
        this.fecref = fecref;
    }
    
    /**
     * Getter for property nomempresa.
     * @return Value of property nomempresa.
     */
    public java.lang.String getNomempresa() {
        return nomempresa;
    }
    
    /**
     * Setter for property nomempresa.
     * @param nomempresa New value of property nomempresa.
     */
    public void setNomempresa(java.lang.String nomempresa) {
        this.nomempresa = nomempresa;
    }
    
    /**
     * Getter for property fecconfir.
     * @return Value of property fecconfir.
     */
    public java.lang.String getFecconfir() {
        return fecconfir;
    }
    
    /**
     * Setter for property fecconfir.
     * @param fecconfir New value of property fecconfir.
     */
    public void setFecconfir(java.lang.String fecconfir) {
        this.fecconfir = fecconfir;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }    
    
    /**
     * Getter for property ref_confirmacion.
     * @return Value of property ref_confirmacion.
     */
    public java.lang.String getRef_confirmacion() {
        return ref_confirmacion;
    }
    
    /**
     * Setter for property ref_confirmacion.
     * @param ref_confirmacion New value of property ref_confirmacion.
     */
    public void setRef_confirmacion(java.lang.String ref_confirmacion) {
        this.ref_confirmacion = ref_confirmacion;
    }
    
    /**
     * Getter for property otra_relacion.
     * @return Value of property otra_relacion.
     */
    public java.lang.String getOtra_relacion() {
        return otra_relacion;
    }
    
    /**
     * Setter for property otra_relacion.
     * @param otra_relacion New value of property otra_relacion.
     */
    public void setOtra_relacion(java.lang.String otra_relacion) {
        this.otra_relacion = otra_relacion;
    }
    
}
