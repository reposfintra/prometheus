/*
 * Nombre        Tramo.java 
 * Modificado    Ing Sandra Escalante
 * Fecha         30 de junio de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Tramo implements Serializable {
    
    private String dstrct;//cia
    private String origin;//origen
    private String destination;//destino
    private String originn;
    private String destinationn;
    private String ticket_id1;
    private String ticket_id2;
    private String ticket_id3;
    private String ticket_id4;
    private String ticket_id5;
    private String ticket_id6;
    private float cantidad_maximo_ticket1;
    private float cantidad_maximo_ticket2;
    private float cantidad_maximo_ticket3;
    private float cantidad_maximo_ticket4;
    private float cantidad_maximo_ticket5;
    private float cantidad_maximo_ticket6;
    private float acpm;//consumo_combustible
    private float km;//distancia
    private float tiempo;//duracion
    
    //PLANEACION
    private String distrito;
    private String origen;
    private String destino;    
    
    //TRAFICO
    private String cia;
    private String ncia;    
    private String norigen;
    private String ndestino;
    private String unidad_consumo;
    private String unidad_distancia;
    private String unidad_duracion;
    private String usuario;
    private float consumo_combustible;
    private float duracion;
    private float distancia;
    private String Scombustible;
    private String Sduracion;
    private String Sdistancia;
    private String paiso;
    private String paisd;
    private String base;
    
    /**
     * Metodo <tt>load</tt>, instancia el objeto Tramo con la inforacion obtenida
     * @autor : Ing. Jesus Cuestas          
     * @version : 1.0
     */
    public static Tramo load(ResultSet rs)throws SQLException{
        
        Tramo tramo = new Tramo();
        
        tramo.setDstrct(rs.getString(2));
        tramo.setOrigin(rs.getString(3));
        tramo.setDestination(rs.getString(4));
        tramo.setTicket_id1(rs.getString(11));
        tramo.setTicket_id2(rs.getString(12));
        tramo.setTicket_id3(rs.getString(13));
        tramo.setTicket_id4(rs.getString(14));
        tramo.setTicket_id5(rs.getString(15));
        tramo.setTicket_id6(rs.getString(16));
        tramo.setCantidad_maximo_ticket1(rs.getFloat(17));
        tramo.setCantidad_maximo_ticket2(rs.getFloat(18));
        tramo.setCantidad_maximo_ticket3(rs.getFloat(19));
        tramo.setCantidad_maximo_ticket4(rs.getFloat(20));
        tramo.setCantidad_maximo_ticket5(rs.getFloat(21));
        tramo.setCantidad_maximo_ticket6(rs.getFloat(22));
        tramo.setAcpm(rs.getFloat(23));
        tramo.setKm(rs.getFloat("km"));
        tramo.setTime(rs.getFloat("tiempo"));
        tramo.setOriginn(rs.getString("origen"));
        tramo.setDestinationn(rs.getString("destino"));
        
        return tramo;
    }
        
    /**
     * Setter for property dstrct
     * @param destino New value of property dstrct
     */    
    public void setDstrct(String dstrct){
        this.dstrct=dstrct;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct
     */
    public String getDstrct(){
        return dstrct;
    }
    
    /**
     * Setter for property origin
     * @param destino New value of property origin
     */
    public void setOrigin(String origin){
        this.origin=origin;
    }

    /**
     * Getter for property origin
     * @return Value of property origin
     */
    public String  getOrigin(){
        return origin;
    }
    
    /**
     * Setter for property destination
     * @param destino New value of property destination
     */
    public void  setDestination(String destination){
        this.destination=destination;
    }
    
    /**
     * Getter for property destination
     * @return Value of property destination.
     */
    public String getDestination(){
        return destination;
    }

    /**
     * Setter for property originn
     * @param destino New value of property originn
     */
    public void setOriginn(String originn){
        this.originn=originn;
    }
    
    /**
     * Getter for property originn
     * @return Value of property originn
     */
    public String  getOriginn(){
        return originn;
    }
    
    /**
     * Setter for property destinationn
     * @param destino New value of property destinationn
     */
    public void  setDestinationn(String destinationn){
        this.destinationn=destinationn;
    }
    
    /**
     * Getter for property destinationn
     * @return Value of property destinationn.
     */
    public String getDestinationn(){
        return destinationn;
    }
    
    /**
     * Setter for property ticket_id1
     * @param destino New value of property ticket_id1
     */
    public void setTicket_id1(String ticket_id1){
        this.ticket_id1=ticket_id1;
    }
    
    /**
     * Getter for property ticket_id1
     * @return Value of property ticket_id1
     */
    public String getTicket_id1(){
        return ticket_id1;
    }
    
    /**
     * Setter for property ticket_id2
     * @param destino New value of property ticket_id2
     */
    public void setTicket_id2(String ticket_id2){
        this.ticket_id2=ticket_id2;
    }
    
    /**
     * Getter for property ticket_id2
     * @return Value of property ticket_id2
     */
    public String getTicket_id2(){
        return ticket_id2;
    }
    
    /**
     * Setter for property ticket_id3
     * @param destino New value of property ticket_id3
     */
    public void  setTicket_id3(String ticket_id3){
        this.ticket_id3=ticket_id3;
    }
    
    /**
     * Getter for property ticket_id3
     * @return Value of property ticket_id3
     */
    public String getTicket_id3(){
        return ticket_id3;
    }
    
    /**
     * Setter for property ticket_id4
     * @param destino New value of property ticket_id4
     */
    public void  setTicket_id4(String ticket_id4){
        this.ticket_id4=ticket_id4;
    }
    
    /**
     * Getter for property ticket_id4
     * @return Value of property ticket_id4
     */
    public String getTicket_id4(){
        return ticket_id4;
    }
    
    /**
     * Setter for property ticket_id5
     * @param destino New value of property ticket_id5
     */
    public void setTicket_id5(String ticket_id5){
        this.ticket_id5=ticket_id5;
    }

    /**
     * Getter for property ticket_id5
     * @return Value of property ticket_id5
     */
    public String getTicket_id5(){
        return ticket_id5;
    }
    
    /**
     * Setter for property ticket_id6
     * @param destino New value of property ticket_id6
     */
    public void  setTicket_id6(String ticket_id6){
        this.ticket_id6=ticket_id6;
    }
    
    /**
     * Getter for property ticket_id6
     * @return Value of property ticket_id6
     */
    public String getTicket_id6(){
        return ticket_id6;
    }
    
    /**
     * Setter for property cantidad_maximo_ticket1
     * @param destino New value of property cantidad_maximo_ticket1
     */
    public void setCantidad_maximo_ticket1(float cantidad_maximo_ticket1){
        this.cantidad_maximo_ticket1=cantidad_maximo_ticket1;
    }
    
    /**
     * Getter for property cantidad_maximo_ticket1
     * @return Value of property cantidad_maximo_ticket1
     */
    public float getCantidad_maximo_ticket1(){
        return cantidad_maximo_ticket1;
    }
    
    /**
     * Setter for property cantidad_maximo_ticket2
     * @param destino New value of property cantidad_maximo_ticket2
     */
    public void setCantidad_maximo_ticket2(float cantidad_maximo_ticket2){
        this.cantidad_maximo_ticket2=cantidad_maximo_ticket2;
    }
    
    /**
     * Getter for property cantidad_maximo_ticket2
     * @return Value of property cantidad_maximo_ticket2
     */
    public float getCantidad_maximo_ticket2(){
        return cantidad_maximo_ticket2;
    }
    
    /**
     * Setter for property cantidad_maximo_ticket3
     * @param destino New value of property cantidad_maximo_ticket3
     */
    public void setCantidad_maximo_ticket3(float cantidad_maximo_ticket3){
        this.cantidad_maximo_ticket3=cantidad_maximo_ticket3;
    }
    
    /**
     * Getter for property cantidad_maximo_ticket3
     * @return Value of property cantidad_maximo_ticket3
     */
    public float getCantidad_maximo_ticket3(){
        return cantidad_maximo_ticket3;
    }
    
    /**
     * Setter for property cantidad_maximo_ticket4
     * @param destino New value of property cantidad_maximo_ticket4
     */
    public void  setCantidad_maximo_ticket4(float cantidad_maximo_ticket4){
        this.cantidad_maximo_ticket4=cantidad_maximo_ticket4;
    }
    
    /**
     * Getter for property cantidad_maximo_ticket4
     * @return Value of property cantidad_maximo_ticket4
     */
    public float getCantidad_maximo_ticket4(){
        return cantidad_maximo_ticket4;
    }
    
    /**
     * Setter for property cantidad_maximo_ticket5
     * @param destino New value of property cantidad_maximo_ticket5
     */
    public void setCantidad_maximo_ticket5(float cantidad_maximo_ticket5){
        this.cantidad_maximo_ticket5=cantidad_maximo_ticket5;
    }
    
    /**
     * Getter for property cantidad_maximo_ticket5
     * @return Value of property cantidad_maximo_ticket5
     */
    public float getCantidad_maximo_ticket5(){
        return cantidad_maximo_ticket5;
    }
    
    /**
     * Setter for property cantidad_maximo_ticket6
     * @param destino New value of property cantidad_maximo_ticket6
     */
    public void setCantidad_maximo_ticket6(float cantidad_maximo_ticket6){
        this.cantidad_maximo_ticket6=cantidad_maximo_ticket6;
    }
    
    /**
     * Getter for property cantidad_maximo_ticket6
     * @return Value of property cantidad_maximo_ticket6
     */
    public float getCantidad_maximo_ticket6(){
        return cantidad_maximo_ticket6;
    }
    
    /**
     * Setter for property acpm
     * @param destino New value of property acpm
     */
    public void  setAcpm(float acpm){
        this.acpm=acpm;
    }
    
    /**
     * Getter for property acpm
     * @return Value of property acpm
     */
    public float getAcpm(){
        return acpm;
    }
    
    /**
     * Setter for property km
     * @param destino New value of property km
     */
    public void  setKm(float km){
        this.km=km;
    }
    
    /**
     * Getter for property km
     * @return Value of property km
     */
    public float getKm(){
        return km;
    }
    
    /**
     * Setter for property time
     * @param destino New value of property time
     */
    public void  setTime(float time){
        this.tiempo=time;
    }
    
    /**
     * Getter for property time
     * @return Value of property time
     */
    public float getTime(){
        return tiempo;
    }

    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property tiempo.
     * @return Value of property tiempo.
     */
    public float getTiempo() {
        return tiempo;
    }
    /**
     * Getter for property tiempo.
     * @return Value of property tiempo.
     */
    /**
     * Setter for property tiempo.
     * @param tiempo New value of property tiempo.
     */
    public void setTiempo(float tiempo) {
        this.tiempo = tiempo;
    }
        
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property consumo_combustible.
     * @return Value of property consumo_combustible.
     */
    public float getConsumo_combustible() {
        return consumo_combustible;
    }
    
    /**
     * Setter for property consumo_combustible.
     * @param consumo_combustible New value of property consumo_combustible.
     */
    public void setConsumo_combustible(float consumo_combustible) {
        this.consumo_combustible = consumo_combustible;
    }
    
    /**
     * Getter for property distancia.
     * @return Value of property distancia.
     */
    public float getDistancia() {
        return distancia;
    }
    
    /**
     * Setter for property distancia.
     * @param distancia New value of property distancia.
     */
    public void setDistancia(float distancia) {
        this.distancia = distancia;
    }
    
    /**
     * Getter for property duracion.
     * @return Value of property duracion.
     */
    public float getDuracion() {
        return duracion;
    }
    
    /**
     * Setter for property duracion.
     * @param duracion New value of property duracion.
     */
    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }
    
    /**
     * Getter for property ncia.
     * @return Value of property ncia.
     */
    public java.lang.String getNcia() {
        return ncia;
    }
    
    /**
     * Setter for property ncia.
     * @param ncia New value of property ncia.
     */
    public void setNcia(java.lang.String ncia) {
        this.ncia = ncia;
    }
    
    /**
     * Getter for property ndestino.
     * @return Value of property ndestino.
     */
    public java.lang.String getNdestino() {
        return ndestino;
    }
    
    /**
     * Setter for property ndestino.
     * @param ndestino New value of property ndestino.
     */
    public void setNdestino(java.lang.String ndestino) {
        this.ndestino = ndestino;
    }
    
    /**
     * Getter for property norigen.
     * @return Value of property norigen.
     */
    public java.lang.String getNorigen() {
        return norigen;
    }
    
    /**
     * Setter for property norigen.
     * @param norigen New value of property norigen.
     */
    public void setNorigen(java.lang.String norigen) {
        this.norigen = norigen;
    }
    
    /**
     * Getter for property paisd.
     * @return Value of property paisd.
     */
    public java.lang.String getPaisd() {
        return paisd;
    }
    
    /**
     * Setter for property paisd.
     * @param paisd New value of property paisd.
     */
    public void setPaisd(java.lang.String paisd) {
        this.paisd = paisd;
    }
    
    /**
     * Getter for property paiso.
     * @return Value of property paiso.
     */
    public java.lang.String getPaiso() {
        return paiso;
    }
    
    /**
     * Setter for property paiso.
     * @param paiso New value of property paiso.
     */
    public void setPaiso(java.lang.String paiso) {
        this.paiso = paiso;
    }
    
    /**
     * Getter for property Scombustible.
     * @return Value of property Scombustible.
     */
    public java.lang.String getScombustible() {
        return Scombustible;
    }
    
    /**
     * Setter for property Scombustible.
     * @param Scombustible New value of property Scombustible.
     */
    public void setScombustible(java.lang.String Scombustible) {
        this.Scombustible = Scombustible;
    }
    
    /**
     * Getter for property Sdistancia.
     * @return Value of property Sdistancia.
     */
    public java.lang.String getSdistancia() {
        return Sdistancia;
    }
    
    /**
     * Setter for property Sdistancia.
     * @param Sdistancia New value of property Sdistancia.
     */
    public void setSdistancia(java.lang.String Sdistancia) {
        this.Sdistancia = Sdistancia;
    }
    
    /**
     * Getter for property Sduracion.
     * @return Value of property Sduracion.
     */
    public java.lang.String getSduracion() {
        return Sduracion;
    }
    
    /**
     * Setter for property Sduracion.
     * @param Sduracion New value of property Sduracion.
     */
    public void setSduracion(java.lang.String Sduracion) {
        this.Sduracion = Sduracion;
    }
    
    /**
     * Getter for property unidad_consumo.
     * @return Value of property unidad_consumo.
     */
    public java.lang.String getUnidad_consumo() {
        return unidad_consumo;
    }
    
    /**
     * Setter for property unidad_consumo.
     * @param unidad_consumo New value of property unidad_consumo.
     */
    public void setUnidad_consumo(java.lang.String unidad_consumo) {
        this.unidad_consumo = unidad_consumo;
    }
    
    /**
     * Getter for property unidad_distancia.
     * @return Value of property unidad_distancia.
     */
    public java.lang.String getUnidad_distancia() {
        return unidad_distancia;
    }
    
    /**
     * Setter for property unidad_distancia.
     * @param unidad_distancia New value of property unidad_distancia.
     */
    public void setUnidad_distancia(java.lang.String unidad_distancia) {
        this.unidad_distancia = unidad_distancia;
    }
    
    /**
     * Getter for property unidad_duracion.
     * @return Value of property unidad_duracion.
     */
    public java.lang.String getUnidad_duracion() {
        return unidad_duracion;
    }
    
    /**
     * Setter for property unidad_duracion.
     * @param unidad_duracion New value of property unidad_duracion.
     */
    public void setUnidad_duracion(java.lang.String unidad_duracion) {
        this.unidad_duracion = unidad_duracion;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
}


