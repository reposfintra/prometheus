/**************************************************************************
 * Nombre:        Captacion.java                                          *
 * Descripci�n:   Beans de acuerdo especial.                              *
 * Autor:         Ing. Julio Barros                                       *
 * Fecha:         13 de junio de 2007, 08:01 PM                           *
 * Versi�n:       Java  1.0                                               *
 * Copyright:     Fintra Valores S.A.                                     *
 **************************************************************************/

package com.tsp.operation.model.beans;


public class Captacion {
    private String reg_status; 
    private String dstrct; 
    private String proveedor; 
    private String tipo_operacion; 
    private String tipo_documento; 
    private String documento; 
    private String fecha_documento; 
    private String fecha_inicio; 
    private double vlr_capital; 
    private String moneda; 
    private double interes;
    private String frecuencia;
    private String clase_ingreso;
    private String ref1;
    private String ref2;
    private String ref3;
    private String fecha_liquidacion;
    private String fecha_corte;
    private double vlr_intereses;
    private double vlr_retefuente;
    private double vlr_reteica; //jpinedo
    private double vlr_total_capital;
    private double vlr_nuevo_capital;
    private String tipo_documento_previo;
    private String documento_previo;
    private String base;
    private String creation_date;
    private String creation_user;
    private String last_update;
    private String user_update;
    private String cod_cuenta_contable;
    private String transaccion_id;
    private String doc_contabilidad;
    private String fecha_contabilazo;
    private String usuario_contabilizo;
    private String fecha_anulacion_con;
    private String usuario_anulacion_con;
    private String fecha_recibo_con;
    private String usuario_recibio_con;    
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property clase_ingreso.
     * @return Value of property clase_ingreso.
     */
    public java.lang.String getClase_ingreso() {
        return clase_ingreso;
    }
    
    /**
     * Setter for property clase_ingreso.
     * @param clase_ingreso New value of property clase_ingreso.
     */
    public void setClase_ingreso(java.lang.String clase_ingreso) {
        this.clase_ingreso = clase_ingreso;
    }
    
    /**
     * Getter for property cod_cuenta_contable.
     * @return Value of property cod_cuenta_contable.
     */
    public java.lang.String getCod_cuenta_contable() {
        return cod_cuenta_contable;
    }
    
    /**
     * Setter for property cod_cuenta_contable.
     * @param cod_cuenta_contable New value of property cod_cuenta_contable.
     */
    public void setCod_cuenta_contable(java.lang.String cod_cuenta_contable) {
        this.cod_cuenta_contable = cod_cuenta_contable;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property doc_contabilidad.
     * @return Value of property doc_contabilidad.
     */
    public java.lang.String getDoc_contabilidad() {
        return doc_contabilidad;
    }
    
    /**
     * Setter for property doc_contabilidad.
     * @param doc_contabilidad New value of property doc_contabilidad.
     */
    public void setDoc_contabilidad(java.lang.String doc_contabilidad) {
        this.doc_contabilidad = doc_contabilidad;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property documento_previo.
     * @return Value of property documento_previo.
     */
    public java.lang.String getDocumento_previo() {
        return documento_previo;
    }
    
    /**
     * Setter for property documento_previo.
     * @param documento_previo New value of property documento_previo.
     */
    public void setDocumento_previo(java.lang.String documento_previo) {
        this.documento_previo = documento_previo;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_anulacion_con.
     * @return Value of property fecha_anulacion_con.
     */
    public java.lang.String getFecha_anulacion_con() {
        return fecha_anulacion_con;
    }
    
    /**
     * Setter for property fecha_anulacion_con.
     * @param fecha_anulacion_con New value of property fecha_anulacion_con.
     */
    public void setFecha_anulacion_con(java.lang.String fecha_anulacion_con) {
        this.fecha_anulacion_con = fecha_anulacion_con;
    }
    
    /**
     * Getter for property fecha_contabilazo.
     * @return Value of property fecha_contabilazo.
     */
    public java.lang.String getFecha_contabilazo() {
        return fecha_contabilazo;
    }
    
    /**
     * Setter for property fecha_contabilazo.
     * @param fecha_contabilazo New value of property fecha_contabilazo.
     */
    public void setFecha_contabilazo(java.lang.String fecha_contabilazo) {
        this.fecha_contabilazo = fecha_contabilazo;
    }
    
    /**
     * Getter for property fecha_corte.
     * @return Value of property fecha_corte.
     */
    public java.lang.String getFecha_corte() {
        return fecha_corte;
    }
    
    /**
     * Setter for property fecha_corte.
     * @param fecha_corte New value of property fecha_corte.
     */
    public void setFecha_corte(java.lang.String fecha_corte) {
        this.fecha_corte = fecha_corte;
    }
    
    /**
     * Getter for property fecha_documento.
     * @return Value of property fecha_documento.
     */
    public java.lang.String getFecha_documento() {
        return fecha_documento;
    }
    
    /**
     * Setter for property fecha_documento.
     * @param fecha_documento New value of property fecha_documento.
     */
    public void setFecha_documento(java.lang.String fecha_documento) {
        this.fecha_documento = fecha_documento;
    }
    
    /**
     * Getter for property fecha_inicio.
     * @return Value of property fecha_inicio.
     */
    public java.lang.String getFecha_inicio() {
        return fecha_inicio;
    }
    
    /**
     * Setter for property fecha_inicio.
     * @param fecha_inicio New value of property fecha_inicio.
     */
    public void setFecha_inicio(java.lang.String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }
    
    /**
     * Getter for property fecha_liquidacion.
     * @return Value of property fecha_liquidacion.
     */
    public java.lang.String getFecha_liquidacion() {
        return fecha_liquidacion;
    }
    
    /**
     * Setter for property fecha_liquidacion.
     * @param fecha_liquidacion New value of property fecha_liquidacion.
     */
    public void setFecha_liquidacion(java.lang.String fecha_liquidacion) {
        this.fecha_liquidacion = fecha_liquidacion;
    }
    
    /**
     * Getter for property fecha_recibo_con.
     * @return Value of property fecha_recibo_con.
     */
    public java.lang.String getFecha_recibo_con() {
        return fecha_recibo_con;
    }
    
    /**
     * Setter for property fecha_recibo_con.
     * @param fecha_recibo_con New value of property fecha_recibo_con.
     */
    public void setFecha_recibo_con(java.lang.String fecha_recibo_con) {
        this.fecha_recibo_con = fecha_recibo_con;
    }
    
    /**
     * Getter for property frecuencia.
     * @return Value of property frecuencia.
     */
    public java.lang.String getFrecuencia() {
        return frecuencia;
    }
    
    /**
     * Setter for property frecuencia.
     * @param frecuencia New value of property frecuencia.
     */
    public void setFrecuencia(java.lang.String frecuencia) {
        this.frecuencia = frecuencia;
    }
    
    /**
     * Getter for property interes.
     * @return Value of property interes.
     */
    public double getInteres() {
        return interes;
    }
    
    /**
     * Setter for property interes.
     * @param interes New value of property interes.
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property ref1.
     * @return Value of property ref1.
     */
    public java.lang.String getRef1() {
        return ref1;
    }
    
    /**
     * Setter for property ref1.
     * @param ref1 New value of property ref1.
     */
    public void setRef1(java.lang.String ref1) {
        this.ref1 = ref1;
    }
    
    /**
     * Getter for property ref2.
     * @return Value of property ref2.
     */
    public java.lang.String getRef2() {
        return ref2;
    }
    
    /**
     * Setter for property ref2.
     * @param ref2 New value of property ref2.
     */
    public void setRef2(java.lang.String ref2) {
        this.ref2 = ref2;
    }
    
    /**
     * Getter for property ref3.
     * @return Value of property ref3.
     */
    public java.lang.String getRef3() {
        return ref3;
    }
    
    /**
     * Setter for property ref3.
     * @param ref3 New value of property ref3.
     */
    public void setRef3(java.lang.String ref3) {
        this.ref3 = ref3;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property tipo_documento_previo.
     * @return Value of property tipo_documento_previo.
     */
    public java.lang.String getTipo_documento_previo() {
        return tipo_documento_previo;
    }
    
    /**
     * Setter for property tipo_documento_previo.
     * @param tipo_documento_previo New value of property tipo_documento_previo.
     */
    public void setTipo_documento_previo(java.lang.String tipo_documento_previo) {
        this.tipo_documento_previo = tipo_documento_previo;
    }
    
    /**
     * Getter for property tipo_operacion.
     * @return Value of property tipo_operacion.
     */
    public java.lang.String getTipo_operacion() {
        return tipo_operacion;
    }
    
    /**
     * Setter for property tipo_operacion.
     * @param tipo_operacion New value of property tipo_operacion.
     */
    public void setTipo_operacion(java.lang.String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }
    
    /**
     * Getter for property transaccion_id.
     * @return Value of property transaccion_id.
     */
    public java.lang.String getTransaccion_id() {
        return transaccion_id;
    }
    
    /**
     * Setter for property transaccion_id.
     * @param transaccion_id New value of property transaccion_id.
     */
    public void setTransaccion_id(java.lang.String transaccion_id) {
        this.transaccion_id = transaccion_id;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property usuario_anulacion_con.
     * @return Value of property usuario_anulacion_con.
     */
    public java.lang.String getUsuario_anulacion_con() {
        return usuario_anulacion_con;
    }
    
    /**
     * Setter for property usuario_anulacion_con.
     * @param usuario_anulacion_con New value of property usuario_anulacion_con.
     */
    public void setUsuario_anulacion_con(java.lang.String usuario_anulacion_con) {
        this.usuario_anulacion_con = usuario_anulacion_con;
    }
    
    /**
     * Getter for property usuario_contabilizo.
     * @return Value of property usuario_contabilizo.
     */
    public java.lang.String getUsuario_contabilizo() {
        return usuario_contabilizo;
    }
    
    /**
     * Setter for property usuario_contabilizo.
     * @param usuario_contabilizo New value of property usuario_contabilizo.
     */
    public void setUsuario_contabilizo(java.lang.String usuario_contabilizo) {
        this.usuario_contabilizo = usuario_contabilizo;
    }
    
    /**
     * Getter for property usuario_recibio_con.
     * @return Value of property usuario_recibio_con.
     */
    public java.lang.String getUsuario_recibio_con() {
        return usuario_recibio_con;
    }
    
    /**
     * Setter for property usuario_recibio_con.
     * @param usuario_recibio_con New value of property usuario_recibio_con.
     */
    public void setUsuario_recibio_con(java.lang.String usuario_recibio_con) {
        this.usuario_recibio_con = usuario_recibio_con;
    }
    
    /**
     * Getter for property vlr_capital.
     * @return Value of property vlr_capital.
     */
    public double getVlr_capital() {
        return vlr_capital;
    }
    
    /**
     * Setter for property vlr_capital.
     * @param vlr_capital New value of property vlr_capital.
     */
    public void setVlr_capital(double vlr_capital) {
        this.vlr_capital = vlr_capital;
    }
    
    /**
     * Getter for property vlr_intereses.
     * @return Value of property vlr_intereses.
     */
    public double getVlr_intereses() {
        return vlr_intereses;
    }
    
    /**
     * Setter for property vlr_intereses.
     * @param vlr_intereses New value of property vlr_intereses.
     */
    public void setVlr_intereses(double vlr_intereses) {
        this.vlr_intereses = vlr_intereses;
    }
    
    /**
     * Getter for property vlr_nuevo_capital.
     * @return Value of property vlr_nuevo_capital.
     */
    public double getVlr_nuevo_capital() {
        return vlr_nuevo_capital;
    }
    
    /**
     * Setter for property vlr_nuevo_capital.
     * @param vlr_nuevo_capital New value of property vlr_nuevo_capital.
     */
    public void setVlr_nuevo_capital(double vlr_nuevo_capital) {
        this.vlr_nuevo_capital = vlr_nuevo_capital;
    }
    
    /**
     * Getter for property vlr_retefuente.
     * @return Value of property vlr_retefuente.
     */
    public double getVlr_retefuente() {
        return vlr_retefuente;
    }
    
    /**
     * Setter for property vlr_retefuente.
     * @param vlr_retefuente New value of property vlr_retefuente.
     */
    public void setVlr_retefuente(double vlr_retefuente) {
        this.vlr_retefuente = vlr_retefuente;
    }
    
    /**
     * Getter for property vlr_total_capital.
     * @return Value of property vlr_total_capital.
     */
    public double getVlr_total_capital() {
        return vlr_total_capital;
    }
    
    /**
     * Setter for property vlr_total_capital.
     * @param vlr_total_capital New value of property vlr_total_capital.
     */
    public void setVlr_total_capital(double vlr_total_capital) {
        this.vlr_total_capital = vlr_total_capital;
    }

        public double getVlr_reteica() {
        return vlr_reteica;
    }

    public void setVlr_reteica(double vlr_reteica) {
        this.vlr_reteica = vlr_reteica;
    }
    
}
