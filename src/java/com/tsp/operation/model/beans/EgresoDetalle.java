/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class EgresoDetalle {

    private String reg_status;
    private String dstrct  ;
    private String branch_code  ;
    private String bank_account_no ;
    private String document_no  ;
    private String item_no  ;
    private String concept_code  ;
    private double vlr ;
    private double vlr_for ;
    private String currency  ;
    private String oc  ;
    private String last_update  ;
    private String user_update ;
    private String creation_date  ;
    private String creation_user ;
    private String description ;
    private String base  ;
    private double tasa ;
    private int    transaccion ;
    private String tipo_documento ;
    private String documento ;
    private String tipo_pago ;
    private String cuenta;
    private String auxiliar;



    /** Creates a new instance of EgresoDetalle */
    public EgresoDetalle() {
    }




    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the branch_code
     */
    public String getBranch_code() {
        return branch_code;
    }

    /**
     * @param branch_code the branch_code to set
     */
    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    /**
     * @return the bank_account_no
     */
    public String getBank_account_no() {
        return bank_account_no;
    }

    /**
     * @param bank_account_no the bank_account_no to set
     */
    public void setBank_account_no(String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }

    /**
     * @return the document_no
     */
    public String getDocument_no() {
        return document_no;
    }

    /**
     * @param document_no the document_no to set
     */
    public void setDocument_no(String document_no) {
        this.document_no = document_no;
    }

    /**
     * @return the item_no
     */
    public String getItem_no() {
        return item_no;
    }

    /**
     * @param item_no the item_no to set
     */
    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    /**
     * @return the concept_code
     */
    public String getConcept_code() {
        return concept_code;
    }

    /**
     * @param concept_code the concept_code to set
     */
    public void setConcept_code(String concept_code) {
        this.concept_code = concept_code;
    }

    /**
     * @return the vlr
     */
    public double getVlr() {
        return vlr;
    }

    /**
     * @param vlr the vlr to set
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }

    /**
     * @return the vlr_for
     */
    public double getVlr_for() {
        return vlr_for;
    }

    /**
     * @param vlr_for the vlr_for to set
     */
    public void setVlr_for(double vlr_for) {
        this.vlr_for = vlr_for;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the oc
     */
    public String getOc() {
        return oc;
    }

    /**
     * @param oc the oc to set
     */
    public void setOc(String oc) {
        this.oc = oc;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the tasa
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * @param tasa the tasa to set
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the tipo_pago
     */
    public String getTipo_pago() {
        return tipo_pago;
    }

    /**
     * @param tipo_pago the tipo_pago to set
     */
    public void setTipo_pago(String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the auxiliar
     */
    public String getAuxiliar() {
        return auxiliar;
    }

    /**
     * @param auxiliar the auxiliar to set
     */
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }


}

