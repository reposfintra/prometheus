/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author jpinedo
 */





public class Siniestros {

    private String tipo;
    private String nombre_siniestro;
    private String direccion;
    private String telefono1;
    private String telefono2;
    private String celular;
    private String codigo_banco;
    private String numero_cuenta_siniestro;
    private String numero_cheque_siniestro;
    private double valor_cheque_siniestro;
    private String fecha_consignacion_siniestro;
    private String fecha_recuperacion;
    private String banco;
    
    private String dias_recuperacion;

    public String getDias_recuperacion() {
        return dias_recuperacion;
    }

    public void setDias_recuperacion(String dias_recuperacion) {
        this.dias_recuperacion = dias_recuperacion;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getFecha_recuperacion() {
        return fecha_recuperacion;
    }

    public void setFecha_recuperacion(String fecha_recuperacion) {
        this.fecha_recuperacion = fecha_recuperacion;
    }

    public String getNumero_cheque_siniestro() {
        return numero_cheque_siniestro;
    }

    public void setNumero_cheque_siniestro(String numero_cheque_siniestro) {
        this.numero_cheque_siniestro = numero_cheque_siniestro;
    }


    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCodigo_banco() {
        return codigo_banco;
    }

    public void setCodigo_banco(String codigo_banco) {
        this.codigo_banco = codigo_banco;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFecha_consignacion_siniestro() {
        return fecha_consignacion_siniestro;
    }

    public void setFecha_consignacion_siniestro(String fecha_consignacion_siniestro) {
        this.fecha_consignacion_siniestro = fecha_consignacion_siniestro;
    }

    public String getNombre_siniestro() {
        return nombre_siniestro;
    }

    public void setNombre_siniestro(String nombre_siniestro) {
        this.nombre_siniestro = nombre_siniestro;
    }

    public String getNumero_cheuqe_siniestro() {
        return numero_cheque_siniestro;
    }

    public void setNumero_cheuqe_siniestro(String numero_cheuqe_siniestro) {
        this.numero_cheque_siniestro = numero_cheuqe_siniestro;
    }

    public String getNumero_cuenta_siniestro() {
        return numero_cuenta_siniestro;
    }

    public void setNumero_cuenta_siniestro(String numero_cuenta_siniestro) {
        this.numero_cuenta_siniestro = numero_cuenta_siniestro;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getValor_cheque_siniestro() {
        return valor_cheque_siniestro;
    }

    public void setValor_cheque_siniestro(double valor_cheque_siniestro) {
        this.valor_cheque_siniestro = valor_cheque_siniestro;
    }


    

}
