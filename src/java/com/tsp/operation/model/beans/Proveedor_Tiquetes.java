package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Proveedor_Tiquetes implements Serializable {
    
    private String dstrct;
    private String city_code;
    private String nit;
    private java.util.Date creation_date;
    private String creation_user;
    private String nombre;
    private float valor;
    private String ciudad;
    private String codigo_migracion;
    private float porcentaje;
    private String codigo;
    
    public static Proveedor_Tiquetes load(ResultSet rs)throws SQLException{
        
        Proveedor_Tiquetes proveedor_tiquetes= new Proveedor_Tiquetes();
        proveedor_tiquetes.setDstrct(rs.getString("Dstrct"));
        proveedor_tiquetes.setCity_code(rs.getString("City_code"));
        proveedor_tiquetes.setNit(rs.getString("Nit"));
        proveedor_tiquetes.setCreation_date(rs.getTimestamp("Creation_date"));
        proveedor_tiquetes.setCreation_user(rs.getString("Creation_user"));
        proveedor_tiquetes.setNombre(rs.getString("Nombre"));
        proveedor_tiquetes.setCiudad(rs.getString("ciudad"));
        proveedor_tiquetes.setCodigo(rs.getString("sucursal"));
        proveedor_tiquetes.setPorcentaje(rs.getFloat("porcentaje"));
        
        return proveedor_tiquetes;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    public void setCodigo(String codigo){
        
        this.codigo=codigo;
    }
    
    public String getCodigo(){
        
        return codigo;
    }
    public void setPorcentaje(float porcentaje){
        
        this.porcentaje=porcentaje;
    }
    
    public float getPorcentaje(){
        
        return porcentaje;
    }
    public void setCod_Migracion(String cod){
        
        this.codigo_migracion=cod;
    }
    
    public String getCod_Migracion(){
        
        return codigo_migracion;
    }
    public void setCiudad(String ciudad){
        
        this.ciudad=ciudad;
    }
    
    public String getCiudad(){
        
        return ciudad;
    }
    public void setValor(float valor){
        
        this.valor=valor;
    }
    
    public float getValor(){
        
        return valor;
    }
    public void setNombre(String nombre){
        
        this.nombre=nombre;
    }
    
    public String getNombre(){
        
        return nombre;
    }
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    
    public void setCity_code(String city_code){
        
        this.city_code=city_code;
    }
    
    public String getCity_code(){
        
        return city_code;
    }
    
    public void setNit(String nit){
        
        this.nit=nit;
    }
    
    public String getNit(){
        
        return nit;
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
}