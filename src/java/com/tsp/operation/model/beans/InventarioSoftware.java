/******************************************************************
* Nombre ......................InventarioSoftware.java
* Descripci�n..................Clase bean para el inventario del 
                               software del sitio slt
* Autor........................Ing. Armando Oviedo
* Fecha........................26/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;

import java.util.*;
/**
 *
 * @author  Armando Oviedo
 */

public class InventarioSoftware {
    
    private String extension;
    private String profundidad[];
    private Date fecha;
    private long tamano;
    private String ruta;
    private String nombre;
    private String base;
    private String dstrct;
    private String reg_status;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String propiedad;
    
    // Fernel   10/01/2006
    private String equipo;
    private String sitio;
    //FVillacob 13.01.2006
    private int    id;
    //FVillacob 14.01.2006
    private String fechaMod;
    private String tamanoMod;
    private String fechaDelete;
    private List   listRevicion;
   
    /**
     * M�todo que setea el equipo
     * @autor.......Fernel      
      * @version.....1.0.
     * @param.......String val
     **/
    public void setEquipo(String val){
        this.equipo = val;
    }
    
    /**
     * M�todo que setea el sitio
     * @autor.......Fernel      
      * @version.....1.0.
     * @param.......String val
     **/
    public void setSitio(String val){
        this.sitio = val;
    }
    
    
    /**
     * M�todo que devuelve el equipo
     * @autor.......Fernel      
      * @version.....1.0.
     **/
    public String getEquipo(){
        return this.equipo ;
    }
    
    
    /**
     * M�todo que devuelve el sitio
     * @autor.......Fernel      
      * @version.....1.0.
     **/
    public String getSitio(){
        return this.sitio ;
    }
    
    
    
    
    /** Creates a new instance of InventarioSoftware */
    public InventarioSoftware() {
    }
    
    /**
     * Getter for property extension.
     * @return Value of property extension.
     */
    public java.lang.String getExtension() {
        return extension;
    }    
    
    /**
     * Setter for property extension.
     * @param extension New value of property extension.
     */
    public void setExtension(java.lang.String extension) {
        this.extension = extension;
    }
    
    /**
     * Getter for property profundidad.
     * @return Value of property profundidad.
     */
    public java.lang.String[] getProfundidad() {
        return this.profundidad;
    }
    
    /**
     * Setter for property profundidad.
     * @param profundidad New value of property profundidad.
     */
    public void setProfundidad(java.lang.String[] profundidad) {
        this.profundidad = profundidad;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.util.Date getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.util.Date fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property tamano.
     * @return Value of property tamano.
     */
    public long getTamano() {
        return tamano;
    }
    
    /**
     * Setter for property tamano.
     * @param tamano New value of property tamano.
     */
    public void setTamano(long tamano) {
        this.tamano = tamano;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property propiedad.
     * @return Value of property propiedad.
     */
    public java.lang.String getPropiedad() {
        return propiedad;
    }
    
    /**
     * Setter for property propiedad.
     * @param propiedad New value of property propiedad.
     */
    public void setPropiedad(java.lang.String propiedad) {
        this.propiedad = propiedad;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getRegStatus() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setRegStatus(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLastUpdate() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLastUpdate(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUserUpdate() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUserUpdate(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreationDate() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreationDate(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreationUser(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * M�todo que setea el id
     * @autor.......Fernel      
     * @version.....1.0.
     * @param.......String val
     **/
    public void setId(int val){
        this.id = val;
    }
    
    
    /**
     * M�todo que devuelve el id
     * @autor.......Fernel      
     * @version.....1.0.
     * @param.......String val
     **/
    public int getId(){
       return  this.id;
    }
    
    /**
     * M�todo que setea las revisiones realizadas al archivo
     * @autor.......Fernel      
     * @version.....1.0.
     * @param.......List list
     **/
    public void setRevisiones(List list){
        this.listRevicion = list;
    }
    
    
    
    /**
     * M�todo que devuelve las revisiones realizadas al archivo
     * @autor.......Fernel      
     * @version.....1.0.
     **/
    public List getRevisiones(){
        return this.listRevicion;
    }
    
    
    
     /**
     * M�todo que setea la fecha modificacion
     * @autor.......Fernel      
     * @version.....1.0.
     * @param.......String val
     **/
    public void setFechaMod(String val){
        this.fechaMod = val;
    }
    
    /**
     * M�todo que setea la fecha eliminacion
     * @autor.......Fernel      
     * @version.....1.0.
     * @param.......String val
     **/
    public void setFechaDelete(String val){
        this.fechaDelete = val;
    }
    
    
    /**
     * M�todo que setea el tamano de mod.
     * @autor.......Fernel      
     * @version.....1.0.
     * @param.......String val
     **/
    public void setTamanoMod(String val){
        this.tamanoMod = val;
    }
    
    
    
    /**
     * M�todo que devuelve la fecha modificacion
     * @autor.......Fernel      
     * @version.....1.0.
     **/
    public String getFechaMod(){
        return this.fechaMod ;
    }
    
    /**
     * M�todo que devuelve la fecha eliminacion
     * @autor.......Fernel      
     * @version.....1.0.
     **/
    public String getFechaDelete(){
        return this.fechaDelete ;
    }
    
    
    /**
     * M�todo que devuelve el tamano de mod.
     * @autor.......Fernel      
     * @version.....1.0.
     **/
    public String getTamanoMod(){
       return this.tamanoMod ;
    }
}
