    /***************************************
    * Nombre Clase ............. Impuesto.java
    * Descripci�n  .. . . . . .  Carga los Impuestos de las facturas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  13/12/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.beans;


public class Impuesto {
    
      private String dstrct;
      private String proveedor;
      private String tipo_documento ;
      private String documento ;
      private String cod_impuesto ;
      private double porcent_impuesto ;
      private double vlr_total_impuesto ;
      private double vlr_total_impuesto_me ;
      private String base;
    
      
      
    public Impuesto() {
    }
    
    
    
     // SET:
    /**
     * M�todos que guardan valores en el objeto
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/
    public void setDistrito   (String val){ this.dstrct                = val; }
    public void setProveedor  (String val){ this.proveedor             = val; }
    public void setTipoDoc    (String val){ this.tipo_documento        = val; }
    public void setDocumento  (String val){ this.documento             = val; }
    public void setCodigoImp  (String val){ this.cod_impuesto          = val; }
    public void setPorcentaje (double val){ this.porcent_impuesto      = val; }    
    public void setValor      (double val){ this.vlr_total_impuesto    = val; }    
    public void setValorMe    (double val){ this.vlr_total_impuesto_me = val; }
    public void setBase       (String val){ this.base                  = val; }
    
    
    
    // GET:
    /**
     * M�todos que retornan  valores
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
    public String getDistrito   (){ return this.dstrct                ; }
    public String getProveedor  (){ return this.proveedor             ; }
    public String getTipoDoc    (){ return this.tipo_documento        ; }
    public String getDocumento  (){ return this.documento             ; }
    public String getCodigoImp  (){ return this.cod_impuesto          ; }
    public double getPorcentaje (){ return this.porcent_impuesto      ; }    
    public double getValor      (){ return this.vlr_total_impuesto    ; }    
    public double getValorMe    (){ return this.vlr_total_impuesto_me ; }
    public String getBase       (){ return this.base                  ; }
   
    
}
