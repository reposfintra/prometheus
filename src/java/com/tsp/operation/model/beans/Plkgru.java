/*
 * Plkgru.java
 *
 * Created on 25 de noviembre de 2004, 10:43 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
import java.io.Serializable;
import java.io.*;
import java.sql.*;
import java.util.*;

public class Plkgru implements Serializable{
    
    
    private String sj;
    private String placa;
    private String group_code;
    
    public static Plkgru load(ResultSet rs)throws SQLException{
        
        Plkgru plkgru = new Plkgru();
        
        plkgru.setSj(rs.getString(1));
        plkgru.setPlaca(rs.getString(2));
        plkgru.setGroupcode(rs.getString(3));
        
        return plkgru;
    }
    public void setSj(String sj){
        this.sj = sj;
    }
    
    public String getSj(){
        return this.sj;
    }
    public void setGroupcode(String group_code){
        this.group_code=group_code;
    }
    
    public String getGroupcode(){
        return this.group_code;
    }
    public void setPlaca(String placa){
        this.placa = placa;
    }
    
    public String getPlaca(){
        return this.placa;
    }
    
}
