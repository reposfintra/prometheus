/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class RequisicionBeans {

    public String prireid;
    public String priredecripcion;
    public String prirecolor;
    public String prireestado;
    public String estreqestado;
    public String estreqdescripcion;
    public String estreqid;
    public String tiptareaid;
    public String tiptareadesc;
    public String tiptareaestado;
    public String esttiptareaid;
    public String esttiptareadesc;
    public String esttiptareaestado;
    public String tiporeqid;
    public String tiporeqdescripcion;
    public String tiporeqdocument;
    public String tiporeqestado;
    public String tiporeqeficacia;
    public String tiporeqeficiencia;

    public String getPrireid() {
        return prireid;
    }

    public void setPrireid(String prireid) {
        this.prireid = prireid;
    }

    public String getPriredecripcion() {
        return priredecripcion;
    }

    public void setPriredecripcion(String priredecripcion) {
        this.priredecripcion = priredecripcion;
    }

    public String getPrirecolor() {
        return prirecolor;
    }

    public void setPrirecolor(String prirecolor) {
        this.prirecolor = prirecolor;
    }

    public String getPrireestado() {
        return prireestado;
    }

    public void setPrireestado(String prireestado) {
        this.prireestado = prireestado;
    }

    public String getEstreqestado() {
        return estreqestado;
    }

    public void setEstreqestado(String estreqestado) {
        this.estreqestado = estreqestado;
    }

    public String getEstreqdescripcion() {
        return estreqdescripcion;
    }

    public void setEstreqdescripcion(String estreqdescripcion) {
        this.estreqdescripcion = estreqdescripcion;
    }

    public String getEstreqid() {
        return estreqid;
    }

    public void setEstreqid(String estreqid) {
        this.estreqid = estreqid;
    }

    public String getTiptareaid() {
        return tiptareaid;
    }

    public void setTiptareaid(String tiptareaid) {
        this.tiptareaid = tiptareaid;
    }

    public String getTiptareadesc() {
        return tiptareadesc;
    }

    public void setTiptareadesc(String tiptareadesc) {
        this.tiptareadesc = tiptareadesc;
    }

    public String getTiptareaestado() {
        return tiptareaestado;
    }

    public void setTiptareaestado(String tiptareaestado) {
        this.tiptareaestado = tiptareaestado;
    }

    public String getEsttiptareaid() {
        return esttiptareaid;
    }

    public void setEsttiptareaid(String esttiptareaid) {
        this.esttiptareaid = esttiptareaid;
    }

    public String getEsttiptareadesc() {
        return esttiptareadesc;
    }

    public void setEsttiptareadesc(String esttiptareadesc) {
        this.esttiptareadesc = esttiptareadesc;
    }

    public String getEsttiptareaestado() {
        return esttiptareaestado;
    }

    public void setEsttiptareaestado(String esttiptareaestado) {
        this.esttiptareaestado = esttiptareaestado;
    }

    public String getTiporeqid() {
        return tiporeqid;
    }

    public void setTiporeqid(String tiporeqid) {
        this.tiporeqid = tiporeqid;
    }

    public String getTiporeqdescripcion() {
        return tiporeqdescripcion;
    }

    public void setTiporeqdescripcion(String tiporeqdescripcion) {
        this.tiporeqdescripcion = tiporeqdescripcion;
    }

    public String getTiporeqdocument() {
        return tiporeqdocument;
    }

    public void setTiporeqdocument(String tiporeqdocument) {
        this.tiporeqdocument = tiporeqdocument;
    }

    public String getTiporeqestado() {
        return tiporeqestado;
    }

    public void setTiporeqestado(String tiporeqestado) {
        this.tiporeqestado = tiporeqestado;
    }

    public String getTiporeqeficacia() {
        return tiporeqeficacia;
    }

    public void setTiporeqeficacia(String tiporeqeficacia) {
        this.tiporeqeficacia = tiporeqeficacia;
    }

    public String getTiporeqeficiencia() {
        return tiporeqeficiencia;
    }

    public void setTiporeqeficiencia(String tiporeqeficiencia) {
        this.tiporeqeficiencia = tiporeqeficiencia;
    }

    
}
