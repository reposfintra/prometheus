/********************************************************************
 *      Nombre Clase.................   CXPImpDoc .java
 *      Descripci�n..................   Bean de la tabla cxp_imp_doc
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
/**
 *
 * @author  dlamadrid
 */
public class CXPImpDoc implements Serializable{
    
    private String reg_status;
    private String dstrct;
    private String proveedor;
    private String tipo_documento;
    private String documento;
    private String cod_impuesto;  
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String base;     
    
     //ivan
    private double porcent_impuesto;
    private double vlr_total_impuesto;
    private double vlr_total_impuesto_me;
    
    /** Creates a new instance of CXPImpDoc */
    public CXPImpDoc() {
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }    
       
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }    
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property cod_impuesto.
     * @return Value of property cod_impuesto.
     */
    public java.lang.String getCod_impuesto() {
        return cod_impuesto;
    }
    
    /**
     * Setter for property cod_impuesto.
     * @param cod_impuesto New value of property cod_impuesto.
     */
    public void setCod_impuesto(java.lang.String cod_impuesto) {
        this.cod_impuesto = cod_impuesto;
    }
    
    /**
     * Getter for property porcent_impuesto.
     * @return Value of property porcent_impuesto.
     */
    public double getPorcent_impuesto() {
        return porcent_impuesto;
    }
    
    /**
     * Setter for property porcent_impuesto.
     * @param porcent_impuesto New value of property porcent_impuesto.
     */
    public void setPorcent_impuesto(double porcent_impuesto) {
        this.porcent_impuesto = porcent_impuesto;
    }
    
  
     
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    
    
    /**
     * Setter for property vlr_total_impuesto.
     * @param vlr_total_impuesto New value of property vlr_total_impuesto.
     */
    public void setVlr_total_impuesto(float vlr_total_impuesto) {
        this.vlr_total_impuesto = vlr_total_impuesto;
    }
    
    
    /**
     * Setter for property vlr_total_impuesto_me.
     * @param vlr_total_impuesto_me New value of property vlr_total_impuesto_me.
     */
    public void setVlr_total_impuesto_me(float vlr_total_impuesto_me) {
        this.vlr_total_impuesto_me = vlr_total_impuesto_me;
    }
    
    /**
     * Setter for property vlr_total_impuesto.
     * @param vlr_total_impuesto New value of property vlr_total_impuesto.
     */
    public void setVlr_total_impuesto(double vlr_total_impuesto) {
        this.vlr_total_impuesto = vlr_total_impuesto;
    }
    
    /**
     * Setter for property vlr_total_impuesto_me.
     * @param vlr_total_impuesto_me New value of property vlr_total_impuesto_me.
     */
    public void setVlr_total_impuesto_me(double vlr_total_impuesto_me) {
        this.vlr_total_impuesto_me = vlr_total_impuesto_me;
    }
    
    /**
     * Getter for property vlr_total_impuesto_me.
     * @return Value of property vlr_total_impuesto_me.
     */
    public double getVlr_total_impuesto_me() {
        return vlr_total_impuesto_me;
    }
    
    /**
     * Getter for property vlr_total_impuesto.
     * @return Value of property vlr_total_impuesto.
     */
    public double getVlr_total_impuesto() {
        return vlr_total_impuesto;
    }
    
}
