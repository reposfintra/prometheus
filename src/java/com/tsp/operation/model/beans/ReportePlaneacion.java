/*
 * ReportePlaneacion.java
 *
 * Created on 15 de julio de 2005, 12:10 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  kreales
 */
public class ReportePlaneacion {
   //REQUERIMENTO
    private String cliente;
    private String recurso_req;
    private java.sql.Timestamp fechadispC;
    private String origenC;
    private String destinoC;
    // datos del requerimiento requeridos por la llave primaria
    private String dstrct_code;
    private int num_sec;
    private String std_job_no;
    private String numpla;
        
    //RECURSO
    private String placa;
    private String clase;
    private java.sql.Timestamp fechadispR;
    private String origenR;
    private String destinoR;
    private String clienteR;
    private String tipo_asoc;
    // datos del recurso requeridos por la llave primaria
    private String dstrct;
    

    //ASIGNACION
    private java.sql.Timestamp fechaasig;
    
    //EJECUCION
    private String numplaejec;
    private String cliejec;
    private String stdejec;
    private java.sql.Timestamp fecdspejec;
    private String oriEjec;
    private String desEjec;
    
    //Datos Por Agencia
    private String agasoc;
    private String nomagasoc;
    private int vehiculos;
    private int remdespachada;
    private int vehiculoutil;
    private double utilflota;
    private double retorno; 
    private String fecha_dispxag;
    private String nomorirec;
    private String nomdesrec;
    private String nomclienreq; 
    private String tipo_rec_rec; 
    private String recurso_rec;
    private String equ_aso_rec;
    private String fecdis_rec; 
    private String fecasig_rec; 
    //Alejandro
    private String std_job_desc_req;
    private String std_job_desc_rec;
    
    //origen requerimiento
    private String origen_req="";
    private String destino_req="";
    private String wgroup="";
    private String nombrecond="";

    private boolean aplazado =false;
    /** Creates a new instance of ReportePlaneacion */
    public ReportePlaneacion() {
    }
    
    /**
     * Getter for property clase.
     * @return Value of property clase.
     */
    public java.lang.String getClase() {
        return clase;
    }    
    
    /**
     * Setter for property clase.
     * @param clase New value of property clase.
     */
    public void setClase(java.lang.String clase) {
        this.clase = clase;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property clienteR.
     * @return Value of property clienteR.
     */
    public java.lang.String getClienteR() {
        return clienteR;
    }
    
    /**
     * Setter for property clienteR.
     * @param clienteR New value of property clienteR.
     */
    public void setClienteR(java.lang.String clienteR) {
        this.clienteR = clienteR;
    }
    
    /**
     * Getter for property destinoC.
     * @return Value of property destinoC.
     */
    public java.lang.String getDestinoC() {
        return destinoC;
    }
    
    /**
     * Setter for property destinoC.
     * @param destinoC New value of property destinoC.
     */
    public void setDestinoC(java.lang.String destinoC) {
        this.destinoC = destinoC;
    }
    
    /**
     * Getter for property destinoR.
     * @return Value of property destinoR.
     */
    public java.lang.String getDestinoR() {
        return destinoR;
    }
    
    /**
     * Setter for property destinoR.
     * @param destinoR New value of property destinoR.
     */
    public void setDestinoR(java.lang.String destinoR) {
        this.destinoR = destinoR;
    }
    
    /**
     * Getter for property fechaasig.
     * @return Value of property fechaasig.
     */
    public java.sql.Timestamp getFechaasig() {
        return fechaasig;
    }
    
    /**
     * Setter for property fechaasig.
     * @param fechaasig New value of property fechaasig.
     */
    public void setFechaasig(java.sql.Timestamp fechaasig) {
        this.fechaasig = fechaasig;
    }
    
    /**
     * Getter for property fechadispC.
     * @return Value of property fechadispC.
     */
    public java.sql.Timestamp getFechadispC() {
        return fechadispC;
    }
    
    /**
     * Setter for property fechadispC.
     * @param fechadispC New value of property fechadispC.
     */
    public void setFechadispC(java.sql.Timestamp fechadispC) {
        this.fechadispC = fechadispC;
    }
    
    /**
     * Getter for property fechadispR.
     * @return Value of property fechadispR.
     */
    public java.sql.Timestamp getFechadispR() {
        return fechadispR;
    }
    
    /**
     * Setter for property fechadispR.
     * @param fechadispR New value of property fechadispR.
     */
    public void setFechadispR(java.sql.Timestamp fechadispR) {
        this.fechadispR = fechadispR;
    }
    
    /**
     * Getter for property origenC.
     * @return Value of property origenC.
     */
    public java.lang.String getOrigenC() {
        return origenC;
    }
    
    /**
     * Setter for property origenC.
     * @param origenC New value of property origenC.
     */
    public void setOrigenC(java.lang.String origenC) {
        this.origenC = origenC;
    }
    
    /**
     * Getter for property origenR.
     * @return Value of property origenR.
     */
    public java.lang.String getOrigenR() {
        return origenR;
    }
    
    /**
     * Setter for property origenR.
     * @param origenR New value of property origenR.
     */
    public void setOrigenR(java.lang.String origenR) {
        this.origenR = origenR;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property recurso_req.
     * @return Value of property recurso_req.
     */
    public java.lang.String getRecurso_req() {
        return recurso_req;
    }
    
    /**
     * Setter for property recurso_req.
     * @param recurso_req New value of property recurso_req.
     */
    public void setRecurso_req(java.lang.String recurso_req) {
        this.recurso_req = recurso_req;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property dstrct_code.
     * @return Value of property dstrct_code.
     */
    public java.lang.String getDstrct_code() {
        return dstrct_code;
    }
    
    /**
     * Setter for property dstrct_code.
     * @param dstrct_code New value of property dstrct_code.
     */
    public void setDstrct_code(java.lang.String dstrct_code) {
        this.dstrct_code = dstrct_code;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property num_sec.
     * @return Value of property num_sec.
     */
    public int getNum_sec() {
        return num_sec;
    }
    
    /**
     * Setter for property num_sec.
     * @param num_sec New value of property num_sec.
     */
    public void setNum_sec(int num_sec) {
        this.num_sec = num_sec;
    }
    
    /**
     * Getter for property cliejec.
     * @return Value of property cliejec.
     */
    public java.lang.String getCliejec() {
        return cliejec;
    }
    
    /**
     * Setter for property cliejec.
     * @param cliejec New value of property cliejec.
     */
    public void setCliejec(java.lang.String cliejec) {
        this.cliejec = cliejec;
    }
    
    /**
     * Getter for property fecdspejec.
     * @return Value of property fecdspejec.
     */
    public java.sql.Timestamp getFecdspejec() {
        return fecdspejec;
    }
    
    /**
     * Setter for property fecdspejec.
     * @param fecdspejec New value of property fecdspejec.
     */
    public void setFecdspejec(java.sql.Timestamp fecdspejec) {
        this.fecdspejec = fecdspejec;
    }
    
    /**
     * Getter for property numplaejec.
     * @return Value of property numplaejec.
     */
    public java.lang.String getNumplaejec() {
        return numplaejec;
    }
    
    /**
     * Setter for property numplaejec.
     * @param numplaejec New value of property numplaejec.
     */
    public void setNumplaejec(java.lang.String numplaejec) {
        this.numplaejec = numplaejec;
    }
    
    /**
     * Getter for property stdejec.
     * @return Value of property stdejec.
     */
    public java.lang.String getStdejec() {
        return stdejec;
    }
    
    /**
     * Setter for property stdejec.
     * @param stdejec New value of property stdejec.
     */
    public void setStdejec(java.lang.String stdejec) {
        this.stdejec = stdejec;
    }
    
    /**
     * Getter for property desEjec.
     * @return Value of property desEjec.
     */
    public java.lang.String getDesEjec() {
        return desEjec;
    }
    
    /**
     * Setter for property desEjec.
     * @param desEjec New value of property desEjec.
     */
    public void setDesEjec(java.lang.String desEjec) {
        this.desEjec = desEjec;
    }
    
    /**
     * Getter for property oriEjec.
     * @return Value of property oriEjec.
     */
    public java.lang.String getOriEjec() {
        return oriEjec;
    }
    
    /**
     * Setter for property oriEjec.
     * @param oriEjec New value of property oriEjec.
     */
    public void setOriEjec(java.lang.String oriEjec) {
        this.oriEjec = oriEjec;
    }
    //Diogenes
    
    public void setAgasoc(java.lang.String agasoc) {
        this.agasoc = agasoc;
    }
        
    public java.lang.String getAgasoc() {
        return agasoc;
    }
    
    public void setNomAgasoc(java.lang.String nomagasoc) {
        this.nomagasoc = nomagasoc;
    }
        
    public java.lang.String getNomAgasoc() {
        return nomagasoc;
    }
    
    public void setNomorirec(java.lang.String nom) {
        this.nomorirec = nom;
    }
    
    public java.lang.String getNomorirec() {
        return nomorirec;
    }
   
    public void setNomDesrec(java.lang.String nom) {
        this.nomdesrec = nom;
    }
    
    public java.lang.String getNomdesrec() {
        return nomdesrec;
    }
    
    public void setNomClienreq(java.lang.String nom) {
        this.nomclienreq = nom;
    }
    
    public java.lang.String getNomClienreq() {
        return nomclienreq;
    }

    public void setVehiculos(int can) {
        this.vehiculos = can;
    }
    
    
    public int getVehiculos() {
        return vehiculos;
    }
    
    public void setRemDespachada(int can) {
        this.remdespachada = can;
    }
    
    
    public int getRemDespachada() {
        return remdespachada;
    }
    
    public void setVehiculoutil(int can) {
        this.vehiculoutil = can;
    }
        
    public int getVehiculoutil() {
        return vehiculoutil;
    }
    
    public void setUtilflota(double can) {
        this.utilflota = can;
    }
        
    public double getUtilflota() {
        return utilflota;
    }
    
    public void setRetorno(double can) {
        this.retorno = can;
    }
        
    public double getRetorno() {
        return retorno;
    }
    
    public void setFecha_dispxag (String fecha_dispxag){
        this.fecha_dispxag=fecha_dispxag;
    }
    public String getFecha_dispxag(){
        return fecha_dispxag;
    }
    
    public void setTipo_rec_rec(java.lang.String tprr) {
        this.tipo_rec_rec = tprr;
    }
        
    public java.lang.String getTipo_rec_rec() {
        return tipo_rec_rec;
    }
    public void setRecurso_rec(java.lang.String rr) {
        this.recurso_rec = rr;
    }
        
    public java.lang.String getRecurso_rec() {
        return recurso_rec;
    }
    public void setEqasorec(java.lang.String eqaso) {
        this.equ_aso_rec = eqaso;
    }
        
    public java.lang.String getEqasorec() {
        return equ_aso_rec;
    }
    public void setFecdisrec(java.lang.String fec) {
        this.fecdis_rec = fec;
    }
        
    public java.lang.String getFecdisrec() {
        return fecdis_rec;
    }
    public void setFecasigrec(java.lang.String fec) {
        this.fecasig_rec = fec;
    }
        
    public java.lang.String getFecasigrec() {
        return fecasig_rec;
    }
    /**
     * Getter for property std_job_desc_req.
     * @return Value of property std_job_desc_req.
     */
    public java.lang.String getStd_job_desc_req() {
        return std_job_desc_req;
    }
    
    /**
     * Setter for property std_job_desc_req.
     * @param std_job_desc_req New value of property std_job_desc_req.
     */
    public void setStd_job_desc_req(java.lang.String std_job_desc_req) {
        this.std_job_desc_req = std_job_desc_req;
    }
    
    /**
     * Getter for property std_job_desc_rec.
     * @return Value of property std_job_desc_rec.
     */
    public java.lang.String getStd_job_desc_rec() {
        return std_job_desc_rec;
    }
    
    /**
     * Setter for property std_job_desc_rec.
     * @param std_job_desc_rec New value of property std_job_desc_rec.
     */
    public void setStd_job_desc_rec(java.lang.String std_job_desc_rec) {
        this.std_job_desc_rec = std_job_desc_rec;
    }
    
    /**
     * Getter for property origen_req.
     * @return Value of property origen_req.
     */
    public java.lang.String getOrigen_req() {
        return origen_req;
    }
    
    /**
     * Setter for property origen_req.
     * @param origen_req New value of property origen_req.
     */
    public void setOrigen_req(java.lang.String origen_req) {
        this.origen_req = origen_req;
    }
    
    /**
     * Getter for property destino_req.
     * @return Value of property destino_req.
     */
    public java.lang.String getDestino_req() {
        return destino_req;
    }
    
    /**
     * Setter for property destino_req.
     * @param destino_req New value of property destino_req.
     */
    public void setDestino_req(java.lang.String destino_req) {
        this.destino_req = destino_req;
    }
    
    /**
     * Getter for property tipo_asoc.
     * @return Value of property tipo_asoc.
     */
    public java.lang.String getTipo_asoc() {
        return tipo_asoc;
    }
    
    /**
     * Setter for property tipo_asoc.
     * @param tipo_asoc New value of property tipo_asoc.
     */
    public void setTipo_asoc(java.lang.String tipo_asoc) {
        this.tipo_asoc = tipo_asoc;
    }
    
    /**
     * Getter for property aplazado.
     * @return Value of property aplazado.
     */
    public boolean isAplazado() {
        return aplazado;
    }
    
    /**
     * Setter for property aplazado.
     * @param aplazado New value of property aplazado.
     */
    public void setAplazado(boolean aplazado) {
        this.aplazado = aplazado;
    }
    
    /**
     * Getter for property wgroup.
     * @return Value of property wgroup.
     */
    public java.lang.String getWgroup() {
        return wgroup;
    }
    
    /**
     * Setter for property wgroup.
     * @param wgroup New value of property wgroup.
     */
    public void setWgroup(java.lang.String wgroup) {
        this.wgroup = wgroup;
    }
    
    /**
     * Getter for property nombrecond.
     * @return Value of property nombrecond.
     */
    public java.lang.String getNombrecond() {
        return nombrecond;
    }
    
    /**
     * Setter for property nombrecond.
     * @param nombrecond New value of property nombrecond.
     */
    public void setNombrecond(java.lang.String nombrecond) {
        this.nombrecond = nombrecond;
    }
    
}
