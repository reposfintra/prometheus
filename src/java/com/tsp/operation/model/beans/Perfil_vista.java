/*
 * Perfil_vista.java
 *
 * Created on 13 de julio de 2005, 15:26
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  rodrigo
 */
public class Perfil_vista implements Serializable{
    private String perfil;
    private String pagina;
    private String campo;
    private String visible;
    private String editable;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;

    public static Perfil_vista load(ResultSet rs)throws SQLException{
        Perfil_vista t = new Perfil_vista();
        t.setPerfil(rs.getString("perfil"));
        t.setPagina(rs.getString("pagina"));
        t.setCampo(rs.getString("campo"));
        t.setVisible(rs.getString("visible"));
        t.setEditable(rs.getString("editable"));
        t.setCia(rs.getString("cia"));
        t.setRec_status(rs.getString("rec_status"));
        t.setLast_update(rs.getDate("last_update"));
        t.setUser_update(rs.getString("user_update"));
        t.setCreation_date(rs.getDate("creation_date"));
        t.setCreation_user(rs.getString("creation_user"));
        return t;
    }
    
    public void setPerfil(java.lang.String perfil) {
        this.perfil = perfil;
    }
    public void setPagina(java.lang.String pagina) {
        this.pagina = pagina;
    }
    public void setCampo(java.lang.String campo) {
        this.campo = campo;
    }
    public void setVisible(java.lang.String visible) {
        this.visible = visible;
    }
    public void setEditable(java.lang.String editable) {
        this.editable = editable;
    }
    
    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }

    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
     public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    } 
    
     public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }

    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }

    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }

    public java.lang.String getPerfil() {
        return perfil;
    }
    public java.lang.String getPagina() {
        return pagina;
    }
    public java.lang.String getCampo() {
        return campo;
    }
    public java.lang.String getVisible() {
        return visible;
    }
    public java.lang.String getEditable() {
        return editable;
    }
    public java.util.Date getCreation_date() {
        return creation_date;
    }
    
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    public java.util.Date getLast_update() {
        return last_update;
    }
    
    public java.lang.String getRec_status() {
        return rec_status;
    }
    
    public java.lang.String getCia() {
        return cia;
    }
    
    public java.lang.String getUser_update() {
        return user_update;
    }
}
