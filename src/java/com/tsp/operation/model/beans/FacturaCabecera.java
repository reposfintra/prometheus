/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class FacturaCabecera {



  

    private String reg_status;
    private String dstrct;
    private String tipo_documento;
    private String documento;
    private String nit;
    private String codcli;
    private String concepto;
    private String fecha_factura;
    private String fecha_vencimiento;
    private String fecha_ultimo_pago;
    private String fecha_impresion;
    private String descripcion;
    private String observacion;
    private Double valor_factura;
    private Double valor_abono;
    private Double valor_saldo;
    private Double valor_facturame;
    private Double valor_abonome;
    private Double valor_saldome;
    private Double valor_tasa;
    private String moneda;
    private int    cantidad_items;
    private String forma_pago;
    private String agencia_facturacion;
    private String agencia_cobro;
    private String zona;
    private String clasificacion1;
    private String clasificacion2;
    private String clasificacion3;
    private int    transaccion;
    private int    transaccion_anulacion;
    private String fecha_contabilizacion;
    private String fecha_anulacion;
    private String fecha_contabilizacion_anulacion ;
    private String base;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String fecha_probable_pago;
    private String flujo;
    private String rif;
    private String cmc;
    private String usuario_anulo;
    private String formato;
    private String agencia_impresion;
    private String periodo;
    private Double valor_tasa_remesa;
    private String negasoc;
    private String num_doc_fen;
    private String obs;
    private String pagado_fenalco;
    private String corficolombiana;
    private String tipo_ref1;
    private String ref1;
    private String tipo_ref2 ;
    private String ref2;
    private String dstrct_ultimo_ingreso;
    private String tipo_documento_ultimo_ingreso;
    private String num_ingreso_ultimo_ingreso;
    private int    item_ultimo_ingreso;
    private String fec_envio_fiducia;
    private String nit_enviado_fiducia;
    private String tipo_referencia_1;
    private String referencia_1;
    private String tipo_referencia_2;
    private String referencia_2;
    private String tipo_referencia_3;
    private String referencia_3;
    private String nc_traslado;
    private String fecha_nc_traslado;
    private String tipo_nc;
    private String numero_nc;
    private String factura_traslado;




    public FacturaCabecera () {
       inicializar() ;

    }


    public  void inicializar() {

        reg_status = "" ;
        dstrct = "" ;
        tipo_documento = "" ;
        documento = "" ;
        nit = "" ;
        codcli = "" ;
        concepto = "" ;
        fecha_factura = "" ;
        fecha_vencimiento = "" ;
        fecha_ultimo_pago = "" ;
        fecha_impresion = "" ;
        descripcion = "" ;
        observacion = "" ;
        valor_factura = 0.00 ;
        valor_abono = 0.00 ;
        valor_saldo = 0.00 ;
        valor_facturame = 0.00 ;
        valor_abonome = 0.00 ;
        valor_saldome = 0.00 ;
        valor_tasa = 0.00 ;
        moneda = "" ;
        cantidad_items = 0 ;
        forma_pago = "" ;
        agencia_facturacion = "" ;
        agencia_cobro = "" ;
        zona = "" ;
        clasificacion1 = "" ;
        clasificacion2 = "" ;
        clasificacion3 = "" ;
        transaccion = 0 ;
        transaccion_anulacion = 0 ;
        fecha_contabilizacion = "0099-01-01 00:00:00" ;
        fecha_anulacion = "0099-01-01 00:00:00" ;
        fecha_contabilizacion_anulacion  = "0099-01-01 00:00:00" ;
        base = "" ;
        last_update = "" ;
        user_update = "" ;
        creation_date = "" ;
        creation_user = "" ;
        fecha_probable_pago = "0099-01-01 00:00:00" ;
        flujo = "" ;
        rif = "" ;
        cmc = "" ;
        usuario_anulo = "" ;
        formato = "" ;
        agencia_impresion = "" ;
        periodo = "" ;
        valor_tasa_remesa = 0.00 ;
        negasoc = "" ;
        num_doc_fen = "" ;
        obs = "" ;
        pagado_fenalco = "" ;
        corficolombiana = "" ;
        tipo_ref1 = "" ;
        ref1 = "" ;
        tipo_ref2  = "" ;
        ref2 = "" ;
        dstrct_ultimo_ingreso = "" ;
        tipo_documento_ultimo_ingreso = "" ;
        num_ingreso_ultimo_ingreso = "" ;
        item_ultimo_ingreso = 0 ;
        fec_envio_fiducia = "0099-01-01 00:00:00" ;
        nit_enviado_fiducia = "" ;
        tipo_referencia_1 = "" ;
        referencia_1 = "" ;
        tipo_referencia_2 = "" ;
        referencia_2 = "" ;
        tipo_referencia_3 = "" ;
        referencia_3 = "" ;
        nc_traslado = "" ;
        fecha_nc_traslado = "0099-01-01 00:00:00" ;
        tipo_nc = "" ;
        numero_nc = "" ;
        factura_traslado = "" ;


    }


    /** Extrae un registro de la BD correspondientes a las acciones de una solicitudes que no esta facturada */
    public static FacturaCabecera load(java.sql.ResultSet rs)throws java.sql.SQLException{


        FacturaCabecera facturaCabecera = new FacturaCabecera();


        facturaCabecera.setReg_status(rs.getString("reg_status") ) ;
        facturaCabecera.setDstrct(rs.getString("dstrct") ) ;
        facturaCabecera.setTipo_documento(rs.getString("tipo_documento") ) ;
        facturaCabecera.setDocumento(rs.getString("documento") ) ;
        facturaCabecera.setNit(rs.getString("nit") ) ;
        facturaCabecera.setCodcli(rs.getString("codcli") ) ;
        facturaCabecera.setConcepto(rs.getString("concepto") ) ;
        facturaCabecera.setFecha_factura(rs.getString("fecha_factura") ) ;
        facturaCabecera.setFecha_vencimiento(rs.getString("fecha_vencimiento") ) ;
        facturaCabecera.setFecha_ultimo_pago(rs.getString("fecha_ultimo_pago") ) ;
        facturaCabecera.setFecha_impresion(rs.getString("fecha_impresion") ) ;
        facturaCabecera.setDescripcion(rs.getString("descripcion") ) ;
        facturaCabecera.setObservacion(rs.getString("observacion") ) ;
        facturaCabecera.setValor_factura(rs.getDouble("valor_factura") ) ;
        facturaCabecera.setValor_abono(rs.getDouble("valor_abono") ) ;
        facturaCabecera.setValor_saldo(rs.getDouble("valor_saldo") ) ;
        facturaCabecera.setValor_facturame(rs.getDouble("valor_facturame") ) ;
        facturaCabecera.setValor_abonome(rs.getDouble("valor_abonome") ) ;
        facturaCabecera.setValor_saldome(rs.getDouble("valor_saldome") ) ;
        facturaCabecera.setValor_tasa(rs.getDouble("valor_tasa") ) ;
        facturaCabecera.setMoneda(rs.getString("moneda") ) ;
        facturaCabecera.setCantidad_items(rs.getInt("cantidad_items") ) ;
        facturaCabecera.setForma_pago(rs.getString("forma_pago") ) ;
        facturaCabecera.setAgencia_facturacion(rs.getString("agencia_facturacion") ) ;
        facturaCabecera.setAgencia_cobro(rs.getString("agencia_cobro") ) ;
        facturaCabecera.setZona(rs.getString("zona") ) ;
        facturaCabecera.setClasificacion1(rs.getString("clasificacion1") ) ;
        facturaCabecera.setClasificacion2(rs.getString("clasificacion2") ) ;
        facturaCabecera.setClasificacion3(rs.getString("clasificacion3") ) ;
        facturaCabecera.setTransaccion(rs.getInt("transaccion") ) ;
        facturaCabecera.setTransaccion_anulacion(rs.getInt("transaccion_anulacion") ) ;
        facturaCabecera.setFecha_contabilizacion(rs.getString("fecha_contabilizacion") ) ;
        facturaCabecera.setFecha_anulacion(rs.getString("fecha_anulacion") ) ;
        facturaCabecera.setFecha_contabilizacion_anulacion(rs.getString("fecha_contabilizacion_anulacion") ) ;
        facturaCabecera.setBase(rs.getString("base") ) ;
        facturaCabecera.setLast_update(rs.getString("last_update") ) ;
        facturaCabecera.setUser_update(rs.getString("user_update") ) ;
        facturaCabecera.setCreation_date(rs.getString("creation_date") ) ;
        facturaCabecera.setCreation_user(rs.getString("creation_user") ) ;
        facturaCabecera.setFecha_probable_pago(rs.getString("fecha_probable_pago") ) ;
        facturaCabecera.setFlujo(rs.getString("flujo") ) ;
        facturaCabecera.setRif(rs.getString("rif") ) ;
        facturaCabecera.setCmc(rs.getString("cmc") ) ;
        facturaCabecera.setUsuario_anulo(rs.getString("usuario_anulo") ) ;
        facturaCabecera.setFormato(rs.getString("formato") ) ;
        facturaCabecera.setAgencia_impresion(rs.getString("agencia_impresion") ) ;
        facturaCabecera.setPeriodo(rs.getString("periodo") ) ;
        facturaCabecera.setValor_tasa_remesa(rs.getDouble("valor_tasa_remesa") ) ;
        facturaCabecera.setNegasoc(rs.getString("negasoc") ) ;
        facturaCabecera.setNum_doc_fen(rs.getString("num_doc_fen") ) ;
        facturaCabecera.setObs(rs.getString("obs") ) ;
        facturaCabecera.setPagado_fenalco(rs.getString("pagado_fenalco") ) ;
        facturaCabecera.setCorficolombiana(rs.getString("corficolombiana") ) ;
        facturaCabecera.setTipo_ref1(rs.getString("tipo_ref1") ) ;
        facturaCabecera.setRef1(rs.getString("ref1") ) ;
        facturaCabecera.setTipo_ref2(rs.getString("tipo_ref2") ) ;
        facturaCabecera.setRef2(rs.getString("ref2") ) ;
        facturaCabecera.setDstrct_ultimo_ingreso(rs.getString("dstrct_ultimo_ingreso") ) ;
        facturaCabecera.setTipo_documento_ultimo_ingreso(rs.getString("tipo_documento_ultimo_ingreso") ) ;
        facturaCabecera.setNum_ingreso_ultimo_ingreso(rs.getString("num_ingreso_ultimo_ingreso") ) ;
        facturaCabecera.setItem_ultimo_ingreso(rs.getInt("item_ultimo_ingreso") ) ;
        facturaCabecera.setFec_envio_fiducia(rs.getString("fec_envio_fiducia") ) ;
        facturaCabecera.setNit_enviado_fiducia(rs.getString("nit_enviado_fiducia") ) ;
        facturaCabecera.setTipo_referencia_1(rs.getString("tipo_referencia_1") ) ;
        facturaCabecera.setReferencia_1(rs.getString("referencia_1") ) ;
        facturaCabecera.setTipo_referencia_2(rs.getString("tipo_referencia_2") ) ;
        facturaCabecera.setReferencia_2(rs.getString("referencia_2") ) ;
        facturaCabecera.setTipo_referencia_3(rs.getString("tipo_referencia_3") ) ;
        facturaCabecera.setReferencia_3(rs.getString("referencia_3") ) ;
        facturaCabecera.setNc_traslado(rs.getString("nc_traslado") ) ;
        facturaCabecera.setFecha_nc_traslado(rs.getString("fecha_nc_traslado") ) ;
        facturaCabecera.setTipo_nc(rs.getString("tipo_nc") ) ;
        facturaCabecera.setNumero_nc(rs.getString("numero_nc") ) ;
        facturaCabecera.setFactura_traslado(rs.getString("factura_traslado") ) ;



        return facturaCabecera;

    }









    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the codcli
     */
    public String getCodcli() {
        return codcli;
    }

    /**
     * @param codcli the codcli to set
     */
    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    /**
     * @return the concepto
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * @param concepto the concepto to set
     */
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    /**
     * @return the fecha_factura
     */
    public String getFecha_factura() {
        return fecha_factura;
    }

    /**
     * @param fecha_factura the fecha_factura to set
     */
    public void setFecha_factura(String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }

    /**
     * @return the fecha_vencimiento
     */
    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * @param fecha_vencimiento the fecha_vencimiento to set
     */
    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * @return the fecha_ultimo_pago
     */
    public String getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    /**
     * @param fecha_ultimo_pago the fecha_ultimo_pago to set
     */
    public void setFecha_ultimo_pago(String fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    /**
     * @return the fecha_impresion
     */
    public String getFecha_impresion() {
        return fecha_impresion;
    }

    /**
     * @param fecha_impresion the fecha_impresion to set
     */
    public void setFecha_impresion(String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the valor_factura
     */
    public Double getValor_factura() {
        return valor_factura;
    }

    /**
     * @param valor_factura the valor_factura to set
     */
    public void setValor_factura(Double valor_factura) {
        this.valor_factura = valor_factura;
    }

    /**
     * @return the valor_abono
     */
    public Double getValor_abono() {
        return valor_abono;
    }

    /**
     * @param valor_abono the valor_abono to set
     */
    public void setValor_abono(Double valor_abono) {
        this.valor_abono = valor_abono;
    }

    /**
     * @return the valor_saldo
     */
    public Double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo(Double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    /**
     * @return the valor_facturame
     */
    public Double getValor_facturame() {
        return valor_facturame;
    }

    /**
     * @param valor_facturame the valor_facturame to set
     */
    public void setValor_facturame(Double valor_facturame) {
        this.valor_facturame = valor_facturame;
    }

    /**
     * @return the valor_abonome
     */
    public Double getValor_abonome() {
        return valor_abonome;
    }

    /**
     * @param valor_abonome the valor_abonome to set
     */
    public void setValor_abonome(Double valor_abonome) {
        this.valor_abonome = valor_abonome;
    }

    /**
     * @return the valor_saldome
     */
    public Double getValor_saldome() {
        return valor_saldome;
    }

    /**
     * @param valor_saldome the valor_saldome to set
     */
    public void setValor_saldome(Double valor_saldome) {
        this.valor_saldome = valor_saldome;
    }

    /**
     * @return the valor_tasa
     */
    public Double getValor_tasa() {
        return valor_tasa;
    }

    /**
     * @param valor_tasa the valor_tasa to set
     */
    public void setValor_tasa(Double valor_tasa) {
        this.valor_tasa = valor_tasa;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the cantidad_items
     */
    public int getCantidad_items() {
        return cantidad_items;
    }

    /**
     * @param cantidad_items the cantidad_items to set
     */
    public void setCantidad_items(int cantidad_items) {
        this.cantidad_items = cantidad_items;
    }

    /**
     * @return the forma_pago
     */
    public String getForma_pago() {
        return forma_pago;
    }

    /**
     * @param forma_pago the forma_pago to set
     */
    public void setForma_pago(String forma_pago) {
        this.forma_pago = forma_pago;
    }

    /**
     * @return the agencia_facturacion
     */
    public String getAgencia_facturacion() {
        return agencia_facturacion;
    }

    /**
     * @param agencia_facturacion the agencia_facturacion to set
     */
    public void setAgencia_facturacion(String agencia_facturacion) {
        this.agencia_facturacion = agencia_facturacion;
    }

    /**
     * @return the agencia_cobro
     */
    public String getAgencia_cobro() {
        return agencia_cobro;
    }

    /**
     * @param agencia_cobro the agencia_cobro to set
     */
    public void setAgencia_cobro(String agencia_cobro) {
        this.agencia_cobro = agencia_cobro;
    }

    /**
     * @return the zona
     */
    public String getZona() {
        return zona;
    }

    /**
     * @param zona the zona to set
     */
    public void setZona(String zona) {
        this.zona = zona;
    }

    /**
     * @return the clasificacion1
     */
    public String getClasificacion1() {
        return clasificacion1;
    }

    /**
     * @param clasificacion1 the clasificacion1 to set
     */
    public void setClasificacion1(String clasificacion1) {
        this.clasificacion1 = clasificacion1;
    }

    /**
     * @return the clasificacion2
     */
    public String getClasificacion2() {
        return clasificacion2;
    }

    /**
     * @param clasificacion2 the clasificacion2 to set
     */
    public void setClasificacion2(String clasificacion2) {
        this.clasificacion2 = clasificacion2;
    }

    /**
     * @return the clasificacion3
     */
    public String getClasificacion3() {
        return clasificacion3;
    }

    /**
     * @param clasificacion3 the clasificacion3 to set
     */
    public void setClasificacion3(String clasificacion3) {
        this.clasificacion3 = clasificacion3;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the transaccion_anulacion
     */
    public int getTransaccion_anulacion() {
        return transaccion_anulacion;
    }

    /**
     * @param transaccion_anulacion the transaccion_anulacion to set
     */
    public void setTransaccion_anulacion(int transaccion_anulacion) {
        this.transaccion_anulacion = transaccion_anulacion;
    }

    /**
     * @return the fecha_contabilizacion
     */
    public String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    /**
     * @param fecha_contabilizacion the fecha_contabilizacion to set
     */
    public void setFecha_contabilizacion(String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    /**
     * @return the fecha_anulacion
     */
    public String getFecha_anulacion() {
        return fecha_anulacion;
    }

    /**
     * @param fecha_anulacion the fecha_anulacion to set
     */
    public void setFecha_anulacion(String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }

    /**
     * @return the fecha_contabilizacion_anulacion
     */
    public String getFecha_contabilizacion_anulacion() {
        return fecha_contabilizacion_anulacion;
    }

    /**
     * @param fecha_contabilizacion_anulacion the fecha_contabilizacion_anulacion to set
     */
    public void setFecha_contabilizacion_anulacion(String fecha_contabilizacion_anulacion) {
        this.fecha_contabilizacion_anulacion = fecha_contabilizacion_anulacion;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the fecha_probable_pago
     */
    public String getFecha_probable_pago() {
        return fecha_probable_pago;
    }

    /**
     * @param fecha_probable_pago the fecha_probable_pago to set
     */
    public void setFecha_probable_pago(String fecha_probable_pago) {
        this.fecha_probable_pago = fecha_probable_pago;
    }

    /**
     * @return the flujo
     */
    public String getFlujo() {
        return flujo;
    }

    /**
     * @param flujo the flujo to set
     */
    public void setFlujo(String flujo) {
        this.flujo = flujo;
    }

    /**
     * @return the rif
     */
    public String getRif() {
        return rif;
    }

    /**
     * @param rif the rif to set
     */
    public void setRif(String rif) {
        this.rif = rif;
    }

    /**
     * @return the cmc
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc the cmc to set
     */
    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    /**
     * @return the usuario_anulo
     */
    public String getUsuario_anulo() {
        return usuario_anulo;
    }

    /**
     * @param usuario_anulo the usuario_anulo to set
     */
    public void setUsuario_anulo(String usuario_anulo) {
        this.usuario_anulo = usuario_anulo;
    }

    /**
     * @return the formato
     */
    public String getFormato() {
        return formato;
    }

    /**
     * @param formato the formato to set
     */
    public void setFormato(String formato) {
        this.formato = formato;
    }

    /**
     * @return the agencia_impresion
     */
    public String getAgencia_impresion() {
        return agencia_impresion;
    }

    /**
     * @param agencia_impresion the agencia_impresion to set
     */
    public void setAgencia_impresion(String agencia_impresion) {
        this.agencia_impresion = agencia_impresion;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the valor_tasa_remesa
     */
    public Double getValor_tasa_remesa() {
        return valor_tasa_remesa;
    }

    /**
     * @param valor_tasa_remesa the valor_tasa_remesa to set
     */
    public void setValor_tasa_remesa(Double valor_tasa_remesa) {
        this.valor_tasa_remesa = valor_tasa_remesa;
    }

    /**
     * @return the negasoc
     */
    public String getNegasoc() {
        return negasoc;
    }

    /**
     * @param negasoc the negasoc to set
     */
    public void setNegasoc(String negasoc) {
        this.negasoc = negasoc;
    }

    /**
     * @return the num_doc_fen
     */
    public String getNum_doc_fen() {
        return num_doc_fen;
    }

    /**
     * @param num_doc_fen the num_doc_fen to set
     */
    public void setNum_doc_fen(String num_doc_fen) {
        this.num_doc_fen = num_doc_fen;
    }

    /**
     * @return the obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * @param obs the obs to set
     */
    public void setObs(String obs) {
        this.obs = obs;
    }

    /**
     * @return the pagado_fenalco
     */
    public String getPagado_fenalco() {
        return pagado_fenalco;
    }

    /**
     * @param pagado_fenalco the pagado_fenalco to set
     */
    public void setPagado_fenalco(String pagado_fenalco) {
        this.pagado_fenalco = pagado_fenalco;
    }

    /**
     * @return the corficolombiana
     */
    public String getCorficolombiana() {
        return corficolombiana;
    }

    /**
     * @param corficolombiana the corficolombiana to set
     */
    public void setCorficolombiana(String corficolombiana) {
        this.corficolombiana = corficolombiana;
    }

    /**
     * @return the tipo_ref1
     */
    public String getTipo_ref1() {
        return tipo_ref1;
    }

    /**
     * @param tipo_ref1 the tipo_ref1 to set
     */
    public void setTipo_ref1(String tipo_ref1) {
        this.tipo_ref1 = tipo_ref1;
    }

    /**
     * @return the tipo_ref2
     */
    public String getTipo_ref2() {
        return tipo_ref2;
    }

    /**
     * @param tipo_ref2 the tipo_ref2 to set
     */
    public void setTipo_ref2(String tipo_ref2) {
        this.tipo_ref2 = tipo_ref2;
    }

    /**
     * @return the ref2
     */
    public String getRef2() {
        return ref2;
    }

    /**
     * @param ref2 the ref2 to set
     */
    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }

    /**
     * @return the dstrct_ultimo_ingreso
     */
    public String getDstrct_ultimo_ingreso() {
        return dstrct_ultimo_ingreso;
    }

    /**
     * @param dstrct_ultimo_ingreso the dstrct_ultimo_ingreso to set
     */
    public void setDstrct_ultimo_ingreso(String dstrct_ultimo_ingreso) {
        this.dstrct_ultimo_ingreso = dstrct_ultimo_ingreso;
    }

    /**
     * @return the tipo_documento_ultimo_ingreso
     */
    public String getTipo_documento_ultimo_ingreso() {
        return tipo_documento_ultimo_ingreso;
    }

    /**
     * @param tipo_documento_ultimo_ingreso the tipo_documento_ultimo_ingreso to set
     */
    public void setTipo_documento_ultimo_ingreso(String tipo_documento_ultimo_ingreso) {
        this.tipo_documento_ultimo_ingreso = tipo_documento_ultimo_ingreso;
    }

    /**
     * @return the num_ingreso_ultimo_ingreso
     */
    public String getNum_ingreso_ultimo_ingreso() {
        return num_ingreso_ultimo_ingreso;
    }

    /**
     * @param num_ingreso_ultimo_ingreso the num_ingreso_ultimo_ingreso to set
     */
    public void setNum_ingreso_ultimo_ingreso(String num_ingreso_ultimo_ingreso) {
        this.num_ingreso_ultimo_ingreso = num_ingreso_ultimo_ingreso;
    }

    /**
     * @return the item_ultimo_ingreso
     */
    public int getItem_ultimo_ingreso() {
        return item_ultimo_ingreso;
    }

    /**
     * @param item_ultimo_ingreso the item_ultimo_ingreso to set
     */
    public void setItem_ultimo_ingreso(int item_ultimo_ingreso) {
        this.item_ultimo_ingreso = item_ultimo_ingreso;
    }

    /**
     * @return the fec_envio_fiducia
     */
    public String getFec_envio_fiducia() {
        return fec_envio_fiducia;
    }

    /**
     * @param fec_envio_fiducia the fec_envio_fiducia to set
     */
    public void setFec_envio_fiducia(String fec_envio_fiducia) {
        this.fec_envio_fiducia = fec_envio_fiducia;
    }

    /**
     * @return the nit_enviado_fiducia
     */
    public String getNit_enviado_fiducia() {
        return nit_enviado_fiducia;
    }

    /**
     * @param nit_enviado_fiducia the nit_enviado_fiducia to set
     */
    public void setNit_enviado_fiducia(String nit_enviado_fiducia) {
        this.nit_enviado_fiducia = nit_enviado_fiducia;
    }

    /**
     * @return the tipo_referencia_1
     */
    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    /**
     * @param tipo_referencia_1 the tipo_referencia_1 to set
     */
    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    /**
     * @return the tipo_referencia_2
     */
    public String getTipo_referencia_2() {
        return tipo_referencia_2;
    }

    /**
     * @param tipo_referencia_2 the tipo_referencia_2 to set
     */
    public void setTipo_referencia_2(String tipo_referencia_2) {
        this.tipo_referencia_2 = tipo_referencia_2;
    }

    /**
     * @return the referencia_2
     */
    public String getReferencia_2() {
        return referencia_2;
    }

    /**
     * @param referencia_2 the referencia_2 to set
     */
    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    /**
     * @return the tipo_referencia_3
     */
    public String getTipo_referencia_3() {
        return tipo_referencia_3;
    }

    /**
     * @param tipo_referencia_3 the tipo_referencia_3 to set
     */
    public void setTipo_referencia_3(String tipo_referencia_3) {
        this.tipo_referencia_3 = tipo_referencia_3;
    }

    /**
     * @return the referencia_3
     */
    public String getReferencia_3() {
        return referencia_3;
    }

    /**
     * @param referencia_3 the referencia_3 to set
     */
    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }

    /**
     * @return the nc_traslado
     */
    public String getNc_traslado() {
        return nc_traslado;
    }

    /**
     * @param nc_traslado the nc_traslado to set
     */
    public void setNc_traslado(String nc_traslado) {
        this.nc_traslado = nc_traslado;
    }

    /**
     * @return the fecha_nc_traslado
     */
    public String getFecha_nc_traslado() {
        return fecha_nc_traslado;
    }

    /**
     * @param fecha_nc_traslado the fecha_nc_traslado to set
     */
    public void setFecha_nc_traslado(String fecha_nc_traslado) {
        this.fecha_nc_traslado = fecha_nc_traslado;
    }

    /**
     * @return the tipo_nc
     */
    public String getTipo_nc() {
        return tipo_nc;
    }

    /**
     * @param tipo_nc the tipo_nc to set
     */
    public void setTipo_nc(String tipo_nc) {
        this.tipo_nc = tipo_nc;
    }

    /**
     * @return the numero_nc
     */
    public String getNumero_nc() {
        return numero_nc;
    }

    /**
     * @param numero_nc the numero_nc to set
     */
    public void setNumero_nc(String numero_nc) {
        this.numero_nc = numero_nc;
    }

    /**
     * @return the factura_traslado
     */
    public String getFactura_traslado() {
        return factura_traslado;
    }

    /**
     * @param factura_traslado the factura_traslado to set
     */
    public void setFactura_traslado(String factura_traslado) {
        this.factura_traslado = factura_traslado;
    }

    /**
     * @return the ref1
     */
    public String getRef1() {
        return ref1;
    }

    /**
     * @param ref1 the ref1 to set
     */
    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }







}
