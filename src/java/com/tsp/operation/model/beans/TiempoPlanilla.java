package com.tsp.operation.model.beans;

import java.util.*;
import java.io.*;

public class TiempoPlanilla {
  // ATRIBUTOS
      private String codigo;
      private int    secuencia;
      private String descripcion;
      private String fecha;
      private String codigo1;
      private String codigo2;
      private String diferencia;
      private String fecha_time1;
      private String fecha_time2;
      
      
  // METODOS
    
      public TiempoPlanilla() {}
    
   //set 
    public void setCodigo(String time_code){
        this.codigo=time_code;
    }
    
    public void setSecuencia(int secuencia){
        this.secuencia=secuencia;
    }
    
    public void setDescripcion(String desc){
        this.descripcion=desc;
    }
    
    public void setFecha(String fecha){
        this.fecha=fecha;
    }
    
    public void setCodigo1(String cod1){
        this.codigo1=cod1;
    }
    
    public void setCodigo2(String cod2){
        this.codigo2=cod2;
    }
    
    public void setDiferencia(String tiempo){
        this.diferencia=tiempo;
    }
    
    public void setFecha_time1(String fecha1){
        this.fecha_time1=fecha1;
    }
    
    public void setFecha_time2(String fecha2){
        this.fecha_time2=fecha2;
    }
    
    
    
    
    //get 
    public String  getCodigo(){
        return this.codigo;
    }
    
    public int getSecuencia(){
        return this.secuencia;
    }
    
    public String  getDescripcion(){
        return this.descripcion;
    }
    
    public String getFecha(){
        return this.fecha;
    }
    
    public String getCodigo1(){
        return this.codigo1;
    }
    
    public String getCodigo2(){
        return this.codigo2;
    }
    
    public String getDiferencia(){
       return this.diferencia;
    }
    
    public String getFecha_time1(){
        return this.fecha_time1;
    }
    
    public String getFecha_time2(){
        return this.fecha_time2;
    }
    
    
    // DIFERENCIA DE TIEMPOS
    // Las fecha deben estar formateadas como :  YYYY-MM-DD  HH:MM:SS
    
    public String BuscarDiferenciaFecha(String fecha1, String fecha2){
         
         String total="";
         int ano1=Integer.parseInt(fecha1.substring(0,4)  );  
         int mes1=Integer.parseInt(fecha1.substring(5,7)  );  
         int dia1=Integer.parseInt(fecha1.substring(8,10) );  
         int hh1= Integer.parseInt(fecha1.substring(11,13));  
         int min1=Integer.parseInt(fecha1.substring(14,16));  
         int ss1= Integer.parseInt(fecha1.substring(17,19));
         String tipo1= fecha1.substring(fecha1.length()-2,fecha1.length());
         if(tipo1.toUpperCase().equals("PM"))
             hh1+=12;
         
         int ano2=Integer.parseInt(fecha2.substring(0,4)  );  
         int mes2=Integer.parseInt(fecha2.substring(5,7)  );  
         int dia2=Integer.parseInt(fecha2.substring(8,10) );
         int hh2= Integer.parseInt(fecha2.substring(11,13));  
         int min2=Integer.parseInt(fecha2.substring(14,16));  
         int ss2= Integer.parseInt(fecha2.substring(17,19));
         String tipo2= fecha2.substring(fecha2.length()-2,fecha2.length());
         if(tipo2.toUpperCase().equals("PM"))
             hh2+=12;
         
         
 // Calculamos diferencia
         
         int totalAno = ano2-ano1;
         int totalMes = mes2-mes1;
         if(totalMes<0){
             totalAno--;
             totalMes+=12;
         }
         
         int totalDia = dia2-dia1;
         if(totalDia<0){
            totalMes--;
            totalDia+=31;
         }
         
         int totalHora= hh2-hh1;
         if(totalHora<0){
             totalDia--;
             totalHora+=24;
         }
         
         int totalMin = min2-min1;
         if(totalMin<0){
             totalHora--;
             totalMin+=60;
         }
         
         int totalSS  = ss2-ss1;
         if(totalSS<0){
             totalMin--;
             totalSS+=60;
         }
         
        
 // Formateamos la Diferencia  
         
         String ano =(totalAno==0)?"0000":"000"+String.valueOf(totalAno);
         
         String mes="";
         if(totalMes==0) mes="00";
         else
           mes=(totalMes>0 && totalMes<10)?"0"+String.valueOf(totalMes):String.valueOf(totalMes);
         
         String dia="";
         if(totalDia==0) dia="00";
         else
           dia=(totalDia>0 && totalDia<10)?"0"+String.valueOf(totalDia):String.valueOf(totalDia);
                  
         String hora="";
         if(totalHora==0) hora="00";
         else
           hora=(totalHora>0 && totalHora<10)?"0"+String.valueOf(totalHora):String.valueOf(totalHora);
         
        String minutos="";
         if(totalMin==0) minutos="00";
         else
           minutos=(totalMin>0 && totalMin<10)?"0"+String.valueOf(totalMin):String.valueOf(totalMin);
       
        String segundos="";
         if(totalSS==0) segundos="00";
         else
           segundos=(totalSS>0 && totalSS<10)?"0"+String.valueOf(totalSS):String.valueOf(totalSS);
              
      // Creamos la diferencia total
          total= ano +"-" + mes + "-" + dia + " " + hora + ":" + minutos + ":"+ segundos;
               
       return total;
    }
    
}
