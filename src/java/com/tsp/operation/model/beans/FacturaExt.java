    /***************************************
    * Nombre Clase ............. FacturaExt.java
    * Descripci�n  .. . . . . .  Factura, tiene una lista para sus itenes
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  21/12/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.*;

public class FacturaExt extends CXP_Doc{
    
    private List  items;
    
    
    
    public FacturaExt() {
        items  = new LinkedList();
    }
    
    
    
    
 // SET : 
    /* M�todos que guardan valores en el objeto
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
    
    public void setItem(List list){ this.items = list;   }
    
    
    
    
    
// GET :
    /**
     * M�todos que retornan  valores
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
    
    public List getItem()         { return this.items ; }
    
    
    
}
