package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;

public class Planilla implements Serializable {
    
    private String numpla;
    private java.util.Date fecpla;
    private String agcpla;
    private String oripla;
    private String despla;
    private String plaveh;
    private String cedcon;
    private String nitpro;
    private String platlr;
    private String ruta_pla;
    private String orden_carga;
    private String fecdsp;
    private String cia;
    private String tipoviaje;
    private java.util.Date fechaposllegada;
    private float pesoreal;
    private float vlrpla;
    private float vlrplafor;
    private String moneda;
    private String despachador;
    private java.util.Date creation_date;
    private String nombrecond;
    private String numrem;
    private String sj;
    private String sj_desc;
    private String nomdest;
    private String nomori;
    private java.util.Date printer_date;
    private String printer_date2;
    private String precinto;
    private String codruta;
    private float saldo;
    private String unit_trasn;
    private float unit_cost;
    private String group_code;
    private String feccum;
    private String traffic_code;
    private java.util.Date fechadesp;
    private String remision;
    private String contenedores="";
    private String tipocont = "FINV";
    private String pesoM ="";
    private String pesoP ="";
    private String nomprop="";
    
    
    
    private float segpla;
    private float retpla;
    private String unidcam;
    private String ultimoreporte;
    private float tiempoentransito;
    private float vlrpla2;
    private String nomcon;
    private String orinom;
    private String desnom;
    private String celularcon;
    private String observacion;
    private String tienedevol;
    private String status_220;
    private String reg_status;
    private String unit_vlr="";
    private String currency;
    private String last_update;
    private String base;
    private String corte;
    private String tipotrailer="";
    private String cf_code="";
    
    
    //PLANEACION DE LA OPERACION
    
    private java.util.Date fechapla;
    private java.sql.Timestamp fechadespacho;
    private java.sql.Timestamp fechaPLlegada;
    private String origenpla;
    private String destinopla;
    private String platr;
    private String distrito;
    private String proveedor="";
    private float totalajustes;
    private float totalanticipo;
    private String std_job_rec;
    
    //
    private String zona;
    private String nomzona;
    //ANULACION DE PLANILLAS
    private String causa;
    private String t_recuperacion;
    
    
    private String std_job_no_rec;//Juan 26.11.05
    
    //Reporte Diario de Despacho 29-11-05
    private String documentos;
   // private String contenedores;
    private String document;
    
    //Jose 30.11.05
    private int dias;
    
    //Jose 15.12.05
    private String fecha_placa;
    private String fecha_conductor;
    private String prox_pto_control="";
    private String fec_prox_pto_control="";
    private String nom_prox_pto_control="";
    private String clientes="";
    
    private String fecha_salida;
    private String inventario;
    private String sticker="";
    private double maxReanticipo=0;
    //Diogenes
    private String nomcliente;
    // Mario Fontalvo 07/05/2006
    private String Nomagc;
    private String Nomdesrel;
    private String destinoRelacionado;
    private double vlrPlanilla;
    private double porcentaje;
    private double vlrParcial;
    private double porcentajeProrrateo;
    private double vlrProrrateado;
    private double tasa;
    private Vector Remesas;
    private String tiene_doc;    
    private String tipoCarga;  
    
    private String account_code_i;
    private String account_code_c;
    private String unidad_i;
    private String unidad_c;
    
    private String factura;

    private String fecplanilla;
    
     //AMATURANA
    private String corrida_fra;
    private String status_fra;
    
    private String cmc;
    
    private boolean mostrarDetalles;
    
    private String eintermedias;
    private String neintermedias;

   /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }
    
    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }


    /**
     * Getter for property status_fra.
     * @return Value of property status_fra.
     */
    public java.lang.String getStatus_fra() {
        return status_fra;
    }
    
    /**
     * Setter for property status_fra.
     * @param status_fra New value of property status_fra.
     */
    public void setStatus_fra(java.lang.String status_fra) {
        this.status_fra = status_fra;
    }
    
    /**
     * Getter for property corrida_fra.
     * @return Value of property corrida_fra.
     */
    public java.lang.String getCorrida_fra() {
        return corrida_fra;
    }
    
    /**
     * Setter for property corrida_fra.
     * @param corrida_fra New value of property corrida_fra.
     */
    public void setCorrida_fra(java.lang.String corrida_fra) {
        this.corrida_fra = corrida_fra;
    }



   /**
     * Getter for property Remesas.
     * @return Value of property Remesas.
     */
    public java.util.Vector getRemesas() {
        return Remesas;
    }
    
    /**
     * Setter for property Remesas.
     * @param Remesas New value of property Remesas.
     */
    public void setRemesas(java.util.Vector Remesas) {
        this.Remesas = Remesas;
    }
    
    /**
     * Getter for property destinoRelacionado.
     * @return Value of property destinoRelacionado.
     */
    public java.lang.String getDestinoRelacionado() {
        return destinoRelacionado;
    }
    
    /**
     * Setter for property destinoRelacionado.
     * @param destinoRelacionado New value of property destinoRelacionado.
     */
    public void setDestinoRelacionado(java.lang.String destinoRelacionado) {
        this.destinoRelacionado = destinoRelacionado;
    }
    
    /**
     * Getter for property Nomdesrel.
     * @return Value of property Nomdesrel.
     */
    public java.lang.String getNomdesrel() {
        return Nomdesrel;
    }
    
    /**
     * Setter for property Nomdesrel.
     * @param Nomdesrel New value of property Nomdesrel.
     */
    public void setNomdesrel(java.lang.String Nomdesrel) {
        this.Nomdesrel = Nomdesrel;
    }
    
    /**
     * Getter for property Nomagc.
     * @return Value of property Nomagc.
     */
    public java.lang.String getNomagc() {
        return Nomagc;
    }
    
    /**
     * Setter for property Nomagc.
     * @param Nomagc New value of property Nomagc.
     */
    public void setNomagc(java.lang.String Nomagc) {
        this.Nomagc = Nomagc;
    }
    
    /**
     * Getter for property porcentaje.
     * @return Value of property porcentaje.
     */
    public double getPorcentaje() {
        return porcentaje;
    }
    
    /**
     * Setter for property porcentaje.
     * @param porcentaje New value of property porcentaje.
     */
    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }
    
    /**
     * Getter for property vlrParcial.
     * @return Value of property vlrParcial.
     */
    public double getVlrParcial() {
        return vlrParcial;
    }
    
    /**
     * Setter for property vlrParcial.
     * @param vlrParcial New value of property vlrParcial.
     */
    public void setVlrParcial(double vlrParcial) {
        this.vlrParcial = vlrParcial;
    }
    
    /**
     * Getter for property porcentajeProrrateo.
     * @return Value of property porcentajeProrrateo.
     */
    public double getPorcentajeProrrateo() {
        return porcentajeProrrateo;
    }
    
    /**
     * Setter for property porcentajeProrrateo.
     * @param porcentajeProrrateo New value of property porcentajeProrrateo.
     */
    public void setPorcentajeProrrateo(double porcentajeProrrateo) {
        this.porcentajeProrrateo = porcentajeProrrateo;
    }
    
    /**
     * Getter for property vlrProrrateado.
     * @return Value of property vlrProrrateado.
     */
    public double getVlrProrrateado() {
        return vlrProrrateado;
    }
    
    /**
     * Setter for property vlrProrrateado.
     * @param vlrProrrateado New value of property vlrProrrateado.
     */
    public void setVlrProrrateado(double vlrProrrateado) {
        this.vlrProrrateado = vlrProrrateado;
    }
    
    /**
     * Getter for property vlrPlanilla.
     * @return Value of property vlrPlanilla.
     */
    public double getVlrPlanilla() {
        return vlrPlanilla;
    }
    
    /**
     * Setter for property vlrPlanilla.
     * @param vlrPlanilla New value of property vlrPlanilla.
     */
    public void setVlrPlanilla(double vlrPlanilla) {
        this.vlrPlanilla = vlrPlanilla;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
    
    public static Planilla load(ResultSet rs)throws SQLException{
        
        Planilla planilla = new Planilla();
        
        planilla.setNumpla(rs.getString("numpla"));
        planilla.setFecpla(rs.getTimestamp("fecpla"));
        planilla.setAgcpla(rs.getString("agcpla"));
        planilla.setOripla(rs.getString("oripla"));
        planilla.setDespla(rs.getString("Despla"));
        planilla.setPlaveh(rs.getString("plaveh"));
        planilla.setCedcon(rs.getString("Cedcon"));
        planilla.setNitpro(rs.getString("nitpro"));
        planilla.setPlatlr(rs.getString("Platlr"));
        planilla.setRuta_pla(rs.getString("ruta_pla"));
        planilla.setOrden_carga(rs.getString("orden_carga"));
        planilla.setFecdsp(rs.getString("Fecdsp"));
        
        planilla.setCia(rs.getString("cia"));
        planilla.setTipoviaje(rs.getString("Tipoviaje"));
        //planilla.setFechaposllegada(rs.getTimestamp("Fechaposllegada"));
        planilla.setPesoreal(rs.getFloat("Pesoreal"));
        planilla.setVlrpla(rs.getFloat("Vlrpla"));
        planilla.setVlrplafor(rs.getFloat("Vlrpla2"));
        planilla.setMoneda(rs.getString("unidcam"));
        planilla.setDespachador(rs.getString("despachador"));
      //  planilla.setCreation_date(rs.getTimestamp("creation_date"));
       // planilla.setPrinter_date(rs.getTimestamp("printer_date"));
        planilla.setPrecinto(rs.getString("precinto"));
        planilla.setFeccum(rs.getString("feccum"));
        return planilla;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    public void setFeccum(String feccum){
        
        this.feccum=feccum;
    }
    
    public String getFeccum(){
        
        return feccum;
    }
    
    public void setUnit_cost(float unit_cost){
        
        this.unit_cost=unit_cost;
    }
    
    public float getUnit_cost(){
        
        return unit_cost;
    }
    
    public void setUnit_transp(String unit_transp){
        
        this.unit_trasn=unit_transp;
    }
    
    public String getUnit_transp(){
        
        return unit_trasn;
    }
    public void setGroup_code(String group_code){
        
        this.group_code=group_code;
    }
    
    public String getGroup_code(){
        
        return group_code;
    }
    
    
    public void setSaldo(float saldo){
        
        this.saldo=saldo;
    }
    
    public float getSaldo(){
        
        return saldo;
    }
    public void setPrecinto(String precinto){
        
        this.precinto=precinto;
    }
    
    public String getPrecinto(){
        
        return precinto;
    }
    public void setCodruta(String codruta){
        
        this.codruta=codruta;
    }
    
    public String getCodruta(){
        
        return codruta;
    }
    public void setNomdest(String nomdest){
        
        this.nomdest=nomdest;
    }
    
    public String getNomdest(){
        
        return nomdest;
    }
    
    public void setNomori(String nomori){
        
        this.nomori=nomori;
    }
    
    public String getNomori(){
        
        return nomori;
    }
    public void setSj(String sj){
        
        this.sj=sj;
    }
    
    public String getSj(){
        
        return sj;
    }
    public void setSj_desc(String sj_desc){
        
        this.sj_desc=sj_desc;
    }
    
    public String getSj_desc(){
        
        return sj_desc;
    }
    
    public void setNomCond(String nombre){
        
        this.nombrecond=nombre;
    }
    
    public String getNomCond(){
        
        return nombrecond;
    }
    
    public void setNumrem(String numrem){
        
        this.numrem=numrem;
    }
    
    public String getNumrem(){
        
        return numrem;
    }
    public void setNumpla(String numpla){
        
        this.numpla=numpla;
    }
    
    public String getNumpla(){
        
        return numpla;
    }
    
    public void setFecpla(java.util.Date fecpla){
        
        this.fecpla=fecpla;
    }
    
    public java.util.Date getFecpla(){
        
        return fecpla;
    }
    public void setPrinter_date(java.util.Date printer_date){
        
        this.printer_date=printer_date;
    }
    
    public void setPrinter_date(String date){
        
        this.printer_date2=date;
    }
    
    public java.util.Date getPrinter_date(){
        
        return printer_date;
    }
    
    public String getPrinter_date2(){
        
        return printer_date2;
    }
    
    public void setAgcpla(String agcpla){
        
        this.agcpla=agcpla;
    }
    
    public String getAgcpla(){
        
        return agcpla;
    }
    
    public void setOripla(String oripla){
        
        this.oripla=oripla;
    }
    
    public String getOripla(){
        
        return oripla;
    }
    
    public void setDespla(String despla){
        
        this.despla=despla;
    }
    public void setDesplaDate(java.util.Date despla){
        
        this.fechadesp=fechadesp;
    }
    
    public String getDespla(){
        
        return despla;
    }
    
    public void setPlaveh(String plaveh){
        
        this.plaveh=plaveh;
    }
    
    public String getPlaveh(){
        
        return plaveh;
    }
    
    public void setCedcon(String cedcon){
        
        this.cedcon=cedcon;
    }
    
    public String getCedcon(){
        
        return cedcon;
    }
    
    public void setNitpro(String nitpro){
        
        this.nitpro=nitpro;
    }
    
    public String getNitpro(){
        
        return nitpro;
    }
    
    public void setPlatlr(String platlr){
        
        this.platlr=platlr;
    }
    
    public String getPlatlr(){
        
        return platlr;
    }
    
    public void setRuta_pla(String ruta_pla){
        
        this.ruta_pla=ruta_pla;
    }
    
    public String getRuta_pla(){
        
        return ruta_pla;
    }
    
    public void setOrden_carga(String orden_carga){
        
        this.orden_carga=orden_carga;
    }
    
    public String getOrden_carga(){
        
        return orden_carga;
    }
    
    public void setFecdsp(String fecdsp){
        
        this.fecdsp=fecdsp;
    }
    
    public String getFecdsp(){
        
        return fecdsp;
    }
    
    public java.util.Date getFecdspDate(){
        
        return fechadesp;
    }
    
    public void setCia(String cia){
        
        this.cia=cia;
    }
    
    public String getCia(){
        
        return cia;
    }
    
    public void setTipoviaje(String tipoviaje){
        
        this.tipoviaje=tipoviaje;
    }
    
    public String getTipoviaje(){
        
        return tipoviaje;
    }
    
    public void setFechaposllegada(java.util.Date fechaposllegada){
        
        this.fechaposllegada=fechaposllegada;
    }
    
    public java.util.Date getFechaposllegada(){
        
        return fechaposllegada;
    }
    
    public void setPesoreal(float pesoreal){
        
        this.pesoreal=pesoreal;
    }
    
    public float getPesoreal(){
        
        return pesoreal;
    }
    
    public void setVlrpla(float vlrpla){
        
        this.vlrpla=vlrpla;
    }
    
    public float getVlrpla(){
        
        return vlrpla;
    }
    
    public void setVlrplafor(float vlrplafor){
        
        this.vlrplafor=vlrplafor;
    }
    
    public float getVlrplafor(){
        
        return vlrplafor;
    }
    
    public void setMoneda(String moneda){
        
        this.moneda=moneda;
    }
    
    public String getMoneda(){
        
        return moneda;
    }
    
    public void setDespachador(String despachador){
        
        this.despachador=despachador;
    }
    
    public String getDespachador(){
        
        return despachador;
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    /**
     * Getter for property traffic_code.
     * @return Value of property traffic_code.
     */
    public java.lang.String getTraffic_code() {
        return traffic_code;
    }
    
    /**
     * Setter for property traffic_code.
     * @param traffic_code New value of property traffic_code.
     */
    public void setTraffic_code(java.lang.String traffic_code) {
        this.traffic_code = traffic_code;
    }
    
    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
    /**
     * Getter for property contenedores.
     * @return Value of property contenedores.
     */
    public java.lang.String getContenedores() {
        return contenedores;
    }
    
    /**
     * Setter for property contenedores.
     * @param contenedores New value of property contenedores.
     */
    public void setContenedores(java.lang.String contenedores) {
        this.contenedores = contenedores;
    }
    
    /**
     * Getter for property tipocont.
     * @return Value of property tipocont.
     */
    public java.lang.String getTipocont() {
        return tipocont;
    }    
    
    /**
     * Setter for property tipocont.
     * @param tipocont New value of property tipocont.
     */
    public void setTipocont(java.lang.String tipocont) {
        this.tipocont = tipocont;
    }    
    
    /**
     * Getter for property pesoM.
     * @return Value of property pesoM.
     */
    public java.lang.String getPesoM() {
        return pesoM;
    }    
    
    /**
     * Setter for property pesoM.
     * @param pesoM New value of property pesoM.
     */
    public void setPesoM(java.lang.String pesoM) {
        this.pesoM = pesoM;
    }    
    
    /**
     * Getter for property pesoP.
     * @return Value of property pesoP.
     */
    public java.lang.String getPesoP() {
        return pesoP;
    }    
   
    /**
     * Setter for property pesoP.
     * @param pesoP New value of property pesoP.
     */
    public void setPesoP(java.lang.String pesoP) {
        this.pesoP = pesoP;
    }
    
    /**
     * Getter for property nomprop.
     * @return Value of property nomprop.
     */
    public java.lang.String getNomprop() {
        return nomprop;
    }
    
    /**
     * Setter for property nomprop.
     * @param nomprop New value of property nomprop.
     */
    public void setNomprop(java.lang.String nomprop) {
        this.nomprop = nomprop;
    }
    
    
    public float getRetpla() {
        return retpla;
    }

    

    public String getUnit_vlr() {
        return unit_vlr;
    }

    
    public String getOrinom() {
        return orinom;
    }

    public String getDesnom() {
        return desnom;
    }

    
    public String getUnidcam() {
        return unidcam;
    }

    
    public String getReg_status() {
        return reg_status;
    }

    public String getObservacion() {
        return observacion;
    }


    public String getNomcon() {
        return nomcon;
    }


    public String getCelularcon() {
        return celularcon;
    }

    public String getTienedevol() {
        return tienedevol;
    }

    public float getSegpla() {
        return segpla;
    }


    public String getLast_update() {
        return last_update;
    }

    public String getBase() {
        return base;
    }

    public float getVlrpla2() {
        return vlrpla2;
    }


    public String getCurrency() {
        return currency;
    }

    public String getUltimoreporte() {
        return ultimoreporte;
    }

    public float getTiempoentransito() {
        return tiempoentransito;
    }



    public String getStatus_220() {
        return status_220;
    }


    public String getCorte() {
        return corte;
    }

    public void setRetpla( float retpla ) {
        this.retpla = retpla;
    }


    public void setUnit_vlr( String unit_vlr ) {
        this.unit_vlr = unit_vlr;
    }

    public void setOrinom( String orinom ) {
        this.orinom = orinom;
    }

    public void setUnidcam( String unidcam ) {
        this.unidcam = unidcam;
    }


    public void setReg_status( String reg_status ) {
        this.reg_status = reg_status;
    }


    public void setObservacion( String observacion ) {
        this.observacion = observacion;
    }


    public void setNomcon( String nomcon ) {
        this.nomcon = nomcon;
    }


    public void setCelularcon( String celularcon ) {
        this.celularcon = celularcon;
    }

    public void setSegpla( float segpla ) {
        this.segpla = segpla;
    }


    public void setLast_update( String last_update ) {
        this.last_update = last_update;
    }

    public void setBase( String base ) {
        this.base = base;
    }

    public void setVlrpla2( float vlrpla2 ) {
        this.vlrpla2 = vlrpla2;
    }


    public void setCurrency( String currency ) {
        this.currency = currency;
    }

    public void setUltimoreporte( String ultimoreporte ) {
        this.ultimoreporte = ultimoreporte;
    }

    public void setTiempoentransito( float tiempoentransito ) {
        this.tiempoentransito = tiempoentransito;
    }



    public void setStatus_220( String status_220 ) {
        this.status_220 = status_220;
    }



    public void setCorte( String corte ) {
        this.corte = corte;
   }
    /**
     * Getter for property destinopla.
     * @return Value of property destinopla.
     */
    public java.lang.String getDestinopla() {
        return destinopla;
    }
    
    /**
     * Setter for property destinopla.
     * @param destinopla New value of property destinopla.
     */
    public void setDestinopla(java.lang.String destinopla) {
        this.destinopla = destinopla;
    }
    
    /**
     * Getter for property fechapla.
     * @return Value of property fechapla.
     */
    public java.util.Date getFechapla() {
        return fechapla;
    }
    
    /**
     * Setter for property fechapla.
     * @param fechapla New value of property fechapla.
     */
    public void setFechapla(java.util.Date fechapla) {
        this.fechapla = fechapla;
    }
    
    
   
    
    /**
     * Getter for property origenpla.
     * @return Value of property origenpla.
     */
    public java.lang.String getOrigenpla() {
        return origenpla;
    }
    
    /**
     * Setter for property origenpla.
     * @param origenpla New value of property origenpla.
     */
    public void setOrigenpla(java.lang.String origenpla) {
        this.origenpla = origenpla;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property platr.
     * @return Value of property platr.
     */
    public java.lang.String getPlatr() {
        return platr;
    }
    
    /**
     * Setter for property platr.
     * @param platr New value of property platr.
     */
    public void setPlatr(java.lang.String platr) {
        this.platr = platr;
    }
    
    /**
     * Setter for property fechaposllegada.
     * @param fechaposllegada New value of property fechaposllegada.
     */
    public void setFechaposllegada(java.sql.Timestamp fechaposllegada) {
        this.fechaposllegada = fechaposllegada;
    }
    
    /**
     * Getter for property fechadespacho.
     * @return Value of property fechadespacho.
     */
    public java.sql.Timestamp getFechadespacho() {
        return fechadespacho;
    }
    
    /**
     * Setter for property fechadespacho.
     * @param fechadespacho New value of property fechadespacho.
     */
    public void setFechadespacho(java.sql.Timestamp fechadespacho) {
        this.fechadespacho = fechadespacho;
    }
    
    public boolean equals(Object otro){
        if ( otro instanceof Planilla){
            Planilla p = (Planilla) otro;
            return p.plaveh.equals(this.plaveh);
        }
        return false;
    }
    
    public int compareTo(Object otro) {
        if ( otro instanceof Planilla ){
            Planilla p = (Planilla) otro;
            return this.fechapla.compareTo(p.fechapla);
        }
        throw new RuntimeException("El objeto "+otro+" no es comparable");
    }

    /**
     * Getter for property fechaPLlegada.
     * @return Value of property fechaPLlegada.
     */
    public java.sql.Timestamp getFechaPLlegada() {
        return fechaPLlegada;
    }
    
    /**
     * Setter for property fechaPLlegada.
     * @param fechaPLlegada New value of property fechaPLlegada.
     */
    public void setFechaPLlegada(java.sql.Timestamp fechaPLlegada) {
        this.fechaPLlegada = fechaPLlegada;
    }
    
    /**
     * Getter for property tipotrailer.
     * @return Value of property tipotrailer.
     */
    public java.lang.String getTipotrailer() {
        return tipotrailer;
    }
    
    /**
     * Setter for property tipotrailer.
     * @param tipotrailer New value of property tipotrailer.
     */
    public void setTipotrailer(java.lang.String tipotrailer) {
        this.tipotrailer = tipotrailer;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property totalajustes.
     * @return Value of property totalajustes.
     */
    public float getTotalajustes() {
        return totalajustes;
    }
    
    /**
     * Setter for property totalajustes.
     * @param totalajustes New value of property totalajustes.
     */
    public void setTotalajustes(float totalajustes) {
        this.totalajustes = totalajustes;
    }
    
    /**
     * Getter for property totalanticipo.
     * @return Value of property totalanticipo.
     */
    public float getTotalanticipo() {
        return totalanticipo;
    }
    
    /**
     * Setter for property totalanticipo.
     * @param totalanticipo New value of property totalanticipo.
     */
    public void setTotalanticipo(float totalanticipo) {
        this.totalanticipo = totalanticipo;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }
    
    /**
     * Getter for property nomzona.
     * @return Value of property nomzona.
     */
    public java.lang.String getNomzona() {
        return nomzona;
    }
    
    /**
     * Setter for property nomzona.
     * @param nomzona New value of property nomzona.
     */
    public void setNomzona(java.lang.String nomzona) {
        this.nomzona = nomzona;
    }
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa() {
        return causa;
    }
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.lang.String causa) {
        this.causa = causa;
    }
    
    /**
     * Getter for property t_recuperacion.
     * @return Value of property t_recuperacion.
     */
    public java.lang.String getT_recuperacion() {
        return t_recuperacion;
    }
    
    /**
     * Setter for property t_recuperacion.
     * @param t_recuperacion New value of property t_recuperacion.
     */
    public void setT_recuperacion(java.lang.String t_recuperacion) {
        this.t_recuperacion = t_recuperacion;
    }
    
    /**
     * Getter for property cf_code.
     * @return Value of property cf_code.
     */
    public java.lang.String getCf_code() {
        return cf_code;
    }
    
    /**
     * Setter for property cf_code.
     * @param cf_code New value of property cf_code.
     */
    public void setCf_code(java.lang.String cf_code) {
        this.cf_code = cf_code;
    }
    
    /**
     * Getter for property std_job_rec.
     * @return Value of property std_job_rec.
     */
    public java.lang.String getStd_job_rec() {
        return std_job_rec;
    }    
   
    /**
     * Setter for property std_job_rec.
     * @param std_job_rec New value of property std_job_rec.
     */
    public void setStd_job_rec(java.lang.String std_job_rec) {
        this.std_job_rec = std_job_rec;
    }    
    
    /**
     * Getter for property std_job_no_rec.
     * @return Value of property std_job_no_rec.
     */
    public java.lang.String getStd_job_no_rec() {
        return std_job_no_rec;
    }
    
    /**
     * Setter for property std_job_no_rec.
     * @param std_job_no_rec New value of property std_job_no_rec.
     */
    public void setStd_job_no_rec(java.lang.String std_job_no_rec) {
        this.std_job_no_rec = std_job_no_rec;
    }
    
    /**
     * Getter for property dias.
     * @return Value of property dias.
     */
    public int getDias() {
        return dias;
    }
    
    /**
     * Setter for property dias.
     * @param dias New value of property dias.
     */
    public void setDias(int dias) {
        this.dias = dias;
    }
    
    /**
     * Getter for property document.
     * @return Value of property document.
     */
    public java.lang.String getDocument() {
        return document;
    }
    
    /**
     * Setter for property document.
     * @param document New value of property document.
     */
    public void setDocument(java.lang.String document) {
        this.document = document;
    }
    
    /**
     * Getter for property documentos.
     * @return Value of property documentos.
     */
    public java.lang.String getDocumentos() {
        return documentos;
    }
    
    /**
     * Setter for property documentos.
     * @param documentos New value of property documentos.
     */
    public void setDocumentos(java.lang.String documentos) {
        this.documentos = documentos;
    }
    
    //Jose 15.12.05
    /**
     * Getter for property fecha_placa.
     * @return Value of property fecha_placa.
     */
    public java.lang.String getFecha_placa () {
        return fecha_placa;
    }
    
    /**
     * Setter for property fecha_placa.
     * @param fecha_placa New value of property fecha_placa.
     */
    public void setFecha_placa (java.lang.String fecha_placa) {
        this.fecha_placa = fecha_placa;
    }

    /**
     * Getter for property fecha_conductor.
     * @return Value of property fecha_conductor.
     */
    public java.lang.String getFecha_conductor () {
        return fecha_conductor;
    }
    
    /**
     * Setter for property fecha_conductor.
     * @param fecha_conductor New value of property fecha_conductor.
     */
    public void setFecha_conductor (java.lang.String fecha_conductor) {
        this.fecha_conductor = fecha_conductor;
    }
    
    /**
     * Getter for property fec_prox_pto_control.
     * @return Value of property fec_prox_pto_control.
     */
    public java.lang.String getFec_prox_pto_control() {
        return fec_prox_pto_control;
    }
    
    /**
     * Setter for property fec_prox_pto_control.
     * @param fec_prox_pto_control New value of property fec_prox_pto_control.
     */
    public void setFec_prox_pto_control(java.lang.String fec_prox_pto_control) {
        this.fec_prox_pto_control = fec_prox_pto_control;
    }
    
    /**
     * Getter for property prox_pto_control.
     * @return Value of property prox_pto_control.
     */
    public java.lang.String getProx_pto_control() {
        return prox_pto_control;
    }
    
    /**
     * Setter for property prox_pto_control.
     * @param prox_pto_control New value of property prox_pto_control.
     */
    public void setProx_pto_control(java.lang.String prox_pto_control) {
        this.prox_pto_control = prox_pto_control;
    }
    
    /**
     * Getter for property nom_prox_pto_control.
     * @return Value of property nom_prox_pto_control.
     */
    public java.lang.String getNom_prox_pto_control() {
        return nom_prox_pto_control;
    }
    
    /**
     * Setter for property nom_prox_pto_control.
     * @param nom_prox_pto_control New value of property nom_prox_pto_control.
     */
    public void setNom_prox_pto_control(java.lang.String nom_prox_pto_control) {
        this.nom_prox_pto_control = nom_prox_pto_control;
    }
    
    /**
     * Getter for property clientes.
     * @return Value of property clientes.
     */
    public java.lang.String getClientes() {
        return clientes;
    }
    
    /**
     * Setter for property clientes.
     * @param clientes New value of property clientes.
     */
    public void setClientes(java.lang.String clientes) {
        this.clientes = clientes;
    }    
   
    /**
     * Getter for property fecha_salida.
     * @return Value of property fecha_salida.
     */
    public java.lang.String getFecha_salida() {
        return fecha_salida;
    }    
    
    /**
     * Setter for property fecha_salida.
     * @param fecha_salida New value of property fecha_salida.
     */
    public void setFecha_salida(java.lang.String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }
    
    /**
     * Getter for property inventario.
     * @return Value of property inventario.
     */
    public java.lang.String getInventario() {
        return inventario;
    }
    
    /**
     * Setter for property inventario.
     * @param inventario New value of property inventario.
     */
    public void setInventario(java.lang.String inventario) {
        this.inventario = inventario;
    }
    
    /**
     * Getter for property sticker.
     * @return Value of property sticker.
     */
    public java.lang.String getSticker() {
        return sticker;
    }
    
    /**
     * Setter for property sticker.
     * @param sticker New value of property sticker.
     */
    public void setSticker(java.lang.String sticker) {
        this.sticker = sticker;
    }
    
    /**
     * Getter for property maxReanticipo.
     * @return Value of property maxReanticipo.
     */
    public double getMaxReanticipo() {
        return maxReanticipo;
    }
    
    /**
     * Setter for property maxReanticipo.
     * @param maxReanticipo New value of property maxReanticipo.
     */
    public void setMaxReanticipo(double maxReanticipo) {
        this.maxReanticipo = maxReanticipo;
    }
    
    /**
     * Getter for property nomcliente.
     * @return Value of property nomcliente.
     */
    public java.lang.String getNomcliente() {
        return nomcliente;
    }
    
    /**
     * Setter for property nomcliente.
     * @param nomcliente New value of property nomcliente.
     */
    public void setNomcliente(java.lang.String nomcliente) {
        this.nomcliente = nomcliente;
    }
    
    /**
     * Getter for property tiene_doc.
     * @return Value of property tiene_doc.
     */
    public java.lang.String getTiene_doc() {
        return tiene_doc;
    }
    
    /**
     * Setter for property tiene_doc.
     * @param tiene_doc New value of property tiene_doc.
     */
    public void setTiene_doc(java.lang.String tiene_doc) {
        this.tiene_doc = tiene_doc;
    }
    
    /**
     * Getter for property tipoCarga.
     * @return Value of property tipoCarga.
     */
    public java.lang.String getTipoCarga() {
        return tipoCarga;
    }
    
    /**
     * Setter for property tipoCarga.
     * @param tipoCarga New value of property tipoCarga.
     */
    public void setTipoCarga(java.lang.String tipoCarga) {
        this.tipoCarga = tipoCarga;
    }
    
    /**
     * Getter for property account_code_i.
     * @return Value of property account_code_i.
     */
    public java.lang.String getAccount_code_i() {
        return account_code_i;
    }
    
    /**
     * Setter for property account_code_i.
     * @param account_code_i New value of property account_code_i.
     */
    public void setAccount_code_i(java.lang.String account_code_i) {
        this.account_code_i = account_code_i;
    }
    
    /**
     * Getter for property account_code_c.
     * @return Value of property account_code_c.
     */
    public java.lang.String getAccount_code_c() {
        return account_code_c;
    }
    
    /**
     * Setter for property account_code_c.
     * @param account_code_c New value of property account_code_c.
     */
    public void setAccount_code_c(java.lang.String account_code_c) {
        this.account_code_c = account_code_c;
    }
    
    /**
     * Getter for property unidad_i.
     * @return Value of property unidad_i.
     */
    public java.lang.String getUnidad_i() {
        return unidad_i;
    }
    
    /**
     * Setter for property unidad_i.
     * @param unidad_i New value of property unidad_i.
     */
    public void setUnidad_i(java.lang.String unidad_i) {
        this.unidad_i = unidad_i;
    }
    
    /**
     * Getter for property unidad_c.
     * @return Value of property unidad_c.
     */
    public java.lang.String getUnidad_c() {
        return unidad_c;
    }
    
    /**
     * Setter for property unidad_c.
     * @param unidad_c New value of property unidad_c.
     */
    public void setUnidad_c(java.lang.String unidad_c) {
        this.unidad_c = unidad_c;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property fecplanilla.
     * @return Value of property fecplanilla.
     */
    public java.lang.String getFecplanilla() {
        return fecplanilla;
    }
    
    /**
     * Setter for property fecplanilla.
     * @param fecplanilla New value of property fecplanilla.
     */
    public void setFecplanilla(java.lang.String fecplanilla) {
        this.fecplanilla = fecplanilla;
    }
    
    /**
     * Getter for property mostrarDetalles.
     * @return Value of property mostrarDetalles.
     */
    public boolean isMostrarDetalles() {
        return mostrarDetalles;
    }    
   
    /**
     * Setter for property mostrarDetalles.
     * @param mostrarDetalles New value of property mostrarDetalles.
     */
    public void setMostrarDetalles(boolean mostrarDetalles) {
        this.mostrarDetalles = mostrarDetalles;
    }    
    
    /**
     * Getter for property eintermedias.
     * @return Value of property eintermedias.
     */
    public java.lang.String getEintermedias() {
        return eintermedias;
    }
    
    /**
     * Setter for property eintermedias.
     * @param eintermedias New value of property eintermedias.
     */
    public void setEintermedias(java.lang.String eintermedias) {
        this.eintermedias = eintermedias;
    }
    
    /**
     * Getter for property neintermedias.
     * @return Value of property neintermedias.
     */
    public java.lang.String getNeintermedias() {
        return neintermedias;
    }
    
    /**
     * Setter for property neintermedias.
     * @param neintermedias New value of property neintermedias.
     */
    public void setNeintermedias(java.lang.String neintermedias) {
        this.neintermedias = neintermedias;
    }
    
/**fin*/
    
}










