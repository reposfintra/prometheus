package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Proveedor_Acpm implements Serializable {
    
    private String dstrct;
    private String city_code;
    private String nit;
    private java.util.Date creation_date;
    private String creation_user;
    private String nombre;
    private float valor;
    private float maximo_e;
    private String moneda;
    private String codigo;
    private String tipo;
    private String codigo_migracion;
    private float porcentaje;
    
    
    public static Proveedor_Acpm load(ResultSet rs)throws SQLException{
        
        Proveedor_Acpm proveedor_acpm = new Proveedor_Acpm();
        proveedor_acpm.setDstrct(rs.getString("dstrct"));
        proveedor_acpm.setCity_code(rs.getString("city_code"));
        proveedor_acpm.setNit(rs.getString("nit"));
        proveedor_acpm.setNombre(rs.getString("nombre"));
        proveedor_acpm.setCodigo(rs.getString("codigo_proveedor"));
        proveedor_acpm.setValor(rs.getFloat("acpm_value"));
        proveedor_acpm.setTipo(rs.getString("tipo_servicio"));
        proveedor_acpm.setMax_e(rs.getFloat("max_efectivo"));
        proveedor_acpm.setMoneda(rs.getString("moneda"));
        proveedor_acpm.setPorcentaje(rs.getFloat("porcentaje"));
        return proveedor_acpm;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    public void setPorcentaje(float porcentaje){
        
        this.porcentaje=porcentaje;
    }
    
    public float getPorcentaje(){
        
        return porcentaje;
    }
    public void setCod_Migracion(String cod){
        
        this.codigo_migracion=cod;
    }
    
    public String getCod_Migracion(){
        
        return codigo_migracion;
    }
    
    public void setValor(float valor){
        
        this.valor=valor;
    }
    
    public float getValor(){
        
        return valor;
    }
    
    public void setMax_e(float max_e){
        
        this.maximo_e=max_e;
    }
    
    public float getMax_e(){
        
        return maximo_e;
    }
    public void setMoneda(String moneda){
        
        this.moneda=moneda;
    }
    
    public String getMoneda(){
        
        return moneda;
    }
    public void setTipo(String tipo){
        
        this.tipo=tipo;
    }
    
    public String getTipo(){
        
        return tipo;
    }
    
    public void setCodigo(String codigo){
        
        this.codigo=codigo;
    }
    
    public String getCodigo(){
        
        return codigo;
    }
    
    public void setNombre(String nombre){
        
        this.nombre=nombre;
    }
    
    public String getNombre(){
        
        return nombre;
    }
    
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    
    public void setCity_code(String city_code){
        
        this.city_code=city_code;
    }
    
    public String getCity_code(){
        
        return city_code;
    }
    
    public void setNit(String nit){
        
        this.nit=nit;
    }
    
    public String getNit(){
        
        return nit;
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
}