/********************************************************************
 *  Nombre Clase.................   Precinto.java
 *  Descripci�n..................   Bean de la tabla precintos
 *  Autor........................   Ing. Tito Andr�s Maturana
 *  Fecha........................   22.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;

public class Precinto {
    private String agencia;
    private String precinto;
    private String numpla;
    private String fec_utilizacion;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String reg_status;
    private String dstrct;
    private String base;
    private String tipoDocumento;
    private String serie;
    private String seriei;
    private String serief;
    
    /** Creates a new instance of Precinto */
    public Precinto() {
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fec_utilizacion.
     * @return Value of property fec_utilizacion.
     */
    public java.lang.String getFec_utilizacion() {
        return fec_utilizacion;
    }
    
    /**
     * Setter for property fec_utilizacion.
     * @param fec_utilizacion New value of property fec_utilizacion.
     */
    public void setFec_utilizacion(java.lang.String fec_utilizacion) {
        this.fec_utilizacion = fec_utilizacion;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property precinto.
     * @return Value of property precinto.
     */
    public java.lang.String getPrecinto() {
        return precinto;
    }
    
    /**
     * Setter for property precinto.
     * @param precinto New value of property precinto.
     */
    public void setPrecinto(java.lang.String precinto) {
        this.precinto = precinto;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Obtiene el valor de cada uno de los campos del <code>ResultSet</code>
     * @autor Ing. Tito Andr�s Maturana
     * @param rs <code>ResultSet</code> de donde se obtienen los campos
     * @throws SQLException
     * @version 1.0
     */
    public void Load(ResultSet rs) throws SQLException{
        this.agencia = rs.getString("agencia");
        this.base = rs.getString("base");
        this.creation_date = rs.getString("creation_date");
        this.creation_user = rs.getString("creation_user");
        this.dstrct = rs.getString("dstrct");
        this.fec_utilizacion = rs.getString("fec_utilizacion");
        this.last_update = rs.getString("last_update");
        this.numpla = rs.getString("numpla");
        this.precinto = rs.getString("precinto");
        this.reg_status = rs.getString("reg_status");
        this.user_update = rs.getString("user_update");
    }
    
    /**
     * Getter for property tipoDocumento.
     * @return Value of property tipoDocumento.
     */
    public java.lang.String getTipoDocumento() {
        return tipoDocumento;
    }
    
    /**
     * Setter for property tipoDocumento.
     * @param tipoDocumento New value of property tipoDocumento.
     */
    public void setTipo_Documento(java.lang.String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
    
    /**
     * Getter for property serie.
     * @return Value of property serie.
     */
    public java.lang.String getSerie() {
        return serie;
    }
    
    /**
     * Setter for property serie.
     * @param serie New value of property serie.
     */
    public void setSerie(java.lang.String serie) {
        this.serie = serie;
    }
    
    /**
     * Getter for property seriei.
     * @return Value of property seriei.
     */
    public java.lang.String getSeriei() {
        return seriei;
    }
    
    /**
     * Setter for property seriei.
     * @param seriei New value of property seriei.
     */
    public void setSeriei(java.lang.String seriei) {
        this.seriei = seriei;
    }
    
    /**
     * Getter for property serief.
     * @return Value of property serief.
     */
    public java.lang.String getSerief() {
        return serief;
    }
    
    /**
     * Setter for property serief.
     * @param serief New value of property serief.
     */
    public void setSerief(java.lang.String serief) {
        this.serief = serief;
    }
    
}
