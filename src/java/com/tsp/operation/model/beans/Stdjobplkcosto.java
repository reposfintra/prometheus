package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Stdjobplkcosto implements Serializable {
	
	private String dstrct;
	private String group_code;
	private String activation_date;
	private String ind_trip;
	private float costo_unitario;
	private float unit_cost;
	private String unit_transp;
	private String currency;
	private java.util.Date creation_date;
	private String creation_user;
	
	public static Stdjobplkcosto load(ResultSet rs)throws SQLException{
		
		Stdjobplkcosto stdjobplkcosto = new Stdjobplkcosto();
		
		stdjobplkcosto.setDstrct(rs.getString(2));
		stdjobplkcosto.setGroup_code(rs.getString(3));
		stdjobplkcosto.setActivation_date(rs.getString(4));
		stdjobplkcosto.setInd_trip(rs.getString(5));
		stdjobplkcosto.setCosto_unitario(rs.getFloat(6));
		stdjobplkcosto.setUnit_cost(rs.getFloat(7));
		stdjobplkcosto.setUnit_transp(rs.getString(8));
		stdjobplkcosto.setCurrency(rs.getString(9));
		stdjobplkcosto.setCreation_date(rs.getTimestamp(12));
		stdjobplkcosto.setCreation_user(rs.getString(13));
		
		return stdjobplkcosto;
	}
	
	//============================================
	//		Metodos de acceso a propiedades
	//============================================
	
	
	public void setDstrct(String dstrct){
			
		this.dstrct=dstrct;
	}
	
	public String getDstrct(){
	
		return dstrct;
	}
	
	public void setGroup_code(String group_code){
		
		this.group_code=group_code;
	}
	
	public String getGroup_code(){
		
		return group_code;
	}
	
	public void setActivation_date(String activation_date){
		
		this.activation_date=activation_date;
	}
	
	public String getActivation_date(){
		
		return activation_date;
	}
	
	public void setInd_trip(String ind_trip){
		
		this.ind_trip=ind_trip;
	}
	
	public String getInd_trip(){
		
		return ind_trip;
	}
	
	public void setCosto_unitario(float costo_unitario){
		
		this.costo_unitario=costo_unitario;
	}
	
	public float getCosto_unitario(){
		
		return costo_unitario;
	}
	
	public void setUnit_cost(float unit_cost){
		
		this.unit_cost=unit_cost;
	}
	
	public float getUnit_cost(){
		
		return unit_cost;
	}
	
	public void setUnit_transp(String unit_transp){
		
		this.unit_transp=unit_transp;
	}
	
	public String getUnit_transp(){
		
		return unit_transp;
	}
	
	public void setCurrency(String currency){
		
		this.currency=currency;
	}
	
	public String getCurrency(){
		
		return currency;
	}
	
	public void setCreation_date(java.util.Date creation_date){
		
		this.creation_date=creation_date;
	}
	
	public java.util.Date getCreation_date(){
		
		return creation_date;
	}
	
	public void setCreation_user(String creation_user){
	
		this.creation_user=creation_user;
	}
	
	public String getCreation_user(){
		
		return creation_user;
	}
}

	




