package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudLaboral<br/> 1/03/2011
 *
 * @author ivargas-Geotech
 */
public class SolicitudLaboral {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String tipo;
    private String fax;
    private String direccion;
    private String ciudad;
    private String departamento;
    private String telefono;
    private String razonSocial;
    private String telefono2;
    private String ocupacion;
    private String actividadEconomica;
    private String nombreEmpresa;
    private String nit;
    private String extension;
    private String cargo;
    private String fechaIngreso;
    private String tipoContrato;
    private String salario;
    private String otrosIngresos;
    private String conceptoOtrosIng;
    private String gastosManutencion;
    private String gastosCreditos;
    private String gastosArriendo;
    private String direccionCobro;
    private String eps;
    private String tipoAfiliacion;
    private String celular;
    private String email;
    private String id_persona = "";

    public SolicitudLaboral load(ResultSet rs) throws SQLException {
        SolicitudLaboral laboral = new SolicitudLaboral();
        laboral.setNumeroSolicitud(rs.getString("numero_solicitud"));
        laboral.setTipo(rs.getString("tipo"));
        laboral.setOcupacion(rs.getString("ocupacion"));
        laboral.setActividadEconomica(rs.getString("actividad_economica"));
        laboral.setNombreEmpresa(rs.getString("nombre_empresa"));
        laboral.setNit(rs.getString("nit"));
        laboral.setDireccion(rs.getString("direccion"));
        laboral.setCiudad(rs.getString("ciudad"));
        laboral.setDepartamento(rs.getString("departamento"));
        laboral.setTelefono(rs.getString("telefono"));
        laboral.setExtension(rs.getString("extension"));
        laboral.setCargo(rs.getString("cargo"));
        laboral.setFechaIngreso(rs.getString("fecha_ingreso"));
        laboral.setTipoContrato(rs.getString("tipo_contrato"));
        laboral.setSalario(rs.getString("salario"));
        laboral.setOtrosIngresos(rs.getString("otros_ingresos"));
        laboral.setConceptoOtrosIng(rs.getString("concepto_otros_ing"));
        laboral.setGastosManutencion(rs.getString("gastos_manutencion"));
        laboral.setGastosCreditos(rs.getString("gastos_creditos"));
        laboral.setGastosArriendo(rs.getString("gastos_arriendo"));
        laboral.setCelular(rs.getString("celular"));
        laboral.setEmail(rs.getString("email"));
        laboral.setEps(rs.getString("eps"));
        laboral.setTipoAfiliacion(rs.getString("tipo_afiliacion"));
        laboral.setDireccionCobro(rs.getString("direccion_cobro"));
        laboral.setId_persona((rs.getString("id_persona") != null) ? rs.getString("id_persona") : "");
        
        return laboral;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * obtiene el valor de actividadEconomica
     *
     * @return el valor de actividadEconomica
     */
    public String getActividadEconomica() {
        return actividadEconomica;
    }

    /**
     * cambia el valor de actividadEconomica
     *
     * @param actividadEconomica nuevo valor actividadEconomica
     */
    public void setActividadEconomica(String actividadEconomica) {
        this.actividadEconomica = actividadEconomica;
    }

    /**
     * obtiene el valor de cargo
     *
     * @return el valor de cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * cambia el valor de cargo
     *
     * @param extension nuevo valor cargo
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * obtiene el valor de conceptoOtrosIng
     *
     * @return el valor de conceptoOtrosIng
     */
    public String getConceptoOtrosIng() {
        return conceptoOtrosIng;
    }

    /**
     * cambia el valor de conceptoOtrosIng
     *
     * @param conceptoOtrosIng nuevo valor conceptoOtrosIng
     */
    public void setConceptoOtrosIng(String conceptoOtrosIng) {
        this.conceptoOtrosIng = conceptoOtrosIng;
    }

    /**
     * obtiene el valor de direccionCobro
     *
     * @return el valor de direccionCobro
     */
    public String getDireccionCobro() {
        return direccionCobro;
    }

    /**
     * cambia el valor de direccionCobro
     *
     * @param direccionCobro nuevo valor direccionCobro
     */
    public void setDireccionCobro(String direccionCobro) {
        this.direccionCobro = direccionCobro;
    }

    /**
     * obtiene el valor de eps
     *
     * @return el valor de eps
     */
    public String getEps() {
        return eps;
    }

    /**
     * cambia el valor de eps
     *
     * @param eps nuevo valor eps
     */
    public void setEps(String eps) {
        this.eps = eps;
    }

    /**
     * obtiene el valor de extension
     *
     * @return el valor de extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * cambia el valor de extension
     *
     * @param extension nuevo valor extension
     */
    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * obtiene el valor de fechaIngreso
     *
     * @return el valor de fechaIngreso
     */
    public String getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * cambia el valor de fechaIngreso
     *
     * @param fechaIngreso nuevo valor fechaIngreso
     */
    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * obtiene el valor de gastosArriendo
     *
     * @return el valor de gastosArriendo
     */
    public String getGastosArriendo() {
        return gastosArriendo;
    }

    /**
     * cambia el valor de gastosArriendo
     *
     * @param gastosArriendo nuevo valor gastosArriendo
     */
    public void setGastosArriendo(String gastosArriendo) {
        this.gastosArriendo = gastosArriendo;
    }

    /**
     * obtiene el valor de gastosCreditos
     *
     * @return el valor de gastosCreditos
     */
    public String getGastosCreditos() {
        return gastosCreditos;
    }

    /**
     * cambia el valor de gastosCreditos
     *
     * @param gastosCreditos nuevo valor gastosCreditos
     */
    public void setGastosCreditos(String gastosCreditos) {
        this.gastosCreditos = gastosCreditos;
    }

    /**
     * obtiene el valor de gastosManutencion
     *
     * @return el valor de gastosManutencion
     */
    public String getGastosManutencion() {
        return gastosManutencion;
    }

    /**
     * cambia el valor de gastosManutencion
     *
     * @param gastosManutencion nuevo valor gastosManutencion
     */
    public void setGastosManutencion(String gastosManutencion) {
        this.gastosManutencion = gastosManutencion;
    }

    /**
     * obtiene el valor de telefono
     *
     * @return el valor de telefono
     */
    public String getNit() {
        return nit;
    }

    /**
     * cambia el valor de nit
     *
     * @param nit nuevo valor nit
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * obtiene el valor de nombreEmpresa
     *
     * @return el valor de nombreEmpresa
     */
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     * cambia el valor de nombreEmpresa
     *
     * @param nombreEmpresa nuevo valor nombreEmpresa
     */
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /**
     * obtiene el valor de ocupacion
     *
     * @return el valor de ocupacion
     */
    public String getOcupacion() {
        return ocupacion;
    }

    /**
     * cambia el valor de ocupacion
     *
     * @param ocupacion nuevo valor ocupacion
     */
    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    /**
     * obtiene el valor de otrosIngresos
     *
     * @return el valor de otrosIngresos
     */
    public String getOtrosIngresos() {
        return otrosIngresos;
    }

    /**
     * cambia el valor de otrosIngresos
     *
     * @param otrosIngresos nuevo valor otrosIngresos
     */
    public void setOtrosIngresos(String otrosIngresos) {
        this.otrosIngresos = otrosIngresos;
    }

    /**
     * obtiene el valor de salario
     *
     * @return el valor de salario
     */
    public String getSalario() {
        return salario;
    }

    /**
     * cambia el valor de salario
     *
     * @param salario nuevo valor salario
     */
    public void setSalario(String salario) {
        this.salario = salario;
    }

    /**
     * obtiene el valor de tipoAfiliacion
     *
     * @return el valor de tipoAfiliacion
     */
    public String getTipoAfiliacion() {
        return tipoAfiliacion;
    }

    /**
     * cambia el valor de tipoAfiliacion
     *
     * @param tipoAfiliacion nuevo valor tipoAfiliacion
     */
    public void setTipoAfiliacion(String tipoAfiliacion) {
        this.tipoAfiliacion = tipoAfiliacion;
    }

    /**
     * obtiene el valor de tipoContrato
     *
     * @return el valor de tipoContrato
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * cambia el valor de tipoContrato
     *
     * @param tipoContrato nuevo valor tipoContrato
     */
    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    /**
     * obtiene el valor de telefono
     *
     * @return el valor de telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * cambia el valor de telefono
     *
     * @param telefono nuevo valor telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * obtiene el valor de ciudad
     *
     * @return el valor de ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * cambia el valor de ciudad
     *
     * @param ciudad nuevo valor ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * obtiene el valor de departamento
     *
     * @return el valor de departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * cambia el valor de departamento
     *
     * @param departamento nuevo valor departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * obtiene el valor de direccion
     *
     * @return el valor de tdireccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * cambia el valor de direccion
     *
     * @param direccion nuevo valor direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * obtiene el valor de razonSocial
     *
     * @return el valor de razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * cambia el valor de razonSocial
     *
     * @param razonSocial nuevo valor razonSocial
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     * obtiene el valor de fax
     *
     * @return el valor de fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * cambia el valor de fax
     *
     * @param fax nuevo valor fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * obtiene el valor de telefono2
     *
     * @return el valor de telefono2
     */
    public String getTelefono2() {
        return telefono2;
    }

    /**
     * cambia el valor de telefono2
     *
     * @param telefono2 nuevo valor telefono2
     */
    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    /**
     * obtiene el valor de tipo
     *
     * @return el valor de tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * cambia el valor de tipo
     *
     * @param tipo nuevo valor tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * obtiene el valor de creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    /**
     * obtiene el valor de regStatus
     *
     * @return el valor de regStatus
     */
    public String getId_persona() {
        return id_persona;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setId_persona(String id_persona) {
        this.id_persona = id_persona;
    }
}
