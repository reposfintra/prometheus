/*
 * Nombre        ReporteCuadroAzul.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         30 de octubre de 2006, 05:24 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

public class ReporteCuadroAzul {
    
    private String despacho;
    private String remision;
    private String fecha_despacho;
    private String empresa_dpto;
    private String origen;
    private String destino;
    private String p_o;
    private String d_o;
    private String contenido;
    private String tipo_vehiculo;
    private String color;
    private String placa;
    private String conductor;
    private String cedula;
    private String celular;
    private String solicitante;
    private String tel_ext_cel;
    private String fecha_entrega;
    private String numero_factura;
    private String fecha_factura;
    private String valor_flete;
    private String cost_center;
        
    
    /**
     * Crea una nueva instancia de  ReporteCuadroAzul
     */
    public ReporteCuadroAzul() {
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula() {
        return cedula;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula(java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Getter for property celular.
     * @return Value of property celular.
     */
    public java.lang.String getCelular() {
        return celular;
    }
    
    /**
     * Setter for property celular.
     * @param celular New value of property celular.
     */
    public void setCelular(java.lang.String celular) {
        this.celular = celular;
    }
    
    /**
     * Getter for property color.
     * @return Value of property color.
     */
    public java.lang.String getColor() {
        return color;
    }
    
    /**
     * Setter for property color.
     * @param color New value of property color.
     */
    public void setColor(java.lang.String color) {
        this.color = color;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property contenido.
     * @return Value of property contenido.
     */
    public java.lang.String getContenido() {
        return contenido;
    }
    
    /**
     * Setter for property contenido.
     * @param contenido New value of property contenido.
     */
    public void setContenido(java.lang.String contenido) {
        this.contenido = contenido;
    }
    
    /**
     * Getter for property cost_center.
     * @return Value of property cost_center.
     */
    public java.lang.String getCost_center() {
        return cost_center;
    }
    
    /**
     * Setter for property cost_center.
     * @param cost_center New value of property cost_center.
     */
    public void setCost_center(java.lang.String cost_center) {
        this.cost_center = cost_center;
    }
    
    /**
     * Getter for property d_o.
     * @return Value of property d_o.
     */
    public java.lang.String getD_o() {
        return d_o;
    }
    
    /**
     * Setter for property d_o.
     * @param d_o New value of property d_o.
     */
    public void setD_o(java.lang.String d_o) {
        this.d_o = d_o;
    }
    
    /**
     * Getter for property despacho.
     * @return Value of property despacho.
     */
    public java.lang.String getDespacho() {
        return despacho;
    }
    
    /**
     * Setter for property despacho.
     * @param despacho New value of property despacho.
     */
    public void setDespacho(java.lang.String despacho) {
        this.despacho = despacho;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property empresa_dpto.
     * @return Value of property empresa_dpto.
     */
    public java.lang.String getEmpresa_dpto() {
        return empresa_dpto;
    }
    
    /**
     * Setter for property empresa_dpto.
     * @param empresa_dpto New value of property empresa_dpto.
     */
    public void setEmpresa_dpto(java.lang.String empresa_dpto) {
        this.empresa_dpto = empresa_dpto;
    }
    
    /**
     * Getter for property fecha_despacho.
     * @return Value of property fecha_despacho.
     */
    public java.lang.String getFecha_despacho() {
        return fecha_despacho;
    }
    
    /**
     * Setter for property fecha_despacho.
     * @param fecha_despacho New value of property fecha_despacho.
     */
    public void setFecha_despacho(java.lang.String fecha_despacho) {
        this.fecha_despacho = fecha_despacho;
    }
    
    /**
     * Getter for property fecha_entrega.
     * @return Value of property fecha_entrega.
     */
    public java.lang.String getFecha_entrega() {
        return fecha_entrega;
    }
    
    /**
     * Setter for property fecha_entrega.
     * @param fecha_entrega New value of property fecha_entrega.
     */
    public void setFecha_entrega(java.lang.String fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }
    
    /**
     * Getter for property fecha_factura.
     * @return Value of property fecha_factura.
     */
    public java.lang.String getFecha_factura() {
        return fecha_factura;
    }
    
    /**
     * Setter for property fecha_factura.
     * @param fecha_factura New value of property fecha_factura.
     */
    public void setFecha_factura(java.lang.String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }
    
    /**
     * Getter for property numero_factura.
     * @return Value of property numero_factura.
     */
    public java.lang.String getNumero_factura() {
        return numero_factura;
    }
    
    /**
     * Setter for property numero_factura.
     * @param numero_factura New value of property numero_factura.
     */
    public void setNumero_factura(java.lang.String numero_factura) {
        this.numero_factura = numero_factura;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property p_o.
     * @return Value of property p_o.
     */
    public java.lang.String getP_o() {
        return p_o;
    }
    
    /**
     * Setter for property p_o.
     * @param p_o New value of property p_o.
     */
    public void setP_o(java.lang.String p_o) {
        this.p_o = p_o;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
    /**
     * Getter for property solicitante.
     * @return Value of property solicitante.
     */
    public java.lang.String getSolicitante() {
        return solicitante;
    }
    
    /**
     * Setter for property solicitante.
     * @param solicitante New value of property solicitante.
     */
    public void setSolicitante(java.lang.String solicitante) {
        this.solicitante = solicitante;
    }
    
    /**
     * Getter for property tel_ext_cel.
     * @return Value of property tel_ext_cel.
     */
    public java.lang.String getTel_ext_cel() {
        return tel_ext_cel;
    }
    
    /**
     * Setter for property tel_ext_cel.
     * @param tel_ext_cel New value of property tel_ext_cel.
     */
    public void setTel_ext_cel(java.lang.String tel_ext_cel) {
        this.tel_ext_cel = tel_ext_cel;
    }
    
    /**
     * Getter for property tipo_vehiculo.
     * @return Value of property tipo_vehiculo.
     */
    public java.lang.String getTipo_vehiculo() {
        return tipo_vehiculo;
    }
    
    /**
     * Setter for property tipo_vehiculo.
     * @param tipo_vehiculo New value of property tipo_vehiculo.
     */
    public void setTipo_vehiculo(java.lang.String tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }
    
    /**
     * Getter for property valor_flete.
     * @return Value of property valor_flete.
     */
    public java.lang.String getValor_flete() {
        return valor_flete;
    }
    
    /**
     * Setter for property valor_flete.
     * @param valor_flete New value of property valor_flete.
     */
    public void setValor_flete(java.lang.String valor_flete) {
        this.valor_flete = valor_flete;
    }
    
}
