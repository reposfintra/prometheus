/***********************************************************************************
 * Nombre clase : ............... Proveedor_Escolta.java                           *
 * Descripcion :................. Clase que maneja los atributos del objeto        *
 *                                Proveedor_Escolta los cuales representan los     *
 *                                campos de la tabla proveedor escolta             *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 26 de Noviembre de 2005, 08:00 AM                *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;

public class Proveedor_Escolta {
    
    private String dstrct;    
    private String nit;        
    private String origen;
    private String destino;
    private double tarifa;        
    private String reg_status; 
    private String cod_contable;    
    private String creation_user;
    private String creation_date;
    private String last_update;
    private String user_update;
    private String base;
    private String nomProveedor;
    private String nomOrigen;
    private String nomDestino;
    
    public Proveedor_Escolta() {
        
    }    
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property tarifa.
     * @return Value of property tarifa.
     */
    public double getTarifa() {
        return tarifa;
    }
    
    /**
     * Setter for property tarifa.
     * @param tarifa New value of property tarifa.
     */
    public void setTarifa(double tarifa) {
        this.tarifa = tarifa;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property cod_contable.
     * @return Value of property cod_contable.
     */
    public java.lang.String getCod_contable() {
        return cod_contable;
    }
    
    /**
     * Setter for property cod_contable.
     * @param cod_contable New value of property cod_contable.
     */
    public void setCod_contable(java.lang.String cod_contable) {
        this.cod_contable = cod_contable;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property nomProveedor.
     * @return Value of property nomProveedor.
     */
    public java.lang.String getNomProveedor() {
        return nomProveedor;
    }
    
    /**
     * Setter for property nomProveedor.
     * @param nomProveedor New value of property nomProveedor.
     */
    public void setNomProveedor(java.lang.String nomProveedor) {
        this.nomProveedor = nomProveedor;
    }
    
    /**
     * Getter for property nomOrigen.
     * @return Value of property nomOrigen.
     */
    public java.lang.String getNomOrigen() {
        return nomOrigen;
    }
    
    /**
     * Setter for property nomOrigen.
     * @param nomOrigen New value of property nomOrigen.
     */
    public void setNomOrigen(java.lang.String nomOrigen) {
        this.nomOrigen = nomOrigen;
    }
    
    /**
     * Getter for property nomDestino.
     * @return Value of property nomDestino.
     */
    public java.lang.String getNomDestino() {
        return nomDestino;
    }
    
    /**
     * Setter for property nomDestino.
     * @param nomDestino New value of property nomDestino.
     */
    public void setNomDestino(java.lang.String nomDestino) {
        this.nomDestino = nomDestino;
    }
    
}