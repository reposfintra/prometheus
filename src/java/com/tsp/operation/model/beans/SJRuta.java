/**
 * Nombre        SJRuta.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         4 de mayo de 2006, 11:07 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.beans;

import java.util.*;

public class SJRuta implements Cloneable {
    
    private String ruta = "";
    private double costoTotal;
    private String monedaCosto;
    
    private String origen;
    private String destino;
    private String ultimo;
    
    private boolean finalizado = false;
    private boolean exitoso    = false;
    
    
    /** Crea una nueva instancia de  SJRuta */
    public SJRuta() {
    }
    
    public void addNodo(SJCosto nodo) {
        
        if (!ruta.equals("") && !nodo.getCodDestino().equals("") ){
            if (ruta.indexOf( nodo.getCodDestino() )!=-1)
                finalizado = true;
        }
        if ( nodo.getCodDestino().equalsIgnoreCase( nodo.getCodOrigen() ) )
            finalizado = true;
        if ( nodo.getCodDestino().equalsIgnoreCase( destino ) ){
            finalizado = true;
            exitoso    = true;
        }
        
        if (ruta.equals(""))
            ruta  = nodo.getCodOrigen();
        
        if (!ruta.equals(""))
            ruta += ", " + nodo.getCodDestino();
        
        
        costoTotal += nodo.getTotalCosto();
        ultimo      = nodo.getCodDestino();
    }
    
    
    public Object clone() {
        try{
            return super.clone();
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
    
    /**
     * Getter for property costoTotal.
     * @return Value of property costoTotal.
     */
    public double getCostoTotal() {
        return costoTotal;
    }
    
    /**
     * Setter for property costoTotal.
     * @param costoTotal New value of property costoTotal.
     */
    public void setCostoTotal(double costoTotal) {
        this.costoTotal = costoTotal;
    }
    
    /**
     * Getter for property finalizado.
     * @return Value of property finalizado.
     */
    public boolean isFinalizado() {
        return finalizado;
    }
    
    /**
     * Setter for property finalizado.
     * @param finalizado New value of property finalizado.
     */
    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
    }
    
    /**
     * Getter for property monedaCosto.
     * @return Value of property monedaCosto.
     */
    public java.lang.String getMonedaCosto() {
        return monedaCosto;
    }
    
    /**
     * Setter for property monedaCosto.
     * @param monedaCosto New value of property monedaCosto.
     */
    public void setMonedaCosto(java.lang.String monedaCosto) {
        this.monedaCosto = monedaCosto;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property exitoso.
     * @return Value of property exitoso.
     */
    public boolean isExitoso() {
        return exitoso;
    }
    
    /**
     * Setter for property exitoso.
     * @param exitoso New value of property exitoso.
     */
    public void setExitoso(boolean exitoso) {
        this.exitoso = exitoso;
    }
    
    /**
     * Getter for property ultimo.
     * @return Value of property ultimo.
     */
    public java.lang.String getUltimo() {
        return ultimo;
    }
    
    /**
     * Setter for property ultimo.
     * @param ultimo New value of property ultimo.
     */
    public void setUltimo(java.lang.String ultimo) {
        this.ultimo = ultimo;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
}
