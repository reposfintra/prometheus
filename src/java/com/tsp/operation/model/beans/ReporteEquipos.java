/*
 * ViajesAgencia.java
 *
 * Created on 31 de julio de 2005, 12:53
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  mario
 */

import java.util.*;
import java.lang.Math;

public class ReporteEquipos extends ViajesProceso{
    
    private String nombreCliente;
    private String placaTrailer;
    private String NoInterno;
    private String Equipo;
    private String Cabezote;
    private String OT;
    private String Oc;
    private String Fecha_despacho;
    private String Origen_oc;
    private String Destino_oc;
    private String fecha_ult_reporte;
    private String Cliente;
    private String WgEquipo;
    private String WgEquipo_cabezote;
    private String Observaciones_trafico;
    private String Origen_ot;
    private String Destino_ot;
    private String Zona_disponibilidad;
    private String F_D;
    private String Fecha_cumplido;
    
    
     public ReporteEquipos () {
        
    }
    /**
     * Getter for property NoInterno.
     * @return Value of property NoInterno.
     */
    public java.lang.String getNoInterno() {
        return NoInterno;
    }    
    
    /**
     * Setter for property NoInterno.
     * @param NoInterno New value of property NoInterno.
     */
    public void setNoInterno(java.lang.String NoInterno) {
        this.NoInterno = NoInterno;
    }    

    /**
     * Getter for property Equipo.
     * @return Value of property Equipo.
     */
    public java.lang.String getEquipo() {
        return Equipo;
    }    
    
    /**
     * Setter for property Equipo.
     * @param Equipo New value of property Equipo.
     */
    public void setEquipo(java.lang.String Equipo) {
        this.Equipo = Equipo;
    }
    
    /**
     * Getter for property Cabezote.
     * @return Value of property Cabezote.
     */
    public java.lang.String getCabezote() {
        return Cabezote;
    }
    
    /**
     * Setter for property Cabezote.
     * @param Cabezote New value of property Cabezote.
     */
    public void setCabezote(java.lang.String Cabezote) {
        this.Cabezote = Cabezote;
    }
    
    /**
     * Getter for property OT.
     * @return Value of property OT.
     */
    public java.lang.String getOT() {
        return OT;
    }
    
    /**
     * Setter for property OT.
     * @param OT New value of property OT.
     */
    public void setOT(java.lang.String OT) {
        this.OT = OT;
    }
    
    /**
     * Getter for property Oc.
     * @return Value of property Oc.
     */
    public java.lang.String getOc() {
        return Oc;
    }
    
    /**
     * Setter for property Oc.
     * @param Oc New value of property Oc.
     */
    public void setOc(java.lang.String Oc) {
        this.Oc = Oc;
    }
    
    /**
     * Getter for property Fecha_despacho.
     * @return Value of property Fecha_despacho.
     */
    public java.lang.String getFecha_despacho() {
        return Fecha_despacho;
    }
    
    /**
     * Setter for property Fecha_despacho.
     * @param Fecha_despacho New value of property Fecha_despacho.
     */
    public void setFecha_despacho(java.lang.String Fecha_despacho) {
        this.Fecha_despacho = Fecha_despacho;
    }
    
    /**
     * Getter for property Origen_oc.
     * @return Value of property Origen_oc.
     */
    public java.lang.String getOrigen_oc() {
        return Origen_oc;
    }
    
    /**
     * Setter for property Origen_oc.
     * @param Origen_oc New value of property Origen_oc.
     */
    public void setOrigen_oc(java.lang.String Origen_oc) {
        this.Origen_oc = Origen_oc;
    }
    
    /**
     * Getter for property Destino_oc.
     * @return Value of property Destino_oc.
     */
    public java.lang.String getDestino_oc() {
        return Destino_oc;
    }
    
    /**
     * Setter for property Destino_oc.
     * @param Destino_oc New value of property Destino_oc.
     */
    public void setDestino_oc(java.lang.String Destino_oc) {
        this.Destino_oc = Destino_oc;
    }
    
    /**
     * Getter for property fecha_ult_reporte.
     * @return Value of property fecha_ult_reporte.
     */
    public java.lang.String getFecha_ult_reporte() {
        return fecha_ult_reporte;
    }
    
    /**
     * Setter for property fecha_ult_reporte.
     * @param fecha_ult_reporte New value of property fecha_ult_reporte.
     */
    public void setFecha_ult_reporte(java.lang.String fecha_ult_reporte) {
        this.fecha_ult_reporte = fecha_ult_reporte;
    }
    
    /**
     * Getter for property Cliente.
     * @return Value of property Cliente.
     */
    public java.lang.String getCliente() {
        return Cliente;
    }
    
    /**
     * Setter for property Cliente.
     * @param Cliente New value of property Cliente.
     */
    public void setCliente(java.lang.String Cliente) {
        this.Cliente = Cliente;
    }
    
    /**
     * Getter for property WgEquipo.
     * @return Value of property WgEquipo.
     */
    public java.lang.String getWgEquipo() {
        return WgEquipo;
    }
    
    /**
     * Setter for property WgEquipo.
     * @param WgEquipo New value of property WgEquipo.
     */
    public void setWgEquipo(java.lang.String WgEquipo) {
        this.WgEquipo = WgEquipo;
    }
    
    /**
     * Getter for property Observaciones_trafico.
     * @return Value of property Observaciones_trafico.
     */
    public java.lang.String getObservaciones_trafico() {
        return Observaciones_trafico;
    }
    
    /**
     * Setter for property Observaciones_trafico.
     * @param Observaciones_trafico New value of property Observaciones_trafico.
     */
    public void setObservaciones_trafico(java.lang.String Observaciones_trafico) {
        this.Observaciones_trafico = Observaciones_trafico;
    }
    
    /**
     * Getter for property Origen_ot.
     * @return Value of property Origen_ot.
     */
    public java.lang.String getOrigen_ot() {
        return Origen_ot;
    }
    
    /**
     * Setter for property Origen_ot.
     * @param Origen_ot New value of property Origen_ot.
     */
    public void setOrigen_ot(java.lang.String Origen_ot) {
        this.Origen_ot = Origen_ot;
    }
    
    /**
     * Getter for property Destino_ot.
     * @return Value of property Destino_ot.
     */
    public java.lang.String getDestino_ot() {
        return Destino_ot;
    }
    
    /**
     * Setter for property Destino_ot.
     * @param Destino_ot New value of property Destino_ot.
     */
    public void setDestino_ot(java.lang.String Destino_ot) {
        this.Destino_ot = Destino_ot;
    }
    
    /**
     * Getter for property Zona_disponibilidad.
     * @return Value of property Zona_disponibilidad.
     */
    public java.lang.String getZona_disponibilidad() {
        return Zona_disponibilidad;
    }
    
    /**
     * Setter for property Zona_disponibilidad.
     * @param Zona_disponibilidad New value of property Zona_disponibilidad.
     */
    public void setZona_disponibilidad(java.lang.String Zona_disponibilidad) {
        this.Zona_disponibilidad = Zona_disponibilidad;
    }
    
    /**
     * Getter for property F_D.
     * @return Value of property F_D.
     */
    public java.lang.String getF_D() {
        return F_D;
    }
    
    /**
     * Setter for property F_D.
     * @param F_D New value of property F_D.
     */
    public void setF_D(java.lang.String F_D) {
        this.F_D = F_D;
    }
    

    /**
     * Getter for property Fecha_cumplido.
     * @return Value of property Fecha_cumplido.
     */
    public java.lang.String getFecha_cumplido() {
        return Fecha_cumplido;
    }
    
    /**
     * Setter for property Fecha_cumplido.
     * @param Fecha_cumplido New value of property Fecha_cumplido.
     */
    public void setFecha_cumplido(java.lang.String Fecha_cumplido) {
        this.Fecha_cumplido = Fecha_cumplido;
    }
    
    /**
     * Getter for property placaTrailer.
     * @return Value of property placaTrailer.
     */
    public java.lang.String getPlacaTrailer() {
        return placaTrailer;
    }
    
    /**
     * Setter for property placaTrailer.
     * @param placaTrailer New value of property placaTrailer.
     */
    public void setPlacaTrailer(java.lang.String placaTrailer) {
        this.placaTrailer = placaTrailer;
    }
    
    /**
     * Getter for property nombreCliente.
     * @return Value of property nombreCliente.
     */
    public java.lang.String getNombreCliente() {
        return nombreCliente;
    }
    
    /**
     * Setter for property nombreCliente.
     * @param nombreCliente New value of property nombreCliente.
     */
    public void setNombreCliente(java.lang.String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }
    
    /**
     * Getter for property WgEquipo_cabezote.
     * @return Value of property WgEquipo_cabezote.
     */
    public java.lang.String getWgEquipo_cabezote() {
        return WgEquipo_cabezote;
    }
    
    /**
     * Setter for property WgEquipo_cabezote.
     * @param WgEquipo_cabezote New value of property WgEquipo_cabezote.
     */
    public void setWgEquipo_cabezote(java.lang.String WgEquipo_cabezote) {
        this.WgEquipo_cabezote = WgEquipo_cabezote;
    }
    
    /** Creates a new instance of ViajesAgencia */
   
  
    
}
