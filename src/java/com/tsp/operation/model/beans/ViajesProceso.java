/*
 * ViajesProceso.java
 *
 * Created on 9 de agosto de 2005, 04:14 PM
 */


package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.UtilFinanzas;
/**
 *
 * @author  mfontalvo
 */
public class ViajesProceso {

    private int [] viajesEj = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    private int [] viajesPt = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    private double []costosPt = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    
    private int    diferencia = 0;
    private double porcentajeInclumplimiento = 0;
    
    private int totalPt  = 0;
    private int totalEj  = 0;
    private int totalPtF = 0;
    private double totalCostos = 0.0;
    
    private String ano = "";
    private String mes = "";
    
    /** Creates a new instance of ViajesProceso */
    public ViajesProceso() {
    }
    
    public void setViajePtdo (int Dia, int newValue){
        viajesPt [Dia - 1] = newValue;
    }       
    
    public void setViajeEjdo (int Dia, int newValue){
        viajesEj [Dia - 1] = newValue;
    }
    
    public void setCostosPtdo (int Dia, double newValue){
        costosPt [Dia - 1] = newValue;
    }
    
    public void setTotalPt (int newValue){
        totalPt = newValue;
    }
    public void setTotalPtF (int newValue){
        totalPtF = newValue;
    }
    public void setTotalEj (int newValue){
        totalEj = newValue;
    }
    public void setTotalCostos( double newValue ){
        totalCostos = newValue;
    }
    public void setDiferencia (int newValue){
        diferencia = newValue;
    }
    public void setAno (String newValue){
        ano = newValue;
    }
    public void setMes(String newValue){
        mes = newValue;
    }
    
    public void setPorcentajeIncumplimiento (double newValue){
        porcentajeInclumplimiento = newValue;
    }
    
    public int getViajePtdo (int Dia){
        return viajesPt [Dia - 1];
    }
    public int getViajeEjdo (int Dia){
        return viajesEj [Dia - 1];
    }
    
    public double getCostosPtdo(int Dia){
        return costosPt[Dia - 1];
    }
    public int getTotalPt (){
        return totalPt;
    }
    public int getTotalPtF (){
        return totalPtF;
    }
    public int getTotalEj (){
        return totalEj;
    }
    
    public double getTotalCostos(){
        return totalCostos;
    }
    public int getDiferencia (){
        return diferencia;
    }
    public double getPorcentajeIncumplimiento (){
        return porcentajeInclumplimiento;
    }
    public String getAno (){
        return ano;
    }
    public String getMes(){
        return mes;
    }
    
    /////////////////////////////////////////////////////////////
    // procesos
    
    public void CalcularDiferencia(){
        int fechaActual = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
        diferencia = 0;
        porcentajeInclumplimiento = 0;
        totalPt = 0;
        totalEj = 0;
        totalCostos = 0.0;
        totalPtF = 0;
        for (int i=0;i<31;i++){
            totalPt += viajesPt[i];
            totalEj += viajesEj[i];
            totalCostos += costosPt[i];
            
            int fecha = Integer.parseInt ( ano + mes + UtilFinanzas.DiaFormat(i+1) );
            if (fecha <= fechaActual ){
                totalPtF += viajesPt[i];
            }
        }
        diferencia += Math.abs(totalPt - totalEj) ;
        if (totalPtF !=0 ) porcentajeInclumplimiento =  (totalEj / (double) totalPtF) * 100;
    }
    
   
    
}
