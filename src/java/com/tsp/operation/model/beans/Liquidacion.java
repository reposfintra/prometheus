/***********************************************
 * Nombre       Clase  Liquidar.java
 * Autor        ING FERNEL VILLACOB DIAZ
 * Modificado   Ing Sandra Escalante
 * Fecha        21/12/2005
 * Copyright    Transportes Sanchez Polo S.A.
 **********************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.*;


public class Liquidacion {
    
    private Planilla planilla;
    
    private String  oc;
    private String  agencia;
    private String  origen;
    private String  destino;
    private String  placa;
    private String  ot;
    private String  cliente;
    
    private List   facturas;
    private String base = "";
     private boolean mostrarDetalles = false;
    
    
    public Liquidacion() {
        facturas  = new LinkedList();
    }
    
    /**
     * Setter for property oc
     * @param pla New value of property oc
     */
    public void setOC( String val ){
        this.oc       =  val;
    }
    
    /**
     * Setter for property agencia
     * @param pla New value of property agencia
     */
    public void setAgencia  ( String val ){
        this.agencia  =  val;
    }
    
    /**
     * Setter for property origen
     * @param pla New value of property origen
     */
    public void setOrigen   ( String val ){
        this.origen   =  val;
    }
    
    /**
     * Setter for property destino
     * @param pla New value of property destino
     */
    public void setDestino  ( String val ){
        this.destino  =  val;
    }
    
    /**
     * Setter for property placa
     * @param pla New value of property placa
     */
    public void setPlaca    ( String val ){
        this.placa    =  val;
    }
    
    /**
     * Setter for property ot
     * @param pla New value of property ot
     */
    public void setOT       ( String val ){
        this.ot       =  val;
    }
    
    /**
     * Setter for property cliente
     * @param pla New value of property cliente
     */
    public void setCliente  ( String val ){
        this.cliente  =  val;
    }
    
    /**
     * Setter for property facturas
     * @param pla New value of property facturas
     */
    public void setFacturas(List list){
        this.facturas    = list;
    }
    
    /**
     * Getter for property oc.
     * @return Value of property oc
     */
    public String getOC       (  ){
        return  this.oc;
    }
    
    /**
     * Getter for property agencia
     * @return Value of property agencia
     */
    public String getAgencia  (  ){
        return  this.agencia  ;
    }
    
    /**
     * Getter for property origen
     * @return Value of property origen
     */
    public String getOrigen   (  ){
        return  this.origen   ;
    }
    
    /**
     * Getter for property destino
     * @return Value of property destino
     */
    public String getDestino  (  ){
        return  this.destino  ;
    }
    
    /**
     * Getter for property placa
     * @return Value of property placa
     */
    public String getPlaca    (  ){
        return  this.placa    ;
    }
    
    /**
     * Getter for property pla.
     * @return Value of property pla.
     */
    public String getOT       (  ){
        return  this.ot       ;
    }
    
    /**
     * Getter for property pla.
     * @return Value of property pla.
     */
    public String getCliente  (  ){
        return  this.cliente  ;
    }
    
    /**
     * Getter for property facturas
     * @return Value of property facturas
     */
    public List getFacturas    (  ){
        return  this.facturas ;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public com.tsp.operation.model.beans.Planilla getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(com.tsp.operation.model.beans.Planilla planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property mostrarDetalles.
     * @return Value of property mostrarDetalles.
     */
    public boolean isMostrarDetalles() {
        return mostrarDetalles;
    }
    
    /**
     * Setter for property mostrarDetalles.
     * @param mostrarDetalles New value of property mostrarDetalles.
     */
    public void setMostrarDetalles(boolean mostrarDetalles) {
        this.mostrarDetalles = mostrarDetalles;
    }
    
}
