/*
 * FlujoCaja.java
 *
 * Created on 23 de febrero de 2005, 18:14
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Mario Fontalvo
 */
public class DatosView implements Serializable{    
    private Hashtable ListaDatos;
    
    /** Creates a new instance of FlujoCaja */
    public DatosView() {
        ListaDatos = new Hashtable();
    }
    ///////////////////////////////////////////////////
    
    public static DatosView loadDatos(ResultSet rs, int inicio, int numColumnas)throws SQLException{
        DatosView datos = new DatosView();        
        for (int i=0;i<=numColumnas-inicio ;i++)
           datos.addValor(rs.getString(i+inicio), String.valueOf(i)); 
        return datos;
    }
    ////////////////////////////////////////////////////
    // setter 
    public void setListaValores(Hashtable valor){
        this.ListaDatos = valor;
    }
    ///////////////////////////////////////////////////////////
    // getter
    public Hashtable getListaDatos(){
        return this.ListaDatos;
    }    
    /////////////////////////////////////////////////////////////////////////////
    public void addValor(String Total, String key){
        this.ListaDatos.put(key, Total);
    }
    public void addDescriptor(String Descriptor, String key){
        this.ListaDatos.put(key, Descriptor);
    }
    public String getDescriptor(String key){
        return this.ListaDatos.get(key).toString();
    }
    public String getValor(String key){
        return this.ListaDatos.get(key).toString();
    }
    
}
