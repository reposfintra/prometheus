/* * CxpPr.java*/

package com.tsp.operation.model.beans;

/** * * @author  Fintra */
public class CxpPr {
    private String num_os,cxc_pr,id_orden,factura_pr,vlr_neto,abono,saldo,fec_open;
    
    private String base_cxp_pr,iva_cxp_pr,esquema_comision,
        sum_tot_prev1,doc_relacionado;
    
    private String banco,sucursal;
    
    public CxpPr() {    }
    public String getBaseCxpPr() {
        return base_cxp_pr;
    }
    public void setBaseCxpPr(String x) {
        this.base_cxp_pr= x;
    }
    public String getDocRelacionado() {
        return doc_relacionado;
    }
    public void setDocRelacionado(String x) {
        this.doc_relacionado= x;
    }
    public String getIvaCxpPr() {
        return iva_cxp_pr;
    }
    public void setIvaCxpPr(String x) {
        this.iva_cxp_pr= x;
    }    
    public String getEsquemaComision() {
        return esquema_comision;
    }
    public void setEsquemaComision(String x) {
        this.esquema_comision= x;
    }
    
    public String getSumTotPrev1() {
        return sum_tot_prev1;
    }
    public void setSumTotPrev1(String x) {
        this.sum_tot_prev1= x;
    }
    
    public String getNumOs() {
        return num_os;
    }
    public void setNumOs(String x) {
        this.num_os= x;
    }
    public String getCxcPr() {
        return cxc_pr;
    }
    public void setCxcPr(String x) {
        this.cxc_pr= x;
    }
    public String getIdOrden() {
        return id_orden;
    }
    public void setIdOrden(String x) {
        this.id_orden= x;
    }
    public String getFacturaPr() {
        return factura_pr;
    }
    public void setFacturaPr(String x) {
        this.factura_pr= x;
    }
    public String getVlrNeto() {
        return vlr_neto;
    }
    public void setVlrNeto(String x) {
        this.vlr_neto= x;
    }
    public String getAbono() {
        return abono;
    }
    public void setAbono(String x) {
        this.abono= x;
    }
    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String x) {
        this.saldo= x;
    }
    public String getFecOpen() {
        return fec_open;
    }
    public void setFecOpen(String x) {
        this.fec_open= x;
    }    
    
    public String getBanco() {
        return banco;
    }
    public void setBanco(String x) {
        this.banco= x;
    }
    public String getSucursal() {
        return sucursal;
    }
    public void setSucursal(String x) {
        this.sucursal= x;
    }
    
    public static CxpPr load(java.sql.ResultSet rs)throws java.sql.SQLException{
        CxpPr cxpPr = new CxpPr();
        cxpPr.setNumOs( rs.getString("num_os") );           
        cxpPr.setCxcPr( rs.getString("cxc_pr") );             
        cxpPr.setIdOrden( rs.getString("id_orden") ); 
        cxpPr.setFacturaPr( rs.getString("factura_pro") ); 
        cxpPr.setVlrNeto( rs.getString("vlr_neto") ); 
        cxpPr.setAbono( rs.getString("vlr_total_abonos") ); 
        cxpPr.setSaldo( rs.getString("vlr_saldo") ); 
        cxpPr.setFecOpen( rs.getString("fec_open") ); 
        cxpPr.setBaseCxpPr( rs.getString("vlr_1") ); 
        cxpPr.setIvaCxpPr( rs.getString("vlr_2") ); 
        cxpPr.setEsquemaComision( rs.getString("esquema_comision") ); 
        cxpPr.setSumTotPrev1( rs.getString("sum_total_prev1") ); 
        
        cxpPr.setBanco(rs.getString("banco") ); 
        cxpPr.setSucursal(rs.getString("sucursal") ); 
        cxpPr.setDocRelacionado(rs.getString("documento_relacionado") ); 
        
        return cxpPr;
    }  
    
    
}




 
    
 
    
  
    
 

