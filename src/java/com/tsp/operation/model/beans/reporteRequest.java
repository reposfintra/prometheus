////*******************MARIO**************************/////

/*
 * reporteRequest.java
 *
 * Created on 24 de octubre de 2005, 10:03 AM
 */

package com.tsp.operation.model.beans;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.input.SAXBuilder;
import org.jdom.output.*;
import java.util.*;
import java.io.*;

import com.tsp.util.Util;

//**
// Clase reporteRequest
//**
public class reporteRequest {
    
    private String distrito; // toma los datos de un bean
    private String estado = ""; // toma los datos de un bean
    private String uc; // toma los datos de un bean
    private String fc; // toma la fecha y hora del sistema
    private String um; // toma los datos de un bean
    private String fm; // toma la fecha y hora del sistema
    private String base; // toma los datos de un bean
    private String usuMigra; // toma los datos de un bean
    private String fechaHoraMigra; // toma la fecha y hora del sistema
    private String nombreArchivo; // captura el nombre del archivo xml 
    
    private String pues; // captura el texto del elemento puesto
    private String cl; // captura el texto del elemento clave
    private String man; // captura el texto del elemento manifiesto
    private String nov; // captura el texto del elemneto novedad
    private String pl; // captura el texto del elemento placa
    private String fec; // captura el texto del elemento fecha
    private String hor; // captura el texto del elemento hora
    private String cc; // captura el texto del elemento codigo_contenedor
    
    private List hijos;
    private List newList;
    private Iterator i;
    
    public reporteRequest(){
        
    }
    
    public void procesarReporte(String Distrito, String usuario, String Base, String archivo){
       
        this.distrito = Distrito;
        this.base = Base;
        this.uc = usuario;
        this.um = usuario;
        this.usuMigra = usuario;
        this.newList = new LinkedList();
        
        /*Calendar fecha1 = Calendar.getInstance();
        java.util.Date fecha2 = fecha1.getTime();
        
        fc = fecha2.toString();*/
        fc = Util.getFechaActual_String(6);
        fm = fc;
        fechaHoraMigra = fc;
                 
        this.nombreArchivo = archivo;
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta");
        String path = ruta + "/exportar/migracion/" + usuario;
        
        
        
        File error = new File(path +  "/logErrorXML.txt");
        int contador = 1;
        
        
        try{
            error.createNewFile();
            FileWriter fw = new FileWriter(error);
            
            SAXBuilder builder = new SAXBuilder(false);
            Document doc = builder.build(archivo);
            Element raiz = doc.getRootElement();
            hijos = raiz.getChildren();
            i = hijos.iterator();
            
            
            pues = cl = man = fec = nov = pl = hor = cc = "";
            
            while( i.hasNext() ){ 
                Element e = (Element) i.next();
                if( e.getName().equals("puesto") ){
                    pues = e.getText();
                }
                else if( e.getName().equals("clave") ){
                    cl = e.getText();
                }
                else if( e.getName().equals("manifiesto") ){
                    man = e.getText();
                }
                else if( e.getName().equals("novedad") ){
                    nov = e.getText();
                }
                else if( e.getName().equals("placa") ){
                    pl = e.getText();
                }
                else if( e.getName().equals("fecha") ){
                    fec = e.getText();
                }
                else if( e.getName().equals("hora") ){
                    hor = e.getText();
                }
                else if( e.getName().equals("codigo_contenedor") ){
                    cc = e.getText();

                    String[] registros = {pues,cl,man,nov,pl,fec,hor,cc,distrito,estado,uc,fc,um,fm,base,usuMigra,fechaHoraMigra,nombreArchivo};
                    newList.add(registros);
                    pues = cl = man = fec = nov = pl = hor = cc = "";

                }
                else{
                 	String err;
                        err = "Error al intentar procesar la linea " + contador + ". Etiqueta ( "+ e.getName() +" ) desconocida para este formato." + "\n";
                        fw.write(err);
                 		
                 }
                 contador ++;
            }
            fw.close();
            
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
    }
    
    public List getListado(){
        return newList;
    }
    
    
}
