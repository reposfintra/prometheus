/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class TransportadorasLogBeans {

    public String fechainicio;
    public String fechafin;
    public String tipo;
    public String estado;
    public String idproceso;
    public String excepcion;
    public String observacion;
    public String tiempo;
    public String jsonn;
    public String empresa;
    public String proId;
    public String proCodProducto;
    public String proDescripcion;
    public String proEstado;
    public String tdid;
    public String tddescripcion;
    public String tdestado;
    private String cpdid;
    private String cpddescripcion;
    private String cpdporcent;
    private String cpdvalor;
    private String cpdestado;
    private String cpddescripcioncorta;
    private String transrazonsocial;
    private String transid;
    private String transnit;
    private String transcod;
    private String tranrazonsocial;
    private String trandireccion;
    private String trancorreo;
    private String tranreplegal;
    private String trandoc;
    private String tranestado;
    private String tranusuario;
    private String perid;
    private String perdescripcion;
    private String pais;
    private String ciudad;
    private String codciudad;
    private String departamento;
    private String clave;
    private String hc;
    private String tipodoc;
    private String codcli;
    private String nitcli;
    private String nomcli;
    private String facvlr;
    private String facabono;
    private String facsaldo;
    private String facfechvencimiento;
    private String facfechultimopago;
    private String usercreacion;
    private String mcfechcorrida;
    private String mrfechacorrida;
    private String mcplanilla;
    private String mrplanilla;
    private String tipomanifiesto;
    private String facdocumento;
    private String fechacorrida;
    private String planilla;
    private String fechcreacion;
    private String faccreacion;
    private String numplanilla;
    private String fechvencimiento;
    private String agcodagencia;
    private String agdireccion;
    private String agcorreo;
    private String agnombre;
    private String cxc_corrida;
    private String autoriza_venta;
    private String autoriza_ventas;
    private String estado_usuario;
    private String cupo_rotativo;

    public TransportadorasLogBeans() {
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getFechainicio() {
        return fechainicio;
    }

    public void setFechainicio(String fechainicio) {
        this.fechainicio = fechainicio;
    }

    public String getFechafin() {
        return fechafin;
    }

    public void setFechafin(String fechafin) {
        this.fechafin = fechafin;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdproceso() {
        return idproceso;
    }

    public void setIdproceso(String idproceso) {
        this.idproceso = idproceso;
    }

    public String getExcepcion() {
        return excepcion;
    }

    public void setExcepcion(String excepcion) {
        this.excepcion = excepcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getJsonn() {
        return jsonn;
    }

    public void setJsonn(String jsonn) {
        this.jsonn = jsonn;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProCodProducto() {
        return proCodProducto;
    }

    public void setProCodProducto(String proCodProducto) {
        this.proCodProducto = proCodProducto;
    }

    public String getProDescripcion() {
        return proDescripcion;
    }

    public void setProDescripcion(String proDescripcion) {
        this.proDescripcion = proDescripcion;
    }

    public String getProEstado() {
        return proEstado;
    }

    public void setProEstado(String proEstado) {
        this.proEstado = proEstado;
    }

    public String getTdid() {
        return tdid;
    }

    public void setTdid(String tdid) {
        this.tdid = tdid;
    }

    public String getTddescripcion() {
        return tddescripcion;
    }

    public void setTddescripcion(String tddescripcion) {
        this.tddescripcion = tddescripcion;
    }

    public String getCpddescripcion() {
        return cpddescripcion;
    }

    public void setCpddescripcion(String cpddescripcion) {
        this.cpddescripcion = cpddescripcion;
    }

    public String getCpdporcent() {
        return cpdporcent;
    }

    public void setCpdporcent(String cpdporcent) {
        this.cpdporcent = cpdporcent;
    }

    public String getCpdvalor() {
        return cpdvalor;
    }

    public void setCpdvalor(String cpdvalor) {
        this.cpdvalor = cpdvalor;
    }

    public String getCpddescripcioncorta() {
        return cpddescripcioncorta;
    }

    public void setCpddescripcioncorta(String cpddescripcioncorta) {
        this.cpddescripcioncorta = cpddescripcioncorta;
    }

    public String getTransrazonsocial() {
        return transrazonsocial;
    }

    public void setTransrazonsocial(String transrazonsocial) {
        this.transrazonsocial = transrazonsocial;
    }

    public String getTransid() {
        return transid;
    }

    public void setTransid(String transid) {
        this.transid = transid;
    }

    public String getTransnit() {
        return transnit;
    }

    public void setTransnit(String transnit) {
        this.transnit = transnit;
    }

    public String getCpdid() {
        return cpdid;
    }

    public void setCpdid(String cpdid) {
        this.cpdid = cpdid;
    }

    public String getCpdestado() {
        return cpdestado;
    }

    public void setCpdestado(String cpdestado) {
        this.cpdestado = cpdestado;
    }

    public String getTdestado() {
        return tdestado;
    }

    public void setTdestado(String tdestado) {
        this.tdestado = tdestado;
    }

    public String getTranscod() {
        return transcod;
    }

    public void setTranscod(String transcod) {
        this.transcod = transcod;
    }

    public String getTranrazonsocial() {
        return tranrazonsocial;
    }

    public void setTranrazonsocial(String tranrazonsocial) {
        this.tranrazonsocial = tranrazonsocial;
    }

    public String getTrandireccion() {
        return trandireccion;
    }

    public void setTrandireccion(String trandireccion) {
        this.trandireccion = trandireccion;
    }

    public String getTrancorreo() {
        return trancorreo;
    }

    public void setTrancorreo(String trancorreo) {
        this.trancorreo = trancorreo;
    }

    public String getTranreplegal() {
        return tranreplegal;
    }

    public void setTranreplegal(String tranreplegal) {
        this.tranreplegal = tranreplegal;
    }

    public String getTrandoc() {
        return trandoc;
    }

    public void setTrandoc(String trandoc) {
        this.trandoc = trandoc;
    }

    public String getTranestado() {
        return tranestado;
    }

    public void setTranestado(String tranestado) {
        this.tranestado = tranestado;
    }

    public String getTranusuario() {
        return tranusuario;
    }

    public void setTranusuario(String tranusuario) {
        this.tranusuario = tranusuario;
    }

    public String getPerid() {
        return perid;
    }

    public void setPerid(String perid) {
        this.perid = perid;
    }

    public String getPerdescripcion() {
        return perdescripcion;
    }

    public void setPerdescripcion(String perdescripcion) {
        this.perdescripcion = perdescripcion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getHc() {
        return hc;
    }

    public void setHc(String hc) {
        this.hc = hc;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getNitcli() {
        return nitcli;
    }

    public void setNitcli(String nitcli) {
        this.nitcli = nitcli;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getFacvlr() {
        return facvlr;
    }

    public void setFacvlr(String facvlr) {
        this.facvlr = facvlr;
    }

    public String getFacabono() {
        return facabono;
    }

    public void setFacabono(String facabono) {
        this.facabono = facabono;
    }

    public String getFacfechvencimiento() {
        return facfechvencimiento;
    }

    public void setFacfechvencimiento(String facfechvencimiento) {
        this.facfechvencimiento = facfechvencimiento;
    }

    public String getFacfechultimopago() {
        return facfechultimopago;
    }

    public void setFacfechultimopago(String facfechultimopago) {
        this.facfechultimopago = facfechultimopago;
    }

    public String getUsercreacion() {
        return usercreacion;
    }

    public void setUsercreacion(String usercreacion) {
        this.usercreacion = usercreacion;
    }

    public String getMcfechcorrida() {
        return mcfechcorrida;
    }

    public void setMcfechcorrida(String mcfechcorrida) {
        this.mcfechcorrida = mcfechcorrida;
    }

    public String getMrfechacorrida() {
        return mrfechacorrida;
    }

    public void setMrfechacorrida(String mrfechacorrida) {
        this.mrfechacorrida = mrfechacorrida;
    }

    public String getMcplanilla() {
        return mcplanilla;
    }

    public void setMcplanilla(String mcplanilla) {
        this.mcplanilla = mcplanilla;
    }

    public String getMrplanilla() {
        return mrplanilla;
    }

    public void setMrplanilla(String mrplanilla) {
        this.mrplanilla = mrplanilla;
    }

    public String getTipomanifiesto() {
        return tipomanifiesto;
    }

    public void setTipomanifiesto(String tipomanifiesto) {
        this.tipomanifiesto = tipomanifiesto;
    }

    public String getFacdocumento() {
        return facdocumento;
    }

    public void setFacdocumento(String facdocumento) {
        this.facdocumento = facdocumento;
    }

    public String getFechacorrida() {
        return fechacorrida;
    }

    public void setFechacorrida(String fechacorrida) {
        this.fechacorrida = fechacorrida;
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getFechcreacion() {
        return fechcreacion;
    }

    public void setFechcreacion(String fechcreacion) {
        this.fechcreacion = fechcreacion;
    }

    public String getFacsaldo() {
        return facsaldo;
    }

    public void setFacsaldo(String facasaldo) {
        this.facsaldo = facasaldo;
    }

    public String getFaccreacion() {
        return faccreacion;
    }

    public void setFaccreacion(String faccreacion) {
        this.faccreacion = faccreacion;
    }

    public String getNumplanilla() {
        return numplanilla;
    }

    public void setNumplanilla(String numplanilla) {
        this.numplanilla = numplanilla;
    }

    public String getFechvencimiento() {
        return fechvencimiento;
    }

    public void setFechvencimiento(String fechvencimiento) {
        this.fechvencimiento = fechvencimiento;
    }

    public String getAgcodagencia() {
        return agcodagencia;
    }

    public void setAgcodagencia(String agcodagencia) {
        this.agcodagencia = agcodagencia;
    }

    public String getAgdireccion() {
        return agdireccion;
    }

    public void setAgdireccion(String agdireccion) {
        this.agdireccion = agdireccion;
    }

    public String getAgcorreo() {
        return agcorreo;
    }

    public void setAgcorreo(String agcorreo) {
        this.agcorreo = agcorreo;
    }

    public String getAgnombre() {
        return agnombre;
    }

    public void setAgnombre(String agnombre) {
        this.agnombre = agnombre;
    }

    public String getCodciudad() {
        return codciudad;
    }

    public void setCodciudad(String codciudad) {
        this.codciudad = codciudad;
    }

    public String getCxc_corrida() {
        return cxc_corrida;
    }

    public void setCxc_corrida(String cxc_corrida) {
        this.cxc_corrida = cxc_corrida;
    }

    public String getAutoriza_venta() {
        return autoriza_venta;
    }

    public void setAutoriza_venta(String autoriza_venta) {
        this.autoriza_venta = autoriza_venta;
    }

    public String getCupo_rotativo() {
        return cupo_rotativo;
    }

    public void setCupo_rotativo(String cupo_rotativo) {
        this.cupo_rotativo = cupo_rotativo;
    }

    public String getAutoriza_ventas() {
        return autoriza_ventas;
    }

    public void setAutoriza_ventas(String autoriza_ventas) {
        this.autoriza_ventas = autoriza_ventas;
    }

    public String getEstado_usuario() {
        return estado_usuario;
    }

    public void setEstado_usuario(String estado_usuario) {
        this.estado_usuario = estado_usuario;
    }

    
}
