/**********************************************************
 * Nombre:        CXPImportacion.java                     *
 * Descripci�n:   Bean de importacion de facturas         *
 * Autor:         Ing. Mario Fontalvo                     *
 * Fecha:         25 de octubre de 2005, 09:08 PM         *
 * Versi�n:       Java  1.0                               *
 * Copyright:     Fintravalores S.A. S.A.            *
 **********************************************************/

package com.tsp.operation.model.beans;

import java.beans.*;
import java.io.Serializable;
import java.sql.*;


public class CXPImportacion implements Serializable {
    
    // cabecera de la factura
    private String dstrct;                //(*)
    private String proveedor;             //(*)
    private String hc;
    private String tipo_documento;        //(*)
    private String documento;             //(*)
    private String descripcion_doc;       //(*)
    private String fecha_documento;       //(*)
    private String fecha_vencimiento;     // (*)
    private int    plazo;
    private String tipo_documento_rel;    // (*)
    private String documento_relacionado; // (*)
    private double vlr_neto_me;           // (*)de la factura
    private String banco;                 // (*)
    private String sucursal;              // (*)
    private String moneda;                // (*)
    private String agencia;
    private String clase_documento;
    private String autorizador;
    private String observacion;
    private String item;                  // (*)
    private String descripcion_item;      // (*)
    private double vlr_me;                // (*) del item
    private String codigo_cuenta;         // (*)
    private String tipo_auxiliar;         // (*)
    private String auxiliar;              // (*)
    private String codigo_abc;            // (*)
    private String planilla;              // (*)
    private String iva;                   // (*)
    private String riva;                   // (*)
    private String rica;                   // (*)
    private String rfte;                   // (*)
    private String creation_date;         // (*) de la factura
    private String creation_user;         // (*) de la factura
    private String last_update;           // (*) de la factura
    private String user_update;           // (*) de la factura
    private String base;                  // (*) de la factura    
    private int    numCoutas   = 0;
    private String fechaInicio = "";    
    private String frecuencia  = "";
    
    private String referencia = "";
    

    /**
     * Getter for property fechaInicio.
     * @return Value of property fechaInicio.
     */
    public java.lang.String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Setter for property fechaInicio.
     * @param fechaInicio New value of property fechaInicio.
     */
    public void setFechaInicio(java.lang.String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    
    /**
     * Getter for property numCoutas.
     * @return Value of property numCoutas.
     */
    public int getNumCoutas() {
        return numCoutas;
    }
    
    /**
     * Setter for property numCoutas.
     * @param numCoutas New value of property numCoutas.
     */
    public void setNumCoutas(int numCoutas) {
        this.numCoutas = numCoutas;
    }
    
    /**
     * Getter for property frecuencia.
     * @return Value of property frecuencia.
     */
    public java.lang.String getFrecuencia() {
        return frecuencia;
    }
    
    /**
     * Setter for property frecuencia.
     * @param frecuencia New value of property frecuencia.
     */
    public void setFrecuencia(java.lang.String frecuencia) {
        this.frecuencia = frecuencia;
    }
    
    
    
    public CXPImportacion() {
    }
    
    public static CXPImportacion loadRXP(ResultSet rs) throws SQLException {
        CXPImportacion dt = new CXPImportacion();
        dt.setDstrct               ( rs.getString("dstrct"));
        dt.setProveedor            ( rs.getString("proveedor"));
        dt.setHc                   ( rs.getString("handle_code"));
        dt.setTipo_documento       ( rs.getString("tipo_documento"));
        dt.setDocumento            ( rs.getString("documento"));
        dt.setDescripcion_doc      ( rs.getString("descripcion_doc"));
        dt.setFecha_documento      ( rs.getString("fecha_documento"));
        dt.setFecha_vencimiento    ( rs.getString("fecha_vencimiento"));
        dt.setPlazo                ( rs.getInt   ("plazo"));
        dt.setTipo_documento_rel   ( rs.getString("tipo_documento_rel"));
        dt.setDocumento_relacionado( rs.getString("documento_relacionado"));
        dt.setVlr_neto_me          ( rs.getDouble("vlr_neto_me"));
        dt.setBanco                ( rs.getString("banco"));
        dt.setSucursal             ( rs.getString("sucursal"));
        dt.setMoneda               ( rs.getString("moneda"));
        dt.setAutorizador          ( rs.getString("autorizador"));
        dt.setObservacion          ( rs.getString("observacion"));        
        dt.setClase_documento      ( "4" );

        dt.setNumCoutas            ( rs.getInt   ("num_cuotas") );
        dt.setFechaInicio          ( rs.getString("fecha_inicio") );
        dt.setFrecuencia           ( rs.getString("frecuencia") );
        dt.setReferencia           ( rs.getString("referencia") );
        
        dt.setItem                 ( rs.getString("item"));
        dt.setDescripcion_item     ( rs.getString("descripcion_item"));
        dt.setVlr_me               ( rs.getDouble("vlr_me"));
        dt.setCodigo_cuenta        ( rs.getString("codigo_cuenta"));
        dt.setTipo_auxiliar        ( rs.getString("tipo_auxiliar"));
        dt.setAuxiliar             ( rs.getString("auxiliar"));
        dt.setCodigo_abc           ( rs.getString("codigo_abc"));
        dt.setPlanilla             ( rs.getString("planilla"));
        dt.setIva                  ( rs.getString("iva"));
        dt.setRiva                 ( rs.getString("riva"));
        dt.setRica                 ( rs.getString("rica"));
        dt.setRfte                 ( rs.getString("rfte"));
        dt.setCreation_date        ( rs.getString("creation_date"));
        dt.setCreation_user        ( rs.getString("creation_user"));
        dt.setLast_update          ( rs.getString("creation_date"));
        dt.setUser_update          ( rs.getString("creation_user"));
        
        return dt;
    }

    public static CXPImportacion load(ResultSet rs) throws SQLException {
        CXPImportacion dt = new CXPImportacion();
        dt.setDstrct               ( rs.getString("dstrct"));
        dt.setProveedor            ( rs.getString("proveedor"));
        dt.setHc                   ( rs.getString("handle_code"));
        dt.setTipo_documento       ( rs.getString("tipo_documento"));
        dt.setDocumento            ( rs.getString("documento"));
        dt.setDescripcion_doc      ( rs.getString("descripcion_doc"));
        dt.setFecha_documento      ( rs.getString("fecha_documento"));
        dt.setFecha_vencimiento    ( rs.getString("fecha_vencimiento"));
        dt.setPlazo                ( rs.getInt   ("plazo"));
        dt.setTipo_documento_rel   ( rs.getString("tipo_documento_rel"));
        dt.setDocumento_relacionado( rs.getString("documento_relacionado"));
        dt.setVlr_neto_me          ( rs.getDouble("vlr_neto_me"));
        dt.setBanco                ( rs.getString("banco"));
        dt.setSucursal             ( rs.getString("sucursal"));
        dt.setMoneda               ( rs.getString("moneda"));
        dt.setAutorizador          ( rs.getString("autorizador"));
        dt.setObservacion          ( rs.getString("observacion"));        
        dt.setClase_documento      ( "4" );
        dt.setItem                 ( rs.getString("item"));
        dt.setDescripcion_item     ( rs.getString("descripcion_item"));
        dt.setVlr_me               ( rs.getDouble("vlr_me"));
        dt.setCodigo_cuenta        ( rs.getString("codigo_cuenta"));
        dt.setTipo_auxiliar        ( rs.getString("tipo_auxiliar"));
        dt.setAuxiliar             ( rs.getString("auxiliar"));
        dt.setCodigo_abc           ( rs.getString("codigo_abc"));
        dt.setPlanilla             ( rs.getString("planilla"));
        dt.setIva                  ( rs.getString("iva"));
        dt.setRiva                 ( rs.getString("riva"));
        dt.setRica                 ( rs.getString("rica"));
        dt.setRfte                 ( rs.getString("rfte"));
        dt.setCreation_date        ( rs.getString("creation_date"));
        dt.setCreation_user        ( rs.getString("creation_user"));
        dt.setLast_update          ( rs.getString("creation_date"));
        dt.setUser_update          ( rs.getString("creation_user"));
        
        return dt;
    }
       
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property codigo_abc.
     * @return Value of property codigo_abc.
     */
    public java.lang.String getCodigo_abc() {
        return codigo_abc;
    }
    
    /**
     * Setter for property codigo_abc.
     * @param codigo_abc New value of property codigo_abc.
     */
    public void setCodigo_abc(java.lang.String codigo_abc) {
        this.codigo_abc = codigo_abc;
    }
    
    /**
     * Getter for property codigo_cuenta.
     * @return Value of property codigo_cuenta.
     */
    public java.lang.String getCodigo_cuenta() {
        return codigo_cuenta;
    }
    
    /**
     * Setter for property codigo_cuenta.
     * @param codigo_cuenta New value of property codigo_cuenta.
     */
    public void setCodigo_cuenta(java.lang.String codigo_cuenta) {
        this.codigo_cuenta = codigo_cuenta;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion_doc.
     * @return Value of property descripcion_doc.
     */
    public java.lang.String getDescripcion_doc() {
        return descripcion_doc;
    }
    
    /**
     * Setter for property descripcion_doc.
     * @param descripcion_doc New value of property descripcion_doc.
     */
    public void setDescripcion_doc(java.lang.String descripcion_doc) {
        this.descripcion_doc = descripcion_doc;
    }
    
    /**
     * Getter for property descripcion_item.
     * @return Value of property descripcion_item.
     */
    public java.lang.String getDescripcion_item() {
        return descripcion_item;
    }
    
    /**
     * Setter for property descripcion_item.
     * @param descripcion_item New value of property descripcion_item.
     */
    public void setDescripcion_item(java.lang.String descripcion_item) {
        this.descripcion_item = descripcion_item;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property documento_relacionado.
     * @return Value of property documento_relacionado.
     */
    public java.lang.String getDocumento_relacionado() {
        return documento_relacionado;
    }
    
    /**
     * Setter for property documento_relacionado.
     * @param documento_relacionado New value of property documento_relacionado.
     */
    public void setDocumento_relacionado(java.lang.String documento_relacionado) {
        this.documento_relacionado = documento_relacionado;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_documento.
     * @return Value of property fecha_documento.
     */
    public java.lang.String getFecha_documento() {
        return fecha_documento;
    }
    
    /**
     * Setter for property fecha_documento.
     * @param fecha_documento New value of property fecha_documento.
     */
    public void setFecha_documento(java.lang.String fecha_documento) {
        this.fecha_documento = fecha_documento;
    }
    
    /**
     * Getter for property fecha_vencimiento.
     * @return Value of property fecha_vencimiento.
     */
    public java.lang.String getFecha_vencimiento() {
        return fecha_vencimiento;
    }
    
    /**
     * Setter for property fecha_vencimiento.
     * @param fecha_vencimiento New value of property fecha_vencimiento.
     */
    public void setFecha_vencimiento(java.lang.String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property tipo_documento_rel.
     * @return Value of property tipo_documento_rel.
     */
    public java.lang.String getTipo_documento_rel() {
        return tipo_documento_rel;
    }
    
    /**
     * Setter for property tipo_documento_rel.
     * @param tipo_documento_rel New value of property tipo_documento_rel.
     */
    public void setTipo_documento_rel(java.lang.String tipo_documento_rel) {
        this.tipo_documento_rel = tipo_documento_rel;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property vlr_me.
     * @return Value of property vlr_me.
     */
    public double getVlr_me() {
        return vlr_me;
    }
    
    /**
     * Setter for property vlr_me.
     * @param vlr_me New value of property vlr_me.
     */
    public void setVlr_me(double vlr_me) {
        this.vlr_me = vlr_me;
    }
    
    /**
     * Getter for property vlr_neto_me.
     * @return Value of property vlr_neto_me.
     */
    public double getVlr_neto_me() {
        return vlr_neto_me;
    }
    
    /**
     * Setter for property vlr_neto_me.
     * @param vlr_neto_me New value of property vlr_neto_me.
     */
    public void setVlr_neto_me(double vlr_neto_me) {
        this.vlr_neto_me = vlr_neto_me;
    }
    
    /**
     * Getter for property plazo.
     * @return Value of property plazo.
     */
    public int getPlazo() {
        return plazo;
    }
    
    /**
     * Setter for property plazo.
     * @param plazo New value of property plazo.
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property clase_documento.
     * @return Value of property clase_documento.
     */
    public java.lang.String getClase_documento() {
        return clase_documento;
    }
    
    /**
     * Setter for property clase_documento.
     * @param clase_documento New value of property clase_documento.
     */
    public void setClase_documento(java.lang.String clase_documento) {
        this.clase_documento = clase_documento;
    }
    
    /**
     * Getter for property tipo_auxiliar.
     * @return Value of property tipo_auxiliar.
     */
    public java.lang.String getTipo_auxiliar() {
        return tipo_auxiliar;
    }
    
    /**
     * Setter for property tipo_auxiliar.
     * @param tipo_auxiliar New value of property tipo_auxiliar.
     */
    public void setTipo_auxiliar(java.lang.String tipo_auxiliar) {
        this.tipo_auxiliar = tipo_auxiliar;
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    public String getKey (){
        return this.dstrct + "|" + this.proveedor + "|" + this.tipo_documento + "|" + this.documento;
    }
    
    /**
     * Getter for property autorizador.
     * @return Value of property autorizador.
     */
    public java.lang.String getAutorizador() {
        return autorizador;
    }
    
    /**
     * Setter for property autorizador.
     * @param autorizador New value of property autorizador.
     */
    public void setAutorizador(java.lang.String autorizador) {
        this.autorizador = autorizador;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property iva.
     * @return Value of property iva.
     */
    public java.lang.String getIva() {
        return iva;
    }
    
    /**
     * Setter for property iva.
     * @param iva New value of property iva.
     */
    public void setIva(java.lang.String iva) {
        this.iva = iva;
    }
    
    /**
     * Getter for property riva.
     * @return Value of property riva.
     */
    public java.lang.String getRiva() {
        return riva;
    }
    
    /**
     * Setter for property riva.
     * @param riva New value of property riva.
     */
    public void setRiva(java.lang.String riva) {
        this.riva = riva;
    }
    
    /**
     * Getter for property rica.
     * @return Value of property rica.
     */
    public java.lang.String getRica() {
        return rica;
    }
    
    /**
     * Setter for property rica.
     * @param rica New value of property rica.
     */
    public void setRica(java.lang.String rica) {
        this.rica = rica;
    }
    
    /**
     * Getter for property rfte.
     * @return Value of property rfte.
     */
    public java.lang.String getRfte() {
        return rfte;
    }
    
    /**
     * Setter for property rfte.
     * @param rfte New value of property rfte.
     */
    public void setRfte(java.lang.String rfte) {
        this.rfte = rfte;
    }
    
    /**
     * Getter for property hc.
     * @return Value of property hc.
     */
    public java.lang.String getHc() {
        return hc;
    }
    
    /**
     * Setter for property hc.
     * @param hc New value of property hc.
     */
    public void setHc(java.lang.String hc) {
        this.hc = hc;
    }
    
    /**
     * Getter for property referencia.
     * @return Value of property referencia.
     */
    public java.lang.String getReferencia() {
        return referencia;
    }
    
    /**
     * Setter for property referencia.
     * @param referencia New value of property referencia.
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }
    
    
}
