/*
 * Carroceria.java
 *
 * Created on 9 de noviembre de 2004, 01:44 PM
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;
/**
 *
 * @author  AMENDEZ
 */
public class Carroceria implements Serializable{
    
    String codigo;
    String descripcion;
    /** Creates a new instance of Color */
    public Carroceria() {
    }
    
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    
    public String getCodigo(){
        return this.codigo;
    }
    
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    
    public String getDescripcion(){
        return this.descripcion;
    }
    
}
