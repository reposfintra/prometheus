/****
 * Nombre:                       Plan_llegada_cargue.java
 * Descripci�n:                  Beans de planeacion llegada cargue
 * Autor:                        Ing. Diogenes Antonio Bastidas Morales
 * Fecha:                        13 de septiembre de 2006, 02:37 PM
 * Versi�n: Java                 1.0
 * Copyright: Fintravalores S.A. S.A.
 ****/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class Plan_llegada_cargue implements Serializable{
    public String reg_status;
    public String dstrct;
    public String nro_trasn;
    public String codcli;
    public String numrem;
    public String llegada_cargue;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String base;
   
    
    
    public Plan_llegada_cargue() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property llegada_cargue.
     * @return Value of property llegada_cargue.
     */
    public java.lang.String getLlegada_cargue() {
        return llegada_cargue;
    }
    
    /**
     * Setter for property llegada_cargue.
     * @param llegada_cargue New value of property llegada_cargue.
     */
    public void setLlegada_cargue(java.lang.String llegada_cargue) {
        this.llegada_cargue = llegada_cargue;
    }
    
    /**
     * Getter for property nro_trasn.
     * @return Value of property nro_trasn.
     */
    public java.lang.String getNro_trasn() {
        return nro_trasn;
    }
    
    /**
     * Setter for property nro_trasn.
     * @param nro_trasn New value of property nro_trasn.
     */
    public void setNro_trasn(java.lang.String nro_trasn) {
        this.nro_trasn = nro_trasn;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}
