/*
 * General.java
 *
 * Created on 10 de enero de 2007
 */

package com.tsp.operation.model.beans;

import java.sql.*;

/**
 *
 * @author  lfrieri
 */
public class Veto {
    private String referencia;
    private String causa;
    private String observacion;
    private String descripcion;
    private String fuente;
        
    /** Creates a new instance of Veto */
    public Veto() {
    }
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa() {
        return causa;
    }    
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.lang.String causa) {
        this.causa = causa;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property referencia.
     * @return Value of property referencia.
     */
    public java.lang.String getReferencia() {
        return referencia;
    }
    
    /**
     * Setter for property referencia.
     * @param referencia New value of property referencia.
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property fuente.
     * @return Value of property fuente.
     */
    public java.lang.String getFuente() {
        return fuente;
    }
    
    /**
     * Setter for property fuente.
     * @param fuente New value of property fuente.
     */
    public void setFuente(java.lang.String fuente) {
        this.fuente = fuente;
    }
    
}
