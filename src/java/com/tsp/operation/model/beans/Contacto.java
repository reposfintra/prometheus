/*
 * Nombre        Contacto.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         24 de febrero de 2005, 10:42 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class Contacto implements Serializable {
    private String cod_contacto;
    private String cod_cia;
    private String tipo;    
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    private String base;    
    private String cia;
    
    private String nomcontacto;
    private String nomcopania;
    private String nomtipo;
    
    /** Creates a new instance of Contacto */
    public static Contacto load(ResultSet rs)throws SQLException {
        Contacto contacto = new Contacto();  
        contacto.setCod_contacto( rs.getString("cod_contacto") );
        contacto.setCod_cia( rs.getString("cod_cia") );
        contacto.setTipo( rs.getString("tipo_contacto") );
        contacto.setRec_status( rs.getString("rec_status") );
        contacto.setLast_update( rs.getDate("last_update") );
        contacto.setUser_update( rs.getString("user_update") );
        contacto.setCreation_date( rs.getDate("creation_date") );
        contacto.setCreation_user( rs.getString("creation_user") ); 
        contacto.setBase(rs.getString("base"));
        contacto.setCia(rs.getString("cia"));     
        return contacto;
    }
    
    /**
     * Setter for property cod_contacto
     * @param base New value of property cod_contacto
     */
    public void setCod_contacto(String cod_contacto){
        this.cod_contacto= cod_contacto;
    }

    /**
     * Setter for property cod_cia.
     * @param base New value of property cod_cia.
     */
    public void setCod_cia(String cod_cia){
        this.cod_cia= cod_cia;
    }
    
    /**
     * Setter for property tipo.
     * @param base New value of property tipo.
     */
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    
    /**
     * Setter for property rec_status.
     * @param base New value of property rec_status.
     */
    public void setRec_status(String rec_status ){       
        this.rec_status = rec_status;       
    }
    
    /**
     * Setter for property last_update.
     * @param base New value of property last_update.
     */
    public void setLast_update(java.util.Date last_update){
        this.last_update = last_update;
    }
    
    /**
     * Setter for property user_update.
     * @param base New value of property user_update.
     */
    public void setUser_update(String user_update){        
        this.user_update= user_update;        
    }
    
    /**
     * Setter for property creation_date.
     * @param base New value of property creation_date.
     */
    public void setCreation_date(java.util.Date creation_date){
        this.creation_date = creation_date;
    }
    
    /**
     * Setter for property creation_user.
     * @param base New value of property creation_user.
     */
    public void setCreation_user(String creation_user){        
        this.creation_user= creation_user;        
    }
   
    
   /**
     * Getter for property cod_contacto.
     * @return Value of property cod_contacto
     */ 
    
    public String getCod_contacto( ){    
        return cod_contacto;       
    }
    
    /**
     * Getter for property cod_cia
     * @return Value of property cod_cia
     */
    public String getCod_cia( ){
        return cod_cia;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public String getTipo( ){
        return tipo;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public String getRec_status( ){       
        return rec_status;       
    }

    /**
     * Getter for property lastUpdate.
     * @return Value of property lastUpdate
     */
    public java.util.Date getLast_update( ){
        return last_update;
    }

    /**
     * Getter for property user_update.
     * @return Value of property user_update
     */
    public String getUser_update( ){        
        return user_update;        
    }

    /**
     * Getter for property creation_date.
     * @return Value of property creation_date
     */
    public java.util.Date getCreation_date( ){
        return creation_date;
    }    

    /**
     * Getter for property creation_user.
     * @return Value of property creation_user
     */
    public String getCreation_user( ){        
        return creation_user;        
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }    
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }    
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }    
   
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }    
    
    /**
     * Getter for property nomcopania.
     * @return Value of property nomcopania.
     */
    public java.lang.String getNomcopania() {
        return nomcopania;
    }    
    
    /**
     * Setter for property nomcopania.
     * @param nomcopania New value of property nomcopania.
     */
    public void setNomcopania(java.lang.String nomcopania) {
        this.nomcopania = nomcopania;
    }    
    
    /**
     * Getter for property nomcontacto.
     * @return Value of property nomcontacto.
     */
    public java.lang.String getNomcontacto() {
        return nomcontacto;
    }
    
    /**
     * Setter for property nomcontacto.
     * @param nomcontacto New value of property nomcontacto.
     */
    public void setNomcontacto(java.lang.String nomcontacto) {
        this.nomcontacto = nomcontacto;
    }
    
    /**
     * Getter for property nomtipo.
     * @return Value of property nomtipo.
     */
    public java.lang.String getNomtipo() {
        return nomtipo;
    }
    
    /**
     * Setter for property nomtipo.
     * @param nomtipo New value of property nomtipo.
     */
    public void setNomtipo(java.lang.String nomtipo) {
        this.nomtipo = nomtipo;
    }
    
}