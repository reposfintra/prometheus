/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;
//darrieta
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author maltamiranda
 */
public class ConveniosRemesas implements Serializable{
    String reg_status, dstrct, id_convenio, id_remesa, ciudad_sede, banco_titulo,
           ciudad_titulo, cuenta_remesa,
           creation_user, user_update, creation_date, last_update;
    double porcentaje_remesa;
    boolean genera_remesa;

    public ConveniosRemesas() {
        this.reg_status = "";
        this.dstrct = "";
        this.id_convenio = "";
        this.id_remesa = "";
        this.ciudad_sede = "";
        this.banco_titulo = "";
        this.ciudad_titulo = "";
        this.genera_remesa = false;
        this.porcentaje_remesa = 0;
        this.cuenta_remesa = "";
        this.creation_user = "";
        this.user_update = "";
        this.creation_date = "";
        this.last_update = "";
    }

    public String getBanco_titulo() {
        return banco_titulo;
    }

    public void setBanco_titulo(String banco_titulo) {
        this.banco_titulo = banco_titulo;
    }

    public String getCiudad_sede() {
        return ciudad_sede;
    }

    public void setCiudad_sede(String ciudad_sede) {
        this.ciudad_sede = ciudad_sede;
    }

    public String getCiudad_titulo() {
        return ciudad_titulo;
    }

    public void setCiudad_titulo(String ciudad_titulo) {
        this.ciudad_titulo = ciudad_titulo;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getCuenta_remesa() {
        return cuenta_remesa;
    }

    public void setCuenta_remesa(String cuenta_remesa) {
        this.cuenta_remesa = cuenta_remesa;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public void setGenera_remesa(boolean genera_remesa) {
        this.genera_remesa = genera_remesa;
    }

    public String getId_convenio() {
        return id_convenio;
    }

    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }

    public String getId_remesa() {
        return id_remesa;
    }

    public void setId_remesa(String id_remesa) {
        this.id_remesa = id_remesa;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public double getPorcentaje_remesa() {
        return porcentaje_remesa;
    }

    public void setPorcentaje_remesa(double porcentaje_remesa) {
        this.porcentaje_remesa = porcentaje_remesa;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }
    public boolean isGenera_remesa() {
        return genera_remesa;
    }

    //darrieta
    public static ConveniosRemesas load(ResultSet rs)throws SQLException{
        ConveniosRemesas remesa = new ConveniosRemesas();
        remesa.setReg_status(rs.getString("reg_status"));
        remesa.setDstrct(rs.getString("dstrct"));
        remesa.setId_convenio(rs.getString("id_convenio"));
        remesa.setId_remesa(rs.getString("id_remesa"));
        remesa.setCiudad_sede(rs.getString("ciudad_sede"));
        remesa.setBanco_titulo(rs.getString("banco_titulo"));
        remesa.setCiudad_titulo(rs.getString("ciudad_titulo"));
        remesa.setGenera_remesa(rs.getBoolean("genera_remesa"));
        remesa.setPorcentaje_remesa(rs.getDouble("porcentaje_remesa"));
        remesa.setCuenta_remesa(rs.getString("cuenta_remesa"));
        remesa.setCreation_user(rs.getString("creation_user"));
        remesa.setUser_update(rs.getString("user_update"));
        remesa.setCreation_date(rs.getString("creation_date"));
        remesa.setLast_update(rs.getString("last_update"));
        return remesa;
    }
}
