package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla caja_pago
 * 8/02/2012
 * @author darrieta
 */
public class CajaPago {

    private long id;
    private String dstrct;
    private String agencia;
    private String fecha;
    private String transportadora;
    private String tipoDocumento;
    private String numDocumento;
    private String beneficiario;
    private String cedulaBeneficiario;
    private String nombreBeneficiario;
    private String banco;
    private String sucursal;
    private double valor;
    private double comision;
    private double valorComision;
    private double valorEntregar;
    private double comisionBancaria;
    private String creationUser;
    private String numCxc;
    private String tipo; 

    /**
     * Get the value of numCxc
     *
     * @return the value of numCxc
     */
    public String getNumCxc() {
        return numCxc;
    }

    /**
     * Set the value of numCxc
     *
     * @param numCxc new value of numCxc
     */
    public void setNumCxc(String numCxc) {
        this.numCxc = numCxc;
    }


    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }


    /**
     * Get the value of valorEntregar
     *
     * @return the value of valorEntregar
     */
    public double getValorEntregar() {
        return valorEntregar;
    }

    /**
     * Set the value of valorEntregar
     *
     * @param valorEntregar new value of valorEntregar
     */
    public void setValorEntregar(double valorEntregar) {
        this.valorEntregar = valorEntregar;
    }

    /**
     * Get the value of valorComision
     *
     * @return the value of valorComision
     */
    public double getValorComision() {
        return valorComision;
    }

    /**
     * Set the value of valorComision
     *
     * @param valorComision new value of valorComision
     */
    public void setValorComision(double valorComision) {
        this.valorComision = valorComision;
    }

    /**
     * Get the value of comision
     *
     * @return the value of comision
     */
    public double getComision() {
        return comision;
    }

    /**
     * Set the value of comision
     *
     * @param comision new value of comision
     */
    public void setComision(double comision) {
        this.comision = comision;
    }

    /**
     * Get the value of valor
     *
     * @return the value of valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * Set the value of valor
     *
     * @param valor new value of valor
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * Get the value of banco
     *
     * @return the value of banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Set the value of banco
     *
     * @param banco new value of banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * Get the value of nombreBeneficiario
     *
     * @return the value of nombreBeneficiario
     */
    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    /**
     * Set the value of nombreBeneficiario
     *
     * @param nombreBeneficiario new value of nombreBeneficiario
     */
    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    /**
     * Get the value of cedulaBeneficiario
     *
     * @return the value of cedulaBeneficiario
     */
    public String getCedulaBeneficiario() {
        return cedulaBeneficiario;
    }

    /**
     * Set the value of cedulaBeneficiario
     *
     * @param cedulaBeneficiario new value of cedulaBeneficiario
     */
    public void setCedulaBeneficiario(String cedulaBeneficiario) {
        this.cedulaBeneficiario = cedulaBeneficiario;
    }

    /**
     * Get the value of beneficiario
     *
     * @return the value of beneficiario
     */
    public String getBeneficiario() {
        return beneficiario;
    }

    /**
     * Set the value of cedulaBeneficiario
     *
     * @param beneficiario new value of cedulaBeneficiario
     */
    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    /**
     * Get the value of numDocumento
     *
     * @return the value of numDocumento
     */
    public String getNumDocumento() {
        return numDocumento;
    }

    /**
     * Set the value of numDocumento
     *
     * @param numDocumento new value of numDocumento
     */
    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    /**
     * Get the value of tipoDocumento
     *
     * @return the value of tipoDocumento
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Set the value of tipoDocumento
     *
     * @param tipoDocumento new value of tipoDocumento
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     * Get the value of transportadora
     *
     * @return the value of transportadora
     */
    public String getTransportadora() {
        return transportadora;
    }

    /**
     * Set the value of transportadora
     *
     * @param transportadora new value of transportadora
     */
    public void setTransportadora(String transportadora) {
        this.transportadora = transportadora;
    }

    /**
     * Get the value of fecha
     *
     * @return the value of fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param fecha new value of fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Get the value of agencia
     *
     * @return the value of agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Set the value of agencia
     *
     * @param agencia new value of agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Get the value of dstrct
     *
     * @return the value of dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * Set the value of dstrct
     *
     * @param dstrct new value of dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }
    
     /**
     * @return the sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }
    
      public double getComisionBancaria() {
        return comisionBancaria;
    }

    public void setComisionBancaria(double comisionBancaria) {
        this.comisionBancaria = comisionBancaria;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
    
    public static CajaPago load(ResultSet rs) throws SQLException{
        CajaPago cajaPago = new CajaPago();
        
        cajaPago.setAgencia(rs.getString("agencia"));
        cajaPago.setId(rs.getLong("id"));
        cajaPago.setFecha(rs.getString("fecha"));
        cajaPago.setTransportadora(rs.getString("transportadora"));
        cajaPago.setTipoDocumento(rs.getString("tipo_documento"));
        cajaPago.setNumDocumento(rs.getString("num_documento"));
        cajaPago.setBeneficiario(rs.getString("beneficiario"));
        cajaPago.setBanco(rs.getString("banco"));
        cajaPago.setValor(rs.getDouble("valor"));
        cajaPago.setComision(rs.getDouble("comision"));
        cajaPago.setValorComision(rs.getDouble("valor_comision"));
        cajaPago.setComisionBancaria(rs.getDouble("comision_bancaria"));
        
        return cajaPago;
    }

}
