/*************************************************************
 * Nombre      ............... ImportacionEstructura.java
 * Descripcion ............... Clase general de importaciones
 * Autor       ............... mfontalvo
 * Fecha       ............... Octubre - 05 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.operation.model.beans;

import java.sql.*;


/**
 * Definicion de la clase ImportacionEstructura, donde se definen las propiedades
 * de un campo de de una tabla mas las propiedades de importacion que este posee.
 */
public class ImportacionEstructura {
    
    /**
     * Secuancia del campo
     */    
    private String Sec;
    /**
     * Campo de la base de datos
     */    
    private String Campo;
    /**
     * Tipo del campo
     */    
    private String Tipo;
    /**
     * Valor extra del campo
     */    
    private String Extra;
    
    /**
     * Inidica si el campo posee o no un default
     */    
    private String Default;
    /**
     * Tipo de default que posee el campo
     */    
    private String Variable;
    /**
     * Valor default del campo
     */    
    private String Constante;
    
    /**
     * Indica si el campo es llave primaria o no.
     */
    private boolean primaryKey;
    
        
    /**
     * Indica si el campo esta seleccionado o no para un proceso
     */
    private boolean seleccionado;
    
    
    private String alias;
    private String validacion;
    private String insercion;
    private boolean aplicaUpdate = false;

    private String obs_validacion;
    private String obs_insercion;    
        
    /**
     * Crea una nueva instancia de ImportacionEstructura
     */
    public ImportacionEstructura() {
        Sec   = "";
        Campo = "";
        Tipo  = "";
        Extra = "";
        primaryKey = false;
        seleccionado = false;
        obs_validacion  = "";
        obs_insercion   = "";
    }
    
    /**
     * Metodo estatico para setear las propiedades del objeto de acuedo a un resultset
     * @autor mfontalvo
     * @param rs ResultSet que contiene datos de la propiedades del bean
     * @throws SQLException .
     * @return retorna un objeto ImportacionEstructura deacuerdo a los valores pasados
     * por el resultset
     */    
    public static ImportacionEstructura load (ResultSet rs) throws SQLException {
        ImportacionEstructura dt = new ImportacionEstructura();
        dt.setSecuencia   ( rs.getString("sec"  ) );
        dt.setCampo       ( rs.getString("campo") );
        dt.setTipo        ( rs.getString("tipo" ) );
        dt.setExtra       ( rs.getString("extra") );
        
        
        dt.setDefault     ( rs.getString("tiene_default" ) );
        dt.setVariable    ( rs.getString("tipo_default"  ) );
        dt.setConstante   ( rs.getString("valor_default" ) );
        
        dt.setAlias       ( com.tsp.util.UtilFinanzas.parametroValido(rs.getString("alias")) ? rs.getString("alias") : rs.getString("campo")  );
        dt.setValidacion  ( rs.getString("validacion") );
        dt.setInsercion   ( rs.getString("insercion") );
        dt.setAplicaUpdate( ( com.tsp.util.Util.coalesce(rs.getString("aplica_update"), "").equalsIgnoreCase("S")?true:false) );
        
        dt.setObs_validacion  ( rs.getString("obs_validacion") );
        dt.setObs_insercion   ( rs.getString("obs_insercion") );        
        
        
        return dt;
    }
    
    /**
     * Modifica el valor de la propiedad Secuencia.
     * @autor mfontalvo
     * @param newValue Nuevo valor de Secuencia
     */
    
    public void setSecuencia (String newValue){
        Sec   = newValue;
    }
    /**
     * Modifica el valor de la propiedad Campo.
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad Campo
     */    
    public void setCampo (String newValue){
        Campo = newValue;
    }
    /**
     * Modifica el valor de la propiedad Tipo.
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad Tipo
     */    
    public void setTipo  (String newValue){
        Tipo  = newValue;
    }
    /**
     * Modifica el valor de la propiedad Extra.
     * @autor mfontalvo
     * @param newValue Nuevo valor de la propiedad Extra
     */    
    public void setExtra (String newValue){
        Extra = newValue;
    }
    
    /**
     * Obtiene el valor de la propiedad Secuencia.
     * @autor mfontalvo
     * @return Valor de la propiedad Secuencia
     */
    
    public String getSecuencia (){
        return Sec;
    }
    /**
     * Obtiene el valor de la propiedad Campo
     * @autor mfontalvo
     * @return Valor de la propiedad Campo
     */    
    public String getCampo(){
        return Campo;
    }
    /**
     * Obtiene el valor de la propiedad Tipo.
     * @autor mfontalvo
     * @return Valor de la propiedad Tipo
     */    
    public String getTipo(){
        return Tipo;
    }   
    /**
     * Obtiene el valor de la propiedad extra.
     * @autor mfontalvo
     * @return Valor de la propiedad extra
     */    
    public String getExtra(){
        return Extra;
    }
    
    /**
     * Obtiene el valor de la propiedad Default.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @return Valor de la propiedad Default.
     */
    public java.lang.String getDefault() {
        return Default;
    }
    
    /**
     * Modifica el valor de la propiedad Default.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @param Default Nuevo valor de la propiedad Default.
     */
    public void setDefault(java.lang.String Default) {
        this.Default = Default;
    }
    
    /**
     * Obtiene el valor de la propiedad Variable.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @return Valor de la propiedad Variable.
     */
    public java.lang.String getVariable() {
        return Variable;
    }
    
    /**
     * Modifica el valor de la propiedad Variable.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @param Variable Nuevo valor de la propiedad Default.
     */
    public void setVariable(java.lang.String Variable) {
        this.Variable = Variable;
    }
    
    /**
     * Obtiene el valor de la propiedad Constante.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @return Valor de la propiedad Constante.
     */
    public java.lang.String getConstante() {
        return Constante;
    }
    
    /**
     * Modifica el valor de la propiedad Constante.
     * @autor mfontalvo
     * @fecha 2006-01-24
     * @param Constante New value of property Constante.
     */
    public void setConstante(java.lang.String Constante) {
        this.Constante = Constante;
    }
    
    /**
     * Getter for property primaryKey.
     * @return Value of property primaryKey.
     */
    public boolean isPrimaryKey() {
        return primaryKey;
    }
    
    /**
     * Setter for property primaryKey.
     * @param primaryKey New value of property primaryKey.
     */
    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }
    
    /**
     * Getter for property seleccionado.
     * @return Value of property seleccionado.
     */
    public boolean isSeleccionado() {
        return seleccionado;
    }
    
    /**
     * Setter for property seleccionado.
     * @param seleccionado New value of property seleccionado.
     */
    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }
    
    /**
     * Getter for property alias.
     * @return Value of property alias.
     */
    public java.lang.String getAlias() {
        return alias;
    }
    
    /**
     * Setter for property alias.
     * @param alias New value of property alias.
     */
    public void setAlias(java.lang.String alias) {
        this.alias = alias;
    }
    
    /**
     * Getter for property validacion.
     * @return Value of property validacion.
     */
    public java.lang.String getValidacion() {
        return validacion;
    }
    
    /**
     * Setter for property validacion.
     * @param validacion New value of property validacion.
     */
    public void setValidacion(java.lang.String validacion) {
        this.validacion = validacion;
    }
    
    /**
     * Getter for property insercion.
     * @return Value of property insercion.
     */
    public java.lang.String getInsercion() {
        return insercion;
    }
    
    /**
     * Setter for property insercion.
     * @param insercion New value of property insercion.
     */
    public void setInsercion(java.lang.String insercion) {
        this.insercion = insercion;
    }
    
    /**
     * Getter for property aplicaUpdate.
     * @return Value of property aplicaUpdate.
     */
    public boolean isAplicaUpdate() {
        return aplicaUpdate;
    }
    
    /**
     * Setter for property aplicaUpdate.
     * @param aplicaUpdate New value of property aplicaUpdate.
     */
    public void setAplicaUpdate(boolean aplicaUpdate) {
        this.aplicaUpdate = aplicaUpdate;
    }
    
    /**
     * Getter for property obs_validacion.
     * @return Value of property obs_validacion.
     */
    public java.lang.String getObs_validacion() {
        return obs_validacion;
    }
    
    /**
     * Setter for property obs_validacion.
     * @param obs_validacion New value of property obs_validacion.
     */
    public void setObs_validacion(java.lang.String obs_validacion) {
        this.obs_validacion = obs_validacion;
    }
    
    /**
     * Getter for property obs_insercion.
     * @return Value of property obs_insercion.
     */
    public java.lang.String getObs_insercion() {
        return obs_insercion;
    }
    
    /**
     * Setter for property obs_insercion.
     * @param obs_insercion New value of property obs_insercion.
     */
    public void setObs_insercion(java.lang.String obs_insercion) {
        this.obs_insercion = obs_insercion;
    }
    
}
