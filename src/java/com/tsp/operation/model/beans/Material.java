/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Rhonalf
 */
public class Material {

    private String codigo;
    private String descripcion;
    private String tipo;
    private String medida;
    private double valor;
    private double valor_compra;
    private String categoria;//091203.

    private String reg_status,idmaterial,last_update,user_update,aprobacion,idmaterial_asociado,fecha_anulacion,user_anulacion;
    
    public Material(){
        this.codigo="";
        this.descripcion="";
        this.medida="UNIDADES";
        this.tipo="M";
        this.valor=0.0;
        this.valor_compra=0.0;
    }
    public void setCodigo(String cod){
        this.codigo=cod;
    }
    public String getCodigo(){
        return this.codigo;
    }
    
    public void setRegStatus(String x){
        this.reg_status=x;
    }
    public String getRegStatus(){
        return this.reg_status;
    }
    public void setIdMaterial(String x){
        this.idmaterial=x;
    }
    public String getIdMaterial(){
        return this.idmaterial;
    }
    public void setLastUpdate(String x){
        this.last_update=x;
    }
    public String getLastUpdate(){
        return this.last_update;
    }
    public void setUserUpdate(String x){
        this.user_update=x;
    }
    public String getUserUpdate(){
        return this.user_update;
    }
    public void setAprobacion(String x){
        this.aprobacion=x;
    }
    public String getAprobacion(){
        return this.aprobacion;
    }
    public void setIdMaterialAsociado(String x){
        this.idmaterial_asociado=x;
    }
    public String getIdMaterialAsociado(){
        return idmaterial_asociado;
    }
    public void setFechaAnulacion(String x){
        this.fecha_anulacion=x;
    }
    public String getFechaAnulacion(){
        return fecha_anulacion;
    }
    public void setUserAnulacion(String x){
        this.user_anulacion=x;
    }
    public String getUserAnulacion(){
        return user_anulacion;
    }
    

    public void setDescripcion(String desc){
        this.descripcion=desc;
    }

    public String getDescripcion(){
        return this.descripcion;
    }

    public void setTipo(String tip){
        this.tipo=tip;
    }

    public String getTipo(){
        return this.tipo;
    }

    public void setMedida(String med){
        this.medida=med;
    }

    public String getMedida(){
        return this.medida;
    }

    public void setValor(double v){
        this.valor=v;
    }

    public double getValor(){
        return this.valor;
    }

    public void setValorCompra(double vcomp){
        this.valor_compra=vcomp;
    }

    public double getValorCompra(){
        return this.valor_compra;
    }

    public void setCategoria(String cat){ //091203
        this.categoria = cat;
    }

    public String getCategoria(){ //091203
        return categoria;
    }


}
