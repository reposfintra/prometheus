/*
 * Codextraflete.java
 *
 * Created on 11 de julio de 2005, 09:29 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  jbarrios
 */
public class Codextraflete implements Serializable {
    
    private String descripcion;
    private String codextraflete;
    private java.util.Date creation_date;
    private String creation_user;
    private String base;
    private String cuenta;
    private String agencia;
    private String elemento;
    private String ajuste;
    private String unidades;
    //JEscandon 
    private String tipo;
    
    
    /** Creates a new instance of Codextraflete */
     public static Codextraflete load(ResultSet rs)throws SQLException {
     Codextraflete codextra = new Codextraflete();
     codextra.setCodextraflete(rs.getString("codextraflete"));   
     codextra.setDescripcion(rs.getString("descripcion"));
     codextra.setCuenta(rs.getString("cuenta"));
     codextra.setAgencia(rs.getString("agencia"));
     codextra.setElemento(rs.getString("elemento"));
     codextra.setAjuste(rs.getString("ajuste"));
     codextra.setUnidades(rs.getString("unidades"));
     codextra.setTipo(rs.getString("tipo"));
     
     return codextra;    
    }
  
    
    //============================================
    //	Metodos de acceso a propiedades
    //============================================
  
   public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
     public String getCodextraflete() {
        return this.codextraflete;
    }    
    
    public void setCodextraflete(String codextraflete) {
        this.codextraflete = codextraflete;
    }
  
     public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
    
    public String getBase() {
        return this.base;
    }    
  
    public void setBase(String base) {
        this.base = base;
    }
   
    public String getCuenta() {
        return this.cuenta;
    }
    
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public String getAgencia() {
        return this.agencia;
    }
 
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }
 
    public String getElemento() {
        return this.elemento;
    }
 
    public void setElemento(String elemento) {
        this.elemento = elemento;
    }
 
    public String getAjuste() {
        return this.ajuste;
    }
 
    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }
    
    public String getUnidades() {
        return this.unidades;
    }
   
    public void setUnidades(String unidades) {
        this.unidades = unidades;
    }
    
     /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
}
