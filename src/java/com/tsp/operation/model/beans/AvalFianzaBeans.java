/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mcamargo
 */
public class AvalFianzaBeans {
    
    private double porc_dto_fianza;
    private double porc_fin_fianza;

    public AvalFianzaBeans() {
    }

    /**
     * @return the porc_dto_fianza
     */
    public double getPorc_dto_fianza() {
        return porc_dto_fianza;
    }

    /**
     * @param porc_dto_fianza the porc_dto_fianza to set
     */
    public void setPorc_dto_fianza(double porc_dto_fianza) {
        this.porc_dto_fianza = porc_dto_fianza;
    }

    /**
     * @return the porc_fin_fianza
     */
    public double getPorc_fin_fianza() {
        return porc_fin_fianza;
    }

    /**
     * @param porc_fin_fianza the porc_fin_fianza to set
     */
    public void setPorc_fin_fianza(double porc_fin_fianza) {
        this.porc_fin_fianza = porc_fin_fianza;
    }
    
}
