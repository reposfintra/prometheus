/***********************************************************************************
 * Nombre clase : ............... FlotaUtilizada.java                              *
 * Descripcion :................. Clase que maneja los atributos del objeto        *
 *                                Corrida. necesarios para el reporte de extractos * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 01 de enero de 2006, 08:00 AM                    *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.model.beans;

import java.io.Serializable;

public class Run implements Serializable{
    private String acct_dstrct;//
    private String banco;//branch_code
    private String sucursal;//bank_account_no
    private String cheque_run_no;//corrida
    private String rc_proc_step;//
    private String rc_proc_sbstep;
    private String rc_due_date;
    private String msg;
    private double cheq_qty;
    private double cheq_val; 
    private String tipo;
    private String tpago;
    
    
    /** Creates a new instance of Run */
    public Run() {
    }
    
    /**
     * Getter for property acct_dstrct.
     * @return Value of property acct_dstrct.
     */
    public java.lang.String getAcct_dstrct() {
        return acct_dstrct;
    }
    
    /**
     * Setter for property acct_dstrct.
     * @param acct_dstrct New value of property acct_dstrct.
     */
    public void setAcct_dstrct(java.lang.String acct_dstrct) {
        this.acct_dstrct = acct_dstrct;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property cheque_run_no.
     * @return Value of property cheque_run_no.
     */
    public java.lang.String getCheque_run_no() {
        return cheque_run_no;
    }
    
    /**
     * Setter for property cheque_run_no.
     * @param cheque_run_no New value of property cheque_run_no.
     */
    public void setCheque_run_no(java.lang.String cheque_run_no) {
        this.cheque_run_no = cheque_run_no;
    }
    
    /**
     * Getter for property rc_due_date.
     * @return Value of property rc_due_date.
     */
    public java.lang.String getRc_due_date() {
        return rc_due_date;
    }
    
    /**
     * Setter for property rc_due_date.
     * @param rc_due_date New value of property rc_due_date.
     */
    public void setRc_due_date(java.lang.String rc_due_date) {
        this.rc_due_date = rc_due_date;
    }
    
    /**
     * Getter for property rc_proc_sbstep.
     * @return Value of property rc_proc_sbstep.
     */
    public java.lang.String getRc_proc_sbstep() {
        return rc_proc_sbstep;
    }
    
    /**
     * Setter for property rc_proc_sbstep.
     * @param rc_proc_sbstep New value of property rc_proc_sbstep.
     */
    public void setRc_proc_sbstep(java.lang.String rc_proc_sbstep) {
        this.rc_proc_sbstep = rc_proc_sbstep;
    }
    
    /**
     * Getter for property rc_proc_step.
     * @return Value of property rc_proc_step.
     */
    public java.lang.String getRc_proc_step() {
        return rc_proc_step;
    }
    
    /**
     * Setter for property rc_proc_step.
     * @param rc_proc_step New value of property rc_proc_step.
     */
    public void setRc_proc_step(java.lang.String rc_proc_step) {
        this.rc_proc_step = rc_proc_step;
    }
    
    /**
     * Getter for property cheq_qty.
     * @return Value of property cheq_qty.
     */
    public double getCheq_qty() {
        return cheq_qty;
    }
    
    /**
     * Setter for property cheq_qty.
     * @param cheq_qty New value of property cheq_qty.
     */
    public void setCheq_qty(double cheq_qty) {
        this.cheq_qty = cheq_qty;
    }
    
    /**
     * Getter for property cheq_val.
     * @return Value of property cheq_val.
     */
    public double getCheq_val() {
        return cheq_val;
    }
    
    /**
     * Setter for property cheq_val.
     * @param cheq_val New value of property cheq_val.
     */
    public void setCheq_val(double cheq_val) {
        this.cheq_val = cheq_val;
    }
    
    /**
     * Getter for property msg.
     * @return Value of property msg.
     */
    public java.lang.String getMsg() {
        return msg;
    }
    
    /**
     * Setter for property msg.
     * @param msg New value of property msg.
     */
    public void setMsg(java.lang.String msg) {
        this.msg = msg;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property tpago.
     * @return Value of property tpago.
     */
    public java.lang.String getTpago() {
        return tpago;
    }
    
    /**
     * Setter for property tpago.
     * @param tpago New value of property tpago.
     */
    public void setTpago(java.lang.String tpago) {
        this.tpago = tpago;
    }
    
}
