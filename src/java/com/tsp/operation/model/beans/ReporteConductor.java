/*
 * HojaReporte.java
 *
 * Created on 22 de noviembre de 2004, 10:50 AM
 */

package com.tsp.operation.model.beans;
import java.io.Serializable;

/**
 *
 * @author  AMENDEZ
 */
public class ReporteConductor implements Serializable{
    
    private String planilla = "";
    private String agencia = "";
    private String usuario = "";
    private String agcUsuario = "";
    private String ruta = "";
    private String placa_vehiculo = "";
    private String placa_unidad_carga = "";
    private String contenedores = "";
    private String precinto = "";
    private String conductor = "";
    private String comentario = "";
    private String creation_date = "";
    private String creation_user = "";
    private String print_date = ""; 
    private String print_user = "";
    private String cia = "";
    /** Creates a new instance of HojaReporte */
    public ReporteConductor() {
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getPlaca_vehiculo() {
        return placa_vehiculo;
    }

    public void setPlaca_vehiculo(String placa_vehiculo) {
        this.placa_vehiculo = placa_vehiculo;
    }

    public String getPlaca_unidad_carga() {
        return placa_unidad_carga;
    }

    public void setPlaca_unidad_carga(String placa_unidad_carga) {
        this.placa_unidad_carga = placa_unidad_carga;
    }

    public String getContenedores() {
        return contenedores;
    }

    public void setContenedores(String contenedores) {
        this.contenedores = contenedores;
    }

    public String getPrecinto() {
        return precinto;
    }

    public void setPrecinto(String precinto) {
        this.precinto = precinto;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        int i = 0;
        if (conductor != null){
            if (conductor.startsWith("0")){
                while ( conductor.substring(i, (i+1)).equals("0") ){
                    i++;
                }
                this.conductor = conductor.substring(i, conductor.length());
            }
            else
                this.conductor = conductor;
        }
        else
            conductor = ""; 
    }
    
    /**
     * Getter for property comentario.
     * @return Value of property comentario.
     */
    public String getComentario() {
        return comentario;
    }
    
    /**
     * Setter for property comentario.
     * @param comentario New value of property comentario.
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property print_date.
     * @return Value of property print_date.
     */
    public java.lang.String getPrint_date() {
        return print_date;
    }
    
    /**
     * Setter for property print_date.
     * @param print_date New value of property print_date.
     */
    public void setPrint_date(java.lang.String print_date) {
        this.print_date = print_date;
    }
    
    /**
     * Getter for property print_user.
     * @return Value of property print_user.
     */
    public java.lang.String getPrint_user() {
        return print_user;
    }
    
    /**
     * Setter for property print_user.
     * @param print_user New value of property print_user.
     */
    public void setPrint_user(java.lang.String print_user) {
        this.print_user = print_user;
    }
    
    /**
     * Getter for property agcUsuario.
     * @return Value of property agcUsuario.
     */
    public java.lang.String getAgcUsuario() {
        return agcUsuario;
    }
    
    /**
     * Setter for property agcUsuario.
     * @param agcUsuario New value of property agcUsuario.
     */
    public void setAgcUsuario(java.lang.String agcUsuario) {
        this.agcUsuario = agcUsuario;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
}
