/********************************************************************
 *      Nombre Clase.................   ReporteRetroactivo.java
 *      Descripci�n..................   Bean del reporte de Placas y sus fotos
 *      Autor........................   Ing. Leonardo Parodi Ponce
 *      Fecha........................   14.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;
/**
 *
 * @author  EQUIPO12
 */
public class ReportePlacaFotos {
        
        private String agencia;
        private String placa;
        private java.util.Date fechacrea;
        private String usuariocrea;
        private String marca;
        private String capacidad;
        private String modelo;
        /*********************************/
        private String agenciafoto;
        private java.util.Date creation_date;
        private String creation_user;
        private String diferencia;
        private String propietario;
        private String agenciaPropietario;
        private String filename;
        private String existe;
        
        /** Creates a new instance of ReportePlacaFotos */
        public ReportePlacaFotos() {
        }
        
        /**
         * Toma el valor de cada uno de los campos y los asigna.
         * @autor Ing. Leonardo Parodi Ponce
         * @param rs ResultSet de la consulta.
         * @throws SQLException
         * @version 1.1
         */
        
            
        public static ReportePlacaFotos load(ResultSet rs)throws SQLException {
                ReportePlacaFotos reportePlacaFotos = new ReportePlacaFotos();
                reportePlacaFotos.setAgencia( rs.getString("agencia") );
                reportePlacaFotos.setDiferencia(rs.getString("diferencia"));
                reportePlacaFotos.setPlaca( rs.getString("placa") );
                reportePlacaFotos.setFechacrea( rs.getTimestamp("fechacrea") );
                reportePlacaFotos.setUsuariocrea(rs.getString("usuariocrea") );
                reportePlacaFotos.setMarca(rs.getString("marca") );
                reportePlacaFotos.setCapacidad(rs.getString("capacidad") );
                reportePlacaFotos.setModelo(rs.getString("modelo") );
                reportePlacaFotos.setAgenciafoto(rs.getString("agenciafoto"));
                reportePlacaFotos.setCreation_date(rs.getTimestamp("creation_date"));
                reportePlacaFotos.setCreation_user(rs.getString("creation_user"));
                reportePlacaFotos.setPropietario(rs.getString("propietario"));
                reportePlacaFotos.setAgenciaPropietario(rs.getString("agenciapropietario"));
                reportePlacaFotos.setFilename(rs.getString("filename"));
                reportePlacaFotos.setExiste(rs.getString("tiene_foto"));
                
                
                return reportePlacaFotos;
        }
        
        /**
         * Setter for property agencia.
         * @param agencia New value of property agencia.
         */
        public void setAgencia(String Agencia){
                
                this.agencia = Agencia;
                
        }
        /**
         * Setter for property filename.
         * @param filename New value of property filename.
         */
        public void setFilename(String Filename){
                
                this.filename = Filename;
                
        }
        /**
         * Setter for property existe.
         * @param existe New value of property existe.
         */
        public void setExiste(String Existe){
                
                this.existe = Existe;
                
        }
        /**
         * Setter for property placa.
         * @param placa New value of property placa.
         */
        public void setPlaca(String Placa){
                
                this.placa = Placa;
                
        }
        
        /**
         * Setter for property fechacrea.
         * @param fechacrea New value of property fechacrea.
         */
        public void setFechacrea(java.util.Date Fechacrea){
                
                this.fechacrea = Fechacrea;
                
        }
        
        /**
         * Setter for property usuariocrea.
         * @param usuariocrea New value of property usuariocrea.
         */
        public void setUsuariocrea(String Usuariocrea){
                
                this.usuariocrea = Usuariocrea;
                
        }
        
        /**
         * Setter for property marca.
         * @param marca New value of property marca.
         */
        public void setMarca(String Marca){
                
                this.marca = Marca;
                
        }
        
        /**
         * Setter for property capacidad.
         * @param capacidad New value of property capacidad.
         */
        public void setCapacidad(String Capacidad){
                
                this.capacidad = Capacidad;
                
        }
        
        /**
         * Setter for property modelo.
         * @param modelo New value of property modelo.
         */
        public void setModelo(String Modelo){
                
                this.modelo = Modelo;
                
        }
        
        /**
         * Setter for property modelo.
         * @param modelo New value of property modelo.
         */
        public void setAgenciafoto(String Agenciafoto){
                
                this.agenciafoto = Agenciafoto;
                
        }
        
        /**
         * Setter for property creation_date.
         * @param creation_date New value of property creation_date.
         */
        public void setCreation_date(java.util.Date Creation_date){
                
                this.creation_date = Creation_date;
                
        }
        
        /**
         * Setter for property creation_user.
         * @param creation_user New value of property creation_user.
         */
        public void setCreation_user(String Creation_user){
                
                this.creation_user = Creation_user;
                
        }
        
        /**
         * Setter for property propietario.
         * @param propietario New value of property propietario.
         */
        public void setPropietario(String Propietario){
                
                this.propietario = Propietario;
                
        }
        
        /**
         * Setter for property agenciaPropietario.
         * @param agenciaPropietario New value of property agenciaPropietario.
         */
        public void setAgenciaPropietario(String AgenciaPropietario){
                
                this.agenciaPropietario = AgenciaPropietario;
                
        }
        
        /**
         * Setter for property diferencia.
         * @param diferencia New value of property diferencia.
         */
        public void setDiferencia(String Diferencia){
                
                this.diferencia = Diferencia;
                
        }
        
        
        /**
         * Getter for property agencia.
         * @return value of property agencia.
         */
        public String getAgencia(){
                
                return this.agencia;
                
        }
        
        /**
         * Getter for property placa.
         * @return value of property placa.
         */
        public String getPlaca(){
                
                return this.placa;
                
        }
        /**
         * Getter for property filename.
         * @return value of property filename.
         */
        public String getFilename(){
                
                return this.filename;
                
        }
        
        /**
         * Getter for property fechacrea.
         * @return value of property fechacrea.
         */
        public java.util.Date getFechacrea(){
                
                return this.fechacrea;
                
        }
        
        /**
         * Getter for property usuariocrea.
         * @return value of property usuariocrea.
         */
        public String getUsuariocrea(){
                
               return this.usuariocrea;
                
        }
        
         /**
         * Getter for property marca.
         * @return value of property marca.
         */
        public String getMarca(){
                
                return this.marca;
                
        }
        
        /**
         * Getter for property capacidad.
         * @return value of property capacidad.
         */
        public String getCapacidad(){
                
                return this.capacidad;
                
        }
        
        /**
         * Getter for property modelo.
         * @return value of property modelo.
         */
        public String getModelo(){
                
                return this.modelo;
                
        }
        
        /**
         * Getter for property agenciafoto.
         * @return value of property agenciafoto.
         */
        public String getAgenciafoto(){
                
                 return this.agenciafoto;
                
        }
        
       /**
         * Getter for property creation_date.
         * @return value of property creation_date.
         */
        public java.util.Date getCreation_date(){
                
                return this.creation_date;
                
        }
        
       /**
         * Getter for property creation_user.
         * @return value of property creation_user.
         */
        public String getCreation_user(){
                
               return this.creation_user;
                
        }
        
        /**
         * Getter for property propietario.
         * @return value of property propietario.
         */
        public String getPropietario(){
                
                return this.propietario;
                
        }
        
        /**
         * Getter for property agenciaPropietario.
         * @return value of property agenciaPropietario.
         */
        public String getAgenciaPropietario(){
                
                return this.agenciaPropietario;
                
        }
        
        /**
         * Getter for property diferencia.
         * @return value of property diferencia.
         */
        public String getDiferencia(){
                
                return this.diferencia;
                
        }
        
        /**
         * Getter for property existe.
         * @return value of property existe.
         */
        public String getExiste(){
                
                return this.existe;
                
        }
        
}
