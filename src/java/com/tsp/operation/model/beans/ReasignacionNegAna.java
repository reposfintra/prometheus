/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author Roberto Parra
 */
public class ReasignacionNegAna {


   private String analista;
   private String nombre;
   private String negocio;
   private String estado_negocio;
   private String cant_cred_asig;//cantidad de creditos asignados al analista
   private String cant_max_cred;
   private String valor;
   private String id_cliente;
   private String cliente;
   private String unidad;
   private String monto_minimo;
   private String monto_maximo;
   private String id_perfil;//Perfil del analista a reasignar
   private String perfil;//Perfil del analista a reasignar
   private String fecha_asignacion;
   private String analista_a_reasignar;
 

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
        /**
     * @return the estado_negocio
     */
    public String getEstado_negocio() {
        return estado_negocio;
    }

    /**
     * @param estado_negocio the estado_negocio to set
     */
    public void setEstado_negocio(String estado_negocio) {
        this.estado_negocio = estado_negocio;
    }

    /**
     * @return the cant_cred_asig
     */
    public String getCant_cred_asig() {
        return cant_cred_asig;
    }

    /**
     * @param cant_cred_asig the cant_cred_asig to set
     */
    public void setCant_cred_asig(String cant_cred_asig) {
        this.cant_cred_asig = cant_cred_asig;
    }
    
    
    /**
     * @return the cant_max_cred
     */
    public String getCant_max_cred() {
        return cant_max_cred;
    }

    /**
     * @param cant_max_cred the cant_max_cred to set
     */
    public void setCant_max_cred(String cant_max_cred) {
        this.cant_max_cred = cant_max_cred;
    }


    /**
     * @return the analista
     */
    public String getAnalista() {
        return analista;
    }

    /**
     * @param analista the analista to set
     */
    public void setAnalista(String analista) {
        this.analista = analista;
    }
    
        /**
     * @return the valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the unidad
     */
    public String getUnidad() {
        return unidad;
    }

    /**
     * @param unidad the unidad to set
     */
    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

        /**
     * @return the id_perfil
     */
    public String getId_perfil() {
        return id_perfil;
    }

    /**
     * @param id_perfil the id_perfil to set
     */
    public void setId_perfil(String id_perfil) {
        this.id_perfil = id_perfil;
    }

    /**
     * @return the perfil
     */
    public String getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
    /**
     * @return the fecha_asignacion
     */
    public String getFecha_asignacion() {
        return fecha_asignacion;
    }

    /**
     * @param fecha_asignacion the fecha_asignacion to set
     */
    public void setFecha_asignacion(String fecha_asignacion) {
        this.fecha_asignacion = fecha_asignacion;
    }

    /**
     * @return the monto_minimo
     */
    public String getMonto_minimo() {
        return monto_minimo;
    }

    /**
     * @param monto_minimo the monto_minimo to set
     */
    public void setMonto_minimo(String monto_minimo) {
        this.monto_minimo = monto_minimo;
    }

    /**
     * @return the monto_maximo
     */
    public String getMonto_maximo() {
        return monto_maximo;
    }

    /**
     * @param monto_maximo the monto_maximo to set
     */
    public void setMonto_maximo(String monto_maximo) {
        this.monto_maximo = monto_maximo;
    }
        /**
     * @return the analista_a_reasignar
     */
    public String getAnalista_a_reasignar() {
        return analista_a_reasignar;
    }

    /**
     * @param analista_a_reasignar the analista_a_reasignar to set
     */
    public void setAnalista_a_reasignar(String analista_a_reasignar) {
        this.analista_a_reasignar = analista_a_reasignar;
    }


}

