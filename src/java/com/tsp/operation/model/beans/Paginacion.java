/*
 * Paginacion.java
 *
 * Created on 14 de febrero de 2005, 12:39 AM
 */

package com.tsp.operation.model.beans;
import java.util.*;
import java.math.*;
import java.sql.*;
/**
 *
 * @author  desconocido
 */
public class Paginacion {
    private String    Accion;
    public  int       Cantidad_Filas   = 0;
    public  int       Cantidad_Indices = 0;
    private int       Cantidad_Vista   = 0;
    private int       Vista_Actual     = 1;        
    private List      Lista            = new LinkedList();    
    private ResultSet rs               = null;
    private boolean   Tipo             = false;
    private String    url;             
    private Vector    list;
    public void Movimiento(String Opcion) {
        if(Opcion.equals("Fin"))    Vista_Actual = Cantidad_Vista;
        if(Opcion.equals("Ant"))    Vista_Actual = ((getInicio()-2)*Cantidad_Indices+1)<1?Vista_Actual:((getInicio()-2)*Cantidad_Indices+1);
        if(Opcion.equals("Sig"))    Vista_Actual = (getInicio()*Cantidad_Indices+1)>Cantidad_Vista?Vista_Actual:(getInicio()*Cantidad_Indices+1);
        if(Opcion.equals("Inicio")) Vista_Actual = 1;
    }
        
    public void setConfiguracion(String Accion, int Cantidad_Indices, int Cantidad_Filas){
        this.Vista_Actual     = 1;
        this.Accion           = Accion;
        this.Cantidad_Indices = Cantidad_Indices;
        this.Cantidad_Filas   = Cantidad_Filas;
    }
    
    public void setLista(List valor){
        this.Lista = valor;
        this.Tipo  = true;
        BigDecimal b1 = new BigDecimal(Lista.size());
        BigDecimal b2 = new BigDecimal(Cantidad_Filas);
        this.Cantidad_Vista = b1.divide(b2,b1.ROUND_UP).intValue();
    }
    
    public void setLista(ResultSet valor) throws Exception{
        this.rs   = valor;
        this.Tipo = false;
        this.rs.last();
        BigDecimal b1 = new BigDecimal(rs.getRow());
        BigDecimal b2 = new BigDecimal(Cantidad_Filas);
        this.Cantidad_Vista = b1.divide(b2,b1.ROUND_UP).intValue();
    }
    
    //Getter
    public List getListado() throws Exception{
        List SubList = new LinkedList();
        if(this.Tipo){
            if(Lista!=null && Lista.size()>0)
                SubList = Lista.subList((Vista_Actual-1)*Cantidad_Filas, (Lista.size()>=(Vista_Actual-1)*Cantidad_Filas+Cantidad_Filas)?((Vista_Actual-1)*Cantidad_Filas+Cantidad_Filas):Lista.size());
        }
        else{
            int Cont = 1;   
            if(((Vista_Actual-1)*Cantidad_Filas)==0) rs.beforeFirst();
            else                                     rs.absolute((Vista_Actual-1)*Cantidad_Filas);
            ResultSetMetaData Md = rs.getMetaData();
            while(rs.next()){
                List Datos = new LinkedList();
                for(int i=1;i<=Md.getColumnCount();i++)
                    Datos.add(rs.getString(i));
                SubList.add(Datos);
                if((Cont++)==this.Cantidad_Filas)
                    break;
            }
        }
        return SubList;
    }
    
    public String getAccion(){        
        return "?estado="+this.Accion;
    }
    
    public Paginacion getPaginacion() {
        return this;
    }
    
    public int getInicio() {
        BigDecimal b1 = new BigDecimal(Vista_Actual);
        BigDecimal b2 = new BigDecimal(Cantidad_Indices);
        return b1.divide(b2,b1.ROUND_UP).intValue();
    }
    
    public int getCantidad_Vista() {
        return this.Cantidad_Vista;
    }
    
    public int getCantidad_Indices() {
        return this.Cantidad_Indices;
    }
    
    public int getVista_Actual(){
        return this.Vista_Actual;
    }
    
    public int getCantidad_Filas(){
        return this.Cantidad_Filas;
    }
    //Setter       
    public void setVista_Actual(int valor){
        this.Vista_Actual = valor;        
    }
    
    /**
     * Getter for property url.
     * @return Value of property url.
     */
    public java.lang.String getUrl() {
        return url;
    }
    
    /**
     * Setter for property url.
     * @param url New value of property url.
     */
    public void setUrl(java.lang.String url) {
        this.url = url;
    }
    
    /**
     * Getter for property list.
     * @return Value of property list.
     */
    public java.util.Vector getList() {
        return list;
    }
    
    /**
     * Setter for property list.
     * @param list New value of property list.
     */
    public void setList(java.util.Vector list) {
        this.list = list;
    }
    
}
