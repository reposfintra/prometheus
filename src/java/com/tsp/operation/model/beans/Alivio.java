package com.tsp.operation.model.beans;

/**
 * Bean para la tabla de alivios<br/>
 * 31/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Alivio {

    private String tipoPadre;
    private String idPadre;
    private String estado;
    private String mes;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of mes
     *
     * @return the value of mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * Set the value of mes
     *
     * @param mes new value of mes
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * Get the value of estado
     *
     * @return the value of estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Set the value of estado
     *
     * @param estado new value of estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Get the value of idPadre
     *
     * @return the value of idPadre
     */
    public String getIdPadre() {
        return idPadre;
    }

    /**
     * Set the value of idPadre
     *
     * @param idPadre new value of idPadre
     */
    public void setIdPadre(String idPadre) {
        this.idPadre = idPadre;
    }

    /**
     * Get the value of tipoPadre
     *
     * @return the value of tipoPadre
     */
    public String getTipoPadre() {
        return tipoPadre;
    }

    /**
     * Set the value of tipoPadre
     *
     * @param tipoPadre new value of tipoPadre
     */
    public void setTipoPadre(String tipoPadre) {
        this.tipoPadre = tipoPadre;
    }


}
