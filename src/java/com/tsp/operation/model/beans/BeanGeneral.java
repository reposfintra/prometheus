/*
 * Nombre        BeanGeneral.java
 * Autor         LREALES
 * Fecha         27 de septiembre de 2006, 10:42 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.io.Serializable;

public class BeanGeneral implements Serializable {
    
    private String reg_status;
    private String dstrct;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String base;
    
    private String valor_01;
    private String valor_02;
    private String valor_03;
    private String valor_04;
    private String valor_05;
    private String valor_06;
    private String valor_07;
    private String valor_08;
    private String valor_09;
    private String valor_10;
    
    private String valor_11;
    private String valor_12;
    private String valor_13;
    private String valor_14;
    private String valor_15;
    private String valor_16;
    private String valor_17;
    private String valor_18;
    private String valor_19;
    private String valor_20;
    
    private String valor_21;
    private String valor_22;
    private String valor_23;
    private String valor_24;
    private String valor_25;
    private String valor_26;
    private String valor_27;
    private String valor_28;
    private String valor_29;
    private String valor_30;
    
    private String valor_31;
    private String valor_32;
    private String valor_33;
    private String valor_34;
    private String valor_35;
    private String valor_36;
    private String valor_37;
    private String valor_38;
    private String valor_39;
    private String valor_40;
    private String valor_41;
    private String valor_42;
    private String valor_43;
    private String valor_44;
    private String valor_45;
    private String valor_46;
    //by rarp
    private float valor_47;
    private float valor_48;
    private float valor_49;
    
    private String valor_50;
    private String valor_51;
    private String valor_52;
    private String valor_53;
    private String valor_54;
    private String valor_55;
    
    // end by rarp
    private Vector vec;
    private String[] v1;
    private String[] v2;
    private String[] v4;
    
    private String fecha_anulacion;
    private String usuario_anulo;
    
    private String ots;
    
    private double[][] v3;
    
    private double[] arr_1;
    private double[] arr_2;

    private String[] v5;/*javier en 20080610*/
    
    /**
     * Getter for property ots.
     * @return Value of property ots.
     */
    public java.lang.String getOts() {
        return ots;
    }
    
    /**
     * Setter for property ots.
     * @param ots New value of property ots.
     */
    public void setOts(java.lang.String ots) {
        this.ots = ots;
    }

    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }    
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property valor_01.
     * @return Value of property valor_01.
     */
    public java.lang.String getValor_01() {
        return valor_01;
    }
    
    /**
     * Setter for property valor_01.
     * @param valor_01 New value of property valor_01.
     */
    public void setValor_01(java.lang.String valor_01) {
        this.valor_01 = valor_01;
    }
    
    /**
     * Getter for property valor_02.
     * @return Value of property valor_02.
     */
    public java.lang.String getValor_02() {
        return valor_02;
    }
    
    /**
     * Setter for property valor_02.
     * @param valor_02 New value of property valor_02.
     */
    public void setValor_02(java.lang.String valor_02) {
        this.valor_02 = valor_02;
    }
    
    /**
     * Getter for property valor_03.
     * @return Value of property valor_03.
     */
    public java.lang.String getValor_03() {
        return valor_03;
    }
    
    /**
     * Setter for property valor_03.
     * @param valor_03 New value of property valor_03.
     */
    public void setValor_03(java.lang.String valor_03) {
        this.valor_03 = valor_03;
    }
    
    /**
     * Getter for property valor_04.
     * @return Value of property valor_04.
     */
    public java.lang.String getValor_04() {
        return valor_04;
    }
    
    /**
     * Setter for property valor_04.
     * @param valor_04 New value of property valor_04.
     */
    public void setValor_04(java.lang.String valor_04) {
        this.valor_04 = valor_04;
    }
    
    /**
     * Getter for property valor_05.
     * @return Value of property valor_05.
     */
    public java.lang.String getValor_05() {
        return valor_05;
    }
    
    /**
     * Setter for property valor_05.
     * @param valor_05 New value of property valor_05.
     */
    public void setValor_05(java.lang.String valor_05) {
        this.valor_05 = valor_05;
    }
    
    /**
     * Getter for property valor_06.
     * @return Value of property valor_06.
     */
    public java.lang.String getValor_06() {
        return valor_06;
    }
    
    /**
     * Setter for property valor_06.
     * @param valor_06 New value of property valor_06.
     */
    public void setValor_06(java.lang.String valor_06) {
        this.valor_06 = valor_06;
    }
    
    /**
     * Getter for property valor_07.
     * @return Value of property valor_07.
     */
    public java.lang.String getValor_07() {
        return valor_07;
    }
    
    /**
     * Setter for property valor_07.
     * @param valor_07 New value of property valor_07.
     */
    public void setValor_07(java.lang.String valor_07) {
        this.valor_07 = valor_07;
    }
    
    /**
     * Getter for property valor_08.
     * @return Value of property valor_08.
     */
    public java.lang.String getValor_08() {
        return valor_08;
    }
    
    /**
     * Setter for property valor_08.
     * @param valor_08 New value of property valor_08.
     */
    public void setValor_08(java.lang.String valor_08) {
        this.valor_08 = valor_08;
    }
    
    /**
     * Getter for property valor_09.
     * @return Value of property valor_09.
     */
    public java.lang.String getValor_09() {
        return valor_09;
    }
    
    /**
     * Setter for property valor_09.
     * @param valor_09 New value of property valor_09.
     */
    public void setValor_09(java.lang.String valor_09) {
        this.valor_09 = valor_09;
    }
    
    /**
     * Getter for property valor_10.
     * @return Value of property valor_10.
     */
    public java.lang.String getValor_10() {
        return valor_10;
    }
    
    /**
     * Setter for property valor_10.
     * @param valor_10 New value of property valor_10.
     */
    public void setValor_10(java.lang.String valor_10) {
        this.valor_10 = valor_10;
    }
    
    /**
     * Getter for property valor_11.
     * @return Value of property valor_11.
     */
    public java.lang.String getValor_11() {
        return valor_11;
    }
    
    /**
     * Setter for property valor_11.
     * @param valor_11 New value of property valor_11.
     */
    public void setValor_11(java.lang.String valor_11) {
        this.valor_11 = valor_11;
    }
    
    /**
     * Getter for property valor_12.
     * @return Value of property valor_12.
     */
    public java.lang.String getValor_12() {
        return valor_12;
    }
    
    /**
     * Setter for property valor_12.
     * @param valor_12 New value of property valor_12.
     */
    public void setValor_12(java.lang.String valor_12) {
        this.valor_12 = valor_12;
    }
    
    /**
     * Getter for property valor_13.
     * @return Value of property valor_13.
     */
    public java.lang.String getValor_13() {
        return valor_13;
    }
    
    /**
     * Setter for property valor_13.
     * @param valor_13 New value of property valor_13.
     */
    public void setValor_13(java.lang.String valor_13) {
        this.valor_13 = valor_13;
    }
    
    /**
     * Getter for property valor_14.
     * @return Value of property valor_14.
     */
    public java.lang.String getValor_14() {
        return valor_14;
    }
    
    /**
     * Setter for property valor_14.
     * @param valor_14 New value of property valor_14.
     */
    public void setValor_14(java.lang.String valor_14) {
        this.valor_14 = valor_14;
    }
    
    /**
     * Getter for property valor_15.
     * @return Value of property valor_15.
     */
    public java.lang.String getValor_15() {
        return valor_15;
    }
    
    /**
     * Setter for property valor_15.
     * @param valor_15 New value of property valor_15.
     */
    public void setValor_15(java.lang.String valor_15) {
        this.valor_15 = valor_15;
    }
    
    /**
     * Getter for property valor_16.
     * @return Value of property valor_16.
     */
    public java.lang.String getValor_16() {
        return valor_16;
    }
    
    /**
     * Setter for property valor_16.
     * @param valor_16 New value of property valor_16.
     */
    public void setValor_16(java.lang.String valor_16) {
        this.valor_16 = valor_16;
    }
    
    /**
     * Getter for property valor_17.
     * @return Value of property valor_17.
     */
    public java.lang.String getValor_17() {
        return valor_17;
    }
    
    /**
     * Setter for property valor_17.
     * @param valor_17 New value of property valor_17.
     */
    public void setValor_17(java.lang.String valor_17) {
        this.valor_17 = valor_17;
    }
    
    /**
     * Getter for property valor_18.
     * @return Value of property valor_18.
     */
    public java.lang.String getValor_18() {
        return valor_18;
    }
    
    /**
     * Setter for property valor_18.
     * @param valor_18 New value of property valor_18.
     */
    public void setValor_18(java.lang.String valor_18) {
        this.valor_18 = valor_18;
    }
    
    /**
     * Getter for property valor_19.
     * @return Value of property valor_19.
     */
    public java.lang.String getValor_19() {
        return valor_19;
    }
    
    /**
     * Setter for property valor_19.
     * @param valor_19 New value of property valor_19.
     */
    public void setValor_19(java.lang.String valor_19) {
        this.valor_19 = valor_19;
    }
    
    /**
     * Getter for property valor_20.
     * @return Value of property valor_20.
     */
    public java.lang.String getValor_20() {
        return valor_20;
    }
    
    /**
     * Setter for property valor_20.
     * @param valor_20 New value of property valor_20.
     */
    public void setValor_20(java.lang.String valor_20) {
        this.valor_20 = valor_20;
    }
    
    /**
     * Getter for property valor_21.
     * @return Value of property valor_21.
     */
    public java.lang.String getValor_21() {
        return valor_21;
    }
    
    /**
     * Setter for property valor_21.
     * @param valor_21 New value of property valor_21.
     */
    public void setValor_21(java.lang.String valor_21) {
        this.valor_21 = valor_21;
    }
    
    /**
     * Getter for property valor_22.
     * @return Value of property valor_22.
     */
    public java.lang.String getValor_22() {
        return valor_22;
    }
    
    /**
     * Setter for property valor_22.
     * @param valor_22 New value of property valor_22.
     */
    public void setValor_22(java.lang.String valor_22) {
        this.valor_22 = valor_22;
    }
    
    /**
     * Getter for property valor_23.
     * @return Value of property valor_23.
     */
    public java.lang.String getValor_23() {
        return valor_23;
    }
    
    /**
     * Setter for property valor_23.
     * @param valor_23 New value of property valor_23.
     */
    public void setValor_23(java.lang.String valor_23) {
        this.valor_23 = valor_23;
    }
    
    /**
     * Getter for property valor_24.
     * @return Value of property valor_24.
     */
    public java.lang.String getValor_24() {
        return valor_24;
    }
    
    /**
     * Setter for property valor_24.
     * @param valor_24 New value of property valor_24.
     */
    public void setValor_24(java.lang.String valor_24) {
        this.valor_24 = valor_24;
    }
    
    /**
     * Getter for property valor_25.
     * @return Value of property valor_25.
     */
    public java.lang.String getValor_25() {
        return valor_25;
    }
    
    /**
     * Setter for property valor_25.
     * @param valor_25 New value of property valor_25.
     */
    public void setValor_25(java.lang.String valor_25) {
        this.valor_25 = valor_25;
    }
    
    /**
     * Getter for property valor_26.
     * @return Value of property valor_26.
     */
    public java.lang.String getValor_26() {
        return valor_26;
    }
    
    /**
     * Setter for property valor_26.
     * @param valor_26 New value of property valor_26.
     */
    public void setValor_26(java.lang.String valor_26) {
        this.valor_26 = valor_26;
    }
    
    /**
     * Getter for property valor_27.
     * @return Value of property valor_27.
     */
    public java.lang.String getValor_27() {
        return valor_27;
    }
    
    /**
     * Setter for property valor_27.
     * @param valor_27 New value of property valor_27.
     */
    public void setValor_27(java.lang.String valor_27) {
        this.valor_27 = valor_27;
    }
    
    /**
     * Getter for property valor_28.
     * @return Value of property valor_28.
     */
    public java.lang.String getValor_28() {
        return valor_28;
    }
    
    /**
     * Setter for property valor_28.
     * @param valor_28 New value of property valor_28.
     */
    public void setValor_28(java.lang.String valor_28) {
        this.valor_28 = valor_28;
    }
    
    /**
     * Getter for property valor_29.
     * @return Value of property valor_29.
     */
    public java.lang.String getValor_29() {
        return valor_29;
    }
    
    /**
     * Setter for property valor_29.
     * @param valor_29 New value of property valor_29.
     */
    public void setValor_29(java.lang.String valor_29) {
        this.valor_29 = valor_29;
    }
    
    /**
     * Getter for property valor_30.
     * @return Value of property valor_30.
     */
    public java.lang.String getValor_30() {
        return valor_30;
    }
    
    /**
     * Setter for property valor_30.
     * @param valor_30 New value of property valor_30.
     */
    public void setValor_30(java.lang.String valor_30) {
        this.valor_30 = valor_30;
    }
    
    /**
     * Getter for property vec.
     * @return Value of property vec.
     */
    public java.util.Vector getVec() {
        return vec;
    }
    
    /**
     * Setter for property vec.
     * @param vec New value of property vec.
     */
    public void setVec(java.util.Vector vec) {
        this.vec = vec;
    }
    
    /**
     * Getter for property valor_31.
     * @return Value of property valor_31.
     */
    public java.lang.String getValor_31() {
        return valor_31;
    }
    
    /**
     * Setter for property valor_31.
     * @param valor_31 New value of property valor_31.
     */
    public void setValor_31(java.lang.String valor_31) {
        this.valor_31 = valor_31;
    }
    
    /**
     * Getter for property valor_32.
     * @return Value of property valor_32.
     */
    public java.lang.String getValor_32() {
        return valor_32;
    }
    
    /**
     * Setter for property valor_32.
     * @param valor_32 New value of property valor_32.
     */
    public void setValor_32(java.lang.String valor_32) {
        this.valor_32 = valor_32;
    }
    
    /**
     * Getter for property valor_33.
     * @return Value of property valor_33.
     */
    public java.lang.String getValor_33() {
        return valor_33;
    }
    
    /**
     * Setter for property valor_33.
     * @param valor_33 New value of property valor_33.
     */
    public void setValor_33(java.lang.String valor_33) {
        this.valor_33 = valor_33;
    }
    
    /**
     * Getter for property valor_34.
     * @return Value of property valor_34.
     */
    public java.lang.String getValor_34() {
        return valor_34;
    }
    
    /**
     * Setter for property valor_34.
     * @param valor_34 New value of property valor_34.
     */
    public void setValor_34(java.lang.String valor_34) {
        this.valor_34 = valor_34;
    }
    
    /**
     * Getter for property valor_35.
     * @return Value of property valor_35.
     */
    public java.lang.String getValor_35() {
        return valor_35;
    }
    
    /**
     * Setter for property valor_35.
     * @param valor_35 New value of property valor_35.
     */
    public void setValor_35(java.lang.String valor_35) {
        this.valor_35 = valor_35;
    }
    
    /**
     * Getter for property valor_36.
     * @return Value of property valor_36.
     */
    public java.lang.String getValor_36() {
        return valor_36;
    }
    
    /**
     * Setter for property valor_36.
     * @param valor_36 New value of property valor_36.
     */
    public void setValor_36(java.lang.String valor_36) {
        this.valor_36 = valor_36;
    }
    
    /**
     * Getter for property valor_37.
     * @return Value of property valor_37.
     */
    public java.lang.String getValor_37() {
        return valor_37;
    }
    
    /**
     * Setter for property valor_37.
     * @param valor_37 New value of property valor_37.
     */
    public void setValor_37(java.lang.String valor_37) {
        this.valor_37 = valor_37;
    }
    
    /**
     * Getter for property valor_38.
     * @return Value of property valor_38.
     */
    public java.lang.String getValor_38() {
        return valor_38;
    }
    
    /**
     * Setter for property valor_38.
     * @param valor_38 New value of property valor_38.
     */
    public void setValor_38(java.lang.String valor_38) {
        this.valor_38 = valor_38;
    }
    
    /**
     * Getter for property valor_39.
     * @return Value of property valor_39.
     */
    public java.lang.String getValor_39() {
        return valor_39;
    }
    
    /**
     * Setter for property valor_39.
     * @param valor_39 New value of property valor_39.
     */
    public void setValor_39(java.lang.String valor_39) {
        this.valor_39 = valor_39;
    }
    
    /**
     * Getter for property valor_40.
     * @return Value of property valor_40.
     */
    public java.lang.String getValor_40() {
        return valor_40;
    }
    
    /**
     * Setter for property valor_40.
     * @param valor_40 New value of property valor_40.
     */
    public void setValor_40(java.lang.String valor_40) {
        this.valor_40 = valor_40;
    }
    
    /**
     * Getter for property valor_41.
     * @return Value of property valor_41.
     */
    public java.lang.String getValor_41() {
        return valor_41;
    }
    
    /**
     * Setter for property valor_41.
     * @param valor_41 New value of property valor_41.
     */
    public void setValor_41(java.lang.String valor_41) {
        this.valor_41 = valor_41;
    }
    
    /**
     * Getter for property valor_42.
     * @return Value of property valor_42.
     */
    public java.lang.String getValor_42() {
        return valor_42;
    }
    
    /**
     * Setter for property valor_42.
     * @param valor_42 New value of property valor_42.
     */
    public void setValor_42(java.lang.String valor_42) {
        this.valor_42 = valor_42;
    }
    
    /**
     * Getter for property valor_43.
     * @return Value of property valor_43.
     */
    public java.lang.String getValor_43() {
        return valor_43;
    }
    
    /**
     * Setter for property valor_43.
     * @param valor_43 New value of property valor_43.
     */
    public void setValor_43(java.lang.String valor_43) {
        this.valor_43 = valor_43;
    }
    
    /**
     * Getter for property valor_44.
     * @return Value of property valor_44.
     */
    public java.lang.String getValor_44() {
        return valor_44;
    }
    
    /**
     * Setter for property valor_44.
     * @param valor_44 New value of property valor_44.
     */
    public void setValor_44(java.lang.String valor_44) {
        this.valor_44 = valor_44;
    }
    
    /**
     * Getter for property valor_45.
     * @return Value of property valor_45.
     */
    public java.lang.String getValor_45() {
        return valor_45;
    }
    
    /**
     * Setter for property valor_45.
     * @param valor_45 New value of property valor_45.
     */
    public void setValor_45(java.lang.String valor_45) {
        this.valor_45 = valor_45;
    }
    
    /**
     * Getter for property valor_46.
     * @return Value of property valor_46.
     */
    public java.lang.String getValor_46() {
        return valor_46;
    }
    
    /**
     * Setter for property valor_46.
     * @param valor_46 New value of property valor_46.
     */
    public void setValor_46(java.lang.String valor_46) {
        this.valor_46 = valor_46;
    }
    
    /**
     * Getter for property fecha_anulacion.
     * @return Value of property fecha_anulacion.
     */
    public java.lang.String getFecha_anulacion() {
        return fecha_anulacion;
    }
    
    /**
     * Setter for property fecha_anulacion.
     * @param fecha_anulacion New value of property fecha_anulacion.
     */
    public void setFecha_anulacion(java.lang.String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }
    
    /**
     * Getter for property usuario_anulo.
     * @return Value of property usuario_anulo.
     */
    public java.lang.String getUsuario_anulo() {
        return usuario_anulo;
    }
    
    /**
     * Setter for property usuario_anulo.
     * @param usuario_anulo New value of property usuario_anulo.
     */
    public void setUsuario_anulo(java.lang.String usuario_anulo) {
        this.usuario_anulo = usuario_anulo;
    }
    
    /**
     * Getter for property valor_47.
     * @return Value of property valor_47.
     */
    public float getValor_47() {
        return valor_47;
    }
    
    /**
     * Setter for property valor_47.
     * @param valor_47 New value of property valor_47.
     */
    public void setValor_47(float valor_47) {
        this.valor_47 = valor_47;
    }
    
    /**
     * Getter for property valor_48.
     * @return Value of property valor_48.
     */
    public float getValor_48() {
        return valor_48;
    }
    
    /**
     * Setter for property valor_48.
     * @param valor_48 New value of property valor_48.
     */
    public void setValor_48(float valor_48) {
        this.valor_48 = valor_48;
    }
    
    /**
     * Getter for property valor_49.
     * @return Value of property valor_49.
     */
    public float getValor_49() {
        return valor_49;
    }
    
    /**
     * Setter for property valor_49.
     * @param valor_49 New value of property valor_49.
     */
    public void setValor_49(float valor_49) {
        this.valor_49 = valor_49;
    }
    
    /**
     * Getter for property valor_50.
     * @return Value of property valor_50.
     */
    public java.lang.String getValor_50() {
        return valor_50;
    }
    
    /**
     * Setter for property valor_50.
     * @param valor_50 New value of property valor_50.
     */
    public void setValor_50(java.lang.String valor_50) {
        this.valor_50 = valor_50;
    }
    
    /**
     * Getter for property valor_51.
     * @return Value of property valor_51.
     */
    public java.lang.String getValor_51() {
        return valor_51;
    }
    
    /**
     * Setter for property valor_51.
     * @param valor_51 New value of property valor_51.
     */
    public void setValor_51(java.lang.String valor_51) {
        this.valor_51 = valor_51;
    }
    
    /**
     * Getter for property valor_52.
     * @return Value of property valor_52.
     */
    public java.lang.String getValor_52() {
        return valor_52;
    }
    
    /**
     * Setter for property valor_52.
     * @param valor_52 New value of property valor_52.
     */
    public void setValor_52(java.lang.String valor_52) {
        this.valor_52 = valor_52;
    }
    
    /**
     * Getter for property valor_53.
     * @return Value of property valor_53.
     */
    public java.lang.String getValor_53() {
        return valor_53;
    }
    
    /**
     * Setter for property valor_53.
     * @param valor_53 New value of property valor_53.
     */
    public void setValor_53(java.lang.String valor_53) {
        this.valor_53 = valor_53;
    }
    
    /**
     * Getter for property valor_54.
     * @return Value of property valor_54.
     */
    public java.lang.String getValor_54() {
        return valor_54;
    }
    
    /**
     * Setter for property valor_54.
     * @param valor_54 New value of property valor_54.
     */
    public void setValor_54(java.lang.String valor_54) {
        this.valor_54 = valor_54;
    }
    
    /**
     * Getter for property v1.
     * @return Value of property v1.
     */
    public java.lang.String[] getV1() {
        return this.v1;
    }
    
    /**
     * Setter for property v1.
     * @param v1 New value of property v1.
     */
    public void setV1(java.lang.String[] v1) {
        this.v1 = v1;
    }
    
    /**
     * Getter for property v2.
     * @return Value of property v2.
     */
    public java.lang.String[] getV2() {
        return this.v2;
    }
    
    /**
     * Setter for property v2.
     * @param v2 New value of property v2.
     */
    public void setV2(java.lang.String[] v2) {
        this.v2 = v2;
    }
    
    /**
     * Getter for property v3.
     * @return Value of property v3.
     */
    public double[][] getV3() {
        return this.v3;
    }
    
    /**
     * Setter for property v3.
     * @param v3 New value of property v3.
     */
    public void setV3(double[][] v3) {
        this.v3 = v3;
    }
    
    /**
     * Getter for property arr_1.
     * @return Value of property arr_1.
     */
    public double[] getArr_1() {
        return this.arr_1;
    }
    
    /**
     * Setter for property arr_1.
     * @param arr_1 New value of property arr_1.
     */
    public void setArr_1(double[] arr_1) {
        this.arr_1 = arr_1;
    }
    
    /**
     * Getter for property arr_2.
     * @return Value of property arr_2.
     */
    public double[] getArr_2() {
        return this.arr_2;
    }
    
    /**
     * Setter for property arr_2.
     * @param arr_2 New value of property arr_2.
     */
    public void setArr_2(double[] arr_2) {
        this.arr_2 = arr_2;
    }
    
    /**
     * Getter for property v4.
     * @return Value of property v4.
     */
    public java.lang.String[] getV4() {
        return this.v4;
    }
    
    /**
     * Setter for property v4.
     * @param v4 New value of property v4.
     */
    public void setV4(java.lang.String[] v4) {
        this.v4 = v4;
    }
    
    /*javier en 20080610*/
    public String[] getV5(){
	return v5;
    }
    /*javier en 20080610*/
    public void setV5(java.lang.String[] v5){
        this.v5 = v5;
    }

    
    public java.lang.String getValor_55() {
        return valor_55;
    }
    
    /**
     * Setter for property valor_05.
     * @param valor_05 New value of property valor_05.
     */
    public void setValor_55(java.lang.String valor_55) {
        this.valor_55 = valor_55;
    }

    
}
