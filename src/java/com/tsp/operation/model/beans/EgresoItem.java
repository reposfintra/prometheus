/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class EgresoItem {

    private String item_no;
    private String tipo_documento;
    private String documento;
    private String item;
    private String concepto  ;
    private String planilla;
    private String descripcion_item_factura;
    private double vlr_item_egreso;
    private double vlr_item_factura ;
    private String descripcion_item_egreso ;

    private String dstrct_factura;
    private String tipo_documento_factura;
    private String documento_factura;
    private int item_factura;
    private double valor_factura;
    private double valor_saldo;
    private double valor_saldo_factura_original;

    private String cuenta;

    private String dstrct_ingreso;
    private String tipo_documento_ingreso;
    private String num_ingreso;
    private int item_ingreso;





    public EgresoItem() {

    }




    public static EgresoItem load(java.sql.ResultSet rs)throws java.sql.SQLException{

        EgresoItem egresoItem = new EgresoItem();
        egresoItem.setItem_no( rs.getString("item_no") ) ;
        egresoItem.setTipo_documento( rs.getString("tipo_documento") ) ;
        egresoItem.setDocumento( rs.getString("documento") ) ;
        egresoItem.setItem( rs.getString("item") ) ;
        egresoItem.setConcepto( rs.getString("concepto") ) ;
        egresoItem.setPlanilla( rs.getString("planilla") ) ;
        egresoItem.setDescripcion_item_factura( rs.getString("descripcion_item_factura") ) ;
        egresoItem.setVlr_item_egreso( rs.getDouble("vlr_item_egreso") ) ;
        egresoItem.setVlr_item_factura( rs.getDouble("vlr_item_factura") ) ;
        egresoItem.setDescripcion_item_egreso( rs.getString("descripcion_item_egreso") ) ;

        egresoItem.setDstrct_factura("");
        egresoItem.setTipo_documento_factura("") ;
        egresoItem.setDocumento_factura("") ;
        egresoItem.setItem_factura(0) ;
        egresoItem.setCuenta("");
        egresoItem.setValor_factura(0.00);
        egresoItem.setValor_saldo_factura_original(0.00);
        egresoItem.setValor_saldo(0.00);

        egresoItem.setDstrct_ingreso("");
        egresoItem.setTipo_documento_ingreso("");
        egresoItem.setNum_ingreso("");
        egresoItem.setItem_ingreso(0);


        return egresoItem;

    }



    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the item
     */
    public String getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * @return the concept_code
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * @param concept_code the concept_code to set
     */
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    /**
     * @return the planilla
     */
    public String getPlanilla() {
        return planilla;
    }

    /**
     * @param planilla the planilla to set
     */
    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    /**
     * @return the descripcion_item_factura
     */
    public String getDescripcion_item_factura() {
        return descripcion_item_factura;
    }

    /**
     * @param descripcion_item_factura the descripcion_item_factura to set
     */
    public void setDescripcion_item_factura(String descripcion_item_factura) {
        this.descripcion_item_factura = descripcion_item_factura;
    }

    /**
     * @return the vlr_item_egreso
     */
    public double getVlr_item_egreso() {
        return vlr_item_egreso;
    }

    /**
     * @param vlr_item_egreso the vlr_item_egreso to set
     */
    public void setVlr_item_egreso(double vlr_item_egreso) {
        this.vlr_item_egreso = vlr_item_egreso;
    }

    /**
     * @return the vlr_item_factura
     */
    public double getVlr_item_factura() {
        return vlr_item_factura;
    }

    /**
     * @param vlr_item_factura the vlr_item_factura to set
     */
    public void setVlr_item_factura(double vlr_item_factura) {
        this.vlr_item_factura = vlr_item_factura;
    }

    /**
     * @return the descripcion_item_egreso
     */
    public String getDescripcion_item_egreso() {
        return descripcion_item_egreso;
    }

    /**
     * @param descripcion_item_egreso the descripcion_item_egreso to set
     */
    public void setDescripcion_item_egreso(String descripcion_item_egreso) {
        this.descripcion_item_egreso = descripcion_item_egreso;
    }

    /**
     * @return the dstrct_factura
     */
    public String getDstrct_factura() {
        return dstrct_factura;
    }

    /**
     * @param dstrct_factura the dstrct_factura to set
     */
    public void setDstrct_factura(String dstrct_factura) {
        this.dstrct_factura = dstrct_factura;
    }

    /**
     * @return the tipo_documento_factura
     */
    public String getTipo_documento_factura() {
        return tipo_documento_factura;
    }

    /**
     * @param tipo_documento_factura the tipo_documento_factura to set
     */
    public void setTipo_documento_factura(String tipo_documento_factura) {
        this.tipo_documento_factura = tipo_documento_factura;
    }

    /**
     * @return the documento_factura
     */
    public String getDocumento_factura() {
        return documento_factura;
    }

    /**
     * @param documento_factura the documento_factura to set
     */
    public void setDocumento_factura(String documento_factura) {
        this.documento_factura = documento_factura;
    }

    /**
     * @return the item_factura
     */
    public int getItem_factura() {
        return item_factura;
    }

    /**
     * @param item_factura the item_factura to set
     */
    public void setItem_factura(int item_factura) {
        this.item_factura = item_factura;
    }

    /**
     * @return the item_no
     */
    public String getItem_no() {
        return item_no;
    }

    /**
     * @param item_no the item_no to set
     */
    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the dstrct_ingreso
     */
    public String getDstrct_ingreso() {
        return dstrct_ingreso;
    }

    /**
     * @param dstrct_ingreso the dstrct_ingreso to set
     */
    public void setDstrct_ingreso(String dstrct_ingreso) {
        this.dstrct_ingreso = dstrct_ingreso;
    }

    /**
     * @return the tipo_documento_ingreso
     */
    public String getTipo_documento_ingreso() {
        return tipo_documento_ingreso;
    }

    /**
     * @param tipo_documento_ingreso the tipo_documento_ingreso to set
     */
    public void setTipo_documento_ingreso(String tipo_documento_ingreso) {
        this.tipo_documento_ingreso = tipo_documento_ingreso;
    }

    /**
     * @return the num_ingreso
     */
    public String getNum_ingreso() {
        return num_ingreso;
    }

    /**
     * @param num_ingreso the num_ingreso to set
     */
    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    /**
     * @return the item_ingreso
     */
    public int getItem_ingreso() {
        return item_ingreso;
    }

    /**
     * @param item_ingreso the item_ingreso to set
     */
    public void setItem_ingreso(int item_ingreso) {
        this.item_ingreso = item_ingreso;
    }

    /**
     * @return the valor_factura
     */
    public double getValor_factura() {
        return valor_factura;
    }

    /**
     * @param valor_factura the valor_factura to set
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }

    /**
     * @return the valor_saldo
     */
    public double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo(double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    
    
     /**
     * @return the valor_saldo
     */
    public double getValor_saldo_factura_original() {
        return valor_saldo_factura_original;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo_factura_original(double valor_saldo_factura_original) {
        this.valor_saldo_factura_original = valor_saldo_factura_original;
    }   
    
    
}

