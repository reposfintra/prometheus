/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;

/**
 *
 * @author Iris Vargas
 */
public class ConvenioCxcFiducias implements Serializable{
    String reg_status, dstrct, id_convenio, titulo_valor, nit_fiducia, creation_user, user_update, creation_date,
           last_update;

    private String prefijo_cxc_fiducia, hc_cxc_fiducia, cuenta_cxc_fiducia;
    //campos para endoso
    private String prefijo_cxc_endoso;
    private String hc_cxc_endoso;
    private String cuenta_cxc_endoso;
    
   public ConvenioCxcFiducias() {
        this.reg_status = "";
        this.dstrct = "";
        this.id_convenio = "";
        this.titulo_valor = "";
        this.creation_user = "";
        this.user_update = "";
        this.creation_date = "";
        this.last_update = "";
        this.nit_fiducia="";
        this.prefijo_cxc_endoso="";
        this.hc_cxc_endoso="";
        this.cuenta_cxc_endoso="";

    }
    
  

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }
  
    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getId_convenio() {
        return id_convenio;
    }

    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }


    public String getNit_fiducia() {
        return nit_fiducia;
    }

    public void setNit_fiducia(String nit_fiducia) {
        this.nit_fiducia = nit_fiducia;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getTitulo_valor() {
        return titulo_valor;
    }

    public void setTitulo_valor(String titulo_valor) {
        this.titulo_valor = titulo_valor;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public String getCuenta_cxc_fiducia() {
        return cuenta_cxc_fiducia;
    }

    public void setCuenta_cxc_fiducia(String cuenta_cxc_fiducia) {
        this.cuenta_cxc_fiducia = cuenta_cxc_fiducia;
    }

    public String getHc_cxc_fiducia() {
        return hc_cxc_fiducia;
    }

    public void setHc_cxc_fiducia(String hc_cxc_fiducia) {
        this.hc_cxc_fiducia = hc_cxc_fiducia;
    }

    public String getPrefijo_cxc_fiducia() {
        return prefijo_cxc_fiducia;
    }

    public void setPrefijo_cxc_fiducia(String prefijo_cxc_fiducia) {
        this.prefijo_cxc_fiducia = prefijo_cxc_fiducia;
    }

    public String getPrefijo_cxc_endoso() {
        return prefijo_cxc_endoso;
    }

    public void setPrefijo_cxc_endoso(String prefijo_cxc_endoso) {
        this.prefijo_cxc_endoso = prefijo_cxc_endoso;
    }

    public String getHc_cxc_endoso() {
        return hc_cxc_endoso;
    }

    public void setHc_cxc_endoso(String hc_cxc_endoso) {
        this.hc_cxc_endoso = hc_cxc_endoso;
    }

    public String getCuenta_cxc_endoso() {
        return cuenta_cxc_endoso;
    }

    public void setCuenta_cxc_endoso(String cuenta_cxc_endoso) {
        this.cuenta_cxc_endoso = cuenta_cxc_endoso;
    }

}