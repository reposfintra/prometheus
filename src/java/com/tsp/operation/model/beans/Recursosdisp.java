//*************************************** clase modificada
/*
 * Recursosdisp.java
 *
 * Created on 16 de junio de 2005, 11:13 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Jm
 */
public class Recursosdisp implements Serializable{
    private String distrito;
    private String reg_status;
    private String numpla;
    private String origen;
    private String destino;
    private String placa;
    private String clase;
    private String tipo;
    private java.sql.Timestamp fecha_asig;
    private java.sql.Timestamp fecha_disp;
    private float tiempo;
    private String tipo_asig;
    private String cod_cliente;
    private String equipo_aso;
    private String tipo_aso;
    private String req_cliente;
    private String usuario_creacion;
    private java.sql.Timestamp fecha_creacion;
    private String usuario_actualizacion;
    private java.sql.Timestamp fecha_actualizacion;
    
    private String agencia;
    private String equipo_propio;
    ///alpay
    ////modif
    private String afecha_asig;
    private String afecha_disp;
    private float atiempo;
    private java.util.Date afecha_creacion;
    private java.util.Date afecha_actualizacion;
    
    private String recurso;
    private Calendar fechaInicio;
    private String fecha_dispo_req;
    private String numpla_req;
    private String std_job_no_req;
    private int num_sec_req;
    private String dstrct_code_req;
    
    //alejo
    private String nuevaFechaDisponibilidad;
    //KAREN REALES
    private String agaAsoc="";
    private String work_gorup="";
    private int no_Viajes=0;
    private boolean favorito=false;
    private boolean internacional=false;
    
    //JuanM
    private String std_job_no_rec;
    
    private boolean frontera=false;
   
    
    /** Creates a new instance of Recursosdisp */
    public Recursosdisp() {
    }
    
    /**
     * Getter for property clase.
     * @return Value of property clase.
     */
    public java.lang.String getClase() {
        return clase;
    }
    
    /**
     * Setter for property clase.
     * @param clase New value of property clase.
     */
    public void setClase(java.lang.String clase) {
        this.clase = clase;
    }
    
    /**
     * Getter for property cod_cliente.
     * @return Value of property cod_cliente.
     */
    public java.lang.String getCod_cliente() {
        return cod_cliente;
    }
    
    /**
     * Setter for property cod_cliente.
     * @param cod_cliente New value of property cod_cliente.
     */
    public void setCod_cliente(java.lang.String cod_cliente) {
        this.cod_cliente = cod_cliente;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property equipo_aso.
     * @return Value of property equipo_aso.
     */
    public java.lang.String getEquipo_aso() {
        return equipo_aso;
    }
    
    /**
     * Setter for property equipo_aso.
     * @param equipo_aso New value of property equipo_aso.
     */
    public void setEquipo_aso(java.lang.String equipo_aso) {
        this.equipo_aso = equipo_aso;
    }
    
    /**
     * Getter for property fecha_actualizacion.
     * @return Value of property fecha_actualizacion.
     */
    public java.sql.Timestamp getFecha_actualizacion() {
        return fecha_actualizacion;
    }
    
    /**
     * Setter for property fecha_actualizacion.
     * @param fecha_actualizacion New value of property fecha_actualizacion.
     */
    public void setFecha_actualizacion(java.sql.Timestamp fecha_actualizacion) {
        this.fecha_actualizacion = fecha_actualizacion;
    }
    
    
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.sql.Timestamp getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.sql.Timestamp fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property req_cliente.
     * @return Value of property req_cliente.
     */
    public java.lang.String getReq_cliente() {
        return req_cliente;
    }
    
    /**
     * Setter for property req_cliente.
     * @param req_cliente New value of property req_cliente.
     */
    public void setReq_cliente(java.lang.String req_cliente) {
        this.req_cliente = req_cliente;
    }
    
    /**
     * Getter for property tiempo.
     * @return Value of property tiempo.
     */
    public float getTiempo() {
        return tiempo;
    }
    
    /**
     * Setter for property tiempo.
     * @param tiempo New value of property tiempo.
     */
    public void setTiempo(float tiempo) {
        this.tiempo = tiempo;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property tipo_asig.
     * @return Value of property tipo_asig.
     */
    public java.lang.String getTipo_asig() {
        return tipo_asig;
    }
    
    /**
     * Setter for property tipo_asig.
     * @param tipo_asig New value of property tipo_asig.
     */
    public void setTipo_asig(java.lang.String tipo_asig) {
        this.tipo_asig = tipo_asig;
    }
    
    /**
     * Getter for property tipo_aso.
     * @return Value of property tipo_aso.
     */
    public java.lang.String getTipo_aso() {
        return tipo_aso;
    }
    
    /**
     * Setter for property tipo_aso.
     * @param tipo_aso New value of property tipo_aso.
     */
    public void setTipo_aso(java.lang.String tipo_aso) {
        this.tipo_aso = tipo_aso;
    }
    
    /**
     * Getter for property usuario_actualizacion.
     * @return Value of property usuario_actualizacion.
     */
    public java.lang.String getUsuario_actualizacion() {
        return usuario_actualizacion;
    }
    
    /**
     * Setter for property usuario_actualizacion.
     * @param usuario_actualizacion New value of property usuario_actualizacion.
     */
    public void setUsuario_actualizacion(java.lang.String usuario_actualizacion) {
        this.usuario_actualizacion = usuario_actualizacion;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property fecha_disp.
     * @return Value of property fecha_disp.
     */
    public java.sql.Timestamp getFecha_disp() {
        return fecha_disp;
    }
    
    /**
     * Setter for property fecha_disp.
     * @param fecha_disp New value of property fecha_disp.
     */
    public void setFecha_disp(java.sql.Timestamp fecha_disp) {
        this.fecha_disp = fecha_disp;
    }
    
    /**
     * Getter for property fecha_asig.
     * @return Value of property fecha_asig.
     */
    public java.sql.Timestamp getFecha_asig() {
        return fecha_asig;
    }
    
    /**
     * Setter for property fecha_asig.
     * @param fecha_asig New value of property fecha_asig.
     */
    public void setFecha_asig(java.sql.Timestamp fecha_asig) {
        this.fecha_asig = fecha_asig;
    }
    
    ////////////////////alpall
    
    public void setRecurso( String recurso ) {
        this.recurso = recurso;
    }
    
    public String getRecurso() {
        return recurso;
    }
    
    public void setFechaInicio( Calendar fechaInicio ) {
        this.fechaInicio = fechaInicio;
    }
    public Calendar getFechaInicio() {
        return fechaInicio;
    }
    
    public String toString(){
        return "placa = "+this.getPlaca()+",  fechaDisponibilidad="+this.getFecha_disp()+", origen="+this.getOrigen();
    }
    
    public String getFecha_dispo_req() {
        return fecha_dispo_req;
    }
    
    public String getNumpla_req() {
        return numpla_req;
    }
    
    public int getNum_sec_req() {
        return num_sec_req;
    }
    
    public String getDstrct_code_req() {
        return dstrct_code_req;
    }
    
    public String getStd_job_no_req() {
        return std_job_no_req;
    }
    
    /**
     * getCod_recurso
     *
     * @return String
     */
    public String getCod_recurso() {
        return "";
    }
    
    /**
     * setFecha_dispo_req
     *
     * @param string String
     */
    public void setFecha_dispo_req( String string ) {
        this.fecha_dispo_req = string;
    }
    
    /**
     * setNumpla_req
     *
     * @param v String
     */
    public void setNumpla_req( String v ) {
        this.numpla_req = v;
    }
    
    /**
     * setStd_job_no_req
     *
     * @param v String
     */
    public void setStd_job_no_req( String v ) {
        this.std_job_no_req = v;
    }
    
    /**
     * setNum_sec_req
     *
     * @param v String
     */
    public void setNum_sec_req( int v ) {
        this.num_sec_req = v;
    }
    
    /**
     * setDstrct_code_req
     *
     * @param v String
     */
    public void setDstrct_code_req( String v ) {
        this.dstrct_code_req = v;
    }
    
    
    /////modif
    /**
     * Getter for property fecha_asig.
     * @return Value of property fecha_asig.
     */
    public java.lang.String getaFecha_asig() {
        return afecha_asig;
    }
    
    /**
     * Setter for property fecha_asig.
     * @param fecha_asig New value of property fecha_asig.
     */
    public void setaFecha_asig(java.lang.String afecha_asig) {
        this.afecha_asig = afecha_asig;
    }
    
    /**
     * Getter for property fecha_disp.
     * @return Value of property fecha_disp.
     */
    public java.lang.String getaFecha_disp() {
        return afecha_disp;
    }
    
    /**
     * Setter for property fecha_disp.
     * @param fecha_disp New value of property fecha_disp.
     */
    public void setaFecha_disp(java.lang.String afecha_disp) {
        this.afecha_disp = afecha_disp;
    }
    
    /**
     * Getter for property tiempo.
     * @return Value of property tiempo.
     */
    public float getaTiempo() {
        return atiempo;
    }
    
    /**
     * Setter for property tiempo.
     * @param tiempo New value of property tiempo.
     */
    public void setaTiempo(float atiempo) {
        this.atiempo = atiempo;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.util.Date getaFecha_creacion() {
        return afecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setaFecha_creacion(java.util.Date afecha_creacion) {
        this.afecha_creacion = afecha_creacion;
    }
    
    /**
     * Getter for property fecha_actualizacion.
     * @return Value of property fecha_actualizacion.
     */
    public java.util.Date geatFecha_actualizacion() {
        return afecha_actualizacion;
    }
    
    /**
     * Setter for property fecha_actualizacion.
     * @param fecha_actualizacion New value of property fecha_actualizacion.
     */
    public void setaFecha_actualizacion(java.util.Date afecha_actualizacion) {
        this.afecha_actualizacion = afecha_actualizacion;
    }
    
    /**
     * Getter for property nuevaFechaDisponibilidad.
     * @return Value of property nuevaFechaDisponibilidad.
     */
    public java.lang.String getNuevaFechaDisponibilidad() {
        return nuevaFechaDisponibilidad;
    }
    
    /**
     * Setter for property nuevaFechaDisponibilidad.
     * @param nuevaFechaDisponibilidad New value of property nuevaFechaDisponibilidad.
     */
    public void setNuevaFechaDisponibilidad(java.lang.String nuevaFechaDisponibilidad) {
        this.nuevaFechaDisponibilidad = nuevaFechaDisponibilidad;
    }
    
    /**
     * Getter for property agaAsoc.
     * @return Value of property agaAsoc.
     */
    public java.lang.String getAgaAsoc() {
        return agaAsoc;
    }
    
    /**
     * Setter for property agaAsoc.
     * @param agaAsoc New value of property agaAsoc.
     */
    public void setAgaAsoc(java.lang.String agaAsoc) {
        this.agaAsoc = agaAsoc;
    }
    
    /**
     * Getter for property std_job_no_rec.
     * @return Value of property std_job_no_rec.
     */
    public java.lang.String getStd_job_no_rec() {
        return std_job_no_rec;
    }
    
    /**
     * Setter for property std_job_no_rec.
     * @param std_job_no_rec New value of property std_job_no_rec.
     */
    public void setStd_job_no_rec(java.lang.String std_job_no_rec) {
        this.std_job_no_rec = std_job_no_rec;
    }
    
    /**
     * Getter for property work_gorup.
     * @return Value of property work_gorup.
     */
    public java.lang.String getWork_gorup() {
        return work_gorup;
    }
    
    /**
     * Setter for property work_gorup.
     * @param work_gorup New value of property work_gorup.
     */
    public void setWork_gorup(java.lang.String work_gorup) {
        this.work_gorup = work_gorup;
    }
    
    /**
     * Getter for property no_Viajes.
     * @return Value of property no_Viajes.
     */
    public int getNo_Viajes() {
        return no_Viajes;
    }
    
    /**
     * Setter for property no_Viajes.
     * @param no_Viajes New value of property no_Viajes.
     */
    public void setNo_Viajes(int no_Viajes) {
        this.no_Viajes = no_Viajes;
    }
    
    /**
     * Getter for property favorito.
     * @return Value of property favorito.
     */
    public boolean isFavorito() {
        return favorito;
    }
    
    /**
     * Setter for property favorito.
     * @param favorito New value of property favorito.
     */
    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }
    
    /**
     * Getter for property internacional.
     * @return Value of property internacional.
     */
    public boolean isInternacional() {
        return internacional;
    }
    
    /**
     * Setter for property internacional.
     * @param internacional New value of property internacional.
     */
    public void setInternacional(boolean internacional) {
        this.internacional = internacional;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property equipo_propio.
     * @return Value of property equipo_propio.
     */
    public java.lang.String getEquipo_propio() {
        return equipo_propio;
    }
    
    /**
     * Setter for property equipo_propio.
     * @param equipo_propio New value of property equipo_propio.
     */
    public void setEquipo_propio(java.lang.String equipo_propio) {
        this.equipo_propio = equipo_propio;
    }
    
    /**
     * Getter for property frontera.
     * @return Value of property frontera.
     */
    public boolean isFrontera() {
        return frontera;
    }
    
    /**
     * Setter for property frontera.
     * @param frontera New value of property frontera.
     */
    public void setFrontera(boolean frontera) {
        this.frontera = frontera;
    }
    
}
