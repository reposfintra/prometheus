package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Beans implements Serializable{    
    private Hashtable Datos;
    
    /** Creates a new instance of FlujoCaja */
    public Beans() {
        Datos = new Hashtable();
    }    
    // setter 
    public void setBeans(Hashtable valor){
        this.Datos = valor;
    }
    public void addValor(String key, String Valor){
        this.Datos.put(key, Valor);
    }
    // getter
    public Hashtable getBeans(){
        return this.Datos;
    }
    public String getValor(String key){
        return this.Datos.get(key).toString();
    }    
}
