/*
 * Auditoria.java
 *
 * Created on 25 de abril de 2007, 04:05 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  equipo
 */
public class Auditoria {
    
    
    private String reg_status = "";
    private String dstrct = "";
    private String id = "";
    private String tabla = "";
    private String llave = "";
    private String campo = "";
    private String cont_original = "";
    private String cont_nuevo = "";
    private String formato = "";
    private String last_update = "";
    private String user_update = "";
    private String creation_date = "";
    private String creation_user = "";
    private String base = "";
    private String campollave = "";
    
    
    
    /** Creates a new instance of Auditoria */
    public Auditoria() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property campo.
     * @return Value of property campo.
     */
    public java.lang.String getCampo() {
        return campo;
    }
    
    /**
     * Setter for property campo.
     * @param campo New value of property campo.
     */
    public void setCampo(java.lang.String campo) {
        this.campo = campo;
    }
    
    /**
     * Getter for property campollave.
     * @return Value of property campollave.
     */
    public java.lang.String getCampollave() {
        return campollave;
    }
    
    /**
     * Setter for property campollave.
     * @param campollave New value of property campollave.
     */
    public void setCampollave(java.lang.String campollave) {
        this.campollave = campollave;
    }
    
    /**
     * Getter for property cont_nuevo.
     * @return Value of property cont_nuevo.
     */
    public java.lang.String getCont_nuevo() {
        return cont_nuevo;
    }
    
    /**
     * Setter for property cont_nuevo.
     * @param cont_nuevo New value of property cont_nuevo.
     */
    public void setCont_nuevo(java.lang.String cont_nuevo) {
        this.cont_nuevo = cont_nuevo;
    }
    
    /**
     * Getter for property cont_original.
     * @return Value of property cont_original.
     */
    public java.lang.String getCont_original() {
        return cont_original;
    }
    
    /**
     * Setter for property cont_original.
     * @param cont_original New value of property cont_original.
     */
    public void setCont_original(java.lang.String cont_original) {
        this.cont_original = cont_original;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property formato.
     * @return Value of property formato.
     */
    public java.lang.String getFormato() {
        return formato;
    }
    
    /**
     * Setter for property formato.
     * @param formato New value of property formato.
     */
    public void setFormato(java.lang.String formato) {
        this.formato = formato;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public java.lang.String getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property llave.
     * @return Value of property llave.
     */
    public java.lang.String getLlave() {
        return llave;
    }
    
    /**
     * Setter for property llave.
     * @param llave New value of property llave.
     */
    public void setLlave(java.lang.String llave) {
        this.llave = llave;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tabla.
     * @return Value of property tabla.
     */
    public java.lang.String getTabla() {
        return tabla;
    }
    
    /**
     * Setter for property tabla.
     * @param tabla New value of property tabla.
     */
    public void setTabla(java.lang.String tabla) {
        this.tabla = tabla;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}
