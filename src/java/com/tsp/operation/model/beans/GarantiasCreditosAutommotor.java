/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class GarantiasCreditosAutommotor {
        
        private String estado;
        private String negocio ;
        private String identificacion ;
        private String nombre;
        private String nro_propiedad ;
        private String chasis ;
        private String motor;        
        private String nr_poliza;
        private String fecha_exp;
        private String fecha_ven;
        private String negocio_poliza;
        private String placa ;
        private String marca ;
        private String clase ;
        private String servicio;
        private String referencia1;
        private String referencia2 ;
        private String referencia3 ;
        private String pais ;
        private String modelo;
        private String aseguradora;
        private String codigo_fasecolda;
        private double valor_fasecolda ;

    public GarantiasCreditosAutommotor() {
    }

    
    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nro_propiedad
     */
    public String getNro_propiedad() {
        return nro_propiedad;
    }

    /**
     * @param nro_propiedad the nro_propiedad to set
     */
    public void setNro_propiedad(String nro_propiedad) {
        this.nro_propiedad = nro_propiedad;
    }

    /**
     * @return the chasis
     */
    public String getChasis() {
        return chasis;
    }

    /**
     * @param chasis the chasis to set
     */
    public void setChasis(String chasis) {
        this.chasis = chasis;
    }

    /**
     * @return the motor
     */
    public String getMotor() {
        return motor;
    }

    /**
     * @param motor the motor to set
     */
    public void setMotor(String motor) {
        this.motor = motor;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the clase
     */
    public String getClase() {
        return clase;
    }

    /**
     * @param clase the clase to set
     */
    public void setClase(String clase) {
        this.clase = clase;
    }

    /**
     * @return the servicio
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * @param servicio the servicio to set
     */
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    /**
     * @return the referencia1
     */
    public String getReferencia1() {
        return referencia1;
    }

    /**
     * @param referencia1 the referencia1 to set
     */
    public void setReferencia1(String referencia1) {
        this.referencia1 = referencia1;
    }

    /**
     * @return the referencia2
     */
    public String getReferencia2() {
        return referencia2;
    }

    /**
     * @param referencia2 the referencia2 to set
     */
    public void setReferencia2(String referencia2) {
        this.referencia2 = referencia2;
    }

    /**
     * @return the referencia3
     */
    public String getReferencia3() {
        return referencia3;
    }

    /**
     * @param referencia3 the referencia3 to set
     */
    public void setReferencia3(String referencia3) {
        this.referencia3 = referencia3;
    }

    /**
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the aseguradora
     */
    public String getAseguradora() {
        return aseguradora;
    }

    /**
     * @param aseguradora the aseguradora to set
     */
    public void setAseguradora(String aseguradora) {
        this.aseguradora = aseguradora;
    }

    /**
     * @return the codigo_fasecolda
     */
    public String getCodigo_fasecolda() {
        return codigo_fasecolda;
    }

    /**
     * @param codigo_fasecolda the codigo_fasecolda to set
     */
    public void setCodigo_fasecolda(String codigo_fasecolda) {
        this.codigo_fasecolda = codigo_fasecolda;
    }

    /**
     * @return the valor_fasecolda
     */
    public double getValor_fasecolda() {
        return valor_fasecolda;
    }

    /**
     * @param valor_fasecolda the valor_fasecolda to set
     */
    public void setValor_fasecolda(double valor_fasecolda) {
        this.valor_fasecolda = valor_fasecolda;
    }
   

    /**
     * @return the nr_poliza
     */
    public String getNr_poliza() {
        return nr_poliza;
    }

    /**
     * @param nr_poliza the nr_poliza to set
     */
    public void setNr_poliza(String nr_poliza) {
        this.nr_poliza = nr_poliza;
    }

    /**
     * @return the fecha_exp
     */
    public String getFecha_exp() {
        return fecha_exp;
    }

    /**
     * @param fecha_exp the fecha_exp to set
     */
    public void setFecha_exp(String fecha_exp) {
        this.fecha_exp = fecha_exp;
    }

    /**
     * @return the fecha_ven
     */
    public String getFecha_ven() {
        return fecha_ven;
    }

    /**
     * @param fecha_ven the fecha_ven to set
     */
    public void setFecha_ven(String fecha_ven) {
        this.fecha_ven = fecha_ven;
    }

    /**
     * @return the negocio_poliza
     */
    public String getNegocio_poliza() {
        return negocio_poliza;
    }

    /**
     * @param negocio_poliza the negocio_poliza to set
     */
    public void setNegocio_poliza(String negocio_poliza) {
        this.negocio_poliza = negocio_poliza;
    }
    
    @Override
    public String toString() {
        return "GarantiasCreditosAutommotor{" + "negocio=" + negocio + ", identificacion=" + identificacion + ", nombre=" + nombre + ", nro_propiedad=" + nro_propiedad + ", chasis=" + chasis + ", motor=" + motor + ", placa=" + placa + ", marca=" + marca + ", clase=" + clase + ", servicio=" + servicio + ", referencia1=" + referencia1 + ", referencia2=" + referencia2 + ", referencia3=" + referencia3 + ", pais=" + pais + ", modelo=" + modelo + ", aseguradora=" + aseguradora + ", codigo_fasecolda=" + codigo_fasecolda + ", valor_fasecolda=" + valor_fasecolda + '}';
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
        
}
