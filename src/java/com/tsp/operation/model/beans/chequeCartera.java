/*
 * chequeCartera.java
 *
 * Created on 2 de enero de 2008, 08:45 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author NAVI
 */
public class chequeCartera {
    static String codigo_negocio="";
    String desc;
    String item,fecha_cheque,fecha_consignacion,porte,valor;
    String dias,factor,descuento,vlrsincustodia,custodia,remesa,valorgirable;
    String nit,proveedor,fechainicio;
    String valorgirablesinremesaniporte;
    String tasa_efectiva,tasa_nominal;
    String porcentajeRemesa,nombreProveedor, nombreNit;
    String mod_remesa,mod_custodia;
    String tipodocx;
    String mod_avalx;
    
    String saldo_inicial,valor_capital,valor_interes,saldo_final;
    
    String no_aval="0";
    
    /** Creates a new instance of chequeCartera */
    public chequeCartera(String item1,String fecha_cheque1,String fecha_consignacion1,String porte1,String valor1,String nit1,String proveedor1,String fechainicio1,String dias1,String factor1,String descuento1,String valorsincustodia1,String custodia1,String remesa1,String valorgirable1,String valorgirablesinremesaniporte1,String tasa_efectiva1,String tasa_nominal1,String codigo_negocio1,String porcentajeRemesa1,String nombreProveedor1,String nombreNit1,String mod_remesa1,String mod_custodia1,String tipodocx1,String mod_avalx1) {
        codigo_negocio=codigo_negocio1;
        porcentajeRemesa=porcentajeRemesa1;
        item=item1;
        
        fecha_cheque=fecha_cheque1;
        fecha_consignacion=fecha_consignacion1;
        porte=porte1;
        valor=valor1;
        
        nit=nit1;
        proveedor=proveedor1;
        fechainicio=fechainicio1;

        dias=dias1;
        factor=factor1;
        descuento=descuento1;
        vlrsincustodia=valorsincustodia1;
        custodia=custodia1;
        remesa=remesa1;
        valorgirable=valorgirable1;
        valorgirablesinremesaniporte=valorgirablesinremesaniporte1;
        //System.out.println("valorgirablesinremesaniporte en creacion de cheque="+valorgirablesinremesaniporte);
        //dias=obtenerDias(fecha_consignacion,fechainicio);
        tasa_efectiva=tasa_efectiva1;
        tasa_nominal=tasa_nominal1;
        
        nombreProveedor=nombreProveedor1;
        nombreNit=nombreNit1;
        
        mod_remesa=mod_remesa1;
        mod_custodia=mod_custodia1;
        tipodocx=tipodocx1;
        mod_avalx=mod_avalx1;
    }

    public chequeCartera(){
        
    }
    
    public String getDescripcion(){
        return "ja";
    }
    
    public String getItem(){
        return item;
    }
    
    public String getFechaCheque(){
        return fecha_cheque;
    }
    
    public String getFechaConsignacion(){
        return fecha_consignacion;
    }
    
    public String getPorte(){
        return porte;
    }
    
    public String getValor(){
        return valor;
    }

    public void setFechaCheque(String fecha_cheque1){
        fecha_cheque=fecha_cheque1;
    }
    
    public void setFechaConsignacion(String fecha_consignacion1){
        fecha_consignacion=fecha_consignacion1;
    }
    
    public void setPorte(String porte1){
        porte=porte1;
    }
    
    public void setValor(String valor1){
        valor=valor1;
    }

    public void setCodeNegocio(String codigo_negocio1){
        codigo_negocio=codigo_negocio1;
    }
    
    public String getDias(){
        return dias;
    }
    
    public String getFactor(){
        return factor;
    }
    public String getDescuento(){
        return descuento;
    }
    public String getValorSinCustodia(){
        return vlrsincustodia;
    }
    public String getCustodia(){
        return custodia;
    }
    public String getRemesa(){
        return remesa;
    }
    public String getValorGirable(){
        return valorgirable;
    }
    public String getValorGirableSinRemesaNiPorte(){
        return valorgirablesinremesaniporte;
    }
    public String getNit(){
        return nit;
    }
    public String getProveedor(){
        return proveedor;
    }
    public String getTasaEfectiva(){
        return tasa_efectiva;
    }
    public String getTasaNominal(){
        return tasa_nominal;
    }
    public String getCodigoNegocio(){
        return codigo_negocio;
    }
    public String getNombreProveedor(){
        return nombreProveedor;
    }
    public String getNombreNit(){
        return nombreNit;
    }
    public String getPorcentajeRemesa(){
        return porcentajeRemesa;
    }
    public String getModRemesa(){
        return mod_remesa;
    }
    public String getModCustodia(){
        return mod_custodia;
    }
    public String getFechaDesembolso(){
        return fechainicio;
    }
    public void setPorcentajeRemesa(String porcentajeRemesa1){
        porcentajeRemesa=porcentajeRemesa1;
    }
    public void setItem(String item1){
        item=item1;
    }
    public void setProveedor(String proveedor1){
        proveedor=proveedor1;
    }
    
    public void setFechaInicio(String fechainicio1){
        fechainicio=fechainicio1;
    }
    public void setDias(String dias1){
        dias=dias1;
    }    
    public void setFactor(String factor1){
        factor=factor1;
    }
    public void setDescuento(String descuento1){
        descuento=descuento1;
    }
    public void setNit(String nit1){
        nit=nit1;
    }    
    public void setValorSinCustodia(String vlrsincustodia1){
        vlrsincustodia=vlrsincustodia1;
    }  
    public void setCustodia(String custodia1){
        custodia=custodia1;
    } 
    public void setRemesa(String remesa1){
        remesa=remesa1;
    } 
    public void setValorGirable(String valorgirable1){
        valorgirable=valorgirable1;
    } 
    public void setValorGirableSinRemesaNiPorte(String valorgirablesinremesaniporte1){
        valorgirablesinremesaniporte=valorgirablesinremesaniporte1;
    } 
    public void setTasaEfectiva(String tasa_efectiva1){
        tasa_efectiva=tasa_efectiva1;
    } 
    public void setTasaNominal(String tasa_nominal1){
        tasa_nominal=tasa_nominal1;
    } 
    public void setNombreProveedor(String nombreProveedor1){
        nombreProveedor=nombreProveedor1;
    } 
    public void setNombreNit(String nombreNit1){
        nombreNit=nombreNit1;
    } 
    public void setModalidadRemesa(String mod_remesa1){
        mod_remesa=mod_remesa1;
    } 
    public void setModalidadCustodia(String mod_custodia1){
        mod_custodia=mod_custodia1;
    }  
    public String getTipoDoc(){
        return tipodocx;
    }
    public String getModAval(){
        return mod_avalx;
    }
    
    public void setSaldoInicial(String saldo_inicial1){
        saldo_inicial=saldo_inicial1;
    }  
    public String getSaldoInicial(){
        return saldo_inicial;
    }
    public void setValorCapital(String valor_capital1){
        valor_capital=valor_capital1;
    }  
    public String getValorCapital(){
        return valor_capital;
    }
    public void setValorInteres(String valor_interes1){
        valor_interes=valor_interes1;
    }  
    public String getValorInteres(){
        return valor_interes;
    }
    public void setSaldoFinal(String saldo_final1){
        saldo_final=saldo_final1;
    }  
    public String getSaldoFinal(){
        return saldo_final;
    }
    public void setNoAval(String x1){
        no_aval=x1;
    }  
    public String getNoAval(){
        return no_aval;
    }
    
    
}
