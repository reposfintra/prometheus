/******************************************************************************
 *      Nombre Clase.................   AplicacionService.java
 *      Descripci�n..................   Permite explorar una ruta y muestra
 *                                      las subcarpetar y p�ginas JSP
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   08.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/

package com.tsp.operation.model.beans;


import java.io.*;
import java.util.*;

public class ExploradorJSP {
    private List Listado;
    
    /** Creates a new instance of ExploradorJSP */
    public ExploradorJSP() {
        Listado = new LinkedList();
    }
    
    /**
     * Explora una ruta de archivo.
     * @autor Tito Andr�s Maturana
     * @param Base El directorio ra�z.
     * @throws Exception
     * @version 1.1
     */
    public static ExploradorJSP Load(String Base) throws Exception{
        ExploradorJSP Lista = new ExploradorJSP();
        File f = null;
        f = new File(Base);
        if(! f.exists() ) f.mkdir();
        if (f.isDirectory()){
            FilenameFilter filter = new FilenameFilter() {
                public boolean accept( File path, String name){
                    String url = path.getAbsolutePath() + "\\" + name;
                    File f = new File(url);
                    return ( name.toLowerCase().endsWith(".jsp") ) || ( f!=null && f.isDirectory() );
                }
            };
            File []arc  =  f.listFiles(filter);
            File []file =  Ordenar(arc);
            for (int i=0;i<file.length;i++){
                ExploradorJSPFile fs = ExploradorJSPFile.Load(i, file[i] );
                Lista.Listado.add(fs);
            }
        }
        return Lista;
    }
    
    /**
     * Getter for property Listado.
     * @return Value of property Listado.
     */
    public java.util.List getListado() {
        return Listado;
    }
    
    /**
     * Setter for property Listado.
     * @param Listado New value of property Listado.
     */
    public void setListado(java.util.List Listado) {
        this.Listado = Listado;
    }
    
    /**
     * Ordena un arreglo de archivos por el tipo (Directorio o Archivo).
     * @autor Tito Andr�s Maturana
     * @param arg El arreglo de archivos
     * @throws Exception
     * @version 1.0
     */
    public static File[] Ordenar(File []arg){
        File temp = null;
        for (int i = 0; i<arg.length;i++)
            for (int j = i+1; j<arg.length ;j++){
                if( arg[i].isFile() && arg[j].isDirectory()){
                    temp = arg[i];
                    arg[i] = arg[j];
                    arg[j] = temp;
                }
            }
        return arg;
    }
    
}
