/************************************************************************
 * Nombre clase: Ingreso_detalle.java                                   *
 * Descripci�n: Clase que maneja los datos de los detalles del ingreso. *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: 9 de mayo de 2006, 09:53 AM                                   *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.beans;
import java.util.LinkedList;

public class Ingreso_detalle  implements java.io.Serializable {
    
    private String cliente;
    private String numero_ingreso;
    private int item;
    private int cantidad_item;
    private double valor_ingreso;
    private double valor_ingreso_me;
    private String factura;
    private String fecha_factura;
    private String codigo_retefuente;
    private double valor_retefuente;
    private double valor_retefuente_me;
    private String tipo_doc;
    private String documento;
    private String codigo_reteica;
    private double valor_reteica;
    private double valor_reteica_me;
    private double valor_diferencia;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String base;
    private String moneda;
    private String distrito;
    private String nit_cliente;
    private String nom_cliente;
    private String fecha_consignacion;
    private boolean seleccionado;
    private String descripcion_factura;
    private double valor_total;
    private double valor_factura;
    private double valor_factura_me; 
    private String moneda_factura;
    private double porcentaje_rfte; 
    private double porcentaje_rica;
    private double valor_total_factura;
    private double valor_total_factura_me;
    private double valor_saldo_factura;
    private double valor_saldo_factura_me; 
    private double valor_tasa_factura;
    private double valor_tasa_ingreso;
    private double valor_tasa;
    private double valor_abono;
    private String fecha_anulacion;
    private String fecha_contabilizacion;
    private String tipo_documento;
    private boolean anulado;
    private double valor_saldo_fact;
    private double valor_saldo_fing;
    private double sal_fact_mlocal;
    private String banco;
    private String sucursal;
    private String agencia_banco;
    private String agencia_facturacion;
    private String fecha_impresion;
    private String concepto;
    private String numero_consignacion;
    private double vlr_tasa;
    private String fecha_tasa;
    private double saldo_facMI;
    private double tasaDB;
    private String pagina;
    private double saldo_ingreso;
    private double valor_ingreso_temporal;
    /*diogenes*/
    private String cuenta;
    private String auxiliar;
    private String descripcion;
    private String tipo_aux;
    private LinkedList tipos; 
    private double sal_factura_vista;
    private String cuenta_banco;
    private String ciudad_cliente;
    private String direccion_cliente;
    private String telefono_cliente;
    private String reg_status;
    
    /** mfontalvo 2007-0206 */
    private String account_code_rfte;
    private String account_code_rica;
    private String abc;
    private int nro_extracto;
    private String negaso;
    private String numCuota;

    
    /**
     * Getter for property account_code_rfte.
     * @return Value of property account_code_rfte.
     */
    public java.lang.String getAccount_code_rfte() {
        return account_code_rfte;
    }
    
    /**
     * Setter for property account_code_rfte.
     * @param account_code_rfte New value of property account_code_rfte.
     */
    public void setAccount_code_rfte(java.lang.String account_code_rfte) {
        this.account_code_rfte = account_code_rfte;
    }
    
    /**
     * Getter for property account_code_rica.
     * @return Value of property account_code_rica.
     */
    public java.lang.String getAccount_code_rica() {
        return account_code_rica;
    }
    
    /**
     * Setter for property account_code_rica.
     * @param account_code_rica New value of property account_code_rica.
     */
    public void setAccount_code_rica(java.lang.String account_code_rica) {
        this.account_code_rica = account_code_rica;
    }


    
    /** Creates a new instance of Ingreso_detalle */
    public Ingreso_detalle () {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente () {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente (java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property codigo_retefuente.
     * @return Value of property codigo_retefuente.
     */
    public java.lang.String getCodigo_retefuente () {
        return codigo_retefuente;
    }
    
    /**
     * Setter for property codigo_retefuente.
     * @param codigo_retefuente New value of property codigo_retefuente.
     */
    public void setCodigo_retefuente (java.lang.String codigo_retefuente) {
        this.codigo_retefuente = codigo_retefuente;
    }
    
    /**
     * Getter for property codigo_reteica.
     * @return Value of property codigo_reteica.
     */
    public java.lang.String getCodigo_reteica () {
        return codigo_reteica;
    }
    
    /**
     * Setter for property codigo_reteica.
     * @param codigo_reteica New value of property codigo_reteica.
     */
    public void setCodigo_reteica (java.lang.String codigo_reteica) {
        this.codigo_reteica = codigo_reteica;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date () {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user () {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento () {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento (java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura () {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura (java.lang.String factura) {
        this.factura = factura;
    }
    
    
    /**
     * Getter for property fecha_factura.
     * @return Value of property fecha_factura.
     */
    public java.lang.String getFecha_factura () {
        return fecha_factura;
    }
    
    /**
     * Setter for property fecha_factura.
     * @param fecha_factura New value of property fecha_factura.
     */
    public void setFecha_factura (java.lang.String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public int getItem () {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem (int item) {
        this.item = item;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update () {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update (java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property numero_ingreso.
     * @return Value of property numero_ingreso.
     */
    public java.lang.String getNumero_ingreso () {
        return numero_ingreso;
    }
    
    /**
     * Setter for property numero_ingreso.
     * @param numero_ingreso New value of property numero_ingreso.
     */
    public void setNumero_ingreso (java.lang.String numero_ingreso) {
        this.numero_ingreso = numero_ingreso;
    }
    
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc () {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc (java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update () {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property valor_diferencia.
     * @return Value of property valor_diferencia.
     */
    public double getValor_diferencia () {
        return valor_diferencia;
    }
    
    /**
     * Setter for property valor_diferencia.
     * @param valor_diferencia New value of property valor_diferencia.
     */
    public void setValor_diferencia (double valor_diferencia) {
        this.valor_diferencia = valor_diferencia;
    }
    
    /**
     * Getter for property valor_ingreso.
     * @return Value of property valor_ingreso.
     */
    public double getValor_ingreso () {
        return valor_ingreso;
    }
    
    /**
     * Setter for property valor_ingreso.
     * @param valor_ingreso New value of property valor_ingreso.
     */
    public void setValor_ingreso (double valor_ingreso) {
        this.valor_ingreso = valor_ingreso;
    }
    
    /**
     * Getter for property valor_ingreso_me.
     * @return Value of property valor_ingreso_me.
     */
    public double getValor_ingreso_me () {
        return valor_ingreso_me;
    }
    
    /**
     * Setter for property valor_ingreso_me.
     * @param valor_ingreso_me New value of property valor_ingreso_me.
     */
    public void setValor_ingreso_me (double valor_ingreso_me) {
        this.valor_ingreso_me = valor_ingreso_me;
    }
    
    /**
     * Getter for property valor_retefuente.
     * @return Value of property valor_retefuente.
     */
    public double getValor_retefuente () {
        return valor_retefuente;
    }
    
    /**
     * Setter for property valor_retefuente.
     * @param valor_retefuente New value of property valor_retefuente.
     */
    public void setValor_retefuente (double valor_retefuente) {
        this.valor_retefuente = valor_retefuente;
    }
    
    /**
     * Getter for property valor_retefuente_me.
     * @return Value of property valor_retefuente_me.
     */
    public double getValor_retefuente_me () {
        return valor_retefuente_me;
    }
    
    /**
     * Setter for property valor_retefuente_me.
     * @param valor_retefuente_me New value of property valor_retefuente_me.
     */
    public void setValor_retefuente_me (double valor_retefuente_me) {
        this.valor_retefuente_me = valor_retefuente_me;
    }
    
    /**
     * Getter for property valor_reteica.
     * @return Value of property valor_reteica.
     */
    public double getValor_reteica () {
        return valor_reteica;
    }
    
    /**
     * Setter for property valor_reteica.
     * @param valor_reteica New value of property valor_reteica.
     */
    public void setValor_reteica (double valor_reteica) {
        this.valor_reteica = valor_reteica;
    }
    
    /**
     * Getter for property valor_reteica_me.
     * @return Value of property valor_reteica_me.
     */
    public double getValor_reteica_me () {
        return valor_reteica_me;
    }
    
    /**
     * Setter for property valor_reteica_me.
     * @param valor_reteica_me New value of property valor_reteica_me.
     */
    public void setValor_reteica_me (double valor_reteica_me) {
        this.valor_reteica_me = valor_reteica_me;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda () {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda (java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property nit_cliente.
     * @return Value of property nit_cliente.
     */
    public java.lang.String getNit_cliente () {
        return nit_cliente;
    }
    
    /**
     * Setter for property nit_cliente.
     * @param nit_cliente New value of property nit_cliente.
     */
    public void setNit_cliente (java.lang.String nit_cliente) {
        this.nit_cliente = nit_cliente;
    }
    
    /**
     * Getter for property nom_cliente.
     * @return Value of property nom_cliente.
     */
    public java.lang.String getNom_cliente () {
        return nom_cliente;
    }
    
    /**
     * Setter for property nom_cliente.
     * @param nom_cliente New value of property nom_cliente.
     */
    public void setNom_cliente (java.lang.String nom_cliente) {
        this.nom_cliente = nom_cliente;
    }
    
    /**
     * Getter for property fecha_consignacion.
     * @return Value of property fecha_consignacion.
     */
    public java.lang.String getFecha_consignacion () {
        return fecha_consignacion;
    }
    
    /**
     * Setter for property fecha_consignacion.
     * @param fecha_consignacion New value of property fecha_consignacion.
     */
    public void setFecha_consignacion (java.lang.String fecha_consignacion) {
        this.fecha_consignacion = fecha_consignacion;
    }
    
    /**
     * Getter for property seleccionado.
     * @return Value of property seleccionado.
     */
    public boolean isSeleccionado () {
        return seleccionado;
    }
    
    /**
     * Setter for property seleccionado.
     * @param seleccionado New value of property seleccionado.
     */
    public void setSeleccionado (boolean seleccionado) {
        this.seleccionado = seleccionado;
    }
    
    /**
     * Getter for property descripcion_factura.
     * @return Value of property descripcion_factura.
     */
    public java.lang.String getDescripcion_factura () {
        return descripcion_factura;
    }
    
    /**
     * Setter for property descripcion_factura.
     * @param descripcion_factura New value of property descripcion_factura.
     */
    public void setDescripcion_factura (java.lang.String descripcion_factura) {
        this.descripcion_factura = descripcion_factura;
    }
    
    /**
     * Getter for property valor_total.
     * @return Value of property valor_total.
     */
    public double getValor_total () {
        return valor_total;
    }
    
    /**
     * Setter for property valor_total.
     * @param valor_total New value of property valor_total.
     */
    public void setValor_total (double valor_total) {
        this.valor_total = valor_total;
    }
    
    /**
     * Getter for property valor_factura.
     * @return Value of property valor_factura.
     */
    public double getValor_factura () {
        return valor_factura;
    }
    
    /**
     * Setter for property valor_factura.
     * @param valor_factura New value of property valor_factura.
     */
    public void setValor_factura (double valor_factura) {
        this.valor_factura = valor_factura;
    }
    
    /**
     * Getter for property valor_factura_me.
     * @return Value of property valor_factura_me.
     */
    public double getValor_factura_me () {
        return valor_factura_me;
    }
    
    /**
     * Setter for property valor_factura_me.
     * @param valor_factura_me New value of property valor_factura_me.
     */
    public void setValor_factura_me (double valor_factura_me) {
        this.valor_factura_me = valor_factura_me;
    }
    
    /**
     * Getter for property moneda_factura.
     * @return Value of property moneda_factura.
     */
    public java.lang.String getMoneda_factura () {
        return moneda_factura;
    }
    
    /**
     * Setter for property moneda_factura.
     * @param moneda_factura New value of property moneda_factura.
     */
    public void setMoneda_factura (java.lang.String moneda_factura) {
        this.moneda_factura = moneda_factura;
    }
    
    /**
     * Getter for property porcentaje_rfte.
     * @return Value of property porcentaje_rfte.
     */
    public double getPorcentaje_rfte () {
        return porcentaje_rfte;
    }
    
    /**
     * Setter for property porcentaje_rfte.
     * @param porcentaje_rfte New value of property porcentaje_rfte.
     */
    public void setPorcentaje_rfte (double porcentaje_rfte) {
        this.porcentaje_rfte = porcentaje_rfte;
    }
    
    /**
     * Getter for property valor_total_factura.
     * @return Value of property valor_total_factura.
     */
    public double getValor_total_factura () {
        return valor_total_factura;
    }
    
    /**
     * Setter for property valor_total_factura.
     * @param valor_total_factura New value of property valor_total_factura.
     */
    public void setValor_total_factura (double valor_total_factura) {
        this.valor_total_factura = valor_total_factura;
    }
    
    /**
     * Getter for property valor_total_factura_me.
     * @return Value of property valor_total_factura_me.
     */
    public double getValor_total_factura_me () {
        return valor_total_factura_me;
    }
    
    /**
     * Setter for property valor_total_factura_me.
     * @param valor_total_factura_me New value of property valor_total_factura_me.
     */
    public void setValor_total_factura_me (double valor_total_factura_me) {
        this.valor_total_factura_me = valor_total_factura_me;
    }
    
    /**
     * Getter for property valor_saldo_factura.
     * @return Value of property valor_saldo_factura.
     */
    public double getValor_saldo_factura () {
        return valor_saldo_factura;
    }
    
    /**
     * Setter for property valor_saldo_factura.
     * @param valor_saldo_factura New value of property valor_saldo_factura.
     */
    public void setValor_saldo_factura (double valor_saldo_factura) {
        this.valor_saldo_factura = valor_saldo_factura;
    }
    
    /**
     * Getter for property valor_saldo_factura_me.
     * @return Value of property valor_saldo_factura_me.
     */
    public double getValor_saldo_factura_me () {
        return valor_saldo_factura_me;
    }
    
    /**
     * Setter for property valor_saldo_factura_me.
     * @param valor_saldo_factura_me New value of property valor_saldo_factura_me.
     */
    public void setValor_saldo_factura_me (double valor_saldo_factura_me) {
        this.valor_saldo_factura_me = valor_saldo_factura_me;
    }
    
    /**
     * Getter for property valor_tasa_factura.
     * @return Value of property valor_tasa_factura.
     */
    public double getValor_tasa_factura () {
        return valor_tasa_factura;
    }
    
    /**
     * Setter for property valor_tasa_factura.
     * @param valor_tasa_factura New value of property valor_tasa_factura.
     */
    public void setValor_tasa_factura (double valor_tasa_factura) {
        this.valor_tasa_factura = valor_tasa_factura;
    }
    
    /**
     * Getter for property valor_tasa_ingreso.
     * @return Value of property valor_tasa_ingreso.
     */
    public double getValor_tasa_ingreso () {
        return valor_tasa_ingreso;
    }
    
    /**
     * Setter for property valor_tasa_ingreso.
     * @param valor_tasa_ingreso New value of property valor_tasa_ingreso.
     */
    public void setValor_tasa_ingreso (double valor_tasa_ingreso) {
        this.valor_tasa_ingreso = valor_tasa_ingreso;
    }
    
    /**
     * Getter for property porcentaje_rica.
     * @return Value of property porcentaje_rica.
     */
    public double getPorcentaje_rica () {
        return porcentaje_rica;
    }
    
    /**
     * Setter for property porcentaje_rica.
     * @param porcentaje_rica New value of property porcentaje_rica.
     */
    public void setPorcentaje_rica (double porcentaje_rica) {
        this.porcentaje_rica = porcentaje_rica;
    }
    
    /**
     * Getter for property valor_tasa.
     * @return Value of property valor_tasa.
     */
    public double getValor_tasa () {
        return valor_tasa;
    }
    
    /**
     * Setter for property valor_tasa.
     * @param valor_tasa New value of property valor_tasa.
     */
    public void setValor_tasa (double valor_tasa) {
        this.valor_tasa = valor_tasa;
    }
    
    /**
     * Getter for property cantidad_item.
     * @return Value of property cantidad_item.
     */
    public int getCantidad_item () {
        return cantidad_item;
    }
    
    /**
     * Setter for property cantidad_item.
     * @param cantidad_item New value of property cantidad_item.
     */
    public void setCantidad_item (int cantidad_item) {
        this.cantidad_item = cantidad_item;
    }
    
    /**
     * Getter for property valor_abono.
     * @return Value of property valor_abono.
     */
    public double getValor_abono () {
        return valor_abono;
    }
    
    /**
     * Setter for property valor_abono.
     * @param valor_abono New value of property valor_abono.
     */
    public void setValor_abono (double valor_abono) {
        this.valor_abono = valor_abono;
    }
    
    /**
     * Getter for property fecha_anulacion.
     * @return Value of property fecha_anulacion.
     */
    public java.lang.String getFecha_anulacion () {
        return fecha_anulacion;
    }
    
    /**
     * Setter for property fecha_anulacion.
     * @param fecha_anulacion New value of property fecha_anulacion.
     */
    public void setFecha_anulacion (java.lang.String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }
    
    /**
     * Getter for property fecha_contabilizacion.
     * @return Value of property fecha_contabilizacion.
     */
    public java.lang.String getFecha_contabilizacion () {
        return fecha_contabilizacion;
    }
    
    /**
     * Setter for property fecha_contabilizacion.
     * @param fecha_contabilizacion New value of property fecha_contabilizacion.
     */
    public void setFecha_contabilizacion (java.lang.String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }
    
    /**
     * Getter for property anulado.
     * @return Value of property anulado.
     */
    public boolean isAnulado () {
        return anulado;
    }
    
    /**
     * Setter for property anulado.
     * @param anulado New value of property anulado.
     */
    public void setAnulado (boolean anulado) {
        this.anulado = anulado;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento () {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento (java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property valor_saldo_fact.
     * @return Value of property valor_saldo_fact.
     */
    public double getValor_saldo_fact () {
        return valor_saldo_fact;
    }
    
    /**
     * Setter for property valor_saldo_fact.
     * @param valor_saldo_fact New value of property valor_saldo_fact.
     */
    public void setValor_saldo_fact (double valor_saldo_fact) {
        this.valor_saldo_fact = valor_saldo_fact;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property tipo_aux.
     * @return Value of property tipo_aux.
     */
    public java.lang.String getTipo_aux() {
        return tipo_aux;
    }
    
    /**
     * Setter for property tipo_aux.
     * @param tipo_aux New value of property tipo_aux.
     */
    public void setTipo_aux(java.lang.String tipo_aux) {
        this.tipo_aux = tipo_aux;
    }
    
    /**
     * Getter for property tipos.
     * @return Value of property tipos.
     */
    public LinkedList getTipos() {
        return tipos;
    }
    
    /**
     * Setter for property tipos.
     * @param tipos New value of property tipos.
     */
    public void setTipos(LinkedList tipos) {
        this.tipos = tipos;
    }
    
    /**
     * Getter for property sal_factura_vista.
     * @return Value of property sal_factura_vista.
     */
    public double getSal_factura_vista () {
        return sal_factura_vista;
    }    
    
    /**
     * Setter for property sal_factura_vista.
     * @param sal_factura_vista New value of property sal_factura_vista.
     */
    public void setSal_factura_vista (double sal_factura_vista) {
        this.sal_factura_vista = sal_factura_vista;
    }
    
    /**
     * Getter for property valor_saldo_fing.
     * @return Value of property valor_saldo_fing.
     */
    public double getValor_saldo_fing () {
        return valor_saldo_fing;
    }
    
    /**
     * Setter for property valor_saldo_fing.
     * @param valor_saldo_fing New value of property valor_saldo_fing.
     */
    public void setValor_saldo_fing (double valor_saldo_fing) {
        this.valor_saldo_fing = valor_saldo_fing;
    }
    
    /**
     * Getter for property sal_fact_mlocal.
     * @return Value of property sal_fact_mlocal.
     */
    public double getSal_fact_mlocal () {
        return sal_fact_mlocal;
    }
    
    /**
     * Setter for property sal_fact_mlocal.
     * @param sal_fact_mlocal New value of property sal_fact_mlocal.
     */
    public void setSal_fact_mlocal (double sal_fact_mlocal) {
        this.sal_fact_mlocal = sal_fact_mlocal;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco () {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco (java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal () {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal (java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property agencia_banco.
     * @return Value of property agencia_banco.
     */
    public java.lang.String getAgencia_banco () {
        return agencia_banco;
    }
    
    /**
     * Setter for property agencia_banco.
     * @param agencia_banco New value of property agencia_banco.
     */
    public void setAgencia_banco (java.lang.String agencia_banco) {
        this.agencia_banco = agencia_banco;
    }
    
    /**
     * Getter for property agencia_facturacion.
     * @return Value of property agencia_facturacion.
     */
    public java.lang.String getAgencia_facturacion () {
        return agencia_facturacion;
    }
    
    /**
     * Setter for property agencia_facturacion.
     * @param agencia_facturacion New value of property agencia_facturacion.
     */
    public void setAgencia_facturacion (java.lang.String agencia_facturacion) {
        this.agencia_facturacion = agencia_facturacion;
    }
    
    /**
     * Getter for property cuenta_banco.
     * @return Value of property cuenta_banco.
     */
    public java.lang.String getCuenta_banco() {
        return cuenta_banco;
    }
    
    /**
     * Setter for property cuenta_banco.
     * @param cuenta_banco New value of property cuenta_banco.
     */
    public void setCuenta_banco(java.lang.String cuenta_banco) {
        this.cuenta_banco = cuenta_banco;
    }
    
    /**
     * Getter for property fecha_impresion.
     * @return Value of property fecha_impresion.
     */
    public java.lang.String getFecha_impresion() {
        return fecha_impresion;
    }
    
    /**
     * Setter for property fecha_impresion.
     * @param fecha_impresion New value of property fecha_impresion.
     */
    public void setFecha_impresion(java.lang.String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property numero_consignacion.
     * @return Value of property numero_consignacion.
     */
    public java.lang.String getNumero_consignacion() {
        return numero_consignacion;
    }
    
    /**
     * Setter for property numero_consignacion.
     * @param numero_consignacion New value of property numero_consignacion.
     */
    public void setNumero_consignacion(java.lang.String numero_consignacion) {
        this.numero_consignacion = numero_consignacion;
    }
    
    /**
     * Getter for property vlr_tasa.
     * @return Value of property vlr_tasa.
     */
    public double getVlr_tasa() {
        return vlr_tasa;
    }
    
    /**
     * Setter for property vlr_tasa.
     * @param vlr_tasa New value of property vlr_tasa.
     */
    public void setVlr_tasa(double vlr_tasa) {
        this.vlr_tasa = vlr_tasa;
    }
    
    /**
     * Getter for property fecha_tasa.
     * @return Value of property fecha_tasa.
     */
    public java.lang.String getFecha_tasa() {
        return fecha_tasa;
    }
    
    /**
     * Setter for property fecha_tasa.
     * @param fecha_tasa New value of property fecha_tasa.
     */
    public void setFecha_tasa(java.lang.String fecha_tasa) {
        this.fecha_tasa = fecha_tasa;
    }
    
    /**
     * Getter for property saldo_facMI.
     * @return Value of property saldo_facMI.
     */
    public double getSaldo_facMI() {
        return saldo_facMI;
    }
    
    /**
     * Setter for property saldo_facMI.
     * @param saldo_facMI New value of property saldo_facMI.
     */
    public void setSaldo_facMI(double saldo_facMI) {
        this.saldo_facMI = saldo_facMI;
    }
    
    /**
     * Getter for property pagina.
     * @return Value of property pagina.
     */
    public java.lang.String getPagina() {
        return pagina;
    }
    
    /**
     * Setter for property pagina.
     * @param pagina New value of property pagina.
     */
    public void setPagina(java.lang.String pagina) {
        this.pagina = pagina;
    }
    
    /**
     * Getter for property tasaDB.
     * @return Value of property tasaDB.
     */
    public double getTasaDB() {
        return tasaDB;
    }
    
    /**
     * Setter for property tasaDB.
     * @param tasaDB New value of property tasaDB.
     */
    public void setTasaDB(double tasaDB) {
        this.tasaDB = tasaDB;
    }
    
    /**
     * Getter for property ciudad_cliente.
     * @return Value of property ciudad_cliente.
     */
    public java.lang.String getCiudad_cliente() {
        return ciudad_cliente;
    }
    
    /**
     * Setter for property ciudad_cliente.
     * @param ciudad_cliente New value of property ciudad_cliente.
     */
    public void setCiudad_cliente(java.lang.String ciudad_cliente) {
        this.ciudad_cliente = ciudad_cliente;
    }
    
    /**
     * Getter for property direccion_cliente.
     * @return Value of property direccion_cliente.
     */
    public java.lang.String getDireccion_cliente() {
        return direccion_cliente;
    }
    
    /**
     * Setter for property direccion_cliente.
     * @param direccion_cliente New value of property direccion_cliente.
     */
    public void setDireccion_cliente(java.lang.String direccion_cliente) {
        this.direccion_cliente = direccion_cliente;
    }
    
    /**
     * Getter for property telefono_cliente.
     * @return Value of property telefono_cliente.
     */
    public java.lang.String getTelefono_cliente() {
        return telefono_cliente;
    }
    
    /**
     * Setter for property telefono_cliente.
     * @param telefono_cliente New value of property telefono_cliente.
     */
    public void setTelefono_cliente(java.lang.String telefono_cliente) {
        this.telefono_cliente = telefono_cliente;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property abc.
     * @return Value of property abc.
     */
    public java.lang.String getAbc() {
        return abc;
    }
    
    /**
     * Setter for property abc.
     * @param abc New value of property abc.
     */
    public void setAbc(java.lang.String abc) {
        this.abc = abc;
    }
    
    /**
     * Getter for property saldo_ingreso.
     * @return Value of property saldo_ingreso.
     */
    public double getSaldo_ingreso() {
        return saldo_ingreso;
    }
    
    /**
     * Setter for property saldo_ingreso.
     * @param saldo_ingreso New value of property saldo_ingreso.
     */
    public void setSaldo_ingreso(double saldo_ingreso) {
        this.saldo_ingreso = saldo_ingreso;
    }
    
    /**
     * Getter for property valor_ingreso_temporal.
     * @return Value of property valor_ingreso_temporal.
     */
    public double getValor_ingreso_temporal() {
        return valor_ingreso_temporal;
    }
    
    /**
     * Setter for property valor_ingreso_temporal.
     * @param valor_ingreso_temporal New value of property valor_ingreso_temporal.
     */
    public void setValor_ingreso_temporal(double valor_ingreso_temporal) {
        this.valor_ingreso_temporal = valor_ingreso_temporal;
    }

    /**
     * @return the nro_extracto
     */
    public int getNro_extracto() {
        return nro_extracto;
    }

    /**
     * @param nro_extracto the nro_extracto to set
     */
    public void setNro_extracto(int nro_extracto) {
        this.nro_extracto = nro_extracto;
    }

    /**
     * @return the negaso
     */
    public String getNegaso() {
        return negaso;
    }

    /**
     * @param negaso the negaso to set
     */
    public void setNegaso(String negaso) {
        this.negaso = negaso;
    }

    /**
     * @return the numCuota
     */
    public String getNumCuota() {
        return numCuota;
    }

    /**
     * @param numCuota the numCuota to set
     */
    public void setNumCuota(String numCuota) {
        this.numCuota = numCuota;
    }
    
}