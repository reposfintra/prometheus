/********************************************************************
 *      Nombre Clase.................   ReporteMovTraficoCerromatoso.java
 *      Descripci�n..................   Bean del reporte de los movimientos
 *                                      de trafico de CerroMatoso
 *      Autor........................   Ing. Leonardo Parodi Ponce
 *      Fecha........................   14.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;

/**
 *
 * @author  EQUIPO12
 */
public class ReporteMovTraficoCerromatoso {
        
        private String numpla;
        private String numrem;
        private String nom_cliente;
        private java.util.Date creation_date;
        private String nom_tipo_reporte;
        private String nom_ubicacion;
        private String nom_zona;
        private String nom_tipo_procedencia;
        
        /** Creates a new instance of ReporteMovTraficoCerromatoso */
        public ReporteMovTraficoCerromatoso() {
        }
        
        /**
         * Toma el valor de cada uno de los campos y los asigna.
         * @autor Ing. Leonardo Parodi Ponce
         * @param rs ResultSet de la consulta.
         * @throws SQLException
         * @version 1.1
         */
        
            
        public static ReporteMovTraficoCerromatoso load(ResultSet rs)throws SQLException {
                ReporteMovTraficoCerromatoso reporteMovTraficoCerromatoso = new ReporteMovTraficoCerromatoso();
                reporteMovTraficoCerromatoso.setNumpla( rs.getString("numpla") );
                reporteMovTraficoCerromatoso.setNumrem( rs.getString("numrem") );
                reporteMovTraficoCerromatoso.setNom_cliente(rs.getString("nom_cliente"));
                reporteMovTraficoCerromatoso.setNom_tipo_procedencia(rs.getString("nom_tipo_procedencia") );
                reporteMovTraficoCerromatoso.setNom_tipo_reporte(rs.getString("nom_tipo_reporte") );
                reporteMovTraficoCerromatoso.setNom_ubicacion(rs.getString("nom_ubicacion_procedencia") );
                reporteMovTraficoCerromatoso.setNom_zona(rs.getString("nom_zona"));
                reporteMovTraficoCerromatoso.setCreation_date(rs.getTimestamp("creation_date"));
                
                return reporteMovTraficoCerromatoso;
        }
        
        /**
         * Setter for property numpla.
         * @param numpla New value of property numpla.
         */
        public void setNumpla(String Numpla){
                
                this.numpla = Numpla;
                
        }
        /**
         * Setter for property numrem.
         * @param numrem New value of property numrem.
         */
        public void setNumrem(String Numrem){
                
                this.numrem = Numrem;
                
        }
        /**
         * Setter for property nom_cliente.
         * @param nom_cliente New value of property nom_cliente.
         */
        public void setNom_cliente(String Nom_cliente){
                
                this.nom_cliente = Nom_cliente;
                
        }
        /**
         * Setter for property nom_tipo_reporte.
         * @param nom_tipo_reporte New value of property nom_tipo_reporte.
         */
        public void setNom_tipo_reporte(String Nom_tipo_reporte){
                
                this.nom_tipo_reporte = Nom_tipo_reporte;
                
        }
        /**
         * Setter for property nom_ubicacion.
         * @param nom_ubicacion New value of property nom_ubicacion.
         */
        public void setNom_ubicacion(String Nom_ubicacion){
                
                this.nom_ubicacion = Nom_ubicacion;
                
        }
        
        /**
         * Setter for property creation_date.
         * @param creation_date New value of property creation_date.
         */
        public void setCreation_date(java.util.Date Creation_date){
                
                this.creation_date = Creation_date;
                
        }
        
        /**
         * Setter for property nom_zona.
         * @param nom_zona New value of property nom_zona.
         */
        public void setNom_zona(String Nom_zona){
                
                this.nom_zona = Nom_zona;
                
        }
        
        /**
         * Setter for property nom_tipo_procedencia.
         * @param nom_tipo_procedencia New value of property nom_tipo_procedencia.
         */
        public void setNom_tipo_procedencia(String Nom_tipo_procedencia){
                
                this.nom_tipo_procedencia = Nom_tipo_procedencia;
                
        }
        
        /**
         * Getter for property numpla.
         * @return value of property numpla.
         */
        public String getNumpla(){
                
                return this.numpla;
                
        }
        
        /**
         * Getter for property numrem.
         * @return value of property numrem.
         */
        public String getNumrem(){
                
                return this.numrem;
                
        }
        
        /**
         * Getter for property nom_cliente.
         * @return value of property nom_cliente.
         */
        public String getNom_cliente(){
                
                return this.nom_cliente;
                
        }
        /**
         * Getter for property nom_tipo_reporte.
         * @return value of property nom_tipo_reporte.
         */
        public String getNom_tipo_reporte(){
                
                return this.nom_tipo_reporte;
                
        }
        
        /**
         * Getter for property creation_date.
         * @return value of property creation_date.
         */
        public java.util.Date getCreation_date(){
                
                return this.creation_date;
                
        }
        
        /**
         * Getter for property nom_ubicacion.
         * @return value of property nom_ubicacion.
         */
        public String getNom_ubicacion(){
                
               return this.nom_ubicacion;
                
        }
        
       
        /**
         * Getter for property nom_zona.
         * @return value of property nom_zona.
         */
        public String getNom_zona(){
                
                 return this.nom_zona;
                
        }
        
        
       /**
         * Getter for property nom_tipo_procedencia.
         * @return value of property nom_tipo_procedencia.
         */
        public String getNom_tipo_procedencia(){
                
               return this.nom_tipo_procedencia;
                
        }
        
}
