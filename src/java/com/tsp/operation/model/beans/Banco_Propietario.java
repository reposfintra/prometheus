/*
 * Cliente.java
 *
 * Created on 20 de abril de 2005, 09:08 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Henry
 */  
public class Banco_Propietario {
    
    private String Distrito;
    private String Propietario;
    private String Hc;
    private String Banco;
    private String Sucursal;
    private String reg_status;
    private String creation_date;
    private String creation_user;
    private String base;
    private String descripcionHc;
    
    /** Creates a new instance of Cliente */
    public Banco_Propietario() {
    }
    public Banco_Propietario(boolean b) {
        this.Distrito ="";
        this.Propietario ="";
        this.Hc ="";
        this.Banco ="";
        this.Sucursal ="";
        this.reg_status ="";
        this.creation_date ="";
        this.creation_user ="";
        this.base ="";
        
        
    }
   
    /**
     * Getter for property Distrito.
     * @return Value of property Distrito.
     */
    public java.lang.String getDistrito() {
        return Distrito;
    }
    
    /**
     * Setter for property Distrito.
     * @param Distrito New value of property Distrito.
     */
    public void setDistrito(java.lang.String Distrito) {
        this.Distrito = Distrito;
    }
    
    /**
     * Getter for property Propietario.
     * @return Value of property Propietario.
     */
    public java.lang.String getPropietario() {
        return Propietario;
    }
    
    /**
     * Setter for property Propietario.
     * @param Propietario New value of property Propietario.
     */
    public void setPropietario(java.lang.String Propietario) {
        this.Propietario = Propietario;
    }
    
    /**
     * Getter for property Hc.
     * @return Value of property Hc.
     */
    public java.lang.String getHc() {
        return Hc;
    }
    
    /**
     * Setter for property Hc.
     * @param Hc New value of property Hc.
     */
    public void setHc(java.lang.String Hc) {
        this.Hc = Hc;
    }
    
    /**
     * Getter for property Banco.
     * @return Value of property Banco.
     */
    public java.lang.String getBanco() {
        return Banco;
    }
    
    /**
     * Setter for property Banco.
     * @param Banco New value of property Banco.
     */
    public void setBanco(java.lang.String Banco) {
        this.Banco = Banco;
    }
    
    /**
     * Getter for property Sucursal.
     * @return Value of property Sucursal.
     */
    public java.lang.String getSucursal() {
        return Sucursal;
    }
    
    /**
     * Setter for property Sucursal.
     * @param Sucursal New value of property Sucursal.
     */
    public void setSucursal(java.lang.String Sucursal) {
        this.Sucursal = Sucursal;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property descripcionHc.
     * @return Value of property descripcionHc.
     */
    public java.lang.String getDescripcionHc() {
        return descripcionHc;
    }
    
    /**
     * Setter for property descripcionHc.
     * @param descripcionHc New value of property descripcionHc.
     */
    public void setDescripcionHc(java.lang.String descripcionHc) {
        this.descripcionHc = descripcionHc;
    }
    
}    

