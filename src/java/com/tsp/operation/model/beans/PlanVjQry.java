/*
 * planVjQry.java
 *
 * Created on 10 de diciembre de 2004, 05:49 PM
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;

/**
 *
 * @author  AMENDEZ
 */
public class PlanVjQry implements Serializable{
    private String desczona = ""; 
    private String nomciu = ""; 
    private String fecha = "";
    private String planilla = ""; 
    private String placa = ""; 
    private String descripcion = "";
    private String leido = "";
    private String clase = "";
    private String dpto = "";
    private String cia = "";
    
    /** Creates a new instance of planVjQry */
    public PlanVjQry() {
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }    
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }    
    
    /**
     * Getter for property desczona.
     * @return Value of property desczona.
     */
    public java.lang.String getDesczona() {
        return desczona;
    }
    
    /**
     * Setter for property desczona.
     * @param desczona New value of property desczona.
     */
    public void setDesczona(java.lang.String desczona) {
        this.desczona = desczona;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property nomciu.
     * @return Value of property nomciu.
     */
    public java.lang.String getNomciu() {
        return nomciu;
    }
    
    /**
     * Setter for property nomciu.
     * @param nomciu New value of property nomciu.
     */
    public void setNomciu(java.lang.String nomciu) {
        this.nomciu = nomciu;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla){
        this.planilla = planilla;
    }
    
    public static PlanVjQry load(ResultSet rs) throws SQLException{
        String value = null;
        PlanVjQry planVjQry = new PlanVjQry();

        value = rs.getString("desczona");
        if(value != null) planVjQry.setDesczona(value);

        value = rs.getString("nomciu");
        if(value != null) planVjQry.setNomciu(value);

        value = rs.getString("fecha");
        if(value != null) planVjQry.setFecha(value);

        value = rs.getString("planilla");
        if(value != null) planVjQry.setPlanilla(value);

        value = rs.getString("placa");
        if(value != null) planVjQry.setPlaca(value);

        value = rs.getString("descripcion");
        if(value != null) planVjQry.setDescripcion(value);
               
        value = rs.getString("leido");
        if(value != null) planVjQry.setLeido(value);
        
        value = rs.getString("cia");
        if(value != null) planVjQry.setCia(value);
        return planVjQry;        
    }
    
    /**
     * Getter for property leido.
     * @return Value of property leido.
     */
    public String getLeido() {
        return leido;
    }
    
    /**
     * Setter for property leido.
     * @param leido New value of property leido.
     */
    public void setLeido(String leido) {
        this.leido = leido;
    }
    
    /**
     * Getter for property clase.
     * @return Value of property clase.
     */
    public String getClase() {
        return clase;
    }
    
    /**
     * Setter for property clase.
     * @param clase New value of property clase.
     */
    public void setClase(String clase) {
        this.clase = clase;
    }
    
    /**
     * Getter for property dpto.
     * @return Value of property dpto.
     */
    public String getDpto() {
        return dpto;
    }
    
    /**
     * Setter for property dpto.
     * @param dpto New value of property dpto.
     */
    public void setDpto(String dpto) {
        this.dpto = dpto;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
}
