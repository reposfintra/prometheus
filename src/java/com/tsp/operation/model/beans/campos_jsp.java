/*
 * campos_jsp.java
 *
 * Created on 30 de junio de 2005, 14:23
 */

package com.tsp.operation.model.beans;


/**
 *
 * @author  DRIGO
 */
import java.io.*;
import java.sql.*;
import java.util.*;

public class campos_jsp implements Serializable{
    
    private String pagina;
    private String campo;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    private String tipo_campo;//Tito Andr�s 12.11.2005
    
    public static campos_jsp load(ResultSet rs)throws SQLException{
        campos_jsp t = new campos_jsp();
        t.setPagina(rs.getString("pagina"));
        t.setCampo(rs.getString("campo"));
        t.setCia(rs.getString("cia"));
        t.setRec_status(rs.getString("rec_status"));
        t.setLast_update(rs.getDate("last_update"));
        t.setUser_update(rs.getString("user_update"));
        t.setCreation_date(rs.getDate("creation_date"));
        t.setCreation_user(rs.getString("creation_user"));
        t.setTipo_campo(rs.getString("tipo_campo"));
        return t;
    }
    
    public java.lang.String getCia() {
        return cia;
    }
    
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    public java.lang.String getPagina() {
        return pagina;
    }
    
    public void setPagina(java.lang.String pagina) {
        this.pagina = pagina;
    }
    
    public java.util.Date getCreation_date() {
        return creation_date;
    }
    
    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }
    
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    public java.lang.String getCampo() {
        return campo;
    }
    
    public void setCampo(java.lang.String campo) {
        this.campo = campo;
    }
    
    public java.util.Date getLast_update() {
        return last_update;
    }
    
    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }
    
    public java.lang.String getRec_status() {
        return rec_status;
    }
    
    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property tipo_campo.
     * @return Value of property tipo_campo.
     */
    public java.lang.String getTipo_campo() {
        return tipo_campo;
    }
    
    /**
     * Setter for property tipo_campo.
     * @param tipo_campo New value of property tipo_campo.
     */
    public void setTipo_campo(java.lang.String tipo_campo) {
        this.tipo_campo = tipo_campo;
    }
    
}
