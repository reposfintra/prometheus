/*
 * veh_externos.java
 *
 * Created on 30 de julio de 2005, 12:25 PM
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  jescandon
 */
public class veh_externos implements Serializable {
    
    private String placa;
    private java.sql.Timestamp fecha_disp;
    private int tiempo_vigencia;
    private String agencia_disp;
    private String base;
    private String estado;
    
    /** Creates a new instance of veh_externos */
    public veh_externos() {
    }
    
    
    public static veh_externos load(ResultSet rs) throws SQLException{
        veh_externos datos = new veh_externos();
        datos.setPlaca(rs.getString(1));
        datos.setFecha_disp(rs.getTimestamp(2));
        datos.setTiempo_vigencia(rs.getInt(3));
        datos.setAgencia_disp(rs.getString(4));
        datos.setBase(rs.getString(5));
        datos.setEstado(rs.getString(6));
        return datos;
    }
    /**
     * Getter for property agencia_disp.
     * @return Value of property agencia_disp.
     */
    public java.lang.String getAgencia_disp() {
        return agencia_disp;
    }
    
    /**
     * Setter for property agencia_disp.
     * @param agencia_disp New value of property agencia_disp.
     */
    public void setAgencia_disp(java.lang.String agencia_disp) {
        this.agencia_disp = agencia_disp;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property fecha_disp.
     * @return Value of property fecha_disp.
     */
    public java.sql.Timestamp getFecha_disp() {
        return fecha_disp;
    }
    
    /**
     * Setter for property fecha_disp.
     * @param fecha_disp New value of property fecha_disp.
     */
    public void setFecha_disp(java.sql.Timestamp fecha_disp) {
        this.fecha_disp = fecha_disp;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property tiempo_vigencia.
     * @return Value of property tiempo_vigencia.
     */
    public int getTiempo_vigencia() {
        return tiempo_vigencia;
    }
    
    /**
     * Setter for property tiempo_vigencia.
     * @param tiempo_vigencia New value of property tiempo_vigencia.
     */
    public void setTiempo_vigencia(int tiempo_vigencia) {
        this.tiempo_vigencia = tiempo_vigencia;
    }
    
}
