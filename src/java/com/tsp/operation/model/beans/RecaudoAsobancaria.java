/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 * Bean para manejar cabecera subida archivo recaudo Asobancaria<br/>
 * 30/03/2015
 * @author mcastillo
 */
public class RecaudoAsobancaria {
    
    private int id;
    private String facturadora_nit;
    private String fecha_recaudo;
    private int recaudadora_cod;
    private String cuenta_cli;
    private String fecha_archivo;
    private String modificador;
    private String tipo_cuenta;
    private int num_lotes;
    private int total_registros;
    private double valor_total;
    private int estado;
    private String fecha_creacion;
    private String usuario_creacion;
    private String fecha_modificacion;
    private String usuario_modificacion;
    private String entidad_recaudo;
    private int total_encontrados;
    private int total_procesados;
    private double valor_aplicado;
    private double total_recaudo_cheque;
    private double numero_lote;
      
    public RecaudoAsobancaria() {
    }

    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the facturadora_nit
     */
    public String getFacturadora_nit() {
        return facturadora_nit;
    }

    /**
     * @param facturadora_nit the facturadora_nit to set
     */
    public void setFacturadora_nit(String facturadora_nit) {
        this.facturadora_nit = facturadora_nit;
    }

    /**
     * @return the fecha_recaudo
     */
    public String getFecha_recaudo() {
        return fecha_recaudo;
    }

    /**
     * @param fecha_recaudo the fecha_recaudo to set
     */
    public void setFecha_recaudo(String fecha_recaudo) {
        this.fecha_recaudo = fecha_recaudo;
    }

    /**
     * @return the recaudadora_cod
     */
    public int getRecaudadora_cod() {
        return recaudadora_cod;
    }

    /**
     * @param recaudadora_cod the recaudadora_cod to set
     */
    public void setRecaudadora_cod(int recaudadora_cod) {
        this.recaudadora_cod = recaudadora_cod;
    }

    /**
     * @return the cuenta_cli
     */
    public String getCuenta_cli() {
        return cuenta_cli;
    }

    /**
     * @param cuenta_cli the cuenta_cli to set
     */
    public void setCuenta_cli(String cuenta_cli) {
        this.cuenta_cli = cuenta_cli;
    }

    /**
     * @return the fecha_archivo
     */
    public String getFecha_archivo() {
        return fecha_archivo;
    }

    /**
     * @param fecha_archivo the fecha_archivo to set
     */
    public void setFecha_archivo(String fecha_archivo) {
        this.fecha_archivo = fecha_archivo;
    }

    /**
     * @return the modificador
     */
    public String getModificador() {
        return modificador;
    }

    /**
     * @param modificador the modificador to set
     */
    public void setModificador(String modificador) {
        this.modificador = modificador;
    }

    /**
     * @return the tipo_cuenta
     */
    public String getTipo_cuenta() {
        return tipo_cuenta;
    }

    /**
     * @param tipo_cuenta the tipo_cuenta to set
     */
    public void setTipo_cuenta(String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    /**
     * @return the num_lotes
     */
    public int getNum_lotes() {
        return num_lotes;
    }

    /**
     * @param num_lotes the num_lotes to set
     */
    public void setNum_lotes(int num_lotes) {
        this.num_lotes = num_lotes;
    }

    /**
     * @return the total_registros
     */
    public int getTotal_registros() {
        return total_registros;
    }

    /**
     * @param total_registros the total_registros to set
     */
    public void setTotal_registros(int total_registros) {
        this.total_registros = total_registros;
    }

    /**
     * @return the valor_total
     */
    public double getValor_total() {
        return valor_total;
    }

    /**
     * @param valor_total the valor_total to set
     */
    public void setValor_total(double valor_total) {
        this.valor_total = valor_total;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    /**
     * @return the fecha_creacion
     */
    public String getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion the fecha_creacion to set
     */
    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return the usuario_creacion
     */
    public String getUsuario_creacion() {
        return usuario_creacion;
    }

    /**
     * @param usuario_creacion the usuario_creacion to set
     */
    public void setUsuario_creacion(String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    /**
     * @return the fecha_modificacion
     */
    public String getFecha_modificacion() {
        return fecha_modificacion;
    }

    /**
     * @param fecha_modificacion the fecha_modificacion to set
     */
    public void setFecha_modificacion(String fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    /**
     * @return the usuario_modificacion
     */
    public String getUsuario_modificacion() {
        return usuario_modificacion;
    }

    /**
     * @param usuario_modificacion the usuario_modificacion to set
     */
    public void setUsuario_modificacion(String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }

    /**
     * @return the entidad_recaudo
     */
    public String getEntidad_recaudo() {
        return entidad_recaudo;
    }

    /**
     * @param entidad_recaudo the entidad_recaudo to set
     */
    public void setEntidad_recaudo(String entidad_recaudo) {
        this.entidad_recaudo = entidad_recaudo;
    }

    /**
     * @return the total_encontrados
     */
    public int getTotal_encontrados() {
        return total_encontrados;
    }

    /**
     * @param total_encontrados the total_encontrados to set
     */
    public void setTotal_encontrados(int total_encontrados) {
        this.total_encontrados = total_encontrados;
    }

    /**
     * @return the total_procesados
     */
    public int getTotal_procesados() {
        return total_procesados;
    }

    /**
     * @param total_procesados the total_procesados to set
     */
    public void setTotal_procesados(int total_procesados) {
        this.total_procesados = total_procesados;
    }

    /**
     * @return the valor_aplicado
     */
    public double getValor_aplicado() {
        return valor_aplicado;
    }

    /**
     * @param valor_aplicado the valor_aplicado to set
     */
    public void setValor_aplicado(double valor_aplicado) {
        this.valor_aplicado = valor_aplicado;
    }

    public double getTotal_recaudo_cheque() {
        return total_recaudo_cheque;
    }

    public void setTotal_recaudo_cheque(double total_recaudo_cheque) {
        this.total_recaudo_cheque = total_recaudo_cheque;
    }

    public double getNumero_lote() {
        return numero_lote;
    }

    public void setNumero_lote(double numero_lote) {
        this.numero_lote = numero_lote;
    }

    
}
