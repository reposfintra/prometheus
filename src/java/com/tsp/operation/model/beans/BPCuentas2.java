/*
 * BPCuentas2.java
 *
 * Created on 18 de abril de 2007, 09:08 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Osvaldo P�rez Ferrer
 */
public class BPCuentas2 {
    
    
    private String elemento;
    private String desc_elemento;
    private String unidad;
    private String desc_unidad;
    private String grupo;
    private String cuenta;
    private String desc_cuenta;
    private double vlr_saldo_anterior;
    private double vlr_debito;
    private double vlr_credito;
    private double vlr_saldo_actual;
    
    /** Creates a new instance of BPCuentas2 */
    public BPCuentas2() {
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property desc_cuenta.
     * @return Value of property desc_cuenta.
     */
    public java.lang.String getDesc_cuenta() {
        return desc_cuenta;
    }
    
    /**
     * Setter for property desc_cuenta.
     * @param desc_cuenta New value of property desc_cuenta.
     */
    public void setDesc_cuenta(java.lang.String desc_cuenta) {
        this.desc_cuenta = desc_cuenta;
    }
    
    /**
     * Getter for property desc_elemento.
     * @return Value of property desc_elemento.
     */
    public java.lang.String getDesc_elemento() {
        return desc_elemento;
    }
    
    /**
     * Setter for property desc_elemento.
     * @param desc_elemento New value of property desc_elemento.
     */
    public void setDesc_elemento(java.lang.String desc_elemento) {
        this.desc_elemento = desc_elemento;
    }
    
    /**
     * Getter for property desc_unidad.
     * @return Value of property desc_unidad.
     */
    public java.lang.String getDesc_unidad() {
        return desc_unidad;
    }
    
    /**
     * Setter for property desc_unidad.
     * @param desc_unidad New value of property desc_unidad.
     */
    public void setDesc_unidad(java.lang.String desc_unidad) {
        this.desc_unidad = desc_unidad;
    }
    
    /**
     * Getter for property elemento.
     * @return Value of property elemento.
     */
    public java.lang.String getElemento() {
        return elemento;
    }
    
    /**
     * Setter for property elemento.
     * @param elemento New value of property elemento.
     */
    public void setElemento(java.lang.String elemento) {
        this.elemento = elemento;
    }
    
    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property vlr_credito.
     * @return Value of property vlr_credito.
     */
    public double getVlr_credito() {
        return vlr_credito;
    }
    
    /**
     * Setter for property vlr_credito.
     * @param vlr_credito New value of property vlr_credito.
     */
    public void setVlr_credito(double vlr_credito) {
        this.vlr_credito = vlr_credito;
    }
    
    /**
     * Getter for property vlr_debito.
     * @return Value of property vlr_debito.
     */
    public double getVlr_debito() {
        return vlr_debito;
    }
    
    /**
     * Setter for property vlr_debito.
     * @param vlr_debito New value of property vlr_debito.
     */
    public void setVlr_debito(double vlr_debito) {
        this.vlr_debito = vlr_debito;
    }
    
    /**
     * Getter for property vlr_saldo_actual.
     * @return Value of property vlr_saldo_actual.
     */
    public double getVlr_saldo_actual() {
        return vlr_saldo_actual;
    }
    
    /**
     * Setter for property vlr_saldo_actual.
     * @param vlr_saldo_actual New value of property vlr_saldo_actual.
     */
    public void setVlr_saldo_actual(double vlr_saldo_actual) {
        this.vlr_saldo_actual = vlr_saldo_actual;
    }
    
    /**
     * Getter for property vlr_saldo_anterior.
     * @return Value of property vlr_saldo_anterior.
     */
    public double getVlr_saldo_anterior() {
        return vlr_saldo_anterior;
    }
    
    /**
     * Setter for property vlr_saldo_anterior.
     * @param vlr_saldo_anterior New value of property vlr_saldo_anterior.
     */
    public void setVlr_saldo_anterior(double vlr_saldo_anterior) {
        this.vlr_saldo_anterior = vlr_saldo_anterior;
    }
    
    /**
     * Getter for property grupo.
     * @return Value of property grupo.
     */
    public java.lang.String getGrupo() {
        return grupo;
    }
    
    /**
     * Setter for property grupo.
     * @param grupo New value of property grupo.
     */
    public void setGrupo(java.lang.String grupo) {
        this.grupo = grupo;
    }
    
}
