/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class PrefacturaContratistaDetalle {

   private String  id_accion;
   private String  id_orden;
   private String  consecutivo;
   private String  id_contratista;
   private String  prefactura;
   private  double vlr_mat;
   private  double vlr_mob;
   private  double vlr_otr;
   private double  comision_ejecutiva;
   private double  comision_canal;
   
   private  String codigo_iva;
   private  double vlr_iva;
   private  String simbolo_variable;
   private  String factura_conformada;
   private double  eca_oferta;
   private double  oferta;
   private String  f_facturado_cliente;
   private  String codigo_rmat;
   private  String codigo_rmob;
   private  String codigo_rotr;
   private  double vlr_rmat;
   private  double vlr_rmob;
   private  double vlr_rotr;
   private  float  porcentaje_iva;
   private  float  porcentaje_rmat;
   private  float  porcentaje_rmob;
   private  float  porcentaje_rotr;
   private  double valor_factoring;
   private  float  porcentaje_formula;
   private  double valor_formula;

   private  int    cuotas_reales;
   private  String tipo_dtf;
   private String  num_os;
   private  double porcentaje_factoring;


   /** Creates a new instance of PrefacturaContratistaDetalle */
   public PrefacturaContratistaDetalle() {
   }



    public static PrefacturaContratistaDetalle load(java.sql.ResultSet rs)throws java.sql.SQLException{

        PrefacturaContratistaDetalle prefacturaContratistaDetalle = new PrefacturaContratistaDetalle();
        
        prefacturaContratistaDetalle.setId_accion( rs.getString("id_accion") );
        prefacturaContratistaDetalle.setId_orden(rs.getString("id_orden") );
        prefacturaContratistaDetalle.setConsecutivo( rs.getString("consecutivo") );        
        prefacturaContratistaDetalle.setId_contratista( rs.getString("id_contratista") );
        prefacturaContratistaDetalle.setPrefactura( rs.getString("prefactura") );
        prefacturaContratistaDetalle.setVlr_mat(rs.getDouble("vlr_mat"));
        prefacturaContratistaDetalle.setVlr_mob(rs.getDouble("vlr_mob"));
        prefacturaContratistaDetalle.setVlr_otr(rs.getDouble("vlr_otr"));
        prefacturaContratistaDetalle.setComision_canal(rs.getDouble("comision_canal"));
        prefacturaContratistaDetalle.setComision_ejecutiva(rs.getDouble("comision_ejecutiva"));
        
        prefacturaContratistaDetalle.setCodigo_iva(rs.getString("codigo_iva"));  
        prefacturaContratistaDetalle.setVlr_iva(rs.getDouble("vlr_iva"));         
        prefacturaContratistaDetalle.setSimbolo_variable(rs.getString("simbolo_variable"));
        prefacturaContratistaDetalle.setFactura_conformada(rs.getString("factura_conformada"));        
        prefacturaContratistaDetalle.setEca_oferta(rs.getDouble("eca_oferta")) ;
        prefacturaContratistaDetalle.setOferta(rs.getDouble("oferta")) ;
        prefacturaContratistaDetalle.setF_facturado_cliente(rs.getString("f_facturado_cliente")) ;
        prefacturaContratistaDetalle.setCodigo_rmat(rs.getString("codigo_rmat"));
        prefacturaContratistaDetalle.setCodigo_rmob(rs.getString("codigo_rmob"));
        prefacturaContratistaDetalle.setCodigo_rotr(rs.getString("codigo_rotr"));       
        prefacturaContratistaDetalle.setVlr_rmat(rs.getDouble("vlr_rmat"));
        prefacturaContratistaDetalle.setVlr_rmob(rs.getDouble("vlr_rmob"));
        prefacturaContratistaDetalle.setVlr_rotr(rs.getDouble("vlr_rotr"));

        prefacturaContratistaDetalle.setPorcentaje_iva(rs.getFloat("porcentaje_iva"));
        prefacturaContratistaDetalle.setPorcentaje_rmat(rs.getFloat("porcentaje_rmat"));
        prefacturaContratistaDetalle.setPorcentaje_rmob(rs.getFloat("porcentaje_rmob"));
        prefacturaContratistaDetalle.setPorcentaje_rotr(rs.getFloat("porcentaje_rotr"));
        prefacturaContratistaDetalle.setValor_factoring(rs.getDouble("vlr_factoring"));       
        prefacturaContratistaDetalle.setPorcentaje_formula(rs.getFloat("porcentaje_formula"));      
        prefacturaContratistaDetalle.setValor_formula(rs.getDouble("vlr_formula"));
        
        prefacturaContratistaDetalle.setCuotas_reales(rs.getInt("cuotas_reales"));
        prefacturaContratistaDetalle.setTipo_dtf(rs.getString("tipo_dtf"));
        prefacturaContratistaDetalle.setCuotas_reales(rs.getInt("cuotas_reales"));
        prefacturaContratistaDetalle.setTipo_dtf(rs.getString("tipo_dtf"));
        prefacturaContratistaDetalle.setNum_os(rs.getString("numos"));      
        prefacturaContratistaDetalle.setPorcentaje_factoring(rs.getString("porcentaje_factoring"));
        
        return prefacturaContratistaDetalle;
    }







    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the id_orden
     */
    public String getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(String id_orden) {
        this.id_orden = id_orden;
    }

    /**
     * @return the consecutivo
     */
    public String getConsecutivo() {
        return consecutivo;
    }

    /**
     * @param consecutivo the consecutivo to set
     */
    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the prefactura
     */
    public String getPrefactura() {
        return prefactura;
    }

    /**
     * @param prefactura the prefactura to set
     */
    public void setPrefactura(String prefactura) {
        this.prefactura = prefactura;
    }

    /**
     * @return the vlr_mat
     */
    public double getVlr_mat() {
        return vlr_mat;
    }

    /**
     * @param vlr_mat the vlr_mat to set
     */
    public void setVlr_mat(double vlr_mat) {
        this.vlr_mat = vlr_mat;
    }

    /**
     * @return the vlr_mob
     */
    public double getVlr_mob() {
        return vlr_mob;
    }

    /**
     * @param vlr_mob the vlr_mob to set
     */
    public void setVlr_mob(double vlr_mob) {
        this.vlr_mob = vlr_mob;
    }

    /**
     * @return the vlr_otr
     */
    public double getVlr_otr() {
        return vlr_otr;
    }

    /**
     * @param vlr_otr the vlr_otr to set
     */
    public void setVlr_otr(double vlr_otr) {
        this.vlr_otr = vlr_otr;
    }

    /**
     * @return the comision_ejecutiva
     */
    public double getComision_ejecutiva() {
        return comision_ejecutiva;
    }

    /**
     * @param comision_ejecutiva the comision_ejecutiva to set
     */
    public void setComision_ejecutiva(double comision_ejecutiva) {
        this.comision_ejecutiva = comision_ejecutiva;
    }

    /**
     * @return the comision_canal
     */
    public double getComision_canal() {
        return comision_canal;
    }

    /**
     * @param comision_canal the comision_canal to set
     */
    public void setComision_canal(double comision_canal) {
        this.comision_canal = comision_canal;
    }

    /**
     * @return the codigo_iva
     */
    public String getCodigo_iva() {
        return codigo_iva;
    }

    /**
     * @param codigo_iva the codigo_iva to set
     */
    public void setCodigo_iva(String codigo_iva) {
        this.codigo_iva = codigo_iva;
    }

    /**
     * @return the vlr_iva
     */
    public double getVlr_iva() {
        return vlr_iva;
    }

    /**
     * @param vlr_iva the vlr_iva to set
     */
    public void setVlr_iva(double vlr_iva) {
        this.vlr_iva = vlr_iva;
    }

    /**
     * @return the simbolo_variable
     */
    public String getSimbolo_variable() {
        return simbolo_variable;
    }

    /**
     * @param simbolo_variable the simbolo_variable to set
     */
    public void setSimbolo_variable(String simbolo_variable) {
        this.simbolo_variable = simbolo_variable;
    }

    /**
     * @return the factura_conformada
     */
    public String getFactura_conformada() {
        return factura_conformada;
    }

    /**
     * @param factura_conformada the factura_conformada to set
     */
    public void setFactura_conformada(String factura_conformada) {
        this.factura_conformada = factura_conformada;
    }

    /**
     * @return the eca_oferta
     */
    public double getEca_oferta() {
        return eca_oferta;
    }

    /**
     * @param eca_oferta the eca_oferta to set
     */
    public void setEca_oferta(double eca_oferta) {
        this.eca_oferta = eca_oferta;
    }

    /**
     * @return the oferta
     */
    public double getOferta() {
        return oferta;
    }

    /**
     * @param oferta the oferta to set
     */
    public void setOferta(double oferta) {
        this.oferta = oferta;
    }

    /**
     * @return the f_facturado_cliente
     */
    public String getF_facturado_cliente() {
        return f_facturado_cliente;
    }

    /**
     * @param f_facturado_cliente the f_facturado_cliente to set
     */
    public void setF_facturado_cliente(String f_facturado_cliente) {
        this.f_facturado_cliente = f_facturado_cliente;
    }

    /**
     * @return the codigo_rmat
     */
    public String getCodigo_rmat() {
        return codigo_rmat;
    }

    /**
     * @param codigo_rmat the codigo_rmat to set
     */
    public void setCodigo_rmat(String codigo_rmat) {
        this.codigo_rmat = codigo_rmat;
    }

    /**
     * @return the codigo_rmob
     */
    public String getCodigo_rmob() {
        return codigo_rmob;
    }

    /**
     * @param codigo_rmob the codigo_rmob to set
     */
    public void setCodigo_rmob(String codigo_rmob) {
        this.codigo_rmob = codigo_rmob;
    }

    /**
     * @return the codigo_rotr
     */
    public String getCodigo_rotr() {
        return codigo_rotr;
    }

    /**
     * @param codigo_rotr the codigo_rotr to set
     */
    public void setCodigo_rotr(String codigo_rotr) {
        this.codigo_rotr = codigo_rotr;
    }

    /**
     * @return the vlr_rmat
     */
    public double getVlr_rmat() {
        return vlr_rmat;
    }

    /**
     * @param vlr_rmat the vlr_rmat to set
     */
    public void setVlr_rmat(double vlr_rmat) {
        this.vlr_rmat = vlr_rmat;
    }

    /**
     * @return the vlr_rmob
     */
    public double getVlr_rmob() {
        return vlr_rmob;
    }

    /**
     * @param vlr_rmob the vlr_rmob to set
     */
    public void setVlr_rmob(double vlr_rmob) {
        this.vlr_rmob = vlr_rmob;
    }

    /**
     * @return the vlr_rotr
     */
    public double getVlr_rotr() {
        return vlr_rotr;
    }

    /**
     * @param vlr_rotr the vlr_rotr to set
     */
    public void setVlr_rotr(double vlr_rotr) {
        this.vlr_rotr = vlr_rotr;
    }

    /**
     * @return the porcentaje_iva
     */
    public float getPorcentaje_iva() {
        return porcentaje_iva;
    }

    /**
     * @param porcentaje_iva the porcentaje_iva to set
     */
    public void setPorcentaje_iva(float porcentaje_iva) {
        this.porcentaje_iva = porcentaje_iva;
    }

    /**
     * @return the porcentaje_rmat
     */
    public float getPorcentaje_rmat() {
        return porcentaje_rmat;
    }

    /**
     * @param porcentaje_rmat the porcentaje_rmat to set
     */
    public void setPorcentaje_rmat(float porcentaje_rmat) {
        this.porcentaje_rmat = porcentaje_rmat;
    }

    /**
     * @return the porcentaje_rmob
     */
    public float getPorcentaje_rmob() {
        return porcentaje_rmob;
    }

    /**
     * @param porcentaje_rmob the porcentaje_rmob to set
     */
    public void setPorcentaje_rmob(float porcentaje_rmob) {
        this.porcentaje_rmob = porcentaje_rmob;
    }

    /**
     * @return the porcentaje_rotr
     */
    public float getPorcentaje_rotr() {
        return porcentaje_rotr;
    }

    /**
     * @param porcentaje_rotr the porcentaje_rotr to set
     */
    public void setPorcentaje_rotr(float porcentaje_rotr) {
        this.porcentaje_rotr = porcentaje_rotr;
    }

    /**
     * @return the valor_factoring
     */
    public double getValor_factoring() {
        return valor_factoring;
    }

    /**
     * @param valor_factoring the valor_factoring to set
     */
    public void setValor_factoring(double valor_factoring) {
        this.valor_factoring = valor_factoring;
    }

    /**
     * @return the porcentaje_formula
     */
    public float getPorcentaje_formula() {
        return porcentaje_formula;
    }

    /**
     * @param porcentaje_formula the porcentaje_formula to set
     */
    public void setPorcentaje_formula(float porcentaje_formula) {
        this.porcentaje_formula = porcentaje_formula;
    }

    /**
     * @return the valor_formula
     */
    public double getValor_formula() {
        return valor_formula;
    }

    /**
     * @param valor_formula the valor_formula to set
     */
    public void setValor_formula(double valor_formula) {
        this.valor_formula = valor_formula;
    }

    /**
     * @return the cuotas_reales
     */
    public int getCuotas_reales() {
        return cuotas_reales;
    }

    /**
     * @param cuotas_reales the cuotas_reales to set
     */
    public void setCuotas_reales(int cuotas_reales) {
        this.cuotas_reales = cuotas_reales;
    }

    /**
     * @return the tipo_dtf
     */
    public String getTipo_dtf() {
        return tipo_dtf;
    }

    /**
     * @param tipo_dtf the tipo_dtf to set
     */
    public void setTipo_dtf(String tipo_dtf) {
        this.tipo_dtf = tipo_dtf;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the porcentaje_factoring
     */
    public double getPorcentaje_factoring() {
        return porcentaje_factoring;
    }

    /**
     * @param porcentaje_factoring the porcentaje_factoring to set
     */
    public void setPorcentaje_factoring(String porcentaje_factoring) {
        double numero=Double.valueOf(porcentaje_factoring).doubleValue();
        this.porcentaje_factoring = numero;
    }

}
