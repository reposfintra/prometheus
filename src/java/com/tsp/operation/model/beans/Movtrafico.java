/*
 * Movtrafico.java
 *
 * Created on 16 de junio de 2005, 10:54 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Jm
 */
public class Movtrafico implements Serializable{
    private String distrito;
    private String tipomov;
    private String numpla;
    private String fechaentrega;
    private String horaentrega;
    /** Creates a new instance of Movtrafico */
    public Movtrafico() {
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fechaentrega.
     * @return Value of property fechaentrega.
     */
    public java.lang.String getFechaentrega() {
        return fechaentrega;
    }
    
    /**
     * Setter for property fechaentrega.
     * @param fechaentrega New value of property fechaentrega.
     */
    public void setFechaentrega(java.lang.String fechaentrega) {
        this.fechaentrega = fechaentrega;
    }
    
    /**
     * Getter for property horaentrega.
     * @return Value of property horaentrega.
     */
    public java.lang.String getHoraentrega() {
        return horaentrega;
    }
    
    /**
     * Setter for property horaentrega.
     * @param horaentrega New value of property horaentrega.
     */
    public void setHoraentrega(java.lang.String horaentrega) {
        this.horaentrega = horaentrega;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property tipomov.
     * @return Value of property tipomov.
     */
    public java.lang.String getTipomov() {
        return tipomov;
    }
    
    /**
     * Setter for property tipomov.
     * @param tipomov New value of property tipomov.
     */
    public void setTipomov(java.lang.String tipomov) {
        this.tipomov = tipomov;
    }
    
}
