/***********************************************
 * Nombre: Acuerdo_especial.java
 * Descripci�n: Beans de descuento.
 * Autor: Ing. Jose de la rosa
 * Fecha: 12 de diciembre de 2005, 08:53 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ***********************************************/

package com.tsp.operation.model.beans;

public class Descuento {
    private String placa_trailer;
    private String placa_cabezote;
    private String num_planilla;
    private String num_equipo;
    private String std_job_no;
    private String codigo_concepto;
    private String proveedor;
    private double valor;
    private String clase_equipo;
    private String tipo_acuerdo;
    private String usuario_creacion;
    private String fecha_proceso;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String base;
    private String distrito;
    private String rec_status;      
    /** Creates a new instance of Descuento */
    public Descuento () {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property clase_equipo.
     * @return Value of property clase_equipo.
     */
    public java.lang.String getClase_equipo () {
        return clase_equipo;
    }
    
    /**
     * Setter for property clase_equipo.
     * @param clase_equipo New value of property clase_equipo.
     */
    public void setClase_equipo (java.lang.String clase_equipo) {
        this.clase_equipo = clase_equipo;
    }
    
    /**
     * Getter for property codigo_concepto.
     * @return Value of property codigo_concepto.
     */
    public java.lang.String getCodigo_concepto () {
        return codigo_concepto;
    }
    
    /**
     * Setter for property codigo_concepto.
     * @param codigo_concepto New value of property codigo_concepto.
     */
    public void setCodigo_concepto (java.lang.String codigo_concepto) {
        this.codigo_concepto = codigo_concepto;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion () {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion (java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property num_planilla.
     * @return Value of property num_planilla.
     */
    public java.lang.String getNum_planilla () {
        return num_planilla;
    }
    
    /**
     * Setter for property num_planilla.
     * @param num_planilla New value of property num_planilla.
     */
    public void setNum_planilla (java.lang.String num_planilla) {
        this.num_planilla = num_planilla;
    }
    
    /**
     * Getter for property placa_cabezote.
     * @return Value of property placa_cabezote.
     */
    public java.lang.String getPlaca_cabezote () {
        return placa_cabezote;
    }
    
    /**
     * Setter for property placa_cabezote.
     * @param placa_cabezote New value of property placa_cabezote.
     */
    public void setPlaca_cabezote (java.lang.String placa_cabezote) {
        this.placa_cabezote = placa_cabezote;
    }
    
    /**
     * Getter for property placa_trailer.
     * @return Value of property placa_trailer.
     */
    public java.lang.String getPlaca_trailer () {
        return placa_trailer;
    }
    
    /**
     * Setter for property placa_trailer.
     * @param placa_trailer New value of property placa_trailer.
     */
    public void setPlaca_trailer (java.lang.String placa_trailer) {
        this.placa_trailer = placa_trailer;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status () {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status (java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no () {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no (java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property ultima_modificacion.
     * @return Value of property ultima_modificacion.
     */
    public java.lang.String getUltima_modificacion () {
        return ultima_modificacion;
    }
    
    /**
     * Setter for property ultima_modificacion.
     * @param ultima_modificacion New value of property ultima_modificacion.
     */
    public void setUltima_modificacion (java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion () {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion (java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion () {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor () {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor (double valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor () {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor (java.lang.String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Getter for property fecha_proceso.
     * @return Value of property fecha_proceso.
     */
    public java.lang.String getFecha_proceso () {
        return fecha_proceso;
    }
    
    /**
     * Setter for property fecha_proceso.
     * @param proveedor New value of property fecha_proceso.
     */
    public void setFecha_proceso (java.lang.String fecha_proceso) {
        this.fecha_proceso = fecha_proceso;
    }    
    
    /**
     * Getter for property tipo_acuerdo.
     * @return Value of property tipo_acuerdo.
     */
    public java.lang.String getTipo_acuerdo () {
        return tipo_acuerdo;
    }
    
    /**
     * Setter for property tipo_acuerdo.
     * @param tipo_acuerdo New value of property tipo_acuerdo.
     */
    public void setTipo_acuerdo (java.lang.String tipo_acuerdo) {
        this.tipo_acuerdo = tipo_acuerdo;
    }
    
    /**
     * Getter for property num_equipo.
     * @return Value of property num_equipo.
     */
    public java.lang.String getNum_equipo () {
        return num_equipo;
    }
    
    /**
     * Setter for property num_equipo.
     * @param num_equipo New value of property num_equipo.
     */
    public void setNum_equipo (java.lang.String num_equipo) {
        this.num_equipo = num_equipo;
    }
    
}
