/*
 * Nombre        GruposReporte.java
 * Descripci�n   Clase que permite representar los diferentes grupos de un reporte con todos sus atributos
 * Autor         Alejandro Payares
 * Fecha         10 de mayo de 2006, 08:27 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.beans;

import java.util.Vector;
import java.util.Hashtable;

/**
 * Clase que permite representar los diferentes grupos de un reporte con todos sus atributos
 * @author Alejandro Payares
 */
public class GruposReporte {
    
    public static final String LLAVE_NOMBRE_GRUPO = "nombre";
    public static final String LLAVE_COLOR_GRUPO = "color";
    public static final String LLAVE_CAMPOS_GRUPO = "campos";
    public static final String PREFIX_GRUPO = "grupo";
    public static final String LLAVE_NOMBRE_CAMPO = "nombre";
    public static final String LLAVE_TITULO_CAMPO = "titulo";
    
    /**
     * El codigo del reporte
     */
    private String codigoReporte;    
    
    /**
     * Un vector para almacenar todos los grupos del reporte
     */
    private Vector grupos;
    
    /**
     * Una tabla para almacenar todos los grupos del reporte
     */
    private Hashtable tablaGrupos;
    
    /**
     * Crea una nueva instancia de GruposReporte
     * @autor  Alejandro Payares
     */
    public GruposReporte() {
        grupos = new Vector();
        tablaGrupos = new Hashtable();
    }
    
    /**
     * Getter for property grupos.
     * @return Value of property grupos.
     */
    public java.util.Vector getGrupos() {
        return grupos;
    }
    
    /**
     * Setter for property grupos.
     * @param grupos New value of property grupos.
     */
    public void setGrupos(java.util.Vector grupos) {
        this.grupos = grupos;
    }
    
    /**
     * Agregar un grupo nuevo a la lista de grupos del reporte
     * @param nombre El nombre del nuevo reporte
     * @param color El color del nuevo reporte
     */    
    public void agregarGrupo(String nombre, String color){
        Hashtable grupo = new Hashtable();
        grupo.put(LLAVE_NOMBRE_GRUPO, nombre);
        grupo.put(LLAVE_COLOR_GRUPO, color);
        tablaGrupos.put(PREFIX_GRUPO + nombre, grupo);
        grupos.addElement(grupo);
    }
    
    /**
     * Agrega un campo a un grupo
     * @param nombreGrupo El nombre del grupo al que se le agregar� el campo
     * @param campo El campoa agregar
     */    
    public void agregarCampoAGrupo(String nombreGrupo, String campo, String titulo){
        Hashtable grupo = (Hashtable) tablaGrupos.get(PREFIX_GRUPO + nombreGrupo);
        Vector campos = (Vector) grupo.get(LLAVE_CAMPOS_GRUPO);
        if ( campos == null ) {
            campos = new Vector();
            grupo.put(LLAVE_CAMPOS_GRUPO,campos);
        }
        Hashtable hcampo = new Hashtable();
        hcampo.put(LLAVE_NOMBRE_CAMPO, campo);
        hcampo.put(LLAVE_TITULO_CAMPO, titulo);
        campos.addElement(hcampo);
    }
    
    /**
     * Devuelve la lista de campos del grupo dado
     * @param nombreGrupo El nombre del grupo
     * @return Un vector con los campos del grupo dado
     */    
    public Vector obtenerCamposDeGrupo(String nombreGrupo){
        Hashtable grupo = (Hashtable) tablaGrupos.get(PREFIX_GRUPO + nombreGrupo);
        Vector campos = (Vector) grupo.get(LLAVE_CAMPOS_GRUPO);
        if ( campos == null ) {
            campos = new Vector();
        }
        return campos;
    }
    
    
    /**
     * Elimina un grupo de la lista de grupos
     * @param nombreGrupo El nombre del grupo a eliminar
     */    
    public void eliminarGrupo(String nombreGrupo){
        Hashtable grupo = (Hashtable) tablaGrupos.get(PREFIX_GRUPO + nombreGrupo);
        if ( grupo != null ) {
            grupos.remove(grupo);
            tablaGrupos.remove(PREFIX_GRUPO + nombreGrupo);
            grupo.clear();
            grupo = null;
        }
    }
    
    /**
     * Getter for property codigoReporte.
     * @return Value of property codigoReporte.
     */
    public java.lang.String getCodigoReporte() {
        return codigoReporte;
    }
    
    /**
     * Setter for property codigoReporte.
     * @param codigoReporte New value of property codigoReporte.
     */
    public void setCodigoReporte(java.lang.String codigoReporte) {
        this.codigoReporte = codigoReporte;
    }
    
}
