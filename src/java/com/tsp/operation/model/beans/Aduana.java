/**************************************************************************
 * Nombre: ......................Aduana.java                              *
 * Descripci�n: .................Beans para el control de aduanas.        *
 * Autor:........................Ing. Karen Reales                        *
 * Fecha:........................03 de abril de 2006, 03:14 PM            *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class Aduana implements Serializable {
    
    // MODIFICADO POR LREALES EL 20 DE ENERO DE 2007
    private String platlr;
    private String ult_observacion;
    private String fecha_ult_reporte;
    private String fec_act_aduana;
    private String obs_act_aduana;
    private String distrito;
    private String tipo_reexp;
    private String ageexp;
    private String ageimp;
    private String almacenadora;
    private String reexpedicion;
    private String codigo;
    //
    
    private String estado;
    private String frontera;
    private String tipo_viaje;
    private String nomfrontera;
    private String agencia;
    private String cliente;
    private String codcliente;
    private String numrem;
    private String orirem;
    private String nomorigen;
    private String destrem;
    private String nomdestino;
    private String numpla;
    private String placa;
    private String remitente;
    private String destinatarios;
    private String faccial;
    private String fecposllegadafron;
    private String numplareex;
    private String placareex;
    private String standard;
    private String ult_pto_control;
    private String ult_act_aduana;
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property destinatarios.
     * @return Value of property destinatarios.
     */
    public java.lang.String getDestinatarios() {
        return destinatarios;
    }
    
    /**
     * Setter for property destinatarios.
     * @param destinatarios New value of property destinatarios.
     */
    public void setDestinatarios(java.lang.String destinatarios) {
        this.destinatarios = destinatarios;
    }
    
    /**
     * Getter for property destrem.
     * @return Value of property destrem.
     */
    public java.lang.String getDestrem() {
        return destrem;
    }
    
    /**
     * Setter for property destrem.
     * @param destrem New value of property destrem.
     */
    public void setDestrem(java.lang.String destrem) {
        this.destrem = destrem;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property faccial.
     * @return Value of property faccial.
     */
    public java.lang.String getFaccial() {
        return faccial;
    }
    
    /**
     * Setter for property faccial.
     * @param faccial New value of property faccial.
     */
    public void setFaccial(java.lang.String faccial) {
        this.faccial = faccial;
    }
    
    /**
     * Getter for property fecposllegadafron.
     * @return Value of property fecposllegadafron.
     */
    public java.lang.String getFecposllegadafron() {
        return fecposllegadafron;
    }
    
    /**
     * Setter for property fecposllegadafron.
     * @param fecposllegadafron New value of property fecposllegadafron.
     */
    public void setFecposllegadafron(java.lang.String fecposllegadafron) {
        this.fecposllegadafron = fecposllegadafron;
    }
    
    /**
     * Getter for property frontera.
     * @return Value of property frontera.
     */
    public java.lang.String getFrontera() {
        return frontera;
    }
    
    /**
     * Setter for property frontera.
     * @param frontera New value of property frontera.
     */
    public void setFrontera(java.lang.String frontera) {
        this.frontera = frontera;
    }
    
    /**
     * Getter for property nomdestino.
     * @return Value of property nomdestino.
     */
    public java.lang.String getNomdestino() {
        return nomdestino;
    }
    
    /**
     * Setter for property nomdestino.
     * @param nomdestino New value of property nomdestino.
     */
    public void setNomdestino(java.lang.String nomdestino) {
        this.nomdestino = nomdestino;
    }
    
    /**
     * Getter for property nomfrontera.
     * @return Value of property nomfrontera.
     */
    public java.lang.String getNomfrontera() {
        return nomfrontera;
    }
    
    /**
     * Setter for property nomfrontera.
     * @param nomfrontera New value of property nomfrontera.
     */
    public void setNomfrontera(java.lang.String nomfrontera) {
        this.nomfrontera = nomfrontera;
    }
    
    /**
     * Getter for property nomorigen.
     * @return Value of property nomorigen.
     */
    public java.lang.String getNomorigen() {
        return nomorigen;
    }
    
    /**
     * Setter for property nomorigen.
     * @param nomorigen New value of property nomorigen.
     */
    public void setNomorigen(java.lang.String nomorigen) {
        this.nomorigen = nomorigen;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numplareex.
     * @return Value of property numplareex.
     */
    public java.lang.String getNumplareex() {
        return numplareex;
    }
    
    /**
     * Setter for property numplareex.
     * @param numplareex New value of property numplareex.
     */
    public void setNumplareex(java.lang.String numplareex) {
        this.numplareex = numplareex;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property orirem.
     * @return Value of property orirem.
     */
    public java.lang.String getOrirem() {
        return orirem;
    }
    
    /**
     * Setter for property orirem.
     * @param orirem New value of property orirem.
     */
    public void setOrirem(java.lang.String orirem) {
        this.orirem = orirem;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property placareex.
     * @return Value of property placareex.
     */
    public java.lang.String getPlacareex() {
        return placareex;
    }
    
    /**
     * Setter for property placareex.
     * @param placareex New value of property placareex.
     */
    public void setPlacareex(java.lang.String placareex) {
        this.placareex = placareex;
    }
    
    /**
     * Getter for property remitente.
     * @return Value of property remitente.
     */
    public java.lang.String getRemitente() {
        return remitente;
    }
    
    /**
     * Setter for property remitente.
     * @param remitente New value of property remitente.
     */
    public void setRemitente(java.lang.String remitente) {
        this.remitente = remitente;
    }
    
    /**
     * Getter for property standard.
     * @return Value of property standard.
     */
    public java.lang.String getStandard() {
        return standard;
    }
    
    /**
     * Setter for property standard.
     * @param standard New value of property standard.
     */
    public void setStandard(java.lang.String standard) {
        this.standard = standard;
    }
    
    /**
     * Getter for property ult_act_aduana.
     * @return Value of property ult_act_aduana.
     */
    public java.lang.String getUlt_act_aduana() {
        return ult_act_aduana;
    }
    
    /**
     * Setter for property ult_act_aduana.
     * @param ult_act_aduana New value of property ult_act_aduana.
     */
    public void setUlt_act_aduana(java.lang.String ult_act_aduana) {
        this.ult_act_aduana = ult_act_aduana;
    }
    
    /**
     * Getter for property ult_pto_control.
     * @return Value of property ult_pto_control.
     */
    public java.lang.String getUlt_pto_control() {
        return ult_pto_control;
    }
    
    /**
     * Setter for property ult_pto_control.
     * @param ult_pto_control New value of property ult_pto_control.
     */
    public void setUlt_pto_control(java.lang.String ult_pto_control) {
        this.ult_pto_control = ult_pto_control;
    }
    
    /**
     * Getter for property tipo_viaje.
     * @return Value of property tipo_viaje.
     */
    public java.lang.String getTipo_viaje() {
        return tipo_viaje;
    }
    
    /**
     * Setter for property tipo_viaje.
     * @param tipo_viaje New value of property tipo_viaje.
     */
    public void setTipo_viaje(java.lang.String tipo_viaje) {
        this.tipo_viaje = tipo_viaje;
    }
    
    /**
     * Getter for property codcliente.
     * @return Value of property codcliente.
     */
    public java.lang.String getCodcliente() {
        return codcliente;
    }
    
    /**
     * Setter for property codcliente.
     * @param codcliente New value of property codcliente.
     */
    public void setCodcliente(java.lang.String codcliente) {
        this.codcliente = codcliente;
    }
    
    
    // MODIFICADO POR LREALES EL 20 DE ENERO DE 2007
    
    /**
     * Getter for property platlr.
     * @return Value of property platlr.
     */
    public java.lang.String getPlatlr() {
        return platlr;
    }
    
    /**
     * Setter for property platlr.
     * @param platlr New value of property platlr.
     */
    public void setPlatlr(java.lang.String platlr) {
        this.platlr = platlr;
    }
    
    /**
     * Getter for property ult_observacion.
     * @return Value of property ult_observacion.
     */
    public java.lang.String getUlt_observacion() {
        return ult_observacion;
    }
    
    /**
     * Setter for property ult_observacion.
     * @param ult_observacion New value of property ult_observacion.
     */
    public void setUlt_observacion(java.lang.String ult_observacion) {
        this.ult_observacion = ult_observacion;
    }
    
    /**
     * Getter for property fecha_ult_reporte.
     * @return Value of property fecha_ult_reporte.
     */
    public java.lang.String getFecha_ult_reporte() {
        return fecha_ult_reporte;
    }
    
    /**
     * Setter for property fecha_ult_reporte.
     * @param fecha_ult_reporte New value of property fecha_ult_reporte.
     */
    public void setFecha_ult_reporte(java.lang.String fecha_ult_reporte) {
        this.fecha_ult_reporte = fecha_ult_reporte;
    }
    
    /**
     * Getter for property fec_act_aduana.
     * @return Value of property fec_act_aduana.
     */
    public java.lang.String getFec_act_aduana() {
        return fec_act_aduana;
    }
    
    /**
     * Setter for property fec_act_aduana.
     * @param fec_act_aduana New value of property fec_act_aduana.
     */
    public void setFec_act_aduana(java.lang.String fec_act_aduana) {
        this.fec_act_aduana = fec_act_aduana;
    }
    
    /**
     * Getter for property obs_act_aduana.
     * @return Value of property obs_act_aduana.
     */
    public java.lang.String getObs_act_aduana() {
        return obs_act_aduana;
    }
    
    /**
     * Setter for property obs_act_aduana.
     * @param obs_act_aduana New value of property obs_act_aduana.
     */
    public void setObs_act_aduana(java.lang.String obs_act_aduana) {
        this.obs_act_aduana = obs_act_aduana;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property tipo_reexp.
     * @return Value of property tipo_reexp.
     */
    public java.lang.String getTipo_reexp() {
        return tipo_reexp;
    }
    
    /**
     * Setter for property tipo_reexp.
     * @param tipo_reexp New value of property tipo_reexp.
     */
    public void setTipo_reexp(java.lang.String tipo_reexp) {
        this.tipo_reexp = tipo_reexp;
    }
    
    /**
     * Getter for property ageexp.
     * @return Value of property ageexp.
     */
    public java.lang.String getAgeexp() {
        return ageexp;
    }
    
    /**
     * Setter for property ageexp.
     * @param ageexp New value of property ageexp.
     */
    public void setAgeexp(java.lang.String ageexp) {
        this.ageexp = ageexp;
    }
    
    /**
     * Getter for property ageimp.
     * @return Value of property ageimp.
     */
    public java.lang.String getAgeimp() {
        return ageimp;
    }
    
    /**
     * Setter for property ageimp.
     * @param ageimp New value of property ageimp.
     */
    public void setAgeimp(java.lang.String ageimp) {
        this.ageimp = ageimp;
    }
    
    /**
     * Getter for property almacenadora.
     * @return Value of property almacenadora.
     */
    public java.lang.String getAlmacenadora() {
        return almacenadora;
    }
    
    /**
     * Setter for property almacenadora.
     * @param almacenadora New value of property almacenadora.
     */
    public void setAlmacenadora(java.lang.String almacenadora) {
        this.almacenadora = almacenadora;
    }
    
    /**
     * Getter for property reexpedicion.
     * @return Value of property reexpedicion.
     */
    public java.lang.String getReexpedicion() {
        return reexpedicion;
    }
    
    /**
     * Setter for property reexpedicion.
     * @param reexpedicion New value of property reexpedicion.
     */
    public void setReexpedicion(java.lang.String reexpedicion) {
        this.reexpedicion = reexpedicion;
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    //
    
    
}