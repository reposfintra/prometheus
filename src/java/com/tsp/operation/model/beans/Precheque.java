/*
 * Precheque.java
 *
 * Created on 9 de febrero de 2007, 04:17 PM
 */
package com.tsp.operation.model.beans;

import java.util.Vector;
/**
 *
 * @author  Osvaldo P�rez Ferrer
 */
public class Precheque {
    
    /** Creates a new instance of Precheque */
    
   private String reg_status;
   private String id;
   private String dstrct;
   private String banco;
   private String sucursal;
   private String cheque;
   private String beneficiario;
   private String proveedor;
   private String agencia;
   private String fecha_impresion;
   private String usuario_impresion;
   private double valor;
   private String moneda;
   private String last_update;
   private String user_update;
   private String creation_date;
   private String creation_user;
   private String base;
   private String nom_beneficiario;
   private String nom_proveedor;
   private String nom_agencia;
   
   private String item;
   private String tipo_documento;
   private String documento;
   private String tipo_pago;
   private String facturas = "";
   
   private Vector items;
    
    public Precheque() {
        this.items = new Vector();
    }
    
    /***********************************************************
    * Metodo para obtener un item especifico de precheque,
    * busca el precheque en la lista de items
    * con el numero de documento dado
    ***********************************************************/
    public Precheque getItemEspecifico( String documento ){
        
        for( int i=0; i<items.size(); i++ ){
            Precheque p = (Precheque)items.get(i);
            if( p.getDocumento().equals(documento) ){                
                return p;                
            }
        }
        return null;
        
    }
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property beneficiario.
     * @return Value of property beneficiario.
     */
    public java.lang.String getBeneficiario() {
        return beneficiario;
    }
    
    /**
     * Setter for property beneficiario.
     * @param beneficiario New value of property beneficiario.
     */
    public void setBeneficiario(java.lang.String beneficiario) {
        this.beneficiario = beneficiario;
    }
    
    /**
     * Getter for property cheque.
     * @return Value of property cheque.
     */
    public java.lang.String getCheque() {
        return cheque;
    }
    
    /**
     * Setter for property cheque.
     * @param cheque New value of property cheque.
     */
    public void setCheque(java.lang.String cheque) {
        this.cheque = cheque;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_impresion.
     * @return Value of property fecha_impresion.
     */
    public java.lang.String getFecha_impresion() {
        return fecha_impresion;
    }
    
    /**
     * Setter for property fecha_impresion.
     * @param fecha_impresion New value of property fecha_impresion.
     */
    public void setFecha_impresion(java.lang.String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public java.lang.String getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property usuario_impresion.
     * @return Value of property usuario_impresion.
     */
    public java.lang.String getUsuario_impresion() {
        return usuario_impresion;
    }
    
    /**
     * Setter for property usuario_impresion.
     * @param usuario_impresion New value of property usuario_impresion.
     */
    public void setUsuario_impresion(java.lang.String usuario_impresion) {
        this.usuario_impresion = usuario_impresion;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property nom_beneficiario.
     * @return Value of property nom_beneficiario.
     */
    public java.lang.String getNom_beneficiario() {
        return nom_beneficiario;
    }
    
    /**
     * Setter for property nom_beneficiario.
     * @param nom_beneficiario New value of property nom_beneficiario.
     */
    public void setNom_beneficiario(java.lang.String nom_beneficiario) {
        this.nom_beneficiario = nom_beneficiario;
    }
    
    /**
     * Getter for property nom_proveedor.
     * @return Value of property nom_proveedor.
     */
    public java.lang.String getNom_proveedor() {
        return nom_proveedor;
    }
    
    /**
     * Setter for property nom_proveedor.
     * @param nom_proveedor New value of property nom_proveedor.
     */
    public void setNom_proveedor(java.lang.String nom_proveedor) {
        this.nom_proveedor = nom_proveedor;
    }
    
    /**
     * Getter for property items.
     * @return Value of property items.
     */
    public java.util.Vector getItems() {
        return items;
    }
    
    /**
     * Setter for property items.
     * @param items New value of property items.
     */
    public void setItems(java.util.Vector items) {
        this.items = items;
    }
    
    /**
     * Getter for property nom_agencia.
     * @return Value of property nom_agencia.
     */
    public java.lang.String getNom_agencia() {
        return nom_agencia;
    }
    
    /**
     * Setter for property nom_agencia.
     * @param nom_agencia New value of property nom_agencia.
     */
    public void setNom_agencia(java.lang.String nom_agencia) {
        this.nom_agencia = nom_agencia;
    }
    
    /**
     * Getter for property tipo_pago.
     * @return Value of property tipo_pago.
     */
    public java.lang.String getTipo_pago() {
        return tipo_pago;
    }
    
    /**
     * Setter for property tipo_pago.
     * @param tipo_pago New value of property tipo_pago.
     */
    public void setTipo_pago(java.lang.String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }
    
    /**
     * Getter for property facturas.
     * @return Value of property facturas.
     */
    public java.lang.String getFacturas() {
        return facturas;
    }
    
    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.lang.String facturas) {
        this.facturas = facturas;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
}
