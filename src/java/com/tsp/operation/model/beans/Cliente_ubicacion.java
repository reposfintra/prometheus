/************************************************************************
 * Nombre clase: Cliente_ubicacion.java                                 *
 * Descripci�n: Clase que maneja los atributos del objeto               *
 *              de cliente_ubicacion.                                   *
 * Autor: Ing. Jose de la rosa                                          *
 * Fecha: Created on 20 de enero de 2006, 08:27 AM                      *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Cliente_ubicacion implements Serializable{
    private String cliente;
    private String ubicacion;
    private String dstrct;
    private String rec_status;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String base;
    /** Creates a new instance of Cliente_ubicacion */
    public static Cliente_ubicacion load(ResultSet rs)throws SQLException{ 
        Cliente_ubicacion c = new Cliente_ubicacion();
        c.setCliente (rs.getString("cedula"));
        c.setUbicacion (rs.getString("cod_ubicacion"));
        c.setDstrct (rs.getString("dstrct"));
        c.setRec_status(rs.getString("reg_status"));
        c.setLast_update(rs.getString("last_update"));
        c.setUser_update(rs.getString("user_update"));
        c.setCreation_date(rs.getString("creation_date"));
        c.setCreation_user(rs.getString("creation_user"));
        c.setBase(rs.getString("base"));
        return c;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente () {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente (java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date () {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user () {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct () {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct (java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update () {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update (java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status () {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status (java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property ubicacion.
     * @return Value of property ubicacion.
     */
    public java.lang.String getUbicacion () {
        return ubicacion;
    }
    
    /**
     * Setter for property ubicacion.
     * @param ubicacion New value of property ubicacion.
     */
    public void setUbicacion (java.lang.String ubicacion) {
        this.ubicacion = ubicacion;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update () {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}
