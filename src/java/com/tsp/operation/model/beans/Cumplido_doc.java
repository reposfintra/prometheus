/*
 * Cumplir_doc.java
 *
 * Created on 3 de octubre de 2005, 02:50 PM
 */

package com.tsp.operation.model.beans;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  R.SALAZAR
numrem varchar(10) NOT NULL DEFAULT ''::character varying,
  numpla varchar(10) NOT NULL DEFAULT ''::character varying,
  tipo_doc varchar(5) NOT NULL DEFAULT ''::character varying,
  documento varchar(50) NOT NULL DEFAULT ''::character varying,
  tipo_doc_rel varchar(5) NOT NULL DEFAULT ''::character varying,
  documento_rel varchar(50) NOT NULL DEFAULT ''::character varying,
   */
public class Cumplido_doc {
    public String numrem;
    public String numpla;
    public String tipo_doc;
    public String documento;
    public String tipo_doc_rel;
    public String documento_rel;
    public String nom_td;
    public String nom_tdr;
    
    public java.util.Date fecha_mod;
    public java.util.Date fecha_crea;
    public String base;
    public String district;
    public String crea_user;
    public String mod_user;
    public String estado;
    public String agencia;
    
    /** Creates a new instance of Cumplir_doc */
    public Cumplido_doc() {
        
    }
    public static Cumplido_doc load(ResultSet rs) throws SQLException {
        Cumplido_doc cumplido_doc = new Cumplido_doc();
        
        cumplido_doc.setNumrem( rs.getString("numrem") );
        cumplido_doc.setNumpla( rs.getString("numpla") );
        cumplido_doc.setTipo_doc( rs.getString("tipo_doc") );
        cumplido_doc.setTipo_doc_rel( rs.getString("tipo_doc_rel") );
        cumplido_doc.setDocumento( rs.getString("documento") );
        cumplido_doc.setDocumento_rel( rs.getString("documento_rel") );
        //
        cumplido_doc.setFecha_mod( rs.getDate("last_update") );
        cumplido_doc.setFecha_crea( rs.getDate("creation_date") );
        cumplido_doc.setBase( rs.getString("base") );
        cumplido_doc.setDistrict( rs.getString("dstrct") );
        cumplido_doc.setCrea_user(rs.getString("creation_user") );
        cumplido_doc.setMod_user(rs.getString("user_update"));
        cumplido_doc.setEstado(rs.getString("reg_status"));
        cumplido_doc.setAgencia(rs.getString("agencia"));
        return cumplido_doc;
    }
    
    public void setNumrem(String var){
        numrem = var;
        
    }
     
    public void setNumpla(String var){
        numpla = var;
    }
     
    public void setTipo_doc(String var){
        tipo_doc = var;
    }
    
    public void setDocumento(String var){
        documento = var;
    }
    
    public void setTipo_doc_rel(String var){
        tipo_doc_rel = var;
    }
    
    public void setDocumento_rel(String var){
        documento_rel = var;
    }
    
    public void setFecha_mod(java.util.Date var){
        fecha_mod = var;
    }
    
    public void setFecha_crea(java.util.Date var){
        fecha_crea = var;
    }
    
    public void setBase(String var){
        base = var;
    }
    
    public void setDistrict(String var){
       district = var;
    }
    
    public void setCrea_user(String var){
        crea_user = var;
    }
    
    public void setMod_user(String var){
        mod_user = var;
    }
    
    public void setEstado(String var){
        estado = var;
    }
    
    public void setAgencia(String var){
        agencia = var;
    }
    
    public void setNom_td(String var){
        nom_td = var;
    }
    
    public void setNom_tdr(String var){
        nom_tdr = var;
    }
    
    
    //Consultas
    
    public String getNumrem(){
        return this.numrem ;
    }
     
    public String getNumpla(){
        return this.numpla ;
    }
     
    public String getTipo_doc(){
        return this.tipo_doc;
    }
    
    public String getDocumento(){
        return this.documento;
    }
    
    public String getTipo_doc_rel(){
        return this.tipo_doc_rel;
    }
    
    public String getDocumento_rel(){
        return this.documento_rel;
    }
    
    public java.util.Date getFecha_mod(){
        return fecha_mod;
    }
    
    public java.util.Date getFecha_crea( ){
        return fecha_crea;
    }
    
    public String getBase( ){
        return base;
    }
    
    public String getDistrict( ){
        return district;
    }
    
    public String getCrea_user( ){
        return crea_user;
    }
    
    public String getMod_user( ){
        return mod_user ;
    }
    
    public String getEstado( ){
        return estado ;
    }
    
    public String getAgencia( ){
        return agencia ;
    }
}
