/*
 * DatosImpresionPlanilla.java
 *
 * Created on 16 de diciembre de 2004, 14:45
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  MFontalvo
 */
public class DatosPlanillaImp implements Serializable{
    
    // datos basicos de la planilla
    private String NumeroPlanilla;
    private String FechaPlanilla;
    private String CodOrigen;
    private String DesOrigen;
    private String CodDestino;
    private String DesDestino;
    private String FechaImpresion;
    private String Ruta;
    private String FechaCargue;
    private String Estado;
    
    //datos basicos del vehiculo
    private String Placa;
    private String Marca;
    private String Modelo;
    private String Linea;
    private String ModeloRepotenciado;
    private String Serial;
    private String Color;
    private String DimensionCarroceria;
    private String NumeroEjes;  //configuracion
    private String PesoVacio;
    private String NumeroPolizaSoat;
    private String CompaniaSoat;
    private String VecimientoSoat;
    private String PlacaTrailer;
    private String RegistroNacionalCarga;
    
    
    //datos propietario del vehiculo
    private String CedulaPropietario;
    private String NombrePropietario;
    private String DireccionPropietario;
    private String TelefonoPropietario;
    private String CiudadPropietario;
    
    //datos tenedor del vehiculo
    private String CedulaTenedor;
    private String NombreTenedor;
    private String DireccionTenedor;
    private String TelefonoTenedor;
    private String CiudadTenedor;
    
    //datos conductor del vehiculo
    private String CedulaConductor;
    private String NombreConductor;
    private String DireccionConductor;
    private String TelefonoConductor;
    private String CiudadConductor;
    private String CategoriaPase;
    
    //datos de movimiento
    private double ValorAnticipo;
    private double ValorAnticipo2;
    private String Moneda;
    
    //Documento Internos y Contenedores
    private String Contenedores;
    private String DocInternos;    
    private String Despachador;
    
    
    //datos remesas
    private List   Remesas;
    private List   Descuentos;
    private String Observaciones;
    
    //Mario 30.12.05
    private TreeMap Soportes;
    
    private String planviaje;
    
    /** Creates a new instance of DatosImpresionPlanilla */
    public DatosPlanillaImp(){
        
    }    
    
    public static DatosPlanillaImp load(ResultSet rs)throws Exception{
        DatosPlanillaImp datos = new DatosPlanillaImp();
            
        datos.setNumeroPlanilla      (rs.getString(1 ));
        datos.setFechaPlanilla       (rs.getString(2 ));
        datos.setCodOrigen           (rs.getString(3 ));
        datos.setDesOrigen           (rs.getString(4 ));
        datos.setCodDestino          (rs.getString(5 ));
        datos.setDesDestino          (rs.getString(6 ));
        datos.setRuta                (rs.getString(7 ));
                
        datos.setPlaca               (rs.getString(8 ));
        datos.setMarca               (rs.getString(9 ));
        datos.setModelo              (rs.getString(10 ));
        datos.setSerial              (rs.getString(11));
        datos.setColor               (rs.getString(12));
        datos.setDimensionCarroceria (rs.getString(13));
        datos.setNumeroEjes          (rs.getString(14));
        datos.setPesoVacio           (rs.getString(15));
                
        datos.setCedulaPropietario   (rs.getString(16));
        datos.setNombrePropietario   (rs.getString(17));
        datos.setDireccionPropietario(rs.getString(18));
        datos.setTelefonoPropietario (rs.getString(19));
        datos.setCiudadPropietario   (rs.getString(20));
        
        datos.setCedulaTenedor       (rs.getString(21));
        datos.setNombreTenedor       (rs.getString(22));
        datos.setDireccionTenedor    (rs.getString(23));
        datos.setTelefonoTenedor     (rs.getString(24));
        datos.setCiudadTenedor       (rs.getString(25));

        datos.setCedulaConductor     (rs.getString(26));
        datos.setNombreConductor     (rs.getString(27));
        datos.setDireccionConductor  (rs.getString(28));
        datos.setTelefonoConductor   (rs.getString(29));
        datos.setCiudadConductor     (rs.getString(30));
        
        datos.setFechaImpresion      (rs.getString(31));
        datos.setValorAnticipo       (rs.getDouble(32));
        datos.setValorAnticipo2      (rs.getDouble(33));
        datos.setMoneda              (rs.getString(34));
        datos.setDespachador         (rs.getString(35));
        
        datos.setEstado           (rs.getString(36));
        datos.setFechaCargue      (rs.getString(37));
        datos.setContenedores     (rs.getString(38));
        datos.setPlacaTrailer     (rs.getString(39));
        datos.setVecimientoSoat   (rs.getString(40));
        datos.setNumeroPolizaSoat (rs.getString(41));
        datos.setCompaniaSoat     (rs.getString(42));
        datos.setRegistroNacionalCarga(rs.getString(43));
        datos.setCategoriaPase    (rs.getString(44));
        
        
        return datos;
        
    }
    
    //setter
    // planilla
    public void setNumeroPlanilla(String valor){
        this.NumeroPlanilla = valor;
    }
    public void setFechaPlanilla(String valor){
        this.FechaPlanilla = valor;
    }
    public void setCodOrigen(String valor){
        this.CodOrigen = valor;
    }
    public void setDesOrigen(String valor){
        this.DesOrigen = valor;
    }
    public void setCodDestino(String valor){
        this.CodDestino = valor;
    }
    public void setDesDestino(String valor){
        this.DesDestino = valor;
    }
    public void setFechaImpresion(String valor){
        this.FechaImpresion = valor;
    }
    public void setRuta(String valor){
        this.Ruta = valor;
    }
    public void setValorAnticipo(double valor){
        this.ValorAnticipo = valor;
    }
    public void setValorAnticipo2(double valor){
        this.ValorAnticipo2 = valor;
    }
    public void setMoneda(String valor){
        this.Moneda = valor;
    }
    public void setEstado(String valor){
        this.Estado = valor;
    }    
    
    
    // vehiculo 
    public void setPlaca(String valor){
        this.Placa = valor;
    }
    public void setMarca(String valor){
        this.Marca = valor;
    }
    public void setModelo(String valor){
        this.Modelo = valor;
    }
    public void setSerial(String valor){
        this.Serial = valor;
    }
    public void setColor(String valor){
        this.Color = valor;
    }
    public void setDimensionCarroceria(String valor){
        this.DimensionCarroceria = valor;
    }
    public void setNumeroEjes(String valor){
        this.NumeroEjes = valor;
    }
    public void setPesoVacio(String valor){
        this.PesoVacio = valor;
    }
    
    //propietario
    public void setCedulaPropietario(String valor){
	this.CedulaPropietario = valor;
    }
    public void setNombrePropietario(String valor){
	this.NombrePropietario = valor;
    }
    public void setDireccionPropietario(String valor){
	this.DireccionPropietario = valor;
    }
    public void setTelefonoPropietario(String valor){
	this.TelefonoPropietario = valor;
    }
    public void setCiudadPropietario(String valor){
	this.CiudadPropietario = valor;
    }

    //tenedor
    public void setCedulaTenedor(String valor){
	this.CedulaTenedor = valor;
    }
    public void setNombreTenedor(String valor){
	this.NombreTenedor = valor;
    }
    public void setDireccionTenedor(String valor){
	this.DireccionTenedor = valor;
    }
    public void setTelefonoTenedor(String valor){
	this.TelefonoTenedor = valor;
    }
    public void setCiudadTenedor(String valor){
	this.CiudadTenedor = valor;
    }
    
    // conductor
    public void setCedulaConductor(String valor){
	this.CedulaConductor = valor;
    }
    public void setNombreConductor(String valor){
	this.NombreConductor = valor;
    }
    public void setDireccionConductor(String valor){
	this.DireccionConductor = valor;
    }
    public void setTelefonoConductor(String valor){
	this.TelefonoConductor = valor;
    }
    public void setCiudadConductor(String valor){
	this.CiudadConductor = valor;
    }
    public void setDespachador(String valor){
	this.Despachador = valor;
    }
    
    public void setRemesas(List valor){
        this.Remesas = valor;
    }
    
    
    // getter

    
    // planilla
    public String getNumeroPlanilla(){
        return this.NumeroPlanilla;
    }
    public String getFechaPlanilla(){
        return this.FechaPlanilla;
    }
    public String getCodOrigen(){
        return this.CodOrigen;
    }
    public String getDesOrigen(){
        return this.DesOrigen;
    }
    public String getCodDestino(){
        return this.CodDestino;
    }
    public String getDesDestino(){
        return this.DesDestino;
    }
    public String getFechaImpresion(){
        return this.FechaImpresion;
    }
    public String getRuta(){
        return this.Ruta;
    }
    public double getValorAnticipo(){
        return this.ValorAnticipo;
    }
    public double getValorAnticipo2(){
        return this.ValorAnticipo2;
    }
    public String getMoneda(){
        return this.Moneda;
    }    
    public String getEstado(){
        return this.Estado;
    }

    
    // vehiculo 
    public String getPlaca(){
        return  this.Placa;
    }
    public String getMarca(){
        return this.Marca;
    }
    public String getModelo(){
        return this.Modelo;
    }
    public String getSerial(){
        return this.Serial;
    }
    public String getColor(){
        return this.Color;
    }
    public String getDimensionCarroceria(){
        return this.DimensionCarroceria;
    }
    public String getNumeroEjes(){
        return this.NumeroEjes;
    }
    public String getPesoVacio(){
        return this.PesoVacio;
    }
    
    //propietario
    public String getCedulaPropietario(){
	return this.CedulaPropietario;
    }
    public String getNombrePropietario(){
	return this.NombrePropietario;
    }
    public String getDireccionPropietario(){
	return this.DireccionPropietario;
    }
    public String getTelefonoPropietario(){
	return this.TelefonoPropietario;
    }
    public String getCiudadPropietario(){
	return this.CiudadPropietario;
    }

    //tenedor
    public String getCedulaTenedor(){
	return this.CedulaTenedor;
    }
    public String getNombreTenedor(){
	return this.NombreTenedor;
    }
    public String getDireccionTenedor(){
	return this.DireccionTenedor;
    }
    public String getTelefonoTenedor(){
	return this.TelefonoTenedor;
    }
    public String getCiudadTenedor(){
	return this.CiudadTenedor;
    }
    
    // conductor
    public String getCedulaConductor(){
	return this.CedulaConductor;
    }
    public String getNombreConductor(){
	return this.NombreConductor;
    }
    public String getDireccionConductor(){
	return this.DireccionConductor;
    }
    public String getTelefonoConductor(){
	return this.TelefonoConductor;
    }
    public String getCiudadConductor(){
	return this.CiudadConductor;
    }
    public String getDespachador(){
	return this.Despachador;
    }
    
    //remesas
    public List getRemesas(){
        return this.Remesas;
    }
    
    /**
     * Obtiene Listado de Descuentos
     * @autor  .... Mario Fontalvo
     * @return  ... valores listado de descuentos de la planilla
     */
    public java.util.List getDescuentos() {
        return Descuentos;
    }
    
    /**
     * modifica Listado de Descuentos
     * @autor           .... Mario Fontalvo
     * @param Descuentos  ... nuevos descuentos de la planilla
     */
    public void setDescuentos(java.util.List Descuentos) {
        this.Descuentos = Descuentos;
    }
    
    /**
     * Obtiene la fecha de cargue de la planilla
     * @autor  .... Mario Fontalvo
     * @return  ... fecha de cargue de la planilla
     */
    public java.lang.String getFechaCargue() {
        return FechaCargue;
    }
    
    
    /**
     * modifica la fecha de Cargue de la Planilla
     * @autor           .... Mario Fontalvo
     * @param FechaCargue  . nueva fecha de Cargue de la planilla
     */
    public void setFechaCargue(java.lang.String FechaCargue) {
        this.FechaCargue = FechaCargue;
    }
    
    /**
     * Obtiene  el valor de propiedad Soportes.
     * @return Valor de la propiedad Soportes.
     */
    public java.util.TreeMap getSoportes() {
        return Soportes;
    }
    
    /**
     * Modifica el valor de la propiedad Soportes.
     * @param Soportes Nuevo valor de la propiedad Soportes.
     */
    public void setSoportes(java.util.TreeMap Soportes) {
        this.Soportes = Soportes;
    }
    
    /**
     * Obtiene  el valor de propiedad RegistroNacionalCarga.
     * @autor mfontalvo
     * @return Nuevo valor de la propiedad RegistroNacionalCarga.
     */
    public java.lang.String getRegistroNacionalCarga() {
        return RegistroNacionalCarga;
    }
    
    /**
     * Modifica el valor de la propiedad RegistroNacionalCarga.
     * @autor mfontalvo
     * @param RegistroNacionalCarga Nuevo valor de la propiedad RegistroNacionalCarga.
     */
    public void setRegistroNacionalCarga(java.lang.String RegistroNacionalCarga) {
        this.RegistroNacionalCarga = RegistroNacionalCarga;
    }
    
    /**
     * Obtiene  el valor de propiedad PlacaTrailer.
     * @autor mfontalvo
     * @return Valor de la propiedad PlacaTrailer.
     */
    public java.lang.String getPlacaTrailer() {
        return PlacaTrailer;
    }
    
    /**
     * Modifica el valor de la propiedad PlacaTrailer.
     * @autor mfontalvo
     * @param PlacaTrailer Nuevo valor de la propiedad PlacaTrailer.
     */
    public void setPlacaTrailer(java.lang.String PlacaTrailer) {
        this.PlacaTrailer = PlacaTrailer;
    }
    
    /**
     * Obtiene  el valor de propiedad VecimientoSoat.
     * @autor mfontalvo
     * @return Valor de la propiedad VecimientoSoat.
     */
    public java.lang.String getVecimientoSoat() {
        return VecimientoSoat;
    }
    
    /**
     * Modifica el valor de la propiedad VecimientoSoat.
     * @autor mfontalvo
     * @param VecimientoSoat Nuevo valor de la propiedad VecimientoSoat.
     */
    public void setVecimientoSoat(java.lang.String VecimientoSoat) {
        this.VecimientoSoat = VecimientoSoat;
    }
    
    /**
     * Obtiene  el valor de propiedad CompaniaSoat.
     * @autor mfontalvo
     * @return Valor de la propiedad CompaniaSoat.
     */
    public java.lang.String getCompaniaSoat() {
        return CompaniaSoat;
    }
    
    /**
     * Modifica el valor de la propiedad CompaniaSoat.
     * @autor mfontalvo
     * @param CompaniaSoat Nuevo valor de la propiedad CompaniaSoat.
     */
    public void setCompaniaSoat(java.lang.String CompaniaSoat) {
        this.CompaniaSoat = CompaniaSoat;
    }
    
    /**
     * Obtiene  el valor de propiedad Linea.
     * @autor mfontalvo
     * @return Valor de la propiedad Linea.
     */
    public java.lang.String getLinea() {
        return Linea;
    }
    
    /**
     * Modifica el valor de la propiedad Linea.
     * @autor mfontalvo
     * @param Linea Nuevo valor de la propiedad Linea.
     */
    public void setLinea(java.lang.String Linea) {
        this.Linea = Linea;
    }
    
    /**
     * Obtiene  el valor de propiedad NumeroPolizaSoat.
     * @autor mfontalvo
     * @return Valor de la propiedad NumeroPolizaSoat.
     */
    public java.lang.String getNumeroPolizaSoat() {
        return NumeroPolizaSoat;
    }
    
    /**
     * Modifica el valor de la propiedad NumeroPolizaSoat.
     * @autor mfontalvo
     * @param NumeroPolizaSoat Nuevo valor de la propiedad NumeroPolizaSoat.
     */
    public void setNumeroPolizaSoat(java.lang.String NumeroPolizaSoat) {
        this.NumeroPolizaSoat = NumeroPolizaSoat;
    }
    
    /**
     * Obtiene  el valor de propiedad Contenedores.
     * @autor mfontalvo
     * @return Valor de la propiedad Contenedores.
     */
    public java.lang.String getContenedores() {
        return Contenedores;
    }
    
    /**
     * Modifica el valor de la propiedad Contenedores.
     * @autor mfontalvo
     * @param Contenedores Nuevo valor de la propiedad Contenedores.
     */
    public void setContenedores(java.lang.String Contenedores) {
        this.Contenedores = Contenedores;
    }
    
    /**
     * Obtiene  el valor de propiedad DocInternos.
     * @autor mfontalvo
     * @return Valor de la propiedad DocInternos.
     */
    public java.lang.String getDocInternos() {
        return DocInternos;
    }
    
    /**
     * Modifica el valor de la propiedad DocInternos.
     * @autor mfontalvo
     * @param DocInternos Nuevo valor de la propiedad DocInternos.
     */
    public void setDocInternos(java.lang.String DocInternos) {
        this.DocInternos = DocInternos;
    }
    
    /**
     * Getter for property CategoriaPase.
     * @return Value of property CategoriaPase.
     */
    public java.lang.String getCategoriaPase() {
        return CategoriaPase;
    }
    
    /**
     * Setter for property CategoriaPase.
     * @param CategoriaPase New value of property CategoriaPase.
     */
    public void setCategoriaPase(java.lang.String CategoriaPase) {
        this.CategoriaPase = CategoriaPase;
    }
    
    /**
     * Getter for property Observaciones.
     * @return Value of property Observaciones.
     */
    public java.lang.String getObservaciones() {
        return Observaciones;
    }
    
    /**
     * Setter for property Observaciones.
     * @param Observaciones New value of property Observaciones.
     */
    public void setObservaciones(java.lang.String Observaciones) {
        this.Observaciones = Observaciones;
    }        
    
    /**
     * Getter for property planviaje.
     * @return Value of property planviaje.
     */
    public java.lang.String getPlanviaje() {
        return planviaje;
    }
    
    /**
     * Setter for property planviaje.
     * @param planviaje New value of property planviaje.
     */
    public void setPlanviaje(java.lang.String planviaje) {
        this.planviaje = planviaje;
    }
    
}
