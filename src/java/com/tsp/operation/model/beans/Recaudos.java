/***************************************************************************
 * Nombre:        Recaudos.java                                            *
 * Descripci�n:   Beans de acuerdo especial.                               *
 * Autor:         Ing. Miguel Altamiranda                                  *
 * Fecha:         13 de Agosto de 2009                                     *
 * Versi�n:       Java  1.0                                                *
 * Copyright:     Fintravalores S.A. S.A.                                  *
 **************************************************************************/


package com.tsp.operation.model.beans;

public class Recaudos {
        private String facturas;
        private String fecha;
        private String banco;
        private String sucursal;
        private double valor;
        private double intereses;
        private String cuenta;
        private String oid;
        private String cod;
        
        /** Creates a new instance of Proveedor */
        public Recaudos() {
                this.facturas = "";
                this.intereses=0;
                this.valor=0;
                this.banco="";
                this.sucursal="";
                this.fecha="";
                this.cuenta="";
                this.oid="";
                this.cod="";
                
        }
        
        /**
         * Getter for property facturas.
         * @return Value of property facturas.
         */
        public java.lang.String getFacturas() {
            return facturas;
        }
        
        /**
         * Setter for property facturas.
         * @param facturas New value of property facturas.
         */
        public void setFacturas(java.lang.String facturas) {
            this.facturas = facturas;
        }
        
        /**
         * Getter for property fecha.
         * @return Value of property fecha.
         */
        public java.lang.String getFecha() {
            return fecha;
        }
        
        /**
         * Setter for property fecha.
         * @param fecha New value of property fecha.
         */
        public void setFecha(java.lang.String fecha) {
            this.fecha = fecha;
        }
        
        /**
         * Getter for property banco.
         * @return Value of property banco.
         */
        public java.lang.String getBanco() {
            return banco;
        }
        
        /**
         * Setter for property banco.
         * @param banco New value of property banco.
         */
        public void setBanco(java.lang.String banco) {
            this.banco = banco;
        }
        
        /**
         * Getter for property sucursal.
         * @return Value of property sucursal.
         */
        public java.lang.String getSucursal() {
            return sucursal;
        }
        
        /**
         * Setter for property sucursal.
         * @param sucursal New value of property sucursal.
         */
        public void setSucursal(java.lang.String sucursal) {
            this.sucursal = sucursal;
        }
        
        /**
         * Getter for property valor.
         * @return Value of property valor.
         */
        public double getValor() {
            return valor;
        }
        
        /**
         * Setter for property valor.
         * @param valor New value of property valor.
         */
        public void setValor(double valor) {
            this.valor = valor;
        }
        
        /**
         * Getter for property intereses.
         * @return Value of property intereses.
         */
        public double getIntereses() {
            return intereses;
        }
        
        /**
         * Setter for property intereses.
         * @param intereses New value of property intereses.
         */
        public void setIntereses(double intereses) {
            this.intereses = intereses;
        }
        
        /**
         * Getter for property cuenta.
         * @return Value of property cuenta.
         */
        public java.lang.String getCuenta() {
            return cuenta;
        }
        
        /**
         * Setter for property cuenta.
         * @param cuenta New value of property cuenta.
         */
        public void setCuenta(java.lang.String cuenta) {
            this.cuenta = cuenta;
        }
        
        /**
         * Getter for property oid.
         * @return Value of property oid.
         */
        public java.lang.String getOid() {
            return oid;
        }
        
        /**
         * Setter for property oid.
         * @param oid New value of property oid.
         */
        public void setOid(java.lang.String oid) {
            this.oid = oid;
        }
        
        /**
         * Getter for property cod.
         * @return Value of property cod.
         */
        public java.lang.String getCod() {
            return cod;
        }
        
        /**
         * Setter for property cod.
         * @param cod New value of property cod.
         */
        public void setCod(java.lang.String cod) {
            this.cod = cod;
        }
        
}
