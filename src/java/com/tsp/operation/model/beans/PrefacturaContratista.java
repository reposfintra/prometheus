/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class PrefacturaContratista {
    
    private String id_contratista ;
    private String  prefactura;


    /** Creates a new instance of PrefacturaContratista */
    public PrefacturaContratista () {

    }



    public static PrefacturaContratista load(java.sql.ResultSet rs)throws java.sql.SQLException{

        PrefacturaContratista prefacturaContratista = new PrefacturaContratista();

        prefacturaContratista.setId_contratista( rs.getString("id_contratista") );
        prefacturaContratista.setPrefactura( rs.getString("prefactura") );

        return prefacturaContratista;

    }



    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the prefactura
     */
    public String getPrefactura() {
        return prefactura;
    }

    /**
     * @param prefactura the prefactura to set
     */
    public void setPrefactura(String prefactura) {
        this.prefactura = prefactura;
    }







}
