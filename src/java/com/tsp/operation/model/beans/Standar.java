/********************************************************************
 * Nombre Clase.................   Standar.java
 * Descripci�n..................   Bean de la tabla cxp_doc
 * Autor........................   Ivan Dario Gomez
 * Fecha........................   20.10.2007
 * Versi�n......................   1.0
 * Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  igomez
 */
public class Standar implements Serializable{
    
    //Informacion de la cabecera
     private String cod_cli;
     private String Nomcli;
     private String ag_duenia;
     private String pagador;
     private String unidad_negocio;
     private String codigo_sj;
     private String descripcion_sj;
     private String tipo;
     private String standar_general;
     private boolean bloq_despacho;
     private String e_mail;
     private String ag_responsable;
     private boolean aprobado;
     private boolean planeable;
     private String origen;
     private String nom_origen;
     private String destino;
     private String nom_destino;
     private boolean ind_vacio;
     private boolean req_remesa_padre;
     private String tipo_ruta;
     private String codigo_carga;
     private double vlr_tope_carga;
     private String tipo_facturacion;
     private boolean remesaFacturable;
     private int periodoFac_inicial;
     private int periodoFac_final;
     private List listaTarifa;
     private List listaTramos;
     private String desc_carga;
     private String desc_carga_corta;
     
     
     /////
     private String dstrct_code;
     private String frontera_asoc;
     private String account_code_c;
     private String account_code_i;
     private int seq_tarifa;
     private String creation_user;
     private String base;
     
     private int seq_flete;
     
     //////
     
     //informacion de las tarifa
     private double vlr_tarifa;
     private String moneda_tarifa;
     private String unidad_tarifa;
     private boolean vigente_tarifa;
    
     
     
     // informacion de los tramos
     private String cod_origen_tramo;
     private String codigo_destino_tramo;
     private String nombre_destino_tramo;
     private String nombre_origen_tramo;
     private String porcentaje_tramo;
     private String peso_lleno_tramo;
     private int numero_escoltas;
     private String tipo_trafico;
     private boolean ind_vacio_tramo;
     private boolean control_cargue;
     private boolean control_descargue;
     private boolean caravana;
     private boolean requiere_planviaje;
     private boolean preferencia;
     private boolean requiere_hojareporte;
     private boolean contingente;
     private List listaRecursos;
     
     
     
     //informacion de los recursos
     
     private String codigo_recurso;
     private String tipo_recurso;
     private String descripcion_recurso;
     private List listaFletes;
     private int prioridad;
     
     //informacion de los fletes
     
     private double vlr_flete;
     private String moneda_flete;
     private String unidad_flete;
     private boolean vigente_flete;
     
     
     
       
                 
      
         
         
        
    /** Creates a new instance of Documento */
        public Standar() {
        }
    
        /**
         * Getter for property ag_duenia.
         * @return Value of property ag_duenia.
         */
        public java.lang.String getAg_duenia() {
            return ag_duenia;
        }        
    
        /**
         * Setter for property ag_duenia.
         * @param ag_duenia New value of property ag_duenia.
         */
        public void setAg_duenia(java.lang.String ag_duenia) {
            this.ag_duenia = ag_duenia;
        }        
      
        /**
         * Getter for property ag_responsable.
         * @return Value of property ag_responsable.
         */
        public java.lang.String getAg_responsable() {
            return ag_responsable;
        }        
        
        /**
         * Setter for property ag_responsable.
         * @param ag_responsable New value of property ag_responsable.
         */
        public void setAg_responsable(java.lang.String ag_responsable) {
            this.ag_responsable = ag_responsable;
        }
        
        /**
         * Getter for property aprobado.
         * @return Value of property aprobado.
         */
        public boolean isAprobado() {
            return aprobado;
        }
        
        /**
         * Setter for property aprobado.
         * @param aprobado New value of property aprobado.
         */
        public void setAprobado(boolean aprobado) {
            this.aprobado = aprobado;
        }
        
        /**
         * Getter for property bloq_despacho.
         * @return Value of property bloq_despacho.
         */
        public boolean isBloq_despacho() {
            return bloq_despacho;
        }
        
        /**
         * Setter for property bloq_despacho.
         * @param bloq_despacho New value of property bloq_despacho.
         */
        public void setBloq_despacho(boolean bloq_despacho) {
            this.bloq_despacho = bloq_despacho;
        }
        
        /**
         * Getter for property cod_cli.
         * @return Value of property cod_cli.
         */
        public java.lang.String getCod_cli() {
            return cod_cli;
        }
        
        /**
         * Setter for property cod_cli.
         * @param cod_cli New value of property cod_cli.
         */
        public void setCod_cli(java.lang.String cod_cli) {
            this.cod_cli = cod_cli;
        }
        
        /**
         * Getter for property codigo_carga.
         * @return Value of property codigo_carga.
         */
        public java.lang.String getCodigo_carga() {
            return codigo_carga;
        }
        
        /**
         * Setter for property codigo_carga.
         * @param codigo_carga New value of property codigo_carga.
         */
        public void setCodigo_carga(java.lang.String codigo_carga) {
            this.codigo_carga = codigo_carga;
        }
        
        /**
         * Getter for property codigo_sj.
         * @return Value of property codigo_sj.
         */
        public java.lang.String getCodigo_sj() {
            return codigo_sj;
        }
        
        /**
         * Setter for property codigo_sj.
         * @param codigo_sj New value of property codigo_sj.
         */
        public void setCodigo_sj(java.lang.String codigo_sj) {
            this.codigo_sj = codigo_sj;
        }
        
        /**
         * Getter for property descripcion_sj.
         * @return Value of property descripcion_sj.
         */
        public java.lang.String getDescripcion_sj() {
            return descripcion_sj;
        }
        
        /**
         * Setter for property descripcion_sj.
         * @param descripcion_sj New value of property descripcion_sj.
         */
        public void setDescripcion_sj(java.lang.String descripcion_sj) {
            this.descripcion_sj = descripcion_sj;
        }
        
        /**
         * Getter for property destino.
         * @return Value of property destino.
         */
        public java.lang.String getDestino() {
            return destino;
        }
        
        /**
         * Setter for property destino.
         * @param destino New value of property destino.
         */
        public void setDestino(java.lang.String destino) {
            this.destino = destino;
        }
        
        /**
         * Getter for property e_mail.
         * @return Value of property e_mail.
         */
        public java.lang.String getE_mail() {
            return e_mail;
        }
        
        /**
         * Setter for property e_mail.
         * @param e_mail New value of property e_mail.
         */
        public void setE_mail(java.lang.String e_mail) {
            this.e_mail = e_mail;
        }
        
        /**
         * Getter for property ind_vacio.
         * @return Value of property ind_vacio.
         */
        public boolean isInd_vacio() {
            return ind_vacio;
        }
        
        /**
         * Setter for property ind_vacio.
         * @param ind_vacio New value of property ind_vacio.
         */
        public void setInd_vacio(boolean ind_vacio) {
            this.ind_vacio = ind_vacio;
        }
        
        /**
         * Getter for property nom_destino.
         * @return Value of property nom_destino.
         */
        public java.lang.String getNom_destino() {
            return nom_destino;
        }
        
        /**
         * Setter for property nom_destino.
         * @param nom_destino New value of property nom_destino.
         */
        public void setNom_destino(java.lang.String nom_destino) {
            this.nom_destino = nom_destino;
        }
        
        /**
         * Getter for property nom_origen.
         * @return Value of property nom_origen.
         */
        public java.lang.String getNom_origen() {
            return nom_origen;
        }
        
        /**
         * Setter for property nom_origen.
         * @param nom_origen New value of property nom_origen.
         */
        public void setNom_origen(java.lang.String nom_origen) {
            this.nom_origen = nom_origen;
        }
        
        /**
         * Getter for property Nomcli.
         * @return Value of property Nomcli.
         */
        public java.lang.String getNomcli() {
            return Nomcli;
        }
        
        /**
         * Setter for property Nomcli.
         * @param Nomcli New value of property Nomcli.
         */
        public void setNomcli(java.lang.String Nomcli) {
            this.Nomcli = Nomcli;
        }
        
        /**
         * Getter for property origen.
         * @return Value of property origen.
         */
        public java.lang.String getOrigen() {
            return origen;
        }
        
        /**
         * Setter for property origen.
         * @param origen New value of property origen.
         */
        public void setOrigen(java.lang.String origen) {
            this.origen = origen;
        }
        
        /**
         * Getter for property pagador.
         * @return Value of property pagador.
         */
        public java.lang.String getPagador() {
            return pagador;
        }
        
        /**
         * Setter for property pagador.
         * @param pagador New value of property pagador.
         */
        public void setPagador(java.lang.String pagador) {
            this.pagador = pagador;
        }
        
        /**
         * Getter for property periodoFac_final.
         * @return Value of property periodoFac_final.
         */
        public int getPeriodoFac_final() {
            return periodoFac_final;
        }
        
        /**
         * Setter for property periodoFac_final.
         * @param periodoFac_final New value of property periodoFac_final.
         */
        public void setPeriodoFac_final(int periodoFac_final) {
            this.periodoFac_final = periodoFac_final;
        }
        
        /**
         * Getter for property periodoFac_inicial.
         * @return Value of property periodoFac_inicial.
         */
        public int getPeriodoFac_inicial() {
            return periodoFac_inicial;
        }
        
        /**
         * Setter for property periodoFac_inicial.
         * @param periodoFac_inicial New value of property periodoFac_inicial.
         */
        public void setPeriodoFac_inicial(int periodoFac_inicial) {
            this.periodoFac_inicial = periodoFac_inicial;
        }
        
        /**
         * Getter for property planeable.
         * @return Value of property planeable.
         */
        public boolean isPlaneable() {
            return planeable;
        }
        
        /**
         * Setter for property planeable.
         * @param planeable New value of property planeable.
         */
        public void setPlaneable(boolean planeable) {
            this.planeable = planeable;
        }
        
        /**
         * Getter for property remesaFacturable.
         * @return Value of property remesaFacturable.
         */
        public boolean isRemesaFacturable() {
            return remesaFacturable;
        }
        
        /**
         * Setter for property remesaFacturable.
         * @param remesaFacturable New value of property remesaFacturable.
         */
        public void setRemesaFacturable(boolean remesaFacturable) {
            this.remesaFacturable = remesaFacturable;
        }
        
        /**
         * Getter for property req_remesa_padre.
         * @return Value of property req_remesa_padre.
         */
        public boolean isReq_remesa_padre() {
            return req_remesa_padre;
        }
        
        /**
         * Setter for property req_remesa_padre.
         * @param req_remesa_padre New value of property req_remesa_padre.
         */
        public void setReq_remesa_padre(boolean req_remesa_padre) {
            this.req_remesa_padre = req_remesa_padre;
        }
        
        /**
         * Getter for property standar_general.
         * @return Value of property standar_general.
         */
        public java.lang.String getStandar_general() {
            return standar_general;
        }
        
        /**
         * Setter for property standar_general.
         * @param standar_general New value of property standar_general.
         */
        public void setStandar_general(java.lang.String standar_general) {
            this.standar_general = standar_general;
        }
        
        /**
         * Getter for property tipo.
         * @return Value of property tipo.
         */
        public java.lang.String getTipo() {
            return tipo;
        }
        
        /**
         * Setter for property tipo.
         * @param tipo New value of property tipo.
         */
        public void setTipo(java.lang.String tipo) {
            this.tipo = tipo;
        }
        
        /**
         * Getter for property tipo_facturacion.
         * @return Value of property tipo_facturacion.
         */
        public java.lang.String getTipo_facturacion() {
            return tipo_facturacion;
        }
        
        /**
         * Setter for property tipo_facturacion.
         * @param tipo_facturacion New value of property tipo_facturacion.
         */
        public void setTipo_facturacion(java.lang.String tipo_facturacion) {
            this.tipo_facturacion = tipo_facturacion;
        }
        
        /**
         * Getter for property tipo_ruta.
         * @return Value of property tipo_ruta.
         */
        public java.lang.String getTipo_ruta() {
            return tipo_ruta;
        }
        
        /**
         * Setter for property tipo_ruta.
         * @param tipo_ruta New value of property tipo_ruta.
         */
        public void setTipo_ruta(java.lang.String tipo_ruta) {
            this.tipo_ruta = tipo_ruta;
        }
        
        /**
         * Getter for property unidad_negocio.
         * @return Value of property unidad_negocio.
         */
        public java.lang.String getUnidad_negocio() {
            return unidad_negocio;
        }
        
        /**
         * Setter for property unidad_negocio.
         * @param unidad_negocio New value of property unidad_negocio.
         */
        public void setUnidad_negocio(java.lang.String unidad_negocio) {
            this.unidad_negocio = unidad_negocio;
        }
        
        /**
         * Getter for property vlr_tope_carga.
         * @return Value of property vlr_tope_carga.
         */
        public double getVlr_tope_carga() {
            return vlr_tope_carga;
        }
        
        /**
         * Setter for property vlr_tope_carga.
         * @param vlr_tope_carga New value of property vlr_tope_carga.
         */
        public void setVlr_tope_carga(double vlr_tope_carga) {
            this.vlr_tope_carga = vlr_tope_carga;
        }
        
        /**
         * Getter for property caravana.
         * @return Value of property caravana.
         */
        public boolean isCaravana() {
            return caravana;
        }
        
        /**
         * Setter for property caravana.
         * @param caravana New value of property caravana.
         */
        public void setCaravana(boolean caravana) {
            this.caravana = caravana;
        }
        
        /**
         * Getter for property cod_origen_tramo.
         * @return Value of property cod_origen_tramo.
         */
        public java.lang.String getCod_origen_tramo() {
            return cod_origen_tramo;
        }
        
        /**
         * Setter for property cod_origen_tramo.
         * @param cod_origen_tramo New value of property cod_origen_tramo.
         */
        public void setCod_origen_tramo(java.lang.String cod_origen_tramo) {
            this.cod_origen_tramo = cod_origen_tramo;
        }
        
        /**
         * Getter for property codigo_destino_tramo.
         * @return Value of property codigo_destino_tramo.
         */
        public java.lang.String getCodigo_destino_tramo() {
            return codigo_destino_tramo;
        }
        
        /**
         * Setter for property codigo_destino_tramo.
         * @param codigo_destino_tramo New value of property codigo_destino_tramo.
         */
        public void setCodigo_destino_tramo(java.lang.String codigo_destino_tramo) {
            this.codigo_destino_tramo = codigo_destino_tramo;
        }
        
        /**
         * Getter for property codigo_recurso.
         * @return Value of property codigo_recurso.
         */
        public java.lang.String getCodigo_recurso() {
            return codigo_recurso;
        }
        
        /**
         * Setter for property codigo_recurso.
         * @param codigo_recurso New value of property codigo_recurso.
         */
        public void setCodigo_recurso(java.lang.String codigo_recurso) {
            this.codigo_recurso = codigo_recurso;
        }
        
        /**
         * Getter for property contingente.
         * @return Value of property contingente.
         */
        public boolean isContingente() {
            return contingente;
        }
        
        /**
         * Setter for property contingente.
         * @param contingente New value of property contingente.
         */
        public void setContingente(boolean contingente) {
            this.contingente = contingente;
        }
        
        /**
         * Getter for property control_descargue.
         * @return Value of property control_descargue.
         */
        public boolean isControl_descargue() {
            return control_descargue;
        }
        
        /**
         * Setter for property control_descargue.
         * @param control_descargue New value of property control_descargue.
         */
        public void setControl_descargue(boolean control_descargue) {
            this.control_descargue = control_descargue;
        }
        
        /**
         * Getter for property control_gargue.
         * @return Value of property control_gargue.
         */
        public boolean isControl_cargue() {
            return control_cargue;
        }
        
        /**
         * Setter for property control_gargue.
         * @param control_gargue New value of property control_gargue.
         */
        public void setControl_cargue(boolean control_cargue) {
            this.control_cargue = control_cargue;
        }
        
        /**
         * Getter for property descripcion_recurso.
         * @return Value of property descripcion_recurso.
         */
        public java.lang.String getDescripcion_recurso() {
            return descripcion_recurso;
        }
        
        /**
         * Setter for property descripcion_recurso.
         * @param descripcion_recurso New value of property descripcion_recurso.
         */
        public void setDescripcion_recurso(java.lang.String descripcion_recurso) {
            this.descripcion_recurso = descripcion_recurso;
        }
        
        /**
         * Getter for property ind_vacio_tramo.
         * @return Value of property ind_vacio_tramo.
         */
        public boolean isInd_vacio_tramo() {
            return ind_vacio_tramo;
        }
        
        /**
         * Setter for property ind_vacio_tramo.
         * @param ind_vacio_tramo New value of property ind_vacio_tramo.
         */
        public void setInd_vacio_tramo(boolean ind_vacio_tramo) {
            this.ind_vacio_tramo = ind_vacio_tramo;
        }
        
        /**
         * Getter for property listaRecursos.
         * @return Value of property listaRecursos.
         */
        public java.util.List getListaRecursos() {
            return listaRecursos;
        }
        
        /**
         * Setter for property listaRecursos.
         * @param listaRecursos New value of property listaRecursos.
         */
        public void setListaRecursos(java.util.List listaRecursos) {
            this.listaRecursos = listaRecursos;
        }
        
        /**
         * Getter for property listaTarifa.
         * @return Value of property listaTarifa.
         */
        public java.util.List getListaTarifa() {
            return listaTarifa;
        }
        
        /**
         * Setter for property listaTarifa.
         * @param listaTarifa New value of property listaTarifa.
         */
        public void setListaTarifa(java.util.List listaTarifa) {
            this.listaTarifa = listaTarifa;
        }
        
        /**
         * Getter for property listaTramos.
         * @return Value of property listaTramos.
         */
        public java.util.List getListaTramos() {
            return listaTramos;
        }
        
        /**
         * Setter for property listaTramos.
         * @param listaTramos New value of property listaTramos.
         */
        public void setListaTramos(java.util.List listaTramos) {
            this.listaTramos = listaTramos;
        }
        
        /**
         * Getter for property moneda_flete.
         * @return Value of property moneda_flete.
         */
        public java.lang.String getMoneda_flete() {
            return moneda_flete;
        }
        
        /**
         * Setter for property moneda_flete.
         * @param moneda_flete New value of property moneda_flete.
         */
        public void setMoneda_flete(java.lang.String moneda_flete) {
            this.moneda_flete = moneda_flete;
        }
        
        /**
         * Getter for property moneda_tarifa.
         * @return Value of property moneda_tarifa.
         */
        public java.lang.String getMoneda_tarifa() {
            return moneda_tarifa;
        }
        
        /**
         * Setter for property moneda_tarifa.
         * @param moneda_tarifa New value of property moneda_tarifa.
         */
        public void setMoneda_tarifa(java.lang.String moneda_tarifa) {
            this.moneda_tarifa = moneda_tarifa;
        }
        
        /**
         * Getter for property nombre_destino_tramo.
         * @return Value of property nombre_destino_tramo.
         */
        public java.lang.String getNombre_destino_tramo() {
            return nombre_destino_tramo;
        }
        
        /**
         * Setter for property nombre_destino_tramo.
         * @param nombre_destino_tramo New value of property nombre_destino_tramo.
         */
        public void setNombre_destino_tramo(java.lang.String nombre_destino_tramo) {
            this.nombre_destino_tramo = nombre_destino_tramo;
        }
        
        /**
         * Getter for property nombre_origen_tramo.
         * @return Value of property nombre_origen_tramo.
         */
        public java.lang.String getNombre_origen_tramo() {
            return nombre_origen_tramo;
        }
        
        /**
         * Setter for property nombre_origen_tramo.
         * @param nombre_origen_tramo New value of property nombre_origen_tramo.
         */
        public void setNombre_origen_tramo(java.lang.String nombre_origen_tramo) {
            this.nombre_origen_tramo = nombre_origen_tramo;
        }
        
        /**
         * Getter for property numero_escoltas.
         * @return Value of property numero_escoltas.
         */
        public int getNumero_escoltas() {
            return numero_escoltas;
        }
        
        /**
         * Setter for property numero_escoltas.
         * @param numero_escoltas New value of property numero_escoltas.
         */
        public void setNumero_escoltas(int numero_escoltas) {
            this.numero_escoltas = numero_escoltas;
        }
        
        /**
         * Getter for property peso_lleno_tramo.
         * @return Value of property peso_lleno_tramo.
         */
        public java.lang.String getPeso_lleno_tramo() {
            return peso_lleno_tramo;
        }
        
        /**
         * Setter for property peso_lleno_tramo.
         * @param peso_lleno_tramo New value of property peso_lleno_tramo.
         */
        public void setPeso_lleno_tramo(java.lang.String peso_lleno_tramo) {
            this.peso_lleno_tramo = peso_lleno_tramo;
        }
        
        /**
         * Getter for property porcentaje_tramo.
         * @return Value of property porcentaje_tramo.
         */
        public java.lang.String getPorcentaje_tramo() {
            return porcentaje_tramo;
        }
        
        /**
         * Setter for property porcentaje_tramo.
         * @param porcentaje_tramo New value of property porcentaje_tramo.
         */
        public void setPorcentaje_tramo(java.lang.String porcentaje_tramo) {
            this.porcentaje_tramo = porcentaje_tramo;
        }
        
        /**
         * Getter for property preferencia.
         * @return Value of property preferencia.
         */
        public boolean isPreferencia() {
            return preferencia;
        }
        
        /**
         * Setter for property preferencia.
         * @param preferencia New value of property preferencia.
         */
        public void setPreferencia(boolean preferencia) {
            this.preferencia = preferencia;
        }
        
        /**
         * Getter for property requiere_hojareporte.
         * @return Value of property requiere_hojareporte.
         */
        public boolean isRequiere_hojareporte() {
            return requiere_hojareporte;
        }
        
        /**
         * Setter for property requiere_hojareporte.
         * @param requiere_hojareporte New value of property requiere_hojareporte.
         */
        public void setRequiere_hojareporte(boolean requiere_hojareporte) {
            this.requiere_hojareporte = requiere_hojareporte;
        }
        
        /**
         * Getter for property requiere_planviaje.
         * @return Value of property requiere_planviaje.
         */
        public boolean isRequiere_planviaje() {
            return requiere_planviaje;
        }
        
        /**
         * Setter for property requiere_planviaje.
         * @param requiere_planviaje New value of property requiere_planviaje.
         */
        public void setRequiere_planviaje(boolean requiere_planviaje) {
            this.requiere_planviaje = requiere_planviaje;
        }
        
        /**
         * Getter for property tipo_recurso.
         * @return Value of property tipo_recurso.
         */
        public java.lang.String getTipo_recurso() {
            return tipo_recurso;
        }
        
        /**
         * Setter for property tipo_recurso.
         * @param tipo_recurso New value of property tipo_recurso.
         */
        public void setTipo_recurso(java.lang.String tipo_recurso) {
            this.tipo_recurso = tipo_recurso;
        }
        
        /**
         * Getter for property tipo_trafico.
         * @return Value of property tipo_trafico.
         */
        public java.lang.String getTipo_trafico() {
            return tipo_trafico;
        }
        
        /**
         * Setter for property tipo_trafico.
         * @param tipo_trafico New value of property tipo_trafico.
         */
        public void setTipo_trafico(java.lang.String tipo_trafico) {
            this.tipo_trafico = tipo_trafico;
        }
        
        /**
         * Getter for property unidad_flete.
         * @return Value of property unidad_flete.
         */
        public java.lang.String getUnidad_flete() {
            return unidad_flete;
        }
        
        /**
         * Setter for property unidad_flete.
         * @param unidad_flete New value of property unidad_flete.
         */
        public void setUnidad_flete(java.lang.String unidad_flete) {
            this.unidad_flete = unidad_flete;
        }
        
        /**
         * Getter for property unidad_tarifa.
         * @return Value of property unidad_tarifa.
         */
        public java.lang.String getUnidad_tarifa() {
            return unidad_tarifa;
        }
        
        /**
         * Setter for property unidad_tarifa.
         * @param unidad_tarifa New value of property unidad_tarifa.
         */
        public void setUnidad_tarifa(java.lang.String unidad_tarifa) {
            this.unidad_tarifa = unidad_tarifa;
        }
        
        /**
         * Getter for property vigente_flete.
         * @return Value of property vigente_flete.
         */
        public boolean isVigente_flete() {
            return vigente_flete;
        }
        
        /**
         * Setter for property vigente_flete.
         * @param vigente_flete New value of property vigente_flete.
         */
        public void setVigente_flete(boolean vigente_flete) {
            this.vigente_flete = vigente_flete;
        }
        
        /**
         * Getter for property vigente_tarifa.
         * @return Value of property vigente_tarifa.
         */
        public boolean isVigente_tarifa() {
            return vigente_tarifa;
        }
        
        /**
         * Setter for property vigente_tarifa.
         * @param vigente_tarifa New value of property vigente_tarifa.
         */
        public void setVigente_tarifa(boolean vigente_tarifa) {
            this.vigente_tarifa = vigente_tarifa;
        }
        
        /**
         * Getter for property vlr_flete.
         * @return Value of property vlr_flete.
         */
        public double getVlr_flete() {
            return vlr_flete;
        }
        
        /**
         * Setter for property vlr_flete.
         * @param vlr_flete New value of property vlr_flete.
         */
        public void setVlr_flete(double vlr_flete) {
            this.vlr_flete = vlr_flete;
        }
        
        /**
         * Getter for property vlr_tarifa.
         * @return Value of property vlr_tarifa.
         */
        public double getVlr_tarifa() {
            return vlr_tarifa;
        }
        
        /**
         * Setter for property vlr_tarifa.
         * @param vlr_tarifa New value of property vlr_tarifa.
         */
        public void setVlr_tarifa(double vlr_tarifa) {
            this.vlr_tarifa = vlr_tarifa;
        }
        
        /**
         * Getter for property listaFletes.
         * @return Value of property listaFletes.
         */
        public java.util.List getListaFletes() {
            return listaFletes;
        }
        
        /**
         * Setter for property listaFletes.
         * @param listaFletes New value of property listaFletes.
         */
        public void setListaFletes(java.util.List listaFletes) {
            this.listaFletes = listaFletes;
        }
        
        /**
         * Getter for property desc_carga.
         * @return Value of property desc_carga.
         */
        public java.lang.String getDesc_carga() {
            return desc_carga;
        }
        
        /**
         * Setter for property desc_carga.
         * @param desc_carga New value of property desc_carga.
         */
        public void setDesc_carga(java.lang.String desc_carga) {
            this.desc_carga = desc_carga;
        }
        
        /**
         * Getter for property desc_carga_corta.
         * @return Value of property desc_carga_corta.
         */
        public java.lang.String getDesc_carga_corta() {
            return desc_carga_corta;
        }
        
        /**
         * Setter for property desc_carga_corta.
         * @param desc_carga_corta New value of property desc_carga_corta.
         */
        public void setDesc_carga_corta(java.lang.String desc_carga_corta) {
            this.desc_carga_corta = desc_carga_corta;
        }
        
        /**
         * Getter for property dstrct_code.
         * @return Value of property dstrct_code.
         */
        public java.lang.String getDstrct_code() {
            return dstrct_code;
        }
        
        /**
         * Setter for property dstrct_code.
         * @param dstrct_code New value of property dstrct_code.
         */
        public void setDstrct_code(java.lang.String dstrct_code) {
            this.dstrct_code = dstrct_code;
        }
        
        /**
         * Getter for property frontera_asoc.
         * @return Value of property frontera_asoc.
         */
        public java.lang.String getFrontera_asoc() {
            return frontera_asoc;
        }
        
        /**
         * Setter for property frontera_asoc.
         * @param frontera_asoc New value of property frontera_asoc.
         */
        public void setFrontera_asoc(java.lang.String frontera_asoc) {
            this.frontera_asoc = frontera_asoc;
        }
        
        /**
         * Getter for property account_code_c.
         * @return Value of property account_code_c.
         */
        public java.lang.String getAccount_code_c() {
            return account_code_c;
        }
        
        /**
         * Setter for property account_code_c.
         * @param account_code_c New value of property account_code_c.
         */
        public void setAccount_code_c(java.lang.String account_code_c) {
            this.account_code_c = account_code_c;
        }
        
        /**
         * Getter for property account_code_i.
         * @return Value of property account_code_i.
         */
        public java.lang.String getAccount_code_i() {
            return account_code_i;
        }
        
        /**
         * Setter for property account_code_i.
         * @param account_code_i New value of property account_code_i.
         */
        public void setAccount_code_i(java.lang.String account_code_i) {
            this.account_code_i = account_code_i;
        }
        
        /**
         * Getter for property seq_tarifa.
         * @return Value of property seq_tarifa.
         */
        public int getSeq_tarifa() {
            return seq_tarifa;
        }
        
        /**
         * Setter for property seq_tarifa.
         * @param seq_tarifa New value of property seq_tarifa.
         */
        public void setSeq_tarifa(int seq_tarifa) {
            this.seq_tarifa = seq_tarifa;
        }
        
        /**
         * Getter for property creation_user.
         * @return Value of property creation_user.
         */
        public java.lang.String getCreation_user() {
            return creation_user;
        }
        
        /**
         * Setter for property creation_user.
         * @param creation_user New value of property creation_user.
         */
        public void setCreation_user(java.lang.String creation_user) {
            this.creation_user = creation_user;
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
            return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
            this.base = base;
        }
        
        /**
         * Getter for property seq_flete.
         * @return Value of property seq_flete.
         */
        public int getSeq_flete() {
            return seq_flete;
        }
        
        /**
         * Setter for property seq_flete.
         * @param seq_flete New value of property seq_flete.
         */
        public void setSeq_flete(int seq_flete) {
            this.seq_flete = seq_flete;
        }
        
        /**
         * Getter for property prioridad.
         * @return Value of property prioridad.
         */
        public int getPrioridad() {
            return prioridad;
        }
        
        /**
         * Setter for property prioridad.
         * @param prioridad New value of property prioridad.
         */
        public void setPrioridad(int prioridad) {
            this.prioridad = prioridad;
        }
        
}

