/***********************************************************************************
 * Nombre clase : ............... CXPObservacionItem.java                          *
 * Descripcion :................. Clase que representa con sus atributos la tabla  *
 *                                cxpobservacionitem (Observacion de un item)      *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 8 de octubre de 2005, 17:35                      *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.beans;

public class CXPObservacionItem {
    
        private String reg_status;
        private String dstrct;
	private String proveedor;
	private String tipo_documento;
	private String documento;
	private String descripcion;
        private String item;
        private String observacion_autorizador;
        private String fecha_ob_autorizador;
        private String usuario_ob_autorizador;
        private String fecha_ob_pagador;
        private String usuario_ob_pagador;
        private String ob_registra_factura;
        private String fecha_ob_reg_factura;
        private String usuario_ob_reg_factura;
        private String observacion_pagador;
        private String cierre_observacion;
        private String textoactivo;
        
	private String last_update;
	private String user_update;
	private String creation_date;
	private String creation_user;
	private String base;                
    
    /** Creates a new instance of Documento */
        public CXPObservacionItem() {
        }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }  
   
        
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }    
   
    /**
     * Getter for property observacion_autorizador.
     * @return Value of property observacion_autorizador.
     */
    public java.lang.String getObservacion_autorizador() {
        return observacion_autorizador;
    }    
    
    /**
     * Setter for property observacion_autorizador.
     * @param observacion_autorizador New value of property observacion_autorizador.
     */
    public void setObservacion_autorizador(java.lang.String observacion_autorizador) {
        this.observacion_autorizador = observacion_autorizador;
    }
    
    /**
     * Getter for property fecha_ob_autorizador.
     * @return Value of property fecha_ob_autorizador.
     */
    public java.lang.String getFecha_ob_autorizador() {
        return fecha_ob_autorizador;
    }
    
    /**
     * Setter for property fecha_ob_autorizador.
     * @param fecha_ob_autorizador New value of property fecha_ob_autorizador.
     */
    public void setFecha_ob_autorizador(java.lang.String fecha_ob_autorizador) {
        this.fecha_ob_autorizador = fecha_ob_autorizador;
    }
    
    /**
     * Getter for property usuario_ob_autorizador.
     * @return Value of property usuario_ob_autorizador.
     */
    public java.lang.String getUsuario_ob_autorizador() {
        return usuario_ob_autorizador;
    }
    
    /**
     * Setter for property usuario_ob_autorizador.
     * @param usuario_ob_autorizador New value of property usuario_ob_autorizador.
     */
    public void setUsuario_ob_autorizador(java.lang.String usuario_ob_autorizador) {
        this.usuario_ob_autorizador = usuario_ob_autorizador;
    }
    
    /**
     * Getter for property fecha_ob_pagador.
     * @return Value of property fecha_ob_pagador.
     */
    public java.lang.String getFecha_ob_pagador() {
        return fecha_ob_pagador;
    }
    
    /**
     * Setter for property fecha_ob_pagador.
     * @param fecha_ob_pagador New value of property fecha_ob_pagador.
     */
    public void setFecha_ob_pagador(java.lang.String fecha_ob_pagador) {
        this.fecha_ob_pagador = fecha_ob_pagador;
    }
    
    /**
     * Getter for property usuario_ob_pagador.
     * @return Value of property usuario_ob_pagador.
     */
    public java.lang.String getUsuario_ob_pagador() {
        return usuario_ob_pagador;
    }
    
    /**
     * Setter for property usuario_ob_pagador.
     * @param usuario_ob_pagador New value of property usuario_ob_pagador.
     */
    public void setUsuario_ob_pagador(java.lang.String usuario_ob_pagador) {
        this.usuario_ob_pagador = usuario_ob_pagador;
    }
    
    /**
     * Getter for property ob_registra_factura.
     * @return Value of property ob_registra_factura.
     */
    public java.lang.String getOb_registra_factura() {
        return ob_registra_factura;
    }    
    
    /**
     * Setter for property ob_registra_factura.
     * @param ob_registra_factura New value of property ob_registra_factura.
     */
    public void setOb_registra_factura(java.lang.String ob_registra_factura) {
        this.ob_registra_factura = ob_registra_factura;
    }
    
    /**
     * Getter for property fecha_ob_reg_factura.
     * @return Value of property fecha_ob_reg_factura.
     */
    public java.lang.String getFecha_ob_reg_factura() {
        return fecha_ob_reg_factura;
    }
    
    /**
     * Setter for property fecha_ob_reg_factura.
     * @param fecha_ob_reg_factura New value of property fecha_ob_reg_factura.
     */
    public void setFecha_ob_reg_factura(java.lang.String fecha_ob_reg_factura) {
        this.fecha_ob_reg_factura = fecha_ob_reg_factura;
    }
    
    /**
     * Getter for property usuario_ob_reg_factura.
     * @return Value of property usuario_ob_reg_factura.
     */
    public java.lang.String getUsuario_ob_reg_factura() {
        return usuario_ob_reg_factura;
    }
    
    /**
     * Setter for property usuario_ob_reg_factura.
     * @param usuario_ob_reg_factura New value of property usuario_ob_reg_factura.
     */
    public void setUsuario_ob_reg_factura(java.lang.String usuario_ob_reg_factura) {
        this.usuario_ob_reg_factura = usuario_ob_reg_factura;
    }
    
    /**
     * Getter for property observacion_pagador.
     * @return Value of property observacion_pagador.
     */
    public java.lang.String getObservacion_pagador() {
        return observacion_pagador;
    }
    
    /**
     * Setter for property observacion_pagador.
     * @param observacion_pagador New value of property observacion_pagador.
     */
    public void setObservacion_pagador(java.lang.String observacion_pagador) {
        this.observacion_pagador = observacion_pagador;
    }
    
    /**
     * Getter for property cierre_observacion.
     * @return Value of property cierre_observacion.
     */
    public java.lang.String getCierre_observacion() {
        return cierre_observacion;
    }
    
    /**
     * Setter for property cierre_observacion.
     * @param cierre_observacion New value of property cierre_observacion.
     */
    public void setCierre_observacion(java.lang.String cierre_observacion) {
        this.cierre_observacion = cierre_observacion;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property textoactivo.
     * @return Value of property textoactivo.
     */
    public java.lang.String getTextoactivo() {
        return textoactivo;
    }
    
    /**
     * Setter for property textoactivo.
     * @param textoactivo New value of property textoactivo.
     */
    public void setTextoactivo(java.lang.String textoactivo) {
        this.textoactivo = textoactivo;
    }
    
}
