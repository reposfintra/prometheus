/*
 * Nombre        ReporteDiferenciaTiempos.java
 * Autor         Ing. Jose de la rosa
 * Fecha         15 de junio del 2006, 05:56 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class ReporteDiferenciaTiempos implements Serializable{
    
    private String planilla;
    private String remesa;
    private String cliente;
    private String std_job_no;
    private String std_job_desc;
    private String fecha_despacho;
    private String placa;
    private String origen;
    private String destino;
    private String agencia;
    private String fecha_carge_planeada;
    private String fecha_descarge_planeada;
    private String fecha_carge_trafico;
    private String fecha_descarge_trafico;
    private String diferencia_carge;
    private String diferencia_descarge;
    private String cabezote;
    
    /** Creates a new instance of ReporteDiferenciaTiempos */
    public ReporteDiferenciaTiempos () {
    }
    
    public static ReporteDiferenciaTiempos load (ResultSet rs)throws SQLException {
        ReporteDiferenciaTiempos rp = new ReporteDiferenciaTiempos ();
        rp.setPlanilla ( rs.getString ("numpla") );
        rp.setRemesa (rs.getString ("numrem"));
        rp.setCliente (rs.getString ("nomcli"));
        rp.setStd_job_no (rs.getString ("std_job_no"));
        rp.setStd_job_desc (rs.getString ("std_job_desc"));
        rp.setFecha_despacho (rs.getString ("fecdsp"));
        rp.setPlaca (rs.getString ("plaveh"));
        rp.setOrigen (rs.getString ("nom_origen"));
        rp.setDestino (rs.getString ("nom_destino"));
        rp.setAgencia (rs.getString ("agencia"));
        rp.setFecha_carge_planeada (rs.getString ("feccarpla")!=null?rs.getString ("feccarpla"):"");
        rp.setFecha_descarge_planeada (rs.getString ("fecdespla")!=null?rs.getString ("fecdespla"):"");
        rp.setFecha_carge_trafico (rs.getString ("feccartrf")!=null?rs.getString ("feccartrf"):"");
        rp.setFecha_descarge_trafico (rs.getString ("fecdestrf")!=null?rs.getString ("fecdestrf"):"");
        rp.setDiferencia_carge (rs.getString ("diferencia_carge")!=null?rs.getString ("diferencia_carge"):"");
        int tiempo_carge = 0;
        if( !rp.getDiferencia_carge ().equals ("") ){
            if( rp.getDiferencia_carge ().indexOf ("days") != -1 ){
                String dif [] = rp.getDiferencia_carge ().split ("days");
                tiempo_carge = Integer.parseInt ( dif[0].trim () ) * 24;
                String hora [] = dif[1].trim ().split (":");
                tiempo_carge +=  Integer.parseInt ( hora [0].trim () );
            }
            else if( rp.getDiferencia_carge ().indexOf ("day") != -1 ){
                String dif [] = rp.getDiferencia_carge ().split ("day");
                tiempo_carge = Integer.parseInt ( dif[0].trim () ) * 24;
                String hora [] = dif[1].trim ().split (":");
                tiempo_carge +=  Integer.parseInt ( hora [0].trim () );
            }            
            else{
                if( rp.getDiferencia_carge ().indexOf (":") != -1 ){
                    String horas [] = rp.getDiferencia_carge ().split (":");
                    tiempo_carge =  Integer.parseInt ( horas [0].trim () );
                }
            }
        }
        rp.setDiferencia_carge ( tiempo_carge != 0? tiempo_carge+"" : "" );
        tiempo_carge = 0;
        rp.setDiferencia_descarge (rs.getString ("diferencia_descarge")!=null?rs.getString ("diferencia_descarge"):"");
        if( !rp.getDiferencia_descarge ().equals ("") ){
            if( rp.getDiferencia_descarge ().indexOf ("days") != -1 ){
                String dif [] = rp.getDiferencia_descarge ().split ("days");
                tiempo_carge = Integer.parseInt ( dif[0].trim () ) * 24;
                String hora [] = dif[1].trim ().split (":");
                tiempo_carge +=  Integer.parseInt ( hora [0].trim () );
            }
            else if( rp.getDiferencia_descarge ().indexOf ("day") != -1 ){
                String dif [] = rp.getDiferencia_carge ().split ("day");
                tiempo_carge = Integer.parseInt ( dif[0].trim () ) * 24;
                String hora [] = dif[1].trim ().split (":");
                tiempo_carge +=  Integer.parseInt ( hora [0].trim () );
            } 
            else{
                if( rp.getDiferencia_descarge ().indexOf (":") != -1 ){
                    String horas [] = rp.getDiferencia_descarge ().split (":");
                    tiempo_carge =  Integer.parseInt ( horas [0].trim () );
                }
            }
        }
        rp.setDiferencia_descarge ( tiempo_carge != 0? tiempo_carge+"" : "" );
        return rp;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia () {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia (java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente () {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente (java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino () {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino (java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property fecha_carge_planeada.
     * @return Value of property fecha_carge_planeada.
     */
    public java.lang.String getFecha_carge_planeada () {
        return fecha_carge_planeada;
    }
    
    /**
     * Setter for property fecha_carge_planeada.
     * @param fecha_carge_planeada New value of property fecha_carge_planeada.
     */
    public void setFecha_carge_planeada (java.lang.String fecha_carge_planeada) {
        this.fecha_carge_planeada = fecha_carge_planeada;
    }
    
    /**
     * Getter for property fecha_carge_trafico.
     * @return Value of property fecha_carge_trafico.
     */
    public java.lang.String getFecha_carge_trafico () {
        return fecha_carge_trafico;
    }
    
    /**
     * Setter for property fecha_carge_trafico.
     * @param fecha_carge_trafico New value of property fecha_carge_trafico.
     */
    public void setFecha_carge_trafico (java.lang.String fecha_carge_trafico) {
        this.fecha_carge_trafico = fecha_carge_trafico;
    }
    
    /**
     * Getter for property fecha_cumplido.
     * @return Value of property fecha_cumplido.
     */
    public java.lang.String getFecha_despacho () {
        return fecha_despacho;
    }
    
    /**
     * Setter for property fecha_cumplido.
     * @param fecha_cumplido New value of property fecha_cumplido.
     */
    public void setFecha_despacho (java.lang.String fecha_despacho) {
        this.fecha_despacho = fecha_despacho;
    }
    
    /**
     * Getter for property fecha_descarge_planeada.
     * @return Value of property fecha_descarge_planeada.
     */
    public java.lang.String getFecha_descarge_planeada () {
        return fecha_descarge_planeada;
    }
    
    /**
     * Setter for property fecha_descarge_planeada.
     * @param fecha_descarge_planeada New value of property fecha_descarge_planeada.
     */
    public void setFecha_descarge_planeada (java.lang.String fecha_descarge_planeada) {
        this.fecha_descarge_planeada = fecha_descarge_planeada;
    }
    
    /**
     * Getter for property fecha_descarge_trafico.
     * @return Value of property fecha_descarge_trafico.
     */
    public java.lang.String getFecha_descarge_trafico () {
        return fecha_descarge_trafico;
    }
    
    /**
     * Setter for property fecha_descarge_trafico.
     * @param fecha_descarge_trafico New value of property fecha_descarge_trafico.
     */
    public void setFecha_descarge_trafico (java.lang.String fecha_descarge_trafico) {
        this.fecha_descarge_trafico = fecha_descarge_trafico;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen () {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen (java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca () {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca (java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla () {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla (java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa () {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa (java.lang.String remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property std_job_desc.
     * @return Value of property std_job_desc.
     */
    public java.lang.String getStd_job_desc () {
        return std_job_desc;
    }
    
    /**
     * Setter for property std_job_desc.
     * @param std_job_desc New value of property std_job_desc.
     */
    public void setStd_job_desc (java.lang.String std_job_desc) {
        this.std_job_desc = std_job_desc;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no () {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no (java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property diferencia_carge.
     * @return Value of property diferencia_carge.
     */
    public java.lang.String getDiferencia_carge () {
        return diferencia_carge;
    }
    
    /**
     * Setter for property diferencia_carge.
     * @param diferencia_carge New value of property diferencia_carge.
     */
    public void setDiferencia_carge (java.lang.String diferencia_carge) {
        this.diferencia_carge = diferencia_carge;
    }
    
    /**
     * Getter for property diferencia_descarge.
     * @return Value of property diferencia_descarge.
     */
    public java.lang.String getDiferencia_descarge () {
        return diferencia_descarge;
    }
    
    /**
     * Setter for property diferencia_descarge.
     * @param diferencia_descarge New value of property diferencia_descarge.
     */
    public void setDiferencia_descarge (java.lang.String diferencia_descarge) {
        this.diferencia_descarge = diferencia_descarge;
    }
    
    /**
     * Getter for property cabezote.
     * @return Value of property cabezote.
     */
    public java.lang.String getCabezote() {
        return cabezote;
    }
    
    /**
     * Setter for property cabezote.
     * @param cabezote New value of property cabezote.
     */
    public void setCabezote(java.lang.String cabezote) {
        this.cabezote = cabezote;
    }
    
}
