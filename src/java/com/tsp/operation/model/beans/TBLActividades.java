/*
 * TBLActividades.java
 *
 * Created on 13 de octubre de 2005, 08:03 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;

/**
 *
 * @author  LEONARDO PARODY
 */
public class TBLActividades implements Serializable {
    private String Codigo;
    private String Descripcion;
    private String Sigla;
    private String Creation_user;
    private String Dstrct_code;
    private java.util.Date Last_update;
    private String User_update;
    private java.util.Date Creation_date;
    private String Status;
    
    public static TBLActividades load(ResultSet rs)throws SQLException {
        
        TBLActividades actividades = new TBLActividades();          
        actividades.setCodigo( rs.getString("activity_type") );
        ////System.out.println("getBD: "+rs.getString("activity_type")+", objetoactividades: "+actividades.getCodigo());
        actividades.setSigla( rs.getString("sigla") );
        actividades.setDescripcion( rs.getString("activity_name") );
        actividades.setCreation_user( rs.getString("creation_user"));
        actividades.setDstrct_code ( rs.getString("dstrct_code"));
        actividades.setLast_update ( rs.getDate("last_update"));
        actividades.setUser_update (rs.getString("user_update"));
        actividades.setCreation_date (rs.getDate("creation_date"));
        actividades.setReg_status(rs.getString("reg_status"));
        return actividades;
        
    }
    
    public void setReg_status(String status){
        this.Status = status;
    }
    public void setCodigo(String codigo){
        this.Codigo = codigo;
    }
    public void setDescripcion(String descripcion){
        this.Descripcion = descripcion;
    }
    public void setSigla(String sigla){
        this.Sigla = sigla;
    }
    public void setCreation_user(String creation_user){
        this.Creation_user = creation_user;
    }
    public void setDstrct_code(String dstrct_code){
        this.Dstrct_code = dstrct_code;
    }
    public void setLast_update(java.util.Date last_update){
        this.Last_update = last_update;
    }
    public void setUser_update(String user_update){
        this.User_update = user_update;
    }
    public void setCreation_date(java.util.Date creation_date){
        this.Creation_date = creation_date;
    }
    
    public String getCodigo(){
        return this.Codigo;
    }
    public String getDescripcion(){
        return this.Descripcion;
    }
    public String getSigla(){
        return this.Sigla;
    }
    public String getCreation_user(){
        return this.Creation_user;
    }
    public String getDstrct_code(){
        return this.Dstrct_code;
    }
    public java.util.Date getLast_update(){
        return this.Last_update;
    }
    public String getUser_update(){
        return this.User_update;
    }
    public java.util.Date getCreation_date(){
        return this.Creation_date;
    }
    public String getReg_status(){
        return this.Status;
    }
}