/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class Cmc {

    private String tipodoc;
    private String cmc;
    private String cuenta;
    private String dbcr;
    private String sigla_comprobante ;


public static Cmc load(java.sql.ResultSet rs)throws java.sql.SQLException{

    Cmc cmc = new Cmc();

    cmc.setTipodoc(rs.getString("tipodoc"));
    cmc.setCmc(rs.getString("cmc"));
    cmc.setCuenta(rs.getString("cuenta"));
    cmc.setDbcr(rs.getString("dbcr"));
    cmc.setSigla_comprobante(rs.getString("sigla_comprobante"));

    return cmc;
}




    /**
     * @return the tipodoc
     */
    public String getTipodoc() {
        return tipodoc;
    }

    /**
     * @param tipodoc the tipodoc to set
     */
    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    /**
     * @return the cmc
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc the cmc to set
     */
    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the dbcr
     */
    public String getDbcr() {
        return dbcr;
    }

    /**
     * @param dbcr the dbcr to set
     */
    public void setDbcr(String dbcr) {
        this.dbcr = dbcr;
    }

    /**
     * @return the sigla_comprobante
     */
    public String getSigla_comprobante() {
        return sigla_comprobante;
    }

    /**
     * @param sigla_comprobante the sigla_comprobante to set
     */
    public void setSigla_comprobante(String sigla_comprobante) {
        this.sigla_comprobante = sigla_comprobante;
    }


}
