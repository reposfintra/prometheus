/*
 * ViajesCliente.java
 *
 * Created on 31 de julio de 2005, 12:56
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  mario
 */

import java.util.*;
import java.lang.Math;

public class ViajesCliente extends ViajesProceso{
    
    
    private String cliente;
    private String cliente_nombre;
    
    private List viajesStandar;
    
    /*14-12-05*/
    private double coperativos_cliente;
    
    /** Creates a new instance of ViajesCliente */
    public ViajesCliente() {
        
        cliente        = "";
        cliente_nombre = "";
       
        viajesStandar = new LinkedList();
        
    }
    
    
    public void setCliente(String newValue){
        cliente = newValue;
    }
    public void setClienteNombre(String newValue){
        cliente_nombre = newValue;
    }
    public void addStandar(ViajesStandar std){
        viajesStandar.add(std);
    }
   
    
    ////////////////////////////////////////////////////////////
    // getter
    
    public String getCliente(){
        return cliente;
    }
    public String getClienteNombre(){
        return cliente_nombre;
    }
    public List getListaStandar(){
        return viajesStandar;
    }
    
    ///////////////////////////////////////////////////////////////
    // proceso
    
    public void Calcular(){
        
        if (viajesStandar!=null && viajesStandar.size()>0){
            for (int i = 0; i < viajesStandar.size();i++){
                ViajesStandar vs = (ViajesStandar) viajesStandar.get(i);
                for (int dia = 1 ; dia <= 31; dia++){
                    this.setViajePtdo(dia, this.getViajePtdo(dia) + vs.getViajePtdo(dia));
                    this.setViajeEjdo(dia, this.getViajeEjdo(dia) + vs.getViajeEjdo(dia));                    
                    this.setCostosPtdo(dia, this.getCostosPtdo(dia) + vs.getCostosPtdo(dia));
                }
            }
            CalcularDiferencia();
        }
        
    }
    
    /**
     * Getter for property coperativos_cliente.
     * @return Value of property coperativos_cliente.
     */
    public double getCoperativos_cliente() {
        return coperativos_cliente;
    }
    
    /**
     * Setter for property coperativos_cliente.
     * @param coperativos_cliente New value of property coperativos_cliente.
     */
    public void setCoperativos_cliente(double coperativos_cliente) {
        this.coperativos_cliente = coperativos_cliente;
    }
    
}
