/* * CxpBoni.java * * Created on agosto de 2009, 16:46 */
package com.tsp.operation.model.beans;
/** * * @author  Fintra */
public class CxpPf{

    private String num_os,cxc_pf,id_orden,factura_pf,vlr_neto,abono,saldo,fec_open;    
    private String sum_tot_prev1,id_accion;    
    private String banco,sucursal,id_contratista,contratista,cantidad_acciones,doc_relacionado,prefactura;
    
    public CxpPf() {    }    
    
    public String getPrefactura() {
        return prefactura;
    }
    public void setPrefactura(String x) {
        this.prefactura= x;
    }
    public String getCantidadAcciones() {
        return cantidad_acciones;
    }
    public void setCantidadAcciones(String x) {
        this.cantidad_acciones= x;
    }
    public String getIdContratista() {
        return id_contratista;
    }
    public void setIdContratista(String x) {
        this.id_contratista= x;
    }
    public String getContratista() {
        return contratista;
    }
    public void setContratista(String x) {
        this.contratista= x;
    }
    public String getIdAccion() {
        return id_accion;
    }
    public void setIdAccion(String x) {
        this.id_accion= x;
    }
    public String getSumTotPrev1() {
        return sum_tot_prev1;
    }
    public void setSumTotPrev1(String x) {
        this.sum_tot_prev1= x;
    }    
    public String getNumOs() {
        return num_os;
    }
    public void setNumOs(String x) {
        this.num_os= x;
    }
    public String getCxcPf() {
        return cxc_pf;
    }
    public void setCxcPf(String x) {
        this.cxc_pf= x;
    }
    public String getIdOrden() {
        return id_orden;
    }
    public void setIdOrden(String x) {
        this.id_orden= x;
    }
    public String getFacturaPf() {
        return factura_pf;
    }
    public void setFacturaPf(String x) {
        this.factura_pf= x;
    }
    public String getVlrNeto() {
        return vlr_neto;
    }
    public void setVlrNeto(String x) {
        this.vlr_neto= x;
    }
    public String getAbono() {
        return abono;
    }
    public void setAbono(String x) {
        this.abono= x;
    }
    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String x) {
        this.saldo= x;
    }
    public String getFecOpen() {
        return fec_open;
    }
    public void setFecOpen(String x) {
        this.fec_open= x;
    }    
    
    public String getBanco() {
        return banco;
    }
    public void setBanco(String x) {
        this.banco= x;
    }
    public String getSucursal() {
        return sucursal;
    }
    public void setSucursal(String x) {
        this.sucursal= x;
    }
    public String getDocRelacionado() {
        return doc_relacionado;
    }
    public void setDocRelacionado(String x) {
        this.doc_relacionado= x;
    }
    
    public static CxpPf load(java.sql.ResultSet rs)throws java.sql.SQLException{
        CxpPf cxpPf = new CxpPf();
        cxpPf.setNumOs( rs.getString("num_os") );           
        cxpPf.setCxcPf( rs.getString("cxc_pf") );             
        cxpPf.setIdOrden( rs.getString("id_orden") ); 
        cxpPf.setFacturaPf( rs.getString("factura_formula_provintegral") ); 
        cxpPf.setVlrNeto( rs.getString("vlr_neto") ); 
        cxpPf.setAbono( rs.getString("vlr_total_abonos") ); 
        cxpPf.setSaldo( rs.getString("vlr_saldo") ); 
        cxpPf.setFecOpen( rs.getString("fec_open") );         
        cxpPf.setSumTotPrev1( rs.getString("sum_total_prev1") );         
        cxpPf.setBanco(rs.getString("banco") ); 
        cxpPf.setSucursal(rs.getString("sucursal") ); 
        cxpPf.setIdAccion(rs.getString("id_accion") ); 
        cxpPf.setIdContratista(rs.getString("id_contratista") ); 
        cxpPf.setContratista(rs.getString("descripcion") ); 
        cxpPf.setCantidadAcciones(rs.getString("cantidad_acciones") ); 
        cxpPf.setDocRelacionado(rs.getString("documento_relacionado") ); 
        cxpPf.setPrefactura(rs.getString("prefactura") ); 
        return cxpPf;
    }  
}

