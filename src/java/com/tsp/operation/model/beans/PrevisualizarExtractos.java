/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author lcanchila
 */
public class PrevisualizarExtractos {
    private String und_negocio;
    private int idConvenio;
    private String negasoc;
    private String nit;
    private String nom_cli;
    private String direccion;
    private String barrio;
    private String ciudad;
    private String departamento;
    private String cuotas_pendientes;
    private String fecha_ultimo_pago;
    private String fecha_vencimiento;
    private String observaciones;
    private String msg_paguese_antes;
    private String msg_estado;
    private double total_cuotas_vencidas;
    private double min_dias_ven;
    private double subtotal_det;
    private double total_sanciones;
    private double total_dscto_det;
    private double total_det;
    private double total_abonos;
    private double capital;
    private double int_cte;
    private double cat;
    private double int_mora;
    private double gxc;
    private double dscto_int_cte;
    private double dscto_int_mora;
    private double dscto_gxc;
    private double subtotal;
    private double total_descuento;
    private double total;
    private double seguros;
    private double subtotal_corriente;
    private double subtotal_vencido;
    private double dscto_capital;
    private double dscto_cat;
    private String vencimiento_rop;
    private double dscto_seguro;
    private int id;
    private String cod_rop;
    private String cod_rop_barcode;
    private String generado_el;
    private String agencia;
    private String venc_mayor;
    private String linea_producto;
    private String est_comercio;
    private int idUndNegocio;
    private String telefono;
    private int id_sancion;
    private int id_concepto_recaudo;
    private int tipo_acto;
    private String concepto_recaudo;
    private String categoria;
    private String descripcion_sancion;
    private int rango_ini;
    private int rango_fin;
    private String porcentaje;
    private int id_aplicado;
    private String aplicado_a;
    private String email;
    private String extracto_email;
    private String periodo_generacion_fact;
    private String periodo_facturacion;
    private int ciclo_facturacion;
    private String periodo_desembolso;

    public String getPeriodo_generacion_fact() {
        return periodo_generacion_fact;
    }

    public void setPeriodo_generacion_fact(String periodo_generacion_fact) {
        this.periodo_generacion_fact = periodo_generacion_fact;
    }

    public String getPeriodo_facturacion() {
        return periodo_facturacion;
    }

    public void setPeriodo_facturacion(String periodo_facturacion) {
        this.periodo_facturacion = periodo_facturacion;
    }

    public int getCiclo_facturacion() {
        return ciclo_facturacion;
    }

    public void setCiclo_facturacion(int ciclo_facturacion) {
        this.ciclo_facturacion = ciclo_facturacion;
    }

    public String getPeriodo_desembolso() {
        return periodo_desembolso;
    }

    public void setPeriodo_desembolso(String periodo_desembolso) {
        this.periodo_desembolso = periodo_desembolso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExtracto_email() {
        return extracto_email;
    }

    public void setExtracto_email(String extracto_email) {
        this.extracto_email = extracto_email;
    }
    

    /**
     * @return the idConvenio
     */
    public int getIdConvenio() {
        return idConvenio;
    }

    /**
     * @param idConvenio the idConvenio to set
     */
    public void setIdConvenio(int idConvenio) {
        this.idConvenio = idConvenio;
    }

    /**
     * @return the negasoc
     */
    public String getNegasoc() {
        return negasoc;
    }

    /**
     * @param negasoc the negasoc to set
     */
    public void setNegasoc(String negasoc) {
        this.negasoc = negasoc;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the nom_cli
     */
    public String getNom_cli() {
        return nom_cli;
    }

    /**
     * @param nom_cli the nom_cli to set
     */
    public void setNom_cli(String nom_cli) {
        this.nom_cli = nom_cli;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * @param barrio the barrio to set
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the cuotas_pendientes
     */
    public String getCuotas_pendientes() {
        return cuotas_pendientes;
    }

    /**
     * @param cuotas_pendientes the cuotas_pendientes to set
     */
    public void setCuotas_pendientes(String cuotas_pendientes) {
        this.cuotas_pendientes = cuotas_pendientes;
    }

    /**
     * @return the fecha_ultimo_pago
     */
    public String getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    /**
     * @param fecha_ultimo_pago the fecha_ultimo_pago to set
     */
    public void setFecha_ultimo_pago(String fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    /**
     * @return the fecha_vencimiento
     */
    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * @param fecha_vencimiento the fecha_vencimiento to set
     */
    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    /**
     * @return the msg_paguese_antes
     */
    public String getMsg_paguese_antes() {
        return msg_paguese_antes;
    }

    /**
     * @param msg_paguese_antes the msg_paguese_antes to set
     */
    public void setMsg_paguese_antes(String msg_paguese_antes) {
        this.msg_paguese_antes = msg_paguese_antes;
    }

    /**
     * @return the msg_estado
     */
    public String getMsg_estado() {
        return msg_estado;
    }

    /**
     * @param msg_estado the msg_estado to set
     */
    public void setMsg_estado(String msg_estado) {
        this.msg_estado = msg_estado;
    }

    /**
     * @return the total_cuotas_vencidas
     */
    public double getTotal_cuotas_vencidas() {
        return total_cuotas_vencidas;
    }

    /**
     * @param total_cuotas_vencidas the total_cuotas_vencidas to set
     */
    public void setTotal_cuotas_vencidas(double total_cuotas_vencidas) {
        this.total_cuotas_vencidas = total_cuotas_vencidas;
    }

    /**
     * @return the min_dias_ven
     */
    public double getMin_dias_ven() {
        return min_dias_ven;
    }

    /**
     * @param min_dias_ven the min_dias_ven to set
     */
    public void setMin_dias_ven(double min_dias_ven) {
        this.min_dias_ven = min_dias_ven;
    }

    /**
     * @return the subtotal_det
     */
    public double getSubtotal_det() {
        return subtotal_det;
    }

    /**
     * @param subtotal_det the subtotal_det to set
     */
    public void setSubtotal_det(double subtotal_det) {
        this.subtotal_det = subtotal_det;
    }

    /**
     * @return the total_sanciones
     */
    public double getTotal_sanciones() {
        return total_sanciones;
    }

    /**
     * @param total_sanciones the total_sanciones to set
     */
    public void setTotal_sanciones(double total_sanciones) {
        this.total_sanciones = total_sanciones;
    }

    /**
     * @return the total_dscto_det
     */
    public double getTotal_dscto_det() {
        return total_dscto_det;
    }

    /**
     * @param total_dscto_det the total_dscto_det to set
     */
    public void setTotal_dscto_det(double total_dscto_det) {
        this.total_dscto_det = total_dscto_det;
    }

    /**
     * @return the total_det
     */
    public double getTotal_det() {
        return total_det;
    }

    /**
     * @param total_det the total_det to set
     */
    public void setTotal_det(double total_det) {
        this.total_det = total_det;
    }

    /**
     * @return the total_abonos
     */
    public double getTotal_abonos() {
        return total_abonos;
    }

    /**
     * @param total_abonos the total_abonos to set
     */
    public void setTotal_abonos(double total_abonos) {
        this.total_abonos = total_abonos;
    }

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the int_cte
     */
    public double getInt_cte() {
        return int_cte;
    }

    /**
     * @param int_cte the int_cte to set
     */
    public void setInt_cte(double int_cte) {
        this.int_cte = int_cte;
    }

    /**
     * @return the cat
     */
    public double getCat() {
        return cat;
    }

    /**
     * @param cat the cat to set
     */
    public void setCat(double cat) {
        this.cat = cat;
    }

    /**
     * @return the int_mora
     */
    public double getInt_mora() {
        return int_mora;
    }

    /**
     * @param int_mora the int_mora to set
     */
    public void setInt_mora(double int_mora) {
        this.int_mora = int_mora;
    }

    /**
     * @return the gxc
     */
    public double getGxc() {
        return gxc;
    }

    /**
     * @param gxc the gxc to set
     */
    public void setGxc(double gxc) {
        this.gxc = gxc;
    }

    /**
     * @return the dscto_int_cte
     */
    public double getDscto_int_cte() {
        return dscto_int_cte;
    }

    /**
     * @param dscto_int_cte the dscto_int_cte to set
     */
    public void setDscto_int_cte(double dscto_int_cte) {
        this.dscto_int_cte = dscto_int_cte;
    }

    /**
     * @return the dscto_int_mora
     */
    public double getDscto_int_mora() {
        return dscto_int_mora;
    }

    /**
     * @param dscto_int_mora the dscto_int_mora to set
     */
    public void setDscto_int_mora(double dscto_int_mora) {
        this.dscto_int_mora = dscto_int_mora;
    }

    /**
     * @return the dscto_gxc
     */
    public double getDscto_gxc() {
        return dscto_gxc;
    }

    /**
     * @param dscto_gxc the dscto_gxc to set
     */
    public void setDscto_gxc(double dscto_gxc) {
        this.dscto_gxc = dscto_gxc;
    }

    /**
     * @return the subtotal
     */
    public double getSubtotal() {
        return subtotal;
    }

    /**
     * @param subtotal the subtotal to set
     */
    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * @return the total_descuento
     */
    public double getTotal_descuento() {
        return total_descuento;
    }

    /**
     * @param total_descuento the total_descuento to set
     */
    public void setTotal_descuento(double total_descuento) {
        this.total_descuento = total_descuento;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return the seguros
     */
    public double getSeguros() {
        return seguros;
    }

    /**
     * @param seguros the seguros to set
     */
    public void setSeguros(double seguros) {
        this.seguros = seguros;
    }

    /**
     * @return the subtotal_corriente
     */
    public double getSubtotal_corriente() {
        return subtotal_corriente;
    }

    /**
     * @param subtotal_corriente the subtotal_corriente to set
     */
    public void setSubtotal_corriente(double subtotal_corriente) {
        this.subtotal_corriente = subtotal_corriente;
    }

    /**
     * @return the subtotal_vencido
     */
    public double getSubtotal_vencido() {
        return subtotal_vencido;
    }

    /**
     * @param subtotal_vencido the subtotal_vencido to set
     */
    public void setSubtotal_vencido(double subtotal_vencido) {
        this.subtotal_vencido = subtotal_vencido;
    }

    /**
     * @return the und_negocio
     */
    public String getUnd_negocio() {
        return und_negocio;
    }

    /**
     * @param und_negocio the und_negocio to set
     */
    public void setUnd_negocio(String und_negocio) {
        this.und_negocio = und_negocio;
    }

    /**
     * @return the dscto_cap
     */
    public double getDscto_capital() {
        return dscto_capital;
    }

    /**
     * @param dscto_cap the dscto_cap to set
     */
    public void setDscto_capital(double dscto_capital) {
        this.dscto_capital = dscto_capital;
    }

    /**
     * @return the dscto_cat
     */
    public double getDscto_cat() {
        return dscto_cat;
    }

    /**
     * @param dscto_cat the dscto_cat to set
     */
    public void setDscto_cat(double dscto_cat) {
        this.dscto_cat = dscto_cat;
    }

    /**
     * @return the vencimiento_rop
     */
    public String getVencimiento_rop() {
        return vencimiento_rop;
    }

    /**
     * @param vencimiento_rop the vencimiento_rop to set
     */
    public void setVencimiento_rop(String vencimiento_rop) {
        this.vencimiento_rop = vencimiento_rop;
    }

    /**
     * @return the dscto_seguro
     */
    public double getDscto_seguro() {
        return dscto_seguro;
    }

    /**
     * @param dscto_seguro the dscto_seguro to set
     */
    public void setDscto_seguro(double dscto_seguro) {
        this.dscto_seguro = dscto_seguro;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the cod_rop
     */
    public String getCod_rop() {
        return cod_rop;
    }

    /**
     * @param cod_rop the cod_rop to set
     */
    public void setCod_rop(String cod_rop) {
        this.cod_rop = cod_rop;
    }

    /**
     * @return the cod_rop_barcode
     */
    public String getCod_rop_barcode() {
        return cod_rop_barcode;
    }

    /**
     * @param cod_rop_barcode the cod_rop_barcode to set
     */
    public void setCod_rop_barcode(String cod_rop_barcode) {
        this.cod_rop_barcode = cod_rop_barcode;
    }

    /**
     * @return the generado_el
     */
    public String getGenerado_el() {
        return generado_el;
    }

    /**
     * @param generado_el the generado_el to set
     */
    public void setGenerado_el(String generado_el) {
        this.generado_el = generado_el;
    }

    /**
     * @return the agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * @param agencia the agencia to set
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * @return the venc_mayor
     */
    public String getVenc_mayor() {
        return venc_mayor;
    }

    /**
     * @param venc_mayor the venc_mayor to set
     */
    public void setVenc_mayor(String venc_mayor) {
        this.venc_mayor = venc_mayor;
    }

    /**
     * @return the linea_producto
     */
    public String getLinea_producto() {
        return linea_producto;
    }

    /**
     * @param linea_producto the linea_producto to set
     */
    public void setLinea_producto(String linea_producto) {
        this.linea_producto = linea_producto;
    }

    /**
     * @return the est_comercio
     */
    public String getEst_comercio() {
        return est_comercio;
    }

    /**
     * @param est_comercio the est_comercio to set
     */
    public void setEst_comercio(String est_comercio) {
        this.est_comercio = est_comercio;
    }

    /**
     * @return the idUndNegocio
     */
    public int getIdUndNegocio() {
        return idUndNegocio;
    }

    /**
     * @param idUndNegocio the idUndNegocio to set
     */
    public void setIdUndNegocio(int idUndNegocio) {
        this.idUndNegocio = idUndNegocio;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the id_sancion
     */
    public int getId_sancion() {
        return id_sancion;
    }

    /**
     * @param id_sancion the id_sancion to set
     */
    public void setId_sancion(int id_sancion) {
        this.id_sancion = id_sancion;
    }

    /**
     * @return the id_concepto_recaudo
     */
    public int getId_concepto_recaudo() {
        return id_concepto_recaudo;
    }

    /**
     * @param id_concepto_recaudo the id_concepto_recaudo to set
     */
    public void setId_concepto_recaudo(int id_concepto_recaudo) {
        this.id_concepto_recaudo = id_concepto_recaudo;
    }

    /**
     * @return the tipo_acto
     */
    public int getTipo_acto() {
        return tipo_acto;
    }

    /**
     * @param tipo_acto the tipo_acto to set
     */
    public void setTipo_acto(int tipo_acto) {
        this.tipo_acto = tipo_acto;
    }

    /**
     * @return the concepto_recaudo
     */
    public String getConcepto_recaudo() {
        return concepto_recaudo;
    }

    /**
     * @param concepto_recaudo the concepto_recaudo to set
     */
    public void setConcepto_recaudo(String concepto_recaudo) {
        this.concepto_recaudo = concepto_recaudo;
    }

    /**
     * @return the categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the descripcion_sancion
     */
    public String getDescripcion_sancion() {
        return descripcion_sancion;
    }

    /**
     * @param descripcion_sancion the descripcion_sancion to set
     */
    public void setDescripcion_sancion(String descripcion_sancion) {
        this.descripcion_sancion = descripcion_sancion;
    }

    /**
     * @return the rango_ini
     */
    public int getRango_ini() {
        return rango_ini;
    }

    /**
     * @param rango_ini the rango_ini to set
     */
    public void setRango_ini(int rango_ini) {
        this.rango_ini = rango_ini;
    }

    /**
     * @return the rango_fin
     */
    public int getRango_fin() {
        return rango_fin;
    }

    /**
     * @param rango_fin the rango_fin to set
     */
    public void setRango_fin(int rango_fin) {
        this.rango_fin = rango_fin;
    }

    /**
     * @return the porcentaje
     */
    public String getPorcentaje() {
        return porcentaje;
    }

    /**
     * @param porcentaje the porcentaje to set
     */
    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    /**
     * @return the id_aplicado
     */
    public int getId_aplicado() {
        return id_aplicado;
    }

    /**
     * @param id_aplicado the id_aplicado to set
     */
    public void setId_aplicado(int id_aplicado) {
        this.id_aplicado = id_aplicado;
    }

    /**
     * @return the aplicado_a
     */
    public String getAplicado_a() {
        return aplicado_a;
    }

    /**
     * @param aplicado_a the aplicado_a to set
     */
    public void setAplicado_a(String aplicado_a) {
        this.aplicado_a = aplicado_a;
    }
    
    
}
