/***********************************************************************************
 * Nombre clase : ............... ControlactAduanera.java                          *
 * Descripcion :.................  bean de reporte control actividad aduanera      *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 30 de noviembre de 2005, 08:35 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/


package com.tsp.operation.model.beans;

/**
 *
 * @author  dbastidas
 */
public class ControlactAduanera {
    String cliente;
    String FacComercial;
    String remesa;
    String fecrem;
    String desstdjob;
    String orirem;
    String desrem;
    String numpla;
    
    
    
    /** Creates a new instance of ControlactAduanera */
    public ControlactAduanera() {
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property FacComercial.
     * @return Value of property FacComercial.
     */
    public java.lang.String getFacComercial() {
        return FacComercial;
    }
    
    /**
     * Setter for property FacComercial.
     * @param FacComercial New value of property FacComercial.
     */
    public void setFacComercial(java.lang.String FacComercial) {
        this.FacComercial = FacComercial;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property fecrem.
     * @return Value of property fecrem.
     */
    public java.lang.String getFecrem() {
        return fecrem;
    }
    
    /**
     * Setter for property fecrem.
     * @param fecrem New value of property fecrem.
     */
    public void setFecrem(java.lang.String fecrem) {
        this.fecrem = fecrem;
    }
    
    /**
     * Getter for property desstdjob.
     * @return Value of property desstdjob.
     */
    public java.lang.String getDesstdjob() {
        return desstdjob;
    }
    
    /**
     * Setter for property desstdjob.
     * @param desstdjob New value of property desstdjob.
     */
    public void setDesstdjob(java.lang.String desstdjob) {
        this.desstdjob = desstdjob;
    }
    
    /**
     * Getter for property orirem.
     * @return Value of property orirem.
     */
    public java.lang.String getOrirem() {
        return orirem;
    }
    
    /**
     * Setter for property orirem.
     * @param orirem New value of property orirem.
     */
    public void setOrirem(java.lang.String orirem) {
        this.orirem = orirem;
    }
    
    /**
     * Getter for property desrem.
     * @return Value of property desrem.
     */
    public java.lang.String getDesrem() {
        return desrem;
    }
    
    /**
     * Setter for property desrem.
     * @param desrem New value of property desrem.
     */
    public void setDesrem(java.lang.String desrem) {
        this.desrem = desrem;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    
    
}
