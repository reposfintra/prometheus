/********************************************************************
 *      Nombre Clase.................   EstadViajes.java
 *      Descripci�n..................   Bean de la tabla estadistica_viajes
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   15 de febrero de 2006, 03:04 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz
 */
public class EstadViajes {
    
    private int ano;
    private String tipo;
    private String codtipocarga;
    private String nit;
    private String mes;
    private int cant_mes;
    private int[] cant_meses;
    
    private String creation_user;
    private String base;
    private String reg_status;
    private String dstrct;
    
    /** Crea una nueva instancia de  EstadViajes */
    public EstadViajes() {
        this.cant_meses = new int[12];
    }
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void Load(ResultSet rs) throws SQLException{
        this.ano = rs.getInt("ano");
        this.tipo = rs.getString("tipo");
        this.codtipocarga = rs.getString("codtipocarga");
        this.nit = rs.getString("nit");
        this.reg_status = rs.getString("reg_status");
        this.dstrct = rs.getString("dstrct");
        String campo_mes = "n_viajes_" + this.mes;
        this.cant_mes = rs.getInt(campo_mes);
    }
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void LoadAll(ResultSet rs) throws SQLException{
        this.ano = rs.getInt("ano");
        this.tipo = rs.getString("tipo");
        this.codtipocarga = rs.getString("codtipocarga");
        this.nit = rs.getString("nit");
        this.reg_status = rs.getString("reg_status");
        this.dstrct = rs.getString("dstrct");
                
        this.cant_meses[0] = rs.getInt("n_viajes_01");
        this.cant_meses[1] = rs.getInt("n_viajes_02");
        this.cant_meses[2] = rs.getInt("n_viajes_03");
        this.cant_meses[3] = rs.getInt("n_viajes_04");
        this.cant_meses[4] = rs.getInt("n_viajes_05");
        this.cant_meses[5] = rs.getInt("n_viajes_06");
        this.cant_meses[6] = rs.getInt("n_viajes_07");
        this.cant_meses[7] = rs.getInt("n_viajes_08");
        this.cant_meses[8] = rs.getInt("n_viajes_09");
        this.cant_meses[9] = rs.getInt("n_viajes_10");
        this.cant_meses[10] = rs.getInt("n_viajes_11");
        this.cant_meses[11] = rs.getInt("n_viajes_12");
    }
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void LoadGroupByNitAno(ResultSet rs) throws SQLException{
        this.ano = rs.getInt("ano");
        this.nit = rs.getString("nit");
                
        this.cant_meses[0] = rs.getInt("n_viajes_01");
        this.cant_meses[1] = rs.getInt("n_viajes_02");
        this.cant_meses[2] = rs.getInt("n_viajes_03");
        this.cant_meses[3] = rs.getInt("n_viajes_04");
        this.cant_meses[4] = rs.getInt("n_viajes_05");
        this.cant_meses[5] = rs.getInt("n_viajes_06");
        this.cant_meses[6] = rs.getInt("n_viajes_07");
        this.cant_meses[7] = rs.getInt("n_viajes_08");
        this.cant_meses[8] = rs.getInt("n_viajes_09");
        this.cant_meses[9] = rs.getInt("n_viajes_10");
        this.cant_meses[10] = rs.getInt("n_viajes_11");
        this.cant_meses[11] = rs.getInt("n_viajes_12");
    }
    
    /**
     * Getter for property ano.
     * @return Value of property ano.
     */
    public int getAno() {
        return ano;
    }
    
    /**
     * Setter for property ano.
     * @param ano New value of property ano.
     */
    public void setAno(int ano) {
        this.ano = ano;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property cant_mes.
     * @return Value of property cant_mes.
     */
    public int getCant_mes() {
        return cant_mes;
    }
    
    /**
     * Setter for property cant_mes.
     * @param cant_mes New value of property cant_mes.
     */
    public void setCant_mes(int cant_mes) {
        this.cant_mes = cant_mes;
    }
    
    /**
     * Getter for property codtipocarga.
     * @return Value of property codtipocarga.
     */
    public java.lang.String getCodtipocarga() {
        return codtipocarga;
    }
    
    /**
     * Setter for property codtipocarga.
     * @param codtipocarga New value of property codtipocarga.
     */
    public void setCodtipocarga(java.lang.String codtipocarga) {
        this.codtipocarga = codtipocarga;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property cant_meses.
     * @return Value of property cant_meses.
     */
    public int[] getCant_meses() {
        return this.cant_meses;
    }
    
    /**
     * Setter for property cant_meses.
     * @param cant_meses New value of property cant_meses.
     */
    public void setCant_meses(int[] cant_meses) {
        this.cant_meses = cant_meses;
    }
    
}
