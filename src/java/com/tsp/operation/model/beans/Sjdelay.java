/*
 * Sjdelay.java
 *
 * Created on 3 de diciembre de 2004, 03:39 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  KREALES
 */
public class Sjdelay {
    
    private String dstrct;
    private String sj;
    private String cf;
    private String creation_user;
    private String descripcion;
    
    /** Creates a new instance of Sjdelay */
    public static Sjdelay load(ResultSet rs)throws SQLException {
        
        Sjdelay sd = new Sjdelay();
        sd.setDstrct(rs.getString("dstrct"));
        sd.setCf(rs.getString("delay_code"));
        sd.setSj(rs.getString("sj"));
        sd.setUser(rs.getString("creation_user"));
        sd.setDescripcion(rs.getString("description"));
        return sd;
    }
    public void setDstrct(String dstrct){
        this.dstrct=dstrct;
    }
    public String getDstrct(){
        return this.dstrct;
            
    }
    public void setDescripcion(String descripcion){
        this.descripcion=descripcion;
    }
    public String getDescripcion(){
        return this.descripcion;
            
    }
    public void setSj(String sj){
        this.sj=sj;
    
    }
    public String getSj(){
        return sj;
    }
    public void setCf(String cf){
        this.cf=cf;
    
    }
    public String getCf(){
        return cf;
    
    }
    public void setUser(String user){
        this.creation_user=user;
    
    }
    public String getUser(){
        return creation_user;
    
    }
    
    


}
