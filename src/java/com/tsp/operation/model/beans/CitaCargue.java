/*
 * Agencia.java
 *
 * Created on 12 de noviembre de 2004, 06:53 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  AMENDEZ
 */
public class CitaCargue {
    
    String reg_status;
    String numpla;
    String causa;
    String observacion;
    String placa;
    String codigo;
    String descripcion;
    String tipo;
    String distrito;
    String usuario;
    String fecha_pla;
    String fecha_real;
    double cumplido_cargue;
    double cumplido_dcargue;
    String propietario;
    int inCargue;
    int inDCargue;
    double porcCargueA;
    double porcCargueM;
    int cantCargueA;
    int cantCargueM;
    
    /** Creates a new instance of Agencia */
    public CitaCargue() {
    }
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa() {
        return causa;
    }
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.lang.String causa) {
        this.causa = causa;
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    
    /**
     * Getter for property fecha_pla.
     * @return Value of property fecha_pla.
     */
    public java.lang.String getFecha_pla() {
        return fecha_pla;
    }    
    
    /**
     * Setter for property fecha_pla.
     * @param fecha_pla New value of property fecha_pla.
     */
    public void setFecha_pla(java.lang.String fecha_pla) {
        this.fecha_pla = fecha_pla;
    }
    
    /**
     * Getter for property fecha_real.
     * @return Value of property fecha_real.
     */
    public java.lang.String getFecha_real() {
        return fecha_real;
    }
    
    /**
     * Setter for property fecha_real.
     * @param fecha_real New value of property fecha_real.
     */
    public void setFecha_real(java.lang.String fecha_real) {
        this.fecha_real = fecha_real;
    }
    
    /**
     * Getter for property cumplido_dcargue.
     * @return Value of property cumplido_dcargue.
     */
    public double getCumplido_dcargue() {
        return cumplido_dcargue;
    }
    
    /**
     * Setter for property cumplido_dcargue.
     * @param cumplido_dcargue New value of property cumplido_dcargue.
     */
    public void setCumplido_dcargue(double cumplido_dcargue) {
        this.cumplido_dcargue = cumplido_dcargue;
    }
    
    /**
     * Getter for property cumplido_cargue.
     * @return Value of property cumplido_cargue.
     */
    public double getCumplido_cargue() {
        return cumplido_cargue;
    }
    
    /**
     * Setter for property cumplido_cargue.
     * @param cumplido_cargue New value of property cumplido_cargue.
     */
    public void setCumplido_cargue(double cumplido_cargue) {
        this.cumplido_cargue = cumplido_cargue;
    }
    
    /**
     * Getter for property inCargue.
     * @return Value of property inCargue.
     */
    public int getInCargue() {
        return inCargue;
    }
    
    /**
     * Setter for property inCargue.
     * @param inCargue New value of property inCargue.
     */
    public void setInCargue(int inCargue) {
        this.inCargue = inCargue;
    }
    
    /**
     * Getter for property inDCargue.
     * @return Value of property inDCargue.
     */
    public int getInDCargue() {
        return inDCargue;
    }
    
    /**
     * Setter for property inDCargue.
     * @param inDCargue New value of property inDCargue.
     */
    public void setInDCargue(int inDCargue) {
        this.inDCargue = inDCargue;
    }
    
    /**
     * Getter for property propietario.
     * @return Value of property propietario.
     */
    public java.lang.String getPropietario() {
        return propietario;
    }
    
    /**
     * Setter for property propietario.
     * @param propietario New value of property propietario.
     */
    public void setPropietario(java.lang.String propietario) {
        this.propietario = propietario;
    }
    
    /**
     * Getter for property cantCargueA.
     * @return Value of property cantCargueA.
     */
    public int getCantCargueA() {
        return cantCargueA;
    }
    
    /**
     * Setter for property cantCargueA.
     * @param cantCargueA New value of property cantCargueA.
     */
    public void setCantCargueA(int cantCargueA) {
        this.cantCargueA = cantCargueA;
    }
    
    /**
     * Getter for property cantCargueM.
     * @return Value of property cantCargueM.
     */
    public int getCantCargueM() {
        return cantCargueM;
    }
    
    /**
     * Setter for property cantCargueM.
     * @param cantCargueM New value of property cantCargueM.
     */
    public void setCantCargueM(int cantCargueM) {
        this.cantCargueM = cantCargueM;
    }
    
    /**
     * Getter for property porcCargueA.
     * @return Value of property porcCargueA.
     */
    public double getPorcCargueA() {
        return porcCargueA;
    }
    
    /**
     * Setter for property porcCargueA.
     * @param porcCargueA New value of property porcCargueA.
     */
    public void setPorcCargueA(double porcCargueA) {
        this.porcCargueA = porcCargueA;
    }
    
    /**
     * Getter for property porcCargueM.
     * @return Value of property porcCargueM.
     */
    public double getPorcCargueM() {
        return porcCargueM;
    }
    
    /**
     * Setter for property porcCargueM.
     * @param porcCargueM New value of property porcCargueM.
     */
    public void setPorcCargueM(double porcCargueM) {
        this.porcCargueM = porcCargueM;
    }
    
}
