/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;


import java.util.*;
import java.text.*;

import com.aspose.cells.*;



/**
 *
 * @author Alvaro
 */
public class AsposeUtil {



    public AsposeUtil() {
    }



     public Workbook crearLibro( String nombreLibro) throws Exception {

        Workbook libro = null;

        try{
            libro = new Workbook();
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error en crear archivo de excell : " + nombreLibro + " ...\n" + e.getMessage() );
        }
        finally {
            return libro;
        }
    }



     public String crearNombreLibro(String nombreLibro, int tipoFormatoLibro, boolean sufijoFechaHora) throws Exception {

        String nombreArchivo = "";
        String extension = "";

        try{
            if (sufijoFechaHora){

                SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyMMdd_hhmmss");
                nombreLibro = nombreLibro + " " + formatoFecha.format( new Date() );
            }
            if (tipoFormatoLibro == 4){
                extension = ".xls";
            }
            else if (tipoFormatoLibro == 10){
                extension = ".xlsx";
            }
            else {
                extension = ".xls";
            }


            nombreLibro = nombreLibro + extension;

        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error en crear archivo de excell : "  + nombreArchivo + " ...\n" + e.getMessage() );
        }
        finally {
            return nombreLibro;
        }
    }



    public Workbook abrirLibro(String ruta, String nombreLibro, int tipoFormatoLibro ) throws Exception {

        Workbook wb = new Workbook();

        try {
            
            
            String extension = ".XLS";
            String nombreLibroUpper = nombreLibro.toUpperCase();
            boolean extensionLocalizada = nombreLibroUpper.contains(extension);
            if (!extensionLocalizada) {
                nombreLibro = nombreLibro + extension;
            }

            
            wb.open(ruta + "/" + nombreLibro , tipoFormatoLibro);
            //  wb.open(ruta + "/" + nombreLibro + ".xls", tipoFormatoLibro);
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error al abrir archivo de excell : "  + ruta + "/" + nombreLibro  + " ...\n" + e.getMessage() );
        }

        return wb;
    }





    public Worksheet crearHoja(Workbook wb, String nombreHoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");

        //Adding a new worksheet to the Workbook object

        Worksheets worksheets = wb.getWorksheets();

        Worksheet sheet = worksheets.getSheet(nombreHoja);
        if (sheet == null)  {
            sheet = worksheets.addSheet();
            sheet.setName(nombreHoja);
        }

        worksheets = null;
        return sheet;

    }




    public Worksheet obtenerHoja(Workbook wb, String nombreHoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");

        //Adding a new worksheet to the Workbook object

        Worksheets worksheets = wb.getWorksheets();

        Worksheet sheet = worksheets.getSheet(nombreHoja);
        if (sheet == null)  {
            sheet = worksheets.addSheet();
            sheet.setName(nombreHoja);
        }

        worksheets = null;
        return sheet;

    }


    public Cell obtenerCelda(Worksheet sheet, int fila, int columna)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);
        cells = null;

        return cell;
    }



    public String getCeldaString(Worksheet sheet, int fila, int columna)throws Exception{

        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);
        String s = "";

        //switch(cell.getValueType()) {

          //  case CellValueType.STRING:
          //  case CellValueType.RICH_TEXT_STRING:
                s = (String)cell.getValue();
        // }

         cells = null;
         cell = null;

         return s;
    }




    public int getCeldaInt(Worksheet sheet, int fila, int columna)throws Exception{

        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);
        int  i = 0;

        // switch(cell.getValueType()) {

            // case CellValueType.INT:

               i = ((Integer)cell.getValue()).intValue();
        // }

         cells = null;
         cell = null;

         return i;
    }



    public double getCeldaDouble(Worksheet sheet, int fila, int columna)throws Exception{

        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);
        double  d = 0.00;

        switch(cell.getValueType()) {

               case CellValueType.BOOLEAN:
                      boolean b = ((Boolean)cell.getValue()).booleanValue();
                      break;

               case CellValueType.DATETIME:
                      Calendar calendar = (Calendar)cell.getValue();
                      break;

               case CellValueType.DOUBLE:
                      d = ((Double)cell.getValue()).doubleValue();
                      break;

               case CellValueType.INT:
                      int i = ((Integer)cell.getValue()).intValue();
                      d = Double.parseDouble(Integer.toString(i));
                      break;

               case CellValueType.STRING:
               case CellValueType.RICH_TEXT_STRING:
                      String s = (String)cell.getValue();
                      break;

               case CellValueType.NULL:
                      // no value
                      break;

        }

        cells = null;
        cell = null;

        return d;
    }













    public void combinarCeldas(Worksheet sheet, int filaInicial, int columnaInicial, int numeroFilas , int numeroColumnas)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        sheet.getCells().merge(filaInicial, columnaInicial, numeroFilas, numeroColumnas);

    }




    public void setCelda(Worksheet sheet, int fila, int columna, String valor, Style estilo)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);
        cell.setValue(valor);
        cell.setStyle(estilo);
    }


        public void setCelda(Worksheet sheet, int fila, int columna, double valor , Style estilo)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);
        cell.setValue(valor);
        cell.setStyle(estilo);

    }

    public void setCelda(Worksheet sheet, int fila, int columna, int valor, Style estilo)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);
        cell.setValue(valor);
        cell.setStyle(estilo);
    }



        public void setCelda(Worksheet sheet, int fila, int columna, Date valor , Style estilo)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        cell.setValue(valor);
        cell.setStyle(estilo);

    }



        public void setCelda(Worksheet sheet, int fila, int columna, boolean valor , Style estilo)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        cell.setValue(valor);
        cell.setStyle(estilo);

    }


    public void setFormula(Worksheet sheet, int fila, int columna, String formula, Style estilo)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        Cell cell = obtenerCelda( sheet,  fila,  columna);

        cell.setFormula (formula);
        cell.setStyle(estilo);

    }



    public void etiquetarHoja(Workbook wb, Worksheet sheet, String etiquetaHoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");

        sheet.setName(etiquetaHoja);
    }



    public void etiquetarHoja(Workbook wb, int numeroHoja, String etiquetaHoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");

        Worksheet sheet = wb.getWorksheets().getSheet(numeroHoja);
        sheet.setName(etiquetaHoja);
    }


    public void etiquetarHoja(Workbook wb, String etiquetaHoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");

        Worksheet sheet = wb.getWorksheets().getActiveSheet() ;
        sheet.setName(etiquetaHoja);

    }

    public void etiquetarHoja(Worksheet sheet, String etiquetaHoja)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");

        sheet.setName(etiquetaHoja);

    }



    public Style crearEstilo(Workbook libro, String nombreLetra, int tamanoLetra,
                             boolean bold, boolean italic, int underline,
                             String  formato,
                             com.aspose.cells.Color color,
                             boolean existeColorFondo,
                             com.aspose.cells.Color colorFondo,
                             short alineacionHorizontal)     {

        Style estilo = libro.createStyle();

        estilo.getFont().setName(nombreLetra);
        estilo.getFont().setSize(tamanoLetra);
        estilo.getFont().setBold(bold);
        estilo.getFont().setItalic(italic);
        estilo.getFont().setUnderline(underline);

        try {
           int formatoEstandar = Integer.parseInt(formato.trim());
           estilo.setNumber(formatoEstandar);
           } catch (NumberFormatException nfe) {
              String formatoPersonalizado = formato;
              estilo.setCustom(formatoPersonalizado);
           }
        estilo.getFont().setColor(color);

        if(existeColorFondo){
            estilo.setColor(colorFondo);
        }

        estilo.setHAlignment(alineacionHorizontal);

        return estilo;

        }






















    public void crearColor(Workbook wb, Hashtable hashColorPersonalizado, String nombreColor, int indice, int rojo, int verde, int azul ){

        com.aspose.cells.Color colorNuevo = new com.aspose.cells.Color();

        colorNuevo.setRed(rojo);
        colorNuevo.setBlue(azul);
        colorNuevo.setGreen(verde);

        adicionarColor( wb, indice, colorNuevo );

        hashColorPersonalizado.put(nombreColor, indice);
    }


    private void adicionarColor( Workbook wb, int indice,  com.aspose.cells.Color colorNuevo ){
        wb.getPalette().setColor(indice, colorNuevo);
    }



    public com.aspose.cells.Color getColorPersonalizado(Workbook wb, Hashtable hashColorPersonalizado, String nombreColor){

        Integer indice = (Integer) hashColorPersonalizado.get(nombreColor);
        if (indice == null) {
            return com.aspose.cells.Color.BLACK;
        }
        return wb.getPalette().getColor(indice);

    }


    public void setColorLetra(Worksheet sheet, int fila, int columna, com.aspose.cells.Color colorLetra){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();
        estiloCelda.getFont().setColor(colorLetra);
        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }


    public void setColorFondo(Worksheet sheet, int fila, int columna, com.aspose.cells.Color colorFondo){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();
        estiloCelda.setColor(colorFondo);

        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }



    public void setBorde(Worksheet sheet, int fila, int columna, com.aspose.cells.Color colorLinea){


        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style style = cell.getStyle();
        style.setBorderLine(BorderType.TOP,BorderLineType.MEDIUM);
        style.setBorderColor(BorderType.TOP,colorLinea);
        style.setBorderLine(BorderType.BOTTOM,BorderLineType.MEDIUM);
        style.setBorderColor(BorderType.BOTTOM,colorLinea);
        style.setBorderLine(BorderType.LEFT,BorderLineType.MEDIUM);
        style.setBorderColor(BorderType.LEFT,colorLinea);
        style.setBorderLine(BorderType.RIGHT,BorderLineType.MEDIUM);
        style.setBorderColor(BorderType.RIGHT,colorLinea);
        cell.setStyle(style);

    }





    public void setTipoLetra (Worksheet sheet, String nombreLetra, int tamanoLetra,
                             boolean bold, boolean italic,
                             boolean existeColorLetra, com.aspose.cells.Color colorLetra,
                             boolean existeColorFondo, com.aspose.cells.Color colorFondo,
                             int fila, int columna ){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();

        estiloCelda.getFont().setName(nombreLetra);
        estiloCelda.getFont().setSize(tamanoLetra);
        estiloCelda.getFont().setBold(bold);
        estiloCelda.getFont().setItalic(italic);

        if(existeColorLetra){
            estiloCelda.getFont().setColor(colorLetra);
        }

        if(existeColorFondo){
            estiloCelda.setColor(colorFondo);
        }

        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }


    public void setTipoLetra (Worksheet sheet, String nombreLetra, int tamanoLetra,
                              com.aspose.cells.Color colorLetra,
                              int fila, int columna ){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();

        estiloCelda.getFont().setName(nombreLetra);
        estiloCelda.getFont().setSize(tamanoLetra);
        estiloCelda.getFont().setColor(colorLetra);

        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }


        public void setTipoLetra (Worksheet sheet, String nombreLetra, int tamanoLetra,
                              com.aspose.cells.Color colorLetra, boolean bold,
                              int fila, int columna ){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();

        estiloCelda.getFont().setName(nombreLetra);
        estiloCelda.getFont().setSize(tamanoLetra);
        estiloCelda.getFont().setColor(colorLetra);
        estiloCelda.getFont().setBold(bold);

        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }


    public void setTipoLetra (Worksheet sheet, String nombreLetra, int tamanoLetra,
                             boolean bold, boolean italic,
                             int fila, int columna ){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();

        estiloCelda.getFont().setName(nombreLetra);
        estiloCelda.getFont().setSize(tamanoLetra);
        estiloCelda.getFont().setBold(bold);
        estiloCelda.getFont().setItalic(italic);

        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }



    public void setTipoLetra (Worksheet sheet, String nombreLetra, int tamanoLetra,
                             int fila, int columna ){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();

        estiloCelda.getFont().setName(nombreLetra);
        estiloCelda.getFont().setSize(tamanoLetra);

        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }

    public void setNegrilla (Worksheet sheet, boolean bold,
                             int fila, int columna ){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();
        estiloCelda.getFont().setBold(bold);
        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }


    public void setNegrilla (Worksheet sheet, boolean bold,
                             com.aspose.cells.Color colorNegrilla,
                             int fila, int columna ){

        Cells cells = sheet.getCells();
        Cell cell = cells.getCell(fila, columna);

        Style estiloCelda = cell.getStyle();
        estiloCelda.getFont().setBold(bold);
        estiloCelda.getFont().setColor(colorNegrilla);
        cell.setStyle(estiloCelda);

        cells = null;
        cell = null;
        estiloCelda = null;

    }





    /**
     * Metodo para cerrar el  archivo de excel
     * @autor apabon
     * @throws Exception.
     */
    public void cerrarLibro(Workbook wb, String ruta, String nombreLibro, int tipoFormatoLibro) throws Exception {
        try{
            if (wb!=null)
                System.out.println("Tipo formato libro: " + tipoFormatoLibro);
                wb.save(ruta + "/" + nombreLibro ,tipoFormatoLibro);

        }catch (Exception e){
            throw new Exception( "Error en cerrarLibro ....\n"  + e.getMessage() );
        }
    }


public void setAlturaFila (Worksheet sheet, int fila, int altura ) {

    Cells cells = sheet.getCells();
    cells.setRowHeight(fila, altura);
}




    public void setAnchoColumna(Worksheet sheet, int columna,  float ancho)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        sheet.getCells().setColumnWidth(columna, ancho);
    }

    /*

    public void setAnchoColumnaPulgadas(Worksheet sheet, int columna,  double ancho)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        sheet.getCells().setColumnWidthInch(columna, ancho);
    }

    public void setAnchoColumnaPixel(Worksheet sheet, int columna,  int ancho)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        sheet.getCells().setColumnWidthPixel(columna, ancho);
    }

    */

    public void generarTitulos(Worksheet sheet, int filaTitulos, Style estilo, String [] cabecera, float[] dimensiones) throws Exception {
        try{



            for ( int i = 0; i<cabecera.length; i++){

                setCelda(sheet, filaTitulos,  i, cabecera[i], estilo);

                // String letraColumna = this.getLetraExcell(i+1);
                // setCelda(sheet, filaTitulos,  i, letraColumna , estilo);


                setBorde(sheet, filaTitulos, i, com.aspose.cells.Color.BLACK);
                if ( i < dimensiones.length )
                    setAnchoColumna(sheet, i, dimensiones[i] );
            }

            setAlturaFila (sheet,  filaTitulos, 20 );

        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error en generaTitulos ...\n" + e.getMessage() );
        }
    }


    public String getLetraExcell(int columna){

        // Para columnas entre 1 y 16384 o numeros enteros que produzcan maximo ZZZ

        String [] letra = {"","",""};
        String [] letraExcell = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

        int entero_previo = columna;

        int residuo = entero_previo % 26;
        int entero  = entero_previo - residuo;
        if (residuo == 0){
            residuo = 26;
            entero = entero - 1;
        }
        // Primer digito
        letra[0] = letraExcell[residuo-1];

        if (entero > 26){
            // El numero tiene mas de una letra

            entero_previo = entero;
            residuo = entero_previo % 26;
            entero  = entero_previo - residuo;
            if (residuo == 0){
                residuo = 26;
                entero = entero - 1;
            }
            // Segundo digito
            letra[1] = letraExcell[residuo-1];
            if (entero > 0){
                // Tercer digito : Numero conformado de 3 letras
                letra[2] = letraExcell[entero-1];
            }
        }
        else if (entero > 0) {
            // Segundo digito : Numero conformado de solo 2 letras
            letra[1] = letraExcell[entero-1];
        }
        return letra[2]+letra[1]+letra[0];

    }

}





