package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class EsquemaCheque implements Serializable{
    
    private String estado;
    private String distrito;
    private String banco;
    private int cpi;
    private int linea;
    private int columna;
    private String descripcion;
    
    private String campo;
    private double top;
    private double left;
    
    
    
    public EsquemaCheque() {}
    
    // SET
    
    public void setEstado(String status){
        this.estado = status;
    }
    
    public void setDistrito(String dstrct){
        this.distrito = dstrct;
    }
    
    public void setBanco(String banco){
        this.banco = banco;
    }
    
    public void setCPI(int cpi){
        this.cpi = cpi;
    }
    
    public void setLinea(int line){
        this.linea=line;
    }
    
    public void setColumna(int column){
        this.columna = column;
    }
    
    public void setDescripcion(String description){
        this.descripcion = description;
    }
    
    //GET
    
    
    public String getEstado(){
        return this.estado;
    }
    
    public  String getDistrito(){
        return this.distrito;
    }
    
    public String getBanco(){
        return this.banco;
    }
    
    public int getCPI(){
        return this.cpi;
    }
    
    public int getLinea(){
        return this.linea;
    }
    
    public int getColumna(){
        return this.columna;
    }
    
    public String getDescripcion(){
        return this.descripcion;
    }
    
    
    
    //--- OTROS
    
    public int getPosition(){
        int pixelesPulgada=74;
        double pos =28;
        //  if(this.getCPI()>0)
        pos= this.getColumna();
        
        return (int)pos;
    }
    
    
    public static EsquemaCheque getObject(HttpServletRequest request){
        EsquemaCheque esquema=new EsquemaCheque();
        esquema.setDistrito( request.getParameter("distrito"));
        esquema.setBanco( request.getParameter("banco"));
        esquema.setCPI( Integer.parseInt(request.getParameter("cpi")));
        esquema.setDescripcion( request.getParameter("descripcion"));
        esquema.setLinea(Integer.parseInt( request.getParameter("linea") ));
        esquema.setColumna(Integer.parseInt( request.getParameter("columna") ) );
        return esquema;
    }
    
    
    public static EsquemaCheque getObject2(HttpServletRequest request){
        EsquemaCheque esquema=new EsquemaCheque();
        esquema.setDistrito( request.getParameter("distrito2"));
        esquema.setBanco( request.getParameter("banco2"));
        esquema.setCPI( Integer.parseInt(request.getParameter("cpi2")));
        esquema.setDescripcion( request.getParameter("descripcion2"));
        esquema.setLinea(Integer.parseInt( request.getParameter("linea2") ));
        esquema.setColumna(Integer.parseInt( request.getParameter("columna2") ) );
        return esquema;
    }
    
    /**
     * Getter for property campo.
     * @return Value of property campo.
     */
    public java.lang.String getCampo() {
        return campo;
    }
    
    /**
     * Setter for property campo.
     * @param campo New value of property campo.
     */
    public void setCampo(java.lang.String campo) {
        this.campo = campo;
    }
    
    /**
     * Getter for property top.
     * @return Value of property top.
     */
    public double getTop() {
        return top;
    }
    
    /**
     * Setter for property top.
     * @param top New value of property top.
     */
    public void setTop(double top) {
        this.top = top;
    }
    
    /**
     * Getter for property left.
     * @return Value of property left.
     */
    public double getLeft() {
        return left;
    }
    
    /**
     * Setter for property left.
     * @param left New value of property left.
     */
    public void setLeft(double left) {
        this.left = left;
    }
    
}
