/*
 * General.java
 *
 * Created on 7 de marzo de 2007
 */

package com.tsp.operation.model.beans;

import java.sql.*;

/**
 *
 * @author  Luigi.
 */
public class AjusteFlete {
    
    private String agoc;
    private String fecoc;
    private String creaoc;
    private String agusu;
    private String numoc;
    private String rutaoc;
    private String placaoc;
    private String recoc;
    private String cantoc;
    private String unioc;
    private String stgoc;
    private String clioc;
    private String agduenaoc;
    private String otoc;
    private String tipooc;
    private String descoc;
    private String decot;
    private String cantot; 
    private String tarifaot;
    private String codcf; 
    private String fetestd;
    private String undocfletestd;
    private String monflete;
    private String fletedespes;
    private String fletedepmonest;
    private String fletestdpes;
    private String ajusteflete;
    private String tarifapes;
    private String varporcflete;
    private String calificacion;
    private String anegativo; 
    private String apositivo;
    private String sinajuste;
    private String valneg;
    private String valpos;
    private String balance;
    private String rentstd;
    private String rentdesp;
    private String base;
    private String valor_unidad;
    
        
    /** Creates a new instance of AjsuteFlete */
    public AjusteFlete() {
    }
    
    /**
     * Getter for property agoc.
     * @return Value of property agoc.
     */
    public java.lang.String getAgoc() {
        return agoc;
    }    
    
    /**
     * Setter for property agoc.
     * @param agoc New value of property agoc.
     */
    public void setAgoc(java.lang.String agoc) {
        this.agoc = agoc;
    }
    
    /**
     * Getter for property fecoc.
     * @return Value of property fecoc.
     */
    public java.lang.String getFecoc() {
        return fecoc;
    }
    
    /**
     * Setter for property fecoc.
     * @param fecoc New value of property fecoc.
     */
    public void setFecoc(java.lang.String fecoc) {
        this.fecoc = fecoc;
    }
    
    /**
     * Getter for property creaoc.
     * @return Value of property creaoc.
     */
    public java.lang.String getCreaoc() {
        return creaoc;
    }
    
    /**
     * Setter for property creaoc.
     * @param creaoc New value of property creaoc.
     */
    public void setCreaoc(java.lang.String creaoc) {
        this.creaoc = creaoc;
    }
    
    /**
     * Getter for property agusu.
     * @return Value of property agusu.
     */
    public java.lang.String getAgusu() {
        return agusu;
    }
    
    /**
     * Setter for property agusu.
     * @param agusu New value of property agusu.
     */
    public void setAgusu(java.lang.String agusu) {
        this.agusu = agusu;
    }
    
    /**
     * Getter for property numoc.
     * @return Value of property numoc.
     */
    public java.lang.String getNumoc() {
        return numoc;
    }
    
    /**
     * Setter for property numoc.
     * @param numoc New value of property numoc.
     */
    public void setNumoc(java.lang.String numoc) {
        this.numoc = numoc;
    }
    
    /**
     * Getter for property rutaoc.
     * @return Value of property rutaoc.
     */
    public java.lang.String getRutaoc() {
        return rutaoc;
    }
    
    /**
     * Setter for property rutaoc.
     * @param rutaoc New value of property rutaoc.
     */
    public void setRutaoc(java.lang.String rutaoc) {
        this.rutaoc = rutaoc;
    }
    
    /**
     * Getter for property placaoc.
     * @return Value of property placaoc.
     */
    public java.lang.String getPlacaoc() {
        return placaoc;
    }
    
    /**
     * Setter for property placaoc.
     * @param placaoc New value of property placaoc.
     */
    public void setPlacaoc(java.lang.String placaoc) {
        this.placaoc = placaoc;
    }
    
    /**
     * Getter for property recoc.
     * @return Value of property recoc.
     */
    public java.lang.String getRecoc() {
        return recoc;
    }
    
    /**
     * Setter for property recoc.
     * @param recoc New value of property recoc.
     */
    public void setRecoc(java.lang.String recoc) {
        this.recoc = recoc;
    }
    
    /**
     * Getter for property cantoc.
     * @return Value of property cantoc.
     */
    public java.lang.String getCantoc() {
        return cantoc;
    }
    
    /**
     * Setter for property cantoc.
     * @param cantoc New value of property cantoc.
     */
    public void setCantoc(java.lang.String cantoc) {
        this.cantoc = cantoc;
    }
    
    /**
     * Getter for property unioc.
     * @return Value of property unioc.
     */
    public java.lang.String getUnioc() {
        return unioc;
    }
    
    /**
     * Setter for property unioc.
     * @param unioc New value of property unioc.
     */
    public void setUnioc(java.lang.String unioc) {
        this.unioc = unioc;
    }
    
    /**
     * Getter for property stgoc.
     * @return Value of property stgoc.
     */
    public java.lang.String getStgoc() {
        return stgoc;
    }
    
    /**
     * Setter for property stgoc.
     * @param stgoc New value of property stgoc.
     */
    public void setStgoc(java.lang.String stgoc) {
        this.stgoc = stgoc;
    }
    
    /**
     * Getter for property clioc.
     * @return Value of property clioc.
     */
    public java.lang.String getClioc() {
        return clioc;
    }
    
    /**
     * Setter for property clioc.
     * @param clioc New value of property clioc.
     */
    public void setClioc(java.lang.String clioc) {
        this.clioc = clioc;
    }
    
    /**
     * Getter for property agduenaoc.
     * @return Value of property agduenaoc.
     */
    public java.lang.String getAgduenaoc() {
        return agduenaoc;
    }
    
    /**
     * Setter for property agduenaoc.
     * @param agduenaoc New value of property agduenaoc.
     */
    public void setAgduenaoc(java.lang.String agduenaoc) {
        this.agduenaoc = agduenaoc;
    }
    
    /**
     * Getter for property otoc.
     * @return Value of property otoc.
     */
    public java.lang.String getOtoc() {
        return otoc;
    }
    
    /**
     * Setter for property otoc.
     * @param otoc New value of property otoc.
     */
    public void setOtoc(java.lang.String otoc) {
        this.otoc = otoc;
    }
    
    /**
     * Getter for property tipooc.
     * @return Value of property tipooc.
     */
    public java.lang.String getTipooc() {
        return tipooc;
    }
    
    /**
     * Setter for property tipooc.
     * @param tipooc New value of property tipooc.
     */
    public void setTipooc(java.lang.String tipooc) {
        this.tipooc = tipooc;
    }
    
    /**
     * Getter for property descoc.
     * @return Value of property descoc.
     */
    public java.lang.String getDescoc() {
        return descoc;
    }
    
    /**
     * Setter for property descoc.
     * @param descoc New value of property descoc.
     */
    public void setDescoc(java.lang.String descoc) {
        this.descoc = descoc;
    }
    
    /**
     * Getter for property decot.
     * @return Value of property decot.
     */
    public java.lang.String getDecot() {
        return decot;
    }
    
    /**
     * Setter for property decot.
     * @param decot New value of property decot.
     */
    public void setDecot(java.lang.String decot) {
        this.decot = decot;
    }
    
    /**
     * Getter for property cantot.
     * @return Value of property cantot.
     */
    public java.lang.String getCantot() {
        return cantot;
    }
    
    /**
     * Setter for property cantot.
     * @param cantot New value of property cantot.
     */
    public void setCantot(java.lang.String cantot) {
        this.cantot = cantot;
    }
    
    /**
     * Getter for property tarifaot.
     * @return Value of property tarifaot.
     */
    public java.lang.String getTarifaot() {
        return tarifaot;
    }
    
    /**
     * Setter for property tarifaot.
     * @param tarifaot New value of property tarifaot.
     */
    public void setTarifaot(java.lang.String tarifaot) {
        this.tarifaot = tarifaot;
    }
    
    /**
     * Getter for property codcf.
     * @return Value of property codcf.
     */
    public java.lang.String getCodcf() {
        return codcf;
    }
    
    /**
     * Setter for property codcf.
     * @param codcf New value of property codcf.
     */
    public void setCodcf(java.lang.String codcf) {
        this.codcf = codcf;
    }
    
    /**
     * Getter for property fetestd.
     * @return Value of property fetestd.
     */
    public java.lang.String getFetestd() {
        return fetestd;
    }
    
    /**
     * Setter for property fetestd.
     * @param fetestd New value of property fetestd.
     */
    public void setFetestd(java.lang.String fetestd) {
        this.fetestd = fetestd;
    }
    
    /**
     * Getter for property undocfletestd.
     * @return Value of property undocfletestd.
     */
    public java.lang.String getUndocfletestd() {
        return undocfletestd;
    }
    
    /**
     * Setter for property undocfletestd.
     * @param undocfletestd New value of property undocfletestd.
     */
    public void setUndocfletestd(java.lang.String undocfletestd) {
        this.undocfletestd = undocfletestd;
    }
    
    /**
     * Getter for property monflete.
     * @return Value of property monflete.
     */
    public java.lang.String getMonflete() {
        return monflete;
    }
    
    /**
     * Setter for property monflete.
     * @param monflete New value of property monflete.
     */
    public void setMonflete(java.lang.String monflete) {
        this.monflete = monflete;
    }
    
    /**
     * Getter for property fletedespes.
     * @return Value of property fletedespes.
     */
    public java.lang.String getFletedespes() {
        return fletedespes;
    }
    
    /**
     * Setter for property fletedespes.
     * @param fletedespes New value of property fletedespes.
     */
    public void setFletedespes(java.lang.String fletedespes) {
        this.fletedespes = fletedespes;
    }
    
    /**
     * Getter for property fletedepmonest.
     * @return Value of property fletedepmonest.
     */
    public java.lang.String getFletedepmonest() {
        return fletedepmonest;
    }
    
    /**
     * Setter for property fletedepmonest.
     * @param fletedepmonest New value of property fletedepmonest.
     */
    public void setFletedepmonest(java.lang.String fletedepmonest) {
        this.fletedepmonest = fletedepmonest;
    }
    
    /**
     * Getter for property fletestdpes.
     * @return Value of property fletestdpes.
     */
    public java.lang.String getFletestdpes() {
        return fletestdpes;
    }
    
    /**
     * Setter for property fletestdpes.
     * @param fletestdpes New value of property fletestdpes.
     */
    public void setFletestdpes(java.lang.String fletestdpes) {
        if (fletestdpes!=null)
            this.fletestdpes = fletestdpes;
        else
            this.fletestdpes = "0";
    }
    
    /**
     * Getter for property ajusteflete.
     * @return Value of property ajusteflete.
     */
    public java.lang.String getAjusteflete() {
        return ajusteflete;
    }
    
    /**
     * Setter for property ajusteflete.
     * @param ajusteflete New value of property ajusteflete.
     */
    public void setAjusteflete(java.lang.String ajusteflete) {
        this.ajusteflete = ajusteflete;
    }
    
    /**
     * Getter for property tarifapes.
     * @return Value of property tarifapes.
     */
    public java.lang.String getTarifapes() {
        return tarifapes;
    }
    
    /**
     * Setter for property tarifapes.
     * @param tarifapes New value of property tarifapes.
     */
    public void setTarifapes(java.lang.String tarifapes) {
        if (tarifapes!=null)
            this.tarifapes = tarifapes;
        else
            this.tarifapes = "0";
    }
    
    /**
     * Getter for property varporcflete.
     * @return Value of property varporcflete.
     */
    public java.lang.String getVarporcflete() {
        return varporcflete;
    }
    
    /**
     * Setter for property varporcflete.
     * @param varporcflete New value of property varporcflete.
     */
    public void setVarporcflete(java.lang.String varporcflete) {
        this.varporcflete = varporcflete;
    }
    
    /**
     * Getter for property calificacion.
     * @return Value of property calificacion.
     */
    public java.lang.String getCalificacion() {
        return calificacion;
    }
    
    /**
     * Setter for property calificacion.
     * @param calificacion New value of property calificacion.
     */
    public void setCalificacion(java.lang.String calificacion) {
        this.calificacion = calificacion;
    }
    
    /**
     * Getter for property anegativo.
     * @return Value of property anegativo.
     */
    public java.lang.String getAnegativo() {
        return anegativo;
    }
    
    /**
     * Setter for property anegativo.
     * @param anegativo New value of property anegativo.
     */
    public void setAnegativo(java.lang.String anegativo) {
        this.anegativo = anegativo;
    }
    
    /**
     * Getter for property apositivo.
     * @return Value of property apositivo.
     */
    public java.lang.String getApositivo() {
        return apositivo;
    }
    
    /**
     * Setter for property apositivo.
     * @param apositivo New value of property apositivo.
     */
    public void setApositivo(java.lang.String apositivo) {
        this.apositivo = apositivo;
    }
    
    /**
     * Getter for property sinajuste.
     * @return Value of property sinajuste.
     */
    public java.lang.String getSinajuste() {
        return sinajuste;
    }
    
    /**
     * Setter for property sinajuste.
     * @param sinajuste New value of property sinajuste.
     */
    public void setSinajuste(java.lang.String sinajuste) {
        this.sinajuste = sinajuste;
    }
    
    /**
     * Getter for property valneg.
     * @return Value of property valneg.
     */
    public java.lang.String getValneg() {
        return valneg;
    }
    
    /**
     * Setter for property valneg.
     * @param valneg New value of property valneg.
     */
    public void setValneg(java.lang.String valneg) {
        this.valneg = valneg;
    }
    
    /**
     * Getter for property valpos.
     * @return Value of property valpos.
     */
    public java.lang.String getValpos() {
        return valpos;
    }
    
    /**
     * Setter for property valpos.
     * @param valpos New value of property valpos.
     */
    public void setValpos(java.lang.String valpos) {
        this.valpos = valpos;
    }
    
    /**
     * Getter for property balance.
     * @return Value of property balance.
     */
    public java.lang.String getBalance() {
        return balance;
    }
    
    /**
     * Setter for property balance.
     * @param balance New value of property balance.
     */
    public void setBalance(java.lang.String balance) {
        this.balance = balance;
    }
    
    /**
     * Getter for property rentstd.
     * @return Value of property rentstd.
     */
    public java.lang.String getRentstd() {
        return rentstd;
    }
    
    /**
     * Setter for property rentstd.
     * @param rentstd New value of property rentstd.
     */
    public void setRentstd(java.lang.String rentstd) {
        this.rentstd = rentstd;
    }
    
    /**
     * Getter for property rentdesp.
     * @return Value of property rentdesp.
     */
    public java.lang.String getRentdesp() {
        return rentdesp;
    }
    
    /**
     * Setter for property rentdesp.
     * @param rentdesp New value of property rentdesp.
     */
    public void setRentdesp(java.lang.String rentdesp) {
        this.rentdesp = rentdesp;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property valor_unidad.
     * @return Value of property valor_unidad.
     */
    public java.lang.String getValor_unidad() {
        return valor_unidad;
    }
    
    /**
     * Setter for property valor_unidad.
     * @param valor_unidad New value of property valor_unidad.
     */
    public void setValor_unidad(java.lang.String valor_unidad) {
        this.valor_unidad = valor_unidad;
    }
    
}
