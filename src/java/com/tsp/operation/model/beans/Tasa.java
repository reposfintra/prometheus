/*
 * Tasa.java
 *
 * Created on 27 de junio de 2005, 12:30 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  DIOGENES
 */
public class Tasa {
    private String cia;
    private String moneda1;
    private String moneda2;
    private float vlr_conver;
    private float compra;
    private float venta;
    private String fecha;
    private String estado;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    private String base;
    //nuevos
    private int diferencia;
    private double valor_tasa1;
    private double valor_tasa2;
    private double valor_tasa;
    
    /** Creates a new instance of Tasa */
    public static Tasa load(ResultSet rs)throws SQLException {
        Tasa tasa = new Tasa();
        tasa.setCia(rs.getString("cia") );
        tasa.setMoneda1(rs.getString("moneda1") );
        tasa.setMoneda2(rs.getString("moneda2") );
        tasa.setVlr_conver(rs.getFloat("vlr_conver") );
        tasa.setCompra( rs.getFloat("compra") );
        tasa.setVenta( rs.getFloat("venta") );
        tasa.setFecha( rs.getString("fecha") );
        tasa.setEstado(rs.getString("estado"));
        tasa.setLast_update(rs.getDate("last_update") );
        tasa.setUser_update( rs.getString("user_update") );
        tasa.setCreation_date( rs.getDate("creation_date") );
        tasa.setCreation_user( rs.getString("creation_user") );           
        return tasa;              
    }
    
    public void setCia (String cia){
        this.cia=cia;
        
    }
    public String getCia (){
        return cia;
    }
    public void setMoneda1 (String moneda1){
        this.moneda1=moneda1;
        
    }
    public String getMoneda1 (){
        return moneda1;
    }
    public void setMoneda2 (String moneda2){
        this.moneda2=moneda2;
        
    }
    public String getMoneda2 (){
        return moneda2;
    }
    public void setVlr_conver (float vlr_conver){
        this.vlr_conver=vlr_conver;
        
    }
    public float getVlr_conver (){
        return vlr_conver;
    }
    public void setCompra (float compra){
        this.compra=compra;
        
    }
    public float getCompra (){
        return compra;
    }
    public void setVenta (float venta){
        this.venta=venta;
        
    }
    public float getVenta (){
        return venta;
    }
    public void setFecha (String fecha){
        this.fecha=fecha;
        
    }
    public String getFecha (){
        return fecha;
    }
     public void setEstado (String estado){
        this.estado=estado;
        
    }
    public String getEstado ( ){
        return estado;
    }
    public void setLast_update(java.util.Date last_update){
        
        this.last_update = last_update;
        
    }
    
    public java.util.Date getLast_update( ){
        
        return last_update;
        
    }
    
    public void setUser_update(String user_update){        
        this.user_update= user_update;        
    }
    
    public String getUser_update( ){        
        
        return user_update;        
        
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date = creation_date;
        
    }
    
    public java.util.Date getCreation_date( ){
      
        return creation_date;
        
    }
    
    public void setCreation_user(String creation_user){        
       
        this.creation_user= creation_user;        
       
    }
            
    public String getCreation_user( ){        
        return creation_user;        
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
   
    /**
     * Getter for property valor_tasa1.
     * @return Value of property valor_tasa1.
     */
    public double getValor_tasa1() {
        return valor_tasa1;
    }
    
    /**
     * Setter for property valor_tasa1.
     * @param valor_tasa1 New value of property valor_tasa1.
     */
    public void setValor_tasa1(double valor_tasa1) {
        this.valor_tasa1 = valor_tasa1;
    }
    
    /**
     * Getter for property valor_tasa2.
     * @return Value of property valor_tasa2.
     */
    public double getValor_tasa2() {
        return valor_tasa2;
    }
    
    /**
     * Setter for property valor_tasa2.
     * @param valor_tasa2 New value of property valor_tasa2.
     */
    public void setValor_tasa2(double valor_tasa2) {
        this.valor_tasa2 = valor_tasa2;
    }
    
    /**
     * Getter for property valor_tasa3.
     * @return Value of property valor_tasa3.
     */
    public double getValor_tasa() {
        return valor_tasa;
    }
    
    /**
     * Setter for property valor_tasa3.
     * @param valor_tasa3 New value of property valor_tasa3.
     */
    public void setValor_tasa(double valor_tasa) {
        this.valor_tasa = valor_tasa;
    }
    
    /**
     * Getter for property diferencia.
     * @return Value of property diferencia.
     */
    public int getDiferencia() {
        return diferencia;
    }
    
    /**
     * Setter for property diferencia.
     * @param diferencia New value of property diferencia.
     */
    public void setDiferencia(int diferencia) {
        this.diferencia = diferencia;
    }
    
}
