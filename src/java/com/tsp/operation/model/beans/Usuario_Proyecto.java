package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Usuario_Proyecto implements Serializable {
	
	private String dstrct;
	private String login;
	private String project;
	private java.util.Date creation_date;
	private String creation_user;
	
	public static Usuario_Proyecto load(ResultSet rs)throws SQLException{
		
		Usuario_Proyecto usuario_proyecto = new Usuario_Proyecto();
	
		usuario_proyecto.setDstrct(rs.getString(2));
		usuario_proyecto.setLogin(rs.getString(3));
		usuario_proyecto.setProject(rs.getString(4));
		usuario_proyecto.setCreation_date(rs.getTimestamp(7));
		usuario_proyecto.setCreation_user(rs.getString(8));
		
		return usuario_proyecto;
	}
	
	//============================================
	//		Metodos de acceso a propiedades
	//============================================
	
	
	public void setDstrct(String dstrct){
			
		this.dstrct=dstrct;
	}
	
	public String getDstrct(){
	
		return dstrct;
	}

	public void setLogin(String login){
		
		this.login=login;
	}
	
	public String getLogin(){
		
		return login;
	}
	
	public void setProject(String project){
		
		this.project=project;
	}
	
	public String getProject(){
		
		return project;
	}
	
	public void setCreation_date(java.util.Date creation_date){
		
		this.creation_date=creation_date;
	}
	
	public java.util.Date getCreation_date(){
		
		return  creation_date;
	}
	
	public void setCreation_user(String creation_user){
		
		this.creation_user=creation_user;
	}
	
	public String getCreation_user(){
	
		return creation_user;
	}
}





