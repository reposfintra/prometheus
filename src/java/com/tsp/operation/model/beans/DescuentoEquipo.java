/********************************************************************
* Nombre ......................DescuentoEquipoBuscarAction.java     *
* Descripci�n..................Clase Bean de descuento de equipos   *
* Autor........................Armando Oviedo                       *
* Fecha Creaci�n...............06/12/2005                           *
* Modificado por...............LREALES                              *
* Fecha Modificaci�n...........22/05/2006                           *
* Versi�n......................1.0                                  *
* Coyright.....................Transportes Sanchez Polo S.A.        *
********************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class DescuentoEquipo {
    
    private String regStatus;
    private String codigo;
    private String concContable;
    private String concEspecial;
    private String descripcion;
    private String dstrct;
    private String creationUser;
    private String creationDate;
    private String userUpdate;
    private String lastUpdate;     
    private String base;
     
    /** Creates a new instance of DescuentoEquipo */
    public DescuentoEquipo() {
    }
         
    /**
     * Metodo:          DescuentoEquipo
     * Descripcion :    M�todo que retorna un objeto descuentoequipo cargado previamente de un resultset
     * @autor :         Armando Oviedo
     * @modificado por: LREALES
     * @param:          ResultSet rs
     * @return:         objeto DescuentoEquipo
     * @throws:         SQLException
     * @version :       1.0
     */
    public DescuentoEquipo loadResultSet( ResultSet rs ) throws SQLException{
        
        DescuentoEquipo de = new DescuentoEquipo();
        
        de.setRegStatus( rs.getString("reg_status") );
        de.setCodigo( rs.getString("codigo") );
        de.setConcContable( rs.getString("conc_contable") );
        de.setConcEspecial( rs.getString("conc_especial") );        
        de.setDescripcion( rs.getString("descripcion") );
        de.setDstrct( rs.getString("dstrct") );
        de.setCreationUser( rs.getString("creation_user") );
        de.setCreationDate( rs.getString("creation_date") );
        de.setUserUpdate( rs.getString("user_update") );
        de.setLastUpdate( rs.getString("last_update") );
        de.setBase( rs.getString("base") );
        
        return de;
        
    }
    
    /**
     * Getter for property regStatus.
     * @return Value of property regStatus.
     */
    public java.lang.String getRegStatus() {
        return regStatus;
    }
    
    /**
     * Setter for property regStatus.
     * @param regStatus New value of property regStatus.
     */
    public void setRegStatus(java.lang.String regStatus) {
        this.regStatus = regStatus;
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property concContable.
     * @return Value of property concContable.
     */
    public java.lang.String getConcContable() {
        return concContable;
    }
    
    /**
     * Setter for property concContable.
     * @param concContable New value of property concContable.
     */
    public void setConcContable(java.lang.String concContable) {
        this.concContable = concContable;
    }
    
    /**
     * Getter for property concEspecial.
     * @return Value of property concEspecial.
     */
    public java.lang.String getConcEspecial() {
        return concEspecial;
    }
    
    /**
     * Setter for property concEspecial.
     * @param concEspecial New value of property concEspecial.
     */
    public void setConcEspecial(java.lang.String concEspecial) {
        this.concEspecial = concEspecial;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property creationUser.
     * @return Value of property creationUser.
     */
    public java.lang.String getCreationUser() {
        return creationUser;
    }
    
    /**
     * Setter for property creationUser.
     * @param creationUser New value of property creationUser.
     */
    public void setCreationUser(java.lang.String creationUser) {
        this.creationUser = creationUser;
    }
    
    /**
     * Getter for property creationDate.
     * @return Value of property creationDate.
     */
    public java.lang.String getCreationDate() {
        return creationDate;
    }
    
    /**
     * Setter for property creationDate.
     * @param creationDate New value of property creationDate.
     */
    public void setCreationDate(java.lang.String creationDate) {
        this.creationDate = creationDate;
    }
    
    /**
     * Getter for property userUpdate.
     * @return Value of property userUpdate.
     */
    public java.lang.String getUserUpdate() {
        return userUpdate;
    }
    
    /**
     * Setter for property userUpdate.
     * @param userUpdate New value of property userUpdate.
     */
    public void setUserUpdate(java.lang.String userUpdate) {
        this.userUpdate = userUpdate;
    }
    
    /**
     * Getter for property lastUpdate.
     * @return Value of property lastUpdate.
     */
    public java.lang.String getLastUpdate() {
        return lastUpdate;
    }
    
    /**
     * Setter for property lastUpdate.
     * @param lastUpdate New value of property lastUpdate.
     */
    public void setLastUpdate(java.lang.String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
}