package com.tsp.operation.model.beans;

/**
 * Bean para la tabla bancos
 * @author darrieta - geotech
 */
public class Bancos {

    private String codigo;
    private String nombre;
    private String nit;
    private int baseAno;

    /**
     * Get the value of baseAno
     *
     * @return the value of baseAno
     */
    public int getBaseAno() {
        return baseAno;
    }

    /**
     * Set the value of baseAno
     *
     * @param baseAno new value of baseAno
     */
    public void setBaseAno(int baseAno) {
        this.baseAno = baseAno;
    }

    /**
     * Get the value of nit
     *
     * @return the value of nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * Set the value of nit
     *
     * @param nit new value of nit
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * Get the value of nombre
     *
     * @return the value of nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the value of nombre
     *
     * @param nombre new value of nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get the value of codigo
     *
     * @return the value of codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Set the value of codigo
     *
     * @param codigo new value of codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
