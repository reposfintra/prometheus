/*
 * Zona.java
 *
 * Created on 13 de juio de 2005, 09:07 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  Henry
 */
public class Novedad implements Serializable {
    private String codnovedad;
    private String nomnovedad;   
    private String base;
    private String creation_user;
    private String user_update;
    
    /** Creates a new instance of Zona */
    public static Novedad load(ResultSet rs)throws SQLException {
        Novedad novedad = new Novedad();  
        novedad.setCodNovedad( rs.getString("codnovedad"));
        novedad.setNomNovedad( rs.getString("nomnovedad") );
        novedad.setBase(rs.getString("base"));
        return novedad;
    }   
   
    //=====================================================
    //              Metodos de acceso 
    //=====================================================
    
    public void setCodNovedad(String codnovedad){    
        this.codnovedad = codnovedad;       
    }
    public void setNomNovedad(String nomnovedad){       
        this.nomnovedad = nomnovedad;
    }    
    public String getCodNovedad(){    
        return this.codnovedad;
    }
    public String getNomNovedad(){        
        return this.nomnovedad;
    }        
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}