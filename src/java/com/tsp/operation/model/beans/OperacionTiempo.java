/*
 * OperacionTiempo.java
 *
 * Created on 26 de febrero de 2002, 0:24
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Mario Fontalvo
 */
public class OperacionTiempo implements Serializable{
    private String TimeCode1;
    private String TimeCode2;
    private String NMonicCode1;
    private String NMonicCode2;
    private String ValotTimeCode1;
    private String ValotTimeCode2;
    /** Creates a new instance of OperacionTiempo */
    public OperacionTiempo() {
    }
    
    public static OperacionTiempo load(ResultSet rs)throws SQLException{
        OperacionTiempo datos = new OperacionTiempo();
        datos.setTimeCode1(rs.getString( 1 ));
        datos.setTimeCode2(rs.getString( 2 ));
        datos.setNMonicCode1("");
        datos.setNMonicCode2("");
        datos.setValorTimeCode1("");
        datos.setValorTimeCode2("");
        return datos;        
    }
    
    //setter
    public void setTimeCode1(String valor){
        this.TimeCode1 = valor;
    }
    public void setTimeCode2(String valor){
        this.TimeCode2 = valor;
    }
    public void setNMonicCode1(String valor){
        this.NMonicCode1 = valor;
    }
    public void setNMonicCode2(String valor){
        this.NMonicCode2 = valor;
    }
    public void setValorTimeCode1(String valor){
        this.ValotTimeCode1 = valor;
    }
    public void setValorTimeCode2(String valor){
        this.ValotTimeCode2 = valor;
    }
    
    public void setDatos(List ListaPlanilla){
        Iterator it = ListaPlanilla.iterator();
        while(it.hasNext()){
            PlanillaTiempo pla = (PlanillaTiempo) it.next();
            if (pla.getTimeCode().equals(this.TimeCode1)){
                setNMonicCode1(pla.getNMonicCode());
                setValorTimeCode1(pla.getDateTimeTraffic());                
            }
            if (pla.getTimeCode().equals(this.TimeCode2)){
                setNMonicCode2(pla.getNMonicCode());
                setValorTimeCode2(pla.getDateTimeTraffic());
            }            
        }        
    }
    
    //getter    
    public String getTimeCode1(){
        return this.TimeCode1;
    }
    public String getTimeCode2(){
        return this.TimeCode2;
    }
    public String getNMonicCode1(){
        return this.NMonicCode1;
    }
    public String getNMonicCode2(){
        return this.NMonicCode2;
    }
    public String getValorTimeCode1(){
        return this.ValotTimeCode1;
    }
    public String getValorTimeCode2(){
        return this.ValotTimeCode2;
    }
    public String getNMonic(){
        return "("+getNMonicCode1()+"-"+getNMonicCode2()+")";
    }
    public String getResultado(){
        String Resultado="";
        if (!getValorTimeCode1().equals("") && !getValorTimeCode2().equals("")){        	
			if (Compare(getValorTimeCode1(),getValorTimeCode2())==1)
			  Resultado = BuscarDiferenciaFecha(getValorTimeCode1(), getValorTimeCode2());
			else  
			  Resultado = BuscarDiferenciaFecha(getValorTimeCode1(), getValorTimeCode1());
        }            
        else
            Resultado = "No Calculada";
        return Resultado;
    }
    
    public static int Compare (String FechaInicial,String FechaFinal){
		int result;
		int ano_ini = Integer.parseInt(FechaInicial.substring(0,4));
        int mes_ini = Integer.parseInt(FechaInicial.substring(5,7));
        int dia_ini = Integer.parseInt(FechaInicial.substring(8,10));        
        int hh_ini  = Integer.parseInt(FechaInicial.substring(11,13));  
        int mm_ini  = Integer.parseInt(FechaInicial.substring(14,16));  
        int ss_ini  = Integer.parseInt(FechaInicial.substring(17,19));
      
		int ano_fin = Integer.parseInt(FechaFinal.substring(0,4));          
		int mes_fin = Integer.parseInt(FechaFinal.substring(5,7));
		int dia_fin = Integer.parseInt(FechaFinal.substring(8,10));
        int hh_fin  = Integer.parseInt(FechaFinal.substring(11,13));  
        int mm_fin  = Integer.parseInt(FechaFinal.substring(14,16));  
        int ss_fin  = Integer.parseInt(FechaFinal.substring(17,19));
		
		
        if(ano_fin>ano_ini) result = 1;
        else if(ano_fin==ano_ini && mes_fin>mes_ini) result = 1;
        else if(ano_fin==ano_ini && mes_fin==mes_ini && dia_fin> dia_ini) result = 1; 
        else if(ano_fin==ano_ini && mes_fin==mes_ini && dia_fin==dia_ini && hh_fin> hh_ini) result = 1;
        else if(ano_fin==ano_ini && mes_fin==mes_ini && dia_fin==dia_ini && hh_fin==hh_ini && mm_fin> mm_ini) result = 1;
        else if(ano_fin==ano_ini && mes_fin==mes_ini && dia_fin==dia_ini && hh_fin==hh_ini && mm_fin==mm_ini && ss_fin> ss_ini) result = 1;
        else if(ano_fin==ano_ini && mes_fin==mes_ini && dia_fin==dia_ini && hh_fin==hh_ini && mm_fin==mm_ini && ss_fin==ss_ini) result = 0;
        else result =  -1;		
        return result;
	}
	
    public String BuscarDiferenciaFecha(String fecha1, String fecha2){
         
         String total="";
         int ano1=Integer.parseInt(fecha1.substring(0,4)  );  
         int mes1=Integer.parseInt(fecha1.substring(5,7)  );  
         int dia1=Integer.parseInt(fecha1.substring(8,10) );  
         int hh1= Integer.parseInt(fecha1.substring(11,13));  
         int min1=Integer.parseInt(fecha1.substring(14,16));  
         int ss1= Integer.parseInt(fecha1.substring(17,19));
         String tipo1= fecha1.substring(fecha1.length()-2,fecha1.length());
         if(tipo1.toUpperCase().equals("PM"))
             hh1+=12;
         
         int ano2=Integer.parseInt(fecha2.substring(0,4)  );  
         int mes2=Integer.parseInt(fecha2.substring(5,7)  );  
         int dia2=Integer.parseInt(fecha2.substring(8,10) );
         int hh2= Integer.parseInt(fecha2.substring(11,13));  
         int min2=Integer.parseInt(fecha2.substring(14,16));  
         int ss2= Integer.parseInt(fecha2.substring(17,19));
         String tipo2= fecha2.substring(fecha2.length()-2,fecha2.length());
         if(tipo2.toUpperCase().equals("PM"))
             hh2+=12;
         
         
 // Calculamos diferencia
         
         int totalAno = ano2-ano1;
         int totalMes = mes2-mes1;
         if(totalMes<0){
             totalAno--;
             totalMes+=12;
         }
         
         int totalDia = dia2-dia1;
         if(totalDia<0){
            totalMes--;
            totalDia+=31;
	         if(totalMes<0){
	             totalAno--;
	             totalMes+=12;
	         }            
         }
         
         int totalHora= hh2-hh1;
         if(totalHora<0){
             totalDia--;
             totalHora+=24;
	         if(totalDia<0){
	            totalMes--;
	            totalDia+=31;
		         if(totalMes<0){
		             totalAno--;
		             totalMes+=12;
		         }            
	         }             
         }
         
         int totalMin = min2-min1;
         if(totalMin<0){
             totalHora--;
             totalMin+=60;
	         if(totalHora<0){
	             totalDia--;
	             totalHora+=24;
		         if(totalDia<0){
		            totalMes--;
		            totalDia+=31;
			         if(totalMes<0){
			             totalAno--;
			             totalMes+=12;
			         }            
		         }             
         	 }
             
         }
         
         int totalSS  = ss2-ss1;
         if(totalSS<0){
             totalMin--;
             totalSS+=60;
	         if(totalMin<0){	
	             totalHora--;
	             totalMin+=60;
		         if(totalHora<0){
		             totalDia--;
		             totalHora+=24;
			         if(totalDia<0){
			            totalMes--;
			            totalDia+=31;
				         if(totalMes<0){
				             totalAno--;
				             totalMes+=12;
				         }            
			         }             
	    	 	}		             
         	 }
         }
         
        
 // Formateamos la Diferencia  
         
         String ano =(totalAno==0)?"0000":"000"+String.valueOf(totalAno);         
         String mes="";
         if(totalMes==0) mes="00";
         else mes=(totalMes>0 && totalMes<10)?"0"+String.valueOf(totalMes):String.valueOf(totalMes);
         
         String dia="";
         if(totalDia==0) dia="00";
         else dia=(totalDia>0 && totalDia<10)?"0"+String.valueOf(totalDia):String.valueOf(totalDia);
                  
         String hora="";
         if(totalHora==0) hora="00";
         else hora=(totalHora>0 && totalHora<10)?"0"+String.valueOf(totalHora):String.valueOf(totalHora);
         
         String minutos="";
         if(totalMin==0) minutos="00";
         else minutos=(totalMin>0 && totalMin<10)?"0"+String.valueOf(totalMin):String.valueOf(totalMin);
       
         String segundos="";
         if(totalSS==0) segundos="00";
         else segundos=(totalSS>0 && totalSS<10)?"0"+String.valueOf(totalSS):String.valueOf(totalSS);
              
      // Creamos la diferencia total
         total= ano +"-" + mes + "-" + dia + " " + hora + ":" + minutos + ":"+ segundos;
               
       return total;
    }
    
}
