
/***************************************
    * Nombre Clase ............. Corrida.java
    * Descripci�n  .. . . . . .  Permite guardar datos de la corrida
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  22/02/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.beans;

import java.util.*;


public class Corrida {
    
    private String corrida;
    private String documento;
    private String branch_code;
    private String bank_account_no;
    private String agencia;
    private String proveedor;
    private String distrito;
    private String nombre; 
    private String planilla;
    private String placa;
    private String moneda;    
    private String impresion;
    private String user_impresion;
    private String pago;
    private String user_pago;
    private String tipoDoc;    
    private String cheque;    
    private double valor;
    private double valor_me;
    
    private String generador;
    private String nitProveedor;
    
    
    //Ivan Dario Gomez 2006-04-18
    private String FechaCreacion;
    private String TotalFacturas;
    private String Autorizadas;
    private String Faltantes;
    private String Banco;
    private String Sucursal;
    private String Selected;
    
    //fernel
    private String  tipoPago;    
    private String  bancoTransferencia;
    private String  sucursalTransferencia;
    private String  tipoCuentaTransferencia;
    private String  noCuentaTransferencia;
    private String  nombreCuentaTransferencia;
    private String  cedulaCuentaTransferencia;
    private String  base;
    
   //ffernandez
    private String  Numerohc;
    
    private String total_cheques;

    
    private int     id;
    
    
    private List item;
    private double total;
    private String key;
    private String moneda_banco;
    
    public Corrida() {
    }    
    
     
    
     // SET :    
     /**
     * M�todo que setea valores de atributos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void setDistrito (String val){ 
        this.distrito   = val; 
    }    
    public void setNombre (String val){ 
        this.nombre  = val; 
    }    
    public void setPlanilla(String val){ 
        this.planilla  = val; 
    }    
    public void setPlaca (String val){ 
        this.placa     = val; 
    }
    public void setMoneda (String val){ 
        this.moneda    = val; 
    }    
    public void setImpresion (String val){ 
        this.impresion  = val; 
    }
    public void setUserImpresion (String val){ 
        this.user_impresion  = val; 
    }
    public void setPago (String val){ 
        this.pago   = val; 
    }
    public void setUserPago (String val){ 
        this.user_pago  = val; 
    }
    public void setTipoDoc(String val){ 
        this.tipoDoc  = val; 
    }
    public void setCheque(String val){ 
        this.cheque = val; 
    }
    public void setValor (double val){ 
        this.valor  = val; 
    }
    
    
    
     // GET :    
     /**
     * M�todo que devuelven valores de atributos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    
    public String getDistrito (){  
        return this.distrito; 
    }
    public String getNombre (){  
        return this.nombre ; 
    }
    public String getPlanilla(){  
        return this.planilla ; 
    }    
    public String getPlaca(){  
        return this.placa ; 
    }
    public String getMoneda(){  
        return this.moneda; 
    }    
    public String getImpresion(){  
        return this.impresion ; 
    }
    public String getUserImpresion (){  
        return this.user_impresion  ; 
    }
    public String getPago(){  
        return this.pago  ; 
    }
    public String getUserPago (){  
        return this.user_pago ; 
    }
    public String getTipoDoc (){  
        return this.tipoDoc ; 
    }
    public String getCheque(){  
        return this.cheque ; 
    }
    public double getValor (){  
        return this.valor ; 
    }
    
    
   
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property bank_account_no.
     * @return Value of property bank_account_no.
     */
    public java.lang.String getBank_account_no() {
        return bank_account_no;
    }
    
    /**
     * Setter for property bank_account_no.
     * @param bank_account_no New value of property bank_account_no.
     */
    public void setBank_account_no(java.lang.String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }
    
    /**
     * Getter for property branch_code.
     * @return Value of property branch_code.
     */
    public java.lang.String getBranch_code() {
        return branch_code;
    }
    
    /**
     * Setter for property branch_code.
     * @param branch_code New value of property branch_code.
     */
    public void setBranch_code(java.lang.String branch_code) {
        this.branch_code = branch_code;
    }
    
    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }
    
    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property valor_me.
     * @return Value of property valor_me.
     */
    public double getValor_me() {
        return valor_me;
    }
    
    /**
     * Setter for property valor_me.
     * @param valor_me New value of property valor_me.
     */
    public void setValor_me(double valor_me) {
        this.valor_me = valor_me;
    }
    
    /**
     * Getter for property generador.
     * @return Value of property generador.
     */
    public java.lang.String getGenerador() {
        return generador;
    }
    
    /**
     * Setter for property generador.
     * @param generador New value of property generador.
     */
    public void setGenerador(java.lang.String generador) {
        this.generador = generador;
    }
    
    /**
     * Getter for property nitProveedor.
     * @return Value of property nitProveedor.
     */
    public java.lang.String getNitProveedor() {
        return nitProveedor;
    }
    
    /**
     * Setter for property nitProveedor.
     * @param nitProveedor New value of property nitProveedor.
     */
    public void setNitProveedor(java.lang.String nitProveedor) {
        this.nitProveedor = nitProveedor;
    }
    
    /**
     * Getter for property FechaCreacion.
     * @return Value of property FechaCreacion.
     */
    public java.lang.String getFechaCreacion() {
        return FechaCreacion;
    }
    
    /**
     * Setter for property FechaCreacion.
     * @param FechaCreacion New value of property FechaCreacion.
     */
    public void setFechaCreacion(java.lang.String FechaCreacion) {
        this.FechaCreacion = FechaCreacion;
    }
    
    /**
     * Getter for property TotalFacturas.
     * @return Value of property TotalFacturas.
     */
    public java.lang.String getTotalFacturas() {
        return TotalFacturas;
    }
    
    /**
     * Setter for property TotalFacturas.
     * @param TotalFacturas New value of property TotalFacturas.
     */
    public void setTotalFacturas(java.lang.String TotalFacturas) {
        this.TotalFacturas = TotalFacturas;
    }
    
    /**
     * Getter for property Autorizadas.
     * @return Value of property Autorizadas.
     */
    public java.lang.String getAutorizadas() {
        return Autorizadas;
    }
    
    /**
     * Setter for property Autorizadas.
     * @param Autorizadas New value of property Autorizadas.
     */
    public void setAutorizadas(java.lang.String Autorizadas) {
        this.Autorizadas = Autorizadas;
    }
    
    /**
     * Getter for property Faltantes.
     * @return Value of property Faltantes.
     */
    public java.lang.String getFaltantes() {
        return Faltantes;
    }
    
    /**
     * Setter for property Faltantes.
     * @param Faltantes New value of property Faltantes.
     */
    public void setFaltantes(java.lang.String Faltantes) {
        this.Faltantes = Faltantes;
    }
    
    /**
     * Getter for property Banco.
     * @return Value of property Banco.
     */
    public java.lang.String getBanco() {
        return Banco;
    }
    
    /**
     * Setter for property Banco.
     * @param Banco New value of property Banco.
     */
    public void setBanco(java.lang.String Banco) {
        this.Banco = Banco;
    }
    
    /**
     * Getter for property Sucursal.
     * @return Value of property Sucursal.
     */
    public java.lang.String getSucursal() {
        return Sucursal;
    }
    
    /**
     * Setter for property Sucursal.
     * @param Sucursal New value of property Sucursal.
     */
    public void setSucursal(java.lang.String Sucursal) {
        this.Sucursal = Sucursal;
    }
    
    /**
     * Getter for property Selected.
     * @return Value of property Selected.
     */
    public java.lang.String getSelected() {
        return Selected;
    }
    
    /**
     * Setter for property Selected.
     * @param Selected New value of property Selected.
     */
    public void setSelected(java.lang.String Selected) {
        this.Selected = Selected;
    }
    
    /**
     * Getter for property tipoPago.
     * @return Value of property tipoPago.
     */
    public java.lang.String getTipoPago() {
        return tipoPago;
    }
    
    /**
     * Setter for property tipoPago.
     * @param tipoPago New value of property tipoPago.
     */
    public void setTipoPago(java.lang.String tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    /**
     * Getter for property bancoTransferencia.
     * @return Value of property bancoTransferencia.
     */
    public java.lang.String getBancoTransferencia() {
        return bancoTransferencia;
    }
    
    /**
     * Setter for property bancoTransferencia.
     * @param bancoTransferencia New value of property bancoTransferencia.
     */
    public void setBancoTransferencia(java.lang.String bancoTransferencia) {
        this.bancoTransferencia = bancoTransferencia;
    }
    
    /**
     * Getter for property sucursalTransferencia.
     * @return Value of property sucursalTransferencia.
     */
    public java.lang.String getSucursalTransferencia() {
        return sucursalTransferencia;
    }
    
    /**
     * Setter for property sucursalTransferencia.
     * @param sucursalTransferencia New value of property sucursalTransferencia.
     */
    public void setSucursalTransferencia(java.lang.String sucursalTransferencia) {
        this.sucursalTransferencia = sucursalTransferencia;
    }
    
    /**
     * Getter for property tipoCuentaTransferencia.
     * @return Value of property tipoCuentaTransferencia.
     */
    public java.lang.String getTipoCuentaTransferencia() {
        return tipoCuentaTransferencia;
    }
    
    /**
     * Setter for property tipoCuentaTransferencia.
     * @param tipoCuentaTransferencia New value of property tipoCuentaTransferencia.
     */
    public void setTipoCuentaTransferencia(java.lang.String tipoCuentaTransferencia) {
        this.tipoCuentaTransferencia = tipoCuentaTransferencia;
    }
    
    /**
     * Getter for property noCuentaTransferencia.
     * @return Value of property noCuentaTransferencia.
     */
    public java.lang.String getNoCuentaTransferencia() {
        return noCuentaTransferencia;
    }
    
    /**
     * Setter for property noCuentaTransferencia.
     * @param noCuentaTransferencia New value of property noCuentaTransferencia.
     */
    public void setNoCuentaTransferencia(java.lang.String noCuentaTransferencia) {
        this.noCuentaTransferencia = noCuentaTransferencia;
    }
    
    /**
     * Getter for property nombreCuentaTransferencia.
     * @return Value of property nombreCuentaTransferencia.
     */
    public java.lang.String getNombreCuentaTransferencia() {
        return nombreCuentaTransferencia;
    }
    
    /**
     * Setter for property nombreCuentaTransferencia.
     * @param nombreCuentaTransferencia New value of property nombreCuentaTransferencia.
     */
    public void setNombreCuentaTransferencia(java.lang.String nombreCuentaTransferencia) {
        this.nombreCuentaTransferencia = nombreCuentaTransferencia;
    }
    
    /**
     * Getter for property cedulaCuentaTransferencia.
     * @return Value of property cedulaCuentaTransferencia.
     */
    public java.lang.String getCedulaCuentaTransferencia() {
        return cedulaCuentaTransferencia;
    }
    
    /**
     * Setter for property cedulaCuentaTransferencia.
     * @param cedulaCuentaTransferencia New value of property cedulaCuentaTransferencia.
     */
    public void setCedulaCuentaTransferencia(java.lang.String cedulaCuentaTransferencia) {
        this.cedulaCuentaTransferencia = cedulaCuentaTransferencia;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public int getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * Getter for property Numerohc.
     * @return Value of property Numerohc.
     */
    public java.lang.String getNumerohc() {
        return Numerohc;
    }
    
    /**
     * Setter for property Numerohc.
     * @param Numerohc New value of property Numerohc.
     */
    public void setNumerohc(java.lang.String Numerohc) {
        this.Numerohc = Numerohc;
    }
    
    /**
     * Getter for property total_cheques.
     * @return Value of property total_cheques.
     */
    public java.lang.String getTotal_cheques() {
        return total_cheques;
    }
    
    /**
     * Setter for property total_cheques.
     * @param total_cheques New value of property total_cheques.
     */
    public void setTotal_cheques(java.lang.String total_cheques) {
        this.total_cheques = total_cheques;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.util.List getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.util.List item) {
        this.item = item;
    }
    
    /**
     * Getter for property total.
     * @return Value of property total.
     */
    public double getTotal() {
        return total;
    }
    
    /**
     * Setter for property total.
     * @param total New value of property total.
     */
    public void setTotal(double total) {
        this.total = total;
    }
    
    /**
     * Getter for property key.
     * @return Value of property key.
     */
    public java.lang.String getKey() {
        return key;
    }
    
    /**
     * Setter for property key.
     * @param key New value of property key.
     */
    public void setKey(java.lang.String key) {
        this.key = key;
    }
    
    /**
     * Getter for property moneda_banco.
     * @return Value of property moneda_banco.
     */
    public java.lang.String getMoneda_banco() {
        return moneda_banco;
    }
    
    /**
     * Setter for property moneda_banco.
     * @param moneda_banco New value of property moneda_banco.
     */
    public void setMoneda_banco(java.lang.String moneda_banco) {
        this.moneda_banco = moneda_banco;
    }
    
}
