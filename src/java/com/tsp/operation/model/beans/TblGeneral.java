/******************************************************************
* Nombre ......................TblGeneral.java
* Descripci�n..................Clase bean para tabla general
* Autor........................Armando Oviedo
* Fecha........................15/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*;
/**  
 * @author  Armando Oviedo
 */
public class TblGeneral {
    
    String regStatus;
    String dstrct;
    String lastUpdate;
    String userUpdate;
    String creationDate;
    String creationUser;
    String base;
    String codtabla;
    String codigo;
    String descripcion;
    String programa;
    private String codigoTabla;
    private String uc;
    private String um;
    private String fc;
    private String fm;
    private String estado;  
    private String distrito;
    private double porcentaje;
    
    /** Creates a new instance of TblGeneral */
    public TblGeneral() {
    }
    
    public static TblGeneral load(ResultSet rs) throws SQLException {
        TblGeneral tbl = new TblGeneral();
        
        tbl.setCodigoTabla(rs.getString("codtabla"));
        tbl.setPrograma(rs.getString("programa"));
        tbl.setCodigo(rs.getString("codigo"));
        tbl.setDescripcion(rs.getString("descripcion"));
        tbl.setEstado(rs.getString("reg_status"));
        String fecha1 =rs.getString("creation_date").substring(0,10);
        tbl.setFC(fecha1);
        tbl.setUc(rs.getString("creation_user"));
        String fecha2 =rs.getString("last_update").substring(0,10);
        tbl.setFm(fecha2);
        tbl.setUm(rs.getString("user_update"));
        tbl.setBase(rs.getString("base"));
        tbl.setDistrito(rs.getString("dstrct"));
        
        return tbl;
    }    
    
    /**
     * M�todo que retorna un objeto TblGeneral cargado previamente de un resultset
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......ResultSet rs
     * @return.......objeto TblGeneral
     **/ 
    
    public static TblGeneral load2(ResultSet rs) throws SQLException {
        TblGeneral tbl = new TblGeneral();
        
        tbl.setCodigoTabla(rs.getString("codtabla"));
        tbl.setCodigo(rs.getString("codigo"));
        tbl.setDescripcion(rs.getString("descripcion"));
        tbl.setEstado(rs.getString("reg_status"));
        String fecha1 =rs.getString("creation_date").substring(0,10);
        tbl.setFC(fecha1);
        tbl.setUc(rs.getString("creation_user"));
        String fecha2 =rs.getString("last_update").substring(0,10);
        tbl.setFm(fecha2);
        tbl.setUm(rs.getString("user_update"));
        
        return tbl;
    }
    
    public TblGeneral loadResultSet(ResultSet rs) throws SQLException{
        TblGeneral tg = new TblGeneral();
        tg.setBase(rs.getString("base"));
        tg.setCodigo(rs.getString("codigo"));
        tg.setCodTabla(rs.getString("codtabla"));
        tg.setPrograma(rs.getString("programa"));
        tg.setCreationDate(rs.getString("creation_date"));
        tg.setCreationUser(rs.getString("creation_user"));
        tg.setDescripcion(rs.getString("descripcion"));
        tg.setDstrct(rs.getString("dstrct"));
        tg.setLastUpdate(rs.getString("last_update"));
        tg.setRegStatus(rs.getString("reg_status"));
        tg.setUserUpdate(rs.getString("user_update"));
        return tg;
    }
    
    /**
    * setFC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setFC(String dato){
        this.fc = dato;
    }
    
    /**
    * getFC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getFC(){
        return this.fc;
    }
    
    /**
    * setCodigoTabla
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */
    public void setCodigoTabla(String dato){
        this.codigoTabla = dato;
    }
    
    /**
    * getCodigoTabla
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getCodigoTabla(){
        return this.codigoTabla;
    }
    
    public void setRegStatus(String regStatus){
        this.regStatus = regStatus;
    }
    public void setDstrct(String dstrct){
        this.dstrct = dstrct;
    }
    public void setLastUpdate(String lastUpdate){
        this.lastUpdate = lastUpdate;
    }
    public void setUserUpdate(String userUpdate){
        this.userUpdate = userUpdate;
    }
    public void setCreationDate(String creationDate){
        this.creationDate = creationDate;
    }
    public void setCreationUser(String creationUser){
        this.creationUser = creationUser;
    }
    public void setBase(String base){
        this.base = base;
    }
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    public void setCodTabla(String codtabla){
        this.codtabla = codtabla;
    }
    public void setPrograma(String programa){
        this.programa = programa;
    }
    public String getRegStatus(){
        return this.regStatus;
    }
    public String getDstrct(){
        return this.dstrct;
    }
    public String getLastUpdate(){
        return this.lastUpdate;
    }
    public String getUserUpdate(){
        return this.userUpdate;
    }
    public String getCreationDate(){
        return this.creationDate;
    }
    public String getCreationUser(){
        return this.creationUser;
    }
    public String getBase(){
        return this.base;
    }
    public String getCodigo(){
        return this.codigo;
    }
    public String getDescripcion(){
        return this.descripcion;
    }
    public String getCodTabla(){
        return this.codtabla;
    }
    public String getPrograma(){
        return this.programa;
    }
    
    /**
     * Getter for property uc.
     * @return Value of property uc.
     */
    public java.lang.String getUc () {
        return uc;
    }
    
    /**
     * Setter for property uc.
     * @param uc New value of property uc.
     */
    public void setUc (java.lang.String uc) {
        this.uc = uc;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado () {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado (java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property um.
     * @return Value of property um.
     */
    public java.lang.String getUm () {
        return um;
    }
    
    /**
     * Setter for property um.
     * @param um New value of property um.
     */
    public void setUm (java.lang.String um) {
        this.um = um;
    }
    
    /**
     * Getter for property fm.
     * @return Value of property fm.
     */
    public java.lang.String getFm () {
        return fm;
    }
    
    /**
     * Setter for property fm.
     * @param fm New value of property fm.
     */
    public void setFm (java.lang.String fm) {
        this.fm = fm;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property porcentaje.
     * @return Value of property porcentaje.
     */
    public double getPorcentaje() {
        return porcentaje;
    }
    
    /**
     * Setter for property porcentaje.
     * @param porcentaje New value of property porcentaje.
     */
    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }
    
}
