/*
 * Nombre        MovimientoAduana.java
 * Descripción   Bean que representa un registro de movimiento_aduana
 * Autor         Alejandro Payares
 * Fecha         19 de enero de 2006, 04:37 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.model.beans;

/**
 * Bean que representa un registro de movimiento_aduana
 * @author Alejandro Payares
 */
public class MovimientoAduana {
    
    /**
     * Holds value of property estado.
     */
    private String estado;
    
    /**
     * Holds value of property fechaestado.
     */
    private String fechaestado;
    
    /**
     * Holds value of property observacion.
     */
    private String observacion;
    
    /**
     * Holds value of property fechaobservacion.
     */
    private String fechaobservacion;
    
    /**
     * Holds value of property numrem.
     */
    private String numrem;
    
    /**
     * Crea una nueva instancia de MovimientoAduana
     * @autor  Alejandro Payares
     */
    public MovimientoAduana() {
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public String getEstado() {
        return this.estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property fechaestado.
     * @return Value of property fechaestado.
     */
    public String getFechaestado() {
        return this.fechaestado;
    }
    
    /**
     * Setter for property fechaestado.
     * @param fechaestado New value of property fechaestado.
     */
    public void setFechaestado(String fechaestado) {
        this.fechaestado = fechaestado;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public String getObservacion() {
        return this.observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property fechaobservacion.
     * @return Value of property fechaobservacion.
     */
    public String getFechaobservacion() {
        return this.fechaobservacion;
    }
    
    /**
     * Setter for property fechaobservacion.
     * @param fechaobservacion New value of property fechaobservacion.
     */
    public void setFechaobservacion(String fechaobservacion) {
        this.fechaobservacion = fechaobservacion;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public String getNumrem() {
        return this.numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(String numrem) {
        this.numrem = numrem;
    }
    
    public static MovimientoAduana load(java.sql.ResultSet rs)throws java.sql.SQLException {
        MovimientoAduana ma = new MovimientoAduana();
        ma.setEstado(rs.getString("estado"));
        ma.setFechaestado(rs.getString("fechaestado"));
        ma.setObservacion(rs.getString("observacion"));
        ma.setFechaobservacion(rs.getString("fechaobservacion"));
        ma.setNumrem(rs.getString("numrem"));
        return ma;
    }
    
}
