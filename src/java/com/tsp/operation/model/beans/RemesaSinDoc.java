/***********************************************************************************
 * Nombre clase : ............... RemesaSinDoc.java                                *
 * Descripcion :................. Clase que maneja los atributos relacionados con  *
 *                                el reporte de viajes                             *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 18 de noviembre de 2005, 09:45 AM              *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;

public class RemesaSinDoc {
    
    private String numrem;
    private String fecharemesa;
    private String cliente;
    private String origen;
    private String destino;
    private double cantidad;
    private String unidad;
    private String agencia;
    private String fechacreacion;
    private String usuariocreacion;
    /** Creates a new instance of RemesaSinDoc */
    public RemesaSinDoc() {
    }
    
     /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo RemesaSinDoc
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static RemesaSinDoc load(ResultSet rs)throws Exception{
        RemesaSinDoc datos = new RemesaSinDoc();
        datos.setNumrem(rs.getString("NUMREM"));
        datos.setFecharemesa(rs.getString("FECREM"));
        datos.setCliente(rs.getString("CLIENTE"));
        datos.setOrigen(rs.getString("ORIGEN"));
        datos.setDestino(rs.getString("DESTINO"));
        datos.setCantidad(rs.getDouble("PESO"));
        datos.setUnidad(rs.getString("UNIDAD"));
        datos.setAgencia(rs.getString("AGENCIA"));
        datos.setFechacreacion(rs.getString("CREATION_DATE"));
        datos.setUsuariocreacion(rs.getString("USUARIO"));
        return datos;
    }
    
    /**
     * Metodo getAgencia, retorna el valor del atributo agencia,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
     /**
     * Metodo setAgencia, setea el valor del atributo agencia,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String agencia
     * @version : 1.0
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
     /**
     * Metodo getCantidad, retorna el valor del atributo cantidad,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public double getCantidad() {
        return cantidad;
    }
    
   /**
     * Metodo setCantidad, setea el valor del atributo cantidad,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : double cantidad
     * @version : 1.0
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
    
     /**
     * Metodo getCliente, retorna el valor del atributo cliente,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Metodo setCliente, setea el valor del atributo cliente,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String cliente
     * @version : 1.0
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
     /**
     * Metodo getDestino, retorna el valor del atributo destino,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Metodo setDestino, setea el valor del atributo destino,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String destino
     * @version : 1.0
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
     /**
     * Metodo getFechacreacion, retorna el valor del atributo fechacreacion
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getFechacreacion() {
        return fechacreacion;
    }
    
    /**
     * Metodo setFechacreacion, setea el valor del atributo fechacreacion,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String fechacreacion
     * @version : 1.0
     */
    public void setFechacreacion(java.lang.String fechacreacion) {
        this.fechacreacion = fechacreacion;
    }
    
    /**
     * Metodo getFecharemesa, retorna el valor del atributo fecharemesa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getFecharemesa() {
        return fecharemesa;
    }
    
    /**
     * Metodo setFecharemesa, setea el valor del atributo fecharemesa,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String fecharemesa
     * @version : 1.0
     */
    public void setFecharemesa(java.lang.String fecharemesa) {
        this.fecharemesa = fecharemesa;
    }
    
    /**
     * Metodo getNumrem, retorna el valor del atributo numrem
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Metodo setNumrem, setea el valor del atributo numrem,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Metodo getOrigen, retorna el valor del atributo origen
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Metodo setOrigen, setea el valor del atributo origen,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String origen
     * @version : 1.0
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Metodo getUnidad, retorna el valor del atributo unidad
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Metodo setUnidad, setea el valor del atributo unidad,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String unidad
     * @version : 1.0
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Metodo getUsuariocreacion, retorna el valor del atributo usuariocreacion
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getUsuariocreacion() {
        return usuariocreacion;
    }
    
    /**
     * Metodo setUsuario, setea el valor del atributo usuariocreacion,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String usuariocreacion
     * @version : 1.0
     */
    public void setUsuariocreacion(java.lang.String usuariocreacion) {
        this.usuariocreacion = usuariocreacion;
    }
    
}
