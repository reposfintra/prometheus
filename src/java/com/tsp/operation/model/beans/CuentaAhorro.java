package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * <br/>
 * 24/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class CuentaAhorro {

    private String estado;
    private String entidad;
    private Timestamp ultimaActualizacion;
    private String numeroCuenta;
    private Timestamp fechaApertura;
    private String oficina;
    private String ciudad;
    private Boolean bloqueada;
    private Short situacionTitular;
    private String tipoIdentificacion;
    private String identificacion;
    private String codSuscriptor;
    private String creationUser;
    private String userUpdate;
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of codSuscriptor
     *
     * @return the value of codSuscriptor
     */
    public String getCodSuscriptor() {
        return codSuscriptor;
    }

    /**
     * Set the value of codSuscriptor
     *
     * @param codSuscriptor new value of codSuscriptor
     */
    public void setCodSuscriptor(String codSuscriptor) {
        this.codSuscriptor = codSuscriptor;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of situacionTitular
     *
     * @return the value of situacionTitular
     */
    public Short getSituacionTitular() {
        return situacionTitular;
    }

    /**
     * Set the value of situacionTitular
     *
     * @param situacionTitular new value of situacionTitular
     */
    public void setSituacionTitular(short situacionTitular) {
        this.situacionTitular = situacionTitular;
    }


    /**
     * Get the value of bloqueada
     *
     * @return the value of bloqueada
     */
    public Boolean getBloqueada() {
        return bloqueada;
    }

    /**
     * Set the value of bloqueada
     *
     * @param bloqueada new value of bloqueada
     */
    public void setBloqueada(boolean bloqueada) {
        this.bloqueada = bloqueada;
    }

    /**
     * Get the value of ciudad
     *
     * @return the value of ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Set the value of ciudad
     *
     * @param ciudad new value of ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }


    /**
     * Get the value of oficina
     *
     * @return the value of oficina
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * Set the value of oficina
     *
     * @param oficina new value of oficina
     */
    public void setOficina(String oficina) {
        this.oficina = oficina;
    }


    /**
     * Get the value of fechaApertura
     *
     * @return the value of fechaApertura
     */
    public Timestamp getFechaApertura() {
        return fechaApertura;
    }

    /**
     * Set the value of fechaApertura
     *
     * @param fechaApertura new value of fechaApertura
     */
    public void setFechaApertura(Timestamp fechaApertura) {
        this.fechaApertura = fechaApertura;
    }


    /**
     * Get the value of numeroCuenta
     *
     * @return the value of numeroCuenta
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Set the value of numeroCuenta
     *
     * @param numeroCuenta new value of numeroCuenta
     */
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }


    /**
     * Get the value of ultimaActualizacion
     *
     * @return the value of ultimaActualizacion
     */
    public Timestamp getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    /**
     * Set the value of ultimaActualizacion
     *
     * @param ultimaActualizacion new value of ultimaActualizacion
     */
    public void setUltimaActualizacion(Timestamp ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    /**
     * Get the value of entidad
     *
     * @return the value of entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Set the value of entidad
     *
     * @param entidad new value of entidad
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }


    /**
     * Get the value of estado
     *
     * @return the value of estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Set the value of estado
     *
     * @param estado new value of estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }


}
