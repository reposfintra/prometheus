
package com.tsp.operation.model.beans;


public class ValorAnticipo {
    
    private  double vlr;
    private  double vlr_for;
    private  String currency;
    
    private String proveedor;
    private String sucursal;
    
    
    
    public ValorAnticipo() {
        vlr      =0;
        vlr_for  =0;
        currency ="";
        proveedor="";
        sucursal ="";
    }
    
    
    //SET
    public void setVlr(double valor){
        this.vlr = valor;
    }
    
    public void setVlrFor(double valor){
        this.vlr_for = valor;
    }
    
    public void setCurrency(String moneda){
        this.currency = moneda;
    }
    
    
    public void setProveedor(String provee){
        this.proveedor = provee;
    }
    
    public void setSucursal(String sucur){
        this.sucursal = sucur;
    }
    
    
    
    
    
   //GET
    
    public double getVlr(){
        return this.vlr;
    }
    
    public double getVlrFor(){
        return this.vlr_for;
    }
    
    public String getCurrency(){
        return this.currency;
    }
    
    public String getProveedor(){
        return this.proveedor;
    }
    
    public String getSucursal(){
        return this.sucursal;
    }
    
    
}
