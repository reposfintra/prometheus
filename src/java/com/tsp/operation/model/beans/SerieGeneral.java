/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class SerieGeneral {
    
    private String prefijo;
    private int    ultimo_numero_entero;
    private String ultimo_numero_cadena;
    private String ultimo_prefijo_numero;



    /** Creates a new instance of Prefactura */
    public SerieGeneral() {
    }


    public static SerieGeneral load(java.sql.ResultSet rs)throws java.sql.SQLException{

        SerieGeneral serieGeneral = new SerieGeneral();

        serieGeneral.setPrefijo( rs.getString("prefijo") );
        serieGeneral.setUltimo_numero_entero(rs.getInt("ultimo_numero_entero") );
        serieGeneral.setUltimo_numero_cadena( rs.getString("ultimo_numero_cadena") );
        serieGeneral.setUltimo_prefijo_numero( rs.getString("ultimo_prefijo_numero") );

        return serieGeneral;
    }



    /**
     * @return the prefijo
     */
    public String getPrefijo() {
        return prefijo;
    }

    /**
     * @param prefijo the prefijo to set
     */
    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    /**
     * @return the ultimo_numero_entero
     */
    public int getUltimo_numero_entero() {
        return ultimo_numero_entero;
    }

    /**
     * @param ultimo_numero_entero the ultimo_numero_entero to set
     */
    public void setUltimo_numero_entero(int ultimo_numero_entero) {
        this.ultimo_numero_entero = ultimo_numero_entero;
    }

    /**
     * @return the ultimo_numero_cadena
     */
    public String getUltimo_numero_cadena() {
        return ultimo_numero_cadena;
    }

    /**
     * @param ultimo_numero_cadena the ultimo_numero_cadena to set
     */
    public void setUltimo_numero_cadena(String ultimo_numero_cadena) {
        this.ultimo_numero_cadena = ultimo_numero_cadena;
    }

    /**
     * @return the ultimo_prefijo_numero
     */
    public String getUltimo_prefijo_numero() {
        return ultimo_prefijo_numero;
    }

    /**
     * @param ultimo_prefijo_numero the ultimo_prefijo_numero to set
     */
    public void setUltimo_prefijo_numero(String ultimo_prefijo_numero) {
        this.ultimo_prefijo_numero = ultimo_prefijo_numero;
    }

}
