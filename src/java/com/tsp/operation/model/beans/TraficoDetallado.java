/*
 * TraficoDetallado.java
 *
 * Created on 11 de julio de 2005, 07:01 PM
 */
package com.tsp.operation.model.beans;
import java.io.*;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  amendez
 *Bean usado en el reporte de trafico detallado
 */
public class TraficoDetallado implements Serializable{
    private String numpla;
    private String placa;
    private String oripla;
    private String despla;
    private String estado;
    private List traficoList;
    private List traficoObList;
    
    /** Creates a new instance of TraficoDetallado */
    public TraficoDetallado() {
        
    }
    
    /**
     *Constructor de trafico detallado
     *@param rs objeto de tipo ResultSet a partir del cual se contruye el objeto de
     *trafico detallado.
     */
    public static TraficoDetallado load(ResultSet rs) throws SQLException{
        String value = null;
        TraficoDetallado trafico = new TraficoDetallado();
        
        value = rs.getString(1);
        if(value != null) trafico.setNumpla(value);
        
        value = rs.getString(2);
        if(value != null) trafico.setPlaca(value);
        
        value = rs.getString(3);
        if(value != null) trafico.setOripla(value);
        
        value = rs.getString(4);
        if(value != null) trafico.setDespla(value);
        
        value = rs.getString(5);
        if(value != null){
            if ( value.endsWith("-E") ) {
                trafico.setEstado( "Con Entrega" ) ;
            }else if (value.trim().equals("") || value.trim().equals("-")) {
                trafico.setEstado( "Por Confirmar Salida" ) ;
            }else {
                trafico.setEstado( "En Ruta"  );
            }
            
        }
        
        return trafico;
    }
    
    /**
     * Getter for property despla.
     * @return Value of property despla.
     */
    public java.lang.String getDespla() {
        return despla;
    }
    
    /**
     * Setter for property despla.
     * @param despla New value of property despla.
     */
    public void setDespla(java.lang.String despla) {
        this.despla = despla;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property oripla.
     * @return Value of property oripla.
     */
    public java.lang.String getOripla() {
        return oripla;
    }
    
    /**
     * Setter for property oripla.
     * @param oripla New value of property oripla.
     */
    public void setOripla(java.lang.String oripla) {
        this.oripla = oripla;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property traficoList.
     * @return Value of property traficoList.
     */
    public java.util.List getTraficoList() {
        return traficoList;
    }
    
    /**
     * Setter for property traficoList.
     * @param traficoList New value of property traficoList.
     */
    public void setTraficoList(java.util.List traficoList) {
        this.traficoList = traficoList;
    }
    
    /**
     * Getter for property traficoObList.
     * @return Value of property traficoObList.
     */
    public java.util.List getTraficoObList() {
        return traficoObList;
    }
    
    /**
     * Setter for property traficoObList.
     * @param traficoObList New value of property traficoObList.
     */
    public void setTraficoObList(java.util.List traficoObList) {
        this.traficoObList = traficoObList;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
}
