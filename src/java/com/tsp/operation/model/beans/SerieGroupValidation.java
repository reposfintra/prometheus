/******************************************************************
* Nombre ......................SerieGroupValidation.java
* Descripci�n..................Bean utilizado para la validaci�n de series
* Autor........................Armando Oviedo
* Fecha........................04/11/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;


import java.sql.*;
/**
 *
 * @author  Armando
 */
public class SerieGroupValidation {
    
    private String initialSerialNo;
    private String finalSerialNo;
    private int lastNumber; 
    private int restantes;
    
    /** Creates a new instance of SerieGroupValidation */
    public SerieGroupValidation() {
    }
        
    public static SerieGroupValidation load(ResultSet rs) throws SQLException{
        SerieGroupValidation tmp = new SerieGroupValidation();
        try{
            tmp = new SerieGroupValidation();
            tmp.setInitialSerialNo(rs.getString("serial_initial_no"));
            tmp.setFinalSerialNo(rs.getString("serial_fished_no"));
            tmp.setLastNumber(rs.getInt("last_number"));
        }
        catch(SQLException ex){
            throw new SQLException("Error en el m�todo load. [SerieGroupValidation] "+ex.getMessage());
        }
        return tmp;
    }    
    public void setInitialSerialNo(String initialSerialNo){
        this.initialSerialNo = initialSerialNo;
    }
    public void setFinalSerialNo(String finalSerialNo){
        this.finalSerialNo = finalSerialNo;
    }
    public void setLastNumber(int lastNumber){
        this.lastNumber = lastNumber;
    }    
    public void setRestantes(int restantes){
        this.restantes = restantes;
    }
    public String getInitialSerialNo(){
        return this.initialSerialNo;
    }
    public String getFinalSerialNo(){
        return this.finalSerialNo;
    }
    public int getLastNumber(){
        return this.lastNumber;
    }
    public int getRestantes(){
        return this.restantes;
    }
}
