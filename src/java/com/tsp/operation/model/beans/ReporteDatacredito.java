/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author lcanchila
 */
public class ReporteDatacredito {
   
    private String hr_periodo;
    private String hr_tipo_id;
    private String hr_identificacion;
    private String hr_nombre;
    private String hr_negocio;
    private String hr_fecha_apertura;
    private String hr_fecha_ven;
    private String hr_novedad;
    private String hr_saldo_mora;
    private String periodo;
    private String tipo_identificacion;
    private String identificacion;
    private String nombre;
    private String negocio;
    private String fecha_apertura;
    private String fecha_vencimiento;
    private String novedad;
    private String saldo_mora;
    private String und_negocio;
    private String nit;
    private String situacion_titular;
    private String fecha_corte_proceso;
    private int dias_mora;
    private int min_dias_mora;
    private String desembolso;
    private double saldo_deuda;
    private double saldo_en_mora;
    private String cuota_mensual;
    private String numero_cuotas;
    private int cuotas_canceladas;
    private int cuotas_mora;
    private String fecha_limite_pago;
    private String ultimo_pago;
    private String ciudad_radicacion;
    private String cod_dane_radicacion;
    private String ciudad_residencia;
    private String departamento_residencia;
    private String cod_dane_residencia;
    private String direccion_residencia;
    private String telefono_residencia;
    private String ciudad_laboral;
    private String cod_dane_laboral;
    private String departamento_laboral;
    private String direccion_laboral;
    private String telefono_laboral;
    private String ciudad_correspondencia;
    private String cod_dane_correspondencia;
    private String direccion_correspondencia;
    private String correo_electronico;
    private String celular_solicitante;
    private String tipo;
    private int id;

    /**
     * @return the hr_periodo
     */
    public String getHr_periodo() {
        return hr_periodo;
    }

    /**
     * @param hr_periodo the hr_periodo to set
     */
    public void setHr_periodo(String hr_periodo) {
        this.hr_periodo = hr_periodo;
    }

    /**
     * @return the hr_tipo_id
     */
    public String getHr_tipo_id() {
        return hr_tipo_id;
    }

    /**
     * @param hr_tipo_id the hr_tipo_id to set
     */
    public void setHr_tipo_id(String hr_tipo_id) {
        this.hr_tipo_id = hr_tipo_id;
    }

    /**
     * @return the hr_identificacion
     */
    public String getHr_identificacion() {
        return hr_identificacion;
    }

    /**
     * @param hr_identificacion the hr_identificacion to set
     */
    public void setHr_identificacion(String hr_identificacion) {
        this.hr_identificacion = hr_identificacion;
    }

    /**
     * @return the hr_nombre
     */
    public String getHr_nombre() {
        return hr_nombre;
    }

    /**
     * @param hr_nombre the hr_nombre to set
     */
    public void setHr_nombre(String hr_nombre) {
        this.hr_nombre = hr_nombre;
    }

    /**
     * @return the hr_negocio
     */
    public String getHr_negocio() {
        return hr_negocio;
    }

    /**
     * @param hr_negocio the hr_negocio to set
     */
    public void setHr_negocio(String hr_negocio) {
        this.hr_negocio = hr_negocio;
    }

    /**
     * @return the hr_fecha_apertura
     */
    public String getHr_fecha_apertura() {
        return hr_fecha_apertura;
    }

    /**
     * @param hr_fecha_apertura the hr_fecha_apertura to set
     */
    public void setHr_fecha_apertura(String hr_fecha_apertura) {
        this.hr_fecha_apertura = hr_fecha_apertura;
    }

    /**
     * @return the hr_fecha_ven
     */
    public String getHr_fecha_ven() {
        return hr_fecha_ven;
    }

    /**
     * @param hr_fecha_ven the hr_fecha_ven to set
     */
    public void setHr_fecha_ven(String hr_fecha_ven) {
        this.hr_fecha_ven = hr_fecha_ven;
    }

    /**
     * @return the hr_novedad
     */
    public String getHr_novedad() {
        return hr_novedad;
    }

    /**
     * @param hr_novedad the hr_novedad to set
     */
    public void setHr_novedad(String hr_novedad) {
        this.hr_novedad = hr_novedad;
    }

    /**
     * @return the hr_saldo_mora
     */
    public String getHr_saldo_mora() {
        return hr_saldo_mora;
    }

    /**
     * @param hr_saldo_mora the hr_saldo_mora to set
     */
    public void setHr_saldo_mora(String hr_saldo_mora) {
        this.hr_saldo_mora = hr_saldo_mora;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the tipo_identificacion
     */
    public String getTipo_identificacion() {
        return tipo_identificacion;
    }

    /**
     * @param tipo_identificacion the tipo_identificacion to set
     */
    public void setTipo_identificacion(String tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    /**
     * @return the fecha_apertura
     */
    public String getFecha_apertura() {
        return fecha_apertura;
    }

    /**
     * @param fecha_apertura the fecha_apertura to set
     */
    public void setFecha_apertura(String fecha_apertura) {
        this.fecha_apertura = fecha_apertura;
    }

    /**
     * @return the fecha_vencimiento
     */
    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * @param fecha_vencimiento the fecha_vencimiento to set
     */
    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * @return the novedad
     */
    public String getNovedad() {
        return novedad;
    }

    /**
     * @param novedad the novedad to set
     */
    public void setNovedad(String novedad) {
        this.novedad = novedad;
    }

    /**
     * @return the saldo_mora
     */
    public String getSaldo_mora() {
        return saldo_mora;
    }

    /**
     * @param saldo_mora the saldo_mora to set
     */
    public void setSaldo_mora(String saldo_mora) {
        this.saldo_mora = saldo_mora;
    }

    /**
     * @return the und_negocio
     */
    public String getUnd_negocio() {
        return und_negocio;
    }

    /**
     * @param und_negocio the und_negocio to set
     */
    public void setUnd_negocio(String und_negocio) {
        this.und_negocio = und_negocio;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the situacion_titular
     */
    public String getSituacion_titular() {
        return situacion_titular;
    }

    /**
     * @param situacion_titular the situacion_titular to set
     */
    public void setSituacion_titular(String situacion_titular) {
        this.situacion_titular = situacion_titular;
    }

    /**
     * @return the fecha_corte_proceso
     */
    public String getFecha_corte_proceso() {
        return fecha_corte_proceso;
    }

    /**
     * @param fecha_corte_proceso the fecha_corte_proceso to set
     */
    public void setFecha_corte_proceso(String fecha_corte_proceso) {
        this.fecha_corte_proceso = fecha_corte_proceso;
    }

    /**
     * @return the dias_mora
     */
    public int getDias_mora() {
        return dias_mora;
    }

    /**
     * @param dias_mora the dias_mora to set
     */
    public void setDias_mora(int dias_mora) {
        this.dias_mora = dias_mora;
    }

    /**
     * @return the min_dias_mora
     */
    public int getMin_dias_mora() {
        return min_dias_mora;
    }

    /**
     * @param min_dias_mora the min_dias_mora to set
     */
    public void setMin_dias_mora(int min_dias_mora) {
        this.min_dias_mora = min_dias_mora;
    }

    /**
     * @return the desembolso
     */
    public String getDesembolso() {
        return desembolso;
    }

    /**
     * @param desembolso the desembolso to set
     */
    public void setDesembolso(String desembolso) {
        this.desembolso = desembolso;
    }

    /**
     * @return the saldo_deuda
     */
    public double getSaldo_deuda() {
        return saldo_deuda;
    }

    /**
     * @param saldo_deuda the saldo_deuda to set
     */
    public void setSaldo_deuda(double saldo_deuda) {
        this.saldo_deuda = saldo_deuda;
    }

    /**
     * @return the saldo_en_mora
     */
    public double getSaldo_en_mora() {
        return saldo_en_mora;
    }

    /**
     * @param saldo_en_mora the saldo_en_mora to set
     */
    public void setSaldo_en_mora(double saldo_en_mora) {
        this.saldo_en_mora = saldo_en_mora;
    }

    /**
     * @return the cuota_mensual
     */
    public String getCuota_mensual() {
        return cuota_mensual;
    }

    /**
     * @param cuota_mensual the cuota_mensual to set
     */
    public void setCuota_mensual(String cuota_mensual) {
        this.cuota_mensual = cuota_mensual;
    }

    /**
     * @return the numero_cuotas
     */
    public String getNumero_cuotas() {
        return numero_cuotas;
    }

    /**
     * @param numero_cuotas the numero_cuotas to set
     */
    public void setNumero_cuotas(String numero_cuotas) {
        this.numero_cuotas = numero_cuotas;
    }

    /**
     * @return the cuotas_canceladas
     */
    public int getCuotas_canceladas() {
        return cuotas_canceladas;
    }

    /**
     * @param cuotas_canceladas the cuotas_canceladas to set
     */
    public void setCuotas_canceladas(int cuotas_canceladas) {
        this.cuotas_canceladas = cuotas_canceladas;
    }

    /**
     * @return the cuotas_mora
     */
    public int getCuotas_mora() {
        return cuotas_mora;
    }

    /**
     * @param cuotas_mora the cuotas_mora to set
     */
    public void setCuotas_mora(int cuotas_mora) {
        this.cuotas_mora = cuotas_mora;
    }

    /**
     * @return the fecha_limite_pago
     */
    public String getFecha_limite_pago() {
        return fecha_limite_pago;
    }

    /**
     * @param fecha_limite_pago the fecha_limite_pago to set
     */
    public void setFecha_limite_pago(String fecha_limite_pago) {
        this.fecha_limite_pago = fecha_limite_pago;
    }

    /**
     * @return the ultimo_pago
     */
    public String getUltimo_pago() {
        return ultimo_pago;
    }

    /**
     * @param ultimo_pago the ultimo_pago to set
     */
    public void setUltimo_pago(String ultimo_pago) {
        this.ultimo_pago = ultimo_pago;
    }

    /**
     * @return the ciudad_radicacion
     */
    public String getCiudad_radicacion() {
        return ciudad_radicacion;
    }

    /**
     * @param ciudad_radicacion the ciudad_radicacion to set
     */
    public void setCiudad_radicacion(String ciudad_radicacion) {
        this.ciudad_radicacion = ciudad_radicacion;
    }

    /**
     * @return the cod_dane_radicacion
     */
    public String getCod_dane_radicacion() {
        return cod_dane_radicacion;
    }

    /**
     * @param cod_dane_radicacion the cod_dane_radicacion to set
     */
    public void setCod_dane_radicacion(String cod_dane_radicacion) {
        this.cod_dane_radicacion = cod_dane_radicacion;
    }

    /**
     * @return the ciudad_residencia
     */
    public String getCiudad_residencia() {
        return ciudad_residencia;
    }

    /**
     * @param ciudad_residencia the ciudad_residencia to set
     */
    public void setCiudad_residencia(String ciudad_residencia) {
        this.ciudad_residencia = ciudad_residencia;
    }

    /**
     * @return the departamento_residencia
     */
    public String getDepartamento_residencia() {
        return departamento_residencia;
    }

    /**
     * @param departamento_residencia the departamento_residencia to set
     */
    public void setDepartamento_residencia(String departamento_residencia) {
        this.departamento_residencia = departamento_residencia;
    }

    /**
     * @return the cod_dane_residencia
     */
    public String getCod_dane_residencia() {
        return cod_dane_residencia;
    }

    /**
     * @param cod_dane_residencia the cod_dane_residencia to set
     */
    public void setCod_dane_residencia(String cod_dane_residencia) {
        this.cod_dane_residencia = cod_dane_residencia;
    }

    /**
     * @return the direccion_residencia
     */
    public String getDireccion_residencia() {
        return direccion_residencia;
    }

    /**
     * @param direccion_residencia the direccion_residencia to set
     */
    public void setDireccion_residencia(String direccion_residencia) {
        this.direccion_residencia = direccion_residencia;
    }

    /**
     * @return the telefono_residencia
     */
    public String getTelefono_residencia() {
        return telefono_residencia;
    }

    /**
     * @param telefono_residencia the telefono_residencia to set
     */
    public void setTelefono_residencia(String telefono_residencia) {
        this.telefono_residencia = telefono_residencia;
    }

    /**
     * @return the ciudad_laboral
     */
    public String getCiudad_laboral() {
        return ciudad_laboral;
    }

    /**
     * @param ciudad_laboral the ciudad_laboral to set
     */
    public void setCiudad_laboral(String ciudad_laboral) {
        this.ciudad_laboral = ciudad_laboral;
    }

    /**
     * @return the cod_dane_laboral
     */
    public String getCod_dane_laboral() {
        return cod_dane_laboral;
    }

    /**
     * @param cod_dane_laboral the cod_dane_laboral to set
     */
    public void setCod_dane_laboral(String cod_dane_laboral) {
        this.cod_dane_laboral = cod_dane_laboral;
    }

    /**
     * @return the departamento_laboral
     */
    public String getDepartamento_laboral() {
        return departamento_laboral;
    }

    /**
     * @param departamento_laboral the departamento_laboral to set
     */
    public void setDepartamento_laboral(String departamento_laboral) {
        this.departamento_laboral = departamento_laboral;
    }

    /**
     * @return the direccion_laboral
     */
    public String getDireccion_laboral() {
        return direccion_laboral;
    }

    /**
     * @param direccion_laboral the direccion_laboral to set
     */
    public void setDireccion_laboral(String direccion_laboral) {
        this.direccion_laboral = direccion_laboral;
    }

    /**
     * @return the telefono_laboral
     */
    public String getTelefono_laboral() {
        return telefono_laboral;
    }

    /**
     * @param telefono_laboral the telefono_laboral to set
     */
    public void setTelefono_laboral(String telefono_laboral) {
        this.telefono_laboral = telefono_laboral;
    }

    /**
     * @return the ciudad_correspondencia
     */
    public String getCiudad_correspondencia() {
        return ciudad_correspondencia;
    }

    /**
     * @param ciudad_correspondencia the ciudad_correspondencia to set
     */
    public void setCiudad_correspondencia(String ciudad_correspondencia) {
        this.ciudad_correspondencia = ciudad_correspondencia;
    }

    /**
     * @return the cod_dane_correspondencia
     */
    public String getCod_dane_correspondencia() {
        return cod_dane_correspondencia;
    }

    /**
     * @param cod_dane_correspondencia the cod_dane_correspondencia to set
     */
    public void setCod_dane_correspondencia(String cod_dane_correspondencia) {
        this.cod_dane_correspondencia = cod_dane_correspondencia;
    }

    /**
     * @return the direccion_correspondencia
     */
    public String getDireccion_correspondencia() {
        return direccion_correspondencia;
    }

    /**
     * @param direccion_correspondencia the direccion_correspondencia to set
     */
    public void setDireccion_correspondencia(String direccion_correspondencia) {
        this.direccion_correspondencia = direccion_correspondencia;
    }

    /**
     * @return the correo_electronico
     */
    public String getCorreo_electronico() {
        return correo_electronico;
    }

    /**
     * @param correo_electronico the correo_electronico to set
     */
    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    /**
     * @return the celular_solicitante
     */
    public String getCelular_solicitante() {
        return celular_solicitante;
    }

    /**
     * @param celular_solicitante the celular_solicitante to set
     */
    public void setCelular_solicitante(String celular_solicitante) {
        this.celular_solicitante = celular_solicitante;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
}
