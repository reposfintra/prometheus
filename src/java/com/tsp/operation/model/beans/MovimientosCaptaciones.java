/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author jpinedo
 */
public class MovimientosCaptaciones implements Cloneable {

    private String nit;
    private String no_transaccion;
    private int id=0;


    private int subcuenta;
    private String dstrct;
    private String creation_user;
    private String user_update;
    private String fecha;
 

    private String banco;
    private String sucursal;
    private double tasa_ea=0;
    private double valor_retefuente=0;
    private double valor_reteica=0;
    private double valor_intereses=0;
    private double subtotal=0;
    private double saldo_inicial=0;
    private double saldo_final=0;
    private double consignacion=0;
    private double retiro=0;
    private String tipo_movimiento;
    private String estado;
    private double base_intereses=0;//aplica para inversionistas con liquidacion con tipo de interes simple
    private double intereses_acomulados=0;//aplica para inversionistas con liquidacion con tipo de interes simple
    private String cuenta;
    private String titular_cuenta;
    private String nit_cuenta;
    private String tipo_cuenta;
    private String nombre_beneficiario;
    private String nit_beneficiario;
    private String cheque_cruzado;
    private String cheque_primer_beneficiario;
    private String nombre_beneficiario_captacion;
    private String tipo_transferencia;
    private String nombre_subcuenta;
    private String nombre_inversionista;
    private String usuario_confirmacion;
    private String fecha_confirmacion;
    private String fecha_causacion;
    
    private Inversionista inversionista = new Inversionista();
    
    ///solo para informe mensual
      private double total=0;


      private double porcentaje_total=0;
      private double promedio_ponderado=0;



    private String concepto_transaccion;

    private String lote_transferencia="";

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


    public MovimientosCaptaciones load(ResultSet rs) throws SQLException {
        MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
        movimiento.setNit(rs.getString("nit"));
        movimiento.setSubcuenta(rs.getInt("subcuenta"));
        movimiento.setFecha(rs.getString("fecha"));
        movimiento.setSaldo_inicial(rs.getDouble("saldo_inicial"));
        movimiento.setValor_intereses(rs.getDouble("intereses"));
        movimiento.setValor_retefuente(rs.getDouble("retefuente"));
        movimiento.setValor_reteica(rs.getDouble("reteica"));
        movimiento.setSubtotal(rs.getDouble("subtotal"));
        movimiento.setSaldo_final(rs.getDouble("saldo_final"));
        movimiento.setConsignacion(rs.getDouble("consignacion"));
        movimiento.setRetiro(rs.getDouble("retiro"));
        movimiento.setDstrct(rs.getString("dstrct"));
        movimiento.setUser_update(rs.getString("user_update"));
        movimiento.setNo_transaccion(rs.getString("no_transaccion"));
        movimiento.setBase_intereses(rs.getDouble("base_intereses"));
        movimiento.setIntereses_acomulados(rs.getDouble("intereses_acomulados"));
        movimiento.setCuenta(rs.getString("cuenta"));
        movimiento.setTitular_cuenta(rs.getString("titular_cuenta"));
        movimiento.setNit_cuenta(rs.getString("nit_cuenta"));
        movimiento.setTipo_cuenta(rs.getString("tipo_cuenta"));
        movimiento.setNit_beneficiario(rs.getString("nit_beneficiario"));
        movimiento.setNombre_beneficiario(rs.getString("nombre_beneficiario"));
        movimiento.setCheque_cruzado(rs.getString("cheque_cruzado"));
        movimiento.setCheque_primer_beneficiario(rs.getString("cheque_primer_beneficiario"));
        movimiento.setTipo_transferencia(rs.getString("tipo_transferencia"));
        movimiento.setNombre_beneficiario_captacion(rs.getString("nombre_beneficiario_cap"));
       movimiento.setBanco(rs.getString("banco"));
       movimiento.setTasa_ea(rs.getDouble("tasa_ea"));
       
       movimiento.setTipo_movimiento(rs.getString("tipo_movimiento"));
       
       try {
           movimiento.setConcepto_transaccion(rs.getString("concepto_transaccion"));
       } catch(Exception e) {
           movimiento.setConcepto_transaccion("");
       }

        return movimiento;
    }

        public String getFecha_confirmacion() {
        return fecha_confirmacion;
    }

    public void setFecha_confirmacion(String fecha_confirmacion) {
        this.fecha_confirmacion = fecha_confirmacion;
    }

    public String getCheque_primer_beneficiario() {
        return cheque_primer_beneficiario;
    }

    public void setCheque_primer_beneficiario(String cheque_primer_beneficiario) {
        this.cheque_primer_beneficiario = cheque_primer_beneficiario;
    }

    public double getBase_intereses() {
        return base_intereses;
    }

    public void setBase_intereses(double base_intereses) {
        this.base_intereses = base_intereses;
    }

        public double getPorcentaje_total() {
        return porcentaje_total;
    }

    public void setPorcentaje_total(double porcentaje_total) {
        this.porcentaje_total = porcentaje_total;
    }

    public double getPromedio_ponderado() {
        return promedio_ponderado;
    }

    public void setPromedio_ponderado(double promedio_ponderado) {
        this.promedio_ponderado = promedio_ponderado;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getIntereses_acomulados() {
        return intereses_acomulados;
    }
        public String getNombre_subcuenta() {
        return nombre_subcuenta;
    }

    public void setNombre_subcuenta(String nombre_subcuenta) {
        this.nombre_subcuenta = nombre_subcuenta;
    }

    public void setIntereses_acomulados(double intereses_acomulados) {
        this.intereses_acomulados = intereses_acomulados;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public double getConsignacion() {
        return consignacion;
    }

    public void setConsignacion(double consignacion) {
        this.consignacion = consignacion;
    }


    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public double getRetiro() {
        return retiro;
    }

    public void setRetiro(double retiro) {
        this.retiro = retiro;
    }

    public double getSaldo_final() {
        return saldo_final;
    }

    public void setSaldo_final(double saldo_final) {
        this.saldo_final = saldo_final;
    }
    public String getNo_transaccion() {
        return no_transaccion;
    }

    public void setNo_transaccion(String no_transaccion) {
        this.no_transaccion = no_transaccion;
    }
    public double getSaldo_inicial() {
        return saldo_inicial;
    }

    public void setSaldo_inicial(double saldo_inicial) {
        this.saldo_inicial = saldo_inicial;
    }

    public int getSubcuenta() {
        return subcuenta;
    }

    public void setSubcuenta(int subcuenta) {
        this.subcuenta = subcuenta;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public double getTasa_ea() {
        return tasa_ea;
    }

    public void setTasa_ea(double tasa_ea) {
        this.tasa_ea = tasa_ea;
    }

    public String getTipo_movimiento() {
        return tipo_movimiento;
    }

    public void setTipo_movimiento(String tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public double getValor_intereses() {
        return valor_intereses;
    }

    public void setValor_intereses(double valor_intereses) {
        this.valor_intereses = valor_intereses;
    }

    public double getValor_retefuente() {
        return valor_retefuente;
    }

    public void setValor_retefuente(double valor_retefuente) {
        this.valor_retefuente = valor_retefuente;
    }

    public double getValor_reteica() {
        return valor_reteica;
    }

    public void setValor_reteica(double valor_reteica) {
        this.valor_reteica = valor_reteica;
    }


    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNit_beneficiario() {
        return nit_beneficiario;
    }

    public void setNit_beneficiario(String nit_beneficiario) {
        this.nit_beneficiario = nit_beneficiario;
    }

    public String getNit_cuenta() {
        return nit_cuenta;
    }

    public void setNit_cuenta(String nit_cuenta) {
        this.nit_cuenta = nit_cuenta;
    }

    public String getNombre_beneficiario() {
        return nombre_beneficiario;
    }

    public void setNombre_beneficiario(String nombre_beneficiario) {
        this.nombre_beneficiario = nombre_beneficiario;
    }

    public String getNombre_beneficiario_captacion() {
        return nombre_beneficiario_captacion;
    }

    public void setNombre_beneficiario_captacion(String nombre_beneficiario_captacion) {
        this.nombre_beneficiario_captacion = nombre_beneficiario_captacion;
    }



    public String getTipo_cuenta() {
        return tipo_cuenta;
    }

    public void setTipo_cuenta(String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    public String getTipo_transferencia() {
        return tipo_transferencia;
    }

    public void setTipo_transferencia(String tipo_transferencia) {
        this.tipo_transferencia = tipo_transferencia;
    }
        public String getTitular_cuenta() {
        return titular_cuenta;
    }

    public void setTitular_cuenta(String titular_cuenta) {
        this.titular_cuenta = titular_cuenta;
    }


    public String getCheque_cruzado() {
        return cheque_cruzado;
    }

    public void setCheque_cruzado(String cheque_cruzado) {
        this.cheque_cruzado = cheque_cruzado;
    }


    public String getNombre_inversionista() {
        return nombre_inversionista;
    }

    public void setNombre_inversionista(String nombre_inversionista) {
        this.nombre_inversionista = nombre_inversionista;
    }


    public String getUsuario_confirmacion() {
        return usuario_confirmacion;
    }

    public void setUsuario_confirmacion(String usuario_confirmacion) {
        this.usuario_confirmacion = usuario_confirmacion;
    }


        public String getFecha_causacion() {
        return fecha_causacion;
    }

    public void setFecha_causacion(String fecha_causacion) {
        this.fecha_causacion = fecha_causacion;
    }

    /** @author JPACOSTA
     * @return the concepto_transaccion
     */
    public String getConcepto_transaccion() {
        return concepto_transaccion;
}

    /** @author JPACOSTA
     * @param concepto_transaccion the concepto_transaccion to set
     */
    public void setConcepto_transaccion(String concepto_transaccion) {
        this.concepto_transaccion = (concepto_transaccion != null) ? concepto_transaccion : "";
    }

    /**
     * @return the inversionista
     */
    public Inversionista getInversionista() {
        return inversionista;
    }

    /**
     * @param inversionista the inversionista to set
     */
    public void setInversionista(Inversionista inversionista) {
        this.inversionista = inversionista;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public String getLote_transferencia() {
        return lote_transferencia;
    }

    public void setLote_transferencia(String lote_transferencia) {
        this.lote_transferencia = lote_transferencia;
    }
    
    

}
