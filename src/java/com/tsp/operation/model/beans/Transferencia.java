/***************************************
* Nombre Clase ............. Transferencia.java
* Descripci�n  .. . . . . .  Permite Encapsular los datos para realizar la transferencia o pago al propietario
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  31/03/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/



package com.tsp.operation.model.beans;

import  java.util.*;


public class Transferencia {
    
   private String distrito       = ""; 
   private String idMims         = "";
   private String nit            = "";
   private String nombre         = "";
   
   private String liquidacion    = "";
   
   private String banco          = "";
   private String sucursal       = "";   
   private String cuenta         = "";
   private String tipoCuenta     = "";
   private String descTipoCuenta = "";
   private String nombreCuenta   = "";
   private String cedulaCuenta   = "";
   
   private String  listaOC       = "";
   
   private String tipoPago       = "";   
   private double vlrLiquidacion;
   private double vlrDescuento;
   private double vlrNeto;
   private double vlrComisionBancaria;
   private double vlrConsignar;
   
   
   private String fecha         = "";
   private String user          = "";
   private String agencia       = "";
   
   private int    secuencia     = 1;
   
   private int    consecutivo  ;
   
   
   private String bancoPagoTransferencia;
   
   
   private List  listCTA;
   
   
    public Transferencia() {
         bancoPagoTransferencia = "";
         listCTA                = new LinkedList();
    }
    
    
    
    
    
    
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property cedulaCuenta.
     * @return Value of property cedulaCuenta.
     */
    public java.lang.String getCedulaCuenta() {
        return cedulaCuenta;
    }
    
    /**
     * Setter for property cedulaCuenta.
     * @param cedulaCuenta New value of property cedulaCuenta.
     */
    public void setCedulaCuenta(java.lang.String cedulaCuenta) {
        this.cedulaCuenta = cedulaCuenta;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property idMims.
     * @return Value of property idMims.
     */
    public java.lang.String getIdMims() {
        return idMims;
    }
    
    /**
     * Setter for property idMims.
     * @param idMims New value of property idMims.
     */
    public void setIdMims(java.lang.String idMims) {
        this.idMims = idMims;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property nombreCuenta.
     * @return Value of property nombreCuenta.
     */
    public java.lang.String getNombreCuenta() {
        return nombreCuenta;
    }
    
    /**
     * Setter for property nombreCuenta.
     * @param nombreCuenta New value of property nombreCuenta.
     */
    public void setNombreCuenta(java.lang.String nombreCuenta) {
        this.nombreCuenta = nombreCuenta;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property tipoCuenta.
     * @return Value of property tipoCuenta.
     */
    public java.lang.String getTipoCuenta() {
        return tipoCuenta;
    }
    
    /**
     * Setter for property tipoCuenta.
     * @param tipoCuenta New value of property tipoCuenta.
     */
    public void setTipoCuenta(java.lang.String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
    /**
     * Getter for property tipoPago.
     * @return Value of property tipoPago.
     */
    public java.lang.String getTipoPago() {
        return tipoPago;
    }
    
    /**
     * Setter for property tipoPago.
     * @param tipoPago New value of property tipoPago.
     */
    public void setTipoPago(java.lang.String tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    /**
     * Getter for property vlrComisionBancaria.
     * @return Value of property vlrComisionBancaria.
     */
    public double getVlrComisionBancaria() {
        return vlrComisionBancaria;
    }
    
    /**
     * Setter for property vlrComisionBancaria.
     * @param vlrComisionBancaria New value of property vlrComisionBancaria.
     */
    public void setVlrComisionBancaria(double vlrComisionBancaria) {
        this.vlrComisionBancaria = vlrComisionBancaria;
    }
    
    /**
     * Getter for property vlrConsignar.
     * @return Value of property vlrConsignar.
     */
    public double getVlrConsignar() {
        return vlrConsignar;
    }
    
    /**
     * Setter for property vlrConsignar.
     * @param vlrConsignar New value of property vlrConsignar.
     */
    public void setVlrConsignar(double vlrConsignar) {
        this.vlrConsignar = vlrConsignar;
    }
    
    /**
     * Getter for property vlrDescuento.
     * @return Value of property vlrDescuento.
     */
    public double getVlrDescuento() {
        return vlrDescuento;
    }
    
    /**
     * Setter for property vlrDescuento.
     * @param vlrDescuento New value of property vlrDescuento.
     */
    public void setVlrDescuento(double vlrDescuento) {
        this.vlrDescuento = vlrDescuento;
    }
    
    /**
     * Getter for property vlrLiquidacion.
     * @return Value of property vlrLiquidacion.
     */
    public double getVlrLiquidacion() {
        return vlrLiquidacion;
    }
    
    /**
     * Setter for property vlrLiquidacion.
     * @param vlrLiquidacion New value of property vlrLiquidacion.
     */
    public void setVlrLiquidacion(double vlrLiquidacion) {
        this.vlrLiquidacion = vlrLiquidacion;
    }
    
    /**
     * Getter for property vlrNeto.
     * @return Value of property vlrNeto.
     */
    public double getVlrNeto() {
        return vlrNeto;
    }
    
    /**
     * Setter for property vlrNeto.
     * @param vlrNeto New value of property vlrNeto.
     */
    public void setVlrNeto(double vlrNeto) {
        this.vlrNeto = vlrNeto;
    }
    
    /**
     * Getter for property liquidacion.
     * @return Value of property liquidacion.
     */
    public java.lang.String getLiquidacion() {
        return liquidacion;
    }
    
    /**
     * Setter for property liquidacion.
     * @param liquidacion New value of property liquidacion.
     */
    public void setLiquidacion(java.lang.String liquidacion) {
        this.liquidacion = liquidacion;
    }
    
    /**
     * Getter for property listaOC.
     * @return Value of property listaOC.
     */
    public String getListaOC() {
        return listaOC;
    }
    
    /**
     * Setter for property listaOC.
     * @param listaOC New value of property listaOC.
     */
    public void setListaOC(String listaOC) {
        this.listaOC = listaOC;
    }
    
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    
    
    /**
     * Getter for property descTipoCuenta.
     * @return Value of property descTipoCuenta.
     */
    public java.lang.String getDescTipoCuenta() {
        return descTipoCuenta;
    }
    
    /**
     * Setter for property descTipoCuenta.
     * @param descTipoCuenta New value of property descTipoCuenta.
     */
    public void setDescTipoCuenta(java.lang.String descTipoCuenta) {
        this.descTipoCuenta = descTipoCuenta;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property user.
     * @return Value of property user.
     */
    public java.lang.String getUser() {
        return user;
    }
    
    /**
     * Setter for property user.
     * @param user New value of property user.
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property bancoPagoTransferencia.
     * @return Value of property bancoPagoTransferencia.
     */
    public java.lang.String getBancoPagoTransferencia() {
        return bancoPagoTransferencia;
    }
    
    /**
     * Setter for property bancoPagoTransferencia.
     * @param bancoPagoTransferencia New value of property bancoPagoTransferencia.
     */
    public void setBancoPagoTransferencia(java.lang.String bancoPagoTransferencia) {
        this.bancoPagoTransferencia = bancoPagoTransferencia;
    }
    
    /**
     * Getter for property secuencia.
     * @return Value of property secuencia.
     */
    public int getSecuencia() {
        return secuencia;
    }    
    
    /**
     * Setter for property secuencia.
     * @param secuencia New value of property secuencia.
     */
    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }
    
    /**
     * Getter for property listCTA.
     * @return Value of property listCTA.
     */
    public List getListCTA() {
        return listCTA;
    }
    
    /**
     * Setter for property listCTA.
     * @param listCTA New value of property listCTA.
     */
    public void setListCTA(List listCTA) {
        this.listCTA = listCTA;
    }
    
    /**
     * Getter for property consecutivo.
     * @return Value of property consecutivo.
     */
    public int getConsecutivo() {
        return consecutivo;
    }
    
    /**
     * Setter for property consecutivo.
     * @param consecutivo New value of property consecutivo.
     */
    public void setConsecutivo(int consecutivo) {
        this.consecutivo = consecutivo;
    }
    
}
