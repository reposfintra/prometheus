/********************************************************************
 * Nombre Clase.................   CXP_Doc .java 200.21.200.2
 * Descripci�n..................   Bean de la tabla cxp_doc
 * Autor........................   Henry A. Osorio Gonzalez
 * Fecha........................   20.10.2005
 * Versi�n......................   1.0
 * Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  Henry
 */
public class CXP_Doc implements Serializable {

    private String reg_status;
    private String dstrct;
    private String proveedor;
    private String tipo_documento;
    private String documento;
    private String descripcion;
    private String agencia;
    private String handle_code;
    private String id_mims;
    private String fecha_documento;
    private String tipo_documento_rel;
    private String documento_relacionado;
    private String fecha_aprobacion;
    private String aprobador;
    private String usuario_aprobacion;
    private String fecha_vencimiento;
    private String ultima_fecha_pago;
    private String banco;
    private String sucursal;
    private String moneda;
    private double vlr_neto;
    private double vlr_total_abonos;
    private double vlr_saldo;
    private double vlr_neto_me;
    private double vlr_total_abonos_me;
    private double vlr_saldo_me;
    private double tasa;
    private String usuario_contabilizo;
    private String fecha_contabilizacion;
    private String usuario_anulo;
    private String fecha_anulacion;
    private String fecha_contabilizacion_anulacion;
    private String observacion;
    private double num_obs_autorizador;
    private double num_obs_pagador;
    private double num_obs_registra;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String base;
    private String numDias;
    boolean banderaRoja;
    boolean banderaVerde;
    boolean responder;
    private String nomProveedor;
    private String document_name;
    private boolean seleccionado;
    //sescalante (reporte facturas creadas)
    private String ucagencia;
    private int plazo;
    //Juan 02-05-2006
    private double vlr_total;
    //Ivan Gomez 15-05-2006
    private String maxfila;
    private String iva;
    private String riva;
    private String rica;
    private String rfte;
    //Lreales 15-06-2006
    private String corrida = "";
    private String cheque = "";
    private String clase_documento = "";
    private String moneda_banco;
    private double vlr_bruto_ml;
    private double vlr_bruto_me;
    //Osvaldo 06-07-2006
//      private String corrida;
//      private String cheque;
    private String periodo;
    private String fecha_contabilizacion_ajc;
    private String fecha_contabilizacion_ajv;
    private String periodo_ajc;
    private String periodo_ajv;
    private String usuario_contabilizo_ajc;
    private String usuario_contabilizo_ajv;
    private int transaccion_ajc;
    private int transaccion_ajv;
    private String fecha_procesado;
    private int transaccion;
    /***************************************/
    private int num_cuotas;
    private String fecha_inicio_transfer;
    private String frecuencia;
    private int num_cuotas_transfer;
    private String fecha_ultima_transfer;
    private double vlr_transfer_ml;
    private double vlr_transfer_me;
    private String tipo;
    //Andr�s Maturana
    private String fecha_inicio_transferO;
    private String beneficiario;
    private String numpla;
    private String prov_tipo_pago;
    private String prov_banco_transfer;
    private String prov_suc_transfer;
    private String prov_tipo_cuenta;
    private String prov_no_cuenta;
    private String prov_cedula_cuenta;
    private String prov_nombre_cuenta;
    //Ivan gomez 21 sep
    private double valor_saldo_anterior;
    private double valor_saldo_me_anterior;
    private String documento_rel_name;
    private String agenciaBanco;
    ///////////////////////////////
    //Ivan Gomez 21 nov
    private double totalIva;
    private double totalRiva;
    private double totalRica;
    private double totalRfte;
    private String documentos_relacionados;
    // mfontalvo - Enero 18 07
    private String planilla;
    private String placa;
    private boolean mostrarDetalle = false;
    private Vector items;
    private String moneda_dstrct;
    private int transaccion_anulacion;
    private String fecha_planilla;
    //AMATURANA 31.01.2007        
    Vector imp_items;
    Vector imp_doc;
    //AMATURANA 07.07.2007
    String egresos_relacionados;
    private double total_neto;
    private String egresos_pendientes;
    //AMATURANA 06.03.2007
    private String remesa;
    public boolean OP = false;
    private String referencia;
    //AMATURANA 10.03.2007
    private String retencion_pago;
    private double saldo;
    private double neto;
    private String clase_documento_rel;
    private String multiservicio ;
    private String tipodoc;
    private String tipo_nomina="N";
    private int key;
    private String tipo_referencia_1;
    private String referencia_1;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public static CXP_Doc loadCXP_Doc(ResultSet rs) throws SQLException {
        CXP_Doc cXP_Doc = new CXP_Doc();
        cXP_Doc.setReg_status(rs.getString("reg_status"));
        cXP_Doc.setDstrct(rs.getString("dstrct"));
        cXP_Doc.setProveedor(rs.getString("proveedor"));
        cXP_Doc.setTipo_documento(rs.getString("tipo_documento"));
        cXP_Doc.setDocumento(rs.getString("documento"));
        cXP_Doc.setDescripcion(rs.getString("descripcion"));
        cXP_Doc.setAgencia(rs.getString("agencia"));
        cXP_Doc.setHandle_code(rs.getString("handle_code"));
        cXP_Doc.setId_mims(rs.getString("id_mims"));

        cXP_Doc.setFecha_documento((rs.getString("fecha_documento") != null) ? rs.getString("fecha_documento") : "");
        cXP_Doc.setFecha_documento(!cXP_Doc.getFecha_documento().equals("0099-01-01 00:00:00") ? cXP_Doc.getFecha_documento() : "");

        cXP_Doc.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
        cXP_Doc.setDocumento_relacionado(rs.getString("documento_relacionado"));
        
    

        cXP_Doc.setFecha_aprobacion((rs.getString("fecha_aprobacion") != null) ? rs.getString("fecha_aprobacion") : "");
        cXP_Doc.setFecha_aprobacion(!cXP_Doc.getFecha_aprobacion().equals("0099-01-01 00:00:00") ? cXP_Doc.getFecha_aprobacion() : "");

        cXP_Doc.setAprobador(rs.getString("aprobador"));
        cXP_Doc.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));
        cXP_Doc.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
        cXP_Doc.setUltima_fecha_pago(rs.getString("ultima_fecha_pago"));
        cXP_Doc.setBanco(rs.getString("banco"));
        cXP_Doc.setSucursal(rs.getString("sucursal"));
        cXP_Doc.setMoneda(rs.getString("moneda"));
        cXP_Doc.setVlr_neto(rs.getDouble("vlr_neto"));
        cXP_Doc.setObservacion(rs.getString("observacion"));
        cXP_Doc.setCreation_user(rs.getString("creation_user"));
        cXP_Doc.setCreation_date(rs.getString("creation_date"));
        cXP_Doc.setCheque(rs.getString("cheque"));
        cXP_Doc.setCorrida(rs.getString("corrida"));
        cXP_Doc.setVlr_saldo(rs.getDouble("vlr_saldo"));
        cXP_Doc.setVlr_total_abonos(rs.getDouble("vlr_total_abonos"));
        cXP_Doc.setVlr_neto_me(rs.getDouble("vlr_neto_me"));
        cXP_Doc.setVlr_saldo_me(rs.getDouble("vlr_saldo_me"));
        cXP_Doc.setVlr_total_abonos_me(rs.getDouble("vlr_total_abonos_me"));
        cXP_Doc.setClase_documento(rs.getString("clase_documento"));

        /*Modificacion 23-01-07*/
        cXP_Doc.setFecha_contabilizacion((rs.getString("fecha_contabilizacion") != null) ? rs.getString("fecha_contabilizacion") : "");
        cXP_Doc.setTransaccion(rs.getInt("transaccion"));

        return cXP_Doc;
    }

    public static CXP_Doc loadRXP_Doc(ResultSet rs) throws SQLException {
        CXP_Doc cXP_Doc = new CXP_Doc();
        cXP_Doc.setReg_status(rs.getString("reg_status"));
        cXP_Doc.setDstrct(rs.getString("dstrct"));
        cXP_Doc.setProveedor(rs.getString("proveedor"));
        cXP_Doc.setTipo_documento(rs.getString("tipo_documento"));
        cXP_Doc.setDocumento(rs.getString("documento"));
        cXP_Doc.setDescripcion(rs.getString("descripcion"));
        cXP_Doc.setAgencia(rs.getString("agencia"));
        cXP_Doc.setHandle_code(rs.getString("handle_code"));
        cXP_Doc.setId_mims(rs.getString("id_mims"));
        cXP_Doc.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
        cXP_Doc.setDocumento_relacionado(rs.getString("documento_relacionado"));
        cXP_Doc.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
        cXP_Doc.setAprobador(rs.getString("aprobador"));
        cXP_Doc.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));
        cXP_Doc.setBanco(rs.getString("banco"));
        cXP_Doc.setSucursal(rs.getString("sucursal"));
        cXP_Doc.setMoneda(rs.getString("moneda"));
        cXP_Doc.setVlr_neto(rs.getDouble("vlr_neto"));
        cXP_Doc.setVlr_total_abonos(rs.getDouble("vlr_total_abonos"));
        cXP_Doc.setVlr_saldo(rs.getDouble("vlr_saldo"));
        cXP_Doc.setVlr_neto_me(rs.getDouble("vlr_neto_me"));
        cXP_Doc.setVlr_total_abonos_me(rs.getDouble("vlr_total_abonos_me"));
        cXP_Doc.setVlr_saldo_me(rs.getDouble("vlr_saldo_me"));
        cXP_Doc.setTasa(rs.getDouble("tasa"));
        cXP_Doc.setUsuario_contabilizo(rs.getString("usuario_contabilizo"));
        cXP_Doc.setFecha_contabilizacion(rs.getString("fecha_contabilizacion"));
        cXP_Doc.setUsuario_anulo(rs.getString("usuario_anulo"));
        cXP_Doc.setFecha_anulacion(rs.getString("fecha_anulacion"));
        cXP_Doc.setFecha_contabilizacion_anulacion(rs.getString("fecha_contabilizacion_anulacion"));
        cXP_Doc.setObservacion(rs.getString("observacion"));
        cXP_Doc.setNum_obs_autorizador(rs.getDouble("num_obs_autorizador"));
        cXP_Doc.setNum_obs_pagador(rs.getDouble("num_obs_pagador"));
        cXP_Doc.setNum_obs_registra(rs.getDouble("num_obs_registra"));

       
        
        cXP_Doc.setCorrida(rs.getString("corrida"));
        cXP_Doc.setCheque(rs.getString("cheque"));
        cXP_Doc.setPeriodo(rs.getString("periodo"));
        cXP_Doc.setFecha_contabilizacion_ajc(rs.getString("fecha_contabilizacion_ajc"));
        cXP_Doc.setFecha_contabilizacion_ajv(rs.getString("fecha_contabilizacion_ajv"));
        cXP_Doc.setPeriodo_ajc(rs.getString("periodo_ajc"));
        cXP_Doc.setPeriodo_ajv(rs.getString("periodo_ajv"));
        cXP_Doc.setUsuario_contabilizo_ajc(rs.getString("usuario_contabilizo_ajc"));
        cXP_Doc.setUsuario_contabilizo_ajv(rs.getString("usuario_contabilizo_ajv"));
        cXP_Doc.setTransaccion_ajc(rs.getInt("transaccion_ajc"));
        cXP_Doc.setTransaccion_ajv(rs.getInt("transaccion_ajv"));
        cXP_Doc.setFecha_procesado(rs.getString("fecha_procesado"));
        cXP_Doc.setClase_documento(rs.getString("clase_documento"));
        cXP_Doc.setTransaccion(rs.getInt("transaccion"));
        cXP_Doc.setMoneda_banco(rs.getString("moneda_banco"));
        cXP_Doc.setFecha_documento(rs.getString("fecha_documento"));
        cXP_Doc.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
        cXP_Doc.setUltima_fecha_pago(rs.getString("ultima_fecha_pago"));

        cXP_Doc.setNum_cuotas(rs.getInt("num_cuotas"));
        cXP_Doc.setFecha_inicio_transfer(rs.getString("fecha_inicio_transfer"));
        cXP_Doc.setFrecuencia(rs.getString("frecuencia"));
        cXP_Doc.setNum_cuotas_transfer(rs.getInt("num_cuotas_transfer"));
        cXP_Doc.setFecha_ultima_transfer(rs.getString("fecha_ultima_transfer"));
        cXP_Doc.setVlr_transfer_ml(rs.getDouble("vlr_transfer_ml"));
        cXP_Doc.setVlr_transfer_me(rs.getDouble("vlr_transfer_me"));

        return cXP_Doc;
    }

    public static CXP_Doc loadCXP_Doc_factura(ResultSet rs) throws SQLException {
        CXP_Doc cXP_Doc = new CXP_Doc();
        cXP_Doc.setProveedor(rs.getString("proveedor"));
        cXP_Doc.setNomProveedor(rs.getString("nomprov"));
        cXP_Doc.setDstrct(rs.getString("dstrct"));
        cXP_Doc.setTipo_documento(rs.getString("tipo_documento"));
        cXP_Doc.setDocumento(rs.getString("documento"));
        cXP_Doc.setDescripcion(rs.getString("descripcion"));
        cXP_Doc.setAgencia(rs.getString("agencia"));
        cXP_Doc.setFecha_documento(rs.getString("fecha_documento"));
        cXP_Doc.setMoneda(rs.getString("moneda"));
        cXP_Doc.setCreation_user(rs.getString("creation_user"));
        cXP_Doc.setVlr_saldo(Double.parseDouble(rs.getString("vlr_saldo")));



        return cXP_Doc;
    }

    /** Creates a new instance of Documento */
    public CXP_Doc() {
    }

    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }

    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }

    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }

    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }

    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }

    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }

    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }

    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }

    /**
     * Getter for property handle_code.
     * @return Value of property handle_code.
     */
    public java.lang.String getHandle_code() {
        return handle_code;
    }

    /**
     * Setter for property handle_code.
     * @param handle_code New value of property handle_code.
     */
    public void setHandle_code(java.lang.String handle_code) {
        this.handle_code = handle_code;
    }

    /**
     * Getter for property id_mims.
     * @return Value of property id_mims.
     */
    public java.lang.String getId_mims() {
        return id_mims;
    }

    /**
     * Setter for property id_mims.
     * @param id_mims New value of property id_mims.
     */
    public void setId_mims(java.lang.String id_mims) {
        this.id_mims = id_mims;
    }

    /**
     * Getter for property fecha_documento.
     * @return Value of property fecha_documento.
     */
    public java.lang.String getFecha_documento() {
        return fecha_documento;
    }

    /**
     * Setter for property fecha_documento.
     * @param fecha_documento New value of property fecha_documento.
     */
    public void setFecha_documento(java.lang.String fecha_documento) {
        this.fecha_documento = fecha_documento;
    }

    /**
     * Getter for property tipo_documento_rel.
     * @return Value of property tipo_documento_rel.
     */
    public java.lang.String getTipo_documento_rel() {
        return tipo_documento_rel;
    }

    /**
     * Setter for property tipo_documento_rel.
     * @param tipo_documento_rel New value of property tipo_documento_rel.
     */
    public void setTipo_documento_rel(java.lang.String tipo_documento_rel) {
        this.tipo_documento_rel = tipo_documento_rel;
    }

    /**
     * Getter for property documento_relacionado.
     * @return Value of property documento_relacionado.
     */
    public java.lang.String getDocumento_relacionado() {
        return documento_relacionado;
    }

    /**
     * Setter for property documento_relacionado.
     * @param documento_relacionado New value of property documento_relacionado.
     */
    public void setDocumento_relacionado(java.lang.String documento_relacionado) {
        this.documento_relacionado = documento_relacionado;
    }

    /**
     * Getter for property fecha_aprobacion.
     * @return Value of property fecha_aprobacion.
     */
    public java.lang.String getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    /**
     * Setter for property fecha_aprobacion.
     * @param fecha_aprobacion New value of property fecha_aprobacion.
     */
    public void setFecha_aprobacion(java.lang.String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    /**
     * Getter for property aprobador.
     * @return Value of property aprobador.
     */
    public java.lang.String getAprobador() {
        return aprobador;
    }

    /**
     * Setter for property aprobador.
     * @param aprobador New value of property aprobador.
     */
    public void setAprobador(java.lang.String aprobador) {
        this.aprobador = aprobador;
    }

    /**
     * Getter for property usuario_aprobacion.
     * @return Value of property usuario_aprobacion.
     */
    public java.lang.String getUsuario_aprobacion() {
        return usuario_aprobacion;
    }

    /**
     * Setter for property usuario_aprobacion.
     * @param usuario_aprobacion New value of property usuario_aprobacion.
     */
    public void setUsuario_aprobacion(java.lang.String usuario_aprobacion) {
        this.usuario_aprobacion = usuario_aprobacion;
    }

    /**
     * Getter for property fecha_vencimiento.
     * @return Value of property fecha_vencimiento.
     */
    public java.lang.String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * Setter for property fecha_vencimiento.
     * @param fecha_vencimiento New value of property fecha_vencimiento.
     */
    public void setFecha_vencimiento(java.lang.String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * Getter for property ultima_fecha_pago.
     * @return Value of property ultima_fecha_pago.
     */
    public java.lang.String getUltima_fecha_pago() {
        return ultima_fecha_pago;
    }

    /**
     * Setter for property ultima_fecha_pago.
     * @param ultima_fecha_pago New value of property ultima_fecha_pago.
     */
    public void setUltima_fecha_pago(java.lang.String ultima_fecha_pago) {
        this.ultima_fecha_pago = ultima_fecha_pago;
    }

    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }

    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }

    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }

    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }

    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }

    /**
     * Getter for property vlr_neto.
     * @return Value of property vlr_neto.
     */
    public double getVlr_neto() {
        return vlr_neto;
    }

    /**
     * Setter for property vlr_neto.
     * @param vlr_neto New value of property vlr_neto.
     */
    public void setVlr_neto(double vlr_neto) {
        this.vlr_neto = vlr_neto;
    }

    /**
     * Getter for property vlr_total_abonos.
     * @return Value of property vlr_total_abonos.
     */
    public double getVlr_total_abonos() {
        return vlr_total_abonos;
    }

    /**
     * Setter for property vlr_total_abonos.
     * @param vlr_total_abonos New value of property vlr_total_abonos.
     */
    public void setVlr_total_abonos(double vlr_total_abonos) {
        this.vlr_total_abonos = vlr_total_abonos;
    }

    /**
     * Getter for property vlr_saldo.
     * @return Value of property vlr_saldo.
     */
    public double getVlr_saldo() {
        return vlr_saldo;
    }

    /**
     * Setter for property vlr_saldo.
     * @param vlr_saldo New value of property vlr_saldo.
     */
    public void setVlr_saldo(double vlr_saldo) {
        this.vlr_saldo = vlr_saldo;
    }

    /**
     * Getter for property vlr_neto_me.
     * @return Value of property vlr_neto_me.
     */
    public double getVlr_neto_me() {
        return vlr_neto_me;
    }

    /**
     * Setter for property vlr_neto_me.
     * @param vlr_neto_me New value of property vlr_neto_me.
     */
    public void setVlr_neto_me(double vlr_neto_me) {
        this.vlr_neto_me = vlr_neto_me;
    }

    /**
     * Getter for property vlr_total_abonos_me.
     * @return Value of property vlr_total_abonos_me.
     */
    public double getVlr_total_abonos_me() {
        return vlr_total_abonos_me;
    }

    /**
     * Setter for property vlr_total_abonos_me.
     * @param vlr_total_abonos_me New value of property vlr_total_abonos_me.
     */
    public void setVlr_total_abonos_me(double vlr_total_abonos_me) {
        this.vlr_total_abonos_me = vlr_total_abonos_me;
    }

    /**
     * Getter for property vlr_saldo_me.
     * @return Value of property vlr_saldo_me.
     */
    public double getVlr_saldo_me() {
        return vlr_saldo_me;
    }

    /**
     * Setter for property vlr_saldo_me.
     * @param vlr_saldo_me New value of property vlr_saldo_me.
     */
    public void setVlr_saldo_me(double vlr_saldo_me) {
        this.vlr_saldo_me = vlr_saldo_me;
    }

    /**
     * Getter for property usuario_contabilizo.
     * @return Value of property usuario_contabilizo.
     */
    public java.lang.String getUsuario_contabilizo() {
        return usuario_contabilizo;
    }

    /**
     * Setter for property usuario_contabilizo.
     * @param usuario_contabilizo New value of property usuario_contabilizo.
     */
    public void setUsuario_contabilizo(java.lang.String usuario_contabilizo) {
        this.usuario_contabilizo = usuario_contabilizo;
    }

    /**
     * Getter for property fecha_contabilizacion.
     * @return Value of property fecha_contabilizacion.
     */
    public java.lang.String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    /**
     * Setter for property fecha_contabilizacion.
     * @param fecha_contabilizacion New value of property fecha_contabilizacion.
     */
    public void setFecha_contabilizacion(java.lang.String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    /**
     * Getter for property usuario_anulo.
     * @return Value of property usuario_anulo.
     */
    public java.lang.String getUsuario_anulo() {
        return usuario_anulo;
    }

    /**
     * Setter for property usuario_anulo.
     * @param usuario_anulo New value of property usuario_anulo.
     */
    public void setUsuario_anulo(java.lang.String usuario_anulo) {
        this.usuario_anulo = usuario_anulo;
    }

    /**
     * Getter for property fecha_anulacion.
     * @return Value of property fecha_anulacion.
     */
    public java.lang.String getFecha_anulacion() {
        return fecha_anulacion;
    }

    /**
     * Setter for property fecha_anulacion.
     * @param fecha_anulacion New value of property fecha_anulacion.
     */
    public void setFecha_anulacion(java.lang.String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }

    /**
     * Getter for property fecha_contabilizacion_anulacion.
     * @return Value of property fecha_contabilizacion_anulacion.
     */
    public java.lang.String getFecha_contabilizacion_anulacion() {
        return fecha_contabilizacion_anulacion;
    }

    /**
     * Setter for property fecha_contabilizacion_anulacion.
     * @param fecha_contabilizacion_anulacion New value of property fecha_contabilizacion_anulacion.
     */
    public void setFecha_contabilizacion_anulacion(java.lang.String fecha_contabilizacion_anulacion) {
        this.fecha_contabilizacion_anulacion = fecha_contabilizacion_anulacion;
    }

    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }

    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }

    /**
     * Getter for property num_obs_autorizador.
     * @return Value of property num_obs_autorizador.
     */
    public double getNum_obs_autorizador() {
        return num_obs_autorizador;
    }

    /**
     * Setter for property num_obs_autorizador.
     * @param num_obs_autorizador New value of property num_obs_autorizador.
     */
    public void setNum_obs_autorizador(double num_obs_autorizador) {
        this.num_obs_autorizador = num_obs_autorizador;
    }

    /**
     * Getter for property num_obs_pagador.
     * @return Value of property num_obs_pagador.
     */
    public double getNum_obs_pagador() {
        return num_obs_pagador;
    }

    /**
     * Setter for property num_obs_pagador.
     * @param num_obs_pagador New value of property num_obs_pagador.
     */
    public void setNum_obs_pagador(double num_obs_pagador) {
        this.num_obs_pagador = num_obs_pagador;
    }

    /**
     * Getter for property num_obs_registra.
     * @return Value of property num_obs_registra.
     */
    public double getNum_obs_registra() {
        return num_obs_registra;
    }

    /**
     * Setter for property num_obs_registra.
     * @param num_obs_registra New value of property num_obs_registra.
     */
    public void setNum_obs_registra(double num_obs_registra) {
        this.num_obs_registra = num_obs_registra;
    }

    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }

    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }

    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }

    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }

    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }

    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }

    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }

    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }

    /**
     * Getter for property numDias.
     * @return Value of property numDias.
     */
    public java.lang.String getNumDias() {
        return numDias;
    }

    /**
     * Setter for property numDias.
     * @param numDias New value of property numDias.
     */
    public void setNumDias(java.lang.String numDias) {
        this.numDias = numDias;
    }

    /**
     * Getter for property banderaRoja.
     * @return Value of property banderaRoja.
     */
    public boolean isBanderaRoja() {
        return banderaRoja;
    }

    /**
     * Setter for property banderaRoja.
     * @param banderaRoja New value of property banderaRoja.
     */
    public void setBanderaRoja(boolean banderaRoja) {
        this.banderaRoja = banderaRoja;
    }

    /**
     * Getter for property banderaVerde.
     * @return Value of property banderaVerde.
     */
    public boolean isBanderaVerde() {
        return banderaVerde;
    }

    /**
     * Setter for property banderaVerde.
     * @param banderaVerde New value of property banderaVerde.
     */
    public void setBanderaVerde(boolean banderaVerde) {
        this.banderaVerde = banderaVerde;
    }

    /**
     * Getter for property responder.
     * @return Value of property responder.
     */
    public boolean isResponder() {
        return responder;
    }

    /**
     * Setter for property responder.
     * @param responder New value of property responder.
     */
    public void setResponder(boolean responder) {
        this.responder = responder;
    }

    /**
     * Getter for property nomProveedor.
     * @return Value of property nomProveedor.
     */
    public java.lang.String getNomProveedor() {
        return nomProveedor;
    }

    /**
     * Setter for property nomProveedor.
     * @param nomProveedor New value of property nomProveedor.
     */
    public void setNomProveedor(java.lang.String nomProveedor) {
        this.nomProveedor = nomProveedor;
    }

    /**
     * Getter for property document_name.
     * @return Value of property document_name.
     */
    public java.lang.String getDocument_name() {
        return document_name;
    }

    /**
     * Setter for property document_name.
     * @param document_name New value of property document_name.
     */
    public void setDocument_name(java.lang.String document_name) {
        this.document_name = document_name;
    }

    /**
     * Getter for property seleccionado.
     * @return Value of property seleccionado.
     */
    public boolean isSeleccionado() {
        return seleccionado;
    }

    /**
     * Setter for property seleccionado.
     * @param seleccionado New value of property seleccionado.
     */
    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    /**
     * Getter for property plazo.
     * @return Value of property plazo.
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * Setter for property plazo.
     * @param plazo New value of property plazo.
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    /**
     * Getter for property ucagencia.
     * @return Value of property ucagencia.
     */
    public java.lang.String getUcagencia() {
        return ucagencia;
    }

    /**
     * Setter for property ucagencia.
     * @param ucagencia New value of property ucagencia.
     */
    public void setUcagencia(java.lang.String ucagencia) {
        this.ucagencia = ucagencia;
    }

    /**
     * Getter for property maxfila.
     * @return Value of property maxfila.
     */
    public java.lang.String getMaxfila() {
        return maxfila;
    }

    /**
     * Setter for property maxfila.
     * @param maxfila New value of property maxfila.
     */
    public void setMaxfila(java.lang.String maxfila) {
        this.maxfila = maxfila;
    }

    /**
     * Getter for property iva.
     * @return Value of property iva.
     */
    public java.lang.String getIva() {
        return iva;
    }

    /**
     * Setter for property iva.
     * @param iva New value of property iva.
     */
    public void setIva(java.lang.String iva) {
        this.iva = iva;
    }

    /**
     * Getter for property riva.
     * @return Value of property riva.
     */
    public java.lang.String getRiva() {
        return riva;
    }

    /**
     * Setter for property riva.
     * @param riva New value of property riva.
     */
    public void setRiva(java.lang.String riva) {
        this.riva = riva;
    }

    /**
     * Getter for property rica.
     * @return Value of property rica.
     */
    public java.lang.String getRica() {
        return rica;
    }

    /**
     * Setter for property rica.
     * @param rica New value of property rica.
     */
    public void setRica(java.lang.String rica) {
        this.rica = rica;
    }

    /**
     * Getter for property rfte.
     * @return Value of property rfte.
     */
    public java.lang.String getRfte() {
        return rfte;
    }

    /**
     * Setter for property rfte.
     * @param rfte New value of property rfte.
     */
    public void setRfte(java.lang.String rfte) {
        this.rfte = rfte;
    }

    /**
     * Getter for property vlr_total.
     * @return Value of property vlr_total.
     */
    public double getVlr_total() {
        return vlr_total;
    }

    /**
     * Setter for property vlr_total.
     * @param vlr_total New value of property vlr_total.
     */
    public void setVlr_total(double vlr_total) {
        this.vlr_total = vlr_total;
    }

    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }

    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }

    /**
     * Getter for property cheque.
     * @return Value of property cheque.
     */
    public java.lang.String getCheque() {
        return cheque;
    }

    /**
     * Setter for property cheque.
     * @param cheque New value of property cheque.
     */
    public void setCheque(java.lang.String cheque) {
        this.cheque = cheque;
    }

    /**
     * Getter for property clase_documento.
     * @return Value of property clase_documento.
     */
    public java.lang.String getClase_documento() {
        return clase_documento;
    }

    /**
     * Setter for property clase_documento.
     * @param clase_documento New value of property clase_documento.
     */
    public void setClase_documento(java.lang.String clase_documento) {
        this.clase_documento = clase_documento;
    }

    /**
     * Getter for property moneda_banco.
     * @return Value of property moneda_banco.
     */
    public java.lang.String getMoneda_banco() {
        return moneda_banco;
    }

    /**
     * Setter for property moneda_banco.
     * @param moneda_banco New value of property moneda_banco.
     */
    public void setMoneda_banco(java.lang.String moneda_banco) {
        this.moneda_banco = moneda_banco;
    }

    /**
     * Getter for property fecha_contabilizacion_ajc.
     * @return Value of property fecha_contabilizacion_ajc.
     */
    public java.lang.String getFecha_contabilizacion_ajc() {
        return fecha_contabilizacion_ajc;
    }

    /**
     * Setter for property fecha_contabilizacion_ajc.
     * @param fecha_contabilizacion_ajc New value of property fecha_contabilizacion_ajc.
     */
    public void setFecha_contabilizacion_ajc(java.lang.String fecha_contabilizacion_ajc) {
        this.fecha_contabilizacion_ajc = fecha_contabilizacion_ajc;
    }

    /**
     * Getter for property fecha_contabilizacion_ajv.
     * @return Value of property fecha_contabilizacion_ajv.
     */
    public java.lang.String getFecha_contabilizacion_ajv() {
        return fecha_contabilizacion_ajv;
    }

    /**
     * Setter for property fecha_contabilizacion_ajv.
     * @param fecha_contabilizacion_ajv New value of property fecha_contabilizacion_ajv.
     */
    public void setFecha_contabilizacion_ajv(java.lang.String fecha_contabilizacion_ajv) {
        this.fecha_contabilizacion_ajv = fecha_contabilizacion_ajv;
    }

    /**
     * Getter for property fecha_inicio_transfer.
     * @return Value of property fecha_inicio_transfer.
     */
    public java.lang.String getFecha_inicio_transfer() {
        return fecha_inicio_transfer;
    }

    /**
     * Setter for property fecha_inicio_transfer.
     * @param fecha_inicio_transfer New value of property fecha_inicio_transfer.
     */
    public void setFecha_inicio_transfer(java.lang.String fecha_inicio_transfer) {
        this.fecha_inicio_transfer = fecha_inicio_transfer;
    }

    /**
     * Getter for property fecha_inicio_transferO.
     * @return Value of property fecha_inicio_transferO.
     */
    public java.lang.String getFecha_inicio_transferO() {
        return fecha_inicio_transferO;
    }

    /**
     * Setter for property fecha_inicio_transferO.
     * @param fecha_inicio_transferO New value of property fecha_inicio_transferO.
     */
    public void setFecha_inicio_transferO(java.lang.String fecha_inicio_transferO) {
        this.fecha_inicio_transferO = fecha_inicio_transferO;
    }

    /**
     * Getter for property fecha_procesado.
     * @return Value of property fecha_procesado.
     */
    public java.lang.String getFecha_procesado() {
        return fecha_procesado;
    }

    /**
     * Setter for property fecha_procesado.
     * @param fecha_procesado New value of property fecha_procesado.
     */
    public void setFecha_procesado(java.lang.String fecha_procesado) {
        this.fecha_procesado = fecha_procesado;
    }

    /**
     * Getter for property fecha_ultima_transfer.
     * @return Value of property fecha_ultima_transfer.
     */
    public java.lang.String getFecha_ultima_transfer() {
        return fecha_ultima_transfer;
    }

    /**
     * Setter for property fecha_ultima_transfer.
     * @param fecha_ultima_transfer New value of property fecha_ultima_transfer.
     */
    public void setFecha_ultima_transfer(java.lang.String fecha_ultima_transfer) {
        this.fecha_ultima_transfer = fecha_ultima_transfer;
    }

    /**
     * Getter for property frecuencia.
     * @return Value of property frecuencia.
     */
    public java.lang.String getFrecuencia() {
        return frecuencia;
    }

    /**
     * Setter for property frecuencia.
     * @param frecuencia New value of property frecuencia.
     */
    public void setFrecuencia(java.lang.String frecuencia) {
        this.frecuencia = frecuencia;
    }

    /**
     * Getter for property num_cuotas.
     * @return Value of property num_cuotas.
     */
    public int getNum_cuotas() {
        return num_cuotas;
    }

    /**
     * Setter for property num_cuotas.
     * @param num_cuotas New value of property num_cuotas.
     */
    public void setNum_cuotas(int num_cuotas) {
        this.num_cuotas = num_cuotas;
    }

    /**
     * Getter for property num_cuotas_transfer.
     * @return Value of property num_cuotas_transfer.
     */
    public int getNum_cuotas_transfer() {
        return num_cuotas_transfer;
    }

    /**
     * Setter for property num_cuotas_transfer.
     * @param num_cuotas_transfer New value of property num_cuotas_transfer.
     */
    public void setNum_cuotas_transfer(int num_cuotas_transfer) {
        this.num_cuotas_transfer = num_cuotas_transfer;
    }

    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }

    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }

    /**
     * Getter for property periodo_ajc.
     * @return Value of property periodo_ajc.
     */
    public java.lang.String getPeriodo_ajc() {
        return periodo_ajc;
    }

    /**
     * Setter for property periodo_ajc.
     * @param periodo_ajc New value of property periodo_ajc.
     */
    public void setPeriodo_ajc(java.lang.String periodo_ajc) {
        this.periodo_ajc = periodo_ajc;
    }

    /**
     * Getter for property periodo_ajv.
     * @return Value of property periodo_ajv.
     */
    public java.lang.String getPeriodo_ajv() {
        return periodo_ajv;
    }

    /**
     * Setter for property periodo_ajv.
     * @param periodo_ajv New value of property periodo_ajv.
     */
    public void setPeriodo_ajv(java.lang.String periodo_ajv) {
        this.periodo_ajv = periodo_ajv;
    }

    /**
     * Getter for property transaccion.
     * @return Value of property transaccion.
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * Setter for property transaccion.
     * @param transaccion New value of property transaccion.
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * Getter for property transaccion_ajc.
     * @return Value of property transaccion_ajc.
     */
    public int getTransaccion_ajc() {
        return transaccion_ajc;
    }

    /**
     * Setter for property transaccion_ajc.
     * @param transaccion_ajc New value of property transaccion_ajc.
     */
    public void setTransaccion_ajc(int transaccion_ajc) {
        this.transaccion_ajc = transaccion_ajc;
    }

    /**
     * Getter for property transaccion_ajv.
     * @return Value of property transaccion_ajv.
     */
    public int getTransaccion_ajv() {
        return transaccion_ajv;
    }

    /**
     * Setter for property transaccion_ajv.
     * @param transaccion_ajv New value of property transaccion_ajv.
     */
    public void setTransaccion_ajv(int transaccion_ajv) {
        this.transaccion_ajv = transaccion_ajv;
    }

    /**
     * Getter for property usuario_contabilizo_ajc.
     * @return Value of property usuario_contabilizo_ajc.
     */
    public java.lang.String getUsuario_contabilizo_ajc() {
        return usuario_contabilizo_ajc;
    }

    /**
     * Setter for property usuario_contabilizo_ajc.
     * @param usuario_contabilizo_ajc New value of property usuario_contabilizo_ajc.
     */
    public void setUsuario_contabilizo_ajc(java.lang.String usuario_contabilizo_ajc) {
        this.usuario_contabilizo_ajc = usuario_contabilizo_ajc;
    }

    /**
     * Getter for property usuario_contabilizo_ajv.
     * @return Value of property usuario_contabilizo_ajv.
     */
    public java.lang.String getUsuario_contabilizo_ajv() {
        return usuario_contabilizo_ajv;
    }

    /**
     * Setter for property usuario_contabilizo_ajv.
     * @param usuario_contabilizo_ajv New value of property usuario_contabilizo_ajv.
     */
    public void setUsuario_contabilizo_ajv(java.lang.String usuario_contabilizo_ajv) {
        this.usuario_contabilizo_ajv = usuario_contabilizo_ajv;
    }

    /**
     * Getter for property vlr_transfer_me.
     * @return Value of property vlr_transfer_me.
     */
    public double getVlr_transfer_me() {
        return vlr_transfer_me;
    }

    /**
     * Setter for property vlr_transfer_me.
     * @param vlr_transfer_me New value of property vlr_transfer_me.
     */
    public void setVlr_transfer_me(double vlr_transfer_me) {
        this.vlr_transfer_me = vlr_transfer_me;
    }

    /**
     * Getter for property vlr_transfer_ml.
     * @return Value of property vlr_transfer_ml.
     */
    public double getVlr_transfer_ml() {
        return vlr_transfer_ml;
    }

    /**
     * Setter for property vlr_transfer_ml.
     * @param vlr_transfer_ml New value of property vlr_transfer_ml.
     */
    public void setVlr_transfer_ml(double vlr_transfer_ml) {
        this.vlr_transfer_ml = vlr_transfer_ml;
    }

    /**
     * Getter for property valor_saldo_anterior.
     * @return Value of property valor_saldo_anterior.
     */
    public double getValor_saldo_anterior() {
        return valor_saldo_anterior;
    }

    /**
     * Setter for property valor_saldo_anterior.
     * @param valor_saldo_anterior New value of property valor_saldo_anterior.
     */
    public void setValor_saldo_anterior(double valor_saldo_anterior) {
        this.valor_saldo_anterior = valor_saldo_anterior;
    }

    /**
     * Getter for property valor_saldo_me_anterior.
     * @return Value of property valor_saldo_me_anterior.
     */
    public double getValor_saldo_me_anterior() {
        return valor_saldo_me_anterior;
    }

    /**
     * Setter for property valor_saldo_me_anterior.
     * @param valor_saldo_me_anterior New value of property valor_saldo_me_anterior.
     */
    public void setValor_saldo_me_anterior(double valor_saldo_me_anterior) {
        this.valor_saldo_me_anterior = valor_saldo_me_anterior;
    }

    /**
     * Getter for property beneficiario.
     * @return Value of property beneficiario.
     */
    public java.lang.String getBeneficiario() {
        return beneficiario;
    }

    /**
     * Setter for property beneficiario.
     * @param beneficiario New value of property beneficiario.
     */
    public void setBeneficiario(java.lang.String beneficiario) {
        this.beneficiario = beneficiario;
    }

    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }

    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }

    /**
     * Getter for property prov_banco_transfer.
     * @return Value of property prov_banco_transfer.
     */
    public java.lang.String getProv_banco_transfer() {
        return prov_banco_transfer;
    }

    /**
     * Setter for property prov_banco_transfer.
     * @param prov_banco_transfer New value of property prov_banco_transfer.
     */
    public void setProv_banco_transfer(java.lang.String prov_banco_transfer) {
        this.prov_banco_transfer = prov_banco_transfer;
    }

    /**
     * Getter for property prov_cedula_cuenta.
     * @return Value of property prov_cedula_cuenta.
     */
    public java.lang.String getProv_cedula_cuenta() {
        return prov_cedula_cuenta;
    }

    /**
     * Setter for property prov_cedula_cuenta.
     * @param prov_cedula_cuenta New value of property prov_cedula_cuenta.
     */
    public void setProv_cedula_cuenta(java.lang.String prov_cedula_cuenta) {
        this.prov_cedula_cuenta = prov_cedula_cuenta;
    }

    /**
     * Getter for property prov_no_cuenta.
     * @return Value of property prov_no_cuenta.
     */
    public java.lang.String getProv_no_cuenta() {
        return prov_no_cuenta;
    }

    /**
     * Setter for property prov_no_cuenta.
     * @param prov_no_cuenta New value of property prov_no_cuenta.
     */
    public void setProv_no_cuenta(java.lang.String prov_no_cuenta) {
        this.prov_no_cuenta = prov_no_cuenta;
    }

    /**
     * Getter for property prov_nombre_cuenta.
     * @return Value of property prov_nombre_cuenta.
     */
    public java.lang.String getProv_nombre_cuenta() {
        return prov_nombre_cuenta;
    }

    /**
     * Setter for property prov_nombre_cuenta.
     * @param prov_nombre_cuenta New value of property prov_nombre_cuenta.
     */
    public void setProv_nombre_cuenta(java.lang.String prov_nombre_cuenta) {
        this.prov_nombre_cuenta = prov_nombre_cuenta;
    }

    /**
     * Getter for property prov_suc_transfer.
     * @return Value of property prov_suc_transfer.
     */
    public java.lang.String getProv_suc_transfer() {
        return prov_suc_transfer;
    }

    /**
     * Setter for property prov_suc_transfer.
     * @param prov_suc_transfer New value of property prov_suc_transfer.
     */
    public void setProv_suc_transfer(java.lang.String prov_suc_transfer) {
        this.prov_suc_transfer = prov_suc_transfer;
    }

    /**
     * Getter for property prov_tipo_cuenta.
     * @return Value of property prov_tipo_cuenta.
     */
    public java.lang.String getProv_tipo_cuenta() {
        return prov_tipo_cuenta;
    }

    /**
     * Setter for property prov_tipo_cuenta.
     * @param prov_tipo_cuenta New value of property prov_tipo_cuenta.
     */
    public void setProv_tipo_cuenta(java.lang.String prov_tipo_cuenta) {
        this.prov_tipo_cuenta = prov_tipo_cuenta;
    }

    /**
     * Getter for property prov_tipo_pago.
     * @return Value of property prov_tipo_pago.
     */
    public java.lang.String getProv_tipo_pago() {
        return prov_tipo_pago;
    }

    /**
     * Setter for property prov_tipo_pago.
     * @param prov_tipo_pago New value of property prov_tipo_pago.
     */
    public void setProv_tipo_pago(java.lang.String prov_tipo_pago) {
        this.prov_tipo_pago = prov_tipo_pago;
    }

    /**
     * Getter for property documento_rel_name.
     * @return Value of property documento_rel_name.
     */
    public java.lang.String getDocumento_rel_name() {
        return documento_rel_name;
    }

    /**
     * Setter for property documento_rel_name.
     * @param documento_rel_name New value of property documento_rel_name.
     */
    public void setDocumento_rel_name(java.lang.String documento_rel_name) {
        this.documento_rel_name = documento_rel_name;
    }

    /**
     * Getter for property agenciaBanco.
     * @return Value of property agenciaBanco.
     */
    public java.lang.String getAgenciaBanco() {
        return agenciaBanco;
    }

    /**
     * Setter for property agenciaBanco.
     * @param agenciaBanco New value of property agenciaBanco.
     */
    public void setAgenciaBanco(java.lang.String agenciaBanco) {
        this.agenciaBanco = agenciaBanco;
    }

    /**
     * Getter for property OP.
     * @return Value of property OP.
     */
    public boolean isOP() {
        return (this.clase_documento.equals("0") || this.clase_documento.equals("1")) ? true : false;
    }

    /**
     * Setter for property OP.
     * @param OP New value of property OP.
     */
    public void setOP(boolean OP) {
        this.OP = OP;
    }

    /**
     * Getter for property totalIva.
     * @return Value of property totalIva.
     */
    public double getTotalIva() {
        return totalIva;
    }

    /**
     * Setter for property totalIva.
     * @param totalIva New value of property totalIva.
     */
    public void setTotalIva(double totalIva) {
        this.totalIva = totalIva;
    }

    /**
     * Getter for property totalRiva.
     * @return Value of property totalRiva.
     */
    public double getTotalRiva() {
        return totalRiva;
    }

    /**
     * Setter for property totalRiva.
     * @param totalRiva New value of property totalRiva.
     */
    public void setTotalRiva(double totalRiva) {
        this.totalRiva = totalRiva;
    }

    /**
     * Getter for property totalRica.
     * @return Value of property totalRica.
     */
    public double getTotalRica() {
        return totalRica;
    }

    /**
     * Setter for property totalRica.
     * @param totalRica New value of property totalRica.
     */
    public void setTotalRica(double totalRica) {
        this.totalRica = totalRica;
    }

    /**
     * Getter for property totalRfte.
     * @return Value of property totalRfte.
     */
    public double getTotalRfte() {
        return totalRfte;
    }

    /**
     * Setter for property totalRfte.
     * @param totalRfte New value of property totalRfte.
     */
    public void setTotalRfte(double totalRfte) {
        this.totalRfte = totalRfte;
    }

    /**
     * Getter for property documentos_relacionados.
     * @return Value of property documentos_relacionados.
     */
    public java.lang.String getDocumentos_relacionados() {
        return documentos_relacionados;
    }

    /**
     * Setter for property documentos_relacionados.
     * @param documentos_relacionados New value of property documentos_relacionados.
     */
    public void setDocumentos_relacionados(java.lang.String documentos_relacionados) {
        this.documentos_relacionados = documentos_relacionados;
    }

    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }

    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }

    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }

    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }

    /**
     * Getter for property mostrarDetalle.
     * @return Value of property mostrarDetalle.
     */
    public boolean isMostrarDetalle() {
        return mostrarDetalle;
    }

    /**
     * Setter for property mostrarDetalle.
     * @param mostrarDetalle New value of property mostrarDetalle.
     */
    public void setMostrarDetalle(boolean mostrarDetalle) {
        this.mostrarDetalle = mostrarDetalle;
    }

    /**
     * Getter for property items.
     * @return Value of property items.
     */
    public java.util.Vector getItems() {
        return items;
    }

    /**
     * Setter for property items.
     * @param items New value of property items.
     */
    public void setItems(java.util.Vector items) {
        this.items = items;
    }

    /**
     * Getter for property moneda_dstrct.
     * @return Value of property moneda_dstrct.
     */
    public java.lang.String getMoneda_dstrct() {
        return moneda_dstrct;
    }

    /**
     * Setter for property moneda_dstrct.
     * @param moneda_dstrct New value of property moneda_dstrct.
     */
    public void setMoneda_dstrct(java.lang.String moneda_dstrct) {
        this.moneda_dstrct = moneda_dstrct;
    }

    /**
     * Getter for property transaccion_anulacion.
     * @return Value of property transaccion_anulacion.
     */
    public int getTransaccion_anulacion() {
        return transaccion_anulacion;
    }

    /**
     * Setter for property transaccion_anulacion.
     * @param transaccion_anulacion New value of property transaccion_anulacion.
     */
    public void setTransaccion_anulacion(int transaccion_anulacion) {
        this.transaccion_anulacion = transaccion_anulacion;
    }

    /**
     * Getter for property fecha_planilla.
     * @return Value of property fecha_planilla.
     */
    public java.lang.String getFecha_planilla() {
        return fecha_planilla;
    }

    /**
     * Setter for property fecha_planilla.
     * @param fecha_planilla New value of property fecha_planilla.
     */
    public void setFecha_planilla(java.lang.String fecha_planilla) {
        this.fecha_planilla = fecha_planilla;
    }

    /**
     * Getter for property imp_items.
     * @return Value of property imp_items.
     */
    public java.util.Vector getImp_items() {
        return imp_items;
    }

    /**
     * Setter for property imp_items.
     * @param imp_items New value of property imp_items.
     */
    public void setImp_items(java.util.Vector imp_items) {
        this.imp_items = imp_items;
    }

    /**
     * Getter for property imp_doc.
     * @return Value of property imp_doc.
     */
    public java.util.Vector getImp_doc() {
        return imp_doc;
    }

    /**
     * Setter for property imp_doc.
     * @param imp_doc New value of property imp_doc.
     */
    public void setImp_doc(java.util.Vector imp_doc) {
        this.imp_doc = imp_doc;
    }

    /**
     * Getter for property egresos_relacionados.
     * @return Value of property egresos_relacionados.
     */
    public java.lang.String getEgresos_relacionados() {
        return egresos_relacionados;
    }

    /**
     * Setter for property egresos_relacionados.
     * @param egresos_relacionados New value of property egresos_relacionados.
     */
    public void setEgresos_relacionados(java.lang.String egresos_relacionados) {
        this.egresos_relacionados = egresos_relacionados;
    }

    /**
     * Getter for property total_neto.
     * @return Value of property total_neto.
     */
    public double getTotal_neto() {
        return total_neto;
    }

    /**
     * Setter for property total_neto.
     * @param total_neto New value of property total_neto.
     */
    public void setTotal_neto(double total_neto) {
        this.total_neto = total_neto;
    }

    /**
     * Getter for property vlr_bruto_ml.
     * @return Value of property vlr_bruto_ml.
     */
    public double getVlr_bruto_ml() {
        return vlr_bruto_ml;
    }

    /**
     * Setter for property vlr_bruto_ml.
     * @param vlr_bruto_ml New value of property vlr_bruto_ml.
     */
    public void setVlr_bruto_ml(double vlr_bruto_ml) {
        this.vlr_bruto_ml = vlr_bruto_ml;
    }

    /**
     * Getter for property vlr_bruto_me.
     * @return Value of property vlr_bruto_me.
     */
    public double getVlr_bruto_me() {
        return vlr_bruto_me;
    }

    /**
     * Setter for property vlr_bruto_me.
     * @param vlr_bruto_me New value of property vlr_bruto_me.
     */
    public void setVlr_bruto_me(double vlr_bruto_me) {
        this.vlr_bruto_me = vlr_bruto_me;
    }

    /**
     * Getter for property egresos_pendientes.
     * @return Value of property egresos_pendientes.
     */
    public java.lang.String getEgresos_pendientes() {
        return egresos_pendientes;
    }

    /**
     * Setter for property egresos_pendientes.
     * @param egresos_pendientes New value of property egresos_pendientes.
     */
    public void setEgresos_pendientes(java.lang.String egresos_pendientes) {
        this.egresos_pendientes = egresos_pendientes;
    }

    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }

    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }

    /**
     * Getter for property referencia.
     * @return Value of property referencia.
     */
    public java.lang.String getReferencia() {
        return referencia;
    }

    /**
     * Setter for property referencia.
     * @param referencia New value of property referencia.
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }

    /**
     * Getter for property saldo.
     * @return Value of property saldo.
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * Setter for property saldo.
     * @param saldo New value of property saldo.
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    /**
     * Getter for property neto.
     * @return Value of property neto.
     */
    public double getNeto() {
        return neto;
    }

    /**
     * Setter for property neto.
     * @param neto New value of property neto.
     */
    public void setNeto(double neto) {
        this.neto = neto;
    }

    /**
     * Getter for property retencion_pago.
     * @return Value of property retencion_pago.
     */
    public java.lang.String getRetencion_pago() {
        return retencion_pago;
    }

    /**
     * Setter for property retencion_pago.
     * @param retencion_pago New value of property retencion_pago.
     */
    public void setRetencion_pago(java.lang.String retencion_pago) {
        this.retencion_pago = retencion_pago;
    }

    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }

    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }

    /**
     * Getter for property clase_documento_rel.
     * @return Value of property clase_documento_rel.
     */
    public java.lang.String getClase_documento_rel() {
        return clase_documento_rel;
    }

    /**
     * Setter for property clase_documento_rel.
     * @param clase_documento_rel New value of property clase_documento_rel.
     */
    public void setClase_documento_rel(java.lang.String clase_documento_rel) {
        this.clase_documento_rel = clase_documento_rel;
    }

    public java.lang.String getTipo_nomina() {
        return tipo_nomina;
    }

    public void setTipo_nomina(java.lang.String tipo_nomina) {
        this.tipo_nomina = tipo_nomina;
    }

    public String getMultiservicio() {
        return multiservicio;
    }

    public void setMultiservicio(String multiservicio) {
        this.multiservicio = multiservicio;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    /**
     * @return the tipo_referencia_1
     */
    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    /**
     * @param tipo_referencia_1 the tipo_referencia_1 to set
     */
    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }
    
    
}
