
package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import java.sql.*;



public class Conductor implements Serializable {
   
    private String estado="";
    private String cedula="";
    private String nombre="";
    private String supplier_no="";
    private String pass_no="";
    private String pass_cat="";
    private String pass_expiry_date="";
    private String passport="";
    private String passport_expiry_date="";
    private String rh="";
    private String birth_place="";
    private String date_birth="";
    private String particular_sign="";
    private String trip_total="";
    private String init_date="";
    private String ref_name="";
    private String address_ref="";
    private String phone_ref="";
    private String city_ref="";
    private String name_last_boss="";
    private String address_boss="";
    private String phone_boss="";
    private String city_boss="";
    private String document="";
    private java.sql.Date vecPase;

     //NUEVOS CAMPOS
    private String nrojudicial;
    private String vencejudicial;
    private String nomeps;
    private String nroeps;
    private String fecafieps;
    private String nomarp;
    private String fecafiarp;
    private String nrovisa;
    private String vencevisa;
    private String nrolibtripulante;
    private String vencelibtripulante;
    private String griesgo;
    private String res_pase;
    private String usuarioverifica;
    private String fecverificacion;
    private String base;
    private String usuario;
    private String usuariocrea;
    private String celular;
    private boolean vetado;
    private boolean tieneFoto;
    private String pension;
    private String fecafipension;
    private String huella_der;
    private String huella_izq;
    private String desrespase;
    
    private String desHuella1;
    private String desHuella2;
    
    /*__________________________________________________________________________________
     *                            METODOS
      __________________________________________________________________________________*/
    
        
    public Conductor() {}
    
    
    // METODOS SET
    
    public void setEstado(String state){
        this.estado=state;
    }
    
    public  void setCedula(String id){
        this.cedula=id;
    }
    
    public void setNombre(String name){
       this.nombre=name; 
    }
    
    public void setSupplierNo(String supplier){
        this.supplier_no=supplier;
    }
    
    public void setPassNo(String pase){
        this.pass_no=pase;
    }
    
    public void setPassCat(String categoria){
        this.pass_cat=categoria;
    }
    
    public void setPassExpiryDate(String fecha){
        this.pass_expiry_date=fecha;
    }
    
    public void setPassport(String passport){
        this.passport=passport;
    }
    
    public void setPassportExpiryDate(String fecha){
        passport_expiry_date=fecha;
    }
    
    
    public void setRh(String rh){
        this.rh = rh;
    }
    
    public void setBirthPlace(String birthPlace){
        this.birth_place=birthPlace;
    }
    
    public void setDateBirth(String dateBirth){
        this.date_birth=dateBirth;
    }
    
    public void setParticularSign(String sign){
        this.particular_sign = sign;
    }
    
    public void setTripTotal(String tripTotal){
        this.trip_total = tripTotal;
    }
    
    public void setInitDate(String fecha){
        this.init_date= fecha;
    }
    
    public void setRefName(String refName){
        this.ref_name=refName;
    }
    
    public void setAddressRef(String address){
        this.address_ref=address;
    }
    
    public void setPhoneRef(String phone){
        this.phone_ref = phone;
    }
    
    public void setCityRef(String city){
        this.city_ref=city;
    }
    
    public void setNameLastBoss(String name){
        this.name_last_boss = name;
    }
    
    public void setAddressBoss(String address){
        this.address_boss=address;
    }
    
    public void setPhoneBoss(String phone){
        this.phone_boss = phone;
    }
    
    public void setCityBoss(String city){
        this.city_boss = city;
    }
    
    
    public void setDocument(String document){
        this.document = document;
    }
    
    
   
    
    
   // METODOS GET
    
    //--- conductor
    
    public String getEstado(){
        return this.estado;
    }
    
    public  String getCedula(){
        return this.cedula;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getSupplierNo(){
        return this.supplier_no;
    }
    
    public String getPassNo(){
        return this.pass_no;
    }
    
    public String getPassCat(){
        return this.pass_cat;
    }
    
    public String getPassExpiryDate(){
        return this.pass_expiry_date;
    }
    
    public String getPassport(){
        return this.passport;
    }
    
    public String getPassportExpiryDate(){
        return passport_expiry_date;
    }
    
    
    public String getRh(){
        return this.rh;
    }
    
    public String getBirthPlace(){
        return this.birth_place;
    }
    
    public String getDateBirth(){
       return this.date_birth;
    }
    
    public String getParticularSign(){
       return this.particular_sign;
    }
    
    public String getTripTotal(){
       return this.trip_total;
    }
    
    public String getInitDate(){
        return this.init_date;
    }
    
    public String getRefName(){
        return this.ref_name;
    }
    
    public String getAddressRef(){
        return this.address_ref;
    }
    
    public String getPhoneRef(){
        return this.phone_ref;
    }
    
    public String getCityRef(){
        return this.city_ref;
    }
    
    public String getNameLastBoss(){
        return this.name_last_boss;
    }
    
    public String getAddressBoss(){
        return this.address_boss;
    }
    
    public String getPhoneBoss(){
        return this.phone_boss;
    }
    
    public String getCityBoss(){
        return this.city_boss;
    }
    
    
    public String getDocument(){
        return this.document;
    }
    
    
    
    
    public static Conductor load(ResultSet rs)throws SQLException{
        
        Conductor conductor = new Conductor();
        
        conductor.setCedula(rs.getString(1));
        conductor.setNombre(rs.getString(2));
        
        return conductor;
    }

    
    //--- formaci�n de objetos
    
    public static Conductor getObject(HttpServletRequest request, Usuario usu ){
       Conductor objeto = new Conductor(); 
        
       //obtenemos los parametros
        String estado = request.getParameter("estadoConductor");
        String id     = request.getParameter("identificacion");
        String placa  = request.getParameter("placa");    
        String pase       = request.getParameter("numeroPase"); 
        String categoria  = request.getParameter("categoriaPase"); 
        String fechaPase  = request.getParameter("VencePase"); 
        String pasaporte         = request.getParameter("pasaporte"); 
        String fechaPasaporte    = request.getParameter("VencePasaporte");
        String rh    = request.getParameter("grupoSanguineo")+ request.getParameter("rh"); 
        String lugarNacimiento    = request.getParameter("lugarNacimiento"); 
        String fechaNacimiento    = request.getParameter("fechaNacimiento"); 
        String se�a  = request.getParameter("se�aParticular"); 
        String totalViajes       = request.getParameter("totalViajes"); 
        String fechaInicio       = request.getParameter("fechaInicio"); 
        String referencia     = request.getParameter("nombreReferencia"); 
        String telReferencia  = request.getParameter("telefonoReferencia"); 
        String dirReferencia  = request.getParameter("direccionReferencia"); 
        String ciuReferencia  = request.getParameter("ciudadReferencia"); 
        String jefe     = request.getParameter("nombreJefe"); 
        String telJefe  = request.getParameter("telefonoJefe"); 
        String dirJefe  = request.getParameter("direccionJefe"); 
        String ciuJefe  = request.getParameter("ciudadJefe"); 
        String documento         = request.getParameter("documento"); 
        if(totalViajes.equals(""))
            totalViajes="0";        
        //nuevos campos DIOGENES 121005
        String griesgo = request.getParameter("griesgo");
        String judicial = request.getParameter("Judicial");
        String vencejudicial = request.getParameter("VenceJudicial");
        String visa = request.getParameter("visa");
        String vecevisa = request.getParameter("VenceVisa");
        String libtripulante = request.getParameter("libTripulante");
        String vencelibTripulante = request.getParameter("VenceLibTripulante");
        String eps = request.getParameter("eps");
        String afiliaeps = request.getParameter("Afiliaeps");
        String nroeps = request.getParameter("nroeps");
        String arp = request.getParameter("arp");
        String afiliaarp = request.getParameter("Vencearp");
        String respase = request.getParameter("respase");
       
        //formamos el objeto 
        objeto.setEstado(estado);
        objeto.setCedula(id);
        objeto.setSupplierNo(placa);
        objeto.setPassNo(pase);
        objeto.setPassCat(categoria);
        objeto.setPassExpiryDate(Util.formatoFechaPostgres(fechaPase)  );
        objeto.setPassport(pasaporte);
        objeto.setPassportExpiryDate(Util.formatoFechaPostgres(fechaPasaporte));
        objeto.setRh(rh);
        objeto.setBirthPlace(lugarNacimiento);
        objeto.setDateBirth(Util.formatoFechaPostgres(fechaNacimiento) );
        objeto.setParticularSign(se�a);
        objeto.setTripTotal(totalViajes);
        objeto.setInitDate(Util.formatoFechaPostgres(fechaInicio));
        objeto.setRefName(referencia);
        objeto.setPhoneRef(telReferencia);
        objeto.setAddressRef(dirReferencia);
        objeto.setCityRef(ciuReferencia);
        objeto.setNameLastBoss(jefe);
        objeto.setPhoneBoss(telJefe);
        objeto.setAddressBoss(dirJefe);
        objeto.setCityBoss(ciuJefe);
        objeto.setDocument(documento);
         //nuevos campos
        objeto.setGradoriesgo(griesgo);
        objeto.setNrojudicial(judicial);
        objeto.setVencejudicial(Util.formatoFechaPostgres(vencejudicial));
        objeto.setNrovisa(visa);
        objeto.setVencevisa(Util.formatoFechaPostgres(vecevisa));
        objeto.setNrolibtripulante(libtripulante);
        objeto.setVencelibtripulante(Util.formatoFechaPostgres(vencelibTripulante));
        objeto.setNomeps(eps);
        objeto.setNroeps(nroeps);
        objeto.setFecafieps(Util.formatoFechaPostgres(afiliaeps));
        objeto.setNomarp(arp);
        objeto.setFecafiarp(Util.formatoFechaPostgres(afiliaarp));
        objeto.setRes_pase(respase);
        objeto.setBase(usu.getBase());
        objeto.setUsuario(usu.getLogin());
        objeto.setUsuariocrea(usu.getLogin());
        
        
       return objeto;
    }
    
    /**
     * Getter for property vecPase.
     * @return Value of property vecPase.
     */
    public java.sql.Date getVecPase() {
        return vecPase;
    }    
    
    /**
     * Setter for property vecPase.
     * @param vecPase New value of property vecPase.
     */
    public void setVecPase(java.sql.Date vecPase) {
        this.vecPase = vecPase;
    }
    
    /**
     * Getter for property nrojudicial.
     * @return Value of property nrojudicial.
     */
    public java.lang.String getNrojudicial() {
        return nrojudicial;
    }
    
    /**
     * Setter for property nrojudicial.
     * @param nrojudicial New value of property nrojudicial.
     */
    public void setNrojudicial(java.lang.String nrojudicial) {
        this.nrojudicial = nrojudicial;
    }
    
    /**
     * Getter for property vencejudicial.
     * @return Value of property vencejudicial.
     */
    public java.lang.String getVencejudicial() {
        return vencejudicial;
    }
    
    /**
     * Setter for property vencejudicial.
     * @param vencejudicial New value of property vencejudicial.
     */
    public void setVencejudicial(java.lang.String vencejudicial) {
        this.vencejudicial = vencejudicial;
    }
    
    /**
     * Getter for property vencelibtripulante.
     * @return Value of property vencelibtripulante.
     */
    public java.lang.String getVencelibtripulante() {
        return vencelibtripulante;
    }
    
    /**
     * Setter for property vencelibtripulante.
     * @param vencelibtripulante New value of property vencelibtripulante.
     */
    public void setVencelibtripulante(java.lang.String vencelibtripulante) {
        this.vencelibtripulante = vencelibtripulante;
    }
    
    /**
     * Getter for property vencevisa.
     * @return Value of property vencevisa.
     */
    public java.lang.String getVencevisa() {
        return vencevisa;
    }
    
    /**
     * Setter for property vencevisa.
     * @param vencevisa New value of property vencevisa.
     */
    public void setVencevisa(java.lang.String vencevisa) {
        this.vencevisa = vencevisa;
    }
    
    /**
     * Getter for property nroeps.
     * @return Value of property nroeps.
     */
    public java.lang.String getNroeps() {
        return nroeps;
    }
    
    /**
     * Setter for property nroeps.
     * @param nroeps New value of property nroeps.
     */
    public void setNroeps(java.lang.String nroeps) {
        this.nroeps = nroeps;
    }
    
    /**
     * Getter for property nomeps.
     * @return Value of property nomeps.
     */
    public java.lang.String getNomeps() {
        return nomeps;
    }
    
    /**
     * Setter for property nomeps.
     * @param nomeps New value of property nomeps.
     */
    public void setNomeps(java.lang.String nomeps) {
        this.nomeps = nomeps;
    }
    
    /**
     * Getter for property fecafieps.
     * @return Value of property fecafieps.
     */
    public java.lang.String getFecafieps() {
        return fecafieps;
    }
    
    /**
     * Setter for property fecafieps.
     * @param fecafieps New value of property fecafieps.
     */
    public void setFecafieps(java.lang.String fecafieps) {
        this.fecafieps = fecafieps;
    }
    
    /**
     * Getter for property nomarp.
     * @return Value of property nomarp.
     */
    public java.lang.String getNomarp() {
        return nomarp;
    }
    
    /**
     * Setter for property nomarp.
     * @param nomarp New value of property nomarp.
     */
    public void setNomarp(java.lang.String nomarp) {
        this.nomarp = nomarp;
    }
    
    /**
     * Getter for property fecafiarp.
     * @return Value of property fecafiarp.
     */
    public java.lang.String getFecafiarp() {
        return fecafiarp;
    }
    
    /**
     * Setter for property fecafiarp.
     * @param fecafiarp New value of property fecafiarp.
     */
    public void setFecafiarp(java.lang.String fecafiarp) {
        this.fecafiarp = fecafiarp;
    }
    
    /**
     * Getter for property nrovisa.
     * @return Value of property nrovisa.
     */
    public java.lang.String getNrovisa() {
        return nrovisa;
    }
    
    /**
     * Setter for property nrovisa.
     * @param nrovisa New value of property nrovisa.
     */
    public void setNrovisa(java.lang.String nrovisa) {
        this.nrovisa = nrovisa;
    }
    
    /**
     * Getter for property nrolibtripulante.
     * @return Value of property nrolibtripulante.
     */
    public java.lang.String getNrolibtripulante() {
        return nrolibtripulante;
    }
    
    /**
     * Setter for property nrolibtripulante.
     * @param nrolibtripulante New value of property nrolibtripulante.
     */
    public void setNrolibtripulante(java.lang.String nrolibtripulante) {
        this.nrolibtripulante = nrolibtripulante;
    }
    
    /**
     * Getter for property griesgo.
     * @return Value of property griesgo.
     */
    public java.lang.String getGradoriesgo() {
        return griesgo;
    }
    
    /**
     * Setter for property griesgo.
     * @param griesgo New value of property griesgo.
     */
    public void setGradoriesgo(java.lang.String griesgo) {
        this.griesgo = griesgo;
    }
    
    /**
     * Getter for property res_pase.
     * @return Value of property res_pase.
     */
    public java.lang.String getRes_pase() {
        return res_pase;
    }
    
    /**
     * Setter for property res_pase.
     * @param res_pase New value of property res_pase.
     */
    public void setRes_pase(java.lang.String res_pase) {
        this.res_pase = res_pase;
    }
    
    /**
     * Getter for property usuarioverifica.
     * @return Value of property usuarioverifica.
     */
    public java.lang.String getUsuarioverifica() {
        return usuarioverifica;
    }
    
    /**
     * Setter for property usuarioverifica.
     * @param usuarioverifica New value of property usuarioverifica.
     */
    public void setUsuarioverifica(java.lang.String usuarioverifica) {
        this.usuarioverifica = usuarioverifica;
    }
    
    /**
     * Getter for property fecverificacion.
     * @return Value of property fecverificacion.
     */
    public java.lang.String getFecverificacion() {
        return fecverificacion;
    }
    
    /**
     * Setter for property fecverificacion.
     * @param fecverificacion New value of property fecverificacion.
     */
    public void setFecverificacion(java.lang.String fecverificacion) {
        this.fecverificacion = fecverificacion;
    }
    
     public void setBase(String base){
        this.base=base;
    }
    public String getBase(){
        return base;
    }
    
    public void setUsuario (String usu){
        this.usuario=usu;        
    }
    public String getUsuario ( ){
        return usuario;
    }
    public void setUsuariocrea (String usu){
        this.usuariocrea=usu;        
    }
    public String getUsuariocrea ( ){
        return usuariocrea;
    }
    
    /**
     * Getter for property celular.
     * @return Value of property celular.
     */
    public java.lang.String getCelular() {
        return celular;
    }
    
    /**
     * Setter for property celular.
     * @param celular New value of property celular.
     */
    public void setCelular(java.lang.String celular) {
        this.celular = celular;
    }
    
    /**
     * Getter for property vetado.
     * @return Value of property vetado.
     */
    public boolean isVetado() {
        return vetado;
    }
    
    /**
     * Setter for property vetado.
     * @param vetado New value of property vetado.
     */
    public void setVetado(boolean vetado) {
        this.vetado = vetado;
    }
    
    /**
     * Getter for property tieneFoto.
     * @return Value of property tieneFoto.
     */
    public boolean isTieneFoto() {
        return tieneFoto;
    }
    
    /**
     * Setter for property tieneFoto.
     * @param tieneFoto New value of property tieneFoto.
     */
    public void setTieneFoto(boolean tieneFoto) {
        this.tieneFoto = tieneFoto;
    }
    
    /**
     * Getter for property fecafipension.
     * @return Value of property fecafipension.
     */
    public java.lang.String getFecafipension() {
        return fecafipension;
    }
    
    /**
     * Setter for property fecafipension.
     * @param fecafipension New value of property fecafipension.
     */
    public void setFecafipension(java.lang.String fecafipension) {
        this.fecafipension = fecafipension;
    }
    
    /**
     * Getter for property pension.
     * @return Value of property pension.
     */
    public java.lang.String getPension() {
        return pension;
    }
    
    /**
     * Setter for property pension.
     * @param pension New value of property pension.
     */
    public void setPension(java.lang.String pension) {
        this.pension = pension;
    }
    
    /**
     * Getter for property huella_der.
     * @return Value of property huella_der.
     */
    public java.lang.String getHuella_der() {
        return huella_der;
    }
    
    /**
     * Setter for property huella_der.
     * @param huella_der New value of property huella_der.
     */
    public void setHuella_der(java.lang.String huella_der) {
        this.huella_der = huella_der;
    }
    
    /**
     * Getter for property huella_izq.
     * @return Value of property huella_izq.
     */
    public java.lang.String getHuella_izq() {
        return huella_izq;
    }
    
    /**
     * Setter for property huella_izq.
     * @param huella_izq New value of property huella_izq.
     */
    public void setHuella_izq(java.lang.String huella_izq) {
        this.huella_izq = huella_izq;
    }
    
    /**
     * Getter for property desrespase.
     * @return Value of property desrespase.
     */
    public java.lang.String getDesrespase() {
        return desrespase;
    }
    
    /**
     * Setter for property desrespase.
     * @param desrespase New value of property desrespase.
     */
    public void setDesrespase(java.lang.String desrespase) {
        this.desrespase = desrespase;
    }
    
       
    
    /**
     * Getter for property desHuella2.
     * @return Value of property desHuella2.
     */
    public java.lang.String getDesHuella2() {
        return desHuella2;
    }
    
    /**
     * Setter for property desHuella2.
     * @param desHuella2 New value of property desHuella2.
     */
    public void setDesHuella2(java.lang.String desHuella2) {
        this.desHuella2 = desHuella2;
    }
        /**
     * Getter for property desHuella1.
     * @return Value of property desHuella1.
     */
    public java.lang.String getDesHuella1() {
        return desHuella1;
    }
    
    /**
     * Setter for property desHuella1.
     * @param desHuella1 New value of property desHuella1.
     */
    public void setDesHuella1(java.lang.String desHuella1) {
        this.desHuella1 = desHuella1;
    }

}
