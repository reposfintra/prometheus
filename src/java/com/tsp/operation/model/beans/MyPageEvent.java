package com.tsp.operation.model.beans;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

public class MyPageEvent extends PdfPageEventHelper{

    private PdfWriter writer;
    private Document document;
    private String ruta;

    private Image img1;
    private Image img2;

    private int f = 0;

    public MyPageEvent(PdfWriter w, Document d, String r, int flag){
        writer = w;
        document = d;
        ruta = r;
        f = flag;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document){
        try {
            Rectangle page = document.getPageSize();

            float[] widths = {0.5f, 0.5f};
            PdfPTable table;
            table = new PdfPTable(widths);
            table.setWidthPercentage(100);

            img1 = Image.getInstance(ruta + "/images/consorcio_logo.png");
            img1.scalePercent(60);
            if (f == 0){
                img2 = Image.getInstance(ruta + "/images/electricaribe_logo.png");
            }
            else{
                img2 = Image.getInstance(ruta + "/images/energia_empresarial.png");
            }
            img2.scalePercent(60);
            celda(img1, table);
            celda(img2, table);

            table.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
            table.writeSelectedRows(0, -1, document.leftMargin(), page.getHeight() - document.topMargin() + table.getTotalHeight(), writer.getDirectContent());

            table = null;

            float[] widths2 = {0.333f, 0.333f, 0.333f};
            table = new PdfPTable(widths2);
            table.setWidthPercentage(100);

            celda("FR-OPAV-002.09", table);
            celda("Version 01", table);

           table.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
            table.writeSelectedRows(0, -1, document.leftMargin(), document.bottomMargin(), writer.getDirectContent());
        }
        catch (Exception ex) {
        }
    }

    public void celda(Image img, PdfPTable tab){
        PdfPCell cell;
        cell = new PdfPCell(img);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(BaseColor.WHITE);
        tab.addCell(cell);
    }

    public void celda(Object val, PdfPTable tab){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val));
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(BaseColor.WHITE);
        tab.addCell(cell);
    }
}

