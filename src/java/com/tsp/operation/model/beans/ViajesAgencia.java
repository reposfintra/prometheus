/*
 * ViajesAgencia.java
 *
 * Created on 31 de julio de 2005, 12:53
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  mario
 */

import java.util.*;
import java.lang.Math;

public class ViajesAgencia extends ViajesProceso{

    private String agencia;
    private String agencia_nombre;
   
    private List viajesClientes;
    
    /*14-12-05*/
    private double coperativos_agencia;

    
    /** Creates a new instance of ViajesAgencia */
    public ViajesAgencia() {
        
        agencia        = "";
        agencia_nombre = "";
        viajesClientes = new LinkedList();
        
    }
    ///////////////////////////////////////////////////
    public void setAgencia (String newValue){
        agencia = newValue;
    }
    public void setAgenciaNombre (String newValue){
        agencia_nombre = newValue;
    }
    public void addCliente(ViajesCliente cli){
        viajesClientes.add(cli);
    }
    
    public String getAgencia (){
        return agencia;
    }
    public String getAgenciaNombre (){
        return agencia_nombre;
    }
    public List getListadoClientes(){
        return viajesClientes;
    }
    
    ///////////////////////////////////////////////////////////////
    // proceso
    
    public void Calcular(){
        if (viajesClientes!=null){
            for (int i = 0; i < viajesClientes.size();i++){
                ViajesCliente vc = (ViajesCliente) viajesClientes.get(i);
                for (int dia = 1 ; dia <= 31; dia++){
                    setViajePtdo(dia, getViajePtdo(dia) + vc.getViajePtdo(dia));
                    setViajeEjdo(dia, getViajeEjdo(dia) + vc.getViajeEjdo(dia));                    
                    setCostosPtdo(dia, getCostosPtdo(dia) + vc.getCostosPtdo(dia));
                }
            }
        }
        CalcularDiferencia();
    }
    
    /**
     * Getter for property coperativos_agencia.
     * @return Value of property coperativos_agencia.
     */
    public double getCoperativos_agencia() {
        return coperativos_agencia;
    }
    
    /**
     * Setter for property coperativos_agencia.
     * @param coperativos_agencia New value of property coperativos_agencia.
     */
    public void setCoperativos_agencia(double coperativos_agencia) {
        this.coperativos_agencia = coperativos_agencia;
    }
    
}
