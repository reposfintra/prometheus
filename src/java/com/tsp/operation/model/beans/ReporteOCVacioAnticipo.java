/*
 *  Nombre clase    :  ReporteOCAnticipo.java
 *  Descripcion     :
 *  Autor           : Ing. Enrique De Lavalle
 *  Fecha           : 23 de Noviembre de 2006, 02:18 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;

public class ReporteOCVacioAnticipo {
    
    private String numpla;
    private String agcpla;
    private String despachador;
    private String vlrpla2;
    private String platlr;
    private String creation_date;
    private String printer_date;
    private String dif;
    private String fechaanticipo;
    private String vlr;
    private String document;
    private String fecha_cheque;
    private String ruta_pla;
    private String origenoc;
    private String destinooc;
    private String numrem;
    private String vlrrem2;
    private String cliente;
    private String facturacliente;
    private String agenciaanticipo;
    private String cedulaanticipo;
    private String nombreanticipo;
    private String fechacumplido;
    private String cedulacumplido;
    private String nombrecumplido;
    private String discrepancia;
    private String valorflete;
    private String clasificacion;
    private String tipo_viaje;
    private String producto;
    private String descripcion_via;
    private String puestocontrol;
    private String ocasociadas;
    private String ciudad;
    private String contaroc;
    private String rango;
    private String calificacion;
    
    private String codCliente;
    private String nomCliente;
    private String dias;
    private String item;
    
    private String agc_origen;
    private String agc_destino;
    
    /*modificacion*/
    private String desc_ruta;
    private String pais;
    
    /** Crea una nueva instancia de  ReporteOCAnticipo */
    public ReporteOCVacioAnticipo() {
        
        numpla="";
        agcpla="";
        despachador="";
        vlrpla2="";
        platlr="";
        creation_date="";
        printer_date="";
        dif="";
        fechaanticipo="";
        vlr="";
        document="";
        fecha_cheque="";
        ruta_pla="";
        origenoc="";
        destinooc="";
        numrem="";
        vlrrem2="";
        cliente="";
        facturacliente="";
        agenciaanticipo="";
        cedulaanticipo="";
        nombreanticipo="";
        fechacumplido="";
        cedulacumplido="";
        nombrecumplido="";
        discrepancia="";
        valorflete="";
        clasificacion="";
        tipo_viaje="";
        producto="";
        descripcion_via="";
        puestocontrol="";
        ocasociadas="";
        ciudad="";
        contaroc="";
        rango="";
        calificacion="";
        codCliente="";
        nomCliente="";
        dias="";
        item="";
        
    }

    public static ReporteOCVacioAnticipo load2(ResultSet rs)throws Exception{
        
        ReporteOCVacioAnticipo datos = new ReporteOCVacioAnticipo();
        
        String pd="";
        datos.setNumpla(  ( rs.getString("oc")!=null )?rs.getString("oc"):"" );
        datos.setAgcpla(  ( rs.getString("agcpla")!=null )?rs.getString("agcpla"):"" );
        datos.setDespachador( ( rs.getString("despachador") !=null )?rs.getString("despachador"):"" );
        datos.setVlrpla2( ( rs.getString("vlrpla2") !=null)?rs.getString("vlrpla2"):"" );
        datos.setPlatlr(  ( rs.getString("platlr") !=null)? rs.getString("platlr"):"");
        datos.setCreation_date( (rs.getString("creation_date") !=null)? validarFecha(rs.getString("creation_date")):"");
        if(rs.getString("printer_date") !=null){
            pd=rs.getString("printer_date");
            if(pd.equals("0099-01-01 00:00:00")){
                datos.setPrinter_date("No Impresa");
            }
            else{
                datos.setPrinter_date(pd);
            }
        }
        else{
            datos.setPrinter_date("");
        }
        datos.setFechaanticipo( (rs.getString("fechaanticipo") !=null)? validarFecha(rs.getString("fechaanticipo")):"");
        datos.setVlr( (rs.getString("vlr") !=null)? rs.getString("vlr"):"");
        datos.setDif( (rs.getString("dif") !=null)? rs.getString("dif"):"");
        datos.setDocument( (rs.getString("document") !=null)? rs.getString("document"):"");
        datos.setFecha_cheque( (rs.getString("fecha_cheque") !=null)? validarFecha(rs.getString("fecha_cheque")):"");
        datos.setRuta_pla( (rs.getString("ruta_pla") !=null)? rs.getString("ruta_pla"):"");
        datos.setOrigenoc((rs.getString("origenoc") !=null)? rs.getString("origenoc"):"");
        datos.setDestinooc( (rs.getString("destinooc") !=null)? rs.getString("destinooc"):"");
        datos.setNumrem( (rs.getString("numrem") !=null)? rs.getString("numrem"):"");
        datos.setVlrrem2( (rs.getString("vlrrem2") !=null)? rs.getString("vlrrem2"):"");
        datos.setCliente( (rs.getString("cliente")!=null)? rs.getString("cliente"):"");
        datos.setFacturacliente( (rs.getString("facturacliente")!=null)? rs.getString("facturacliente"):"");
        datos.setAgenciaanticipo( (rs.getString("agenciaanticipo") !=null)? rs.getString("agenciaanticipo"):"");
        datos.setCedulaanticipo( (rs.getString("cedulaanticipo")!=null)? rs.getString("cedulaanticipo"):"");
        datos.setNombreanticipo((rs.getString("nombreanticipo") !=null)?rs.getString("nombreanticipo"):"");
        datos.setFechacumplido((rs.getString("fechacumplido")!=null)? validarFecha(rs.getString("fechacumplido")):"");
        datos.setCedulacumplido((rs.getString("cedulacumplido")!=null)? rs.getString("cedulacumplido"):"");
        datos.setNombrecumplido((rs.getString("nombrecumplido")!=null)?rs.getString("nombrecumplido"):"");
        datos.setValorflete((rs.getString("valorflete")!=null)? rs.getString("valorflete"):"");
        datos.setClasificacion((rs.getString("clasificacion")!=null)? rs.getString("clasificacion"):"");
        datos.setTipo_viaje((rs.getString("tipo_viaje")!=null)? rs.getString("tipo_viaje"):"");
        datos.setProducto((rs.getString("producto")!=null)? rs.getString("producto"):"");
        datos.setDescripcion_via((rs.getString("descripcion_via")!=null)? rs.getString("descripcion_via"):"");
        datos.setPuestocontrol((rs.getString("puestocontrol")!=null)? rs.getString("puestocontrol"):"");
        datos.setCiudad( (rs.getString("ciudad")!=null)? rs.getString("ciudad"):"");
        datos.setCodCliente((rs.getString("codCliente")!=null)? rs.getString("codCliente"):"");
        datos.setNomCliente((rs.getString("nomCliente")!=null)? rs.getString("nomCliente"):"");
        datos.setDias((rs.getString("dias")!=null)? rs.getString("dias"):"");
        datos.setItem((rs.getString("item")!=null)? rs.getString("item"):"");
        datos.setAgc_origen( (rs.getString("agc_origen")!=null)? rs.getString("agc_origen"):"");
        datos.setAgc_destino( (rs.getString("agc_destino")!=null)? rs.getString("agc_destino"):"");
        datos.setDesc_ruta(( (rs.getString("desc_ruta")!=null)? rs.getString("desc_ruta"):""));
        datos.setPais(( (rs.getString("pais")!=null)? rs.getString("pais"):""));
        
        return datos;
    }
    
   
    
    public static String esNull(String valor){
        
        return valor == null? "":valor;
    }
    
    
    /**
     * Getter for property agcpla.
     * @return Value of property agcpla.
     */
    public java.lang.String getAgcpla() {
        return agcpla;
    }
    
    /**
     * Setter for property agcpla.
     * @param agcpla New value of property agcpla.
     */
    public void setAgcpla(java.lang.String agcpla) {
        this.agcpla = agcpla;
    }
    
    /**
     * Getter for property agenciaanticipo.
     * @return Value of property agenciaanticipo.
     */
    public java.lang.String getAgenciaanticipo() {
        return agenciaanticipo;
    }
    
    /**
     * Setter for property agenciaanticipo.
     * @param agenciaanticipo New value of property agenciaanticipo.
     */
    public void setAgenciaanticipo(java.lang.String agenciaanticipo) {
        this.agenciaanticipo = agenciaanticipo;
    }
    
    /**
     * Getter for property cedulaanticipo.
     * @return Value of property cedulaanticipo.
     */
    public java.lang.String getCedulaanticipo() {
        return cedulaanticipo;
    }
    
    /**
     * Setter for property cedulaanticipo.
     * @param cedulaanticipo New value of property cedulaanticipo.
     */
    public void setCedulaanticipo(java.lang.String cedulaanticipo) {
        this.cedulaanticipo = cedulaanticipo;
    }
    
    /**
     * Getter for property cedulacumplido.
     * @return Value of property cedulacumplido.
     */
    public java.lang.String getCedulacumplido() {
        return cedulacumplido;
    }
    
    /**
     * Setter for property cedulacumplido.
     * @param cedulacumplido New value of property cedulacumplido.
     */
    public void setCedulacumplido(java.lang.String cedulacumplido) {
        this.cedulacumplido = cedulacumplido;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.lang.String getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(java.lang.String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property descripcion_via.
     * @return Value of property descripcion_via.
     */
    public java.lang.String getDescripcion_via() {
        return descripcion_via;
    }
    
    /**
     * Setter for property descripcion_via.
     * @param descripcion_via New value of property descripcion_via.
     */
    public void setDescripcion_via(java.lang.String descripcion_via) {
        this.descripcion_via = descripcion_via;
    }
    
    /**
     * Getter for property despachador.
     * @return Value of property despachador.
     */
    public java.lang.String getDespachador() {
        return despachador;
    }
    
    /**
     * Setter for property despachador.
     * @param despachador New value of property despachador.
     */
    public void setDespachador(java.lang.String despachador) {
        this.despachador = despachador;
    }
    
    /**
     * Getter for property destinooc.
     * @return Value of property destinooc.
     */
    public java.lang.String getDestinooc() {
        return destinooc;
    }
    
    /**
     * Setter for property destinooc.
     * @param destinooc New value of property destinooc.
     */
    public void setDestinooc(java.lang.String destinooc) {
        this.destinooc = destinooc;
    }
    
    /**
     * Getter for property dif.
     * @return Value of property dif.
     */
    public java.lang.String getDif() {
        return dif;
    }
    
    /**
     * Setter for property dif.
     * @param dif New value of property dif.
     */
    public void setDif(java.lang.String dif) {
        this.dif = dif;
    }
    
    /**
     * Getter for property document.
     * @return Value of property document.
     */
    public java.lang.String getDocument() {
        return document;
    }
    
    /**
     * Setter for property document.
     * @param document New value of property document.
     */
    public void setDocument(java.lang.String document) {
        this.document = document;
    }
    
    /**
     * Getter for property facturacliente.
     * @return Value of property facturacliente.
     */
    public java.lang.String getFacturacliente() {
        return facturacliente;
    }
    
    /**
     * Setter for property facturacliente.
     * @param facturacliente New value of property facturacliente.
     */
    public void setFacturacliente(java.lang.String facturacliente) {
        this.facturacliente = facturacliente;
    }
    
    /**
     * Getter for property fecha_cheque.
     * @return Value of property fecha_cheque.
     */
    public java.lang.String getFecha_cheque() {
        return fecha_cheque;
    }
    
    /**
     * Setter for property fecha_cheque.
     * @param fecha_cheque New value of property fecha_cheque.
     */
    public void setFecha_cheque(java.lang.String fecha_cheque) {
        this.fecha_cheque = fecha_cheque;
    }
    
    /**
     * Getter for property fechaanticipo.
     * @return Value of property fechaanticipo.
     */
    public java.lang.String getFechaanticipo() {
        return fechaanticipo;
    }
    
    /**
     * Setter for property fechaanticipo.
     * @param fechaanticipo New value of property fechaanticipo.
     */
    public void setFechaanticipo(java.lang.String fechaanticipo) {
        this.fechaanticipo = fechaanticipo;
    }
    
    /**
     * Getter for property fechacumplido.
     * @return Value of property fechacumplido.
     */
    public java.lang.String getFechacumplido() {
        return fechacumplido;
    }
    
    /**
     * Setter for property fechacumplido.
     * @param fechacumplido New value of property fechacumplido.
     */
    public void setFechacumplido(java.lang.String fechacumplido) {
        this.fechacumplido = fechacumplido;
    }
    
    /**
     * Getter for property nombreanticipo.
     * @return Value of property nombreanticipo.
     */
    public java.lang.String getNombreanticipo() {
        return nombreanticipo;
    }
    
    /**
     * Setter for property nombreanticipo.
     * @param nombreanticipo New value of property nombreanticipo.
     */
    public void setNombreanticipo(java.lang.String nombreanticipo) {
        this.nombreanticipo = nombreanticipo;
    }
    
    /**
     * Getter for property nombrecumplido.
     * @return Value of property nombrecumplido.
     */
    public java.lang.String getNombrecumplido() {
        return nombrecumplido;
    }
    
    /**
     * Setter for property nombrecumplido.
     * @param nombrecumplido New value of property nombrecumplido.
     */
    public void setNombrecumplido(java.lang.String nombrecumplido) {
        this.nombrecumplido = nombrecumplido;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property origenoc.
     * @return Value of property origenoc.
     */
    public java.lang.String getOrigenoc() {
        return origenoc;
    }
    
    /**
     * Setter for property origenoc.
     * @param origenoc New value of property origenoc.
     */
    public void setOrigenoc(java.lang.String origenoc) {
        this.origenoc = origenoc;
    }
    
    /**
     * Getter for property platlr.
     * @return Value of property platlr.
     */
    public java.lang.String getPlatlr() {
        return platlr;
    }
    
    /**
     * Setter for property platlr.
     * @param platlr New value of property platlr.
     */
    public void setPlatlr(java.lang.String platlr) {
        this.platlr = platlr;
    }
    
    /**
     * Getter for property printer_date.
     * @return Value of property printer_date.
     */
    public java.lang.String getPrinter_date() {
        return printer_date;
    }
    
    /**
     * Setter for property printer_date.
     * @param printer_date New value of property printer_date.
     */
    public void setPrinter_date(java.lang.String printer_date) {
        this.printer_date = printer_date;
    }
    
    /**
     * Getter for property producto.
     * @return Value of property producto.
     */
    public java.lang.String getProducto() {
        return producto;
    }
    
    /**
     * Setter for property producto.
     * @param producto New value of property producto.
     */
    public void setProducto(java.lang.String producto) {
        this.producto = producto;
    }
    
    /**
     * Getter for property puestocontrol.
     * @return Value of property puestocontrol.
     */
    public java.lang.String getPuestocontrol() {
        return puestocontrol;
    }
    
    /**
     * Setter for property puestocontrol.
     * @param puestocontrol New value of property puestocontrol.
     */
    public void setPuestocontrol(java.lang.String puestocontrol) {
        this.puestocontrol = puestocontrol;
    }
    
    /**
     * Getter for property ruta_pla.
     * @return Value of property ruta_pla.
     */
    public java.lang.String getRuta_pla() {
        return ruta_pla;
    }
    
    /**
     * Setter for property ruta_pla.
     * @param ruta_pla New value of property ruta_pla.
     */
    public void setRuta_pla(java.lang.String ruta_pla) {
        this.ruta_pla = ruta_pla;
    }
    
    /**
     * Getter for property tipo_viaje.
     * @return Value of property tipo_viaje.
     */
    public java.lang.String getTipo_viaje() {
        return tipo_viaje;
    }
    
    /**
     * Setter for property tipo_viaje.
     * @param tipo_viaje New value of property tipo_viaje.
     */
    public void setTipo_viaje(java.lang.String tipo_viaje) {
        this.tipo_viaje = tipo_viaje;
    }
    
    /**
     * Getter for property valorflete.
     * @return Value of property valorflete.
     */
    public java.lang.String getValorflete() {
        return valorflete;
    }
    
    /**
     * Setter for property valorflete.
     * @param valorflete New value of property valorflete.
     */
    public void setValorflete(java.lang.String valorflete) {
        this.valorflete = valorflete;
    }
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public java.lang.String getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(java.lang.String vlr) {
        this.vlr = vlr;
    }
    
    /**
     * Getter for property vlrpla2.
     * @return Value of property vlrpla2.
     */
    public java.lang.String getVlrpla2() {
        return vlrpla2;
    }
    
    /**
     * Setter for property vlrpla2.
     * @param vlrpla2 New value of property vlrpla2.
     */
    public void setVlrpla2(java.lang.String vlrpla2) {
        this.vlrpla2 = vlrpla2;
    }
    
    /**
     * Getter for property vlrrem2.
     * @return Value of property vlrrem2.
     */
    public java.lang.String getVlrrem2() {
        return vlrrem2;
    }
    
    /**
     * Setter for property vlrrem2.
     * @param vlrrem2 New value of property vlrrem2.
     */
    public void setVlrrem2(java.lang.String vlrrem2) {
        this.vlrrem2 = vlrrem2;
    }
    
    /**
     * Getter for property discrepancia.
     * @return Value of property discrepancia.
     */
    public java.lang.String getDiscrepancia() {
        return discrepancia;
    }
    
    /**
     * Setter for property discrepancia.
     * @param discrepancia New value of property discrepancia.
     */
    public void setDiscrepancia(java.lang.String discrepancia) {
        this.discrepancia = discrepancia;
    }
    
    /**
     * Getter for property ocasociadas.
     * @return Value of property ocasociadas.
     */
    public java.lang.String getOcasociadas() {
        return ocasociadas;
    }
    
    /**
     * Setter for property ocasociadas.
     * @param ocasociadas New value of property ocasociadas.
     */
    public void setOcasociadas(java.lang.String ocasociadas) {
        this.ocasociadas = ocasociadas;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property contaroc.
     * @return Value of property contaroc.
     */
    public java.lang.String getContaroc() {
        return contaroc;
    }
    
    /**
     * Setter for property contaroc.
     * @param contaroc New value of property contaroc.
     */
    public void setContaroc(java.lang.String contaroc) {
        this.contaroc = contaroc;
    }
    
    /**
     * Getter for property rango.
     * @return Value of property rango.
     */
    public java.lang.String getRango() {
        return rango;
    }
    
    /**
     * Setter for property rango.
     * @param rango New value of property rango.
     */
    public void setRango(java.lang.String rango) {
        this.rango = rango;
    }
    
    /**
     * Getter for property calificacion.
     * @return Value of property calificacion.
     */
    public java.lang.String getCalificacion() {
        return calificacion;
    }
    
    /**
     * Setter for property calificacion.
     * @param calificacion New value of property calificacion.
     */
    public void setCalificacion(java.lang.String calificacion) {
        this.calificacion = calificacion;
    }
    
    /**
     * Getter for property codCliente.
     * @return Value of property codCliente.
     */
    public java.lang.String getCodCliente() {
        return codCliente;
    }
    
    /**
     * Setter for property codCliente.
     * @param codCliente New value of property codCliente.
     */
    public void setCodCliente(java.lang.String codCliente) {
        this.codCliente = codCliente;
    }
    
    /**
     * Getter for property nomCliente.
     * @return Value of property nomCliente.
     */
    public java.lang.String getNomCliente() {
        return nomCliente;
    }
    
    /**
     * Setter for property nomCliente.
     * @param nomCliente New value of property nomCliente.
     */
    public void setNomCliente(java.lang.String nomCliente) {
        this.nomCliente = nomCliente;
    }
    
    /**
     * Getter for property dias.
     * @return Value of property dias.
     */
    public java.lang.String getDias() {
        return dias;
    }
    
    /**
     * Setter for property dias.
     * @param dias New value of property dias.
     */
    public void setDias(java.lang.String dias) {
        this.dias = dias;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    public static String validarFecha(String fecha){
        return fecha!=null&&!fecha.equals("0099-01-01 00:00:00")? fecha:"";
    }
    
     public static String validarFecha2(String fecha){
        return fecha!=null&&!fecha.equals("0099-01-01")? fecha:"";
    }
    
    /**
     * Getter for property agc_origen.
     * @return Value of property agc_origen.
     */
    public java.lang.String getAgc_origen() {
        return agc_origen;
    }
    
    /**
     * Setter for property agc_origen.
     * @param agc_origen New value of property agc_origen.
     */
    public void setAgc_origen(java.lang.String agc_origen) {
        this.agc_origen = agc_origen;
    }
    
    /**
     * Getter for property agc_destino.
     * @return Value of property agc_destino.
     */
    public java.lang.String getAgc_destino() {
        return agc_destino;
    }
    
    /**
     * Setter for property agc_destino.
     * @param agc_destino New value of property agc_destino.
     */
    public void setAgc_destino(java.lang.String agc_destino) {
        this.agc_destino = agc_destino;
    }
    
    /**
     * Getter for property desc_ruta.
     * @return Value of property desc_ruta.
     */
    public java.lang.String getDesc_ruta() {
        return desc_ruta;
    }
    
    /**
     * Setter for property desc_ruta.
     * @param desc_ruta New value of property desc_ruta.
     */
    public void setDesc_ruta(java.lang.String desc_ruta) {
        this.desc_ruta = desc_ruta;
    }
    
    /**
     * Getter for property pais.
     * @return Value of property pais.
     */
    public java.lang.String getPais() {
        return pais;
    }
    
    /**
     * Setter for property pais.
     * @param pais New value of property pais.
     */
    public void setPais(java.lang.String pais) {
        this.pais = pais;
    }
    
}
