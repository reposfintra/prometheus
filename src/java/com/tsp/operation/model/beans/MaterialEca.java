package com.tsp.operation.model.beans;

public class MaterialEca {

    private String descripcion;
    private String unidad;
    private String cantidad;
    private String vlr_unitario;
    private String vlr_total;

    public MaterialEca(){
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad; 
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public void setVlr_total(String vlr_total) {
        this.vlr_total = vlr_total;
    }

    public void setVlr_unitario(String vlr_unitario) {
        this.vlr_unitario = vlr_unitario;
    }

    public String getCantidad() {
        return cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getUnidad() {
        return unidad;
    }

    public String getVlr_total() {
        return vlr_total;
    }

    public String getVlr_unitario() {
        return vlr_unitario;
    }
}

