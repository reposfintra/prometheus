/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Harold Cuello G.
 */
public class SolicitudTransacciones {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;

    private String id_persona;
    private String tipo;

    private String transacciones_al_extanjero, importaciones, exportaciones, inversiones, giros, prestamos,
            otras_transacciones, pago_cuenta_exterior, banco, cuenta, pais,
            ciudad, moneda, tipo_pro, monto;

    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;

    public SolicitudTransacciones load(ResultSet rs) throws SQLException {
        SolicitudTransacciones st = new SolicitudTransacciones();
        st.setNumeroSolicitud(rs.getString("numero_solicitud"));
        st.setTipo(rs.getString("tipo"));
        st.setId_persona(rs.getString("id_persona"));
        
        st.setTransacciones_al_extanjero(rs.getString("transacciones_al_extanjero"));
        st.setImportaciones(rs.getString("importaciones")); 
        st.setExportaciones(rs.getString("exportaciones"));
        st.setInversiones(rs.getString("inversiones"));
        st.setGiros(rs.getString("giros"));
        st.setPrestamos(rs.getString("prestamos"));
        st.setOtras_transacciones(rs.getString("otras_transacciones"));
        st.setPago_cuenta_exterior(rs.getString("pago_cuenta_exterior"));
        st.setBanco(rs.getString("banco"));
        st.setCiudad(rs.getString("ciudad"));
        st.setCuenta(rs.getString("cuenta"));
        st.setPais(rs.getString("pais"));
        st.setMoneda(rs.getString("moneda"));
        st.setTipo_pro(rs.getString("tipo_pro"));
        st.setMonto(rs.getString("monto"));
        
        return st;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getId_persona() {
        return id_persona;
    }

    public void setId_persona(String id_persona) {
        this.id_persona = id_persona;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTransacciones_al_extanjero() {
        return transacciones_al_extanjero;
    }

    public void setTransacciones_al_extanjero(String transacciones_al_extanjero) {
        this.transacciones_al_extanjero = transacciones_al_extanjero;
    }

    public String getImportaciones() {
        return importaciones;
    }

    public void setImportaciones(String importaciones) {
        this.importaciones = importaciones;
    }

    public String getExportaciones() {
        return exportaciones;
    }

    public void setExportaciones(String exportaciones) {
        this.exportaciones = exportaciones;
    }

    public String getInversiones() {
        return inversiones;
    }

    public void setInversiones(String inversiones) {
        this.inversiones = inversiones;
    }

    public String getGiros() {
        return giros;
    }

    public void setGiros(String giros) {
        this.giros = giros;
    }

    public String getPrestamos() {
        return prestamos;
    }

    public void setPrestamos(String prestamos) {
        this.prestamos = prestamos;
    }

    public String getOtras_transacciones() {
        return otras_transacciones;
    }

    public void setOtras_transacciones(String otras_transacciones) {
        this.otras_transacciones = otras_transacciones;
    }

    public String getPago_cuenta_exterior() {
        return pago_cuenta_exterior;
    }

    public void setPago_cuenta_exterior(String pago_cuenta_exterior) {
        this.pago_cuenta_exterior = pago_cuenta_exterior;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getTipo_pro() {
        return tipo_pro;
    }

    public void setTipo_pro(String tipo_pro) {
        this.tipo_pro = tipo_pro;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

}
