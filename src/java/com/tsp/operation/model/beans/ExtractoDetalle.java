/**************************************************************************
 * Nombre:        ExtractoDetalle.java                     
 * Descripci�n:   Beans de acuerdo especial.               *
 * Autor:         Ing. Julio Barros   *
 * Fecha:         20 de noviembre de 2006, 09:06 AM        *
 * Versi�n:       Java  1.0                                      *
 * Copyright:     Fintravalores S.A. S.A.                                *
 **************************************************************************/


package com.tsp.operation.model.beans;

public class ExtractoDetalle {
    
    private String reg_status;
    private String dstrct;
    private String nit;
    private String fecha;
    private String tipo_documento;
    private String documento; 
    private String concepto; 
    private String descripcion; 
    private String factura; 
    private float vlr;
    private float retefuente;
    private float reteica;
    private float impuestos;
    private float vlr_pp_item;
    private float vlr_ppa_item;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String base;
    
    
    private String fecdsp;
    private String oripla;
    private String despla;
    private String plaveh;
    private String nom;
    private String nomc;
    private String numrem;
    private String stapla;
    private String pesoreal;
    private String unit_vlr;
    
    private int secuencia; 
    private String placa;
    
    private String remision;
    
    
    
    /** Creates a new instance of ExtractoDetalle */
    public ExtractoDetalle() {
    }
    
    
    
    // set's
    public void setReg_status(String reg_status){
        this.reg_status=reg_status;
    }
    
    public void  setDstrct( String dstrct ){
        this.dstrct=dstrct;
    }
    
    public void  setNit( String nit ){ 
        this.nit=nit;
    }
    
    public void  setFecha( String fecha ){ 
        this.fecha=fecha;
    }
    
    public void  setTipo_documento( String tipo_documento ){ 
        this.tipo_documento=tipo_documento;
    }
    public void  setDocumento( String documento ){ 
        this.documento=documento;
    } 
    
    public void  setConcepto( String concepto ){ 
        this.concepto=concepto;
    }
    
    public void  setDescripcion( String descripcion ){ 
        this.descripcion=descripcion;
    } 
    
    public void  setFactura( String factura ){ 
        this.factura=factura;
    } 
    
    public void  setVlr( float vlr ){ 
        this.vlr=vlr;
    }
    
    public void  setRetefuente( float retefuente ){ 
        this.retefuente=retefuente;
    }
    
    public void  setReteica( float reteica ){ 
        this.reteica=reteica;
    }
    
    public void  setImpuestos( float impuestos ){ 
        this.impuestos=impuestos;
    }
    
    public void  setVlr_pp_item( float vlr_pp_item ){ 
        this.vlr_pp_item=vlr_pp_item;
    }
    
    public void  setVlr_ppa_item( float vlr_ppa_item ){ 
        this.vlr_ppa_item=vlr_ppa_item;
    }
    
    public void  setCreation_user( String creation_user ){ 
        this.creation_user=creation_user;
    }
   
    public void  setCreation_date( String creation_date ){  
        this.creation_date=creation_date;
    } 
    
    public void  setUser_update( String user_update ){  
        this.user_update=user_update;
    } 
    
    public void  setLast_update( String last_update){  
        this.last_update=last_update;
    }
    
    public void  setBase( String base){  
        this.base=base;
    }
    
    //para la consulta de datos
    public void setFecdsp( String fecdsp){
        this.fecdsp=fecdsp;
    }
    
    public void setOripla( String oripla){         
        this.oripla=oripla;     
    }
    
    public void setDespla( String despla){         
        this.despla=despla;     
    }
    
    public void setPlaveh( String plaveh){         
        this.plaveh=plaveh;     
    }
    
    public void setNom( String nom){         
        this.nom=nom;    
    }
    
    public void setNomc( String nomc){         
        this.nomc=nomc;     
    }
    
    public void setNumrem( String numrem){         
        this.numrem=numrem;     
    }
    
    public void setStapla( String stapla){         
        this.stapla=stapla;     
    }
    
    public void setPesoreal( String pesoreal){         
        this.pesoreal=pesoreal;     
    }
    
    public void setUnit_vlr( String unit_vlr){         
        this.unit_vlr=unit_vlr;     
    }
    
    //get's
    
    public String getReg_status(){
        return this.reg_status;
    }
    
    public String  getDstrct(){
        return this.dstrct;
    }
    
     public String  getNit(){ 
        return this.nit;
    }
    
    public String  getFecha(){ 
        return this.fecha;
    }
    
    public String  getTipo_documento(){ 
        return this.tipo_documento;
    }
    public String  getDocumento(){
        return this.documento;
    } 
    
    public String  getConcepto(){
        return this.concepto;
    }
    
    public String  getDescripcion(){
        return this.descripcion;
    } 
    
    public String  getFactura(){
        return this.factura;
    } 
    
    public float  getVlr(){
        return this.vlr;
    }
    
    public float  getRetefuente(){
        return this.retefuente;
    }
    
    public float  getReteica(){
        return this.reteica;
    }
    
    public float  getImpuestos(){
        return this.impuestos;
    }
    
    public float  getVlr_pp_item(){
        return this.vlr_pp_item;
    }
    
    public float  getVlr_ppa_item(){
        return this.vlr_ppa_item;
    }
    
    public String  getCreation_user(){
        return this.creation_user;
    }
   
    public String  getCreation_date(){ 
        return this.creation_date;
    } 
    
    public String  getUser_update(){ 
        return this.user_update;
    } 
    
    public String  getLast_update(){ 
        return this.last_update;
    }
    
    public String  getBase(){ 
        return this.base;
    }
    
    
    
    //para la consulta de datos
    public String getFecdsp(){
        return this.fecdsp;
    }
    
    public String getOripla(){         
        return this.oripla;     
    }
    
    public String getDespla(){         
        return this.despla;     
    }
    
    public String getPlaveh(){         
        return this.plaveh;     
    }
    
    public String getNom(){         
        return this.nom;    
    }
    
    public String getNomc(){         
        return this.nomc;     
    }
    
    public String getNumrem(){         
        return this.numrem;     
    }
    
    public String getStapla(){         
        return this.stapla;     
    }
    
    public String getPesoreal(){         
        return this.pesoreal;     
    }
    
    public String getUnit_vlr(){         
        return this.unit_vlr;     
    }
    
    /**
     * Getter for property secuencia.
     * @return Value of property secuencia.
     */
    public int getSecuencia() {
        return secuencia;
    }
    
    /**
     * Setter for property secuencia.
     * @param secuencia New value of property secuencia.
     */
    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
}
