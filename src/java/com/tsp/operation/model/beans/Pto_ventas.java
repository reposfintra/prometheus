/*
 * Pto_ventas.java
 *
 * Created on 26 de mayo de 2005, 11:49 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  DIOGENES
 */
public class Pto_ventas implements Serializable {
    private String dstrct_code;
    private String std_job_no;    
    private String ano;
    private String mes;
    private String cliente;
    private String dia;
    private int viaje;
    private String estado;
    private String Usuario_creacion;
    private java.util.Date fecha_creacion;
    private String Usuario_actualizacion;
    private java.util.Date fecha_actualizacion;
    
    /** Creates a new instance of Pto_ventas */
    public void setdstrct_code (String dstrct_code){
        this.dstrct_code=dstrct_code;
    }
    public void setstd_job_no (String std_job_no){
        this.std_job_no=std_job_no;
    }
    public void setano (String ano){
        this.ano=ano;
    }
    public void setmes (String mes){
        this.mes=mes;
    }
    
    public void setcliente (String cliente){
        this.cliente=cliente;
    }
    
    public void setdia (String dia){
        this.dia=dia;
    }
    public void setviaje (int viaje){
        this.viaje=viaje;
    }
    public void setestado (String estado){
        this.estado=estado;
    }
    public void setUsuario_creacion (String Usuario_creacion){
        this.Usuario_creacion=Usuario_creacion;
    }
    public void setfecha_creacion (java.util.Date fecha_creacion){
        this.fecha_creacion=fecha_creacion;
    }
    public void setUsuario_actualizacion (String Usuario_actualizacion){
        this.Usuario_actualizacion=Usuario_actualizacion;
    }
    public void setfecha_actualizacion (java.util.Date fecha_actualizacion){
        this.fecha_actualizacion=fecha_actualizacion;
    }
    //**********
    public String getdstrct_code ( ){
        return dstrct_code;
    }
    public String getstd_job_no (){
        return std_job_no;
    }
    public String getano (){
        return ano;
    }
    public String getmes (){
        return mes;
    }
    
    public String getcliente (){
        return cliente;
    }
    
    public String getdia (){
        return dia;
    }
    public int getviaje (){
        return viaje;
    }
    public String getestado (){
        return estado;
    }
    public String getUsuario_creacion (){
        return Usuario_creacion;
    }
    public java.util.Date getfecha_creacion (){
        return fecha_creacion;
    }
    public String getUsuario_actualizacion (){
        return Usuario_actualizacion;
    }
    public java.util.Date getfecha_actualizacion (){
        return fecha_actualizacion;
    }
}
