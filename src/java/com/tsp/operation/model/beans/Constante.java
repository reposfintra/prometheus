/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class Constante {


    private String codigo;
    private String descripcion;
    private String valor;


    public Constante(){

    }





    public static Constante load(java.sql.ResultSet rs)throws java.sql.SQLException{

        Constante constante = new Constante();

        constante.setCodigo( rs.getString("codigo") );
        constante.setDescripcion( rs.getString("descripcion") );
        constante.setValor(rs.getString("valor") );

        return constante;

    }






    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(String valor) {
        this.valor = valor;
    }


}
