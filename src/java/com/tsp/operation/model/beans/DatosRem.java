
package com.tsp.operation.model.beans;


public class DatosRem {
    private String cliente;
    private String remesa;
    private String fechaCumplido;
    private String agenciaCumplido;
    private String stj;
    private String stj_desc;
    private String unidad;
    private String noRemision;
    private float tarifa=0;
    private String estado;
    public DatosRem() {
    }
    
    // SET
    
    public void setNoRemision(String no){
        this.noRemision = no;
    }
    
    public void setCliente(String customer){
        this.cliente = customer;
    }
    
    public void setRemesa(String rem){
        this.remesa = rem;
    }
    
    public void setFechaCumplido(String fecha){
        this.fechaCumplido = fecha;
    }
    
    public void setAgenciaCumplido(String agencia){
        this.agenciaCumplido=agencia;
    }
    
    public void setStj(String standar){
        this.stj = standar;
    }
    
    public void setDescripcion(String des){
        this.stj_desc = des;
    }
    
    public void setUnidad(String unidad){
        this.unidad = unidad;
    }
    
    // GET
    
    public String getNoRemision(){
        return this.noRemision ;
    }
    
    public String getCliente(){
        return this.cliente;
    }
    
    public String getRemesa(){
        return this.remesa;
    }
    
    public String getFechaCumplido(){
        return this.fechaCumplido;
    }
    
    public String getAgenciaCumplido(){
        return this.agenciaCumplido;
    }
    
    public String getStj(){
        return this.stj;
    }
    
    public String getDescripcion(){
        return this.stj_desc;
    }
    
    public String getUnidad(){
        return this.unidad;
    }
    
    /**
     * Getter for property tarifa.
     * @return Value of property tarifa.
     */
    public float getTarifa() {
        return tarifa;
    }
    
    /**
     * Setter for property tarifa.
     * @param tarifa New value of property tarifa.
     */
    public void setTarifa(float tarifa) {
        this.tarifa = tarifa;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
}
