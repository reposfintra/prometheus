/****************************************************************************************
 * Nombre clase : ............... FlotaUtilizada.java                                   *
 * Descripcion :................. Bean utilizado para el reporte de extractos.          * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                          *
 * Fecha :....................... 01 de enero de 2006, 08:00 AM                         *
 * Version :..................... 1.0                                                   *
 * Copyright :................... Fintravalores S.A.                               *
 ****************************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;


public class EncFactura implements Serializable{
 
    private String nc;//Nota credito
    private String po_no;//Planilla
    private String remision;//Remesa
    private String fecha;
    private String conductor;
    private String carga;
    private String total;
    private String origen;
    private String destino;
    
    /** Creates a new instance of EncFactura */
    public EncFactura() {
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property nc.
     * @return Value of property nc.
     */
    public java.lang.String getNc() {
        return nc;
    }
    
    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
    /**
     * Getter for property total.
     * @return Value of property total.
     */
    public java.lang.String getTotal() {
        return total;
    }
    
    /**
     * Setter for property total.
     * @param total New value of property total.
     */
    public void setTotal(java.lang.String total) {
        this.total = total;
    }
    
    /**
     * Setter for property nc.
     * @param nc New value of property nc.
     */
    public void setNc(java.lang.String nc) {
        this.nc = nc;
    }
    
    /**
     * Getter for property carga.
     * @return Value of property carga.
     */
    public java.lang.String getCarga() {
        return carga;
    }
    
    /**
     * Setter for property carga.
     * @param carga New value of property carga.
     */
    public void setCarga(java.lang.String carga) {
        this.carga = carga;
    }
    
    /**
     * Getter for property po_no.
     * @return Value of property po_no.
     */
    public java.lang.String getPo_no() {
        return po_no;
    }
    
    /**
     * Setter for property po_no.
     * @param po_no New value of property po_no.
     */
    public void setPo_no(java.lang.String po_no) {
        this.po_no = po_no;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
}
