/***********************************************************************************
 * Nombre clase : ............... Stdjob_tbldoc.java                               *
 * Descripcion :................. Clase que maneja los atributos relacionados con  *
 *                                la tabla Stdjob_tbldoc                           *                              
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 27 de octubre de 2005, 10:33 AM                 *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;

public class Stdjob_tbldoc {
        
        /*
         *DECLARACION DE LOS ATRIBUTOS
         */
        
        private String reg_status;
        private String dstrct;
        private String document_type;
        private String std_job_no;
        private String last_update;
        private String user_update;
        private String creation_date;
        private String creation_user;
        
        /** Creates a new instance of Stdjob_tbldoc */
        public Stdjob_tbldoc() {
        }
        
        
         /**
         * Metodo Load, recibe una variable de tipo ResultSet,
         * permite cargar los atributos de un objeto de tipo Stdjob_tbldoc 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : ResultSet rs
         * @version : 1.0
         */
        public static Stdjob_tbldoc load(ResultSet rs)throws Exception{
                Stdjob_tbldoc datos = new Stdjob_tbldoc();
                datos.setReg_status(rs.getString("REG_STATUS"));
                datos.setDocument_type(rs.getString("DOCUMENT_TYPE"));
                datos.setStd_job_no(rs.getString("STD_JOB_NO"));
                datos.setDstrct(rs.getString("dstrct"));
                return datos;
        }
        
        /**
         * Metodo getCreation_date, retorna el valor del atributo creation_date, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getCreation_date() {
                return creation_date;
        }
        
       /**
         * Metodo setCreation_date, setea el valor del atributo creation_date, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String fecha de creacion
         * @version : 1.0
         */
        public void setCreation_date(java.lang.String creation_date) {
                this.creation_date = creation_date;
        }
        
        /**
         * Metodo getCreation_user, retorna el valor del atributo creation_user, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getCreation_user() {
                return creation_user;
        }
        
       /**
         * Metodo setCreation_user, setea el valor del atributo creation_date, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String usuario de creacion
         * @version : 1.0
         */
        public void setCreation_user(java.lang.String creation_user) {
                this.creation_user = creation_user;
        }
        
         /**
         * Metodo getDocument_type, retorna el valor del atributo document_type, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getDocument_type() {
                return document_type;
        }
        
        /**
         * Metodo setDocument_type, setea el valor del atributo document_type, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String tipo de documento
         * @version : 1.0
         */
        public void setDocument_type(java.lang.String document_type) {
                this.document_type = document_type;
        }
        
         /**
         * Metodo getDstrct, retorna el valor del atributo dstrct, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getDstrct() {
                return dstrct;
        }
        
        /**
         * Metodo setDstrct, setea el valor del atributo Dstrct, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String distrito
         * @version : 1.0
         */
        public void setDstrct(java.lang.String dstrct) {
                this.dstrct = dstrct;
        }
        
         /**
         * Metodo getLast_update, retorna el valor del atributo last_update, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getLast_update() {
                return last_update;
        }
        
        /**
         * Metodo setLast_update, setea el valor del atributo last_update, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String fecha de modificacion
         * @version : 1.0
         */
        public void setLast_update(java.lang.String last_update) {
                this.last_update = last_update;
        }
        
        /**
         * Metodo getReg_status, retorna el valor del atributo reg_status, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getReg_status() {
                return reg_status;
        }
        
        /**
         * Metodo setReg_status, setea el valor del atributo reg_status, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String reg_status
         * @version : 1.0
         */
        public void setReg_status(java.lang.String reg_status) {
                this.reg_status = reg_status;
        }
        
        /**
         * Metodo getStd_job_no, retorna el valor del atributo Std_job_no, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getStd_job_no() {
                return std_job_no;
        }
        
        /**
         * Metodo setStd_job_no, setea el valor del atributo std_job_no, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String numero de standart
         * @version : 1.0
         */
        public void setStd_job_no(java.lang.String std_job_no) {
                this.std_job_no = std_job_no;
        }
        
        /**
         * Metodo getUser_update, retorna el valor del atributo user_update, 
         * @autor : Ing. Juan Manuel Escandon Perez
         * @version : 1.0
         */
        public java.lang.String getUser_update() {
                return user_update;
        }
        
        /**
         * Metodo setUser_update, setea el valor del atributo user_update
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String usuario de actulizacion
         * @version : 1.0
         */
        public void setUser_update(java.lang.String user_update) {
                this.user_update = user_update;
        }
        
}
