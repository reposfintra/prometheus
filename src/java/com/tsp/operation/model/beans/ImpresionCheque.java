/***************************************
    * Nombre Clase ............. ImpresionCheque.java
    * Descripci�n  .. . . . . .  Permite Encapsular datos del cheques
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  26/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import com.tsp.util.*;

public class ImpresionCheque {

    private int id;
    private int idBanco;
    private String distrito;
    private String agencia;
    private String usuario;
   //inf banco
    private String banco;
    private String agenciaBanco;
    private String cuenta;
    private String cheque;
    private String monedaBanco;
    
    private String fechaMov;
  
    // inf serie 
    private String inicioSerie;
    private int    numero;    // last number de la serie
    private int    tope;
    private String prefijo;
    private double valor;
    private String moneda;
    
    // datos del cheque
    private String ano;
    private String mes;
    private String dia;
    private String beneficiario;
    private String cedula;
    private String planilla;
    private String parafo;    
    
 // datos de posicion para los campos
    private List   positionColumn;   
    private String campos;
    private String posiciones;
    
    
    private boolean  impreso;
    private boolean  selected;
    private boolean  noSerie;
    private List     ListBancos;
    
    
    private String  nombreUsurio;
    private String  nombreAgencia;
    
    //Jose 2006-09-30
    private String propietario;
    
    private String  proveedor_anticipo;
    private String  nombre_propietario;
    
    private boolean activado;
    
    
    private String valoresPlanillas;
    
    
    
    private String  laser  =  "N";
    private int     copias =  1;
    
    /*JEscandon 2006-12-13*/
    private boolean estado_impresion_oc;
    private String auxiliar;
    private String fecha_imp_oc;
    
    
    private int   id_serie;
    
    
    
    public ImpresionCheque() {
       this.impreso          = false;
       this.selected         = false;
       this.campos           = "";
       this.posiciones       = "";
       this.planilla         = "";
       this.valoresPlanillas = "";
       nombreUsurio          = "";
       nombreAgencia         = "";
       
    }
    
    
    
    
    
    
    //SET
    
    
    
    /**
     * Metodo que setea fechaMov
     * @param ........String val
     * @version ...... 1.0
     */ 
    public void setFechaMov(String val){
        this.fechaMov = val;
    }
    
    
    /**
     * Metodo que setea monedaBanco
     * @param ........String val
     * @version ...... 1.0
     */ 
    public void setMonedaBanco(String val){
        this.monedaBanco = val;
    }
    
    
    
    /**
     * Metodo que setea valoresPlanillas
     * @param ........String val
     * @version ...... 1.0
     */ 
    public void setValores(String val){
        this.valoresPlanillas = val;
    }
    
    
    /**
     * Metodo que setea id
     * @param ........int id
     * @version ...... 1.0
     */ 
    public void setId(int id){
        this.id = id;
    }
    
    
    /**
     * Metodo que setea positionColumn
     * @param ........List lista
     * @version ...... 1.0
     */ 
    public void setPositionColumn(List lista){
        this.positionColumn = lista;
    }
    

     /**
     * Metodo que setea idBanco
     * @param ........int id
     * @version ...... 1.0
     */ 
    public void setIdBanco(int id){
        this.idBanco = id;     
    }
    
    
    
     /**
     * Metodo que setea distrito
     * @param ........String distrito
     * @version ...... 1.0
     */ 
    public void setDistrito(String distrito){
        this.distrito = distrito;
    }
    
    
     /**
     * Metodo que setea agencia
     * @param ........String agencia
     * @version ...... 1.0
     */ 
    public void setAgencia(String agencia){
        this.agencia = agencia;
    }
    
    
     /**
     * Metodo que setea banco
     * @param ........ String banco
     * @version ...... 1.0
     */ 
    public void setBanco(String banco){
        this.banco = banco;
    }
    
    
    
     /**
     * Metodo que setea agenciaBanco
     * @param ........ String agency_id
     * @version ...... 1.0
     */ 
    public void setAgenciaBanco(String agency_id){
        this.agenciaBanco = agency_id;
    }
    
    
    
    
    /**
     * Metodo que setea cuenta
     * @param ........ String cuenta
     * @version ...... 1.0
     */ 
    public void setCuenta(String cuenta){
        this.cuenta = cuenta;
    }
    
    
     /**
     * Metodo que setea cheque
     * @param ........ String cheque
     * @version ...... 1.0
     */ 
    public void setCheque(String cheque){
        this.cheque = cheque;
    }
    
    
    
    /**
     * Metodo que setea numero
     * @param ........ int lastNumber
     * @version ...... 1.0
     */ 
    public void setNumber(int lastNumber){
        this.numero = lastNumber;
    }
    
    
    /**
     * Metodo que setea tope
     * @param ........ int tope
     * @version ...... 1.0
     */ 
    public void setTope(int tope){
        this.tope = tope;
    }
    
    
    
    /**
     * Metodo que setea prefijo
     * @param ........ String prefijo
     * @version ...... 1.0
     */ 
    public void setPrefix(String prefijo){
        this.prefijo =prefijo;
    }
    
    
    
    
    
     /**
     * Metodo que setea valor
     * @param ........ double valor
     * @version ...... 1.0
     */ 
    public void setValor(double valor){
        this.valor = valor;
    }
    
    
     /**
     * Metodo que setea moneda
     * @param ........ String moneda
     * @version ...... 1.0
     */ 
    public void setMoneda(String moneda){
        this.moneda = moneda;
    }
    
    
     /**
     * Metodo que setea ano
     * @param ........ String ano
     * @version ...... 1.0
     */ 
    public void setAno(String ano){
        this.ano = ano;
    }
    
    
    /**
     * Metodo que setea mes
     * @param ........ String mes
     * @version ...... 1.0
     */ 
    public void setMes(String mes){
        this.mes = mes;
    }
    
    
     /**
     * Metodo que setea dia
     * @param ........ String dia
     * @version ...... 1.0
     */ 
    public void setDia(String dia){
        this.dia = dia;
    }
    
    
    
     /**
     * Metodo que setea beneficiario
     * @param ........ String beneficiario
     * @version ...... 1.0
     */ 
    public void setBeneficiario(String beneficiario){
        this.beneficiario = beneficiario;
    }
    

    /**
     * Metodo que setea cedula
     * @param ........ String cedula
     * @version ...... 1.0
     */ 
    public void setCedula(String cedula){
        this.cedula = cedula;
    }
    
    
    /**
     * Metodo que setea planilla
     * @param ........ String planilla
     * @version ...... 1.0
     */ 
    public void setPlanilla(String planilla){
       this.planilla = planilla;    
    }
    
    
    
    /**
     * Metodo que setea parafo
     * @param ........ List bancos
     * @version ...... 1.0
     */ 
    public void setParafo(String parafo){
        this.parafo = parafo;
    }
    
    
    
    /**
     * Metodo que setea ListBancos
     * @param ........ List bancos
     * @version ...... 1.0
     */ 
    public void setListBancos(List bancos){
        this.ListBancos=bancos;
    }
    
    
    
     /**
     * Metodo que setea impreso
     * @param ........ boolean estado
     * @version ...... 1.0
     */ 
    public void setImpresion(boolean estado){
        this.impreso = estado;
    }
    
    
    
     /**
     * Metodo que setea selected
     * @param ........ boolean estado
     * @version ...... 1.0
     */ 
    public void setSeleted(boolean estado){
        this.selected = estado;
    }
    
    
    
    
    

     
    
    //GET    
    
    
    
    /**
     * Metodo que devuelve fechaMov
     * @param ........String val
     * @version ...... 1.0
     */ 
    public String  getFechaMov(){
        return this.fechaMov ;
    }
    
    
    /**
     * Metodo que devuelve monedaBanco
     * @param ........String val
     * @version ...... 1.0
     */ 
    public String getMonedaBanco(){
       return this.monedaBanco ;
    }
    
    
    
     /**
     * Metodo que devuelve valoresPlanillas
     * @version ...... 1.0
     */
    public String getValores(){
       return  this.valoresPlanillas ;
    }
    
    
    /**
     * Metodo que devuelve id
     * @version ...... 1.0
     */
    public int getId(){
        return this.id;
    }
    
    
     /**
     * Metodo que devuelve positionColumn
     * @version ...... 1.0
     */
    public List getPositionColumn(){
        return  this.positionColumn;
    }
    
    
     /**
     * Metodo que devuelve campos
     * @version ...... 1.0
     */
    public String getCampos(){
        return this.campos;
    }
    
    
    /**
     * Metodo que devuelve posiciones
     * @version ...... 1.0
     */
    public String getPosiciones(){
        return this.posiciones;
    }
    
    
    
    /**
     * Metodo que devuelve idBanco
     * @version ...... 1.0
     */
    public int getIdBanco(){
       return this.idBanco;     
    }
    
    
    
    /**
     * Metodo que devuelve distrito
     * @version ...... 1.0
     */
    public String getDistrito(){
        return this.distrito;
    }
    
    
    
    /**
     * Metodo que devuelve agencia
     * @version ...... 1.0
     */
    public String getAgencia(){
        return this.agencia;
    }
   

    /**
     * Metodo que devuelve banco
     * @version ...... 1.0
     */
    public String getBanco(){
        return this.banco;
    }
    
    
    
    
    /**
     * Metodo que devuelve agenciaBanco
     * @version ...... 1.0
     */
    public String getAgenciaBanco(){
        return this.agenciaBanco;
    }
    
    
     /**
     * Metodo que devuelve cuenta
     * @version ...... 1.0
     */
    public String getCuenta(){
        return this.cuenta;
    }
    
    
     /**
     * Metodo que devuelve cheque
     * @version ...... 1.0
     */
    public String getCheque(){
        return this.cheque;
    }
    
    
    /**
     * Metodo que devuelve numero
     * @version ...... 1.0
     */
    public int getNumber(){
        return this.numero;
    }
    
    
    /**
     * Metodo que devuelve tope
     * @version ...... 1.0
     */
    public int getTope(){
        return this.tope;
    }
    
    
     /**
     * Metodo que devuelve prefijo
     * @version ...... 1.0
     */
    public String getPrefix(){
        return this.prefijo;
    }
    
    
    /**
     * Metodo que devuelve valor
     * @version ...... 1.0
     */
    public double getValor(){
        return this.valor;
    }
    
    
    
     /**
     * Metodo que devuelve moneda
     * @version ...... 1.0
     */
    public String getMoneda(){
        return this.moneda;
    }
    
    
     /**
     * Metodo que devuelve ano
     * @version ...... 1.0
     */
    public String getAno(){
        return this.ano;
    }
    
    
    /**
     * Metodo que devuelve mes
     * @version ...... 1.0
     */
    public String getMes(){
        return this.mes;
    }
    

    /**
     * Metodo que devuelve dia
     * @version ...... 1.0
     */
    public String getDia(){
        return this.dia;
    }
    
    
    /**
     * Metodo que devuelve beneficiario
     * @version ...... 1.0
     */
    public String getBeneficiario(){
        return this.beneficiario;
    }
    
    
     /**
     * Metodo que devuelve cedula
     * @version ...... 1.0
     */
    public String getCedula(){
        return this.cedula;
    }
    
    
     /**
     * Metodo que devuelve planilla
     * @version ...... 1.0
     */
    public String getPlanilla(){
       return this.planilla;    
    }
    
    
     /**
     * Metodo que devuelve parafo
     * @version ...... 1.0
     */ 
    public String getParafo(){
        
        parafo="propietarios : \n Factura = Valor(es) de la(s) Planilla(s) - Descuento \n Descuento = Reteiva + Reteica";
        return this.parafo;
    }
    
    
    /**
     * Metodo que devuelve ListBancos
     * @version ...... 1.0
     */ 
    public List getListBancos(){
        return this.ListBancos;
    }
    
    
    /**
     * Metodo que devuelve impreso
     * @version ...... 1.0
     */ 
    public boolean getImpresion(){
        return this.impreso;
    }
        
    
    
    /**
     * Metodo que devuelve selected
     * @version ...... 1.0
     */ 
    public boolean getSeleted(){
        return this.selected;
    }
    
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property noSerie.
     * @return Value of property noSerie.
     */
    public boolean isNoSerie() {
        return noSerie;
    }
    
    /**
     * Setter for property noSerie.
     * @param noSerie New value of property noSerie.
     */
    public void setNoSerie(boolean noSerie) {
        this.noSerie = noSerie;
    }
    
    
    /**
     * Getter for property inicioSerie.
     * @return Value of property inicioSerie.
     */
    public java.lang.String getInicioSerie() {
        return inicioSerie;
    }
    
    /**
     * Setter for property inicioSerie.
     * @param inicioSerie New value of property inicioSerie.
     */
    public void setInicioSerie(java.lang.String inicioSerie) {
        this.inicioSerie = inicioSerie;
    }
    
    /**
     * Getter for property nombreUsurio.
     * @return Value of property nombreUsurio.
     */
    public java.lang.String getNombreUsurio() {
        return nombreUsurio;
    }
    
    /**
     * Setter for property nombreUsurio.
     * @param nombreUsurio New value of property nombreUsurio.
     */
    public void setNombreUsurio(java.lang.String nombreUsurio) {
        this.nombreUsurio = nombreUsurio;
    }
    
    /**
     * Getter for property nombreAgencia.
     * @return Value of property nombreAgencia.
     */
    public java.lang.String getNombreAgencia() {
        return nombreAgencia;
    }
    
    /**
     * Setter for property nombreAgencia.
     * @param nombreAgencia New value of property nombreAgencia.
     */
    public void setNombreAgencia(java.lang.String nombreAgencia) {
        this.nombreAgencia = nombreAgencia;
    }
    
    /**
     * Getter for property propietario.
     * @return Value of property propietario.
     */
    public java.lang.String getPropietario () {
        return propietario;
    }
    
    /**
     * Setter for property propietario.
     * @param propietario New value of property propietario.
     */
    public void setPropietario (java.lang.String propietario) {
        this.propietario = propietario;
    }
    
    /**
     * Getter for property proveedor_anticipo.
     * @return Value of property proveedor_anticipo.
     */
    public java.lang.String getProveedor_anticipo() {
        return proveedor_anticipo;
    }
    
    /**
     * Setter for property proveedor_anticipo.
     * @param proveedor_anticipo New value of property proveedor_anticipo.
     */
    public void setProveedor_anticipo(java.lang.String proveedor_anticipo) {
        this.proveedor_anticipo = proveedor_anticipo;
    }
    
    /**
     * Getter for property nombre_propietario.
     * @return Value of property nombre_propietario.
     */
    public java.lang.String getNombre_propietario() {
        return nombre_propietario;
    }
    
    /**
     * Setter for property nombre_propietario.
     * @param nombre_propietario New value of property nombre_propietario.
     */
    public void setNombre_propietario(java.lang.String nombre_propietario) {
        this.nombre_propietario = nombre_propietario;
    }
    
    /**
     * Getter for property activado.
     * @return Value of property activado.
     */
    public boolean isActivado() {
        return activado;
    }
    
    /**
     * Setter for property activado.
     * @param activado New value of property activado.
     */
    public void setActivado(boolean activado) {
        this.activado = activado;
    }
    
    /**
     * Getter for property laser.
     * @return Value of property laser.
     */
    public java.lang.String getLaser() {
        return laser;
    }
    
    /**
     * Setter for property laser.
     * @param laser New value of property laser.
     */
    public void setLaser(java.lang.String laser) {
        this.laser = laser;
    }
    
    /**
     * Getter for property copias.
     * @return Value of property copias.
     */
    public int getCopias() {
        return copias;
    }
    
    /**
     * Setter for property copias.
     * @param copias New value of property copias.
     */
    public void setCopias(int copias) {
        this.copias = copias;
    }
    
    /**
     * Getter for property estado_impresion_oc.
     * @return Value of property estado_impresion_oc.
     */
    public boolean isEstado_impresion_oc() {
        return estado_impresion_oc;
    }
    
    /**
     * Setter for property estado_impresion_oc.
     * @param estado_impresion_oc New value of property estado_impresion_oc.
     */
    public void setEstado_impresion_oc(boolean estado_impresion_oc) {
        this.estado_impresion_oc = estado_impresion_oc;
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    /**
     * Getter for property fecha_imp_oc.
     * @return Value of property fecha_imp_oc.
     */
    public java.lang.String getFecha_imp_oc() {
        return fecha_imp_oc;
    }
    
    /**
     * Setter for property fecha_imp_oc.
     * @param fecha_imp_oc New value of property fecha_imp_oc.
     */
    public void setFecha_imp_oc(java.lang.String fecha_imp_oc) {
        this.fecha_imp_oc = fecha_imp_oc;
    }
    
  
     
     /**
     * Metodo que formate esquema de impresio para el cheque
     * @version ...... 1.0
     */ 
     public void formatEsquema(){
         String separador = "#NEWCOLUMN#";
         //String newLinea  = "<BR><BR>";
         
        
        List listaEsquema = this.getPositionColumn();
         if(listaEsquema!=null || listaEsquema.size()>0){
            Iterator itEsquema = listaEsquema.iterator();  
            while(itEsquema.hasNext()){
                EsquemaCheque esquema = (EsquemaCheque) itEsquema.next();
                String valor="";
               if(esquema.getDescripcion().toUpperCase().equals("BENEFICIARIO"))
                   valor = this.getBeneficiario();
                if(esquema.getDescripcion().toUpperCase().equals("A�O"))
                   valor = Util.getFechaActual_String(1); //this.getAno();
                if(esquema.getDescripcion().toUpperCase().equals("MES")){
                    if( esquema.getBanco().toUpperCase().equals("NACIONAL CREDIT")){
                         valor = com.tsp.util.Util.NombreMes( Integer.parseInt(Util.getFechaActual_String(3))).toUpperCase();
                    }
                    else{
                        valor = Util.getFechaActual_String(3); //this.getMes();
                    }
                }
                if(esquema.getDescripcion().toUpperCase().equals("DIA"))
                   valor = Util.getFechaActual_String(5); //this.getDia();
                if(esquema.getDescripcion().toUpperCase().equals("VALOR"))
                   valor =  Util.customFormat( this.getValor() ); //String.valueOf(this.getValor());
                if(esquema.getDescripcion().toUpperCase().equals("MONTO")){
                   String[] vector =  RMCantidadEnLetras.getTexto( this.getValor(), this.getMoneda()  );
                    String newlinea =  ( esquema.getBanco().toUpperCase().equals("NACIONAL CREDIT") )?"<BR>":"<BR><BR>";
                   for(int i=0;i<vector.length;i++){                      
                       valor +=vector[i] + newlinea;
                   }
                }  
                
                if(!this.campos.equals("")) this.campos+=separador;
                this.campos+=valor;
                if(!this.posiciones.equals("")) this.posiciones+=separador;
                this.posiciones+= String.valueOf(esquema.getPosition()) +","+ String.valueOf(esquema.getLinea()*13 );
            }
         }
    }
     
     
    public void formatEsquemaCMS(){
         String separador = "#NEWCOLUMN#";
         //String newLinea  = "<BR><BR>";
         
        
        List listaEsquema = this.getPositionColumn();
         if(listaEsquema!=null || listaEsquema.size()>0){
            Iterator itEsquema = listaEsquema.iterator();  
            while(itEsquema.hasNext()){
                EsquemaCheque esquema = (EsquemaCheque) itEsquema.next();
                String valor="";
               if(esquema.getCampo().toUpperCase().equals("BENEFICIARIO"))
                   valor = this.getBeneficiario();
                if(esquema.getCampo().toUpperCase().equals("A�O"))
                   valor = Util.getFechaActual_String(1); //this.getAno();
                if(esquema.getCampo().toUpperCase().equals("MES")){
                    if( esquema.getBanco().toUpperCase().equals("NACIONAL CREDIT")){
                         valor = com.tsp.util.Util.NombreMes( Integer.parseInt(Util.getFechaActual_String(3))).toUpperCase();
                    }
                    else{
                        valor = Util.getFechaActual_String(3); //this.getMes();
                    }
                }
                if(esquema.getCampo().toUpperCase().equals("DIA"))
                   valor = Util.getFechaActual_String(5); //this.getDia();
                if(esquema.getCampo().toUpperCase().equals("VALOR"))
                   valor =  Util.customFormat( this.getValor() ); //String.valueOf(this.getValor());
                if(esquema.getCampo().toUpperCase().equals("MONTO")){
                   String[] vector =  RMCantidadEnLetras.getTexto( this.getValor(), this.getMoneda()  );
                    String newlinea =  ( esquema.getBanco().toUpperCase().equals("NACIONAL CREDIT") )?"<BR>":"<BR><BR>";
                   for(int i=0;i<vector.length;i++){                      
                       valor +=vector[i] + newlinea;
                   }
                }  
                
                if(!this.campos.equals("")) this.campos+=separador;
                this.campos+=valor;
                if(!this.posiciones.equals("")) this.posiciones+=separador;
                this.posiciones+= String.valueOf(esquema.getLeft()) +","+ String.valueOf(esquema.getTop() );
            }
         }
    }
    
    /**
     * Getter for property id_serie.
     * @return Value of property id_serie.
     */
    public int getId_serie() {
        return id_serie;
    }    
   
    /**
     * Setter for property id_serie.
     * @param id_serie New value of property id_serie.
     */
    public void setId_serie(int id_serie) {
        this.id_serie = id_serie;
    }    
    
}
