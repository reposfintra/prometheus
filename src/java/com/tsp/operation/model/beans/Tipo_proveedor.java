/*
 * Tipo_proveedor.java
 *
 * Created on 22 de septiembre de 2005, 08:25 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
/**
 *
 * @author  Jm
 */
public class Tipo_proveedor {
    
    private String dstrct;
    private String reg_status;
    private String codigo_proveedor;
    private String descripcion;
    private String creation_user;
    private String user_update;
    
    /** Creates a new instance of Tipo_proveedor */
    public Tipo_proveedor() {
    }
    
    
    public static Tipo_proveedor load(ResultSet rs)throws Exception{
        Tipo_proveedor datos = new Tipo_proveedor();
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setDstrct(rs.getString("DSTRCT"));
        datos.setCodigo_proveedor(rs.getString("CODIGO_PROVEEDOR"));
        datos.setDescripcion(rs.getString("DESCRIPCION"));
        return datos;
    }
    /**
     * Getter for property codigo_proveedor.
     * @return Value of property codigo_proveedor.
     */
    public java.lang.String getCodigo_proveedor() {
        return codigo_proveedor;
    }
    
    /**
     * Setter for property codigo_proveedor.
     * @param codigo_proveedor New value of property codigo_proveedor.
     */
    public void setCodigo_proveedor(java.lang.String codigo_proveedor) {
        this.codigo_proveedor = codigo_proveedor;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}
