/*
 *  Nombre clase    :  FacturaProveedorAnticipo.java
 *  Descripcion     :
 *  Autor           : Ing. Enrique De Lavalle Rizo 
 *  Fecha           : 30 de Enero de 2007, 01:36 AM  
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;

public class FacturaProveedorAnticipo {
    
    //guarda los datos para el listado
    private String planilla;
    private String concepto;
    private String fecha;
    private String valor;
    private String moneda;
    
    //guarda los datos para la factura en cxp_doc
    private String agenciaProveedor;
    private String handleCodeProveedor;
    private String idMimsProveedor;
    private String bancoProveedor;
    private String sucursalProveedor;
    private String monedaBanco;   
    private int plazo;
    private String codCuenta;
    private String concept_code;
    
    /** Crea una nueva instancia de  ReporteOCAnticipo */
    public FacturaProveedorAnticipo() {
        
        planilla = "";
        concepto= "";
        fecha= "";
        valor= "";
        moneda= "";
        
        agenciaProveedor = "";
        handleCodeProveedor= "";
        idMimsProveedor= "";
        bancoProveedor= "";
        sucursalProveedor= "";
        monedaBanco= "";   
        plazo = 0 ;
        codCuenta = "";
        concept_code = "";
                
    }

    public static FacturaProveedorAnticipo load(ResultSet rs)throws Exception{
        
        FacturaProveedorAnticipo datos = new FacturaProveedorAnticipo();
        datos.setPlanilla( ( rs.getString("planilla")!=null )?rs.getString("planilla"):"" );
        datos.setConcepto( ( rs.getString("concepto")!=null )?rs.getString("concepto"):"" );
        datos.setFecha( ( rs.getString("fecha_creacion")!=null )?rs.getString("fecha_creacion"):"" );
        datos.setValor( ( rs.getString("valor")!=null )?rs.getString("valor"):"" );
        datos.setMoneda( ( rs.getString("moneda")!=null )?rs.getString("moneda"):"" );
        
        datos.setAgenciaProveedor( ( rs.getString("agencia_proveedor")!=null )?rs.getString("agencia_proveedor"):"" );
        datos.setHandleCodeProveedor(( rs.getString("handle_code_proveedor")!=null )?rs.getString("handle_code_proveedor"):"");
        datos.setIdMimsProveedor(( rs.getString("id_mims_proveedor")!=null )?rs.getString("id_mims_proveedor"):"");
        datos.setBancoProveedor(( rs.getString("banco_proveedor")!=null )?rs.getString("banco_proveedor"):"");
        datos.setSucursalProveedor(( rs.getString("sucursal_proveedor")!=null )?rs.getString("sucursal_proveedor"):"");
        datos.setMonedaBanco(( rs.getString("moneda_banco")!=null )?rs.getString("moneda_banco"):""); 
        datos.setPlazo( rs.getInt("plazo"));
        datos.setCodCuenta(( rs.getString("codCuenta")!=null )?rs.getString("codCuenta"):""); 
        datos.setConcept_code(( rs.getString("concept_code")!=null )?rs.getString("concept_code"):""); 
        
        return datos;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }    

    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }    
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public java.lang.String getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(java.lang.String valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property agenciaProveedor.
     * @return Value of property agenciaProveedor.
     */
    public java.lang.String getAgenciaProveedor() {
        return agenciaProveedor;
    }
    
    /**
     * Setter for property agenciaProveedor.
     * @param agenciaProveedor New value of property agenciaProveedor.
     */
    public void setAgenciaProveedor(java.lang.String agenciaProveedor) {
        this.agenciaProveedor = agenciaProveedor;
    }
    
    /**
     * Getter for property handleCodeProveedor.
     * @return Value of property handleCodeProveedor.
     */
    public java.lang.String getHandleCodeProveedor() {
        return handleCodeProveedor;
    }
    
    /**
     * Setter for property handleCodeProveedor.
     * @param handleCodeProveedor New value of property handleCodeProveedor.
     */
    public void setHandleCodeProveedor(java.lang.String handleCodeProveedor) {
        this.handleCodeProveedor = handleCodeProveedor;
    }
    
    /**
     * Getter for property idMimsProveedor.
     * @return Value of property idMimsProveedor.
     */
    public java.lang.String getIdMimsProveedor() {
        return idMimsProveedor;
    }
    
    /**
     * Setter for property idMimsProveedor.
     * @param idMimsProveedor New value of property idMimsProveedor.
     */
    public void setIdMimsProveedor(java.lang.String idMimsProveedor) {
        this.idMimsProveedor = idMimsProveedor;
    }
    
    /**
     * Getter for property bancoProveedor.
     * @return Value of property bancoProveedor.
     */
    public java.lang.String getBancoProveedor() {
        return bancoProveedor;
    }
    
    /**
     * Setter for property bancoProveedor.
     * @param bancoProveedor New value of property bancoProveedor.
     */
    public void setBancoProveedor(java.lang.String bancoProveedor) {
        this.bancoProveedor = bancoProveedor;
    }
    
    /**
     * Getter for property sucursalProveedor.
     * @return Value of property sucursalProveedor.
     */
    public java.lang.String getSucursalProveedor() {
        return sucursalProveedor;
    }
    
    /**
     * Setter for property sucursalProveedor.
     * @param sucursalProveedor New value of property sucursalProveedor.
     */
    public void setSucursalProveedor(java.lang.String sucursalProveedor) {
        this.sucursalProveedor = sucursalProveedor;
    }
    
    /**
     * Getter for property monedaBanco.
     * @return Value of property monedaBanco.
     */
    public java.lang.String getMonedaBanco() {
        return monedaBanco;
    }
    
    /**
     * Setter for property monedaBanco.
     * @param monedaBanco New value of property monedaBanco.
     */
    public void setMonedaBanco(java.lang.String monedaBanco) {
        this.monedaBanco = monedaBanco;
    }
    
    /**
     * Getter for property plazo.
     * @return Value of property plazo.
     */
    public int getPlazo() {
        return plazo;
    }
    
    /**
     * Setter for property plazo.
     * @param plazo New value of property plazo.
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    /**
     * Getter for property codCuenta.
     * @return Value of property codCuenta.
     */
    public java.lang.String getCodCuenta() {
        return codCuenta;
    }
    
    /**
     * Setter for property codCuenta.
     * @param codCuenta New value of property codCuenta.
     */
    public void setCodCuenta(java.lang.String codCuenta) {
        this.codCuenta = codCuenta;
    }
    
    /**
     * Getter for property concept_code.
     * @return Value of property concept_code.
     */
    public java.lang.String getConcept_code() {
        return concept_code;
    }
    
    /**
     * Setter for property concept_code.
     * @param concept_code New value of property concept_code.
     */
    public void setConcept_code(java.lang.String concept_code) {
        this.concept_code = concept_code;
    }
    
}
