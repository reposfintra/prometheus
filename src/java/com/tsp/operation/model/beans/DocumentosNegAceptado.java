package com.tsp.operation.model.beans;

/**
 * Bean para la tabla documentos_neg_aceptado<br/>
 * 17/09/2010
 * @author darrieta-Geotech
 */
public class DocumentosNegAceptado {

    private String cod_neg;
    private String item;
    private String fecha;
    private String dias;
    private double saldo_inicial;
    private double capital;
    private double interes;
    private double valor;
    private double saldo_final;
    private String reg_status;
    private String creation_date;
    private double no_aval;
    private double capacitacion;
    private double seguro;
    private double cat;
    private double remesa;
    private double custodia;
    private double capital_poliza;
    private double valor_aval;
    private double capital_aval;
    private double interes_aval;
    private double aval;

    
    private double cuota_manejo = 0;

    /**
     * Get the value of no_aval
     *
     * @return the value of no_aval
     */
    public double getNo_aval() {
        return no_aval;
    }

    /**
     * Set the value of no_aval
     *
     * @param no_aval new value of no_aval
     */
    public void setNo_aval(double no_aval) {
        this.no_aval = no_aval;
    }

    /**
     * Get the value of creation_date
     *
     * @return the value of creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * Set the value of creation_date
     *
     * @param creation_date new value of creation_date
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * Get the value of reg_status
     *
     * @return the value of reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * Set the value of reg_status
     *
     * @param reg_status new value of reg_status
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * Get the value of saldo_final
     *
     * @return the value of saldo_final
     */
    public double getSaldo_final() {
        return saldo_final;
    }

    /**
     * Set the value of saldo_final
     *
     * @param saldo_final new value of saldo_final
     */
    public void setSaldo_final(double saldo_final) {
        this.saldo_final = saldo_final;
    }

    /**
     * Get the value of valor
     *
     * @return the value of valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * Set the value of valor
     *
     * @param valor new value of valor
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * Get the value of interes
     *
     * @return the value of interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * Set the value of interes
     *
     * @param interes new value of interes
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * Get the value of capital
     *
     * @return the value of capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * Set the value of capital
     *
     * @param capital new value of capital
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * Get the value of saldo_inicial
     *
     * @return the value of saldo_inicial
     */
    public double getSaldo_inicial() {
        return saldo_inicial;
    }

    /**
     * Set the value of saldo_inicial
     *
     * @param saldo_inicial new value of saldo_inicial
     */
    public void setSaldo_inicial(double saldo_inicial) {
        this.saldo_inicial = saldo_inicial;
    }

    /**
     * Get the value of dias
     *
     * @return the value of dias
     */
    public String getDias() {
        return dias;
    }

    /**
     * Set the value of dias
     *
     * @param dias new value of dias
     */
    public void setDias(String dias) {
        this.dias = dias;
    }

    /**
     * Get the value of fecha
     *
     * @return the value of fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param fecha new value of fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Get the value of item
     *
     * @return the value of item
     */
    public String getItem() {
        return item;
    }

    /**
     * Set the value of item
     *
     * @param item new value of item
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * Get the value of cod_neg
     *
     * @return the value of cod_neg
     */
    public String getCod_neg() {
        return cod_neg;
    }

    /**
     * Set the value of cod_neg
     *
     * @param cod_neg new value of cod_neg
     */
    public void setCod_neg(String cod_neg) {
        this.cod_neg = cod_neg;
    }
    
    
    
    public double getCapacitacion() {
        return capacitacion;
    }

    public void setCapacitacion(double capacitacion) {
        this.capacitacion = capacitacion;
    }

    public double getCat() {
        return cat;
    }

    public void setCat(double cat) {
        this.cat = cat;
    }

    public double getSeguro() {
        return seguro;
    }

    public void setSeguro(double seguro) {
        this.seguro = seguro;
    }

 public double getCustodia() {
        return custodia;
    }

    public void setCustodia(double custodia) {
        this.custodia = custodia;
    }

    public double getRemesa() {
        return remesa;
    }
    
        public void setRemesa(double remesa) {
        this.remesa = remesa;
    }

    public double getCuota_manejo() {
        return cuota_manejo;
    }

    public void setCuota_manejo(double cuota_manejo) {
        this.cuota_manejo = cuota_manejo;
    }
    
    public double getCapital_Poliza() {
        return capital_poliza;
    }

    public void setCapital_Poliza(double capital_poliza) {
        this.capital_poliza = capital_poliza;
    }
    
    public double getValor_aval() {
        return valor_aval;
    }

    public void setValor_aval(double valor_aval) {
        this.valor_aval = valor_aval;
    }

    public double getCapital_aval() {
        return capital_aval;
    }

    public void setCapital_aval(double capital_aval) {
        this.capital_aval = capital_aval;
    }

    public double getInteres_aval() {
        return interes_aval;
    }

    public void setInteres_aval(double interes_aval) {
        this.interes_aval = interes_aval;
    }

    public double getCapital_poliza() {
        return capital_poliza;
    }

    public void setCapital_poliza(double capital_poliza) {
        this.capital_poliza = capital_poliza;
    }
     
    /**
     * @return the aval
     */
    public double getAval() {
        return aval;
    }

    /**
     * @param aval the aval to set
     */
    public void setAval(double aval) {
        this.aval = aval;
    }

}