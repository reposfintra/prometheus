/*
 * Nombre        Perfil.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Administrador
 */
public class Perfil implements Serializable {
    
    private String id_perfil;
    private String nombre;    
    private String creado_por;
    private String user_update;
    
    //Fernell 251005     
    private String distrito;
    private String base;
    
    /**
     * Setter for property distrito.
     * @param p New value of property distrito.
     */
    public void setDistrito  (String val){ 
        this.distrito = val; 
    }
    
    /**
     * Setter for property base.
     * @param p New value of property base.
     */
    public void setBase      (String val){ 
        this.base     = val; 
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public String getDistrito() { 
        return this.distrito; 
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public String getBase    () { 
        return this.base    ; 
    }
    
    /** Creates a new instance of Perfil */
    public static Perfil load ( ResultSet rs ) throws SQLException {
        Perfil perfil = new Perfil();
        perfil.setId (rs.getString ("id_perfil"));
        perfil.setNombre (rs.getString("nombre"));        
        perfil.setCreado_por (rs.getString("creation_user"));
        
        return perfil;
    }
    
    /**
     * Setter for property id.
     * @param p New value of property id.
     */
    public void setId ( String id ){
        this.id_perfil = id;
    }
    
    /**
     * Setter for property nombre.
     * @param p New value of property nombre.
     */
    public void setNombre (String nombre ){
        this.nombre = nombre;
    }
    
    /**
     * Setter for property creado_por.
     * @param p New value of property creado_por.
     */
    public void setCreado_por ( String creado_por ){
        this.creado_por = creado_por;
    }
    
    /**
     * Setter for property user_update.
     * @param p New value of property user_update.
     */
    public void setUser_update ( String uu ){
        this.user_update = uu;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public String getId ( ){
        return id_perfil;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public String getNombre( ){
        return nombre;
    }
    
    /**
     * Getter for property creado_por.
     * @return Value of property creado_por.
     */
    public String getCreado_por ( ){
        return creado_por;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public String getUser_update ( ){
        return user_update;
    }
}
