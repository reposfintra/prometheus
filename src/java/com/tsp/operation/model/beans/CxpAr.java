/* * CxpBoni.java * * Created on agosto de 2009, 16:46 */
package com.tsp.operation.model.beans;
/** * * @author  Fintra */
public class CxpAr{

    private String num_os,cxc_ar,id_orden,factura_ar,vlr_neto,abono,saldo,fec_open;    
    private String sum_tot_prev1,id_accion;    
    private String banco,sucursal,id_contratista,contratista,cantidad_acciones,doc_relacionado,prefactura;
    
    public CxpAr() {    }    
    
    public String getPrefactura() {
        return prefactura;
    }
    public void setPrefactura(String x) {
        this.prefactura= x;
    }
    public String getCantidadAcciones() {
        return cantidad_acciones;
    }
    public void setCantidadAcciones(String x) {
        this.cantidad_acciones= x;
    }
    public String getIdContratista() {
        return id_contratista;
    }
    public void setIdContratista(String x) {
        this.id_contratista= x;
    }
    public String getContratista() {
        return contratista;
    }
    public void setContratista(String x) {
        this.contratista= x;
    }
    public String getIdAccion() {
        return id_accion;
    }
    public void setIdAccion(String x) {
        this.id_accion= x;
    }
    public String getSumTotPrev1() {
        return sum_tot_prev1;
    }
    public void setSumTotPrev1(String x) {
        this.sum_tot_prev1= x;
    }    
    public String getNumOs() {
        return num_os;
    }
    public void setNumOs(String x) {
        this.num_os= x;
    }
    public String getCxcAr() {
        return cxc_ar;
    }
    public void setCxcAr(String x) {
        this.cxc_ar= x;
    }
    public String getIdOrden() {
        return id_orden;
    }
    public void setIdOrden(String x) {
        this.id_orden= x;
    }
    public String getFacturaAr() {
        return factura_ar;
    }
    public void setFacturaAr(String x) {
        this.factura_ar= x;
    }
    public String getVlrNeto() {
        return vlr_neto;
    }
    public void setVlrNeto(String x) {
        this.vlr_neto= x;
    }
    public String getAbono() {
        return abono;
    }
    public void setAbono(String x) {
        this.abono= x;
    }
    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String x) {
        this.saldo= x;
    }
    public String getFecOpen() {
        return fec_open;
    }
    public void setFecOpen(String x) {
        this.fec_open= x;
    }    
    
    public String getBanco() {
        return banco;
    }
    public void setBanco(String x) {
        this.banco= x;
    }
    public String getSucursal() {
        return sucursal;
    }
    public void setSucursal(String x) {
        this.sucursal= x;
    }
    public String getDocRelacionado() {
        return doc_relacionado;
    }
    public void setDocRelacionado(String x) {
        this.doc_relacionado= x;
    }
    
    public static CxpAr load(java.sql.ResultSet rs)throws java.sql.SQLException{
        CxpAr cxpAr = new CxpAr();
        cxpAr.setNumOs( rs.getString("num_os") );           
        cxpAr.setCxcAr( rs.getString("cxc_ar") );             
        cxpAr.setIdOrden( rs.getString("id_orden") ); 
        cxpAr.setFacturaAr( rs.getString("factura_retencion") ); 
        cxpAr.setVlrNeto( rs.getString("vlr_neto") ); 
        cxpAr.setAbono( rs.getString("vlr_total_abonos") ); 
        cxpAr.setSaldo( rs.getString("vlr_saldo") ); 
        cxpAr.setFecOpen( rs.getString("fec_open") );         
        cxpAr.setSumTotPrev1( rs.getString("sum_total_prev1") );         
        cxpAr.setBanco(rs.getString("banco") ); 
        cxpAr.setSucursal(rs.getString("sucursal") ); 
        cxpAr.setIdAccion(rs.getString("id_accion") ); 
        cxpAr.setIdContratista(rs.getString("id_contratista") ); 
        cxpAr.setContratista(rs.getString("descripcion") ); 
        cxpAr.setCantidadAcciones(rs.getString("cantidad_acciones") ); 
        cxpAr.setDocRelacionado(rs.getString("documento_relacionado") ); 
        cxpAr.setPrefactura(rs.getString("prefactura") ); 
        return cxpAr;
    }  
}

