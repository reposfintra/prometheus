/********************************************************************
 *      Nombre Clase.................   Concepto.java    
 *      Descripci�n..................   Bean de la tabla otros_conceptos
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   27.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.Util;
import java.sql.*;


public class Concepto {
        private String c_tabla;
        private String c_descripcion;
        private String ntabla;
        private String ndescripcion;
        
        private String usuario_creacion;
        private String fecha_creacion;
        private String usuario_modificacion;
        private String ultima_modificacion;
        private String distrito;
        private String estado;
        private String base;

        
        /** Creates a new instance of Documento */
        public Concepto() {
                this.c_tabla = "";
                this.c_descripcion = "";
        }
        
        /**
         * Toma el valor de cada uno de los campos y los asigna.
         * @autor Tito Andr�s Maturana
         * @param rs ResultSet de la consulta.
         * @throws SQLException
         * @version 1.0
         */  
        public void Load(ResultSet rs) throws SQLException{
                this.c_tabla = rs.getString("tabla");
                this.c_descripcion = rs.getString("descripcion");
                this.ntabla = rs.getString("tabla");
                this.ndescripcion = rs.getString("descripcion");
                this.estado = rs.getString("reg_status");                
                this.distrito = rs.getString("dstrct"); 
                this.base = rs.getString("base");
                this.fecha_creacion = rs.getString("creation_date");
                this.usuario_creacion = rs.getString("creation_user");
                this.ultima_modificacion = rs.getString("last_update");
                this.usuario_modificacion = rs.getString("user_update");
        }   
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }        
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }
        
        /**
         * Getter for property c_descripcion.
         * @return Value of property c_descripcion.
         */
        public java.lang.String getC_descripcion() {
                return c_descripcion;
        }
        
        /**
         * Setter for property c_descripcion.
         * @param c_descripcion New value of property c_descripcion.
         */
        public void setC_descripcion(java.lang.String c_descripcion) {
                this.c_descripcion = c_descripcion;
        }
        
        /**
         * Getter for property distrito.
         * @return Value of property distrito.
         */
        public java.lang.String getDistrito() {
                return distrito;
        }
        
        /**
         * Setter for property distrito.
         * @param distrito New value of property distrito.
         */
        public void setDistrito(java.lang.String distrito) {
                this.distrito = distrito;
        }
        
        /**
         * Getter for property estado.
         * @return Value of property estado.
         */
        public java.lang.String getEstado() {
                return estado;
        }
        
        /**
         * Setter for property estado.
         * @param estado New value of property estado.
         */
        public void setEstado(java.lang.String estado) {
                this.estado = estado;
        }
        
        /**
         * Getter for property fecha_creacion.
         * @return Value of property fecha_creacion.
         */
        public java.lang.String getFecha_creacion() {
                return fecha_creacion;
        }
        
        /**
         * Setter for property fecha_creacion.
         * @param fecha_creacion New value of property fecha_creacion.
         */
        public void setFecha_creacion(java.lang.String fecha_creacion) {
                this.fecha_creacion = fecha_creacion;
        }
        
        /**
         * Getter for property ultima_modificacion.
         * @return Value of property ultima_modificacion.
         */
        public java.lang.String getUltima_modificacion() {
                return ultima_modificacion;
        }
        
        /**
         * Setter for property ultima_modificacion.
         * @param ultima_modificacion New value of property ultima_modificacion.
         */
        public void setUltima_modificacion(java.lang.String ultima_modificacion) {
                this.ultima_modificacion = ultima_modificacion;
        }
        
        /**
         * Getter for property usuario_creacion.
         * @return Value of property usuario_creacion.
         */
        public java.lang.String getUsuario_creacion() {
                return usuario_creacion;
        }
        
        /**
         * Setter for property usuario_creacion.
         * @param usuario_creacion New value of property usuario_creacion.
         */
        public void setUsuario_creacion(java.lang.String usuario_creacion) {
                this.usuario_creacion = usuario_creacion;
        }
        
        /**
         * Getter for property usuario_modificacion.
         * @return Value of property usuario_modificacion.
         */
        public java.lang.String getUsuario_modificacion() {
                return usuario_modificacion;
        }
        
        /**
         * Setter for property usuario_modificacion.
         * @param usuario_modificacion New value of property usuario_modificacion.
         */
        public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
                this.usuario_modificacion = usuario_modificacion;
        }
        
        /**
         * Getter for property c_tabla.
         * @return Value of property c_tabla.
         */
        public java.lang.String getC_tabla() {
                return c_tabla;
        }
        
        /**
         * Setter for property c_tabla.
         * @param c_tabla New value of property c_tabla.
         */
        public void setC_tabla(java.lang.String c_tabla) {
                this.c_tabla = c_tabla;
        }
        
        /**
         * Getter for property ndescripcion.
         * @return Value of property ndescripcion.
         */
        public java.lang.String getNdescripcion() {
                return ndescripcion;
        }
        
        /**
         * Setter for property ndescripcion.
         * @param ndescripcion New value of property ndescripcion.
         */
        public void setNdescripcion(java.lang.String ndescripcion) {
                this.ndescripcion = ndescripcion;
        }
        
        /**
         * Getter for property ntabla.
         * @return Value of property ntabla.
         */
        public java.lang.String getNtabla() {
                return ntabla;
        }
        
        /**
         * Setter for property ntabla.
         * @param ntabla New value of property ntabla.
         */
        public void setNtabla(java.lang.String ntabla) {
                this.ntabla = ntabla;
        }
        
}
