/************************************************
 * VerDoumento.java
 *
 * Created on 30 de septiembre de 2005, 03:54 PM
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  dbastidas
 */
public class VerificacionDoc {
    public String Nombre;
    public String libmilitar;
    public String cedula;
    public String nrojudicial;
    public String vencejudicial;
    public String nomeps;
    public String nroeps;
    public String fecafieps;
    public String nomarp;
    public String fecafiarp;
    public String nopase;
    public String categoriapase;
    public String vigenciapase;
    public String nrolibtripulante;
    public String vencelibtripulante;
    public String nopasaporte;
    public String vencepasaporte;
    public String pasaportevence;
    public String nrovisa;
    public String vencevisa;
    public String placa;
    public String tarprop;
    public String fecvenprop;
    public String certemgases;
    public String fecvegases;
    public String ciasoat;
    public String venseguroobliga;
    public String reg_nal_carga;
    public String fecvenreg;
    public String tarempresa;
    public String fecvenempresa;
    public String tarhabil;
    public String fecvenhabil;
    public String tarpropsemi;
    public String ventarpropsemi;
    public String regnalsemi;
    public String venregnalsemi;
    public String poliza_andina;
    public String fecvenandina;
    //verificacion
    public String t_libmilitar;
    public String t_cedula;
    public String t_judicial;
    public String t_eps;
    public String t_arp;
    public String t_categoriapase;
    public String t_libtripulante;
    public String t_pasaporte;
    public String t_visa;
    public String usuarioverica;
    public String fechavericacion;
    //placa
    public String t_tarpropcab;
    public String t_copiacontcomventa;
    public String t_copiacontarr;
    public String t_tarpropsemi;
    public String t_certemgases;
    public String t_soat;
    public String t_regnalcarga;
    public String t_regnalsemire;
    public String t_tarempresarial;
    public String t_fotocceducond;
    public String t_polizaandina;
    public String t_tarhabil;
    
    /** Creates a new instance of VerDoumento */
    public VerificacionDoc() {
    }
    
    /**
     * Getter for property libmilitar.
     * @return Value of property libmilitar.
     */
    public java.lang.String getLibmilitar() {
        return libmilitar;
    }
    
    /**
     * Setter for property libmilitar.
     * @param libmilitar New value of property libmilitar.
     */
    public void setLibmilitar(java.lang.String libmilitar) {
        this.libmilitar = libmilitar;
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula() {
        return cedula;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula(java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Getter for property nrojudicial.
     * @return Value of property nrojudicial.
     */
    public java.lang.String getNrojudicial() {
        return nrojudicial;
    }
    
    /**
     * Setter for property nrojudicial.
     * @param nrojudicial New value of property nrojudicial.
     */
    public void setNrojudicial(java.lang.String nrojudicial) {
        this.nrojudicial = nrojudicial;
    }
    
    /**
     * Getter for property vencejudicial.
     * @return Value of property vencejudicial.
     */
    public java.lang.String getVencejudicial() {
        return vencejudicial;
    }
    
    /**
     * Setter for property vencejudicial.
     * @param vencejudicial New value of property vencejudicial.
     */
    public void setVencejudicial(java.lang.String vencejudicial) {
        this.vencejudicial = vencejudicial;
    }
    
    /**
     * Getter for property nroeps.
     * @return Value of property nroeps.
     */
    public java.lang.String getNroeps() {
        return nroeps;
    }
    
    /**
     * Setter for property nroeps.
     * @param nroeps New value of property nroeps.
     */
    public void setNroeps(java.lang.String nroeps) {
        this.nroeps = nroeps;
    }
    
    /**
     * Getter for property fecafieps.
     * @return Value of property fecafieps.
     */
    public java.lang.String getFecafieps() {
        return fecafieps;
    }
    
    /**
     * Setter for property fecafieps.
     * @param fecafieps New value of property fecafieps.
     */
    public void setFecafieps(java.lang.String fecafieps) {
        this.fecafieps = fecafieps;
    }
    
    /**
     * Getter for property nomarp.
     * @return Value of property nomarp.
     */
    public java.lang.String getNomarp() {
        return nomarp;
    }
    
    /**
     * Setter for property nomarp.
     * @param nomarp New value of property nomarp.
     */
    public void setNomarp(java.lang.String nomarp) {
        this.nomarp = nomarp;
    }
    
    /**
     * Getter for property fecafiarp.
     * @return Value of property fecafiarp.
     */
    public java.lang.String getFecafiarp() {
        return fecafiarp;
    }
    
    /**
     * Setter for property fecafiarp.
     * @param fecafiarp New value of property fecafiarp.
     */
    public void setFecafiarp(java.lang.String fecafiarp) {
        this.fecafiarp = fecafiarp;
    }
    
    /**
     * Getter for property nopase.
     * @return Value of property nopase.
     */
    public java.lang.String getNopase() {
        return nopase;
    }
    
    /**
     * Setter for property nopase.
     * @param nopase New value of property nopase.
     */
    public void setNopase(java.lang.String nopase) {
        this.nopase = nopase;
    }
    
    /**
     * Getter for property categoriapase.
     * @return Value of property categoriapase.
     */
    public java.lang.String getCategoriapase() {
        return categoriapase;
    }
    
    /**
     * Setter for property categoriapase.
     * @param categoriapase New value of property categoriapase.
     */
    public void setCategoriapase(java.lang.String categoriapase) {
        this.categoriapase = categoriapase;
    }
    
    /**
     * Getter for property vigenciapase.
     * @return Value of property vigenciapase.
     */
    public java.lang.String getVigenciapase() {
        return vigenciapase;
    }
    
    /**
     * Setter for property vigenciapase.
     * @param vigenciapase New value of property vigenciapase.
     */
    public void setVigenciapase(java.lang.String vigenciapase) {
        this.vigenciapase = vigenciapase;
    }
    
    /**
     * Getter for property nrolibtripulante.
     * @return Value of property nrolibtripulante.
     */
    public java.lang.String getNrolibtripulante() {
        return nrolibtripulante;
    }
    
    /**
     * Setter for property nrolibtripulante.
     * @param nrolibtripulante New value of property nrolibtripulante.
     */
    public void setNrolibtripulante(java.lang.String nrolibtripulante) {
        this.nrolibtripulante = nrolibtripulante;
    }
    
    /**
     * Getter for property vencelibtripulante.
     * @return Value of property vencelibtripulante.
     */
    public java.lang.String getVencelibtripulante() {
        return vencelibtripulante;
    }
    
    /**
     * Setter for property vencelibtripulante.
     * @param vencelibtripulante New value of property vencelibtripulante.
     */
    public void setVencelibtripulante(java.lang.String vencelibtripulante) {
        this.vencelibtripulante = vencelibtripulante;
    }
    
    /**
     * Getter for property nrovisa.
     * @return Value of property nrovisa.
     */
    public java.lang.String getNrovisa() {
        return nrovisa;
    }
    
    /**
     * Setter for property nrovisa.
     * @param nrovisa New value of property nrovisa.
     */
    public void setNrovisa(java.lang.String nrovisa) {
        this.nrovisa = nrovisa;
    }
    
    /**
     * Getter for property vencevisa.
     * @return Value of property vencevisa.
     */
    public java.lang.String getVencevisa() {
        return vencevisa;
    }
    
    /**
     * Setter for property vencevisa.
     * @param vencevisa New value of property vencevisa.
     */
    public void setVencevisa(java.lang.String vencevisa) {
        this.vencevisa = vencevisa;
    }
    
   
   
   
    /**
     * Getter for property certemgases.
     * @return Value of property certemgases.
     */
    public java.lang.String getCertemgases() {
        return certemgases;
    }
    
    /**
     * Setter for property certemgases.
     * @param certemgases New value of property certemgases.
     */
    public void setCertemgases(java.lang.String certemgases) {
        this.certemgases = certemgases;
    }
    
    /**
     * Getter for property fecvegases.
     * @return Value of property fecvegases.
     */
    public java.lang.String getFecvegases() {
        return fecvegases;
    }
    
    /**
     * Setter for property fecvegases.
     * @param fecvegases New value of property fecvegases.
     */
    public void setFecvegases(java.lang.String fecvegases) {
        this.fecvegases = fecvegases;
    }
    
    /**
     * Getter for property ciasoat.
     * @return Value of property ciasoat.
     */
    public java.lang.String getCiasoat() {
        return ciasoat;
    }
    
    /**
     * Setter for property ciasoat.
     * @param ciasoat New value of property ciasoat.
     */
    public void setCiasoat(java.lang.String ciasoat) {
        this.ciasoat = ciasoat;
    }
    
    /**
     * Getter for property venseguroobliga.
     * @return Value of property venseguroobliga.
     */
    public java.lang.String getVenseguroobliga() {
        return venseguroobliga;
    }
    
    /**
     * Setter for property venseguroobliga.
     * @param venseguroobliga New value of property venseguroobliga.
     */
    public void setVenseguroobliga(java.lang.String venseguroobliga) {
        this.venseguroobliga = venseguroobliga;
    }
    
    /**
     * Getter for property reg_nal_carga.
     * @return Value of property reg_nal_carga.
     */
    public java.lang.String getReg_nal_carga() {
        return reg_nal_carga;
    }
    
    /**
     * Setter for property reg_nal_carga.
     * @param reg_nal_carga New value of property reg_nal_carga.
     */
    public void setReg_nal_carga(java.lang.String reg_nal_carga) {
        this.reg_nal_carga = reg_nal_carga;
    }
    
    /**
     * Getter for property fecvenreg.
     * @return Value of property fecvenreg.
     */
    public java.lang.String getFecvenreg() {
        return fecvenreg;
    }
    
    /**
     * Setter for property fecvenreg.
     * @param fecvenreg New value of property fecvenreg.
     */
    public void setFecvenreg(java.lang.String fecvenreg) {
        this.fecvenreg = fecvenreg;
    }
    
    /**
     * Getter for property tarempresa.
     * @return Value of property tarempresa.
     */
    public java.lang.String getTarempresa() {
        return tarempresa;
    }
    
    /**
     * Setter for property tarempresa.
     * @param tarempresa New value of property tarempresa.
     */
    public void setTarempresa(java.lang.String tarempresa) {
        this.tarempresa = tarempresa;
    }
    
    /**
     * Getter for property fecvenempresa.
     * @return Value of property fecvenempresa.
     */
    public java.lang.String getFecvenempresa() {
        return fecvenempresa;
    }
    
    /**
     * Setter for property fecvenempresa.
     * @param fecvenempresa New value of property fecvenempresa.
     */
    public void setFecvenempresa(java.lang.String fecvenempresa) {
        this.fecvenempresa = fecvenempresa;
    }
    
    /**
     * Getter for property tarhabil.
     * @return Value of property tarhabil.
     */
    public java.lang.String getTarhabil() {
        return tarhabil;
    }
    
    /**
     * Setter for property tarhabil.
     * @param tarhabil New value of property tarhabil.
     */
    public void setTarhabil(java.lang.String tarhabil) {
        this.tarhabil = tarhabil;
    }
    
    /**
     * Getter for property fecvenhabil.
     * @return Value of property fecvenhabil.
     */
    public java.lang.String getFecvenhabil() {
        return fecvenhabil;
    }
    
    /**
     * Setter for property fecvenhabil.
     * @param fecvenhabil New value of property fecvenhabil.
     */
    public void setFecvenhabil(java.lang.String fecvenhabil) {
        this.fecvenhabil = fecvenhabil;
    }
    
    /**
     * Getter for property tarpropsemi.
     * @return Value of property tarpropsemi.
     */
    public java.lang.String getTarpropsemi() {
        return tarpropsemi;
    }
    
    /**
     * Setter for property tarpropsemi.
     * @param tarpropsemi New value of property tarpropsemi.
     */
    public void setTarpropsemi(java.lang.String tarpropsemi) {
        this.tarpropsemi = tarpropsemi;
    }
    
    /**
     * Getter for property regnalsemire.
     * @return Value of property regnalsemire.
     */
    public java.lang.String getRegnalsemi() {
        return regnalsemi;
    }
    
    /**
     * Setter for property regnalsemire.
     * @param regnalsemire New value of property regnalsemire.
     */
    public void setRegnalsemi(java.lang.String regnalsemire) {
        this.regnalsemi = regnalsemire;
    }
    
    /**
     * Getter for property t_libmilitar.
     * @return Value of property t_libmilitar.
     */
    public java.lang.String getT_libmilitar() {
        return t_libmilitar;
    }
    
    /**
     * Setter for property t_libmilitar.
     * @param t_libmilitar New value of property t_libmilitar.
     */
    public void setT_libmilitar(java.lang.String t_libmilitar) {
        this.t_libmilitar = t_libmilitar;
    }
    
    /**
     * Getter for property t_cedula.
     * @return Value of property t_cedula.
     */
    public java.lang.String getT_cedula() {
        return t_cedula;
    }
    
    /**
     * Setter for property t_cedula.
     * @param t_cedula New value of property t_cedula.
     */
    public void setT_cedula(java.lang.String t_cedula) {
        this.t_cedula = t_cedula;
    }
    
    /**
     * Getter for property t_judicial.
     * @return Value of property t_judicial.
     */
    public java.lang.String getT_judicial() {
        return t_judicial;
    }
    
    /**
     * Setter for property t_judicial.
     * @param t_judicial New value of property t_judicial.
     */
    public void setT_judicial(java.lang.String t_judicial) {
        this.t_judicial = t_judicial;
    }
    
    /**
     * Getter for property t_eps.
     * @return Value of property t_eps.
     */
    public java.lang.String getT_eps() {
        return t_eps;
    }
    
    /**
     * Setter for property t_eps.
     * @param t_eps New value of property t_eps.
     */
    public void setT_eps(java.lang.String t_eps) {
        this.t_eps = t_eps;
    }
    
    /**
     * Getter for property t_arp.
     * @return Value of property t_arp.
     */
    public java.lang.String getT_arp() {
        return t_arp;
    }
    
    /**
     * Setter for property t_arp.
     * @param t_arp New value of property t_arp.
     */
    public void setT_arp(java.lang.String t_arp) {
        this.t_arp = t_arp;
    }
    
    /**
     * Getter for property t_categoriapase.
     * @return Value of property t_categoriapase.
     */
    public java.lang.String getT_categoriapase() {
        return t_categoriapase;
    }
    
    /**
     * Setter for property t_categoriapase.
     * @param t_categoriapase New value of property t_categoriapase.
     */
    public void setT_categoriapase(java.lang.String t_categoriapase) {
        this.t_categoriapase = t_categoriapase;
    }
    
    /**
     * Getter for property t_libtripulante.
     * @return Value of property t_libtripulante.
     */
    public java.lang.String getT_libtripulante() {
        return t_libtripulante;
    }
    
    /**
     * Setter for property t_libtripulante.
     * @param t_libtripulante New value of property t_libtripulante.
     */
    public void setT_libtripulante(java.lang.String t_libtripulante) {
        this.t_libtripulante = t_libtripulante;
    }
    
    /**
     * Getter for property t_pasaporte.
     * @return Value of property t_pasaporte.
     */
    public java.lang.String getT_pasaporte() {
        return t_pasaporte;
    }
    
    /**
     * Setter for property t_pasaporte.
     * @param t_pasaporte New value of property t_pasaporte.
     */
    public void setT_pasaporte(java.lang.String t_pasaporte) {
        this.t_pasaporte = t_pasaporte;
    }
    
    /**
     * Getter for property t_visa.
     * @return Value of property t_visa.
     */
    public java.lang.String getT_visa() {
        return t_visa;
    }
    
    /**
     * Setter for property t_visa.
     * @param t_visa New value of property t_visa.
     */
    public void setT_visa(java.lang.String t_visa) {
        this.t_visa = t_visa;
    }
    
    /**
     * Getter for property usuarioverica.
     * @return Value of property usuarioverica.
     */
    public java.lang.String getUsuarioverica() {
        return usuarioverica;
    }    
    
    /**
     * Setter for property usuarioverica.
     * @param usuarioverica New value of property usuarioverica.
     */
    public void setUsuarioverica(java.lang.String usuarioverica) {
        this.usuarioverica = usuarioverica;
    }
    
    /**
     * Getter for property fechavericacion.
     * @return Value of property fechavericacion.
     */
    public java.lang.String getFechavericacion() {
        return fechavericacion;
    }
    
    /**
     * Setter for property fechavericacion.
     * @param fechavericacion New value of property fechavericacion.
     */
    public void setFechavericacion(java.lang.String fechavericacion) {
        this.fechavericacion = fechavericacion;
    }
    
    /**
     * Getter for property t_tarpropcab.
     * @return Value of property t_tarpropcab.
     */
    public java.lang.String getT_tarpropcab() {
        return t_tarpropcab;
    }
    
    /**
     * Setter for property t_tarpropcab.
     * @param t_tarpropcab New value of property t_tarpropcab.
     */
    public void setT_tarpropcab(java.lang.String t_tarpropcab) {
        this.t_tarpropcab = t_tarpropcab;
    }
    
    /**
     * Getter for property t_copiacontcomventa.
     * @return Value of property t_copiacontcomventa.
     */
    public java.lang.String getT_copiacontcomventa() {
        return t_copiacontcomventa;
    }
    
    /**
     * Setter for property t_copiacontcomventa.
     * @param t_copiacontcomventa New value of property t_copiacontcomventa.
     */
    public void setT_copiacontcomventa(java.lang.String t_copiacontcomventa) {
        this.t_copiacontcomventa = t_copiacontcomventa;
    }
    
    /**
     * Getter for property t_copiacontarr.
     * @return Value of property t_copiacontarr.
     */
    public java.lang.String getT_copiacontarr() {
        return t_copiacontarr;
    }
    
    /**
     * Setter for property t_copiacontarr.
     * @param t_copiacontarr New value of property t_copiacontarr.
     */
    public void setT_copiacontarr(java.lang.String t_copiacontarr) {
        this.t_copiacontarr = t_copiacontarr;
    }
    
    /**
     * Getter for property t_tarpropsemi.
     * @return Value of property t_tarpropsemi.
     */
    public java.lang.String getT_tarpropsemi() {
        return t_tarpropsemi;
    }
    
    /**
     * Setter for property t_tarpropsemi.
     * @param t_tarpropsemi New value of property t_tarpropsemi.
     */
    public void setT_tarpropsemi(java.lang.String t_tarpropsemi) {
        this.t_tarpropsemi = t_tarpropsemi;
    }
    
    /**
     * Getter for property t_certemgases.
     * @return Value of property t_certemgases.
     */
    public java.lang.String getT_certemgases() {
        return t_certemgases;
    }
    
    /**
     * Setter for property t_certemgases.
     * @param t_certemgases New value of property t_certemgases.
     */
    public void setT_certemgases(java.lang.String t_certemgases) {
        this.t_certemgases = t_certemgases;
    }
    
    /**
     * Getter for property t_soat.
     * @return Value of property t_soat.
     */
    public java.lang.String getT_soat() {
        return t_soat;
    }
    
    /**
     * Setter for property t_soat.
     * @param t_soat New value of property t_soat.
     */
    public void setT_soat(java.lang.String t_soat) {
        this.t_soat = t_soat;
    }
    
    /**
     * Getter for property t_regnalcarga.
     * @return Value of property t_regnalcarga.
     */
    public java.lang.String getT_regnalcarga() {
        return t_regnalcarga;
    }
    
    /**
     * Setter for property t_regnalcarga.
     * @param t_regnalcarga New value of property t_regnalcarga.
     */
    public void setT_regnalcarga(java.lang.String t_regnalcarga) {
        this.t_regnalcarga = t_regnalcarga;
    }
    
    /**
     * Getter for property t_regnalsemire.
     * @return Value of property t_regnalsemire.
     */
    public java.lang.String getT_regnalsemire() {
        return t_regnalsemire;
    }
    
    /**
     * Setter for property t_regnalsemire.
     * @param t_regnalsemire New value of property t_regnalsemire.
     */
    public void setT_regnalsemire(java.lang.String t_regnalsemire) {
        this.t_regnalsemire = t_regnalsemire;
    }
    
    /**
     * Getter for property t_tarempresarial.
     * @return Value of property t_tarempresarial.
     */
    public java.lang.String getT_tarempresarial() {
        return t_tarempresarial;
    }
    
    /**
     * Setter for property t_tarempresarial.
     * @param t_tarempresarial New value of property t_tarempresarial.
     */
    public void setT_tarempresarial(java.lang.String t_tarempresarial) {
        this.t_tarempresarial = t_tarempresarial;
    }
    
    /**
     * Getter for property t_fotocceducond.
     * @return Value of property t_fotocceducond.
     */
    public java.lang.String getT_fotocceducond() {
        return t_fotocceducond;
    }
    
    /**
     * Setter for property t_fotocceducond.
     * @param t_fotocceducond New value of property t_fotocceducond.
     */
    public void setT_fotocceducond(java.lang.String t_fotocceducond) {
        this.t_fotocceducond = t_fotocceducond;
    }
    
    /**
     * Getter for property t_tarhabil.
     * @return Value of property t_tarhabil.
     */
    public java.lang.String getT_tarhabil() {
        return t_tarhabil;
    }
    
    /**
     * Setter for property t_tarhabil.
     * @param t_tarhabil New value of property t_tarhabil.
     */
    public void setT_tarhabil(java.lang.String t_tarhabil) {
        this.t_tarhabil = t_tarhabil;
    }
    
    /**
     * Getter for property Nombre.
     * @return Value of property Nombre.
     */
    public java.lang.String getNombre() {
        return Nombre;
    }
    
    /**
     * Setter for property Nombre.
     * @param Nombre New value of property Nombre.
     */
    public void setNombre(java.lang.String Nombre) {
        this.Nombre = Nombre;
    }
    
    /**
     * Getter for property nomeps.
     * @return Value of property nomeps.
     */
    public java.lang.String getNomeps() {
        return nomeps;
    }
    
    /**
     * Setter for property nomeps.
     * @param nomeps New value of property nomeps.
     */
    public void setNomeps(java.lang.String nomeps) {
        this.nomeps = nomeps;
    }
    
    /**
     * Getter for property nopasaporte.
     * @return Value of property nopasaporte.
     */
    public java.lang.String getNopasaporte() {
        return nopasaporte;
    }
    
    /**
     * Setter for property nopasaporte.
     * @param nopasaporte New value of property nopasaporte.
     */
    public void setNopasaporte(java.lang.String nopasaporte) {
        this.nopasaporte = nopasaporte;
    }
    
    /**
     * Getter for property tarprop.
     * @return Value of property tarprop.
     */
    public java.lang.String getTarprop() {
        return tarprop;
    }
    
    /**
     * Setter for property tarprop.
     * @param tarprop New value of property tarprop.
     */
    public void setTarprop(java.lang.String tarprop) {
        this.tarprop = tarprop;
    }
    
    /**
     * Getter for property fecvenprop.
     * @return Value of property fecvenprop.
     */
    public java.lang.String getFecvenprop() {
        return fecvenprop;
    }
    
    /**
     * Setter for property fecvenprop.
     * @param fecvenprop New value of property fecvenprop.
     */
    public void setFecvenprop(java.lang.String fecvenprop) {
        this.fecvenprop = fecvenprop;
    }
    
    /**
     * Getter for property ventarpropsemi.
     * @return Value of property ventarpropsemi.
     */
    public java.lang.String getVentarpropsemi() {
        return ventarpropsemi;
    }
    
    /**
     * Setter for property ventarpropsemi.
     * @param ventarpropsemi New value of property ventarpropsemi.
     */
    public void setVentarpropsemi(java.lang.String ventarpropsemi) {
        this.ventarpropsemi = ventarpropsemi;
    }
    
    /**
     * Getter for property venregnalsemi.
     * @return Value of property venregnalsemi.
     */
    public java.lang.String getVenregnalsemi() {
        return venregnalsemi;
    }    
     
    /**
     * Setter for property venregnalsemi.
     * @param venregnalsemi New value of property venregnalsemi.
     */
    public void setVenregnalsemi(java.lang.String venregnalsemi) {
        this.venregnalsemi = venregnalsemi;
    }
    
    /**
     * Getter for property vencepasaporte.
     * @return Value of property vencepasaporte.
     */
    public java.lang.String getVencepasaporte() {
        return vencepasaporte;
    }
    
    /**
     * Setter for property vencepasaporte.
     * @param vencepasaporte New value of property vencepasaporte.
     */
    public void setVencepasaporte(java.lang.String vencepasaporte) {
        this.vencepasaporte = vencepasaporte;
    }
    
    /**
     * Getter for property poliza_andina.
     * @return Value of property poliza_andina.
     */
    public java.lang.String getPoliza_andina() {
        return poliza_andina;
    }
    
    /**
     * Setter for property poliza_andina.
     * @param poliza_andina New value of property poliza_andina.
     */
    public void setPoliza_andina(java.lang.String poliza_andina) {
        this.poliza_andina = poliza_andina;
    }
    
    /**
     * Getter for property fecvenandina.
     * @return Value of property fecvenandina.
     */
    public java.lang.String getFecvenandina() {
        return fecvenandina;
    }
    
    /**
     * Setter for property fecvenandina.
     * @param fecvenandina New value of property fecvenandina.
     */
    public void setFecvenandina(java.lang.String fecvenandina) {
        this.fecvenandina = fecvenandina;
    }
    
    /**
     * Getter for property t_polizaandina.
     * @return Value of property t_polizaandina.
     */
    public java.lang.String getT_polizaandina() {
        return t_polizaandina;
    }
    
    /**
     * Setter for property t_polizaandina.
     * @param t_polizaandina New value of property t_polizaandina.
     */
    public void setT_polizaandina(java.lang.String t_polizaandina) {
        this.t_polizaandina = t_polizaandina;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
}

