/*
 * ConfiguracionUsuario.java
 *
 * Created on 30 de noviembre de 2005, 02:49 PM
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  dlamadrid
 */
public class ConfiguracionUsuario
{
    private String nombre="";
    private String configuracion="";
    private String linea="";
    private String var1="";
    private String var2="";
    private String last_update="" ;
    private String user_update="";
    private String creation_date="";
    private String creation_user="";
    private String base="";
    private String dstrct="";
    
    public static ConfiguracionUsuario load(ResultSet rs)throws SQLException {
         ConfiguracionUsuario conf = new ConfiguracionUsuario();
         conf.setNombre (rs.getString ("nombre"));
         conf.setConfiguracion (rs.getString ("configuracion"));
         conf.setLinea (rs.getString ("linea"));
         conf.setVar1 (rs.getString ("var1"));
         conf.setVar2 (rs.getString ("var2"));
         conf.setCreation_user (rs.getString ("creation_user"));
         conf.setCreation_date (rs.getString ("creation_date")); 
         conf.setDstrct (rs.getString ("dstrct"));
          return conf;
    }
    
    /** Creates a new instance of ConfiguracionUsuario */
    public ConfiguracionUsuario ()
    {
    }
    
    
    /**
     * Getter for property configuracion.
     * @return Value of property configuracion.
     */
    public java.lang.String getConfiguracion ()
    {
        return configuracion;
    }
    
    /**
     * Setter for property configuracion.
     * @param configuracion New value of property configuracion.
     */
    public void setConfiguracion (java.lang.String configuracion)
    {
        this.configuracion = configuracion;
    }
    
    /**
     * Getter for property linea.
     * @return Value of property linea.
     */
    public java.lang.String getLinea ()
    {
        return linea;
    }
    
    /**
     * Setter for property linea.
     * @param linea New value of property linea.
     */
    public void setLinea (java.lang.String linea)
    {
        this.linea = linea;
    }
    
    /**
     * Getter for property var1.
     * @return Value of property var1.
     */
    public java.lang.String getVar1 ()
    {
        return var1;
    }
    
    /**
     * Setter for property var1.
     * @param var1 New value of property var1.
     */
    public void setVar1 (java.lang.String var1)
    {
        this.var1 = var1;
    }
    
    /**
     * Getter for property var2.
     * @return Value of property var2.
     */
    public java.lang.String getVar2 ()
    {
        return var2;
    }
    
    /**
     * Setter for property var2.
     * @param var2 New value of property var2.
     */
    public void setVar2 (java.lang.String var2)
    {
        this.var2 = var2;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user ()
    {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user)
    {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date ()
    {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date)
    {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre ()
    {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre (java.lang.String nombre)
    {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct ()
    {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct (java.lang.String dstrct)
    {
        this.dstrct = dstrct;
    }
    
}
