/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.util.*;


/**
 *
 * @author Alvaro
 */
public class EgresoDetalleTsp extends EgresoDetalle{
    


    private String fecha_envio_ws;
    private String creation_date_real;
    private int pk_novedad;

    private String dstrct_factura;
    private String tipo_documento_factura;
    private String documento_factura;

    private List   listaCXPItem;


    /** Creates a new instance of EgresoDetalleTsp */
    public EgresoDetalleTsp() {
    }
    


    public static EgresoDetalleTsp load(java.sql.ResultSet rs)throws java.sql.SQLException{

        EgresoDetalleTsp egresoDetalle = new EgresoDetalleTsp();
        egresoDetalle.setReg_status( rs.getString("reg_status") ) ;
        egresoDetalle.setDstrct( rs.getString("reg_status") ) ;
        egresoDetalle.setBranch_code( rs.getString("branch_code") ) ;
        egresoDetalle.setBank_account_no( rs.getString("bank_account_no") ) ;
        egresoDetalle.setDocument_no( rs.getString("document_no") ) ;
        egresoDetalle.setItem_no( rs.getString("item_no") ) ;
        egresoDetalle.setConcept_code( rs.getString("concept_code") ) ;
        egresoDetalle.setVlr( rs.getDouble("vlr") ) ;
        egresoDetalle.setVlr_for( rs.getDouble("vlr_for") ) ;
        egresoDetalle.setCurrency( rs.getString("currency") ) ;
        egresoDetalle.setOc( rs.getString("oc") ) ;        
        egresoDetalle.setLast_update( rs.getString("last_update") ) ;
        egresoDetalle.setUser_update( rs.getString("user_update") ) ;
        egresoDetalle.setCreation_date( rs.getString("creation_date") ) ;
        egresoDetalle.setCreation_user( rs.getString("creation_user") ) ;
        egresoDetalle.setDescription( rs.getString("description") ) ;        
        egresoDetalle.setBase( rs.getString("base") ) ;
        egresoDetalle.setTasa( rs.getDouble("tasa") ) ;
        egresoDetalle.setTransaccion(rs.getInt("transaccion"));
        egresoDetalle.setTipo_documento( rs.getString("tipo_documento") ) ;
        egresoDetalle.setDocumento( rs.getString("documento") ) ;
        egresoDetalle.setTipo_pago( rs.getString("tipo_pago") ) ;
        egresoDetalle.setCuenta( rs.getString("cuenta") ) ;
        egresoDetalle.setAuxiliar( rs.getString("auxiliar") ) ;
        egresoDetalle.setFecha_envio_ws( rs.getString("fecha_envio_ws") );
        egresoDetalle.setCreation_date_real( rs.getString("creation_date_real") );
        egresoDetalle.setPk_novedad( rs.getInt("pk_novedad") );
        egresoDetalle.setDstrct_factura( rs.getString("dstrct_factura") ) ;
        egresoDetalle.setTipo_documento_factura( rs.getString("tipo_documento_factura") ) ;
        egresoDetalle.setDocumento_factura( rs.getString("documento_factura") ) ;

        return egresoDetalle;

    }

    /**
     * @return the dstrct_factura
     */
    public String getDstrct_factura() {
        return dstrct_factura;
    }

    /**
     * @param dstrct_factura the dstrct_factura to set
     */
    public void setDstrct_factura(String dstrct_factura) {
        this.dstrct_factura = dstrct_factura;
    }

    /**
     * @return the tipo_documento_factura
     */
    public String getTipo_documento_factura() {
        return tipo_documento_factura;
    }

    /**
     * @param tipo_documento_factura the tipo_documento_factura to set
     */
    public void setTipo_documento_factura(String tipo_documento_factura) {
        this.tipo_documento_factura = tipo_documento_factura;
    }

    /**
     * @return the documento_factura
     */
    public String getDocumento_factura() {
        return documento_factura;
    }

    /**
     * @param documento_factura the documento_factura to set
     */
    public void setDocumento_factura(String documento_factura) {
        this.documento_factura = documento_factura;
    }

    /**
     * @return the fecha_envio_ws
     */
    public String getFecha_envio_ws() {
        return fecha_envio_ws;
    }

    /**
     * @param fecha_envio_ws the fecha_envio_ws to set
     */
    public void setFecha_envio_ws(String fecha_envio_ws) {
        this.fecha_envio_ws = fecha_envio_ws;
    }

    /**
     * @return the creation_date_real
     */
    public String getCreation_date_real() {
        return creation_date_real;  
    }

    /**
     * @param creation_date_real the creation_date_real to set
     */
    public void setCreation_date_real(String creation_date_real) {
        this.creation_date_real = creation_date_real;
    }

    /**
     * @return the pk_novedad
     */
    public int getPk_novedad() {
        return pk_novedad;
    }

    /**
     * @param pk_novedad the pk_novedad to set
     */
    public void setPk_novedad(int pk_novedad) {
        this.pk_novedad = pk_novedad;
    }



}
