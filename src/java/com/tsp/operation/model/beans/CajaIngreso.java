package com.tsp.operation.model.beans;

/**
 * Bean para la tabla caja_ingreso
 * 3/02/2012
 * @author darrieta
 */
public class CajaIngreso {

    private String dstrct;
    private int id;
    private String agencia;
    private String fecha;
    private String numIngreso;
    private double valor;
    private String recibido;
    private String creation_user;
    private String usuarioRecibido;

    /**
     * Get the value of usuarioRecibido
     *
     * @return the value of usuarioRecibido
     */
    public String getUsuarioRecibido() {
        return usuarioRecibido;
    }

    /**
     * Set the value of usuarioRecibido
     *
     * @param usuarioRecibido new value of usuarioRecibido
     */
    public void setUsuarioRecibido(String usuarioRecibido) {
        this.usuarioRecibido = usuarioRecibido;
    }

    /**
     * Get the value of creation_user
     *
     * @return the value of creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * Set the value of creation_user
     *
     * @param creation_user new value of creation_user
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * Get the value of recibido
     *
     * @return the value of recibido
     */
    public String getRecibido() {
        return recibido;
    }

    /**
     * Set the value of recibido
     *
     * @param recibido new value of recibido
     */
    public void setRecibido(String recibido) {
        this.recibido = recibido;
    }

    /**
     * Get the value of valor
     *
     * @return the value of valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * Set the value of valor
     *
     * @param valor new value of valor
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * Get the value of numIngreso
     *
     * @return the value of numIngreso
     */
    public String getNumIngreso() {
        return numIngreso;
    }

    /**
     * Set the value of numIngreso
     *
     * @param numIngreso new value of numIngreso
     */
    public void setNumIngreso(String numIngreso) {
        this.numIngreso = numIngreso;
    }

    /**
     * Get the value of fecha
     *
     * @return the value of fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param fecha new value of fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Get the value of agencia
     *
     * @return the value of agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Set the value of agencia
     *
     * @param agencia new value of agencia
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Get the value of dstrct
     *
     * @return the value of dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * Set the value of dstrct
     *
     * @param dstrct new value of dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

}
