
package com.tsp.operation.model.beans;

import java.util.*;
/*
  @author  fvillacob
 */

public class Planillas {
    
    
// Atributos
    
    private String distrito;
    private String planilla;
    private String origen;
    private String destino;
    private double carga;
    private String tipo;
    private String unidad;
    private String placa;
    private String conductor;
    private String cedulaConductor;
    private String propietario;
    private String cedulaPropietario;
    private String fechaDespacho;
    private String fechaPlaneacionLLegada;
    private String fechaCreacion;
    private String fechaImpresion;
    private String fechaCumplido;
    private double valor;
    private String moneda;
    private String agenciaElaboracion;
    private String agenciaCumplido;
    private String despachador;
    private String cedulaDespachador;
    
    private String remesa;
    private String sj;
    private String sjDescripcion;
    private String cliente;
    private String nitCliente;
    private String proveedorPeajes;
    private String nitProveedorPeajes;   
    private int cantidadPeajes;
    private double valorPeajes;
    private double galonesACPM;
    private double valorACPM;
    private String proveedorACPM;
    private String nitProveedorACPM;
    private String remision="";
    private List listaMovimiento;
    private List listaTiempo;
    
    //Mario
    private DatosPlanilla Datos = null;
    private List Tiempos        = null;
    private List Operaciones    = null;

    //karen
    private String precintos;
    private String contenedores;
    private double valorFlete;
    private String estado;
    private String trailer;
    private String cumplido;
    private String fecha_anul="";
    
    //Henry
    private String cantCumplida;
    private String unidadCumplida;
    
    //Diogenes
    private String usuario_cumplio;
    private String discrepancia;
    private String cod_agencia_despacho; 
    
    private String account_code_i;
    private String account_code_c;
    private String cmc; 
    private double valorMe;
// Metodos  
    
    public Planillas() { }
   
  //set    
    public void setDistrito(String distrito){
        this.distrito=distrito;
    }
    
    public void setPlanilla(String oc){
        this.planilla=oc;
    }
    
    public void setOrigen(String origen){
        this.origen=origen;
    }
    public void setTipo(String tipo){
        this.tipo=tipo;
    }
    
    public void setDestino(String destino){
        this.destino=destino;
    }
    
    public void setCarga(double cantidad){
        this.carga=cantidad;
    }
    
    public void setUnidad(String unidad){
        this.unidad=unidad;
    }
    
    public void setPlaca(String placa){
        this.placa=placa;
    }
    
    public void setConductor(String nombre){
        this.conductor=nombre;
    }
    
    public void setCedulaConductor(String cedula){
        this.cedulaConductor=cedula;
    }
    
    public void setPropietario(String nombre){
        this.propietario=nombre;
    }
   
    public void setCedulaPropietario(String cedula){
        this.cedulaPropietario=cedula;
    }
    
    public void setFechaDespacho(String fecha){
        this.fechaDespacho=fecha;
    }
    
    public void setFechaPlaneacion(String fecha){
        this.fechaPlaneacionLLegada=fecha;
    }
    
    public void setFechaCreacion(String fecha){
        this.fechaCreacion=fecha;
    }
    
    public void setFechaImpresion(String fecha){
        this.fechaImpresion=fecha;
    }
    
    public void setFechaCumplido(String fecha){
        this.fechaCumplido=fecha;
    }
    
    public void setValor(double valor){
        this.valor=valor;
    }
    
    public void setMoneda(String moneda){
        this.moneda = moneda;
    }
    
    public void setAgenciaDespacho(String agencia){
        this.agenciaElaboracion=agencia;
    }
    
    public void setAgenciaCumplido(String agencia){
        this.agenciaCumplido=agencia;
    }
    
     public void setDespachador(String nombre){
        this.despachador=nombre;
    }
   
    public void setCedulaDespachador(String cedula){
        this.cedulaDespachador=cedula;
    }
    
    public void setRemesa(String ot){
        this.remesa=ot;
    }
    
    public void  setSJ(String sj){
        this.sj=sj;
    }
    
    public void setSJDescripcion(String descripcion){
        this.sjDescripcion=descripcion;
    }
    
    public void setCliente(String nombre){
        this.cliente=nombre;
    }
   
    public void setNitCliente(String nit){
        this.nitCliente=nit;
    }
    
    public void setCantidadPeajes(int total){
        this.cantidadPeajes=total;
    }
    
    public void setValorPeajes(double valor){
        this.valorPeajes=valor;
    }
    
    public void setGalonesACPM(double cantidad){
        this.galonesACPM=cantidad;
    }
    
    public void setValorACPM(double valor){
        this.valorACPM=valor;
    }
    
    public void setProveedorACPM(String nombre){
        this.proveedorACPM=nombre;
    }
   
    public void setNitProveedorACPM(String nit){
        this.nitProveedorACPM=nit;
    }
    
    public void setListaMovimiento(List mov){
        this.listaMovimiento=mov;
    }
    
    public void setListaTiempo(List lista){
        this.listaTiempo=lista;
    }
    
    public void setProveedorPeajes(String nombre){
        this.proveedorPeajes=nombre;
    }
   
    public void setNitProveedorPeajes(String nit){
        this.nitProveedorPeajes=nit;
    }
  
    
  
    
    // get    
    public String getDistrito(){
        return this.distrito;
    }
    
    public String getPlanilla(){
        return this.planilla;
    }
    
    public String getOrigen(){
        return this.origen;
    }
    
    public String getDestino(){
        return this.destino;
    }
    
    public double getCarga(){
        return this.carga;
    }
    
    public String getUnidad(){
        return this.unidad;
    }
    
    public String getPlaca(){
        return this.placa;
    }
    
    public String getConductor(){
        return this.conductor;
    }
    
    public String  getCedulaConductor(){
        return this.cedulaConductor;
    }
    
    public String getPropietario(){
        return this.propietario;
    }
   
    public String getCedulaPropietario(){
        return this.cedulaPropietario;
    }
    
    public String getFechaDespacho(){
        return this.fechaDespacho;
    }
    
    public String getFechaPlaneacion(){
        return this.fechaPlaneacionLLegada;
    }
    
    public String getFechaCreacion(){
        return this.fechaCreacion;
    }
    
    public String getFechaImpresion(){
        return this.fechaImpresion;
    }
    
    public String getFechaCumplido(){
        return this.fechaCumplido;
    }
    
    public double getValor(){
        return this.valor;
    }
    
    public String getMoneda(){
        return this.moneda;
    }
    
    public String getAgenciaDespacho(){
        return this.agenciaElaboracion;
    }
    
    public String getAgenciaCumplido(){
        return this.agenciaCumplido;
    }
    
     public String getDespachador(){
       return this.despachador;
    }
   
    public String getCedulaDespachador(){
        return this.cedulaDespachador;
    }
    
    public String getRemesa(){
        return this.remesa;
    }
    
    public String getSJ(){
        return this.sj;
    }
    
    public String getSJDescripcion(){
        return this.sjDescripcion;
    }
    
    public String getCliente(){
        return this.cliente;
    }
   
    public String getNitCliente(){
        return this.nitCliente;
    }
    
    public int getCantidadPeajes(){
        return this.cantidadPeajes;
    }
    
    public double getValorPeajes(){
       return this.valorPeajes;
    }
    
    public double getGalonesACPM(){
        return this.galonesACPM;
    }
    
    public double getValorACPM(){
        return this.valorACPM;
    }
    
    public String getProveedorACPM(){
        return this.proveedorACPM;
    }
   
    public String getNitProveedorACPM(){
        return this.nitProveedorACPM;
    }
    
    public List getListaMovimiento(){
        return this.listaMovimiento;
    }
   
    public String getTipo(){
        return this.tipo;
    }
  
    public String getProveedorPeajes(){
        return this.proveedorPeajes;
    }
   
    public String getNitProveedorPeajes(){
        return this.nitProveedorPeajes;
    }
  
   public List  getListaTiempo(){
       return this.listaTiempo;
   }
   
   
   //Mario
   // setter
    public void setDatos(DatosPlanilla valor){
        this.Datos = valor;
    }   
    public void setTiempos(List ListaTiempos){
        this.Tiempos = ListaTiempos;
    }
    public void setOperaciones(List ListaOperaciones){
        this.Operaciones = ListaOperaciones;
    }
    
    //getter
    public List getOperaciones(){
        return this.Operaciones;
    }
    public List getTiempos(){
        return this.Tiempos;
    }
    public DatosPlanilla getDatos(){
        return this.Datos;
    }

    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }    
   
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }    
   
    /**
     * Getter for property precintos.
     * @return Value of property precintos.
     */
    public java.lang.String getPrecintos() {
        return precintos;
    }    
    
    /**
     * Setter for property precintos.
     * @param precintos New value of property precintos.
     */
    public void setPrecintos(java.lang.String precintos) {
        this.precintos = precintos;
    }
    
    /**
     * Getter for property contenedores.
     * @return Value of property contenedores.
     */
    public java.lang.String getContenedores() {
        return contenedores;
    }
    
    /**
     * Setter for property contenedores.
     * @param contenedores New value of property contenedores.
     */
    public void setContenedores(java.lang.String contenedores) {
        this.contenedores = contenedores!=null?contenedores:"";
    }
    
    /**
     * Getter for property valorFlete.
     * @return Value of property valorFlete.
     */
    public double getValorFlete() {
        return valorFlete;
    }
    
    /**
     * Setter for property valorFlete.
     * @param valorFlete New value of property valorFlete.
     */
    public void setValorFlete(double valorFlete) {
        this.valorFlete = valorFlete;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property trailer.
     * @return Value of property trailer.
     */
    public java.lang.String getTrailer() {
        return trailer;
    }
    
    /**
     * Setter for property trailer.
     * @param trailer New value of property trailer.
     */
    public void setTrailer(java.lang.String trailer) {
        this.trailer = trailer;
    }
    
    /**
     * Getter for property cumplido.
     * @return Value of property cumplido.
     */
    public java.lang.String getCumplido() {
        return cumplido;
    }
    
    /**
     * Setter for property cumplido.
     * @param cumplido New value of property cumplido.
     */
    public void setCumplido(java.lang.String cumplido) {
        this.cumplido = cumplido;
    }    
    
    /**
     * Getter for property fecha_anul.
     * @return Value of property fecha_anul.
     */
    public java.lang.String getFecha_anul() {
        return fecha_anul;
    }
    
    /**
     * Setter for property fecha_anul.
     * @param fecha_anul New value of property fecha_anul.
     */
    public void setFecha_anul(java.lang.String fecha_anul) {
        this.fecha_anul = fecha_anul;
    }
    
    /**
     * Getter for property cantCumplida.
     * @return Value of property cantCumplida.
     */
    public java.lang.String getCantCumplida() {
        return cantCumplida;
    }
    
    /**
     * Setter for property cantCumplida.
     * @param cantCumplida New value of property cantCumplida.
     */
    public void setCantCumplida(java.lang.String cantCumplida) {
        if (cantCumplida!=null)
            this.cantCumplida = cantCumplida;
        else
            this.cantCumplida = "";
    }
    
    /**
     * Getter for property unidadCumplida.
     * @return Value of property unidadCumplida.
     */
    public java.lang.String getUnidadCumplida() {
        return unidadCumplida;
    }
    
    /**
     * Setter for property unidadCumplida.
     * @param unidadCumplida New value of property unidadCumplida.
     */
    public void setUnidadCumplida(java.lang.String unidadCumplida) {
        if (unidadCumplida!=null)
            this.unidadCumplida = unidadCumplida;
        else
            this.unidadCumplida = "";
    }
    
    /**
     * Getter for property usuario_cumplio.
     * @return Value of property usuario_cumplio.
     */
    public java.lang.String getUsuario_cumplio() {
        return usuario_cumplio;
    }
    
    /**
     * Setter for property usuario_cumplio.
     * @param usuario_cumplio New value of property usuario_cumplio.
     */
    public void setUsuario_cumplio(java.lang.String usuario_cumplio) {
        this.usuario_cumplio = usuario_cumplio;
    }
    
    /**
     * Getter for property discrepancia.
     * @return Value of property discrepancia.
     */
    public java.lang.String getDiscrepancia() {
        return discrepancia;
    }
    
    /**
     * Setter for property discrepancia.
     * @param discrepancia New value of property discrepancia.
     */
    public void setDiscrepancia(java.lang.String discrepancia) {
        this.discrepancia = discrepancia;
    }
    
    /**
     * Getter for property cod_agencia_despacho.
     * @return Value of property cod_agencia_despacho.
     */
    public java.lang.String getCod_agencia_despacho() {
        return cod_agencia_despacho;
    }
    
    /**
     * Setter for property cod_agencia_despacho.
     * @param cod_agencia_despacho New value of property cod_agencia_despacho.
     */
    public void setCod_agencia_despacho(java.lang.String cod_agencia_despacho) {
        this.cod_agencia_despacho = cod_agencia_despacho;
    }
    
    /**
     * Getter for property valorMe.
     * @return Value of property valorMe.
     */
    public double getValorMe() {
        return valorMe;
    }
    
    /**
     * Setter for property valorMe.
     * @param valorMe New value of property valorMe.
     */
    public void setValorMe(double valorMe) {
        this.valorMe = valorMe;
    }
    
    /**
     * Getter for property account_code_i.
     * @return Value of property account_code_i.
     */
    public java.lang.String getAccount_code_i() {
        return account_code_i;
    }
    
    /**
     * Setter for property account_code_i.
     * @param account_code_i New value of property account_code_i.
     */
    public void setAccount_code_i(java.lang.String account_code_i) {
        this.account_code_i = account_code_i;
    }
    
    /**
     * Getter for property account_code_c.
     * @return Value of property account_code_c.
     */
    public java.lang.String getAccount_code_c() {
        return account_code_c;
    }
    
    /**
     * Setter for property account_code_c.
     * @param account_code_c New value of property account_code_c.
     */
    public void setAccount_code_c(java.lang.String account_code_c) {
        this.account_code_c = account_code_c;
    }
    
    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }
    
    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }
    
}// end clase
