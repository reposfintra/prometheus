/**************************************************************************
 * Nombre:        Retroactivo.java
 * Descripci�n:   Beans de acuerdo especial.               *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:         09 de febrero de 2006, 02:01 PM       *
 * Versi�n:       Java  1.0                                      *
 * Copyright:     Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  dbastidas
 */
public class Retroactivo implements Serializable {
    private String dstrct;
    private String std_job_no;
    private String desStdjob;
    private String ruta;
    private String fecha1;
    private String fecha2;
    private double valor_retro;
    private double valor_stdjob;
    private String estado;
    private String base;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    
    
    /** Creates a new instance of Retroactivo */
    public Retroactivo() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property fecha1.
     * @return Value of property fecha1.
     */
    public java.lang.String getFecha1() {
        return fecha1;
    }
    
    /**
     * Setter for property fecha1.
     * @param fecha1 New value of property fecha1.
     */
    public void setFecha1(java.lang.String fecha1) {
        this.fecha1 = fecha1;
    }
    
    /**
     * Getter for property fecha2.
     * @return Value of property fecha2.
     */
    public java.lang.String getFecha2() {
        return fecha2;
    }
    
    /**
     * Setter for property fecha2.
     * @param fecha2 New value of property fecha2.
     */
    public void setFecha2(java.lang.String fecha2) {
        this.fecha2 = fecha2;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property valor_retro.
     * @return Value of property valor_retro.
     */
    public double getValor_retro() {
        return valor_retro;
    }
    
    /**
     * Setter for property valor_retro.
     * @param valor_retro New value of property valor_retro.
     */
    public void setValor_retro(double valor_retro) {
        this.valor_retro = valor_retro;
    }
    
    /**
     * Getter for property valor_stdjob.
     * @return Value of property valor_stdjob.
     */
    public double getValor_stdjob() {
        return valor_stdjob;
    }
    
    /**
     * Setter for property valor_stdjob.
     * @param valor_stdjob New value of property valor_stdjob.
     */
    public void setValor_stdjob(double valor_stdjob) {
        this.valor_stdjob = valor_stdjob;
    }
    
    /**
     * Getter for property desStdjob.
     * @return Value of property desStdjob.
     */
    public java.lang.String getDesStdjob() {
        return desStdjob;
    }
    
    /**
     * Setter for property desStdjob.
     * @param desStdjob New value of property desStdjob.
     */
    public void setDesStdjob(java.lang.String desStdjob) {
        this.desStdjob = desStdjob;
    }
    
}
