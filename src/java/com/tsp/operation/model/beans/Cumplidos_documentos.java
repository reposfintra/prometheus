/*
 * Cumplidos_documentos.java
 *
 * Created on 27 de octubre de 2005, 05:22 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Jose
 */
public class Cumplidos_documentos {
    private String numpla;
    private String numrem;
    private String std_job_no;
    private String document_type;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String distrito;
    private String base;
    private String rec_status;
    /** Creates a new instance of Cumplidos_documentos */
    public Cumplidos_documentos() {
    }

    public java.lang.String getBase() {
        return base;
    }

    public void setBase(java.lang.String base) {
        this.base = base;
    }

    public java.lang.String getDistrito() {
        return distrito;
    }

    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }

    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public java.lang.String getNumpla() {
        return numpla;
    }

    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }

    public java.lang.String getNumrem() {
        return numrem;
    }

    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }

    public java.lang.String getRec_status() {
        return rec_status;
    }

    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }

    public java.lang.String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }

    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }

    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }

    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }

    public java.lang.String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(java.lang.String document_type) {
        this.document_type = document_type;
    }
    
}
