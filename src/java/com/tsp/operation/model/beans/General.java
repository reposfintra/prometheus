/*
 * General.java
 *
 * Created on 16 de septiembre de 2005, 04:00 PM
 */

package com.tsp.operation.model.beans;

import java.sql.*;

/**
 *
 * @author  mfontalvo
 */
public class General {
    
    private String Codigo;
    private String Descripcion;

    private String Codigo1;
    private String Descripcion1;    

    private String Codigo2;
    private String Descripcion2;    
        
    /** Creates a new instance of General */
    public General() {
        Codigo = "";
        Descripcion = "";
        Codigo1 = "";
        Descripcion1 = "";
        Codigo2 = "";
        Descripcion2 = "";
    }
    
    public static General load(ResultSet rs )throws SQLException{
        General datos = new General();
        datos.setCodigo(rs.getString(1));
        datos.setDescripcion(rs.getString(2));
        return datos;
    }
    
    public static General load1(ResultSet rs )throws SQLException{
        General datos = new General();
        datos.setCodigo(rs.getString(1));
        datos.setDescripcion(rs.getString(2));
        datos.setCodigo1(rs.getString(3));
        datos.setDescripcion1(rs.getString(4));
        return datos;
    }
    
    
    public void setCodigo(String value){
        Codigo = value;
    }
    public void setDescripcion(String value){
        Descripcion = value;
    }
    public void setCodigo1(String value){
        Codigo1 = value;
    }
    public void setDescripcion1(String value){
        Descripcion1 = value;
    }
    public void setCodigo2(String value){
        Codigo2 = value;
    }
    public void setDescripcion2(String value){
        Descripcion2 = value;
    }

    
    
    public String getCodigo(){
        return Codigo;
    }
    public String getDescripcion(){
        return Descripcion ;
    }
    public String getCodigo1(){
        return Codigo1;
    }
    public String getDescripcion1(){
        return Descripcion1 ;
    }
    public String getCodigo2(){
        return Codigo2;
    }
    public String getDescripcion2(){
        return Descripcion2 ;
    }
    
    
    
}
