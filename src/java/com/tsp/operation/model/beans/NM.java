/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class NM {
 
    private String fom;
    private String documento_maestro;
    private int cuota_primera;
    private int cuota_ultima;
    private String solicitud;
    private String nit;
    private String codcli;
    private String nomcli;
    private String fecha_factura;
    private Double valor_saldo;
    
    
    

    /** Extrae un registro de la BD correspondientes a las acciones de una solicitudes que no esta facturada */
    public static NM load(java.sql.ResultSet rs)throws java.sql.SQLException{

        NM nm = new NM();

        nm.setFom(rs.getString("fom") ) ;
        nm.setDocumento_maestro(rs.getString("documento_maestro") ) ;
        nm.setCuota_primera(rs.getInt("cuota_primera") ) ;
        nm.setCuota_ultima(rs.getInt("cuota_ultima") ) ;
        nm.setSolicitud(rs.getString("solicitud") ) ;
        nm.setNit(rs.getString("nit") ) ;
        nm.setCodcli(rs.getString("codcli") ) ;
        nm.setNomcli(rs.getString("nomcli") ) ;
        nm.setNit(rs.getString("nit") ) ;
        nm.setFecha_factura(rs.getString("fecha_factura") ) ;
        nm.setValor_saldo(rs.getDouble("valor_saldo") ) ;

        return nm;

    }

    
    
    

    /**
     * @return the fom
     */
    public String getFom() {
        return fom;
    }

    /**
     * @param fom the fom to set
     */
    public void setFom(String fom) {
        this.fom = fom;
    }

    /**
     * @return the documento_maestro
     */
    public String getDocumento_maestro() {
        return documento_maestro;
    }

    /**
     * @param documento_maestro the documento_maestro to set
     */
    public void setDocumento_maestro(String documento_maestro) {
        this.documento_maestro = documento_maestro;
    }

    /**
     * @return the solicitud
     */
    public String getSolicitud() {
        return solicitud;
    }

    /**
     * @param solicitud the solicitud to set
     */
    public void setSolicitud(String solicitud) {
        this.solicitud = solicitud;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the codcli
     */
    public String getCodcli() {
        return codcli;
    }

    /**
     * @param codcli the codcli to set
     */
    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    /**
     * @return the nomcli
     */
    public String getNomcli() {
        return nomcli;
    }

    /**
     * @param nomcli the nomcli to set
     */
    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    /**
     * @return the fecha_factura
     */
    public String getFecha_factura() {
        return fecha_factura;
    }

    /**
     * @param fecha_factura the fecha_factura to set
     */
    public void setFecha_factura(String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }

    /**
     * @return the valor_saldo
     */
    public Double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo(Double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    /**
     * @return the cuota_primera
     */
    public int getCuota_primera() {
        return cuota_primera;
    }

    /**
     * @param cuota_primera the cuota_primera to set
     */
    public void setCuota_primera(int cuota_primera) {
        this.cuota_primera = cuota_primera;
    }

    /**
     * @return the cuota_ultima
     */
    public int getCuota_ultima() {
        return cuota_ultima;
    }

    /**
     * @param cuota_ultima the cuota_ultima to set
     */
    public void setCuota_ultima(int cuota_ultima) {
        this.cuota_ultima = cuota_ultima;
    }
    
}
