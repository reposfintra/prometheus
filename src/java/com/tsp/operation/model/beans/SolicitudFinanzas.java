/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Harold Cuello G.
 */
public class SolicitudFinanzas {
    
    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    
    private String id_persona;
    private String tipo;
    
    private String salario
                , honorarios
                , otros_ingresos
                , total_ingresos
                , descuento_nomina
                , gastos_arriendo
                , gastos_creditos
                , otros_gastos
                , total_egresos
                , activos
                , pasivos
                , total_patrimonio;
    
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    
    public SolicitudFinanzas load (ResultSet rs) throws SQLException {
        SolicitudFinanzas sf = new SolicitudFinanzas();
        sf.setNumeroSolicitud(rs.getString("numero_solicitud"));
        sf.setTipo(rs.getString("tipo"));
        sf.setDstrct(rs.getString("dstrct"));
        sf.setId_persona(rs.getString("id_persona"));
        
        sf.setSalario(rs.getString("salario"));
        sf.setHonorarios(rs.getString("honorarios"));
        sf.setOtros_ingresos(rs.getString("otros_ingresos"));
        sf.setTotal_ingresos(rs.getString("total_ingresos")); 
        sf.setDescuento_nomina(rs.getString("descuento_nomina"));
        sf.setGastos_arriendo(rs.getString("gastos_arriendo"));
        sf.setGastos_creditos(rs.getString("gastos_creditos"));
        sf.setOtros_gastos(rs.getString("otros_gastos"));
        sf.setTotal_egresos(rs.getString("total_egresos"));
        sf.setActivos(rs.getString("activos")); 
        sf.setPasivos(rs.getString("pasivos"));
        sf.setTotal_patrimonio(rs.getString("total_patrimonio"));
        
        return sf;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    public String getId_persona() {
        return id_persona;
    }

    public void setId_persona(String id_persona) {
        this.id_persona = id_persona;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSalario() {
        return salario;
    }

    public void setSalario(String salario) {
        this.salario = salario;
    }

    public String getHonorarios() {
        return honorarios;
    }

    public void setHonorarios(String honorarios) {
        this.honorarios = honorarios;
    }

    public String getOtros_ingresos() {
        return otros_ingresos;
    }

    public void setOtros_ingresos(String otros_ingresos) {
        this.otros_ingresos = otros_ingresos;
    }

    public String getTotal_ingresos() {
        return total_ingresos;
    }

    public void setTotal_ingresos(String total_ingresos) {
        this.total_ingresos = total_ingresos;
    }

    public String getDescuento_nomina() {
        return descuento_nomina;
    }

    public void setDescuento_nomina(String descuento_nomina) {
        this.descuento_nomina = descuento_nomina;
    }

    public String getGastos_arriendo() {
        return gastos_arriendo;
    }

    public void setGastos_arriendo(String gastos_arriendo) {
        this.gastos_arriendo = gastos_arriendo;
    }

    public String getGastos_creditos() {
        return gastos_creditos;
    }

    public void setGastos_creditos(String gastos_creditos) {
        this.gastos_creditos = gastos_creditos;
    }

    public String getOtros_gastos() {
        return otros_gastos;
    }

    public void setOtros_gastos(String otros_gastos) {
        this.otros_gastos = otros_gastos;
    }

    public String getTotal_egresos() {
        return total_egresos;
    }

    public void setTotal_egresos(String total_egresos) {
        this.total_egresos = total_egresos;
    }

    public String getActivos() {
        return activos;
    }

    public void setActivos(String activos) {
        this.activos = activos;
    }

    public String getPasivos() {
        return pasivos;
    }

    public void setPasivos(String pasivos) {
        this.pasivos = pasivos;
    }

    public String getTotal_patrimonio() {
        return total_patrimonio;
    }

    public void setTotal_patrimonio(String total_patrimonio) {
        this.total_patrimonio = total_patrimonio;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }
}
