package com.tsp.operation.model.beans;

public class Serie {

    private String prefix;
    private String last_number;
    private String document_type;
    private String last_update;
    public String getPrefix() {
        return prefix;
    }

    public String getLast_number() {
        return last_number;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setLast_update( String last_update ) {
        this.last_update = last_update;
    }

    public void setPrefix( String prefix ) {
        this.prefix = prefix;
    }

    public void setLast_number( String last_number ) {
        this.last_number = last_number;
    }

    public void setDocument_type( String document_type ) {
        this.document_type = document_type;
    }

    public String getLast_update() {
        return last_update;
    }

}
