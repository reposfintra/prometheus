/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class TramaJsonAnticipo {

    private ArrayList<BeansPropietario> propietarios;    
    private ArrayList<BeansConductor>  conductores;   
    private ArrayList<String>  intermediarios;   
    private ArrayList<BeansVehiculo>  vehiculos;    
    private ArrayList<BeansAnticipo>  anticipos;

    /**
     * @return the propietario
     */
    public ArrayList<BeansPropietario> getPropietario() {
        return propietarios;
    }

    /**
     * @param propietario the propietario to set
     */
    public void setPropietario(ArrayList<BeansPropietario> propietario) {
        this.propietarios = propietario;
    }

    /**
     * @return the conductor
     */
    public ArrayList<BeansConductor> getConductor() {
        return conductores;
    }

    /**
     * @param conductor the conductor to set
     */
    public void setConductor(ArrayList<BeansConductor> conductor) {
        this.conductores = conductor;
    }
    
    /**
     * @return the intermediarios
     */
    public ArrayList<String> getIntermediarios() {
        return intermediarios;
    }

    /**
     * @param intermediarios the intermediarios to set
     */
    public void setIntermediarios(ArrayList<String> intermediarios) {
        this.intermediarios = intermediarios;
    }

    /**
     * @return the vehiculo
     */
    public ArrayList<BeansVehiculo> getVehiculo() {
        return vehiculos;
    }

    /**
     * @param vehiculo the vehiculo to set
     */
    public void setVehiculo(ArrayList<BeansVehiculo> vehiculo) {
        this.vehiculos = vehiculo;
    }

    /**
     * @return the anticipo
     */
    public ArrayList<BeansAnticipo> getAnticipo() {
        return anticipos;
    }

    /**
     * @param anticipo the anticipo to set
     */
    public void setAnticipo(ArrayList<BeansAnticipo> anticipo) {
        this.anticipos = anticipo;
    }

  
}
