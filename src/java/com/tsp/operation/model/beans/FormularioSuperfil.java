package com.tsp.operation.model.beans;

/**
 * Bean para manejar los parametros del formulario de datacredito
 * 29/11/2011
 * @author darrieta
 */
public class FormularioSuperfil {
    
    private String tipoIdentificacion;
    private String identificacion;
    private String primerApellido;
    private String fechaNacimiento;
    private String tipoProducto;
    private String servicio;
    private String ciudadMatricula;
    private String tipoVivienda;
    private String ingreso;
    private String vlrCuotaMensual;
    private String montoSolicitado;
    private String vlrVehiculo;
    private String vlrSemestre;
    private String plazo;
    private String plazoUniversitario;
    private String haTenidoCreditoFintra;
    
    
    private String arriendo;
    private String estadoCivil;
    private String conyugueTrabaja;
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    public String getArriendo() {
        return arriendo;
    }

    public void setArriendo(String arriendo) {
        this.arriendo = arriendo;
    }

    public String getConyugueTrabaja() {
        return conyugueTrabaja;
    }

    public void setConyugueTrabaja(String conyugueTrabaja) {
        this.conyugueTrabaja = conyugueTrabaja;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    /**
     * Get the value of haTenidoCreditoFintra
     *
     * @return the value of haTenidoCreditoFintra
     */
    public String getHaTenidoCreditoFintra() {
        return haTenidoCreditoFintra;
    }

    /**
     * Set the value of haTenidoCreditoFintra
     *
     * @param haTenidoCreditoFintra new value of haTenidoCreditoFintra
     */
    public void setHaTenidoCreditoFintra(String haTenidoCreditoFintra) {
        this.haTenidoCreditoFintra = haTenidoCreditoFintra;
    }



    /**
     * Get the value of plazoUniversitario
     *
     * @return the value of plazoUniversitario
     */
    public String getPlazoUniversitario() {
        return plazoUniversitario;
    }

    /**
     * Set the value of plazoUniversitario
     *
     * @param plazoUniversitario new value of plazoUniversitario
     */
    public void setPlazoUniversitario(String plazoUniversitario) {
        this.plazoUniversitario = plazoUniversitario;
    }

    /**
     * Get the value of plazo
     *
     * @return the value of plazo
     */
    public String getPlazo() {
        return plazo;
    }

    /**
     * Set the value of plazo
     *
     * @param plazo new value of plazo
     */
    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    /**
     * Get the value of vlrSemestre
     *
     * @return the value of vlrSemestre
     */
    public String getVlrSemestre() {
        return vlrSemestre;
    }

    /**
     * Set the value of vlrSemestre
     *
     * @param vlrSemestre new value of vlrSemestre
     */
    public void setVlrSemestre(String vlrSemestre) {
        this.vlrSemestre = vlrSemestre;
    }

    /**
     * Get the value of vlrVehiculo
     *
     * @return the value of vlrVehiculo
     */
    public String getVlrVehiculo() {
        return vlrVehiculo;
    }

    /**
     * Set the value of vlrVehiculo
     *
     * @param vlrVehiculo new value of vlrVehiculo
     */
    public void setVlrVehiculo(String vlrVehiculo) {
        this.vlrVehiculo = vlrVehiculo;
    }

    /**
     * Get the value of montoSolicitado
     *
     * @return the value of montoSolicitado
     */
    public String getMontoSolicitado() {
        return montoSolicitado;
    }

    /**
     * Set the value of montoSolicitado
     *
     * @param montoSolicitado new value of montoSolicitado
     */
    public void setMontoSolicitado(String montoSolicitado) {
        this.montoSolicitado = montoSolicitado;
    }

    /**
     * Get the value of vlrCuotaMensual
     *
     * @return the value of vlrCuotaMensual
     */
    public String getVlrCuotaMensual() {
        return vlrCuotaMensual;
    }

    /**
     * Set the value of vlrCuotaMensual
     *
     * @param vlrCuotaMensual new value of vlrCuotaMensual
     */
    public void setVlrCuotaMensual(String vlrCuotaMensual) {
        this.vlrCuotaMensual = vlrCuotaMensual;
    }


    /**
     * Get the value of ingreso
     *
     * @return the value of ingreso
     */
    public String getIngreso() {
        return ingreso;
    }

    /**
     * Set the value of ingreso
     *
     * @param ingreso new value of ingreso
     */
    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    /**
     * Get the value of tipoVivienda
     *
     * @return the value of tipoVivienda
     */
    public String getTipoVivienda() {
        return tipoVivienda;
    }

    /**
     * Set the value of tipoVivienda
     *
     * @param tipoVivienda new value of tipoVivienda
     */
    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }


    /**
     * Get the value of ciudadMatricula
     *
     * @return the value of ciudadMatricula
     */
    public String getCiudadMatricula() {
        return ciudadMatricula;
    }

    /**
     * Set the value of ciudadMatricula
     *
     * @param ciudadMatricula new value of ciudadMatricula
     */
    public void setCiudadMatricula(String ciudadMatricula) {
        this.ciudadMatricula = ciudadMatricula;
    }


    /**
     * Get the value of servicio
     *
     * @return the value of servicio
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * Set the value of servicio
     *
     * @param servicio new value of servicio
     */
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    /**
     * Get the value of tipoProducto
     *
     * @return the value of tipoProducto
     */
    public String getTipoProducto() {
        return tipoProducto;
    }

    /**
     * Set the value of tipoProducto
     *
     * @param tipoProducto new value of tipoProducto
     */
    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }


    /**
     * Get the value of fechaNacimiento
     *
     * @return the value of fechaNacimiento
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Set the value of fechaNacimiento
     *
     * @param fechaNacimiento new value of fechaNacimiento
     */
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }


    /**
     * Get the value of primerApellido
     *
     * @return the value of primerApellido
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * Set the value of primerApellido
     *
     * @param primerApellido new value of primerApellido
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }
}
