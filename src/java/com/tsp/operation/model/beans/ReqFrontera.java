/*
 * ReqFrontera.java
 *
 * Created on 15 de junio de 2005, 09:50 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  DIOGENES
 */


public class ReqFrontera {
    private String dstrct_code;
    private String std_job_no;    
    private String numpla;
    private String oripla;
    private String despla;
    private String fecpla;
    private String numrem;
    private String orirem;
    private String desrem;
    private String cliente;
    private String fec_dispo;
    private String Tipo_recurso1;
    private String Tipo_recurso2;
    private String Tipo_recurso3;
    private String Tipo_recurso4;
    private String Tipo_recurso5;
    private String recurso1;
    private String recurso2;
    private String recurso3;
    private String recurso4;
    private String recurso5;
    private String prioridad1;
    private String prioridad2;
    private String prioridad3;
    private String prioridad4;
    private String prioridad5;
    private String error;
    //Diogenes 16.11.05
    private String frontera_asoc;
    private String id_rec_tra;
    private String equipo_aso;
    private String stdjob_esp;
    
    public void setdstrct_code (String dstrct_code){
        this.dstrct_code=dstrct_code;
    }
     public void setstd_job_no (String std_job_no){
        this.std_job_no=std_job_no;
    }
    public void setnumpla (String numpla){
        this.numpla=numpla;
    }
    public void setoripla (String oripla){
        this.oripla=oripla;
    }
    public void setdespla (String despla){
        this.despla=despla;
    }
    public void setfecpla (String fecpla){
        this.fecpla=fecpla;
    }
    public void setnumrem (String numrem){
        this.numrem=numrem;
    }
    public void setorirem (String orirem){
        this.orirem=orirem;
    }
    public void setdesrem (String desrem){
        this.desrem=desrem;
    }
    public void setcliente (String cliente){
        this.cliente=cliente;
    }
    public void setfecdispo (String fec_dispo){
        this.fec_dispo=fec_dispo;
    }
    public void setTipo_recurso1 (String Tipo_recurso1){
        this.Tipo_recurso1=Tipo_recurso1;
    }
    public void setTipo_recurso2 (String Tipo_recurso2){
        this.Tipo_recurso2=Tipo_recurso2;
    }
    public void setTipo_recurso3 (String Tipo_recurso3){
        this.Tipo_recurso3=Tipo_recurso3;
    }
    public void setTipo_recurso4 (String Tipo_recurso4){
        this.Tipo_recurso4=Tipo_recurso4;
    }
    public void setTipo_recurso5 (String Tipo_recurso5){
        this.Tipo_recurso5=Tipo_recurso5;
    }
    public void setrecurso1 (String recurso1){
        this.recurso1=recurso1;
    }
    public void setrecurso2 (String recurso2){
        this.recurso2=recurso2;
    }
    public void setrecurso3 (String recurso3){
        this.recurso3=recurso3;
    }
    public void setrecurso4 (String recurso4){
        this.recurso4=recurso4;
    }
    public void setrecurso5 (String recurso5){
        this.recurso5=recurso5;
    }
    public void setprioridad1 (String prioridad1){
        this.prioridad1=prioridad1;
    }
    public void setprioridad2 (String prioridad2){
        this.prioridad2=prioridad2;
    }
    public void setprioridad3 (String prioridad3){
        this.prioridad3=prioridad3;
    }
    public void setprioridad4 (String prioridad4){
        this.prioridad4=prioridad4;
    }
    public void setprioridad5 (String prioridad5){
        this.prioridad5=prioridad5;
    }
    public void setError (String error){
        this.error=error;
    }
    //***************************************************************
    
    public String getdstrct_code ( ){
        return dstrct_code;
    }
    public String getstd_job_no (){
        return std_job_no;
    }
    public String getnumpla (){
        return numpla;
    }
    public String getoripla (){
        return oripla;
    }
    public String getdespla (){
        return despla;
    }
    public String getfecpla (){
        return fecpla;
    }
    public String getnumrem (){
        return numrem;
    }
    public String getorirem (){
        return orirem;
    }
    public String getdesrem (){
        return desrem;
    }
    public String getcliente (){
        return cliente;
    }
    public String getfecdispo (){
        return fec_dispo;
    }    
    public String getTipo_recurso1 (){
        return Tipo_recurso1;
    }
    public String getTipo_recurso2 (){
        return Tipo_recurso2;
    }
    public String getTipo_recurso3 (){
        return Tipo_recurso3;
    }
    public String getTipo_recurso4 (){
        return Tipo_recurso4;
    }
    public String getTipo_recurso5 (){
        return Tipo_recurso5;
    }
    public String getrecurso1 (){
        return recurso1;
    }
    public String getrecurso2 (){
        return recurso2;
    }
    public String getrecurso3 (){
        return recurso3;
    }
    public String getrecurso4 (){
        return recurso4;
    }
    public String getrecurso5 (){
        return recurso5;
    }
    public String getprioridad1 (){
        return prioridad1;
    }
    public String getprioridad2 (){
        return prioridad2;
    }
    public String getprioridad3 (){
        return prioridad3;
    }
    public String getprioridad4 (){
        return prioridad4;
    }
    public String getprioridad5 (){
        return prioridad5;
    }
    public String getError (){
        return error;
    }
     /**
     * Getter for property frontera_asoc.
     * @return Value of property frontera_asoc.
     */
    public java.lang.String getFrontera_asoc() {
        return frontera_asoc;
    }
    
    /**
     * Setter for property frontera_asoc.
     * @param frontera_asoc New value of property frontera_asoc.
     */
    public void setFrontera_asoc(java.lang.String frontera_asoc) {
        this.frontera_asoc = frontera_asoc;
    }
    
    /**
     * Getter for property id_rec_tra.
     * @return Value of property id_rec_tra.
     */
    public java.lang.String getId_rec_tra() {
        return id_rec_tra;
    }
    
    /**
     * Setter for property id_rec_tra.
     * @param id_rec_tra New value of property id_rec_tra.
     */
    public void setId_rec_tra(java.lang.String id_rec_tra) {
        this.id_rec_tra = id_rec_tra;
    }
    
    /**
     * Getter for property equipo_aso.
     * @return Value of property equipo_aso.
     */
    public java.lang.String getEquipo_aso() {
        return equipo_aso;
    }
    
    /**
     * Setter for property equipo_aso.
     * @param equipo_aso New value of property equipo_aso.
     */
    public void setEquipo_aso(java.lang.String equipo_aso) {
        this.equipo_aso = equipo_aso;
    }
    
    /**
     * Getter for property stdjob_esp.
     * @return Value of property stdjob_esp.
     */
    public java.lang.String getStdjob_esp() {
        return stdjob_esp;
    }
    
    /**
     * Setter for property stdjob_esp.
     * @param stdjob_esp New value of property stdjob_esp.
     */
    public void setStdjob_esp(java.lang.String stdjob_esp) {
        this.stdjob_esp = stdjob_esp;
    }
    
}
