/******************************************************************
* Nombre ......................TblGeneralDato.java
* Descripci�n..................Clase bean para tabla general dato
* Autor........................Armando Oviedo
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*;
/**  
 * @author  Armando Oviedo
 */
public class TblGeneralDato {
    
    String regStatus;
    String dstrct;
    String lastUpdate;
    String userUpdate;
    String creationDate;
    String creationUser;
    String base;
    String codtabla;
    String secuencia;
    String leyenda;
    String tipo;
    String longitud;
    
    
    /** Creates a new instance of TblGeneralDato */
    public TblGeneralDato() {
    }
    
    /**
     * M�todo que retorna un objeto TblGeneralDato cargado previamente de un resultset
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......ResultSet rs
     * @return.......objeto TblGeneralDato
     **/ 
    public TblGeneralDato loadResultSet(ResultSet rs) throws SQLException{
        TblGeneralDato tgd = new TblGeneralDato();
        tgd.setBase(rs.getString("base"));
        tgd.setCodTabla(rs.getString("codtabla"));        
        tgd.setSecuencia(rs.getString("secuencia"));
        tgd.setLeyenda(rs.getString("leyenda"));
        tgd.setTipo(rs.getString("tipo"));
        tgd.setLongitud(rs.getString("longitud"));
        tgd.setCreationDate(rs.getString("creation_date"));
        tgd.setCreationUser(rs.getString("creation_user"));        
        tgd.setDstrct(rs.getString("dstrct"));
        tgd.setLastUpdate(rs.getString("last_update"));
        tgd.setRegStatus(rs.getString("reg_status"));
        tgd.setUserUpdate(rs.getString("user_update"));
        return tgd;
    }
    public void setRegStatus(String regStatus){
        this.regStatus = regStatus;
    }
    public void setDstrct(String dstrct){
        this.dstrct = dstrct;
    }
    public void setLastUpdate(String lastUpdate){
        this.lastUpdate = lastUpdate;
    }
    public void setUserUpdate(String userUpdate){
        this.userUpdate = userUpdate;
    }
    public void setCreationDate(String creationDate){
        this.creationDate = creationDate;
    }
    public void setCreationUser(String creationUser){
        this.creationUser = creationUser;
    }
    public void setBase(String base){
        this.base = base;
    }
    public void setCodTabla(String codtabla){
        this.codtabla = codtabla;
    }
    public void setSecuencia(String secuencia){
        this.secuencia = secuencia;
    }
    public void setLeyenda(String leyenda){
        this.leyenda = leyenda;
    }
    public void setTipo(String tipo){
        this.tipo = tipo;
    }    
    public void setLongitud(String longitud){
        this.longitud = longitud;
    }
    public String getRegStatus(){
        return this.regStatus;
    }
    public String getDstrct(){
        return this.dstrct;
    }
    public String getLastUpdate(){
        return this.lastUpdate;
    }
    public String getUserUpdate(){
        return this.userUpdate;
    }
    public String getCreationDate(){
        return this.creationDate;
    }
    public String getCreationUser(){
        return this.creationUser;
    }
    public String getBase(){
        return this.base;
    }
    public String getCodTabla(){
        return this.codtabla = codtabla;
    }
    public String getSecuencia(){
        return this.secuencia = secuencia;
    }
    public String getLeyenda(){
        return this.leyenda = leyenda;
    }
    public String getTipo(){
        return this.tipo = tipo;
    }    
    public String getLongitud(){
        return this.longitud = longitud;
    } 
}