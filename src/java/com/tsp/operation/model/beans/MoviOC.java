  /***************************************
    * Nombre Clase ............. MoviOC.java
    * Descripci�n  .. . . . . .  Obtiene datos movimiento de planillas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  05/12/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.beans;




public class MoviOC {
    
    
    private  String  concepto        = "";
    private  String  descripcion     = "";
    private  String  aplica          = "";
    private  String  indicador_valor = "";
    private  double  valor;
    private  double  valor_me;
    private  String  moneda          = "";
    private  String  proveedor       = "";
    private  String  nameProveedor   = "";
    private  String  fecha           = "";
    private  String  usuario         = "";
    
    
    private double   vlrReal;
    private String   factura;
    
    
    
    public MoviOC() {
    }
    
    /**
     * Getter for property aplica.
     * @return Value of property aplica.
     */
    public java.lang.String getAplica() {
        return aplica;
    }    
    
    /**
     * Setter for property aplica.
     * @param aplica New value of property aplica.
     */
    public void setAplica(java.lang.String aplica) {
        this.aplica = aplica;
    }    
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }    
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property indicador_valor.
     * @return Value of property indicador_valor.
     */
    public java.lang.String getIndicador_valor() {
        return indicador_valor;
    }
    
    /**
     * Setter for property indicador_valor.
     * @param indicador_valor New value of property indicador_valor.
     */
    public void setIndicador_valor(java.lang.String indicador_valor) {
        this.indicador_valor = indicador_valor;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property nameProveedor.
     * @return Value of property nameProveedor.
     */
    public java.lang.String getNameProveedor() {
        return nameProveedor;
    }
    
    /**
     * Setter for property nameProveedor.
     * @param nameProveedor New value of property nameProveedor.
     */
    public void setNameProveedor(java.lang.String nameProveedor) {
        this.nameProveedor = nameProveedor;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property valor_me.
     * @return Value of property valor_me.
     */
    public double getValor_me() {
        return valor_me;
    }
    
    /**
     * Setter for property valor_me.
     * @param valor_me New value of property valor_me.
     */
    public void setValor_me(double valor_me) {
        this.valor_me = valor_me;
    }
    
    /**
     * Getter for property vlrReal.
     * @return Value of property vlrReal.
     */
    public double getVlrReal() {
        return vlrReal;
    }
    
    /**
     * Setter for property vlrReal.
     * @param vlrReal New value of property vlrReal.
     */
    public void setVlrReal(double vlrReal) {
        this.vlrReal = vlrReal;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }    
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
}
