 /********************************************************************
 *      Nombre Clase.................   RepGral.java
 *      Descripci�n..................
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   20 de septiembre de 2006, 04:04 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*;

import com.tsp.util.Util;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class RepGral {
    
    private String numpla;
    private String oripla;
    private String despla;
    private String nitpro;
    private String agcpla;
    private String creation_date;
    private String creation_user;
    private String existe_nit;
    private String existe_prov;
    private String existe_cmc;
    private String fecpla;
    
    private String usuario;
    private String login;
    private String registros;
    private String oportunos;
    private String inoportunos;
    private String porcent_opor;
    private String porcent_inop;
    private String nitter;
    
      /* Reporte de Cartera */
    private String codcli;
    private String nomcli;
    private String agcfacturacion;    
    private String codagcfacturacion;
    private double no_vencida;
    private double vencida_1;
    private double vencida_2;
    private double vencida_3;
    private double vencida_4;
    private double vencida_5;
    private double vencida_6;
    private double vlr_saldo_me;
    private double vlr_saldo;
    private String moneda;
    private String fecha_fra;
    private String fecha_venc_fra;
    private String factura;
    private int ndias;
    private String vencimiento;
    
    
    //Pago Almacenadora
    private String oc;
    private String ot;
    private String placa;
    private String nomcli_fto;
    private String codcli_fto;
    private String codcli_ot;
    private String almacenadora;
    private double vlr_cargue;
    private double vlr_descargue;
    private double vlr_bodegaje;
    private double vlr_inspeccion;
    private String tipo_costo_1;
    private String tipo_costo_2;
    private String tipo_costo_3;
    private String tipo_costo_4;
    private String codigo_agencia_1;
    private String codigo_agencia_2;
    private String codigo_agencia_3;
    private String codigo_agencia_4;
    private String nom_costo_1;
    private String nom_costo_2;
    private String nom_costo_3;
    private String nom_costo_4;    
    private String agc_cobro;
    private String agc_duenia;
    
    /* Anulacion Cheque CxP */
    private String dstrct;
    private String proveedor;
    private String tipo_documento;
    private String documento;
    private String fra_moneda;
    private String cheq_moneda;
    private double cheq_vlr;
    private double cheq_vlr_for;
    
    //Pago almacenadora
    private int fila;
    
    private double valor_factura;
    private double valor_facturame;
    private String fecha_corte;
    
    
     /* Reporte RxP (Por Concepto) */
    private String no_recurrente;
    private String fecha_rxp;
    private String descripcion_rxp;
    private String proveedor_rxp;
    private String nom_proveedor_rxp;
    private double vlr_rxp;
    private int no_cuotas_rxp;
    private String fecha_ini_dscto;
    private double vlr_prox_cuota;
    private String fecha_prox_cuota;
    private String fecha_fin_dcto;
    private String no_item;
    private String descripcion_rxp_item;
    private String concepto_code;
    private String concepto_desc;
    private String cod_cuenta;
    private int no_cuotas_transfer;

    /*Amaturana 30-04-07*/
    private double vlr_transfer_ml;
    private double saldo;
    
    private String docu;
    private String numaval;
    private String codbco;
    private String refcl;
    private String cmc;

    private String negocio;
    private String fechaFacturacion;

    
    private String tipo_ref1,ref1,tipo_ref2,ref2;
    private int no_solicitud;
    private int ciclo;
    private String numero_pagare;
    private String negocio_rel;
    
    /*nuevos campos reporte cartera*/
    private String gestion;
    private String proxima_accion;
    private String fecha_proxima_accion;
    private String estado_cliente;
    private String direccion;
    private String barrio;
    private String ciudad;
    private String cedula_estudiante;
    private String nombre_estudiante;
    private String telefono_estudiante;
    private String direccion_estudiante;
    private String cedula_codeudor;
    private String nombre_codeudor;
    private String telefono_codeudor;
    private String direccion_codeudor;
    private String asesor;
    private String fecha_ultimo_pago;
    private double vlr_ultimo_pago;
    private String rango;
    private String id_convenio;
    private double vlr_cuota_manejo;
    private double valor_interes;
    private double valor_cat;
    private double capital;
    private double seguro;
    private String altura_mora;
    private String estado_indemnizar;
    private double capital_aval;  
    private double interes_aval;
    private double valor_aval;
    private String plazo;
    private double tasa;
    private String plan_al_Dia;
    private String agencia;

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }
    
    public String getPlazo() {
        return plazo;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public double getTasa() {
        return tasa;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    public String getPlan_al_Dia() {
        return plan_al_Dia;
    }

    public void setPlan_al_Dia(String plan_al_Dia) {
        this.plan_al_Dia = plan_al_Dia;
    }
    
    
    
      public double getCapital_aval() {
        return capital_aval;
    }

    public void setCapital_aval(double capital_aval) {
        this.capital_aval = capital_aval;
    }

    public double getInteres_aval() {
        return interes_aval;
    }

    public void setInteres_aval(double interes_aval) {
        this.interes_aval = interes_aval;
    }

    public double getValor_aval() {
        return valor_aval;
    }

    public void setValor_aval(double valor_aval) {
        this.valor_aval = valor_aval;
    }

    public String getEstado_indemnizar() {
        return estado_indemnizar;
    }

    public void setEstado_indemnizar(String estado_indemnizar) {
        this.estado_indemnizar = estado_indemnizar;
    }

    public String getAltura_mora() {
        return altura_mora;
    }

    public void setAltura_mora(String altura_mora) {
        this.altura_mora = altura_mora;
    }
    
    
     
    public String getNegocio_rel() {
        return negocio_rel;
    }

    public void setNegocio_rel(String negocio_rel) {
        this.negocio_rel = negocio_rel;
    }

    /**
     * Get the value of ciclo
     *
     * @return the value of ciclo
     */
    public int getCiclo() {
        return ciclo;
    }

    /**
     * Set the value of ciclo
     *
     * @param ciclo new value of ciclo
     */
    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    /**
     * Get the value of no_solicitud
     *
     * @return the value of no_solicitud
     */
    public int getNo_solicitud() {
        return no_solicitud;
    }

    /**
     * Set the value of no_solicitud
     *
     * @param no_solicitud new value of no_solicitud
     */
    public void setNo_solicitud(int no_solicitud) {
        this.no_solicitud = no_solicitud;
    }


/**
     * Getter for property login.
     * @return Value of property login.
     */
    public java.lang.String getLogin() {
        return login;
    }
    
    /**
     * Setter for property login.
     * @param login New value of property login.
     */
    public void setLogin(java.lang.String login) {
        this.login = login;
    }
    
    /**
     * Getter for property porcent_inop.
     * @return Value of property porcent_inop.
     */
    public java.lang.String getPorcent_inop() {
        return porcent_inop;
    }
    
    /**
     * Setter for property porcent_inop.
     * @param porcent_inop New value of property porcent_inop.
     */
    public void setPorcent_inop(java.lang.String porcent_inop) {
        this.porcent_inop = porcent_inop;
    }
    
    /**
     * Getter for property porcent_opor.
     * @return Value of property porcent_opor.
     */
    public java.lang.String getPorcent_opor() {
        return porcent_opor;
    }
    
    /**
     * Setter for property porcent_opor.
     * @param porcent_opor New value of property porcent_opor.
     */
    public void setPorcent_opor(java.lang.String porcent_opor) {
        this.porcent_opor = porcent_opor;
    }
    
    /**
     * Getter for property registros.
     * @return Value of property registros.
     */
    public java.lang.String getRegistros() {
        return registros;
    }
    
    /**
     * Setter for property registros.
     * @param registros New value of property registros.
     */
    public void setRegistros(java.lang.String registros) {
        this.registros = registros;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property inoportunos.
     * @return Value of property inoportunos.
     */
    public java.lang.String getInoportunos() {
        return inoportunos;
    }
    
    /**
     * Setter for property inoportunos.
     * @param inoportunos New value of property inoportunos.
     */
    public void setInoportunos(java.lang.String inoportunos) {
        this.inoportunos = inoportunos;
    }
    
    /**
     * Getter for property oportunos.
     * @return Value of property oportunos.
     */
    public java.lang.String getOportunos() {
        return oportunos;
    }
    
    /**
     * Setter for property oportunos.
     * @param oportunos New value of property oportunos.
     */
    public void setOportunos(java.lang.String oportunos) {
        this.oportunos = oportunos;
    }
    
   
    
    /** Crea una nueva instancia de  RepGral */
    public RepGral() {
    }
    
    /**
     * Obtiene los campos de la consulta llamada
     * @autor Ing. Andr�s Maturana D.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void LoadRepVerificacionNitpro(ResultSet rs) throws SQLException{
        this.setAgcpla(rs.getString("agcpla"));
        this.setCreation_date(rs.getString("creation_date"));
        this.setCreation_user(rs.getString("creation_user"));
        this.setDespla(rs.getString("despla"));
        this.setExiste_cmc(rs.getString("existe_cmc"));
        this.setExiste_nit(rs.getString("existe_nit"));
        this.setExiste_prov(rs.getString("existe_prov"));
        this.setNitpro(rs.getString("nitpro"));
        this.setNumpla(rs.getString("numpla"));
        this.setOripla(rs.getString("oripla"));
        this.setFecpla(rs.getString("fecpla"));
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property oripla.
     * @return Value of property oripla.
     */
    public java.lang.String getOripla() {
        return oripla;
    }
    
    /**
     * Setter for property oripla.
     * @param oripla New value of property oripla.
     */
    public void setOripla(java.lang.String oripla) {
        this.oripla = oripla;
    }
    
    /**
     * Getter for property despla.
     * @return Value of property despla.
     */
    public java.lang.String getDespla() {
        return despla;
    }
    
    /**
     * Setter for property despla.
     * @param despla New value of property despla.
     */
    public void setDespla(java.lang.String despla) {
        this.despla = despla;
    }
    
    /**
     * Getter for property nitpro.
     * @return Value of property nitpro.
     */
    public java.lang.String getNitpro() {
        return nitpro;
    }
    
    /**
     * Setter for property nitpro.
     * @param nitpro New value of property nitpro.
     */
    public void setNitpro(java.lang.String nitpro) {
        this.nitpro = nitpro;
    }
    
    /**
     * Getter for property agcpla.
     * @return Value of property agcpla.
     */
    public java.lang.String getAgcpla() {
        return agcpla;
    }
    
    /**
     * Setter for property agcpla.
     * @param agcpla New value of property agcpla.
     */
    public void setAgcpla(java.lang.String agcpla) {
        this.agcpla = agcpla;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property existe_nit.
     * @return Value of property existe_nit.
     */
    public java.lang.String getExiste_nit() {
        return existe_nit;
    }
    
    /**
     * Setter for property existe_nit.
     * @param existe_nit New value of property existe_nit.
     */
    public void setExiste_nit(java.lang.String existe_nit) {
        this.existe_nit = existe_nit;
    }
    
    /**
     * Getter for property existe_prov.
     * @return Value of property existe_prov.
     */
    public java.lang.String getExiste_prov() {
        return existe_prov;
    }
    
    /**
     * Setter for property existe_prov.
     * @param existe_prov New value of property existe_prov.
     */
    public void setExiste_prov(java.lang.String existe_prov) {
        this.existe_prov = existe_prov;
    }
    
    /**
     * Getter for property existe_cmc.
     * @return Value of property existe_cmc.
     */
    public java.lang.String getExiste_cmc() {
        return existe_cmc;
    }
    
    /**
     * Setter for property existe_cmc.
     * @param existe_cmc New value of property existe_cmc.
     */
    public void setExiste_cmc(java.lang.String existe_cmc) {
        this.existe_cmc = existe_cmc;
    }
    
    /**
     * Getter for property fecpla.
     * @return Value of property fecpla.
     */
    public java.lang.String getFecpla() {
        return fecpla;
    }
    
    /**
     * Setter for property fecpla.
     * @param fecpla New value of property fecpla.
     */
    public void setFecpla(java.lang.String fecpla) {
        this.fecpla = fecpla;
    }
    
     
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }

    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property vlr_saldo_me.
     * @return Value of property vlr_saldo_me.
     */
    public double getVlr_saldo_me() {
        return vlr_saldo_me;
    }
    
    /**
     * Setter for property vlr_saldo_me.
     * @param vlr_saldo_me New value of property vlr_saldo_me.
     */
    public void setVlr_saldo_me(double vlr_saldo_me) {
        this.vlr_saldo_me = vlr_saldo_me;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property fecha_fra.
     * @return Value of property fecha_fra.
     */
    public java.lang.String getFecha_fra() {
        return fecha_fra;
    }
    
    /**
     * Setter for property fecha_fra.
     * @param fecha_fra New value of property fecha_fra.
     */
    public void setFecha_fra(java.lang.String fecha_fra) {
        this.fecha_fra = fecha_fra;
    }
    
    /**
     * Getter for property fecha_venc_fra.
     * @return Value of property fecha_venc_fra.
     */
    public java.lang.String getFecha_venc_fra() {
        return fecha_venc_fra;
    }
    
    /**
     * Setter for property fecha_venc_fra.
     * @param fecha_venc_fra New value of property fecha_venc_fra.
     */
    public void setFecha_venc_fra(java.lang.String fecha_venc_fra) {
        this.fecha_venc_fra = fecha_venc_fra;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property ndias.
     * @return Value of property ndias.
     */
     public int getNdias() {
        return ndias;
    }
    
    /**
     * Setter for property ndias.
     * @param ndias New value of property ndias.
     */
    public void setNdias(int ndias) {
        this.ndias = ndias;
    }
    
    /**
     * Getter for property codagcfacturacion.
     * @return Value of property codagcfacturacion.
     */
    public java.lang.String getCodagcfacturacion() {
        return codagcfacturacion;
    }
    
    /**
     * Setter for property codagcfacturacion.
     * @param codagcfacturacion New value of property codagcfacturacion.
     */
    public void setCodagcfacturacion(java.lang.String codagcfacturacion) {
        this.codagcfacturacion = codagcfacturacion;
    }
    
    /**
     * Getter for property vencida_6.
     * @return Value of property vencida_6.
     */
    public double getVencida_6() {
        return vencida_6;
    }
    
    /**
     * Setter for property vencida_6.
     * @param vencida_6 New value of property vencida_6.
     */
    public void setVencida_6(double vencida_6) {
        this.vencida_6 = vencida_6;
    }
    
    /**
     * Getter for property vencida_5.
     * @return Value of property vencida_5.
     */
    public double getVencida_5() {
        return vencida_5;
    }
    
    /**
     * Setter for property vencida_5.
     * @param vencida_5 New value of property vencida_5.
     */
    public void setVencida_5(double vencida_5) {
        this.vencida_5 = vencida_5;
    }
    
    /**
     * Getter for property vencida_4.
     * @return Value of property vencida_4.
     */
    public double getVencida_4() {
        return vencida_4;
    }
    
    /**
     * Setter for property vencida_4.
     * @param vencida_4 New value of property vencida_4.
     */
    public void setVencida_4(double vencida_4) {
        this.vencida_4 = vencida_4;
    }
    
    /**
     * Getter for property vencida_3.
     * @return Value of property vencida_3.
     */
    public double getVencida_3() {
        return vencida_3;
    }
    
    /**
     * Setter for property vencida_3.
     * @param vencida_3 New value of property vencida_3.
     */
    public void setVencida_3(double vencida_3) {
        this.vencida_3 = vencida_3;
    }
    
    /**
     * Getter for property vencida_2.
     * @return Value of property vencida_2.
     */
    public double getVencida_2() {
        return vencida_2;
    }
    
    /**
     * Setter for property vencida_2.
     * @param vencida_2 New value of property vencida_2.
     */
    public void setVencida_2(double vencida_2) {
        this.vencida_2 = vencida_2;
    }
    
    /**
     * Getter for property vencida_1.
     * @return Value of property vencida_1.
     */
    public double getVencida_1() {
        return vencida_1;
    }
    
    /**
     * Setter for property vencida_1.
     * @param vencida_1 New value of property vencida_1.
     */
    public void setVencida_1(double vencida_1) {
        this.vencida_1 = vencida_1;
    }
    
    /**
     * Getter for property no_vencida.
     * @return Value of property no_vencida.
     */
    public double getNo_vencida() {
        return no_vencida;
    }
    
    /**
     * Setter for property no_vencida.
     * @param no_vencida New value of property no_vencida.
     */
    public void setNo_vencida(double no_vencida) {
        this.no_vencida = no_vencida;
    }
    
    /**
     * Getter for property agcfacturacion.
     * @return Value of property agcfacturacion.
     */
    public java.lang.String getAgcfacturacion() {
        return agcfacturacion;
    }
    
    /**
     * Setter for property agcfacturacion.
     * @param agcfacturacion New value of property agcfacturacion.
     */
    public void setAgcfacturacion(java.lang.String agcfacturacion) {
        this.agcfacturacion = agcfacturacion;
    }
    
    /**
     * Getter for property nomcli.
     * @return Value of property nomcli.
     */
    public java.lang.String getNomcli() {
        return nomcli;
    }
    
    /**
     * Setter for property nomcli.
     * @param nomcli New value of property nomcli.
     */
    public void setNomcli(java.lang.String nomcli) {
        this.nomcli = nomcli;
    }
    
    /**
     * Getter for property oc.
     * @return Value of property oc.
     */
    public java.lang.String getOc() {
        return oc;
    }
    
    /**
     * Setter for property oc.
     * @param oc New value of property oc.
     */
    public void setOc(java.lang.String oc) {
        this.oc = oc;
    }
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt() {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt(java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property nomcli_fto.
     * @return Value of property nomcli_fto.
     */
    public java.lang.String getNomcli_fto() {
        return nomcli_fto;
    }
    
    /**
     * Setter for property nomcli_fto.
     * @param nomcli_fto New value of property nomcli_fto.
     */
    public void setNomcli_fto(java.lang.String nomcli_fto) {
        this.nomcli_fto = nomcli_fto;
    }
    
    /**
     * Getter for property codcli_fto.
     * @return Value of property codcli_fto.
     */
    public java.lang.String getCodcli_fto() {
        return codcli_fto;
    }
    
    /**
     * Setter for property codcli_fto.
     * @param codcli_fto New value of property codcli_fto.
     */
    public void setCodcli_fto(java.lang.String codcli_fto) {
        this.codcli_fto = codcli_fto;
    }
    
    /**
     * Getter for property codcli_ot.
     * @return Value of property codcli_ot.
     */
    public java.lang.String getCodcli_ot() {
        return codcli_ot;
    }
    
    /**
     * Setter for property codcli_ot.
     * @param codcli_ot New value of property codcli_ot.
     */
    public void setCodcli_ot(java.lang.String codcli_ot) {
        this.codcli_ot = codcli_ot;
    }
    
    /**
     * Getter for property almacenadora.
     * @return Value of property almacenadora.
     */
    public java.lang.String getAlmacenadora() {
        return almacenadora;
    }
    
    /**
     * Setter for property almacenadora.
     * @param almacenadora New value of property almacenadora.
     */
    public void setAlmacenadora(java.lang.String almacenadora) {
        this.almacenadora = almacenadora;
    }
    
    /**
     * Getter for property vlr_cargue.
     * @return Value of property vlr_cargue.
     */
    public double getVlr_cargue() {
        return vlr_cargue;
    }
    
    /**
     * Setter for property vlr_cargue.
     * @param vlr_cargue New value of property vlr_cargue.
     */
    public void setVlr_cargue(double vlr_cargue) {
        this.vlr_cargue = vlr_cargue;
    }
    
    /**
     * Getter for property vlr_descargue.
     * @return Value of property vlr_descargue.
     */
    public double getVlr_descargue() {
        return vlr_descargue;
    }
    
    /**
     * Setter for property vlr_descargue.
     * @param vlr_descargue New value of property vlr_descargue.
     */
    public void setVlr_descargue(double vlr_descargue) {
        this.vlr_descargue = vlr_descargue;
    }
    
    /**
     * Getter for property vlr_bodegaje.
     * @return Value of property vlr_bodegaje.
     */
    public double getVlr_bodegaje() {
        return vlr_bodegaje;
    }
    
    /**
     * Setter for property vlr_bodegaje.
     * @param vlr_bodegaje New value of property vlr_bodegaje.
     */
    public void setVlr_bodegaje(double vlr_bodegaje) {
        this.vlr_bodegaje = vlr_bodegaje;
    }
    
    /**
     * Getter for property vlr_inspeccion.
     * @return Value of property vlr_inspeccion.
     */
    public double getVlr_inspeccion() {
        return vlr_inspeccion;
    }
    
    /**
     * Setter for property vlr_inspeccion.
     * @param vlr_inspeccion New value of property vlr_inspeccion.
     */
    public void setVlr_inspeccion(double vlr_inspeccion) {
        this.vlr_inspeccion = vlr_inspeccion;
    }
    
    /**
     * Getter for property vlr_saldo.
     * @return Value of property vlr_saldo.
     */
    public double getVlr_saldo() {
        return vlr_saldo;
    }
    
    /**
     * Setter for property vlr_saldo.
     * @param vlr_saldo New value of property vlr_saldo.
     */
    public void setVlr_saldo(double vlr_saldo) {
        this.vlr_saldo = vlr_saldo;
    }
    
    /**
     * Getter for property tipo_costo_1.
     * @return Value of property tipo_costo_1.
     */
    public java.lang.String getTipo_costo_1() {
        return tipo_costo_1;
    }
    
    /**
     * Setter for property tipo_costo_1.
     * @param tipo_costo_1 New value of property tipo_costo_1.
     */
    public void setTipo_costo_1(java.lang.String tipo_costo_1) {
        this.tipo_costo_1 = tipo_costo_1;
    }
    
    /**
     * Getter for property tipo_costo_2.
     * @return Value of property tipo_costo_2.
     */
    public java.lang.String getTipo_costo_2() {
        return tipo_costo_2;
    }
    
    /**
     * Setter for property tipo_costo_2.
     * @param tipo_costo_2 New value of property tipo_costo_2.
     */
    public void setTipo_costo_2(java.lang.String tipo_costo_2) {
        this.tipo_costo_2 = tipo_costo_2;
    }
    
    /**
     * Getter for property tipo_costo_3.
     * @return Value of property tipo_costo_3.
     */
    public java.lang.String getTipo_costo_3() {
        return tipo_costo_3;
    }
    
    /**
     * Setter for property tipo_costo_3.
     * @param tipo_costo_3 New value of property tipo_costo_3.
     */
    public void setTipo_costo_3(java.lang.String tipo_costo_3) {
        this.tipo_costo_3 = tipo_costo_3;
    }
    
    /**
     * Getter for property tipo_costo_4.
     * @return Value of property tipo_costo_4.
     */
    public java.lang.String getTipo_costo_4() {
        return tipo_costo_4;
    }
    
    /**
     * Setter for property tipo_costo_4.
     * @param tipo_costo_4 New value of property tipo_costo_4.
     */
    public void setTipo_costo_4(java.lang.String tipo_costo_4) {
        this.tipo_costo_4 = tipo_costo_4;
    }
    
    /**
     * Getter for property codigo_agencia_1.
     * @return Value of property codigo_agencia_1.
     */
    public java.lang.String getCodigo_agencia_1() {
        return codigo_agencia_1;
    }
    
    /**
     * Setter for property codigo_agencia_1.
     * @param codigo_agencia_1 New value of property codigo_agencia_1.
     */
    public void setCodigo_agencia_1(java.lang.String codigo_agencia_1) {
        this.codigo_agencia_1 = codigo_agencia_1;
    }
    
    /**
     * Getter for property codigo_agencia_2.
     * @return Value of property codigo_agencia_2.
     */
    public java.lang.String getCodigo_agencia_2() {
        return codigo_agencia_2;
    }
    
    /**
     * Setter for property codigo_agencia_2.
     * @param codigo_agencia_2 New value of property codigo_agencia_2.
     */
    public void setCodigo_agencia_2(java.lang.String codigo_agencia_2) {
        this.codigo_agencia_2 = codigo_agencia_2;
    }
    
    /**
     * Getter for property codigo_agencia_3.
     * @return Value of property codigo_agencia_3.
     */
    public java.lang.String getCodigo_agencia_3() {
        return codigo_agencia_3;
    }
    
    /**
     * Setter for property codigo_agencia_3.
     * @param codigo_agencia_3 New value of property codigo_agencia_3.
     */
    public void setCodigo_agencia_3(java.lang.String codigo_agencia_3) {
        this.codigo_agencia_3 = codigo_agencia_3;
    }
    
    /**
     * Getter for property codigo_agencia_4.
     * @return Value of property codigo_agencia_4.
     */
    public java.lang.String getCodigo_agencia_4() {
        return codigo_agencia_4;
    }
    
    /**
     * Setter for property codigo_agencia_4.
     * @param codigo_agencia_4 New value of property codigo_agencia_4.
     */
    public void setCodigo_agencia_4(java.lang.String codigo_agencia_4) {
        this.codigo_agencia_4 = codigo_agencia_4;
    }
    
    /**
     * Getter for property nom_costo_1.
     * @return Value of property nom_costo_1.
     */
    public java.lang.String getNom_costo_1() {
        return nom_costo_1;
    }
    
    /**
     * Setter for property nom_costo_1.
     * @param nom_costo_1 New value of property nom_costo_1.
     */
    public void setNom_costo_1(java.lang.String nom_costo_1) {
        this.nom_costo_1 = nom_costo_1;
    }
    
    /**
     * Getter for property nom_costo_2.
     * @return Value of property nom_costo_2.
     */
    public java.lang.String getNom_costo_2() {
        return nom_costo_2;
    }
    
    /**
     * Setter for property nom_costo_2.
     * @param nom_costo_2 New value of property nom_costo_2.
     */
    public void setNom_costo_2(java.lang.String nom_costo_2) {
        this.nom_costo_2 = nom_costo_2;
    }
    
    /**
     * Getter for property nom_costo_3.
     * @return Value of property nom_costo_3.
     */
    public java.lang.String getNom_costo_3() {
        return nom_costo_3;
    }
    
    /**
     * Setter for property nom_costo_3.
     * @param nom_costo_3 New value of property nom_costo_3.
     */
    public void setNom_costo_3(java.lang.String nom_costo_3) {
        this.nom_costo_3 = nom_costo_3;
    }
    
    /**
     * Getter for property nom_costo_4.
     * @return Value of property nom_costo_4.
     */
    public java.lang.String getNom_costo_4() {
        return nom_costo_4;
    }
    
    /**
     * Setter for property nom_costo_4.
     * @param nom_costo_4 New value of property nom_costo_4.
     */
    public void setNom_costo_4(java.lang.String nom_costo_4) {
        this.nom_costo_4 = nom_costo_4;
    }
    
    /**
     * Getter for property agc_cobro.
     * @return Value of property agc_cobro.
     */
    public java.lang.String getAgc_cobro() {
        return agc_cobro;
    }
    
    /**
     * Setter for property agc_cobro.
     * @param agc_cobro New value of property agc_cobro.
     */
    public void setAgc_cobro(java.lang.String agc_cobro) {
        this.agc_cobro = agc_cobro;
    }
    
    /**
     * Getter for property agc_duenia.
     * @return Value of property agc_duenia.
     */
    public java.lang.String getAgc_duenia() {
        return agc_duenia;
    }
    
    /**
     * Setter for property agc_duenia.
     * @param agc_duenia New value of property agc_duenia.
     */
    public void setAgc_duenia(java.lang.String agc_duenia) {
        this.agc_duenia = agc_duenia;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property fra_moneda.
     * @return Value of property fra_moneda.
     */
    public java.lang.String getFra_moneda() {
        return fra_moneda;
    }
    
    /**
     * Setter for property fra_moneda.
     * @param fra_moneda New value of property fra_moneda.
     */
    public void setFra_moneda(java.lang.String fra_moneda) {
        this.fra_moneda = fra_moneda;
    }
    
    /**
     * Getter for property cheq_moneda.
     * @return Value of property cheq_moneda.
     */
    public java.lang.String getCheq_moneda() {
        return cheq_moneda;
    }
    
    /**
     * Setter for property cheq_moneda.
     * @param cheq_moneda New value of property cheq_moneda.
     */
    public void setCheq_moneda(java.lang.String cheq_moneda) {
        this.cheq_moneda = cheq_moneda;
    }
    
    /**
     * Getter for property cheq_vlr.
     * @return Value of property cheq_vlr.
     */
    public double getCheq_vlr() {
        return cheq_vlr;
    }
    
    /**
     * Setter for property cheq_vlr.
     * @param cheq_vlr New value of property cheq_vlr.
     */
    public void setCheq_vlr(double cheq_vlr) {
        this.cheq_vlr = cheq_vlr;
    }
    
    /**
     * Getter for property cheq_vlr_for.
     * @return Value of property cheq_vlr_for.
     */
    public double getCheq_vlr_for() {
        return cheq_vlr_for;
    }
    
    /**
     * Setter for property cheq_vlr_for.
     * @param cheq_vlr_for New value of property cheq_vlr_for.
     */
    public void setCheq_vlr_for(double cheq_vlr_for) {
        this.cheq_vlr_for = cheq_vlr_for;
    }
    
    /*
     * Carga las fras relacionadas a un cheque de cxp
     * @autor Ing. Andr�s Maturana D.
     * @param rs <code>ResultSet</code>
     */
    public void loadChequeCxP( ResultSet rs ) throws SQLException{
        this.setDstrct(rs.getString("dstrct"));
        this.setProveedor(rs.getString("proveedor"));
        this.setTipo_documento(rs.getString("tipo_documento"));
        this.setDocumento(rs.getString("documento"));
        this.setFra_moneda(rs.getString("moneda"));
        this.setCheq_moneda(rs.getString("currency"));
        this.setCheq_vlr(rs.getDouble("vlr"));
        this.setCheq_vlr_for(rs.getDouble("vlr_for"));
        this.setFecha_fra(rs.getString("fecha_documento"));
    }
    
    /**
     * Getter for property vencimiento.
     * @return Value of property vencimiento.
     */
    public java.lang.String getVencimiento() {
        return vencimiento;
    }
    
    /**
     * Setter for property vencimiento.
     * @param vencimiento New value of property vencimiento.
     */
    public void setVencimiento(java.lang.String vencimiento) {
        this.vencimiento = vencimiento;
    }
    
    /**
     * Getter for property fila.
     * @return Value of property fila.
     */
    public int getFila() {
        return fila;
    }
    
    /**
     * Setter for property fila.
     * @param fila New value of property fila.
     */
    public void setFila(int fila) {
        this.fila = fila;
    }
    
    /**
     * Getter for property valor_factura.
     * @return Value of property valor_factura.
     */
    public double getValor_factura() {
        return valor_factura;
    }
    
    /**
     * Setter for property valor_factura.
     * @param valor_factura New value of property valor_factura.
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }
    
    /**
     * Getter for property valor_facturame.
     * @return Value of property valor_facturame.
     */
    public double getValor_facturame() {
        return valor_facturame;
    }
    
    /**
     * Setter for property valor_facturame.
     * @param valor_facturame New value of property valor_facturame.
     */
    public void setValor_facturame(double valor_facturame) {
        this.valor_facturame = valor_facturame;
    }
    
    /**
     * Getter for property fecha_corte.
     * @return Value of property fecha_corte.
     */
    public java.lang.String getFecha_corte() {
        return fecha_corte;
    }
    
    /**
     * Setter for property fecha_corte.
     * @param fecha_corte New value of property fecha_corte.
     */
    public void setFecha_corte(java.lang.String fecha_corte) {
        this.fecha_corte = fecha_corte;
    }
    
    /**
     * Getter for property cod_cuenta.
     * @return Value of property cod_cuenta.
     */
    public java.lang.String getCod_cuenta() {
        return cod_cuenta;
    }
    
    /**
     * Setter for property cod_cuenta.
     * @param cod_cuenta New value of property cod_cuenta.
     */
    public void setCod_cuenta(java.lang.String cod_cuenta) {
        this.cod_cuenta = cod_cuenta;
    }
    
    /**
     * Getter for property concepto_code.
     * @return Value of property concepto_code.
     */
    public java.lang.String getConcepto_code() {
        return concepto_code;
    }
    
    /**
     * Setter for property concepto_code.
     * @param concepto_code New value of property concepto_code.
     */
    public void setConcepto_code(java.lang.String concepto_code) {
        this.concepto_code = concepto_code;
    }
    
    /**
     * Getter for property concepto_desc.
     * @return Value of property concepto_desc.
     */
    public java.lang.String getConcepto_desc() {
        return concepto_desc;
    }
    
    /**
     * Setter for property concepto_desc.
     * @param concepto_desc New value of property concepto_desc.
     */
    public void setConcepto_desc(java.lang.String concepto_desc) {
        this.concepto_desc = concepto_desc;
    }
    
    /**
     * Getter for property descripcion_rxp.
     * @return Value of property descripcion_rxp.
     */
    public java.lang.String getDescripcion_rxp() {
        return descripcion_rxp;
    }
    
    /**
     * Setter for property descripcion_rxp.
     * @param descripcion_rxp New value of property descripcion_rxp.
     */
    public void setDescripcion_rxp(java.lang.String descripcion_rxp) {
        this.descripcion_rxp = descripcion_rxp;
    }
    
    /**
     * Getter for property descripcion_rxp_item.
     * @return Value of property descripcion_rxp_item.
     */
    public java.lang.String getDescripcion_rxp_item() {
        return descripcion_rxp_item;
    }
    
    /**
     * Setter for property descripcion_rxp_item.
     * @param descripcion_rxp_item New value of property descripcion_rxp_item.
     */
    public void setDescripcion_rxp_item(java.lang.String descripcion_rxp_item) {
        this.descripcion_rxp_item = descripcion_rxp_item;
    }
    
    /**
     * Getter for property fecha_fin_dcto.
     * @return Value of property fecha_fin_dcto.
     */
    public java.lang.String getFecha_fin_dcto() {
        return fecha_fin_dcto;
    }
    
    /**
     * Setter for property fecha_fin_dcto.
     * @param fecha_fin_dcto New value of property fecha_fin_dcto.
     */
    public void setFecha_fin_dcto(java.lang.String fecha_fin_dcto) {
        this.fecha_fin_dcto = fecha_fin_dcto;
    }
    
    /**
     * Getter for property fecha_ini_dscto.
     * @return Value of property fecha_ini_dscto.
     */
    public java.lang.String getFecha_ini_dscto() {
        return fecha_ini_dscto;
    }
    
    /**
     * Setter for property fecha_ini_dscto.
     * @param fecha_ini_dscto New value of property fecha_ini_dscto.
     */
    public void setFecha_ini_dscto(java.lang.String fecha_ini_dscto) {
        this.fecha_ini_dscto = fecha_ini_dscto;
    }
    
    /**
     * Getter for property fecha_prox_cuota.
     * @return Value of property fecha_prox_cuota.
     */
    public java.lang.String getFecha_prox_cuota() {
        return fecha_prox_cuota;
    }
    
    /**
     * Setter for property fecha_prox_cuota.
     * @param fecha_prox_cuota New value of property fecha_prox_cuota.
     */
    public void setFecha_prox_cuota(java.lang.String fecha_prox_cuota) {
        this.fecha_prox_cuota = fecha_prox_cuota;
    }
    
    /**
     * Getter for property fecha_rxp.
     * @return Value of property fecha_rxp.
     */
    public java.lang.String getFecha_rxp() {
        return fecha_rxp;
    }
    
    /**
     * Setter for property fecha_rxp.
     * @param fecha_rxp New value of property fecha_rxp.
     */
    public void setFecha_rxp(java.lang.String fecha_rxp) {
        this.fecha_rxp = fecha_rxp;
    }
    
    /**
     * Getter for property no_cuotas_rxp.
     * @return Value of property no_cuotas_rxp.
     */
    public int getNo_cuotas_rxp() {
        return no_cuotas_rxp;
    }
    
    /**
     * Setter for property no_cuotas_rxp.
     * @param no_cuotas_rxp New value of property no_cuotas_rxp.
     */
    public void setNo_cuotas_rxp(int no_cuotas_rxp) {
        this.no_cuotas_rxp = no_cuotas_rxp;
    }
    
    /**
     * Getter for property no_cuotas_transfer.
     * @return Value of property no_cuotas_transfer.
     */
    public int getNo_cuotas_transfer() {
        return no_cuotas_transfer;
    }
    
    /**
     * Setter for property no_cuotas_transfer.
     * @param no_cuotas_transfer New value of property no_cuotas_transfer.
     */
    public void setNo_cuotas_transfer(int no_cuotas_transfer) {
        this.no_cuotas_transfer = no_cuotas_transfer;
    }
    
    /**
     * Getter for property no_item.
     * @return Value of property no_item.
     */
    public java.lang.String getNo_item() {
        return no_item;
    }
    
    /**
     * Setter for property no_item.
     * @param no_item New value of property no_item.
     */
    public void setNo_item(java.lang.String no_item) {
        this.no_item = no_item;
    }
    
    /**
     * Getter for property no_recurrente.
     * @return Value of property no_recurrente.
     */
    public java.lang.String getNo_recurrente() {
        return no_recurrente;
    }
    
    /**
     * Setter for property no_recurrente.
     * @param no_recurrente New value of property no_recurrente.
     */
    public void setNo_recurrente(java.lang.String no_recurrente) {
        this.no_recurrente = no_recurrente;
    }
    
    /**
     * Getter for property nom_proveedor_rxp.
     * @return Value of property nom_proveedor_rxp.
     */
    public java.lang.String getNom_proveedor_rxp() {
        return nom_proveedor_rxp;
    }
    
    /**
     * Setter for property nom_proveedor_rxp.
     * @param nom_proveedor_rxp New value of property nom_proveedor_rxp.
     */
    public void setNom_proveedor_rxp(java.lang.String nom_proveedor_rxp) {
        this.nom_proveedor_rxp = nom_proveedor_rxp;
    }
    
    /**
     * Getter for property proveedor_rxp.
     * @return Value of property proveedor_rxp.
     */
    public java.lang.String getProveedor_rxp() {
        return proveedor_rxp;
    }
    
    /**
     * Setter for property proveedor_rxp.
     * @param proveedor_rxp New value of property proveedor_rxp.
     */
    public void setProveedor_rxp(java.lang.String proveedor_rxp) {
        this.proveedor_rxp = proveedor_rxp;
    }
    
    /**
     * Getter for property vlr_prox_cuota.
     * @return Value of property vlr_prox_cuota.
     */
    public double getVlr_prox_cuota() {
        return vlr_prox_cuota;
    }
    
    /**
     * Setter for property vlr_prox_cuota.
     * @param vlr_prox_cuota New value of property vlr_prox_cuota.
     */
    public void setVlr_prox_cuota(double vlr_prox_cuota) {
        this.vlr_prox_cuota = vlr_prox_cuota;
    }
    
    /**
     * Getter for property vlr_rxp.
     * @return Value of property vlr_rxp.
     */
    public double getVlr_rxp() {
        return vlr_rxp;
    }
    
    /**
     * Setter for property vlr_rxp.
     * @param vlr_rxp New value of property vlr_rxp.
     */
    public void setVlr_rxp(double vlr_rxp) {
        this.vlr_rxp = vlr_rxp;
    }
    
   /*
     * Carga los campos del reporte RxP por Concepto
     * @autor Ing. Andr�s Maturana D.
     * @param rs <code>ResultSet</code>
     */
    public void loadReporteRxP( ResultSet rs ) throws SQLException{
        this.setNo_recurrente(rs.getString("documento"));
        this.setFecha_rxp(rs.getString("fecha_documento"));
        this.setDescripcion_rxp(rs.getString("descripcion"));
        this.setProveedor_rxp(rs.getString("proveedor"));
        this.setNom_proveedor_rxp(rs.getString("nom_proveedor"));
        this.setVlr_rxp(rs.getDouble("vlr_neto"));
        this.setNo_cuotas_rxp(rs.getInt("num_cuotas"));
        this.setFecha_ini_dscto(rs.getString("fecha_inicio_transfer").replaceAll("Sunday","Domingo").replaceAll("Monday","Lunes").replaceAll("Tuesday","Martes")
            .replaceAll("Wednesday","Mi�rcoles").replaceAll("Thursday","Jueves").replaceAll("Friday","Viernes").replaceAll("Saturday","S�bado"));
        this.setNo_cuotas_transfer(rs.getInt("num_cuotas_transfer"));

        double neto_item = ( this.getNo_cuotas_transfer() + 1 < this.getNo_cuotas_rxp() )?
        this.redondear(this.getVlr_rxp()/(double)this.getNo_cuotas_rxp(), "PES"):
            this.getVlr_rxp() - this.redondear(this.getVlr_rxp()/(double)this.getNo_cuotas_rxp(), "PES") * (double)( this.getNo_cuotas_rxp() - 1 );

        this.setVlr_prox_cuota(neto_item);
        this.setFecha_prox_cuota(rs.getString("fecha_prox_transfer"));
        this.setFecha_fin_dcto(rs.getString("fecha_fin_transfer"));
        this.setNo_item(rs.getString("item"));
        this.setDescripcion_rxp_item(rs.getString("descripcion_item"));
        this.setConcepto_code(rs.getString("concepto"));
        this.setConcepto_desc(rs.getString("concepto_desc"));
        this.setCod_cuenta(rs.getString("codigo_cuenta"));
        
        //AMATURANA 27.04.2007
        this.setVlr_transfer_ml(rs.getDouble("vlr_transfer_ml"));
        this.setSaldo(this.getVlr_rxp() - this.getVlr_transfer_ml());
    }
    
      private double redondear( double valor, String moneda){
        return ( moneda.equals("DOL") ? Util.roundByDecimal(valor, 2) : (int)Math.round(valor));
    }
    
      /**
       * Getter for property vlr_transfer_ml.
       * @return Value of property vlr_transfer_ml.
       */
      public double getVlr_transfer_ml() {
          return vlr_transfer_ml;
      }
      
      /**
       * Setter for property vlr_transfer_ml.
       * @param vlr_transfer_ml New value of property vlr_transfer_ml.
       */
      public void setVlr_transfer_ml(double vlr_transfer_ml) {
          this.vlr_transfer_ml = vlr_transfer_ml;
      }
      
      /**
       * Getter for property saldo.
       * @return Value of property saldo.
       */
      public double getSaldo() {
          return saldo;
      }
      
      /**
       * Setter for property saldo.
       * @param saldo New value of property saldo.
       */
      public void setSaldo(double saldo) {
          this.saldo = saldo;
      }
      
      /**
       * Getter for property nitter.
       * @return Value of property nitter.
       */
      public java.lang.String getNitter() {
          return nitter;
      }
      
      /**
       * Setter for property nitter.
       * @param nitter New value of property nitter.
       */
      public void setNitter(java.lang.String nitter) {
          this.nitter = nitter;
      }
      
      /**
       * Getter for property docu.
       * @return Value of property docu.
       */
      public java.lang.String getDocu() {
          return docu;
      }
      
      /**
       * Setter for property docu.
       * @param docu New value of property docu.
       */
      public void setDocu(java.lang.String docu) {
          this.docu = docu;
      }
      
      /**
       * Getter for property numaval.
       * @return Value of property numaval.
       */
      public java.lang.String getNumaval() {
          return numaval;
      }
      
      /**
       * Setter for property numaval.
       * @param numaval New value of property numaval.
       */
      public void setNumaval(java.lang.String numaval) {
          this.numaval = numaval;
      }
      
      /**
       * Getter for property codbco.
       * @return Value of property codbco.
       */
      public java.lang.String getCodbco() {
          return codbco;
      }
      
      /**
       * Setter for property codbco.
       * @param codbco New value of property codbco.
       */
      public void setCodbco(java.lang.String codbco) {
          this.codbco = codbco;
      }
      
      /**
       * Getter for property refcl.
       * @return Value of property refcl.
       */
      public java.lang.String getRefcl() {
          return refcl;
      }
      
      /**
       * Setter for property refcl.
       * @param refcl New value of property refcl.
       */
      public void setRefcl(java.lang.String refcl) {
          this.refcl = refcl;
      }
      
      /**
       * Getter for property cmc.
       * @return Value of property cmc.
       */
      public java.lang.String getCmc() {
          return cmc;
      }
      
      /**
       * Setter for property cmc.
       * @param cmc New value of property cmc.
       */
      public void setCmc(java.lang.String cmc) {
          this.cmc = cmc;
      }
      
      /**
     * Getter for property negocio.
     * @return Value of property negocio.
     */
    public java.lang.String getNegocio() {
        return negocio;
    }
    
    /**
     * Setter for property negocio asocioado a una factura.
     * @param negocio New value of property negocio.
     */
    public void setNegocio(java.lang.String negocio) {
        this.negocio = negocio;
    }
    
    
    public java.lang.String getTipoRef1() {
        return tipo_ref1;
    }
        
    public void setTipoRef1(java.lang.String x1) {
        this.tipo_ref1 = x1;
    }
    
    public java.lang.String getRef1() {
        return ref1;
    }
        
    public void setRef1(java.lang.String x1) {
        this.ref1 = x1;
    }
    
    public java.lang.String getTipoRef2() {
        return tipo_ref2;
    }
        
    public void setTipoRef2(java.lang.String x1) {
        this.tipo_ref2 = x1;
    }
    
    public java.lang.String getRef2() {
        return ref2;
    }
        
    public void setRef2(java.lang.String x1) {
        this.ref2 = x1;
    }
    public String getFechaFacturacion() {
        return fechaFacturacion;
    }

    public void setFechaFacturacion(String fechaFacturacion) {
        this.fechaFacturacion = fechaFacturacion;
    }
    
         public String getNumero_pagare() {
        return numero_pagare;
    }

    public void setNumero_pagare(String numero_pagare) {
        this.numero_pagare = numero_pagare;
    }

    /**
     * @return the gestion
     */
    public String getGestion() {
        return gestion;
    }

    /**
     * @param gestion the gestion to set
     */
    public void setGestion(String gestion) {
        this.gestion = gestion;
    }

    /**
     * @return the proxima_accion
     */
    public String getProxima_accion() {
        return proxima_accion;
    }

    /**
     * @param proxima_accion the proxima_accion to set
     */
    public void setProxima_accion(String proxima_accion) {
        this.proxima_accion = proxima_accion;
    }

    /**
     * @return the fecha_proxima_accion
     */
    public String getFecha_proxima_accion() {
        return fecha_proxima_accion;
    }

    /**
     * @param fecha_proxima_accion the fecha_proxima_accion to set
     */
    public void setFecha_proxima_accion(String fecha_proxima_accion) {
        this.fecha_proxima_accion = fecha_proxima_accion;
    }

    /**
     * @return the estado_cliente
     */
    public String getEstado_cliente() {
        return estado_cliente;
    }

    /**
     * @param estado_cliente the estado_cliente to set
     */
    public void setEstado_cliente(String estado_cliente) {
        this.estado_cliente = estado_cliente;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * @param barrio the barrio to set
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the cedula_estudiante
     */
    public String getCedula_estudiante() {
        return cedula_estudiante;
    }

    /**
     * @param cedula_estudiante the cedula_estudiante to set
     */
    public void setCedula_estudiante(String cedula_estudiante) {
        this.cedula_estudiante = cedula_estudiante;
    }

    /**
     * @return the nombre_estudiante
     */
    public String getNombre_estudiante() {
        return nombre_estudiante;
    }

    /**
     * @param nombre_estudiante the nombre_estudiante to set
     */
    public void setNombre_estudiante(String nombre_estudiante) {
        this.nombre_estudiante = nombre_estudiante;
    }

    /**
     * @return the telefono_estudiante
     */
    public String getTelefono_estudiante() {
        return telefono_estudiante;
    }

    /**
     * @param telefono_estudiante the telefono_estudiante to set
     */
    public void setTelefono_estudiante(String telefono_estudiante) {
        this.telefono_estudiante = telefono_estudiante;
    }

    /**
     * @return the direccion_estudiante
     */
    public String getDireccion_estudiante() {
        return direccion_estudiante;
    }

    /**
     * @param direccion_estudiante the direccion_estudiante to set
     */
    public void setDireccion_estudiante(String direccion_estudiante) {
        this.direccion_estudiante = direccion_estudiante;
    }

    /**
     * @return the cedula_codeudor
     */
    public String getCedula_codeudor() {
        return cedula_codeudor;
    }

    /**
     * @param cedula_codeudor the cedula_codeudor to set
     */
    public void setCedula_codeudor(String cedula_codeudor) {
        this.cedula_codeudor = cedula_codeudor;
    }

    /**
     * @return the nombre_codeudor
     */
    public String getNombre_codeudor() {
        return nombre_codeudor;
    }

    /**
     * @param nombre_codeudor the nombre_codeudor to set
     */
    public void setNombre_codeudor(String nombre_codeudor) {
        this.nombre_codeudor = nombre_codeudor;
    }

    /**
     * @return the telefono_codeudor
     */
    public String getTelefono_codeudor() {
        return telefono_codeudor;
    }

    /**
     * @param telefono_codeudor the telefono_codeudor to set
     */
    public void setTelefono_codeudor(String telefono_codeudor) {
        this.telefono_codeudor = telefono_codeudor;
    }

    /**
     * @return the direccion_codeudor
     */
    public String getDireccion_codeudor() {
        return direccion_codeudor;
    }

    /**
     * @param direccion_codeudor the direccion_codeudor to set
     */
    public void setDireccion_codeudor(String direccion_codeudor) {
        this.direccion_codeudor = direccion_codeudor;
    }

    /**
     * @return the asesor
     */
    public String getAsesor() {
        return asesor;
    }

    /**
     * @param asesor the asesor to set
     */
    public void setAsesor(String asesor) {
        this.asesor = asesor;
    }

    /**
     * @return the fecha_ultimo_pago
     */
    public String getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    /**
     * @param fecha_ultimo_pago the fecha_ultimo_pago to set
     */
    public void setFecha_ultimo_pago(String fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    /**
     * @return the vlr_ultimo_pago
     */
    public double getVlr_ultimo_pago() {
        return vlr_ultimo_pago;
    }

    /**
     * @param vlr_ultimo_pago the vlr_ultimo_pago to set
     */
    public void setVlr_ultimo_pago(double vlr_ultimo_pago) {
        this.vlr_ultimo_pago = vlr_ultimo_pago;
    }

    /**
     * @return the rango
     */
    public String getRango() {
        return rango;
    }

    /**
     * @param rango the rango to set
     */
    public void setRango(String rango) {
        this.rango = rango;
    }

    /**
     * @return the id_convenio
     */
    public String getId_convenio() {
        return id_convenio;
    }

    /**
     * @param id_convenio the id_convenio to set
     */
    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }

    /**
     * @return the vlr_cuota_manejo
     */
    public double getVlr_cuota_manejo() {
        return vlr_cuota_manejo;
    }

    /**
     * @param vlr_cuota_manejo the vlr_cuota_manejo to set
     */
    public void setVlr_cuota_manejo(double vlr_cuota_manejo) {
        this.vlr_cuota_manejo = vlr_cuota_manejo;
    }
    
    public double getValor_interes() {
        return valor_interes;
    }

    public void setValor_interes(double valor_interes) {
        this.valor_interes = valor_interes;
    }

    public double getValor_cat() {
        return valor_cat;
    }

    public void setValor_cat(double valor_cat) {
        this.valor_cat = valor_cat;
    }

    public double getVlr_capital() {
        return capital;
    }
    
    public void setVlr_capital(double capital) {
        this.capital = capital;
    }
    

    public double getValor_seguro() {
        return seguro;
    }
    
     public void setValor_seguro(double seguro) {
        this.seguro = seguro;
    }
     
    
    
}
