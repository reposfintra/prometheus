/*
 * StringStatement.java
 * Version b2.0
 * Created on 17 de mayo de 2007, 04:37 PM
 */

package com.tsp.operation.model.beans;

import java.io.InputStream;//JJCastro fase2
import java.sql.SQLException;//JJCastro fase2
import java.util.Vector;
/**
 *
 * @author  Osvaldo P�rez Ferrer
 */

/*********************************************************
 * Clase para implementar seteo de parammetros a consultas sql
 *********************************************************/
public class StringStatement {
    
    private StringBuffer sql;
    private Vector posiciones;
    private boolean index;
    private String control = "-C-O-N-T-R-O-L-";
    private StringBuffer sbuf;//JJCastro fase2

   //JJCastro fase2
    /** Tag maker for parameters */
    public static final String TAG_MARKER        = "!%";
    /** Escape for tag maker */
    public static final String TAG_MARKER_ESCAPE = TAG_MARKER + ";";
    /** Tag for parameters start delimiter */
    public static final String START_PARAM_TAG   = "<" + TAG_MARKER;
    /** Tag for parameters end delimiter */
    public static final String END_PARAM_TAG     = "|" + TAG_MARKER + ">";
    /** Tag for a byte parameter */
    public static final String BYTE_TAG          = "b|";
    /** Tag for a bytes (used for Blob) parameter */
    public static final String BYTES_TAG         = "B|";
    /** Tag for a BLOB (used for null Blob) parameter */
    public static final String BLOB_TAG          = "c|";
    /** Tag for a CLOB (used for null Clob) parameter */
    public static final String CLOB_TAG          = "C|";
    
    /*********************************************************
     * Constructor para trabajar sin especificacion de indices
     * de parametros
     * @param: String sql, la consulta a trabajar
     *********************************************************/
    public StringStatement( String sql ) {
        this.sql = new StringBuffer(sql.replaceAll("\\?",this.control));
    }
    
    /*********************************************************
     * Constructor para trabajar especificando los indices
     * de parametros
     * @param: String sql, la consulta a trabajar
     * @param: boolean index, indicar si se va a trabajar especificando indices
     *********************************************************/
    public StringStatement( String sql, boolean index ) {
        
        this.sql    = new StringBuffer(sql.replaceAll("\\?",this.control));
        this.index  = index;
        this.posiciones = new Vector();
        
        int last_index = 0;
        
        for( int i=0; i<this.sql.length(); i++ ){
            
            last_index = this.sql.indexOf( this.control,last_index+1);
            
            if(last_index==-1){
                break;
            }else{
                posiciones.add( null );
            }
            
        }
        
    }

    /**
     * Metodo getSql
     * @return: String, la consulta construida
     */
    public java.lang.String getSql() throws Exception{

        if( this.index == false ){
            return sql.toString();
        }else{


            for( int i=0; i<this.posiciones.size(); i++ ){

                Object o = this.posiciones.get(i);
                if(o==null){
                    o=new String ("");
                }

                if( o instanceof Double ){
                    this.setDouble( (o!=null)?((Double)o).doubleValue():00);
                }else if( o instanceof Integer ){
                    this.setInt( (o!=null)?((Integer)o).intValue():00);
                } else if (o instanceof String) {
                    this.setString((o != null) ? (String) o : "");
                } else if (o instanceof Boolean) {
                    this.setBoolean(((Boolean) o).booleanValue());
                }
            }

            return sql.toString();

        }
    }

    /**
     * Metodo setSql
     * @param String sql, especificar la consulta con la que se va a trabajar
     */
    public void setSql(java.lang.String sql) {
        this.sql = new StringBuffer(sql);
    }
    
    private void replace( String value, String coma ){
        
        this.sql.replace( sql.indexOf( this.control ), sql.indexOf( this.control )+ this.control.length() , coma+value+coma );
        
    }
    
    public void setString( String value ){
        
        value = value.replaceAll("'","''");
        this.replace( value, "'" );
        
    }
    
    public void setDouble( double value ){
        
        this.replace( String.valueOf( value ), "" );
        
    }
    
    public void setInt( int value ){
        
        this.replace( String.valueOf( value ), "" );
        
    }
    
    public void setString( int index, String value ) throws Exception{
        //System.out.println("llego aca con :"+index+"   value  : "+value);
        try{
            
            this.posiciones.setElementAt( value, index-1 );
            
        }catch ( java.lang.ArrayIndexOutOfBoundsException e ){
            throw new Exception(" ERROR StringStatement.setString - Parametro fuera de rango "+ index);
        }
        
    }
    
    public void setDouble( int index, double value ) throws Exception{
        
        try{
            
            Double doub  = new Double(value);
            this.posiciones.setElementAt( doub, index-1 );
            Object b;
            
        }catch ( java.lang.ArrayIndexOutOfBoundsException e ){
            throw new Exception(" ERROR StringStatement.setDouble - Parametro fuera de rango "+ index);
        }
    }
    
    public void setInt( int index, int value ) throws Exception{
        
        try{
            
            Integer in = new Integer( value );
            this.posiciones.setElementAt( in, index-1 );
            
        }catch ( java.lang.ArrayIndexOutOfBoundsException e ){
            throw new Exception(" ERROR StringStatement.setInt - Parametro fuera de rango "+ index);
        }
        
    }

        /**
     * Stores a binary stream into parameters array, using an intermediate byte[].
     * When a very large binary value is input to a LONGVARBINARY parameter, it
     * may be more practical to send it via a java.io.InputStream. JDBC will read
     * the data from the stream as needed, until it reaches end-of-file. This
     * should be more or less equivalent to setBytes(blob.getBytes()).
     * <p>
     * <b>Note: </b> This stream object can either be a standard Java stream
     * object or your own subclass that implements the standard interface.
     *
     * @param parameterIndex the first parameter is 1...
     * @param inStreamArg the parameter value
     * @param length the parameter length
     * @exception SQLException if a database access error occurs
     * @see java.sql.PreparedStatement#setBinaryStream(int, java.io.InputStream,
     *      int)
     */
    //JJCastro fase2
    public void setBinaryStream(InputStream inStreamArg,
    int length) throws SQLException {
        byte[] data = new byte[length];
        try {
            inStreamArg.read(data, 0, length);
        }
        catch (Exception ioe) {
            ioe.printStackTrace();
            throw new SQLException("Problem with streaming of data");
        }
        setBytes(data);
    }

        /**
     * Sets a parameter to a Java array of bytes.
     * <p>
     *
     * @param parameterIndex the first parameter is 1...
     * @param x the parameter value
     * @exception SQLException if a database access error occurs
     */
    //JJCastro fase2
    public void setBytes(byte[] x) throws SQLException {
        String blob;
        HexaBlobFilter hexBlobFilter = new HexaBlobFilter();
        try {
            synchronized (sbuf) {
                /**
                 * Encoded only for request inlining. Decoded right away by the
                 * controller at static
                 * {@link #setPreparedStatement(String, java.sql.PreparedStatement)}
                 */
                blob = new HexaBlobFilter().encode(x);
                setWithTag(BYTES_TAG, blob);

            }
        }
        catch (OutOfMemoryError oome) {
            blob = null;
            sbuf = null;
            System.gc();
            throw new SQLException("Out of memory");
        }
    }



        /**
     * Stores parameter and its type as a <em>quoted</em> String, so the
     * controller can decode them back. Used when driverProcessed is false.
     * <p>
     * When isDriverProcessed() is false, we could avoid inlining the arguments
     * and just tag them and send them apart as an object list. But this would
     * imply a couple of changes elsewhere, among other: macro-handling,
     * recoverylog,...
     *
     * @param paramIndex the index into the inString
     * @param typeTag type of the parameter
     * @param param the parameter string to be stored
     * @exception SQLException if something goes wrong
     * @see #setPreparedStatement(String, java.sql.PreparedStatement)
     *
     */
    //JJCastro fase2
    private void setWithTag(String typeTag, String param)
    throws SQLException {
        /**
         * insert TAGS so the controller can parse and "unset" the request using
         * {@link #setPreparedStatement(String, java.sql.PreparedStatement)
         */
        set(START_PARAM_TAG + typeTag
        + Strings.replace(param, TAG_MARKER, TAG_MARKER_ESCAPE) + END_PARAM_TAG);
    }


        /**
     * Actually stores the IN parameter into parameters String array. Called by
     * most setXXX() methods.
     *
     * @param paramIndex the index into the inString
     * @param s a string to be stored
     * @exception SQLException if something goes wrong
     */
    private void set(String s) throws SQLException {
        setString(s);
    }
public void setBoolean(int index, boolean value) throws Exception {

        try {

            Boolean in = new Boolean(value);
            this.posiciones.setElementAt(in, index - 1);

        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
            throw new Exception(" ERROR StringStatement.setBoolean - Parametro fuera de rango " + index);
        }

    }

    public void setBoolean(boolean value) {

        this.replace(String.valueOf(value), "");

    }

    public void setInt(int i, String id_bodega_origen) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
