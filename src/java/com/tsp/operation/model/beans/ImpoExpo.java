/*
 * Nombre        ImpoExpo.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         19 de octubre de 2006, 11:11 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

public class ImpoExpo {
        
    private String reg_status;
    private String dstrct;
    private String tipo_doc;
    private String documento;
    private String fecha_sia;
    private String fecha_eta;
    private String descripcion;
    private String usuario_genero;
    private String fecha_genero;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String codcli;
    private String fecha_llegada_cdr;
    private String fecha_salida_cdr;
    
    //para el reporte
    
    private String contenedor;    
    private String conductor; 
    private String placa;
    private String fecha_cargue;
    private String hora_salida;
    private String patio;
    private String ot;
    private String factura;
    private String fecha_fac;
    private String vlr_transporte;
    private String vlr_factura;
    
    /**
     * Crea una nueva instancia de  ImpoExpo
     */
    public ImpoExpo() {
    }
    
    /**
     * Getter for property fecha_sia.
     * @return Value of property fecha_sia.
     */
    public java.lang.String getFecha_sia() {
        return fecha_sia;
    }
    
    /**
     * Setter for property fecha_sia.
     * @param fecha_sia New value of property fecha_sia.
     */
    public void setFecha_sia(java.lang.String fecha_sia) {
        this.fecha_sia = fecha_sia;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_eta.
     * @return Value of property fecha_eta.
     */
    public java.lang.String getFecha_eta() {
        return fecha_eta;
    }
    
    /**
     * Setter for property fecha_eta.
     * @param fecha_eta New value of property fecha_eta.
     */
    public void setFecha_eta(java.lang.String fecha_eta) {
        this.fecha_eta = fecha_eta;
    }
    
    /**
     * Getter for property fecha_genero.
     * @return Value of property fecha_genero.
     */
    public java.lang.String getFecha_genero() {
        return fecha_genero;
    }
    
    /**
     * Setter for property fecha_genero.
     * @param fecha_genero New value of property fecha_genero.
     */
    public void setFecha_genero(java.lang.String fecha_genero) {
        this.fecha_genero = fecha_genero;
    }
    
    /**
     * Getter for property fecha_llegada_cdr.
     * @return Value of property fecha_llegada_cdr.
     */
    public java.lang.String getFecha_llegada_cdr() {
        return fecha_llegada_cdr;
    }
    
    /**
     * Setter for property fecha_llegada_cdr.
     * @param fecha_llegada_cdr New value of property fecha_llegada_cdr.
     */
    public void setFecha_llegada_cdr(java.lang.String fecha_llegada_cdr) {
        this.fecha_llegada_cdr = fecha_llegada_cdr;
    }
    
    /**
     * Getter for property fecha_salida_cdr.
     * @return Value of property fecha_salida_cdr.
     */
    public java.lang.String getFecha_salida_cdr() {
        return fecha_salida_cdr;
    }
    
    /**
     * Setter for property fecha_salida_cdr.
     * @param fecha_salida_cdr New value of property fecha_salida_cdr.
     */
    public void setFecha_salida_cdr(java.lang.String fecha_salida_cdr) {
        this.fecha_salida_cdr = fecha_salida_cdr;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc() {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc(java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property usuario_genero.
     * @return Value of property usuario_genero.
     */
    public java.lang.String getUsuario_genero() {
        return usuario_genero;
    }
    
    /**
     * Setter for property usuario_genero.
     * @param usuario_genero New value of property usuario_genero.
     */
    public void setUsuario_genero(java.lang.String usuario_genero) {
        this.usuario_genero = usuario_genero;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property contenedor.
     * @return Value of property contenedor.
     */
    public java.lang.String getContenedor() {
        return contenedor;
    }
    
    /**
     * Setter for property contenedor.
     * @param contenedor New value of property contenedor.
     */
    public void setContenedor(java.lang.String contenedor) {
        this.contenedor = contenedor;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property hora_salida.
     * @return Value of property hora_salida.
     */
    public java.lang.String getHora_salida() {
        return hora_salida;
    }
    
    /**
     * Setter for property hora_salida.
     * @param hora_salida New value of property hora_salida.
     */
    public void setHora_salida(java.lang.String hora_salida) {
        this.hora_salida = hora_salida;
    }
    
    /**
     * Getter for property patio.
     * @return Value of property patio.
     */
    public java.lang.String getPatio() {
        return patio;
    }
    
    /**
     * Setter for property patio.
     * @param patio New value of property patio.
     */
    public void setPatio(java.lang.String patio) {
        this.patio = patio;
    }
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt() {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt(java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property fecha_fac.
     * @return Value of property fecha_fac.
     */
    public java.lang.String getFecha_fac() {
        return fecha_fac;
    }
    
    /**
     * Setter for property fecha_fac.
     * @param fecha_fac New value of property fecha_fac.
     */
    public void setFecha_fac(java.lang.String fecha_fac) {
        this.fecha_fac = fecha_fac;
    }
    
    /**
     * Getter for property vlr_transporte.
     * @return Value of property vlr_transporte.
     */
    public java.lang.String getVlr_transporte() {
        return vlr_transporte;
    }
    
    /**
     * Setter for property vlr_transporte.
     * @param vlr_transporte New value of property vlr_transporte.
     */
    public void setVlr_transporte(java.lang.String vlr_transporte) {
        this.vlr_transporte = vlr_transporte;
    }
    
    /**
     * Getter for property vlr_factura.
     * @return Value of property vlr_factura.
     */
    public java.lang.String getVlr_factura() {
        return vlr_factura;
    }
    
    /**
     * Setter for property vlr_factura.
     * @param vlr_factura New value of property vlr_factura.
     */
    public void setVlr_factura(java.lang.String vlr_factura) {
        this.vlr_factura = vlr_factura;
    }
    
    /**
     * Getter for property fecha_cargue.
     * @return Value of property fecha_cargue.
     */
    public java.lang.String getFecha_cargue() {
        return fecha_cargue;
    }
    
    /**
     * Setter for property fecha_cargue.
     * @param fecha_cargue New value of property fecha_cargue.
     */
    public void setFecha_cargue(java.lang.String fecha_cargue) {
        this.fecha_cargue = fecha_cargue;
    }
    
}
