/*
 * Cambio_cheque.java
 *
 * Created on 20 de octubre de 2005, 05:18 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
/**
 *
 * @author  JuanM
 */
public class Cambio_cheque {
        private String dstrct;
        private String branch_code; /*banco*/
        private String bank_account; /*sucursal*/
        private String document_no; /*cheque*/
        private String new_document_no; /*nuevo cheque*/
        private String usuario_migracion;
        private String fecha_migracion;
        private double valorchk;
        private String agencia;
        private String planilla;      
        
        /** Creates a new instance of Cambio_cheque */
        public Cambio_cheque() {
        }
        
        public static Cambio_cheque load(ResultSet rs)throws Exception{
                Cambio_cheque datos = new Cambio_cheque();
                datos.setDstrct(rs.getString("DSTRCT"));
                datos.setBranch_code(rs.getString("BRANCH_CODE"));
                datos.setBank_account(rs.getString("BANK_ACCOUNT_NO"));
                datos.setDocument_no(rs.getString("DOCUMENT_NO"));
                datos.setNew_document_no(rs.getString("NEW_DOCUMENT_NO"));
                datos.setUsuario_migracion(rs.getString("USUARIO_MIGRACION"));
                datos.setFecha_migracion(rs.getString("FECHA_MIGRACION"));               
                return datos;
        }
        
        /**
         * Getter for property bank_account.
         * @return Value of property bank_account.
         */
        public java.lang.String getBank_account() {
                return bank_account;
        }
        
        /**
         * Setter for property bank_account.
         * @param bank_account New value of property bank_account.
         */
        public void setBank_account(java.lang.String bank_account) {
                this.bank_account = bank_account;
        }
        
        /**
         * Getter for property branch_code.
         * @return Value of property branch_code.
         */
        public java.lang.String getBranch_code() {
                return branch_code;
        }
        
        /**
         * Setter for property branch_code.
         * @param branch_code New value of property branch_code.
         */
        public void setBranch_code(java.lang.String branch_code) {
                this.branch_code = branch_code;
        }
        
        /**
         * Getter for property document_no.
         * @return Value of property document_no.
         */
        public java.lang.String getDocument_no() {
                return document_no;
        }
        
        /**
         * Setter for property document_no.
         * @param document_no New value of property document_no.
         */
        public void setDocument_no(java.lang.String document_no) {
                this.document_no = document_no;
        }
        
        /**
         * Getter for property dstrct.
         * @return Value of property dstrct.
         */
        public java.lang.String getDstrct() {
                return dstrct;
        }
        
        /**
         * Setter for property dstrct.
         * @param dstrct New value of property dstrct.
         */
        public void setDstrct(java.lang.String dstrct) {
                this.dstrct = dstrct;
        }
        
        /**
         * Getter for property fecha_migracion.
         * @return Value of property fecha_migracion.
         */
        public java.lang.String getFecha_migracion() {
                return fecha_migracion;
        }
        
        /**
         * Setter for property fecha_migracion.
         * @param fecha_migracion New value of property fecha_migracion.
         */
        public void setFecha_migracion(java.lang.String fecha_migracion) {
                this.fecha_migracion = fecha_migracion;
        }
        
        /**
         * Getter for property new_document_no.
         * @return Value of property new_document_no.
         */
        public java.lang.String getNew_document_no() {
                return new_document_no;
        }
        
        /**
         * Setter for property new_document_no.
         * @param new_document_no New value of property new_document_no.
         */
        public void setNew_document_no(java.lang.String new_document_no) {
                this.new_document_no = new_document_no;
        }
        
        /**
         * Getter for property usuario_migracion.
         * @return Value of property usuario_migracion.
         */
        public java.lang.String getUsuario_migracion() {
                return usuario_migracion;
        }
        
        /**
         * Setter for property usuario_migracion.
         * @param usuario_migracion New value of property usuario_migracion.
         */
        public void setUsuario_migracion(java.lang.String usuario_migracion) {
                this.usuario_migracion = usuario_migracion;
        }
        
        /**
         * Getter for property valorchk.
         * @return Value of property valorchk.
         */
        public double getValorchk() {
                return valorchk;
        }
        
        /**
         * Setter for property valorchk.
         * @param valorchk New value of property valorchk.
         */
        public void setValorchk(double valorchk) {
                this.valorchk = valorchk;
        }
        
        /**
         * Getter for property agencia.
         * @return Value of property agencia.
         */
        public java.lang.String getAgencia() {
            return agencia;
        }
        
        /**
         * Setter for property agencia.
         * @param agencia New value of property agencia.
         */
        public void setAgencia(java.lang.String agencia) {
            this.agencia = agencia;
        }
        
        /**
         * Getter for property planilla.
         * @return Value of property planilla.
         */
        public java.lang.String getPlanilla() {
            return planilla;
        }
        
        /**
         * Setter for property planilla.
         * @param planilla New value of property planilla.
         */
        public void setPlanilla(java.lang.String planilla) {
            this.planilla = planilla;
        }
        
}
