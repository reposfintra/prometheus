/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class Contratista {

  private String  id_contratista ;
  private String  nombre_contratista ;
  private int     secuencia_prefactura ;
  private String  nit;



    /** Creates a new instance of Contratista */
    public Contratista() {
    }

    public static Contratista load(java.sql.ResultSet rs)throws java.sql.SQLException{

        Contratista contratista = new Contratista();

        contratista.setId_contratista( rs.getString("id_contratista") );
        contratista.setNombre_contratista( rs.getString("nombre_contratista") ); // renombrando campo descripcion
        contratista.setSecuencia_prefactura( rs.getInt("secuencia_prefactura") );
        contratista.setNit( rs.getString("nit") );
        return contratista;
    }


    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the nombre_contratista
     */
    public String getNombre_contratista() {
        return nombre_contratista;
    }

    /**
     * @param nombre_contratista the nombre_contratista to set
     */
    public void setNombre_contratista(String nombre_contratista) {
        this.nombre_contratista = nombre_contratista;
    }

    /**
     * @return the secuencia_prefactura
     */
    public int getSecuencia_prefactura() {
        return secuencia_prefactura;
    }

    /**
     * @param secuencia_prefactura the secuencia_prefactura to set
     */
    public void setSecuencia_prefactura(int secuencia_prefactura) {
        this.secuencia_prefactura = secuencia_prefactura;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }



}
