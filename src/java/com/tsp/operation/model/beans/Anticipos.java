/*
 * Anticipos.java
 *
 * Created on 20 de abril de 2005, 09:08 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  kreales
 */
public class Anticipos {
    
    private String dstrct;
    private String sj;
    private String sj_nombre;
    private String tipo_s;
    private String anticipo_code;
    private String anticipo_desc;
    private String creation_date;
    private String creation_user;
    private float valor;
    private String moneda;
    private String indicador;
    private String codmigra;
    private String modif;
    private boolean chequeado;
    private String proveedor;
    
    private java.util.Vector proveedores;
    
    
    /** Creates a new instance of Anticipos */
    public Anticipos() {
    }
    
    public float getValor(){
        return valor;
    }
    public String getMoneda(){
        return moneda;
    }
    public String getIndicador(){
        return indicador;
    }
    public String getCodmigra(){
        return codmigra;
    }
    
    public void setValor(float valor){
        this.valor=valor;
    }
    public void setMoneda(String moneda){
        this.moneda=moneda;
    }
    public void setIndicador(String indicador){
        this.indicador=indicador;
    }
    public void setCodmigra(String codmigra){
        this.codmigra=codmigra;
    }
    
    public String getDstrct(){
        return dstrct;
    }
    public void setSj_nombre(String sj_nombre){
        this.sj_nombre=sj_nombre;
    }
    
    public String getSj_nombre(){
        return sj_nombre;
    }
    public String getAnticipo_code(){
        return anticipo_code;
    }
    public String getSj(){
        return sj;
    }
    public String getTipo_s(){
        return tipo_s;
    }
    public String getAnticipo_desc(){
        return anticipo_desc;
    }
    public String getCreation_user(){
        return creation_user;
    }
    public void setDstrct(String dstrct){
        this.dstrct=dstrct;
    }
    public void setAnticipo_code(String anticipo_code){
        this.anticipo_code=anticipo_code;
    }
    public void setAnticipo_desc(String anticipo_desc){
        this.anticipo_desc=anticipo_desc;
    }
    public void setSj(String sj){
        this.sj=sj;
    }
    public void setTipo_s(String tipo_s){
        this.tipo_s=tipo_s;
    }
    public void setCreation_user(String creation_user){
        this.creation_user=creation_user;
    }
    
    /**
     * Getter for property modif.
     * @return Value of property modif.
     */
    public java.lang.String getModif() {
        return modif;
    }
    
    /**
     * Setter for property modif.
     * @param modif New value of property modif.
     */
    public void setModif(java.lang.String modif) {
        this.modif = modif;
    }
    
    /**
     * Getter for property chequeado.
     * @return Value of property chequeado.
     */
    public boolean isChequeado() {
        return chequeado;
    }
    
    /**
     * Setter for property chequeado.
     * @param chequeado New value of property chequeado.
     */
    public void setChequeado(boolean chequeado) {
        this.chequeado = chequeado;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property proveedores.
     * @return Value of property proveedores.
     */
    public java.util.Vector getProveedores() {
        return proveedores;
    }
    
    /**
     * Setter for property proveedores.
     * @param proveedores New value of property proveedores.
     */
    public void setProveedores(java.util.Vector proveedores) {
        this.proveedores = proveedores;
    }
    
}
