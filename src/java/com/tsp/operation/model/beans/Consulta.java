package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * <br/>
 * 25/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Consulta {

    private Timestamp fecha;
    private String tipoCuenta;
    private String entidad;
    private String oficina;
    private String ciudad;
    private String razon;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private int numConsultas;
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of numConsultas
     *
     * @return the value of numConsultas
     */
    public int getNumConsultas() {
        return numConsultas;
    }

    /**
     * Set the value of numConsultas
     *
     * @param numConsultas new value of numConsultas
     */
    public void setNumConsultas(int numConsultas) {
        this.numConsultas = numConsultas;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of razon
     *
     * @return the value of razon
     */
    public String getRazon() {
        return razon;
    }

    /**
     * Set the value of razon
     *
     * @param razon new value of razon
     */
    public void setRazon(String razon) {
        this.razon = razon;
    }

    /**
     * Get the value of ciudad
     *
     * @return the value of ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Set the value of ciudad
     *
     * @param ciudad new value of ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * Get the value of oficina
     *
     * @return the value of oficina
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * Set the value of oficina
     *
     * @param oficina new value of oficina
     */
    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    /**
     * Get the value of entidad
     *
     * @return the value of entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Set the value of entidad
     *
     * @param entidad new value of entidad
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * Get the value of tipoCuenta
     *
     * @return the value of tipoCuenta
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * Set the value of tipoCuenta
     *
     * @param tipoCuenta new value of tipoCuenta
     */
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    /**
     * Get the value of fecha
     *
     * @return the value of fecha
     */
    public Timestamp getFecha() {
        return fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param fecha new value of fecha
     */
    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

}
