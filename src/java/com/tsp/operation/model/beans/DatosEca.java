/*
 * DatosEca.java
 * Created on 20 de mayo de 2009, 15:26
 */
package com.tsp.operation.model.beans;
public class DatosEca {   
    
    private String sv,valor, fecha, last_update, user_update, creation_date ;
    private String creation_user, reg_status, dstrct, saldo, fecha_cruce, factura;
    private String id,cxcExcel,ms_excel;
    
    public DatosEca() {    }
    
    public String getCxcExcel() {
        return cxcExcel;
    }
    public void setCxcExcel(String x) {
        this.cxcExcel= x;
    }
    public String getMsExcel() {
        return ms_excel;
    }
    public void setMsExcel(String x) {
        this.ms_excel= x;
    }
    public String getSv() {
        return sv;
    }
    public void setSv(String x) {
        this.sv= x;
    }
    public String getValor() {
        return valor;
    }
    public void setValor(String x) {
        this.valor= x;
    }
    public String getFecha() {
        return fecha;
    }
    public void setFecha(String x) {
        this.fecha= x;
    }
    public String getLastUpdate() {
        return last_update;
    }
    public void setLastUpdate(String x) {
        this.last_update= x;
    }
    public String getUserUpdate() {
        return user_update;
    }
    public void setUserUpdate(String x) {
        this.user_update= x;
    }
    public String getCreationDate() {
        return creation_date;
    }
    public void setCreationDate(String x) {
        this.creation_date= x;
    }
    public String getCreationUser() {
        return creation_user;
    }
    public void setCreationUser(String x) {
        this.creation_user= x;
    }    
    
    public String getRegStatus() {
        return reg_status;
    }
    public void setRegStatus(String x) {
        this.reg_status= x;
    }
    public String getDistrito() {
        return dstrct;
    }
    public void setDistrito(String x) {
        this.dstrct= x;
    }
    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String x) {
        this.saldo= x;
    }
    public String getFechaCruce() {
        return fecha_cruce;
    }
    public void setFechaCruce(String x) {
        this.fecha_cruce= x;
    }
    public String getFactura() {
        return factura;
    }
    public void setFactura(String x) {
        this.factura= x;
    }
    public String getId() {
        return id;
    }
    public void setId(String x) {
        this.id= x;
    }    
    
    public static DatosEca load(java.sql.ResultSet rs)throws java.sql.SQLException{
        DatosEca datosEca = new DatosEca();
        datosEca.setSv( rs.getString("simbolo_variable") );
        datosEca.setValor( rs.getString("valor") );
        datosEca.setFecha( rs.getString("fecha") );
        datosEca.setLastUpdate( rs.getString("last_update") );
        datosEca.setUserUpdate( rs.getString("user_update") );
        datosEca.setCreationDate( rs.getString("creation_date") );
        datosEca.setCreationUser( rs.getString("creation_user") );
        datosEca.setRegStatus( rs.getString("reg_status") );
        datosEca.setDistrito( rs.getString("dstrct") );
        datosEca.setSaldo( rs.getString("saldo") );
        datosEca.setFechaCruce( rs.getString("fecha_cruce") );
        datosEca.setFactura( rs.getString("factura") );            
        datosEca.setId( rs.getString("id") );            
        datosEca.setCxcExcel(rs.getString("cxc_excel"));
        datosEca.setMsExcel(rs.getString("ms_excel"));
        return datosEca;
    }    
}