/*
 * RemesaPadre.java
 *
 * Created on 21 de julio de 2005, 08:15 PM
 */
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;


/**
 *
 * @author  Henry
 */
public class  RemesaPadre implements Serializable {
    
    private String code;    
    private String descripcion;        
    private String user_update;
    private String creation_user;
    
    /** Creates a new instance of RemesaAnulada */
    public RemesaPadre() {
    }
    
    //METODOS GET & SET
    public java.lang.String getCodigo() {
        return code;
    }
    
    public void setCodigo(String cod) {
        this.code = cod;
    }
    
    public String getDescripcion() {
        return this.descripcion;
    }    
    
    public String getCreationUser() {
        return this.creation_user;
    }
    
    public void setCreationUser(String usuario_creacion) {
        this.creation_user = usuario_creacion;
    }
    public String getUserUpdate() {
        return this.user_update;
    }    
    public void setUserUpdate( String usuario) {
        this.user_update = usuario;
    }    
}
