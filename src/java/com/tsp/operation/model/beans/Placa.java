/*
 * Placa.java
 *
 * Created on 22 de noviembre de 2004, 02:37 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
import java.io.Serializable;

/**
 * @author AMENDEZT
 */
public class Placa extends Object implements Serializable {
    private String estado;
    private String reg_status;
    private String placa;
    private String condicion;
    private String tarjetaoper;
    private String venctarjetaoper; //verificar si se cambia a date
    private String marca;
    private String clase;
    private String capacidad;
    private String carroceria;
    private String modelo;
    private String color;
    private String nomotor;
    private String nochasis;
    private String noejes;
    private String agencia;
    private String dimcarroceria;
    private String venseguroobliga; //verificar si se cambia a date
    private String homologado;
    private String atitulo;
    private String trailer;
    private String empresaafil;
    private String propietario;
    private String conductor;
    private String tenedor;
    private String tipo;
    private String serial;
    private String tara;
    private String largo;
    private String alto;
    private String llantas;
    private String enganche;
    private String ancho;
    private String piso;
    private String cargue;
    private String volumen;
    private String fechaultact; //verificar cambio a time stamp
    private String usuario;
    private String fechacrea; //verificar cambi a time stamp
    private String usuariocrea;
    private String recurso;
    private String grupoid;
    private String nombre;
    private String grupo;
    private String estadoequipo;
    private String localizacion;
    private java.sql.Date vencSOAT;
    private String proveedor="";
    private String vprop="";
    //nuevo 20-09-2005
    private String numero_rin;
    private String ciasoat;
    private String placa_trailer;
    private int grado_riesgo;
    private String capacidad_trailer;
    private String reg_nal_carga;
    private String fecvenreg;
    private String poliza_andina;
    private String fecvenandina;
    private String tarempresa;
    private String fecvenempresa;
    private String tarhabil;
    private String fecvenhabil;
    private String tarprop;
    private String fecvenprop;
    private String certgases;
    private String fecvengases;
    private String polizasoat;
    //HOsorio 2006 - 01  03
    private String empAfiliada;
    private String ciudadAfiliada;
    private String paisAfiliada;
    private String clasificacion;
    private boolean vetado;
    private boolean tractomula;
    private boolean traile;
    private boolean tieneFoto;
    private String estadoEscolta;;
    //Dbastidas 18.01.06
    private String aprobado;
    //jose de la rosa
    private String ciudad_tarjeta;
    private String resp_civil;
    private String fecvresp_civil;
    private String grupo_equipo;
    private String descuento_equipo;
    private String clasificacion_equipo;
    //DBastidas 22-04-2006
    private String veto;
    
    private String operador_gps = "";
    private String passwd_seguimiento = "";
    private String tiene_gps = "";
    
    
     private String capac_gal;
     private String capac_mts;
    /**
     * Holds value of property textoEstado.
     */
    private String textoEstado;
    
    //Miguel Angel
    public void setSerial(String valor) {
        this.serial = valor;
    }
    
    public String getSerial() {
        return this.serial;
    }
    
    
    public void setEnganche(String valor) {
        this.enganche = valor;
    }
    
    public String getEnganche() {
        return this.enganche;
    }
    
    /**********************************/
    
    public void setReg_status(String reg_status){
        this.reg_status = reg_status;
        if("I".equals(reg_status) )
            this.setTextoEstado("Inactivo");
        else
            this.setTextoEstado("Activo");
    }
    
    public String getReg_status(){
        return this.reg_status;
    }
    
    public void setEstado(String estado){
        this.estado = estado;
    }
    
    public String getEstado(){
        return this.estado;
    }
    
    public void setPlaca(String placa){
        this.placa = placa;
    }
    
    public String getPlaca(){
        return this.placa;
    }
    
    public void setCondicion(String condicion){
        this.condicion = condicion;
    }
    
    public String getCondicion(){
        return this.condicion;
    }
    
    public void setTarjetaoper(String tarjetaoper){
        this.tarjetaoper = tarjetaoper;
    }
    
    public String getTarjetaoper(){
        return this.tarjetaoper;
    }
    
    public void setVenctarjetaoper(String venctarjetaoper){
        this.venctarjetaoper = venctarjetaoper;
    }
    
    public String getVenctarjetaoper(){
        return this.venctarjetaoper;
    }
    
    public void setMarca(String marca){
        this.marca = marca;
    }
    
    public String getMarca(){
        return this.marca;
    }
    
    public void setClase(String clase){
        this.clase = clase;
    }
    
    public String getClase(){
        return this.clase;
    }
    
    public void setCapacidad(String capacidad){
        this.capacidad = capacidad;
    }
    
    public String getCapacidad(){
        return this.capacidad;
    }
    
    public void setCarroceria(String carroceria){
        this.carroceria = carroceria;
    }
    
    public String getCarroceria(){
        return this.carroceria;
    }
    
    public void setModelo(String modelo){
        this.modelo = modelo;
    }
    
    public String getModelo(){
        return this.modelo;
    }
    
    public void setColor(String color){
        this.color = color;
    }
    
    public String getColor(){
        return this.color;
    }
    
    public void setNomotor(String nomotor){
        this.nomotor = nomotor;
    }
    
    public String getNomotor(){
        return this.nomotor;
    }
    
    public void setNochasis(String nochasis){
        this.nochasis = nochasis;
    }
    
    public String getNochasis(){
        return this.nochasis;
    }
    
    public void setNoejes(String noejes){
        this.noejes = noejes;
    }
    
    public String getNoejes(){
        return this.noejes;
    }
    
    public void setAgencia(String agencia){
        this.agencia = agencia;
    }
    
    public String getAgencia(){
        return this.agencia;
    }
    
    public void setDimcarroceria(String dimcarroceria){
        this.dimcarroceria = dimcarroceria;
    }
    
    public String getDimcarroceria(){
        return this.dimcarroceria;
    }
    
    public void setVenseguroobliga(String venseguroobliga){
        this.venseguroobliga = venseguroobliga;
    }
    
    public String getVenseguroobliga(){
        return this.venseguroobliga;
    }
    
    public void setHomologado(String homologado){
        this.homologado = homologado;
    }
    
    public String getHomologado(){
        return this.homologado;
    }
    
    public void setAtitulo(String atitulo){
        this.atitulo = atitulo;
    }
    
    public String getAtitulo(){
        return this.atitulo;
    }
    
    public void setEmpresaafil(String empresaafil){
        this.empresaafil = empresaafil;
    }
    
    public String getEmpresaafil(){
        return this.empresaafil;
    }
    
    public void setPropietario(String propietario){
        this.propietario = propietario;
    }
    
    public String getPropietario(){
        return this.propietario;
    }
    
    public void setConductor(String conductor){
        this.conductor = conductor;
    }
    
    public String getConductor(){
        return this.conductor;
    }
    
    public void setTenedor(String tenedor){
        this.tenedor = tenedor;
    }
    
    public String getTenedor(){
        return this.tenedor;
    }
    
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    
    public String getTipo(){
        return this.tipo;
    }
    
    public void setTara(String tara){
        this.tara = tara;
    }
    
    public String getTara(){
        return this.tara;
    }
    
    public void setLargo(String largo){
        this.largo = largo;
    }
    
    public String getLargo(){
        return this.largo;
    }
    
    public void setAlto(String alto){
        this.alto = alto;
    }
    
    public String getAlto(){
        return this.alto;
    }
    
    public void setLlantas(String llantas){
        this.llantas = llantas;
    }
    
    public String getLlantas(){
        return this.llantas;
    }
    
    public void setAncho(String ancho){
        this.ancho = ancho;
    }
    
    public String getAncho(){
        return this.ancho;
    }
    
    public void setPiso(String piso){
        this.piso = piso;
    }
    
    public String getPiso(){
        return this.piso;
    }
    
    public void setCargue(String cargue){
        this.cargue = cargue;
    }
    
    public String getCargue(){
        return this.cargue;
    }
    
    public void setVolumen(String volumen){
        this.volumen = volumen;
    }
    
    public String getVolumen(){
        return this.volumen;
    }
    
    public void setFechaultact(String fechaultact){
        this.fechaultact = fechaultact;
    }
    
    public String getFechaultact(){
        return this.fechaultact;
    }
    
    public void setUsuario(String usuario){
        this.usuario = usuario;
    }
    
    public String getUsuario(){
        return this.usuario;
    }
    
    public void setFechacrea(String fechacrea){
        this.fechacrea = fechacrea;
    }
    
    public String getFechacrea(){
        return this.fechacrea;
    }
    
    public void setUsuariocrea(String usuariocrea){
        this.usuariocrea = usuariocrea;
    }
    
    public String getUsuariocrea(){
        return this.usuariocrea;
    }
    
    public void setRecurso(String recurso){
        this.recurso = recurso;
    }
    
    public String getRecurso(){
        return this.recurso;
    }
    
    public void setGrupoid(String grupoid){
        this.grupoid = grupoid;
    }
    
    public String getGrupoid(){
        return this.grupoid;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public String getNombre(){
        return this.nombre;
    }
    
    public void setGrupo(String grupo){
        this.grupo = grupo;
    }
    
    public String getGrupo(){
        return this.grupo;
    }
    
    public void setEstadoequipo(String estadoequipo){
        this.estadoequipo = estadoequipo;
    }
    
    public String getEstadoequipo(){
        return this.estadoequipo;
    }
    
    public void setLocalizacion(String localizacion){
        this.localizacion = localizacion;
    }
    
    public String getLocalizacion(){
        return this.localizacion;
    }
    
    /**
     * Getter for property vencSOAT.
     * @return Value of property vencSOAT.
     */
    public java.sql.Date getVencSOAT() {
        return vencSOAT;
    }
    
    /**
     * Setter for property vencSOAT.
     * @param vencSOAT New value of property vencSOAT.
     */
    public void setVencSOAT(java.sql.Date vencSOAT) {
        this.vencSOAT = vencSOAT;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    //henry 111005
    /**
     * Getter for property ciasoat.
     * @return Value of property ciasoat.
     */
    public java.lang.String getCiasoat() {
        return ciasoat;
    }
    
    /**
     * Setter for property ciasoat.
     * @param ciasoat New value of property ciasoat.
     */
    public void setCiasoat(java.lang.String ciasoat) {
        this.ciasoat = ciasoat;
    }
    
    /**
     * Getter for property placa_trailer.
     * @return Value of property placa_trailer.
     */
    public java.lang.String getPlaca_trailer() {
        return placa_trailer;
    }
    
    /**
     * Setter for property placa_trailer.
     * @param placa_trailer New value of property placa_trailer.
     */
    public void setPlaca_trailer(java.lang.String placa_trailer) {
        this.placa_trailer = placa_trailer;
    }
    
    /**
     * Getter for property certgases.
     * @return Value of property certgases.
     */
    public java.lang.String getCertgases() {
        return certgases;
    }
    
    /**
     * Setter for property certgases.
     * @param certgases New value of property certgases.
     */
    public void setCertgases(java.lang.String certgases) {
        this.certgases = certgases;
    }
    
    /**
     * Getter for property numero_rin.
     * @return Value of property numero_rin.
     */
    public java.lang.String getNumero_rin() {
        return numero_rin;
    }
    
    /**
     * Setter for property numero_rin.
     * @param numero_rin New value of property numero_rin.
     */
    public void setNumero_rin(java.lang.String numero_rin) {
        this.numero_rin = numero_rin;
    }
    
    /**
     * Getter for property grado_riesgo.
     * @return Value of property grado_riesgo.
     */
    public int getGrado_riesgo() {
        return grado_riesgo;
    }
    
    /**
     * Setter for property grado_riesgo.
     * @param grado_riesgo New value of property grado_riesgo.
     */
    public void setGrado_riesgo(java.lang.String grado_riesgo) {
        this.grado_riesgo = Integer.parseInt(grado_riesgo);
    }
    
    /**
     * Getter for property capacidad_trailer.
     * @return Value of property capacidad_trailer.
     */
    public java.lang.String getCapacidad_trailer() {
        return capacidad_trailer;
    }
    
    /**
     * Setter for property capacidad_trailer.
     * @param capacidad_trailer New value of property capacidad_trailer.
     */
    public void setCapacidad_trailer(java.lang.String capacidad_trailer) {
        this.capacidad_trailer = capacidad_trailer;
    }
    
    /**
     * Getter for property reg_nal_carga.
     * @return Value of property reg_nal_carga.
     */
    public java.lang.String getReg_nal_carga() {
        return reg_nal_carga;
    }
    
    /**
     * Setter for property reg_nal_carga.
     * @param reg_nal_carga New value of property reg_nal_carga.
     */
    public void setReg_nal_carga(java.lang.String reg_nal_carga) {
        this.reg_nal_carga = reg_nal_carga;
    }
    
    /**
     * Getter for property fecvenreg.
     * @return Value of property fecvenreg.
     */
    public java.lang.String getFecvenreg() {
        return fecvenreg;
    }
    
    /**
     * Setter for property fecvenreg.
     * @param fecvenreg New value of property fecvenreg.
     */
    public void setFecvenreg(java.lang.String fecvenreg) {
        this.fecvenreg = fecvenreg;
    }
    
    /**
     * Getter for property poliza_andina.
     * @return Value of property poliza_andina.
     */
    public java.lang.String getPoliza_andina() {
        return poliza_andina;
    }
    
    /**
     * Setter for property poliza_andina.
     * @param poliza_andina New value of property poliza_andina.
     */
    public void setPoliza_andina(java.lang.String poliza_andina) {
        this.poliza_andina = poliza_andina;
    }
    
    /**
     * Getter for property fecvenandina.
     * @return Value of property fecvenandina.
     */
    public java.lang.String getFecvenandina() {
        return fecvenandina;
    }
    
    /**
     * Setter for property fecvenandina.
     * @param fecvenandina New value of property fecvenandina.
     */
    public void setFecvenandina(java.lang.String fecvenandina) {
        this.fecvenandina = fecvenandina;
    }
    
    /**
     * Getter for property tarempresa.
     * @return Value of property tarempresa.
     */
    public java.lang.String getTarempresa() {
        return tarempresa;
    }
    
    /**
     * Setter for property tarempresa.
     * @param tarempresa New value of property tarempresa.
     */
    public void setTarempresa(java.lang.String tarempresa) {
        this.tarempresa = tarempresa;
    }
    
    /**
     * Getter for property fecvenempresa.
     * @return Value of property fecvenempresa.
     */
    public java.lang.String getFecvenempresa() {
        return fecvenempresa;
    }
    
    /**
     * Setter for property fecvenempresa.
     * @param fecvenempresa New value of property fecvenempresa.
     */
    public void setFecvenempresa(java.lang.String fecvenempresa) {
        this.fecvenempresa = fecvenempresa;
    }
    
    /**
     * Getter for property tarhabil.
     * @return Value of property tarhabil.
     */
    public java.lang.String getTarhabil() {
        return tarhabil;
    }
    
    /**
     * Setter for property tarhabil.
     * @param tarhabil New value of property tarhabil.
     */
    public void setTarhabil(java.lang.String tarhabil) {
        this.tarhabil = tarhabil;
    }
    
    /**
     * Getter for property fecvenhabil.
     * @return Value of property fecvenhabil.
     */
    public java.lang.String getFecvenhabil() {
        return fecvenhabil;
    }
    
    /**
     * Setter for property fecvenhabil.
     * @param fecvenhabil New value of property fecvenhabil.
     */
    public void setFecvenhabil(java.lang.String fecvenhabil) {
        this.fecvenhabil = fecvenhabil;
    }
    
    /**
     * Getter for property tarprop.
     * @return Value of property tarprop.
     */
    public java.lang.String getTarprop() {
        return tarprop;
    }
    
    /**
     * Setter for property tarprop.
     * @param tarprop New value of property tarprop.
     */
    public void setTarprop(java.lang.String tarprop) {
        this.tarprop = tarprop;
    }
    
    /**
     * Getter for property fecvenprop.
     * @return Value of property fecvenprop.
     */
    public java.lang.String getFecvenprop() {
        return fecvenprop;
    }
    
    /**
     * Setter for property fecvenprop.
     * @param fecvenprop New value of property fecvenprop.
     */
    public void setFecvenprop(java.lang.String fecvenprop) {
        this.fecvenprop = fecvenprop;
    }
    
    /**
     * Getter for property fecvengases.
     * @return Value of property fecvengases.
     */
    public java.lang.String getFecvengases() {
        return fecvengases;
    }
    
    /**
     * Setter for property fecvengases.
     * @param fecvengases New value of property fecvengases.
     */
    public void setFecvengases(java.lang.String fecvengases) {
        this.fecvengases = fecvengases;
    }
    
    /**
     * Getter for property polizasoat.
     * @return Value of property polizasoat.
     */
    public java.lang.String getPolizasoat() {
        return polizasoat;
    }
    
    /**
     * Setter for property polizasoat.
     * @param polizasoat New value of property polizasoat.
     */
    public void setPolizasoat(java.lang.String polizasoat) {
        this.polizasoat = polizasoat;
    }
    
    /**
     * Getter for property vprop.
     * @return Value of property vprop.
     */
    public java.lang.String getVprop() {
        return vprop;
    }
    
    /**
     * Setter for property vprop.
     * @param vprop New value of property vprop.
     */
    public void setVprop(java.lang.String vprop) {
        this.vprop = vprop;
    }
    
    /**
     * Getter for property empAfiliada.
     * @return Value of property empAfiliada.
     */
    public java.lang.String getEmpAfiliada() {
        return empAfiliada;
    }
    
    /**
     * Setter for property empAfiliada.
     * @param empAfiliada New value of property empAfiliada.
     */
    public void setEmpAfiliada(java.lang.String empAfiliada) {
        this.empAfiliada = empAfiliada;
    }
    /**
     * Getter for property paiAfiliada.
     * @return Value of property paiAfiliada.
     */
    public java.lang.String getPaisAfiliada() {
        return paisAfiliada;
    }
    
    /**
     * Setter for property paiAfiliada.
     * @param paiAfiliada New value of property paiAfiliada.
     */
    public void setPaisAfiliada(java.lang.String paisAfiliada) {
        this.paisAfiliada = paisAfiliada;
    }
    
    /**
     * Getter for property ciudadAfiliada.
     * @return Value of property ciudadAfiliada.
     */
    public java.lang.String getCiudadAfiliada() {
        return ciudadAfiliada;
    }
    
    /**
     * Setter for property ciudadAfiliada.
     * @param ciudadAfiliada New value of property ciudadAfiliada.
     */
    public void setCiudadAfiliada(java.lang.String ciudadAfiliada) {
        this.ciudadAfiliada = ciudadAfiliada;
    }
    
    /**
     * Getter for property aprobado.
     * @return Value of property aprobado.
     */
    public java.lang.String getAprobado() {
        return aprobado;
    }
    
    /**
     * Setter for property aprobado.
     * @param aprobado New value of property aprobado.
     */
    public void setAprobado(java.lang.String aprobado) {
        this.aprobado = aprobado;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.lang.String getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(java.lang.String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property vetado.
     * @return Value of property vetado.
     */
    public boolean isVetado() {
        return vetado;
    }
    
    /**
     * Setter for property vetado.
     * @param vetado New value of property vetado.
     */
    public void setVetado(boolean vetado) {
        this.vetado = vetado;
    }
    
    /**
     * Getter for property traile.
     * @return Value of property traile.
     */
    public boolean isTraile() {
        return traile;
    }
    
    /**
     * Setter for property traile.
     * @param traile New value of property traile.
     */
    public void setTraile(boolean traile) {
        this.traile = traile;
    }
    
    /**
     * Getter for property tractomula.
     * @return Value of property tractomula.
     */
    public boolean isTractomula() {
        return tractomula;
    }
    
    /**
     * Setter for property tractomula.
     * @param tractomula New value of property tractomula.
     */
    public void setTractomula(boolean tractomula) {
        this.tractomula = tractomula;
    }
    
    /**
     * Getter for property tieneFoto.
     * @return Value of property tieneFoto.
     */
    public boolean isTieneFoto() {
        return tieneFoto;
    }
    
    /**
     * Setter for property tieneFoto.
     * @param tieneFoto New value of property tieneFoto.
     */
    public void setTieneFoto(boolean tieneFoto) {
        this.tieneFoto = tieneFoto;
    }
    
    /**
     * Getter for property ciudad_tarjeta.
     * @return Value of property ciudad_tarjeta.
     */
    public java.lang.String getCiudad_tarjeta() {
        return ciudad_tarjeta;
    }
    
    /**
     * Setter for property ciudad_tarjeta.
     * @param ciudad_tarjeta New value of property ciudad_tarjeta.
     */
    public void setCiudad_tarjeta(java.lang.String ciudad_tarjeta) {
        this.ciudad_tarjeta = ciudad_tarjeta;
    }
    
    /**
     * Getter for property resp_civil.
     * @return Value of property resp_civil.
     */
    public java.lang.String getResp_civil() {
        return resp_civil;
    }
    
    /**
     * Setter for property resp_civil.
     * @param resp_civil New value of property resp_civil.
     */
    public void setResp_civil(java.lang.String resp_civil) {
        this.resp_civil = resp_civil;
    }
    
    /**
     * Getter for property fecvresp_civil.
     * @return Value of property fecvresp_civil.
     */
    public java.lang.String getFecvresp_civil() {
        return fecvresp_civil;
    }
    
    /**
     * Setter for property fecvresp_civil.
     * @param fecvresp_civil New value of property fecvresp_civil.
     */
    public void setFecvresp_civil(java.lang.String fecvresp_civil) {
        this.fecvresp_civil = fecvresp_civil;
    }
    
    /**
     * Getter for property grupo_equipo.
     * @return Value of property grupo_equipo.
     */
    public java.lang.String getGrupo_equipo() {
        return grupo_equipo;
    }
    
    /**
     * Setter for property grupo_equipo.
     * @param grupo_equipo New value of property grupo_equipo.
     */
    public void setGrupo_equipo(java.lang.String grupo_equipo) {
        this.grupo_equipo = grupo_equipo;
    }
    
    /**
     * Getter for property descuento_equipo.
     * @return Value of property descuento_equipo.
     */
    public java.lang.String getDescuento_equipo() {
        return descuento_equipo;
    }
    
    /**
     * Setter for property descuento_equipo.
     * @param descuento_equipo New value of property descuento_equipo.
     */
    public void setDescuento_equipo(java.lang.String descuento_equipo) {
        this.descuento_equipo = descuento_equipo;
    }
    
    /**
     * Getter for property clasificacion_equipo.
     * @return Value of property clasificacion_equipo.
     */
    public java.lang.String getClasificacion_equipo() {
        return clasificacion_equipo;
    }
    
    /**
     * Setter for property clasificacion_equipo.
     * @param clasificacion_equipo New value of property clasificacion_equipo.
     */
    public void setClasificacion_equipo(java.lang.String clasificacion_equipo) {
        this.clasificacion_equipo = clasificacion_equipo;
    }
    
    /**
     * Getter for property textoEstado.
     * @return Value of property textoEstado.
     */
    public String getTextoEstado() {
        return this.textoEstado;
    }
    
    /**
     * Setter for property textoEstado.
     * @param textoEstado New value of property textoEstado.
     */
    public void setTextoEstado(String textoEstado) {
        this.textoEstado = textoEstado;
    }
    
    /**
     * Getter for property veto.
     * @return Value of property veto.
     */
    public java.lang.String getVeto() {
        return veto;
    }
    
    /**
     * Setter for property veto.
     * @param veto New value of property veto.
     */
    public void setVeto(java.lang.String veto) {
        this.veto = veto;
    }
    
    /**
     * Getter for property estadoEscolta.
     * @return Value of property estadoEscolta.
     */
    public java.lang.String getEstadoEscolta() {
        return estadoEscolta;
    }
    
    /**
     * Setter for property estadoEscolta.
     * @param estadoEscolta New value of property estadoEscolta.
     */
    public void setEstadoEscolta(java.lang.String estadoEscolta) {
        this.estadoEscolta = estadoEscolta;
    }
    
    /**
     * Getter for property operador_gps.
     * @return Value of property operador_gps.
     */
    public java.lang.String getOperador_gps() {
        return operador_gps;
    }
    
    /**
     * Setter for property operador_gps.
     * @param operador_gps New value of property operador_gps.
     */
    public void setOperador_gps(java.lang.String operador_gps) {
        this.operador_gps = operador_gps;
    }
    
    /**
     * Getter for property passwd_seguimiento.
     * @return Value of property passwd_seguimiento.
     */
    public java.lang.String getPasswd_seguimiento() {
        return passwd_seguimiento;
    }
    
    /**
     * Setter for property passwd_seguimiento.
     * @param passwd_seguimiento New value of property passwd_seguimiento.
     */
    public void setPasswd_seguimiento(java.lang.String passwd_seguimiento) {
        this.passwd_seguimiento = passwd_seguimiento;
    }
    
    /**
     * Getter for property tiene_gps.
     * @return Value of property tiene_gps.
     */
    public java.lang.String getTiene_gps() {
        return tiene_gps;
    }
    
    /**
     * Setter for property tiene_gps.
     * @param tiene_gps New value of property tiene_gps.
     */
    public void setTiene_gps(java.lang.String tiene_gps) {
        this.tiene_gps = tiene_gps;
    }
    
    /**
     * Getter for property capac_gal.
     * @return Value of property capac_gal.
     */
    public java.lang.String getCapac_gal() {
        return capac_gal;
    }
    
    /**
     * Setter for property capac_gal.
     * @param capac_gal New value of property capac_gal.
     */
    public void setCapac_gal(java.lang.String capac_gal) {
        this.capac_gal = capac_gal;
    }
    
    /**
     * Getter for property capac_mts.
     * @return Value of property capac_mts.
     */
    public java.lang.String getCapac_mts() {
        return capac_mts;
    }
    
    /**
     * Setter for property capac_mts.
     * @param capac_mts New value of property capac_mts.
     */
    public void setCapac_mts(java.lang.String capac_mts) {
        this.capac_mts = capac_mts;
    }
    
}
