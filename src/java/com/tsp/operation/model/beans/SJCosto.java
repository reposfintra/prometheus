/**
 * Nombre        SJCosto.java
 * Descripci�n   Cost
 * Autor         Mario Fontalvo Solano
 * Fecha         3 de mayo de 2006, 11:02 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.beans;

import java.util.Date;

public class SJCosto {
    
    private String codOrigen;
    private String codDestino;
    private String nomOrigen;
    private String nomDestino;
    
    private String tipoCosto;
    private double valorCosto;
    private String moneda;
    private double tasa;
    
    private String codAgenciaRelDes;
    private String nomAgenciaRelDes;
    
    private String codMasCF;
    private String codMasFT;
    private Date   fechaCreacion;
    
    
    private int indiceSig = -1;
    
    /** Crea una nueva instancia de  SJCosto */
    public SJCosto() {
    }
    
    
    public double getTotalCosto(){
        return (valorCosto * tasa) * ( tipoCosto.equalsIgnoreCase("V")?1:30 );
    }
    
    /**
     * Getter for property codAgenciaRelDes.
     * @return Value of property codAgenciaRelDes.
     */
    public java.lang.String getCodAgenciaRelDes() {
        return codAgenciaRelDes;
    }
    
    /**
     * Setter for property codAgenciaRelDes.
     * @param codAgenciaRelDes New value of property codAgenciaRelDes.
     */
    public void setCodAgenciaRelDes(java.lang.String codAgenciaRelDes) {
        this.codAgenciaRelDes = codAgenciaRelDes;
    }
    
    /**
     * Getter for property codDestino.
     * @return Value of property codDestino.
     */
    public java.lang.String getCodDestino() {
        return codDestino;
    }
    
    /**
     * Setter for property codDestino.
     * @param codDestino New value of property codDestino.
     */
    public void setCodDestino(java.lang.String codDestino) {
        this.codDestino = codDestino;
    }
    
    /**
     * Getter for property codMasCF.
     * @return Value of property codMasCF.
     */
    public java.lang.String getCodMasCF() {
        return codMasCF;
    }
    
    /**
     * Setter for property codMasCF.
     * @param codMasCF New value of property codMasCF.
     */
    public void setCodMasCF(java.lang.String codMasCF) {
        this.codMasCF = codMasCF;
    }
    
    /**
     * Getter for property codMasFT.
     * @return Value of property codMasFT.
     */
    public java.lang.String getCodMasFT() {
        return codMasFT;
    }
    
    /**
     * Setter for property codMasFT.
     * @param codMasFT New value of property codMasFT.
     */
    public void setCodMasFT(java.lang.String codMasFT) {
        this.codMasFT = codMasFT;
    }
    
    /**
     * Getter for property codOrigen.
     * @return Value of property codOrigen.
     */
    public java.lang.String getCodOrigen() {
        return codOrigen;
    }
    
    /**
     * Setter for property codOrigen.
     * @param codOrigen New value of property codOrigen.
     */
    public void setCodOrigen(java.lang.String codOrigen) {
        this.codOrigen = codOrigen;
    }
    
    /**
     * Getter for property fechaCreacion.
     * @return Value of property fechaCreacion.
     */
    public java.util.Date getFechaCreacion() {
        return fechaCreacion;
    }
    
    /**
     * Setter for property fechaCreacion.
     * @param fechaCreacion New value of property fechaCreacion.
     */
    public void setFechaCreacion(java.util.Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property nomAgenciaRelDes.
     * @return Value of property nomAgenciaRelDes.
     */
    public java.lang.String getNomAgenciaRelDes() {
        return nomAgenciaRelDes;
    }
    
    /**
     * Setter for property nomAgenciaRelDes.
     * @param nomAgenciaRelDes New value of property nomAgenciaRelDes.
     */
    public void setNomAgenciaRelDes(java.lang.String nomAgenciaRelDes) {
        this.nomAgenciaRelDes = nomAgenciaRelDes;
    }
    
    /**
     * Getter for property nomDestino.
     * @return Value of property nomDestino.
     */
    public java.lang.String getNomDestino() {
        return nomDestino;
    }
    
    /**
     * Setter for property nomDestino.
     * @param nomDestino New value of property nomDestino.
     */
    public void setNomDestino(java.lang.String nomDestino) {
        this.nomDestino = nomDestino;
    }
    
    /**
     * Getter for property nomOrigen.
     * @return Value of property nomOrigen.
     */
    public java.lang.String getNomOrigen() {
        return nomOrigen;
    }
    
    /**
     * Setter for property nomOrigen.
     * @param nomOrigen New value of property nomOrigen.
     */
    public void setNomOrigen(java.lang.String nomOrigen) {
        this.nomOrigen = nomOrigen;
    }
    
    /**
     * Getter for property tipoCosto.
     * @return Value of property tipoCosto.
     */
    public java.lang.String getTipoCosto() {
        return tipoCosto;
    }
    
    /**
     * Setter for property tipoCosto.
     * @param tipoCosto New value of property tipoCosto.
     */
    public void setTipoCosto(java.lang.String tipoCosto) {
        this.tipoCosto = tipoCosto;
    }
    
    /**
     * Getter for property valorCosto.
     * @return Value of property valorCosto.
     */
    public double getValorCosto() {
        return valorCosto;
    }
    
    /**
     * Setter for property valorCosto.
     * @param valorCosto New value of property valorCosto.
     */
    public void setValorCosto(double valorCosto) {
        this.valorCosto = valorCosto;
    }
    
    /**
     * Getter for property indiceSig.
     * @return Value of property indiceSig.
     */
    public int getIndiceSig() {
        return indiceSig;
    }
    
    /**
     * Setter for property indiceSig.
     * @param indiceSig New value of property indiceSig.
     */
    public void setIndiceSig(int indiceSig) {
        this.indiceSig = indiceSig;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }    
}
