/**
 * Nombre        Amortizacion.java
 * Descripción   Bean de amortizacion
 * Autor         Mario Fontalvo Solano
 * Fecha         9 de febrero de 2006, 09:22 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 **/
package com.tsp.operation.model.beans;


import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * @autor mfontalvo
 */

public class Amortizacion {
    
    public static String FORMAT_DATE = "yyyy-MM-dd";
    public static SimpleDateFormat fmt = new SimpleDateFormat(FORMAT_DATE);
    
    
    
    private int    Cuota;
    private Date   Fecha;
    private double Monto;
    private double Capital;
    private double Interes;
    private double TotalAPagar;
    private double Saldo;
    
    
    private int    idObjeto;
    private String dstrct           = "";
    private String prestamo         = "";
    private String item             = "";
    private String beneficiario     = "";
    private String nameBeneficiario = "";
    private String tercero          = "";
    private String nameTercero      = "";
    private String fechaPago        = "";
    
    private String reg_status       = "";
    private String fechaMigracion   = "0099-01-01 00:00:00";
    private String userMigracion    = "";
    
    private String fechaDescuento    = "0099-01-01";
    private String bancoDescuento    = "";
    private String sucursalDescuento = "";
    private String chequeDescuento   = "";
    private String corridaDescuento  = "";
    private double valorDescuento    = 0;
    private String estadoDescuento   = "";
    private boolean modificarDescuento = false;
    
    private String fechaPagoTercero    = "0099-01-01";
    private String bancoPagoTercero    = "";
    private String sucursalPagoTercero = "";
    private String chequePagoTercero   = "";
    private String corridaPagoTercero  = "";
    private double valorPagoTercero  = 0;
    private String estadoPagoTercero   = "";
    private boolean modificarPagoTercero = false;
    
    private boolean seleccionada = false;
    private String  referencia   = "";
    
    
    private String placa = "";
    
    
    
    /** Crea una nueva instancia de  Amortizacion */
    public Amortizacion() {
    }
    
    /**
     * Getter for property bancoDescuento.
     * @return Value of property bancoDescuento.
     */
    public java.lang.String getBancoDescuento() {
        return bancoDescuento;
    }    
    
    /**
     * Setter for property bancoDescuento.
     * @param bancoDescuento New value of property bancoDescuento.
     */
    public void setBancoDescuento(java.lang.String bancoDescuento) {
        this.bancoDescuento = bancoDescuento;
    }
    
    /**
     * Getter for property bancoPagoTercero.
     * @return Value of property bancoPagoTercero.
     */
    public java.lang.String getBancoPagoTercero() {
        return bancoPagoTercero;
    }
    
    /**
     * Setter for property bancoPagoTercero.
     * @param bancoPagoTercero New value of property bancoPagoTercero.
     */
    public void setBancoPagoTercero(java.lang.String bancoPagoTercero) {
        this.bancoPagoTercero = bancoPagoTercero;
    }
    
    /**
     * Getter for property beneficiario.
     * @return Value of property beneficiario.
     */
    public java.lang.String getBeneficiario() {
        return beneficiario;
    }
    
    /**
     * Setter for property beneficiario.
     * @param beneficiario New value of property beneficiario.
     */
    public void setBeneficiario(java.lang.String beneficiario) {
        this.beneficiario = beneficiario;
    }
    
    /**
     * Getter for property Capital.
     * @return Value of property Capital.
     */
    public double getCapital() {
        return Capital;
    }
    
    /**
     * Setter for property Capital.
     * @param Capital New value of property Capital.
     */
    public void setCapital(double Capital) {
        this.Capital = Capital;
    }
    
    /**
     * Getter for property chequeDescuento.
     * @return Value of property chequeDescuento.
     */
    public java.lang.String getChequeDescuento() {
        return chequeDescuento;
    }
    
    /**
     * Setter for property chequeDescuento.
     * @param chequeDescuento New value of property chequeDescuento.
     */
    public void setChequeDescuento(java.lang.String chequeDescuento) {
        this.chequeDescuento = chequeDescuento;
    }
    
    /**
     * Getter for property chequePagoTercero.
     * @return Value of property chequePagoTercero.
     */
    public java.lang.String getChequePagoTercero() {
        return chequePagoTercero;
    }
    
    /**
     * Setter for property chequePagoTercero.
     * @param chequePagoTercero New value of property chequePagoTercero.
     */
    public void setChequePagoTercero(java.lang.String chequePagoTercero) {
        this.chequePagoTercero = chequePagoTercero;
    }
    
    /**
     * Getter for property corridaDescuento.
     * @return Value of property corridaDescuento.
     */
    public java.lang.String getCorridaDescuento() {
        return corridaDescuento;
    }
    
    /**
     * Setter for property corridaDescuento.
     * @param corridaDescuento New value of property corridaDescuento.
     */
    public void setCorridaDescuento(java.lang.String corridaDescuento) {
        this.corridaDescuento = corridaDescuento;
    }
    
    /**
     * Getter for property corridaPagoTercero.
     * @return Value of property corridaPagoTercero.
     */
    public java.lang.String getCorridaPagoTercero() {
        return corridaPagoTercero;
    }
    
    /**
     * Setter for property corridaPagoTercero.
     * @param corridaPagoTercero New value of property corridaPagoTercero.
     */
    public void setCorridaPagoTercero(java.lang.String corridaPagoTercero) {
        this.corridaPagoTercero = corridaPagoTercero;
    }
    
    /**
     * Getter for property Cuota.
     * @return Value of property Cuota.
     */
    public int getCuota() {
        return Cuota;
    }
    
    /**
     * Setter for property Cuota.
     * @param Cuota New value of property Cuota.
     */
    public void setCuota(int Cuota) {
        this.Cuota = Cuota;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property estadoDesceunto.
     * @return Value of property estadoDesceunto.
     */
    public java.lang.String getEstadoDescuento() {
        return estadoDescuento;
    }
    
    /**
     * Setter for property estadoDesceunto.
     * @param estadoDesceunto New value of property estadoDesceunto.
     */
    public void setEstadoDescuento(java.lang.String estadoDescuento) {
        this.estadoDescuento = estadoDescuento;
    }
    
    /**
     * Getter for property estadoPagoTercero.
     * @return Value of property estadoPagoTercero.
     */
    public java.lang.String getEstadoPagoTercero() {
        return estadoPagoTercero;
    }
    
    /**
     * Setter for property estadoPagoTercero.
     * @param estadoPagoTercero New value of property estadoPagoTercero.
     */
    public void setEstadoPagoTercero(java.lang.String estadoPagoTercero) {
        this.estadoPagoTercero = estadoPagoTercero;
    }
    
    /**
     * Getter for property Fecha.
     * @return Value of property Fecha.
     */
    public java.util.Date getFecha() {
        return Fecha;
    }
    
    /**
     * Setter for property Fecha.
     * @param Fecha New value of property Fecha.
     */
    public void setFecha(java.util.Date Fecha) {
        this.Fecha = Fecha;
    }
    
    /**
     * Getter for property fechaDescuento.
     * @return Value of property fechaDescuento.
     */
    public java.lang.String getFechaDescuento() {
        return fechaDescuento;
    }
    
    /**
     * Setter for property fechaDescuento.
     * @param fechaDescuento New value of property fechaDescuento.
     */
    public void setFechaDescuento(java.lang.String fechaDescuento) {
        this.fechaDescuento = fechaDescuento;
    }
    
    /**
     * Getter for property fechaMigracion.
     * @return Value of property fechaMigracion.
     */
    public java.lang.String getFechaMigracion() {
        return fechaMigracion;
    }
    
    /**
     * Setter for property fechaMigracion.
     * @param fechaMigracion New value of property fechaMigracion.
     */
    public void setFechaMigracion(java.lang.String fechaMigracion) {
        this.fechaMigracion = fechaMigracion;
    }
    
    /**
     * Getter for property fechaPago.
     * @return Value of property fechaPago.
     */
    public java.lang.String getFechaPago() {
        return fechaPago;
    }
    
    /**
     * Setter for property fechaPago.
     * @param fechaPago New value of property fechaPago.
     */
    public void setFechaPago(java.lang.String fechaPago) {
        this.fechaPago = fechaPago;
    }
    
    /**
     * Getter for property fechaPagoTercero.
     * @return Value of property fechaPagoTercero.
     */
    public java.lang.String getFechaPagoTercero() {
        return fechaPagoTercero;
    }
    
    /**
     * Setter for property fechaPagoTercero.
     * @param fechaPagoTercero New value of property fechaPagoTercero.
     */
    public void setFechaPagoTercero(java.lang.String fechaPagoTercero) {
        this.fechaPagoTercero = fechaPagoTercero;
    }

    /**
     * Getter for property idObjeto.
     * @return Value of property idObjeto.
     */
    public int getIdObjeto() {
        return idObjeto;
    }
    
    /**
     * Setter for property idObjeto.
     * @param idObjeto New value of property idObjeto.
     */
    public void setIdObjeto(int idObjeto) {
        this.idObjeto = idObjeto;
    }
    
    /**
     * Getter for property Interes.
     * @return Value of property Interes.
     */
    public double getInteres() {
        return Interes;
    }
    
    /**
     * Setter for property Interes.
     * @param Interes New value of property Interes.
     */
    public void setInteres(double Interes) {
        this.Interes = Interes;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property Monto.
     * @return Value of property Monto.
     */
    public double getMonto() {
        return Monto;
    }
    
    /**
     * Setter for property Monto.
     * @param Monto New value of property Monto.
     */
    public void setMonto(double Monto) {
        this.Monto = Monto;
    }
    
    /**
     * Getter for property nameBeneficiario.
     * @return Value of property nameBeneficiario.
     */
    public java.lang.String getNameBeneficiario() {
        return nameBeneficiario;
    }
    
    /**
     * Setter for property nameBeneficiario.
     * @param nameBeneficiario New value of property nameBeneficiario.
     */
    public void setNameBeneficiario(java.lang.String nameBeneficiario) {
        this.nameBeneficiario = nameBeneficiario;
    }
    
    /**
     * Getter for property nameTercero.
     * @return Value of property nameTercero.
     */
    public java.lang.String getNameTercero() {
        return nameTercero;
    }
    
    /**
     * Setter for property nameTercero.
     * @param nameTercero New value of property nameTercero.
     */
    public void setNameTercero(java.lang.String nameTercero) {
        this.nameTercero = nameTercero;
    }
    
    /**
     * Getter for property prestamo.
     * @return Value of property prestamo.
     */
    public java.lang.String getPrestamo() {
        return prestamo;
    }
    
    /**
     * Setter for property prestamo.
     * @param prestamo New value of property prestamo.
     */
    public void setPrestamo(java.lang.String prestamo) {
        this.prestamo = prestamo;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property Saldo.
     * @return Value of property Saldo.
     */
    public double getSaldo() {
        return Saldo;
    }
    
    /**
     * Setter for property Saldo.
     * @param Saldo New value of property Saldo.
     */
    public void setSaldo(double Saldo) {
        this.Saldo = Saldo;
    }
    
    /**
     * Getter for property seleccionada.
     * @return Value of property seleccionada.
     */
    public boolean isSeleccionada() {
        return seleccionada;
    }
    
    /**
     * Setter for property seleccionada.
     * @param seleccionada New value of property seleccionada.
     */
    public void setSeleccionada(boolean seleccionada) {
        this.seleccionada = seleccionada;
    }
    
    /**
     * Getter for property sucursalDescuento.
     * @return Value of property sucursalDescuento.
     */
    public java.lang.String getSucursalDescuento() {
        return sucursalDescuento;
    }
    
    /**
     * Setter for property sucursalDescuento.
     * @param sucursalDescuento New value of property sucursalDescuento.
     */
    public void setSucursalDescuento(java.lang.String sucursalDescuento) {
        this.sucursalDescuento = sucursalDescuento;
    }
    
    /**
     * Getter for property sucursalPagoTercero.
     * @return Value of property sucursalPagoTercero.
     */
    public java.lang.String getSucursalPagoTercero() {
        return sucursalPagoTercero;
    }
    
    /**
     * Setter for property sucursalPagoTercero.
     * @param sucursalPagoTercero New value of property sucursalPagoTercero.
     */
    public void setSucursalPagoTercero(java.lang.String sucursalPagoTercero) {
        this.sucursalPagoTercero = sucursalPagoTercero;
    }
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }
    
    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }
    
    /**
     * Getter for property TotalAPagar.
     * @return Value of property TotalAPagar.
     */
    public double getTotalAPagar() {
        return TotalAPagar;
    }
    
    /**
     * Setter for property TotalAPagar.
     * @param TotalAPagar New value of property TotalAPagar.
     */
    public void setTotalAPagar(double TotalAPagar) {
        this.TotalAPagar = TotalAPagar;
    }
    
    /**
     * Getter for property userMigracion.
     * @return Value of property userMigracion.
     */
    public java.lang.String getUserMigracion() {
        return userMigracion;
    }
    
    /**
     * Setter for property userMigracion.
     * @param userMigracion New value of property userMigracion.
     */
    public void setUserMigracion(java.lang.String userMigracion) {
        this.userMigracion = userMigracion;
    }
    
    /**
     * Getter for property valorDescuento.
     * @return Value of property valorDescuento.
     */
    public double getValorDescuento() {
        return valorDescuento;
    }
    
    /**
     * Setter for property valorDescuento.
     * @param valorDescuento New value of property valorDescuento.
     */
    public void setValorDescuento(double valorDescuento) {
        this.valorDescuento = valorDescuento;
    }
    
    /**
     * Getter for property valorPagadoTercero.
     * @return Value of property valorPagadoTercero.
     */
    public double getValorPagoTercero() {
        return valorPagoTercero;
    }
    
    /**
     * Setter for property valorPagadoTercero.
     * @param valorPagadoTercero New value of property valorPagadoTercero.
     */
    public void setValorPagoTercero(double valorPagoTercero) {
        this.valorPagoTercero = valorPagoTercero;
    }
    
    /**
     * Getter for property modificarDescuento.
     * @return Value of property modificarDescuento.
     */
    public boolean isModificarDescuento() {
        return modificarDescuento;
    }
    
    /**
     * Setter for property modificarDescuento.
     * @param modificarDescuento New value of property modificarDescuento.
     */
    public void setModificarDescuento(boolean modificarDescuento) {
        this.modificarDescuento = modificarDescuento;
    }
    
    /**
     * Getter for property modificarPagoTercero.
     * @return Value of property modificarPagoTercero.
     */
    public boolean isModificarPagoTercero() {
        return modificarPagoTercero;
    }
    
    /**
     * Setter for property modificarPagoTercero.
     * @param modificarPagoTercero New value of property modificarPagoTercero.
     */
    public void setModificarPagoTercero(boolean modificarPagoTercero) {
        this.modificarPagoTercero = modificarPagoTercero;
    }
    
    
    
    public String getDescripcionEstado( int tipo ){
        String estado = ( tipo ==0 ? estadoDescuento : estadoPagoTercero );
        String retorno = "";
        if (estado.equals(""))
            retorno = "No existe en MIMS";
        if (estado.equals("40") || estado.equals("30"))
            retorno = "Pendiente";
        else if (estado.equals("50") )
            retorno = "Pagado";
        return retorno;
    }
    
    /**
     * Getter for property referencia.
     * @return Value of property referencia.
     */
    public java.lang.String getReferencia() {
        return referencia;
    }
    
    /**
     * Setter for property referencia.
     * @param referencia New value of property referencia.
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
}
