/**************************************************************************
 * Nombre: ......................Actividad.java                           *
 * Descripci�n: .................Beans de acuerdo especial.               *
 * Autor:........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:........................24 de agosto de 2005, 03:14 PM           *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class Instrucciones_Despacho implements Serializable {
    private int desp;
    private String  zona;
    private String ciudad;
    private String modelo;
    private int order_qty;
    private double total_volumen;
    private String order_no;
    private String ship_to_name;
    private String expr1;
    private String bodega;
    private String status;
    private String order_date;
    private String expr2;
    private String expire_date;
    private double net_amt_txn;
    private double goods_amt_txn;
    private double dc_amt1_txn;
    private String ref_reg_code;
    private String remark;
    private String addr1;
    private String loc;
    private String appoint_date;
    private String appoint_time;
    private String remark2;
    private String inst_especiales;
    private String item;
    private String factura;
    private String fecha_factura;
    private String numrem;
    private String reg_status;
    private String usuario;
    private String base;
    private String codcli;
    private int cant_despachada;
    private String causa;
    private String desc_causa;
    private String dstrct;
    
    //PARA EL REPORTE
    private double cub_unit;
    private String observacion;
    private String fecha_cargue;
    private String numpla;
    private String numplaRex;
    private String fechaDespRex;
    private String fechaDesp;
    private String placa;
    private String conductor;
    private String fechaEntrega;
    private double cantida_entregada;
    private double cantida_novedad;
    private String observacion_novedad;
    private String codigo_novedad;
    private String motivo_novedad;
    
    //REPORTE NOVEDAD
    private String fecha_novedad;
    private String ubicacion_mercancia;
    private String solucion_novedad;
    private String nom_solucion_novedad;
    private String fecha_solucion;
    private String mes;
    private String fecha_devolucion;
    private String observacion_devolucion;
    
    private String descripcionMaterial;
    private double cantida_entrada;
    private double cantida_salida;
    private double cantida_saldo;
    private String unidad;
    private String documento;
    private String cod_ubicacion;
    
    private String tipo_doc;
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa() {
        return causa;
    }
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.lang.String causa) {
        this.causa = causa;
    }
    
    /**
     * Getter for property desp.
     * @return Value of property desp.
     */
    public int getDesp() {
        return desp;
    }
    
    /**
     * Setter for property desp.
     * @param desp New value of property desp.
     */
    public void setDesp(int desp) {
        this.desp = desp;
    }
    
    /**
     * Getter for property addr1.
     * @return Value of property addr1.
     */
    public java.lang.String getAddr1() {
        return addr1;
    }
    
    /**
     * Setter for property addr1.
     * @param addr1 New value of property addr1.
     */
    public void setAddr1(java.lang.String addr1) {
        this.addr1 = addr1;
    }
    
    /**
     * Getter for property appoint_date.
     * @return Value of property appoint_date.
     */
    public java.lang.String getAppoint_date() {
        return appoint_date;
    }
    
    /**
     * Setter for property appoint_date.
     * @param appoint_date New value of property appoint_date.
     */
    public void setAppoint_date(java.lang.String appoint_date) {
        this.appoint_date = appoint_date;
    }
    
    /**
     * Getter for property appoint_time.
     * @return Value of property appoint_time.
     */
    public java.lang.String getAppoint_time() {
        return appoint_time;
    }
    
    /**
     * Setter for property appoint_time.
     * @param appoint_time New value of property appoint_time.
     */
    public void setAppoint_time(java.lang.String appoint_time) {
        this.appoint_time = appoint_time;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property bodega.
     * @return Value of property bodega.
     */
    public java.lang.String getBodega() {
        return bodega;
    }
    
    /**
     * Setter for property bodega.
     * @param bodega New value of property bodega.
     */
    public void setBodega(java.lang.String bodega) {
        this.bodega = bodega;
    }
    
    /**
     * Getter for property cant_despachada.
     * @return Value of property cant_despachada.
     */
    public int getCant_despachada() {
        return cant_despachada;
    }
    
    /**
     * Setter for property cant_despachada.
     * @param cant_despachada New value of property cant_despachada.
     */
    public void setCant_despachada(int cant_despachada) {
        this.cant_despachada = cant_despachada;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property dc_amt1_txn.
     * @return Value of property dc_amt1_txn.
     */
    public double getDc_amt1_txn() {
        return dc_amt1_txn;
    }
    
    /**
     * Setter for property dc_amt1_txn.
     * @param dc_amt1_txn New value of property dc_amt1_txn.
     */
    public void setDc_amt1_txn(double dc_amt1_txn) {
        this.dc_amt1_txn = dc_amt1_txn;
    }
    
    /**
     * Getter for property expire_date.
     * @return Value of property expire_date.
     */
    public java.lang.String getExpire_date() {
        return expire_date;
    }
    
    /**
     * Setter for property expire_date.
     * @param expire_date New value of property expire_date.
     */
    public void setExpire_date(java.lang.String expire_date) {
        this.expire_date = expire_date;
    }
    
    /**
     * Getter for property expr1.
     * @return Value of property expr1.
     */
    public java.lang.String getExpr1() {
        return expr1;
    }
    
    /**
     * Setter for property expr1.
     * @param expr1 New value of property expr1.
     */
    public void setExpr1(java.lang.String expr1) {
        this.expr1 = expr1;
    }
    
    /**
     * Getter for property expr2.
     * @return Value of property expr2.
     */
    public java.lang.String getExpr2() {
        return expr2;
    }
    
    /**
     * Setter for property expr2.
     * @param expr2 New value of property expr2.
     */
    public void setExpr2(java.lang.String expr2) {
        this.expr2 = expr2;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property fecha_factura.
     * @return Value of property fecha_factura.
     */
    public java.lang.String getFecha_factura() {
        return fecha_factura;
    }
    
    /**
     * Setter for property fecha_factura.
     * @param fecha_factura New value of property fecha_factura.
     */
    public void setFecha_factura(java.lang.String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }
    
    /**
     * Getter for property goods_amt_txn.
     * @return Value of property goods_amt_txn.
     */
    public double getGoods_amt_txn() {
        return goods_amt_txn;
    }
    
    /**
     * Setter for property goods_amt_txn.
     * @param goods_amt_txn New value of property goods_amt_txn.
     */
    public void setGoods_amt_txn(double goods_amt_txn) {
        this.goods_amt_txn = goods_amt_txn;
    }
    
    /**
     * Getter for property inst_especiales.
     * @return Value of property inst_especiales.
     */
    public java.lang.String getInst_especiales() {
        return inst_especiales;
    }
    
    /**
     * Setter for property inst_especiales.
     * @param inst_especiales New value of property inst_especiales.
     */
    public void setInst_especiales(java.lang.String inst_especiales) {
        this.inst_especiales = inst_especiales;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property loc.
     * @return Value of property loc.
     */
    public java.lang.String getLoc() {
        return loc;
    }
    
    /**
     * Setter for property loc.
     * @param loc New value of property loc.
     */
    public void setLoc(java.lang.String loc) {
        this.loc = loc;
    }
    
    /**
     * Getter for property net_amt_txn.
     * @return Value of property net_amt_txn.
     */
    public double getNet_amt_txn() {
        return net_amt_txn;
    }
    
    /**
     * Setter for property net_amt_txn.
     * @param net_amt_txn New value of property net_amt_txn.
     */
    public void setNet_amt_txn(double net_amt_txn) {
        this.net_amt_txn = net_amt_txn;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property odelo.
     * @return Value of property odelo.
     */
    public java.lang.String getModelo() {
        return modelo;
    }
    
    /**
     * Setter for property odelo.
     * @param odelo New value of property odelo.
     */
    public void setModelo(java.lang.String modelo) {
        this.modelo = modelo;
    }
    
    /**
     * Getter for property order_date.
     * @return Value of property order_date.
     */
    public java.lang.String getOrder_date() {
        return order_date;
    }
    
    /**
     * Setter for property order_date.
     * @param order_date New value of property order_date.
     */
    public void setOrder_date(java.lang.String order_date) {
        this.order_date = order_date;
    }
    
    /**
     * Getter for property order_no.
     * @return Value of property order_no.
     */
    public java.lang.String getOrder_no() {
        return order_no;
    }
    
    /**
     * Setter for property order_no.
     * @param order_no New value of property order_no.
     */
    public void setOrder_no(java.lang.String order_no) {
        this.order_no = order_no;
    }
    
    /**
     * Getter for property order_qty.
     * @return Value of property order_qty.
     */
    public int getOrder_qty() {
        return order_qty;
    }
    
    /**
     * Setter for property order_qty.
     * @param order_qty New value of property order_qty.
     */
    public void setOrder_qty(int order_qty) {
        this.order_qty = order_qty;
    }
    
    /**
     * Getter for property ref_reg_code.
     * @return Value of property ref_reg_code.
     */
    public java.lang.String getRef_reg_code() {
        return ref_reg_code;
    }
    
    /**
     * Setter for property ref_reg_code.
     * @param ref_reg_code New value of property ref_reg_code.
     */
    public void setRef_reg_code(java.lang.String ref_reg_code) {
        this.ref_reg_code = ref_reg_code;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property remark.
     * @return Value of property remark.
     */
    public java.lang.String getRemark() {
        return remark;
    }
    
    /**
     * Setter for property remark.
     * @param remark New value of property remark.
     */
    public void setRemark(java.lang.String remark) {
        this.remark = remark;
    }
    
    /**
     * Getter for property remark2.
     * @return Value of property remark2.
     */
    public java.lang.String getRemark2() {
        return remark2;
    }
    
    /**
     * Setter for property remark2.
     * @param remark2 New value of property remark2.
     */
    public void setRemark2(java.lang.String remark2) {
        this.remark2 = remark2;
    }
    
    /**
     * Getter for property ship_to_name.
     * @return Value of property ship_to_name.
     */
    public java.lang.String getShip_to_name() {
        return ship_to_name;
    }
    
    /**
     * Setter for property ship_to_name.
     * @param ship_to_name New value of property ship_to_name.
     */
    public void setShip_to_name(java.lang.String ship_to_name) {
        this.ship_to_name = ship_to_name;
    }
    
    /**
     * Getter for property status.
     * @return Value of property status.
     */
    public java.lang.String getStatus() {
        return status;
    }
    
    /**
     * Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }
    
    /**
     * Getter for property total_volumen.
     * @return Value of property total_volumen.
     */
    public double getTotal_volumen() {
        return total_volumen;
    }
    
    /**
     * Setter for property total_volumen.
     * @param total_volumen New value of property total_volumen.
     */
    public void setTotal_volumen(double total_volumen) {
        this.total_volumen = total_volumen;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property desc_causa.
     * @return Value of property desc_causa.
     */
    public java.lang.String getDesc_causa() {
        return desc_causa;
    }
    
    /**
     * Setter for property desc_causa.
     * @param desc_causa New value of property desc_causa.
     */
    public void setDesc_causa(java.lang.String desc_causa) {
        this.desc_causa = desc_causa;
    }
    
    /**
     * Getter for property cub_unit.
     * @return Value of property cub_unit.
     */
    public double getCub_unit() {
        return cub_unit;
    }
    
    /**
     * Setter for property cub_unit.
     * @param cub_unit New value of property cub_unit.
     */
    public void setCub_unit(double cub_unit) {
        this.cub_unit = cub_unit;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property fecha_cargue.
     * @return Value of property fecha_cargue.
     */
    public java.lang.String getFecha_cargue() {
        return fecha_cargue;
    }
    
    /**
     * Setter for property fecha_cargue.
     * @param fecha_cargue New value of property fecha_cargue.
     */
    public void setFecha_cargue(java.lang.String fecha_cargue) {
        this.fecha_cargue = fecha_cargue;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numplaRex.
     * @return Value of property numplaRex.
     */
    public java.lang.String getNumplaRex() {
        return numplaRex;
    }
    
    /**
     * Setter for property numplaRex.
     * @param numplaRex New value of property numplaRex.
     */
    public void setNumplaRex(java.lang.String numplaRex) {
        this.numplaRex = numplaRex;
    }
    
    /**
     * Getter for property fechaDespRex.
     * @return Value of property fechaDespRex.
     */
    public java.lang.String getFechaDespRex() {
        return fechaDespRex;
    }
    
    /**
     * Setter for property fechaDespRex.
     * @param fechaDespRex New value of property fechaDespRex.
     */
    public void setFechaDespRex(java.lang.String fechaDespRex) {
        this.fechaDespRex = fechaDespRex;
    }
    
    /**
     * Getter for property fechaDesp.
     * @return Value of property fechaDesp.
     */
    public java.lang.String getFechaDesp() {
        return fechaDesp;
    }
    
    /**
     * Setter for property fechaDesp.
     * @param fechaDesp New value of property fechaDesp.
     */
    public void setFechaDesp(java.lang.String fechaDesp) {
        this.fechaDesp = fechaDesp;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property fechaEntrega.
     * @return Value of property fechaEntrega.
     */
    public java.lang.String getFechaEntrega() {
        return fechaEntrega;
    }
    
    /**
     * Setter for property fechaEntrega.
     * @param fechaEntrega New value of property fechaEntrega.
     */
    public void setFechaEntrega(java.lang.String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }
    
    /**
     * Getter for property cantida_entregada.
     * @return Value of property cantida_entregada.
     */
    public double getCantida_entregada() {
        return cantida_entregada;
    }
    
    /**
     * Setter for property cantida_entregada.
     * @param cantida_entregada New value of property cantida_entregada.
     */
    public void setCantida_entregada(double cantida_entregada) {
        this.cantida_entregada = cantida_entregada;
    }
    
    /**
     * Getter for property cantida_novedad.
     * @return Value of property cantida_novedad.
     */
    public double getCantida_novedad() {
        return cantida_novedad;
    }
    
    /**
     * Setter for property cantida_novedad.
     * @param cantida_novedad New value of property cantida_novedad.
     */
    public void setCantida_novedad(double cantida_novedad) {
        this.cantida_novedad = cantida_novedad;
    }
    
    /**
     * Getter for property observacion_novedad.
     * @return Value of property observacion_novedad.
     */
    public java.lang.String getObservacion_novedad() {
        return observacion_novedad;
    }
    
    /**
     * Setter for property observacion_novedad.
     * @param observacion_novedad New value of property observacion_novedad.
     */
    public void setObservacion_novedad(java.lang.String observacion_novedad) {
        this.observacion_novedad = observacion_novedad;
    }
    
    /**
     * Getter for property codigo_novedad.
     * @return Value of property codigo_novedad.
     */
    public java.lang.String getCodigo_novedad() {
        return codigo_novedad;
    }
    
    /**
     * Setter for property codigo_novedad.
     * @param codigo_novedad New value of property codigo_novedad.
     */
    public void setCodigo_novedad(java.lang.String codigo_novedad) {
        this.codigo_novedad = codigo_novedad;
    }
    
    /**
     * Getter for property motivo_novedad.
     * @return Value of property motivo_novedad.
     */
    public java.lang.String getMotivo_novedad() {
        return motivo_novedad;
    }
    
    /**
     * Setter for property motivo_novedad.
     * @param motivo_novedad New value of property motivo_novedad.
     */
    public void setMotivo_novedad(java.lang.String motivo_novedad) {
        this.motivo_novedad = motivo_novedad;
    }
    
    /**
     * Getter for property fecha_novedad.
     * @return Value of property fecha_novedad.
     */
    public java.lang.String getFecha_novedad() {
        return fecha_novedad;
    }
    
    /**
     * Setter for property fecha_novedad.
     * @param fecha_novedad New value of property fecha_novedad.
     */
    public void setFecha_novedad(java.lang.String fecha_novedad) {
        this.fecha_novedad = fecha_novedad;
    }
    
    /**
     * Getter for property ubicacion_mercancia.
     * @return Value of property ubicacion_mercancia.
     */
    public java.lang.String getUbicacion_mercancia() {
        return ubicacion_mercancia;
    }
    
    /**
     * Setter for property ubicacion_mercancia.
     * @param ubicacion_mercancia New value of property ubicacion_mercancia.
     */
    public void setUbicacion_mercancia(java.lang.String ubicacion_mercancia) {
        this.ubicacion_mercancia = ubicacion_mercancia;
    }
    
    /**
     * Getter for property solucion_novedad.
     * @return Value of property solucion_novedad.
     */
    public java.lang.String getSolucion_novedad() {
        return solucion_novedad;
    }
    
    /**
     * Setter for property solucion_novedad.
     * @param solucion_novedad New value of property solucion_novedad.
     */
    public void setSolucion_novedad(java.lang.String solucion_novedad) {
        this.solucion_novedad = solucion_novedad;
    }
    
    /**
     * Getter for property fecha_solucion.
     * @return Value of property fecha_solucion.
     */
    public java.lang.String getFecha_solucion() {
        return fecha_solucion;
    }
    
    /**
     * Setter for property fecha_solucion.
     * @param fecha_solucion New value of property fecha_solucion.
     */
    public void setFecha_solucion(java.lang.String fecha_solucion) {
        this.fecha_solucion = fecha_solucion;
    }
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property nom_solucion_novedad.
     * @return Value of property nom_solucion_novedad.
     */
    public java.lang.String getNom_solucion_novedad() {
        return nom_solucion_novedad;
    }
    
    /**
     * Setter for property nom_solucion_novedad.
     * @param nom_solucion_novedad New value of property nom_solucion_novedad.
     */
    public void setNom_solucion_novedad(java.lang.String nom_solucion_novedad) {
        this.nom_solucion_novedad = nom_solucion_novedad;
    }
    
    /**
     * Getter for property fecha_devolucion.
     * @return Value of property fecha_devolucion.
     */
    public java.lang.String getFecha_devolucion() {
        return fecha_devolucion;
    }
    
    /**
     * Setter for property fecha_devolucion.
     * @param fecha_devolucion New value of property fecha_devolucion.
     */
    public void setFecha_devolucion(java.lang.String fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }
    
    /**
     * Getter for property observacion_devolucion.
     * @return Value of property observacion_devolucion.
     */
    public java.lang.String getObservacion_devolucion() {
        return observacion_devolucion;
    }
    
    /**
     * Setter for property observacion_devolucion.
     * @param observacion_devolucion New value of property observacion_devolucion.
     */
    public void setObservacion_devolucion(java.lang.String observacion_devolucion) {
        this.observacion_devolucion = observacion_devolucion;
    }
    
    /**
     * Getter for property descripcionMaterial.
     * @return Value of property descripcionMaterial.
     */
    public java.lang.String getDescripcionMaterial() {
        return descripcionMaterial;
    }
    
    /**
     * Setter for property descripcionMaterial.
     * @param descripcionMaterial New value of property descripcionMaterial.
     */
    public void setDescripcionMaterial(java.lang.String descripcionMaterial) {
        this.descripcionMaterial = descripcionMaterial;
    }
    
    /**
     * Getter for property cantida_entrada.
     * @return Value of property cantida_entrada.
     */
    public double getCantida_entrada() {
        return cantida_entrada;
    }
    
    /**
     * Setter for property cantida_entrada.
     * @param cantida_entrada New value of property cantida_entrada.
     */
    public void setCantida_entrada(double cantida_entrada) {
        this.cantida_entrada = cantida_entrada;
    }
    
    /**
     * Getter for property cantida_salida.
     * @return Value of property cantida_salida.
     */
    public double getCantida_salida() {
        return cantida_salida;
    }
    
    /**
     * Setter for property cantida_salida.
     * @param cantida_salida New value of property cantida_salida.
     */
    public void setCantida_salida(double cantida_salida) {
        this.cantida_salida = cantida_salida;
    }
    
    /**
     * Getter for property cantida_saldo.
     * @return Value of property cantida_saldo.
     */
    public double getCantida_saldo() {
        return cantida_saldo;
    }
    
    /**
     * Setter for property cantida_saldo.
     * @param cantida_saldo New value of property cantida_saldo.
     */
    public void setCantida_saldo(double cantida_saldo) {
        this.cantida_saldo = cantida_saldo;
    }
    
    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property cod_ubicacion.
     * @return Value of property cod_ubicacion.
     */
    public java.lang.String getCod_ubicacion() {
        return cod_ubicacion;
    }
    
    /**
     * Setter for property cod_ubicacion.
     * @param cod_ubicacion New value of property cod_ubicacion.
     */
    public void setCod_ubicacion(java.lang.String cod_ubicacion) {
        this.cod_ubicacion = cod_ubicacion;
    }
    
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc() {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc(java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
}
