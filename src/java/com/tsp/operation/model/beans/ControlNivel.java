/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;


import java.io.Serializable;


/**
 * Clase que almacena los registros de tipo Sx.
 * nivel Indica el numero x del nivel
 * inicio_nivel Indica la fila en excell donde comienza el grupo Sx
 * final_nivel Indica la fila donde termina el grupo Sx
 * niveles_previos_cerrados Indentifica si los niveles previos a este registro ya fueron cerrados.
 * Es decir, si ya se les identifico su final de grupo
 * formula_suma  Identifica una formula que suma el detalle del grupo
 *
 * @author  Alvaro Pabon Martinez
 * @version %I%, %G%
 * @since   1.0
 *
 */
public class ControlNivel implements Serializable{


    private int nivel;
    private int inicio_nivel;
    private int final_nivel;
    private boolean niveles_previos_cerrados;
    private String formula_suma;


    public ControlNivel (){
        nivel = 0;
        inicio_nivel = 0;
        final_nivel = 0;
        niveles_previos_cerrados = false;
        formula_suma = "";


    }

    public ControlNivel (int nivel, int inicio_nivel, int final_nivel){

        this.nivel = nivel;
        this.inicio_nivel = inicio_nivel;
        this.final_nivel = final_nivel;
        this.niveles_previos_cerrados = false;
        this.formula_suma = "";


    }

    public void  setControlNivel(int nivel, int inicio_nivel, int final_nivel, boolean niveles_previos_cerrados ){
        this.nivel = nivel;
        this.inicio_nivel = inicio_nivel;
        this.final_nivel = final_nivel;
        this.setNiveles_previos_cerrados(niveles_previos_cerrados);
    }


    /**
     * @return the nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * @param nivel the nivel to set
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    /**
     * @return the inicio_nivel
     */
    public int getInicio_nivel() {
        return inicio_nivel;
    }

    /**
     * @param inicio_nivel the inicio_nivel to set
     */
    public void setInicio_nivel(int inicio_nivel) {
        this.inicio_nivel = inicio_nivel;
    }

    /**
     * @return the final_nivel
     */
    public int getFinal_nivel() {
        return final_nivel;
    }

    /**
     * @param final_nivel the final_nivel to set
     */
    public void setFinal_nivel(int final_nivel) {
        this.final_nivel = final_nivel;
    }

    /**
     * @return the niveles_previos_cerrados
     */
    public boolean isNiveles_previos_cerrados() {
        return niveles_previos_cerrados;
    }

    /**
     * @param niveles_previos_cerrados the niveles_previos_cerrados to set
     */
    public void setNiveles_previos_cerrados(boolean niveles_previos_cerrados) {
        this.niveles_previos_cerrados = niveles_previos_cerrados;
    }

    /**
     * @return the formula_suma
     */
    public String getFormula_suma() {
        return formula_suma;
    }

    /**
     * @param formula_suma the formula_suma to set
     */
    public void setFormula_suma(String formula_suma) {
        this.formula_suma = formula_suma;
    }
}










