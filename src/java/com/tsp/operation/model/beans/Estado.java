/*
 * Estado.java
 *
 * Created on 1 de marzo de 2005, 11:49 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;


/**
 *
 * @author  DIBASMO
 */
public class Estado implements Serializable {
    private String departament_code;
    private String departament_name;
    private String pais_code;
    private String pais_name;
    private String zona;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    
    /** Creates a new instance of Estado */
    public static Estado load(ResultSet rs)throws SQLException { 
        Estado estado = new Estado(); 
        estado.setdepartament_code( rs.getString( "departament_code") );
        estado.setdepartament_name( rs.getString("departament_name" ) );
        estado.setpais_code( rs.getString("country_code") );
        estado.setpais_name( rs.getString("country_name") );
        estado.setzona( rs.getString("zona") );  
        estado.setRec_status( rs.getString("rec_status") );
        estado.setLast_update( rs.getDate("last_update") );
        estado.setUser_update( rs.getString("user_update") );
        estado.setCreation_date( rs.getDate("creation_date") );
        estado.setCreation_user( rs.getString("creation_user") );
        return estado;
        
    }
     
    //=====================================================
    //              Metodos de acceso 
    //=====================================================
    public void setdepartament_code (String departament_code) {
        
        this.departament_code = departament_code;
        
    }
    public void setdepartament_name (String departament_name) {
        
        this.departament_name = departament_name;
        
    }
    public void setpais_code (String pais_code) {
        
        this.pais_code = pais_code;
        
    }
    public void setpais_name (String pais_name) {
        
        this.pais_name = pais_name;
        
    }
    public void setzona (String zona) {
        
        this.zona = zona;
        
    }
    public void setRec_status(String rec_status ){       
     
        this.rec_status = rec_status;       
    }
    public void setLast_update(java.util.Date last_update){
        
        this.last_update = last_update;
        
    }
    public void setUser_update(String user_update){        
        this.user_update= user_update;        
    }
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date = creation_date;
        
    }
    public void setCreation_user(String creation_user){        
       
        this.creation_user= creation_user;        
        
    }
    
    public String getdepartament_code ( ) {
        
       return departament_code;
        
    }
    public String getdepartament_name () {
        
        return departament_name;
        
    }
    public String getpais_code () {
        
        return pais_code;
        
    }
    public String getpais_name () {
        
        return pais_name;
        
    }
    public String getzona () {
        
        return zona;
        
    }
    public String getRec_status(){       
     
        return rec_status;       
    }
    public java.util.Date getLast_update( ){
        
        return last_update;
        
    }
    public String getUser_update( ){        
        
        return user_update;        
        
    }
    public java.util.Date getCreation_date( ){
      
        return creation_date;
        
    }    
    public String getCreation_user( ){        
        return creation_user;        
    }
    
}
