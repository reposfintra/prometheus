package com.tsp.operation.model.beans;

/**
 * Bean para almacenar el codigo de respuesta del webservice historia de credito
 * 29/11/2011
 * @author darrieta
 */
public class RespuestaSuperfil {
    
    private String codigoRespuesta;
    private int score = -1;
    private String clasificacion;

    /**
     * Get the value of score
     *
     * @return the value of score
     */
    public int getScore() {
        return score;
    }

    /**
     * Set the value of score
     *
     * @param score new value of score
     */
    public void setScore(int score) {
        this.score = score;
    }


    /**
     * Get the value of codigoRespuesta
     *
     * @return the value of codigoRespuesta
     */
    public String getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Set the value of codigoRespuesta
     *
     * @param codigoRespuesta new value of codigoRespuesta
     */
    public void setCodigoRespuesta(String codigoRespuesta) {
        this.codigoRespuesta = codigoRespuesta;
    }
    
    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
}
