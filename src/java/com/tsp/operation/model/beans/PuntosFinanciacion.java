/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class PuntosFinanciacion {

    private String clase_dtf;
    private double puntos;
    private String tipo;


    /** Creates a new instance of PuntosFinanciacion */
    public PuntosFinanciacion() {
    }


    public static PuntosFinanciacion load(java.sql.ResultSet rs)throws java.sql.SQLException{

        PuntosFinanciacion puntos = new PuntosFinanciacion();
        puntos.setClase_dtf(rs.getString("clase_dtf"));
        puntos.setPuntos( rs.getDouble("puntos") );
        puntos.setTipo( rs.getString("tipo") );
        System.out.println("puntos.getClase_dtf()"+puntos.getClase_dtf()+"puntos"+puntos.getPuntos()+"tipo"+puntos.getTipo());
        return puntos;
    }


    /**
     * @return the puntos
     */
    public double getPuntos() {
        return puntos;
    }

    /**
     * @param puntos the puntos to set
     */
    public void setPuntos(double puntos) {
        this.puntos = puntos;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the clase_dtf
     */
    public String getClase_dtf() {
        return clase_dtf;
    }

    /**
     * @param clase_dtf the clase_dtf to set
     */
    public void setClase_dtf(String clase_dtf) {
        this.clase_dtf = clase_dtf;
    }

}
