/*
 * remesa_docto.java
 *
 * Created on 12 de septiembre de 2005, 10:35 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Administrador
 */
public class remesa_docto {
    private String reg_status;
    private String dstrct;
    private String numrem;
    private String tipo_doc;
    private String documento;
    private String tipo_doc_rel;
    private String documento_rel;
    private String destinatario;
    private String recibido_documento;
    private String user_recibio_documento;
    private String fecha_recibido;
    private String recibido_documento_rel;
    private String user_recibio_documento_rel;
    private String fecha_recibido_rel;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    
    //TIPOS DE DOCUMENTOS
    private String document_type;
    private String document_name;
    private Vector hijos;
    private String cadena;
    
    //Diogenes
    private String document_rel_name;
    private String ciudad_destinatario;
    
    private String fecha_sia;
    private String fecha_eta;
    private String descripcion_cga;
    private String importacion;
    private String codcli;
    private String exportacion;
    
        // LREALES - MODIFICADO 5 DICIEMBRE 2006
    private String usuario_genero;
    private String fecha_genero;
    private String fecha_llegada_cdr;
    private String fecha_salida_cdr;
    //
    
    
    /** Creates a new instance of remesa_docto */
    public remesa_docto() {
        hijos = new Vector();
    }
    
    public static remesa_docto load(ResultSet rs)throws Exception{
        remesa_docto datos = new remesa_docto();
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setNumrem(rs.getString("NUMREM"));
        datos.setTipo_doc(rs.getString("TIPO_DOC"));
        datos.setDocumento(rs.getString("DOCUMENTO"));
        datos.setTipo_doc_rel(rs.getString("TIPO_DOC_REL"));
        datos.setDocumento_rel(rs.getString("DOCUMENTO_REL"));
        datos.setDestinatario(rs.getString("DESTINATARIO"));
        return datos;
    }
   
    
        
    // LREALES - MODIFICADO 5 DICIEMBRE 2006
    public static remesa_docto loadImpoExpo ( ResultSet rs ) throws Exception {
        
        remesa_docto datos = new remesa_docto();
        
        datos.setReg_status( rs.getString( "reg_status" )!=null?rs.getString( "reg_status" ).toUpperCase():"" );
        datos.setDstrct( rs.getString( "dstrct" )!=null?rs.getString( "dstrct" ).toUpperCase():"" );
        datos.setTipo_doc( rs.getString( "tipo_doc" )!=null?rs.getString( "tipo_doc" ).toUpperCase():"" );
        datos.setDocumento( rs.getString( "documento" )!=null?rs.getString( "documento" ).toUpperCase():"" );
        datos.setFecha_sia( rs.getString( "fecha_sia" )!=null?rs.getString( "fecha_sia" ):"0099-01-01 00:00:00" );
        datos.setFecha_eta( rs.getString( "fecha_eta" )!=null?rs.getString( "fecha_eta" ):"0099-01-01 00:00:00" );
        datos.setDescripcion_cga( rs.getString( "descripcion" )!=null?rs.getString( "descripcion" ).toUpperCase():"" );
        datos.setUsuario_genero( rs.getString( "usuario_genero" )!=null?rs.getString( "usuario_genero" ).toUpperCase():"" );
        datos.setFecha_genero( rs.getString( "fecha_genero" )!=null?rs.getString( "fecha_genero" ):"0099-01-01 00:00:00" );
        datos.setLast_update( rs.getString( "last_update" )!=null?rs.getString( "last_update" ):"0099-01-01 00:00:00" );
        datos.setUser_update( rs.getString( "user_update" )!=null?rs.getString( "user_update" ).toUpperCase():"" );
        datos.setCreation_date( rs.getString( "creation_date" )!=null?rs.getString( "creation_date" ):"0099-01-01 00:00:00" );
        datos.setCreation_user( rs.getString( "creation_user" )!=null?rs.getString( "creation_user" ).toUpperCase():"" );
        datos.setCodcli( rs.getString( "codcli" )!=null?rs.getString( "codcli" ).toUpperCase():"" );
        datos.setFecha_llegada_cdr( rs.getString( "fecha_llegada_cdr" )!=null?rs.getString( "fecha_llegada_cdr" ):"0099-01-01" );
        datos.setFecha_salida_cdr( rs.getString( "fecha_salida_cdr" )!=null?rs.getString( "fecha_salida_cdr" ):"0099-01-01" );
        
        return datos;
        
    }
    
     public static remesa_docto loadTBLDOC(ResultSet rs)throws Exception{
        remesa_docto datos = new remesa_docto();
        datos.setDocument_type(rs.getString("DOCUMENT_TYPE"));
        //System.out.println("Documento "+rs.getString("DOCUMENT_TYPE"));
        datos.setDocument_name(rs.getString("DOCUMENT_NAME"));
        //System.out.println("nombre "+rs.getString("DOCUMENT_NAME"));
        datos.setImportacion(rs.getString("dato").length()>0?rs.getString("dato").substring(0,1):"");
        //System.out.println("Dato "+rs.getString("dato"));
        datos.setExportacion(rs.getString("dato").length()>1?rs.getString("dato").substring(1,2):"");
        return datos;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property documento_rel.
     * @return Value of property documento_rel.
     */
    public java.lang.String getDocumento_rel() {
        return documento_rel;
    }
    
    /**
     * Setter for property documento_rel.
     * @param documento_rel New value of property documento_rel.
     */
    public void setDocumento_rel(java.lang.String documento_rel) {
        this.documento_rel = documento_rel;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_recibido.
     * @return Value of property fecha_recibido.
     */
    public java.lang.String getFecha_recibido() {
        return fecha_recibido;
    }
    
    /**
     * Setter for property fecha_recibido.
     * @param fecha_recibido New value of property fecha_recibido.
     */
    public void setFecha_recibido(java.lang.String fecha_recibido) {
        this.fecha_recibido = fecha_recibido;
    }
    
    /**
     * Getter for property fecha_recibido_rel.
     * @return Value of property fecha_recibido_rel.
     */
    public java.lang.String getFecha_recibido_rel() {
        return fecha_recibido_rel;
    }
    
    /**
     * Setter for property fecha_recibido_rel.
     * @param fecha_recibido_rel New value of property fecha_recibido_rel.
     */
    public void setFecha_recibido_rel(java.lang.String fecha_recibido_rel) {
        this.fecha_recibido_rel = fecha_recibido_rel;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property recibido_documento.
     * @return Value of property recibido_documento.
     */
    public java.lang.String getRecibido_documento() {
        return recibido_documento;
    }
    
    /**
     * Setter for property recibido_documento.
     * @param recibido_documento New value of property recibido_documento.
     */
    public void setRecibido_documento(java.lang.String recibido_documento) {
        this.recibido_documento = recibido_documento;
    }
    
    /**
     * Getter for property recibido_documento_rel.
     * @return Value of property recibido_documento_rel.
     */
    public java.lang.String getRecibido_documento_rel() {
        return recibido_documento_rel;
    }
    
    /**
     * Setter for property recibido_documento_rel.
     * @param recibido_documento_rel New value of property recibido_documento_rel.
     */
    public void setRecibido_documento_rel(java.lang.String recibido_documento_rel) {
        this.recibido_documento_rel = recibido_documento_rel;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc() {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc(java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    /**
     * Getter for property tipo_doc_rel.
     * @return Value of property tipo_doc_rel.
     */
    public java.lang.String getTipo_doc_rel() {
        return tipo_doc_rel;
    }
    
    /**
     * Setter for property tipo_doc_rel.
     * @param tipo_doc_rel New value of property tipo_doc_rel.
     */
    public void setTipo_doc_rel(java.lang.String tipo_doc_rel) {
        this.tipo_doc_rel = tipo_doc_rel;
    }
    
    /**
     * Getter for property user_recibio_documento.
     * @return Value of property user_recibio_documento.
     */
    public java.lang.String getUser_recibio_documento() {
        return user_recibio_documento;
    }
    
    /**
     * Setter for property user_recibio_documento.
     * @param user_recibio_documento New value of property user_recibio_documento.
     */
    public void setUser_recibio_documento(java.lang.String user_recibio_documento) {
        this.user_recibio_documento = user_recibio_documento;
    }
    
    /**
     * Getter for property user_recibio_documento_rel.
     * @return Value of property user_recibio_documento_rel.
     */
    public java.lang.String getUser_recibio_documento_rel() {
        return user_recibio_documento_rel;
    }
    
    /**
     * Setter for property user_recibio_documento_rel.
     * @param user_recibio_documento_rel New value of property user_recibio_documento_rel.
     */
    public void setUser_recibio_documento_rel(java.lang.String user_recibio_documento_rel) {
        this.user_recibio_documento_rel = user_recibio_documento_rel;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property document_type.
     * @return Value of property document_type.
     */
    public java.lang.String getDocument_type() {
        return document_type;
    }
    
    /**
     * Setter for property document_type.
     * @param document_type New value of property document_type.
     */
    public void setDocument_type(java.lang.String document_type) {
        this.document_type = document_type;
    }
    
    /**
     * Getter for property document_name.
     * @return Value of property document_name.
     */
    public java.lang.String getDocument_name() {
        return document_name;
    }
    
    /**
     * Setter for property document_name.
     * @param document_name New value of property document_name.
     */
    public void setDocument_name(java.lang.String document_name) {
        this.document_name = document_name;
    }
    
    /**
     * Getter for property destinatario.
     * @return Value of property destinatario.
     */
    public java.lang.String getDestinatario() {
        return destinatario;
    }
    
    /**
     * Setter for property destinatario.
     * @param destinatario New value of property destinatario.
     */
    public void setDestinatario(java.lang.String destinatario) {
        this.destinatario = destinatario;
    }
    
    /**
     * Getter for property hijos.
     * @return Value of property hijos.
     */
    public java.util.Vector getHijos() {
        return hijos;
    }
    
    /**
     * Setter for property hijos.
     * @param hijos New value of property hijos.
     */
    public void setHijos(java.util.Vector hijos) {
        this.hijos = hijos;
    }
    //JUan 04.11.05
    public void agregarHijo(remesa_docto hijo){
        hijos.addElement(hijo);
    }
    
    /**
     * Getter for property document_rel_name.
     * @return Value of property document_rel_name.
     */
    public java.lang.String getDocument_rel_name() {
        return document_rel_name;
    }
    
    /**
     * Setter for property document_rel_name.
     * @param document_rel_name New value of property document_rel_name.
     */
    public void setDocument_rel_name(java.lang.String document_rel_name) {
        this.document_rel_name = document_rel_name;
    }
    
    /**
     * Getter for property ciudad_destinatario.
     * @return Value of property ciudad_destinatario.
     */
    public java.lang.String getCiudad_destinatario() {
        return ciudad_destinatario;
    }
    
    /**
     * Setter for property ciudad_destinatario.
     * @param ciudad_destinatario New value of property ciudad_destinatario.
     */
    public void setCiudad_destinatario(java.lang.String ciudad_destinatario) {
        this.ciudad_destinatario = ciudad_destinatario;
    }
    
    /**
     * Getter for property cadena.
     * @return Value of property cadena.
     */
    public java.lang.String getCadena() {
        return cadena;
    }
    
    /**
     * Setter for property cadena.
     * @param cadena New value of property cadena.
     */
    public void setCadena(java.lang.String cadena) {
        this.cadena = cadena;
    }
    
    /**
     * Getter for property fecha_sia.
     * @return Value of property fecha_sia.
     */
    public java.lang.String getFecha_sia() {
        return fecha_sia;
    }
    
    /**
     * Setter for property fecha_sia.
     * @param fecha_sia New value of property fecha_sia.
     */
    public void setFecha_sia(java.lang.String fecha_sia) {
        this.fecha_sia = fecha_sia;
    }
    
    /**
     * Getter for property fecha_eta.
     * @return Value of property fecha_eta.
     */
    public java.lang.String getFecha_eta() {
        return fecha_eta;
    }
    
    /**
     * Setter for property fecha_eta.
     * @param fecha_eta New value of property fecha_eta.
     */
    public void setFecha_eta(java.lang.String fecha_eta) {
        this.fecha_eta = fecha_eta;
    }
    
    /**
     * Getter for property descripcion_cga.
     * @return Value of property descripcion_cga.
     */
    public java.lang.String getDescripcion_cga() {
        return descripcion_cga;
    }
    
    /**
     * Setter for property descripcion_cga.
     * @param descripcion_cga New value of property descripcion_cga.
     */
    public void setDescripcion_cga(java.lang.String descripcion_cga) {
        this.descripcion_cga = descripcion_cga;
    }
    
    /**
     * Getter for property importacion.
     * @return Value of property importacion.
     */
    public java.lang.String getImportacion() {
        return importacion;
    }
    
    /**
     * Setter for property importacion.
     * @param importacion New value of property importacion.
     */
    public void setImportacion(java.lang.String importacion) {
        this.importacion = importacion;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property exportacion.
     * @return Value of property exportacion.
     */
    public java.lang.String getExportacion() {
        return exportacion;
    }
    
    /**
     * Setter for property exportacion.
     * @param exportacion New value of property exportacion.
     */
    public void setExportacion(java.lang.String exportacion) {
        this.exportacion = exportacion;
    }
    
    /**
     * Getter for property fecha_salida_cdr.
     * @return Value of property fecha_salida_cdr.
     */
    public java.lang.String getFecha_salida_cdr() {
        return fecha_salida_cdr;
    }
    
    /**
     * Setter for property fecha_salida_cdr.
     * @param fecha_salida_cdr New value of property fecha_salida_cdr.
     */
    public void setFecha_salida_cdr(java.lang.String fecha_salida_cdr) {
        this.fecha_salida_cdr = fecha_salida_cdr;
    }
    
    /**
     * Getter for property fecha_llegada_cdr.
     * @return Value of property fecha_llegada_cdr.
     */
    public java.lang.String getFecha_llegada_cdr() {
        return fecha_llegada_cdr;
    }
    
    /**
     * Setter for property fecha_llegada_cdr.
     * @param fecha_llegada_cdr New value of property fecha_llegada_cdr.
     */
    public void setFecha_llegada_cdr(java.lang.String fecha_llegada_cdr) {
        this.fecha_llegada_cdr = fecha_llegada_cdr;
    }
    
    /**
     * Getter for property fecha_genero.
     * @return Value of property fecha_genero.
     */
    public java.lang.String getFecha_genero() {
        return fecha_genero;
    }
    
    /**
     * Setter for property fecha_genero.
     * @param fecha_genero New value of property fecha_genero.
     */
    public void setFecha_genero(java.lang.String fecha_genero) {
        this.fecha_genero = fecha_genero;
    }
    
    /**
     * Getter for property usuario_genero.
     * @return Value of property usuario_genero.
     */
    public java.lang.String getUsuario_genero() {
        return usuario_genero;
    }
    
    /**
     * Setter for property usuario_genero.
     * @param usuario_genero New value of property usuario_genero.
     */
    public void setUsuario_genero(java.lang.String usuario_genero) {
        this.usuario_genero = usuario_genero;
    }
    
}
