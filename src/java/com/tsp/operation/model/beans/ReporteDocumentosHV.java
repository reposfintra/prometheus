/****
 * Nombre:                      ReporteDocumentosHV.java
 * Descripci�n:                 Beans de reporte Documentos de hoja de vida
 * Autor:                       Ing. Diogenes Antonio Bastidas Morales  
 * Fecha:                       15 de septiembre de 2006, 10:05 AM
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 *****/


package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  dbastidas
 */
public class ReporteDocumentosHV implements Serializable{
    
   private String agencia;
   private String numpla;
   private String placa;
   private String conductor;
   private String nrocedula;
   private String cliente;
   private String despachador;
   private String fechadsp;
   private String ruta;
   private String tipo_viaje;
   //documentos Conductor
   private String arp;
   private String fecha_arp;
   private String cedula;
   private String fecha_cedula;
   private String cert_jud;
   private String fecha_cert_jud;
   private String eps;
   private String fecha_eps;
   private String firma;
   private String fecha_firma;
   private String foto_con;
   private String fecha_foto_con;
   private String lib_mil;
   private String fecha_lib_mil;
   private String lib_tripulante;
   private String fecha_lib_tripulante;
   private String pasaporte;
   private String fecha_pasaporte;
   private String pase;
   private String fecha_pase;
   private String visa;
   private String fecha_visa;
   private String primera_h;
   private String fecha_primera_h;
   private String segunda_h;
   private String fecha_segunda_h;
   //documentos Placa
   private String emision_gas;
   private String fecha_emision_gas;
   private String cont_arriendo;
   private String fecha_cont_arriendo;
   private String cont_compra;
   private String fecha_cont_compra;
   private String foto_placa;
   private String fecha_foto_placa;
   private String poliza_andina;
   private String fecha_poliza_andina;
   private String reg_nal_carga;
   private String fecha_reg_nal_carga;
   private String reg_nal_semire;
   private String fecha_reg_nal_semire;
   private String soat;
   private String fecha_soat;
   private String tar_habi;
   private String fecha_tar_habi;
   private String tar_empresarial;
   private String fecha_tar_empresarial;
   private String tar_propiedad_cab;
   private String fecha_tar_propiedad_cab;
   private String tar_propiedad_semi;
   private String fecha_tar_propiedad_semi;
   //datos de plan de viaje
   private String planviaje;
   private String puesto1;
   private String puesto2;
   private String puesto3;
   private String tel1;
   private String tel2;
   private String tel3;
   private String celular;
   private String tproyectado;
   private String hreporte;
   private double tiempo;
   //nuevos campos 09-01-2007
   private String nro_celular;
   private String fecha_salida_dsp;
   
   private String debe_pernoctar;
   private String pernotacion; 
   private String fecha_pos_llegada;
   private String fecha_salida;
   
   
   
   /**
    * Getter for property agencia.
    * @return Value of property agencia.
    */
   public java.lang.String getAgencia() {
       return agencia;
   }   
   
   /**
    * Setter for property agencia.
    * @param agencia New value of property agencia.
    */
   public void setAgencia(java.lang.String agencia) {
       this.agencia = agencia;
   }   
   
   /**
    * Getter for property arp.
    * @return Value of property arp.
    */
   public java.lang.String getArp() {
       return arp;
   }   
   
   /**
    * Setter for property arp.
    * @param arp New value of property arp.
    */
   public void setArp(java.lang.String arp) {
       this.arp = arp;
   }   
    
   /**
    * Getter for property cedula.
    * @return Value of property cedula.
    */
   public java.lang.String getCedula() {
       return cedula;
   }
   
   /**
    * Setter for property cedula.
    * @param cedula New value of property cedula.
    */
   public void setCedula(java.lang.String cedula) {
       this.cedula = cedula;
   }
   
   /**
    * Getter for property cert_jud.
    * @return Value of property cert_jud.
    */
   public java.lang.String getCert_jud() {
       return cert_jud;
   }
   
   /**
    * Setter for property cert_jud.
    * @param cert_jud New value of property cert_jud.
    */
   public void setCert_jud(java.lang.String cert_jud) {
       this.cert_jud = cert_jud;
   }
   
   /**
    * Getter for property cliente.
    * @return Value of property cliente.
    */
   public java.lang.String getCliente() {
       return cliente;
   }
   
   /**
    * Setter for property cliente.
    * @param cliente New value of property cliente.
    */
   public void setCliente(java.lang.String cliente) {
       this.cliente = cliente;
   }
   
   /**
    * Getter for property conductor.
    * @return Value of property conductor.
    */
   public java.lang.String getConductor() {
       return conductor;
   }
   
   /**
    * Setter for property conductor.
    * @param conductor New value of property conductor.
    */
   public void setConductor(java.lang.String conductor) {
       this.conductor = conductor;
   }
   
   /**
    * Getter for property cont_arriendo.
    * @return Value of property cont_arriendo.
    */
   public java.lang.String getCont_arriendo() {
       return cont_arriendo;
   }
   
   /**
    * Setter for property cont_arriendo.
    * @param cont_arriendo New value of property cont_arriendo.
    */
   public void setCont_arriendo(java.lang.String cont_arriendo) {
       this.cont_arriendo = cont_arriendo;
   }
   
   /**
    * Getter for property cont_compra.
    * @return Value of property cont_compra.
    */
   public java.lang.String getCont_compra() {
       return cont_compra;
   }
   
   /**
    * Setter for property cont_compra.
    * @param cont_compra New value of property cont_compra.
    */
   public void setCont_compra(java.lang.String cont_compra) {
       this.cont_compra = cont_compra;
   }
   
   /**
    * Getter for property despachador.
    * @return Value of property despachador.
    */
   public java.lang.String getDespachador() {
       return despachador;
   }
   
   /**
    * Setter for property despachador.
    * @param despachador New value of property despachador.
    */
   public void setDespachador(java.lang.String despachador) {
       this.despachador = despachador;
   }
   
   /**
    * Getter for property emision_gas.
    * @return Value of property emision_gas.
    */
   public java.lang.String getEmision_gas() {
       return emision_gas;
   }
   
   /**
    * Setter for property emision_gas.
    * @param emision_gas New value of property emision_gas.
    */
   public void setEmision_gas(java.lang.String emision_gas) {
       this.emision_gas = emision_gas;
   }
   
   /**
    * Getter for property eps.
    * @return Value of property eps.
    */
   public java.lang.String getEps() {
       return eps;
   }
   
   /**
    * Setter for property eps.
    * @param eps New value of property eps.
    */
   public void setEps(java.lang.String eps) {
       this.eps = eps;
   }
   
   /**
    * Getter for property fecha_arp.
    * @return Value of property fecha_arp.
    */
   public java.lang.String getFecha_arp() {
       return fecha_arp;
   }
   
   /**
    * Setter for property fecha_arp.
    * @param fecha_arp New value of property fecha_arp.
    */
   public void setFecha_arp(java.lang.String fecha_arp) {
       this.fecha_arp = fecha_arp;
   }
   
   /**
    * Getter for property fecha_cedula.
    * @return Value of property fecha_cedula.
    */
   public java.lang.String getFecha_cedula() {
       return fecha_cedula;
   }
   
   /**
    * Setter for property fecha_cedula.
    * @param fecha_cedula New value of property fecha_cedula.
    */
   public void setFecha_cedula(java.lang.String fecha_cedula) {
       this.fecha_cedula = fecha_cedula;
   }
   
   /**
    * Getter for property fecha_cert_jud.
    * @return Value of property fecha_cert_jud.
    */
   public java.lang.String getFecha_cert_jud() {
       return fecha_cert_jud;
   }
   
   /**
    * Setter for property fecha_cert_jud.
    * @param fecha_cert_jud New value of property fecha_cert_jud.
    */
   public void setFecha_cert_jud(java.lang.String fecha_cert_jud) {
       this.fecha_cert_jud = fecha_cert_jud;
   }
   
   /**
    * Getter for property fecha_cont_arriendo.
    * @return Value of property fecha_cont_arriendo.
    */
   public java.lang.String getFecha_cont_arriendo() {
       return fecha_cont_arriendo;
   }
   
   /**
    * Setter for property fecha_cont_arriendo.
    * @param fecha_cont_arriendo New value of property fecha_cont_arriendo.
    */
   public void setFecha_cont_arriendo(java.lang.String fecha_cont_arriendo) {
       this.fecha_cont_arriendo = fecha_cont_arriendo;
   }
   
   /**
    * Getter for property fecha_cont_compra.
    * @return Value of property fecha_cont_compra.
    */
   public java.lang.String getFecha_cont_compra() {
       return fecha_cont_compra;
   }
   
   /**
    * Setter for property fecha_cont_compra.
    * @param fecha_cont_compra New value of property fecha_cont_compra.
    */
   public void setFecha_cont_compra(java.lang.String fecha_cont_compra) {
       this.fecha_cont_compra = fecha_cont_compra;
   }
   
   /**
    * Getter for property fecha_emision_gas.
    * @return Value of property fecha_emision_gas.
    */
   public java.lang.String getFecha_emision_gas() {
       return fecha_emision_gas;
   }
   
   /**
    * Setter for property fecha_emision_gas.
    * @param fecha_emision_gas New value of property fecha_emision_gas.
    */
   public void setFecha_emision_gas(java.lang.String fecha_emision_gas) {
       this.fecha_emision_gas = fecha_emision_gas;
   }
   
   /**
    * Getter for property fecha_eps.
    * @return Value of property fecha_eps.
    */
   public java.lang.String getFecha_eps() {
       return fecha_eps;
   }
   
   /**
    * Setter for property fecha_eps.
    * @param fecha_eps New value of property fecha_eps.
    */
   public void setFecha_eps(java.lang.String fecha_eps) {
       this.fecha_eps = fecha_eps;
   }
   
   /**
    * Getter for property fecha_firma.
    * @return Value of property fecha_firma.
    */
   public java.lang.String getFecha_firma() {
       return fecha_firma;
   }
   
   /**
    * Setter for property fecha_firma.
    * @param fecha_firma New value of property fecha_firma.
    */
   public void setFecha_firma(java.lang.String fecha_firma) {
       this.fecha_firma = fecha_firma;
   }
   
   /**
    * Getter for property fecha_foto_con.
    * @return Value of property fecha_foto_con.
    */
   public java.lang.String getFecha_foto_con() {
       return fecha_foto_con;
   }
   
   /**
    * Setter for property fecha_foto_con.
    * @param fecha_foto_con New value of property fecha_foto_con.
    */
   public void setFecha_foto_con(java.lang.String fecha_foto_con) {
       this.fecha_foto_con = fecha_foto_con;
   }
   
   /**
    * Getter for property fecha_foto_placa.
    * @return Value of property fecha_foto_placa.
    */
   public java.lang.String getFecha_foto_placa() {
       return fecha_foto_placa;
   }
   
   /**
    * Setter for property fecha_foto_placa.
    * @param fecha_foto_placa New value of property fecha_foto_placa.
    */
   public void setFecha_foto_placa(java.lang.String fecha_foto_placa) {
       this.fecha_foto_placa = fecha_foto_placa;
   }
   
   /**
    * Getter for property fecha_lib_mil.
    * @return Value of property fecha_lib_mil.
    */
   public java.lang.String getFecha_lib_mil() {
       return fecha_lib_mil;
   }
   
   /**
    * Setter for property fecha_lib_mil.
    * @param fecha_lib_mil New value of property fecha_lib_mil.
    */
   public void setFecha_lib_mil(java.lang.String fecha_lib_mil) {
       this.fecha_lib_mil = fecha_lib_mil;
   }
   
   /**
    * Getter for property fecha_lib_tripulante.
    * @return Value of property fecha_lib_tripulante.
    */
   public java.lang.String getFecha_lib_tripulante() {
       return fecha_lib_tripulante;
   }
   
   /**
    * Setter for property fecha_lib_tripulante.
    * @param fecha_lib_tripulante New value of property fecha_lib_tripulante.
    */
   public void setFecha_lib_tripulante(java.lang.String fecha_lib_tripulante) {
       this.fecha_lib_tripulante = fecha_lib_tripulante;
   }
   
   /**
    * Getter for property fecha_pasaporte.
    * @return Value of property fecha_pasaporte.
    */
   public java.lang.String getFecha_pasaporte() {
       return fecha_pasaporte;
   }
   
   /**
    * Setter for property fecha_pasaporte.
    * @param fecha_pasaporte New value of property fecha_pasaporte.
    */
   public void setFecha_pasaporte(java.lang.String fecha_pasaporte) {
       this.fecha_pasaporte = fecha_pasaporte;
   }
   
   /**
    * Getter for property fecha_pase.
    * @return Value of property fecha_pase.
    */
   public java.lang.String getFecha_pase() {
       return fecha_pase;
   }
   
   /**
    * Setter for property fecha_pase.
    * @param fecha_pase New value of property fecha_pase.
    */
   public void setFecha_pase(java.lang.String fecha_pase) {
       this.fecha_pase = fecha_pase;
   }
   
   /**
    * Getter for property fecha_poliza_andina.
    * @return Value of property fecha_poliza_andina.
    */
   public java.lang.String getFecha_poliza_andina() {
       return fecha_poliza_andina;
   }
   
   /**
    * Setter for property fecha_poliza_andina.
    * @param fecha_poliza_andina New value of property fecha_poliza_andina.
    */
   public void setFecha_poliza_andina(java.lang.String fecha_poliza_andina) {
       this.fecha_poliza_andina = fecha_poliza_andina;
   }
   
   /**
    * Getter for property fecha_primera_h.
    * @return Value of property fecha_primera_h.
    */
   public java.lang.String getFecha_primera_h() {
       return fecha_primera_h;
   }
   
   /**
    * Setter for property fecha_primera_h.
    * @param fecha_primera_h New value of property fecha_primera_h.
    */
   public void setFecha_primera_h(java.lang.String fecha_primera_h) {
       this.fecha_primera_h = fecha_primera_h;
   }
   
   /**
    * Getter for property fecha_reg_nal_carga.
    * @return Value of property fecha_reg_nal_carga.
    */
   public java.lang.String getFecha_reg_nal_carga() {
       return fecha_reg_nal_carga;
   }
   
   /**
    * Setter for property fecha_reg_nal_carga.
    * @param fecha_reg_nal_carga New value of property fecha_reg_nal_carga.
    */
   public void setFecha_reg_nal_carga(java.lang.String fecha_reg_nal_carga) {
       this.fecha_reg_nal_carga = fecha_reg_nal_carga;
   }
   
   /**
    * Getter for property fecha_reg_nal_semire.
    * @return Value of property fecha_reg_nal_semire.
    */
   public java.lang.String getFecha_reg_nal_semire() {
       return fecha_reg_nal_semire;
   }
   
   /**
    * Setter for property fecha_reg_nal_semire.
    * @param fecha_reg_nal_semire New value of property fecha_reg_nal_semire.
    */
   public void setFecha_reg_nal_semire(java.lang.String fecha_reg_nal_semire) {
       this.fecha_reg_nal_semire = fecha_reg_nal_semire;
   }
   
   /**
    * Getter for property fecha_segunda_h.
    * @return Value of property fecha_segunda_h.
    */
   public java.lang.String getFecha_segunda_h() {
       return fecha_segunda_h;
   }
   
   /**
    * Setter for property fecha_segunda_h.
    * @param fecha_segunda_h New value of property fecha_segunda_h.
    */
   public void setFecha_segunda_h(java.lang.String fecha_segunda_h) {
       this.fecha_segunda_h = fecha_segunda_h;
   }
   
   /**
    * Getter for property fecha_soat.
    * @return Value of property fecha_soat.
    */
   public java.lang.String getFecha_soat() {
       return fecha_soat;
   }
   
   /**
    * Setter for property fecha_soat.
    * @param fecha_soat New value of property fecha_soat.
    */
   public void setFecha_soat(java.lang.String fecha_soat) {
       this.fecha_soat = fecha_soat;
   }
   
   /**
    * Getter for property fecha_tar_empresarial.
    * @return Value of property fecha_tar_empresarial.
    */
   public java.lang.String getFecha_tar_empresarial() {
       return fecha_tar_empresarial;
   }
   
   /**
    * Setter for property fecha_tar_empresarial.
    * @param fecha_tar_empresarial New value of property fecha_tar_empresarial.
    */
   public void setFecha_tar_empresarial(java.lang.String fecha_tar_empresarial) {
       this.fecha_tar_empresarial = fecha_tar_empresarial;
   }
   
   /**
    * Getter for property fecha_tar_habi.
    * @return Value of property fecha_tar_habi.
    */
   public java.lang.String getFecha_tar_habi() {
       return fecha_tar_habi;
   }
   
   /**
    * Setter for property fecha_tar_habi.
    * @param fecha_tar_habi New value of property fecha_tar_habi.
    */
   public void setFecha_tar_habi(java.lang.String fecha_tar_habi) {
       this.fecha_tar_habi = fecha_tar_habi;
   }
   
   /**
    * Getter for property fecha_tar_propiedad_cab.
    * @return Value of property fecha_tar_propiedad_cab.
    */
   public java.lang.String getFecha_tar_propiedad_cab() {
       return fecha_tar_propiedad_cab;
   }
   
   /**
    * Setter for property fecha_tar_propiedad_cab.
    * @param fecha_tar_propiedad_cab New value of property fecha_tar_propiedad_cab.
    */
   public void setFecha_tar_propiedad_cab(java.lang.String fecha_tar_propiedad_cab) {
       this.fecha_tar_propiedad_cab = fecha_tar_propiedad_cab;
   }
   
   /**
    * Getter for property fecha_tar_propiedad_semi.
    * @return Value of property fecha_tar_propiedad_semi.
    */
   public java.lang.String getFecha_tar_propiedad_semi() {
       return fecha_tar_propiedad_semi;
   }
   
   /**
    * Setter for property fecha_tar_propiedad_semi.
    * @param fecha_tar_propiedad_semi New value of property fecha_tar_propiedad_semi.
    */
   public void setFecha_tar_propiedad_semi(java.lang.String fecha_tar_propiedad_semi) {
       this.fecha_tar_propiedad_semi = fecha_tar_propiedad_semi;
   }
   
   /**
    * Getter for property fecha_visa.
    * @return Value of property fecha_visa.
    */
   public java.lang.String getFecha_visa() {
       return fecha_visa;
   }
   
   /**
    * Setter for property fecha_visa.
    * @param fecha_visa New value of property fecha_visa.
    */
   public void setFecha_visa(java.lang.String fecha_visa) {
       this.fecha_visa = fecha_visa;
   }
   
   /**
    * Getter for property fechadsp.
    * @return Value of property fechadsp.
    */
   public java.lang.String getFechadsp() {
       return fechadsp;
   }
   
   /**
    * Setter for property fechadsp.
    * @param fechadsp New value of property fechadsp.
    */
   public void setFechadsp(java.lang.String fechadsp) {
       this.fechadsp = fechadsp;
   }
   
   /**
    * Getter for property firma.
    * @return Value of property firma.
    */
   public java.lang.String getFirma() {
       return firma;
   }
   
   /**
    * Setter for property firma.
    * @param firma New value of property firma.
    */
   public void setFirma(java.lang.String firma) {
       this.firma = firma;
   }
   
   /**
    * Getter for property foto_con.
    * @return Value of property foto_con.
    */
   public java.lang.String getFoto_con() {
       return foto_con;
   }
   
   /**
    * Setter for property foto_con.
    * @param foto_con New value of property foto_con.
    */
   public void setFoto_con(java.lang.String foto_con) {
       this.foto_con = foto_con;
   }
   
   /**
    * Getter for property foto_placa.
    * @return Value of property foto_placa.
    */
   public java.lang.String getFoto_placa() {
       return foto_placa;
   }
   
   /**
    * Setter for property foto_placa.
    * @param foto_placa New value of property foto_placa.
    */
   public void setFoto_placa(java.lang.String foto_placa) {
       this.foto_placa = foto_placa;
   }
   
   /**
    * Getter for property lib_mil.
    * @return Value of property lib_mil.
    */
   public java.lang.String getLib_mil() {
       return lib_mil;
   }
   
   /**
    * Setter for property lib_mil.
    * @param lib_mil New value of property lib_mil.
    */
   public void setLib_mil(java.lang.String lib_mil) {
       this.lib_mil = lib_mil;
   }
   
   /**
    * Getter for property lib_tripulante.
    * @return Value of property lib_tripulante.
    */
   public java.lang.String getLib_tripulante() {
       return lib_tripulante;
   }
   
   /**
    * Setter for property lib_tripulante.
    * @param lib_tripulante New value of property lib_tripulante.
    */
   public void setLib_tripulante(java.lang.String lib_tripulante) {
       this.lib_tripulante = lib_tripulante;
   }
   
   /**
    * Getter for property numpla.
    * @return Value of property numpla.
    */
   public java.lang.String getNumpla() {
       return numpla;
   }
   
   /**
    * Setter for property numpla.
    * @param numpla New value of property numpla.
    */
   public void setNumpla(java.lang.String numpla) {
       this.numpla = numpla;
   }
   
   /**
    * Getter for property pasaporte.
    * @return Value of property pasaporte.
    */
   public java.lang.String getPasaporte() {
       return pasaporte;
   }
   
   /**
    * Setter for property pasaporte.
    * @param pasaporte New value of property pasaporte.
    */
   public void setPasaporte(java.lang.String pasaporte) {
       this.pasaporte = pasaporte;
   }
   
   /**
    * Getter for property pase.
    * @return Value of property pase.
    */
   public java.lang.String getPase() {
       return pase;
   }
   
   /**
    * Setter for property pase.
    * @param pase New value of property pase.
    */
   public void setPase(java.lang.String pase) {
       this.pase = pase;
   }
   
   /**
    * Getter for property placa.
    * @return Value of property placa.
    */
   public java.lang.String getPlaca() {
       return placa;
   }
   
   /**
    * Setter for property placa.
    * @param placa New value of property placa.
    */
   public void setPlaca(java.lang.String placa) {
       this.placa = placa;
   }
   
   /**
    * Getter for property poliza_andina.
    * @return Value of property poliza_andina.
    */
   public java.lang.String getPoliza_andina() {
       return poliza_andina;
   }
   
   /**
    * Setter for property poliza_andina.
    * @param poliza_andina New value of property poliza_andina.
    */
   public void setPoliza_andina(java.lang.String poliza_andina) {
       this.poliza_andina = poliza_andina;
   }
   
   /**
    * Getter for property primera_h.
    * @return Value of property primera_h.
    */
   public java.lang.String getPrimera_h() {
       return primera_h;
   }
   
   /**
    * Setter for property primera_h.
    * @param primera_h New value of property primera_h.
    */
   public void setPrimera_h(java.lang.String primera_h) {
       this.primera_h = primera_h;
   }
   
   /**
    * Getter for property reg_nal_carga.
    * @return Value of property reg_nal_carga.
    */
   public java.lang.String getReg_nal_carga() {
       return reg_nal_carga;
   }
   
   /**
    * Setter for property reg_nal_carga.
    * @param reg_nal_carga New value of property reg_nal_carga.
    */
   public void setReg_nal_carga(java.lang.String reg_nal_carga) {
       this.reg_nal_carga = reg_nal_carga;
   }
   
   /**
    * Getter for property reg_nal_semire.
    * @return Value of property reg_nal_semire.
    */
   public java.lang.String getReg_nal_semire() {
       return reg_nal_semire;
   }
   
   /**
    * Setter for property reg_nal_semire.
    * @param reg_nal_semire New value of property reg_nal_semire.
    */
   public void setReg_nal_semire(java.lang.String reg_nal_semire) {
       this.reg_nal_semire = reg_nal_semire;
   }
   
   /**
    * Getter for property segunda_h.
    * @return Value of property segunda_h.
    */
   public java.lang.String getSegunda_h() {
       return segunda_h;
   }
   
   /**
    * Setter for property segunda_h.
    * @param segunda_h New value of property segunda_h.
    */
   public void setSegunda_h(java.lang.String segunda_h) {
       this.segunda_h = segunda_h;
   }
   
   /**
    * Getter for property soat.
    * @return Value of property soat.
    */
   public java.lang.String getSoat() {
       return soat;
   }
   
   /**
    * Setter for property soat.
    * @param soat New value of property soat.
    */
   public void setSoat(java.lang.String soat) {
       this.soat = soat;
   }
   
   /**
    * Getter for property tar_empresarial.
    * @return Value of property tar_empresarial.
    */
   public java.lang.String getTar_empresarial() {
       return tar_empresarial;
   }
   
   /**
    * Setter for property tar_empresarial.
    * @param tar_empresarial New value of property tar_empresarial.
    */
   public void setTar_empresarial(java.lang.String tar_empresarial) {
       this.tar_empresarial = tar_empresarial;
   }
   
   /**
    * Getter for property tar_habi.
    * @return Value of property tar_habi.
    */
   public java.lang.String getTar_habi() {
       return tar_habi;
   }
   
   /**
    * Setter for property tar_habi.
    * @param tar_habi New value of property tar_habi.
    */
   public void setTar_habi(java.lang.String tar_habi) {
       this.tar_habi = tar_habi;
   }
   
   /**
    * Getter for property tar_propiedad_cab.
    * @return Value of property tar_propiedad_cab.
    */
   public java.lang.String getTar_propiedad_cab() {
       return tar_propiedad_cab;
   }
   
   /**
    * Setter for property tar_propiedad_cab.
    * @param tar_propiedad_cab New value of property tar_propiedad_cab.
    */
   public void setTar_propiedad_cab(java.lang.String tar_propiedad_cab) {
       this.tar_propiedad_cab = tar_propiedad_cab;
   }
   
   /**
    * Getter for property tar_propiedad_semi.
    * @return Value of property tar_propiedad_semi.
    */
   public java.lang.String getTar_propiedad_semi() {
       return tar_propiedad_semi;
   }
   
   /**
    * Setter for property tar_propiedad_semi.
    * @param tar_propiedad_semi New value of property tar_propiedad_semi.
    */
   public void setTar_propiedad_semi(java.lang.String tar_propiedad_semi) {
       this.tar_propiedad_semi = tar_propiedad_semi;
   }
   
   /**
    * Getter for property visa.
    * @return Value of property visa.
    */
   public java.lang.String getVisa() {
       return visa;
   }
   
   /**
    * Setter for property visa.
    * @param visa New value of property visa.
    */
   public void setVisa(java.lang.String visa) {
       this.visa = visa;
   }
   
   /**
    * Getter for property nrocedula.
    * @return Value of property nrocedula.
    */
   public java.lang.String getNrocedula() {
       return nrocedula;
   }
   
   /**
    * Setter for property nrocedula.
    * @param nrocedula New value of property nrocedula.
    */
   public void setNrocedula(java.lang.String nrocedula) {
       this.nrocedula = nrocedula;
   }
   
   /**
    * Getter for property ruta.
    * @return Value of property ruta.
    */
   public java.lang.String getRuta() {
       return ruta;
   }
   
   /**
    * Setter for property ruta.
    * @param ruta New value of property ruta.
    */
   public void setRuta(java.lang.String ruta) {
       this.ruta = ruta;
   }
   
   /**
    * Getter for property tipo_viaje.
    * @return Value of property tipo_viaje.
    */
   public java.lang.String getTipo_viaje() {
       return tipo_viaje;
   }
   
   /**
    * Setter for property tipo_viaje.
    * @param tipo_viaje New value of property tipo_viaje.
    */
   public void setTipo_viaje(java.lang.String tipo_viaje) {
       this.tipo_viaje = tipo_viaje;
   }
   
  
   
   /**
    * Getter for property puesto1.
    * @return Value of property puesto1.
    */
   public java.lang.String getPuesto1() {
       return puesto1;
   }
   
   /**
    * Setter for property puesto1.
    * @param puesto1 New value of property puesto1.
    */
   public void setPuesto1(java.lang.String puesto1) {
       this.puesto1 = puesto1;
   }
   
   /**
    * Getter for property puesto2.
    * @return Value of property puesto2.
    */
   public java.lang.String getPuesto2() {
       return puesto2;
   }
   
   /**
    * Setter for property puesto2.
    * @param puesto2 New value of property puesto2.
    */
   public void setPuesto2(java.lang.String puesto2) {
       this.puesto2 = puesto2;
   }
   
   /**
    * Getter for property puesto3.
    * @return Value of property puesto3.
    */
   public java.lang.String getPuesto3() {
       return puesto3;
   }
   
   /**
    * Setter for property puesto3.
    * @param puesto3 New value of property puesto3.
    */
   public void setPuesto3(java.lang.String puesto3) {
       this.puesto3 = puesto3;
   }
   
   /**
    * Getter for property tel1.
    * @return Value of property tel1.
    */
   public java.lang.String getTel1() {
       return tel1;
   }
   
   /**
    * Setter for property tel1.
    * @param tel1 New value of property tel1.
    */
   public void setTel1(java.lang.String tel1) {
       this.tel1 = tel1;
   }
   
   /**
    * Getter for property tel2.
    * @return Value of property tel2.
    */
   public java.lang.String getTel2() {
       return tel2;
   }
   
   /**
    * Setter for property tel2.
    * @param tel2 New value of property tel2.
    */
   public void setTel2(java.lang.String tel2) {
       this.tel2 = tel2;
   }
   
   /**
    * Getter for property tel3.
    * @return Value of property tel3.
    */
   public java.lang.String getTel3() {
       return tel3;
   }
   
   /**
    * Setter for property tel3.
    * @param tel3 New value of property tel3.
    */
   public void setTel3(java.lang.String tel3) {
       this.tel3 = tel3;
   }
   
   /**
    * Getter for property celular.
    * @return Value of property celular.
    */
   public java.lang.String getCelular() {
       return celular;
   }
   
   /**
    * Setter for property celular.
    * @param celular New value of property celular.
    */
   public void setCelular(java.lang.String celular) {
       this.celular = celular;
   }
   
   /**
    * Getter for property pernotacion.
    * @return Value of property pernotacion.
    */
   public java.lang.String getPernotacion() {
       return pernotacion;
   }
   
   /**
    * Setter for property pernotacion.
    * @param pernotacion New value of property pernotacion.
    */
   public void setPernotacion(java.lang.String pernotacion) {
       this.pernotacion = pernotacion;
   }
   
   /**
    * Getter for property tproyectado.
    * @return Value of property tproyectado.
    */
   public java.lang.String getTproyectado() {
       return tproyectado;
   }
   
   /**
    * Setter for property tproyectado.
    * @param tproyectado New value of property tproyectado.
    */
   public void setTproyectado(java.lang.String tproyectado) {
       this.tproyectado = tproyectado;
   }
   
   /**
    * Getter for property hreporte.
    * @return Value of property hreporte.
    */
   public java.lang.String getHreporte() {
       return hreporte;
   }
   
   /**
    * Setter for property hreporte.
    * @param hreporte New value of property hreporte.
    */
   public void setHreporte(java.lang.String hreporte) {
       this.hreporte = hreporte;
   }
   
   /**
    * Getter for property tiempo.
    * @return Value of property tiempo.
    */
   public double getTiempo() {
       return tiempo;
   }
   
   /**
    * Setter for property tiempo.
    * @param tiempo New value of property tiempo.
    */
   public void setTiempo(double tiempo) {
       this.tiempo = tiempo;
   }
   
   /**
    * Getter for property nro_celular.
    * @return Value of property nro_celular.
    */
   public java.lang.String getNro_celular() {
       return nro_celular;
   }
   
   /**
    * Setter for property nro_celular.
    * @param nro_celular New value of property nro_celular.
    */
   public void setNro_celular(java.lang.String nro_celular) {
       this.nro_celular = nro_celular;
   }
   
   /**
    * Getter for property fecha_salida_dsp.
    * @return Value of property fecha_salida_dsp.
    */
   public java.lang.String getFecha_salida_dsp() {
       return fecha_salida_dsp;
   }
   
   /**
    * Setter for property fecha_salida_dsp.
    * @param fecha_salida_dsp New value of property fecha_salida_dsp.
    */
   public void setFecha_salida_dsp(java.lang.String fecha_salida_dsp) {
       this.fecha_salida_dsp = fecha_salida_dsp;
   }
   
   /**
    * Getter for property planviaje.
    * @return Value of property planviaje.
    */
   public java.lang.String getPlanviaje() {
       return planviaje;
   }
   
   /**
    * Setter for property planviaje.
    * @param planviaje New value of property planviaje.
    */
   public void setPlanviaje(java.lang.String planviaje) {
       this.planviaje = planviaje;
   }
   
   /**
    * Getter for property fecha_pos_llegada.
    * @return Value of property fecha_pos_llegada.
    */
   public java.lang.String getFecha_pos_llegada() {
       return fecha_pos_llegada;
   }   
   
   /**
    * Setter for property fecha_pos_llegada.
    * @param fecha_pos_llegada New value of property fecha_pos_llegada.
    */
   public void setFecha_pos_llegada(java.lang.String fecha_pos_llegada) {
       this.fecha_pos_llegada = fecha_pos_llegada;
   }   
   
   /**
    * Getter for property fecha_salida.
    * @return Value of property fecha_salida.
    */
   public java.lang.String getFecha_salida() {
       return fecha_salida;
   }
   
   /**
    * Setter for property fecha_salida.
    * @param fecha_salida New value of property fecha_salida.
    */
   public void setFecha_salida(java.lang.String fecha_salida) {
       this.fecha_salida = fecha_salida;
   }
   
   /**
    * Getter for property debe_pernoctar.
    * @return Value of property debe_pernoctar.
    */
   public java.lang.String getDebe_pernoctar() {
       return debe_pernoctar;
   }
   
   /**
    * Setter for property debe_pernoctar.
    * @param debe_pernoctar New value of property debe_pernoctar.
    */
   public void setDebe_pernoctar(java.lang.String debe_pernoctar) {
       this.debe_pernoctar = debe_pernoctar;
   }
   
}
