/*
 * Tramo.java
 *
 * Created on 24 de junio de 2005, 09:52 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Jcuesta
 */
public class Ruta {
    
    private String cia;
    private String ncia;//
    private String origen;
    private String norigen;//
    private String destino;
    private String ndestino;//
    private String secuencia;
    private String via;
    private String usuario;
    private String pais_o;
    private String npais_o;//
    private String pais_d;
    private String npais_d;//
    ///
    private double duracion;
    private double tiempo;
    private String descripcion;
    private String base;
    private String nvia;//miestra la ruta x nombre
    private String tvia;//muesta la ruta x codigo con \n
    /** Creates a new instance of via */
    public Ruta() {
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }    
   
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property ncia.
     * @return Value of property ncia.
     */
    public java.lang.String getNcia() {
        return ncia;
    }
    
    /**
     * Setter for property ncia.
     * @param ncia New value of property ncia.
     */
    public void setNcia(java.lang.String ncia) {
        this.ncia = ncia;
    }
    
    /**
     * Getter for property ndestino.
     * @return Value of property ndestino.
     */
    public java.lang.String getNdestino() {
        return ndestino;
    }
    
    /**
     * Setter for property ndestino.
     * @param ndestino New value of property ndestino.
     */
    public void setNdestino(java.lang.String ndestino) {
        this.ndestino = ndestino;
    }
    
    /**
     * Getter for property norigen.
     * @return Value of property norigen.
     */
    public java.lang.String getNorigen() {
        return norigen;
    }
    
    /**
     * Setter for property norigen.
     * @param norigen New value of property norigen.
     */
    public void setNorigen(java.lang.String norigen) {
        this.norigen = norigen;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property secuencia.
     * @return Value of property secuencia.
     */
    public java.lang.String getSecuencia() {
        return secuencia;
    }
    
    /**
     * Setter for property secuencia.
     * @param secuencia New value of property secuencia.
     */
    public void setSecuencia(java.lang.String secuencia) {
        this.secuencia = secuencia;
    }
    
    /**
     * Getter for property via.
     * @return Value of property via.
     */
    public java.lang.String getVia() {
        return via;
    }
    
    /**
     * Setter for property via.
     * @param via New value of property via.
     */
    public void setVia(java.lang.String via) {
        this.via = via;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property npais_d.
     * @return Value of property npais_d.
     */
    public java.lang.String getNpais_d() {
        return npais_d;
    }
    
    /**
     * Setter for property npais_d.
     * @param npais_d New value of property npais_d.
     */
    public void setNpais_d(java.lang.String npais_d) {
        this.npais_d = npais_d;
    }
    
    /**
     * Getter for property npais_o.
     * @return Value of property npais_o.
     */
    public java.lang.String getNpais_o() {
        return npais_o;
    }
    
    /**
     * Setter for property npais_o.
     * @param npais_o New value of property npais_o.
     */
    public void setNpais_o(java.lang.String npais_o) {
        this.npais_o = npais_o;
    }
    
    /**
     * Getter for property pais_d.
     * @return Value of property pais_d.
     */
    public java.lang.String getPais_d() {
        return pais_d;
    }
    
    /**
     * Setter for property pais_d.
     * @param pais_d New value of property pais_d.
     */
    public void setPais_d(java.lang.String pais_d) {
        this.pais_d = pais_d;
    }
    
    /**
     * Getter for property pais_o.
     * @return Value of property pais_o.
     */
    public java.lang.String getPais_o() {
        return pais_o;
    }
    
    /**
     * Setter for property pais_o.
     * @param pais_o New value of property pais_o.
     */
    public void setPais_o(java.lang.String pais_o) {
        this.pais_o = pais_o;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property duracion.
     * @return Value of property duracion.
     */
    public double getDuracion() {
        return duracion;
    }
    
    /**
     * Setter for property duracion.
     * @param duracion New value of property duracion.
     */
    public void setDuracion(double duracion) {
        this.duracion = duracion;
    }
    
    /**
     * Getter for property tiempo.
     * @return Value of property tiempo.
     */
    public double getTiempo() {
        return tiempo;
    }
    
    /**
     * Setter for property tiempo.
     * @param tiempo New value of property tiempo.
     */
    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property nvia.
     * @return Value of property nvia.
     */
    public java.lang.String getNvia() {
        return nvia;
    }
    
    /**
     * Setter for property nvia.
     * @param nvia New value of property nvia.
     */
    public void setNvia(java.lang.String nvia) {
        this.nvia = nvia;
    }
    
    /**
     * Getter for property tvia.
     * @return Value of property tvia.
     */
    public java.lang.String getTvia() {
        return tvia;
    }
    
    /**
     * Setter for property tvia.
     * @param tvia New value of property tvia.
     */
    public void setTvia(java.lang.String tvia) {
        this.tvia = tvia;
    }
    
}
