/*
 * Nombre        DatoFormato.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         27 de octubre de 2006, 04:20 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

public class DatoFormato {
    
    private String reg_status;    
    private String dstrct;
    private String tipo_doc;
    private String documento;
    private String formato;
    private String dato;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String base;
    private String cadena_datos;
    
    /*Datos Remision*/
    private String ot;
    private String remesion;
    private String sucursal;
    private String dirsucursal;
    private String telsucursal;
    private String fechadespacho;
    private String horadespacho;
    private String fechactual;
    private String placa;
    private String nomcond;
    private String cedcond;
    private String celularcond;
    private String piezas;
    private String peso;
    private String tipo;
    private String ruta;
    private String centrocosto;
    private String compensac;
    private String origen;
    private String dirorigen;
    private String telorigen;
    private String contactoorigen;
    private String destino;
    private String dirdestino;
    private String teldestino;
    private String contactodestino;
    private String elementodo;
    private String po;
    private String contenido;
    private String observaciones;
    private String nombreenvio;
    
    private String numpla;
    private String agencia;
    private String auxfecha;
    private String DesRemision;
    
    
    
    /**
     * Crea una nueva instancia de  DatoFormato
     */
    public DatoFormato() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dato.
     * @return Value of property dato.
     */
    public java.lang.String getDato() {
        return dato;
    }
    
    /**
     * Setter for property dato.
     * @param dato New value of property dato.
     */
    public void setDato(java.lang.String dato) {
        this.dato = dato;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property formato.
     * @return Value of property formato.
     */
    public java.lang.String getFormato() {
        return formato;
    }
    
    /**
     * Setter for property formato.
     * @param formato New value of property formato.
     */
    public void setFormato(java.lang.String formato) {
        this.formato = formato;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc() {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc(java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property cadena_datos.
     * @return Value of property cadena_datos.
     */
    public java.lang.String getCadena_datos() {
        return cadena_datos;
    }
    
    /**
     * Setter for property cadena_datos.
     * @param cadena_datos New value of property cadena_datos.
     */
    public void setCadena_datos(java.lang.String cadena_datos) {
        this.cadena_datos = cadena_datos;
    }
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt() {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt(java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property remesion.
     * @return Value of property remesion.
     */
    public java.lang.String getRemesion() {
        return remesion;
    }
    
    /**
     * Setter for property remesion.
     * @param remesion New value of property remesion.
     */
    public void setRemesion(java.lang.String remesion) {
        this.remesion = remesion;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property dirsucursal.
     * @return Value of property dirsucursal.
     */
    public java.lang.String getDirsucursal() {
        return dirsucursal;
    }
    
    /**
     * Setter for property dirsucursal.
     * @param dirsucursal New value of property dirsucursal.
     */
    public void setDirsucursal(java.lang.String dirsucursal) {
        this.dirsucursal = dirsucursal;
    }
    
    /**
     * Getter for property telsucursal.
     * @return Value of property telsucursal.
     */
    public java.lang.String getTelsucursal() {
        return telsucursal;
    }
    
    /**
     * Setter for property telsucursal.
     * @param telsucursal New value of property telsucursal.
     */
    public void setTelsucursal(java.lang.String telsucursal) {
        this.telsucursal = telsucursal;
    }
    
    /**
     * Getter for property fechadespacho.
     * @return Value of property fechadespacho.
     */
    public java.lang.String getFechadespacho() {
        return fechadespacho;
    }
    
    /**
     * Setter for property fechadespacho.
     * @param fechadespacho New value of property fechadespacho.
     */
    public void setFechadespacho(java.lang.String fechadespacho) {
        this.fechadespacho = fechadespacho;
    }
    
    /**
     * Getter for property horadespacho.
     * @return Value of property horadespacho.
     */
    public java.lang.String getHoradespacho() {
        return horadespacho;
    }
    
    /**
     * Setter for property horadespacho.
     * @param horadespacho New value of property horadespacho.
     */
    public void setHoradespacho(java.lang.String horadespacho) {
        this.horadespacho = horadespacho;
    }
    
    /**
     * Getter for property fechactual.
     * @return Value of property fechactual.
     */
    public java.lang.String getFechactual() {
        return fechactual;
    }
    
    /**
     * Setter for property fechactual.
     * @param fechactual New value of property fechactual.
     */
    public void setFechactual(java.lang.String fechactual) {
        this.fechactual = fechactual;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property nomcond.
     * @return Value of property nomcond.
     */
    public java.lang.String getNomcond() {
        return nomcond;
    }
    
    /**
     * Setter for property nomcond.
     * @param nomcond New value of property nomcond.
     */
    public void setNomcond(java.lang.String nomcond) {
        this.nomcond = nomcond;
    }
    
    /**
     * Getter for property cedcond.
     * @return Value of property cedcond.
     */
    public java.lang.String getCedcond() {
        return cedcond;
    }
    
    /**
     * Setter for property cedcond.
     * @param cedcond New value of property cedcond.
     */
    public void setCedcond(java.lang.String cedcond) {
        this.cedcond = cedcond;
    }
    
    /**
     * Getter for property celularcond.
     * @return Value of property celularcond.
     */
    public java.lang.String getCelularcond() {
        return celularcond;
    }
    
    /**
     * Setter for property celularcond.
     * @param celularcond New value of property celularcond.
     */
    public void setCelularcond(java.lang.String celularcond) {
        this.celularcond = celularcond;
    }
    
    /**
     * Getter for property piezas.
     * @return Value of property piezas.
     */
    public java.lang.String getPiezas() {
        return piezas;
    }
    
    /**
     * Setter for property piezas.
     * @param piezas New value of property piezas.
     */
    public void setPiezas(java.lang.String piezas) {
        this.piezas = piezas;
    }
    
    /**
     * Getter for property peso.
     * @return Value of property peso.
     */
    public java.lang.String getPeso() {
        return peso;
    }
    
    /**
     * Setter for property peso.
     * @param peso New value of property peso.
     */
    public void setPeso(java.lang.String peso) {
        this.peso = peso;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property centrocosto.
     * @return Value of property centrocosto.
     */
    public java.lang.String getCentrocosto() {
        return centrocosto;
    }
    
    /**
     * Setter for property centrocosto.
     * @param centrocosto New value of property centrocosto.
     */
    public void setCentrocosto(java.lang.String centrocosto) {
        this.centrocosto = centrocosto;
    }
    
    /**
     * Getter for property compensac.
     * @return Value of property compensac.
     */
    public java.lang.String getCompensac() {
        return compensac;
    }
    
    /**
     * Setter for property compensac.
     * @param compensac New value of property compensac.
     */
    public void setCompensac(java.lang.String compensac) {
        this.compensac = compensac;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property telorigen.
     * @return Value of property telorigen.
     */
    public java.lang.String getTelorigen() {
        return telorigen;
    }
    
    /**
     * Setter for property telorigen.
     * @param telorigen New value of property telorigen.
     */
    public void setTelorigen(java.lang.String telorigen) {
        this.telorigen = telorigen;
    }
    
    /**
     * Getter for property contactoorigen.
     * @return Value of property contactoorigen.
     */
    public java.lang.String getContactoorigen() {
        return contactoorigen;
    }
    
    /**
     * Setter for property contactoorigen.
     * @param contactoorigen New value of property contactoorigen.
     */
    public void setContactoorigen(java.lang.String contactoorigen) {
        this.contactoorigen = contactoorigen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property dirdestino.
     * @return Value of property dirdestino.
     */
    public java.lang.String getDirdestino() {
        return dirdestino;
    }
    
    /**
     * Setter for property dirdestino.
     * @param dirdestino New value of property dirdestino.
     */
    public void setDirdestino(java.lang.String dirdestino) {
        this.dirdestino = dirdestino;
    }
    
    /**
     * Getter for property teldestino.
     * @return Value of property teldestino.
     */
    public java.lang.String getTeldestino() {
        return teldestino;
    }
    
    /**
     * Setter for property teldestino.
     * @param teldestino New value of property teldestino.
     */
    public void setTeldestino(java.lang.String teldestino) {
        this.teldestino = teldestino;
    }
    
    /**
     * Getter for property contactodestino.
     * @return Value of property contactodestino.
     */
    public java.lang.String getContactodestino() {
        return contactodestino;
    }
    
    /**
     * Setter for property contactodestino.
     * @param contactodestino New value of property contactodestino.
     */
    public void setContactodestino(java.lang.String contactodestino) {
        this.contactodestino = contactodestino;
    }
    
    /**
     * Getter for property elementodo.
     * @return Value of property elementodo.
     */
    public java.lang.String getElementodo() {
        return elementodo;
    }
    
    /**
     * Setter for property elementodo.
     * @param elementodo New value of property elementodo.
     */
    public void setElementodo(java.lang.String elementodo) {
        this.elementodo = elementodo;
    }
    
    /**
     * Getter for property po.
     * @return Value of property po.
     */
    public java.lang.String getPo() {
        return po;
    }
    
    /**
     * Setter for property po.
     * @param po New value of property po.
     */
    public void setPo(java.lang.String po) {
        this.po = po;
    }
    
    /**
     * Getter for property contenido.
     * @return Value of property contenido.
     */
    public java.lang.String getContenido() {
        return contenido;
    }
    
    /**
     * Setter for property contenido.
     * @param contenido New value of property contenido.
     */
    public void setContenido(java.lang.String contenido) {
        this.contenido = contenido;
    }
    
    /**
     * Getter for property observaciones.
     * @return Value of property observaciones.
     */
    public java.lang.String getObservaciones() {
        return observaciones;
    }
    
    /**
     * Setter for property observaciones.
     * @param observaciones New value of property observaciones.
     */
    public void setObservaciones(java.lang.String observaciones) {
        this.observaciones = observaciones;
    }
    
    /**
     * Getter for property nombreenvio.
     * @return Value of property nombreenvio.
     */
    public java.lang.String getNombreenvio() {
        return nombreenvio;
    }
    
    /**
     * Setter for property nombreenvio.
     * @param nombreenvio New value of property nombreenvio.
     */
    public void setNombreenvio(java.lang.String nombreenvio) {
        this.nombreenvio = nombreenvio;
    }
    
    /**
     * Getter for property dirorigen.
     * @return Value of property dirorigen.
     */
    public java.lang.String getDirorigen() {
        return dirorigen;
    }
    
    /**
     * Setter for property dirorigen.
     * @param dirorigen New value of property dirorigen.
     */
    public void setDirorigen(java.lang.String dirorigen) {
        this.dirorigen = dirorigen;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property auxfecha.
     * @return Value of property auxfecha.
     */
    public java.lang.String getAuxfecha() {
        return auxfecha;
    }
    
    /**
     * Setter for property auxfecha.
     * @param auxfecha New value of property auxfecha.
     */
    public void setAuxfecha(java.lang.String auxfecha) {
        this.auxfecha = auxfecha;
    }
    
    /**
     * Getter for property DesRemision.
     * @return Value of property DesRemision.
     */
    public java.lang.String getDesRemision() {
        return DesRemision;
    }
    
    /**
     * Setter for property DesRemision.
     * @param DesRemision New value of property DesRemision.
     */
    public void setDesRemision(java.lang.String DesRemision) {
        this.DesRemision = DesRemision;
    }
    
}
