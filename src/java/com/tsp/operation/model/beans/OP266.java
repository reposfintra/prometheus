/***************************************
    * Nombre Clase ............. MigrarOP266DAO.java
    * Descripci�n  .. . . . . .  Permite Guardar inf. de la OP para migrar 266
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.beans;


import java.io.*;
import java.util.*;


public class OP266 {
    
    
    private String distrito      = "";
    private String oc            = "";
    private String banco         = "";
    private String sucursal      = "";
    private String grabador      = "";
    private String placa         = "";
    private String op            = "";
    private String fechaCumplido = "";
    private String fechaPago     = "";
    private String valor         = "";
    private String moneda        = "";
    private String valICA        = "";
    
    private List   items;
    
    
    
    public OP266() {
        items = null;
    }
    
    
    
    
     // SET:
    
    public void setDistrito      (String val){  this.distrito      = val;  }
    public void setOc            (String val){  this.oc            = val;  }
    public void setBanco         (String val){  this.banco         = val;  }
    public void setSucursal      (String val){  this.sucursal      = val;  }
    public void setGrabador      (String val){  this.grabador      = val;  }    
    public void setPlaca         (String val){  this.placa         = val;  }
    public void setOP            (String val){  this.op            = val;  }
    public void setFechaCumplido (String val){  this.fechaCumplido = val;  }
    public void setFechaPago     (String val){  this.fechaPago     = val;  }
    public void setValor         (String val){  this.valor         = val;  }    
    public void setMoneda        (String val){  this.moneda        = val;  }
    public void setVlrIca        (String val){  this.valICA        = val;  }
    public void setItems         (List list ){  this.items         = list; }
    
    
    
    // GET:    
    
    
    public String getDistrito      (){  return this.distrito      ;  }
    public String getOc            (){  return this.oc            ;  }
    public String getBanco         (){  return this.banco         ;  }
    public String getSucursal      (){  return this.sucursal      ;  }
    public String getGrabador      (){  return this.grabador      ;  }    
    public String getPlaca         (){  return this.placa         ;  }
    public String getOP            (){  return this.op            ;  }
    public String getFechaCumplido (){  return this.fechaCumplido ;  }
    public String getFechaPago     (){  return this.fechaPago     ;  }
    public String getValor         (){  return this.valor         ;  }    
    public String getMoneda        (){  return this.moneda        ;  }
    public String getVlrIca        (){  return this.valICA        ;  }
    public List   getItems         (){  return this.items         ;  }
    
    
    
}
