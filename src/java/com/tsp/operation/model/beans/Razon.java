package com.tsp.operation.model.beans;

/**
 * Bean para la tabla wsdc.razon<br/>
 * 28/11/2011
 * @author darrieta
 */
public class Razon {
    
    private int id;
    private int score_id;
    private String codigo;
    private String creationUser;
    private String userUpdate;
    
    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of codigo
     *
     * @return the value of codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Set the value of codigo
     *
     * @param codigo new value of codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    /**
     * Get the value of score_id
     *
     * @return the value of score_id
     */
    public int getScore_id() {
        return score_id;
    }

    /**
     * Set the value of score_id
     *
     * @param score_id new value of score_id
     */
    public void setScore_id(int score_id) {
        this.score_id = score_id;
    }


    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }

}
