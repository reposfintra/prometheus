/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class FintraSoporteBeansFacturacion {

    public String id;
    public String cod_rop;
    public String cod_rop_barcode;
    public String generado_el;
    public String vencimiento_rop;
    public String negocio;
    public String cedula;
    public String nombre_cliente;
    public String direccion;
    public String departamento;
    public String ciudad;
    public String barrio;
    public String agencia;
    public String linea_producto;
    public String cuotas_vencidas;
    public String cuotas_pendientes;
    public String dias_vencidos;
    public String fch_ultimo_pago;
    public String subtotal_rop;
    public String total_sanciones;
    public String total_descuentos;
    public String total_rop;
    public String total_abonos;
    public String observacion;
    public String msg_paguese_antes;
    public String msg_estado_credito;
    public String capital;
    public String interes_financiacion;    
    public String seguro;
    public String interes_xmora;
    public String gastos_cobranza;
    public String dscto_capital;
    public String dscto_interes_financiacion;
    public String dscto_interes_xmora;    
    public String dscto_gastos_cobranza;
    public String dscto_seguro;
    public String ksubtotal_corriente;
    public String ksubtotal_vencido;
    public String ksubtotalneto;
    public String kdescuentos;    
    public String ktotal;
    public String Items;
    public String establecimiento_comercio;
    public String telefono;
    public String email;
    public String extracto_email;
    public String generado;
    public String periodo;
    public String ciclo;
    public String lote;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCod_rop() {
        return cod_rop;
    }

    public void setCod_rop(String cod_rop) {
        this.cod_rop = cod_rop;
    }

    public String getCod_rop_barcode() {
        return cod_rop_barcode;
    }

    public void setCod_rop_barcode(String cod_rop_barcode) {
        this.cod_rop_barcode = cod_rop_barcode;
    }

    public String getGenerado_el() {
        return generado_el;
    }

    public void setGenerado_el(String generado_el) {
        this.generado_el = generado_el;
    }

    public String getVencimiento_rop() {
        return vencimiento_rop;
    }

    public void setVencimiento_rop(String vencimiento_rop) {
        this.vencimiento_rop = vencimiento_rop;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getLinea_producto() {
        return linea_producto;
    }

    public void setLinea_producto(String linea_producto) {
        this.linea_producto = linea_producto;
    }

    public String getCuotas_vencidas() {
        return cuotas_vencidas;
    }

    public void setCuotas_vencidas(String cuotas_vencidas) {
        this.cuotas_vencidas = cuotas_vencidas;
    }

    public String getCuotas_pendientes() {
        return cuotas_pendientes;
    }

    public void setCuotas_pendientes(String cuotas_pendientes) {
        this.cuotas_pendientes = cuotas_pendientes;
    }

    public String getDias_vencidos() {
        return dias_vencidos;
    }

    public void setDias_vencidos(String dias_vencidos) {
        this.dias_vencidos = dias_vencidos;
    }

    public String getFch_ultimo_pago() {
        return fch_ultimo_pago;
    }

    public void setFch_ultimo_pago(String fch_ultimo_pago) {
        this.fch_ultimo_pago = fch_ultimo_pago;
    }

    public String getSubtotal_rop() {
        return subtotal_rop;
    }

    public void setSubtotal_rop(String subtotal_rop) {
        this.subtotal_rop = subtotal_rop;
    }

    public String getTotal_sanciones() {
        return total_sanciones;
    }

    public void setTotal_sanciones(String total_sanciones) {
        this.total_sanciones = total_sanciones;
    }

    public String getTotal_descuentos() {
        return total_descuentos;
    }

    public void setTotal_descuentos(String total_descuentos) {
        this.total_descuentos = total_descuentos;
    }

    public String getTotal_rop() {
        return total_rop;
    }

    public void setTotal_rop(String total_rop) {
        this.total_rop = total_rop;
    }

    public String getTotal_abonos() {
        return total_abonos;
    }

    public void setTotal_abonos(String total_abonos) {
        this.total_abonos = total_abonos;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getMsg_paguese_antes() {
        return msg_paguese_antes;
    }

    public void setMsg_paguese_antes(String msg_paguese_antes) {
        this.msg_paguese_antes = msg_paguese_antes;
    }

    public String getMsg_estado_credito() {
        return msg_estado_credito;
    }

    public void setMsg_estado_credito(String msg_estado_credito) {
        this.msg_estado_credito = msg_estado_credito;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getInteres_financiacion() {
        return interes_financiacion;
    }

    public void setInteres_financiacion(String interes_financiacion) {
        this.interes_financiacion = interes_financiacion;
    }

    public String getSeguro() {
        return seguro;
    }

    public void setSeguro(String seguro) {
        this.seguro = seguro;
    }

    public String getInteres_xmora() {
        return interes_xmora;
    }

    public void setInteres_xmora(String interes_xmora) {
        this.interes_xmora = interes_xmora;
    }

    public String getGastos_cobranza() {
        return gastos_cobranza;
    }

    public void setGastos_cobranza(String gastos_cobranza) {
        this.gastos_cobranza = gastos_cobranza;
    }

    public String getDscto_capital() {
        return dscto_capital;
    }

    public void setDscto_capital(String dscto_capital) {
        this.dscto_capital = dscto_capital;
    }

    public String getDscto_interes_financiacion() {
        return dscto_interes_financiacion;
    }

    public void setDscto_interes_financiacion(String dscto_interes_financiacion) {
        this.dscto_interes_financiacion = dscto_interes_financiacion;
    }

    public String getDscto_interes_xmora() {
        return dscto_interes_xmora;
    }

    public void setDscto_interes_xmora(String dscto_interes_xmora) {
        this.dscto_interes_xmora = dscto_interes_xmora;
    }

    public String getDscto_gastos_cobranza() {
        return dscto_gastos_cobranza;
    }

    public void setDscto_gastos_cobranza(String dscto_gastos_cobranza) {
        this.dscto_gastos_cobranza = dscto_gastos_cobranza;
    }

    public String getDscto_seguro() {
        return dscto_seguro;
    }

    public void setDscto_seguro(String dscto_seguro) {
        this.dscto_seguro = dscto_seguro;
    }

    public String getKsubtotal_corriente() {
        return ksubtotal_corriente;
    }

    public void setKsubtotal_corriente(String ksubtotal_corriente) {
        this.ksubtotal_corriente = ksubtotal_corriente;
    }

    public String getKsubtotal_vencido() {
        return ksubtotal_vencido;
    }

    public void setKsubtotal_vencido(String ksubtotal_vencido) {
        this.ksubtotal_vencido = ksubtotal_vencido;
    }

    public String getKsubtotalneto() {
        return ksubtotalneto;
    }

    public void setKsubtotalneto(String ksubtotalneto) {
        this.ksubtotalneto = ksubtotalneto;
    }

    public String getKdescuentos() {
        return kdescuentos;
    }

    public void setKdescuentos(String kdescuentos) {
        this.kdescuentos = kdescuentos;
    }

    public String getKtotal() {
        return ktotal;
    }

    public void setKtotal(String ktotal) {
        this.ktotal = ktotal;
    }

    public String getItems() {
        return Items;
    }

    public void setItems(String Items) {
        this.Items = Items;
    }

    public String getEstablecimiento_comercio() {
        return establecimiento_comercio;
    }

    public void setEstablecimiento_comercio(String establecimiento_comercio) {
        this.establecimiento_comercio = establecimiento_comercio;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExtracto_email() {
        return extracto_email;
    }

    public void setExtracto_email(String extracto_email) {
        this.extracto_email = extracto_email;
    }

    public String getGenerado() {
        return generado;
    }

    public void setGenerado(String generado) {
        this.generado = generado;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

       
}
