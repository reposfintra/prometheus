package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Remesa implements Serializable {
    
    private String numrem;
    private java.util.Date fecrem;
    private java.util.Date fechacargue;
    private String agcrem;
    private String orirem;
    private String desrem;
    private String cliente;
    private String destinatario;
    private String tipoviaje;
    private String cia;
    private String remitente;
    private float pesoreal;
    private String unit_of_work;
    private java.util.Date fecremori;
    private String std_job_no;
    private String descripcion;
    private String unidcam;
    private String usuario;
    private float vlrrem;
    private String docinterno;
    private String observacion;
    private String Oc;
    private String Placa;
    private String Conductor;
    private String DirRemitente;
    private String DirDestinatario;
    private String CiuRemitente;
    private String CiuDestinatario;
    private String DerechosCedido;
    private String Printer_Date;
    private String feccum;
    private String remision;
    private String faccial;
    private float cantreal;
    private float porcentaje;
    private String unidad = "";
    private String padre ="";
    private String facturable="S";
    
    private String starem;
    private String docuinterno;
    private String facturacial;
    private String lastupdate;
    private float vlrrem2;
    private String ultreporte;
    private String demoras;
    private String crossdocking="N";
    private String estado;
    private String unit;
    private String ot_padre;
    private String ot_rela;
    private String tieneplanilla;
    private String plan_str_date;
    private String reg_status;
    private String printer_date;
    private String derechos_cedido;
    private String creation_date;
    private String base;
    private String codtipocarga;
    private float qty_value;
    private float qty_packed;
    private float qty_value_received;
    private float qty_packed_received;
    private String unit_packed;
    private String corte;
    
    private String ndest;
    private String nrem;
    private String distrito;
    private String currency;
    private float tarifa;
    private double vlr_pesos;
    
    private String contenedor;
    //Henry
    private String codcli;
    private String nfacturable="N";
    private String numpla;
    private String moneda;
    
    //jose 211005
    private String tipo_documento;
    private String documento;
    private String tipo_doc_rel;
    private String nombre_tipo_doc_rel;
    private String nombre_tipo_doc;
    private String documento_rel;
    private String nom_destinatario;
    private String fec_anul="";
    
    private String fec_sal_imp;
    private String fec_lle_imp;
    
    private String nombre_cli="";
    private String nombre_ciu="";
    private String aduana="N";
    private String fec_fron;
    private String fec_sal_fron;
    private String fec_ori_fron;
    private String fec_fron_des;
    private String tiem_viaje;
    private String tiem_fron;
    private String fec_fin;
    
    private String cadena;
    
    //MFontalvo 18.01.06
    private String fiduciaria;
    private String destinoRelacionado;
    private String nombreOrigen;
    private String nombreDestino;
    private String nombreDestinoRelacionado;
    private String mes;
    private String std_job_desc;
    private Vector planillas;
    private double valorRemesa;
    private double valorTotalPlanillas;
    private double tasa;
    private String account_code_i;
    private String account_code_c;
    private String unidad_i;
    private String unidad_c;
    
    //DBASTIDAS 20-04-2006
    private String cant_cumplida;
    private String unidad_cumplida;
    private String usuario_cumple;
    private String cumplida;
    
    private String tipoCarga;
    
    private String origenOC;//Origen de la planilla Asociada
    private String destinoOC;//Destino de la planilla Asociada
    
    private String factura;
    private String pagador;
    
    private String fecha_factura;
    private Vector referencias;
    
     private String fecha_cargue;
    private String fecha_realcargue;
    
    private String cmc;
    
     // mfontalvo 20070309
    private int dias_facturacion;
    private int dias_pago;
    private java.util.Date fecha_corte;
    private double valor_facturacion;
    private double valor_pago;
    private double valor_itemf;
    
    private String fecha_contabilidad;

 
    /**
     * Getter for property dias_facturacion.
     * @return Value of property dias_facturacion.
     */
    public int getDias_facturacion() {
        return dias_facturacion;
    }
    
    /**
     * Setter for property dias_facturacion.
     * @param dias_facturacion New value of property dias_facturacion.
     */
    public void setDias_facturacion(int dias_facturacion) {
        this.dias_facturacion = dias_facturacion;
    }
    
    /**
     * Getter for property dias_pago.
     * @return Value of property dias_pago.
     */
    public int getDias_pago() {
        return dias_pago;
    }
    
    /**
     * Setter for property dias_pago.
     * @param dias_pago New value of property dias_pago.
     */
    public void setDias_pago(int dias_pago) {
        this.dias_pago = dias_pago;
    }
    
    /**
     * Getter for property valor_facturacion.
     * @return Value of property valor_facturacion.
     */
    public double getValor_facturacion() {
        return valor_facturacion;
    }
    
    /**
     * Setter for property valor_facturacion.
     * @param valor_facturacion New value of property valor_facturacion.
     */
    public void setValor_facturacion(double valor_facturacion) {
        this.valor_facturacion = valor_facturacion;
    }
    
    /**
     * Getter for property valor_pago.
     * @return Value of property valor_pago.
     */
    public double getValor_pago() {
        return valor_pago;
    }
    
    /**
     * Setter for property valor_pago.
     * @param valor_pago New value of property valor_pago.
     */
    public void setValor_pago(double valor_pago) {
        this.valor_pago = valor_pago;
    }
    
    /**
     * Getter for property valor_itemf.
     * @return Value of property valor_itemf.
     */
    public double getValor_itemf() {
        return valor_itemf;
    }
    
    /**
     * Setter for property valor_itemf.
     * @param valor_itemf New value of property valor_itemf.
     */
    public void setValor_itemf(double valor_itemf) {
        this.valor_itemf = valor_itemf;
    }
    
    
    
    /**
     * Getter for property planillas.
     * @return Value of property planillas.
     */
    public java.util.Vector getPlanillas() {
        return planillas;
    }
    
    /**
     * Setter for property planillas.
     * @param planillas New value of property planillas.
     */
    public void setPlanillas(java.util.Vector planillas) {
        this.planillas = planillas;
    }
    
    /**
     * Getter for property valorTotalPlanillas.
     * @return Value of property valorTotalPlanillas.
     */
    public double getValorTotalPlanillas() {
        return valorTotalPlanillas;
    }
    
    /**
     * Setter for property valorTotalPlanillas.
     * @param valorTotalPlanillas New value of property valorTotalPlanillas.
     */
    public void setValorTotalPlanillas(double valorTotalPlanillas) {
        this.valorTotalPlanillas = valorTotalPlanillas;
    }
    
    /**
     * Getter for property std_job_desc.
     * @return Value of property std_job_desc.
     */
    public java.lang.String getStd_job_desc() {
        return std_job_desc;
    }
    
    /**
     * Setter for property std_job_desc.
     * @param std_job_desc New value of property std_job_desc.
     */
    public void setStd_job_desc(java.lang.String std_job_desc) {
        this.std_job_desc = std_job_desc;
    }
    
    /**
     * Getter for property destinoRelacionado.
     * @return Value of property destinoRelacionado.
     */
    public java.lang.String getDestinoRelacionado() {
        return destinoRelacionado;
    }
    
    /**
     * Setter for property destinoRelacionado.
     * @param destinoRelacionado New value of property destinoRelacionado.
     */
    public void setDestinoRelacionado(java.lang.String destinoRelacionado) {
        this.destinoRelacionado = destinoRelacionado;
    }
    
    /**
     * Getter for property nombreOrigen.
     * @return Value of property nombreOrigen.
     */
    public java.lang.String getNombreOrigen() {
        return nombreOrigen;
    }
    
    /**
     * Setter for property nombreOrigen.
     * @param nombreOrigen New value of property nombreOrigen.
     */
    public void setNombreOrigen(java.lang.String nombreOrigen) {
        this.nombreOrigen = nombreOrigen;
    }
    
    /**
     * Getter for property nombreDestino.
     * @return Value of property nombreDestino.
     */
    public java.lang.String getNombreDestino() {
        return nombreDestino;
    }
    
    /**
     * Setter for property nombreDestino.
     * @param nombreDestino New value of property nombreDestino.
     */
    public void setNombreDestino(java.lang.String nombreDestino) {
        this.nombreDestino = nombreDestino;
    }
    
    /**
     * Getter for property nombreDestinoRelacionado.
     * @return Value of property nombreDestinoRelacionado.
     */
    public java.lang.String getNombreDestinoRelacionado() {
        return nombreDestinoRelacionado;
    }
    
    /**
     * Setter for property nombreDestinoRelacionado.
     * @param nombreDestinoRelacionado New value of property nombreDestinoRelacionado.
     */
    public void setNombreDestinoRelacionado(java.lang.String nombreDestinoRelacionado) {
        this.nombreDestinoRelacionado = nombreDestinoRelacionado;
    }
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property valorRemesa.
     * @return Value of property valorRemesa.
     */
    public double getValorRemesa() {
        return valorRemesa;
    }
    
    /**
     * Setter for property valorRemesa.
     * @param valorRemesa New value of property valorRemesa.
     */
    public void setValorRemesa(double valorRemesa) {
        this.valorRemesa = valorRemesa;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
    
    public static Remesa load(ResultSet rs)throws SQLException{
        
        Remesa remesa = new Remesa();
        
        remesa.setNumRem(rs.getString("numrem"));
        remesa.setFecRem(rs.getTimestamp("fecrem"));
        remesa.setAgcRem(rs.getString("agcrem"));
        remesa.setOriRem(rs.getString("orirem"));
        remesa.setDesRem(rs.getString("desrem"));
        remesa.setCliente(rs.getString("nomcli"));
        remesa.setDestinatario(rs.getString("destinatario"));
        remesa.setTipoViaje(rs.getString("tipoviaje"));
        remesa.setCia(rs.getString("cia"));
        remesa.setRemitente(rs.getString("remitente"));
        remesa.setPesoReal(rs.getFloat("pesoreal"));
        remesa.setUnitOfWork(rs.getString("unit_of_work"));
        remesa.setFecRemOri(rs.getTimestamp("fecremori"));
        remesa.setStdJobNo(rs.getString("std_job_no"));
        remesa.setDescripcion(rs.getString("descripcion"));
        remesa.setUnidcam(rs.getString("unidcam"));
        remesa.setUsuario(rs.getString("usuario"));
        remesa.setDocInterno("");
        remesa.setObservacion(rs.getString("observacion"));
        remesa.setVlrRem(rs.getFloat("vlrrem"));
        remesa.setCodcli(rs.getString("codcli"));
        
        return remesa;
        
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    
    
    public void setContenedor(String val){
        this.contenedor = val;
    }
    
    public void setOc(String valor) {
        this.Oc = valor;
    }
    
    public void setPlaca(String valor) {
        this.Placa = valor;
    }
    
    public void setConductor(String valor) {
        this.Conductor = valor;
    }
    
    public void setDirRemitente(String valor) {
        this.DirRemitente = valor;
    }
    
    public void setDirDestinatario(String valor) {
        this.DirDestinatario = valor;
    }
    
    public void setCiuDestinatario(String valor) {
        this.CiuDestinatario = valor;
    }
    
    public void setCiuRemitente(String valor) {
        this.CiuRemitente = valor;
    }
    
    public void setDerechosCedido(String valor) {
        this.DerechosCedido = valor;
    }
    
    public void setPrinter_Date(String valor) {
        this.Printer_Date = valor;
    }
    
    
    
    
    
    //----
    
    public String  getContenedor(){
        return this.contenedor;
    }
    
    public String getOc() {
        return this.Oc;
    }
    
    public String getPlaca() {
        return this.Placa;
    }
    
    public String getConductor() {
        return this.Conductor;
    }
    
    public String getDirRemitente() {
        return this.DirRemitente;
    }
    
    public String getDirDestinatario() {
        return this.DirDestinatario;
    }
    
    public String getCiuRemitente() {
        return this.CiuRemitente;
    }
    
    public String getCiuDestinatario() {
        return this.CiuDestinatario;
    }
    
    public String getDerechosCedido() {
        return this.DerechosCedido;
    }
    
    public String getPrinter_Date() {
        return this.Printer_Date;
    }
    public void setVlrRem(float vlrrem){
        
        this.vlrrem=vlrrem;
    }
    
    public float getVlrRem(){
        
        return vlrrem;
    }
    
    public void setObservacion(String observacion){
        
        this.observacion=observacion;
    }
    
    public String getObservacion(){
        
        return observacion;
    }
    public void setRemision(String remision){
        
        this.remision=remision;
    }
    
    public String getRemision(){
        
        return remision;
    }
    
    public void setDocInterno(String docinterno){
        
        this.docinterno=docinterno;
    }
    
    public String getDocInterno(){
        
        return docinterno;
    }
    
    public void setNumRem(String numrem){
        
        this.numrem=numrem;
    }
    
    public String getNumrem(){
        
        return numrem;
    }
    public void setUsuario(String usuario){
        
        this.usuario=usuario;
    }
    
    public String getUsuario(){
        
        return usuario;
    }
    public void setFeccum(String feccum){
        
        this.feccum=feccum;
    }
    
    public String getFeccum(){
        
        return feccum;
    }
    public void setUnidcam(String unidcam){
        
        this.unidcam=unidcam;
    }
    
    public String getUnidcam(){
        
        return unidcam;
    }
    
    public void	setFecRem(java.util.Date fecrem){
        
        this.fecrem=fecrem;
    }
    
    public java.util.Date getFecRem(){
        
        return fecrem;
    }
    
    public void setFechaCargue(java.util.Date fechacargue){
        
        this.fechacargue=fechacargue;
    }
    
    public java.util.Date getFechaCargue(){
        
        return fechacargue;
    }
    
    public void setAgcRem(String agcrem){
        
        this.agcrem=agcrem;
    }
    
    public String getAgcRem(){
        
        return agcrem;
    }
    
    public void setOriRem(String orirem){
        
        this.orirem=orirem;
    }
    
    public String getOriRem(){
        
        return orirem;
    }
    
    public void setDesRem(String desrem){
        
        this.desrem=desrem;
    }
    
    public String getDesRem(){
        
        return desrem;
    }
    
    public void setCliente(String cliente){
        
        this.cliente=cliente;
    }
    
    public String getCliente(){
        
        return cliente;
    }
    
    public void setDestinatario(String destinatario){
        
        this.destinatario=destinatario;
    }
    
    public String getDestinatario(){
        
        return destinatario;
    }
    
    public void setTipoViaje(String tipoviaje){
        
        this.tipoviaje=tipoviaje;
    }
    
    public String getTipoViaje(){
        
        return tipoviaje;
    }
    
    public void setCia(String cia){
        
        this.cia=cia;
    }
    
    public String getCia(){
        
        return cia;
    }
    
    public void setRemitente(String remitente){
        
        this.remitente=remitente;
    }
    
    public String getRemitente(){
        
        return remitente;
    }
    
    public void setPesoReal(float pesoreal){
        
        this.pesoreal=pesoreal;
    }
    
    public float getPesoReal(){
        
        return pesoreal;
    }
    
    public void setUnitOfWork(String unit_of_work){
        
        this.unit_of_work=unit_of_work;
    }
    
    public String getUnitOfWork(){
        
        return unit_of_work;
    }
    
    public void setFecRemOri(java.util.Date fecremori){
        
        this.fecremori=fecremori;
    }
    
    public java.util.Date getFecRemOri(){
        
        return fecremori;
    }
    
    public void setStdJobNo(String std_job_no){
        
        this.std_job_no=std_job_no;
    }
    
    public String getStdJobNo(){
        
        return std_job_no;
    }
    
    public void setDescripcion(String descripcion){
        
        this.descripcion=descripcion;
    }
    
    public String getDescripcion(){
        
        return descripcion;
    }
    
    /**
     * Getter for property faccial.
     * @return Value of property faccial.
     */
    public java.lang.String getFaccial() {
        return faccial;
    }
    
    /**
     * Setter for property faccial.
     * @param faccial New value of property faccial.
     */
    public void setFaccial(java.lang.String faccial) {
        this.faccial = faccial;
    }
    
    /**
     * Getter for property cantreal.
     * @return Value of property cantreal.
     */
    public float getCantreal() {
        return cantreal;
    }
    
    /**
     * Setter for property cantreal.
     * @param cantreal New value of property cantreal.
     */
    public void setCantreal(float cantreal) {
        this.cantreal = cantreal;
    }
    
    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property porcentaje.
     * @return Value of property porcentaje.
     */
    public float getPorcentaje() {
        return porcentaje;
    }
    
    /**
     * Setter for property porcentaje.
     * @param porcentaje New value of property porcentaje.
     */
    public void setPorcentaje(float porcentaje) {
        this.porcentaje = porcentaje;
    }
    
    /**
     * Getter for property padre.
     * @return Value of property padre.
     */
    public java.lang.String getPadre() {
        return padre;
    }
    
    /**
     * Setter for property padre.
     * @param padre New value of property padre.
     */
    public void setPadre(java.lang.String padre) {
        this.padre = padre;
    }
    
    /**
     * Getter for property facturable.
     * @return Value of property facturable.
     */
    public java.lang.String getFacturable() {
        return facturable;
    }
    
    /**
     * Setter for property facturable.
     * @param facturable New value of property facturable.
     */
    public void setFacturable(java.lang.String facturable) {
        this.facturable = facturable;
    }
    public String getUltreporte() {
        return ultreporte;
    }
    
    public String getStarem() {
        return starem;
    }
    
    
    public String getOt_rela() {
        return ot_rela;
    }
    
    
    public String getOrirem() {
        return orirem;
    }
    
    
    public String getDerechos_cedido() {
        return derechos_cedido;
    }
    
    
    
    public String getDemoras() {
        return demoras;
    }
    
    public String getPlan_str_date() {
        return plan_str_date;
    }
    
    public String getTipoviaje() {
        return tipoviaje;
    }
    
    public String getStd_job_no() {
        return std_job_no;
    }
    
    public String getUnit() {
        return unit;
    }
    
    public String getFacturacial() {
        return facturacial;
    }
    
    public String getOt_padre() {
        return ot_padre;
    }
    
    public String getTieneplanilla() {
        return tieneplanilla;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public String getReg_status() {
        return reg_status;
    }
    
    public String getCodtipocarga() {
        return codtipocarga;
    }
    
    public String getAgcrem() {
        return agcrem;
    }
    
    public String getDesrem() {
        return desrem;
    }
    
    public String getCrossdocking() {
        return crossdocking;
    }
    
    
    public String getLastupdate() {
        return lastupdate;
    }
    
    public String getCreation_date() {
        return creation_date;
    }
    
    public String getBase() {
        return base;
    }
    
    
    public String getUnit_packed() {
        return unit_packed;
    }
    
    public String getDocuinterno() {
        return docuinterno;
    }
    
    
    public String getPrinter_date() {
        return printer_date;
    }
    
    public void setCorte( String corte ) {
        this.corte = corte;
    }
    
    public void setUltreporte( String ultreporte ) {
        this.ultreporte = ultreporte;
    }
    
    public void setStarem( String starem ) {
        this.starem = starem;
    }
    
    
    public void setOt_rela( String ot_rela ) {
        this.ot_rela = ot_rela;
    }
    
    
    public void setOrirem( String orirem ) {
        this.orirem = orirem;
    }
    
    
    public void setDerechos_cedido( String derechos_cedido ) {
        this.derechos_cedido = derechos_cedido;
    }
    
    
    public void setNumrem( String numrem ) {
        this.numrem = numrem;
    }
    
    public void setDemoras( String demoras ) {
        this.demoras = demoras;
    }
    
    public void setPlan_str_date( String plan_str_date ) {
        this.plan_str_date = plan_str_date;
    }
    
    public void setTipoviaje( String tipoviaje ) {
        this.tipoviaje = tipoviaje;
    }
    
    public void setStd_job_no( String std_job_no ) {
        this.std_job_no = std_job_no;
    }
    
    
    public void setUnit( String unit ) {
        this.unit = unit;
    }
    
    public void setFacturacial( String facturacial ) {
        this.facturacial = facturacial;
    }
    
    
    
    public void setOt_padre( String ot_padre ) {
        this.ot_padre = ot_padre;
    }
    
    public void setTieneplanilla( String tieneplanilla ) {
        this.tieneplanilla = tieneplanilla;
    }
    
    public void setEstado( String estado ) {
        this.estado = estado;
    }
    
    public void setReg_status( String reg_status ) {
        this.reg_status = reg_status;
    }
    
    public void setCodtipocarga( String codtipocarga ) {
        this.codtipocarga = codtipocarga;
    }
    
    public void setAgcrem( String agcrem ) {
        this.agcrem = agcrem;
    }
    
    public void setDesrem( String desrem ) {
        this.desrem = desrem;
    }
    
    public void setCrossdocking( String crossdocking ) {
        this.crossdocking = crossdocking;
    }
    
    public void setLastupdate( String lastupdate ) {
        this.lastupdate = lastupdate;
    }
    
    
    public void setCreation_date( String creation_date ) {
        this.creation_date = creation_date;
    }
    
    public void setBase( String base ) {
        this.base = base;
    }
    
    
    public void setUnit_packed( String unit_packed ) {
        this.unit_packed = unit_packed;
    }
    
    
    public void setDocuinterno( String docuinterno ) {
        this.docuinterno = docuinterno;
    }
    
    
    public void setPrinter_date( String printer_date ) {
        this.printer_date = printer_date;
    }
    
    public void setVlrrem( float vlrrem ) {
        this.vlrrem = vlrrem;
    }
    
    public void setQty_value_received( float qty_value_received ) {
        this.qty_value_received = qty_value_received;
    }
    
    public void setQty_packed( float qty_packed ) {
        this.qty_packed = qty_packed;
    }
    
    public void setVlrrem2( float vlrrem2 ) {
        this.vlrrem2 = vlrrem2;
    }
    
    public void setQty_value( float qty_value ) {
        this.qty_value = qty_value;
    }
    
    public void setQty_packed_received( float qty_packed_received ) {
        this.qty_packed_received = qty_packed_received;
    }
    
    public String getCorte() {
        return corte;
    }
    
    public float getVlrrem() {
        return vlrrem;
    }
    
    public float getQty_value_received() {
        return qty_value_received;
    }
    
    public float getQty_packed() {
        return qty_packed;
    }
    
    public float getVlrrem2() {
        return vlrrem2;
    }
    
    public float getQty_value() {
        return qty_value;
    }
    
    public float getQty_packed_received() {
        return qty_packed_received;
    }
    
    /**
     * Getter for property ndest.
     * @return Value of property ndest.
     */
    public java.lang.String getNdest() {
        return ndest;
    }
    
    /**
     * Setter for property ndest.
     * @param ndest New value of property ndest.
     */
    public void setNdest(java.lang.String ndest) {
        this.ndest = ndest;
    }
    
    /**
     * Getter for property nrem.
     * @return Value of property nrem.
     */
    public java.lang.String getNrem() {
        return nrem;
    }
    
    /**
     * Setter for property nrem.
     * @param nrem New value of property nrem.
     */
    public void setNrem(java.lang.String nrem) {
        this.nrem = nrem;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property tarifa.
     * @return Value of property tarifa.
     */
    public float getTarifa() {
        return tarifa;
    }
    
    /**
     * Setter for property tarifa.
     * @param tarifa New value of property tarifa.
     */
    public void setTarifa(float tarifa) {
        this.tarifa = tarifa;
    }
    
    /**
     * Getter for property currency.
     * @return Value of property currency.
     */
    public java.lang.String getCurrency() {
        return currency;
    }
    
    /**
     * Setter for property currency.
     * @param currency New value of property currency.
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }
    
    /**
     * Getter for property vlr_pesos.
     * @return Value of property vlr_pesos.
     */
    public double getVlr_pesos() {
        return vlr_pesos;
    }
    
    /**
     * Setter for property vlr_pesos.
     * @param vlr_pesos New value of property vlr_pesos.
     */
    public void setVlr_pesos(double vlr_pesos) {
        this.vlr_pesos = vlr_pesos;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property nfacturable.
     * @return Value of property nfacturable.
     */
    public java.lang.String getNfacturable() {
        return nfacturable;
    }
    
    /**
     * Setter for property nfacturable.
     * @param nfacturable New value of property nfacturable.
     */
    public void setNfacturable(java.lang.String nfacturable) {
        this.nfacturable = nfacturable;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property documento_rel.
     * @return Value of property documento_rel.
     */
    public java.lang.String getDocumento_rel() {
        return documento_rel;
    }
    
    /**
     * Setter for property documento_rel.
     * @param documento_rel New value of property documento_rel.
     */
    public void setDocumento_rel(java.lang.String documento_rel) {
        this.documento_rel = documento_rel;
    }
    
    /**
     * Getter for property nombre_tipo_doc.
     * @return Value of property nombre_tipo_doc.
     */
    public java.lang.String getNombre_tipo_doc() {
        return nombre_tipo_doc;
    }
    
    /**
     * Setter for property nombre_tipo_doc.
     * @param nombre_tipo_doc New value of property nombre_tipo_doc.
     */
    public void setNombre_tipo_doc(java.lang.String nombre_tipo_doc) {
        this.nombre_tipo_doc = nombre_tipo_doc;
    }
    
    /**
     * Getter for property nombre_tipo_doc_rel.
     * @return Value of property nombre_tipo_doc_rel.
     */
    public java.lang.String getNombre_tipo_doc_rel() {
        return nombre_tipo_doc_rel;
    }
    
    /**
     * Setter for property nombre_tipo_doc_rel.
     * @param nombre_tipo_doc_rel New value of property nombre_tipo_doc_rel.
     */
    public void setNombre_tipo_doc_rel(java.lang.String nombre_tipo_doc_rel) {
        this.nombre_tipo_doc_rel = nombre_tipo_doc_rel;
    }
    
    /**
     * Getter for property nom_destinatario.
     * @return Value of property nom_destinatario.
     */
    public java.lang.String getNom_destinatario() {
        return nom_destinatario;
    }
    
    /**
     * Setter for property nom_destinatario.
     * @param nom_destinatario New value of property nom_destinatario.
     */
    public void setNom_destinatario(java.lang.String nom_destinatario) {
        this.nom_destinatario = nom_destinatario;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property tipo_doc_rel.
     * @return Value of property tipo_doc_rel.
     */
    public java.lang.String getTipo_doc_rel() {
        return tipo_doc_rel;
    }
    
    /**
     * Setter for property tipo_doc_rel.
     * @param tipo_doc_rel New value of property tipo_doc_rel.
     */
    public void setTipo_doc_rel(java.lang.String tipo_doc_rel) {
        this.tipo_doc_rel = tipo_doc_rel;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property fec_anul.
     * @return Value of property fec_anul.
     */
    public java.lang.String getFec_anul() {
        return fec_anul;
    }
    
    /**
     * Setter for property fec_anul.
     * @param fec_anul New value of property fec_anul.
     */
    public void setFec_anul(java.lang.String fec_anul) {
        this.fec_anul = fec_anul;
    }
    
    /**
     * Getter for property nombre_ciu.
     * @return Value of property nombre_ciu.
     */
    public java.lang.String getNombre_ciu() {
        return nombre_ciu;
    }
    
    /**
     * Setter for property nombre_ciu.
     * @param nombre_ciu New value of property nombre_ciu.
     */
    public void setNombre_ciu(java.lang.String nombre_ciu) {
        this.nombre_ciu = nombre_ciu;
    }
    
    /**
     * Getter for property nombre_cli.
     * @return Value of property nombre_cli.
     */
    public java.lang.String getNombre_cli() {
        return nombre_cli;
    }
    
    /**
     * Setter for property nombre_cli.
     * @param nombre_cli New value of property nombre_cli.
     */
    public void setNombre_cli(java.lang.String nombre_cli) {
        this.nombre_cli = nombre_cli;
    }
    
    /**
     * Obtiene el valor de la propiedad fiduciaria.
     * @autor mfontalvo
     * @return valor de la propiedad fiduciaria.
     */
    public java.lang.String getFiduciaria() {
        return fiduciaria;
    }
    
    /**
     * Modifica el valor de la propiedad fiduciaria.
     * @autor mfontalvo
     * @param fiduciaria nuevo valor de la propiedad  fiduciaria.
     */
    public void setFiduciaria(java.lang.String fiduciaria) {
        this.fiduciaria = fiduciaria;
    }
    
    /**
     * Getter for property aduana.
     * @return Value of property aduana.
     */
    public java.lang.String getAduana() {
        return aduana;
    }
    
    /**
     * Setter for property aduana.
     * @param aduana New value of property aduana.
     */
    public void setAduana(java.lang.String aduana) {
        this.aduana = aduana;
    }
    
    /**
     * Getter for property cant_cumplida.
     * @return Value of property cant_cumplida.
     */
    public java.lang.String getCant_cumplida() {
        return cant_cumplida;
    }
    
    /**
     * Setter for property cant_cumplida.
     * @param cant_cumplida New value of property cant_cumplida.
     */
    public void setCant_cumplida(java.lang.String cant_cumplida) {
        this.cant_cumplida = cant_cumplida;
    }
    
    /**
     * Getter for property unidad_cumplida.
     * @return Value of property unidad_cumplida.
     */
    public java.lang.String getUnidad_cumplida() {
        return unidad_cumplida;
    }
    
    /**
     * Setter for property unidad_cumplida.
     * @param unidad_cumplida New value of property unidad_cumplida.
     */
    public void setUnidad_cumplida(java.lang.String unidad_cumplida) {
        this.unidad_cumplida = unidad_cumplida;
    }
    
    /**
     * Getter for property usuario_cumple.
     * @return Value of property usuario_cumple.
     */
    public java.lang.String getUsuario_cumple() {
        return usuario_cumple;
    }
    
    /**
     * Setter for property usuario_cumple.
     * @param usuario_cumple New value of property usuario_cumple.
     */
    public void setUsuario_cumple(java.lang.String usuario_cumple) {
        this.usuario_cumple = usuario_cumple;
    }
    
    /**
     * Getter for property cumplida.
     * @return Value of property cumplida.
     */
    public java.lang.String getCumplida() {
        return cumplida;
    }
    
    /**
     * Setter for property cumplida.
     * @param cumplida New value of property cumplida.
     */
    public void setCumplida(java.lang.String cumplida) {
        this.cumplida = cumplida;
    }
    
    /**
     * Getter for property tipoCarga.
     * @return Value of property tipoCarga.
     */
    public java.lang.String getTipoCarga() {
        return tipoCarga;
    }
    
    /**
     * Setter for property tipoCarga.
     * @param tipoCarga New value of property tipoCarga.
     */
    public void setTipoCarga(java.lang.String tipoCarga) {
        this.tipoCarga = tipoCarga;
    }
    
    /**
     * Getter for property cadena.
     * @return Value of property cadena.
     */
    public java.lang.String getCadena() {
        return cadena;
    }
    
    /**
     * Setter for property cadena.
     * @param cadena New value of property cadena.
     */
    public void setCadena(java.lang.String cadena) {
        this.cadena = cadena;
    }
    
    /**
     * Getter for property account_code_i.
     * @return Value of property account_code_i.
     */
    public java.lang.String getAccount_code_i() {
        return account_code_i;
    }
    
    /**
     * Setter for property account_code_i.
     * @param account_code_i New value of property account_code_i.
     */
    public void setAccount_code_i(java.lang.String account_code_i) {
        this.account_code_i = account_code_i;
    }
    
    /**
     * Getter for property account_code_c.
     * @return Value of property account_code_c.
     */
    public java.lang.String getAccount_code_c() {
        return account_code_c;
    }
    
    /**
     * Setter for property account_code_c.
     * @param account_code_c New value of property account_code_c.
     */
    public void setAccount_code_c(java.lang.String account_code_c) {
        this.account_code_c = account_code_c;
    }
    
    /**
     * Getter for property unidad_i.
     * @return Value of property unidad_i.
     */
    public java.lang.String getUnidad_i() {
        return unidad_i;
    }
    
    /**
     * Setter for property unidad_i.
     * @param unidad_i New value of property unidad_i.
     */
    public void setUnidad_i(java.lang.String unidad_i) {
        this.unidad_i = unidad_i;
    }
    
    /**
     * Getter for property unidad_c.
     * @return Value of property unidad_c.
     */
    public java.lang.String getUnidad_c() {
        return unidad_c;
    }
    
    /**
     * Setter for property unidad_c.
     * @param unidad_c New value of property unidad_c.
     */
    public void setUnidad_c(java.lang.String unidad_c) {
        this.unidad_c = unidad_c;
    }
    
    /**
     * Getter for property docinterno.
     * @return Value of property docinterno.
     */
    public java.lang.String getDocinterno() {
        return docinterno;
    }
    
    /**
     * Setter for property docinterno.
     * @param docinterno New value of property docinterno.
     */
    public void setDocinterno(java.lang.String docinterno) {
        this.docinterno = docinterno;
    }
    
    /**
     * Getter for property fec_fin.
     * @return Value of property fec_fin.
     */
    public java.lang.String getFec_fin() {
        return fec_fin;
    }
    
    /**
     * Setter for property fec_fin.
     * @param fec_fin New value of property fec_fin.
     */
    public void setFec_fin(java.lang.String fec_fin) {
        this.fec_fin = fec_fin;
    }
    
    /**
     * Getter for property fec_fron.
     * @return Value of property fec_fron.
     */
    public java.lang.String getFec_fron() {
        return fec_fron;
    }
    
    /**
     * Setter for property fec_fron.
     * @param fec_fron New value of property fec_fron.
     */
    public void setFec_fron(java.lang.String fec_fron) {
        this.fec_fron = fec_fron;
    }
    
    /**
     * Getter for property fec_fron_des.
     * @return Value of property fec_fron_des.
     */
    public java.lang.String getFec_fron_des() {
        return fec_fron_des;
    }
    
    /**
     * Setter for property fec_fron_des.
     * @param fec_fron_des New value of property fec_fron_des.
     */
    public void setFec_fron_des(java.lang.String fec_fron_des) {
        this.fec_fron_des = fec_fron_des;
    }
    
    /**
     * Getter for property fec_ori_fron.
     * @return Value of property fec_ori_fron.
     */
    public java.lang.String getFec_ori_fron() {
        return fec_ori_fron;
    }
    
    /**
     * Setter for property fec_ori_fron.
     * @param fec_ori_fron New value of property fec_ori_fron.
     */
    public void setFec_ori_fron(java.lang.String fec_ori_fron) {
        this.fec_ori_fron = fec_ori_fron;
    }
    
    /**
     * Getter for property fec_sal_fron.
     * @return Value of property fec_sal_fron.
     */
    public java.lang.String getFec_sal_fron() {
        return fec_sal_fron;
    }
    
    /**
     * Setter for property fec_sal_fron.
     * @param fec_sal_fron New value of property fec_sal_fron.
     */
    public void setFec_sal_fron(java.lang.String fec_sal_fron) {
        this.fec_sal_fron = fec_sal_fron;
    }
    
    /**
     * Getter for property fechacargue.
     * @return Value of property fechacargue.
     */
    public java.util.Date getFechacargue() {
        return fechacargue;
    }
    
    /**
     * Setter for property fechacargue.
     * @param fechacargue New value of property fechacargue.
     */
    public void setFechacargue(java.util.Date fechacargue) {
        this.fechacargue = fechacargue;
    }
    
    /**
     * Getter for property fecrem.
     * @return Value of property fecrem.
     */
    public java.util.Date getFecrem() {
        return fecrem;
    }
    
    /**
     * Setter for property fecrem.
     * @param fecrem New value of property fecrem.
     */
    public void setFecrem(java.util.Date fecrem) {
        this.fecrem = fecrem;
    }
    
    /**
     * Getter for property fecremori.
     * @return Value of property fecremori.
     */
    public java.util.Date getFecremori() {
        return fecremori;
    }
    
    /**
     * Setter for property fecremori.
     * @param fecremori New value of property fecremori.
     */
    public void setFecremori(java.util.Date fecremori) {
        this.fecremori = fecremori;
    }
    
    /**
     * Getter for property pesoreal.
     * @return Value of property pesoreal.
     */
    public float getPesoreal() {
        return pesoreal;
    }
    
    /**
     * Setter for property pesoreal.
     * @param pesoreal New value of property pesoreal.
     */
    public void setPesoreal(float pesoreal) {
        this.pesoreal = pesoreal;
    }
    
    /**
     * Getter for property tiem_fron.
     * @return Value of property tiem_fron.
     */
    public java.lang.String getTiem_fron() {
        return tiem_fron;
    }
    
    /**
     * Setter for property tiem_fron.
     * @param tiem_fron New value of property tiem_fron.
     */
    public void setTiem_fron(java.lang.String tiem_fron) {
        this.tiem_fron = tiem_fron;
    }
    
    /**
     * Getter for property tiem_viaje.
     * @return Value of property tiem_viaje.
     */
    public java.lang.String getTiem_viaje() {
        return tiem_viaje;
    }
    
    /**
     * Setter for property tiem_viaje.
     * @param tiem_viaje New value of property tiem_viaje.
     */
    public void setTiem_viaje(java.lang.String tiem_viaje) {
        this.tiem_viaje = tiem_viaje;
    }
    
    /**
     * Getter for property unit_of_work.
     * @return Value of property unit_of_work.
     */
    public java.lang.String getUnit_of_work() {
        return unit_of_work;
    }
    
    /**
     * Setter for property unit_of_work.
     * @param unit_of_work New value of property unit_of_work.
     */
    public void setUnit_of_work(java.lang.String unit_of_work) {
        this.unit_of_work = unit_of_work;
    }
    
    /**
     * Getter for property origenOC.
     * @return Value of property origenOC.
     */
    public java.lang.String getOrigenOC() {
        return origenOC;
    }
    
    /**
     * Setter for property origenOC.
     * @param origenOC New value of property origenOC.
     */
    public void setOrigenOC(java.lang.String origenOC) {
        this.origenOC = origenOC;
    }
    
    /**
     * Getter for property destinoOC.
     * @return Value of property destinoOC.
     */
    public java.lang.String getDestinoOC() {
        return destinoOC;
    }
    
    /**
     * Setter for property destinoOC.
     * @param destinoOC New value of property destinoOC.
     */
    public void setDestinoOC(java.lang.String destinoOC) {
        this.destinoOC = destinoOC;
    }
    
    /**
     * Getter for property fec_sal_imp.
     * @return Value of property fec_sal_imp.
     */
    public java.lang.String getFec_sal_imp() {
        return fec_sal_imp;
    }
    
    /**
     * Setter for property fec_sal_imp.
     * @param fec_sal_imp New value of property fec_sal_imp.
     */
    public void setFec_sal_imp(java.lang.String fec_sal_imp) {
        this.fec_sal_imp = fec_sal_imp;
    }
    
    /**
     * Getter for property fec_lle_imp.
     * @return Value of property fec_lle_imp.
     */
    public java.lang.String getFec_lle_imp() {
        return fec_lle_imp;
    }
    
    /**
     * Setter for property fec_lle_imp.
     * @param fec_lle_imp New value of property fec_lle_imp.
     */
    public void setFec_lle_imp(java.lang.String fec_lle_imp) {
        this.fec_lle_imp = fec_lle_imp;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property pagador.
     * @return Value of property pagador.
     */
    public java.lang.String getPagador() {
        return pagador;
    }
    
    /**
     * Setter for property pagador.
     * @param pagador New value of property pagador.
     */
    public void setPagador(java.lang.String pagador) {
        this.pagador = pagador;
    }
    
    /**
     * Getter for property referencias.
     * @return Value of property referencias.
     */
    public java.util.Vector getReferencias() {
        return referencias;
    }
    
    /**
     * Setter for property referencias.
     * @param referencias New value of property referencias.
     */
    public void setReferencias(java.util.Vector referencias) {
        this.referencias = referencias;
    }
    
    /**
     * Getter for property fecha_factura.
     * @return Value of property fecha_factura.
     */
    public java.lang.String getFecha_factura() {
        return fecha_factura;
    }
    
    /**
     * Setter for property fecha_factura.
     * @param fecha_factura New value of property fecha_factura.
     */
    public void setFecha_factura(java.lang.String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }
    
    /**
     * Getter for property fecha_cargue.
     * @return Value of property fecha_cargue.
     */
    public java.lang.String getFecha_cargue() {
        return fecha_cargue;
    }
    
    /**
     * Setter for property fecha_cargue.
     * @param fecha_cargue New value of property fecha_cargue.
     */
    public void setFecha_cargue(java.lang.String fecha_cargue) {
        this.fecha_cargue = fecha_cargue;
    }
    
    /**
     * Getter for property fecha_realcargue.
     * @return Value of property fecha_realcargue.
     */
    public java.lang.String getFecha_realcargue() {
        return fecha_realcargue;
    }
    
    /**
     * Setter for property fecha_realcargue.
     * @param fecha_realcargue New value of property fecha_realcargue.
     */
    public void setFecha_realcargue(java.lang.String fecha_realcargue) {
        this.fecha_realcargue = fecha_realcargue;
    }
    
    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }
    
    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }
    
    /**
     * Getter for property fecha_corte.
     * @return Value of property fecha_corte.
     */
    public java.util.Date getFecha_corte() {
        return fecha_corte;
    }
    
    /**
     * Setter for property fecha_corte.
     * @param fecha_corte New value of property fecha_corte.
     */
    public void setFecha_corte(java.util.Date fecha_corte) {
        this.fecha_corte = fecha_corte;
    }
    
    /**
     * Getter for property fecha_contabilidad.
     * @return Value of property fecha_contabilidad.
     */
    public java.lang.String getFecha_contabilidad() {
        return fecha_contabilidad;
    }
    
    /**
     * Setter for property fecha_contabilidad.
     * @param fecha_contabilidad New value of property fecha_contabilidad.
     */
    public void setFecha_contabilidad(java.lang.String fecha_contabilidad) {
        this.fecha_contabilidad = fecha_contabilidad;
    }
    
}
