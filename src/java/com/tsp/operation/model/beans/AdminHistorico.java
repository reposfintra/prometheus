/********************************************************************
 *      Nombre Clase.................   AdminHistorico.java
 *      Descripci�n..................   Bean de la tabla admin_historicos
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import java.sql.*;
/**
 *
 * @author  dlamadrid
 */
public class AdminHistorico
{
        
        private String creationDate="";
        private String fechaInicio="";
        private String fechaFinal="";
        private String tablaO="";
        private String tablaD="";
        private String rutina="";
        private int duracion=0;
        private String estado="";
        private String usuario="";
        private String id="";
        
        /** Creates a new instance of AdminHistorico */
        public AdminHistorico ()
        {
        }
        
        public AdminHistorico (String cd,String fi,String ff,String to,String td,String r,int d,String e,String u,String id)
        {
                this.creationDate=cd;
                this.fechaInicio=fi;
                this.fechaFinal=ff;
                this.tablaO=to;
                this.tablaD=td;
                this.rutina=r;
                this.duracion=d;
                this.estado=e;
                this.usuario=u;
                this.id=id;
        }
        
        
        /**
         * Metodo que Inserta Toma el valor de cada uno de los campos y los asigna.
         * @autor.......David Lamadrid
         * @param.......rs ResultSet de la consulta.
         * @see.........
         * @throws......campo invalido en la consulta
         * @version.....1.0.
         * @return.......
         */
        public static AdminHistorico load (ResultSet rs)throws SQLException
        {
                AdminHistorico admin = new AdminHistorico ();
                admin.setCreationDate (rs.getString ("creation_date"));
                admin.setFechaInicio (rs.getString ("fecha_inicio"));
                admin.setFechaFinal (rs.getString ("fecha_final"));
                admin.setTablaO (rs.getString ("tabla_o"));
                admin.setTablaD (rs.getString ("tabla_d"));
                admin.setRutina (rs.getString ("rutina"));
                admin.setDuracion (rs.getInt ("duracion"));
                admin.setEstado (rs.getString ("estado"));
                admin.setUsuario (rs.getString ("usuario"));
                ////System.out.println ("bean "+rs.getString ("oid"));
                admin.setId (rs.getString ("oid"));
                return admin;
                
        }
        
        
        /**
         * Metodo que obtiene el atributo creationDate(fecha de creacion).
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String creationDate
         */
        public java.lang.String getCreationDate ()
        {
                return creationDate;
        }
        
        /**
         * Metodo que setea el atributo creationDate(fecha de creacion).
         * @autor.......David Lamadrid
         * @param.......String creationDate
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setCreationDate (java.lang.String creationDate)
        {
                this.creationDate = creationDate;
        }
        
        /**
         * Metodo que obtiene el atributo fechaInicio(fecha de inicio).
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String fechaInicio
         */
        public java.lang.String getFechaInicio ()
        {
                return fechaInicio;
        }
        
        /**
         * Metodo que setea el atributo fechaInicio(fecha de inicio).
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see.........String fechaInicio
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setFechaInicio (java.lang.String fechaInicio)
        {
                this.fechaInicio = fechaInicio;
        }
        
        
        /**
         * Metodo que obtiene el atributo fechaFinal(fecha final).
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String fechaFinal
         */
        public java.lang.String getFechaFinal ()
        {
                return fechaFinal;
        }
        
        /**
         * Metodo que setea el atributo fechaFinal(fecha final).
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see.........String fechaFinal
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setFechaFinal (java.lang.String fechaFinal)
        {
                this.fechaFinal = fechaFinal;
        }
        
        /**
         * Metodo que obtiene el atributo tablaO.
         * @autor.......David Lamadrid
         * @param.......ninguno
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String tablaO
         */
        public java.lang.String getTablaO ()
        {
                return tablaO;
        }
        
        /**
         * Metodo que setea el atributo tablaO.
         * @autor.......David Lamadrid
         * @param.......String tablaO
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setTablaO (java.lang.String tablaO)
        {
                this.tablaO = tablaO;
        }
        
        /**
         * Metodo que obtiene el atributo tablaD.
         * @autor.......David Lamadrid
         * @param.......
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String tablaD
         */
        public java.lang.String getTablaD ()
        {
                return tablaD;
        }
        
        /**
         * Metodo que setea el atributo tablaD.
         * @autor.......David Lamadrid
         * @param.......String tablaD
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setTablaD (java.lang.String tablaD)
        {
                this.tablaD = tablaD;
        }
        
        /**
         * Metodo que obtiene el atributo rutina.
         * @autor.......David Lamadrid
         * @param.......
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String rutina
         */
        public java.lang.String getRutina ()
        {
                return rutina;
        }
        
        /**
         * Metodo que setea el atributo rutina.
         * @autor.......David Lamadrid
         * @param.......String rutinaString rutina
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setRutina (java.lang.String rutina)
        {
                this.rutina = rutina;
        }
        
        /**
         * Metodo que obtiene el atributo duracion.
         * @autor.......David Lamadrid
         * @param.......
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......int duracion
         */
        public int getDuracion ()
        {
                return duracion;
        }
        
        /**
         * Metodo que obtiene el atributo duracion.
         * @autor.......David Lamadrid
         * @param.......int duracion
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setDuracion (int duracion)
        {
                this.duracion = duracion;
        }
        
        /**
         * Metodo que obtiene el atributo estado.
         * @autor.......David Lamadrid
         * @param.......
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String estado
         */
        public java.lang.String getEstado ()
        {
                return estado;
        }
        
        /**
         * Metodo que seteo el atributo estado.
         * @autor.......David Lamadrid
         * @param.......String estado
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setEstado (java.lang.String estado)
        {
                this.estado = estado;
        }
        
        /**
         * Metodo que seteo el atributo usuario.
         * @autor.......David Lamadrid
         * @param.......
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String usuario
         */
        public java.lang.String getUsuario ()
        {
                return usuario;
        }
        
        /**
         * Metodo que seteo el atributo usuario.
         * @autor.......David Lamadrid
         * @param.......String usuario
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setUsuario (java.lang.String usuario)
        {
                this.usuario = usuario;
        }
        
        /**
         * Metodo que obtiene el atributo id.
         * @autor.......David Lamadrid
         * @param.......
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......String id
         */
        public java.lang.String getId ()
        {
                return id;
        }
        
        /**
         * Metodo que setea el atributo id.
         * @autor.......David Lamadrid
         * @param.......String id
         * @see.........ninguno
         * @throws......Clase no encontrada
         * @version.....1.0.
         * @return.......
         */
        public void setId (java.lang.String id)
        {
                this.id = id;
        }
        
}
