/*
 * TblEstado.java
 *
 * Created on 4 de octubre de 2005, 05:34 PM
 */

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.Util;
import java.io.*;
import java.sql.*;

/**
 *
 * @author  Tito Andres
 */
public class TblEstado {
        private String tipo;
        private String codestado;
        private String descripcion;
        private String color;
        private String fondo;
        private String color_letra;
        private InputStream imagen;
        private int longitud_imagen;
        private String filename;
        private String path;
        
        private String usuario_creacion;
        private String fecha_creacion;
        private String usuario_modificacion;
        private String ultima_modificacion;
        private String distrito;
        private String estado;
        private String base; 
        
        /** Creates a new instance of TblEstado */
        public TblEstado() {
        }
        
        public void load(ResultSet rs) throws SQLException{
                this.setBase(rs.getString("base"));
                this.setCodestado(rs.getString("codestado"));
                this.setColor(rs.getString("color"));
                this.setColor_letra(rs.getString("color_letra"));
                this.setDescripcion(rs.getString("descripcion"));
                this.setDistrito(rs.getString("dstrct"));
                this.setEstado(rs.getString("reg_status"));
                this.setFecha_creacion(rs.getString("creation_date"));
                this.setFilename(rs.getString("filename"));
                this.setFondo(rs.getString("fondo"));
                this.setTipo(rs.getString("tipo"));
                this.setUltima_modificacion(rs.getString("last_update"));
                this.setUsuario_creacion(rs.getString("creation_user"));
                this.setUsuario_modificacion(rs.getString("user_update"));
                this.setImagen(rs.getBinaryStream("imagen"));
        }
        
        /**
         * Getter for property codestado.
         * @return Value of property codestado.
         */
        public java.lang.String getCodestado() {
                return codestado;
        }
        
        /**
         * Setter for property codestado.
         * @param codestado New value of property codestado.
         */
        public void setCodestado(java.lang.String codestado) {
                this.codestado = codestado;
        }
        
        /**
         * Getter for property color.
         * @return Value of property color.
         */
        public java.lang.String getColor() {
                return color;
        }
        
        /**
         * Setter for property color.
         * @param color New value of property color.
         */
        public void setColor(java.lang.String color) {
                this.color = color;
        }
        
        /**
         * Getter for property color_letra.
         * @return Value of property color_letra.
         */
        public java.lang.String getColor_letra() {
                return color_letra;
        }
        
        /**
         * Setter for property color_letra.
         * @param color_letra New value of property color_letra.
         */
        public void setColor_letra(java.lang.String color_letra) {
                this.color_letra = color_letra;
        }
        
        /**
         * Getter for property descripcion.
         * @return Value of property descripcion.
         */
        public java.lang.String getDescripcion() {
                return descripcion;
        }
        
        /**
         * Setter for property descripcion.
         * @param descripcion New value of property descripcion.
         */
        public void setDescripcion(java.lang.String descripcion) {
                this.descripcion = descripcion;
        }
        
        /**
         * Getter for property fondo.
         * @return Value of property fondo.
         */
        public java.lang.String getFondo() {
                return fondo;
        }
        
        /**
         * Setter for property fondo.
         * @param fondo New value of property fondo.
         */
        public void setFondo(java.lang.String fondo) {
                this.fondo = fondo;
        }
        
        /**
         * Getter for property imagen.
         * @return Value of property imagen.
         */
        public java.io.InputStream getImagen() {
                return imagen;
        }
        
        /**
         * Setter for property imagen.
         * @param imagen New value of property imagen.
         */
        public void setImagen(java.io.InputStream imagen) {
                this.imagen = imagen;
        }
        
        /**
         * Getter for property tipo.
         * @return Value of property tipo.
         */
        public java.lang.String getTipo() {
                return tipo;
        }
        
        /**
         * Setter for property tipo.
         * @param tipo New value of property tipo.
         */
        public void setTipo(java.lang.String tipo) {
                this.tipo = tipo;
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }
        
        /**
         * Getter for property distrito.
         * @return Value of property distrito.
         */
        public java.lang.String getDistrito() {
                return distrito;
        }
        
        /**
         * Setter for property distrito.
         * @param distrito New value of property distrito.
         */
        public void setDistrito(java.lang.String distrito) {
                this.distrito = distrito;
        }
        
        /**
         * Getter for property fecha_creacion.
         * @return Value of property fecha_creacion.
         */
        public java.lang.String getFecha_creacion() {
                return fecha_creacion;
        }
        
        /**
         * Setter for property fecha_creacion.
         * @param fecha_creacion New value of property fecha_creacion.
         */
        public void setFecha_creacion(java.lang.String fecha_creacion) {
                this.fecha_creacion = fecha_creacion;
        }
        
        /**
         * Getter for property ultima_modificacion.
         * @return Value of property ultima_modificacion.
         */
        public java.lang.String getUltima_modificacion() {
                return ultima_modificacion;
        }
        
        /**
         * Setter for property ultima_modificacion.
         * @param ultima_modificacion New value of property ultima_modificacion.
         */
        public void setUltima_modificacion(java.lang.String ultima_modificacion) {
                this.ultima_modificacion = ultima_modificacion;
        }
        
        /**
         * Getter for property usuario_creacion.
         * @return Value of property usuario_creacion.
         */
        public java.lang.String getUsuario_creacion() {
                return usuario_creacion;
        }
        
        /**
         * Setter for property usuario_creacion.
         * @param usuario_creacion New value of property usuario_creacion.
         */
        public void setUsuario_creacion(java.lang.String usuario_creacion) {
                this.usuario_creacion = usuario_creacion;
        }
        
        /**
         * Getter for property usuario_modificacion.
         * @return Value of property usuario_modificacion.
         */
        public java.lang.String getUsuario_modificacion() {
                return usuario_modificacion;
        }
        
        /**
         * Setter for property usuario_modificacion.
         * @param usuario_modificacion New value of property usuario_modificacion.
         */
        public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
                this.usuario_modificacion = usuario_modificacion;
        }
        
        /**
         * Getter for property estado.
         * @return Value of property estado.
         */
        public java.lang.String getEstado() {
                return estado;
        }
        
        /**
         * Setter for property estado.
         * @param estado New value of property estado.
         */
        public void setEstado(java.lang.String estado) {
                this.estado = estado;
        }
        
        /**
         * Getter for property longitud_imagen.
         * @return Value of property longitud_imagen.
         */
        public int getLongitud_imagen() {
                return longitud_imagen;
        }
        
        /**
         * Setter for property longitud_imagen.
         * @param longitud_imagen New value of property longitud_imagen.
         */
        public void setLongitud_imagen(int longitud_imagen) {
                this.longitud_imagen = longitud_imagen;
        }
        
        /**
         * Getter for property filename.
         * @return Value of property filename.
         */
        public java.lang.String getFilename() {
                return filename;
        }
        
        /**
         * Setter for property filename.
         * @param filename New value of property filename.
         */
        public void setFilename(java.lang.String filename) {
                this.filename = filename;
        }
        
        /**
         * Getter for property path.
         * @return Value of property path.
         */
        public java.lang.String getPath() {
                return path;
        }
        
        /**
         * Setter for property path.
         * @param path New value of property path.
         */
        public void setPath(java.lang.String path) {
                this.path = path;
        }
        
}

