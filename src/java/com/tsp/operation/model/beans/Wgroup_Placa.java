/*************************************************************************************
 * Nombre clase :                   Wgroup_Placa.java                                *
 * Descripcion :                    Clase que maneja los atributos relacionados con  *
 *                                  la tabla work_group - placa                      *
 * Autor :                          Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                          13 de enero de 2006, 02:30 PM                    *
 * Version :                        1.0                                              *
 * Copyright :                      Fintravalores S.A.                          *
 ************************************************************************************/
package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;

public class Wgroup_Placa {
    private String placa;
    private String wgroup;
    private String reg_status;
    /** Creates a new instance of Wgroup_Placa */
    public Wgroup_Placa() {
    }
    
    /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo Wgroup_Placa
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static Wgroup_Placa load(ResultSet rs)throws Exception{
        Wgroup_Placa datos = new Wgroup_Placa();
        datos.setPlaca(rs.getString("placa"));
        datos.setWgroup(rs.getString("work_group"));
        datos.setReg_status(rs.getString("reg_status"));
        return datos;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property wgroup.
     * @return Value of property wgroup.
     */
    public java.lang.String getWgroup() {
        return wgroup;
    }
    
    /**
     * Setter for property wgroup.
     * @param wgroup New value of property wgroup.
     */
    public void setWgroup(java.lang.String wgroup) {
        this.wgroup = wgroup;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
}
