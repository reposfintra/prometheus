/******************************************************************
* Nombre                        EstCargaPlaca.java
* Descripci�n                   Clase bean para la tabla est_carga_placa
* Autor                         ricardo rosero
* Fecha                         13/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import com.tsp.operation.model.beans.*;

/**
 * @author  Ricardo Rosero
 */
public class EstCargaPlaca implements java.io.Serializable {
    // Atributos
    private String placa;
    private String uc;
    private String fc;
    private String um;
    private String fm;
    private String base;
    private String estado;
    private String distrito;
    private String periodo;
    private String fecha;
    private int carga;
    private int viaje;
    
    // Funciones
    /**
    * load
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ ResulSet rs
    * @throws ....... SQLException
    * @version ...... 1.0
    */   
    public static EstCargaPlaca load(ResultSet rs)throws SQLException {
        EstCargaPlaca ecp = new EstCargaPlaca();
        
        ecp.setPlaca(rs.getString("plaveh"));
        ecp.setCarga(rs.getInt("carga"));
        ecp.setViajes(rs.getInt("num_viaje"));
        ecp.setPeriodo(rs.getString("periodo"));
        ecp.setFecha(rs.getString("fecha"));
        ecp.setUC(rs.getString("creation_user"));
        String fecha2 =rs.getString("creation_date").substring(0,10);
        ecp.setFC(fecha2);
        ecp.setUM(rs.getString("user_update"));
        String fecha3 =rs.getString("last_update").substring(0,10);
        ecp.setFM(fecha3);
        ecp.setBase(rs.getString("base"));
        ecp.setEstado(rs.getString("reg_status"));
        ecp.setDistrito(rs.getString("dstrct"));
            
        return ecp;
    }
    
    /**
    * setPlaca
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setPlaca(String dato){
        this.placa = dato;
    }
    
    /**
    * getPlaca
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getPlaca(){
        return this.placa;
    }
    
    /**
    * setCarga
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setCarga(int dato){
        this.carga = dato;
    }
    
    /**
    * getCarga
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public int getCarga(){
        return this.carga;
    }
    
    /**
    * setViajes
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setViajes(int dato){
        this.viaje = dato;
    }
    
    /**
    * getViajes
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public int getViajes(){
        return this.viaje;
    }
    
    /**
    * setUC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setUC(String dato){
        this.uc = dato;
    }
    
    /**
    * getUC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getUC(){
        return this.uc;
    }
    
    /**
    * setFC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setFC(String dato){
        this.fc = dato;
    }
    
    /**
    * getFC
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getFC(){
        return this.fc;
    }
    
    /**
    * setUM
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setUM(String dato){
        this.um = dato;
    }
    
    /**
    * getUM
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getUM(){
        return this.um;
    }
    
    /**
    * setFM
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setFM(String dato){
        this.fm = dato;
    }
    
    /**
    * getFM
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getFM(){
        return this.fm;
    }
    
    /**
    * setBase
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setBase(String dato){
        this.base = dato;
    }
    
    /**
    * getBase
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getBase(){
        return this.base;
    }
    
    /**
    * setEstado
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setEstado(String dato){
        this.estado = dato;
    }
    
    /**
    * getEstado
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getEstado(){
        return this.estado;
    }
    
    /**
    * setDistrito
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setDistrito(String dato){
        this.distrito = dato;
    }
    
    /**
    * getDistrito
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getDistrito(){
        return this.distrito;
    }
    
    /**
    * setPeriodo
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ dato
    * @throws ....... none
    * @version ...... 1.0
    */   
    public void setPeriodo(String dato){
        this.periodo = dato;
    }
    
    /**
    * getPeriodo
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ none
    * @throws ....... none
    * @version ...... 1.0
    */   
    public String getPeriodo(){
        return this.periodo;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
}