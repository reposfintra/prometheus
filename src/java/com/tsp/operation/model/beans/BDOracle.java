/*
 * BDOracle.java
 *
 * Created on 20 de junio de 2005, 11:16 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Jm
 */
public class BDOracle implements Serializable{
    String asso_rec;
    /** Creates a new instance of BDOracle */
    public BDOracle() {
    }
    
    /**
     * Getter for property asso_rec.
     * @return Value of property asso_rec.
     */
    public java.lang.String getAsso_rec() {
        return asso_rec;
    }
    
    /**
     * Setter for property asso_rec.
     * @param asso_rec New value of property asso_rec.
     */
    public void setAsso_rec(java.lang.String asso_rec) {
        this.asso_rec = asso_rec;
    }
    
}
