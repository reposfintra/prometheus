/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author edgonzalez
 */
public class VariablesRefinanciacion {
    
       private String negocio;
       private String tipoRefi;
       private String fechaPrimeraCuota ;
       private int plazo ;
       private String fechaProyeccion;
       private int cuota_inicio ;
       private String ciudad ;
       private String compraCartera ;
       private double porce ;
       private boolean isPago ;
       private double interes ;
       private double catVencido ;
       private double cuotaAdmon ;
       private double intMora ;
       private double gastoCobranza ;
       private double pagoInicial ;
       private double capitalRefinanciacion ;
       private double saldoCat ;
       private double total_a_pagar;
       private int periodo_pago_inicial;
       private String observacion;
       private long celular;

    public VariablesRefinanciacion() {
    }

    public VariablesRefinanciacion(String negocio, String tipoRefi, String fechaPrimeraCuota, int plazo, String fechaProyeccion, 
                    int cuota_inicio, String ciudad, String compraCartera, double porce, boolean isPago, double interes, double catVencido, 
                    double cuotaAdmon, double intMora, double gastoCobranza, double pagoInicial, double capitalRefinanciacion, double saldoCat, double total_a_pagar, int periodo_pago_inicial
                    ,String observacion, long celular) {
        this.negocio = negocio;
        this.tipoRefi = tipoRefi;
        this.fechaPrimeraCuota = fechaPrimeraCuota;
        this.plazo = plazo;
        this.fechaProyeccion = fechaProyeccion;
        this.cuota_inicio = cuota_inicio;
        this.ciudad = ciudad;
        this.compraCartera = compraCartera;
        this.porce = porce;
        this.isPago = isPago;
        this.interes = interes;
        this.catVencido = catVencido;
        this.cuotaAdmon = cuotaAdmon;
        this.intMora = intMora;
        this.gastoCobranza = gastoCobranza;
        this.pagoInicial = pagoInicial;
        this.capitalRefinanciacion = capitalRefinanciacion;
        this.saldoCat = saldoCat;
        this.total_a_pagar=total_a_pagar;
        this.periodo_pago_inicial=periodo_pago_inicial;
        this.observacion=observacion;
        this.celular=celular;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getTipoRefi() {
        return tipoRefi;
    }

    public void setTipoRefi(String tipoRefi) {
        this.tipoRefi = tipoRefi;
    }

    public String getFechaPrimeraCuota() {
        return fechaPrimeraCuota;
    }

    public void setFechaPrimeraCuota(String fechaPrimeraCuota) {
        this.fechaPrimeraCuota = fechaPrimeraCuota;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public String getFechaProyeccion() {
        return fechaProyeccion;
    }

    public void setFechaProyeccion(String fechaProyeccion) {
        this.fechaProyeccion = fechaProyeccion;
    }

    public int getCuota_inicio() {
        return cuota_inicio;
    }

    public void setCuota_inicio(int cuota_inicio) {
        this.cuota_inicio = cuota_inicio;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCompraCartera() {
        return compraCartera;
    }

    public void setCompraCartera(String compraCartera) {
        this.compraCartera = compraCartera;
    }

    public double getPorce() {
        return porce;
    }

    public void setPorce(double porce) {
        this.porce = porce;
    }

    public boolean isIsPago() {
        return isPago;
    }

    public void setIsPago(boolean isPago) {
        this.isPago = isPago;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }

    public double getCatVencido() {
        return catVencido;
    }

    public void setCatVencido(double catVencido) {
        this.catVencido = catVencido;
    }

    public double getCuotaAdmon() {
        return cuotaAdmon;
    }

    public void setCuotaAdmon(double cuotaAdmon) {
        this.cuotaAdmon = cuotaAdmon;
    }

    public double getIntMora() {
        return intMora;
    }

    public void setIntMora(double intMora) {
        this.intMora = intMora;
    }

    public double getGastoCobranza() {
        return gastoCobranza;
    }

    public void setGastoCobranza(double gastoCobranza) {
        this.gastoCobranza = gastoCobranza;
    }

    public double getPagoInicial() {
        return pagoInicial;
    }

    public void setPagoInicial(double pagoInicial) {
        this.pagoInicial = pagoInicial;
    }

    public double getCapitalRefinanciacion() {
        return capitalRefinanciacion;
    }

    public void setCapitalRefinanciacion(double capitalRefinanciacion) {
        this.capitalRefinanciacion = capitalRefinanciacion;
    }

    public double getSaldoCat() {
        return saldoCat;
    }

    public void setSaldoCat(double saldoCat) {
        this.saldoCat = saldoCat;
    }

    public double getTotal_a_pagar() {
        return total_a_pagar;
    }

    public void setTotal_a_pagar(double total_a_pagar) {
        this.total_a_pagar = total_a_pagar;
    }

    public int getPeriodo_pago_inicial() {
        return periodo_pago_inicial;
    }

    public void setPeriodo_pago_inicial(int periodo_pago_inicial) {
        this.periodo_pago_inicial = periodo_pago_inicial;
    }
    
    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public long getCelular() {
        return celular;
    }

    public void setCelular(long celular) {
        this.celular = celular;
    }
 
}
