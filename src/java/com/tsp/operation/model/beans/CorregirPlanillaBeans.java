/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author hcuello
 */
public class CorregirPlanillaBeans {
    
    private String nit;
    private int numero_operacion;
    private String tipo_documento;
    private String clase;
    private String documento;
    private double valor_calculado;
    private double valor_registrado;
    private double diferencia_interna;
    private String placa;

    public CorregirPlanillaBeans() {
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the numero_operacion
     */
    public int getNumero_operacion() {
        return numero_operacion;
    }

    /**
     * @param numero_operacion the numero_operacion to set
     */
    public void setNumero_operacion(int numero_operacion) {
        this.numero_operacion = numero_operacion;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the clase
     */
    public String getClase() {
        return clase;
    }

    /**
     * @param clase the clase to set
     */
    public void setClase(String clase) {
        this.clase = clase;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the valor_calculado
     */
    public double getValor_calculado() {
        return valor_calculado;
    }

    /**
     * @param valor_calculado the valor_calculado to set
     */
    public void setValor_calculado(double valor_calculado) {
        this.valor_calculado = valor_calculado;
    }

    /**
     * @return the valor_registrado
     */
    public double getValor_registrado() {
        return valor_registrado;
    }

    /**
     * @param valor_registrado the valor_registrado to set
     */
    public void setValor_registrado(double valor_registrado) {
        this.valor_registrado = valor_registrado;
    }

    /**
     * @return the diferencia_interna
     */
    public double getDiferencia_interna() {
        return diferencia_interna;
    }

    /**
     * @param diferencia_interna the diferencia_interna to set
     */
    public void setDiferencia_interna(double diferencia_interna) {
        this.diferencia_interna = diferencia_interna;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    
    
}
