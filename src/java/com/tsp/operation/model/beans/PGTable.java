/*
 * PGTable.java
 *
 * Created on 15 de marzo de 2007, 09:10 AM
 */

package com.tsp.operation.model.beans;

import java.util.Vector;
import com.tsp.util.connectionpool.*;
import java.sql.*;
import com.tsp.operation.model.beans.PGColumn;
/**
 *
 * @author  Osvaldo P�rez Ferrer
 */
public class PGTable {
    
    private String name;
    private String schema;
    private String dataBaseName;
    private String columns_names;
    private Vector columns;
    
    /** Creates a new instance of PGTable */
        
    private PGTable( String name, String schema, String dataBaseName ) throws Exception{
               
        this.name           = name;
        this.schema         = schema;
        this.dataBaseName   = dataBaseName;
        this.columns_names  = "";
        this.columns        = new Vector();
        this.getInfo();
        
    }
    
    public static PGTable getInstance( String name, String schema ) {
        
        PGTable t;
        
        try{
            t = new PGTable(name, schema, "fintra");            
        }catch (Exception ex){
            return null;
        }
        return t;
    }
    public static PGTable getInstance( String name, String schema, String dataBaseName ) {
        
        PGTable t;
        
        try{
            t = new PGTable(name, schema, dataBaseName);            
        }catch (Exception ex){
            return null;
        }
        return t;
    }
    
    public void getInfo() throws Exception{
        
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        Conexion    = poolManager.getConnection(this.dataBaseName);
        
        if (Conexion == null)
            throw new SQLException("Sin conexion");
        try{
            
            String tipos[] = new String [] { "TABLE" };
            DatabaseMetaData dbmd   = Conexion.getMetaData();
            ResultSet        tablas = dbmd.getTables( null, schema, name , tipos );
            
            if ( tablas.next() ) {
                String    tabla    = tablas.getString( tablas.findColumn( "TABLE_NAME" ) );
                ResultSet columnas = dbmd.getColumns(null, schema, tabla, null);
                
                while(columnas.next()){
                    
                    PGColumn c = new PGColumn();
                    
                    c.setName(           columnas.getString("COLUMN_NAME") );
                    c.setData_type(      columnas.getString("TYPE_NAME") );
                    c.setLength(         columnas.getInt("COLUMN_SIZE") );
                    c.setDefinition(     columnas.getString("COLUMN_DEF") );
                    c.setDecimal_digits( columnas.getInt("DECIMAL_DIGITS") );
                    c.setNullable(       columnas.getString("IS_NULLABLE").equals("YES")? true : false );
                    
                    this.columns.add( c );
                    this.columns_names += c.getName() + " ";
                    
                }
                
                this.columns_names = this.columns_names.trim();
                this.columns_names = this.columns_names.replaceAll(" ",",");
                
            }else{
                throw new Exception("No se encontr� la tabla en el esquema dado");
            }
            
        }catch(Exception e) {
            throw new Exception("Error en PGTable.getInfo "+e.getMessage());
        }
        finally {
            poolManager.freeConnection(this.dataBaseName  ,Conexion);
        }
    }
    
    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public java.lang.String getName() {
        return name;
    }
    
    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }
    
    /**
     * Getter for property schema.
     * @return Value of property schema.
     */
    public java.lang.String getSchema() {
        return schema;
    }
    
    /**
     * Setter for property schema.
     * @param schema New value of property schema.
     */
    public void setSchema(java.lang.String schema) {
        this.schema = schema;
    }
    
    /**
     * Getter for property columns_names.
     * @return Value of property columns_names.
     */
    public java.lang.String getColumns_names() {
        return columns_names;
    }
    
    /**
     * Setter for property columns_names.
     * @param columns_names New value of property columns_names.
     */
    public void setColumns_names(java.lang.String columns_names) {
        this.columns_names = columns_names;
    }
    
    /**
     * Getter for property columns.
     * @return Value of property columns.
     */
    public java.util.Vector getColumns() {
        return columns;
    }
    
    /**
     * Setter for property columns.
     * @param columns New value of property columns.
     */
    public void setColumns(java.util.Vector columns) {
        this.columns = columns;
    }
    
}
