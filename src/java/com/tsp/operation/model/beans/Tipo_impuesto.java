/*
 * Tipo_impuesto.java
 *
 * Created on 27 de septiembre de 2005, 09:48 AM
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
/**
 *
 * @author  JuanM
 */
public class Tipo_impuesto {
    private String dstrct;
    private String reg_status;
    private String tipo_impuesto;
    private String codigo_impuesto;
    private String concepto;
    private String descripcion;
    private String fecha_vigencia;
    private double porcentaje1;
    private double porcentaje2;
    private String cod_cuenta_contable;
    private String agencia;
    private String creation_user;
    private String user_update;
    private int ind_signo;
    /** Creates a new instance of Tipo_impuesto */
    public Tipo_impuesto() {
    }
    /*CARGA EL OBJETO TIPO DE IMPUESTO CON LOS DATOS RELACIONADOS AL TIPO DE IMPUESTO IVA*/
    public static Tipo_impuesto loadIVA(ResultSet rs)throws Exception{
        Tipo_impuesto datos = new Tipo_impuesto();
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setDstrct(rs.getString("DSTRCT"));
        datos.setCodigo_impuesto(rs.getString("CODIGO_IMPUESTO"));
        datos.setTipo_impuesto(rs.getString("TIPO_IMPUESTO"));
        datos.setConcepto(rs.getString("CONCEPTO"));
        datos.setFecha_vigencia(rs.getString("FECHA_VIGENCIA"));
        datos.setPorcentaje1(rs.getDouble("PORCENTAJE1"));
        datos.setPorcentaje2(rs.getDouble("PORCENTAJE2"));
        datos.setCod_cuenta_contable(rs.getString("COD_CUENTA_CONTABLE"));
        return datos;
    }
    
    /*CARGA EL OBJETO TIPO DE IMPUESTO CON LOS DATOS RELACIONADOS AL TIPO DE IMPUESTO RICA*/
    public static Tipo_impuesto loadRICA(ResultSet rs)throws Exception{
        Tipo_impuesto datos = new Tipo_impuesto();
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setDstrct(rs.getString("DSTRCT"));
        datos.setCodigo_impuesto(rs.getString("CODIGO_IMPUESTO"));
        datos.setTipo_impuesto(rs.getString("TIPO_IMPUESTO"));
        datos.setConcepto(rs.getString("CONCEPTO"));
        datos.setDescripcion(rs.getString("DESCRIPCION"));
        datos.setFecha_vigencia(rs.getString("FECHA_VIGENCIA"));
        datos.setPorcentaje1(rs.getDouble("PORCENTAJE1"));
        datos.setCod_cuenta_contable(rs.getString("COD_CUENTA_CONTABLE"));
        datos.setAgencia(rs.getString("AGENCIA"));
        return datos;
    }
    
    
    /*CARGA EL OBJETO TIPO DE IMPUESTO CON LOS DATOS RELACIONADOS AL TIPO DE IMPUESTO RFTE*/
    public static Tipo_impuesto loadRFTE(ResultSet rs)throws Exception{
        Tipo_impuesto datos = new Tipo_impuesto();
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setDstrct(rs.getString("DSTRCT"));
        datos.setCodigo_impuesto(rs.getString("CODIGO_IMPUESTO"));
        datos.setTipo_impuesto(rs.getString("TIPO_IMPUESTO"));
        datos.setConcepto(rs.getString("CONCEPTO"));
        datos.setDescripcion(rs.getString("DESCRIPCION"));
        datos.setFecha_vigencia(rs.getString("FECHA_VIGENCIA"));
        datos.setPorcentaje1(rs.getDouble("PORCENTAJE1"));
        datos.setCod_cuenta_contable(rs.getString("COD_CUENTA_CONTABLE"));
        datos.setAgencia(rs.getString("AGENCIA"));
        datos.setPorcentaje2(rs.getDouble("PORCENTAJE2"));
        datos.setInd_signo(rs.getInt("ind_signo"));//AMATURANA 13.03.2007
        return datos;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property cod_cuenta_contable.
     * @return Value of property cod_cuenta_contable.
     */
    public java.lang.String getCod_cuenta_contable() {
        return cod_cuenta_contable;
    }
    
    /**
     * Setter for property cod_cuenta_contable.
     * @param cod_cuenta_contable New value of property cod_cuenta_contable.
     */
    public void setCod_cuenta_contable(java.lang.String cod_cuenta_contable) {
        this.cod_cuenta_contable = cod_cuenta_contable;
    }
    
    /**
     * Getter for property codigo_impuesto.
     * @return Value of property codigo_impuesto.
     */
    public java.lang.String getCodigo_impuesto() {
        return codigo_impuesto;
    }
    
    /**
     * Setter for property codigo_impuesto.
     * @param codigo_impuesto New value of property codigo_impuesto.
     */
    public void setCodigo_impuesto(java.lang.String codigo_impuesto) {
        this.codigo_impuesto = codigo_impuesto;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_vigencia.
     * @return Value of property fecha_vigencia.
     */
    public java.lang.String getFecha_vigencia() {
        return fecha_vigencia;
    }
    
    /**
     * Setter for property fecha_vigencia.
     * @param fecha_vigencia New value of property fecha_vigencia.
     */
    public void setFecha_vigencia(java.lang.String fecha_vigencia) {
        this.fecha_vigencia = fecha_vigencia;
    }
    
    /**
     * Getter for property porcentaje1.
     * @return Value of property porcentaje1.
     */
    public double getPorcentaje1() {
        return porcentaje1;
    }
    
    /**
     * Setter for property porcentaje1.
     * @param porcentaje1 New value of property porcentaje1.
     */
    public void setPorcentaje1(double porcentaje1) {
        this.porcentaje1 = porcentaje1;
    }
    
    /**
     * Getter for property porcentaje2.
     * @return Value of property porcentaje2.
     */
    public double getPorcentaje2() {
        return porcentaje2;
    }
    
    /**
     * Setter for property porcentaje2.
     * @param porcentaje2 New value of property porcentaje2.
     */
    public void setPorcentaje2(double porcentaje2) {
        this.porcentaje2 = porcentaje2;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_impuesto.
     * @return Value of property tipo_impuesto.
     */
    public java.lang.String getTipo_impuesto() {
        return tipo_impuesto;
    }
    
    /**
     * Setter for property tipo_impuesto.
     * @param tipo_impuesto New value of property tipo_impuesto.
     */
    public void setTipo_impuesto(java.lang.String tipo_impuesto) {
        this.tipo_impuesto = tipo_impuesto;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property ind_signo.
     * @return Value of property ind_signo.
     */
    public int getInd_signo() {
        return ind_signo;
    }
    
    /**
     * Setter for property ind_signo.
     * @param ind_signo New value of property ind_signo.
     */
    public void setInd_signo(int ind_signo) {
        this.ind_signo = ind_signo;
    }
    
}
