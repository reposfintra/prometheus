/*
 * factura.java
 *
 * Created on 12 de noviembre de 2004, 06:53 PM
 */

package com.tsp.operation.model.beans;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.tsp.util.*;
/**
 *
 * @author  AMARTINEZ
 */
public class factura implements java.io.Serializable,Cloneable{

    private String plazo;
    private String nit;
    private String codcli;
    private String factura;
    private String concepto;
    private String fecha_factura;
    private String fecha_vencimiento;
    private String fecha_ultimo_pago;
    private String fecha_impresion;
    private String descripcion;
    private String observacion;
    private double valor_factura;
    private double valor_abono;
    private double valor_saldo;
    private double valor_facturame;
    private double valor_abonome;
    private double valor_saldome;
    private Double valor_tasa;
    private String moneda;
    private int cantidad_items;
    private String forma_pago;
    private String agencia_facturacion;
    private String agencia_cobro;
    private String zona;
    private String clasificacion1;
    private String clasificacion2;
    private String clasificacion3;
    private Double transaccion;
    private String fecha_contabilizacion;
    private String fecha_anulacion;
    private String fecha_contabilizacion_anulacion;

    private String direccion;
    private String telefono;
    private String ciudad;
    private String nomcli;
    private String ccuenta;
    private String nom_agencia_facturacion;
    private String simbolo;

    /*Modificacion 28-12-06*/
    private String resol_dian; //Resolucion Dian
    private String resol_gran; //Resolucion Gran Contribuyente
    private String hc;

    private double vlr_bruto_ml;

    private boolean estadoImpresion = false; //Estado Impresion
    private String rif;
    private String cmc;


    private String Documen_ing;
    private String num_ingreso;
    private String vlr_ingreso;
    private String codmoneda;
    private String tipo_documento;
    private String Nfacturas;
    private String documento;

    private String fecha_ingreso;
    //Jescandon 16-03-07
    private String cplazo;// Plazo de la Tabla Cliente

    private String valor_43;

    private String descripcion_ruta;
    private double vlr_freight;
    private String simbolo_descripcion;

    //igomez 14/04/2007
    private String agencia_impresion;
    private String formato;

    private double vlr_tasa_remesas;

    private String fisico;
    private String estado;
    private int dias_mora;
    private String fecha_ven_prejurid;
    
    private String banco;
    private String sucursal;
    
    private String diferencia;

    /**
     * Getter for property estadoImpresion.
     * @return Value of property estadoImpresion.
     */
    public boolean isEstadoImpresion() {
        return estadoImpresion;
    }

    /**
     * Setter for property estadoImpresion.
     * @param estadoImpresion New value of property estadoImpresion.
     */
    public void setEstadoImpresion(boolean estadoImpresion) {
        this.estadoImpresion = estadoImpresion;
    }





    /**
     * Getter for property resol_dian.
     * @return Value of property resol_dian.
     */
    public java.lang.String getResol_dian() {
        return resol_dian;
    }

    /**
     * Setter for property resol_dian.
     * @param resol_dian New value of property resol_dian.
     */
    public void setResol_dian(java.lang.String resol_dian) {
        this.resol_dian = resol_dian;
    }

    /**
     * Getter for property resol_gran.
     * @return Value of property resol_gran.
     */
    public java.lang.String getResol_gran() {
        return resol_gran;
    }

    /**
     * Setter for property resol_gran.
     * @param resol_gran New value of property resol_gran.
     */
    public void setResol_gran(java.lang.String resol_gran) {
        this.resol_gran = resol_gran;
    }



    public void LoadBusqueda(ResultSet rs) throws SQLException{
        this.tipo_documento=rs.getString("tipo_documento");
        this.nit=rs.getString("nit");
        this.codcli= rs.getString("codcli");
        this.moneda=rs.getString("moneda");
        this.forma_pago=rs.getString("forma_pago");
        this.factura= rs.getString("documento");
        this.concepto=rs.getString("concepto");
        this.agencia_facturacion=rs.getString("agencia_facturacion");
        this.agencia_cobro=rs.getString("agencia_cobro");
        this.zona=rs.getString("zona");
        this.fecha_factura=rs.getString("fecha_factura");
        this.fecha_vencimiento=rs.getString("fecha_vencimiento");

        this.plazo=rs.getString("plazo");

        this.fecha_ultimo_pago= rs.getString("fecha_ultimo_pago");
        this.fecha_impresion= rs.getString("fecha_impresion");
        this.descripcion= rs.getString("descripcion");
        this.observacion= rs.getString("observacion");
        this.valor_factura= rs.getDouble("valor_facturame");
        this.valor_abono= rs.getDouble("valor_abono");
        this.valor_saldo= rs.getDouble("valor_saldo");
        this.valor_facturame= rs.getDouble("valor_facturame");
        this.valor_abonome= rs.getDouble("valor_abonome");
        this.valor_saldome= rs.getDouble("valor_saldome");
        this.valor_tasa= new Double(rs.getString("valor_tasa"));
        this.cantidad_items= rs.getInt("cantidad_items");
        this.fecha_contabilizacion = rs.getString("fecha_contabilizacion");
        this.ccuenta = rs.getString("cuenta");

        this.nomcli=rs.getString("nomcli");
        this.direccion=rs.getString("direccion");
        this.telefono=rs.getString("telefono");
        this.ciudad = rs.getString("ciudad");
        this.nom_agencia_facturacion = rs.getString("nom_agencia_facturacion");

        /*Modificacion 28-12-06*/
        this.resol_dian = ( rs.getString("resol_dian") != null )?rs.getString("resol_dian"):"" ;
        this.resol_gran = ( rs.getString("resol_gran_contribuyente") != null )? rs.getString("resol_gran_contribuyente"):"";
        this.hc         = ( rs.getString("hc") != null )?rs.getString("hc"):"";
        this.cplazo     = ( rs.getString("cplazo") != null )?rs.getString("cplazo"):"";

        //jose 2007-01-22
        this.transaccion = new Double(rs.getString("transaccion"));

        this.rif = (rs.getString("rif") != null)?rs.getString("rif"):"";
    }

    public Object clonee() {
        try{
            return super.clone();
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
    public void Load(ResultSet rs) throws SQLException{
        this.nit=rs.getString("nit");
        this.nomcli=rs.getString("nomcli");
        this.direccion=rs.getString("direccion");
        this.telefono=rs.getString("telefono1");
        this.moneda=rs.getString("moneda");
        this.forma_pago=rs.getString("forma_pago");
        this.plazo=rs.getString("plazo");
        this.agencia_facturacion=rs.getString("agfacturacion");
        this.ccuenta = rs.getString("cuenta");
        this.rif = rs.getString("rif");
        this.cmc = rs.getString("cmc");
        this.zona = (rs.getString("zona")!=null)?rs.getString("zona"):"";
    }

    public void Load2(ResultSet rs) throws SQLException{
        this.nit=rs.getString("nit");
        this.codcli= rs.getString("codcli");
        this.moneda=rs.getString("moneda");
        this.forma_pago=rs.getString("forma_pago");
        this.factura= rs.getString("documento");
        this.concepto=rs.getString("concepto");
        this.agencia_facturacion=rs.getString("agencia_facturacion");
        this.agencia_cobro=rs.getString("agencia_cobro");
        this.zona=rs.getString("zona");
        this.fecha_factura=rs.getString("fecha_factura");
        this.fecha_vencimiento=rs.getString("fecha_vencimiento");

        this.plazo=rs.getString("plazo");

        this.fecha_ultimo_pago= rs.getString("fecha_ultimo_pago");
        this.fecha_impresion= rs.getString("fecha_impresion");
        this.descripcion= rs.getString("descripcion");
        this.observacion= rs.getString("observacion");
        this.valor_factura= rs.getDouble("valor_factura");
        this.valor_abono= rs.getDouble("valor_abono");
        this.valor_saldo= rs.getDouble("valor_saldo");
        this.valor_facturame= rs.getDouble("valor_facturame");
        this.valor_abonome= rs.getDouble("valor_abonome");
        this.valor_saldome= rs.getDouble("valor_saldome");
        this.valor_tasa= new Double(rs.getString("valor_tasa"));
        this.cantidad_items= rs.getInt("cantidad_items");
        this.fecha_contabilizacion = rs.getString("fecha_contabilizacion");
        this.ccuenta = rs.getString("cuenta");

        this.nomcli=rs.getString("nomcli");
        this.direccion=rs.getString("direccion");
        this.telefono=rs.getString("telefono");
        this.ciudad = rs.getString("ciudad");
        this.nom_agencia_facturacion = rs.getString("nom_agencia_facturacion");

    }
    /**
     * Getter for property agencia_cobro.
     * @return Value of property agencia_cobro.
     */
    public java.lang.String getAgencia_cobro() {
        return agencia_cobro;
    }

    /**
     * Setter for property agencia_cobro.
     * @param agencia_cobro New value of property agencia_cobro.
     */
    public void setAgencia_cobro(java.lang.String agencia_cobro) {
        this.agencia_cobro = agencia_cobro;
    }

    /**
     * Getter for property agencia_facturacion.
     * @return Value of property agencia_facturacion.
     */
    public java.lang.String getAgencia_facturacion() {
        return agencia_facturacion;
    }

    /**
     * Setter for property agencia_facturacion.
     * @param agencia_facturacion New value of property agencia_facturacion.
     */
    public void setAgencia_facturacion(java.lang.String agencia_facturacion) {
        this.agencia_facturacion = agencia_facturacion;
    }

    /**
     * Getter for property cantidad_items.
     * @return Value of property cantidad_items.
     */
    public int getCantidad_items() {
        return cantidad_items;
    }

    /**
     * Setter for property cantidad_items.
     * @param cantidad_items New value of property cantidad_items.
     */
    public void setCantidad_items(int cantidad_items) {
        this.cantidad_items = cantidad_items;
    }

    /**
     * Getter for property clasificacion1.
     * @return Value of property clasificacion1.
     */
    public java.lang.String getClasificacion1() {
        return clasificacion1;
    }

    /**
     * Setter for property clasificacion1.
     * @param clasificacion1 New value of property clasificacion1.
     */
    public void setClasificacion1(java.lang.String clasificacion1) {
        this.clasificacion1 = clasificacion1;
    }

    /**
     * Getter for property clasificacion2.
     * @return Value of property clasificacion2.
     */
    public java.lang.String getClasificacion2() {
        return clasificacion2;
    }

    /**
     * Setter for property clasificacion2.
     * @param clasificacion2 New value of property clasificacion2.
     */
    public void setClasificacion2(java.lang.String clasificacion2) {
        this.clasificacion2 = clasificacion2;
    }

    /**
     * Getter for property clasificacion3.
     * @return Value of property clasificacion3.
     */
    public java.lang.String getClasificacion3() {
        return clasificacion3;
    }

    /**
     * Setter for property clasificacion3.
     * @param clasificacion3 New value of property clasificacion3.
     */
    public void setClasificacion3(java.lang.String clasificacion3) {
        this.clasificacion3 = clasificacion3;
    }

    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }

    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }

    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }

    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }

    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }

    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }

    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }

    /**
     * Getter for property fecha_anulacion.
     * @return Value of property fecha_anulacion.
     */
    public java.lang.String getFecha_anulacion() {
        return fecha_anulacion;
    }

    /**
     * Setter for property fecha_anulacion.
     * @param fecha_anulacion New value of property fecha_anulacion.
     */
    public void setFecha_anulacion(java.lang.String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }

    /**
     * Getter for property fecha_contabilizacion.
     * @return Value of property fecha_contabilizacion.
     */
    public java.lang.String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    /**
     * Setter for property fecha_contabilizacion.
     * @param fecha_contabilizacion New value of property fecha_contabilizacion.
     */
    public void setFecha_contabilizacion(java.lang.String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    /**
     * Getter for property fecha_contabilizacion_anulacion.
     * @return Value of property fecha_contabilizacion_anulacion.
     */
    public java.lang.String getFecha_contabilizacion_anulacion() {
        return fecha_contabilizacion_anulacion;
    }

    /**
     * Setter for property fecha_contabilizacion_anulacion.
     * @param fecha_contabilizacion_anulacion New value of property fecha_contabilizacion_anulacion.
     */
    public void setFecha_contabilizacion_anulacion(java.lang.String fecha_contabilizacion_anulacion) {
        this.fecha_contabilizacion_anulacion = fecha_contabilizacion_anulacion;
    }

    /**
     * Getter for property fecha_factura.
     * @return Value of property fecha_factura.
     */
    public java.lang.String getFecha_factura() {
        return fecha_factura;
    }

    /**
     * Setter for property fecha_factura.
     * @param fecha_factura New value of property fecha_factura.
     */
    public void setFecha_factura(java.lang.String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }

    /**
     * Getter for property fecha_impresion.
     * @return Value of property fecha_impresion.
     */
    public java.lang.String getFecha_impresion() {
        return fecha_impresion;
    }

    /**
     * Setter for property fecha_impresion.
     * @param fecha_impresion New value of property fecha_impresion.
     */
    public void setFecha_impresion(java.lang.String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }

    /**
     * Getter for property fecha_ultimo_pago.
     * @return Value of property fecha_ultimo_pago.
     */
    public java.lang.String getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    /**
     * Setter for property fecha_ultimo_pago.
     * @param fecha_ultimo_pago New value of property fecha_ultimo_pago.
     */
    public void setFecha_ultimo_pago(java.lang.String fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    /**
     * Getter for property fecha_vencimiento.
     * @return Value of property fecha_vencimiento.
     */
    public java.lang.String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * Setter for property fecha_vencimiento.
     * @param fecha_vencimiento New value of property fecha_vencimiento.
     */
    public void setFecha_vencimiento(java.lang.String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * Getter for property forma_pago.
     * @return Value of property forma_pago.
     */
    public java.lang.String getForma_pago() {
        return forma_pago;
    }

    /**
     * Setter for property forma_pago.
     * @param forma_pago New value of property forma_pago.
     */
    public void setForma_pago(java.lang.String forma_pago) {
        this.forma_pago = forma_pago;
    }

    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }

    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }

    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }

    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }

    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }

    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }

    /**
     * Getter for property transaccion.
     * @return Value of property transaccion.
     */
    public java.lang.Double getTransaccion() {
        return transaccion;
    }

    /**
     * Setter for property transaccion.
     * @param transaccion New value of property transaccion.
     */
    public void setTransaccion(java.lang.Double transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * Getter for property valor_abono.
     * @return Value of property valor_abono.
     */
    public double getValor_abono() {
        return valor_abono;
    }

    /**
     * Setter for property valor_abono.
     * @param valor_abono New value of property valor_abono.
     */
    public void setValor_abono(double valor_abono) {
        this.valor_abono = valor_abono;
    }

    /**
     * Getter for property valor_abonome.
     * @return Value of property valor_abonome.
     */
    public double getValor_abonome() {
        return valor_abonome;
    }

    /**
     * Setter for property valor_abonome.
     * @param valor_abonome New value of property valor_abonome.
     */
    public void setValor_abonome(double valor_abonome) {
        this.valor_abonome = valor_abonome;
    }

    /**
     * Getter for property valor_factura.
     * @return Value of property valor_factura.
     */
    public double getValor_factura() {
        return valor_factura;
    }

    /**
     * Setter for property valor_factura.
     * @param valor_factura New value of property valor_factura.
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }

    /**
     * Getter for property valor_facturame.
     * @return Value of property valor_facturame.
     */
    public double getValor_facturame() {
        return valor_facturame;
    }

    /**
     * Setter for property valor_facturame.
     * @param valor_facturame New value of property valor_facturame.
     */
    public void setValor_facturame(double valor_facturame) {
        this.valor_facturame = valor_facturame;
    }

    /**
     * Getter for property valor_saldo.
     * @return Value of property valor_saldo.
     */
    public double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * Setter for property valor_saldo.
     * @param valor_saldo New value of property valor_saldo.
     */
    public void setValor_saldo(double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    /**
     * Getter for property valor_saldome.
     * @return Value of property valor_saldome.
     */
    public double getValor_saldome() {
        return valor_saldome;
    }

    /**
     * Setter for property valor_saldome.
     * @param valor_saldome New value of property valor_saldome.
     */
    public void setValor_saldome(double valor_saldome) {
        this.valor_saldome = valor_saldome;
    }

    /**
     * Getter for property valor_tasa.
     * @return Value of property valor_tasa.
     */
    public java.lang.Double getValor_tasa() {
        return valor_tasa;
    }

    /**
     * Setter for property valor_tasa.
     * @param valor_tasa New value of property valor_tasa.
     */
    public void setValor_tasa(java.lang.Double valor_tasa) {
        this.valor_tasa = valor_tasa;
    }

    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }

    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }

    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }

    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }

    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public java.lang.String getTelefono() {
        return telefono;
    }

    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }

    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }

    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * Getter for property nomcli.
     * @return Value of property nomcli.
     */
    public java.lang.String getNomcli() {
        return nomcli;
    }

    /**
     * Setter for property nomcli.
     * @param nomcli New value of property nomcli.
     */
    public void setNomcli(java.lang.String nomcli) {
        this.nomcli = nomcli;
    }

    /**
     * Getter for property plazo.
     * @return Value of property plazo.
     */
    public java.lang.String getPlazo() {
        return plazo;
    }

    /**
     * Setter for property plazo.
     * @param plazo New value of property plazo.
     */
    public void setPlazo(java.lang.String plazo) {
        this.plazo = plazo;
    }

    /**
     * Getter for property ccuenta.
     * @return Value of property ccuenta.
     */
    public java.lang.String getCcuenta() {
        return ccuenta;
    }

    /**
     * Setter for property ccuenta.
     * @param ccuenta New value of property ccuenta.
     */
    public void setCcuenta(java.lang.String ccuenta) {
        this.ccuenta = ccuenta;
    }

    /**
     * Getter for property nom_agencia_facturacion.
     * @return Value of property nom_agencia_facturacion.
     */
    public java.lang.String getNom_agencia_facturacion() {
        return nom_agencia_facturacion;
    }

    /**
     * Setter for property nom_agencia_facturacion.
     * @param nom_agencia_facturacion New value of property nom_agencia_facturacion.
     */
    public void setNom_agencia_facturacion(java.lang.String nom_agencia_facturacion) {
        this.nom_agencia_facturacion = nom_agencia_facturacion;
    }

    /**
     * Getter for property hc.
     * @return Value of property hc.
     */
    public java.lang.String getHc() {
        return hc;
    }

    /**
     * Setter for property hc.
     * @param hc New value of property hc.
     */
    public void setHc(java.lang.String hc) {
        this.hc = hc;
    }

    /**
     * Getter for property rif.
     * @return Value of property rif.
     */
    public java.lang.String getRif() {
        return rif;
    }

    /**
     * Setter for property rif.
     * @param rif New value of property rif.
     */
    public void setRif(java.lang.String rif) {
        this.rif = rif;
    }

    /**
     * Getter for property vlr_bruto_ml.
     * @return Value of property vlr_bruto_ml.
     */
    public double getVlr_bruto_ml() {
        return vlr_bruto_ml;
    }

    /**
     * Setter for property vlr_bruto_ml.
     * @param vlr_bruto_ml New value of property vlr_bruto_ml.
     */
    public void setVlr_bruto_ml(double vlr_bruto_ml) {
        this.vlr_bruto_ml = vlr_bruto_ml;
    }

    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }

    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }

    /**
     * Getter for property Documen_ing.
     * @return Value of property Documen_ing.
     */
    public java.lang.String getDocumen_ing() {
        return Documen_ing;
    }

    /**
     * Setter for property Documen_ing.
     * @param Documen_ing New value of property Documen_ing.
     */
    public void setDocumen_ing(java.lang.String Documen_ing) {
        this.Documen_ing = Documen_ing;
    }

    /**
     * Getter for property num_ingreso.
     * @return Value of property num_ingreso.
     */
    public java.lang.String getNum_ingreso() {
        return num_ingreso;
    }

    /**
     * Setter for property num_ingreso.
     * @param num_ingreso New value of property num_ingreso.
     */
    public void setNum_ingreso(java.lang.String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    /**
     * Getter for property vlr_ingreso.
     * @return Value of property vlr_ingreso.
     */
    public java.lang.String getVlr_ingreso() {
        return vlr_ingreso;
    }

    /**
     * Setter for property vlr_ingreso.
     * @param vlr_ingreso New value of property vlr_ingreso.
     */
    public void setVlr_ingreso(java.lang.String vlr_ingreso) {
        this.vlr_ingreso = vlr_ingreso;
    }

    /**
     * Getter for property codmoneda.
     * @return Value of property codmoneda.
     */
    public java.lang.String getCodmoneda() {
        return codmoneda;
    }

    /**
     * Setter for property codmoneda.
     * @param codmoneda New value of property codmoneda.
     */
    public void setCodmoneda(java.lang.String codmoneda) {
        this.codmoneda = codmoneda;
    }

    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * Getter for property Nfacturas.
     * @return Value of property Nfacturas.
     */
    public java.lang.String getNfacturas() {
        return Nfacturas;
    }

    /**
     * Setter for property Nfacturas.
     * @param Nfacturas New value of property Nfacturas.
     */
    public void setNfacturas(java.lang.String Nfacturas) {
        this.Nfacturas = Nfacturas;
    }

    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }

    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }

    /**
     * Getter for property fecha_ingreso.
     * @return Value of property fecha_ingreso.
     */
    public java.lang.String getFecha_ingreso() {
        return fecha_ingreso;
    }

    /**
     * Setter for property fecha_ingreso.
     * @param fecha_ingreso New value of property fecha_ingreso.
     */
    public void setFecha_ingreso(java.lang.String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    /**
     * Getter for property cplazo.
     * @return Value of property cplazo.
     */
    public java.lang.String getCplazo() {
        return cplazo;
    }

    /**
     * Setter for property cplazo.
     * @param cplazo New value of property cplazo.
     */
    public void setCplazo(java.lang.String cplazo) {
        this.cplazo = cplazo;
    }

    /**
     * Getter for property simbolo.
     * @return Value of property simbolo.
     */
    public java.lang.String getSimbolo() {
        return simbolo;
    }

    /**
     * Setter for property simbolo.
     * @param simbolo New value of property simbolo.
     */
    public void setSimbolo(java.lang.String simbolo) {
        this.simbolo = simbolo;
    }

    /**
     * Getter for property descripcion_ruta.
     * @return Value of property descripcion_ruta.
     */
    public java.lang.String getDescripcion_ruta() {
        return descripcion_ruta;
    }

    /**
     * Setter for property descripcion_ruta.
     * @param descripcion_ruta New value of property descripcion_ruta.
     */
    public void setDescripcion_ruta(java.lang.String descripcion_ruta) {
        this.descripcion_ruta = descripcion_ruta;
    }

    /**
     * Getter for property vlr_freight.
     * @return Value of property vlr_freight.
     */
    public double getVlr_freight() {
        return vlr_freight;
    }

    /**
     * Setter for property vlr_freight.
     * @param vlr_freight New value of property vlr_freight.
     */
    public void setVlr_freight(double vlr_freight) {
        this.vlr_freight = vlr_freight;
    }

    /**
     * Getter for property simbolo_descripcion.
     * @return Value of property simbolo_descripcion.
     */
    public java.lang.String getSimbolo_descripcion() {
        return simbolo_descripcion;
    }

    /**
     * Setter for property simbolo_descripcion.
     * @param simbolo_descripcion New value of property simbolo_descripcion.
     */
    public void setSimbolo_descripcion(java.lang.String simbolo_descripcion) {
        this.simbolo_descripcion = simbolo_descripcion;
    }

    /**
     * Getter for property agencia_impresion.
     * @return Value of property agencia_impresion.
     */
    public java.lang.String getAgencia_impresion() {
        return agencia_impresion;
    }

    /**
     * Setter for property agencia_impresion.
     * @param agencia_impresion New value of property agencia_impresion.
     */
    public void setAgencia_impresion(java.lang.String agencia_impresion) {
        this.agencia_impresion = agencia_impresion;
    }

    /**
     * Getter for property formato.
     * @return Value of property formato.
     */
    public java.lang.String getFormato() {
        return formato;
    }

    /**
     * Setter for property formato.
     * @param formato New value of property formato.
     */
    public void setFormato(java.lang.String formato) {
        this.formato = formato;
    }

    public double getVlr_tasa_remesas() {
        return vlr_tasa_remesas;
    }

    /**
     * Setter for property vlr_tasa_remesas.
     * @param vlr_tasa_remesas New value of property vlr_tasa_remesas.
     */
    public void setVlr_tasa_remesas(double vlr_tasa_remesas) {
        this.vlr_tasa_remesas = vlr_tasa_remesas;
    }

    public int getDias_mora() {
        return dias_mora;
    }

    //rhonalf 2010-07-22
    public void setDias_mora(int dias_mora) {
        this.dias_mora = dias_mora;
    }

    //rhonalf 2010-07-22
    public String getEstado() {
        return estado;
    }

    //rhonalf 2010-07-22
    public void setEstado(String estado) {
        this.estado = estado;
    }

    //rhonalf 2010-07-22
    public String getFisico() {
        return fisico;
    }

    //rhonalf 2010-07-22
    public void setFisico(String fisico) {
        this.fisico = fisico;
    }

    //rhonalf 2010-07-22
    public String getFecha_ven_prejurid() {
        return fecha_ven_prejurid;
    }

    //rhonalf 2010-07-22
    public void setFecha_ven_prejurid(String fecha_ven_prejurid) {
        this.fecha_ven_prejurid = fecha_ven_prejurid;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }
    
    public String getBanco() {
        return banco;
    }
    
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }
    
    public String getSucursal() {
        return sucursal;
    }

    public String getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(String diferencia) {
        this.diferencia = diferencia;
    }
    
}
