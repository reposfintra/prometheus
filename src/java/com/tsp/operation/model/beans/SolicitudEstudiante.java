package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudEstudiante<br/>
 * 1/03/2011
 * @author ivargas-Geotech
 */
public class SolicitudEstudiante {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String direccionEmpresa;
    private String telefonoEmpresa;
    private String nombreEmpresa;
    private String parentescoGirador;
    private String salario;
    private String universidad;
    private String programa;
    private String fechaIngresoPrograma;
    private String codigo;
    private String semestre;
    private String valorSemestre;
    private String tipoCarrera;
    private String trabaja;

    public SolicitudEstudiante load(ResultSet rs) throws SQLException {
        SolicitudEstudiante estudiante = new SolicitudEstudiante();
        estudiante.setNumeroSolicitud(rs.getString("numero_solicitud"));
        estudiante.setParentescoGirador(rs.getString("parentesco_girador"));
        estudiante.setUniversidad(rs.getString("universidad"));
        estudiante.setPrograma(rs.getString("programa"));
        estudiante.setFechaIngresoPrograma(rs.getString("fecha_ingreso_programa"));
        estudiante.setCodigo(rs.getString("codigo"));
        estudiante.setSemestre(rs.getString("semestre"));
        estudiante.setValorSemestre(rs.getString("valor_semestre"));
        estudiante.setTipoCarrera(rs.getString("tipo_carrera"));
        estudiante.setTrabaja(rs.getString("trabaja"));
        estudiante.setNombreEmpresa(rs.getString("nombre_empresa"));
        estudiante.setDireccionEmpresa(rs.getString("direccion_empresa"));
        estudiante.setTelefonoEmpresa(rs.getString("telefono_empresa"));
        estudiante.setSalario(rs.getString("salario"));

        return estudiante;
    }

    /**
     * obtiene el valor de  codigo
     *
     * @return el valor de codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * cambia el valor de codigo
     *
     * @param codigo nuevo valor codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * obtiene el valor de  fechaIngresoPrograma
     *
     * @return el valor de fechaIngresoPrograma
     */
    public String getFechaIngresoPrograma() {
        return fechaIngresoPrograma;
    }

    /**
     * cambia el valor de fechaIngresoPrograma
     *
     * @param fechaIngresoPrograma nuevo valor fechaIngresoPrograma
     */
    public void setFechaIngresoPrograma(String fechaIngresoPrograma) {
        this.fechaIngresoPrograma = fechaIngresoPrograma;
    }

    /**
     * obtiene el valor de  programa
     *
     * @return el valor de programa
     */
    public String getPrograma() {
        return programa;
    }

    /**
     * cambia el valor de programa
     *
     * @param programa nuevo valor programa
     */
    public void setPrograma(String programa) {
        this.programa = programa;
    }

    /**
     * obtiene el valor de salario
     *
     * @return el valor de salario
     */
    public String getSalario() {
        return salario;
    }

    /**
     * cambia el valor de salario
     *
     * @param salario nuevo valor salario
     */
    public void setSalario(String salario) {
        this.salario = salario;
    }

    /**
     * obtiene el valor de  semestre
     *
     * @return el valor de semestre
     */
    public String getSemestre() {
        return semestre;
    }

    /**
     * cambia el valor de valor Semestre
     *
     * @param Semestre nuevo valor Semestre
     */
    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    /**
     * obtiene el valor de  TipoCarrera
     *
     * @return el valor de TipoCarrera
     */
    public String getTipoCarrera() {
        return tipoCarrera;
    }

    /**
     * cambia el valor de tipoCarrera
     *
     * @param tipoCarrera nuevo valor tipoCarrera
     */
    public void setTipoCarrera(String tipoCarrera) {
        this.tipoCarrera = tipoCarrera;
    }

    /**
     * obtiene el valor de  trabaja
     *
     * @return el valor de trabaja
     */
    public String getTrabaja() {
        return trabaja;
    }

    /**
     * cambia el valor de trabaja
     *
     * @param valorSemestre nuevo valor trabaja
     */
    public void setTrabaja(String trabaja) {
        this.trabaja = trabaja;
    }

    /**
     * obtiene el valor de universidad
     *
     * @return el valor de universidad
     */
    public String getUniversidad() {
        return universidad;
    }

    /**
     * cambia el valor de universidad
     *
     * @param valorSemestre nuevo valor universidad
     */
    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    /**
     * obtiene el valor de  valorSemestre
     *
     * @return el valor de valorSemestre
     */
    public String getValorSemestre() {
        return valorSemestre;
    }

    /**
     * cambia el valor de valorSemestre
     *
     * @param valorSemestre nuevo valor valorSemestre
     */
    public void setValorSemestre(String valorSemestre) {
        this.valorSemestre = valorSemestre;
    }

    /**
     * obtiene el valor de nombreEmpresa
     *
     * @return el valor de nombreEmpresa
     */
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    /**
     * cambia el valor de nombreEmpresa
     *
     * @param nombre nuevo valor nombreEmpresa
     */
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    /**
     * obtiene el valor de parentescoGirador
     *
     * @return el valor de parentescoGirador
     */
    public String getParentescoGirador() {
        return parentescoGirador;
    }

    /**
     * cambia el valor de parentescoGirador
     *
     * @param parentesco nuevo valor parentescoGirador
     */
    public void setParentescoGirador(String parentescoGirador) {
        this.parentescoGirador = parentescoGirador;
    }

    /**
     * obtiene el valor de telefonoEmpresa
     *
     * @return el valor de telefonoEmpresa
     */
    public String getTelefonoEmpresa() {
        return telefonoEmpresa;
    }

    /**
     * cambia el valor de telefonoEmpresa
     *
     * @param telefono nuevo valor telefonoEmpresa
     */
    public void setTelefonoEmpresa(String telefonoEmpresa) {
        this.telefonoEmpresa = telefonoEmpresa;
    }

    /**
     * obtiene el valor de direccionEmpresa
     *
     * @return el valor de tdireccionEmpresa
     */
    public String getDireccionEmpresa() {
        return direccionEmpresa;
    }

    /**
     * cambia el valor de direccionEmpresa
     *
     * @param direccion nuevo valor direccionEmpresa
     */
    public void setDireccionEmpresa(String direccionEmpresa) {
        this.direccionEmpresa = direccionEmpresa;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }
}
