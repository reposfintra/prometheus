/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author user
 */
public class BeansVehiculo {
    
    private String cedula_propietario;
    private String placa;
    private String marca;
    private String modelo;
    private String servicio;
    private String tipo_vehiculo;
    private String veto;
    private String veto_causal;

    /**
     * @return the cedula_propietario
     */
    public String getCedula_propietario() {
        return cedula_propietario;
    }

    /**
     * @param cedula_propietario the cedula_propietario to set
     */
    public void setCedula_propietario(String cedula_propietario) {
        this.cedula_propietario = cedula_propietario;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the servicio
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * @param servicio the servicio to set
     */
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    /**
     * @return the tipo_vehiculo
     */
    public String getTipo_vehiculo() {
        return tipo_vehiculo;
    }

    /**
     * @param tipo_vehiculo the tipo_vehiculo to set
     */
    public void setTipo_vehiculo(String tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }

    /**
     * @return the veto
     */
    public String getVeto() {
        return veto;
    }

    /**
     * @param veto the veto to set
     */
    public void setVeto(String veto) {
        this.veto = veto;
    }

    /**
     * @return the veto_causal
     */
    public String getVeto_causal() {
        return veto_causal;
    }

    /**
     * @param veto_causal the veto_causal to set
     */
    public void setVeto_causal(String veto_causal) {
        this.veto_causal = veto_causal;
    }
    
}
