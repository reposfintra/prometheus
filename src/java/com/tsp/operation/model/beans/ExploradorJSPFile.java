/******************************************************************************
 *      Nombre Clase.................   AplicacionService.java
 *      Descripci�n..................   Objeto par arepresentar un archivo:
 *                                      ruta, nombre, tipo.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   08.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;

public class ExploradorJSPFile {
    
    private String path;
    private String nombre;
    private String tipo; /* c - Carpeta, j - P�gina JSP */
    private int id;
    
    /** Creates a new instance of ExploradorJSPFile */
    public ExploradorJSPFile() {
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property path.
     * @return Value of property path.
     */
    public java.lang.String getPath() {
        return path;
    }
    
    /**
     * Setter for property path.
     * @param path New value of property path.
     */
    public void setPath(java.lang.String path) {
        this.path = path;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Toma de un archivo nombre, la ruta, y el tipo
     * @autor Tito Andr�s Maturana
     * @param id N�mero id del archivo o carpeta.
     * @param arc Archivo
     * @version 1.0
     */
    public static ExploradorJSPFile Load( int id, File arc ){
        ExploradorJSPFile datos = new ExploradorJSPFile();
        datos.nombre  = arc.getName();
        datos.path = arc.getPath();
        datos.tipo = (arc.isFile())? "a" : "c";
        datos.id = id;
        return datos;
    }
    
}
