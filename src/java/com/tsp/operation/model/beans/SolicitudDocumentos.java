package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudCuentas<br/>
 * 1/03/2011
 * @author ivargas-Geotech
 */
public class SolicitudDocumentos {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String numTitulo;
    private String valor;
    private String fecha;
    private double comisionAval;
    private String estIndemnizacion;
    private int devuelto;

    public SolicitudDocumentos load(ResultSet rs) throws SQLException {
        SolicitudDocumentos documento = new SolicitudDocumentos();
        documento.setNumeroSolicitud(rs.getString("numero_solicitud"));
        documento.setNumTitulo(rs.getString("num_titulo"));
        documento.setValor(rs.getString("valor"));
        documento.setFecha((rs.getString("fecha")).substring(0, 10));
        documento.setComisionAval(rs.getDouble("comision_aval"));
        documento.setEstIndemnizacion(rs.getString("est_indemnizacion"));
        documento.setDevuelto(rs.getInt("devuelto"));
        
        return documento;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNumTitulo() {
        return numTitulo;
    }

    public void setNumTitulo(String numTitulo) {
        this.numTitulo = numTitulo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }
    
    public double getComisionAval() {
        return comisionAval;
    }

    public void setComisionAval(double comisionAval) {
        this.comisionAval = comisionAval;
    }
   
    public String getEstIndemnizacion() {
        return estIndemnizacion;
    }

    public void setEstIndemnizacion(String estIndemnizacion) {
        this.estIndemnizacion = estIndemnizacion;
    }

    public int getDevuelto() {
        return devuelto;
    }

    public void setDevuelto(int devuelto) {
        this.devuelto = devuelto;
    }
}
