/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class extractoDuplicadoBeans {

    public String cedula;
    public String negocio;
    public String nombre;
    public String convenio;
    public String direccion;
    public String barrio;
    public String ciudad;
    public String departamento;
    public String total_cuotas_vencidas;
    public String cuotas_pendientes;
    public String estado_obligacion;
    public String capital;
    public String interes_financiacion;
    public String int_mora;
    public String gasto_cobranza;
    public String subtotal_corriente;
    public String subtotal_vencido;
    public String subtotal;
    public String total_descuento;
    public String total;
    public String documento;
    public String fecha_vencimiento;
    public String cuota;
    public String dias_mora;
    public String tipo_negocio;
    public String valor_factura;
    public String valor_abono;
    public String valor_saldo;
    public String valor_saldo_capital;
    public String valor_saldo_interes;
    public String IxM;
    public String GaC;
    public String suma_saldos;
    
   
    private double porct_capital;
    private double porct_interes;
    private double porct_ixmora;
    private double porct_gacobranza;
    private double totalExtracto;
    
    private String linea_negocio;
    private String unidad_negocio;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getConvenio() {
        return convenio;
    }

    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getTotal_cuotas_vencidas() {
        return total_cuotas_vencidas;
    }

    public void setTotal_cuotas_vencidas(String total_cuotas_vencidas) {
        this.total_cuotas_vencidas = total_cuotas_vencidas;
    }

    public String getCuotas_pendientes() {
        return cuotas_pendientes;
    }

    public void setCuotas_pendientes(String cuotas_pendientes) {
        this.cuotas_pendientes = cuotas_pendientes;
    }

    public String getEstado_obligacion() {
        return estado_obligacion;
    }

    public void setEstado_obligacion(String estado_obligacion) {
        this.estado_obligacion = estado_obligacion;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getInteres_financiacion() {
        return interes_financiacion;
    }

    public void setInteres_financiacion(String interes_financiacion) {
        this.interes_financiacion = interes_financiacion;
    }

    public String getInt_mora() {
        return int_mora;
    }

    public void setInt_mora(String int_mora) {
        this.int_mora = int_mora;
    }

    public String getGasto_cobranza() {
        return gasto_cobranza;
    }

    public void setGasto_cobranza(String gasto_cobranza) {
        this.gasto_cobranza = gasto_cobranza;
    }

    public String getSubtotal_corriente() {
        return subtotal_corriente;
    }

    public void setSubtotal_corriente(String subtotal_corriente) {
        this.subtotal_corriente = subtotal_corriente;
    }

    public String getSubtotal_vencido() {
        return subtotal_vencido;
    }

    public void setSubtotal_vencido(String subtotal_vencido) {
        this.subtotal_vencido = subtotal_vencido;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTotal_descuento() {
        return total_descuento;
    }

    public void setTotal_descuento(String total_descuento) {
        this.total_descuento = total_descuento;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getCuota() {
        return cuota;
    }

    public void setCuota(String cuota) {
        this.cuota = cuota;
    }

    public String getDias_mora() {
        return dias_mora;
    }

    public void setDias_mora(String dias_mora) {
        this.dias_mora = dias_mora;
    }

    public String getTipo_negocio() {
        return tipo_negocio;
    }

    public void setTipo_negocio(String tipo_negocio) {
        this.tipo_negocio = tipo_negocio;
    }

    public String getValor_factura() {
        return valor_factura;
    }

    public void setValor_factura(String valor_factura) {
        this.valor_factura = valor_factura;
    }

    public String getValor_abono() {
        return valor_abono;
    }

    public void setValor_abono(String valor_abono) {
        this.valor_abono = valor_abono;
    }

    public String getValor_saldo() {
        return valor_saldo;
    }

    public void setValor_saldo(String valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    public String getValor_saldo_capital() {
        return valor_saldo_capital;
    }

    public void setValor_saldo_capital(String valor_saldo_capital) {
        this.valor_saldo_capital = valor_saldo_capital;
    }

    public String getValor_saldo_interes() {
        return valor_saldo_interes;
    }

    public void setValor_saldo_interes(String valor_saldo_interes) {
        this.valor_saldo_interes = valor_saldo_interes;
    }

    public String getIxM() {
        return IxM;
    }

    public void setIxM(String IxM) {
        this.IxM = IxM;
    }

    public String getGaC() {
        return GaC;
    }

    public void setGaC(String GaC) {
        this.GaC = GaC;
    }

    public String getSuma_saldos() {
        return suma_saldos;
    }

    public void setSuma_saldos(String suma_saldos) {
        this.suma_saldos = suma_saldos;
    }

    public double getPorct_ixmora() {
        return porct_ixmora;
    }

    public void setPorct_ixmora(double porct_ixmora) {
        this.porct_ixmora = porct_ixmora;
    }

    public double getPorct_gacobranza() {
        return porct_gacobranza;
    }

    public void setPorct_gacobranza(double porct_gacobranza) {
        this.porct_gacobranza = porct_gacobranza;
    }

    public double getTotalExtracto() {
        return totalExtracto;
    }

    public void setTotalExtracto(double totalExtracto) {
        this.totalExtracto = totalExtracto;
    }

    public double getPorct_capital() {
        return porct_capital;
    }

    public void setPorct_capital(double porct_capital) {
        this.porct_capital = porct_capital;
    }

    public double getPorct_interes() {
        return porct_interes;
    }

    public void setPorct_interes(double porct_interes) {
        this.porct_interes = porct_interes;
    }

    public String getLinea_negocio() {
        return linea_negocio;
    }

    public void setLinea_negocio(String linea_negocio) {
        this.linea_negocio = linea_negocio;
    }

    public String getUnidad_negocio() {
        return unidad_negocio;
    }

    public void setUnidad_negocio(String unidad_negocio) {
        this.unidad_negocio = unidad_negocio;
    }
    
    

}
