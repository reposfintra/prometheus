/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author edgonzalez
 */
public class ConceptoFactura {
    
    private double saldoCapital;
    private double saldoCapitalAval;
    private double saldoInteres;
    private double saldoCatVencido;
    private double saldoCat;    
    private double saldoCuotaAdmin ;
    private double saldoInteresAval ;
    private double saldoIntXmora ;
    private double saldoGac ;
    private int dias_mora;
    

    public ConceptoFactura() {
    }

    public ConceptoFactura(double saldoCapital, double saldoCapitalAval, double saldoInteres, double saldoCatVencido, double saldoCat, double saldoCuotaAdmin, double saldoInteresAval, double saldoIntXmora, double saldoGac, int dias_mora) {
        this.saldoCapital = saldoCapital;
        this.saldoCapitalAval = saldoCapitalAval;
        this.saldoInteres = saldoInteres;
        this.saldoCatVencido = saldoCatVencido;
        this.saldoCat = saldoCat;
        this.saldoCuotaAdmin = saldoCuotaAdmin;
        this.saldoInteresAval = saldoInteresAval;
        this.saldoIntXmora = saldoIntXmora;
        this.saldoGac = saldoGac;
        this.dias_mora = dias_mora;
    }

 

    /**
     * @return the saldoCapital
     */
    public double getSaldoCapital() {
        return saldoCapital;
    }

    /**
     * @param saldoCapital the saldoCapital to set
     */
    public void setSaldoCapital(double saldoCapital) {
        this.saldoCapital = saldoCapital;
    }

    /**
     * @return the saldoCapitalAval
     */
    public double getSaldoCapitalAval() {
        return saldoCapitalAval;
    }

    /**
     * @param saldoCapitalAval the saldoCapitalAval to set
     */
    public void setSaldoCapitalAval(double saldoCapitalAval) {
        this.saldoCapitalAval = saldoCapitalAval;
    }

    /**
     * @return the saldoInteres
     */
    public double getSaldoInteres() {
        return saldoInteres;
    }

    /**
     * @param saldoInteres the saldoInteres to set
     */
    public void setSaldoInteres(double saldoInteres) {
        this.saldoInteres = saldoInteres;
    }

    /**
     * @return the saldoCat
     */
    public double getSaldoCat() {
        return saldoCat;
    }

    /**
     * @param saldoCat the saldoCat to set
     */
    public void setSaldoCat(double saldoCat) {
        this.saldoCat = saldoCat;
    }

    /**
     * @return the saldoCuotaAdmin
     */
    public double getSaldoCuotaAdmin() {
        return saldoCuotaAdmin;
    }

    /**
     * @param saldoCuotaAdmin the saldoCuotaAdmin to set
     */
    public void setSaldoCuotaAdmin(double saldoCuotaAdmin) {
        this.saldoCuotaAdmin = saldoCuotaAdmin;
    }

    /**
     * @return the saldoInteresAval
     */
    public double getSaldoInteresAval() {
        return saldoInteresAval;
    }

    /**
     * @param saldoInteresAval the saldoInteresAval to set
     */
    public void setSaldoInteresAval(double saldoInteresAval) {
        this.saldoInteresAval = saldoInteresAval;
    }

    /**
     * @return the saldoIntXmora
     */
    public double getSaldoIntXmora() {
        return saldoIntXmora;
    }

    /**
     * @param saldoIntXmora the saldoIntXmora to set
     */
    public void setSaldoIntXmora(double saldoIntXmora) {
        this.saldoIntXmora = saldoIntXmora;
    }

    /**
     * @return the saldoGac
     */
    public double getSaldoGac() {
        return saldoGac;
    }

    /**
     * @param saldoGac the saldoGac to set
     */
    public void setSaldoGac(double saldoGac) {
        this.saldoGac = saldoGac;
    }
    
    
    public double getSaldoActual(){
        return saldoCapital+saldoInteres+saldoCat+saldoCuotaAdmin+saldoCapitalAval+saldoInteresAval;
    }

     
    public double getSaldoActualSanciones(){
        return saldoCapital+saldoInteres+saldoCat+saldoCuotaAdmin+saldoCapitalAval+saldoInteresAval+saldoIntXmora+saldoGac;
    }

    /**
     * @return the dias_mora
     */
    public int getDias_mora() {
        return dias_mora;
    }

    /**
     * @param dias_mora the dias_mora to set
     */
    public void setDias_mora(int dias_mora) {
        this.dias_mora = dias_mora;
    }

    public double getSaldoCatVencido() {
        return saldoCatVencido;
    }

    public void setSaldoCatVencido(double saldoCatVencido) {
        this.saldoCatVencido = saldoCatVencido;
    }
    
    
}
