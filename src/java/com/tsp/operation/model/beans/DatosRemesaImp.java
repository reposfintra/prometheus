/*
 * DatosRemesaImp.java
 *
 * Created on 16 de diciembre de 2004, 19:41
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Mario Fontalvo
 */
public class DatosRemesaImp implements Serializable{
    
    //datos basicos de la remesa
    private String NumeroRemesa;
    private String DocumentoInterno;
    private String Sj;
    private String CodigoCliente;
    private String Cliente;
    private String TextoOC;
    
    // otros datos
    private List Descuentos;
    private List Remitentes;
    private List Destinatarios;
    
    //Mario 30.12.05
    private TreeMap Soportes;
    
    //jescandon 26.01.06
    private String observaciones;
    
    /** Creates a new instance of DatosRemesaImp */
    public DatosRemesaImp() {        
    }
    
    public static DatosRemesaImp load(ResultSet rs)throws Exception{
        DatosRemesaImp datos = new DatosRemesaImp();
        datos.setNumeroRemesa    (rs.getString(1));
        datos.setDocumentoInterno(rs.getString(2));
        datos.setSJ              (rs.getString(3));
        datos.setCodigoCliente   (rs.getString(4));
        datos.setCliente         (rs.getString(5));
        datos.setTextoOC         (rs.getString(6));
        
        datos.setDescuentos      (null);
        datos.setRemitentes      (null);
        datos.setDestinatarios   (null);
        //jescandon 26.01.06
        datos.setObservaciones   (rs.getString("observacion"));
        return datos;        
    }  
    
    public static DatosRemesaImp loadRemesas(ResultSet rs)throws Exception{
        DatosRemesaImp datos = new DatosRemesaImp();
        datos.setNumeroRemesa    (rs.getString(1));
        datos.setDocumentoInterno(rs.getString(2));
        datos.setSJ              (rs.getString(3));
        datos.setCodigoCliente   (rs.getString(4));
        datos.setCliente         (rs.getString(5));
        datos.setTextoOC         (rs.getString(6));
        
        datos.setDescuentos      (null);
        datos.setRemitentes      (null);
        datos.setDestinatarios   (null);
        //jescandon 26.01.06
        datos.setObservaciones   (rs.getString("observacion"));
        return datos;        
    } 
    
    //setter
    public void setNumeroRemesa(String valor){
        this.NumeroRemesa = valor;
    }
    public void setDocumentoInterno(String valor){
        this.DocumentoInterno = valor;
    }
    public void setSJ(String valor){
        this.Sj = valor;
    }
    public void setCliente(String valor){
        this.Cliente = valor;
    }
    public void setCodigoCliente(String valor){
        this.CodigoCliente = valor;
    }
    public void setTextoOC(String valor){
        this.TextoOC= valor;
    }

    public void setDescuentos(List valor){
        this.Descuentos = valor;
    }
    public void setRemitentes(List valor){
        this.Remitentes = valor;
    }
    public void setDestinatarios(List valor){
        this.Destinatarios = valor;
    }
    //getter
    public String getNumeroRemesa(){
        return this.NumeroRemesa;
    }
    public String getDocumentoInterno(){
        return this.DocumentoInterno;
    }
    public String getSJ(){
        return this.Sj;
    }
    public String getCliente(){
        return this.Cliente;
    }
    public String getCodigoCliente(){
        return this.CodigoCliente;
    }
    public String getTextoOC(){
        return this.TextoOC;
    }

    public List getDescuentos(){
        return this.Descuentos;
    }
    public List getRemitentes(){
        return this.Remitentes;
    }
    public List getDestinatarios(){
        return this.Destinatarios;
    }
    
    /**
     * Getter for property Soportes.
     * @return Value of property Soportes.
     */
    public java.util.TreeMap getSoportes() {
        return Soportes;
    }    
    
    /**
     * Setter for property Soportes.
     * @param Soportes New value of property Soportes.
     */
    public void setSoportes(java.util.TreeMap Soportes) {
        this.Soportes = Soportes;
    }
    
    /**
     * Getter for property observaciones.
     * @return Value of property observaciones.
     */
    public java.lang.String getObservaciones() {
        return observaciones;
    }
    
    /**
     * Setter for property observaciones.
     * @param observaciones New value of property observaciones.
     */
    public void setObservaciones(java.lang.String observaciones) {
        this.observaciones = observaciones;
    }
    
}
