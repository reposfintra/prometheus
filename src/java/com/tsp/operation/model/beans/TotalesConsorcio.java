/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class TotalesConsorcio {


    // Control totales para 1
    private double valor_inicial_subtotal_base_1;
    private double subtotal_base_1;
    // Control totales para 2
    private double valor_inicial_base_iva;
    private double base_iva;
    // Control totales para 3
    private double valor_inicial_a_financiar;
    private double valor_a_financiar;
    // Control totales para 4
    private double valor_inicial_intereses;
    private double intereses;
    // Control totales para 5
    private double valor_inicial_con_financiacion;
    private double valor_con_financiacion;




    /** Creates a new instance of Subtotal_base_1 */
    public TotalesConsorcio() {

        this.valor_inicial_subtotal_base_1 = 0.00;
        this.subtotal_base_1 = 0.00;
        
        
        this.valor_inicial_base_iva = 0.00;
        this.base_iva = 0.00;
        

        this.valor_inicial_a_financiar = 0.00;
        this.valor_a_financiar = 0.00;

        this.valor_inicial_intereses = 0.00;
        this.intereses = 0.00;

        this.valor_inicial_con_financiacion = 0.00;
        this.valor_con_financiacion = 0.00;



    }



    /** Inicializa las variables del objeto */
    public void clear() {


        this.setValor_inicial_subtotal_base_1(0.00);
        this.setSubtotal_base_1(0.00);
        
        
        this.setValor_inicial_base_iva(0.00);
        this.setBase_iva(0.00);
        
        this.setValor_inicial_a_financiar(0.00);
        this.setValor_a_financiar(0.00);
        
        this.setValor_inicial_intereses(0.00);
        this.setIntereses(0.00);
        
        this.setValor_inicial_con_financiacion(0.00);
        this.setValor_con_financiacion(0.00);


    }




    public void setValorInicial(double valor, int k){
      
        switch (k) {
        case 1:  this.valor_inicial_subtotal_base_1 = valor ; break;
        case 2:  this.valor_inicial_base_iva = valor ; break;

        case 3:  this.valor_inicial_a_financiar = valor ; break;
        case 4:  this.valor_inicial_intereses = valor ; break;
        case 5:  this.valor_inicial_con_financiacion = valor ; break;

        }        
    }


    public void setValorTotal(double valor, int k){

        switch (k) {
        case 1:  this.subtotal_base_1 = valor ; break;
        case 2:  this.base_iva        = valor ; break;

        case 3:  this.valor_a_financiar      = valor ; break;
        case 4:  this.intereses              = valor ; break;
        case 5:  this.valor_con_financiacion = valor ; break;

        }
    }

    















    /**
     * Devuelve el mismo valor o la diferencia entre un valor total menos el valor recibido.
     * La diferencia entre el valor total menos el valor recibido corresponderia al ultimo item
     * Desarrollado para determinar el ultimo valor sin problemas de redondeo cuando numeroTotalItem = numeroItemProcesado
     * @param valor Valor del item
     * @param k Permite identificar con cual de los valores se va a generar el calculo
     * @param numeroTotalItem Indica el numero total de items a calcular
     * @param numeroItemProcesado Indica el item a calcular
     * @return Retorna el mismo valor o la diferencia entre el total menos los acumulados hasta el momento
     */
    public double set(double valor, int k,  int numeroTotalItem, int numeroItemProcesado){

        switch (k) {
            case 1 : {
                if(numeroItemProcesado==numeroTotalItem){
                    // Calcula el ultimo item por diferencia entre el total - totales acumulados y acumula
                    double valor_item = this.valor_inicial_subtotal_base_1 - this.subtotal_base_1;
                    this.subtotal_base_1  += valor_item;
                    return valor_item;
                }
                else {
                    // Devuelve el mismo valor del item y acumula
                    this.subtotal_base_1 += valor;
                    return valor;
                }
            }
            case 2 : {
                if(numeroItemProcesado==numeroTotalItem){
                    // Calcula el ultimo item por diferencia entre el total - totales acumulados
                    double valor_item = this.valor_inicial_base_iva - this.base_iva;
                    this.base_iva  += valor_item;
                    return valor_item;
                }
                else {
                    // Devuelve el mismo valor del item
                    this.base_iva += valor;
                    return valor;
                }
            }
             // CALCULO DE LA ULTIMA CUOTA DE VALOR A FINANCIAR
             case 3 : {
                if(numeroItemProcesado==numeroTotalItem){
                    // Calcula el ultimo item por diferencia entre el total - totales acumulados
                    double valor_item = this.valor_inicial_a_financiar - this.valor_a_financiar;
                    this.valor_a_financiar  += valor_item;
                    return valor_item;
                }
                else {
                    // Devuelve el mismo valor del item
                    this.valor_a_financiar += valor;
                    return valor;
                }
            }
            //CALCULO DE LA ULTIMA CUOTA DE LOS INTERESES
            case 4 : {
                if(numeroItemProcesado==numeroTotalItem){
                    // Calcula el ultimo item por diferencia entre el total - totales acumulados
                    double valor_item = this.valor_inicial_intereses - this.intereses;
                    this.intereses  += valor_item;
                    return valor_item;
                }
                else {
                    // Devuelve el mismo valor del item
                    this.intereses += valor;
                    return valor;
                }
            }
            // VALOR DE LA ULTIMA CUOTA DEL VALOR CON FINANCIACION
            case 5 : {
                if(numeroItemProcesado==numeroTotalItem){
                    // Calcula el ultimo item por diferencia entre el total - totales acumulados
                    double valor_item = this.valor_inicial_con_financiacion - this.valor_con_financiacion;
                    this.valor_con_financiacion  += valor_item;
                    return valor_item;
                }
                else {
                    // Devuelve el mismo valor del item
                    this.valor_con_financiacion += valor;
                    return valor;
                }
            }


        }
        
        return 0.00;
    }












    /**
     * @return the subtotal_base_1
     */
    public double getSubtotal_base_1() {
        return subtotal_base_1;
    }

    /**
     * @param subtotal_base_1 the subtotal_base_1 to set
     */
    public void setSubtotal_base_1(double subtotal_base_1) {
        this.subtotal_base_1 = subtotal_base_1;
    }

    /**
     * @return the valor_inicial_subtotal_base_1
     */
    public double getValor_inicial_subtotal_base_1() {
        return valor_inicial_subtotal_base_1;
    }

    /**
     * @param valor_inicial_subtotal_base_1 the valor_inicial_subtotal_base_1 to set
     */
    public void setValor_inicial_subtotal_base_1(double valor_inicial_subtotal_base_1) {
        this.valor_inicial_subtotal_base_1 = valor_inicial_subtotal_base_1;
    }


    /**
     * @return the base_iva
     */
    public double getBase_iva() {
        return base_iva;
    }

    /**
     * @param base_iva the base_iva to set
     */
    public void setBase_iva(double base_iva) {
        this.base_iva = base_iva;
    }

    /**
     * @return the valor_inicial_base_iva
     */
    public double getValor_inicial_base_iva() {
        return valor_inicial_base_iva;
    }

    /**
     * @param valor_inicial_base_iva the valor_inicial_base_iva to set
     */
    public void setValor_inicial_base_iva(double valor_inicial_base_iva) {
        this.valor_inicial_base_iva = valor_inicial_base_iva;
    }

    /**
     * @return the valor_inicial_a_financiar
     */
    public double getValor_inicial_a_financiar() {
        return valor_inicial_a_financiar;
    }

    /**
     * @param valor_inicial_a_financiar the valor_inicial_a_financiar to set
     */
    public void setValor_inicial_a_financiar(double valor_inicial_a_financiar) {
        this.valor_inicial_a_financiar = valor_inicial_a_financiar;
    }

    /**
     * @return the valor_a_financiar
     */
    public double getValor_a_financiar() {
        return valor_a_financiar;
    }

    /**
     * @param valor_a_financiar the valor_a_financiar to set
     */
    public void setValor_a_financiar(double valor_a_financiar) {
        this.valor_a_financiar = valor_a_financiar;
    }

    /**
     * @return the valor_inicial_intereses
     */
    public double getValor_inicial_intereses() {
        return valor_inicial_intereses;
    }

    /**
     * @param valor_inicial_intereses the valor_inicial_intereses to set
     */
    public void setValor_inicial_intereses(double valor_inicial_intereses) {
        this.valor_inicial_intereses = valor_inicial_intereses;
    }

    /**
     * @return the intereses
     */
    public double getIntereses() {
        return intereses;
    }

    /**
     * @param intereses the intereses to set
     */
    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }

    /**
     * @return the valor_inicial_con_financiacion
     */
    public double getValor_inicial_con_financiacion() {
        return valor_inicial_con_financiacion;
    }

    /**
     * @param valor_inicial_con_financiacion the valor_inicial_con_financiacion to set
     */
    public void setValor_inicial_con_financiacion(double valor_inicial_con_financiacion) {
        this.valor_inicial_con_financiacion = valor_inicial_con_financiacion;
    }

    /**
     * @return the valor_con_financiacion
     */
    public double getValor_con_financiacion() {
        return valor_con_financiacion;
    }

    /**
     * @param valor_con_financiacion the valor_con_financiacion to set
     */
    public void setValor_con_financiacion(double valor_con_financiacion) {
        this.valor_con_financiacion = valor_con_financiacion;
    }


}
