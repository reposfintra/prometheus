/***********************************************************************************
 * Nombre clase : ............... ReporteViaje.java                                *
 * Descripcion :................. Clase que maneja los atributos relacionados con  *
 *                                el reporte de viajes                             *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 16 de noviembre de 2005, 10:55 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;

public class ReporteViaje {
    private String placa;
    private String fecdescargue;
    private String numpla;
    private String numrem;
    private String remision;
    private double pesoreal;
    private String unidad;
    private String origen;
    private String destino;
    private String descripcion;
    /** Creates a new instance of ReporteViaje */
    public ReporteViaje() {
    }
    
    
     /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo ReporteViaje
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static ReporteViaje load(ResultSet rs)throws Exception{
        ReporteViaje datos = new ReporteViaje();
        datos.setPlaca(rs.getString("PLAVEH"));
        datos.setFecdescargue(rs.getString("FECCUM"));
        datos.setNumpla(rs.getString("NUMPLA"));
        datos.setNumrem(rs.getString("NUMREM"));
        datos.setRemision(rs.getString("REMISION"));
        datos.setPesoreal(rs.getDouble("PESOREAL"));
        datos.setUnidad(rs.getString("UNIT_VLR"));
        datos.setOrigen(rs.getString("ORIGEN"));
        datos.setDestino(rs.getString("DESTINO"));
        datos.setDescripcion(rs.getString("DESCRIPCION"));
        return datos;
    }
    
    /**
     * Metodo getDescripcion, retorna el valor del atributo descripcion,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
     /**
     * Metodo setDescripcion, setea el valor del atributo descripcion,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String Descripcion
     * @version : 1.0
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
     /**
     * Metodo getDestino, retorna el valor del atributo destino,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Metodo setDestino, setea el valor del atributo destino,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String Destino
     * @version : 1.0
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
     /**
     * Metodo getFecdescargue, retorna el valor del atributo fecdescargue,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getFecdescargue() {
        return fecdescargue;
    }
    
    /**
     * Metodo setFecdescargue, setea el valor del atributo fecdescargue,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String fecdescargue
     * @version : 1.0
     */
    public void setFecdescargue(java.lang.String fecdescargue) {
        this.fecdescargue = fecdescargue;
    }
    
      /**
     * Metodo getNumpla, retorna el valor del atributo numpla,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
   /**
     * Metodo setNumpla, setea el valor del atributo numpla,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numpla
     * @version : 1.0
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
     /**
     * Metodo getNumrem, retorna el valor del atributo numrem,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Metodo setNumrem, setea el valor del atributo numrem,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String numrem
     * @version : 1.0
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
     /**
     * Metodo getOrigen, retorna el valor del atributo origen,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Metodo setOrigen, setea el valor del atributo origen,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String origen
     * @version : 1.0
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
     /**
     * Metodo getPesoreal, retorna el valor del atributo pesoreal,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public double getPesoreal() {
        return pesoreal;
    }
    
    /**
     * Metodo setPesoreal, setea el valor del atributo pesoreal,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String pesoreal
     * @version : 1.0
     */
    public void setPesoreal(double pesoreal) {
        this.pesoreal = pesoreal;
    }
    
     /**
     * Metodo getPlaca, retorna el valor del atributo placa,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Metodo setPlaca, setea el valor del atributo placa,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String placa
     * @version : 1.0
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Metodo getRemision, retorna el valor del atributo remision,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Metodo setRemision, setea el valor del atributo remision,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String remision
     * @version : 1.0
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
    /**
     * Metodo getUnidad, retorna el valor del atributo unidad,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @version : 1.0
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Metodo setUnidad, setea el valor del atributo unidad,
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : String unidad
     * @version : 1.0
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    
}
