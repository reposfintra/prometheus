/*
 * FacturaEca.java
 * Created on 20 de mayo de 2009, 16:13
 */
package com.tsp.operation.model.beans;
/**
 * @author  Fintra
 */
public class FacturaEca {
    private String saldo,ms,doc,codCli,nit,fecha_factura,fecha_vencimiento,valor_factura,clasificacion1;

    private String cmc,ref1;//20100603

    /** Creates a new instance of FacturaEca */
    public FacturaEca() {    }

    public String getValorFactura() {
        return valor_factura;
    }
    public void setValorFactura(String x) {
        this.valor_factura= x;
    }

    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String x) {
        this.saldo= x;
    }
    public String getMs() {
        return ms;
    }
    public void setMs(String x) {
        this.ms= x;
    }
    public String getDoc() {
        return doc;
    }
    public void setDoc(String x) {
        this.doc= x;
    }

    public String getCodCli() {
        return codCli;
    }
    public void setCodCli(String x) {
        this.codCli= x;
    }

    public String getNit() {
        return nit;
    }
    public void setNit(String x) {
        this.nit= x;
    }

    public String getFechaFactura() {
        return fecha_factura;
    }
    public void setFechaFactura(String x) {
        this.fecha_factura= x;
    }

    public String getFechaVencimiento() {
        return fecha_vencimiento;
    }
    public void setFechaVencimiento(String x) {
        this.fecha_vencimiento= x;
    }

    public String getClasificacion1() {
        return clasificacion1;
    }

    public void setClasificacion1(String clasificacion1) {
        this.clasificacion1 = clasificacion1;
    }

    public String getRef1() {
        return ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }
    
    public static FacturaEca load(java.sql.ResultSet rs)throws java.sql.SQLException{
        FacturaEca facturaEca = new FacturaEca();
        facturaEca.setSaldo( rs.getString("valor_saldo") );
        facturaEca.setDoc( rs.getString("documento") );

        facturaEca.setNit( rs.getString("nit") );
        facturaEca.setCodCli( rs.getString("codcli") );
        facturaEca.setFechaFactura( rs.getString("fecha_factura") );

        facturaEca.setFechaVencimiento( rs.getString("fecha_vencimiento") );

        facturaEca.setValorFactura( rs.getString("valor_factura") );

        return facturaEca;
    }

    public String getCmc() {
        return cmc;
    }
    public void setCmc(String x) {
        this.cmc= x;
    }

    public static FacturaEca load3(java.sql.ResultSet rs)throws java.sql.SQLException{
        FacturaEca facturaEca = new FacturaEca();
        facturaEca.setSaldo( rs.getString("valor_saldo") );
        facturaEca.setDoc( rs.getString("documento") );

        facturaEca.setNit( rs.getString("nit") );
        facturaEca.setCodCli( rs.getString("codcli") );
        facturaEca.setFechaFactura( rs.getString("fecha_factura") );

        facturaEca.setFechaVencimiento( rs.getString("fecha_vencimiento") );

        facturaEca.setValorFactura( rs.getString("valor_factura") );

        facturaEca.setCmc(rs.getString("cmc"));
        facturaEca.setClasificacion1(rs.getString("clasificacion1"));
        
        facturaEca.setRef1(rs.getString("ref1"));
        return facturaEca;
    }

}