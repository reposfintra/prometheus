/*
 * TipoDocumento.java
 *
 * Created on 26 de marzo de 2005, 08:36 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Administrador
 */
public class Tipo_Documento implements Serializable {
    
    private String tdoc_id;
    private String descripcion;
    private String creado_por;
    private String user_update;
    
    /** Creates a new instance */
    public static Tipo_Documento load ( ResultSet rs ) throws SQLException {
        
        Tipo_Documento tdoc = new Tipo_Documento();
        
        tdoc.setId (rs.getString ("doc_id"));
        tdoc.setDescripcion (rs.getString("descripcion"));
        tdoc.setCreado_por (rs.getString("creation_user"));
        return tdoc;
    }
    
    //**Metodos de acceso a propiedades**
    
    //**set**
    
    public void setId ( String id ){
        this.tdoc_id = id;
    }
    
    public void setDescripcion (String desc ){
        this.descripcion = desc;
    }
    
    public void setCreado_por ( String creado_por ){
        this.creado_por = creado_por;
    }
    
    public void setUser_update ( String user ){
        this.user_update = user;
    }
    
    //**get**
    
    public String getId ( ){
        return tdoc_id;
    }
    
    public String getDescripcion( ){
        return descripcion;
    }
    
    public String getCreado_por ( ){
        return creado_por;
    }
    
    public String getUser_update ( ){
        return user_update;
    }
}

