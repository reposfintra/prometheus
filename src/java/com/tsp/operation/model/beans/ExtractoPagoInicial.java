/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.util.Date;

/**
 *
 * @author egonzalez
 */
public class ExtractoPagoInicial {
    
    private String cod_rop;
    private int id_unidad_negocio;
    private String periodo_rop;
    private String vencimiento_rop;
    private String negocio;
    private String cedula;
    private String nombre_cliente;
    private String direccion;
    private String ciudad;
    private String departamento;
    private String barrio;
    private String cuotas_vencidas;
    private String cuotas_pendientes;
    private String dias_vencidos;
    private String fecha_ultimo_pago;
    private double subtotal;
    private double total_sanciones;
    private double total_descuentos;
    private double total;
    private double total_abonos;
    private Date creation_date;
    private String creation_user;
    private Date last_update;
    private String user_update;
    private String observacion;
    private String msg_paguese_antes;
    private String msg_estado_credito;
    private String referencia_1;
    private String referencia_2;
    private String referencia_3;
    private String referencia_4;
    private String linea_negocio;
    private String fecha_generacion;
    private String establecimiento_comercio;
    

    public ExtractoPagoInicial() {
    }

    public ExtractoPagoInicial(String cod_rop, int id_unidad_negocio, String periodo_rop, String vencimiento_rop, String negocio, String cedula, String nombre_cliente, String direccion, String ciudad, String departamento, String barrio, String cuotas_vencidas, String cuotas_pendientes, String dias_vencidos, String fecha_ultimo_pago, double subtotal, double total_sanciones, double total_descuentos, double total, double total_abonos, Date creation_date, String creation_user, Date last_update, String user_update, String observacion, String msg_paguese_antes, String msg_estado_credito, String referencia_1, String referencia_2, String referencia_3, String referencia_4, String linea_negocio, String fecha_generacion, String establecimiento_comercio) {
        this.cod_rop = cod_rop;
        this.id_unidad_negocio = id_unidad_negocio;
        this.periodo_rop = periodo_rop;
        this.vencimiento_rop = vencimiento_rop;
        this.negocio = negocio;
        this.cedula = cedula;
        this.nombre_cliente = nombre_cliente;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.departamento = departamento;
        this.barrio = barrio;
        this.cuotas_vencidas = cuotas_vencidas;
        this.cuotas_pendientes = cuotas_pendientes;
        this.dias_vencidos = dias_vencidos;
        this.fecha_ultimo_pago = fecha_ultimo_pago;
        this.subtotal = subtotal;
        this.total_sanciones = total_sanciones;
        this.total_descuentos = total_descuentos;
        this.total = total;
        this.total_abonos = total_abonos;
        this.creation_date = creation_date;
        this.creation_user = creation_user;
        this.last_update = last_update;
        this.user_update = user_update;
        this.observacion = observacion;
        this.msg_paguese_antes = msg_paguese_antes;
        this.msg_estado_credito = msg_estado_credito;
        this.referencia_1 = referencia_1;
        this.referencia_2 = referencia_2;
        this.referencia_3 = referencia_3;
        this.referencia_4 = referencia_4;
        this.linea_negocio = linea_negocio;
        this.fecha_generacion = fecha_generacion;
        this.establecimiento_comercio = establecimiento_comercio;
    }


    /**
     * @return the cod_rop
     */
    public String getCod_rop() {
        return cod_rop;
    }

    /**
     * @param cod_rop the cod_rop to set
     */
    public void setCod_rop(String cod_rop) {
        this.cod_rop = cod_rop;
    }

    /**
     * @return the id_unidad_negocio
     */
    public int getId_unidad_negocio() {
        return id_unidad_negocio;
    }

    /**
     * @param id_unidad_negocio the id_unidad_negocio to set
     */
    public void setId_unidad_negocio(int id_unidad_negocio) {
        this.id_unidad_negocio = id_unidad_negocio;
    }

    /**
     * @return the periodo_rop
     */
    public String getPeriodo_rop() {
        return periodo_rop;
    }

    /**
     * @param periodo_rop the periodo_rop to set
     */
    public void setPeriodo_rop(String periodo_rop) {
        this.periodo_rop = periodo_rop;
    }

    /**
     * @return the vencimiento_rop
     */
    public String getVencimiento_rop() {
        return vencimiento_rop;
    }

    /**
     * @param vencimiento_rop the vencimiento_rop to set
     */
    public void setVencimiento_rop(String vencimiento_rop) {
        this.vencimiento_rop = vencimiento_rop;
    }

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre_cliente
     */
    public String getNombre_cliente() {
        return nombre_cliente;
    }

    /**
     * @param nombre_cliente the nombre_cliente to set
     */
    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the cuotas_vencidas
     */
    public String getCuotas_vencidas() {
        return cuotas_vencidas;
    }

    /**
     * @param cuotas_vencidas the cuotas_vencidas to set
     */
    public void setCuotas_vencidas(String cuotas_vencidas) {
        this.cuotas_vencidas = cuotas_vencidas;
    }

    /**
     * @return the cuotas_pendientes
     */
    public String getCuotas_pendientes() {
        return cuotas_pendientes;
    }

    /**
     * @param cuotas_pendientes the cuotas_pendientes to set
     */
    public void setCuotas_pendientes(String cuotas_pendientes) {
        this.cuotas_pendientes = cuotas_pendientes;
    }

    /**
     * @return the dias_vencidos
     */
    public String getDias_vencidos() {
        return dias_vencidos;
    }

    /**
     * @param dias_vencidos the dias_vencidos to set
     */
    public void setDias_vencidos(String dias_vencidos) {
        this.dias_vencidos = dias_vencidos;
    }

    /**
     * @return the fecha_ultimo_pago
     */
    public String getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    /**
     * @param fecha_ultimo_pago the fecha_ultimo_pago to set
     */
    public void setFecha_ultimo_pago(String fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    /**
     * @return the subtotal
     */
    public double getSubtotal() {
        return subtotal;
    }

    /**
     * @param subtotal the subtotal to set
     */
    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * @return the total_sanciones
     */
    public double getTotal_sanciones() {
        return total_sanciones;
    }

    /**
     * @param total_sanciones the total_sanciones to set
     */
    public void setTotal_sanciones(double total_sanciones) {
        this.total_sanciones = total_sanciones;
    }

    /**
     * @return the total_descuentos
     */
    public double getTotal_descuentos() {
        return total_descuentos;
    }

    /**
     * @param total_descuentos the total_descuentos to set
     */
    public void setTotal_descuentos(double total_descuentos) {
        this.total_descuentos = total_descuentos;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return the total_abonos
     */
    public double getTotal_abonos() {
        return total_abonos;
    }

    /**
     * @param total_abonos the total_abonos to set
     */
    public void setTotal_abonos(double total_abonos) {
        this.total_abonos = total_abonos;
    }

    /**
     * @return the creation_date
     */
    public Date getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the last_update
     */
    public Date getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the msg_paguese_antes
     */
    public String getMsg_paguese_antes() {
        return msg_paguese_antes;
    }

    /**
     * @param msg_paguese_antes the msg_paguese_antes to set
     */
    public void setMsg_paguese_antes(String msg_paguese_antes) {
        this.msg_paguese_antes = msg_paguese_antes;
    }

    /**
     * @return the msg_estado_credito
     */
    public String getMsg_estado_credito() {
        return msg_estado_credito;
    }

    /**
     * @param msg_estado_credito the msg_estado_credito to set
     */
    public void setMsg_estado_credito(String msg_estado_credito) {
        this.msg_estado_credito = msg_estado_credito;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    /**
     * @return the referencia_2
     */
    public String getReferencia_2() {
        return referencia_2;
    }

    /**
     * @param referencia_2 the referencia_2 to set
     */
    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    /**
     * @return the referencia_3
     */
    public String getReferencia_3() {
        return referencia_3;
    }

    /**
     * @param referencia_3 the referencia_3 to set
     */
    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }

    /**
     * @return the referencia_4
     */
    public String getReferencia_4() {
        return referencia_4;
    }

    /**
     * @param referencia_4 the referencia_4 to set
     */
    public void setReferencia_4(String referencia_4) {
        this.referencia_4 = referencia_4;
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * @param barrio the barrio to set
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * @return the linea_negocio
     */
    public String getLinea_negocio() {
        return linea_negocio;
    }

    /**
     * @param linea_negocio the linea_negocio to set
     */
    public void setLinea_negocio(String linea_negocio) {
        this.linea_negocio = linea_negocio;
    }

    /**
     * @return the fecha_generacion
     */
    public String getFecha_generacion() {
        return fecha_generacion;
    }

    /**
     * @param fecha_generacion the fecha_generacion to set
     */
    public void setFecha_generacion(String fecha_generacion) {
        this.fecha_generacion = fecha_generacion;
    }

    /**
     * @return the establecimiento_comercio
     */
    public String getEstablecimiento_comercio() {
        return establecimiento_comercio;
    }

    /**
     * @param establecimiento_comercio the establecimiento_comercio to set
     */
    public void setEstablecimiento_comercio(String establecimiento_comercio) {
        this.establecimiento_comercio = establecimiento_comercio;
    }
    
    
   
    
}
