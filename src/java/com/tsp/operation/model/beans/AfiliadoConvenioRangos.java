/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Iris Vargas
 */
public class AfiliadoConvenioRangos {
    String idProvConvenio;
    String cuotaIni;
    String cuotaFin;
    String porcentajeComision;

    public void load(ResultSet rs) throws SQLException {
      
        this.idProvConvenio = rs.getString("id_prov_convenio");
        this.cuotaIni = rs.getString("cuota_ini");
        this.cuotaFin = rs.getString("cuota_fin");
        this.porcentajeComision = rs.getString("porcentaje_comision");
    }

    public String getCuotaFin() {
        return cuotaFin;
    }

    public void setCuotaFin(String cuotaFin) {
        this.cuotaFin = cuotaFin;
    }

    public String getCuotaIni() {
        return cuotaIni;
    }

    public void setCuotaIni(String cuotaIni) {
        this.cuotaIni = cuotaIni;
    }

    public String getIdProvConvenio() {
        return idProvConvenio;
    }

    public void setIdProvConvenio(String idProvConvenio) {
        this.idProvConvenio = idProvConvenio;
    }

    public String getPorcentajeComision() {
        return porcentajeComision;
    }

    public void setPorcentajeComision(String porcentajeComision) {
        this.porcentajeComision = porcentajeComision;
    }

}
