/**************************************************************************
 * Nombre:                        Jerarquia_tiempo.java                   *
 * Descripci�n:                   Beans de acuerdo especial.              *
 * Autor:                         Ing. Jose de la Rosa                    *
 * Fecha:                         24 de junio de 2005, 10:47 AM           *
 * Versi�n: Java                  1.0                                     *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/


package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;


public class Jerarquia_tiempo implements Serializable{
    private String actividad;
    private String responsable;
    private String demora;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    private String base;
    
    //Diogenes 16.12.05
    private String desResponsable;
    private String desCausa;
    private String desactividad;
    
    /** Creates a new instance of Jerarquia_tiempo */
    public static Jerarquia_tiempo load(ResultSet rs)throws SQLException{
        Jerarquia_tiempo j = new Jerarquia_tiempo();
        j.setActividad(rs.getString("actividad"));
        j.setResponsable(rs.getString("responsable"));
        j.setDemora(rs.getString("demora"));
        j.setCia(rs.getString("cia"));
        j.setRec_status(rs.getString("rec_status"));
        j.setLast_update(rs.getDate("last_update"));
        j.setUser_update(rs.getString("user_update"));
        j.setCreation_date(rs.getDate("creation_date"));
        j.setCreation_user(rs.getString("creation_user"));
        return j;
    }
    
     /**
     * Getter for property getActividad.
     * @return Value of property getActividad.
     */
    public java.lang.String getActividad() {
        return actividad;
    }
     /**
     * Setter for property setActividad.
     * @param desResponsable New value of property setActividad.
     */
    public void setActividad(java.lang.String actividad) {
        this.actividad = actividad;
    }
    /**
     * Getter for property getCia.
     * @return Value of property getCia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    /**
     * Setter for property setCia.
     * @param desResponsable New value of property setCia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    /**
     * Getter for  getCreation_date.
     * @return Value of property getCia.
     */
    public java.util.Date getCreation_date() {
        return creation_date;
    }
    /**
     * Setter for property setCreation_date.
     * @param desResponsable New value of property setCreation_date.
     */
    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }
    /**
     * Getter for  getCreation_user.
     * @return Value of property getCreation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    /**
     * Setter for property setCreation_user.
     * @param desResponsable New value of property setCreation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    /**
     * Getter for  getDemora.
     * @return Value of property getDemora.
     */
    public java.lang.String getDemora() {
        return demora;
    }
    /**
     * Setter for property setDemora.
     * @param desResponsable New value of property setDemora.
     */
    public void setDemora(java.lang.String demora) {
        this.demora = demora;
    }
    /**
     * Getter for  getLast_update.
     * @return Value of property getLast_update.
     */
    public java.util.Date getLast_update() {
        return last_update;
    }
    /**
     * Setter for property setLast_update.
     * @param desResponsable New value of property setLast_update.
     */
    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }
    /**
     * Getter for  getRec_status.
     * @return Value of property getRec_status.
     */
    public java.lang.String getRec_status() {
        return rec_status;
    }
    /**
     * Setter for property setRec_status.
     * @param desResponsable New value of property setRec_status.
     */
    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    /**
     * Getter for getResponsable.
     * @return Value of property getResponsable.
     */
    public java.lang.String getResponsable() {
        return responsable;
    }
    /**
     * Setter for property setResponsable.
     * @param desResponsable New value of property setResponsable.
     */
    public void setResponsable(java.lang.String responsable) {
        this.responsable = responsable;
    }
     /**
     * Getter for getUser_update.
     * @return Value of property getUser_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
   /**
     * Setter for property setUser_update.
     * @param desResponsable New value of property setUser_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property desResponsable.
     * @return Value of property desResponsable.
     */
    public java.lang.String getDesResponsable() {
        return desResponsable;
    }
    
    /**
     * Setter for property desResponsable.
     * @param desResponsable New value of property desResponsable.
     */
    public void setDesResponsable(java.lang.String desResponsable) {
        this.desResponsable = desResponsable;
    }
    
    /**
     * Getter for property desCauda.
     * @return Value of property desCauda.
     */
    public java.lang.String getDesCausa() {
        return desCausa;
    }
    
    /**
     * Setter for property desCauda.
     * @param desCauda New value of property desCauda.
     */
    public void setDesCausa(java.lang.String desCausa) {
        this.desCausa = desCausa;
    }
    
    /**
     * Getter for property desactividad.
     * @return Value of property desactividad.
     */
    public java.lang.String getDesactividad() {
        return desactividad;
    }
    
    /**
     * Setter for property desactividad.
     * @param desactividad New value of property desactividad.
     */
    public void setDesactividad(java.lang.String desactividad) {
        this.desactividad = desactividad;
    }
    
}
