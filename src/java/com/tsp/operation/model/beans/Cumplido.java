/*
 * Cumplido_Planilla.java
 *
 * Created on 19 de septiembre de 2005, 05:14 PM
 */
package com.tsp.operation.model.beans;
import java.util.*;
import java.sql.*;


/**
 *
 * @author  R.SALAZAR
 */
public class Cumplido{
    public String planilla;
    public String tipo_doc;
    public String cod_doc;
    public double cantidad;
    public String unidad;
    public String coment;
    public java.util.Date fecha_mod;
    public java.util.Date fecha_crea;
    public String fecha_migracion;
    public String usuario_migracion;
    public String base;
    public String district;
    public String crea_user;
    public String mod_user;
    public String estado;
    public String agencia;
    
    //jdelarosa 13-01-2006
    public String remesa;
    public String soporte;
    public String usuario_modificacion;
    public String usuario_creacion;
    public String entregado_conductor;
    
    // mfontalvo 2006-01-17
    public String fecha_envio;
    public String fecha_recibido;
    public String fecha_envio_logico;
    public String fecha_recibido_logico;
    public String agencia_envio;
    
    public String ur_envio;
    public String ur_envio_logico;
    public String ur_recibido;
    public String ur_recibido_logico;
    
    /** Creates a new instance of Cumplido_Planilla */
    public Cumplido() {
    }
    public static Cumplido load(ResultSet rs) throws SQLException {
        Cumplido cumplido = new Cumplido();
        
        cumplido.setTipo_doc( rs.getString("tipo_doc") );
        cumplido.setCod_doc( rs.getString("cod_doc") );
        cumplido.setCantidad( rs.getDouble("cantidad") );
        cumplido.setUnidad( rs.getString("unidad") );
        cumplido.setComent( rs.getString("coment"));
        //
        cumplido.setFecha_mod( rs.getDate("last_update") );
        cumplido.setFecha_crea( rs.getDate("creation_date") );
        cumplido.setBase( rs.getString("base") );
        cumplido.setDistrict( rs.getString("dstrct") );
        cumplido.setCrea_user(rs.getString("creation_user") );
        cumplido.setMod_user(rs.getString("user_update"));
        cumplido.setEstado(rs.getString("reg_status"));
        cumplido.setAgencia(rs.getString("id_agencia"));
        ////System.out.println("BEAN CUMP="+cumplido.getCantidad());
        return cumplido;
        
         
    }
    
    
     public void setTipo_doc(String var){
         tipo_doc = var;
     }
     
     public void setCod_doc(String var){
         cod_doc = var;
     }
     
     public void setCantidad(double var){
         cantidad = var;
     }
     
     public void setUnidad(String var){
         unidad = var;
     }
     
     public void setComent(String var){
         coment = var;
     }
     
     public void setFecha_mod(java.util.Date var){
         fecha_mod = var;
     }
     
     public void setFecha_crea(java.util.Date var){
         fecha_crea = var;
     }
     
     public void setBase(String var){
         base = var;
     }
     
     public void setDistrict(String var){
         district = var;
     }
     
     public void setCrea_user(String var){
         crea_user = var;
     }
     
     public void setMod_user(String var){
         mod_user = var;
     }
     
     public void setEstado(String var){
         estado = var;
     }
     
     public void setAgencia(String var){
         agencia = var;
     }
     
     
     
     //Consultas
     
     
     
     public String getTipo_doc( ){
         return tipo_doc ;
     }
     
     public String getCod_doc( ){
         return cod_doc ;
     }
     
     public double getCantidad( ){
         return cantidad;
     }
     
     public String getUnidad( ){
         return unidad;
     }
     
     public String getComent(){
         return coment;
     }
     
     public java.util.Date getFecha_mod(){
         return fecha_mod;
     }
     
     public java.util.Date getFecha_crea( ){
         return fecha_crea;
     }
     
     public String getBase( ){
         return base;
     }
     
     public String getDistrict( ){
         return district;
     }
     
     public String getCrea_user( ){
         return crea_user;
     }
     
     public String getMod_user( ){
         return mod_user ;
     }
     
     public String getEstado( ){
         return estado ;
     }
     
     public String getAgencia( ){
         return agencia ;
     }

     public java.lang.String getPlanilla() {
         return planilla;
     }     

     public void setPlanilla(java.lang.String planilla) {
         this.planilla = planilla;
     }

     public java.lang.String getFecha_migracion() {
         return fecha_migracion;
     }

     public void setFecha_migracion(java.lang.String fecha_migracion) {
         this.fecha_migracion = fecha_migracion;
     }

     public java.lang.String getUsuario_migracion() {
         return usuario_migracion;
     }

     public void setUsuario_migracion(java.lang.String usuario_migracion) {
         this.usuario_migracion = usuario_migracion;
     }
     
     /**
      * Getter for property remesa.
      * @return Value of property remesa.
      */
     public java.lang.String getRemesa () {
         return remesa;
     }
     
     /**
      * Setter for property remesa.
      * @param remesa New value of property remesa.
      */
     public void setRemesa (java.lang.String remesa) {
         this.remesa = remesa;
     }
     
     /**
      * Getter for property soporte.
      * @return Value of property soporte.
      */
     public java.lang.String getSoporte () {
         return soporte;
     }
     
     /**
      * Setter for property soporte.
      * @param soporte New value of property soporte.
      */
     public void setSoporte (java.lang.String soporte) {
         this.soporte = soporte;
     }
     
     /**
      * Getter for property usuario_modificacion.
      * @return Value of property usuario_modificacion.
      */
     public java.lang.String getUsuario_modificacion () {
         return usuario_modificacion;
     }
     
     /**
      * Setter for property usuario_modificacion.
      * @param usuario_modificacion New value of property usuario_modificacion.
      */
     public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
         this.usuario_modificacion = usuario_modificacion;
     }
     
     /**
      * Getter for property usuario_creacion.
      * @return Value of property usuario_creacion.
      */
     public java.lang.String getUsuario_creacion () {
         return usuario_creacion;
     }
     
     /**
      * Setter for property usuario_creacion.
      * @param usuario_creacion New value of property usuario_creacion.
      */
     public void setUsuario_creacion (java.lang.String usuario_creacion) {
         this.usuario_creacion = usuario_creacion;
     }
     
     /**
      * Getter for property entregado_conductor.
      * @return Value of property entregado_conductor.
      */
     public java.lang.String getEntregado_conductor () {
         return entregado_conductor;
     }
     
     /**
      * Setter for property entregado_conductor.
      * @param entregado_conductor New value of property entregado_conductor.
      */
     public void setEntregado_conductor (java.lang.String entregado_conductor) {
         this.entregado_conductor = entregado_conductor;
     }
     
     /**
      * Metodo que obtiene el valor de la propiedad fecha_envio.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07      
      * @return valor de la propiedad fecha_envio.
      */
     public java.lang.String getFecha_envio() {
         return fecha_envio;
     }
     
     /**
      * Metodo que cambia el valor de la propiedad fecha_envio.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07      
      * @param fecha_envio nuevo valor de la propiedad fecha_envio.
      */
     public void setFecha_envio(java.lang.String fecha_envio) {
         this.fecha_envio = fecha_envio;
     }
     
     /**
      * Metodo que obtiene el valor de la propiedad fecha_recibido.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07      
      * @return valor de la propiedad fecha_recibido.
      */
     public java.lang.String getFecha_recibido() {
         return fecha_recibido;
     }
     
     /**
      * Metodo que cambia el valor de la propiedad fecha_recibido.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07      
      * @param fecha_recibido nuevo valor de la propiedad fecha_recibido.
      */
     public void setFecha_recibido(java.lang.String fecha_recibido) {
         this.fecha_recibido = fecha_recibido;
     }
     
     /**
      * Metodo que obtiene el valor de la propiedad agencia_envio.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07      
      * @return valor de la propiedad agencia_envio.
      */
     public java.lang.String getAgencia_envio() {
         return agencia_envio;
     }
     
     /**
      * Metodo que cambia el valor de la propiedad agencia_envio.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07      
      * @param agencia_envio nuevo valor de la propiedad agencia_envio.
      */
     public void setAgencia_envio(java.lang.String agencia_envio) {
         this.agencia_envio = agencia_envio;
     }
     
     /**
      * Metodo que obtiene el valor de la propiedad fecha_envio_logico.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07
      * @return valor de la propiedad fecha_envio_logico.
      */
     public java.lang.String getFecha_envio_logico() {
         return fecha_envio_logico;
     }
     
     /**
      * Metodo que cambia el valor de la propiedad fecha_envio_logico.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07
      * @param fecha_envio_logico nuevo valor de la propiedad fecha_envio_logico.
      */
     public void setFecha_envio_logico(java.lang.String fecha_envio_logico) {
         this.fecha_envio_logico = fecha_envio_logico;
     }
     
     /**
      * Metodo que obtiene el valor de la propiedad fecha_recibido_logico.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07
      * @return valor de la propiedad fecha_recibido_logico.
      */
     public java.lang.String getFecha_recibido_logico() {
         return fecha_recibido_logico;
     }
     
     /**
      * Metodo que cambia el valor de la propiedad fecha_recibido_logico.
      * @autor mfontalvo
      * @fecha fecha 2006-01-07
      * @param fecha_recibido_logico nuevo valor de la propiedad fecha_recibido_logico.
      */
     public void setFecha_recibido_logico(java.lang.String fecha_recibido_logico) {
         this.fecha_recibido_logico = fecha_recibido_logico;
     }
     
     /**
      * Getter for property ur_envio.
      * @return Value of property ur_envio.
      */
     public java.lang.String getUr_envio() {
         return ur_envio;
     }
     
     /**
      * Setter for property ur_envio.
      * @param ur_envio New value of property ur_envio.
      */
     public void setUr_envio(java.lang.String ur_envio) {
         this.ur_envio = ur_envio;
     }
     
     /**
      * Getter for property ur_envio_logico.
      * @return Value of property ur_envio_logico.
      */
     public java.lang.String getUr_envio_logico() {
         return ur_envio_logico;
     }
     
     /**
      * Setter for property ur_envio_logico.
      * @param ur_envio_logico New value of property ur_envio_logico.
      */
     public void setUr_envio_logico(java.lang.String ur_envio_logico) {
         this.ur_envio_logico = ur_envio_logico;
     }
     
     /**
      * Getter for property ur_recibido.
      * @return Value of property ur_recibido.
      */
     public java.lang.String getUr_recibido() {
         return ur_recibido;
     }
     
     /**
      * Setter for property ur_recibido.
      * @param ur_recibido New value of property ur_recibido.
      */
     public void setUr_recibido(java.lang.String ur_recibido) {
         this.ur_recibido = ur_recibido;
     }
     
     /**
      * Getter for property ur_recibido_logico.
      * @return Value of property ur_recibido_logico.
      */
     public java.lang.String getUr_recibido_logico() {
         return ur_recibido_logico;
     }
     
     /**
      * Setter for property ur_recibido_logico.
      * @param ur_recibido_logico New value of property ur_recibido_logico.
      */
     public void setUr_recibido_logico(java.lang.String ur_recibido_logico) {
         this.ur_recibido_logico = ur_recibido_logico;
     }
     
}
