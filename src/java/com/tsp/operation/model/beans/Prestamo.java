
/***************************************
* Nombre Clase ............. Prestamo.java
* Descripción  .. . . . . .  Permite Encapsular los datos de un Prestamo
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  09/02/2006
* versión . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/


package com.tsp.operation.model.beans;



import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;


public class Prestamo {
    
    
    private String   distrito;    
    private String   tercero;
    private String   terceroName;
    private double   monto;
    private double   intereses;
    private int      cuotas;
    private int      frecuencias;
    private double   tasa;
    private String   fechaInicialCobro;
    private String   fechaEntregaDinero;
    private String   tipoPrestamo;
    private String   tipoPrestamoName;
    private double   interesDemora;
    private int      id;
    private String   aprobado;    
    private List     amortizacion;
    private String   beneficiario;
    private String   beneficiarioName;
    
    private double   valorPagado;
    private double   valorDescontado;
    private double   valorMigrado;
    private double   valorRegistrado;
    private double   capitalDescontado;
    private double   interesDescontado;
    
    
    private String   concepto;
    private String   observacion;
    private String   clasificación;
    private String   clasificacionName;
    

    private String   placa;
    private String   equipo;
    private String   descripcion_equipo;
    private double   cuotaInicial = 0;
    private double   cuotaMonitoreo = 0;
    private double   cuotaFinanciacion = 0;
    private String   fecha_migracion;
    private String   factura_inicial;
    private String   factura_financiacion;
    private String   factura_monitoreo;
    private String   factura_monitoreo_inicial;
    private String   nit_tercero;
    private String   banco_tercero;
    private String   sucursal_tercero;
    private String   banco_beneficiario;
    private String   sucursal_beneficiario;
    
    
    private double saldo_prestamo = 0;
    private int    inicio_financiacion = 1;
    private int    inicio_monitoreo = 1;
    private String creation_date;
    private String creation_user;
    
    public Prestamo() {
        String vacio       = "";
        String   hoy       = Util.getFechaActual_String(6).replaceAll("/","-");
        
        amortizacion       = new LinkedList();        
        distrito           = vacio;    
        tercero            = vacio;
        fechaInicialCobro  = hoy;
        fechaEntregaDinero = hoy;
        tipoPrestamo       = vacio;
        aprobado           = vacio;
        beneficiario       = vacio;
        beneficiarioName   = vacio;
        concepto           = vacio;
        observacion        = vacio;
        clasificación      = vacio;
        clasificacionName  = vacio;
        equipo             = vacio;
        descripcion_equipo = vacio;
        placa              = vacio;
        
        fecha_migracion      = vacio;
        factura_inicial      = vacio;
        factura_financiacion = vacio;
        factura_monitoreo    = vacio;
        factura_monitoreo_inicial = vacio;
        nit_tercero          = vacio;
        
        banco_tercero        = vacio;
        sucursal_tercero     = vacio;
        banco_beneficiario   = vacio;
        sucursal_beneficiario= vacio;
        
    }
    
    
    
    
    
    // GET :
    
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }    
    
    
     /**
     * Getter for property amortizacion.
     * @return Value of property amortizacion.
     */
    public java.util.List getAmortizacion() {
        return amortizacion;
    } 
    
    
     /**
     * Getter for property aprobado.
     * @return Value of property aprobado.
     */
    public java.lang.String getAprobado() {
        return aprobado;
    }  
    
    
    /**
     * Getter for property cuotas.
     * @return Value of property cuotas.
     */
    public int getCuotas() {
        return cuotas;
    } 
    
    
     /**
     * Getter for property fechaEntregaDinero.
     * @return Value of property fechaEntregaDinero.
     */
    public java.lang.String getFechaEntregaDinero() {
        return fechaEntregaDinero;
    }    
    
    
    /**
     * Getter for property fechaInicialCobro.
     * @return Value of property fechaInicialCobro.
     */
    public java.lang.String getFechaInicialCobro() {
        return fechaInicialCobro;
    }
    
    
     /**
     * Getter for property frecuencias.
     * @return Value of property frecuencias.
     */
    public int getFrecuencias() {
        return frecuencias;
    }
    
    
     /**
     * Getter for property id.
     * @return Value of property id.
     */
    public int getId() {
        return id;
    }
    
    
     /**
     * Getter for property interesMora.
     * @return Value of property interesMora.
     */
    public double getInteresDemora() {
        return interesDemora;
    }
    
    
    /**
     * Getter for property monto.
     * @return Value of property monto.
     */
    public double getMonto() {
        return monto;
    }
    
    
     /**
     * Getter for property tipoPrestamo.
     * @return Value of property tipoPrestamo.
     */
    public java.lang.String getTipoPrestamo() {
        return tipoPrestamo;
    }
    
    
    
    
    
   // SET : 
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    } 
    
    
    /**
     * Setter for property amortizacion.
     * @param amortizacion New value of property amortizacion.
     */
    public void setAmortizacion(java.util.List amortizacion) {
        this.amortizacion = amortizacion;
    }    
    
    
    /**
     * Setter for property aprobado.
     * @param aprobado New value of property aprobado.
     */
    public void setAprobado(java.lang.String aprobado) {
        this.aprobado = aprobado;
    } 
    
    
    /**
     * Setter for property cuotas.
     * @param cuotas New value of property cuotas.
     */
    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }  
   
    
    /**
     * Setter for property fechaEntregaDinero.
     * @param fechaEntregaDinero New value of property fechaEntregaDinero.
     */
    public void setFechaEntregaDinero(java.lang.String fechaEntregaDinero) {
        this.fechaEntregaDinero = fechaEntregaDinero;
    }    
    
    
    /**
     * Setter for property fechaInicialCobro.
     * @param fechaInicialCobro New value of property fechaInicialCobro.
     */
    public void setFechaInicialCobro(java.lang.String fechaInicialCobro) {
        this.fechaInicialCobro = fechaInicialCobro;
    }
    
   
    
    /**
     * Setter for property frecuencias.
     * @param frecuencias New value of property frecuencias.
     */
    public void setFrecuencias(int frecuencias) {
        this.frecuencias = frecuencias;
    }
       
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
   
    /**
     * Setter for property interesMora.
     * @param interesMora New value of property interesMora.
     */
    public void setInteresDemora(double val) {
        this.interesDemora = val;
    }
        
    
    /**
     * Setter for property monto.
     * @param monto New value of property monto.
     */
    public void setMonto(double monto) {
        this.monto = monto;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }
    
    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }
    
       
    /**
     * Setter for property tipoPrestamo.
     * @param tipoPrestamo New value of property tipoPrestamo.
     */
    public void setTipoPrestamo(java.lang.String tipoPrestamo) {
        this.tipoPrestamo = tipoPrestamo;
    }
    
    
    
    
    
    
    
    public static Prestamo  loadRequest( HttpServletRequest request ){
        Prestamo prestamo = new Prestamo();        
        
           double monto   = resetDouble(  request.getParameter("monto")   );
           double tasa    = resetDouble(  request.getParameter("tasa")    );
           double demora  = resetDouble(  request.getParameter("demora")  );           
           int    cuotas  = resetInt   (  request.getParameter("cuotas")  );
           int    periodo = resetInt   (  request.getParameter("periodo") );
           int    inicio_financiacion = resetInt   (  request.getParameter("cobrar_financiacion") );
           int    inicio_monitoreo    = resetInt   (  request.getParameter("cobrar_monitoreo")    );
           
           
           
           
           double cuota_inicial    =  resetDouble(  request.getParameter("cuota_inicial")  ) ;   
           double cuota_monitoreo  =  resetDouble(  request.getParameter("cuota_monitoreo")) ;
           
           
           prestamo.setDistrito           (  reset    (  request.getParameter("distrito")           ) );
           prestamo.setBeneficiario       (  reset    (  request.getParameter("Beneficiario")       ) );
           prestamo.setBeneficiarioName   (  reset    (  request.getParameter("NombreBeneficiario") ) );           
           prestamo.setTercero            (  reset    (  request.getParameter("tercero")            ) );           
           prestamo.setMonto              (  monto    );
           prestamo.setCuotas             (  cuotas   );
           prestamo.setFrecuencias        (  periodo  );
           prestamo.setTipoPrestamo       (  reset    (  request.getParameter("tipoPrestamo")     ) ); 
           prestamo.setTipoPrestamoName   (  reset    (  request.getParameter("tipoPrestamoName") ) );
           prestamo.setTasa               (  tasa     );
           prestamo.setInteresDemora      (  demora   );
           prestamo.setFechaEntregaDinero (  reset    (  request.getParameter("entrega")          ) );
           prestamo.setFechaInicialCobro  (  reset    (  request.getParameter("primerPago")       ) );
           prestamo.setAprobado           (  reset    (  request.getParameter("aprobado")         ) );
           prestamo.setConcepto           (  reset    (  request.getParameter("concepto")         ) );
           prestamo.setObservacion        (  reset    (  request.getParameter("observacion")      ) );
           prestamo.setClasificación      (  reset    (  request.getParameter("clasificacion")    ) );
           prestamo.setPlaca              (  reset    (  request.getParameter("placa")            ) );
           
           if ( prestamo.getTipoPrestamo().equals("EQ") ){
               prestamo.setEquipo             (  reset    (  request.getParameter("tipoEquipo")       ) );
               prestamo.setCuotaInicial       (  cuota_inicial );
               prestamo.setCuotaMonitoreo     (  cuota_monitoreo );
               prestamo.setInicio_financiacion(  inicio_financiacion );
               prestamo.setInicio_monitoreo   (  inicio_monitoreo );
           } else if (prestamo.getTipoPrestamo().equals("AP") ){
               prestamo.setCuotas( 0 );
           }
           
           
        return prestamo;
    }
    
    
    
    
    public static String reset(String val){
        if(val==null)
            val="";
        return val;
    }
    
    
    
    public static double resetDouble(String val){
        double valor = 0;
        if(val!=null && !val.equals("") )
               valor = Double.parseDouble(  val );
        return valor;
    }
    
    
    
    public static int resetInt(String val){
        int valor = 0;
        if(val!=null && !val.equals(""))
               valor = Integer.parseInt( val );
        return valor;
    }
     
     
     
    
    
    
    
    /**
     * Getter for property beneficiario.
     * @return Value of property beneficiario.
     */
    public java.lang.String getBeneficiario() {
        return beneficiario;
    }    
    
    /**
     * Setter for property beneficiario.
     * @param beneficiario New value of property beneficiario.
     */
    public void setBeneficiario(java.lang.String beneficiario) {
        this.beneficiario = beneficiario;
    }    
    
    
    
    
    
    
    
    /**
     * Getter for property beneficiarioName.
     * @return Value of property beneficiarioName.
     */
    public java.lang.String getBeneficiarioName() {
        return beneficiarioName;
    }
    
    /**
     * Setter for property beneficiarioName.
     * @param beneficiarioName New value of property beneficiarioName.
     */
    public void setBeneficiarioName(java.lang.String beneficiarioName) {
        this.beneficiarioName = beneficiarioName;
    }
    
    /**
     * Getter for property terceroName.
     * @return Value of property terceroName.
     */
    public java.lang.String getTerceroName() {
        return terceroName;
    }
    
    /**
     * Setter for property terceroName.
     * @param terceroName New value of property terceroName.
     */
    public void setTerceroName(java.lang.String terceroName) {
        this.terceroName = terceroName;
    }
    
    /**
     * Getter for property tipoPrestamoName.
     * @return Value of property tipoPrestamoName.
     */
    public java.lang.String getTipoPrestamoName() {
        return tipoPrestamoName;
    }
    
    /**
     * Setter for property tipoPrestamoName.
     * @param tipoPrestamoName New value of property tipoPrestamoName.
     */
    public void setTipoPrestamoName(java.lang.String tipoPrestamoName) {
        this.tipoPrestamoName = tipoPrestamoName;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property intereses.
     * @return Value of property intereses.
     */
    public double getIntereses() {
        return intereses;
    }
    
    /**
     * Setter for property intereses.
     * @param intereses New value of property intereses.
     */
    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }
    
    
    
    
    
    /**
     * Getter for property clasificación.
     * @return Value of property clasificación.
     */
    public java.lang.String getClasificación() {
        return clasificación;
    }
    
    /**
     * Setter for property clasificación.
     * @param clasificación New value of property clasificación.
     */
    public void setClasificación(java.lang.String clasificación) {
        this.clasificación = clasificación;
    }
    
    /**
     * Getter for property clasificacionName.
     * @return Value of property clasificacionName.
     */
    public java.lang.String getClasificacionName() {
        return clasificacionName;
    }
    
    /**
     * Setter for property clasificacionName.
     * @param clasificacionName New value of property clasificacionName.
     */
    public void setClasificacionName(java.lang.String clasificacionName) {
        this.clasificacionName = clasificacionName;
    }
    
    /**
     * Getter for property valorPagado.
     * @return Value of property valorPagado.
     */
    public double getValorPagado() {
        return valorPagado;
    }
    
    /**
     * Setter for property valorPagado.
     * @param valorPagado New value of property valorPagado.
     */
    public void setValorPagado(double valorPagado) {
        this.valorPagado = valorPagado;
    }
    
    /**
     * Getter for property valorDescontado.
     * @return Value of property valorDescontado.
     */
    public double getValorDescontado() {
        return valorDescontado;
    }
    
    /**
     * Setter for property valorDescontado.
     * @param valorDescontado New value of property valorDescontado.
     */
    public void setValorDescontado(double valorDescontado) {
        this.valorDescontado = valorDescontado;
    }
    
    /**
     * Getter for property valorMigrado.
     * @return Value of property valorMigrado.
     */
    public double getValorMigrado() {
        return valorMigrado;
    }
    
    /**
     * Setter for property valorMigrado.
     * @param valorMigrado New value of property valorMigrado.
     */
    public void setValorMigrado(double valorMigrado) {
        this.valorMigrado = valorMigrado;
    }
    
    /**
     * Getter for property valorRegistrado.
     * @return Value of property valorRegistrado.
     */
    public double getValorRegistrado() {
        return valorRegistrado;
    }
    
    /**
     * Setter for property valorRegistrado.
     * @param valorRegistrado New value of property valorRegistrado.
     */
    public void setValorRegistrado(double valorRegistrado) {
        this.valorRegistrado = valorRegistrado;
    }
    
    /**
     * Getter for property capitalDescontado.
     * @return Value of property capitalDescontado.
     */
    public double getCapitalDescontado() {
        return capitalDescontado;
    }
    
    /**
     * Setter for property capitalDescontado.
     * @param capitalDescontado New value of property capitalDescontado.
     */
    public void setCapitalDescontado(double capitalDescontado) {
        this.capitalDescontado = capitalDescontado;
    }
    
    /**
     * Getter for property interesDescontado.
     * @return Value of property interesDescontado.
     */
    public double getInteresDescontado() {
        return interesDescontado;
    }
    
    /**
     * Setter for property interesDescontado.
     * @param interesDescontado New value of property interesDescontado.
     */
    public void setInteresDescontado(double interesDescontado) {
        this.interesDescontado = interesDescontado;
    }
    
    /**
     * Getter for property equipo.
     * @return Value of property equipo.
     */
    public java.lang.String getEquipo() {
        return equipo;
    }
    
    /**
     * Setter for property equipo.
     * @param equipo New value of property equipo.
     */
    public void setEquipo(java.lang.String equipo) {
        this.equipo = equipo;
    }
    
    /**
     * Getter for property descripcion_equipo.
     * @return Value of property descripcion_equipo.
     */
    public java.lang.String getDescripcion_equipo() {
        return descripcion_equipo;
    }
    
    /**
     * Setter for property descripcion_equipo.
     * @param descripcion_equipo New value of property descripcion_equipo.
     */
    public void setDescripcion_equipo(java.lang.String descripcion_equipo) {
        this.descripcion_equipo = descripcion_equipo;
    }
    
    /**
     * Getter for property cuotaInicial.
     * @return Value of property cuotaInicial.
     */
    public double getCuotaInicial() {
        return cuotaInicial;
    }
    
    /**
     * Setter for property cuotaInicial.
     * @param cuotaInicial New value of property cuotaInicial.
     */
    public void setCuotaInicial(double cuotaInicial) {
        this.cuotaInicial = cuotaInicial;
    }
    
    /**
     * Getter for property cuotaMonitoreo.
     * @return Value of property cuotaMonitoreo.
     */
    public double getCuotaMonitoreo() {
        return cuotaMonitoreo;
    }
    
    /**
     * Setter for property cuotaMonitoreo.
     * @param cuotaMonitoreo New value of property cuotaMonitoreo.
     */
    public void setCuotaMonitoreo(double cuotaMonitoreo) {
        this.cuotaMonitoreo = cuotaMonitoreo;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property cuotaFinanciacion.
     * @return Value of property cuotaFinanciacion.
     */
    public double getCuotaFinanciacion() {
        return cuotaFinanciacion;
    }
    
    /**
     * Setter for property cuotaFinanciacion.
     * @param cuotaFinanciacion New value of property cuotaFinanciacion.
     */
    public void setCuotaFinanciacion(double cuotaFinanciacion) {
        this.cuotaFinanciacion = cuotaFinanciacion;
    }
    
    /**
     * Getter for property fecha_migracion.
     * @return Value of property fecha_migracion.
     */
    public java.lang.String getFecha_migracion() {
        return fecha_migracion;
    }
    
    /**
     * Setter for property fecha_migracion.
     * @param fecha_migracion New value of property fecha_migracion.
     */
    public void setFecha_migracion(java.lang.String fecha_migracion) {
        this.fecha_migracion = fecha_migracion;
    }
    
    /**
     * Getter for property factura_inicial.
     * @return Value of property factura_inicial.
     */
    public java.lang.String getFactura_inicial() {
        return factura_inicial;
    }
    
    /**
     * Setter for property factura_inicial.
     * @param factura_inicial New value of property factura_inicial.
     */
    public void setFactura_inicial(java.lang.String factura_inicial) {
        this.factura_inicial = factura_inicial;
    }
    
    /**
     * Getter for property factura_financiacion.
     * @return Value of property factura_financiacion.
     */
    public java.lang.String getFactura_financiacion() {
        return factura_financiacion;
    }
    
    /**
     * Setter for property factura_financiacion.
     * @param factura_financiacion New value of property factura_financiacion.
     */
    public void setFactura_financiacion(java.lang.String factura_financiacion) {
        this.factura_financiacion = factura_financiacion;
    }
    
    /**
     * Getter for property factura_monitoreo.
     * @return Value of property factura_monitoreo.
     */
    public java.lang.String getFactura_monitoreo() {
        return factura_monitoreo;
    }
    
    /**
     * Setter for property factura_monitoreo.
     * @param factura_monitoreo New value of property factura_monitoreo.
     */
    public void setFactura_monitoreo(java.lang.String factura_monitoreo) {
        this.factura_monitoreo = factura_monitoreo;
    }
    
    /**
     * Getter for property nit_tercero.
     * @return Value of property nit_tercero.
     */
    public java.lang.String getNit_tercero() {
        return nit_tercero;
    }
    
    /**
     * Setter for property nit_tercero.
     * @param nit_tercero New value of property nit_tercero.
     */
    public void setNit_tercero(java.lang.String nit_tercero) {
        this.nit_tercero = nit_tercero;
    }
    
    /**
     * Getter for property banco_tercero.
     * @return Value of property banco_tercero.
     */
    public java.lang.String getBanco_tercero() {
        return banco_tercero;
    }
    
    /**
     * Setter for property banco_tercero.
     * @param banco_tercero New value of property banco_tercero.
     */
    public void setBanco_tercero(java.lang.String banco_tercero) {
        this.banco_tercero = banco_tercero;
    }
    
    /**
     * Getter for property sucursal_tercero.
     * @return Value of property sucursal_tercero.
     */
    public java.lang.String getSucursal_tercero() {
        return sucursal_tercero;
    }
    
    /**
     * Setter for property sucursal_tercero.
     * @param sucursal_tercero New value of property sucursal_tercero.
     */
    public void setSucursal_tercero(java.lang.String sucursal_tercero) {
        this.sucursal_tercero = sucursal_tercero;
    }
    
    /**
     * Getter for property banco_beneficiario.
     * @return Value of property banco_beneficiario.
     */
    public java.lang.String getBanco_beneficiario() {
        return banco_beneficiario;
    }
    
    /**
     * Setter for property banco_beneficiario.
     * @param banco_beneficiario New value of property banco_beneficiario.
     */
    public void setBanco_beneficiario(java.lang.String banco_beneficiario) {
        this.banco_beneficiario = banco_beneficiario;
    }
    
    /**
     * Getter for property sucursal_beneficiario.
     * @return Value of property sucursal_beneficiario.
     */
    public java.lang.String getSucursal_beneficiario() {
        return sucursal_beneficiario;
    }
    
    /**
     * Setter for property sucursal_beneficiario.
     * @param sucursal_beneficiario New value of property sucursal_beneficiario.
     */
    public void setSucursal_beneficiario(java.lang.String sucursal_beneficiario) {
        this.sucursal_beneficiario = sucursal_beneficiario;
    }
    
    
    public String getPeriodo(){
        switch (frecuencias ){
            case 30 : return "MENSUAL";    
            case 15 : return "QUINCENAL";  
            case  7 : return "SEMANAL";    
            case  1 : return "DIARIO";     
            default : return "INDEFINIDO"; 
        }
    }
    
    /**
     * Getter for property factura_monitoreo_inicial.
     * @return Value of property factura_monitoreo_inicial.
     */
    public java.lang.String getFactura_monitoreo_inicial() {
        return factura_monitoreo_inicial;
    }
    
    /**
     * Setter for property factura_monitoreo_inicial.
     * @param factura_monitoreo_inicial New value of property factura_monitoreo_inicial.
     */
    public void setFactura_monitoreo_inicial(java.lang.String factura_monitoreo_inicial) {
        this.factura_monitoreo_inicial = factura_monitoreo_inicial;
    }
    
    /**
     * Getter for property saldo_prestamo.
     * @return Value of property saldo_prestamo.
     */
    public double getSaldo_prestamo() {
        return saldo_prestamo;
    }
    
    /**
     * Setter for property saldo_prestamo.
     * @param saldo_prestamo New value of property saldo_prestamo.
     */
    public void setSaldo_prestamo(double saldo_prestamo) {
        this.saldo_prestamo = saldo_prestamo;
    }
    
    /**
     * Getter for property inicio_financiacion.
     * @return Value of property inicio_financiacion.
     */
    public int getInicio_financiacion() {
        return inicio_financiacion;
    }
    
    /**
     * Setter for property inicio_financiacion.
     * @param inicio_financiacion New value of property inicio_financiacion.
     */
    public void setInicio_financiacion(int inicio_financiacion) {
        this.inicio_financiacion = inicio_financiacion;
    }
    
    /**
     * Getter for property inicio_monitoreo.
     * @return Value of property inicio_monitoreo.
     */
    public int getInicio_monitoreo() {
        return inicio_monitoreo;
    }
    
    /**
     * Setter for property inicio_monitoreo.
     * @param inicio_monitoreo New value of property inicio_monitoreo.
     */
    public void setInicio_monitoreo(int inicio_monitoreo) {
        this.inicio_monitoreo = inicio_monitoreo;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
}
