/*
 * Nombre        StdJobMims.java
 * Autor         LREALES
 * Fecha         6 de septiembre de 2006, 9:24 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

public class StdJobMims {

    private String std_job_no_web;
    private String wo_type_web;
    private String work_group_web;
    private String account_code_c_web;
    private String account_code_i_web;
    private String unit_of_work_web;
    private String vlr_freight_web;
    private String currency_web;
    private String stdjobgen_web;
    private String origin_code_web;
    private String destination_code_web;
    
    private String std_job_no_mims;
    private String wo_type_mims;
    private String work_group_mims;
    private String account_code_c_mims;
    private String account_code_i_mims;
    private String unit_of_work_mims;
    private String vlr_freight_mims;
    private String currency_mims;
    private String stdjobgen_mims;
    private String origin_code_mims;
    private String destination_code_mims;
    
    private String Tipo_recurso1_web;
    private String Tipo_recurso2_web;
    private String Tipo_recurso3_web;
    private String Tipo_recurso4_web;
    private String Tipo_recurso5_web;
    private String recurso1_web;
    private String recurso2_web;
    private String recurso3_web;
    private String recurso4_web;
    private String recurso5_web;
    
    private String Tipo_recurso1_mims;
    private String Tipo_recurso2_mims;
    private String Tipo_recurso3_mims;
    private String Tipo_recurso4_mims;
    private String Tipo_recurso5_mims;
    private String recurso1_mims;
    private String recurso2_mims;
    private String recurso3_mims;
    private String recurso4_mims;
    private String recurso5_mims;
    
    public StdJobMims() { }
    
    /**
     * Getter for property std_job_no_web.
     * @return Value of property std_job_no_web.
     */
    public java.lang.String getStd_job_no_web() {
        return std_job_no_web;
    }
    
    /**
     * Setter for property std_job_no_web.
     * @param std_job_no_web New value of property std_job_no_web.
     */
    public void setStd_job_no_web(java.lang.String std_job_no_web) {
        this.std_job_no_web = std_job_no_web;
    }
    
    /**
     * Getter for property wo_type_web.
     * @return Value of property wo_type_web.
     */
    public java.lang.String getWo_type_web() {
        return wo_type_web;
    }
    
    /**
     * Setter for property wo_type_web.
     * @param wo_type_web New value of property wo_type_web.
     */
    public void setWo_type_web(java.lang.String wo_type_web) {
        this.wo_type_web = wo_type_web;
    }
    
    /**
     * Getter for property work_group_web.
     * @return Value of property work_group_web.
     */
    public java.lang.String getWork_group_web() {
        return work_group_web;
    }
    
    /**
     * Setter for property work_group_web.
     * @param work_group_web New value of property work_group_web.
     */
    public void setWork_group_web(java.lang.String work_group_web) {
        this.work_group_web = work_group_web;
    }
    
    /**
     * Getter for property account_code_c_web.
     * @return Value of property account_code_c_web.
     */
    public java.lang.String getAccount_code_c_web() {
        return account_code_c_web;
    }
    
    /**
     * Setter for property account_code_c_web.
     * @param account_code_c_web New value of property account_code_c_web.
     */
    public void setAccount_code_c_web(java.lang.String account_code_c_web) {
        this.account_code_c_web = account_code_c_web;
    }
    
    /**
     * Getter for property account_code_i_web.
     * @return Value of property account_code_i_web.
     */
    public java.lang.String getAccount_code_i_web() {
        return account_code_i_web;
    }
    
    /**
     * Setter for property account_code_i_web.
     * @param account_code_i_web New value of property account_code_i_web.
     */
    public void setAccount_code_i_web(java.lang.String account_code_i_web) {
        this.account_code_i_web = account_code_i_web;
    }
    
    /**
     * Getter for property unit_of_work_web.
     * @return Value of property unit_of_work_web.
     */
    public java.lang.String getUnit_of_work_web() {
        return unit_of_work_web;
    }
    
    /**
     * Setter for property unit_of_work_web.
     * @param unit_of_work_web New value of property unit_of_work_web.
     */
    public void setUnit_of_work_web(java.lang.String unit_of_work_web) {
        this.unit_of_work_web = unit_of_work_web;
    }
    
    /**
     * Getter for property vlr_freight_web.
     * @return Value of property vlr_freight_web.
     */
    public java.lang.String getVlr_freight_web() {
        return vlr_freight_web;
    }
    
    /**
     * Setter for property vlr_freight_web.
     * @param vlr_freight_web New value of property vlr_freight_web.
     */
    public void setVlr_freight_web(java.lang.String vlr_freight_web) {
        this.vlr_freight_web = vlr_freight_web;
    }
    
    /**
     * Getter for property currency_web.
     * @return Value of property currency_web.
     */
    public java.lang.String getCurrency_web() {
        return currency_web;
    }
    
    /**
     * Setter for property currency_web.
     * @param currency_web New value of property currency_web.
     */
    public void setCurrency_web(java.lang.String currency_web) {
        this.currency_web = currency_web;
    }
    
    /**
     * Getter for property stdjobgen_web.
     * @return Value of property stdjobgen_web.
     */
    public java.lang.String getStdjobgen_web() {
        return stdjobgen_web;
    }
    
    /**
     * Setter for property stdjobgen_web.
     * @param stdjobgen_web New value of property stdjobgen_web.
     */
    public void setStdjobgen_web(java.lang.String stdjobgen_web) {
        this.stdjobgen_web = stdjobgen_web;
    }
    
    /**
     * Getter for property origin_code_web.
     * @return Value of property origin_code_web.
     */
    public java.lang.String getOrigin_code_web() {
        return origin_code_web;
    }
    
    /**
     * Setter for property origin_code_web.
     * @param origin_code_web New value of property origin_code_web.
     */
    public void setOrigin_code_web(java.lang.String origin_code_web) {
        this.origin_code_web = origin_code_web;
    }
    
    /**
     * Getter for property destination_code_web.
     * @return Value of property destination_code_web.
     */
    public java.lang.String getDestination_code_web() {
        return destination_code_web;
    }
    
    /**
     * Setter for property destination_code_web.
     * @param destination_code_web New value of property destination_code_web.
     */
    public void setDestination_code_web(java.lang.String destination_code_web) {
        this.destination_code_web = destination_code_web;
    }
    
    /**
     * Getter for property std_job_no_mims.
     * @return Value of property std_job_no_mims.
     */
    public java.lang.String getStd_job_no_mims() {
        return std_job_no_mims;
    }
    
    /**
     * Setter for property std_job_no_mims.
     * @param std_job_no_mims New value of property std_job_no_mims.
     */
    public void setStd_job_no_mims(java.lang.String std_job_no_mims) {
        this.std_job_no_mims = std_job_no_mims;
    }
    
    /**
     * Getter for property wo_type_mims.
     * @return Value of property wo_type_mims.
     */
    public java.lang.String getWo_type_mims() {
        return wo_type_mims;
    }
    
    /**
     * Setter for property wo_type_mims.
     * @param wo_type_mims New value of property wo_type_mims.
     */
    public void setWo_type_mims(java.lang.String wo_type_mims) {
        this.wo_type_mims = wo_type_mims;
    }
    
    /**
     * Getter for property work_group_mims.
     * @return Value of property work_group_mims.
     */
    public java.lang.String getWork_group_mims() {
        return work_group_mims;
    }
    
    /**
     * Setter for property work_group_mims.
     * @param work_group_mims New value of property work_group_mims.
     */
    public void setWork_group_mims(java.lang.String work_group_mims) {
        this.work_group_mims = work_group_mims;
    }
    
    /**
     * Getter for property account_code_c_mims.
     * @return Value of property account_code_c_mims.
     */
    public java.lang.String getAccount_code_c_mims() {
        return account_code_c_mims;
    }
    
    /**
     * Setter for property account_code_c_mims.
     * @param account_code_c_mims New value of property account_code_c_mims.
     */
    public void setAccount_code_c_mims(java.lang.String account_code_c_mims) {
        this.account_code_c_mims = account_code_c_mims;
    }
    
    /**
     * Getter for property account_code_i_mims.
     * @return Value of property account_code_i_mims.
     */
    public java.lang.String getAccount_code_i_mims() {
        return account_code_i_mims;
    }
    
    /**
     * Setter for property account_code_i_mims.
     * @param account_code_i_mims New value of property account_code_i_mims.
     */
    public void setAccount_code_i_mims(java.lang.String account_code_i_mims) {
        this.account_code_i_mims = account_code_i_mims;
    }
    
    /**
     * Getter for property unit_of_work_mims.
     * @return Value of property unit_of_work_mims.
     */
    public java.lang.String getUnit_of_work_mims() {
        return unit_of_work_mims;
    }
    
    /**
     * Setter for property unit_of_work_mims.
     * @param unit_of_work_mims New value of property unit_of_work_mims.
     */
    public void setUnit_of_work_mims(java.lang.String unit_of_work_mims) {
        this.unit_of_work_mims = unit_of_work_mims;
    }
    
    /**
     * Getter for property vlr_freight_mims.
     * @return Value of property vlr_freight_mims.
     */
    public java.lang.String getVlr_freight_mims() {
        return vlr_freight_mims;
    }
    
    /**
     * Setter for property vlr_freight_mims.
     * @param vlr_freight_mims New value of property vlr_freight_mims.
     */
    public void setVlr_freight_mims(java.lang.String vlr_freight_mims) {
        this.vlr_freight_mims = vlr_freight_mims;
    }
    
    /**
     * Getter for property currency_mims.
     * @return Value of property currency_mims.
     */
    public java.lang.String getCurrency_mims() {
        return currency_mims;
    }
    
    /**
     * Setter for property currency_mims.
     * @param currency_mims New value of property currency_mims.
     */
    public void setCurrency_mims(java.lang.String currency_mims) {
        this.currency_mims = currency_mims;
    }
    
    /**
     * Getter for property stdjobgen_mims.
     * @return Value of property stdjobgen_mims.
     */
    public java.lang.String getStdjobgen_mims() {
        return stdjobgen_mims;
    }
    
    /**
     * Setter for property stdjobgen_mims.
     * @param stdjobgen_mims New value of property stdjobgen_mims.
     */
    public void setStdjobgen_mims(java.lang.String stdjobgen_mims) {
        this.stdjobgen_mims = stdjobgen_mims;
    }
    
    /**
     * Getter for property origin_code_mims.
     * @return Value of property origin_code_mims.
     */
    public java.lang.String getOrigin_code_mims() {
        return origin_code_mims;
    }
    
    /**
     * Setter for property origin_code_mims.
     * @param origin_code_mims New value of property origin_code_mims.
     */
    public void setOrigin_code_mims(java.lang.String origin_code_mims) {
        this.origin_code_mims = origin_code_mims;
    }
    
    /**
     * Getter for property destination_code_mims.
     * @return Value of property destination_code_mims.
     */
    public java.lang.String getDestination_code_mims() {
        return destination_code_mims;
    }
    
    /**
     * Setter for property destination_code_mims.
     * @param destination_code_mims New value of property destination_code_mims.
     */
    public void setDestination_code_mims(java.lang.String destination_code_mims) {
        this.destination_code_mims = destination_code_mims;
    }
    
    /**
     * Getter for property Tipo_recurso1_web.
     * @return Value of property Tipo_recurso1_web.
     */
    public java.lang.String getTipo_recurso1_web() {
        return Tipo_recurso1_web;
    }
    
    /**
     * Setter for property Tipo_recurso1_web.
     * @param Tipo_recurso1_web New value of property Tipo_recurso1_web.
     */
    public void setTipo_recurso1_web(java.lang.String Tipo_recurso1_web) {
        this.Tipo_recurso1_web = Tipo_recurso1_web;
    }
    
    /**
     * Getter for property Tipo_recurso2_web.
     * @return Value of property Tipo_recurso2_web.
     */
    public java.lang.String getTipo_recurso2_web() {
        return Tipo_recurso2_web;
    }
    
    /**
     * Setter for property Tipo_recurso2_web.
     * @param Tipo_recurso2_web New value of property Tipo_recurso2_web.
     */
    public void setTipo_recurso2_web(java.lang.String Tipo_recurso2_web) {
        this.Tipo_recurso2_web = Tipo_recurso2_web;
    }
    
    /**
     * Getter for property Tipo_recurso3_web.
     * @return Value of property Tipo_recurso3_web.
     */
    public java.lang.String getTipo_recurso3_web() {
        return Tipo_recurso3_web;
    }
    
    /**
     * Setter for property Tipo_recurso3_web.
     * @param Tipo_recurso3_web New value of property Tipo_recurso3_web.
     */
    public void setTipo_recurso3_web(java.lang.String Tipo_recurso3_web) {
        this.Tipo_recurso3_web = Tipo_recurso3_web;
    }
    
    /**
     * Getter for property Tipo_recurso4_web.
     * @return Value of property Tipo_recurso4_web.
     */
    public java.lang.String getTipo_recurso4_web() {
        return Tipo_recurso4_web;
    }
    
    /**
     * Setter for property Tipo_recurso4_web.
     * @param Tipo_recurso4_web New value of property Tipo_recurso4_web.
     */
    public void setTipo_recurso4_web(java.lang.String Tipo_recurso4_web) {
        this.Tipo_recurso4_web = Tipo_recurso4_web;
    }
    
    /**
     * Getter for property Tipo_recurso5_web.
     * @return Value of property Tipo_recurso5_web.
     */
    public java.lang.String getTipo_recurso5_web() {
        return Tipo_recurso5_web;
    }
    
    /**
     * Setter for property Tipo_recurso5_web.
     * @param Tipo_recurso5_web New value of property Tipo_recurso5_web.
     */
    public void setTipo_recurso5_web(java.lang.String Tipo_recurso5_web) {
        this.Tipo_recurso5_web = Tipo_recurso5_web;
    }
    
    /**
     * Getter for property recurso1_web.
     * @return Value of property recurso1_web.
     */
    public java.lang.String getRecurso1_web() {
        return recurso1_web;
    }
    
    /**
     * Setter for property recurso1_web.
     * @param recurso1_web New value of property recurso1_web.
     */
    public void setRecurso1_web(java.lang.String recurso1_web) {
        this.recurso1_web = recurso1_web;
    }
    
    /**
     * Getter for property recurso2_web.
     * @return Value of property recurso2_web.
     */
    public java.lang.String getRecurso2_web() {
        return recurso2_web;
    }
    
    /**
     * Setter for property recurso2_web.
     * @param recurso2_web New value of property recurso2_web.
     */
    public void setRecurso2_web(java.lang.String recurso2_web) {
        this.recurso2_web = recurso2_web;
    }
    
    /**
     * Getter for property recurso3_web.
     * @return Value of property recurso3_web.
     */
    public java.lang.String getRecurso3_web() {
        return recurso3_web;
    }
    
    /**
     * Setter for property recurso3_web.
     * @param recurso3_web New value of property recurso3_web.
     */
    public void setRecurso3_web(java.lang.String recurso3_web) {
        this.recurso3_web = recurso3_web;
    }
    
    /**
     * Getter for property recurso4_web.
     * @return Value of property recurso4_web.
     */
    public java.lang.String getRecurso4_web() {
        return recurso4_web;
    }
    
    /**
     * Setter for property recurso4_web.
     * @param recurso4_web New value of property recurso4_web.
     */
    public void setRecurso4_web(java.lang.String recurso4_web) {
        this.recurso4_web = recurso4_web;
    }
    
    /**
     * Getter for property recurso5_web.
     * @return Value of property recurso5_web.
     */
    public java.lang.String getRecurso5_web() {
        return recurso5_web;
    }
    
    /**
     * Setter for property recurso5_web.
     * @param recurso5_web New value of property recurso5_web.
     */
    public void setRecurso5_web(java.lang.String recurso5_web) {
        this.recurso5_web = recurso5_web;
    }
    
    /**
     * Getter for property Tipo_recurso1_mims.
     * @return Value of property Tipo_recurso1_mims.
     */
    public java.lang.String getTipo_recurso1_mims() {
        return Tipo_recurso1_mims;
    }
    
    /**
     * Setter for property Tipo_recurso1_mims.
     * @param Tipo_recurso1_mims New value of property Tipo_recurso1_mims.
     */
    public void setTipo_recurso1_mims(java.lang.String Tipo_recurso1_mims) {
        this.Tipo_recurso1_mims = Tipo_recurso1_mims;
    }
    
    /**
     * Getter for property Tipo_recurso2_mims.
     * @return Value of property Tipo_recurso2_mims.
     */
    public java.lang.String getTipo_recurso2_mims() {
        return Tipo_recurso2_mims;
    }
    
    /**
     * Setter for property Tipo_recurso2_mims.
     * @param Tipo_recurso2_mims New value of property Tipo_recurso2_mims.
     */
    public void setTipo_recurso2_mims(java.lang.String Tipo_recurso2_mims) {
        this.Tipo_recurso2_mims = Tipo_recurso2_mims;
    }
    
    /**
     * Getter for property Tipo_recurso3_mims.
     * @return Value of property Tipo_recurso3_mims.
     */
    public java.lang.String getTipo_recurso3_mims() {
        return Tipo_recurso3_mims;
    }
    
    /**
     * Setter for property Tipo_recurso3_mims.
     * @param Tipo_recurso3_mims New value of property Tipo_recurso3_mims.
     */
    public void setTipo_recurso3_mims(java.lang.String Tipo_recurso3_mims) {
        this.Tipo_recurso3_mims = Tipo_recurso3_mims;
    }
    
    /**
     * Getter for property Tipo_recurso4_mims.
     * @return Value of property Tipo_recurso4_mims.
     */
    public java.lang.String getTipo_recurso4_mims() {
        return Tipo_recurso4_mims;
    }
    
    /**
     * Setter for property Tipo_recurso4_mims.
     * @param Tipo_recurso4_mims New value of property Tipo_recurso4_mims.
     */
    public void setTipo_recurso4_mims(java.lang.String Tipo_recurso4_mims) {
        this.Tipo_recurso4_mims = Tipo_recurso4_mims;
    }
    
    /**
     * Getter for property Tipo_recurso5_mims.
     * @return Value of property Tipo_recurso5_mims.
     */
    public java.lang.String getTipo_recurso5_mims() {
        return Tipo_recurso5_mims;
    }
    
    /**
     * Setter for property Tipo_recurso5_mims.
     * @param Tipo_recurso5_mims New value of property Tipo_recurso5_mims.
     */
    public void setTipo_recurso5_mims(java.lang.String Tipo_recurso5_mims) {
        this.Tipo_recurso5_mims = Tipo_recurso5_mims;
    }
    
    /**
     * Getter for property recurso1_mims.
     * @return Value of property recurso1_mims.
     */
    public java.lang.String getRecurso1_mims() {
        return recurso1_mims;
    }
    
    /**
     * Setter for property recurso1_mims.
     * @param recurso1_mims New value of property recurso1_mims.
     */
    public void setRecurso1_mims(java.lang.String recurso1_mims) {
        this.recurso1_mims = recurso1_mims;
    }
    
    /**
     * Getter for property recurso2_mims.
     * @return Value of property recurso2_mims.
     */
    public java.lang.String getRecurso2_mims() {
        return recurso2_mims;
    }
    
    /**
     * Setter for property recurso2_mims.
     * @param recurso2_mims New value of property recurso2_mims.
     */
    public void setRecurso2_mims(java.lang.String recurso2_mims) {
        this.recurso2_mims = recurso2_mims;
    }
    
    /**
     * Getter for property recurso3_mims.
     * @return Value of property recurso3_mims.
     */
    public java.lang.String getRecurso3_mims() {
        return recurso3_mims;
    }
    
    /**
     * Setter for property recurso3_mims.
     * @param recurso3_mims New value of property recurso3_mims.
     */
    public void setRecurso3_mims(java.lang.String recurso3_mims) {
        this.recurso3_mims = recurso3_mims;
    }
    
    /**
     * Getter for property recurso4_mims.
     * @return Value of property recurso4_mims.
     */
    public java.lang.String getRecurso4_mims() {
        return recurso4_mims;
    }
    
    /**
     * Setter for property recurso4_mims.
     * @param recurso4_mims New value of property recurso4_mims.
     */
    public void setRecurso4_mims(java.lang.String recurso4_mims) {
        this.recurso4_mims = recurso4_mims;
    }
    
    /**
     * Getter for property recurso5_mims.
     * @return Value of property recurso5_mims.
     */
    public java.lang.String getRecurso5_mims() {
        return recurso5_mims;
    }
    
    /**
     * Setter for property recurso5_mims.
     * @param recurso5_mims New value of property recurso5_mims.
     */
    public void setRecurso5_mims(java.lang.String recurso5_mims) {
        this.recurso5_mims = recurso5_mims;
    }
    
}