/*
 * Nombre        Ciudad.java
 * Autor         Ing. Diogenes Bastidas.
 * Fecha         01 de abril de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.tsp.util.*;

public class Ciudad implements java.io.Serializable {
    private String codciu;
    private String nomciu;
    private String pais;
    private String aplica;
    private String codica;
    private String zona;
    private String frontera;
    private String agasoc;
    
    //TRAFICO
    private String pais_code;
    private String pais_name;
    private String departament_code;
    private String departament_name;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    
    //Nuevos..
    private String base;
    private String tipociu;
    private String nomzona;
    private String nomagasoc;
    private boolean reporteUrbano;
    //Diogenes
    private int indicativo;
    
    private String frontera_asoc;
    private String tiene_rep_urbano;
    private String zona_urb;
    
    private Vector detalle;
    
    public static Ciudad load(ResultSet rs)throws SQLException {
        Ciudad ciudad = new Ciudad();
        
        ciudad.setCodCiu( rs.getString("CODCIU") );
        ciudad.setNomCiu( rs.getString("NOMCIU") );
        ciudad.setPais( rs.getString("PAIS") );
        ciudad.setAplIca( rs.getString("APLICA") );
        ciudad.setCodIca( rs.getString("CODICA") );
        ciudad.setZona( rs.getString("ZONA") );
        ciudad.setFrontera( rs.getString("FRONTERA") );
        ciudad.setAgAsoc( rs.getString("AGASOC") );
        ciudad.setCodigo(rs.getString("CODCIU") );
        ciudad.setNombre(rs.getString("NOMCIU"));
        ciudad.setdepartament_code(rs.getString("coddpt"));
        return ciudad;
    }
    
    
    public static Ciudad load2(ResultSet rs)throws SQLException {
        Ciudad ciudad = new Ciudad();
        ciudad.setcodciu(rs.getString( "codciu") );
        ciudad.setnomciu(rs.getString( "nomciu") );
        ciudad.setpais_code( rs.getString("pais_code") );
        ciudad.setpais_name( rs.getString("pais_name") );
        ciudad.setdepartament_code( rs.getString( "departament_code") );
        ciudad.setdepartament_name( rs.getString("departament_name" ) );
        ciudad.setRec_status( rs.getString("rec_status") );
        ciudad.setLast_update( rs.getDate("last_update") );
        ciudad.setUser_update( rs.getString("user_update") );
        ciudad.setCreation_date( rs.getDate("creation_date") );
        ciudad.setCreation_user( rs.getString("creation_user") );
        
        return ciudad;
        
    }
    
    public static Ciudad load3(ResultSet rs)throws SQLException {
        Ciudad ciudad = new Ciudad();
        ciudad.setcodciu(rs.getString( "codciu") );
        ciudad.setnomciu(rs.getString( "nomciu") );
        ciudad.setpais_code( rs.getString("country_code") );
        ciudad.setpais_name( rs.getString("country_name") );
        ciudad.setdepartament_code( rs.getString( "departament_code") );
        ciudad.setdepartament_name( rs.getString("departament_name" ) );
        ciudad.setRec_status( rs.getString("reg_status") );
        ciudad.setLast_update( rs.getDate("last_update") );
        ciudad.setUser_update( rs.getString("user_update") );
        ciudad.setCreation_date( rs.getDate("creation_date") );
        ciudad.setCreation_user( rs.getString("creation_user") );
        ciudad.setAplIca( rs.getString("aplica"));
        ciudad.setCodIca( rs.getString("codica"));
        ciudad.setZona( rs.getString("zona"));
        ciudad.setFrontera( rs.getString("frontera"));
        ciudad.setAgAsoc( rs.getString("agasoc"));
        ciudad.setBase( rs.getString("base"));
        ciudad.setTipociu( rs.getString("tipociu"));
        ciudad.setNomzona( rs.getString("desczona"));
        ciudad.setNomagasoc( rs.getString("nomagasoc"));
        //nuevo
        ciudad.setIndicativo(rs.getInt("indicativo"));
        return ciudad;
        
    }
    
    
    public static Ciudad load4(ResultSet rs)throws SQLException {
        Ciudad ciudad = new Ciudad();
        ciudad.setcodciu(rs.getString( "codciu") );
        ciudad.setnomciu(rs.getString( "nomciu") );
        
        return ciudad;
        
    }
    
    
    
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        
        if (getCodCiu() != null)
            sb.append(Util.quote(getCodCiu()));
        
        sb.append(",");
        if (getNomCiu() != null)
            sb.append(Util.quote(getNomCiu()));
        
        return sb.toString();
    }
    
    public String getCodCiu() {
        return codciu;
    }
    
    public void setCodCiu(String codciu) {
        this.codciu = codciu;
    }
    
    public String getNomCiu() {
        return nomciu;
    }
    
    public void setNomCiu(String nomciu) {
        this.nomciu = nomciu;
    }
    
    public String getPais() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais = pais;
    }
    
    public String getAplIca() {
        return aplica;
    }
    
    public void setAplIca(String aplica) {
        this.aplica = aplica;
    }
    
    public String getCodIca() {
        return codica;
    }
    
    public void setCodIca(String codica) {
        this.codica = codica;
    }
    
    public String getZona() {
        return zona;
    }
    
    public void setZona(String zona) {
        this.zona = zona;
    }
    
    public String getFrontera() {
        return frontera;
    }
    
    public void setFrontera(String frontera) {
        this.frontera = frontera;
    }
    
    public String getAgAsoc() {
        return agasoc;
    }
    
    public void setAgAsoc(String agasoc) {
        this.agasoc = agasoc;
    }
    public void setCodigo(String valor) {
        this.codciu = valor;
    }
    
    public void setNombre(String valor) {
        this.nomciu = valor;
    }
    
    //Getter
    public String getCodigo() {
        return this.codciu;
    }
    
    public String getNombre() {
        return this.nomciu;
    }
    
    //trafico
    
    public void setcodciu(String codciu) {
        
        this.codciu = codciu;
        
    }
    public void setnomciu(String nomciu) {
        
        this.nomciu = nomciu;
        
    }
    public void setpais_code(String pais_code) {
        
        this.pais_code = pais_code;
        
    }
    public void setpais_name(String pais_name) {
        
        this.pais_name = pais_name;
        
    }
    public void setdepartament_code(String departament_code) {
        
        this.departament_code = departament_code;
        
    }
    public void setdepartament_name(String departament_name) {
        
        this.departament_name = departament_name;
        
    }
    
    public void setRec_status(String rec_status ){
        
        this.rec_status = rec_status;
    }
    public void setLast_update(java.util.Date last_update){
        
        this.last_update = last_update;
        
    }
    public void setUser_update(String user_update){
        this.user_update= user_update;
    }
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date = creation_date;
        
    }
    public void setCreation_user(String creation_user){
        
        this.creation_user= creation_user;
        
    }
    
    public String getcodciu( ) {
        
        return codciu;
        
    }
    public String getnomciu( ) {
        
        return nomciu;
        
    }
    public String getpais_code( ) {
        
        return pais_code;
        
    }
    public String getpais_name() {
        
        return pais_name;
        
    }
    public String getdepartament_code( ) {
        
        return departament_code;
        
    }
    public String getdepartament_name() {
        
        return departament_name;
        
    }
    public String getRec_status(){
        
        return rec_status;
    }
    public java.util.Date getLast_update( ){
        
        return last_update;
        
    }
    public String getUser_update( ){
        
        return user_update;
        
    }
    public java.util.Date getCreation_date( ){
        
        return creation_date;
        
    }
    public String getCreation_user( ){
        return creation_user;
    }
    
   
    public String getBase() {
        return this.base;
    }
    
    public void setBase(String base) {
        this.base = base;
    }
    
    public String getTipociu() {
        return this.tipociu;
    }
    
    public void setTipociu(String tipociu) {
        this.tipociu = tipociu;
    }
    
    public String getNomzona() {
        return this.nomzona;
    }
    
    public void setNomzona(String nomzona) {
        this.nomzona = nomzona;
    }
    
    /**
     * Getter for property nomagasoc.
     * @return Value of property nomagasoc.
     */
    public String getNomagasoc() {
        return this.nomagasoc;
    }
    
    /**
     * Setter for property nomagasoc.
     * @param nomagasoc New value of property nomagasoc.
     */
    public void setNomagasoc(String nomagasoc) {
        this.nomagasoc = nomagasoc;
    }
    
    /**
     * Getter for property indicativo.
     * @return Value of property indicativo.
     */
    public int getIndicativo() {
        return indicativo;
    }
    
    /**
     * Setter for property indicativo.
     * @param indicativo New value of property indicativo.
     */
    public void setIndicativo(int indicativo) {
        this.indicativo = indicativo;
    }
    
    /**
     * Getter for property reporteUrbano.
     * @return Value of property reporteUrbano.
     */
    public boolean isReporteUrbano() {
        return reporteUrbano;
    }
    
    /**
     * Setter for property reporteUrbano.
     * @param reporteUrbano New value of property reporteUrbano.
     */
    public void setReporteUrbano(boolean reporteUrbano) {
        this.reporteUrbano = reporteUrbano;
    }
    
    /**
     * Getter for property frontera_asoc.
     * @return Value of property frontera_asoc.
     */
    public java.lang.String getFrontera_asoc() {
        return frontera_asoc;
    }
    
    /**
     * Setter for property frontera_asoc.
     * @param frontera_asoc New value of property frontera_asoc.
     */
    public void setFrontera_asoc(java.lang.String frontera_asoc) {
        this.frontera_asoc = frontera_asoc;
    }
    
    /**
     * Getter for property tiene_rep_urbano.
     * @return Value of property tiene_rep_urbano.
     */
    public java.lang.String getTiene_rep_urbano() {
        return tiene_rep_urbano;
    }
    
    /**
     * Setter for property tiene_rep_urbano.
     * @param tiene_rep_urbano New value of property tiene_rep_urbano.
     */
    public void setTiene_rep_urbano(java.lang.String tiene_rep_urbano) {
        this.tiene_rep_urbano = tiene_rep_urbano;
    }
    
    /**
     * Getter for property zona_urb.
     * @return Value of property zona_urb.
     */
    public java.lang.String getZona_urb() {
        return zona_urb;
    }
    
    /**
     * Setter for property zona_urb.
     * @param zona_urb New value of property zona_urb.
     */
    public void setZona_urb(java.lang.String zona_urb) {
        this.zona_urb = zona_urb;
    }
    
    /**
     * Getter for property detalle.
     * @return Value of property detalle.
     */
    public java.util.Vector getDetalle() {
        return detalle;
    }
    
    /**
     * Setter for property detalle.
     * @param detalle New value of property detalle.
     */
    public void setDetalle(java.util.Vector detalle) {
        this.detalle = detalle;
    }
    
}
