/*************************************************************************************
 * Nombre clase :                   HojaControlViaje.java                            *
 * Descripcion :                    Clase que maneja los atributos relacionados con  *
 *                                  la impresion de Hoja de Control de Viaje         *
 * Autor :                          Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                          16 de diciembre de 2005, 11:31 AM                *
 * Version :                        1.0                                              *
 * Copyright :                      Fintravalores S.A.                          *
 ************************************************************************************/
package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;


public class HojaControlViaje {
    private String cliente_remitente;
    private String ot;
    private String oc;
    private String origen_carga;
    private String destino_final;   
    private String observaciones;
    private String dia;
    private String mes;
    private String ano;
    private String salida;
    private String estadol;   
    private String si;
    private String no;
    private String cant;
    private String cabezote;
    private String trailer;
    private String tipo_trailer;
    private String estados;
    private String llegada;
    private String firma;
    private String clase;
    private String precintos;
    
    
    /** Creates a new instance of HojaControlViaje */
    public HojaControlViaje() {
    }
    
    public static HojaControlViaje load(ResultSet rs)throws Exception{
        HojaControlViaje datos = new HojaControlViaje();
        String fecha = rs.getString("fecha");
        datos.setAno(fecha.substring(0,4));
        datos.setMes(fecha.substring(5,7));
        datos.setDia(fecha.substring(8,10));
        datos.setOrigen_carga(rs.getString("origen"));
        datos.setDestino_final(rs.getString("destino"));
        datos.setCliente_remitente(rs.getString("cliente"));
        datos.setOt(rs.getString("numrem"));
        datos.setOc(rs.getString("numpla"));
        datos.setCabezote(rs.getString("plaveh"));
        datos.setTrailer(rs.getString("platlr"));
        datos.setClase(rs.getString("clase"));
        datos.setPrecintos(rs.getString("precinto"));
        return datos;
    }
    
    /**
     * Getter for property cliente_remitente.
     * @return Value of property cliente_remitente.
     */
    public java.lang.String getCliente_remitente() {
        return cliente_remitente;
    }
    
    /**
     * Setter for property cliente_remitente.
     * @param cliente_remitente New value of property cliente_remitente.
     */
    public void setCliente_remitente(java.lang.String cliente_remitente) {
        this.cliente_remitente = cliente_remitente;
    }
    
    /**
     * Getter for property destino_final.
     * @return Value of property destino_final.
     */
    public java.lang.String getDestino_final() {
        return destino_final;
    }
    
    /**
     * Setter for property destino_final.
     * @param destino_final New value of property destino_final.
     */
    public void setDestino_final(java.lang.String destino_final) {
        this.destino_final = destino_final;
    }
    

    
    /**
     * Getter for property observaciones.
     * @return Value of property observaciones.
     */
    public java.lang.String getObservaciones() {
        return observaciones;
    }
    
    /**
     * Setter for property observaciones.
     * @param observaciones New value of property observaciones.
     */
    public void setObservaciones(java.lang.String observaciones) {
        this.observaciones = observaciones;
    }
    
    /**
     * Getter for property oc.
     * @return Value of property oc.
     */
    public java.lang.String getOc() {
        return oc;
    }
    
    /**
     * Setter for property oc.
     * @param oc New value of property oc.
     */
    public void setOc(java.lang.String oc) {
        this.oc = oc;
    }
    
    /**
     * Getter for property origen_carga.
     * @return Value of property origen_carga.
     */
    public java.lang.String getOrigen_carga() {
        return origen_carga;
    }
    
    /**
     * Setter for property origen_carga.
     * @param origen_carga New value of property origen_carga.
     */
    public void setOrigen_carga(java.lang.String origen_carga) {
        this.origen_carga = origen_carga;
    }
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt() {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt(java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property dia.
     * @return Value of property dia.
     */
    public java.lang.String getDia() {
        return dia;
    }    
   
    /**
     * Setter for property dia.
     * @param dia New value of property dia.
     */
    public void setDia(java.lang.String dia) {
        this.dia = dia;
    }    
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property ano.
     * @return Value of property ano.
     */
    public java.lang.String getAno() {
        return ano;
    }
    
    /**
     * Setter for property ano.
     * @param ano New value of property ano.
     */
    public void setAno(java.lang.String ano) {
        this.ano = ano;
    }
    
    /**
     * Getter for property salida.
     * @return Value of property salida.
     */
    public java.lang.String getSalida() {
        return salida;
    }
    
    /**
     * Setter for property salida.
     * @param salida New value of property salida.
     */
    public void setSalida(java.lang.String salida) {
        this.salida = salida;
    }
    
    /**
     * Getter for property estadol.
     * @return Value of property estadol.
     */
    public java.lang.String getEstadol() {
        return estadol;
    }
    
    /**
     * Setter for property estadol.
     * @param estadol New value of property estadol.
     */
    public void setEstadol(java.lang.String estadol) {
        this.estadol = estadol;
    }
    
    /**
     * Getter for property si.
     * @return Value of property si.
     */
    public java.lang.String getSi() {
        return si;
    }
    
    /**
     * Setter for property si.
     * @param si New value of property si.
     */
    public void setSi(java.lang.String si) {
        this.si = si;
    }
    
    /**
     * Getter for property no.
     * @return Value of property no.
     */
    public java.lang.String getNo() {
        return no;
    }
    
    /**
     * Setter for property no.
     * @param no New value of property no.
     */
    public void setNo(java.lang.String no) {
        this.no = no;
    }
    
    /**
     * Getter for property cant.
     * @return Value of property cant.
     */
    public java.lang.String getCant() {
        return cant;
    }
    
    /**
     * Setter for property cant.
     * @param cant New value of property cant.
     */
    public void setCant(java.lang.String cant) {
        this.cant = cant;
    }
    
    /**
     * Getter for property cabezote.
     * @return Value of property cabezote.
     */
    public java.lang.String getCabezote() {
        return cabezote;
    }
    
    /**
     * Setter for property cabezote.
     * @param cabezote New value of property cabezote.
     */
    public void setCabezote(java.lang.String cabezote) {
        this.cabezote = cabezote;
    }
    
    /**
     * Getter for property trailer.
     * @return Value of property trailer.
     */
    public java.lang.String getTrailer() {
        return trailer;
    }
    
    /**
     * Setter for property trailer.
     * @param trailer New value of property trailer.
     */
    public void setTrailer(java.lang.String trailer) {
        this.trailer = trailer;
    }
    
    /**
     * Getter for property tipo_trailer.
     * @return Value of property tipo_trailer.
     */
    public java.lang.String getTipo_trailer() {
        return tipo_trailer;
    }
    
    /**
     * Setter for property tipo_trailer.
     * @param tipo_trailer New value of property tipo_trailer.
     */
    public void setTipo_trailer(java.lang.String tipo_trailer) {
        this.tipo_trailer = tipo_trailer;
    }
    
    /**
     * Getter for property estados.
     * @return Value of property estados.
     */
    public java.lang.String getEstados() {
        return estados;
    }
    
    /**
     * Setter for property estados.
     * @param estados New value of property estados.
     */
    public void setEstados(java.lang.String estados) {
        this.estados = estados;
    }
    
    /**
     * Getter for property llegada.
     * @return Value of property llegada.
     */
    public java.lang.String getLlegada() {
        return llegada;
    }
    
    /**
     * Setter for property llegada.
     * @param llegada New value of property llegada.
     */
    public void setLlegada(java.lang.String llegada) {
        this.llegada = llegada;
    }
    
    /**
     * Getter for property firma.
     * @return Value of property firma.
     */
    public java.lang.String getFirma() {
        return firma;
    }
    
    /**
     * Setter for property firma.
     * @param firma New value of property firma.
     */
    public void setFirma(java.lang.String firma) {
        this.firma = firma;
    }
    
    /**
     * Getter for property clase.
     * @return Value of property clase.
     */
    public java.lang.String getClase() {
        return clase;
    }
    
    /**
     * Setter for property clase.
     * @param clase New value of property clase.
     */
    public void setClase(java.lang.String clase) {
        this.clase = clase;
    }
    
    /**
     * Getter for property precintos.
     * @return Value of property precintos.
     */
    public java.lang.String getPrecintos() {
        return precintos;
    }
    
    /**
     * Setter for property precintos.
     * @param precintos New value of property precintos.
     */
    public void setPrecintos(java.lang.String precintos) {
        this.precintos = precintos;
    }
    
}
