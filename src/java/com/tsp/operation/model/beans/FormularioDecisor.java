/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class FormularioDecisor {

    private String tipoIdentificacion;
    private String identificacion;
    private String primerApellido;
    private String anio_Nacimiento;
    private String mes_Nacimiento;
    private String tipo_Producto;
    private String servicio;
    private String ciudad_Matricula;
    private String tipo_Vivienda;
    private String arriendo;
    private String estado_Civil;
    private String conyugue;
    private String ingreso;
    private String monto_Solicitado;
    private String cuota_Credito_Solicitado;
    private String valor_Vehiculo;
    private String valor_Semestre;
    private String plazo;
    private String plazoUniversitario;
    private String credito_Fenalco_Bolivar;
    private String nitEmpresa;

    public FormularioDecisor() {
    }

    /**
     * @return the tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the primerApellido
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * @param primerApellido the primerApellido to set
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     * @return the anio_Nacimiento
     */
    public String getAnio_Nacimiento() {
        return anio_Nacimiento;
    }

    /**
     * @param anio_Nacimiento the anio_Nacimiento to set
     */
    public void setAnio_Nacimiento(String anio_Nacimiento) {
        this.anio_Nacimiento = anio_Nacimiento;
    }

    /**
     * @return the mes_Nacimiento
     */
    public String getMes_Nacimiento() {
        return mes_Nacimiento;
    }

    /**
     * @param mes_Nacimiento the mes_Nacimiento to set
     */
    public void setMes_Nacimiento(String mes_Nacimiento) {
        this.mes_Nacimiento = mes_Nacimiento;
    }

    /**
     * @return the tipo_Producto
     */
    public String getTipo_Producto() {
        return tipo_Producto;
    }

    /**
     * @param tipo_Producto the tipo_Producto to set
     */
    public void setTipo_Producto(String tipo_Producto) {
        this.tipo_Producto = tipo_Producto;
    }

    /**
     * @return the servicio
     */
    public String getServicio() {
        return servicio;
    }

    /**
     * @param servicio the servicio to set
     */
    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    /**
     * @return the ciudad_Matricula
     */
    public String getCiudad_Matricula() {
        return ciudad_Matricula;
    }

    /**
     * @param ciudad_Matricula the ciudad_Matricula to set
     */
    public void setCiudad_Matricula(String ciudad_Matricula) {
        this.ciudad_Matricula = ciudad_Matricula;
    }

    /**
     * @return the tipo_Vivienda
     */
    public String getTipo_Vivienda() {
        return tipo_Vivienda;
    }

    /**
     * @param tipo_Vivienda the tipo_Vivienda to set
     */
    public void setTipo_Vivienda(String tipo_Vivienda) {
        this.tipo_Vivienda = tipo_Vivienda;
    }

    /**
     * @return the arriendo
     */
    public String getArriendo() {
        return arriendo;
    }

    /**
     * @param arriendo the arriendo to set
     */
    public void setArriendo(String arriendo) {
        this.arriendo = arriendo;
    }

    /**
     * @return the estado_Civil
     */
    public String getEstado_Civil() {
        return estado_Civil;
    }

    /**
     * @param estado_Civil the estado_Civil to set
     */
    public void setEstado_Civil(String estado_Civil) {
        this.estado_Civil = estado_Civil;
    }

    /**
     * @return the conyugue
     */
    public String getConyugue() {
        return conyugue;
    }

    /**
     * @param conyugue the conyugue to set
     */
    public void setConyugue(String conyugue) {
        this.conyugue = conyugue;
    }

    /**
     * @return the ingreso
     */
    public String getIngreso() {
        return ingreso;
    }

    /**
     * @param ingreso the ingreso to set
     */
    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    /**
     * @return the monto_Solicitado
     */
    public String getMonto_Solicitado() {
        return monto_Solicitado;
    }

    /**
     * @param monto_Solicitado the monto_Solicitado to set
     */
    public void setMonto_Solicitado(String monto_Solicitado) {
        this.monto_Solicitado = monto_Solicitado;
    }

    /**
     * @return the cuota_Credito_Solicitado
     */
    public String getCuota_Credito_Solicitado() {
        return cuota_Credito_Solicitado;
    }

    /**
     * @param cuota_Credito_Solicitado the cuota_Credito_Solicitado to set
     */
    public void setCuota_Credito_Solicitado(String cuota_Credito_Solicitado) {
        this.cuota_Credito_Solicitado = cuota_Credito_Solicitado;
    }

    /**
     * @return the valor_Vehiculo
     */
    public String getValor_Vehiculo() {
        return valor_Vehiculo;
    }

    /**
     * @param valor_Vehiculo the valor_Vehiculo to set
     */
    public void setValor_Vehiculo(String valor_Vehiculo) {
        this.valor_Vehiculo = valor_Vehiculo;
    }

    /**
     * @return the valor_Semestre
     */
    public String getValor_Semestre() {
        return valor_Semestre;
    }

    /**
     * @param valor_Semestre the valor_Semestre to set
     */
    public void setValor_Semestre(String valor_Semestre) {
        this.valor_Semestre = valor_Semestre;
    }

    /**
     * @return the plazo
     */
    public String getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the plazoUniversitario
     */
    public String getPlazoUniversitario() {
        return plazoUniversitario;
    }

    /**
     * @param plazoUniversitario the plazoUniversitario to set
     */
    public void setPlazoUniversitario(String plazoUniversitario) {
        this.plazoUniversitario = plazoUniversitario;
    }

    /**
     * @return the credito_Fenalco_Bolivar
     */
    public String getCredito_Fenalco_Bolivar() {
        return credito_Fenalco_Bolivar;
    }

    /**
     * @param credito_Fenalco_Bolivar the credito_Fenalco_Bolivar to set
     */
    public void setCredito_Fenalco_Bolivar(String credito_Fenalco_Bolivar) {
        this.credito_Fenalco_Bolivar = credito_Fenalco_Bolivar;
    }

    /**
     * @return the nitEmpresa
     */
    public String getNitEmpresa() {
        return nitEmpresa;
    }

    /**
     * @param nitEmpresa the nitEmpresa to set
     */
    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }
    
    
}
