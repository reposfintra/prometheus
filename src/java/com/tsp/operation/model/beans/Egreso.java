/*
 * Egreso.java
 *
 * Created on 11 de enero de 2005, 08:14 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
public class Egreso {
    
    /** Creates a new instance of Egreso */
    private String dstrct;
    private String branch_code;
    private String bank_account_no;
    private String document_no;
    private String nit;
    private String payment_name;
    private String agency_id;
    private String pmt_date;
    private String printer_date;
    private String concept_code;
    private float vlr;
    private float vlr_for;
    private String currency;
    private String item_no;
    private String oc;
    private String creation_date;
    
    //MARIO 201005
    private String fecha_entrega;
    private String fecha_envio;
    private String fecha_recibido;
    
    //AMATURANA
    private String base;
    private int transaccion;
    
    private String descripcion;
    private String concept_name;
    private boolean mostrarDetalleEgreso  = false;
    private double tasa;
    private String tipo_documento;
    private String documento;
    private String tipo_pago;
    
    private java.util.Vector detalle;
    private CXP_Doc factura;
    private Planilla planilla;
    //AMATURANA 13.02.2007
    private String estado;
    private boolean reimpresion = false;
    private String usuario_impresion;
    
    private String reg_status;
    
   private String banco_reemplazo;
   private String suc_reemplazo;
    
   private String cheque_reemplazo;
   
    //AMATURANA 30.04.2007
   private double valor;
   private double valor_for;
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property transaccion.
     * @return Value of property transaccion.
     */
    public int getTransaccion() {
        return transaccion;
    }
    
    /**
     * Setter for property transaccion.
     * @param transaccion New value of property transaccion.
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }
    
    
    public Egreso(){
        pmt_date       = "";
        fecha_entrega  = "";
        fecha_envio    = "";
        fecha_recibido = "";
    }
    
    public String getItem_no(){
        return this.item_no;
    }
    public void setItem_no(String item_no){
        this.item_no=item_no;
    }
    
    public String getOc(){
        return this.oc;
    }
    public void setOc(String oc){
        this.oc=oc;
    }
    
    public String getDstrct(){
        return this.dstrct;
    }
    public void setDstrct(String dstrct){
        this.dstrct=dstrct;
    }
    
    public String getBranch_code(){
        return this.branch_code;
    }
    public void setBranch_code(String branch_code){
        this.branch_code=branch_code;
    }
    
    public String getBank_account_no(){
        return this.bank_account_no;
    }
    public void setBank_account_no(String bank_account_no){
        this.bank_account_no=bank_account_no;
    }
    
    public String getDocument_no(){
        return this.document_no;
    }
    public void setDocument_no(String document_no){
        this.document_no=document_no;
    }
    
    public String getNit(){
        return this.nit;
    }
    public void setNit(String nit){
        this.nit=nit;
    }
    
    public String getPayment_name(){
        return this.payment_name;
    }
    public void setPayment_name(String payment_name){
        this.payment_name=payment_name;
    }
    
    public String getAgency_id(){
        return this.agency_id;
    }
    public void setAgency_id(String agency_id){
        this.agency_id=agency_id;
    }
    
    
    public String getPmt_date(){
        return this.pmt_date;
    }
    public void setPmt_date(String pmt_date){
        this.pmt_date=pmt_date;
    }
    
    public String getPrinter_date(){
        return this.printer_date;
    }
    public void setPrinter_date(String printer_date){
        this.printer_date=printer_date;
    }
    
    public String getCreation_date(){
        return this.creation_date;
    }
    public void setCreation_date(String creation_date){
        this.creation_date=creation_date;
    }
    
    public String getConcept_code(){
        return this.concept_code;
    }
    public void setConcept_code(String concept_code){
        this.concept_code=concept_code;
    }
    
    public float getVlr(){
        return this.vlr;
    }
    public void setVlr(float vlr){
        this.vlr=vlr;
    }
    
    public float getVlr_for(){
        return this.vlr_for;
    }
    public void setVlr_for(float vlr_for){
        this.vlr_for=vlr_for;
    }
    
    public String getCurrency(){
        return this.currency;
    }
    public void setCurrency(String currency){
        this.currency=currency;
    }
    
    //MARIO 201005
    public String getFechaEntrega(){
        return this.fecha_entrega;
    }
    public void setFechaEntrega(String fecha){
        this.fecha_entrega=fecha;
    }
    
    public String getFechaEnvio(){
        return this.fecha_envio;
    }
    public void setFechaEnvio(String fecha){
        this.fecha_envio=fecha;
    }
    
    public String getFechaRecibido(){
        return this.fecha_recibido;
    }
    public void setFechaRecibido(String fecha){
        this.fecha_recibido=fecha;
    }
    
    /**
     * Getter for property concept_name.
     * @return Value of property concept_name.
     */
    public java.lang.String getConcept_name() {
        return concept_name;
    }
    
    /**
     * Setter for property concept_name.
     * @param concept_name New value of property concept_name.
     */
    public void setConcept_name(java.lang.String concept_name) {
        this.concept_name = concept_name;
    }
    
    /**
     * Getter for property mostrarDetalleEgreso.
     * @return Value of property mostrarDetalleEgreso.
     */
    public boolean isMostrarDetalleEgreso() {
        return mostrarDetalleEgreso;
    }
    
    /**
     * Setter for property mostrarDetalleEgreso.
     * @param mostrarDetalleEgreso New value of property mostrarDetalleEgreso.
     */
    public void setMostrarDetalleEgreso(boolean mostrarDetalleEgreso) {
        this.mostrarDetalleEgreso = mostrarDetalleEgreso;
    }
    
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property tipo_pago.
     * @return Value of property tipo_pago.
     */
    public java.lang.String getTipo_pago() {
        return tipo_pago;
    }
    
    /**
     * Setter for property tipo_pago.
     * @param tipo_pago New value of property tipo_pago.
     */
    public void setTipo_pago(java.lang.String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }
    
    /**
     * Getter for property detalle.
     * @return Value of property detalle.
     */
    public java.util.Vector getDetalle() {
        return detalle;
    }
    
    /**
     * Setter for property detalle.
     * @param detalle New value of property detalle.
     */
    public void setDetalle(java.util.Vector detalle) {
        this.detalle = detalle;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public com.tsp.operation.model.beans.CXP_Doc getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(com.tsp.operation.model.beans.CXP_Doc factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public com.tsp.operation.model.beans.Planilla getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(com.tsp.operation.model.beans.Planilla planilla) {
        this.planilla = planilla;
    }
    
    
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property reimpresion.
     * @return Value of property reimpresion.
     */
    public boolean isReimpresion() {
        return reimpresion;
    }
    
    /**
     * Setter for property reimpresion.
     * @param reimpresion New value of property reimpresion.
     */
    public void setReimpresion(boolean reimpresion) {
        this.reimpresion = reimpresion;
    }
    
    /**
     * Getter for property usuario_impresion.
     * @return Value of property usuario_impresion.
     */
    public java.lang.String getUsuario_impresion() {
        return usuario_impresion;
    }
    
    /**
     * Setter for property usuario_impresion.
     * @param usuario_impresion New value of property usuario_impresion.
     */
    public void setUsuario_impresion(java.lang.String usuario_impresion) {
        this.usuario_impresion = usuario_impresion;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property banco_reemplazo.
     * @return Value of property banco_reemplazo.
     */
    public java.lang.String getBanco_reemplazo() {
        return banco_reemplazo;
    }
    
    /**
     * Setter for property banco_reemplazo.
     * @param banco_reemplazo New value of property banco_reemplazo.
     */
    public void setBanco_reemplazo(java.lang.String banco_reemplazo) {
        this.banco_reemplazo = banco_reemplazo;
    }
    
    /**
     * Getter for property suc_reemplazo.
     * @return Value of property suc_reemplazo.
     */
    public java.lang.String getSuc_reemplazo() {
        return suc_reemplazo;
    }
    
    /**
     * Setter for property suc_reemplazo.
     * @param suc_reemplazo New value of property suc_reemplazo.
     */
    public void setSuc_reemplazo(java.lang.String suc_reemplazo) {
        this.suc_reemplazo = suc_reemplazo;
    }
    
    /**
     * Getter for property cheque_reemplazo.
     * @return Value of property cheque_reemplazo.
     */
    public java.lang.String getCheque_reemplazo() {
        return cheque_reemplazo;
    }
    
    /**
     * Setter for property cheque_reemplazo.
     * @param cheque_reemplazo New value of property cheque_reemplazo.
     */
    public void setCheque_reemplazo(java.lang.String cheque_reemplazo) {
        this.cheque_reemplazo = cheque_reemplazo;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property valor_for.
     * @return Value of property valor_for.
     */
    public double getValor_for() {
        return valor_for;
    }
    
    /**
     * Setter for property valor_for.
     * @param valor_for New value of property valor_for.
     */
    public void setValor_for(double valor_for) {
        this.valor_for = valor_for;
    }
    
}
