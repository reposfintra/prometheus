/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;


import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author maltamiranda
 */
public class ConvenioCxc implements Serializable{
    String reg_status, dstrct, id_convenio,titulo_valor, prefijo_factura,
           cuenta_cxc, hc_cxc, creation_user, user_update, creation_date,
           last_update;
    private boolean genRemesa;//2010-09-28
    private ArrayList<ConvenioCxcFiducias>convenioCxcFiducias;
    private String cuenta_prov_cxc, cuenta_prov_cxp;
   
    public ConvenioCxc() {
        this.reg_status = "";
        this.dstrct = "";
        this.id_convenio = "";
        this.titulo_valor = "";
        this.prefijo_factura = "";
        this.cuenta_cxc = "";
        this.hc_cxc = "";
        this.creation_user = "";
        this.user_update = "";
        this.creation_date = "";
        this.last_update = "";
        this.genRemesa = false;//2010-09-28

    }
    
    public String getCuenta_prov_cxc() {
        return cuenta_prov_cxc;
    }

    public void setCuenta_prov_cxc(String cuenta_prov_cxc) {
        this.cuenta_prov_cxc = cuenta_prov_cxc;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getCuenta_cxc() {
        return cuenta_cxc;
    }

    public void setCuenta_cxc(String cuenta_cxc) {
        this.cuenta_cxc = cuenta_cxc;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getHc_cxc() {
        return hc_cxc;
    }

    public void setHc_cxc(String hc_cxc) {
        this.hc_cxc = hc_cxc;
    }

    public String getId_convenio() {
        return id_convenio;
    }

    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }

       

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getPrefijo_factura() {
        return prefijo_factura;
    }

    public void setPrefijo_factura(String prefijo_factura) {
        this.prefijo_factura = prefijo_factura;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getTitulo_valor() {
        return titulo_valor;
    }

    public void setTitulo_valor(String titulo_valor) {
        this.titulo_valor = titulo_valor;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public boolean isGenRemesa() {
        return genRemesa;//2010-09-28
    }

    public void setGenRemesa(boolean genRemesa) {
        this.genRemesa = genRemesa;//2010-09-28
    }
    public ArrayList<ConvenioCxcFiducias> getConvenioCxcFiducias() {
        return convenioCxcFiducias;
    }

    public void setConvenioCxcFiducias(ArrayList<ConvenioCxcFiducias> convenioCxcFiducias) {
        this.convenioCxcFiducias = convenioCxcFiducias;
    }    
    
    public String getCuenta_prov_cxp() {
        return cuenta_prov_cxp;
    }

    public void setCuenta_prov_cxp(String cuenta_prov_cxp) {
        this.cuenta_prov_cxp = cuenta_prov_cxp;
    }

}