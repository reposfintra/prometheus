/*
 * edicionExcel.java
 *
 * Created on 16 de septiembre de 2006, 09:58 AM
 */

/*
 * Nombre        edicionExcel.java
 * Descripci�n   Permite la modificaci�n de un excel escribiendo sobre este
 * Autor         David Pi�a L�pez
 * Fecha         16 de septiembre de 2006, 09:58 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import jxl.*;
import jxl.write.*;
import jxl.write.WritableFont.*;
import jxl.format.*;
import jxl.biff.*;
import java.io.*;

/**
 *
 * @author  David
 */
public class EdicionExcel {
    
    Workbook wb;
    WritableWorkbook rw;
    File archivo;
    WritableSheet sheetRW;
    
    
    /** Creates a new instance of edicionExcel */
    public EdicionExcel() {
    }
    
    public EdicionExcel(String ruta) {
        try{
            this.archivo = new File( ruta );
        }catch( Exception ex ){
            ////System.out.println( "Error: edicionExcel --> m�todo edicionExcel( String ruta ): " + ex.getMessage() );
        }
    }
    
    public EdicionExcel(File archivo) {
        this.archivo = archivo;
    }
    
    public void setArchivo( File archivo ){
        this.archivo = archivo;
    }
    
    public void abrirArchivo() throws Exception{
        wb = Workbook.getWorkbook( archivo );
    }
    
    public void crearArchivoRW()  throws Exception{
        abrirArchivo();
        rw = Workbook.createWorkbook( archivo, wb );        
    }
    
    public void modificarArchivoRW( int hoja, int columna, int fila, String contenido ){
        try{
            sheetRW = rw.getSheet( hoja );
            Label label = new Label( columna, fila, contenido ); 
            sheetRW.addCell( label ); 
        }catch( Exception ex ){
            ////System.out.println( "Error: edicionExcel --> m�todo modificarArchivoRW( int hoja, int columna, int fila, String contenido ): " + ex.getMessage() );
        }
    }
    
    public String leerArchivoRW( int hoja, int columna, int fila ){
        String contenido = "";
        try{ 
            sheetRW = rw.getSheet( hoja );
            Cell celda = sheetRW.getCell( columna, fila );
            //////System.out.println( "Tipo Celda: "+celda.getType().toString() );
            contenido = celda.getContents();
        }catch( Exception ex ){
            ////System.out.println( "Error: edicionExcel --> m�todo String leerArchivoRW( int hoja, int columna, int fila ): " + ex.getMessage() );
        }
        return contenido;
    }
    
    public void escribirArchivoRW(){
        try{
            rw.write();
        }catch( Exception ex ){
            ////System.out.println( "Error: edicionExcel --> m�todo escribirArchivoRW: " + ex.getMessage() );
        }
        
    }
    
    public void cerrarArchivoRW(){
        try {
            wb.close();
            rw.close();            
        }catch( Exception ex ){
            ////System.out.println( "Error: edicionExcel --> m�todo cerrarArchivoRW: " + ex.getMessage() );
        }
    }
    public int totalFilas( int hoja ){
        int total = 0;
        try {
            sheetRW = rw.getSheet( hoja );
            total = sheetRW.getRows();
        }catch( Exception ex ){
            ////System.out.println( "Error: edicionExcel --> m�todo totalFilas: " + ex.getMessage() );
        }
        return total;
    }
    /*
    public static void main( String args[] ) throws Exception{
         EdicionExcel excel = new EdicionExcel( "C:/Tabla de elementos.XLS" );
         excel.crearArchivoRW();
         excel.modificarArchivoRW( 0, 0, 5, "Hola Mundo" );
         for( int i = 0; i < 50; i++ ){
            ////System.out.println( "Contenido Columna A, Fila "+ (i + 1) + ": " + excel.leerArchivoRW( 0, 0, i ) );
         }
         ////System.out.println( "Cantidad de Filas " + excel.totalFilas( 0 ) );
         excel.escribirArchivoRW();
         excel.cerrarArchivoRW();
    }*/
}
