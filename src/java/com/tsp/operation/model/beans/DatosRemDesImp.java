/*
 * DatosRemDesImp.java
 *
 * Created on 17 de diciembre de 2004, 7:23
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;

/**
 *
 * @author  Mario Fontalvo
 */
public class DatosRemDesImp {
    
    private String Codigo;
    private String Nombre;
    private String CodCiudad;
    private String NomCiudad;
    
    /** Creates a new instance of DatosRemDesImp */
    public DatosRemDesImp() {
    }
    
    public static DatosRemDesImp load(ResultSet rs)throws Exception{
        DatosRemDesImp datos = new DatosRemDesImp();
        datos.setCodigo   (rs.getString(1));
        datos.setNombre   (rs.getString(2));
        datos.setCodCiudad(rs.getString(3));
        datos.setNomCiudad(rs.getString(4));
        return datos;
    }
    
    
    //setter
    public void setCodigo(String valor){
        this.Codigo = valor;
    }
    public void setNombre(String valor){
        this.Nombre = valor;
    }
    public void setCodCiudad(String valor){
        this.CodCiudad = valor;
    }
    public void setNomCiudad(String valor){
        this.NomCiudad = valor;
    }
    
    //getter
    public String getCodigo (){
        return this.Codigo;
    }
    public String getNombre (){
        return this.Nombre;
    }
    public String getCodCiudad (){
        return this.CodCiudad;
    }
    public String getNomCiudad (){
        return this.NomCiudad;
    }
    
}
