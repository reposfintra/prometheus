/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class ImpuestoContrato {


    private String codigo_impuesto;
    private String descripcion;
    private double porcentaje1;



    /** Creates a new instance of Prefactura */
    public ImpuestoContrato() {
    }


    public static ImpuestoContrato load(java.sql.ResultSet rs)throws java.sql.SQLException{

        ImpuestoContrato impuestoContrato = new ImpuestoContrato();

        impuestoContrato.setCodigo_impuesto( rs.getString("codigo_impuesto") );
        impuestoContrato.setDescripcion(rs.getString("descripcion") );
        impuestoContrato.setPorcentaje1( rs.getDouble("porcentaje1") );

        return impuestoContrato;
    }



    /**
     * @return the codigo_impuesto
     */
    public String getCodigo_impuesto() {
        return codigo_impuesto;
    }

    /**
     * @param codigo_impuesto the codigo_impuesto to set
     */
    public void setCodigo_impuesto(String codigo_impuesto) {
        this.codigo_impuesto = codigo_impuesto;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the porcentaje1
     */
    public double getPorcentaje1() {
        return porcentaje1;
    }

    /**
     * @param porcentaje1 the porcentaje1 to set
     */
    public void setPorcentaje1(double porcentaje1) {
        this.porcentaje1 = porcentaje1;
    }




}
