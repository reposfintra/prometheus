/***********************************************************************
 *      Nombre Clase.................   RelacionGrupoSubgrupoPlaca.java
 *      Descripci�n..................   Bean del reporte de tablagen,
 *      Autor........................   Ing. Leonardo Parodi Ponce
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ***********************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;

/**
 *
 * @author  EQUIPO12
 */

public class RelacionGrupoSubgrupoPlaca implements Serializable{
        private String placa;
        private String group;
        private String subgroup;
        private String creation_user;
        private java.util.Date creation_date;
        private String base;
        private String dstrct;
        private java.util.Date last_update;
        private String user_update;
        private String reg_status;
        private String propietario;
        private String descripcionGrupo;
        private String descripcionSubGrupo;
        
        /** Creates a new instance of RelacionGrupoSubgrupoPlaca */
        public RelacionGrupoSubgrupoPlaca() {
        }
        
        /**
         * Obtiene el valor de cada uno de los campos del <code>ResultSet</code>
         * @autor Ing.Leonardo Parody
         * @param rs <code>ResultSet</code> de donde se obtienen los campos
         * @throws SQLException
         * @version 1.0
         */
        public RelacionGrupoSubgrupoPlaca Load(ResultSet rs) throws SQLException{
                RelacionGrupoSubgrupoPlaca relacion = new RelacionGrupoSubgrupoPlaca();
                relacion.setPlaca(rs.getString("placa"));
                relacion.setGroup(rs.getString("wgroup"));
                relacion.setSubgroup(rs.getString("subgroup"));
                relacion.setBase(rs.getString("base"));
                relacion.setCreation_user(rs.getString("creation_user"));
                relacion.setDstrct(rs.getString("dstrct"));
                relacion.setLast_update(rs.getTimestamp("last_update"));
                relacion.setCreation_date(rs.getTimestamp("creation_date"));
                relacion.setReg_status(rs.getString("reg_status"));
                relacion.setUser_update(rs.getString("user_update"));
                String prop = rs.getString("propietario");
                String grupo = rs.getString("descripcionwg");
                String subgrupo = rs.getString("descripcionsg");
                if (prop!=null){
                        relacion.setPropietario(prop);
                }else{
                        relacion.setPropietario(" ");
                }
                if (grupo!=null){
                        relacion.setDescripcionGrupo(grupo);
                }else{
                        relacion.setDescripcionGrupo(" ");
                }
                if (subgrupo!=null){
                        relacion.setDescripcionSubGrupo(subgrupo);
                }else{
                        relacion.setDescripcionSubGrupo(" ");
                }
                return relacion;
        }
        
        /**
         * Setter for property propietario.
         * @param placa New value of property propietario.
         */
        public void setPropietario(String Propietario){
                
                this.propietario = Propietario;
                
        }
        
        /**
         * Setter for property descripcionGrupo.
         * @param placa New value of property descripcionGrupo.
         */
        public void setDescripcionGrupo(String DescripcionGrupo){
                
                this.descripcionGrupo = DescripcionGrupo;
                
        }
        /**
         * Setter for property descripcionGrupo.
         * @param placa New value of property descripcionGrupo.
         */
        public void setDescripcionSubGrupo(String DescripcionSubGrupo){
                
                this.descripcionSubGrupo = DescripcionSubGrupo;
                
        }
        /**
         * Setter for property placa.
         * @param placa New value of property placa.
         */
        public void setPlaca(String Placa){
                
                this.placa = Placa;
                
        }
        /**
         * Setter for property group.
         * @param group New value of property group.
         */
        public void setGroup(String Group){
                
                this.group = Group;
                
        }
        /**
         * Setter for property subgroup.
         * @param subgroup New value of property subgroup.
         */
        public void setSubgroup(String Subgroup){
                
                this.subgroup = Subgroup;
                
        }
        
        /**
         * Setter for property creation_date.
         * @param creation_date New value of property creation_date.
         */
        public void setCreation_date(java.util.Date Creation_date){
                
                this.creation_date = Creation_date;
                
        }
        
        /**
         * Setter for property creation_user.
         * @param creation_user New value of property creation_user.
         */
        public void setCreation_user(String Creation_user){
                
                this.creation_user = Creation_user;
                
        }
        
        /**
         * Getter for property propietario.
         * @return value of property propietario.
         */
        public String getPropietario(){
                
                return this.propietario;
                
        }
        /**
         * Getter for property placa.
         * @return value of property placa.
         */
        public String getPlaca(){
                
                return this.placa;
                
        }
        /**
         * Getter for property descripcionSubGrupo.
         * @return value of property descripcionSubGrupo.
         */
        public String getDescripcionSubGrupo(){
                
                return this.descripcionSubGrupo;
                
        }
        
        /**
         * Getter for property descripcionGrupo.
         * @return value of property descripcionGrupo.
         */
        public String getDescripcionGrupo(){
                
                return this.descripcionGrupo;
                
        }
        
        /**
         * Getter for property group.
         * @return value of property group.
         */
        public String getGroup(){
                
                return this.group;
                
        }
        
        /**
         * Getter for property subgroup.
         * @return value of property subgroup.
         */
        public String getSubgroup(){
                
                return this.subgroup;
                
        }
        
        /**
         * Getter for property creation_date.
         * @return value of property creation_date.
         */
        public java.util.Date getCreation_date(){
                
                return this.creation_date;
                
        }
        
        /**
         * Getter for property creation_user.
         * @return value of property creation_user.
         */
        public String getCreation_user(){
                
                return this.creation_user;
                
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }
        
        /**
         * Getter for property dstrct.
         * @return Value of property dstrct.
         */
        public java.lang.String getDstrct() {
                return dstrct;
        }
        
        /**
         * Setter for property dstrct.
         * @param dstrct New value of property dstrct.
         */
        public void setDstrct(java.lang.String dstrct) {
                this.dstrct = dstrct;
        }
        
        /**
         * Getter for property last_update.
         * @return Value of property last_update.
         */
        public java.util.Date getLast_update() {
                return last_update;
        }
        
        /**
         * Setter for property last_update.
         * @param last_update New value of property last_update.
         */
        public void setLast_update(java.util.Date Last_update) {
                this.last_update = Last_update;
        }
        
        /**
         * Getter for property reg_status.
         * @return Value of property reg_status.
         */
        public java.lang.String getReg_status() {
                return reg_status;
        }
        
        /**
         * Setter for property reg_status.
         * @param reg_status New value of property reg_status.
         */
        public void setReg_status(java.lang.String reg_status) {
                this.reg_status = reg_status;
        }
        
        /**
         * Getter for property user_update.
         * @return Value of property user_update.
         */
        public java.lang.String getUser_update() {
                return user_update;
        }
        
        /**
         * Setter for property user_update.
         * @param user_update New value of property user_update.
         */
        public void setUser_update(java.lang.String user_update) {
                this.user_update = user_update;
        }
        
}
