 /***************************************
    * Nombre Clase ............. ResumenPrestamo.java
    * Descripci�n  .. . . . . .  Resumen de los prestamos
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  04/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.beans;



import java.util.*;


public class ResumenPrestamo {
    
    
    
    private int     id;
    
    private String  distrito;
    private String  tercero;
    private String  nit;
    private String  nombre;
    private double  valor;
    private double  intereses;
    private double  valorDesc;
    private double  interesesDesc;
    private double  vlrMigradoMims;
    private double  vlrRegistradoMims;
    private double  vlrDescontadoProp;
    private double  vlrPagadoFintra;
    private double  vlrSaldo;
    
    
    
    private List   Amortizaciones;
    
    
    public ResumenPrestamo() {
        distrito   = "";
        tercero    = "";
        nit        = "";
        nombre     = "";
        Amortizaciones = new LinkedList();
    }
    
    
    
    
    
    
    
    public double saldoNoMigrado(){
        return this.valor - this.vlrMigradoMims;
    }
    
    public double saldoDescontado(){
        return this.valor - getValorDesc();
    }
    
    
    public double saldoPagadoTercero(){
        return this.valor - getValorDesc();
    }
    
    
    
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public int getId() {
        return id;
    }    
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }    
    
    /**
     * Getter for property intereses.
     * @return Value of property intereses.
     */
    public double getIntereses() {
        return intereses;
    }    
    
    /**
     * Setter for property intereses.
     * @param intereses New value of property intereses.
     */
    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }    
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
   
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property vlrSaldo.
     * @return Value of property vlrSaldo.
     */
    public double getVlrSaldo() {
        return vlrSaldo;
    }
    
    /**
     * Setter for property vlrSaldo.
     * @param vlrSaldo New value of property vlrSaldo.
     */
    public void setVlrSaldo(double vlrSaldo) {
        this.vlrSaldo = vlrSaldo;
    }
    
    /**
     * Getter for property vlrDescontadoProp.
     * @return Value of property vlrDescontadoProp.
     */
    public double getVlrDescontadoProp() {
        return vlrDescontadoProp;
    }
    
    /**
     * Setter for property vlrDescontadoProp.
     * @param vlrDescontadoProp New value of property vlrDescontadoProp.
     */
    public void setVlrDescontadoProp(double vlrDescontadoProp) {
        this.vlrDescontadoProp = vlrDescontadoProp;
    }
    
    /**
     * Getter for property vlrMigradoMims.
     * @return Value of property vlrMigradoMims.
     */
    public double getVlrMigradoMims() {
        return vlrMigradoMims;
    }
    
    /**
     * Setter for property vlrMigradoMims.
     * @param vlrMigradoMims New value of property vlrMigradoMims.
     */
    public void setVlrMigradoMims(double vlrMigradoMims) {
        this.vlrMigradoMims = vlrMigradoMims;
    }
    
    /**
     * Getter for property vlrPagadoFintra.
     * @return Value of property vlrPagadoFintra.
     */
    public double getVlrPagadoFintra() {
        return vlrPagadoFintra;
    }
    
    /**
     * Setter for property vlrPagadoFintra.
     * @param vlrPagadoFintra New value of property vlrPagadoFintra.
     */
    public void setVlrPagadoFintra(double vlrPagadoFintra) {
        this.vlrPagadoFintra = vlrPagadoFintra;
    }
    
    /**
     * Getter for property vlrRegistradoMims.
     * @return Value of property vlrRegistradoMims.
     */
    public double getVlrRegistradoMims() {
        return vlrRegistradoMims;
    }
    
    /**
     * Setter for property vlrRegistradoMims.
     * @param vlrRegistradoMims New value of property vlrRegistradoMims.
     */
    public void setVlrRegistradoMims(double vlrRegistradoMims) {
        this.vlrRegistradoMims = vlrRegistradoMims;
    }
    
    
    
    /**
     * Getter for property interesesDesc.
     * @return Value of property interesesDesc.
     */
    public double getInteresesDesc() {
        return interesesDesc;
    }
    
    /**
     * Setter for property interesesDesc.
     * @param interesesDesc New value of property interesesDesc.
     */
    public void setInteresesDesc(double interesesDesc) {
        this.interesesDesc = interesesDesc;
    }
    
    /**
     * Getter for property valorDesc.
     * @return Value of property valorDesc.
     */
    public double getValorDesc() {
        return valorDesc;
    }
    
    /**
     * Setter for property valorDesc.
     * @param valorDesc New value of property valorDesc.
     */
    public void setValorDesc(double valorDesc) {
        this.valorDesc = valorDesc;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }
    
    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }
    
    
    
    
    
    
    /**
     * Getter for property Amortizaciones.
     * @return Value of property Amortizaciones.
     */
    public java.util.List getAmortizaciones() {
        return Amortizaciones;
    }
    
    /**
     * Setter for property Amortizaciones.
     * @param Amortizaciones New value of property Amortizaciones.
     */
    public void setAmortizaciones(java.util.List Amortizaciones) {
        this.Amortizaciones = Amortizaciones;
    }
    
}
