package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * <br/>
 * 25/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Alerta {

    private Timestamp colocacion;
    private Timestamp vencimiento;
    private Timestamp modificacion;
    private String codigo;
    private String texto;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    protected String codigoFuente;
    private String nitEmpresa;
    private String fuente;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of codigoFuente
     *
     * @return the value of codigoFuente
     */
    public String getCodigoFuente() {
        return codigoFuente;
    }

    /**
     * Set the value of codigoFuente
     *
     * @param codigoFuente new value of codigoFuente
     */
    public void setCodigoFuente(String codigoFuente) {
        this.codigoFuente = codigoFuente;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }
    
    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }
    
    /**
     * Get the value of texto
     *
     * @return the value of texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * Set the value of texto
     *
     * @param texto new value of texto
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     * Get the value of codigo
     *
     * @return the value of codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Set the value of codigo
     *
     * @param codigo new value of codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Get the value of modificacion
     *
     * @return the value of modificacion
     */
    public Timestamp getModificacion() {
        return modificacion;
    }

    /**
     * Set the value of modificacion
     *
     * @param modificacion new value of modificacion
     */
    public void setModificacion(Timestamp modificacion) {
        this.modificacion = modificacion;
    }

    /**
     * Get the value of vencimiento
     *
     * @return the value of vencimiento
     */
    public Timestamp getVencimiento() {
        return vencimiento;
    }

    /**
     * Set the value of vencimiento
     *
     * @param vencimiento new value of vencimiento
     */
    public void setVencimiento(Timestamp vencimiento) {
        this.vencimiento = vencimiento;
    }

    /**
     * Get the value of colocacion
     *
     * @return the value of colocacion
     */
    public Timestamp getColocacion() {
        return colocacion;
    }

    /**
     * Set the value of colocacion
     *
     * @param colocacion new value of colocacion
     */
    public void setColocacion(Timestamp colocacion) {
        this.colocacion = colocacion;
    }

    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }
    
    
    
}
