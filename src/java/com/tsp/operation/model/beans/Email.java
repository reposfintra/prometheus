/*
 * SendMail.java
 *
 * Created on 27 de diciembre de 2004, 03:55 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;
import com.tsp.exceptions.EmailSendingException;

/**
 * Javabean utilizado para representar un correo electr�nico.
 * @author  AMENDEZ
 */
public class Email implements Serializable
{
  private String recstatus;
  private String emailcode;
  private String emailfrom;
  private String emailto;
  private String emailcopyto;
  private String emailsubject;
  private String emailbody;
  private String lastupdat;
  private String senderName;
  private String [] emailtoarray;
  private String [] emailcopytoarray;

  private String remarks;//FVILLACOB 24.11.05

  /** Crea una nueva instancia de esta clase */
  public Email()
  {
    this.recstatus = "";
    this.emailcode = "";
    this.emailfrom = "";
    this.emailto = "";
    this.emailcopyto = "";
    this.emailsubject = "";
    this.emailbody = "";
    this.lastupdat = "";
    this.senderName = "";
    this.emailtoarray = null;
    this.emailcopytoarray = null;
    this.remarks          = " ";//FVILLACOB 24.11.05
  }

  /**
   * Getter for property emailbody.
   * @return Value of property emailbody.
   */
  public String getEmailbody() {
    return emailbody;
  }
  
  /**
   * Setter for property emailbody.
   * @param emailbody New value of property emailbody.
   */
  public void setEmailbody(String emailbody) {
    this.emailbody = emailbody;
  }
  
  /**
   * Getter for property emailcode.
   * @return Value of property emailcode.
   */
  public String getEmailcode() {
    return emailcode;
  }

  /**
   * Setter for property emailcode.
   * @param emailcode New value of property emailcode.
   */
  public void setEmailcode(String emailcode) {
    this.emailcode = emailcode;
  }
  
  /**
   * Getter for property emailfrom.
   * @return Value of property emailfrom.
   */
  public String getEmailfrom() {
    return emailfrom;
  }
  
  /**
   * Setter for property emailfrom.
   * @param emailfrom New value of property emailfrom.
   */
  public void setEmailfrom(String emailfrom) {
    this.emailfrom = emailfrom;
  }
  
  /**
   * Getter for property emailsubject.
   * @return Value of property emailsubject.
   */
  public String getEmailsubject() {
    return emailsubject;
  }
  
  /**
   * Setter for property emailsubject.
   * @param emailsubject New value of property emailsubject.
   */
  public void setEmailsubject(String emailsubject) {
    this.emailsubject = emailsubject;
  }
  
  /**
   * Getter for property emailto.
   * @return Value of property emailto.
   */
  public String getEmailto() {
    return emailto;
  }
  
  /**
   * Indexed getter for property emailto.
   * @return An email.
   */
  public String getEmailto(int idx) {
    return this.emailtoarray[idx];
  }
  
  /**
   * Numero de destinos a copiar.
   * @return An email.
   */
  public int getEmailToLength() {
    return this.emailtoarray.length;
  }
  
  /**
   * Setter for property emailto.
   * @param emailto New value of property emailto.
   */
  public void setEmailto(String emailto) {
    this.emailto = emailto;
    this.emailtoarray = this.emailto.split(";");
    for( int idx = 0; idx < emailtoarray.length; idx++ )
      this.emailtoarray[idx] = this.emailtoarray[idx].trim();
  }
  
  /**
   * Getter for property emailcopyto.
   * @return Value of property emailcopyto.
   */
  public String getEmailcopyto() {
    return emailcopyto;
  }
  
  /**
   * Indexed getter for property emailcopyto.
   * @return An email.
   */
  public String getEmailcopyto(int idx) {
    return this.emailcopytoarray[idx];
  }
  
  /**
   * Numero de destinos a copiar.
   * @return An email.
   */
  public int getEmailCopytoLength() {
    return this.emailcopytoarray.length;
  }
  
  /**
   * Setter for property emailcopyto.
   * @param emailcopyto New value of property emailcopyto.
   */
  public void setEmailcopyto(String emailcopyto) {
    this.emailcopyto = emailcopyto;
    this.emailcopytoarray = this.emailcopyto.split(";");
    for( int idx = 0; idx < emailcopytoarray.length; idx++ )
      this.emailcopytoarray[idx] = this.emailcopytoarray[idx].trim();
  }
  
  /**
   * Getter for property lastupdat.
   * @return Value of property lastupdat.
   */
  public String getLastupdat() {
    return lastupdat;
  }
  
  /**
   * Setter for property lastupdat.
   * @param lastupdat New value of property lastupdat.
   */
  public void setLastupdat(String lastupdat) {
    this.lastupdat = lastupdat;
  }
  
  /**
   * Getter for property recstatus.
   * @return Value of property recstatus.
   */
  public String getRecstatus() {
    return recstatus;
  }
  
  /**
   * Setter for property recstatus.
   * @param recstatus New value of property recstatus.
   */
  public void setRecstatus(String recstatus) {
    this.recstatus = recstatus;
  }
  
  /**
   * Getter for property senderName.
   * @return Value of property senderName.
   */
  public String getSenderName() {
    return this.senderName;
  }
  
  /**
   * Setter for property senderName.
   * @param senderName New value of property senderName.
   */
  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }
  
  /**
   * Env�a el correo electr�nico.
   * @param smtpHost Nombre o Direcci�n IP del servidor de correo SMTP.
   * @param email Objeto con los datos del correo a ser enviado.
   * @throws EmailSendingException Si ocurre un error durante el env�o de correo.
   */
  public static void send(String smtpHost, Email email)
  throws EmailSendingException
  {
    String emailFrom    = email.getEmailfrom().trim();
    String senderName   = email.getSenderName().trim();
    String emailSubject = email.getEmailsubject().trim();
    String emailBody    = email.getEmailbody().trim();
    Transport trans = null;
    try {
      Session session = Session.getInstance(System.getProperties(), null);
      MimeMessage msg = new MimeMessage(session);
      
      // Remitente
      if(!senderName.equals(""))
        msg.setFrom( new InternetAddress(emailFrom, senderName) );
      else
        msg.setFrom( new InternetAddress(emailFrom) );
      
      // Destinatarios
      if (!email.getEmailto(0).equals("")) {
        for ( int i = 0; i < email.getEmailToLength(); i++) {
          msg.addRecipient(
            Message.RecipientType.TO, new InternetAddress(email.getEmailto(i))
          );
        }
      }

      // Copiar a
      if (!email.getEmailcopyto(0).equals("")) {
        for ( int i = 0; i < email.getEmailCopytoLength(); i++) {
          msg.addRecipient(
            Message.RecipientType.CC, new InternetAddress(email.getEmailcopyto(i))
          );
        }
      }
      msg.setSubject( emailSubject );
      msg.setText( emailBody );
      
      // Conectarse al servidor de correo que efectuar� el env�o.
      trans = session.getTransport("smtp");
      trans.connect(smtpHost, "nparejo", "");
      
      // Asignar la fecha de env�o.
      msg.setSentDate(new java.util.Date());
      
      // Enviar notificaci�n.
      trans.sendMessage(msg, msg.getAllRecipients());
    }catch(Exception E){
      throw new EmailSendingException(E.getMessage());
    }finally{
      try {
        if( trans != null ) trans.close();
      }catch (Exception ignore){}
    }
  }

 public void setRemarks(String remarks){
      this.remarks = remarks;
  }

  public String getRemarks(){
      return remarks;
  }

}
