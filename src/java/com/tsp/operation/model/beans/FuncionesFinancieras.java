/***************************************
* Nombre Clase ............. FuncionesFinancieras.java
* Descripci�n  .. . . . . .  Funciones de Calculos de Amortizaciones
* Autor  . . . . . . . . . . MARIO FONTALVO
* Fecha . . . . . . . . . .  09/02/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/


/*
 * test.java
 *
 * Created on 9 de febrero de 2006, 09:40 AM
 */

package com.tsp.operation.model.beans;


import java.util.*;
import java.text.*;

/**
 *
 * @author  Mario Fontalvo Solano
 */
public class FuncionesFinancieras {
    
    // tipos de prestamos
    public static final int TESORERIA  = 0;
    public static final int CUOTA_FIJA = 1;
    public static final int INTERESES_ANTICIPADOS = 2;
    
    // frecuencia de prestamos
    public static final int ANUAL     = 0 ;
    public static final int MENSUAL   = 30;
    public static final int QUINCENAL = 15;
    public static final int SEMANAL   = 7 ;
    
    private static double interes_acumulado = 0;
    
    
    /** Creates a new instance of test */
    public FuncionesFinancieras() {
    }
    
    
    
    
    
    
    public static List getAmortizacion(Prestamo pt){
         FuncionesFinancieras x = new FuncionesFinancieras();
         List amortizaciones    = new LinkedList();
         amortizaciones         = x.auto(pt);
         
         pt.setAmortizacion(amortizaciones);
         if (pt.getTipoPrestamo().equals("IA") || pt.getTipoPrestamo().equals("CV")){
             modificarPrestamoInteresAnticipadoPorFechas(pt);
         }
         
         return pt.getAmortizacion();
    }
    
    
    
    
    public List auto (Prestamo pt){
        List amortizaciones = new LinkedList();
        if (pt!=null){
            
            if (pt.getTipoPrestamo().equals("TE") ){
                amortizaciones = 
                    this.prestamosTesoreria(
                       pt.getFrecuencias(), 
                       pt.getMonto(), 
                       pt.getTasa(), 
                       pt.getCuotas(),  
                       new Date(pt.getFechaInicialCobro().replaceAll("-","/"))
                    );
            }
            else if (pt.getTipoPrestamo().equals("IA") ){
                amortizaciones = 
                    this.prestamosInteresAnticipado(
                       pt.getFrecuencias(), 
                       pt.getMonto(), 
                       pt.getTasa(), 
                       pt.getCuotas(),  
                       new Date(pt.getFechaInicialCobro().replaceAll("-","/"))
                    );
            }
            else if (pt.getTipoPrestamo().equals("CV") ){
                amortizaciones = 
                    this.prestamosCuotasVaribles(
                       pt.getFrecuencias(), 
                       pt.getMonto(), 
                       pt.getTasa(), 
                       pt.getCuotas(),  
                       new Date(pt.getFechaInicialCobro().replaceAll("-","/"))
                    );
            }  
            else if (pt.getTipoPrestamo().equals("FI") ){
                amortizaciones = 
                    this.prestamosCuotaFija(
                       pt.getFrecuencias(), 
                       pt.getMonto(), 
                       pt.getTasa(), 
                       pt.getCuotas(),  
                       new Date(pt.getFechaInicialCobro().replaceAll("-","/"))
                    );
            }  
            if(pt.getTipoPrestamo().equals("AP")){
                pt.setIntereses(0);
            }else{
                pt.setIntereses( interes_acumulado );
            }          
        }
        return amortizaciones;
            
    }
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Procedimiento que calcula las amortizaciones para los prestamos de Interes Anticipados
     * @autor mfontalvo
     * @param frecuencia Tipos: MENSUAL (1), QUINCENAL (2), SEMANAL(3)
     * @param valor Valor del Prestamo
     * @param tasa Tasa de Interes del prestamo
     * @param cuotas Numero de cuotas del prestamo
     * @param fechaInicial Fecha de cobro inicial
     * @return Lista de amortizaciones
     */    
    public List prestamosTesoreria(int frecuencia, double valor, double tasa, int cuotas, Date fechaInicial) {
        
        List   lista  = new LinkedList();
        double factor = this.getFactor(frecuencia);
        double nuevaTasa = tasa * factor ;
        Date   fecha = fechaInicial;
        double saldo = valor;
        interes_acumulado = 0;
        
        for (int i=1; i<=cuotas; i++){
            Amortizacion amort = new Amortizacion();
            amort.setCuota(i);
            
            // fecha de la amortizacion
            fecha = (i==1?fecha:this.Incrementar(frecuencia, fecha));
            amort.setFecha(new Date(fecha.getTime()));
            
            // calculo del monto
            double monto   = saldo;
            amort.setMonto(monto);
            
            //calculo del capital
            double capital = (i==cuotas?saldo:0);
            amort.setCapital(capital);
            
            // calculo del interes
            double interes = monto * (nuevaTasa / 100) ;
            amort.setInteres(interes);
            interes_acumulado += interes;
            
            // calculo total a pagar
            double total  = capital + interes;
            amort.setTotalAPagar(total);
            
            // calculo del nuevo saldo
            saldo = monto - capital ;
            amort.setSaldo(saldo);
            
            lista.add(amort);
            //////System.out.println(i + "\t" + fecha + "\t" + customFormat(monto) + "\t" + customFormat(capital) + "\t" + customFormat(interes) + "\t" + customFormat(total) + "\t" + customFormat(saldo));
        }
        return lista;
    }
    
    
    /**
     * Procedimiento que calcula las amortizaciones para los prestamos de Cuotas Fijas
     * @autor mfontalvo
     * @param frecuencia Tipos: MENSUAL (1), QUINCENAL (2), SEMANAL(3)
     * @param valor Valor del Prestamo
     * @param tasa Tasa de Interes del prestamo
     * @param cuotas Numero de cuotas del prestamo
     * @param fechaInicial Fecha de cobro inicial
     * @return Lista de amortizaciones
     */     
    public List prestamosCuotasVaribles(int frecuencia, double valor, double tasa, int cuotas, Date fechaInicial) {
        
        List   lista  = new LinkedList();
        double factor = this.getFactor(frecuencia);
        double nuevaTasa = tasa * factor;
        Date   fecha = fechaInicial;
        double saldo = valor;
        interes_acumulado = 0;
        
        for (int i=1; i<=cuotas; i++){
            Amortizacion amort = new Amortizacion();
            amort.setCuota(i);
            
            // fecha de la amortizacion
            fecha = (i==1?fecha:this.Incrementar(frecuencia, fecha));
            amort.setFecha(new Date(fecha.getTime()));
            
            // calculo del monto
            double monto   = saldo;
            amort.setMonto(monto);
            
            //calculo del capital
            double capital = valor / cuotas;
            amort.setCapital(capital);
            
            // calculo del interes
            double interes = monto * (nuevaTasa / 100);
            amort.setInteres(interes);
            interes_acumulado += interes;
            
            // calculo total a pagar
            double total  = capital + interes;
            amort.setTotalAPagar(total);
            
            // calculo del nuevo saldo
            saldo = monto - capital ;
            amort.setSaldo(saldo);
            
            lista.add(amort);
            //////System.out.println(i + "\t" + fecha + "\t" + customFormat(monto) + "\t" + customFormat(capital) + "\t" + customFormat(interes) + "\t" + customFormat(total) + "\t" + customFormat(saldo));
        }
        return lista;
        
    }
    
    

    /**
     * Procedimiento que calcula las amortizaciones para los prestamos de Cuotas Fijas
     * @autor mfontalvo
     * @param frecuencia Tipos: MENSUAL (1), QUINCENAL (2), SEMANAL(3)
     * @param valor Valor del Prestamo
     * @param tasa Tasa de Interes del prestamo
     * @param cuotas Numero de cuotas del prestamo
     * @param fechaInicial Fecha de cobro inicial
     * @return Lista de amortizaciones
     */     
    public List prestamosCuotaFija(int frecuencia, double valor, double tasa, int cuotas, Date fechaInicial) {
        
        List   lista  = new LinkedList();
        double factor = this.getFactor(frecuencia);
        
        Date   fecha = fechaInicial;
        double saldo = valor;
        double nuevaTasa = tasa * factor;
        double valor_cuota = this.cuotaFija(valor, nuevaTasa , cuotas);
        interes_acumulado = 0;
        
        for (int i=1; i<=cuotas; i++){
            Amortizacion amort = new Amortizacion();
            amort.setCuota(i);
            
            // fecha de la amortizacion
            fecha = (i==1?fecha:this.Incrementar(frecuencia, fecha));
            amort.setFecha(new Date(fecha.getTime()));
            
            // calculo del monto
            double monto   = saldo;
            amort.setMonto(monto);
            
            // calculo del interes
            double interes =  monto * (nuevaTasa/100);
            amort.setInteres(interes);
            interes_acumulado += interes;

            
            //calculo del capital
            double capital = valor_cuota -  interes;
            amort.setCapital(capital);
            
            
            // calculo total a pagar
            double total  = capital + interes;
            amort.setTotalAPagar(valor_cuota);
            
            // calculo del nuevo saldo
            saldo = monto - capital ;
            amort.setSaldo(saldo);
            
            lista.add(amort);
            //////System.out.println(i + "\t" + fecha + "\t" + customFormat(monto) + "\t" + customFormat(capital) + "\t" + customFormat(interes) + "\t" + customFormat(total) + "\t" + customFormat(saldo));
        }
        return lista;
        
    }    
    
    /**
     * Procedimiento que calcula las amortizaciones para los prestamos de Interes Anticipado
     * @autor mfontalvo
     * @param frecuencia Tipos: MENSUAL (1), QUINCENAL (2), SEMANAL(3)
     * @param valor Valor del Prestamo
     * @param tasa Tasa de Interes del prestamo
     * @param cuotas Numero de cuotas del prestamo
     * @param fechaInicial Fecha de cobro inicial
     * @return Lista de amortizaciones
     */     
    public List prestamosInteresAnticipado(int frecuencia, double valor, double tasa, int cuotas, Date fechaInicial) {
        
        List    lista = new LinkedList();
        double factor = this.getFactor(frecuencia);
        double nuevaTasa = tasa * factor;
        Date   fecha = fechaInicial;
        interes_acumulado = 0;
        double saldo = valor;
        
        for (int i=1; i<=cuotas; i++){
            Amortizacion amort = new Amortizacion();
            amort.setCuota(i);
            
            // fecha de la amortizacion
            fecha = (i==1?fecha:this.Incrementar(frecuencia, fecha));
            amort.setFecha(new Date(fecha.getTime()));
            
            // calculo del monto
            double monto   = saldo;
            amort.setMonto(monto);
            
            //calculo del capital
            double capital = valor / cuotas;
            amort.setCapital(capital);
            
            // calculo del interes
            double interes = (0);
            amort.setInteres(interes);
            interes_acumulado += monto * (nuevaTasa/100);
            
            // calculo total a pagar
            double total  = capital + interes;
            amort.setTotalAPagar(total);
            
            // calculo del nuevo saldo
            saldo = monto - capital ;
            amort.setSaldo(saldo);
            
            lista.add(amort);
            //////System.out.println(i + "\t" + fecha + "\t" + customFormat(monto) + "\t" + customFormat(capital) + "\t" + customFormat(interes) + "\t" + customFormat(total) + "\t" + customFormat(saldo));
        }
        return lista;
        
    }
    
    
    
    
    private double cuotaFija(double capital, double interes, int periodos){
        double cuota = capital *  (interes/100) / 
                       ( 1 - ( 1 / pow(1+(interes/100), periodos) ));
        return cuota;
    }
    
    private double pow(double base, int exp){
        double result = 1;
        for (int i=0; i<exp; i++, result *= base );
        return (exp==0? 1: result);
    }
    
    
    private static double getFactor(int frecuencia){
        double factor = 0;
        switch (frecuencia){
            case MENSUAL  : factor = 1   ;  break;
            case QUINCENAL: factor = 0.5 ;  break;
            case SEMANAL  : factor = 7/(double) 30;  break;
        }
        ////System.out.println("FACTOR : " + factor);
        return factor;
    }
    
    
    private static Date Incrementar(int frecuencia, Date fecha){
        switch (frecuencia){
            case ANUAL    : fecha.setYear ( fecha.getYear()  + 1 );  break;
            case MENSUAL  : fecha.setMonth( fecha.getMonth() + 1 );  break;
            case QUINCENAL: fecha.setDate ( fecha.getDate()  + 15);  break;
            case SEMANAL  : fecha.setDate ( fecha.getDate()  + 7 );  break;
        }
        return fecha;
    }
    
    
    /**
     * Formato de Numero con decimales
     * @autor mfontalvo.
     * @param value valor del formato
     * @return valor formateado
     */    
    public static String customFormat(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###");
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        return df.format(value);
    }
    
    /**
     * Getter for property interes_acumulado.
     * @return Value of property interes_acumulado.
     */
    public static double getInteres_acumulado() {
        return interes_acumulado;
    }  
    
    
    
    public static void modificarPrestamoInteresAnticipadoPorFechas(Prestamo pt){
        int    cuotaAjuste     = -1;
        double interes_cobrado = 0;
        double factor          = getFactor(pt.getFrecuencias());
        double nuevaTasa       = pt.getTasa() * factor;
        interes_acumulado      = 0;
        
        List lista = pt.getAmortizacion();        
        if (lista!=null && !lista.isEmpty()){
            Date fechaAnterior = new Date(pt.getFechaEntregaDinero().replaceAll("-","/") );
            for (int i=0; i<lista.size(); i++){
                Amortizacion am    = (Amortizacion) lista.get(i);
                int diferencia     = (int) ((am.getFecha().getTime()-fechaAnterior.getTime()) /86400000);
                double porcentaje  = diferencia / (double) ( pt.getFrecuencias() );
                double interes     = ((am.getMonto() * (nuevaTasa/100) ) * porcentaje);
                fechaAnterior      = am.getFecha();
                interes_cobrado   += am.getInteres();
                interes_acumulado += interes;
                
                ////System.out.println("Interes cuota "+ i +" : " + am.getInteres() );
                
                // bloque para determinar el proximo interes pendiente
                if ( am.getEstadoDescuento().equals("") && am.getEstadoPagoTercero().equals("") &&
                     am.getFechaMigracion().equals("0099-01-01 00:00:00") && cuotaAjuste==-1) {
                    cuotaAjuste = i;
                    ////System.out.println("Cuota a generar variaciones : " + (cuotaAjuste + 1 ) );
                }
                if (cuotaAjuste!=-1 && pt.getTipoPrestamo().equals("CV") ){
                    am.setInteres(interes);
                    am.setTotalAPagar( am.getInteres() + am.getCapital() );
                }
                    
            }
            
            ////System.out.println("Interes Actual   : " + pt.getIntereses() );
            ////System.out.println("Nuevos Intereses : " + interes_acumulado );
            ////System.out.println("Interes Cobrados : " + interes_cobrado   );

            
            if (cuotaAjuste!=-1 && pt.getAprobado().equalsIgnoreCase("S") && pt.getTipoPrestamo().equalsIgnoreCase("IA")){
                Amortizacion am   = (Amortizacion) lista.get(cuotaAjuste);
                am.setInteres    ( am.getInteres() + interes_acumulado - pt.getIntereses() - interes_cobrado );
                am.setTotalAPagar( am.getCapital() + am.getInteres() ); 
            }
            
            
            if (pt.getAprobado().equalsIgnoreCase("N") || pt.getTipoPrestamo().equalsIgnoreCase("CV"))
                pt.setIntereses( interes_acumulado );
        }
    }
    
    
    public static void modificarPrestamoInteresAnticipadoPorCuotas(Prestamo pt){
        int    cuotaAjuste     = -1;
        int    numCuotas       = 1;
        double interes_cobrado = 0;
        double nuevoMonto      = pt.getMonto();
        double factor          = getFactor(pt.getFrecuencias());
        double nuevaTasa       = pt.getTasa() * factor;        
        interes_acumulado      = 0;
        List   lista           = pt.getAmortizacion();
        
        if (lista!=null && !lista.isEmpty()){
            Date fechaAnterior = new Date(pt.getFechaEntregaDinero().replaceAll("-","/") );
            for (int i=0; i<lista.size(); i++){
                Amortizacion am   = (Amortizacion) lista.get(i);
                
                // bloque para determinar el proximo interes pendiente
                if ( am.getEstadoDescuento().equals("") && am.getEstadoPagoTercero().equals("") &&
                     am.getFechaMigracion().equals("0099-01-01 00:00:00") && cuotaAjuste==-1) {
                    
                    cuotaAjuste = i;
                    numCuotas   = pt.getCuotas() - cuotaAjuste ;
                    if (cuotaAjuste != 0 ) nuevoMonto = am.getMonto();
                    
                    ////System.out.println("**************************************************");
                    ////System.out.println("Numero de cuotas nuevas:  " + numCuotas);
                    ////System.out.println("Cuota a generar variaciones : " + (cuotaAjuste + 1 ) );
                    ////System.out.println("Nuevo Monto : " + nuevoMonto );
                    ////System.out.println("**************************************************");
                    break;
                }
                
                int    diferencia  = (int) ((am.getFecha().getTime()-fechaAnterior.getTime()) /86400000);
                double porcentaje  = diferencia / (double) (pt.getFrecuencias() );
                interes_acumulado += ((am.getMonto() * (pt.getTasa()/100)) * porcentaje);
                fechaAnterior      = am.getFecha();
                interes_cobrado   += am.getInteres();
                
            }
            
            
            ////System.out.println("** intereses hasta la cuota #"+ (cuotaAjuste) +"  **************");
            ////System.out.println("Interes Actual   : " + pt.getIntereses() );
            ////System.out.println("Nuevos Intereses : " + interes_acumulado );
            ////System.out.println("Interes Cobrados : " + interes_cobrado   );
            ////System.out.println("*******************************************");
            ////System.out.println("Ultima fecha     : " + Amortizacion.fmt.format(fechaAnterior));
            
            
            // eliminamos el resto de posiciones 
            List tmp = new LinkedList(lista.subList(cuotaAjuste, lista.size()));
            lista.removeAll( tmp );
            Date cobroInicial = new Date ( pt.getFechaInicialCobro().replaceAll("-","/"  )  );
            Date proximoCobro = new Date (  cuotaAjuste == 0 ? cobroInicial.getTime() :fechaAnterior.getTime() );
            double saldo = nuevoMonto;
            double interes_pendiente = interes_cobrado;
            for (int i = 0; i < numCuotas ; i++){
                Amortizacion am = new Amortizacion();
                proximoCobro  = (i==0 && cuotaAjuste == 0 ? proximoCobro : Incrementar(pt.getFrecuencias(), proximoCobro));
                am.setFecha   ( new Date( proximoCobro.getTime() ) );
                int    diferencia  = (int) ((am.getFecha().getTime()-fechaAnterior.getTime()) /86400000);
                double porcentaje  = diferencia / (double) ( pt.getFrecuencias());                
                
                ////System.out.println("Diferencia  :  " + diferencia );
                am.setSeleccionada ( true);
                am.setCuota   ( i + cuotaAjuste + 1 );
                am.setItem    ( String.valueOf( am.getCuota()) );
                am.setMonto   ( saldo );
                am.setCapital ( nuevoMonto / (double) numCuotas );

                // INTERES
                double interes = am.getMonto() * (nuevaTasa / 100) * porcentaje;
                ////System.out.println("int p: " + interes);
                interes_pendiente += interes;
                interes_acumulado += interes;
                am.setInteres ( ( pt.getTipoPrestamo().equals("IA")?0:interes ) );
                
                am.setTotalAPagar( am.getCapital() + am.getInteres() );
                saldo = am.getMonto() - am.getCapital();
                am.setSaldo   ( saldo );
                
                lista.add     ( am ) ;
                fechaAnterior      = am.getFecha();
            }
            
            
            ////System.out.println("Interes Actual   : " + pt.getIntereses() );
            ////System.out.println("Total Intereses  : " + interes_acumulado );
            ////System.out.println("Interes Cobrados : " + interes_cobrado   );
            ////System.out.println("*******************************************");
            
            
            if (cuotaAjuste!=-1 && pt.getAprobado().equalsIgnoreCase("S") && pt.getTipoPrestamo().equalsIgnoreCase("IA") ){
                Amortizacion am   = (Amortizacion) lista.get(cuotaAjuste);
                am.setInteres    ( am.getInteres() + interes_acumulado - pt.getIntereses() - interes_cobrado );
                am.setTotalAPagar( am.getCapital() + am.getInteres() ); 
            }
            

            if (! ( pt.getAprobado().equalsIgnoreCase("S") && pt.getTipoPrestamo().equalsIgnoreCase("IA"))  ){
             
                if (pt.getTipoPrestamo().equals("IA") )
                    pt.setIntereses( interes_acumulado ); 
                else if (pt.getTipoPrestamo().equals("CV") )
                    pt.setIntereses( interes_pendiente ); 
                
            }
             
        }
    }    
    
    
}
