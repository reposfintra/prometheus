/*
 * Nombre        FacturasMims.java
 * Autor         Ing. Lissett Reales.
 * Fecha         19 de julio de 2006, 10:42 AM
 * Modificado    26 de octubre de 2006, 08:14 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class FacturasMims implements Serializable {
    
    // LREALES - 12 octubre 2006
    private String retencion;
    private String retencion_me;
    
    private String suma_items;
    private String suma_retefuentes;
    private String suma_ivas;
    private String suma_otros_imp;
    //
    
    // LREALES - 22 septiembre 2006
    private String estado;
    //
    
    private String distrito;
    private String placa;
    private String documento;
    private String documento2;
    private String vlr_fact;    
    private String vlrme_fact;
    private String id_mims;
    private String clase_doc;
    private String moneda;
    private String fec_grabacion;
    
    private String dia_modificacion;
    private String hora_modificacion;
    private String item;
    private String descripcion;
    private String vlr_item;
    private String vlrme_item;
    private String cuenta;
    private String user_update;
    private String planilla;
    private String aprobador;
    
    private String cod_imp;
    private String porc_imp;
    private String vlr_imp;
    private String vlrme_imp;
    private String vlr_iva;
    private String banco;
    private String sucursal;
    private String hc;
    private String fecdocumento;
    private String fecpago;
    
    private String fecvencimiento;
    private String fecaprobacion;
    private String corrida;
    private String cheque;
    private String creation_user;
    private String proveedor;
    
    private String otro_distrito;
    private String otro_proveedor;
    private String otro_tipo_documento;
    private String otro_documento;
    private String otro_descripcion;
    
    //////////////CXP_DOC////////////////
    private String reg_status;
    private String tipo_documento;
    private String descripcionfact;
    private String agencia;
    private String handle_code;
    private String tipo_documento_rel;
    private String documento_relacionado;
    private String fecha_aprobacion;
    private String usuario_aprobacion;
    private String vlr_neto;    
    private String vlr_total_abonos;
    private String vlr_saldo;
    private String vlr_neto_me;
    private String vlr_total_abonos_me;
    private String vlr_saldo_me;    
    private String tasa;
    private String usuario_contabilizo;
    private String fecha_contabilizacion;
    private String usuario_anulo;
    private String fecha_anulacion;    
    private String fecha_contabilizacion_anulacion;
    private String observacion;
    private String num_obs_autorizador;
    private String num_obs_pagador;
    private String num_obs_registra;    
    private String last_update;
    private String creation_date;
    private String base;
    private String periodo;
    private String fecha_contabilizacion_ajc;
    private String fecha_contabilizacion_ajv;    
    private String periodo_ajc;
    private String periodo_ajv;
    private String usuario_contabilizo_ajc;
    private String usuario_contabilizo_ajv;
    private String transaccion_ajc;    
    private String transaccion_ajv;
    private String fecha_procesado;
    private String clase_documento;
    private String transaccion;
    private String moneda_banco;    
    private String fecha_documento;
    private String fecha_vencimiento;
    private String ultima_fecha_pago;
    
    //////////////CXP_ITEMS_DOC////////////////
    //private String item;
    //private String descripcion;    
    private String vlritem;
    private String vlrmeitem;
    //private String cuenta;
    private String codigo_abc;
    //private String planilla;    
    private String codcliarea;
    private String tipcliarea;
    private String concepto;
    private String auxiliar;
    
    //////////////CXP_IMP_ITEM////////////////
    /*private String cod_imp;
    private String porc_imp;
    private String vlr_imp;
    private String vlrme_imp;
    private String vlr_iva;*/
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property documento2.
     * @return Value of property documento2.
     */
    public java.lang.String getDocumento2() {
        return documento2;
    }
    
    /**
     * Setter for property documento2.
     * @param documento2 New value of property documento2.
     */
    public void setDocumento2(java.lang.String documento2) {
        this.documento2 = documento2;
    }
    
    /**
     * Getter for property vlr_fact.
     * @return Value of property vlr_fact.
     */
    public java.lang.String getVlr_fact() {
        return vlr_fact;
    }
    
    /**
     * Setter for property vlr_fact.
     * @param vlr_fact New value of property vlr_fact.
     */
    public void setVlr_fact(java.lang.String vlr_fact) {
        this.vlr_fact = vlr_fact;
    }
    
    /**
     * Getter for property vlrme_fact.
     * @return Value of property vlrme_fact.
     */
    public java.lang.String getVlrme_fact() {
        return vlrme_fact;
    }
    
    /**
     * Setter for property vlrme_fact.
     * @param vlrme_fact New value of property vlrme_fact.
     */
    public void setVlrme_fact(java.lang.String vlrme_fact) {
        this.vlrme_fact = vlrme_fact;
    }
    
    /**
     * Getter for property id_mims.
     * @return Value of property id_mims.
     */
    public java.lang.String getId_mims() {
        return id_mims;
    }
    
    /**
     * Setter for property id_mims.
     * @param id_mims New value of property id_mims.
     */
    public void setId_mims(java.lang.String id_mims) {
        this.id_mims = id_mims;
    }
    
    /**
     * Getter for property clase_doc.
     * @return Value of property clase_doc.
     */
    public java.lang.String getClase_doc() {
        return clase_doc;
    }
    
    /**
     * Setter for property clase_doc.
     * @param clase_doc New value of property clase_doc.
     */
    public void setClase_doc(java.lang.String clase_doc) {
        this.clase_doc = clase_doc;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property fec_grabacion.
     * @return Value of property fec_grabacion.
     */
    public java.lang.String getFec_grabacion() {
        return fec_grabacion;
    }
    
    /**
     * Setter for property fec_grabacion.
     * @param fec_grabacion New value of property fec_grabacion.
     */
    public void setFec_grabacion(java.lang.String fec_grabacion) {
        this.fec_grabacion = fec_grabacion;
    }
    
    /**
     * Getter for property dia_modificacion.
     * @return Value of property dia_modificacion.
     */
    public java.lang.String getDia_modificacion() {
        return dia_modificacion;
    }
    
    /**
     * Setter for property dia_modificacion.
     * @param dia_modificacion New value of property dia_modificacion.
     */
    public void setDia_modificacion(java.lang.String dia_modificacion) {
        this.dia_modificacion = dia_modificacion;
    }
    
    /**
     * Getter for property hora_modificacion.
     * @return Value of property hora_modificacion.
     */
    public java.lang.String getHora_modificacion() {
        return hora_modificacion;
    }
    
    /**
     * Setter for property hora_modificacion.
     * @param hora_modificacion New value of property hora_modificacion.
     */
    public void setHora_modificacion(java.lang.String hora_modificacion) {
        this.hora_modificacion = hora_modificacion;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property vlr_item.
     * @return Value of property vlr_item.
     */
    public java.lang.String getVlr_item() {
        return vlr_item;
    }
    
    /**
     * Setter for property vlr_item.
     * @param vlr_item New value of property vlr_item.
     */
    public void setVlr_item(java.lang.String vlr_item) {
        this.vlr_item = vlr_item;
    }
    
    /**
     * Getter for property vlrme_item.
     * @return Value of property vlrme_item.
     */
    public java.lang.String getVlrme_item() {
        return vlrme_item;
    }
    
    /**
     * Setter for property vlrme_item.
     * @param vlrme_item New value of property vlrme_item.
     */
    public void setVlrme_item(java.lang.String vlrme_item) {
        this.vlrme_item = vlrme_item;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property aprobador.
     * @return Value of property aprobador.
     */
    public java.lang.String getAprobador() {
        return aprobador;
    }
    
    /**
     * Setter for property aprobador.
     * @param aprobador New value of property aprobador.
     */
    public void setAprobador(java.lang.String aprobador) {
        this.aprobador = aprobador;
    }
    
    /**
     * Getter for property cod_imp.
     * @return Value of property cod_imp.
     */
    public java.lang.String getCod_imp() {
        return cod_imp;
    }
    
    /**
     * Setter for property cod_imp.
     * @param cod_imp New value of property cod_imp.
     */
    public void setCod_imp(java.lang.String cod_imp) {
        this.cod_imp = cod_imp;
    }
    
    /**
     * Getter for property porc_imp.
     * @return Value of property porc_imp.
     */
    public java.lang.String getPorc_imp() {
        return porc_imp;
    }
    
    /**
     * Setter for property porc_imp.
     * @param porc_imp New value of property porc_imp.
     */
    public void setPorc_imp(java.lang.String porc_imp) {
        this.porc_imp = porc_imp;
    }
    
    /**
     * Getter for property vlr_imp.
     * @return Value of property vlr_imp.
     */
    public java.lang.String getVlr_imp() {
        return vlr_imp;
    }
    
    /**
     * Setter for property vlr_imp.
     * @param vlr_imp New value of property vlr_imp.
     */
    public void setVlr_imp(java.lang.String vlr_imp) {
        this.vlr_imp = vlr_imp;
    }
    
    /**
     * Getter for property vlrme_imp.
     * @return Value of property vlrme_imp.
     */
    public java.lang.String getVlrme_imp() {
        return vlrme_imp;
    }
    
    /**
     * Setter for property vlrme_imp.
     * @param vlrme_imp New value of property vlrme_imp.
     */
    public void setVlrme_imp(java.lang.String vlrme_imp) {
        this.vlrme_imp = vlrme_imp;
    }
    
    /**
     * Getter for property vlr_iva.
     * @return Value of property vlr_iva.
     */
    public java.lang.String getVlr_iva() {
        return vlr_iva;
    }
    
    /**
     * Setter for property vlr_iva.
     * @param vlr_iva New value of property vlr_iva.
     */
    public void setVlr_iva(java.lang.String vlr_iva) {
        this.vlr_iva = vlr_iva;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property hc.
     * @return Value of property hc.
     */
    public java.lang.String getHc() {
        return hc;
    }
    
    /**
     * Setter for property hc.
     * @param hc New value of property hc.
     */
    public void setHc(java.lang.String hc) {
        this.hc = hc;
    }
    
    /**
     * Getter for property fecdocumento.
     * @return Value of property fecdocumento.
     */
    public java.lang.String getFecdocumento() {
        return fecdocumento;
    }
    
    /**
     * Setter for property fecdocumento.
     * @param fecdocumento New value of property fecdocumento.
     */
    public void setFecdocumento(java.lang.String fecdocumento) {
        this.fecdocumento = fecdocumento;
    }
    
    /**
     * Getter for property fecpago.
     * @return Value of property fecpago.
     */
    public java.lang.String getFecpago() {
        return fecpago;
    }
    
    /**
     * Setter for property fecpago.
     * @param fecpago New value of property fecpago.
     */
    public void setFecpago(java.lang.String fecpago) {
        this.fecpago = fecpago;
    }
    
    /**
     * Getter for property fecvencimiento.
     * @return Value of property fecvencimiento.
     */
    public java.lang.String getFecvencimiento() {
        return fecvencimiento;
    }
    
    /**
     * Setter for property fecvencimiento.
     * @param fecvencimiento New value of property fecvencimiento.
     */
    public void setFecvencimiento(java.lang.String fecvencimiento) {
        this.fecvencimiento = fecvencimiento;
    }
    
    /**
     * Getter for property fecaprobacion.
     * @return Value of property fecaprobacion.
     */
    public java.lang.String getFecaprobacion() {
        return fecaprobacion;
    }
    
    /**
     * Setter for property fecaprobacion.
     * @param fecaprobacion New value of property fecaprobacion.
     */
    public void setFecaprobacion(java.lang.String fecaprobacion) {
        this.fecaprobacion = fecaprobacion;
    }
    
    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }
    
    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }
    
    /**
     * Getter for property cheque.
     * @return Value of property cheque.
     */
    public java.lang.String getCheque() {
        return cheque;
    }
    
    /**
     * Setter for property cheque.
     * @param cheque New value of property cheque.
     */
    public void setCheque(java.lang.String cheque) {
        this.cheque = cheque;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property otro_distrito.
     * @return Value of property otro_distrito.
     */
    public java.lang.String getOtro_distrito() {
        return otro_distrito;
    }
    
    /**
     * Setter for property otro_distrito.
     * @param otro_distrito New value of property otro_distrito.
     */
    public void setOtro_distrito(java.lang.String otro_distrito) {
        this.otro_distrito = otro_distrito;
    }
    
    /**
     * Getter for property otro_proveedor.
     * @return Value of property otro_proveedor.
     */
    public java.lang.String getOtro_proveedor() {
        return otro_proveedor;
    }
    
    /**
     * Setter for property otro_proveedor.
     * @param otro_proveedor New value of property otro_proveedor.
     */
    public void setOtro_proveedor(java.lang.String otro_proveedor) {
        this.otro_proveedor = otro_proveedor;
    }
    
    /**
     * Getter for property otro_tipo_documento.
     * @return Value of property otro_tipo_documento.
     */
    public java.lang.String getOtro_tipo_documento() {
        return otro_tipo_documento;
    }
    
    /**
     * Setter for property otro_tipo_documento.
     * @param otro_tipo_documento New value of property otro_tipo_documento.
     */
    public void setOtro_tipo_documento(java.lang.String otro_tipo_documento) {
        this.otro_tipo_documento = otro_tipo_documento;
    }
    
    /**
     * Getter for property otro_documento.
     * @return Value of property otro_documento.
     */
    public java.lang.String getOtro_documento() {
        return otro_documento;
    }
    
    /**
     * Setter for property otro_documento.
     * @param otro_documento New value of property otro_documento.
     */
    public void setOtro_documento(java.lang.String otro_documento) {
        this.otro_documento = otro_documento;
    }
    
    /**
     * Getter for property otro_descripcion.
     * @return Value of property otro_descripcion.
     */
    public java.lang.String getOtro_descripcion() {
        return otro_descripcion;
    }
    
    /**
     * Setter for property otro_descripcion.
     * @param otro_descripcion New value of property otro_descripcion.
     */
    public void setOtro_descripcion(java.lang.String otro_descripcion) {
        this.otro_descripcion = otro_descripcion;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property descripcionfact.
     * @return Value of property descripcionfact.
     */
    public java.lang.String getDescripcionfact() {
        return descripcionfact;
    }
    
    /**
     * Setter for property descripcionfact.
     * @param descripcionfact New value of property descripcionfact.
     */
    public void setDescripcionfact(java.lang.String descripcionfact) {
        this.descripcionfact = descripcionfact;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property handle_code.
     * @return Value of property handle_code.
     */
    public java.lang.String getHandle_code() {
        return handle_code;
    }
    
    /**
     * Setter for property handle_code.
     * @param handle_code New value of property handle_code.
     */
    public void setHandle_code(java.lang.String handle_code) {
        this.handle_code = handle_code;
    }
    
    /**
     * Getter for property tipo_documento_rel.
     * @return Value of property tipo_documento_rel.
     */
    public java.lang.String getTipo_documento_rel() {
        return tipo_documento_rel;
    }
    
    /**
     * Setter for property tipo_documento_rel.
     * @param tipo_documento_rel New value of property tipo_documento_rel.
     */
    public void setTipo_documento_rel(java.lang.String tipo_documento_rel) {
        this.tipo_documento_rel = tipo_documento_rel;
    }
    
    /**
     * Getter for property documento_relacionado.
     * @return Value of property documento_relacionado.
     */
    public java.lang.String getDocumento_relacionado() {
        return documento_relacionado;
    }
    
    /**
     * Setter for property documento_relacionado.
     * @param documento_relacionado New value of property documento_relacionado.
     */
    public void setDocumento_relacionado(java.lang.String documento_relacionado) {
        this.documento_relacionado = documento_relacionado;
    }
    
    /**
     * Getter for property fecha_aprobacion.
     * @return Value of property fecha_aprobacion.
     */
    public java.lang.String getFecha_aprobacion() {
        return fecha_aprobacion;
    }
    
    /**
     * Setter for property fecha_aprobacion.
     * @param fecha_aprobacion New value of property fecha_aprobacion.
     */
    public void setFecha_aprobacion(java.lang.String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }
    
    /**
     * Getter for property usuario_aprobacion.
     * @return Value of property usuario_aprobacion.
     */
    public java.lang.String getUsuario_aprobacion() {
        return usuario_aprobacion;
    }
    
    /**
     * Setter for property usuario_aprobacion.
     * @param usuario_aprobacion New value of property usuario_aprobacion.
     */
    public void setUsuario_aprobacion(java.lang.String usuario_aprobacion) {
        this.usuario_aprobacion = usuario_aprobacion;
    }
    
    /**
     * Getter for property vlr_neto.
     * @return Value of property vlr_neto.
     */
    public java.lang.String getVlr_neto() {
        return vlr_neto;
    }
    
    /**
     * Setter for property vlr_neto.
     * @param vlr_neto New value of property vlr_neto.
     */
    public void setVlr_neto(java.lang.String vlr_neto) {
        this.vlr_neto = vlr_neto;
    }
    
    /**
     * Getter for property vlr_total_abonos.
     * @return Value of property vlr_total_abonos.
     */
    public java.lang.String getVlr_total_abonos() {
        return vlr_total_abonos;
    }
    
    /**
     * Setter for property vlr_total_abonos.
     * @param vlr_total_abonos New value of property vlr_total_abonos.
     */
    public void setVlr_total_abonos(java.lang.String vlr_total_abonos) {
        this.vlr_total_abonos = vlr_total_abonos;
    }
    
    /**
     * Getter for property vlr_saldo.
     * @return Value of property vlr_saldo.
     */
    public java.lang.String getVlr_saldo() {
        return vlr_saldo;
    }
    
    /**
     * Setter for property vlr_saldo.
     * @param vlr_saldo New value of property vlr_saldo.
     */
    public void setVlr_saldo(java.lang.String vlr_saldo) {
        this.vlr_saldo = vlr_saldo;
    }
    
    /**
     * Getter for property vlr_neto_me.
     * @return Value of property vlr_neto_me.
     */
    public java.lang.String getVlr_neto_me() {
        return vlr_neto_me;
    }
    
    /**
     * Setter for property vlr_neto_me.
     * @param vlr_neto_me New value of property vlr_neto_me.
     */
    public void setVlr_neto_me(java.lang.String vlr_neto_me) {
        this.vlr_neto_me = vlr_neto_me;
    }
    
    /**
     * Getter for property vlr_total_abonos_me.
     * @return Value of property vlr_total_abonos_me.
     */
    public java.lang.String getVlr_total_abonos_me() {
        return vlr_total_abonos_me;
    }
    
    /**
     * Setter for property vlr_total_abonos_me.
     * @param vlr_total_abonos_me New value of property vlr_total_abonos_me.
     */
    public void setVlr_total_abonos_me(java.lang.String vlr_total_abonos_me) {
        this.vlr_total_abonos_me = vlr_total_abonos_me;
    }
    
    /**
     * Getter for property vlr_saldo_me.
     * @return Value of property vlr_saldo_me.
     */
    public java.lang.String getVlr_saldo_me() {
        return vlr_saldo_me;
    }
    
    /**
     * Setter for property vlr_saldo_me.
     * @param vlr_saldo_me New value of property vlr_saldo_me.
     */
    public void setVlr_saldo_me(java.lang.String vlr_saldo_me) {
        this.vlr_saldo_me = vlr_saldo_me;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public java.lang.String getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(java.lang.String tasa) {
        this.tasa = tasa;
    }
    
    /**
     * Getter for property usuario_contabilizo.
     * @return Value of property usuario_contabilizo.
     */
    public java.lang.String getUsuario_contabilizo() {
        return usuario_contabilizo;
    }
    
    /**
     * Setter for property usuario_contabilizo.
     * @param usuario_contabilizo New value of property usuario_contabilizo.
     */
    public void setUsuario_contabilizo(java.lang.String usuario_contabilizo) {
        this.usuario_contabilizo = usuario_contabilizo;
    }
    
    /**
     * Getter for property fecha_contabilizacion.
     * @return Value of property fecha_contabilizacion.
     */
    public java.lang.String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }
    
    /**
     * Setter for property fecha_contabilizacion.
     * @param fecha_contabilizacion New value of property fecha_contabilizacion.
     */
    public void setFecha_contabilizacion(java.lang.String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }
    
    /**
     * Getter for property usuario_anulo.
     * @return Value of property usuario_anulo.
     */
    public java.lang.String getUsuario_anulo() {
        return usuario_anulo;
    }
    
    /**
     * Setter for property usuario_anulo.
     * @param usuario_anulo New value of property usuario_anulo.
     */
    public void setUsuario_anulo(java.lang.String usuario_anulo) {
        this.usuario_anulo = usuario_anulo;
    }
    
    /**
     * Getter for property fecha_anulacion.
     * @return Value of property fecha_anulacion.
     */
    public java.lang.String getFecha_anulacion() {
        return fecha_anulacion;
    }
    
    /**
     * Setter for property fecha_anulacion.
     * @param fecha_anulacion New value of property fecha_anulacion.
     */
    public void setFecha_anulacion(java.lang.String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }
    
    /**
     * Getter for property fecha_contabilizacion_anulacion.
     * @return Value of property fecha_contabilizacion_anulacion.
     */
    public java.lang.String getFecha_contabilizacion_anulacion() {
        return fecha_contabilizacion_anulacion;
    }
    
    /**
     * Setter for property fecha_contabilizacion_anulacion.
     * @param fecha_contabilizacion_anulacion New value of property fecha_contabilizacion_anulacion.
     */
    public void setFecha_contabilizacion_anulacion(java.lang.String fecha_contabilizacion_anulacion) {
        this.fecha_contabilizacion_anulacion = fecha_contabilizacion_anulacion;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property num_obs_autorizador.
     * @return Value of property num_obs_autorizador.
     */
    public java.lang.String getNum_obs_autorizador() {
        return num_obs_autorizador;
    }
    
    /**
     * Setter for property num_obs_autorizador.
     * @param num_obs_autorizador New value of property num_obs_autorizador.
     */
    public void setNum_obs_autorizador(java.lang.String num_obs_autorizador) {
        this.num_obs_autorizador = num_obs_autorizador;
    }
    
    /**
     * Getter for property num_obs_pagador.
     * @return Value of property num_obs_pagador.
     */
    public java.lang.String getNum_obs_pagador() {
        return num_obs_pagador;
    }
    
    /**
     * Setter for property num_obs_pagador.
     * @param num_obs_pagador New value of property num_obs_pagador.
     */
    public void setNum_obs_pagador(java.lang.String num_obs_pagador) {
        this.num_obs_pagador = num_obs_pagador;
    }
    
    /**
     * Getter for property num_obs_registra.
     * @return Value of property num_obs_registra.
     */
    public java.lang.String getNum_obs_registra() {
        return num_obs_registra;
    }
    
    /**
     * Setter for property num_obs_registra.
     * @param num_obs_registra New value of property num_obs_registra.
     */
    public void setNum_obs_registra(java.lang.String num_obs_registra) {
        this.num_obs_registra = num_obs_registra;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property fecha_contabilizacion_ajc.
     * @return Value of property fecha_contabilizacion_ajc.
     */
    public java.lang.String getFecha_contabilizacion_ajc() {
        return fecha_contabilizacion_ajc;
    }
    
    /**
     * Setter for property fecha_contabilizacion_ajc.
     * @param fecha_contabilizacion_ajc New value of property fecha_contabilizacion_ajc.
     */
    public void setFecha_contabilizacion_ajc(java.lang.String fecha_contabilizacion_ajc) {
        this.fecha_contabilizacion_ajc = fecha_contabilizacion_ajc;
    }
    
    /**
     * Getter for property fecha_contabilizacion_ajv.
     * @return Value of property fecha_contabilizacion_ajv.
     */
    public java.lang.String getFecha_contabilizacion_ajv() {
        return fecha_contabilizacion_ajv;
    }
    
    /**
     * Setter for property fecha_contabilizacion_ajv.
     * @param fecha_contabilizacion_ajv New value of property fecha_contabilizacion_ajv.
     */
    public void setFecha_contabilizacion_ajv(java.lang.String fecha_contabilizacion_ajv) {
        this.fecha_contabilizacion_ajv = fecha_contabilizacion_ajv;
    }
    
    /**
     * Getter for property periodo_ajc.
     * @return Value of property periodo_ajc.
     */
    public java.lang.String getPeriodo_ajc() {
        return periodo_ajc;
    }
    
    /**
     * Setter for property periodo_ajc.
     * @param periodo_ajc New value of property periodo_ajc.
     */
    public void setPeriodo_ajc(java.lang.String periodo_ajc) {
        this.periodo_ajc = periodo_ajc;
    }
    
    /**
     * Getter for property periodo_ajv.
     * @return Value of property periodo_ajv.
     */
    public java.lang.String getPeriodo_ajv() {
        return periodo_ajv;
    }
    
    /**
     * Setter for property periodo_ajv.
     * @param periodo_ajv New value of property periodo_ajv.
     */
    public void setPeriodo_ajv(java.lang.String periodo_ajv) {
        this.periodo_ajv = periodo_ajv;
    }
    
    /**
     * Getter for property usuario_contabilizo_ajc.
     * @return Value of property usuario_contabilizo_ajc.
     */
    public java.lang.String getUsuario_contabilizo_ajc() {
        return usuario_contabilizo_ajc;
    }
    
    /**
     * Setter for property usuario_contabilizo_ajc.
     * @param usuario_contabilizo_ajc New value of property usuario_contabilizo_ajc.
     */
    public void setUsuario_contabilizo_ajc(java.lang.String usuario_contabilizo_ajc) {
        this.usuario_contabilizo_ajc = usuario_contabilizo_ajc;
    }
    
    /**
     * Getter for property usuario_contabilizo_ajv.
     * @return Value of property usuario_contabilizo_ajv.
     */
    public java.lang.String getUsuario_contabilizo_ajv() {
        return usuario_contabilizo_ajv;
    }
    
    /**
     * Setter for property usuario_contabilizo_ajv.
     * @param usuario_contabilizo_ajv New value of property usuario_contabilizo_ajv.
     */
    public void setUsuario_contabilizo_ajv(java.lang.String usuario_contabilizo_ajv) {
        this.usuario_contabilizo_ajv = usuario_contabilizo_ajv;
    }
    
    /**
     * Getter for property transaccion_ajc.
     * @return Value of property transaccion_ajc.
     */
    public java.lang.String getTransaccion_ajc() {
        return transaccion_ajc;
    }
    
    /**
     * Setter for property transaccion_ajc.
     * @param transaccion_ajc New value of property transaccion_ajc.
     */
    public void setTransaccion_ajc(java.lang.String transaccion_ajc) {
        this.transaccion_ajc = transaccion_ajc;
    }
    
    /**
     * Getter for property transaccion_ajv.
     * @return Value of property transaccion_ajv.
     */
    public java.lang.String getTransaccion_ajv() {
        return transaccion_ajv;
    }
    
    /**
     * Setter for property transaccion_ajv.
     * @param transaccion_ajv New value of property transaccion_ajv.
     */
    public void setTransaccion_ajv(java.lang.String transaccion_ajv) {
        this.transaccion_ajv = transaccion_ajv;
    }
    
    /**
     * Getter for property fecha_procesado.
     * @return Value of property fecha_procesado.
     */
    public java.lang.String getFecha_procesado() {
        return fecha_procesado;
    }
    
    /**
     * Setter for property fecha_procesado.
     * @param fecha_procesado New value of property fecha_procesado.
     */
    public void setFecha_procesado(java.lang.String fecha_procesado) {
        this.fecha_procesado = fecha_procesado;
    }
    
    /**
     * Getter for property clase_documento.
     * @return Value of property clase_documento.
     */
    public java.lang.String getClase_documento() {
        return clase_documento;
    }
    
    /**
     * Setter for property clase_documento.
     * @param clase_documento New value of property clase_documento.
     */
    public void setClase_documento(java.lang.String clase_documento) {
        this.clase_documento = clase_documento;
    }
    
    /**
     * Getter for property transaccion.
     * @return Value of property transaccion.
     */
    public java.lang.String getTransaccion() {
        return transaccion;
    }
    
    /**
     * Setter for property transaccion.
     * @param transaccion New value of property transaccion.
     */
    public void setTransaccion(java.lang.String transaccion) {
        this.transaccion = transaccion;
    }
    
    /**
     * Getter for property moneda_banco.
     * @return Value of property moneda_banco.
     */
    public java.lang.String getMoneda_banco() {
        return moneda_banco;
    }
    
    /**
     * Setter for property moneda_banco.
     * @param moneda_banco New value of property moneda_banco.
     */
    public void setMoneda_banco(java.lang.String moneda_banco) {
        this.moneda_banco = moneda_banco;
    }
    
    /**
     * Getter for property fecha_documento.
     * @return Value of property fecha_documento.
     */
    public java.lang.String getFecha_documento() {
        return fecha_documento;
    }
    
    /**
     * Setter for property fecha_documento.
     * @param fecha_documento New value of property fecha_documento.
     */
    public void setFecha_documento(java.lang.String fecha_documento) {
        this.fecha_documento = fecha_documento;
    }
    
    /**
     * Getter for property fecha_vencimiento.
     * @return Value of property fecha_vencimiento.
     */
    public java.lang.String getFecha_vencimiento() {
        return fecha_vencimiento;
    }
    
    /**
     * Setter for property fecha_vencimiento.
     * @param fecha_vencimiento New value of property fecha_vencimiento.
     */
    public void setFecha_vencimiento(java.lang.String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }
    
    /**
     * Getter for property ultima_fecha_pago.
     * @return Value of property ultima_fecha_pago.
     */
    public java.lang.String getUltima_fecha_pago() {
        return ultima_fecha_pago;
    }
    
    /**
     * Setter for property ultima_fecha_pago.
     * @param ultima_fecha_pago New value of property ultima_fecha_pago.
     */
    public void setUltima_fecha_pago(java.lang.String ultima_fecha_pago) {
        this.ultima_fecha_pago = ultima_fecha_pago;
    }
    
    /**
     * Getter for property vlritem.
     * @return Value of property vlritem.
     */
    public java.lang.String getVlritem() {
        return vlritem;
    }
    
    /**
     * Setter for property vlritem.
     * @param vlritem New value of property vlritem.
     */
    public void setVlritem(java.lang.String vlritem) {
        this.vlritem = vlritem;
    }
    
    /**
     * Getter for property vlrmeitem.
     * @return Value of property vlrmeitem.
     */
    public java.lang.String getVlrmeitem() {
        return vlrmeitem;
    }
    
    /**
     * Setter for property vlrmeitem.
     * @param vlrmeitem New value of property vlrmeitem.
     */
    public void setVlrmeitem(java.lang.String vlrmeitem) {
        this.vlrmeitem = vlrmeitem;
    }
    
    /**
     * Getter for property codigo_abc.
     * @return Value of property codigo_abc.
     */
    public java.lang.String getCodigo_abc() {
        return codigo_abc;
    }
    
    /**
     * Setter for property codigo_abc.
     * @param codigo_abc New value of property codigo_abc.
     */
    public void setCodigo_abc(java.lang.String codigo_abc) {
        this.codigo_abc = codigo_abc;
    }
    
    /**
     * Getter for property codcliarea.
     * @return Value of property codcliarea.
     */
    public java.lang.String getCodcliarea() {
        return codcliarea;
    }
    
    /**
     * Setter for property codcliarea.
     * @param codcliarea New value of property codcliarea.
     */
    public void setCodcliarea(java.lang.String codcliarea) {
        this.codcliarea = codcliarea;
    }
    
    /**
     * Getter for property tipcliarea.
     * @return Value of property tipcliarea.
     */
    public java.lang.String getTipcliarea() {
        return tipcliarea;
    }
    
    /**
     * Setter for property tipcliarea.
     * @param tipcliarea New value of property tipcliarea.
     */
    public void setTipcliarea(java.lang.String tipcliarea) {
        this.tipcliarea = tipcliarea;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }
    
    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property retencion.
     * @return Value of property retencion.
     */
    public java.lang.String getRetencion() {
        return retencion;
    }
    
    /**
     * Setter for property retencion.
     * @param retencion New value of property retencion.
     */
    public void setRetencion(java.lang.String retencion) {
        this.retencion = retencion;
    }
    
    /**
     * Getter for property suma_items.
     * @return Value of property suma_items.
     */
    public java.lang.String getSuma_items() {
        return suma_items;
    }
    
    /**
     * Setter for property suma_items.
     * @param suma_items New value of property suma_items.
     */
    public void setSuma_items(java.lang.String suma_items) {
        this.suma_items = suma_items;
    }
    
    /**
     * Getter for property suma_retefuentes.
     * @return Value of property suma_retefuentes.
     */
    public java.lang.String getSuma_retefuentes() {
        return suma_retefuentes;
    }
    
    /**
     * Setter for property suma_retefuentes.
     * @param suma_retefuentes New value of property suma_retefuentes.
     */
    public void setSuma_retefuentes(java.lang.String suma_retefuentes) {
        this.suma_retefuentes = suma_retefuentes;
    }
    
    /**
     * Getter for property suma_ivas.
     * @return Value of property suma_ivas.
     */
    public java.lang.String getSuma_ivas() {
        return suma_ivas;
    }
    
    /**
     * Setter for property suma_ivas.
     * @param suma_ivas New value of property suma_ivas.
     */
    public void setSuma_ivas(java.lang.String suma_ivas) {
        this.suma_ivas = suma_ivas;
    }
    
    /**
     * Getter for property suma_otros_imp.
     * @return Value of property suma_otros_imp.
     */
    public java.lang.String getSuma_otros_imp() {
        return suma_otros_imp;
    }
    
    /**
     * Setter for property suma_otros_imp.
     * @param suma_otros_imp New value of property suma_otros_imp.
     */
    public void setSuma_otros_imp(java.lang.String suma_otros_imp) {
        this.suma_otros_imp = suma_otros_imp;
    }
    
    /**
     * Getter for property retencion_me.
     * @return Value of property retencion_me.
     */
    public java.lang.String getRetencion_me() {
        return retencion_me;
    }
    
    /**
     * Setter for property retencion_me.
     * @param retencion_me New value of property retencion_me.
     */
    public void setRetencion_me(java.lang.String retencion_me) {
        this.retencion_me = retencion_me;
    }
    
}