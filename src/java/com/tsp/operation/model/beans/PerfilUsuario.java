/*
 * Created on 11 de agosto de 2005, 04:53 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  trafisur
 */
public class PerfilUsuario {
    
    private String perfil;
    private String usuarios;
    private String perfilName;
    private String userName;
    
    public PerfilUsuario() {
       perfil     = "";
       usuarios   = "";
       perfilName = "";
       userName   = "";
    }
    
    
    public void setPerfil   (String va){ this.perfil     = va;  }
    public void setUsuarios (String va){ this.usuarios   = va;  }
    public void setUserName (String va){ this.userName   = va;  }
    public void setPerName  (String va){ this.perfilName = va;  }
    
    public String getPerfil   (){ return this.perfil     ;  }
    public String getUsuarios (){ return this.usuarios   ;  }
    public String getUserName (){ return this.userName   ;  }
    public String getPerName  (){ return this.perfilName ;  }
 
  
    
}
