package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * Bean para la tabla dc.telefono<br/>
 * 1/09/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Telefono {

    private int id;
    private String tipo;
    private long numero;
    private String nombreCiudad;
    private String nombreDepartamento;
    private int codigoPais;
    private int codigoArea;
    private String fuente;
    private Timestamp creacion;
    private Timestamp actualizacion;
    private int numReportes;
    private String entidad;
    private String reportado;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;

    /**
     * Get the value of reportado
     *
     * @return the value of reportado
     */
    public String getReportado() {
        return reportado;
    }

    /**
     * Set the value of reportado
     *
     * @param reportado new value of reportado
     */
    public void setReportado(String reportado) {
        this.reportado = reportado;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of entidad
     *
     * @return the value of entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Set the value of entidad
     *
     * @param entidad new value of entidad
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * Get the value of numReportes
     *
     * @return the value of numReportes
     */
    public int getNumReportes() {
        return numReportes;
    }

    /**
     * Set the value of numReportes
     *
     * @param numReportes new value of numReportes
     */
    public void setNumReportes(int numReportes) {
        this.numReportes = numReportes;
    }


    /**
     * Get the value of actualizacion
     *
     * @return the value of actualizacion
     */
    public Timestamp getActualizacion() {
        return actualizacion;
    }

    /**
     * Set the value of actualizacion
     *
     * @param actualizacion new value of actualizacion
     */
    public void setActualizacion(Timestamp actualizacion) {
        this.actualizacion = actualizacion;
    }


    /**
     * Get the value of creacion
     *
     * @return the value of creacion
     */
    public Timestamp getCreacion() {
        return creacion;
    }

    /**
     * Set the value of creacion
     *
     * @param creacion new value of creacion
     */
    public void setCreacion(Timestamp creacion) {
        this.creacion = creacion;
    }

    /**
     * Get the value of fuente
     *
     * @return the value of fuente
     */
    public String getFuente() {
        return fuente;
    }

    /**
     * Set the value of fuente
     *
     * @param fuente new value of fuente
     */
    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    /**
     * Get the value of codigoArea
     *
     * @return the value of codigoArea
     */
    public int getCodigoArea() {
        return codigoArea;
    }

    /**
     * Set the value of codigoArea
     *
     * @param codigoArea new value of codigoArea
     */
    public void setCodigoArea(int codigoArea) {
        this.codigoArea = codigoArea;
    }

    /**
     * Get the value of codigoPais
     *
     * @return the value of codigoPais
     */
    public int getCodigoPais() {
        return codigoPais;
    }

    /**
     * Set the value of codigoPais
     *
     * @param codigoPais new value of codigoPais
     */
    public void setCodigoPais(int codigoPais) {
        this.codigoPais = codigoPais;
    }

    /**
     * Get the value of nombreDepartamento
     *
     * @return the value of nombreDepartamento
     */
    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    /**
     * Set the value of nombreDepartamento
     *
     * @param nombreDepartamento new value of nombreDepartamento
     */
    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    /**
     * Get the value of tipo
     *
     * @return the value of tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set the value of tipo
     *
     * @param tipo new value of tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Get the value of nombreCiudad
     *
     * @return the value of nombreCiudad
     */
    public String getNombreCiudad() {
        return nombreCiudad;
    }

    /**
     * Set the value of nombreCiudad
     *
     * @param nombreCiudad new value of nombreCiudad
     */
    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    /**
     * Get the value of numero
     *
     * @return the value of numero
     */
    public long getNumero() {
        return numero;
    }

    /**
     * Set the value of numero
     *
     * @param numero new value of numero
     */
    public void setNumero(long numero) {
        this.numero = numero;
    }


    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }


}
