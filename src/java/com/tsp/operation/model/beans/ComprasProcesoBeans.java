/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class ComprasProcesoBeans {

    public String id_tipo_insumo;
    public String tipo_insumo;
    public String id_insumo;
    public String codigo_material;
    public String descripcion_insumo;
    public String unidad_medida_insumo;
    public String nombre_unidad_insumo;
    public String total_insumos;

    public String responsable;
    public String id_solicitud;
    public String insumos_total;
    public String insumos_solicitados;
    public String insumos_disponibles;
    public String solicitado_temporal;
    public String filtro_insumo;
    
    public String costo_presupuesto;
    public String cantidad_disponible;
    public String cantidad_solicitada;
    public String cantidad_total;
    public String cod_solicitud;  
    
    public String id_apu;
    public String nombre_apu;
    public String insumo_adicional;
    public String referencia_externa;
    public String observacion_material;
    
    public String cantidad_recibida;
    public String costo_unitario_recibido;
    public String costo_total_recibido;
    public String no_despacho;
    public String id_ocs;
    public String id_ocs_detalle;
    public String reg_status;
    public String orden_compra;
    public String cod_proveedor;
    public String nombre_proveedor;
    public String forma_pago;
    public String estado_ocompra_apoteosys;

    public String getObservacion_material() {
        return observacion_material;
    }

    public void setObservacion_material(String observacion_material) {
        this.observacion_material = observacion_material;
    }  
        
    public String getIdTipoInsumo() {
        return id_tipo_insumo;
    }

    public void setIdTipoInsumo(String id_tipo_insumo) {
        this.id_tipo_insumo = id_tipo_insumo;
    }

    public String getTipoInsumo() {
        return tipo_insumo;
    }

    public void setTipoInsumo(String tipo_insumo) {
        this.tipo_insumo = tipo_insumo;
    }

    public String getIdInsumo() {
        return id_insumo;
    }

    public void setIdInsumo(String id_insumo) {
        this.id_insumo = id_insumo;
    }

    public String getCodigoMaterial() {
        return codigo_material;
    }

    public void setCodigoMaterial(String codigo_material) {
        this.codigo_material = codigo_material;
    }

    public String getDescripcionInsumo() {
        return descripcion_insumo;
    }

    public void setDescripcionInsumo(String descripcion_insumo) {
        this.descripcion_insumo = descripcion_insumo;
    }

    public String getUnidadMedidaInsumo() {
        return unidad_medida_insumo;
    }

    public void setUnidadMedidaInsumo(String unidad_medida_insumo) {
        this.unidad_medida_insumo = unidad_medida_insumo;
    }

    public String getNombreUnidadInsumo() {
        return nombre_unidad_insumo;
    }

    public void setNombreUnidadInsumo(String nombre_unidad_insumo) {
        this.nombre_unidad_insumo = nombre_unidad_insumo;
    }

    public String getTotalInsumos() {
        return total_insumos;
    }

    public void setTotalInsumos(String total_insumos) {
        this.total_insumos = total_insumos;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }    

    public String getIdSolicitud() {
        return id_solicitud;
    }

    public void setIdSolicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }    

    public String getInsumosTotal() {
        return insumos_total;
    }

    public void setInsumosTotal(String insumos_total) {
        this.insumos_total = insumos_total;
    }
    
    public String getInsumosSolicitados() {
        return insumos_solicitados;
    }

    public void setInsumosSolicitados(String insumos_solicitados) {
        this.insumos_solicitados = insumos_solicitados;
    }

    public String getInsumosDisponibles() {
        return insumos_disponibles;
    }

    public void setInsumosDisponibles(String insumos_disponibles) {
        this.insumos_disponibles = insumos_disponibles;
    }
    
    public String getSolicitadoTemporal() {
        return solicitado_temporal;
    }

    public void setSolicitadoTemporal(String solicitado_temporal) {
        this.solicitado_temporal = solicitado_temporal;
    }    
    
    public String getFiltroInsumo() {
        return filtro_insumo;
    }

    public void setFiltroInsumo(String filtro_insumo) {
        this.filtro_insumo = filtro_insumo;
    }
    
    
    public String getCodSolicitud() {
        return cod_solicitud;
    }

    public void setCodSolicitud(String cod_solicitud) {
        this.cod_solicitud = cod_solicitud;
    }    

    public String getCantidadTotal() {
        return cantidad_total;
    }

    public void setCantidadTotal(String cantidad_total) {
        this.cantidad_total = cantidad_total;
    }

    public String getCantidadSolicitada() {
        return cantidad_solicitada;
    }

    public void setCantidadSolicitada(String cantidad_solicitada) {
        this.cantidad_solicitada = cantidad_solicitada;
    }
    
    public String getCantidadDisponible() {
        return cantidad_disponible;
    }

    public void setCantidadDisponible(String cantidad_disponible) {
        this.cantidad_disponible = cantidad_disponible;
    }
    
    public String getCostoPresupuesto() {
        return costo_presupuesto;
    }

    public void setCostoPresupuesto(String costo_presupuesto) {
        this.costo_presupuesto = costo_presupuesto;
    }    
    

    public String getIdApu() {
        return id_apu;
    }

    public void setIdApu(String id_apu) {
        this.id_apu = id_apu;
    }    
    
    
    public String getNombreApu() {
        return nombre_apu;
    }

    public void setNombreApu(String nombre_apu) {
        this.nombre_apu = nombre_apu;
    }
    
    public String getInsumoAdicional() {
        return insumo_adicional;
    }

    public void setInsumoAdicional(String insumo_adicional) {
        this.insumo_adicional = insumo_adicional;
    }  
    
    public String getReferenciaExterna() {
        return referencia_externa;
    }

    public void setReferenciaExterna(String referencia_externa) {
        this.referencia_externa = referencia_externa;
    }      
    
    
    public String getCantidadRecibida() {
        return cantidad_recibida;
    }

    public void setCantidadRecibida(String cantidad_recibida) {
        this.cantidad_recibida = cantidad_recibida;
    }      
    
    public String getCostoUnitarioRecibido() {
        return costo_unitario_recibido;
    }

    public void setCostoUnitarioRecibido(String costo_unitario_recibido) {
        this.costo_unitario_recibido = costo_unitario_recibido;
    }      

    public String getCostoTotalRecibido() {
        return costo_total_recibido;
    }

    public void setCostoTotalRecibido(String costo_total_recibido) {
        this.costo_total_recibido = costo_total_recibido;
    }      

    public String getNoDespacho() {
        return no_despacho;
    }

    public void setNoDespacho(String no_despacho) {
        this.no_despacho = no_despacho;
    }      

    public String getIdOcs() {
        return id_ocs;
    }

    public void setIdOcs(String id_ocs) {
        this.id_ocs = id_ocs;
    }      

    public String getIdOcsDetalle() {
        return id_ocs_detalle;
    }

    public void setIdOcsDetalle(String id_ocs_detalle) {
        this.id_ocs_detalle = id_ocs_detalle;
    }
    
    public String getRegStatus() {
        return reg_status;
    }

    public void setRegStatus(String reg_status) {
        this.reg_status = reg_status;
    }
            
    public String getOrdenCompra() {
        return orden_compra;
    }

    public void setOrdenCompra(String orden_compra) {
        this.orden_compra = orden_compra;
    }
    
    public String getCodProveedor() {
        return cod_proveedor;
    }

    public void setCodProveedor(String cod_proveedor) {
        this.cod_proveedor = cod_proveedor;
    }
    
    public String getNombreProveedor() {
        return nombre_proveedor;
    }

    public void setNombreProveedor(String nombre_proveedor) {
        this.nombre_proveedor = nombre_proveedor;
    }
    
    public String getFormaPago() {
        return forma_pago;
    }

    public void setFormaPago(String forma_pago) {
        this.forma_pago = forma_pago;
    }
    
    public String getEstadoOcompraApoteosys() {
        return estado_ocompra_apoteosys;
    }

    public void setEstadoOcompraApoteosys(String estado_ocompra_apoteosys) {
        this.estado_ocompra_apoteosys = estado_ocompra_apoteosys;
    }

}
