package com.tsp.operation.model.beans;

public class StdJob {

    private String work_group;
    private float porcentaje_maximo_anticipo;
    private String dstrct_code;
    private String std_job_no;
    private String std_job_desc;
    private String origin_code;
    private String destination_code;
    private String origin_name;
    private String destination_name;
    private String estado;
    private String Usuario_creacion;
    private java.util.Date fecha_creacion;
    private String Usuario_actualizacion;
    private java.util.Date fecha_actualizacion;
    private String Tipo_recurso1;
    private String Tipo_recurso2;
    private String Tipo_recurso3;
    private String Tipo_recurso4;
    private String Tipo_recurso5;
    private String recurso1;
    private String recurso2;
    private String recurso3;
    private String recurso4;
    private String recurso5;
    private String prioridad1;
    private String prioridad2;
    private String prioridad3;
    private String prioridad4;
    private String prioridad5;
    private boolean bloquea_despacho;
    //Diogenes 16.11.05
    private String frontera_asoc;
    
    public StdJob() {
    }
    public float getPorcentaje_maximo_anticipo() {
        return porcentaje_maximo_anticipo;
    }

    public void setWork_group( String work_group ) {
        this.work_group = work_group;
    }
    public void setstd_job_desc (String desc){
        this.std_job_desc = desc;
    }

    public void setPorcentaje_maximo_anticipo( float porcentaje_maximo_anticipo ) {
        this.porcentaje_maximo_anticipo = porcentaje_maximo_anticipo;
    }

    public String getWork_group() {
        return work_group;
    }
      
    public void setdstrct_code (String dstrct_code){
        this.dstrct_code=dstrct_code;
    }
    public void setstd_job_no (String std_job_no){
        this.std_job_no=std_job_no;
    }
    public void setorigin_code (String origin_code){
        this.origin_code=origin_code;
    }
    public void setdestination_code (String destination_code){
        this.destination_code=destination_code;
    }
    public void setorigin_name (String origin_name){
        this.origin_name=origin_name;
    }
    public void setdestination_name (String destination_name){
        this.destination_name=destination_name;
    }
  
    public void setestado (String estado){
        this.estado=estado;
    }
    public void setUsuario_creacion (String Usuario_creacion){
        this.Usuario_creacion=Usuario_creacion;
    }
    public void setfecha_creacion (java.util.Date fecha_creacion){
        this.fecha_creacion=fecha_creacion;
    }
    public void setUsuario_actualizacion (String Usuario_actualizacion){
        this.Usuario_actualizacion=Usuario_actualizacion;
    }
    public void setfecha_actualizacion (java.util.Date fecha_actualizacion){
        this.fecha_actualizacion=fecha_actualizacion;
    }
    public void setTipo_recurso1 (String Tipo_recurso1){
        this.Tipo_recurso1=Tipo_recurso1;
    }
    public void setTipo_recurso2 (String Tipo_recurso2){
        this.Tipo_recurso2=Tipo_recurso2;
    }
    public void setTipo_recurso3 (String Tipo_recurso3){
        this.Tipo_recurso3=Tipo_recurso3;
    }
    public void setTipo_recurso4 (String Tipo_recurso4){
        this.Tipo_recurso4=Tipo_recurso4;
    }
    public void setTipo_recurso5 (String Tipo_recurso5){
        this.Tipo_recurso5=Tipo_recurso5;
    }
    public void setrecurso1 (String recurso1){
        this.recurso1=recurso1;
    }
    public void setrecurso2 (String recurso2){
        this.recurso2=recurso2;
    }
    public void setrecurso3 (String recurso3){
        this.recurso3=recurso3;
    }
    public void setrecurso4 (String recurso4){
        this.recurso4=recurso4;
    }
    public void setrecurso5 (String recurso5){
        this.recurso5=recurso5;
    }
    public void setprioridad1 (String prioridad1){
        this.prioridad1=prioridad1;
    }
    public void setprioridad2 (String prioridad2){
        this.prioridad2=prioridad2;
    }
    public void setprioridad3 (String prioridad3){
        this.prioridad3=prioridad3;
    }
    public void setprioridad4 (String prioridad4){
        this.prioridad4=prioridad4;
    }
    public void setprioridad5 (String prioridad5){
        this.prioridad5=prioridad5;
    }
    
    public String getdstrct_code (){
        return dstrct_code;
    }
     public String getstd_job_desc (){
        return this.std_job_desc;
    }
    public String getstd_job_no (){
        return std_job_no;
    }
    public String getorigin_code (){
        return origin_code;
    }
    public String getdestination_code (){
        return destination_code;
    }
    public String getorigin_name (){
        return origin_name;
    }
    public String getdestination_name (){
        return destination_name;
    }
    public String getestado (){
        return estado;
    }
    public String getUsuario_creacion (){
        return Usuario_creacion;
    }
    public java.util.Date getfecha_creacion (){
        return fecha_creacion;
    }
    public String getUsuario_actualizacion (){
        return Usuario_actualizacion;
    }
    public java.util.Date getfecha_actualizacion (){
        return fecha_actualizacion;
    }
    public String getTipo_recurso1 (){
        return Tipo_recurso1;
    }
    public String getTipo_recurso2 (){
        return Tipo_recurso2;
    }
    public String getTipo_recurso3 (){
        return Tipo_recurso3;
    }
    public String getTipo_recurso4 (){
        return Tipo_recurso4;
    }
    public String getTipo_recurso5 (){
        return Tipo_recurso5;
    }
    public String getrecurso1 (){
        return recurso1;
    }
    public String getrecurso2 (){
        return recurso2;
    }
    public String getrecurso3 (){
        return recurso3;
    }
    public String getrecurso4 (){
        return recurso4;
    }
    public String getrecurso5 (){
        return recurso5;
    }
    public String getprioridad1 (){
        return prioridad1;
    }
    public String getprioridad2 (){
        return prioridad2;
    }
    public String getprioridad3 (){
        return prioridad3;
    }
    public String getprioridad4 (){
        return prioridad4;
    }
    public String getprioridad5 (){
        return prioridad5;
    }
    
    /**
     * Getter for property bloquea_despacho.
     * @return Value of property bloquea_despacho.
     */
    public boolean isBloquea_despacho() {
        return bloquea_despacho;
    }
    
    /**
     * Setter for property bloquea_despacho.
     * @param bloquea_despacho New value of property bloquea_despacho.
     */
    public void setBloquea_despacho(boolean bloquea_despacho) {
        this.bloquea_despacho = bloquea_despacho;
    }
    
    /**
     * Getter for property frontera_asoc.
     * @return Value of property frontera_asoc.
     */
    public java.lang.String getFrontera_asoc() {
        return frontera_asoc;
    }    
    
    /**
     * Setter for property frontera_asoc.
     * @param frontera_asoc New value of property frontera_asoc.
     */
    public void setFrontera_asoc(java.lang.String frontera_asoc) {
        this.frontera_asoc = frontera_asoc;
    }
    
}
