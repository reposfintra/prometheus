
package com.tsp.operation.model.beans;

public class CodeValue {
   private String codigo;
   private String descripcion;
   private String referencia;
  
 //-- Metodos
   
   public CodeValue() {}
 
 // Sets
   public void setCodigo(String id){
       this.codigo=id;
   }
   
   public void setDescripcion(String descripcion){
       this.descripcion=descripcion;
   }
   
   public void setReferencia(String referencia){
       this.referencia=referencia;
   }
   
// Gets  
   public String getCodigo(){
       return this.codigo;
   }
    
   public String getDescripcion(){
       return this.descripcion;
   }
    
   public String getReferencia(){
       return this.referencia;
   }
   
   
   
}

