/*
 * Pais.java
 *
 * Created on 24 de febrero de 2005, 08:07 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  DIBASMO
 */
public class Pais implements Serializable {
    private String country_code;
    private String country_name;
    private String reg_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    
     
    
    
    /** Creates a new instance of Pais */
    public static Pais load(ResultSet rs)throws SQLException {
        Pais pais = new Pais();  
        pais.setReg_status( rs.getString("reg_status") );
        pais.setCountry_code( rs.getString("country_code") );
        pais.setCountry_name( rs.getString("country_name") );
        pais.setLast_update( rs.getDate("last_update") );
        pais.setUser_update( rs.getString("user_update") );
        pais.setCreation_date( rs.getDate("creation_date") );
        pais.setCreation_user( rs.getString("creation_user") ); 
        
        return pais;
    }
    
    
    //=====================================================
    //              Metodos de acceso 
    //=====================================================
    
    public void setCountry_code(String country_code){
    
        this.country_code= country_code;
       
    }
    public void setCountry_name(String country_name){
        
        this.country_name= country_name;
        
    }
    public void setReg_status(String reg_status){
        this.reg_status = reg_status;
    }
    public void setLast_update(java.util.Date last_update){
        
        this.last_update = last_update;
        
    }
    public void setUser_update(String user_update){        
        this.user_update= user_update;        
    }
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date = creation_date;
        
    }
    public void setCreation_user(String creation_user){        
       
        this.creation_user= creation_user;        
        
    }
    public String getCountry_code(){
        return country_code;
    }
    public String getCountry_name(){
        return country_name;
    }
    public String getReg_status(){
        return reg_status;
    }
    public java.util.Date getLast_update( ){
        
        return last_update;
        
    }
    public String getUser_update( ){        
        
        return user_update;        
        
    }
    public java.util.Date getCreation_date( ){
      
        return creation_date;
        
    }    
    public String getCreation_user( ){        
        return creation_user;        
    }
    
}
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    