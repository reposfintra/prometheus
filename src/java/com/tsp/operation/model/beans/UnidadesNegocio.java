/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author desarrollo
 */
public class UnidadesNegocio {
    private int id;
    private String codigo;
    private String descripcion;
    private String ciudad;
    private String codCentralRiesgo;
    private Convenio convenios;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the codCentralRiesgo
     */
    public String getCodCentralRiesgo() {
        return codCentralRiesgo;
    }

    /**
     * @param codCentralRiesgo the codCentralRiesgo to set
     */
    public void setCodCentralRiesgo(String codCentralRiesgo) {
        this.codCentralRiesgo = codCentralRiesgo;
    }

    /**
     * @return the convenios
     */
    public Convenio getConvenios() {
        return convenios;
    }

    /**
     * @param convenios the convenios to set
     */
    public void setConvenios(Convenio convenios) {
        this.convenios = convenios;
    }
    
}
