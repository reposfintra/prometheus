/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author dvalencia
 */
public class EstadoCuentaIngresos {

    private String cod_neg;
    private String nombre_cliente;
    private String nit;
    private String fecha_negocio;
    private String num_doc_fen;
    private String fecha_vencimiento;
    private String fecha_ingreso;
    private String num_ingreso;
    private String valor_factura;
    private String vlr_ingreso;
    private String valor_aplicado;
    private String ixm;
    private String gac;
    private String valor_saldo;
    private String vr_negocio;
    


    public EstadoCuentaIngresos(String cod_neg, String nombre_cliente, String nit, String fecha_negocio) {
        this.cod_neg = cod_neg;
        this.nombre_cliente = nombre_cliente;
        this.nit = nit;
        this.fecha_negocio = fecha_negocio;
        this.num_doc_fen = num_doc_fen;
 
    }
    
    public EstadoCuentaIngresos() {
        
        
    }
    

    public String getCod_neg() {
        return cod_neg;
    }

    public void setCod_neg(String cod_neg) {
        this.cod_neg = cod_neg;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getFecha_negocio() {
        return fecha_negocio;
    }

    public void setFecha_negocio(String fecha_negocio) {
        this.fecha_negocio = fecha_negocio;
    }

   public String getNum_doc_fen() {
        return num_doc_fen;
    }

    public void setNum_doc_fen(String num_doc_fen) {
        this.num_doc_fen = num_doc_fen;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public String getNum_ingreso() {
        return num_ingreso;
    }

    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    public String getValor_factura() {
        return valor_factura;
    }

    public void setValor_factura(String valor_factura) {
        this.valor_factura = valor_factura;
    }

    public String getVlr_ingreso() {
        return vlr_ingreso;
    }

    public void setVlr_ingreso(String vlr_ingreso) {
        this.vlr_ingreso = vlr_ingreso;
    }

    public String getValor_aplicado() {
        return valor_aplicado;
    }

    public void setValor_aplicado(String valor_aplicado) {
        this.valor_aplicado = valor_aplicado;
    }

    public String getValor_saldo() {
        return valor_saldo;
    }

    public void setValor_saldo(String valor_saldo) {
        this.valor_saldo = valor_saldo;
    
}

    public String getIxm() {
        return ixm;
    }

    public void setIxm(String ixm) {
        this.ixm = ixm;
    }

    public String getGac() {
        return gac;
    }

    public void setGac(String gac) {
        this.gac = gac;
    }

    public String getVr_negocio() {
        return vr_negocio;
    }
    
     public void setVr_negocio(String vr_negocio) {
        this.vr_negocio = vr_negocio;
    
}
}
