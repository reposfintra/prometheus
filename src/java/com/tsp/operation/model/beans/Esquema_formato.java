/*
 * Esquema_formato.java
 *
 * Created on 18 de octubre de 2006, 05:40 PM
 */

package com.tsp.operation.model.beans;
import java.util.*;
import java.sql.*;

/**
 *
 * @author  EQUIPO13
 */
public class Esquema_formato {
    public String codigo_programa;
    public String nombre_programa;
    public String nombre_campo;
    public String nom_campo;
    public int posicion_inicial;
    public int posicion_final;
    public int orden;
    public String titulo;
    public String distrito;
    public String usuario;
    public String base;
    public String tipo;
    public String nom_tipo;
    
    /** Creates a new instance of Esquema_formato */
    public Esquema_formato () {
    }
    
    
    public static Esquema_formato load(ResultSet rs) throws SQLException {
        Esquema_formato esquema = new Esquema_formato();
        esquema.setCodigo_programa  ( rs.getString("cod_programa")!=null?rs.getString("cod_programa"):"" );
        esquema.setNombre_campo     ( rs.getString("nom_campo")!=null?rs.getString("nom_campo"):"" );
        esquema.setPosicion_inicial ( rs.getInt("pos_inicial") );
        esquema.setPosicion_final   ( rs.getInt("pos_final") );
        esquema.setOrden            ( rs.getInt("orden") );
        esquema.setTitulo           ( rs.getString("titulo")!=null?rs.getString("titulo"):"" );
        esquema.setNombre_programa  ( rs.getString("nom_programa")!=null?rs.getString("nom_programa"):"" );
        esquema.setNom_campo        ( rs.getString("nombr_campo")!=null?rs.getString("nombr_campo"):"" );
        esquema.setTipo             ( rs.getString("tipo")!=null?rs.getString("tipo"):"" );
        esquema.setNom_tipo         ( rs.getString("nom_tipo")!=null?rs.getString("nom_tipo"):"" );
        return esquema;
    }
    
    /**
     * Getter for property codigo_programa.
     * @return Value of property codigo_programa.
     */
    public java.lang.String getCodigo_programa () {
        return codigo_programa;
    }
    
    /**
     * Setter for property codigo_programa.
     * @param codigo_programa New value of property codigo_programa.
     */
    public void setCodigo_programa (java.lang.String codigo_programa) {
        this.codigo_programa = codigo_programa;
    }
    
    /**
     * Getter for property nombre_campo.
     * @return Value of property nombre_campo.
     */
    public java.lang.String getNombre_campo () {
        return nombre_campo;
    }
    
    /**
     * Setter for property nombre_campo.
     * @param nombre_campo New value of property nombre_campo.
     */
    public void setNombre_campo (java.lang.String nombre_campo) {
        this.nombre_campo = nombre_campo;
    }
    
    /**
     * Getter for property orden.
     * @return Value of property orden.
     */
    public int getOrden () {
        return orden;
    }
    
    /**
     * Setter for property orden.
     * @param orden New value of property orden.
     */
    public void setOrden (int orden) {
        this.orden = orden;
    }
    
    /**
     * Getter for property posicion_final.
     * @return Value of property posicion_final.
     */
    public int getPosicion_final () {
        return posicion_final;
    }
    
    /**
     * Setter for property posicion_final.
     * @param posicion_final New value of property posicion_final.
     */
    public void setPosicion_final (int posicion_final) {
        this.posicion_final = posicion_final;
    }
    
    /**
     * Getter for property posicion_inicial.
     * @return Value of property posicion_inicial.
     */
    public int getPosicion_inicial () {
        return posicion_inicial;
    }
    
    /**
     * Setter for property posicion_inicial.
     * @param posicion_inicial New value of property posicion_inicial.
     */
    public void setPosicion_inicial (int posicion_inicial) {
        this.posicion_inicial = posicion_inicial;
    }
    
    /**
     * Getter for property titulo.
     * @return Value of property titulo.
     */
    public java.lang.String getTitulo () {
        return titulo;
    }
    
    /**
     * Setter for property titulo.
     * @param titulo New value of property titulo.
     */
    public void setTitulo (java.lang.String titulo) {
        this.titulo = titulo;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario () {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario (java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property nombre_programa.
     * @return Value of property nombre_programa.
     */
    public java.lang.String getNombre_programa () {
        return nombre_programa;
    }
    
    /**
     * Setter for property nombre_programa.
     * @param nombre_programa New value of property nombre_programa.
     */
    public void setNombre_programa (java.lang.String nombre_programa) {
        this.nombre_programa = nombre_programa;
    }
    
    /**
     * Getter for property nom_campo.
     * @return Value of property nom_campo.
     */
    public java.lang.String getNom_campo () {
        return nom_campo;
    }
    
    /**
     * Setter for property nom_campo.
     * @param nom_campo New value of property nom_campo.
     */
    public void setNom_campo (java.lang.String nom_campo) {
        this.nom_campo = nom_campo;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo () {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo (java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property nom_tipo.
     * @return Value of property nom_tipo.
     */
    public java.lang.String getNom_tipo () {
        return nom_tipo;
    }
    
    /**
     * Setter for property nom_tipo.
     * @param nom_tipo New value of property nom_tipo.
     */
    public void setNom_tipo (java.lang.String nom_tipo) {
        this.nom_tipo = nom_tipo;
    }
    
}
