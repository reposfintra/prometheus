  /***************************************
    * Nombre Clase ............. AnticiposTerceros.java
    * Descripci�n  .. . . . . .  Obtiene datos para generar anticipos por terceros
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  04/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.beans;


import java.util.*;
import com.tsp.util.Utility;




public class AnticiposTerceros {
    
  
          private int     id;
          private int     secuencia                 ;
          private String  dstrct                    = "";                   
          private String  agency_id                 = "";                   
          private String  pla_owner                 = "";                   
          private String  planilla                  = "";                   
          private String  supplier                  = "";                   
          private String  proveedor_anticipo        = "";          
          private String  concept_code              = "";                
          private double  vlr                       = 0;                    
          private double  vlr_for                   = 0;                    
          private String  currency                  = "";                    
          private String  fecha_anticipo            = "";              
          private String  fecha_autorizacion        = "";          
          private String  user_autorizacion         = "";          
          private String  fecha_transferencia       = "";
          
          
          private String  periodo_contabilizacion   = "";
          private String  docum_contable            = "";
          
          
          private String  banco_transferencia       = "";         
          private String  cuenta_transferencia      = "";        
          private String  tcta_transferencia        = "";          
          private String  user_transferencia        = "";          
          private String  fecha_migracion           = "";             
          private String  user_migracion            = "";              
          private String  factura_mims              = "";                
          private double  vlr_mims_tercero          = 0;            
          private double  vlr_mims_propietario      = 0;         
          private String  estado_pago_tercero       = "";          
          private String  estado_desc_propietario   = "";      
          private String  fecha_pago_tercero        = "";           
          private String  fecha_desc_propietario    = "";       
          private String  cheque_pago_tercero       = "";         
          private String  cheque_desc_propietario   = "";      
          private String  corrida_pago_tercero      = "";         
          private String  corrida_desc_propietario  = "";
          private String  aprobado                  = "";
          private String  transferido               = "";     
          private String  banco                     = "";
          private String  sucursal                  = "";
          private String  nombre_cuenta             = "";
          private String  cuenta                    = "";
          private String  tipo_cuenta               = "";
          private String  nit_cuenta                = "";
          private String  reg_status                = "";


          private String  obs_anulacion             = "";
          
    
          private List    cuentasBancos             = new LinkedList();
          
          
          private  String  nombreAgencia            = "";
          private  String  nombreProveedor          = "";
          private  String  nombrePropietario        = "";
          
          private  boolean tieneCuenta              = false;
           private String  fecha_envio                = "";
          
          
          
          
          private  double   porcentaje              = 0;
          private  double   vlrDescuento            = 0;          
          private  double   vlrNeto                 = 0;
          private  double   vlrComision             = 0;
          private  double   vlrConsignar            = 0;
          
          private  String   reanticipo              = "";
          
          
          private  String   conductor               = "";
          private  String   nombreConductor         = "";
          
          
          
          private String   liquidacion              = "";
          private String   transferencia            = "";
          
          private String asesor           = "";
          private String referenciado     = "";
          private String status           = "";
          private String observacion      = "";
          private String usuario_creacion = "";
          
          //navi
          private String fecha_creacion   = "";
          private String des_concept      = "";
          private String dato_generico      = "";

           private String diferencia      = "";//diferencia entre fecha anticipo y fecha transferencia
          private String rango_diferencia      = "";
          
          private String fecha_trans_gasolina="";
          private String user_trans_gasolina="";
          private String nombre_eds;
          private String lote_transferencia="";
          

          
          
          
    
    public AnticiposTerceros() {
    }
    
    
    
    
    public String getObs_anulacion() {
        return obs_anulacion;
    }

    public void setObs_anulacion(String obs_anulacion) {
        this.obs_anulacion = obs_anulacion;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }
    /**
     * Getter for property agency_id.
     * @return Value of property agency_id.
     */
    public java.lang.String getAgency_id() {
        return agency_id;
    }    
    
    /**
     * Setter for property agency_id.
     * @param agency_id New value of property agency_id.
     */
    public void setAgency_id(java.lang.String agency_id) {
        this.agency_id = agency_id;
    }    
    
    /**
     * Getter for property banco_transferencia.
     * @return Value of property banco_transferencia.
     */
    public java.lang.String getBanco_transferencia() {
        return banco_transferencia;
    }    
    
    /**
     * Setter for property banco_transferencia.
     * @param banco_transferencia New value of property banco_transferencia.
     */
    public void setBanco_transferencia(java.lang.String banco_transferencia) {
        this.banco_transferencia = banco_transferencia;
    }    
    
    /**
     * Getter for property cheque_desc_propietario.
     * @return Value of property cheque_desc_propietario.
     */
    public java.lang.String getCheque_desc_propietario() {
        return cheque_desc_propietario;
    }
    
    /**
     * Setter for property cheque_desc_propietario.
     * @param cheque_desc_propietario New value of property cheque_desc_propietario.
     */
    public void setCheque_desc_propietario(java.lang.String cheque_desc_propietario) {
        this.cheque_desc_propietario = cheque_desc_propietario;
    }
    
    /**
     * Getter for property cheque_pago_tercero.
     * @return Value of property cheque_pago_tercero.
     */
    public java.lang.String getCheque_pago_tercero() {
        return cheque_pago_tercero;
    }
    
    /**
     * Setter for property cheque_pago_tercero.
     * @param cheque_pago_tercero New value of property cheque_pago_tercero.
     */
    public void setCheque_pago_tercero(java.lang.String cheque_pago_tercero) {
        this.cheque_pago_tercero = cheque_pago_tercero;
    }
    
    /**
     * Getter for property concept_code.
     * @return Value of property concept_code.
     */
    public java.lang.String getConcept_code() {
        return concept_code;
    }
    
    /**
     * Setter for property concept_code.
     * @param concept_code New value of property concept_code.
     */
    public void setConcept_code(java.lang.String concept_code) {
        this.concept_code = concept_code;
    }
    
    /**
     * Getter for property corrida_desc_propietario.
     * @return Value of property corrida_desc_propietario.
     */
    public java.lang.String getCorrida_desc_propietario() {
        return corrida_desc_propietario;
    }
    
    /**
     * Setter for property corrida_desc_propietario.
     * @param corrida_desc_propietario New value of property corrida_desc_propietario.
     */
    public void setCorrida_desc_propietario(java.lang.String corrida_desc_propietario) {
        this.corrida_desc_propietario = corrida_desc_propietario;
    }
    
    /**
     * Getter for property corrida_pago_tercero.
     * @return Value of property corrida_pago_tercero.
     */
    public java.lang.String getCorrida_pago_tercero() {
        return corrida_pago_tercero;
    }
    
    /**
     * Setter for property corrida_pago_tercero.
     * @param corrida_pago_tercero New value of property corrida_pago_tercero.
     */
    public void setCorrida_pago_tercero(java.lang.String corrida_pago_tercero) {
        this.corrida_pago_tercero = corrida_pago_tercero;
    }
    
    /**
     * Getter for property cuenta_transferencia.
     * @return Value of property cuenta_transferencia.
     */
    public java.lang.String getCuenta_transferencia() {
        return cuenta_transferencia;
    }
    
    /**
     * Setter for property cuenta_transferencia.
     * @param cuenta_transferencia New value of property cuenta_transferencia.
     */
    public void setCuenta_transferencia(java.lang.String cuenta_transferencia) {
        this.cuenta_transferencia = cuenta_transferencia;
    }
    
    /**
     * Getter for property cuentasBancos.
     * @return Value of property cuentasBancos.
     */
    public java.util.List getCuentasBancos() {
        return cuentasBancos;
    }
    
    /**
     * Setter for property cuentasBancos.
     * @param cuentasBancos New value of property cuentasBancos.
     */
    public void setCuentasBancos(java.util.List cuentasBancos) {
        this.cuentasBancos = cuentasBancos;
    }
    
    /**
     * Getter for property currency.
     * @return Value of property currency.
     */
    public java.lang.String getCurrency() {
        return currency;
    }
    
    /**
     * Setter for property currency.
     * @param currency New value of property currency.
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property estado_desc_propietario.
     * @return Value of property estado_desc_propietario.
     */
    public java.lang.String getEstado_desc_propietario() {
        return estado_desc_propietario;
    }
    
    /**
     * Setter for property estado_desc_propietario.
     * @param estado_desc_propietario New value of property estado_desc_propietario.
     */
    public void setEstado_desc_propietario(java.lang.String estado_desc_propietario) {
        this.estado_desc_propietario = estado_desc_propietario;
    }
    
    /**
     * Getter for property estado_pago_tercero.
     * @return Value of property estado_pago_tercero.
     */
    public java.lang.String getEstado_pago_tercero() {
        return estado_pago_tercero;
    }
    
    /**
     * Setter for property estado_pago_tercero.
     * @param estado_pago_tercero New value of property estado_pago_tercero.
     */
    public void setEstado_pago_tercero(java.lang.String estado_pago_tercero) {
        this.estado_pago_tercero = estado_pago_tercero;
    }
    
    /**
     * Getter for property factura_mims.
     * @return Value of property factura_mims.
     */
    public java.lang.String getFactura_mims() {
        return factura_mims;
    }
    
    /**
     * Setter for property factura_mims.
     * @param factura_mims New value of property factura_mims.
     */
    public void setFactura_mims(java.lang.String factura_mims) {
        this.factura_mims = factura_mims;
    }
    
    /**
     * Getter for property fecha_anticipo.
     * @return Value of property fecha_anticipo.
     */
    public java.lang.String getFecha_anticipo() {
        return fecha_anticipo;
    }
    
    /**
     * Setter for property fecha_anticipo.
     * @param fecha_anticipo New value of property fecha_anticipo.
     */
    public void setFecha_anticipo(java.lang.String fecha_anticipo) {
        this.fecha_anticipo = fecha_anticipo;
    }
    
    /**
     * Getter for property fecha_autorizacion.
     * @return Value of property fecha_autorizacion.
     */
    public java.lang.String getFecha_autorizacion() {
        return fecha_autorizacion;
    }
    
    /**
     * Setter for property fecha_autorizacion.
     * @param fecha_autorizacion New value of property fecha_autorizacion.
     */
    public void setFecha_autorizacion(java.lang.String fecha_autorizacion) {
        this.fecha_autorizacion = fecha_autorizacion;
    }
    
    /**
     * Getter for property fecha_desc_propietario.
     * @return Value of property fecha_desc_propietario.
     */
    public java.lang.String getFecha_desc_propietario() {
        return fecha_desc_propietario;
    }
    
    /**
     * Setter for property fecha_desc_propietario.
     * @param fecha_desc_propietario New value of property fecha_desc_propietario.
     */
    public void setFecha_desc_propietario(java.lang.String fecha_desc_propietario) {
        this.fecha_desc_propietario = fecha_desc_propietario;
    }
    
    /**
     * Getter for property fecha_migracion.
     * @return Value of property fecha_migracion.
     */
    public java.lang.String getFecha_migracion() {
        return fecha_migracion;
    }
    
    /**
     * Setter for property fecha_migracion.
     * @param fecha_migracion New value of property fecha_migracion.
     */
    public void setFecha_migracion(java.lang.String fecha_migracion) {
        this.fecha_migracion = fecha_migracion;
    }
    
    /**
     * Getter for property fecha_pago_tercero.
     * @return Value of property fecha_pago_tercero.
     */
    public java.lang.String getFecha_pago_tercero() {
        return fecha_pago_tercero;
    }
    
    /**
     * Setter for property fecha_pago_tercero.
     * @param fecha_pago_tercero New value of property fecha_pago_tercero.
     */
    public void setFecha_pago_tercero(java.lang.String fecha_pago_tercero) {
        this.fecha_pago_tercero = fecha_pago_tercero;
    }
    
    /**
     * Getter for property fecha_transferencia.
     * @return Value of property fecha_transferencia.
     */
    public java.lang.String getFecha_transferencia() {
        return fecha_transferencia;
    }
    
    /**
     * Setter for property fecha_transferencia.
     * @param fecha_transferencia New value of property fecha_transferencia.
     */
    public void setFecha_transferencia(java.lang.String fecha_transferencia) {
        this.fecha_transferencia = fecha_transferencia;
    }
    
    /**
     * Getter for property periodo_contabilizacion.
     * @return Value of property periodo_contabilizacion.
     */
    public java.lang.String getPeriodo_Contabilizacion() {
        return periodo_contabilizacion;
    }
    
    /**
     * Setter for property periodo_contabilizacion.
     * @param periodo_contabilizacion New value of property periodo_contabilizacion.
     */
    public void setPeriodo_Contabilizacion(java.lang.String periodo_contabilizacion) {
        this.periodo_contabilizacion = periodo_contabilizacion;
    }    
    
    /**
     * Getter for property docum_contable.
     * @return Value of property docum_contable.
     */
    public java.lang.String getDocum_Contable() {
        return docum_contable;
    }
    
    /**
     * Setter for property docum_contable.
     * @param docum_contable New value of property docum_contable.
     */
    public void setDocum_Contable(java.lang.String docum_contable) {
        this.docum_contable = docum_contable;
    }    

            
    /**
     * Getter for property pla_owner.
     * @return Value of property pla_owner.
     */
    public java.lang.String getPla_owner() {
        return pla_owner;
    }
    
    /**
     * Setter for property pla_owner.
     * @param pla_owner New value of property pla_owner.
     */
    public void setPla_owner(java.lang.String pla_owner) {
        this.pla_owner = pla_owner;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property proveedor_anticipo.
     * @return Value of property proveedor_anticipo.
     */
    public java.lang.String getProveedor_anticipo() {
        return proveedor_anticipo;
    }
    
    /**
     * Setter for property proveedor_anticipo.
     * @param proveedor_anticipo New value of property proveedor_anticipo.
     */
    public void setProveedor_anticipo(java.lang.String proveedor_anticipo) {
        this.proveedor_anticipo = proveedor_anticipo;
    }
    
    /**
     * Getter for property supplier.
     * @return Value of property supplier.
     */
    public java.lang.String getSupplier() {
        return supplier;
    }
    
    /**
     * Setter for property supplier.
     * @param supplier New value of property supplier.
     */
    public void setSupplier(java.lang.String supplier) {
        this.supplier = supplier;
    }
    
    /**
     * Getter for property tcta_transferencia.
     * @return Value of property tcta_transferencia.
     */
    public java.lang.String getTcta_transferencia() {
        return tcta_transferencia;
    }
    
    /**
     * Setter for property tcta_transferencia.
     * @param tcta_transferencia New value of property tcta_transferencia.
     */
    public void setTcta_transferencia(java.lang.String tcta_transferencia) {
        this.tcta_transferencia = tcta_transferencia;
    }
    
    /**
     * Getter for property user_autorizacion.
     * @return Value of property user_autorizacion.
     */
    public java.lang.String getUser_autorizacion() {
        return user_autorizacion;
    }
    
    /**
     * Setter for property user_autorizacion.
     * @param user_autorizacion New value of property user_autorizacion.
     */
    public void setUser_autorizacion(java.lang.String user_autorizacion) {
        this.user_autorizacion = user_autorizacion;
    }
    
    /**
     * Getter for property user_migracion.
     * @return Value of property user_migracion.
     */
    public java.lang.String getUser_migracion() {
        return user_migracion;
    }
    
    /**
     * Setter for property user_migracion.
     * @param user_migracion New value of property user_migracion.
     */
    public void setUser_migracion(java.lang.String user_migracion) {
        this.user_migracion = user_migracion;
    }
    
    /**
     * Getter for property user_transferencia.
     * @return Value of property user_transferencia.
     */
    public java.lang.String getUser_transferencia() {
        return user_transferencia;
    }
    
    /**
     * Setter for property user_transferencia.
     * @param user_transferencia New value of property user_transferencia.
     */
    public void setUser_transferencia(java.lang.String user_transferencia) {
        this.user_transferencia = user_transferencia;
    }
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public double getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }
    
    /**
     * Getter for property vlr_for.
     * @return Value of property vlr_for.
     */
    public double getVlr_for() {
        return vlr_for;
    }
    
    /**
     * Setter for property vlr_for.
     * @param vlr_for New value of property vlr_for.
     */
    public void setVlr_for(double vlr_for) {
        this.vlr_for = vlr_for;
    }
    
    /**
     * Getter for property vlr_mims_propietario.
     * @return Value of property vlr_mims_propietario.
     */
    public double getVlr_mims_propietario() {
        return vlr_mims_propietario;
    }
    
    /**
     * Setter for property vlr_mims_propietario.
     * @param vlr_mims_propietario New value of property vlr_mims_propietario.
     */
    public void setVlr_mims_propietario(double vlr_mims_propietario) {
        this.vlr_mims_propietario = vlr_mims_propietario;
    }
    
    /**
     * Getter for property vlr_mims_tercero.
     * @return Value of property vlr_mims_tercero.
     */
    public double getVlr_mims_tercero() {
        return vlr_mims_tercero;
    }
    
    /**
     * Setter for property vlr_mims_tercero.
     * @param vlr_mims_tercero New value of property vlr_mims_tercero.
     */
    public void setVlr_mims_tercero( double vlr_mims_tercero) {
        this.vlr_mims_tercero = vlr_mims_tercero;
    }
    
    /**
     * Getter for property aprobado.
     * @return Value of property aprobado.
     */
    public java.lang.String getAprobado() {
        return aprobado;
    }
    
    /**
     * Setter for property aprobado.
     * @param aprobado New value of property aprobado.
     */
    public void setAprobado(java.lang.String aprobado) {
        this.aprobado = aprobado;
    }
    
    /**
     * Getter for property transferido.
     * @return Value of property transferido.
     */
    public java.lang.String getTransferido() {
        return transferido;
    }
    
    /**
     * Setter for property transferido.
     * @param transferido New value of property transferido.
     */
    public void setTransferido(java.lang.String transferido) {
        this.transferido = transferido;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property nombre_cuenta.
     * @return Value of property nombre_cuenta.
     */
    public java.lang.String getNombre_cuenta() {
        return nombre_cuenta;
    }
    
    /**
     * Setter for property nombre_cuenta.
     * @param nombre_cuenta New value of property nombre_cuenta.
     */
    public void setNombre_cuenta(java.lang.String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property tipo_cuenta.
     * @return Value of property tipo_cuenta.
     */
    public java.lang.String getTipo_cuenta() {
        return tipo_cuenta;
    }
    
    /**
     * Setter for property tipo_cuenta.
     * @param tipo_cuenta New value of property tipo_cuenta.
     */
    public void setTipo_cuenta(java.lang.String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }
    
    /**
     * Getter for property nit_cuenta.
     * @return Value of property nit_cuenta.
     */
    public java.lang.String getNit_cuenta() {
        return nit_cuenta;
    }
    
    /**
     * Setter for property nit_cuenta.
     * @param nit_cuenta New value of property nit_cuenta.
     */
    public void setNit_cuenta(java.lang.String nit_cuenta) {
        this.nit_cuenta = nit_cuenta;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public int getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * Getter for property secuencia.
     * @return Value of property secuencia.
     */
    public int getSecuencia() {
        return secuencia;
    }
    
    /**
     * Setter for property secuencia.
     * @param secuencia New value of property secuencia.
     */
    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }
    
    /**
     * Getter for property nombreAgencia.
     * @return Value of property nombreAgencia.
     */
    public java.lang.String getNombreAgencia() {
        return nombreAgencia;
    }
    
    /**
     * Setter for property nombreAgencia.
     * @param nombreAgencia New value of property nombreAgencia.
     */
    public void setNombreAgencia(java.lang.String nombreAgencia) {
        this.nombreAgencia = nombreAgencia;
    }
    
    /**
     * Getter for property nombreProveedor.
     * @return Value of property nombreProveedor.
     */
    public java.lang.String getNombreProveedor() {
        return nombreProveedor;
    }
    
    /**
     * Setter for property nombreProveedor.
     * @param nombreProveedor New value of property nombreProveedor.
     */
    public void setNombreProveedor(java.lang.String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }
    
    /**
     * Getter for property nombrePropietario.
     * @return Value of property nombrePropietario.
     */
    public java.lang.String getNombrePropietario() {
        return nombrePropietario;
    }
    
    /**
     * Setter for property nombrePropietario.
     * @param nombrePropietario New value of property nombrePropietario.
     */
    public void setNombrePropietario(java.lang.String nombrePropietario) {
        this.nombrePropietario = nombrePropietario;
    }
    
    /**
     * Getter for property tieneCuenta.
     * @return Value of property tieneCuenta.
     */
    public boolean isTieneCuenta() {
        return tieneCuenta;
    }
    
    /**
     * Setter for property tieneCuenta.
     * @param tieneCuenta New value of property tieneCuenta.
     */
    public void setTieneCuenta(boolean tieneCuenta) {
        this.tieneCuenta = tieneCuenta;
    }
    
    /**
     * Getter for property porcentaje.
     * @return Value of property porcentaje.
     */
    public double getPorcentaje() {
        return porcentaje;
    }
    
    /**
     * Setter for property porcentaje.
     * @param porcentaje New value of property porcentaje.
     */
    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }
    
    /**
     * Getter for property vlrDescuento.
     * @return Value of property vlrDescuento.
     */
    public double getVlrDescuento() {
        return vlrDescuento;
    }
    
    /**
     * Setter for property vlrDescuento.
     * @param vlrDescuento New value of property vlrDescuento.
     */
    public void setVlrDescuento(double vlrDescuento) {
        this.vlrDescuento = vlrDescuento;
    }
    
    /**
     * Getter for property vlrNeto.
     * @return Value of property vlrNeto.
     */
    public double getVlrNeto() {
        return vlrNeto;
    }
    
    /**
     * Setter for property vlrNeto.
     * @param vlrNeto New value of property vlrNeto.
     */
    public void setVlrNeto(double vlrNeto) {
        this.vlrNeto = vlrNeto;
    }
    
    /**
     * Getter for property vlrComision.
     * @return Value of property vlrComision.
     */
    public double getVlrComision() {
        return vlrComision;
    }
    
    /**
     * Setter for property vlrComision.
     * @param vlrComision New value of property vlrComision.
     */
    public void setVlrComision(double vlrComision) {
        this.vlrComision = vlrComision;
    }
    
    /**
     * Getter for property vlrConsignar.
     * @return Value of property vlrConsignar.
     */
    public double getVlrConsignar() {
        return vlrConsignar;
    }
    
    /**
     * Setter for property vlrConsignar.
     * @param vlrConsignar New value of property vlrConsignar.
     */
    public void setVlrConsignar(double vlrConsignar) {
        this.vlrConsignar = vlrConsignar;
    }
    
    /**
     * Getter for property reanticipo.
     * @return Value of property reanticipo.
     */
    public java.lang.String getReanticipo() {
        return reanticipo;
    }
    
    /**
     * Setter for property reanticipo.
     * @param reanticipo New value of property reanticipo.
     */
    public void setReanticipo(java.lang.String reanticipo) {
        this.reanticipo = reanticipo;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property nombreConductor.
     * @return Value of property nombreConductor.
     */
    public java.lang.String getNombreConductor() {
        return nombreConductor;
    }
    
    /**
     * Setter for property nombreConductor.
     * @param nombreConductor New value of property nombreConductor.
     */
    public void setNombreConductor(java.lang.String nombreConductor) {
        this.nombreConductor = nombreConductor;
    }
    
    /**
     * Getter for property liquidacion.
     * @return Value of property liquidacion.
     */
    public java.lang.String getLiquidacion() {
        return liquidacion;
    }
    
    /**
     * Setter for property liquidacion.
     * @param liquidacion New value of property liquidacion.
     */
    public void setLiquidacion(java.lang.String liquidacion) {
        this.liquidacion = liquidacion;
    }
    
    /**
     * Getter for property transferencia.
     * @return Value of property transferencia.
     */
    public java.lang.String getTransferencia() {
        return transferencia;
    }
    
    /**
     * Setter for property transferencia.
     * @param transferencia New value of property transferencia.
     */
    public void setTransferencia(java.lang.String transferencia) {
        this.transferencia = transferencia;
    }
    /**
     * Getter for property asesor.
     * @return Value of property asesor.
     */
    public java.lang.String getAsesor() {
        return asesor;
    }
    
    /**
     * Setter for property asesor.
     * @param asesor New value of property asesor.
     */
    public void setAsesor(java.lang.String asesor) {
        this.asesor = asesor;
    }
    
    /**
     * Getter for property referenciado.
     * @return Value of property referenciado.
     */
    public java.lang.String getReferenciado() {
        return referenciado;
    }
    
    /**
     * Setter for property referenciado.
     * @param referenciado New value of property referenciado.
     */
    public void setReferenciado(java.lang.String referenciado) {
        this.referenciado = referenciado;
    }
    
    /**
     * Getter for property status.
     * @return Value of property status.
     */
    public java.lang.String getStatus() {
        return status;
    }
    
    /**
     * Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }




    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
     /**
     * Getter for property des_concept.
     * @return Value of property des_concept.
     */
    public java.lang.String getDes_concept() {
        return des_concept;
    }    

    /**
     * Setter for property des_concept.
     * @param des_concept New value of property des_concept.
     */
    public void setDes_concept(java.lang.String des_concept) {
        this.des_concept = des_concept;
    }    
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }


        /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getDato_generico() {
        return dato_generico;
    }

    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setDato_generico(java.lang.String dato_generico) {
        this.dato_generico = dato_generico;
    }


        public String getFecha_envio() {
        return fecha_envio;
    }

    public void setFecha_envio(String fecha_envio) {
        this.fecha_envio = fecha_envio;
    }



   public String getRango_diferencia()
   {
        return rango_diferencia;
   }

    public void setRango_diferencia(String rango_diferencia)
    {
        this.rango_diferencia = rango_diferencia;
    }


    public String getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(String diferencia) {
        this.diferencia = diferencia;
    }

    /**
     * @return the fecha_trans_gasolina
     */
    public String getFecha_trans_gasolina() {
        return fecha_trans_gasolina;
    }

    /**
     * @return the user_trans_gasolina
     */
    public String getUser_trans_gasolina() {
        return user_trans_gasolina;
    }

    /**
     * @param fecha_trans_gasolina the fecha_trans_gasolina to set
     */
    public void setFecha_trans_gasolina(String fecha_trans_gasolina) {
        this.fecha_trans_gasolina = fecha_trans_gasolina;
    }

    /**
     * @param user_trans_gasolina the user_trans_gasolina to set
     */
    public void setUser_trans_gasolina(String user_trans_gasolina) {
        this.user_trans_gasolina = user_trans_gasolina;
    }
    
    public void setNombre_eds(String Nombre_eds) {
        this.nombre_eds = Nombre_eds;
    }
    
    public String getNombre_eds() {
        return nombre_eds;
    }

    public String getLote_transferencia() {
        return lote_transferencia;
    }

    public void setLote_transferencia(String lote_transferencia) {
        this.lote_transferencia = lote_transferencia;
    }
    
    
    
}
