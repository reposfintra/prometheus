    /******************************************************************
 * Nombre ......................Series.java
 * Descripci�n..................Bean utilizado para la validaci�n de series
 * Autor........................Armando Oviedo
 * Fecha........................10/10/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.http.*;

public class Series implements Serializable {
    
    private String dstrct;
    private String agency_id;
    private String document_type;
    private String prefix;
    private String serial_initial_no;
    private String serial_fished_no;
    private int last_number;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    private String branch_code;
    private String bank_account_no;
    private String Estado;
    private String Distrito;
    private String Codigo_Ciudad;
    private String Nombre_Ciudad;
    private String Codigo_Documento;
    private String Nombre_Documento;
    private String Banco;
    private String Agencia_Banco;
    private String Cuenta;
    private String Prefijo;
    private String N_Inicial;
    private String N_Final;
    private String L_Numero;
    private String Usuario;
    private String Nombre_Boton;
    
    //AOviedo 10.12.05
    private Vector listaSeries;
    
    //AMATURANA 16.04.2007
    private String concepto = "";
    private String[] usuarios;
    private int id;


    
    public static Series load(ResultSet rs) throws SQLException{
        try {
            Series datos = new Series();
            if(rs.getString(1).equals("A"))
                datos.setEstado("checked='ckecked'");
            else
                datos.setEstado("");
                datos.setDistrito(rs.getString(2));
                datos.setCodigo_Ciudad(rs.getString(3));
                datos.setNombre_Ciudad(rs.getString(4));
                datos.setCodigo_Doucmento(rs.getString(5));
                datos.setNombre_Doucmento(rs.getString(6));
                datos.setBanco(rs.getString(7));
                datos.setAgencia_Banco(rs.getString(8));
                datos.setCuenta(rs.getString(9));
                datos.setPrefijo(rs.getString(10));
                datos.setN_Inicial(rs.getString(11));
                datos.setN_Final(rs.getString(12));
                datos.setL_Numero(rs.getString(13));
                //AMATURANA 17.04.2007
                datos.setConcepto(rs.getString("concepto"));
                datos.setId(rs.getInt("id"));
            
            return datos;
        }
        catch(SQLException e) {
            throw new SQLException("Error en load 1 [Serie]...\n"+e.getMessage());
        }
    }
    
    public static Series load(HttpServletRequest request) throws Exception {
        try {
            Series datos = new Series();
            if(request.getParameter("Estado")!= null && request.getParameter("Estado").equals("Disabled"))
                datos.setEstado("disabled='disabled'");
            else
                datos.setEstado(" ");
            
                datos.setDistrito(request.getParameter("Distrito"));
                datos.setCodigo_Ciudad(request.getParameter("Ciudad"));
                datos.setCodigo_Doucmento(request.getParameter("Documento").split("~")[1]);
                datos.setBanco(request.getParameter("Banco"));
                datos.setAgencia_Banco(request.getParameter("Agencia"));
                datos.setCuenta(request.getParameter("Cuenta"));
                //datos.setPrefijo(request.getParameter("Prefijo"));
                datos.setN_Inicial(request.getParameter("Serie_Inicial"));
                datos.setN_Final(request.getParameter("Serie_Final"));
                datos.setL_Numero(request.getParameter("Ultimo_Numero"));
                datos.setUsuario(request.getParameter("Usuario"));
                datos.setNombre_Boton("Modificar");
                
                //AMATURANA 16.04.2007
                datos.setConcepto(request.getParameter("concept_code")!=null ? request.getParameter("concept_code") : "");
                datos.setPrefijo(request.getParameter("Prefijo")!=null ? request.getParameter("Prefijo") : "");
                String[] logins = {};
                if( request.getParameterValues("c_docSelec")!=null )
                    logins = request.getParameterValues("c_docSelec");
                datos.setUsuarios(logins);
                datos.setId(request.getParameter("id")!=null ? Integer.parseInt(request.getParameter("id")) : 0);
                
            return datos;
        }
        catch(Exception e) {
            throw new Exception("Error en load 2 [Serie]...\n"+e.getMessage());
        }
    }
    /******************************************************************
 * Nombre ......................Series.java
 * Descripci�n..................Bean utilizado para la validaci�n de series
 * Autor........................Armando Oviedo
 * Fecha........................10/10/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/
    public void addListaSeriesElement(SerieGroupValidation tmp){
        listaSeries.add(tmp);
    }
    public Series(){
        listaSeries = new Vector();
    }
    public void Restart(){
        listaSeries = new Vector();
    }
    public static Series loadV(ResultSet rs) throws Exception{
        Series datos = new Series();
        try{            
            datos.setDistrito(rs.getString("dstrct"));
            datos.setAgency_id(rs.getString("agency_id"));
            datos.setDocument_type(rs.getString("document_type"));
            datos.setBranch_code(rs.getString("branch_code"));
            datos.setBank_account_no(rs.getString("bank_account_no"));
            datos.setN_Inicial(rs.getString("serial_initial_no"));
            datos.setN_Final(rs.getString("serial_fished_no"));
            datos.setL_Numero(rs.getString("last_number"));
            datos.setCreation_date(rs.getDate("creation_date"));
            datos.setCreation_user(rs.getString("creation_user"));
        }
        catch(Exception ex){
            throw new Exception("Error en loadV [serie]. "+ex.getMessage());
        }
        return datos;
    }
    public Vector getListaSeries(){
        return listaSeries;
    }
//Termina Armando Oviedo
    public static Series Reiniciar(){
        Series datos = new Series();
        datos.setDistrito("");
        datos.setCodigo_Ciudad("");
        datos.setNombre_Ciudad("");
        datos.setCodigo_Doucmento("");
        datos.setNombre_Doucmento("");
        datos.setBanco("");
        datos.setAgencia_Banco("");
        datos.setCuenta("");
        datos.setPrefijo("");
        datos.setN_Inicial("");
        datos.setN_Final("");
        datos.setL_Numero("");
        datos.setUsuario("");
        datos.setNombre_Boton("Guardar");
        //AMATURANA 17.04.2007
        datos.setConcepto("");
        //AMATURANA 19.04.2007
        datos.setId(0);
        return datos;
    }
    
    //Setter
    public void setEstado(String valor) {
        this.Estado = valor;
    }
    
    public void setDistrito(String valor) {
        this.Distrito = valor;
    }
    
    public void setCodigo_Ciudad(String valor) {
        this.Codigo_Ciudad = valor;
    }
    
    public void setNombre_Ciudad(String valor) {
        this.Nombre_Ciudad = valor;
    }
    
    public void setCodigo_Doucmento(String valor) {
        this.Codigo_Documento = valor;
    }
    
    public void setNombre_Doucmento(String valor) {
        this.Nombre_Documento = valor;
    }
    
    public void setBanco(String valor) {
        this.Banco = valor;
    }
    
    public void setAgencia_Banco(String valor) {
        this.Agencia_Banco = valor;
    }
    
    public void setCuenta(String valor) {
        this.Cuenta = valor;
    }
    
    public void setPrefijo(String valor) {
        this.Prefijo = valor;
    }
    
    public void setN_Inicial(String valor) {
        this.N_Inicial = valor;
    }
    
    public void setN_Final(String valor) {
        this.N_Final = valor;
    }
    
    public void setL_Numero(String valor) {
        this.L_Numero = valor;
    }
    
    public void setUsuario(String valor) {
        this.Usuario = valor;
    }
    
    public void setNombre_Boton(String valor) {
        this.Nombre_Boton = valor;
    }
    //Getter
    public String getEstado() {
        return this.Estado;
    }
    
    public String getDistrito() {
        return this.Distrito;
    }
    
    public String getCodigo_Ciudad() {
        return this.Codigo_Ciudad;
    }
    
    public String getNombre_Ciudad() {
        return this.Nombre_Ciudad;
    }
    
    public String getCodigo_Documento() {
        return this.Codigo_Documento;
    }
    
    public String getNombre_Documento() {
        return this.Nombre_Documento;
    }
    
    public String getBanco() {
        return this.Banco;
    }
    
    public String getAgencia_Banco() {
        return this.Agencia_Banco;
    }
    
    public String getCuenta() {
        return this.Cuenta;
    }
    
    public String getPrefijo() {
        return this.Prefijo;
    }
    
    public String getN_Inicial() {
        return this.N_Inicial;
    }
    
    public String getN_Final() {
        return this.N_Final;
    }
    
    public String getL_Numero() {
        return this.L_Numero;
    }
    
    public String getUsuario() {
        return this.Usuario;
    }
    
    public String getNombre_Boton() {
        return this.Nombre_Boton;
    }
    
    public void setBank_account_no(String bank_account_no){
        
        this.bank_account_no=bank_account_no;
    }
    
    public String getBank_account_no(){
        
        return bank_account_no;
    }
    public void setBranch_code(String branch_code){
        
        this.branch_code=branch_code;
    }
    
    public String getBranch_code(){
        
        return branch_code;
    }
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    
    public void setAgency_id(String agency_id){
        
        this.agency_id=agency_id;
    }
    
    public String getAgency_id(){
        
        return agency_id;
    }
    
    public void setDocument_type(String document_type){
        
        this.document_type=document_type;
    }
    
    public String getDocument_type(){
        
        return document_type;
    }
    
    public void setPrefix(String prefix){
        
        this.prefix=prefix;
    }
    
    public String getPrefix(){
        
        return prefix;
    }
    
    public void setSerial_initial_no(String serial_initial_no){
        
        this.serial_initial_no=serial_initial_no;
    }
    
    public String getSerial_initial_no(){
        
        return serial_initial_no;
    }
    
    public void setSerial_fished_no(String serial_fished_no){
        
        this.serial_fished_no=serial_fished_no;
    }
    
    public String getSerial_fished_no(){
        
        return serial_fished_no;
    }
    
    public void setLast_number(int last_number){
        
        this.last_number=last_number;
    }
    
    public int getLast_number(){
        
        return last_number;
    }
    
    public void setLast_update(java.util.Date last_update){
        
        this.last_update=last_update;
    }
    
    public java.util.Date getLast_update(){
        
        return last_update;
    }
    
    public void setUser_update(String user_update){
        
        this.user_update=user_update;
    }
    
    public String getUser_update(){
        
        return user_update;
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property usuarios.
     * @return Value of property usuarios.
     */
    public java.lang.String[] getUsuarios() {
        return this.usuarios;
    }
    
    /**
     * Setter for property usuarios.
     * @param usuarios New value of property usuarios.
     */
    public void setUsuarios(java.lang.String[] usuarios) {
        this.usuarios = usuarios;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public int getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
}



