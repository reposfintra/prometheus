/*
 * DatosConsulta.java
 *
 * Created on 20 de julio de 2005, 04:47 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  amendez
 */
import java.util.*;
import java.io.*;

public class DatosConsulta implements Serializable{
    String id = "";
    String creador = "";
    TreeMap userAdd;
    String descripcion = "";
    String baseDeDatos = "";
    Hashtable paramNamesList;
    String query = "";
    
    /** Creates a new instance of DatosConsulta */
    public DatosConsulta() {
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property userAdd.
     * @return Value of property userAdd.
     */
    public java.util.TreeMap getUserAdd() {
        return userAdd;
    }
    
    /**
     * Setter for property userAdd.
     * @param userAdd New value of property userAdd.
     */
    public void setUserAdd(java.util.TreeMap userAdd) {
        this.userAdd = userAdd;
    }
    
    /**
     * Getter for property paramNamesList.
     * @return Value of property paramNamesList.
     */
    public java.util.Hashtable getParamNamesList() {
        return paramNamesList;
    }
    
    /**
     * Setter for property paramNamesList.
     * @param paramNamesList New value of property paramNamesList.
     */
    public void setParamNamesList(java.util.Hashtable paramNamesList) {
        this.paramNamesList = paramNamesList;
    }
    
    public void setParamNamesList(String params[]) {
        this.paramNamesList = new Hashtable();
        if (params!=null){
            for(int i = 0; i<params.length; i++){
               this.paramNamesList.put(params[i], params[i]); 
            }
        }
    }
    
    /**
     * Getter for property creador.
     * @return Value of property creador.
     */
    public java.lang.String getCreador() {
        return creador;
    }
    
    /**
     * Setter for property creador.
     * @param creador New value of property creador.
     */
    public void setCreador(java.lang.String creador) {
        this.creador = creador;
    }
    
    /**
     * Getter for property baseDeDatos.
     * @return Value of property baseDeDatos.
     */
    public java.lang.String getBaseDeDatos() {
        return baseDeDatos;
    }
    
    /**
     * Setter for property baseDeDatos.
     * @param baseDeDatos New value of property baseDeDatos.
     */
    public void setBaseDeDatos(java.lang.String baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
    }
    
    /**
     * Getter for property query.
     * @return Value of property query.
     */
    public java.lang.String getQuery() {
        return query;
    }
    
    /**
     * Setter for property query.
     * @param query New value of property query.
     */
    public void setQuery(java.lang.String query) {
        this.query = query;
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public java.lang.String getId() {
        return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }
}
