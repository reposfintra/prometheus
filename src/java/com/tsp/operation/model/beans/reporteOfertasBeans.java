/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class reporteOfertasBeans {

    public String id_solicitud;
    public String nombre_solicitud;
    public String id_cliente;
    public String nomcli;
    public String nic;
    public String num_os;
    public String tipo_solicitud;
    public String fecha_oferta;
    public String fecha_inicial;
    public String esquema;
    public String material;
    public String mano_obra;
    public String transporte;
    public String porc_administracion;
    public String administracion;
    public String porc_imprevisto;
    public String imprevisto;
    public String porc_utilidad;
    public String utilidad;
    public String bonificacion;
    public String opav;
    public String fintra;
    public String interventoria;
    public String provintegral;
    public String eca;
    public String base_iva_contratista;
    public String iva_contratista;
    public String iva_bonificacion;
    public String iva_opav;
    public String iva_fintra;
    public String iva_interventoria;
    public String iva_provintegral;
    public String iva_eca;
    public String financiar_sin_iva;
    public String iva;
    public String financiar_con_iva;
    public String precio_total;
    public String contratista;
    public String nombre_contratista;
    public String id_accion;
    public String estado;
    public String descripcion;
    public String distribucion;
    public String tipo;
    public String porc_opav;
    public String porc_fintra;
    public String porc_interventoria;
    public String porc_provintegral;
    public String porc_eca;
    public String porc_iva;
    public String valor_agregado;
    public String id_cliente_padre;
    public String nomcli_padre;
    public String descripcion_estado;
    public String costo_contratista;
    public String id;
    public String cod_proyecto;
    public String proyecto;
    public String cod_tipoCliente;
    public String tipoCliente;

    public String getId_solicitud() {
        return id_solicitud;
    }

    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public String getNombre_solicitud() {
        return nombre_solicitud;
    }

    public void setNombre_solicitud(String nombre_solicitud) {
        this.nombre_solicitud = nombre_solicitud;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getNum_os() {
        return num_os;
    }

    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    public String getTipo_solicitud() {
        return tipo_solicitud;
    }

    public void setTipo_solicitud(String tipo_solicitud) {
        this.tipo_solicitud = tipo_solicitud;
    }

    public String getFecha_oferta() {
        return fecha_oferta;
    }

    public void setFecha_oferta(String fecha_oferta) {
        this.fecha_oferta = fecha_oferta;
    }

    public String getEsquema() {
        return esquema;
    }

    public void setEsquema(String esquema) {
        this.esquema = esquema;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMano_obra() {
        return mano_obra;
    }

    public void setMano_obra(String mano_obra) {
        this.mano_obra = mano_obra;
    }

    public String getTransporte() {
        return transporte;
    }

    public void setTransporte(String transporte) {
        this.transporte = transporte;
    }

    public String getPorc_administracion() {
        return porc_administracion;
    }

    public void setPorc_administracion(String porc_administracion) {
        this.porc_administracion = porc_administracion;
    }

    public String getAdministracion() {
        return administracion;
    }

    public void setAdministracion(String administracion) {
        this.administracion = administracion;
    }

    public String getPorc_imprevisto() {
        return porc_imprevisto;
    }

    public void setPorc_imprevisto(String porc_imprevisto) {
        this.porc_imprevisto = porc_imprevisto;
    }

    public String getImprevisto() {
        return imprevisto;
    }

    public void setImprevisto(String imprevisto) {
        this.imprevisto = imprevisto;
    }

    public String getPorc_utilidad() {
        return porc_utilidad;
    }

    public void setPorc_utilidad(String porc_utilidad) {
        this.porc_utilidad = porc_utilidad;
    }

    public String getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(String utilidad) {
        this.utilidad = utilidad;
    }

    public String getBonificacion() {
        return bonificacion;
    }

    public void setBonificacion(String bonificacion) {
        this.bonificacion = bonificacion;
    }

    public String getOpav() {
        return opav;
    }

    public void setOpav(String opav) {
        this.opav = opav;
    }

    public String getFintra() {
        return fintra;
    }

    public void setFintra(String fintra) {
        this.fintra = fintra;
    }

    public String getInterventoria() {
        return interventoria;
    }

    public void setInterventoria(String interventoria) {
        this.interventoria = interventoria;
    }

    public String getProvintegral() {
        return provintegral;
    }

    public void setProvintegral(String provintegral) {
        this.provintegral = provintegral;
    }

    public String getEca() {
        return eca;
    }

    public void setEca(String eca) {
        this.eca = eca;
    }

    public String getBase_iva_contratista() {
        return base_iva_contratista;
    }

    public void setBase_iva_contratista(String base_iva_contratista) {
        this.base_iva_contratista = base_iva_contratista;
    }

    public String getIva_contratista() {
        return iva_contratista;
    }

    public void setIva_contratista(String iva_contratista) {
        this.iva_contratista = iva_contratista;
    }

    public String getIva_bonificacion() {
        return iva_bonificacion;
    }

    public void setIva_bonificacion(String iva_bonificacion) {
        this.iva_bonificacion = iva_bonificacion;
    }

    public String getIva_opav() {
        return iva_opav;
    }

    public void setIva_opav(String iva_opav) {
        this.iva_opav = iva_opav;
    }

    public String getIva_fintra() {
        return iva_fintra;
    }

    public void setIva_fintra(String iva_fintra) {
        this.iva_fintra = iva_fintra;
    }

    public String getIva_interventoria() {
        return iva_interventoria;
    }

    public void setIva_interventoria(String iva_interventoria) {
        this.iva_interventoria = iva_interventoria;
    }

    public String getIva_provintegral() {
        return iva_provintegral;
    }

    public void setIva_provintegral(String iva_provintegral) {
        this.iva_provintegral = iva_provintegral;
    }

    public String getIva_eca() {
        return iva_eca;
    }

    public void setIva_eca(String iva_eca) {
        this.iva_eca = iva_eca;
    }

    public String getFinanciar_sin_iva() {
        return financiar_sin_iva;
    }

    public void setFinanciar_sin_iva(String financiar_sin_iva) {
        this.financiar_sin_iva = financiar_sin_iva;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getFinanciar_con_iva() {
        return financiar_con_iva;
    }

    public void setFinanciar_con_iva(String financiar_con_iva) {
        this.financiar_con_iva = financiar_con_iva;
    }

    public String getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(String precio_total) {
        this.precio_total = precio_total;
    }

    public String getContratista() {
        return contratista;
    }

    public void setContratista(String contratista) {
        this.contratista = contratista;
    }

    public String getNombre_contratista() {
        return nombre_contratista;
    }

    public void setNombre_contratista(String nombre_contratista) {
        this.nombre_contratista = nombre_contratista;
    }

    public String getId_accion() {
        return id_accion;
    }

    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDistribucion() {
        return distribucion;
    }

    public void setDistribucion(String distribucion) {
        this.distribucion = distribucion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPorc_opav() {
        return porc_opav;
    }

    public void setPorc_opav(String porc_opav) {
        this.porc_opav = porc_opav;
    }

    public String getPorc_fintra() {
        return porc_fintra;
    }

    public void setPorc_fintra(String porc_fintra) {
        this.porc_fintra = porc_fintra;
    }

    public String getPorc_interventoria() {
        return porc_interventoria;
    }

    public void setPorc_interventoria(String porc_interventoria) {
        this.porc_interventoria = porc_interventoria;
    }

    public String getPorc_provintegral() {
        return porc_provintegral;
    }

    public void setPorc_provintegral(String porc_provintegral) {
        this.porc_provintegral = porc_provintegral;
    }

    public String getPorc_eca() {
        return porc_eca;
    }

    public void setPorc_eca(String porc_eca) {
        this.porc_eca = porc_eca;
    }

    public String getPorc_iva() {
        return porc_iva;
    }

    public void setPorc_iva(String porc_iva) {
        this.porc_iva = porc_iva;
    }

    public String getValor_agregado() {
        return valor_agregado;
    }

    public void setValor_agregado(String valor_agregado) {
        this.valor_agregado = valor_agregado;
    }

    public String getId_cliente_padre() {
        return id_cliente_padre;
    }

    public void setId_cliente_padre(String id_cliente_padre) {
        this.id_cliente_padre = id_cliente_padre;
    }

    public String getNomcli_padre() {
        return nomcli_padre;
    }

    public void setNomcli_padre(String nomcli_padre) {
        this.nomcli_padre = nomcli_padre;
    }

    public String getFecha_inicial() {
        return fecha_inicial;
    }

    public void setFecha_inicial(String fecha_inicial) {
        this.fecha_inicial = fecha_inicial;
    }

    public String getDescripcion_estado() {
        return descripcion_estado;
    }

    public void setDescripcion_estado(String descripcion_estado) {
        this.descripcion_estado = descripcion_estado;
    }

    public String getCosto_contratista() {
        return costo_contratista;
    }

    public void setCosto_contratista(String costo_contratista) {
        this.costo_contratista = costo_contratista;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCod_proyecto() {
        return cod_proyecto;
    }

    public void setCod_proyecto(String cod_proyecto) {
        this.cod_proyecto = cod_proyecto;
    }

    public String getProyecto() {
        return proyecto;
    }

    public void setProyecto(String proyecto) {
        this.proyecto = proyecto;
    }

    public String getCod_tipoCliente() {
        return cod_tipoCliente;
    }

    public void setCod_tipoCliente(String cod_tipoCliente) {
        this.cod_tipoCliente = cod_tipoCliente;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

}
