/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Harold Cuello G.
 */
public class CmbGeneralScBeans {

    private int idCombo;
    private String DescripcionCombo;
    private String idCombostr;
    private int DiasMesCombo;

    
    public CmbGeneralScBeans() {
    }

    /**
     * @param idCombo the idCombo to set
     */
    public void setIdCmb(int idCombo) {
        this.idCombo = idCombo;
    }

    /**
     * @return the idCombo
     */
    public int getIdCmb() {
        return idCombo;
    }
    
    /**
     * @param idCombostr the idCombostr to set
     */
    public void setIdCmbStr(String idCombostr) {
        this.idCombostr = idCombostr;
    }

    /**
     * @return the idCombostr
     */
    public String getIdCmbStr() {
        return idCombostr;
    }    

    /**
     * @param DescripcionCombo the DescripcionCombo to set
     */
    public void setDescripcionCmb(String DescripcionCombo) {
        this.DescripcionCombo = DescripcionCombo;
    }

    /**
     * @return the DescripcionCombo
     */
    public String getDescripcionCmb() {
        return DescripcionCombo;
    }


    /**
     * @param DiasMesCombo the DescripcionCombo to set
     */
    public void setDiasMesCmb(int DiasMesCombo) {
        this.DiasMesCombo = DiasMesCombo;
    }

    /**
     * @return the DiasMesCombo
     */
    public int getDiasMesCmb() {
        return DiasMesCombo;
    }


}
