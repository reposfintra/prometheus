/*
 * Agencia.java
 *
 * Created on 12 de noviembre de 2004, 06:53 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  AMENDEZ
 */
public class Agencia {
    
    String reg_status;
    String dstrct;
    String agency_id;
    String id_mims_agency;
    String name;
    String last_update;
    String user_update;
    String creation_date;
    String creation_user;
    
    
    //ALEJANDRO
    
    private String id_agencia;
    private String id_mims;
    private String nombre;
    private String estado;
    private String fecha_cambio_estado;
    
      //Ivan Dario Gomez
    private String agenciaContable;
    private String unidadNegocio;
    
    /** Creates a new instance of Agencia */
    public Agencia() {
    }
    
    public void setReg_Status(String reg_status){
        this.reg_status = reg_status;
    }
    
    public String getReg_Status(){
        return this.reg_status;
    }
    
    public void setDstrct(String dstrct){
        this.dstrct = dstrct;
    }
    
    public String getDstrct(){
        return this.dstrct;
    }
    
    public void setAgency_id(String agency_id){
        this.agency_id = agency_id;
    }
    
    public String getAgency_id(){
        return this.agency_id;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setLast_update(String last_update){
        this.last_update = last_update;
    }
    
    public String getLast_update(){
        return this.last_update;
    }
    
    public void setUser_update(String user_update){
        this.user_update = user_update;
    }
    
    public String getUser_update(){
        return this.user_update;
    }
    
    public void setCreation_date(String creation_date){
        this.creation_date = creation_date;
    }
    
    public String getCreation_date(){
        return this.creation_date;
    }
    
    public void setCreation_user(String creation_user){
        this.creation_user = creation_user;
    }
    
    public String getCreation_user(){
        return this.creation_user;
    }
    
    //ALEJO
    public String getId_agencia() {
        return id_agencia;
    }
    
    public String getId_mims() {
        return id_mims;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public String getFecha_cambio_estado() {
        return fecha_cambio_estado;
    }
    
    public String getNombre() {
        return nombre;
    }
    
   public void setId_agencia( String id_agencia ) {
        this.id_agencia = id_agencia;
    }
    
    public void setId_mims( String id_mims ) {
        this.id_mims = id_mims;
    }
    
    public void setEstado( String estado ) {
        this.estado = estado;
    }
    
    public void setFecha_cambio_estado( String fecha_cambio_estado ) {
        this.fecha_cambio_estado = fecha_cambio_estado;
    }
    
    public void setNombre( String nombre ) {
        this.nombre = nombre;
    }
    
    
    public String toString(){
        return nombre;
    }
    
    /**
     * Getter for property agenciaContable.
     * @return Value of property agenciaContable.
     */
    public java.lang.String getAgenciaContable() {
        return agenciaContable;
    }
    
    /**
     * Setter for property agenciaContable.
     * @param agenciaContable New value of property agenciaContable.
     */
    public void setAgenciaContable(java.lang.String agenciaContable) {
        this.agenciaContable = agenciaContable;
    }
    
    /**
     * Getter for property unidadNegocio.
     * @return Value of property unidadNegocio.
     */
    public java.lang.String getUnidadNegocio() {
        return unidadNegocio;
    }
    
    /**
     * Setter for property unidadNegocio.
     * @param unidadNegocio New value of property unidadNegocio.
     */
    public void setUnidadNegocio(java.lang.String unidadNegocio) {
        this.unidadNegocio = unidadNegocio;
    }
    
}
