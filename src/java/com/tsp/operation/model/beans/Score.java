package com.tsp.operation.model.beans;

/**
 * Bean para la tabla wsdc.score<br/>
 * 28/11/2011
 * @author darrieta
 */
public class Score {

    private int id;
    private int tipo;
    private int puntaje = -1;
    private String clasificacion;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private String razon;
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of razon
     *
     * @return the value of razon
     */
    public String getRazon() {
        return razon;
    }

    /**
     * Set the value of razon
     *
     * @param razon new value of razon
     */
    public void setRazon(String razon) {
        this.razon = razon;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }
    
    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }
    
    /**
     * Get the value of clasificacion
     *
     * @return the value of clasificacion
     */
    public String getClasificacion() {
        return clasificacion;
    }

    /**
     * Set the value of clasificacion
     *
     * @param clasificacion new value of clasificacion
     */
    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }


    /**
     * Get the value of puntaje
     *
     * @return the value of puntaje
     */
    public int getPuntaje() {
        return puntaje;
    }

    /**
     * Set the value of puntaje
     *
     * @param puntaje new value of puntaje
     */
    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }


    /**
     * Get the value of tipo
     *
     * @return the value of tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * Set the value of tipo
     *
     * @param tipo new value of tipo
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }


    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }

}
