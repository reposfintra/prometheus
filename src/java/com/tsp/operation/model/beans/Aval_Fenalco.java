/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Bean que maneja los avales de Fenalco
**/
package com.tsp.operation.model.beans;

import java.io.Serializable;
public class Aval_Fenalco implements Serializable{
    
    private String  nit;
    private String nom;
    private int nodoc;
    private float tc30;
    private float tc45;
    private float tl30;
    private float tl45;
    public Aval_Fenalco() {
    }
    
    public void setNit(String nit){
        this.nit = nit;
    }
    
    public String getNit(){
        return this.nit;
    }
    ////////***////////////////
    public void setNodoc(int nodoc){
        this.nodoc = nodoc;
    }
    
    public int getNodoc(){
        return this.nodoc;
    }
    ////////***////////////////
    public void setTc30(float tc30){
        this.tc30 = tc30;
    }
    
    public float getTc30(){
        //System.out.println("Entro al bean");
        return this.tc30;
    }
    ////////***////////////////
    public void setTc45(float tc45){
        this.tc45 = tc45;
    }
    
    public float getTc45(){
        return this.tc45;
    }
    ////////***////////////////
    public void setTl30(float tl30){
        
        //System.out.println("Entro al metodo");
        this.tl30 = tl30;
    }
    
    public float getTl30(){
        return this.tl30;
    }
    ////////***////////////////
    public void setTl45(float tl45){
        this.tl45 = tl45;
    }
    
    public float getTl45(){
        return this.tl45;
    }
    
    /**
     * Getter for property nom.
     * @return Value of property nom.
     */
    public java.lang.String getNom() {
        return nom;
    }
    
    /**
     * Setter for property nom.
     * @param nom New value of property nom.
     */
    public void setNom(java.lang.String nom) {
        this.nom = nom;
    }
    
    /*public static void main(String arg[]) throws Exception
	{
		Aval_Fenalco av= new Aval_Fenalco();
                av.setTl30(1);
	}*/
}
