package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Usuario implements Serializable {

    private String login;
    private String password;
    private String nombre;
    private String id_agencia;
    private String id_mims;
    private String cedula;
    private String email;
    private String proyect_ind;
    private java.util.Date creation_date;
    private String creation_user;
    private String project;
    private String dstrct;
    private String perfil;
    private String base;
    private String conexion;
    private String nit;
    private String idusuario;
    ////sandrameg
    private String fecha_ini_act;
    private String ult_fecha_renov_clave;
    private int num_dias_vigencia;
    private int num_dias_para_renovar_clave;
    //AMENDEZ 20050708
    private String dpto;
    private String cia;
    private String accesoplanviaje;
    private boolean puedeReanticipo=false;

    private String nitPropietario; //Ivan
    private String creation_date_cad;
    private String bd;
    private String codCiudad;
    /**apayares 12.12.05*/
    /**
     * Holds value of property tipo.
     */
    private String tipo;

    /**
     * Holds value of property clienteDestinat.
     */
    private String clienteDestinat;

    /**
     * Holds value of property telefono.
     */
    private String telefono;

    /**
     * Holds value of property ciudad.
     */
    private String ciudad;

    /**
     * Holds value of property direccion.
     */
    private String direccion;


    /**
     * Holds value of property codPais.
     */
    private String codPais;

    /**
     * Holds value of property estado.
     */
    private String estado;

    /**
     * Holds value of property cambiarClaveLogin.
     */
    private boolean cambiarClaveLogin = false;

    /**
     * Holds value of property nombre_agencia.
     */
    private String nombre_agencia;

    /**
     * Holds value of property perfiles.
     */
    private java.util.Vector perfiles;
    
    private int codUsuario;

    private String moderador;

    
    private String empresa;
    private String token_api;


    /*fin*/

     public static Usuario load(ResultSet rs, boolean login)throws SQLException{

        Usuario usuario = new Usuario();
        if ( login ){
            usuario.setProject(rs.getString("project"));
            usuario.setDstrct(rs.getString("dstrct"));
        }
        usuario.setLogin(rs.getString("idusuario").toUpperCase().trim());
        usuario.setPassword(rs.getString("claveencr"));
        usuario.setNombre(rs.getString("nombre"));
        usuario.setId_agencia(rs.getString("id_agencia"));
        //usuario.setDstrct(rs.getString("dstrct"));
        usuario.setCedula(rs.getString("nit"));
        //usuario.setProject(rs.getString("project"));
        usuario.setPerfil(rs.getString("perfil"));
        usuario.setBase(rs.getString("base"));
        usuario.setDpto(rs.getString("dpto"));
        usuario.setCia(rs.getString("cia"));
        usuario.setAccesoplanviaje(rs.getString("accesoplanviaje"));
        /////
        usuario.setFecha_ini_act(rs.getString("fecha_ini_act"));
        usuario.setUlt_fecha_renov_clave(rs.getString("ult_fecha_renovo_clave"));
        usuario.setNum_dias_vigencia(rs.getInt("num_dias_vigencia"));
        usuario.setNum_dias_para_renovar_clave(rs.getInt("num_dias_para_renovar_clave"));
        //Alejandro Payares, Nov 17 / 2005
        usuario.setCambiarClaveLogin(rs.getBoolean("cambioclavelogin"));
        usuario.setEstado(rs.getString("estado"));
        usuario.setCodPais(rs.getString("codpais"));
        usuario.setTipo(rs.getString("tipo"));
        usuario.setClienteDestinat(rs.getString("clientedestinat"));
        usuario.setDireccion(rs.getString("direccion"));
        usuario.setTelefono(rs.getString("telefono"));
        usuario.setCiudad(rs.getString("ciudad"));
        usuario.setEmail(rs.getString("email"));
        usuario.setNitPropietario(rs.getString("nits_propietario"));
        usuario.setConexion("");
        usuario.setCreationDateCad(rs.getString("creation_date"));
        try {
            usuario.setToken_api(rs.getString("token_api"));
            usuario.setBd(rs.getString("base_datos"));
        } catch(Exception e) {
            usuario.setBd("fintra");
            usuario.setToken_api("");
        }
        usuario.setCodCiudad("BQ");
        return usuario;
    }

    //============================================
    //		Metodos de acceso a propiedades
    //============================================

    public void setBase(String base){

        this.base=base;
    }

    public String getBase(){

        return base;
    }
    public void setConexion(String conexion){

        this.conexion=conexion;
    }

    public String getConexion(){

        return conexion;
    }
    public void setPerfil(String perfil){

        this.perfil=perfil;
    }

    public String getPerfil(){

        return perfil;
    }
    public void setProject(String project){

        this.project=project;
    }

    public String getProject(){

        return project;
    }
    public void setDstrct(String dstrct){

        this.dstrct=dstrct;
    }

    public String getDstrct(){

        return dstrct;
    }

    public void setLogin(String login){

        this.login=login;
    }

    public String getLogin(){

        return login;
    }

    public void setPassword(String password){

        this.password=password;
    }

    public String getPassword(){

        return password;
    }

    public void setNombre(String nombre){

        this.nombre=nombre;
    }

    public String getNombre(){

        return nombre;
    }

    public void setId_agencia(String id_agencia){

        this.id_agencia=id_agencia;
    }

    public String getId_agencia(){

        return id_agencia;
    }

    public void setId_mims(String id_mims){

        this.id_mims=id_mims;
    }

    public String getId_mims(){

        return id_mims;
    }

    public void setCedula(String cedula){

        this.cedula=cedula;
    }

    public String getCedula(){

        return cedula;
    }

    public void setEmail(String email){

        this.email=email;
    }

    public String getEmail(){

        return email;
    }

    public void setProyect_ind(String proyect_ind){

        this.proyect_ind=proyect_ind;
    }

    public String getProyect_ind(){

        return proyect_ind;
    }

    public void setCreation_date(java.util.Date creation_date){

        this.creation_date=creation_date;
    }

    public java.util.Date getCreation_date(){

        return creation_date;
    }

    public void setCreation_user(String creation_user){

        this.creation_user=creation_user;
    }

    public String getCreation_user(){

        return creation_user;
    }
    public String getNit() {
        return nit;
    }

    public void setIdusuario( String idusuario ) {
        this.idusuario = idusuario;
    }

    public void setNit( String nit ) {
        this.nit = nit;
    }

    public String getIdusuario() {
        return idusuario;
    }
    /**
     * Getter for property dpto.
     * @return Value of property dpto.
     */
    public java.lang.String getDpto() {
        return dpto;
    }

    /**
     * Setter for property dpto.
     * @param dpto New value of property dpto.
     */
    public void setDpto(java.lang.String dpto) {
        this.dpto = dpto;
    }

    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }

    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }

    /**
     *Retorna el listado de distritos en los que puede hacer modificaciones un usuario
     */
    public TreeMap getListCia(){
          String[] list;
          TreeMap listCia = new TreeMap();
          list = this.cia.toString().split(",");
          for(int i = 0; i<list.length; i++){
              listCia.put(list[i], list[i]);
          }
          return listCia;
    }

    /**
     * Getter for property accesoplanviaje.
     * @return Value of property accesoplanviaje.
     */
    public java.lang.String getAccesoplanviaje() {
        return accesoplanviaje;
    }

    /**
     * Setter for property accesoplanviaje.
     * @param accesoplanviaje New value of property accesoplanviaje.
     */
    public void setAccesoplanviaje(java.lang.String accesoplanviaje) {
        this.accesoplanviaje = accesoplanviaje;
    }


    public int getNum_dias_para_renovar_clave() {
        return num_dias_para_renovar_clave;
    }

    public void setNum_dias_para_renovar_clave(int num_dias_para_renovar_clave) {
        this.num_dias_para_renovar_clave = num_dias_para_renovar_clave;
    }

    public int getNum_dias_vigencia() {
        return num_dias_vigencia;
    }

    public void setNum_dias_vigencia(int num_dias_vigencia) {
        this.num_dias_vigencia = num_dias_vigencia;
    }

    public java.lang.String getUlt_fecha_renov_clave() {
        return ult_fecha_renov_clave;
    }

    public void setUlt_fecha_renov_clave(java.lang.String ult_fecha_renov_clave) {
        this.ult_fecha_renov_clave = ult_fecha_renov_clave;
    }

    public java.lang.String getFecha_ini_act() {
        return fecha_ini_act;
    }

    public void setFecha_ini_act(java.lang.String fecha_ini_act) {
        this.fecha_ini_act = fecha_ini_act;
    }

    /**
     * Getter for property puedeReanticipo.
     * @return Value of property puedeReanticipo.
     */
    public boolean isPuedeReanticipo() {
        return puedeReanticipo;
    }

    /**
     * Setter for property puedeReanticipo.
     * @param puedeReanticipo New value of property puedeReanticipo.
     */
    public void setPuedeReanticipo(boolean puedeReanticipo) {
        this.puedeReanticipo = puedeReanticipo;
    }


    //**Alejandro 12.12.05***/
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public String getTipo() {
        return this.tipo;
    }

    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Getter for property clienteDestinat.
     * @return Value of property clienteDestinat.
     */
    public String getClienteDestinat() {
        return this.clienteDestinat;
    }

    /**
     * Setter for property clienteDestinat.
     * @param clienteDestinat New value of property clienteDestinat.
     */
    public void setClienteDestinat(String clienteDestinat) {
        this.clienteDestinat = clienteDestinat;
    }

    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public String getTelefono() {
        return this.telefono;
    }

    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public String getCiudad() {
        return this.ciudad;
    }

    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCodPais(String value) {
        this.codPais = value;
    }

    /**
     * Descripcion del tipo de usuario.
     * @return Value of property tipo.
     */
    public String getDescTipo() {
        String codigoTipo = this.tipo;
        String nombreTipo = "";
        if( codigoTipo.equals("ADMIN") )
            nombreTipo = "Administrador - TSP";
        else if( codigoTipo.equals("TSPUSER") )
            nombreTipo = "TSP - Usuario Consulta";
        else if( codigoTipo.equals("TSPSERVCLI") )
            nombreTipo = "TSP - Servicio al Cliente";
        else if( codigoTipo.equals("CLIENTEADM") )
            nombreTipo = "Cliente - Admin. de Transporte";
        else if( codigoTipo.equals("CLIENTETSP") )
            nombreTipo = "Cliente de TSP";
        else if( codigoTipo.equals("DEST") )
            nombreTipo = "Destinatario";
        return nombreTipo;
    }

    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public String getDireccion() {
        return this.direccion;
    }

    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Retorna el verdadero nombre del pais basandose en el valor
     * de la propiedad "codPais".
     * @return Value of property codPais.
     */
    public String getNombrePais()  {
        String codigoPais = this.codPais.toString();
        String nombrePais = "";
        if( codigoPais.equals("COL") )
            nombrePais = "Colombia";
        else if( codigoPais.equals("VEN") )
            nombrePais = "Venezuela";
        else if( codigoPais.equals("ECU") )
            nombrePais = "Ecuador";
        return nombrePais;
    }

    public String getNombreClienteDestinat() {
        return "*** Cliente/Destina ***";
    }

    /**
     * Getter for property codPais.
     * @return Value of property codPais.
     */
    public java.lang.String getCodPais() {
        return codPais;
    }

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public String getEstado() {
        return this.estado;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Getter for property cambiarClaveLogin.
     * @return Value of property cambiarClaveLogin.
     */
    public boolean isCambiarClaveLogin() {
        return this.cambiarClaveLogin;
    }

    /**
     * Setter for property cambiarClaveLogin.
     * @param cambiarClaveLogin New value of property cambiarClaveLogin.
     */
    public void setCambiarClaveLogin(boolean cambiarClaveLogin) {
        this.cambiarClaveLogin = cambiarClaveLogin;
    }

    /**
     * Getter for property nitPropietario.
     * @return Value of property nitPropietario.
     */
    public java.lang.String getNitPropietario() {
        return nitPropietario;
    }

    /**
     * Setter for property nitPropietario.
     * @param nitPropietario New value of property nitPropietario.
     */
    public void setNitPropietario(java.lang.String nitPropietario) {
        this.nitPropietario = nitPropietario;
    }

    /**
     * Getter for property nombre_agencia.
     * @return Value of property nombre_agencia.
     */
    public String getNombre_agencia() {
        return this.nombre_agencia;
    }

    /**
     * Setter for property nombre_agencia.
     * @param nombre_agencia New value of property nombre_agencia.
     */
    public void setNombre_agencia(String nombre_agencia) {
        this.nombre_agencia = nombre_agencia;
    }

    /**
     * Getter for property perfiles.
     * @return Value of property perfiles.
     */
    public java.util.Vector getPerfiles() {
        return this.perfiles;
    }

    /**
     * Setter for property perfiles.
     * @param perfiles New value of property perfiles.
     */
    public void setPerfiles(java.util.Vector perfiles) {
        this.perfiles = perfiles;
    }

    public String getCreationDateCad(){
        return creation_date_cad;
    }
    public void setCreationDateCad(String x){

        this.creation_date_cad=x;
    }

    /**
     * Get the value of codCiudad
     *
     * @return the value of codCiudad
     */
    public String getCodCiudad() {
        return codCiudad;
    }

    /**
     * Set the value of codCiudad
     *
     * @param codCiudad new value of codCiudad
     */
    public void setCodCiudad(String codCiudad) {
        this.codCiudad = codCiudad;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    /*fin*/

    /**
     * @return the codUsuario
     */
    public int getCodUsuario() {
        return codUsuario;
    }

    /**
     * @param codUsuario the codUsuario to set
     */
    public void setCodUsuario(int codUsuario) {
        this.codUsuario = codUsuario;
    }
    
    public void setEmpresa( String empresa ){
        this.empresa = empresa;
    }
    
    public String getEmpresa() {
        return this.empresa;
    }
    
   
    /**
     * @return the moderador
     */
    public String getModerador() {
        return moderador;
    }

    /**
     * @param moderador the moderador to set
     */
    public void setModerador(String moderador) {
        this.moderador = moderador;
    }

    public String getToken_api() {
        return token_api;
    }

    public void setToken_api(String token_api) {
        this.token_api = token_api;
    }
    
    
}