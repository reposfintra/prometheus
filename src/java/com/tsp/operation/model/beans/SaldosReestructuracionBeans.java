/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author hcuello
 */
public class SaldosReestructuracionBeans {
    
    private String documento;
    private String cod_neg;
    private int cuota;
    private String fecha_vencimiento;
    private int dias_mora;
    private String estado;
    private String tipo_negocio;
    private double valor_factura;
    private double valor_abono;
    private double valor_saldo;
    private double saldo_capital;
    private double saldo_interes;
    private double saldo_cat;
    private double seguro;
    private double interes_mora;
    private double gasto_cobranza;
    private double total_item;

    public SaldosReestructuracionBeans() {
    }
    
    

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the cod_neg
     */
    public String getCod_neg() {
        return cod_neg;
    }

    /**
     * @param cod_neg the cod_neg to set
     */
    public void setCod_neg(String cod_neg) {
        this.cod_neg = cod_neg;
    }

    /**
     * @return the cuota
     */
    public int getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(int cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the fecha_vencimiento
     */
    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * @param fecha_vencimiento the fecha_vencimiento to set
     */
    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * @return the dias_mora
     */
    public int getDias_mora() {
        return dias_mora;
    }

    /**
     * @param dias_mora the dias_mora to set
     */
    public void setDias_mora(int dias_mora) {
        this.dias_mora = dias_mora;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the saldo_capital
     */
    public double getSaldo_capital() {
        return saldo_capital;
    }

    /**
     * @param saldo_capital the saldo_capital to set
     */
    public void setSaldo_capital(double saldo_capital) {
        this.saldo_capital = saldo_capital;
    }

    /**
     * @return the saldo_interes
     */
    public double getSaldo_interes() {
        return saldo_interes;
    }

    /**
     * @param saldo_interes the saldo_interes to set
     */
    public void setSaldo_interes(double saldo_interes) {
        this.saldo_interes = saldo_interes;
    }

    /**
     * @return the saldo_cat
     */
    public double getSaldo_cat() {
        return saldo_cat;
    }

    /**
     * @param saldo_cat the saldo_cat to set
     */
    public void setSaldo_cat(double saldo_cat) {
        this.saldo_cat = saldo_cat;
    }

    /**
     * @return the seguro
     */
    public double getSeguro() {
        return seguro;
    }

    /**
     * @param seguro the seguro to set
     */
    public void setSeguro(double seguro) {
        this.seguro = seguro;
    }

    /**
     * @return the interes_mora
     */
    public double getInteres_mora() {
        return interes_mora;
    }

    /**
     * @param interes_mora the interes_mora to set
     */
    public void setInteres_mora(double interes_mora) {
        this.interes_mora = interes_mora;
    }

    /**
     * @return the gasto_cobranza
     */
    public double getGasto_cobranza() {
        return gasto_cobranza;
    }

    /**
     * @param gasto_cobranza the gasto_cobranza to set
     */
    public void setGasto_cobranza(double gasto_cobranza) {
        this.gasto_cobranza = gasto_cobranza;
    }

    /**
     * @return the total_item
     */
    public double getTotal_item() {
        return total_item;
    }

    /**
     * @param total_item the total_item to set
     */
    public void setTotal_item(double total_item) {
        this.total_item = total_item;
    }

    /**
     * @return the valor_factura
     */
    public double getValor_factura() {
        return valor_factura;
    }

    /**
     * @param valor_factura the valor_factura to set
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }

    /**
     * @return the valor_abono
     */
    public double getValor_abono() {
        return valor_abono;
    }

    /**
     * @param valor_abono the valor_abono to set
     */
    public void setValor_abono(double valor_abono) {
        this.valor_abono = valor_abono;
    }

    /**
     * @return the valor_saldo
     */
    public double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo(double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    /**
     * @return the tipo_negocio
     */
    public String getTipo_negocio() {
        return tipo_negocio;
    }

    /**
     * @param tipo_negocio the tipo_negocio to set
     */
    public void setTipo_negocio(String tipo_negocio) {
        this.tipo_negocio = tipo_negocio;
    }
    
    
    
    
    
    
}
