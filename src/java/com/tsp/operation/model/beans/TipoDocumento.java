/*
 * TipoDocumento.java
 *
 * Created on 1 de diciembre de 2004, 05:49 PM
 */

package com.tsp.operation.model.beans;
import java.sql.*;
/**
 *
 * @author  mcelin
 */
public class TipoDocumento {
    private String Tipo;
    private String Nombre;    
    /** Creates a new instance of TipoDocumento */
    public TipoDocumento load(ResultSet rs) throws SQLException {
        TipoDocumento datos = new TipoDocumento();
        datos.setTipo(rs.getString(2)+"~"+rs.getString(3));
        datos.setNombre(rs.getString(4));        
        return datos;
    }
    
    //Setter
    public void setTipo(String valor) {
        this.Tipo = valor;
    }
    
    public void setNombre(String valor) {
        this.Nombre = valor;
    }  
    //Getter
    public String getTipo() {
        return this.Tipo;
    }
    
    public String getNombre() {
        return this.Nombre;
    }  
}
