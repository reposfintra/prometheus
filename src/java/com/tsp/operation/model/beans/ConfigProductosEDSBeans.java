/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class ConfigProductosEDSBeans {

    public String id;
    public String id_config_productos;
    public String galonaje_inicial;
    public String galonaje_final;
    public String vlr_descuento;
    public String porct_descuento;
    public String historico_id;
    public String periodo;
    public String usuario;
    public String fecha;
  
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_config_productos() {
        return id_config_productos;
    }

    public void setId_config_productos(String id_config_productos) {
        this.id_config_productos = id_config_productos;
    }

    public String getGalonaje_inicial() {
        return galonaje_inicial;
    }

    public void setGalonaje_inicial(String galonaje_inicial) {
        this.galonaje_inicial = galonaje_inicial;
    }

    public String getGalonaje_final() {
        return galonaje_final;
    }

    public void setGalonaje_final(String galonaje_final) {
        this.galonaje_final = galonaje_final;
    }

    public String getVlr_descuento() {
        return vlr_descuento;
    }

    public void setVlr_descuento(String vlr_descuento) {
        this.vlr_descuento = vlr_descuento;
    }

    public String getPorct_descuento() {
        return porct_descuento;
    }

    public void setPorct_descuento(String porct_descuento) {
        this.porct_descuento = porct_descuento;
    }

    public String getHistorico_id() {
        return historico_id;
    }

    public void setHistorico_id(String historico_id) {
        this.historico_id = historico_id;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
}
