/*
 * General.java
 *
 * Created on 10 de enero de 2007
 */

package com.tsp.operation.model.beans;

import java.sql.*;

/**
 *
 * @author  lfrieri
 */
public class Festivos {
    
    private String fecha;
    private String descripcion;
    private String pais; 
        
    /** Creates a new instance of Festivos */
    public Festivos() {
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }    
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property pais.
     * @return Value of property pais.
     */
    public java.lang.String getPais() {
        return pais;
    }
    
    /**
     * Setter for property pais.
     * @param pais New value of property pais.
     */
    public void setPais(java.lang.String pais) {
        this.pais = pais;
    }
    
}
