/*
 * ReporteEgreso.java
 *
 * Created on 13 de septiembre de 2005, 09:47 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.awt.*;
/**
 *
 * @author  Administrador
 */
public class ReporteEgreso{
    private String NumeroChk;
    private String NumeroPlanilla;
    private double ValorChk;
    private String Beneficiario;
    private String FechaChk;
    private String Despachador;
    private String Banco;
    private String BancoCuenta;
    private String Agencia;
    ///JUAN 191005
    private boolean flag;
    /*Nuevo numero de cheque*/
    private String NuevoNChk;
    
    /*Item egreso*/
    private String Banck;
    private String Sucursal;
    private String NumChk;
    private String Item;
    private String moneda;
    private String planilla;
    private double ValorC;
    
    private String reg_status;
    
    /*Jescandon 14-07-06*/
    private String tipo_doc;
    private double vlr_for;
    
    private String fecha_reporte;
    private String proveedor;   
    
    //JEscandon 22-02-07
    private String documento;
    private String distrito;
    
    //Reemplazar
    public static ReporteEgreso load2(ResultSet rs)throws Exception{
        ReporteEgreso datos = new ReporteEgreso();
        datos.setNumeroChk(rs.getString("NUMEROCHK"));
        datos.setValorChk(rs.getDouble("VALORCHK"));
        datos.setBeneficiario(rs.getString("BENEFICIARIO"));
        datos.setFechaChk(rs.getString("FECHACHK"));
        //Jescandon 14-07-06
        datos.setTipo_doc(rs.getString("TIPO_DOCUMENTO"));
        //Jescandon 22-01-07
        datos.setProveedor(rs.getString("nit_proveedor"));
        //datos.setDocumento(rs.getString("DOCUMENTO"));
        datos.setDistrito(rs.getString("DSTRCT"));
        
        return datos;
    }
    
    public static ReporteEgreso load(ResultSet rs)throws Exception{
        ReporteEgreso datos = new ReporteEgreso();
        datos.setNumeroChk(rs.getString("NUMEROCHK"));
        //datos.setNumeroPlanilla(rs.getString("PLANILLA"));
        datos.setValorChk(rs.getDouble("VALORCHK"));
        datos.setBeneficiario(rs.getString("BENEFICIARIO"));
        datos.setFechaChk(rs.getString("FECHACHK"));
        datos.setDespachador(rs.getString("DESPACHADOR"));
        datos.setBanco(rs.getString("BANCO"));
        datos.setBancoCuenta(rs.getString("BANCOCUENTA"));
        datos.setAgencia(rs.getString("AGENCIA"));
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setDistrito(rs.getString("DSTRCT"));
        
        //Modificacion 06/10/06
        datos.setVlr_for(rs.getDouble("VALORFOR"));
        
        //Modificacion 09/10/06
        datos.setProveedor(rs.getString("NIT_PROVEEDOR") + " " + rs.getString("NOMBREP"));        
        datos.setFecha_reporte(rs.getString("FECHA_REPORTE"));
        
        //Modificacion 20/10/06
        datos.setMoneda(rs.getString("MONEDA"));
        return datos;
    }
    
    public static ReporteEgreso loadAnulados(ResultSet rs)throws Exception{
        ReporteEgreso datos = new ReporteEgreso();
        datos.setNumeroChk(rs.getString("NUMEROCHK"));
        //datos.setNumeroPlanilla(rs.getString("PLANILLA"));
        datos.setValorChk(rs.getDouble("VALORCHK")*-1);
        datos.setBeneficiario(rs.getString("BENEFICIARIO"));
        datos.setFechaChk(rs.getString("FECHACHK"));
        datos.setDespachador(rs.getString("DESPACHADOR"));
        datos.setBanco(rs.getString("BANCO"));
        datos.setBancoCuenta(rs.getString("BANCOCUENTA"));
        datos.setAgencia(rs.getString("AGENCIA"));
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setDistrito(rs.getString("DSTRCT"));
        
        //Modificacion 06/10/06
        datos.setVlr_for(rs.getDouble("VALORFOR")*-1);
        
        //Modificacion 09/10/06
        datos.setProveedor(rs.getString("NIT_PROVEEDOR") + " " + rs.getString("NOMBREP"));
        datos.setFecha_reporte(rs.getString("FECHA_REPORTE"));
        
        //Modificacion 20/10/06
        datos.setMoneda(rs.getString("MONEDA"));
        return datos;
    }
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc() {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc(java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    /** Creates a new instance of ReporteEgreso */
    public ReporteEgreso() {
    }
    ///juan 191005
    public static ReporteEgreso loadItem(ResultSet rs)throws Exception{
        ReporteEgreso datos = new ReporteEgreso();
        datos.setNumChk(rs.getString("NUMEROCHK"));
        datos.setPlanilla(rs.getString("PLANILLA"));
        datos.setValorC(rs.getDouble("VALORCHK"));
        datos.setItem(rs.getString("ITEM"));
        datos.setMoneda(rs.getString("MONEDA"));
        datos.setBanck(rs.getString("BANCO"));
        datos.setSucursal(rs.getString("SUCURSAL"));
        return datos;
    }
   /* public static ReporteEgreso load2(ResultSet rs)throws Exception{
        ReporteEgreso datos = new ReporteEgreso();
        datos.setNumeroChk(rs.getString("NUMEROCHK"));
        datos.setValorChk(rs.getDouble("VALORCHK"));
        datos.setBeneficiario(rs.getString("BENEFICIARIO"));
        datos.setFechaChk(rs.getString("FECHACHK"));
        return datos;
    }*/
    
    public static ReporteEgreso load3(ResultSet rs)throws Exception{
        ReporteEgreso datos = new ReporteEgreso();
        datos.setFechaChk(rs.getString("DATE_DOC"));
        return datos;
    }
    
   
    
    /**
     * Getter for property Agencia.
     * @return Value of property Agencia.
     */
    public java.lang.String getAgencia() {
        return Agencia;
    }
    
    /**
     * Setter for property Agencia.
     * @param Agencia New value of property Agencia.
     */
    public void setAgencia(java.lang.String Agencia) {
        this.Agencia = Agencia;
    }
    
    /**
     * Getter for property Banco.
     * @return Value of property Banco.
     */
    public java.lang.String getBanco() {
        return Banco;
    }
    
    /**
     * Setter for property Banco.
     * @param Banco New value of property Banco.
     */
    public void setBanco(java.lang.String Banco) {
        this.Banco = Banco;
    }
    
    /**
     * Getter for property BancoCuenta.
     * @return Value of property BancoCuenta.
     */
    public java.lang.String getBancoCuenta() {
        return BancoCuenta;
    }
    
    /**
     * Setter for property BancoCuenta.
     * @param BancoCuenta New value of property BancoCuenta.
     */
    public void setBancoCuenta(java.lang.String BancoCuenta) {
        this.BancoCuenta = BancoCuenta;
    }
    
    /**
     * Getter for property Beneficiario.
     * @return Value of property Beneficiario.
     */
    public java.lang.String getBeneficiario() {
        return Beneficiario;
    }
    
    /**
     * Setter for property Beneficiario.
     * @param Beneficiario New value of property Beneficiario.
     */
    public void setBeneficiario(java.lang.String Beneficiario) {
        this.Beneficiario = Beneficiario;
    }
    
    /**
     * Getter for property Despachador.
     * @return Value of property Despachador.
     */
    public java.lang.String getDespachador() {
        return Despachador;
    }
    
    /**
     * Setter for property Despachador.
     * @param Despachador New value of property Despachador.
     */
    public void setDespachador(java.lang.String Despachador) {
        this.Despachador = Despachador;
    }
    
    /**
     * Getter for property FechaChk.
     * @return Value of property FechaChk.
     */
    public java.lang.String getFechaChk() {
        return FechaChk;
    }
    
    /**
     * Setter for property FechaChk.
     * @param FechaChk New value of property FechaChk.
     */
    public void setFechaChk(java.lang.String FechaChk) {
        this.FechaChk = FechaChk;
    }
    
    /**
     * Getter for property NumeroChk.
     * @return Value of property NumeroChk.
     */
    public java.lang.String getNumeroChk() {
        return NumeroChk;
    }
    
    /**
     * Setter for property NumeroChk.
     * @param NumeroChk New value of property NumeroChk.
     */
    public void setNumeroChk(java.lang.String NumeroChk) {
        this.NumeroChk = NumeroChk;
    }
    
    /**
     * Getter for property NumeroPlanilla.
     * @return Value of property NumeroPlanilla.
     */
    public java.lang.String getNumeroPlanilla() {
        return NumeroPlanilla;
    }
    
    /**
     * Setter for property NumeroPlanilla.
     * @param NumeroPlanilla New value of property NumeroPlanilla.
     */
    public void setNumeroPlanilla(java.lang.String NumeroPlanilla) {
        this.NumeroPlanilla = NumeroPlanilla;
    }
    
    /**
     * Getter for property ValorChk.
     * @return Value of property ValorChk.
     */
    public double getValorChk() {
        return ValorChk;
    }
    
    /**
     * Setter for property ValorChk.
     * @param ValorChk New value of property ValorChk.
     */
    public void setValorChk(double ValorChk) {
        this.ValorChk = ValorChk;
    }
    
    /**
     * Getter for property Sucursal.
     * @return Value of property Sucursal.
     */
    public java.lang.String getSucursal() {
        return Sucursal;
    }
    
    /**
     * Setter for property Sucursal.
     * @param Sucursal New value of property Sucursal.
     */
    public void setSucursal(java.lang.String Sucursal) {
        this.Sucursal = Sucursal;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property NumChk.
     * @return Value of property NumChk.
     */
    public java.lang.String getNumChk() {
        return NumChk;
    }
    
    /**
     * Setter for property NumChk.
     * @param NumChk New value of property NumChk.
     */
    public void setNumChk(java.lang.String NumChk) {
        this.NumChk = NumChk;
    }
    
    /**
     * Getter for property Banck.
     * @return Value of property Banck.
     */
    public java.lang.String getBanck() {
        return Banck;
    }
    
    /**
     * Setter for property Banck.
     * @param Banck New value of property Banck.
     */
    public void setBanck(java.lang.String Banck) {
        this.Banck = Banck;
    }
    
    /**
     * Getter for property NuevoNChk.
     * @return Value of property NuevoNChk.
     */
    public java.lang.String getNuevoNChk() {
        return NuevoNChk;
    }
    
    /**
     * Setter for property NuevoNChk.
     * @param NuevoNChk New value of property NuevoNChk.
     */
    public void setNuevoNChk(java.lang.String NuevoNChk) {
        this.NuevoNChk = NuevoNChk;
    }
    
    /**
     * Getter for property ValorC.
     * @return Value of property ValorC.
     */
    public double getValorC() {
        return ValorC;
    }
    
    /**
     * Setter for property ValorC.
     * @param ValorC New value of property ValorC.
     */
    public void setValorC(double ValorC) {
        this.ValorC = ValorC;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property Item.
     * @return Value of property Item.
     */
    public java.lang.String getItem() {
        return Item;
    }
    
    /**
     * Setter for property Item.
     * @param Item New value of property Item.
     */
    public void setItem(java.lang.String Item) {
        this.Item = Item;
    }
    
    /**
     * Getter for property flag.
     * @return Value of property flag.
     */
    public boolean isFlag() {
        return flag;
    }
    
    /**
     * Setter for property flag.
     * @param flag New value of property flag.
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property vlr_for.
     * @return Value of property vlr_for.
     */
    public double getVlr_for() {
        return vlr_for;
    }
    
    /**
     * Setter for property vlr_for.
     * @param vlr_for New value of property vlr_for.
     */
    public void setVlr_for(double vlr_for) {
        this.vlr_for = vlr_for;
    }
    
    /**
     * Getter for property fecha_reporte.
     * @return Value of property fecha_reporte.
     */
    public java.lang.String getFecha_reporte() {
        return fecha_reporte;
    }
    
    /**
     * Setter for property fecha_reporte.
     * @param fecha_reporte New value of property fecha_reporte.
     */
    public void setFecha_reporte(java.lang.String fecha_reporte) {
        this.fecha_reporte = fecha_reporte;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    public int compare(Object o1, Object o2) {
        ReporteEgreso e1 = (ReporteEgreso)o1;
        ReporteEgreso e2 = (ReporteEgreso)o2;
        
        
        String d1 = e1.getNumeroChk();
        String d2 = e2.getNumeroChk();
        
        return d1.compareTo(d2);
        
        //Negativo obj1 < obj2
        //Positivo obj1 > obj2
        //O obj1 = obj2
    }
    
    public boolean equals(Object o1, Object o2) {
        return compare(o1, o2) == 0;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
}

    
