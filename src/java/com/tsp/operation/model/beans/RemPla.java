package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class RemPla implements Serializable {
    
    private String dstrct;
    private String remesa;
    private String planilla;
    private String fecha="0099-01-01 00:00:00";
    private String creation_user;
    private float  quvrcm;
    private String observacion;
    private float  porcent=100;
    String account_code_i="";
    String account_code_c="";
    
    public static RemPla load(ResultSet rs)throws SQLException{
        
        RemPla rempla = new RemPla();
        
        rempla.setDstrct(rs.getString("cia"));
        rempla.setRemesa(rs.getString("numrem"));
        rempla.setPlanilla(rs.getString("numpla"));
        rempla.setQuvrcm(rs.getFloat("quvrcm"));
        rempla.setObservacion(rs.getString("observacion"));
        rempla.setFecha(rs.getString("feccum"));     
        return rempla;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    
    public void setQuvrcm(float quvrcm){
        
        this.quvrcm=quvrcm;
    }
    public float getQuemcm(){
        
        return quvrcm;
    }
    
    public void setObservacion(String observacion){
        
        this.observacion=observacion;
    }
    
    public String getObservacion(){
        
        return observacion;
    }
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    
    public void	setRemesa(String remesa){
        
        this.remesa=remesa;
    }
    
    public String getRemesa(){
        
        return remesa;
    }
    
    public void	setPlanilla(String planilla){
        
        this.planilla=planilla;
    }
    
    public String getPlanilla(){
        
        return planilla;
    }
    
    public void setFecha(String fecha){
        
        this.fecha=fecha;
    }
    
    public String getFecha(){
        
        return fecha;
    }
    
    public void	setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
    
    /**
     * Getter for property porcent.
     * @return Value of property porcent.
     */
    public float getPorcent() {
        return porcent;
    }
    
    /**
     * Setter for property porcent.
     * @param porcent New value of property porcent.
     */
    public void setPorcent(float porcent) {
        this.porcent = porcent;
    }
    
    /**
     * Getter for property account_code_c.
     * @return Value of property account_code_c.
     */
    public java.lang.String getAccount_code_c() {
        return account_code_c;
    }
    
    /**
     * Setter for property account_code_c.
     * @param account_code_c New value of property account_code_c.
     */
    public void setAccount_code_c(java.lang.String account_code_c) {
        this.account_code_c = account_code_c;
    }
    
    /**
     * Getter for property account_code_i.
     * @return Value of property account_code_i.
     */
    public java.lang.String getAccount_code_i() {
        return account_code_i;
    }
    
    /**
     * Setter for property account_code_i.
     * @param account_code_i New value of property account_code_i.
     */
    public void setAccount_code_i(java.lang.String account_code_i) {
        this.account_code_i = account_code_i;
    }
    
}