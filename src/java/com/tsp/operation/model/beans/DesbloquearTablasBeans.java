/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class DesbloquearTablasBeans {

    public String nombre_tabla;
    public String nombre_campo;
    public String es_last_update;
    public String es_fecha_envio;
    public String es_pk;
    public String es_fecha_anulacion;
    public String condicion;

    public String getNombre_tabla() {
        return nombre_tabla;
    }

    public void setNombre_tabla(String nombre_tabla) {
        this.nombre_tabla = nombre_tabla;
    }

    public String getNombre_campo() {
        return nombre_campo;
    }

    public void setNombre_campo(String nombre_campo) {
        this.nombre_campo = nombre_campo;
    }

    public String getEs_last_update() {
        return es_last_update;
    }

    public void setEs_last_update(String es_last_update) {
        this.es_last_update = es_last_update;
    }

    public String getEs_fecha_envio() {
        return es_fecha_envio;
    }

    public void setEs_fecha_envio(String es_fecha_envio) {
        this.es_fecha_envio = es_fecha_envio;
    }

    public String getEs_pk() {
        return es_pk;
    }

    public void setEs_pk(String es_pk) {
        this.es_pk = es_pk;
    }

    public String getEs_fecha_anulacion() {
        return es_fecha_anulacion;
    }

    public void setEs_fecha_anulacion(String es_fecha_anulacion) {
        this.es_fecha_anulacion = es_fecha_anulacion;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

}
