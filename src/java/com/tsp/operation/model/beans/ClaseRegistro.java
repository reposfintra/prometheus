/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;



/**
 * Clase que almacena todas las lineas del esquema que tienen un tipo de registro
 * diferente a una linea de detalle.
 * Almacena el tipo de registro, la fila en excell donde qued� ubicado el registro
 * y la formula a traducir si existe
 *
 * @author  Alvaro Pabon Martinez
 * @version %I%, %G%
 * @since   1.0
 *
 */

public class ClaseRegistro implements Serializable {



    private String tipoRegistro;
    private int fila;
    private String formula;
    private double secuencia;


    public ClaseRegistro(){
        tipoRegistro = "";
        fila = 0;
        formula = "";

    }

    public ClaseRegistro(String tipoRegistro, String formula, int fila,double secuencia){
        this.tipoRegistro = tipoRegistro;
        this.fila = fila;
        this.formula = formula;
        this.secuencia=secuencia;
    }

    /**
     * @return the tipoRegistro
     */
    public String getTipoRegistro() {
        return tipoRegistro;
    }

    /**
     * @param tipoRegistro the tipoRegistro to set
     */
    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    /**
     * @return the fila
     */
    public int getFila() {
        return fila;
    }

    /**
     * @param fila the fila to set
     */
    public void setFila(int fila) {
        this.fila = fila;
    }

    /**
     * @return the formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * @param formula the formula to set
     */
    public void setFormula(String formula) {
        this.formula = formula;
    }

    /**
     * @return the secuencia
     */
    public double getSecuencia() {
        return secuencia;
    }

    /**
     * @param secuencia the secuencia to set
     */
    public void setSecuencia(double secuencia) {
        this.secuencia = secuencia;
    }




}




