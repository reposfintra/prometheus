/********************************************************************
 *      Nombre Clase.................   Indicador.java
 *      Descripci�n..................   Bean del archivo tblindicadores
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   2 de marzo de 2006, 10:38 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz
 */

import java.sql.*;

public class Indicador {
    
    private String codigo;
    private String descripcion;
    private String descripcion_padre;
    private String codpadre;
    private int orden;
    private int nivel;
    private String id_folder;
    private long vlr_min;
    private long vlr_max;
    private long valor;
    private String ejecucion;
    private String metodo;
    private String formula;
    private int porcentaje;
    private String usuario;
    private String base;
    private String reg_status;
    private String district;
    
    /** Crea una nueva instancia de  Indicador */
    public Indicador() {
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }    
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void load0(ResultSet rs) throws SQLException{
        //= rs.getString("");
        this.id_folder = rs.getString("idfolder");
        this.descripcion = rs.getString("descripcion");
        this.nivel = rs.getInt("nivel");
        this.codigo = rs.getString("codigo");
        this.codpadre = rs.getString("codpadre");
        this.descripcion_padre = rs.getString("nompadre");
    }
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void load1(ResultSet rs) throws SQLException{
        this.id_folder = rs.getString("idfolder");
        this.descripcion = rs.getString("descripcion");
        this.nivel = rs.getInt("nivel");
        this.codigo = rs.getString("codigo");
        this.codpadre = rs.getString("codpadre");
        this.valor = Double.valueOf(rs.getString("valor")).longValue();
        this.vlr_max = Double.valueOf(rs.getString("vlr_max")).longValue();
        this.vlr_min = Double.valueOf(rs.getString("vlr_min")).longValue();
        this.formula = rs.getString("formula");
        this.ejecucion = rs.getString("ejecucion");
        this.metodo = rs.getString("metodo");
        this.porcentaje = rs.getInt("porcentaje");
        this.district = rs.getString("dstrct");
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property codpadre.
     * @return Value of property codpadre.
     */
    public java.lang.String getCodpadre() {
        return codpadre;
    }
    
    /**
     * Setter for property codpadre.
     * @param codpadre New value of property codpadre.
     */
    public void setCodpadre(java.lang.String codpadre) {
        this.codpadre = codpadre;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property ejecucion.
     * @return Value of property ejecucion.
     */
    public java.lang.String getEjecucion() {
        return ejecucion;
    }
    
    /**
     * Setter for property ejecucion.
     * @param ejecucion New value of property ejecucion.
     */
    public void setEjecucion(java.lang.String ejecucion) {
        this.ejecucion = ejecucion;
    }
    
    /**
     * Getter for property formula.
     * @return Value of property formula.
     */
    public java.lang.String getFormula() {
        return formula;
    }
    
    /**
     * Setter for property formula.
     * @param formula New value of property formula.
     */
    public void setFormula(java.lang.String formula) {
        this.formula = formula;
    }
    
    /**
     * Getter for property id_folder.
     * @return Value of property id_folder.
     */
    public java.lang.String getId_folder() {
        return id_folder;
    }
    
    /**
     * Setter for property id_folder.
     * @param id_folder New value of property id_folder.
     */
    public void setId_folder(java.lang.String id_folder) {
        this.id_folder = id_folder;
    }
    
    /**
     * Getter for property metodo.
     * @return Value of property metodo.
     */
    public java.lang.String getMetodo() {
        return metodo;
    }
    
    /**
     * Setter for property metodo.
     * @param metodo New value of property metodo.
     */
    public void setMetodo(java.lang.String metodo) {
        this.metodo = metodo;
    }
    
    /**
     * Getter for property nivel.
     * @return Value of property nivel.
     */
    public int getNivel() {
        return nivel;
    }
    
    /**
     * Setter for property nivel.
     * @param nivel New value of property nivel.
     */
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    /**
     * Getter for property orden.
     * @return Value of property orden.
     */
    public int getOrden() {
        return orden;
    }
    
    /**
     * Setter for property orden.
     * @param orden New value of property orden.
     */
    public void setOrden(int orden) {
        this.orden = orden;
    }
    
    /**
     * Getter for property porcentaje.
     * @return Value of property porcentaje.
     */
    public int getPorcentaje() {
        return porcentaje;
    }
    
    /**
     * Setter for property porcentaje.
     * @param porcentaje New value of property porcentaje.
     */
    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public long getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(long valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property vlr_max.
     * @return Value of property vlr_max.
     */
    public long getVlr_max() {
        return vlr_max;
    }
    
    /**
     * Setter for property vlr_max.
     * @param vlr_max New value of property vlr_max.
     */
    public void setVlr_max(long vlr_max) {
        this.vlr_max = vlr_max;
    }
    
    /**
     * Getter for property vlr_min.
     * @return Value of property vlr_min.
     */
    public long getVlr_min() {
        return vlr_min;
    }
    
    /**
     * Setter for property vlr_min.
     * @param vlr_min New value of property vlr_min.
     */
    public void setVlr_min(long vlr_min) {
        this.vlr_min = vlr_min;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property district.
     * @return Value of property district.
     */
    public java.lang.String getDistrict() {
        return district;
    }
    
    /**
     * Setter for property district.
     * @param district New value of property district.
     */
    public void setDistrict(java.lang.String district) {
        this.district = district;
    }
    
}
