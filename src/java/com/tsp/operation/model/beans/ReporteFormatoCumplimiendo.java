/*
 * ReporteFormatoCumplimiendo.java
 *
 * Created on 31 de octubre de 2006, 06:31 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;

/**
 *
 * @author  EQUIPO13
 */
public class ReporteFormatoCumplimiendo {
    private String pedido;
    private String cliente;
    private String ciudad_destino;
    private String peso;
    private String tipo_vehiculo;
    private String fecha_solicitud;
    private String hora_solicitud;
    private String fecha_max_carge_24;
    private String hora_max_carge_24;
    private String max_carge_solicitud_24;
    private String fecha_max_carge_36;
    private String hora_max_carge_36;
    private String max_carge_solicitud_36;    
    private String programado;
    private String fecha_programada_carge;
    private String placa;
    private String cedula;
    private String nombre_cond;
    private String comentario;
    private String factura;
    private String fecha_llegada_planta;
    private String hora_llegada_planta;
    private String hora_radicada_ocarge;
    private String fecha_entrada_carge;
    private String hora_entrada_carge;
    private String hora_terminacion_carge;
    private String hora_entrega_doc_xom;
    private String hora_salida_planta;
    private String tiempo_espera;
    private String tiempo_entrega_doc;
    private String tiempo_carge;
    private String ot;
    private String oc;
    private String fecha_carge;
    private String fecha_diferencia_colocacion;
    private String cargado;
    private String causa1;
    private String diferencia_fcarge_freal36;
    private String tiempo_carge_despues_sol;
    private String causa2;
    private String diferencia_fcarge_freal24;
    private String delivery;
    private String otros;
    private String orirem;
    private String oripla;    
    
    
    
    /** Creates a new instance of ReporteFormatoCumplimiendo */
    public ReporteFormatoCumplimiendo () {
    }
    
    /** 
     * Getter for property cargado.
     * @return Value of property cargado.
     */
    public java.lang.String getCargado () {
        return cargado;
    }
    
    /**
     * Setter for property cargado.
     * @param cargado New value of property cargado.
     */
    public void setCargado (java.lang.String cargado) {
        this.cargado = cargado;
    }
    
    /**
     * Getter for property causa1.
     * @return Value of property causa1.
     */
    public java.lang.String getCausa1 () {
        return causa1;
    }
    
    /**
     * Setter for property causa1.
     * @param causa1 New value of property causa1.
     */
    public void setCausa1 (java.lang.String causa1) {
        this.causa1 = causa1;
    }
    
    /**
     * Getter for property causa2.
     * @return Value of property causa2.
     */
    public java.lang.String getCausa2 () {
        return causa2;
    }
    
    /**
     * Setter for property causa2.
     * @param causa2 New value of property causa2.
     */
    public void setCausa2 (java.lang.String causa2) {
        this.causa2 = causa2;
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula () {
        return cedula;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula (java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Getter for property ciudad_destino.
     * @return Value of property ciudad_destino.
     */
    public java.lang.String getCiudad_destino () {
        return ciudad_destino;
    }
    
    /**
     * Setter for property ciudad_destino.
     * @param ciudad_destino New value of property ciudad_destino.
     */
    public void setCiudad_destino (java.lang.String ciudad_destino) {
        this.ciudad_destino = ciudad_destino;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente () {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente (java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property comentario.
     * @return Value of property comentario.
     */
    public java.lang.String getComentario () {
        return comentario;
    }
    
    /**
     * Setter for property comentario.
     * @param comentario New value of property comentario.
     */
    public void setComentario (java.lang.String comentario) {
        this.comentario = comentario;
    }
    
    /**
     * Getter for property diferencia_fcarge_freal.
     * @return Value of property diferencia_fcarge_freal.
     */
    public java.lang.String getDiferencia_fcarge_freal36 () {
        return diferencia_fcarge_freal36;
    }
    
    /**
     * Setter for property diferencia_fcarge_freal.
     * @param diferencia_fcarge_freal New value of property diferencia_fcarge_freal.
     */
    public void setDiferencia_fcarge_freal36 (java.lang.String diferencia_fcarge_freal36) {
        this.diferencia_fcarge_freal36 = diferencia_fcarge_freal36;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura () {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura (java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property fecha_carge.
     * @return Value of property fecha_carge.
     */
    public java.lang.String getFecha_carge () {
        return fecha_carge;
    }
    
    /**
     * Setter for property fecha_carge.
     * @param fecha_carge New value of property fecha_carge.
     */
    public void setFecha_carge (java.lang.String fecha_carge) {
        this.fecha_carge = fecha_carge;
    }
    
    /**
     * Getter for property fecha_diferencia_colocacion.
     * @return Value of property fecha_diferencia_colocacion.
     */
    public java.lang.String getFecha_diferencia_colocacion () {
        return fecha_diferencia_colocacion;
    }
    
    /**
     * Setter for property fecha_diferencia_colocacion.
     * @param fecha_diferencia_colocacion New value of property fecha_diferencia_colocacion.
     */
    public void setFecha_diferencia_colocacion (java.lang.String fecha_diferencia_colocacion) {
        this.fecha_diferencia_colocacion = fecha_diferencia_colocacion;
    }
    
    /**
     * Getter for property fecha_entrada_carge.
     * @return Value of property fecha_entrada_carge.
     */
    public java.lang.String getFecha_entrada_carge () {
        return fecha_entrada_carge;
    }
    
    /**
     * Setter for property fecha_entrada_carge.
     * @param fecha_entrada_carge New value of property fecha_entrada_carge.
     */
    public void setFecha_entrada_carge (java.lang.String fecha_entrada_carge) {
        this.fecha_entrada_carge = fecha_entrada_carge;
    }
    
    /**
     * Getter for property fecha_llegada_planta.
     * @return Value of property fecha_llegada_planta.
     */
    public java.lang.String getFecha_llegada_planta () {
        return fecha_llegada_planta;
    }
    
    /**
     * Setter for property fecha_llegada_planta.
     * @param fecha_llegada_planta New value of property fecha_llegada_planta.
     */
    public void setFecha_llegada_planta (java.lang.String fecha_llegada_planta) {
        this.fecha_llegada_planta = fecha_llegada_planta;
    }
    
    /**
     * Getter for property fecha_max_carge_24.
     * @return Value of property fecha_max_carge_24.
     */
    public java.lang.String getFecha_max_carge_24 () {
        return fecha_max_carge_24;
    }
    
    /**
     * Setter for property fecha_max_carge_24.
     * @param fecha_max_carge_24 New value of property fecha_max_carge_24.
     */
    public void setFecha_max_carge_24 (java.lang.String fecha_max_carge_24) {
        this.fecha_max_carge_24 = fecha_max_carge_24;
    }
    
    /**
     * Getter for property fecha_max_carge_36.
     * @return Value of property fecha_max_carge_36.
     */
    public java.lang.String getFecha_max_carge_36 () {
        return fecha_max_carge_36;
    }
    
    /**
     * Setter for property fecha_max_carge_36.
     * @param fecha_max_carge_36 New value of property fecha_max_carge_36.
     */
    public void setFecha_max_carge_36 (java.lang.String fecha_max_carge_36) {
        this.fecha_max_carge_36 = fecha_max_carge_36;
    }
    
    /**
     * Getter for property fecha_programada_carge.
     * @return Value of property fecha_programada_carge.
     */
    public java.lang.String getFecha_programada_carge () {
        return fecha_programada_carge;
    }
    
    /**
     * Setter for property fecha_programada_carge.
     * @param fecha_programada_carge New value of property fecha_programada_carge.
     */
    public void setFecha_programada_carge (java.lang.String fecha_programada_carge) {
        this.fecha_programada_carge = fecha_programada_carge;
    }
    
    /**
     * Getter for property fecha_solicitud.
     * @return Value of property fecha_solicitud.
     */
    public java.lang.String getFecha_solicitud () {
        return fecha_solicitud;
    }
    
    /**
     * Setter for property fecha_solicitud.
     * @param fecha_solicitud New value of property fecha_solicitud.
     */
    public void setFecha_solicitud (java.lang.String fecha_solicitud) {
        this.fecha_solicitud = fecha_solicitud;
    }
    
    /**
     * Getter for property hora_entrada_carge.
     * @return Value of property hora_entrada_carge.
     */
    public java.lang.String getHora_entrada_carge () {
        return hora_entrada_carge;
    }
    
    /**
     * Setter for property hora_entrada_carge.
     * @param hora_entrada_carge New value of property hora_entrada_carge.
     */
    public void setHora_entrada_carge (java.lang.String hora_entrada_carge) {
        this.hora_entrada_carge = hora_entrada_carge;
    }
    
    /**
     * Getter for property hora_entrega_doc_xom.
     * @return Value of property hora_entrega_doc_xom.
     */
    public java.lang.String getHora_entrega_doc_xom () {
        return hora_entrega_doc_xom;
    }
    
    /**
     * Setter for property hora_entrega_doc_xom.
     * @param hora_entrega_doc_xom New value of property hora_entrega_doc_xom.
     */
    public void setHora_entrega_doc_xom (java.lang.String hora_entrega_doc_xom) {
        this.hora_entrega_doc_xom = hora_entrega_doc_xom;
    }
    
    /**
     * Getter for property hora_llegada_planta.
     * @return Value of property hora_llegada_planta.
     */
    public java.lang.String getHora_llegada_planta () {
        return hora_llegada_planta;
    }
    
    /**
     * Setter for property hora_llegada_planta.
     * @param hora_llegada_planta New value of property hora_llegada_planta.
     */
    public void setHora_llegada_planta (java.lang.String hora_llegada_planta) {
        this.hora_llegada_planta = hora_llegada_planta;
    }
    
    /**
     * Getter for property hora_max_carge_24.
     * @return Value of property hora_max_carge_24.
     */
    public java.lang.String getHora_max_carge_24 () {
        return hora_max_carge_24;
    }
    
    /**
     * Setter for property hora_max_carge_24.
     * @param hora_max_carge_24 New value of property hora_max_carge_24.
     */
    public void setHora_max_carge_24 (java.lang.String hora_max_carge_24) {
        this.hora_max_carge_24 = hora_max_carge_24;
    }
    
    /**
     * Getter for property hora_max_carge_36.
     * @return Value of property hora_max_carge_36.
     */
    public java.lang.String getHora_max_carge_36 () {
        return hora_max_carge_36;
    }
    
    /**
     * Setter for property hora_max_carge_36.
     * @param hora_max_carge_36 New value of property hora_max_carge_36.
     */
    public void setHora_max_carge_36 (java.lang.String hora_max_carge_36) {
        this.hora_max_carge_36 = hora_max_carge_36;
    }
    
    /**
     * Getter for property hora_radicada_ocarge.
     * @return Value of property hora_radicada_ocarge.
     */
    public java.lang.String getHora_radicada_ocarge () {
        return hora_radicada_ocarge;
    }
    
    /**
     * Setter for property hora_radicada_ocarge.
     * @param hora_radicada_ocarge New value of property hora_radicada_ocarge.
     */
    public void setHora_radicada_ocarge (java.lang.String hora_radicada_ocarge) {
        this.hora_radicada_ocarge = hora_radicada_ocarge;
    }
    
    /**
     * Getter for property hora_salida_planta.
     * @return Value of property hora_salida_planta.
     */
    public java.lang.String getHora_salida_planta () {
        return hora_salida_planta;
    }
    
    /**
     * Setter for property hora_salida_planta.
     * @param hora_salida_planta New value of property hora_salida_planta.
     */
    public void setHora_salida_planta (java.lang.String hora_salida_planta) {
        this.hora_salida_planta = hora_salida_planta;
    }
    
    /**
     * Getter for property hora_solicitud.
     * @return Value of property hora_solicitud.
     */
    public java.lang.String getHora_solicitud () {
        return hora_solicitud;
    }
    
    /**
     * Setter for property hora_solicitud.
     * @param hora_solicitud New value of property hora_solicitud.
     */
    public void setHora_solicitud (java.lang.String hora_solicitud) {
        this.hora_solicitud = hora_solicitud;
    }
    
    /**
     * Getter for property hora_terminacion_carge.
     * @return Value of property hora_terminacion_carge.
     */
    public java.lang.String getHora_terminacion_carge () {
        return hora_terminacion_carge;
    }
    
    /**
     * Setter for property hora_terminacion_carge.
     * @param hora_terminacion_carge New value of property hora_terminacion_carge.
     */
    public void setHora_terminacion_carge (java.lang.String hora_terminacion_carge) {
        this.hora_terminacion_carge = hora_terminacion_carge;
    }
    
    /**
     * Getter for property max_carge_solicitud_24.
     * @return Value of property max_carge_solicitud_24.
     */
    public java.lang.String getMax_carge_solicitud_24 () {
        return max_carge_solicitud_24;
    }
    
    /**
     * Setter for property max_carge_solicitud_24.
     * @param max_carge_solicitud_24 New value of property max_carge_solicitud_24.
     */
    public void setMax_carge_solicitud_24 (java.lang.String max_carge_solicitud_24) {
        this.max_carge_solicitud_24 = max_carge_solicitud_24;
    }
    
    /**
     * Getter for property max_carge_solicitud_36.
     * @return Value of property max_carge_solicitud_36.
     */
    public java.lang.String getMax_carge_solicitud_36 () {
        return max_carge_solicitud_36;
    }
    
    /**
     * Setter for property max_carge_solicitud_36.
     * @param max_carge_solicitud_36 New value of property max_carge_solicitud_36.
     */
    public void setMax_carge_solicitud_36 (java.lang.String max_carge_solicitud_36) {
        this.max_carge_solicitud_36 = max_carge_solicitud_36;
    }
    
    /**
     * Getter for property nombre_cond.
     * @return Value of property nombre_cond.
     */
    public java.lang.String getNombre_cond () {
        return nombre_cond;
    }
    
    /**
     * Setter for property nombre_cond.
     * @param nombre_cond New value of property nombre_cond.
     */
    public void setNombre_cond (java.lang.String nombre_cond) {
        this.nombre_cond = nombre_cond;
    }
    
    /**
     * Getter for property oc.
     * @return Value of property oc.
     */
    public java.lang.String getOc () {
        return oc;
    }
    
    /**
     * Setter for property oc.
     * @param oc New value of property oc.
     */
    public void setOc (java.lang.String oc) {
        this.oc = oc;
    }
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt () {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt (java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property pedido.
     * @return Value of property pedido.
     */
    public java.lang.String getPedido () {
        return pedido;
    }
    
    /**
     * Setter for property pedido.
     * @param pedido New value of property pedido.
     */
    public void setPedido (java.lang.String pedido) {
        this.pedido = pedido;
    }
    
    /**
     * Getter for property peso.
     * @return Value of property peso.
     */
    public java.lang.String getPeso () {
        return peso;
    }
    
    /**
     * Setter for property peso.
     * @param peso New value of property peso.
     */
    public void setPeso (java.lang.String peso) {
        this.peso = peso;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca () {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca (java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property programado.
     * @return Value of property programado.
     */
    public java.lang.String getProgramado () {
        return programado;
    }
    
    /**
     * Setter for property programado.
     * @param programado New value of property programado.
     */
    public void setProgramado (java.lang.String programado) {
        this.programado = programado;
    }
    
    /**
     * Getter for property tiempo_carge.
     * @return Value of property tiempo_carge.
     */
    public java.lang.String getTiempo_carge () {
        return tiempo_carge;
    }
    
    /**
     * Setter for property tiempo_carge.
     * @param tiempo_carge New value of property tiempo_carge.
     */
    public void setTiempo_carge (java.lang.String tiempo_carge) {
        this.tiempo_carge = tiempo_carge;
    }
    
    /**
     * Getter for property tiempo_carge_despues_sol_36.
     * @return Value of property tiempo_carge_despues_sol_36.
     */
    public java.lang.String getTiempo_carge_despues_sol () {
        return tiempo_carge_despues_sol;
    }
    
    /**
     * Setter for property tiempo_carge_despues_sol_36.
     * @param tiempo_carge_despues_sol_36 New value of property tiempo_carge_despues_sol_36.
     */
    public void setTiempo_carge_despues_sol (java.lang.String tiempo_carge_despues_sol) {
        this.tiempo_carge_despues_sol = tiempo_carge_despues_sol;
    }
    
    /**
     * Getter for property tiempo_entrega_doc.
     * @return Value of property tiempo_entrega_doc.
     */
    public java.lang.String getTiempo_entrega_doc () {
        return tiempo_entrega_doc;
    }
    
    /**
     * Setter for property tiempo_entrega_doc.
     * @param tiempo_entrega_doc New value of property tiempo_entrega_doc.
     */
    public void setTiempo_entrega_doc (java.lang.String tiempo_entrega_doc) {
        this.tiempo_entrega_doc = tiempo_entrega_doc;
    }
    
    /**
     * Getter for property tiempo_espera.
     * @return Value of property tiempo_espera.
     */
    public java.lang.String getTiempo_espera () {
        return tiempo_espera;
    }
    
    /**
     * Setter for property tiempo_espera.
     * @param tiempo_espera New value of property tiempo_espera.
     */
    public void setTiempo_espera (java.lang.String tiempo_espera) {
        this.tiempo_espera = tiempo_espera;
    }
    
    /**
     * Getter for property tipo_vehiculo.
     * @return Value of property tipo_vehiculo.
     */
    public java.lang.String getTipo_vehiculo () {
        return tipo_vehiculo;
    }
    
    /**
     * Setter for property tipo_vehiculo.
     * @param tipo_vehiculo New value of property tipo_vehiculo.
     */
    public void setTipo_vehiculo (java.lang.String tipo_vehiculo) {
        this.tipo_vehiculo = tipo_vehiculo;
    }
    
    /**
     * Getter for property diferencia_fcarge_freal24.
     * @return Value of property diferencia_fcarge_freal24.
     */
    public java.lang.String getDiferencia_fcarge_freal24 () {
        return diferencia_fcarge_freal24;
    }
    
    /**
     * Setter for property diferencia_fcarge_freal24.
     * @param diferencia_fcarge_freal24 New value of property diferencia_fcarge_freal24.
     */
    public void setDiferencia_fcarge_freal24 (java.lang.String diferencia_fcarge_freal24) {
        this.diferencia_fcarge_freal24 = diferencia_fcarge_freal24;
    }
    
    /**
     * Getter for property delivery.
     * @return Value of property delivery.
     */
    public java.lang.String getDelivery() {
        return delivery;
    }
    
    /**
     * Setter for property delivery.
     * @param delivery New value of property delivery.
     */
    public void setDelivery(java.lang.String delivery) {
        this.delivery = delivery;
    }
    
    /**
     * Getter for property otros.
     * @return Value of property otros.
     */
    public java.lang.String getOtros() {
        return otros;
    }
    
    /**
     * Setter for property otros.
     * @param otros New value of property otros.
     */
    public void setOtros(java.lang.String otros) {
        this.otros = otros;
    }
    
    /**
     * Getter for property orirem.
     * @return Value of property orirem.
     */
    public java.lang.String getOrirem() {
        return orirem;
    }
    
    /**
     * Setter for property orirem.
     * @param orirem New value of property orirem.
     */
    public void setOrirem(java.lang.String orirem) {
        this.orirem = orirem;
    }
    
    /**
     * Getter for property oripla.
     * @return Value of property oripla.
     */
    public java.lang.String getOripla() {
        return oripla;
    }
    
    /**
     * Setter for property oripla.
     * @param oripla New value of property oripla.
     */
    public void setOripla(java.lang.String oripla) {
        this.oripla = oripla;
    }
    
}
