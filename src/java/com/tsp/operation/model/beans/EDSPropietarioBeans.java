
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;
//EDSPropietarioBeans

/**
 *
 * @author mariana
 */
public class EDSPropietarioBeans {

    public String nombreprop;
    public String idpropietario;
    public String direccion;
    public String correo;
    public String tipopersona;
    public String telefono;
    public String pais;
    public String codPais;
    public String ciudad;
    public String codCiudad;
    public String codDepartamento;
    public String nit;
    public String tipodoc;
    public String contribuyente;
    public String tiporegimen;
    public String iva;
    public String ica;
    public String retefuente;
    public String autoretenedor;
    public String hc;
    public String numcuenta;
    public String tipocuenta;
    public String banco;
    public String sedebanco;
    public String banagencia;
    public String tipopago;
    public String bancotransfer;
    public String sucursaltransfer;
    public String tipousu;
    public String perfil;
    public String estadousu;
    public String agencia;
    public String idusuario;
    public String passw;
    public String cia;
    public String cambioclav;
    public String base;
    public String representante;
    public String nitem;
    public String banRazonsocial;
    public String banDocRep;
    public String banNit;
    public String banCorreo;
    public String banRepresentante;
    public String bandireccion;
    public String banId;
    public String bancodbandera;
    public String bantippersona;
    public String banestado;
    public String edsnombre;
    public String edsdireccion;
    public String edsusuario;
    public String edsestadousuario;
    public String edscorreo;
    public String edstelefono;
    public String edsciudad;
    public String edsid;
    public String edsencargado;
    public String edspass;
    public String edsnitencargado;
    public String edsnitestacion;
    public String edsestado;
    public String edsdpagof;
    public String proEstado;
    public String proDescripcion;
    public String proId;
    public String proCodProducto;
    public String unid;
    public String uniNombreUnidad;
    public String unimedicion;
    public String uniestado;
    public String confperiodo;
    public String confproduc;
    public String confdescripcion;
    public String confcomision;
    public String confpreciosesion;
    public String confporcentaje;
    public String confvalor;
    public String codcli;
    public String num_venta;
    public String fecha_venta;
    public String planilla;
    public String cedula;
    public String conductor;
    public String placa;
    public String producto;
    public String precio_producto;
    public String cantidad_suministrada;
    public String unidad_medida;
    public String subtotal;
    public String comision;
    public String total;
    public String nombre_cuenta;
    public String cedula_cuenta;
    public String tipo_documento_con;
    public String cxp_proveedor;
    public String cxp_documento;
    public String cxp_vlr_neto;
    public String cxp_vlr_total_abonos;
    public String cxp_vlr_saldo;
    public String cxp_estado;
    public String usuarios_creacion;
    public String fecha_creacion;
    public String documentoajuste;
    public String nc_documento;
    public String nc_proveedor;
    public String base_numgalon_totalventa;
    public String factor_descuento;
    public String nc_vlr_neto;
    public String nc_vlr_total_abonos;
    public String nc_estado;
    public String nc_valor_saldo;
    public String existente;
    public String cantidad;

    public String getNombreprop() {
        return nombreprop;
    }

    public String getIdpropietario() {
        return idpropietario;
    }

    public void setIdpropietario(String idpropietario) {
        this.idpropietario = idpropietario;
    }

    public void setNombreprop(String nombreprop) {
        this.nombreprop = nombreprop;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipopersona() {
        return tipopersona;
    }

    public void setTipopersona(String tipopersona) {
        this.tipopersona = tipopersona;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getTipousu() {
        return tipousu;
    }

    public void setTipousu(String tipousu) {
        this.tipousu = tipousu;
    }

    public String getCia() {
        return cia;
    }

    public void setCia(String cia) {
        this.cia = cia;
    }

    public String getCambioclav() {
        return cambioclav;
    }

    public void setCambioclav(String cambioclav) {
        this.cambioclav = cambioclav;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getEstadousu() {
        return estadousu;
    }

    public void setEstadousu(String estadousu) {
        this.estadousu = estadousu;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }

    public String getPassw() {
        return passw;
    }

    public void setPassw(String passw) {
        this.passw = passw;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getNitem() {
        return nitem;
    }

    public void setNitem(String nitem) {
        this.nitem = nitem;
    }

    public String getBanId() {
        return banId;
    }

    public String getBancodbandera() {
        return bancodbandera;
    }

    public void setBancodbandera(String bancodbandera) {
        this.bancodbandera = bancodbandera;
    }

    public String getBantippersona() {
        return bantippersona;
    }

    public void setBantippersona(String bantippersona) {
        this.bantippersona = bantippersona;
    }

    public String getBanestado() {
        return banestado;
    }

    public void setBanestado(String banestado) {
        this.banestado = banestado;
    }

    public void setBanId(String banId) {
        this.banId = banId;
    }

    public String getBanRazonsocial() {
        return banRazonsocial;
    }

    public void setBanRazonsocial(String banRazonsocial) {
        this.banRazonsocial = banRazonsocial;
    }

    public String getBanDocRep() {
        return banDocRep;
    }

    public void setBanDocRep(String banDocRep) {
        this.banDocRep = banDocRep;
    }

    public String getBanNit() {
        return banNit;
    }

    public void setBanNit(String banNit) {
        this.banNit = banNit;
    }

    public String getBanCorreo() {
        return banCorreo;
    }

    public void setBanCorreo(String banCorreo) {
        this.banCorreo = banCorreo;
    }

    public String getBanRepresentante() {
        return banRepresentante;
    }

    public void setBanRepresentante(String banRepresentante) {
        this.banRepresentante = banRepresentante;
    }

    public String getBandireccion() {
        return bandireccion;
    }

    public void setBandireccion(String bandireccion) {
        this.bandireccion = bandireccion;
    }

    public String getEdsnombre() {
        return edsnombre;
    }

    public void setEdsnombre(String edsnombre) {
        this.edsnombre = edsnombre;
    }

    public String getEdsdireccion() {
        return edsdireccion;
    }

    public void setEdsdireccion(String edsdireccion) {
        this.edsdireccion = edsdireccion;
    }

    public String getEdsusuario() {
        return edsusuario;
    }

    public void setEdsusuario(String edsusuario) {
        this.edsusuario = edsusuario;
    }

    public String getEdsestadousuario() {
        return edsestadousuario;
    }

    public void setEdsestadousuario(String edsestadousuario) {
        this.edsestadousuario = edsestadousuario;
    }

    public String getEdscorreo() {
        return edscorreo;
    }

    public void setEdscorreo(String edscorreo) {
        this.edscorreo = edscorreo;
    }

    public String getEdstelefono() {
        return edstelefono;
    }

    public void setEdstelefono(String edstelefono) {
        this.edstelefono = edstelefono;
    }

    public String getEdsciudad() {
        return edsciudad;
    }

    public void setEdsciudad(String edsciudad) {
        this.edsciudad = edsciudad;
    }

    public String getEdsid() {
        return edsid;
    }

    public void setEdsid(String edsid) {
        this.edsid = edsid;
    }

    public String getEdsestado() {
        return edsestado;
    }

    public void setEdsestado(String edsestado) {
        this.edsestado = edsestado;
    }

    public String getEdsencargado() {
        return edsencargado;
    }

    public void setEdsencargado(String edsencargado) {
        this.edsencargado = edsencargado;
    }

    public String getEdspass() {
        return edspass;
    }

    public void setEdspass(String edspass) {
        this.edspass = edspass;
    }

    public String getEdsnitencargado() {
        return edsnitencargado;
    }

    public void setEdsnitencargado(String edsnitencargado) {
        this.edsnitencargado = edsnitencargado;
    }

    public String getCodCiudad() {
        return codCiudad;
    }

    public void setCodCiudad(String codCiudad) {
        this.codCiudad = codCiudad;
    }

    public String getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(String codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public String getCodPais() {
        return codPais;
    }

    public void setCodPais(String codPais) {
        this.codPais = codPais;
    }

    public String getProEstado() {
        return proEstado;
    }

    public void setProEstado(String proEstado) {
        this.proEstado = proEstado;
    }

    public String getProDescripcion() {
        return proDescripcion;
    }

    public void setProDescripcion(String proDescripcion) {
        this.proDescripcion = proDescripcion;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProCodProducto() {
        return proCodProducto;
    }

    public void setProCodProducto(String proCodProducto) {
        this.proCodProducto = proCodProducto;
    }

    public String getUnid() {
        return unid;
    }

    public void setUnid(String unid) {
        this.unid = unid;
    }

    public String getUniNombreUnidad() {
        return uniNombreUnidad;
    }

    public void setUniNombreUnidad(String uniNombreUnidad) {
        this.uniNombreUnidad = uniNombreUnidad;
    }

    public String getConfperiodo() {
        return confperiodo;
    }

    public void setConfperiodo(String confperiodo) {
        this.confperiodo = confperiodo;
    }

    public String getConfproduc() {
        return confproduc;
    }

    public void setConfproduc(String confproduc) {
        this.confproduc = confproduc;
    }

    public String getConfdescripcion() {
        return confdescripcion;
    }

    public void setConfdescripcion(String confdescripcion) {
        this.confdescripcion = confdescripcion;
    }

    public String getConfcomision() {
        return confcomision;
    }

    public void setConfcomision(String confcomision) {
        this.confcomision = confcomision;
    }

    public String getConfpreciosesion() {
        return confpreciosesion;
    }

    public void setConfpreciosesion(String confpreciosesion) {
        this.confpreciosesion = confpreciosesion;
    }

    public String getConfporcentaje() {
        return confporcentaje;
    }

    public void setConfporcentaje(String confporcentaje) {
        this.confporcentaje = confporcentaje;
    }

    public String getConfvalor() {
        return confvalor;
    }

    public void setConfvalor(String confvalor) {
        this.confvalor = confvalor;
    }

    public String getEdsnitestacion() {
        return edsnitestacion;
    }

    public void setEdsnitestacion(String edsnitestacion) {
        this.edsnitestacion = edsnitestacion;
    }

    public String getUnimedicion() {
        return unimedicion;
    }

    public void setUnimedicion(String unimedicion) {
        this.unimedicion = unimedicion;
    }

    public String getUniestado() {
        return uniestado;
    }

    public void setUniestado(String uniestado) {
        this.uniestado = uniestado;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getContribuyente() {
        return contribuyente;
    }

    public void setContribuyente(String contribuyente) {
        this.contribuyente = contribuyente;
    }

    public String getTiporegimen() {
        return tiporegimen;
    }

    public void setTiporegimen(String tiporegimen) {
        this.tiporegimen = tiporegimen;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getIca() {
        return ica;
    }

    public void setIca(String ica) {
        this.ica = ica;
    }

    public String getRetefuente() {
        return retefuente;
    }

    public void setRetefuente(String retefuente) {
        this.retefuente = retefuente;
    }

    public String getAutoretenedor() {
        return autoretenedor;
    }

    public void setAutoretenedor(String autoretenedor) {
        this.autoretenedor = autoretenedor;
    }

    public String getHc() {
        return hc;
    }

    public void setHc(String hc) {
        this.hc = hc;
    }

    public String getNumcuenta() {
        return numcuenta;
    }

    public void setNumcuenta(String numcuenta) {
        this.numcuenta = numcuenta;
    }

    public String getTipocuenta() {
        return tipocuenta;
    }

    public void setTipocuenta(String tipocuenta) {
        this.tipocuenta = tipocuenta;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getSedebanco() {
        return sedebanco;
    }

    public void setSedebanco(String sedebanco) {
        this.sedebanco = sedebanco;
    }

    public String getBanagencia() {
        return banagencia;
    }

    public void setBanagencia(String banagencia) {
        this.banagencia = banagencia;
    }

    public String getTipopago() {
        return tipopago;
    }

    public void setTipopago(String tipopago) {
        this.tipopago = tipopago;
    }

    public String getBancotransfer() {
        return bancotransfer;
    }

    public void setBancotransfer(String bancotransfer) {
        this.bancotransfer = bancotransfer;
    }

    public String getSucursaltransfer() {
        return sucursaltransfer;
    }

    public void setSucursaltransfer(String sucursaltransfer) {
        this.sucursaltransfer = sucursaltransfer;
    }

    public String getEdsdpagof() {
        return edsdpagof;
    }

    public void setEdsdpagof(String edsdpagof) {
        this.edsdpagof = edsdpagof;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getNum_venta() {
        return num_venta;
    }

    public void setNum_venta(String num_venta) {
        this.num_venta = num_venta;
    }

    public String getFecha_venta() {
        return fecha_venta;
    }

    public void setFecha_venta(String fecha_venta) {
        this.fecha_venta = fecha_venta;
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPrecio_producto() {
        return precio_producto;
    }

    public void setPrecio_producto(String precio_producto) {
        this.precio_producto = precio_producto;
    }

    public String getCantidad_suministrada() {
        return cantidad_suministrada;
    }

    public void setCantidad_suministrada(String cantidad_suministrada) {
        this.cantidad_suministrada = cantidad_suministrada;
    }

    public String getUnidad_medida() {
        return unidad_medida;
    }

    public void setUnidad_medida(String unidad_medida) {
        this.unidad_medida = unidad_medida;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getComision() {
        return comision;
    }

    public void setComision(String comision) {
        this.comision = comision;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getNombre_cuenta() {
        return nombre_cuenta;
    }

    public void setNombre_cuenta(String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }

    public String getCedula_cuenta() {
        return cedula_cuenta;
    }

    public void setCedula_cuenta(String cedula_cuenta) {
        this.cedula_cuenta = cedula_cuenta;
    }

    public String getCxp_proveedor() {
        return cxp_proveedor;
    }

    public void setCxp_proveedor(String cxp_proveedor) {
        this.cxp_proveedor = cxp_proveedor;
    }

    public String getCxp_documento() {
        return cxp_documento;
    }

    public void setCxp_documento(String cxp_documento) {
        this.cxp_documento = cxp_documento;
    }

    public String getCxp_vlr_neto() {
        return cxp_vlr_neto;
    }

    public void setCxp_vlr_neto(String cxp_vlr_neto) {
        this.cxp_vlr_neto = cxp_vlr_neto;
    }

    public String getCxp_vlr_total_abonos() {
        return cxp_vlr_total_abonos;
    }

    public void setCxp_vlr_total_abonos(String cxp_vlr_total_abonos) {
        this.cxp_vlr_total_abonos = cxp_vlr_total_abonos;
    }

    public String getCxp_vlr_saldo() {
        return cxp_vlr_saldo;
    }

    public void setCxp_vlr_saldo(String cxp_vlr_saldo) {
        this.cxp_vlr_saldo = cxp_vlr_saldo;
    }

    public String getUsuarios_creacion() {
        return usuarios_creacion;
    }

    public void setUsuarios_creacion(String usuarios_creacion) {
        this.usuarios_creacion = usuarios_creacion;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getCxp_estado() {
        return cxp_estado;
    }

    public void setCxp_estado(String cxp_estado) {
        this.cxp_estado = cxp_estado;
    }

    public String getDocumentoajuste() {
        return documentoajuste;
    }

    public void setDocumentoajuste(String documentoajuste) {
        this.documentoajuste = documentoajuste;
    }

    public String getTipo_documento_con() {
        return tipo_documento_con;
    }

    public void setTipo_documento_con(String tipo_documento_con) {
        this.tipo_documento_con = tipo_documento_con;
    }

    public String getNc_documento() {
        return nc_documento;
    }

    public void setNc_documento(String nc_documento) {
        this.nc_documento = nc_documento;
    }

    public String getNc_proveedor() {
        return nc_proveedor;
    }

    public void setNc_proveedor(String nc_proveedor) {
        this.nc_proveedor = nc_proveedor;
    }

    public String getBase_numgalon_totalventa() {
        return base_numgalon_totalventa;
    }

    public void setBase_numgalon_totalventa(String base_numgalon_totalventa) {
        this.base_numgalon_totalventa = base_numgalon_totalventa;
    }

    public String getFactor_descuento() {
        return factor_descuento;
    }

    public void setFactor_descuento(String factor_descuento) {
        this.factor_descuento = factor_descuento;
    }

    public String getNc_vlr_neto() {
        return nc_vlr_neto;
    }

    public void setNc_vlr_neto(String nc_vlr_neto) {
        this.nc_vlr_neto = nc_vlr_neto;
    }

    public String getNc_vlr_total_abonos() {
        return nc_vlr_total_abonos;
    }

    public void setNc_vlr_total_abonos(String nc_vlr_total_abonos) {
        this.nc_vlr_total_abonos = nc_vlr_total_abonos;
    }

    public String getNc_estado() {
        return nc_estado;
    }

    public void setNc_estado(String nc_estado) {
        this.nc_estado = nc_estado;
    }

    public String getNc_valor_saldo() {
        return nc_valor_saldo;
    }

    public void setNc_valor_saldo(String nc_valor_saldo) {
        this.nc_valor_saldo = nc_valor_saldo;
    }

    public String getExistente() {
        return existente;
    }

    public void setExistente(String existente) {
        this.existente = existente;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

}
