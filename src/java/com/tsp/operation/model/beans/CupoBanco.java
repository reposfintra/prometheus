package com.tsp.operation.model.beans;

/**
 * Bean para la tabla fin.cupo_banco
 * 24/11/2011
 * @author darrieta
 */
public class CupoBanco {
    
    private String banco;
    private String lineaCupo;
    private double cupo;
    private String dstrct;
    private String nombreBanco;
    private String descripcionLinea;
    private double saldo;

    /**
     * Get the value of saldo
     *
     * @return the value of saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * Set the value of saldo
     *
     * @param saldo new value of saldo
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    /**
     * Get the value of descipcionLinea
     *
     * @return the value of descipcionLinea
     */
    public String getDescripcionLinea() {
        return descripcionLinea;
    }

    /**
     * Set the value of descipcionLinea
     *
     * @param descipcionLinea new value of descipcionLinea
     */
    public void setDescripcionLinea(String descripcionLinea) {
        this.descripcionLinea = descripcionLinea;
    }


    /**
     * Get the value of nombreBanco
     *
     * @return the value of nombreBanco
     */
    public String getNombreBanco() {
        return nombreBanco;
    }

    /**
     * Set the value of nombreBanco
     *
     * @param nombreBanco new value of nombreBanco
     */
    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }


    /**
     * Get the value of dstrct
     *
     * @return the value of dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * Set the value of dstrct
     *
     * @param dstrct new value of dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }


    /**
     * Get the value of cupo
     *
     * @return the value of cupo
     */
    public double getCupo() {
        return cupo;
    }

    /**
     * Set the value of cupo
     *
     * @param cupo new value of cupo
     */
    public void setCupo(double cupo) {
        this.cupo = cupo;
    }


    /**
     * Get the value of linea_cupo
     *
     * @return the value of linea_cupo
     */
    public String getLineaCupo() {
        return lineaCupo;
    }

    /**
     * Set the value of linea_cupo
     *
     * @param linea_cupo new value of linea_cupo
     */
    public void setLineaCupo(String linea_cupo) {
        this.lineaCupo = linea_cupo;
    }


    /**
     * Get the value of banco
     *
     * @return the value of banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Set the value of banco
     *
     * @param banco new value of banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    
}
