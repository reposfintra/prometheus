/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class tablaPagoInicial {
    
    private String negocio;
    private String Tipo_neg;
    private String capital;
    private String interes;
    private String intXmora;
    private String GAC;
    private String Total;
    private String pctPagar;
    private String ValorPagar;
    private String SaldoVencido;
    private String SaldoCorriente;

    public tablaPagoInicial(String negocio, String Tipo_neg, String capital, String interes, String intXmora, String GAC, String Total, String pctPagar, String ValorPagar, String SaldoVencido, String SaldoCorriente) {
        this.negocio = negocio;
        this.Tipo_neg = Tipo_neg;
        this.capital = capital;
        this.interes = interes;
        this.intXmora = intXmora;
        this.GAC = GAC;
        this.Total = Total;
        this.pctPagar = pctPagar;
        this.ValorPagar = ValorPagar;
        this.SaldoVencido = SaldoVencido;
        this.SaldoCorriente = SaldoCorriente;
    }

   


    public tablaPagoInicial() {
    }

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    /**
     * @return the capital
     */
    public String getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(String capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public String getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(String interes) {
        this.interes = interes;
    }

    /**
     * @return the intXmora
     */
    public String getIntXmora() {
        return intXmora;
    }

    /**
     * @param intXmora the intXmora to set
     */
    public void setIntXmora(String intXmora) {
        this.intXmora = intXmora;
    }

    /**
     * @return the GAC
     */
    public String getGAC() {
        return GAC;
    }

    /**
     * @param GAC the GAC to set
     */
    public void setGAC(String GAC) {
        this.GAC = GAC;
    }

    /**
     * @return the Total
     */
    public String getTotal() {
        return Total;
    }

    /**
     * @param Total the Total to set
     */
    public void setTotal(String Total) {
        this.Total = Total;
    }
    
    
    /**
     * @return the Tipo_neg
     */
    public String getTipo_neg() {
        return Tipo_neg;
    }

    /**
     * @param Tipo_neg the Tipo_neg to set
     */
    public void setTipo_neg(String Tipo_neg) {
        this.Tipo_neg = Tipo_neg;
    }
    
  
    /**
     * @return the pctPagar
     */
    public String getPctPagar() {
        return pctPagar;
    }

    /**
     * @param pctPagar the pctPagar to set
     */
    public void setPctPagar(String pctPagar) {
        this.pctPagar = pctPagar;
    }

    /**
     * @return the ValorPagar
     */
    public String getValorPagar() {
        return ValorPagar;
    }

    /**
     * @param ValorPagar the ValorPagar to set
     */
    public void setValorPagar(String ValorPagar) {
        this.ValorPagar = ValorPagar;
    }

    /**
     * @return the SaldoVencido
     */
    public String getSaldoVencido() {
        return SaldoVencido;
    }

    /**
     * @param SaldoVencido the SaldoVencido to set
     */
    public void setSaldoVencido(String SaldoVencido) {
        this.SaldoVencido = SaldoVencido;
    }

    /**
     * @return the SaldoCorriente
     */
    public String getSaldoCorriente() {
        return SaldoCorriente;
    }

    /**
     * @param SaldoCorriente the SaldoCorriente to set
     */
    public void setSaldoCorriente(String SaldoCorriente) {
        this.SaldoCorriente = SaldoCorriente;
    }

    @Override
    public String toString() {
        return "tablaPagoInicial{" + "negocio=" + negocio + ", Tipo_neg=" + Tipo_neg + ", capital=" + capital + ", interes=" + interes + ", intXmora=" + intXmora + ", GAC=" + GAC + ", Total=" + Total + ", pctPagar=" + pctPagar + ", ValorPagar=" + ValorPagar + ", SaldoVencido=" + SaldoVencido + ", SaldoCorriente=" + SaldoCorriente + '}';
    }
    
}

