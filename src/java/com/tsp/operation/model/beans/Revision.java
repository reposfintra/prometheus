/***************************************
    * Nombre Clase ............. Revision.java
    * Descripci�n  .. . . . . .  Encapsula datos de las revisiones de los programas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/

package com.tsp.operation.model.beans;


import java.util.*;


public class Revision {
        
  private String dstrct ;
  private String tipo_reg ;
  private String equipo ;
  private String sitio ;
  private String ruta ;
  private String nombre_archivo ;
  private String extension ;
  private String fecha_revision ;
  private String usuario_revision ;
  private String tipo_revision ;
  private String observacion ;
  private String aprobado; 
  

  
    public Revision() {
          String vacio      = "";
          dstrct            = vacio ;
          tipo_reg          = vacio ;
          equipo            = vacio ;
          sitio             = vacio ;
          ruta              = vacio ;
          nombre_archivo    = vacio ;
          extension         = vacio ;
          fecha_revision    = vacio ;
          usuario_revision  = vacio ;
          tipo_revision     = vacio ;
          observacion       = vacio ;
          aprobado          = vacio ;
     }
    
        
    // SET :
    /**
     * M�todos que guardan valores
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    
    
     /**
     * M�todos que setea el valor del distrito
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    
    public void setDistrito(String val){
        this.dstrct   = val;
    }
    
    
     /**
     * M�todos que setea el valor del tipo registro
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setTipoRegistro(String val){
        this.tipo_reg = val;
    }
    
    
     /**
     * M�todos que setea el valor del equipo
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setEquipo(String val){
        this.equipo   = val;
    }
    
     /**
     * M�todos que setea el valor del sitio
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setSitio(String val){
        this.sitio    = val;
    }
    
     /**
     * M�todos que setea el valor de la ruta
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setRuta(String val){
        this.ruta     = val;
    }
    
    
     /**
     * M�todos que setea el valor del archivo
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setArchivo(String val){
        this.nombre_archivo = val;
    }
    
    
     /**
     * M�todos que setea el valor de la extension
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setExtension(String val){
        this.extension = val;
    }
    
    
     /**
     * M�todos que setea el valor del la fecha
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setFecha(String val){
        this.fecha_revision = val;
    }
    
     /**
     * M�todos que setea el valor del usuario
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setUser(String val){
        this.usuario_revision = val;
    }
    
    
     /**
     * M�todos que setea el valor del tipo revision
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setTipoRevision(String val){
        this.tipo_revision = val;
    }
    
     /**
     * M�todos que setea el valor de la observacion
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setObservacion(String val){
        this.observacion = val;
    }
    
    
     /**
     * M�todos que setea el valor del aprobador
     * @autor.......fvillacob 
     * @parameter ... String val
     * @version.....1.0.     
     **/ 
    public void setAprovado(String val){
        this.dstrct = val;
    }
    
    
    
    
    
    
    // GET :
    /**
     * M�todos que devuelven valores
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    
    
    
    /**
     * M�todos que devuelve el valor del distrito
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getDistrito(){
        return this.dstrct   ;
    }
    
    
    /**
     * M�todos que devuelve el valor del tipo registro
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getTipoRegistro(){
        return this.tipo_reg ;
    }
    
    
    /**
     * M�todos que devuelve el valor del equipo
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getEquipo(){
        return this.equipo   ;
    }
    
    
    /**
     * M�todos que devuelve el valor del sitio
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getSitio(){
       return  this.sitio    ;
    }
    
    
    /**
     * M�todos que devuelve el valor de la ruta
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getRuta(){
        return this.ruta    ;
    }
    
    
    /**
     * M�todos que devuelve el valor del archivo
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getArchivo(){
        return this.nombre_archivo ;
    }
    
    
    /**
     * M�todos que devuelve el valor de la extension
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getExtension(){
        return this.extension;
    }
    
    
    /**
     * M�todos que devuelve el valor de la fecha
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getFecha(){
        return this.fecha_revision ;
    }
    
    
    /**
     * M�todos que devuelve el valor del usuario
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getUser(){
        return this.usuario_revision ;
    }
    
    
    /**
     * M�todos que devuelve el valor del tipo revision
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getTipoRevision(){
        return this.tipo_revision ;
    }
    
    
    /**
     * M�todos que devuelve el valor de la observacion
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getObservacion(){
        return this.observacion ;
    }
    
    
    /**
     * M�todos que devuelve el valor del aprobador
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/
    public String getAprovado(){
        return this.dstrct ;
    }
    
    
    
    
}
