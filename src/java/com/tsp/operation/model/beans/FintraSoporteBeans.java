/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class FintraSoporteBeans {

    public String numdoc;
    public String tipodoc;
    public String tercero;
    public String grupo_transaccion;
    public String transaccion;
    public String periodo;
    public String cuenta;
    public String detalle;
    public String nit_tercero;
    public String nuevo_detalle;
    public String valor_debito;
    public String valor_credito;
    public String creation_date;
    public String creation_user;
    public String comprobante;

    public String id;
    public String tipo_documento;
    public String documento;
    public String nit;
    public String tipo;
    public String banco;
    public String sucursal;
    public String cambio;
    public String fecha_nueva;
    public String periodo_nuevo;
    public String codigo;
    public String descripcion;
    public String fecha_contabilizacion;
    
    public String factura;
    public String codcli;
    public String concepto;
    public String valor;
    public String cuenta_ajuste;
    public String fecha_consignacion;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getTercero() {
        return tercero;
    }

    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    public String getGrupo_transaccion() {
        return grupo_transaccion;
    }

    public void setGrupo_transaccion(String grupo_transaccion) {
        this.grupo_transaccion = grupo_transaccion;
    }

    public String getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.transaccion = transaccion;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getNit_tercero() {
        return nit_tercero;
    }

    public void setNit_tercero(String nit_tercero) {
        this.nit_tercero = nit_tercero;
    }

    public String getNuevo_detalle() {
        return nuevo_detalle;
    }

    public void setNuevo_detalle(String nuevo_detalle) {
        this.nuevo_detalle = nuevo_detalle;
    }

    public String getValor_debito() {
        return valor_debito;
    }

    public void setValor_debito(String valor_debito) {
        this.valor_debito = valor_debito;
    }

    public String getValor_credito() {
        return valor_credito;
    }

    public void setValor_credito(String valor_credito) {
        this.valor_credito = valor_credito;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getComprobante() {
        return comprobante;
    }

    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getCambio() {
        return cambio;
    }

    public void setCambio(String cambio) {
        this.cambio = cambio;
    }

    public String getFecha_nueva() {
        return fecha_nueva;
    }

    public void setFecha_nueva(String fecha_nueva) {
        this.fecha_nueva = fecha_nueva;
    }

    public String getPeriodo_nuevo() {
        return periodo_nuevo;
    }

    public void setPeriodo_nuevo(String periodo_nuevo) {
        this.periodo_nuevo = periodo_nuevo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    public void setFecha_contabilizacion(String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getCodcli() {
        return codcli;
    }

    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getCuenta_ajuste() {
        return cuenta_ajuste;
    }

    public void setCuenta_ajuste(String cuenta_ajuste) {
        this.cuenta_ajuste = cuenta_ajuste;
    }

    public String getFecha_consignacion() {
        return fecha_consignacion;
    }

    public void setFecha_consignacion(String fecha_consignacion) {
        this.fecha_consignacion = fecha_consignacion;
    }

    
}
