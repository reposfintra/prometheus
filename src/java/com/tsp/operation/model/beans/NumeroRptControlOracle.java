/*
 * NumeroRptControlOracle.java
 *
 * Created on 12 de septiembre de 2005, 10:06
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;

/**
 *
 * @author  Henry
 */

public class NumeroRptControlOracle implements Serializable{
    
    private String BRANCH_CODE;
    private String BANCK_ACCT_NO;
    private String SUPP_TO_PAY;
    private String EXT_INV_NO;
    private String CHEQUE_RUN_NO;
    private String CHEQUE_NO;
    private String LOC_INV_ORIG;
    private String SUPPLIER_NO;
    private String INV_NO;
    private String SUPPLIER_NAME;
    private String PO_NO;
    private String NumMov;
    
    public NumeroRptControlOracle load(ResultSet rs) throws SQLException {
        NumeroRptControlOracle  numRpt = new NumeroRptControlOracle();
        numRpt.setBRANCH_CODE(rs.getString("BRANCH_CODE"));
        numRpt.setBANCK_ACCT_NO(rs.getString("BANCK_ACCT_NO"));
        numRpt.setSUPP_TO_PAY(rs.getString("SUPP_TO_PAY"));
        numRpt.setEXT_INV_NO(rs.getString("EXT_INV_NO"));
        numRpt.setCHEQUE_RUN_NO(rs.getString("CHEQUE_RUN_NO"));
        numRpt.setCHEQUE_NO(rs.getString("CHEQUE_NO"));
        numRpt.setLOC_INV_ORIG(rs.getString("LOC_INV_ORIG"));
        numRpt.setSUPPLIER_NO(rs.getString("SUPPLIER_NO"));
        numRpt.setINV_NO(rs.getString("INV_NO"));
        numRpt.setSUPPLIER_NAME(rs.getString("SUPPLIER_NAME"));
        numRpt.setPO_NO(rs.getString("PO_NO"));
        numRpt.setNumMov(rs.getString("NUMMOV"));
        return numRpt;
    }

    /** Creates a new instance of NumeroRptControlOracle */
    public NumeroRptControlOracle() {
    }
    
    /**
     * Getter for property BRANCH_CODE.
     * @return Value of property BRANCH_CODE.
     */
    public java.lang.String getBRANCH_CODE() {
        return BRANCH_CODE;
    }
    
    /**
     * Setter for property BRANCH_CODE.
     * @param BRANCH_CODE New value of property BRANCH_CODE.
     */
    public void setBRANCH_CODE(java.lang.String BRANCH_CODE) {
        this.BRANCH_CODE = BRANCH_CODE;
    }
    
    /**
     * Getter for property BANCK_ACCT_NO.
     * @return Value of property BANCK_ACCT_NO.
     */
    public java.lang.String getBANCK_ACCT_NO() {
        return BANCK_ACCT_NO;
    }
    
    /**
     * Setter for property BANCK_ACCT_NO.
     * @param BANCK_ACCT_NO New value of property BANCK_ACCT_NO.
     */
    public void setBANCK_ACCT_NO(java.lang.String BANCK_ACCT_NO) {
        this.BANCK_ACCT_NO = BANCK_ACCT_NO;
    }
    
    /**
     * Getter for property SUPP_TO_PAY.
     * @return Value of property SUPP_TO_PAY.
     */
    public java.lang.String getSUPP_TO_PAY() {
        return SUPP_TO_PAY;
    }
    
    /**
     * Setter for property SUPP_TO_PAY.
     * @param SUPP_TO_PAY New value of property SUPP_TO_PAY.
     */
    public void setSUPP_TO_PAY(java.lang.String SUPP_TO_PAY) {
        this.SUPP_TO_PAY = SUPP_TO_PAY;
    }
    
    /**
     * Getter for property EXT_INV_NO.
     * @return Value of property EXT_INV_NO.
     */
    public java.lang.String getEXT_INV_NO() {
        return EXT_INV_NO;
    }
    
    /**
     * Setter for property EXT_INV_NO.
     * @param EXT_INV_NO New value of property EXT_INV_NO.
     */
    public void setEXT_INV_NO(java.lang.String EXT_INV_NO) {
        this.EXT_INV_NO = EXT_INV_NO;
    }
    
    /**
     * Getter for property CHEQUE_RUN_NO.
     * @return Value of property CHEQUE_RUN_NO.
     */
    public java.lang.String getCHEQUE_RUN_NO() {
        return CHEQUE_RUN_NO;
    }
    
    /**
     * Setter for property CHEQUE_RUN_NO.
     * @param CHEQUE_RUN_NO New value of property CHEQUE_RUN_NO.
     */
    public void setCHEQUE_RUN_NO(java.lang.String CHEQUE_RUN_NO) {
        this.CHEQUE_RUN_NO = CHEQUE_RUN_NO;
    }
    
    /**
     * Getter for property CHEQUE_NO.
     * @return Value of property CHEQUE_NO.
     */
    public java.lang.String getCHEQUE_NO() {
        return CHEQUE_NO;
    }
    
    /**
     * Setter for property CHEQUE_NO.
     * @param CHEQUE_NO New value of property CHEQUE_NO.
     */
    public void setCHEQUE_NO(java.lang.String CHEQUE_NO) {
        this.CHEQUE_NO = CHEQUE_NO;
    }
    
    /**
     * Getter for property LOC_INV_ORIG.
     * @return Value of property LOC_INV_ORIG.
     */
    public java.lang.String getLOC_INV_ORIG() {
        return LOC_INV_ORIG;
    }
    
    /**
     * Setter for property LOC_INV_ORIG.
     * @param LOC_INV_ORIG New value of property LOC_INV_ORIG.
     */
    public void setLOC_INV_ORIG(java.lang.String LOC_INV_ORIG) {
        this.LOC_INV_ORIG = LOC_INV_ORIG;
    }
    
    /**
     * Getter for property SUPPLIER_NO.
     * @return Value of property SUPPLIER_NO.
     */
    public java.lang.String getSUPPLIER_NO() {
        return SUPPLIER_NO;
    }
    
    /**
     * Setter for property SUPPLIER_NO.
     * @param SUPPLIER_NO New value of property SUPPLIER_NO.
     */
    public void setSUPPLIER_NO(java.lang.String SUPPLIER_NO) {
        this.SUPPLIER_NO = SUPPLIER_NO;
    }
    
    /**
     * Getter for property INV_NO.
     * @return Value of property INV_NO.
     */
    public java.lang.String getINV_NO() {
        return INV_NO;
    }
    
    /**
     * Setter for property INV_NO.
     * @param INV_NO New value of property INV_NO.
     */
    public void setINV_NO(java.lang.String INV_NO) {
        this.INV_NO = INV_NO;
    }
    
    /**
     * Getter for property SUPPLIER_NAME.
     * @return Value of property SUPPLIER_NAME.
     */
    public java.lang.String getSUPPLIER_NAME() {
        return SUPPLIER_NAME;
    }
    
    /**
     * Setter for property SUPPLIER_NAME.
     * @param SUPPLIER_NAME New value of property SUPPLIER_NAME.
     */
    public void setSUPPLIER_NAME(java.lang.String SUPPLIER_NAME) {
        this.SUPPLIER_NAME = SUPPLIER_NAME;
    }
    
    /**
     * Getter for property PO_NO.
     * @return Value of property PO_NO.
     */
    public java.lang.String getPO_NO() {
        return PO_NO;
    }
    
    /**
     * Setter for property PO_NO.
     * @param PO_NO New value of property PO_NO.
     */
    public void setPO_NO(java.lang.String PO_NO) {
        this.PO_NO = PO_NO;
    }
    
    /**
     * Getter for property NumMov.
     * @return Value of property NumMov.
     */
    public java.lang.String getNumMov() {
        return NumMov;
    }
    
    /**
     * Setter for property NumMov.
     * @param NumMov New value of property NumMov.
     */
    public void setNumMov(java.lang.String NumMov) {
        this.NumMov = NumMov;
    }
    
}
