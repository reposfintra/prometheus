/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class ConfiguracionTablaInicial {
    
    private int id;
    private String tipo_negocio;
    private String incluyeCapital;
    private String incluyeInteres;
    private String incluyeIntxmora;
    private String incluyeGac;
    private String pagoTotal;
    private String dstrct;
    private String reg_status;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;

    public ConfiguracionTablaInicial() {
    }

    public ConfiguracionTablaInicial(int id, String tipo_negocio, String incluyeCapital, String incluyeInteres, String incluyeIntxmora, String incluyeGac, String pagoTotal, String dstrct, String reg_status, String creation_user, String creation_date, String user_update, String last_update) {
        this.id = id;
        this.tipo_negocio = tipo_negocio;
        this.incluyeCapital = incluyeCapital;
        this.incluyeInteres = incluyeInteres;
        this.incluyeIntxmora = incluyeIntxmora;
        this.incluyeGac = incluyeGac;
        this.pagoTotal = pagoTotal;
        this.dstrct = dstrct;
        this.reg_status = reg_status;
        this.creation_user = creation_user;
        this.creation_date = creation_date;
        this.user_update = user_update;
        this.last_update = last_update;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the tipo_negocio
     */
    public String getTipo_negocio() {
        return tipo_negocio;
    }

    /**
     * @param tipo_negocio the tipo_negocio to set
     */
    public void setTipo_negocio(String tipo_negocio) {
        this.tipo_negocio = tipo_negocio;
    }

    /**
     * @return the incluyeCapital
     */
    public String getIncluyeCapital() {
        return incluyeCapital;
    }

    /**
     * @param incluyeCapital the incluyeCapital to set
     */
    public void setIncluyeCapital(String incluyeCapital) {
        this.incluyeCapital = incluyeCapital;
    }

    /**
     * @return the incluyeInteres
     */
    public String getIncluyeInteres() {
        return incluyeInteres;
    }

    /**
     * @param incluyeInteres the incluyeInteres to set
     */
    public void setIncluyeInteres(String incluyeInteres) {
        this.incluyeInteres = incluyeInteres;
    }

    /**
     * @return the incluyeIntxmora
     */
    public String getIncluyeIntxmora() {
        return incluyeIntxmora;
    }

    /**
     * @param incluyeIntxmora the incluyeIntxmora to set
     */
    public void setIncluyeIntxmora(String incluyeIntxmora) {
        this.incluyeIntxmora = incluyeIntxmora;
    }

    /**
     * @return the incluyeGac
     */
    public String getIncluyeGac() {
        return incluyeGac;
    }

    /**
     * @param incluyeGac the incluyeGac to set
     */
    public void setIncluyeGac(String incluyeGac) {
        this.incluyeGac = incluyeGac;
    }

    /**
     * @return the pagoTotal
     */
    public String getPagoTotal() {
        return pagoTotal;
    }

    /**
     * @param pagoTotal the pagoTotal to set
     */
    public void setPagoTotal(String pagoTotal) {
        this.pagoTotal = pagoTotal;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }
    
    
           
    
}
