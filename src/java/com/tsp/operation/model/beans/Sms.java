/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author jpinedo
 * fecha : 2011-08-25
 */




public class Sms
{

    /*----------Datos Cuenta-------------------*/
    private String cuenta="";
    private String usuario="";
    private float creditos_cuenta=0;
    /*-----------------------------------------*/

    private int id;
    private String idsms;
    private String token;
    private String estado_fintra="N";
    private String estado="";
    private float creditos=0;      
    private String cell;
    private String sms;
    private String creationDateFintra;
    private String creationDate;
    private String fecha_envio_fintra;
    private String fecha_envio="0099-01-01 00:00:00";
    private String comentario="";
    private String  nit="";
    private String  tipo="";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdsms() {
        return idsms;
    }

    public void setIdsms(String idsms) {
        this.idsms = idsms;
    }


    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }


    public float getCreditos_cuenta() {
        return creditos_cuenta;
    }

    public void setCreditos_cuenta(float creditos_cuenta) {
        this.creditos_cuenta = creditos_cuenta;
    }



    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public float getCreditos() {
        return creditos;
    }

   public void SetCreditos(float creditos)
   {
        this.creditos=creditos;
   }

   public String getEstado_fintra() {
        return this.estado_fintra;
    }
    public void setEstado_fintra(String estado) {
        this.estado_fintra = estado;
    }

   public String getEstado() {
        return this.estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }



    public String getCell() {
        return this.cell;
    }
    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getSMS() {
        return this.sms;
    }
    public void setSMS(String sms)
    {
        this.sms = sms;
    }


    public String getcreationDateFintra()
    {
        return this.creationDateFintra;
    }
    public void setcreationDateFintra(String creationDateFintra)
    {
        this.creationDateFintra = creationDateFintra;
    }

    public String getcreationDate()
    {
        return this.creationDate;
    }
    public void setcreationDate(String creationDate)
    {
        this.creationDate = creationDate;
    }


    public String getFechaEnvioFintra()
    {
        return this.fecha_envio_fintra;
    }
    public void setFechaEnvioFintra(String fecha_envio_fintra)
    {
        this.fecha_envio_fintra = fecha_envio_fintra;
    }

    public String getFechaEnvio()
    {
        return this.fecha_envio;
    }
    public void setFechaEnvio(String fecha_envio)
    {
        this.fecha_envio = fecha_envio;
    }


    public String getComentario()
    {
        return this.comentario;
    }
    public void setComentario(String comentario)
    {
        this.comentario = comentario;
    }


    public String getNit()
    {
        return this.nit;
    }
    public void setNit(String nit)
    {
        this.nit = nit;
    }
    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }



}
