/*
 * Unidades.java
 *
 * Created on 8 de junio de 2005, 05:49 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  kreales
 */
public class Unidades {
    
    private String codigo;
    private String descripcion;
    
    /** Creates a new instance of Unidades */
    public Unidades() {
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
}
