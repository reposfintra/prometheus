/*
 * FiltrosUsuarios.java
 *
 * Created on 13 de septiembre de 2005, 01:36 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  David A
 */
public class FiltrosUsuarios {
    private String idUsuario="";
    private String consulta="";
    private String linea ="";
    private String nombre="";
    private String dstrct="";
    private String creation_user="";
    private String filtro ="";
    /** Creates a new instance of FiltrosUsuarios */
    public FiltrosUsuarios() {
    }
    
     public static FiltrosUsuarios load(ResultSet rs)throws SQLException {
         FiltrosUsuarios filtros = new FiltrosUsuarios();
         filtros.setIdUsuario(rs.getString("idusuario"));
         filtros.setConsulta(rs.getString("consulta"));
         filtros.setLinea(rs.getString("linea"));
         filtros.setNombre(rs.getString("nombre"));
         filtros.setDstrct (rs.getString ("dstrct"));
         filtros.setCreation_user (rs.getString("creation_user"));
         return filtros;
    }
     
     public void setIdUsuario(String idUsuario){
         this.idUsuario=idUsuario;
     }
     
     public String getIdUsuario(){
         return this.idUsuario;
     }
    
     public void setConsulta(String consulta){
         this.consulta=consulta;
     }
     
     public String getConsulta(){
         return this.consulta;
     }
     
     public void setLinea(String linea){
         this.linea=linea;
     }
     
     public String getLinea(){
         return this.linea;
     }
     
     public void setNombre(String nombre){
         this.nombre=nombre;
     }
     
     public String getNombre(){
         return this.nombre;
     }
     
     /**
      * Getter for property dstrct.
      * @return Value of property dstrct.
      */
     public java.lang.String getDstrct ()
     {
         return dstrct;
     }
     
     /**
      * Setter for property dstrct.
      * @param dstrct New value of property dstrct.
      */
     public void setDstrct (java.lang.String dstrct)
     {
         this.dstrct = dstrct;
     }
     
     /**
      * Getter for property creation_user.
      * @return Value of property creation_user.
      */
     public java.lang.String getCreation_user ()
     {
         return creation_user;
     }
     
     /**
      * Setter for property creation_user.
      * @param creation_user New value of property creation_user.
      */
     public void setCreation_user (java.lang.String creation_user)
     {
         this.creation_user = creation_user;
     }
     
     /**
      * Getter for property filtro.
      * @return Value of property filtro.
      */
     public java.lang.String getFiltro() {
         return filtro;
     }
     
     /**
      * Setter for property filtro.
      * @param filtro New value of property filtro.
      */
     public void setFiltro(java.lang.String filtro) {
         this.filtro = filtro;
     }
     
}
