/***********************************************************************************
 * Nombre clase :                 ReporteVehPerdidos.java                          *
 * Descripcion :                  Clase que maneja los atributos relacionados con  *
 *                                el reporte de viajes                             *
 * Autor :                        Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                        10 de enero de 2006, 10:44 AM                    *
 * Version :                      1.0                                              *
 * Copyright :                    Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;

public class ReporteVehPerdidos {
    private String placa;
    private String numpla;
    private String origen;
    private String destino;
    private String estado;
    private String fecha_disponibilidad;
    private String clase;
    private String agencia;
    /** Creates a new instance of ReporteVehPerdidos */
    public ReporteVehPerdidos() {
    }
    
    
    /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo ReporteVehPerdidos
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static ReporteVehPerdidos load(ResultSet rs)throws Exception{
        ReporteVehPerdidos datos = new ReporteVehPerdidos();
        datos.setPlaca(rs.getString("placa"));
        datos.setNumpla(rs.getString("planilla"));
        datos.setAgencia(rs.getString("agencia"));
        datos.setOrigen(rs.getString("origen"));
        datos.setDestino(rs.getString("destino"));
        datos.setFecha_disponibilidad(rs.getString("disponibilidad"));
        String tipo = (rs.getString("tipo").equals("T"))?"TRAILER":"CABEZOTE";
        datos.setClase(tipo);
        String status = (!rs.getString("planilla").equals(""))?"Inicial":"Asignacion";
        datos.setEstado(status);
        return datos;
    }
    /**
     * Getter for property clase.
     * @return Value of property clase.
     */
    public java.lang.String getClase() {
        return clase;
    }
    
    /**
     * Setter for property clase.
     * @param clase New value of property clase.
     */
    public void setClase(java.lang.String clase) {
        this.clase = clase;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property fecha_disponibilidad.
     * @return Value of property fecha_disponibilidad.
     */
    public java.lang.String getFecha_disponibilidad() {
        return fecha_disponibilidad;
    }
    
    /**
     * Setter for property fecha_disponibilidad.
     * @param fecha_disponibilidad New value of property fecha_disponibilidad.
     */
    public void setFecha_disponibilidad(java.lang.String fecha_disponibilidad) {
        this.fecha_disponibilidad = fecha_disponibilidad;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
}
