/*
 * factura_importacion.java
 *
 * Created on 22 de marzo de 2007, 08:55 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  E
 */
public class factura_importacion {
    private String dstrct;
    private String plazo;
    private String nit;
    private String codcli;
    private String concepto;
    private String fecha_factura;
    private String descripcion;
    private String observacion;
    private double valor_factura;
    private Double valor_tasa;
    private String moneda;
    private int cantidad_items;
    private String forma_pago;
    private String agencia_facturacion;
    
    
    private int item;
    private boolean  extraflete;
    private String codigo;
    private String std_job_no;
    private String numero_remesa;
    private String descripcion_item;
    private String codigo_cuenta_contable;
    private Double cantidad;
    private double valor_unitario;
    private double valor_item;
    private String fecrem;
    private String unidad;
    private String auxliliar;
    private String tiposubledger;
    
    
   private String creation_user;
   private String creation_date; 
   private String user_update; 
   private String last_update; 
   private String fecha_migracion; 
    
    
    /** Creates a new instance of factura_importacion */
    public factura_importacion() {
    }
    
    /**
     * Getter for property agencia_facturacion.
     * @return Value of property agencia_facturacion.
     */
    public java.lang.String getAgencia_facturacion() {
        return agencia_facturacion;
    }
    
    /**
     * Setter for property agencia_facturacion.
     * @param agencia_facturacion New value of property agencia_facturacion.
     */
    public void setAgencia_facturacion(java.lang.String agencia_facturacion) {
        this.agencia_facturacion = agencia_facturacion;
    }
    
    /**
     * Getter for property auxliliar.
     * @return Value of property auxliliar.
     */
    public java.lang.String getAuxliliar() {
        return auxliliar;
    }
    
    /**
     * Setter for property auxliliar.
     * @param auxliliar New value of property auxliliar.
     */
    public void setAuxliliar(java.lang.String auxliliar) {
        this.auxliliar = auxliliar;
    }
    
    /**
     * Getter for property cantidad.
     * @return Value of property cantidad.
     */
    public java.lang.Double getCantidad() {
        return cantidad;
    }
    
    /**
     * Setter for property cantidad.
     * @param cantidad New value of property cantidad.
     */
    public void setCantidad(java.lang.Double cantidad) {
        this.cantidad = cantidad;
    }
    
    /**
     * Getter for property cantidad_items.
     * @return Value of property cantidad_items.
     */
    public int getCantidad_items() {
        return cantidad_items;
    }
    
    /**
     * Setter for property cantidad_items.
     * @param cantidad_items New value of property cantidad_items.
     */
    public void setCantidad_items(int cantidad_items) {
        this.cantidad_items = cantidad_items;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property codigo_cuenta_contable.
     * @return Value of property codigo_cuenta_contable.
     */
    public java.lang.String getCodigo_cuenta_contable() {
        return codigo_cuenta_contable;
    }
    
    /**
     * Setter for property codigo_cuenta_contable.
     * @param codigo_cuenta_contable New value of property codigo_cuenta_contable.
     */
    public void setCodigo_cuenta_contable(java.lang.String codigo_cuenta_contable) {
        this.codigo_cuenta_contable = codigo_cuenta_contable;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property descripcion_item.
     * @return Value of property descripcion_item.
     */
    public java.lang.String getDescripcion_item() {
        return descripcion_item;
    }
    
    /**
     * Setter for property descripcion_item.
     * @param descripcion_item New value of property descripcion_item.
     */
    public void setDescripcion_item(java.lang.String descripcion_item) {
        this.descripcion_item = descripcion_item;
    }
    
    /**
     * Getter for property fecha_factura.
     * @return Value of property fecha_factura.
     */
    public java.lang.String getFecha_factura() {
        return fecha_factura;
    }
    
    /**
     * Setter for property fecha_factura.
     * @param fecha_factura New value of property fecha_factura.
     */
    public void setFecha_factura(java.lang.String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }
    
    /**
     * Getter for property fecha_migracion.
     * @return Value of property fecha_migracion.
     */
    public java.lang.String getFecha_migracion() {
        return fecha_migracion;
    }
    
    /**
     * Setter for property fecha_migracion.
     * @param fecha_migracion New value of property fecha_migracion.
     */
    public void setFecha_migracion(java.lang.String fecha_migracion) {
        this.fecha_migracion = fecha_migracion;
    }
    
    /**
     * Getter for property fecrem.
     * @return Value of property fecrem.
     */
    public java.lang.String getFecrem() {
        return fecrem;
    }
    
    /**
     * Setter for property fecrem.
     * @param fecrem New value of property fecrem.
     */
    public void setFecrem(java.lang.String fecrem) {
        this.fecrem = fecrem;
    }
    
    /**
     * Getter for property forma_pago.
     * @return Value of property forma_pago.
     */
    public java.lang.String getForma_pago() {
        return forma_pago;
    }
    
    /**
     * Setter for property forma_pago.
     * @param forma_pago New value of property forma_pago.
     */
    public void setForma_pago(java.lang.String forma_pago) {
        this.forma_pago = forma_pago;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public int getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(int item) {
        this.item = item;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property numero_remesa.
     * @return Value of property numero_remesa.
     */
    public java.lang.String getNumero_remesa() {
        return numero_remesa;
    }
    
    /**
     * Setter for property numero_remesa.
     * @param numero_remesa New value of property numero_remesa.
     */
    public void setNumero_remesa(java.lang.String numero_remesa) {
        this.numero_remesa = numero_remesa;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property plazo.
     * @return Value of property plazo.
     */
    public java.lang.String getPlazo() {
        return plazo;
    }
    
    /**
     * Setter for property plazo.
     * @param plazo New value of property plazo.
     */
    public void setPlazo(java.lang.String plazo) {
        this.plazo = plazo;
    }
    
    /**
     * Getter for property tiposubledger.
     * @return Value of property tiposubledger.
     */
    public java.lang.String getTiposubledger() {
        return tiposubledger;
    }
    
    /**
     * Setter for property tiposubledger.
     * @param tiposubledger New value of property tiposubledger.
     */
    public void setTiposubledger(java.lang.String tiposubledger) {
        this.tiposubledger = tiposubledger;
    }
    
    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property valor_factura.
     * @return Value of property valor_factura.
     */
    public double getValor_factura() {
        return valor_factura;
    }
    
    /**
     * Setter for property valor_factura.
     * @param valor_factura New value of property valor_factura.
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }
    
    /**
     * Getter for property valor_item.
     * @return Value of property valor_item.
     */
    public double getValor_item() {
        return valor_item;
    }
    
    /**
     * Setter for property valor_item.
     * @param valor_item New value of property valor_item.
     */
    public void setValor_item(double valor_item) {
        this.valor_item = valor_item;
    }
    
    /**
     * Getter for property valor_tasa.
     * @return Value of property valor_tasa.
     */
    public java.lang.Double getValor_tasa() {
        return valor_tasa;
    }
    
    /**
     * Setter for property valor_tasa.
     * @param valor_tasa New value of property valor_tasa.
     */
    public void setValor_tasa(java.lang.Double valor_tasa) {
        this.valor_tasa = valor_tasa;
    }
    
    /**
     * Getter for property valor_unitario.
     * @return Value of property valor_unitario.
     */
    public double getValor_unitario() {
        return valor_unitario;
    }
    
    /**
     * Setter for property valor_unitario.
     * @param valor_unitario New value of property valor_unitario.
     */
    public void setValor_unitario(double valor_unitario) {
        this.valor_unitario = valor_unitario;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
   
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property extraflete.
     * @return Value of property extraflete.
     */
    public boolean isExtraflete() {
        return extraflete;
    }
    
    /**
     * Setter for property extraflete.
     * @param extraflete New value of property extraflete.
     */
    public void setExtraflete(boolean extraflete) {
        this.extraflete = extraflete;
    }
    
}
