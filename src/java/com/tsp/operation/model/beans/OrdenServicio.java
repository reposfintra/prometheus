/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class OrdenServicio {


  private String reg_status;
  private String dstrct;
  private String tipo_operacion;
  private String numero_operacion;
  private String proveedor;
  private String cmc;
  private String numero_transferencia;
  private String nombre_propietario;
  private String cuenta_banco;
  private String banco;
  private String sucursal;
  private double vlr_extracto;
  private double porcentaje;
  private double vlr_descuento;
  private double vlr_neto;
  private double vlr_combancaria;
  private double vlr_consignacion;
  private double vlr_consignacion_calculada;
  private double vlr_ajuste;
  private double vlr_ext_detalle_calculado;
  private double vlr_ext_detalle_registrado;
  private double vlr_ext_detalle_diferencia;
  private double vlr_diferencia_ext_ant;
  private String last_update;
  private String user_update;
  private String creation_date;
  private String creation_user;
  private int grupo_transaccion_os;
  private String periodo_os ;
  private String fecha_contabilizacion_os;
  private String usuario_contabilizacion_os;
  private String cuenta_contable_banco;
  private String cuenta_os;
  private String dbcr_os;
  private String sigla_comprobante_os;
  private String egreso;
  private String fecha_egreso;
  private String fecha_transferencia;
  private String factura_cxp;



    /** Creates a new instance of OrdenServicio */
    public OrdenServicio() {
    }

    public static OrdenServicio load(java.sql.ResultSet rs)throws java.sql.SQLException{

      OrdenServicio ordenServicio = new OrdenServicio();

      ordenServicio.setReg_status(rs.getString( "reg_status")) ;
      ordenServicio.setDstrct(rs.getString( "dstrct")) ;
      ordenServicio.setTipo_operacion(rs.getString( "tipo_operacion")) ;
      ordenServicio.setNumero_operacion(rs.getString( "numero_operacion")) ;
      ordenServicio.setProveedor(rs.getString( "proveedor")) ;
      ordenServicio.setCmc(rs.getString( "cmc")) ;
      ordenServicio.setNumero_transferencia(rs.getString( "numero_transferencia")) ;
      ordenServicio.setNombre_propietario(rs.getString( "nombre_propietario")) ;
      ordenServicio.setCuenta_banco(rs.getString( "cuenta_banco")) ;
      ordenServicio.setBanco(rs.getString( "banco")) ;
      ordenServicio.setSucursal(rs.getString( "sucursal")) ;
      ordenServicio.setVlr_extracto(rs.getDouble( "vlr_extracto")) ;
      ordenServicio.setPorcentaje(rs.getDouble( "porcentaje")) ;
      ordenServicio.setVlr_descuento(rs.getDouble( "vlr_descuento")) ;
      ordenServicio.setVlr_neto(rs.getDouble( "vlr_neto")) ;
      ordenServicio.setVlr_combancaria(rs.getDouble( "vlr_combancaria")) ;
      ordenServicio.setVlr_consignacion(rs.getDouble( "vlr_consignacion")) ;
      ordenServicio.setVlr_consignacion_calculada(rs.getDouble( "vlr_consignacion_calculada")) ;
      ordenServicio.setVlr_ajuste(rs.getDouble( "vlr_ajuste")) ;
      ordenServicio.setVlr_ext_detalle_calculado(rs.getDouble( "vlr_ext_detalle_calculado")) ;
      ordenServicio.setVlr_ext_detalle_registrado(rs.getDouble( "vlr_ext_detalle_registrado")) ;
      ordenServicio.setVlr_ext_detalle_diferencia(rs.getDouble( "vlr_ext_detalle_diferencia")) ;
      ordenServicio.setVlr_diferencia_ext_ant(rs.getDouble( "vlr_diferencia_ext_ant")) ;
      ordenServicio.setLast_update(rs.getString( "last_update")) ;
      ordenServicio.setUser_update(rs.getString( "user_update")) ;
      ordenServicio.setCreation_date(rs.getString( "creation_date")) ;
      ordenServicio.setCreation_user(rs.getString( "creation_user")) ;
      ordenServicio.setGrupo_transaccion_os(rs.getInt( "grupo_transaccion_os")) ;
      ordenServicio.setPeriodo_os (rs.getString( "periodo_os")) ;
      ordenServicio.setFecha_contabilizacion_os(rs.getString( "fecha_contabilizacion_os")) ;
      ordenServicio.setUsuario_contabilizacion_os(rs.getString( "usuario_contabilizacion_os")) ;
      ordenServicio.setCuenta_contable_banco(rs.getString( "cuenta_contable_banco")) ;
      ordenServicio.setCuenta_os(rs.getString( "cuenta_os")) ;
      ordenServicio.setDbcr_os(rs.getString( "dbcr_os")) ;
      ordenServicio.setSigla_comprobante_os(rs.getString( "sigla_comprobante_os")) ;
      ordenServicio.setEgreso(rs.getString( "egreso")) ;
      ordenServicio.setFecha_egreso(rs.getString( "fecha_egreso")) ;
      ordenServicio.setFecha_transferencia(rs.getString( "fecha_transferencia"));
      ordenServicio.setFactura_cxp(rs.getString("factura_cxp"));

      
      return ordenServicio;
    }





    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_operacion
     */
    public String getTipo_operacion() {
        return tipo_operacion;
    }

    /**
     * @param tipo_operacion the tipo_operacion to set
     */
    public void setTipo_operacion(String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    /**
     * @return the numero_operacion
     */
    public String getNumero_operacion() {
        return numero_operacion;
    }

    /**
     * @param numero_operacion the numero_operacion to set
     */
    public void setNumero_operacion(String numero_operacion) {
        this.numero_operacion = numero_operacion;
    }

    /**
     * @return the proveedor
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the cmc
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc the cmc to set
     */
    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    /**
     * @return the numero_transferencia
     */
    public String getNumero_transferencia() {
        return numero_transferencia;
    }

    /**
     * @param numero_transferencia the numero_transferencia to set
     */
    public void setNumero_transferencia(String numero_transferencia) {
        this.numero_transferencia = numero_transferencia;
    }

    /**
     * @return the nombre_propietario
     */
    public String getNombre_propietario() {
        return nombre_propietario;
    }

    /**
     * @param nombre_propietario the nombre_propietario to set
     */
    public void setNombre_propietario(String nombre_propietario) {
        this.nombre_propietario = nombre_propietario;
    }

    /**
     * @return the cuenta_banco
     */
    public String getCuenta_banco() {
        return cuenta_banco;
    }

    /**
     * @param cuenta_banco the cuenta_banco to set
     */
    public void setCuenta_banco(String cuenta_banco) {
        this.cuenta_banco = cuenta_banco;
    }

    /**
     * @return the banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * @return the sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the vlr_extracto
     */
    public double getVlr_extracto() {
        return vlr_extracto;
    }

    /**
     * @param vlr_extracto the vlr_extracto to set
     */
    public void setVlr_extracto(double vlr_extracto) {
        this.vlr_extracto = vlr_extracto;
    }

    /**
     * @return the porcentaje
     */
    public double getPorcentaje() {
        return porcentaje;
    }

    /**
     * @param porcentaje the porcentaje to set
     */
    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

    /**
     * @return the vlr_descuento
     */
    public double getVlr_descuento() {
        return vlr_descuento;
    }

    /**
     * @param vlr_descuento the vlr_descuento to set
     */
    public void setVlr_descuento(double vlr_descuento) {
        this.vlr_descuento = vlr_descuento;
    }

    /**
     * @return the vlr_neto
     */
    public double getVlr_neto() {
        return vlr_neto;
    }

    /**
     * @param vlr_neto the vlr_neto to set
     */
    public void setVlr_neto(double vlr_neto) {
        this.vlr_neto = vlr_neto;
    }

    /**
     * @return the vlr_combancaria
     */
    public double getVlr_combancaria() {
        return vlr_combancaria;
    }

    /**
     * @param vlr_combancaria the vlr_combancaria to set
     */
    public void setVlr_combancaria(double vlr_combancaria) {
        this.vlr_combancaria = vlr_combancaria;
    }

    /**
     * @return the vlr_consignacion
     */
    public double getVlr_consignacion() {
        return vlr_consignacion;
    }

    /**
     * @param vlr_consignacion the vlr_consignacion to set
     */
    public void setVlr_consignacion(double vlr_consignacion) {
        this.vlr_consignacion = vlr_consignacion;
    }

    /**
     * @return the vlr_consignacion_calculada
     */
    public double getVlr_consignacion_calculada() {
        return vlr_consignacion_calculada;
    }

    /**
     * @param vlr_consignacion_calculada the vlr_consignacion_calculada to set
     */
    public void setVlr_consignacion_calculada(double vlr_consignacion_calculada) {
        this.vlr_consignacion_calculada = vlr_consignacion_calculada;
    }

    /**
     * @return the vlr_ajuste
     */
    public double getVlr_ajuste() {
        return vlr_ajuste;
    }

    /**
     * @param vlr_ajuste the vlr_ajuste to set
     */
    public void setVlr_ajuste(double vlr_ajuste) {
        this.vlr_ajuste = vlr_ajuste;
    }

    /**
     * @return the vlr_ext_detalle_calculado
     */
    public double getVlr_ext_detalle_calculado() {
        return vlr_ext_detalle_calculado;
    }

    /**
     * @param vlr_ext_detalle_calculado the vlr_ext_detalle_calculado to set
     */
    public void setVlr_ext_detalle_calculado(double vlr_ext_detalle_calculado) {
        this.vlr_ext_detalle_calculado = vlr_ext_detalle_calculado;
    }

    /**
     * @return the vlr_ext_detalle_registrado
     */
    public double getVlr_ext_detalle_registrado() {
        return vlr_ext_detalle_registrado;
    }

    /**
     * @param vlr_ext_detalle_registrado the vlr_ext_detalle_registrado to set
     */
    public void setVlr_ext_detalle_registrado(double vlr_ext_detalle_registrado) {
        this.vlr_ext_detalle_registrado = vlr_ext_detalle_registrado;
    }

    /**
     * @return the vlr_ext_detalle_diferencia
     */
    public double getVlr_ext_detalle_diferencia() {
        return vlr_ext_detalle_diferencia;
    }

    /**
     * @param vlr_ext_detalle_diferencia the vlr_ext_detalle_diferencia to set
     */
    public void setVlr_ext_detalle_diferencia(double vlr_ext_detalle_diferencia) {
        this.vlr_ext_detalle_diferencia = vlr_ext_detalle_diferencia;
    }

    /**
     * @return the vlr_diferencia_ext_ant
     */
    public double getVlr_diferencia_ext_ant() {
        return vlr_diferencia_ext_ant;
    }

    /**
     * @param vlr_diferencia_ext_ant the vlr_diferencia_ext_ant to set
     */
    public void setVlr_diferencia_ext_ant(double vlr_diferencia_ext_ant) {
        this.vlr_diferencia_ext_ant = vlr_diferencia_ext_ant;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the grupo_transaccion_os
     */
    public int getGrupo_transaccion_os() {
        return grupo_transaccion_os;
    }

    /**
     * @param grupo_transaccion_os the grupo_transaccion_os to set
     */
    public void setGrupo_transaccion_os(int grupo_transaccion_os) {
        this.grupo_transaccion_os = grupo_transaccion_os;
    }

    /**
     * @return the periodo_os
     */
    public String getPeriodo_os() {
        return periodo_os;
    }

    /**
     * @param periodo_os the periodo_os to set
     */
    public void setPeriodo_os(String periodo_os) {
        this.periodo_os = periodo_os;
    }

    /**
     * @return the fecha_contabilizacion_os
     */
    public String getFecha_contabilizacion_os() {
        return fecha_contabilizacion_os;
    }

    /**
     * @param fecha_contabilizacion_os the fecha_contabilizacion_os to set
     */
    public void setFecha_contabilizacion_os(String fecha_contabilizacion_os) {
        this.fecha_contabilizacion_os = fecha_contabilizacion_os;
    }

    /**
     * @return the usuario_contabilizacion_os
     */
    public String getUsuario_contabilizacion_os() {
        return usuario_contabilizacion_os;
    }

    /**
     * @param usuario_contabilizacion_os the usuario_contabilizacion_os to set
     */
    public void setUsuario_contabilizacion_os(String usuario_contabilizacion_os) {
        this.usuario_contabilizacion_os = usuario_contabilizacion_os;
    }

    /**
     * @return the cuenta_contable_banco
     */
    public String getCuenta_contable_banco() {
        return cuenta_contable_banco;
    }

    /**
     * @param cuenta_contable_banco the cuenta_contable_banco to set
     */
    public void setCuenta_contable_banco(String cuenta_contable_banco) {
        this.cuenta_contable_banco = cuenta_contable_banco;
    }

    /**
     * @return the cuenta_os
     */
    public String getCuenta_os() {
        return cuenta_os;
    }

    /**
     * @param cuenta_os the cuenta_os to set
     */
    public void setCuenta_os(String cuenta_os) {
        this.cuenta_os = cuenta_os;
    }

    /**
     * @return the dbcr_os
     */
    public String getDbcr_os() {
        return dbcr_os;
    }

    /**
     * @param dbcr_os the dbcr_os to set
     */
    public void setDbcr_os(String dbcr_os) {
        this.dbcr_os = dbcr_os;
    }

    /**
     * @return the sigla_comprobante_os
     */
    public String getSigla_comprobante_os() {
        return sigla_comprobante_os;
    }

    /**
     * @param sigla_comprobante_os the sigla_comprobante_os to set
     */
    public void setSigla_comprobante_os(String sigla_comprobante_os) {
        this.sigla_comprobante_os = sigla_comprobante_os;
    }

    /**
     * @return the egreso
     */
    public String getEgreso() {
        return egreso;
    }

    /**
     * @param egreso the egreso to set
     */
    public void setEgreso(String egreso) {
        this.egreso = egreso;
    }

    /**
     * @return the fecha_egreso
     */
    public String getFecha_egreso() {
        return fecha_egreso;
    }

    /**
     * @param fecha_egreso the fecha_egreso to set
     */
    public void setFecha_egreso(String fecha_egreso) {
        this.fecha_egreso = fecha_egreso;
    }

    /**
     * @return the fecha_transferencia
     */
    public String getFecha_transferencia() {
        return fecha_transferencia;
    }

    /**
     * @param fecha_transferencia the fecha_transferencia to set
     */
    public void setFecha_transferencia(String fecha_transferencia) {
        this.fecha_transferencia = fecha_transferencia;
    }

    /**
     * @return the factura_cxp
     */
    public String getFactura_cxp() {
        return factura_cxp;
    }

    /**
     * @param factura_cxp the factura_cxp to set
     */
    public void setFactura_cxp(String factura_cxp) {
        this.factura_cxp = factura_cxp;
    }








}
