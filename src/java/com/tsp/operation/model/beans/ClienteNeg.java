/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author rhonalf
 */
public class ClienteNeg extends Cliente {

    private int dvenc;
    private String fecha_ult_gestion;
    private String fecha_prox_gestion;
    private String fecha_next_venc;
    private double saldo_vencido;

    public ClienteNeg(){
        this.dvenc = 0;
        this.fecha_prox_gestion="";
        this.fecha_ult_gestion="";
        this.saldo_vencido = 0;
    }

    //rhonalf 2010-10-04
    public int getDvenc() {
        return dvenc;
    }

    //rhonalf 2010-10-04
    public void setDvenc(int dvenc) {
        this.dvenc = dvenc;
    }

    //rhonalf 2010-10-04
    public String getFecha_prox_gestion() {
        return fecha_prox_gestion;
    }

    //rhonalf 2010-10-04
    public void setFecha_prox_gestion(String fecha_prox_gestion) {
        this.fecha_prox_gestion = fecha_prox_gestion;
    }

    //rhonalf 2010-10-04
    public String getFecha_ult_gestion() {
        return fecha_ult_gestion;
    }

    //rhonalf 2010-10-04
    public void setFecha_ult_gestion(String fecha_ult_gestion) {
        this.fecha_ult_gestion = fecha_ult_gestion;
    }

    public String getFecha_next_venc() {
        return fecha_next_venc;
    }

    public void setFecha_next_venc(String fecha_next_venc) {
        this.fecha_next_venc = fecha_next_venc;
    }

    public double getSaldo_vencido() {
        return saldo_vencido;
    }

    public void setSaldo_vencido(double saldo_vencido) {
        this.saldo_vencido = saldo_vencido;
    }
    
}
