package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * <br/>
 * 29/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Reclamo {

    private String tipoPadre;
    private int idPadre;
    private String tipoLeyenda;
    private Timestamp fechaCierre;
    private String estado;
    private String tipo;
    private Timestamp fecha;
    private Boolean ratificado;
    private String texto;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private String entidad;
    private String numeroCuenta;
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of numeroCuenta
     *
     * @return the value of numeroCuenta
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Set the value of numeroCuenta
     *
     * @param numeroCuenta new value of numeroCuenta
     */
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Get the value of entidad
     *
     * @return the value of entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Set the value of entidad
     *
     * @param entidad new value of entidad
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }


    /**
     * Get the value of texto
     *
     * @return the value of texto
     */
    public String getTexto() {
        return texto;
    }

    /**
     * Set the value of texto
     *
     * @param texto new value of texto
     */
    public void setTexto(String texto) {

        this.texto = texto;
    }
    
    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of ratificado
     *
     * @return the value of ratificado
     */
    public Boolean getRatificado() {
        return ratificado;
    }

    /**
     * Set the value of ratificado
     *
     * @param ratificado new value of ratificado
     */
    public void setRatificado(boolean ratificado) {
        this.ratificado = ratificado;
    }

    /**
     * Get the value of fecha
     *
     * @return the value of fecha
     */
    public Timestamp getFecha() {
        return fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param fecha new value of fecha
     */
    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    /**
     * Get the value of tipo
     *
     * @return the value of tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set the value of tipo
     *
     * @param tipo new value of tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Get the value of estado
     *
     * @return the value of estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Set the value of estado
     *
     * @param estado new value of estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Get the value of fechaCierre
     *
     * @return the value of fechaCierre
     */
    public Timestamp getFechaCierre() {
        return fechaCierre;
    }

    /**
     * Set the value of fechaCierre
     *
     * @param fechaCierre new value of fechaCierre
     */
    public void setFechaCierre(Timestamp fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    /**
     * Get the value of tipoLeyenda
     *
     * @return the value of tipoLeyenda
     */
    public String getTipoLeyenda() {
        return tipoLeyenda;
    }

    /**
     * Set the value of tipoLeyenda
     *
     * @param tipoLeyenda new value of tipoLeyenda
     */
    public void setTipoLeyenda(String tipoLeyenda) {
        this.tipoLeyenda = tipoLeyenda;
    }


    /**
     * Get the value of idPadre
     *
     * @return the value of idPadre
     */
    public int getIdPadre() {
        return idPadre;
    }

    /**
     * Set the value of idPadre
     *
     * @param idPadre new value of idPadre
     */
    public void setIdPadre(int idPadre) {
        this.idPadre = idPadre;
    }

    /**
     * Get the value of tipoPadre
     *
     * @return the value of tipoPadre
     */
    public String getTipoPadre() {
        return tipoPadre;
    }

    /**
     * Set the value of tipoPadre
     *
     * @param tipoPadre new value of tipoPadre
     */
    public void setTipoPadre(String tipoPadre) {
        this.tipoPadre = tipoPadre;
    }


}
