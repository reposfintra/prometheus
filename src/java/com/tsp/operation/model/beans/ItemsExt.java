    /***************************************
    * Nombre Clase ............. ItemsExt.java
    * Descripci�n  .. . . . . .  Item, tiene una lista para sus impuestos
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  21/12/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.beans;


import java.util.*;
import com.tsp.util.*;



public class ItemsExt extends  CXPItemDoc  {
    
    private List  impuestos;
    
    
    private double valPago;
    private double valPagoImp;
    
    private double valImpNeto;
    private double valImpNeto_me;
     
     
    public ItemsExt() {
    }
    
    
    
    
    // SET : 
    /* M�todos que guardan valores en el objeto
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
    
    public void setImpuesto       ( List list  ){ this.impuestos     = list;   }    
    public void setVlrImpNeto     ( double val ){ this.valImpNeto    = val;    }    
    public void setVlrImpNeto_me  ( double val ){ this.valImpNeto_me = val;    }    
    public void setVlrPago        ( double val ){ this.valPago       = val;    }
    public void setVlrPagoImp     ( double val ){ this.valPagoImp    = val;    }
    
    
    
    // GET :
    /**
     * M�todos que retornan  valores
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
    
    public List   getImpuesto       () { return  this.impuestos     ;    }
    public double getVlrImpNeto     () { return  this.valImpNeto    ;    }    
    public double getVlrImpNeto_me  () { return  this.valImpNeto_me ;    }  
    public double getVlrPago        () { return  this.valPago       ;    }
    public double getVlrPagoImp     () { return  this.valPagoImp    ;    }
   
    
    
    
}
