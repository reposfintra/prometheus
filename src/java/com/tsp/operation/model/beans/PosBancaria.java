/********************************************************************
 *      Nombre Clase.................   PosBancaria.java    
 *      Descripci�n..................   Bean del archivo posicion_bancaria
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   18.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author  Andres
 */
public class PosBancaria {
    
    
    private String agency_id;
    private String branch_code;
    private String bank_account_no;
    private String fecha;
    private String nom_agencia;
    private double saldo_inicial;
    private double saldo_anterior;
    private double anticipos;
    private double proveedores;
    private double nuevo_saldo;
    private double cupo;
    
    private String creation_user;
    private String base;
    private String reg_status;
    private String dstrct;
    
    /** Creates a new instance of PosBancaria */
    public PosBancaria() {
    }
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void Load(ResultSet rs) throws SQLException{
        this.fecha = rs.getString("fecha");
        this.anticipos = rs.getDouble("anticipos");
        this.proveedores = rs.getDouble("proveedores");
        this.cupo = rs.getDouble("cupo");
        this.nuevo_saldo = rs.getDouble("nuevo_saldo");
        this.saldo_anterior = rs.getDouble("saldo_anterior");
        this.saldo_inicial = rs.getDouble("saldo_inicial");
        this.agency_id = rs.getString("agency_id");
        this.bank_account_no = rs.getString("bank_account_no");
        this.base = rs.getString("base");
        this.branch_code = rs.getString("branch_code");
        this.creation_user = rs.getString("creation_user");
        this.dstrct = rs.getString("dstrct");
        this.reg_status = rs.getString("reg_status");
    }
    
    /**
     * Getter for property agency_id.
     * @return Value of property agency_id.
     */
    public java.lang.String getAgency_id() {
        return agency_id;
    }
    
    /**
     * Setter for property agency_id.
     * @param agency_id New value of property agency_id.
     */
    public void setAgency_id(java.lang.String agency_id) {
        this.agency_id = agency_id;
    }
    
    /**
     * Getter for property bank_account_no.
     * @return Value of property bank_account_no.
     */
    public java.lang.String getBank_account_no() {
        return bank_account_no;
    }
    
    /**
     * Setter for property bank_account_no.
     * @param bank_account_no New value of property bank_account_no.
     */
    public void setBank_account_no(java.lang.String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property branch_code.
     * @return Value of property branch_code.
     */
    public java.lang.String getBranch_code() {
        return branch_code;
    }
    
    /**
     * Setter for property branch_code.
     * @param branch_code New value of property branch_code.
     */
    public void setBranch_code(java.lang.String branch_code) {
        this.branch_code = branch_code;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }    
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property nom_agencia.
     * @return Value of property nom_agencia.
     */
    public java.lang.String getNom_agencia() {
        return nom_agencia;
    }
    
    /**
     * Setter for property nom_agencia.
     * @param nom_agencia New value of property nom_agencia.
     */
    public void setNom_agencia(java.lang.String nom_agencia) {
        this.nom_agencia = nom_agencia;
    }
    
    /**
     * Getter for property saldo_inicial.
     * @return Value of property saldo_inicial.
     */
    public double getSaldo_inicial() {
        return saldo_inicial;
    }
    
    /**
     * Setter for property saldo_inicial.
     * @param saldo_inicial New value of property saldo_inicial.
     */
    public void setSaldo_inicial(double saldo_inicial) {
        this.saldo_inicial = saldo_inicial;
    }
    
    /**
     * Getter for property saldo_anterior.
     * @return Value of property saldo_anterior.
     */
    public double getSaldo_anterior() {
        return saldo_anterior;
    }
    
    /**
     * Setter for property saldo_anterior.
     * @param saldo_anterior New value of property saldo_anterior.
     */
    public void setSaldo_anterior(double saldo_anterior) {
        this.saldo_anterior = saldo_anterior;
    }
    
    /**
     * Getter for property anticipos.
     * @return Value of property anticipos.
     */
    public double getAnticipos() {
        return anticipos;
    }
    
    /**
     * Setter for property anticipos.
     * @param anticipos New value of property anticipos.
     */
    public void setAnticipos(double anticipos) {
        this.anticipos = anticipos;
    }
    
    /**
     * Getter for property proveedores.
     * @return Value of property proveedores.
     */
    public double getProveedores() {
        return proveedores;
    }
    
    /**
     * Setter for property proveedores.
     * @param proveedores New value of property proveedores.
     */
    public void setProveedores(double proveedores) {
        this.proveedores = proveedores;
    }
    
    /**
     * Getter for property nuevo_saldo.
     * @return Value of property nuevo_saldo.
     */
    public double getNuevo_saldo() {
        return nuevo_saldo;
    }
    
    /**
     * Setter for property nuevo_saldo.
     * @param nuevo_saldo New value of property nuevo_saldo.
     */
    public void setNuevo_saldo(double nuevo_saldo) {
        this.nuevo_saldo = nuevo_saldo;
    }
    
    /**
     * Getter for property cupo.
     * @return Value of property cupo.
     */
    public double getCupo() {
        return cupo;
    }
    
    /**
     * Setter for property cupo.
     * @param cupo New value of property cupo.
     */
    public void setCupo(double cupo) {
        this.cupo = cupo;
    }
    
}
