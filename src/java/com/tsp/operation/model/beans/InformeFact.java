/********************************************************************
 *      Nombre Clase.................   InformeFacturacion.java    
 *      Descripci�n..................   Bean de la generaci�n del informe
 *                                      de facturaci�n.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   31.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.Util;
import java.sql.*;

public class InformeFact {
        private Integer sum_tonelaje;
        private Vector remisiones;
        private String std_job;
        
        
        /** Creates a new instance of InformeFact */
        public InformeFact() {
        }
        
        /**
         * Getter for property remisiones.
         * @return Value of property remisiones.
         */
        public java.util.Vector getRemisiones() {
                return remisiones;
        }
        
        /**
         * Setter for property remisiones.
         * @param remisiones New value of property remisiones.
         */
        public void setRemisiones(java.util.Vector remisiones) {
                this.remisiones = remisiones;
        }
        
        /**
         * Getter for property std_job.
         * @return Value of property std_job.
         */
        public java.lang.String getStd_job() {
                return std_job;
        }
        
        /**
         * Setter for property std_job.
         * @param std_job New value of property std_job.
         */
        public void setStd_job(java.lang.String std_job) {
                this.std_job = std_job;
        }
        
        /**
         * Getter for property sum_tonelaje.
         * @return Value of property sum_tonelaje.
         */
        public java.lang.Integer getSum_tonelaje() {
                return sum_tonelaje;
        }
        
        /**
         * Setter for property sum_tonelaje.
         * @param sum_tonelaje New value of property sum_tonelaje.
         */
        public void setSum_tonelaje(java.lang.Integer sum_tonelaje) {
                this.sum_tonelaje = sum_tonelaje;
        }
        
}
