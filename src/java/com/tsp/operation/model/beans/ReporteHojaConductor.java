/*
 * HojaReporte.java
 *
 * Created on 22 de noviembre de 2004, 10:50 AM
 */

package com.tsp.operation.model.beans;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author  AMENDEZ
 */
public class ReporteHojaConductor implements Serializable {
    
    private String planilla = "";
    private String cedula = "";
    private String conductor = "";
    private String ruta = ""; 
    private String placa_vehiculo = "";
    private String placa_unidad_carga = "";
    private String contenedores = "";
    private String precinto = "";
    private String usuario = "";
    private Collection detalleruta;
    private String comentario = "";
    private String foto = "";
    private String telCel = "";
    private String telFijo = "";
    private String avantel = "";
    //LINEAS GRATUITAS
    private String lineaG = "";
    private String pais = "";
    private String cia = "";
    
    
    /** Creates a new instance of HojaReporte */
    public ReporteHojaConductor() {
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getPlaca_vehiculo() {
        return placa_vehiculo;
    }

    public void setPlaca_vehiculo(String placa_vehiculo) {
        this.placa_vehiculo = placa_vehiculo;
    }

    public String getPlaca_unidad_carga() {
        return placa_unidad_carga;
    }

    public void setPlaca_unidad_carga(String placa_unidad_carga) {
        this.placa_unidad_carga = placa_unidad_carga;
    }

    public String getContenedores() {
        return contenedores;
    }

    public void setContenedores(String contenedores) {
        this.contenedores = contenedores;
    }

    public String getPrecinto() {
        return precinto;
    }

    public void setPrecinto(String precinto) {
        this.precinto = precinto;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property comentario.
     * @return Value of property comentario.
     */
    public java.lang.String getComentario() {
        return comentario;
    }
    
    /**
     * Setter for property comentario.
     * @param comentario New value of property comentario.
     */
    public void setComentario(java.lang.String comentario) {
        this.comentario = comentario;
    }
    
    /**
     * Getter for property detalleruta.
     * @return Value of property detalleruta.
     */
    public java.util.Collection getDetalleruta() {
        return detalleruta;
    }
    
    /**
     * Setter for property detalleruta.
     * @param detalleruta New value of property detalleruta.
     */
    public void setDetalleruta(java.util.Collection detalleruta) {
        this.detalleruta = detalleruta;
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula() {
        return cedula;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula(java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Getter for property foto.
     * @return Value of property foto.
     */
    public java.lang.String getFoto() {
        return foto;
    }
    
    /**
     * Setter for property foto.
     * @param foto New value of property foto.
     */
    public void setFoto(java.lang.String foto) {
        this.foto = foto;
    }
    
    /**
     * Getter for property telFijo.
     * @return Value of property telFijo.
     */
    public java.lang.String getTelFijo() {
        return telFijo;
    }
    
    /**
     * Setter for property telFijo.
     * @param telFijo New value of property telFijo.
     */
    public void setTelFijo(java.lang.String telFijo) {
        this.telFijo = telFijo;
    }
    
    /**
     * Getter for property telCel.
     * @return Value of property telCel.
     */
    public java.lang.String getTelCel() {
        return telCel;
    }
    
    /**
     * Setter for property telCel.
     * @param telCel New value of property telCel.
     */
    public void setTelCel(java.lang.String telCel) {
        this.telCel = telCel;
    }
    
    /**
     * Getter for property avantel.
     * @return Value of property avantel.
     */
    public java.lang.String getAvantel() {
        return avantel;
    }
    
    /**
     * Setter for property avantel.
     * @param avantel New value of property avantel.
     */
    public void setAvantel(java.lang.String avantel) {
        this.avantel = avantel;
    }
    
    /**
     * Getter for property pais.
     * @return Value of property pais.
     */
    public java.lang.String getPais() {
        return pais;
    }
    
    /**
     * Setter for property pais.
     * @param pais New value of property pais.
     */
    public void setPais(java.lang.String pais) {
        this.pais = pais;
    }
    
    /**
     * Getter for property lineaG.
     * @return Value of property lineaG.
     */
    public java.lang.String getLineaG() {
        return lineaG;
    }
    
    /**
     * Setter for property lineaG.
     * @param lineaG New value of property lineaG.
     */
    public void setLineaG(java.lang.String lineaG) {
        this.lineaG = lineaG;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
}
