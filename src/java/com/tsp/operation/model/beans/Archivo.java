package com.tsp.operation.model.beans;

import java.io.*;

public class Archivo extends File implements Serializable{
    private int      id;
    private long     fecha;
    private long     tama�o;
    
    public Archivo(String FileName) {
        super(FileName);
        id           = 0;
        fecha        = 0;
        tama�o       = 0;
    }

    public static Archivo Load (int id, File arc ){        
        Archivo datos = new Archivo( arc.getName());
        datos.tama�o  = arc.length();
        datos.fecha   = arc.lastModified();
        datos.id      = id; 
        return datos;
    }
    
    
    public int  get_Id   (){ return this.id;      }
    public long getFecha (){ return this.fecha;   }
    public long getTama�o(){ return this.tama�o;  }
    
   
}
