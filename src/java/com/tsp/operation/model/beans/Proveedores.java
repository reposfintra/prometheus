/*
 * proveedores.java
 *
 * Created on 6 de abril de 2005, 03:12 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
public class Proveedores {
    
    private String dstrct;
    private String nit;
    private String sucursal;
    private String nombre;
    private float cantacpm;
    private float valoracpm;
    private int cantpeaje;
    private float valorpeaje;
    private float totale;
    private String tipo_s;
    private String moneda;
    private float porcentaje;
    private String city_code;
    private String migracion;
    private String creation_user;
    private String codant;
    /** Creates a new instance of proveedores */
    public Proveedores() {
    }
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    public void setAnticipo(String codant){
        this.codant=codant;
    }
    public String getAnticipo(){
        return codant;
    }
    public String getTipo_s(){
        return tipo_s;
    }
    public String getMoneda(){
        return moneda;
    }
    public float getPorcentaje(){
        return porcentaje;
    }
    public String getCity_code(){
        return city_code;
    }
    public String getmigracion(){
        return migracion;
    }
    public void setTipo_s(String tipo_s){
        this.tipo_s = tipo_s;
    }
    public void setMoneda(String moneda){
        this.moneda = moneda;
    }
    public void setPorcentaje(float porcentaje){
        this.porcentaje = porcentaje;
    }
    public void setCity_code(String city_code){
        this.city_code = city_code;
    }
    public void setmigracion(String migracion){
        this.migracion = migracion;
    }
    
    public void setNit(String nit){
        this.nit = nit;
    }
    public void setSucursal(String suc){
        this.sucursal = suc;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    public void setCantacpm(float cantacpm){
        this.cantacpm = cantacpm;
    }
    public void setValoracpm(float vacpm){
        this.valoracpm = vacpm;
    }
    public void setCantPeaje(int cant){
        cantpeaje = cant;
    }
    public void setValorPeaje(float valorp){
        valorpeaje = valorp;
    }
    public void setTotale(float totale){
        this.totale=totale;
    }
    
    public int getCantPeaje(){
        return this.cantpeaje;
    }
    public float getValorPeaje(){
        return this.valorpeaje;
    }
    public float getTotale(){
        return this.totale;
    }
    public String getNit(){
        return nit;
    }
    public String getNombre(){
        return nombre;
    }
    public String getSucursal(){
        return sucursal;
    }
    public float getCantacpm(){
        return cantacpm;
    }
    public float getValoracpm(){
        return valoracpm;
    }
     public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
}
