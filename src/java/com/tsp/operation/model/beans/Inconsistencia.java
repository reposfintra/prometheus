/*
 * Incosistencia.java
 *
 * Created on 24 de julio de 2005, 09:48 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Jm
 */
public class Inconsistencia {
    private int Tipo;
    private String Descripcion;
    private String numpla;
    private String Registro;
    private String Columna;
    private String Archivo;
    private String placa;
    
    /** Creates a new instance of Incosistencia */
    public Inconsistencia() {
    }
    
    
    
    public void load( int i, String d, String np, String reg, String c, String arc, String pla ){
        this.Tipo = i;
        this.Descripcion = d;
        this.numpla = np;
        this.Registro = reg;
        this.Columna = c;
        this.Archivo = arc;
        this.placa = pla;
    }
    
    /**
     * Getter for property Columna.
     * @return Value of property Columna.
     */
    public java.lang.String getColumna() {
        return Columna;
    }
    
    /**
     * Setter for property Columna.
     * @param Columna New value of property Columna.
     */
    public void setColumna(java.lang.String Columna) {
        this.Columna = Columna;
    }
    
    /**
     * Getter for property Descripcion.
     * @return Value of property Descripcion.
     */
    public java.lang.String getDescripcion() {
        return Descripcion;
    }
    
    /**
     * Setter for property Descripcion.
     * @param Descripcion New value of property Descripcion.
     */
    public void setDescripcion(java.lang.String Descripcion) {
        this.Descripcion = Descripcion;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property Registro.
     * @return Value of property Registro.
     */
    public java.lang.String getRegistro() {
        return Registro;
    }
    
    /**
     * Setter for property Registro.
     * @param Registro New value of property Registro.
     */
    public void setRegistro(java.lang.String Registro) {
        this.Registro = Registro;
    }
    
    /**
     * Getter for property Tipo.
     * @return Value of property Tipo.
     */
    public int getTipo() {
        return Tipo;
    }
    
    /**
     * Setter for property Tipo.
     * @param Tipo New value of property Tipo.
     */
    public void setTipo(int Tipo) {
        this.Tipo = Tipo;
    }
    
    /**
     * Getter for property Archivo.
     * @return Value of property Archivo.
     */
    public java.lang.String getArchivo() {
        return Archivo;
    }    
    
    /**
     * Setter for property Archivo.
     * @param Archivo New value of property Archivo.
     */
    public void setArchivo(java.lang.String Archivo) {
        this.Archivo = Archivo;
    }    
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
}
