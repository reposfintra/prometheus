/* * Accord.java  * Created on 1 de junio de 2009, 8:08 */
package com.tsp.operation.model.beans;
/** * @author  Fintra */
public class OfertaElca {

    private String id_solicitud, id_cliente, id_oferta, num_os, descripcion, last_update,
       creation_date, creation_user, user_update, reg_status, nic, fecha_entrega_oferta,
       creacion_fecha_entrega_oferta, usuario_entrega_oferta, tipo_solicitud, consecutivo_oferta,
       nombre_solicitud, aviso;

    private String consideraciones, otras_consideraciones;  //pbassil

    private String oficial;                               //pbassil

    public OfertaElca(){
    }

    public String getOficial() {
        return oficial;
    }

    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

    public String getAviso() {
        return aviso;
    }

    public void setAviso(String aviso) {
        this.aviso = aviso;
    }

    public String getConsecutivo_oferta() {
        return consecutivo_oferta;
    }

    public void setConsecutivo_oferta(String consecutivo_oferta) {
        this.consecutivo_oferta = consecutivo_oferta;
    }

    public String getConsideraciones() {
        return consideraciones;
    }

    public void setConsideraciones(String consideraciones) {
        this.consideraciones = consideraciones;
    }

    public String getCreacion_fecha_entrega_oferta() {
        return creacion_fecha_entrega_oferta;
    }

    public void setCreacion_fecha_entrega_oferta(String creacion_fecha_entrega_oferta) {
        this.creacion_fecha_entrega_oferta = creacion_fecha_entrega_oferta;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha_entrega_oferta() {
        return fecha_entrega_oferta;
    }

    public void setFecha_entrega_oferta(String fecha_entrega_oferta) {
        this.fecha_entrega_oferta = fecha_entrega_oferta;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getId_oferta() {
        return id_oferta;
    }

    public void setId_oferta(String id_oferta) {
        this.id_oferta = id_oferta;
    }

    public String getId_solicitud() {
        return id_solicitud;
    }

    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getNombre_solicitud() {
        return nombre_solicitud;
    }

    public void setNombre_solicitud(String nombre_solicitud) {
        this.nombre_solicitud = nombre_solicitud;
    }

    public String getNum_os() {
        return num_os;
    }

    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    public String getOtras_consideraciones() {
        return otras_consideraciones;
    }

    public void setOtras_consideraciones(String otras_consideraciones) {
        this.otras_consideraciones = otras_consideraciones;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getTipo_solicitud() {
        return tipo_solicitud;
    }

    public void setTipo_solicitud(String tipo_solicitud) {
        this.tipo_solicitud = tipo_solicitud;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public String getUsuario_entrega_oferta() {
        return usuario_entrega_oferta;
    }

    public void setUsuario_entrega_oferta(String usuario_entrega_oferta) {
        this.usuario_entrega_oferta = usuario_entrega_oferta;
    }
}