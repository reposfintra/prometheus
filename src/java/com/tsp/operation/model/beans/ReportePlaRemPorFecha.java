/*
 * ReportePlaRemPorFecha.java
 *
 * Created on 3 de octubre de 2005, 03:37 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;

/**
 *
 * @author  LEONARDO PARODY
 */
public class ReportePlaRemPorFecha implements Serializable {
    private String NumPla;
    private java.util.Date FechaPla;
    private String AgcPlanilla;
    private String AOriPlanilla;
    private String ADesPlanilla;
    private java.util.Date FechadspPla;
    private String FechaCumPla;
    private double CantOrPlanilla;
    private double CantCumpPla;
    private double DifOrCumpPla;
    private String AgenCumplPla;
    private String UsuCumplPla;
    private String NumDisPla;
    private String FecEntPla;
    private double DifFecEntCumPla;
    
    private String NumRem;
    private java.util.Date FecRem;
    private java.util.Date FechadspRem;
    private java.util.Date FechaCumRem;
    private double CantOrRemesa;
    private double CantCumpRem;
    private String AgcRem;
    private double DifOrCumpRem;
    private String AgenCumplRem;
    private String UsuCumplRem;
    private String AgcOriRem;
    private String AgcDesRem;
    private double DocIntRem;
    private double DocIntPendRem;
    static double cantplanilla1;
    static double cantidadpla1;
    private String Cliente;
   
    /** Creates a new instance of ReportePlaRemPorFecha */
    public static ReportePlaRemPorFecha load(ResultSet rs)throws SQLException {
        ReportePlaRemPorFecha reportePlaRemPorFecha = new ReportePlaRemPorFecha();  
        reportePlaRemPorFecha.setNumpla( rs.getString("numpla") );
        reportePlaRemPorFecha.setFecdsp( rs.getTimestamp("fecdsp") );
        reportePlaRemPorFecha.setAgcOriPla(rs.getString("oriplanilla") );
        reportePlaRemPorFecha.setAgcPla(rs.getString("agcplanilla") );
        String cantplanilla = rs.getString("cantplanilla");
        String cantidadpla = rs.getString("cantidadpla");
        if (cantplanilla.equals(null)){
            cantplanilla1=0;
        }else{
            cantplanilla1=Double.parseDouble(cantplanilla);
        }
        ////System.out.println("yy");
        if (cantidadpla==null){
            cantidadpla1 = 0;
        }else{
            cantidadpla1=Double.parseDouble(cantidadpla);
        }
        reportePlaRemPorFecha.setAgcDesPla(rs.getString("desplanilla") );
        reportePlaRemPorFecha.setFechaCumPla(rs.getString("creation_datepla") );
        reportePlaRemPorFecha.setCantOrPlanilla(cantplanilla1);
        reportePlaRemPorFecha.setCantCumpPla(cantidadpla1);
        reportePlaRemPorFecha.setDifOrCumpPla(cantplanilla1, cantidadpla1);
        reportePlaRemPorFecha.setAgenCumplPla(rs.getString("agcumpla"));
        reportePlaRemPorFecha.setUsuCumplPla(rs.getString("creation_userpla"));
        reportePlaRemPorFecha.setCliente(rs.getString("nomcli"));
                    
        double cantremesa = rs.getDouble("cantremesa");
        double cantidadrem = rs.getDouble("cantidadrem");
        reportePlaRemPorFecha.setNumRem( rs.getString("numrem") );
        reportePlaRemPorFecha.setFecRem( rs.getTimestamp("fecrem") );
        reportePlaRemPorFecha.setAgcOriRem(rs.getString("oriremesa") );
        reportePlaRemPorFecha.setAgcRem(rs.getString("agcremesa") );
        reportePlaRemPorFecha.setAgcDesRem(rs.getString("desremesa") );
        reportePlaRemPorFecha.setFechaCumRem(rs.getTimestamp("creation_daterem") );
        reportePlaRemPorFecha.setCantOrRemesa(cantremesa);
        reportePlaRemPorFecha.setCantCumpRem(cantidadrem);
        reportePlaRemPorFecha.setDifOrCumpRem(cantremesa, cantidadrem);
        reportePlaRemPorFecha.setAgenCumplRem(rs.getString("agcumrem"));
        reportePlaRemPorFecha.setUsuCumplRem(rs.getString("creation_userrem"));
         
        
        return reportePlaRemPorFecha;
    }
    
    
    //=====================================================
    //              Metodos de acceso 
    //=====================================================
    
     public void setNumDisPla(String numDisPla){
    
        this.NumDisPla = numDisPla;
       
    }
    public void setNumpla(String numpla){
    
        this.NumPla = numpla;
       
    }
    public void setFecdsp(java.util.Date fechadspPla){
        
        this.FechadspPla = fechadspPla;
        
    }
    public void setAgcOriPla(String aOriPlanilla){
        this.AOriPlanilla = aOriPlanilla;
    }
    public void setAgcPla(String agcPlanilla){
        
        this.AgcPlanilla = agcPlanilla;
        
    }
    public void setAgcDesPla(String aDesPlanilla){
        
        this.ADesPlanilla = aDesPlanilla;
        
    }
    public void setFechadspPla(java.util.Date fechadspPla){
        
        this.FechadspPla = fechadspPla;
        
    }
    public void setFechaCumPla(String fechaCumPla){
        
        this.FechaCumPla = fechaCumPla;
        
    }
    public void setCantOrPlanilla(double cantOrPlanilla){
        
        this.CantOrPlanilla = cantOrPlanilla;
        
    }
    public void setCantCumpPla(double cantCumpPla){
        
        this.CantCumpPla = cantCumpPla;
        
    }
    public void setDifOrCumpPla(double cantOrPlanilla, double cantCumpPla){
        ////System.out.println("-----------------------------------------"+cantOrPlanilla+" "+cantCumpPla);
        double total = cantOrPlanilla - cantCumpPla;
        ////System.out.println("-----------------------------------------"+total);
        this.DifOrCumpPla = total;
    }
    public void setUsuCumplPla(String usuCumplPla){
        
        this.UsuCumplPla = usuCumplPla;
        
    }
    public void setAgenCumplPla(String agenCumplPla){
        
        this.AgenCumplPla = agenCumplPla;
        
    }
    public void setCliente(String cliente){
        
        this.Cliente = cliente;
        
    }
    public void setFecEntPla(String fecEntPla){
        
        this.FecEntPla = fecEntPla;
        
    }
    
    public void setDifFecEntCumPla(double difFecEntCumPla){
        
        this.DifFecEntCumPla = difFecEntCumPla;
        
    }    
    public void setNumRem(String numrem){
    
        this.NumRem = numrem;
       
    }
    public void setFecRem(java.util.Date fechadspRem){
        
        this.FecRem = fechadspRem;
        
    }
    public void setAgcOriRem(String aOriRemesa){
        this.AgcOriRem = aOriRemesa;
    }
    public void setAgcRem(String agcRemesa){
        
        this.AgcRem = agcRemesa;
        
    }
    public void setAgcDesRem(String aDesRemesa){
        
        this.AgcDesRem = aDesRemesa;
        
    }
    public void setFechadspRem(java.util.Date fechadspRem){
        
        this.FechadspRem = fechadspRem;
        
    }
    public void setFechaCumRem(java.util.Date fechaCumRem){
        
        this.FechaCumRem = fechaCumRem;
        
    }
    public void setCantOrRemesa(double cantOrRemesa){
        
        this.CantOrRemesa = cantOrRemesa;
        
    }
    public void setCantCumpRem(double cantCumpRem){
        
        this.CantCumpRem = cantCumpRem;
        
    }
    public void setDifOrCumpRem(double cantOrRemesa, double cantCumpRem){
        
        double total = cantOrRemesa - cantCumpRem;
        this.DifOrCumpRem = total;
        
    }
    public void setUsuCumplRem(String usuCumplRem){
        
        this.UsuCumplRem = usuCumplRem;
        
    }
    public void setAgenCumplRem(String agenCumplRem){
        
        this.AgenCumplRem = agenCumplRem;
        
    }
    public void setDocIntRem(double docIntRem){
        
        this.DocIntRem = docIntRem;
        
    }
    public void setDocIntPendRem(double docIntPendRem){
        
        this.DocIntPendRem = docIntPendRem;
        
    }
    /*--------------------------------------------------------------------------------*/
    
    
    public String getNumDisPla(){
    
        return NumDisPla;
       
    }
    public String getNumpla(){
    
        return NumPla;
       
    }
    public java.util.Date getFecdsp(){
        
        return FechadspPla;
        
    }
    public String getAgcOriPla(){
        return AOriPlanilla;
    }
    public String getAgcPla(){
        
        return AgcPlanilla;
        
    }
    public String getAgcDesPla(){
        
        return ADesPlanilla;
        
    }
    public java.util.Date getFechadspPla(){
        
        return FechadspPla;
        
    }
    
    public String getFechaCumPla(){
        
        return FechaCumPla;
        
    }
    public double getCantOrPlanilla(){
        
        return CantOrPlanilla;
        
    }
    public double getCantCumpPla(){
        
        return CantCumpPla;
        
    }
    public double getDifOrCumpPla(){
        
        return DifOrCumpPla;
        
    }
    public String getUsuCumplPla(){
        
        return UsuCumplPla;
        
    }
    public String getAgenCumplPla(){
        
        return AgenCumplPla;
        
    }
    public String getCliente(){
        
        return Cliente;
        
    }
    public String getFecEntPla(){
        
        return FecEntPla;
        
    }
    
    public double getDifFecEntCumPla(){
        
        return DifFecEntCumPla;
        
    }
    
    public String getNumRem(){
    
        return NumRem;
       
    }
    public java.util.Date getFecRem(){
        
        return FecRem;
        
    }
    public String getAgcOriRem(){
        return AgcOriRem;
    }
    public String getAgcRem(){
        
        return AgcRem;
        
    }
    public String getAgcDesRem(){
        
        return AgcDesRem;
        
    }
    public java.util.Date getFechadspRem(){
        
        return FechadspRem;
        
    }
    public java.util.Date getFechaCumRem(){
        
        return FechaCumRem;
        
    }
    public double getCantOrRemesa(){
        
        return CantOrRemesa;
        
    }
    public double getCantCumpRem(){
        
        return CantCumpRem;
        
    }
    public double getDifOrCumpRem(){
        
        return DifOrCumpRem;
        
    }
    public String getUsuCumplRem(){
        
        return UsuCumplRem;
        
    }
    public String getAgenCumplRem(){
        
        return AgenCumplRem;
        
    }
    public double getDocIntRem(){
    
        return DocIntRem;
       
    }
    public double getDocIntPendRem(){
    
        return DocIntPendRem;
       
    }
    
}