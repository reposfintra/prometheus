/*
 * Ingreso_Trafico.java
 *
 * Created on 22 de agosto de 2005, 01:41 PM
 */

package com.tsp.operation.model.beans;

import java.sql.*;
/**
 *
 * @author  kreales
 */
public class Ingreso_Trafico {
    
    private String dstrct;
    private String planilla;
    private String placa;
    private String cedcon;
    private String nomcond;
    private String cedprop;
    private String nomprop;
    private String origen;
    private String nomorigen;
    private String destino;
    private String nomdestino;
    private String zona;
    private String nomzona;
    private String escolta;
    private String caravana;
    private String fecha_despacho;
    private String pto_control_ultreporte;
    private String nompto_control_ultreporte;
    private String fecha_ult_reporte;
    private String pto_control_proxreporte;
    private String nompto_control_proxreporte;
    private String fecha_prox_reporte;
    private String ult_observacion;
    private String creation_user="";
    private String base;
    private String fecha_salida;
    /*Lisset 05/05/2006*/
    private String last_update;
     
    //Osvaldo 02/08/2006
    private String reg_status;
    private String user_update;
    private String creation_date;
    private int demora;
    private String via;
    private String cel_cond;
    private String cliente;
    private String tipo_despacho;
    private String tipo_reporte;
    private String nom_reporte;
    private String nom_ciudad;
    private String cod_ciudad;
    private String cedcon_anterior;
    private String nomcond_anterior;
    private String cedprop_anterior;
    private String nomprop_anterior;
    private String placa_anterior;
    private String via_anterior;
    private String cadena;
    
    /*Lisset 05/05/2006*/
    private String observacion;
    private String tipo_procedencia;
    private String ubicacion_procedencia;
    private String fechareporte;
    private String fechareporte_planeado;
    private String causa;
    private String clasificacion;
    
    //
    private String tipo_eliminacion;
    
    /** Creates a new instance of Ingreso_Trafico */
    public Ingreso_Trafico() {
    }
    
    public static Ingreso_Trafico load(ResultSet rs) throws SQLException{
        Ingreso_Trafico i = new Ingreso_Trafico();
        i.setReg_status( rs.getString( "reg_status" ) );
        i.setDstrct( rs.getString("dstrct") );
        i.setPlanilla( rs.getString("planilla") );
        i.setPlaca( rs.getString("placa") );
        i.setCedcon( rs.getString("cedcon") );
        i.setNomcond( rs.getString("nomcond") );
        i.setCedprop( rs.getString("cedprop") );
        i.setNomprop( rs.getString("nomprop") );
        i.setOrigen( rs.getString("origen") );
        i.setNomorigen( rs.getString("nomorigen") );
        i.setDestino( rs.getString("destino") );
        i.setNomdestino( rs.getString("nomdestino") );
        i.setZona( rs.getString("zona") );
        i.setNomzona( rs.getString("nomzona") );
        i.setEscolta( rs.getString("escolta") );
        i.setCaravana( rs.getString("caravana") );
        i.setFecha_despacho( rs.getString("fecha_despacho") );
        i.setPto_control_ultreporte( rs.getString("pto_control_ultreporte") );
        i.setNompto_control_ultreporte( rs.getString("nompto_control_ultreporte") );
        i.setFecha_ult_reporte( rs.getString("fecha_ult_reporte") );
        i.setPto_control_proxreporte( rs.getString("pto_control_proxreporte") );
        i.setNompto_control_proxreporte( rs.getString("nompto_control_proxreporte") );
        i.setFecha_prox_reporte( rs.getString("fecha_prox_reporte") );
        i.setUlt_observacion( rs.getString("ult_observacion") );
        i.setLast_update( rs.getString("last_update") );
        i.setUser_update( rs.getString("user_update") );
        i.setCreation_date( rs.getString("creation_date") );
        i.setCreation_user( rs.getString("creation_user") );
        i.setBase(rs.getString("base") );
        i.setDemora( rs.getInt("demora") );
        i.setVia( rs.getString("via") );
        i.setFecha_salida( rs.getString("fecha_salida") );
        i.setCel_cond( rs.getString("cel_cond") );
        i.setCliente( rs.getString("cliente") );
        i.setTipo_despacho( rs.getString("tipo_despacho") );
        i.setTipo_reporte( rs.getString("tipo_reporte") );
        i.setNom_reporte( rs.getString("nom_reporte") );
        i.setNom_ciudad( rs.getString("nom_ciudad") );
        i.setCod_ciudad( rs.getString("cod_ciudad") );
        i.setCedcon_anterior( rs.getString("cedcon_anterior") );
        i.setNomcond_anterior( rs.getString("nomcond_anterior") );
        i.setCedprop_anterior( rs.getString("cedprop_anterior") );
        i.setNomprop_anterior( rs.getString("nomprop_anterior") );
        i.setPlaca_anterior( rs.getString("placa_anterior") );        
        i.setVia_anterior( rs.getString("via_anterior") );
        i.setCadena( rs.getString("cadena") );
        
        return i;        
    }
    
    /**
     * Getter for property caravana.
     * @return Value of property caravana.
     */
    public java.lang.String getCaravana() {
        return caravana;
    }
    
    /**
     * Setter for property caravana.
     * @param caravana New value of property caravana.
     */
    public void setCaravana(java.lang.String caravana) {
        this.caravana = caravana;
    }
    
    /**
     * Getter for property cedcon.
     * @return Value of property cedcon.
     */
    public java.lang.String getCedcon() {
        return cedcon;
    }
    
    /**
     * Setter for property cedcon.
     * @param cedcon New value of property cedcon.
     */
    public void setCedcon(java.lang.String cedcon) {
        this.cedcon = cedcon;
    }
    
    /**
     * Getter for property cedprop.
     * @return Value of property cedprop.
     */
    public java.lang.String getCedprop() {
        return cedprop;
    }
    
    /**
     * Setter for property cedprop.
     * @param cedprop New value of property cedprop.
     */
    public void setCedprop(java.lang.String cedprop) {
        this.cedprop = cedprop;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property escolta.
     * @return Value of property escolta.
     */
    public java.lang.String getEscolta() {
        return escolta;
    }
    
    /**
     * Setter for property escolta.
     * @param escolta New value of property escolta.
     */
    public void setEscolta(java.lang.String escolta) {
        this.escolta = escolta;
    }
    
    /**
     * Getter for property fecha_despacho.
     * @return Value of property fecha_despacho.
     */
    public java.lang.String getFecha_despacho() {
        return fecha_despacho;
    }
    
    /**
     * Setter for property fecha_despacho.
     * @param fecha_despacho New value of property fecha_despacho.
     */
    public void setFecha_despacho(java.lang.String fecha_despacho) {
        this.fecha_despacho = fecha_despacho;
    }
    
    /**
     * Getter for property fecha_prox_reporte.
     * @return Value of property fecha_prox_reporte.
     */
    public java.lang.String getFecha_prox_reporte() {
        return fecha_prox_reporte;
    }
    
    /**
     * Setter for property fecha_prox_reporte.
     * @param fecha_prox_reporte New value of property fecha_prox_reporte.
     */
    public void setFecha_prox_reporte(java.lang.String fecha_prox_reporte) {
        this.fecha_prox_reporte = fecha_prox_reporte;
    }
    
    /**
     * Getter for property fecha_ult_reporte.
     * @return Value of property fecha_ult_reporte.
     */
    public java.lang.String getFecha_ult_reporte() {
        return fecha_ult_reporte;
    }
    
    /**
     * Setter for property fecha_ult_reporte.
     * @param fecha_ult_reporte New value of property fecha_ult_reporte.
     */
    public void setFecha_ult_reporte(java.lang.String fecha_ult_reporte) {
        this.fecha_ult_reporte = fecha_ult_reporte;
    }
    
    /**
     * Getter for property nomcond.
     * @return Value of property nomcond.
     */
    public java.lang.String getNomcond() {
        return nomcond;
    }
    
    /**
     * Setter for property nomcond.
     * @param nomcond New value of property nomcond.
     */
    public void setNomcond(java.lang.String nomcond) {
        this.nomcond = nomcond;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property nomdestino.
     * @return Value of property nomdestino.
     */
    public java.lang.String getNomdestino() {
        return nomdestino;
    }
    
    /**
     * Setter for property nomdestino.
     * @param nomdestino New value of property nomdestino.
     */
    public void setNomdestino(java.lang.String nomdestino) {
        this.nomdestino = nomdestino;
    }
    
    /**
     * Getter for property nomorigen.
     * @return Value of property nomorigen.
     */
    public java.lang.String getNomorigen() {
        return nomorigen;
    }
    
    /**
     * Setter for property nomorigen.
     * @param nomorigen New value of property nomorigen.
     */
    public void setNomorigen(java.lang.String nomorigen) {
        this.nomorigen = nomorigen;
    }
    
    /**
     * Getter for property nomprop.
     * @return Value of property nomprop.
     */
    public java.lang.String getNomprop() {
        return nomprop;
    }
    
    /**
     * Setter for property nomprop.
     * @param nomprop New value of property nomprop.
     */
    public void setNomprop(java.lang.String nomprop) {
        this.nomprop = nomprop;
    }
    
    /**
     * Getter for property nompto_control_proxreporte.
     * @return Value of property nompto_control_proxreporte.
     */
    public java.lang.String getNompto_control_proxreporte() {
        return nompto_control_proxreporte;
    }
    
    /**
     * Setter for property nompto_control_proxreporte.
     * @param nompto_control_proxreporte New value of property nompto_control_proxreporte.
     */
    public void setNompto_control_proxreporte(java.lang.String nompto_control_proxreporte) {
        this.nompto_control_proxreporte = nompto_control_proxreporte;
    }
    
    /**
     * Getter for property nompto_control_ultreporte.
     * @return Value of property nompto_control_ultreporte.
     */
    public java.lang.String getNompto_control_ultreporte() {
        return nompto_control_ultreporte;
    }
    
    /**
     * Setter for property nompto_control_ultreporte.
     * @param nompto_control_ultreporte New value of property nompto_control_ultreporte.
     */
    public void setNompto_control_ultreporte(java.lang.String nompto_control_ultreporte) {
        this.nompto_control_ultreporte = nompto_control_ultreporte;
    }
    
    /**
     * Getter for property nomzona.
     * @return Value of property nomzona.
     */
    public java.lang.String getNomzona() {
        return nomzona;
    }
    
    /**
     * Setter for property nomzona.
     * @param nomzona New value of property nomzona.
     */
    public void setNomzona(java.lang.String nomzona) {
        this.nomzona = nomzona;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property pto_control_proxreporte.
     * @return Value of property pto_control_proxreporte.
     */
    public java.lang.String getPto_control_proxreporte() {
        return pto_control_proxreporte;
    }
    
    /**
     * Setter for property pto_control_proxreporte.
     * @param pto_control_proxreporte New value of property pto_control_proxreporte.
     */
    public void setPto_control_proxreporte(java.lang.String pto_control_proxreporte) {
        this.pto_control_proxreporte = pto_control_proxreporte;
    }
    
    /**
     * Getter for property pto_control_ultreporte.
     * @return Value of property pto_control_ultreporte.
     */
    public java.lang.String getPto_control_ultreporte() {
        return pto_control_ultreporte;
    }
    
    /**
     * Setter for property pto_control_ultreporte.
     * @param pto_control_ultreporte New value of property pto_control_ultreporte.
     */
    public void setPto_control_ultreporte(java.lang.String pto_control_ultreporte) {
        this.pto_control_ultreporte = pto_control_ultreporte;
    }
    
    /**
     * Getter for property ult_observacion.
     * @return Value of property ult_observacion.
     */
    public java.lang.String getUlt_observacion() {
        return ult_observacion;
    }
    
    /**
     * Setter for property ult_observacion.
     * @param ult_observacion New value of property ult_observacion.
     */
    public void setUlt_observacion(java.lang.String ult_observacion) {
        this.ult_observacion = ult_observacion;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property fecha_salida.
     * @return Value of property fecha_salida.
     */
    public java.lang.String getFecha_salida() {
        return fecha_salida;
    }
    
    /**
     * Setter for property fecha_salida.
     * @param fecha_salida New value of property fecha_salida.
     */
    public void setFecha_salida(java.lang.String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property cadena.
     * @return Value of property cadena.
     */
    public java.lang.String getCadena() {
        return cadena;
    }
    
    /**
     * Setter for property cadena.
     * @param cadena New value of property cadena.
     */
    public void setCadena(java.lang.String cadena) {
        this.cadena = cadena;
    }
    
    /**
     * Getter for property cedcon_anterior.
     * @return Value of property cedcon_anterior.
     */
    public java.lang.String getCedcon_anterior() {
        return cedcon_anterior;
    }
    
    /**
     * Setter for property cedcon_anterior.
     * @param cedcon_anterior New value of property cedcon_anterior.
     */
    public void setCedcon_anterior(java.lang.String cedcon_anterior) {
        this.cedcon_anterior = cedcon_anterior;
    }
    
    /**
     * Getter for property cedprop_anterior.
     * @return Value of property cedprop_anterior.
     */
    public java.lang.String getCedprop_anterior() {
        return cedprop_anterior;
    }
    
    /**
     * Setter for property cedprop_anterior.
     * @param cedprop_anterior New value of property cedprop_anterior.
     */
    public void setCedprop_anterior(java.lang.String cedprop_anterior) {
        this.cedprop_anterior = cedprop_anterior;
    }
    
    /**
     * Getter for property cel_cond.
     * @return Value of property cel_cond.
     */
    public java.lang.String getCel_cond() {
        return cel_cond;
    }
    
    /**
     * Setter for property cel_cond.
     * @param cel_cond New value of property cel_cond.
     */
    public void setCel_cond(java.lang.String cel_cond) {
        this.cel_cond = cel_cond;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property cod_ciudad.
     * @return Value of property cod_ciudad.
     */
    public java.lang.String getCod_ciudad() {
        return cod_ciudad;
    }
    
    /**
     * Setter for property cod_ciudad.
     * @param cod_ciudad New value of property cod_ciudad.
     */
    public void setCod_ciudad(java.lang.String cod_ciudad) {
        this.cod_ciudad = cod_ciudad;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property demora.
     * @return Value of property demora.
     */
    public int getDemora() {
        return demora;
    }
    
    /**
     * Setter for property demora.
     * @param demora New value of property demora.
     */
    public void setDemora(int demora) {
        this.demora = demora;
    }
    
    /**
     * Getter for property nom_ciudad.
     * @return Value of property nom_ciudad.
     */
    public java.lang.String getNom_ciudad() {
        return nom_ciudad;
    }
    
    /**
     * Setter for property nom_ciudad.
     * @param nom_ciudad New value of property nom_ciudad.
     */
    public void setNom_ciudad(java.lang.String nom_ciudad) {
        this.nom_ciudad = nom_ciudad;
    }
    
    /**
     * Getter for property nom_reporte.
     * @return Value of property nom_reporte.
     */
    public java.lang.String getNom_reporte() {
        return nom_reporte;
    }
    
    /**
     * Setter for property nom_reporte.
     * @param nom_reporte New value of property nom_reporte.
     */
    public void setNom_reporte(java.lang.String nom_reporte) {
        this.nom_reporte = nom_reporte;
    }
    
    /**
     * Getter for property nomcond_anterior.
     * @return Value of property nomcond_anterior.
     */
    public java.lang.String getNomcond_anterior() {
        return nomcond_anterior;
    }
    
    /**
     * Setter for property nomcond_anterior.
     * @param nomcond_anterior New value of property nomcond_anterior.
     */
    public void setNomcond_anterior(java.lang.String nomcond_anterior) {
        this.nomcond_anterior = nomcond_anterior;
    }
    
    /**
     * Getter for property nomprop_anterior.
     * @return Value of property nomprop_anterior.
     */
    public java.lang.String getNomprop_anterior() {
        return nomprop_anterior;
    }
    
    /**
     * Setter for property nomprop_anterior.
     * @param nomprop_anterior New value of property nomprop_anterior.
     */
    public void setNomprop_anterior(java.lang.String nomprop_anterior) {
        this.nomprop_anterior = nomprop_anterior;
    }
    
    /**
     * Getter for property placa_anterior.
     * @return Value of property placa_anterior.
     */
    public java.lang.String getPlaca_anterior() {
        return placa_anterior;
    }
    
    /**
     * Setter for property placa_anterior.
     * @param placa_anterior New value of property placa_anterior.
     */
    public void setPlaca_anterior(java.lang.String placa_anterior) {
        this.placa_anterior = placa_anterior;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_despacho.
     * @return Value of property tipo_despacho.
     */
    public java.lang.String getTipo_despacho() {
        return tipo_despacho;
    }
    
    /**
     * Setter for property tipo_despacho.
     * @param tipo_despacho New value of property tipo_despacho.
     */
    public void setTipo_despacho(java.lang.String tipo_despacho) {
        this.tipo_despacho = tipo_despacho;
    }
    
    /**
     * Getter for property tipo_reporte.
     * @return Value of property tipo_reporte.
     */
    public java.lang.String getTipo_reporte() {
        return tipo_reporte;
    }
    
    /**
     * Setter for property tipo_reporte.
     * @param tipo_reporte New value of property tipo_reporte.
     */
    public void setTipo_reporte(java.lang.String tipo_reporte) {
        this.tipo_reporte = tipo_reporte;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property via.
     * @return Value of property via.
     */
    public java.lang.String getVia() {
        return via;
    }
    
    /**
     * Setter for property via.
     * @param via New value of property via.
     */
    public void setVia(java.lang.String via) {
        this.via = via;
    }
    
    /**
     * Getter for property via_anterior.
     * @return Value of property via_anterior.
     */
    public java.lang.String getVia_anterior() {
        return via_anterior;
    }
    
    /**
     * Setter for property via_anterior.
     * @param via_anterior New value of property via_anterior.
     */
    public void setVia_anterior(java.lang.String via_anterior) {
        this.via_anterior = via_anterior;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property tipo_procedencia.
     * @return Value of property tipo_procedencia.
     */
    public java.lang.String getTipo_procedencia() {
        return tipo_procedencia;
    }
    
    /**
     * Setter for property tipo_procedencia.
     * @param tipo_procedencia New value of property tipo_procedencia.
     */
    public void setTipo_procedencia(java.lang.String tipo_procedencia) {
        this.tipo_procedencia = tipo_procedencia;
    }
    
    /**
     * Getter for property ubicacion_procedencia.
     * @return Value of property ubicacion_procedencia.
     */
    public java.lang.String getUbicacion_procedencia() {
        return ubicacion_procedencia;
    }
    
    /**
     * Setter for property ubicacion_procedencia.
     * @param ubicacion_procedencia New value of property ubicacion_procedencia.
     */
    public void setUbicacion_procedencia(java.lang.String ubicacion_procedencia) {
        this.ubicacion_procedencia = ubicacion_procedencia;
    }
    
    /**
     * Getter for property fechareporte.
     * @return Value of property fechareporte.
     */
    public java.lang.String getFechareporte() {
        return fechareporte;
    }
    
    /**
     * Setter for property fechareporte.
     * @param fechareporte New value of property fechareporte.
     */
    public void setFechareporte(java.lang.String fechareporte) {
        this.fechareporte = fechareporte;
    }
    
    /**
     * Getter for property fechareporte_planeado.
     * @return Value of property fechareporte_planeado.
     */
    public java.lang.String getFechareporte_planeado() {
        return fechareporte_planeado;
    }
    
    /**
     * Setter for property fechareporte_planeado.
     * @param fechareporte_planeado New value of property fechareporte_planeado.
     */
    public void setFechareporte_planeado(java.lang.String fechareporte_planeado) {
        this.fechareporte_planeado = fechareporte_planeado;
    }
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa() {
        return causa;
    }
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.lang.String causa) {
        this.causa = causa;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.lang.String getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(java.lang.String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property tipo_eliminacion.
     * @return Value of property tipo_eliminacion.
     */
    public java.lang.String getTipo_eliminacion() {
        return tipo_eliminacion;
    }
    
    /**
     * Setter for property tipo_eliminacion.
     * @param tipo_eliminacion New value of property tipo_eliminacion.
     */
    public void setTipo_eliminacion(java.lang.String tipo_eliminacion) {
        this.tipo_eliminacion = tipo_eliminacion;
    }
    
}
