/**
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/** 
 *
 * @author Alvaro
 */
public class OfertaEca {

  private int id_orden;
  private String fecha_oferta;
  private int id_cliente;
  private int nic;
  private String nit;
  private String nombre_cliente; 
  private String num_os;
  private String estudio_economico;
  private String tipo_dtf;
  private int cuotas_reales;

  private String tipo_identificacion;

    /** Creates a new instance of OfertaEca */
    public OfertaEca() {
    }

    public static OfertaEca load(java.sql.ResultSet rs)throws java.sql.SQLException{

        OfertaEca ofertaEca = new OfertaEca();

        ofertaEca.setId_orden( rs.getInt("id_orden") );
        ofertaEca.setFecha_oferta(rs.getString("fecha_oferta") );
        ofertaEca.setId_cliente(rs.getInt("id_cliente") );
        ofertaEca.setNic(rs.getInt("nic") );
        ofertaEca.setNit(rs.getString("nit") );
        ofertaEca.setNombre_cliente(rs.getString("nombre_cliente") );
        ofertaEca.setNum_os(rs.getString("num_os") );
        ofertaEca.setEstudio_economico(rs.getString("estudio_economico") );
        ofertaEca.setTipo_dtf(rs.getString("tipo_dtf") );
        ofertaEca.setCuotas_reales( rs.getInt("cuotas_reales") );
        
        ofertaEca.setTipoIdentificacion( rs.getString("tipo_identificacion") );
        return ofertaEca;

    }









    /**
     * @return the id_orden
     */
    public int getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(int id_orden) {
        this.id_orden = id_orden;
    }

    /**
     * @return the fecha_oferta
     */
    public String getFecha_oferta() {
        return fecha_oferta;
    }

    /**
     * @param fecha_oferta the fecha_oferta to set
     */
    public void setFecha_oferta(String fecha_oferta) {
        this.fecha_oferta = fecha_oferta;
    }

    /**
     * @return the id_cliente
     */
    public int getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the nic
     */
    public int getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(int nic) {
        this.nic = nic;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the nombre_cliente
     */
    public String getNombre_cliente() {
        return nombre_cliente;
    }

    /**
     * @param nombre_cliente the nombre_cliente to set
     */
    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the estudio_economico
     */
    public String getEstudio_economico() {
        return estudio_economico;
    }

    /**
     * @param estudio_economico the estudio_economico to set
     */
    public void setEstudio_economico(String estudio_economico) {
        this.estudio_economico = estudio_economico;
    }

    /**
     * @return the tipo_dtf
     */
    public String getTipo_dtf() {
        return tipo_dtf;
    }

    /**
     * @param tipo_dtf the tipo_dtf to set
     */
    public void setTipo_dtf(String tipo_dtf) {
        this.tipo_dtf = tipo_dtf;
    }

    /**
     * @return the cuotas_reales
     */
    public int getCuotas_reales() {
        return cuotas_reales;
    }

    /**
     * @param cuotas_reales the cuotas_reales to set
     */
    public void setCuotas_reales(int cuotas_reales) {
        this.cuotas_reales = cuotas_reales;
    }



    public String getTipoIdentificacion() {
        return tipo_identificacion;
    }

    public void setTipoIdentificacion(String tipo_identificacion1) {
        this.tipo_identificacion= tipo_identificacion1;
    }




}
