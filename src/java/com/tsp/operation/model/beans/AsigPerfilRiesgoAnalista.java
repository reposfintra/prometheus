/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author Roberto Parra
 */
public class AsigPerfilRiesgoAnalista {
  
    
    
   private String  idusuario;
   private String nombre;
   private String perfil;
   private String id_perfil;
   private String estado;
   private String idrol;
   private String rol;
   private String perfilAnalista;
   private String eliminar;
   private String modificar;
   private String monto_minimo_decision;
   private String monto_decision;

   

    /**
     * @return the idrol
     */
    public String getIdrol() {
        return idrol;
    }

    /**
     * @param idrol the idrol to set
     */
    public void setIdrol(String idrol) {
        this.idrol = idrol;
    }

    /**
     * @return the idusuario
     */
    public String getIdusuario() {
        return idusuario;
    }

    /**
     * @param idusuario the idusuario to set
     */
    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }
    /**
     * @return the rol
     */
    public String getRol() {
        return rol;
    }

    /**
     * @param rol the rol to set
     */
    public void setRol(String rol) {
        this.rol = rol;
    }


    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the perfil
     */
    public String getPerfil() {
        return perfil;
    }

    /**
     * @param perfil the perfil to set
     */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    /**
     * @return the eliminar
     */
    public String getEliminar() {
        return eliminar;
    }

    /**
     * @param eliminar the eliminar to set
     */
    public void setEliminar(String eliminar) {
        this.eliminar = eliminar;
    }

    /**
     * @return the modificar
     */
    public String getModificar() {
        return modificar;
    }

    /**
     * @param modificar the modificar to set
     */
    public void setModificar(String modificar) {
        this.modificar = modificar;
    }

    /**
     * @return the id_perfil
     */
    public String getId_perfil() {
        return id_perfil;
    }

    /**
     * @param id_perfil the id_perfil to set
     */
    public void setId_perfil(String id_perfil) {
        this.id_perfil = id_perfil;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
        /**
     * @return the perfilAnalista
     */
    public String getPerfilAnalista() {
        return perfilAnalista;
    }

    /**
     * @param perfilAnalista the perfilAnalista to set
     */
    public void setPerfilAnalista(String perfilAnalista) {
        this.perfilAnalista = perfilAnalista;
    }
       /**
     * @return the monto_decision
     */
    public String getMonto_decision() {
        return monto_decision;
    }

    /**
     * @param monto_decision the monto_decision to set
     */
    public void setMonto_decision(String monto_decision) {
        this.monto_decision = monto_decision;
    }
    
        /**
     * @return the monto_minimo_decision
     */
    public String getMonto_minimo_decision() {
        return monto_minimo_decision;
    }

    /**
     * @param monto_minimo_decision the monto_minimo_decision to set
     */
    public void setMonto_minimo_decision(String monto_minimo_decision) {
        this.monto_minimo_decision = monto_minimo_decision;
    }

}
