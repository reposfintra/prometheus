/*
 * PrefacturaEcaContratista.java
 *
 * Created on 19 de marzo de 2009, 18:10
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Fintra
 */
public class PrefacturaEcaContratista {
    
    private int id_orden;
    private String id_contratista;
    private String nombre_contratista;
    private double e_total_prev1;
    private double e_iva_total_prev1;
    private String nit;
    private double bonificacion;
    private double e_iva_bonificacion;
   
    
    
    /** Creates a new instance of PrefacturaEcaContratista */
    public PrefacturaEcaContratista() {
    }
    



    public static PrefacturaEcaContratista load(java.sql.ResultSet rs)throws java.sql.SQLException{

        PrefacturaEcaContratista prefacturaEcaContratista = new PrefacturaEcaContratista();

        prefacturaEcaContratista.setId_contratista( rs.getString("id_contratista") );
        prefacturaEcaContratista.setId_orden(rs.getInt("id_orden") );
        prefacturaEcaContratista.setNombre_contratista( rs.getString("nombre_contratista") );
        prefacturaEcaContratista.setE_total_prev1( rs.getDouble("e_total_prev1") );
        prefacturaEcaContratista.setE_iva_total_prev1( rs.getDouble("e_iva_total_prev1") );
        prefacturaEcaContratista.setNit( rs.getString("nit") ); 
        prefacturaEcaContratista.setBonificacion(rs.getDouble("bonificacion"));
        prefacturaEcaContratista.setE_iva_bonificacion(rs.getDouble("e_iva_bonificacion"));
        System.out.println("setE_iva_bonificacion"+prefacturaEcaContratista.getE_iva_bonificacion());
        return prefacturaEcaContratista;
    }
    
    
    
    
    

    /**
     * @return the contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param contratista the contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    
    /**
     * @return the nombre_contratista
     */
    public String getNombre_contratista() {
        return nombre_contratista;
    }

    /**
     * @param contratista the nombre_contratista to set
     */
    public void setNombre_contratista(String nombre_contratista) {
        this.nombre_contratista = nombre_contratista;
    }    
    
    
 
    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param contratista the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }        
    
    
    
    
    
    
    
    
    /**
     * @return the id_orden
     */
    public int getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(int id_orden) {
        this.id_orden = id_orden;
    }
    

    /**
     * @return the e_total_prev1
     */
    public Double getE_total_prev1() {
        return e_total_prev1;
    }

    /**
     * @param e_total_prev1 the e_total_prev1 to set
     */
    public void setE_total_prev1(Double e_total_prev1) {
        this.e_total_prev1 = e_total_prev1;
    }

    /**
     * @return the e_iva_total_prev1
     */
    public Double getE_iva_total_prev1() {
        return e_iva_total_prev1;
    }

    /**
     * @param e_iva_total_prev1 the e_iva_total_prev1 to set
     */
    public void setE_iva_total_prev1(Double e_iva_total_prev1) {
        this.e_iva_total_prev1 = e_iva_total_prev1;
    }

    /**
     * @return the bonificacion
     */
    public double getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the e_iva_bonificacion
     */
    public double getE_iva_bonificacion() {
        return e_iva_bonificacion;
    }

    /**
     * @param e_iva_bonificacion the e_iva_bonificacion to set
     */
    public void setE_iva_bonificacion(double e_iva_bonificacion) {
        this.e_iva_bonificacion = e_iva_bonificacion;
    }
    
    
   
}    