/*
 * Nombre        Unidad.java
 * Autor         Ing Jose de la Rosa
 * Modificado    Ing Sandra Escalante
 * Fecha         26 de junio de 2005, 11:15 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Unidad implements Serializable{
    
    private String codigo;
    private String descripcion;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    private String tipo;
    private String base;
    
    private String ntipo;
    
    public static Unidad load(ResultSet rs)throws SQLException{
        Unidad u = new Unidad();
        u.setCodigo(rs.getString("codigo"));
        u.setDescripcion(rs.getString("descripcion"));
        u.setCia(rs.getString("cia"));
        u.setRec_status(rs.getString("rec_status"));
        u.setLast_update(rs.getDate("last_update"));
        u.setUser_update(rs.getString("user_update"));
        u.setCreation_date(rs.getDate("creation_date"));
        u.setCreation_user(rs.getString("creation_user"));
        u.setTipo(rs.getString("tipo"));
        u.setBase(rs.getString("base"));
        return u;
    }
    
    /**
     * Getter for property cia
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param tipo New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property codigo
     * @return Value of property codigo
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo
     * @param tipo New value of property codigo
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property creation_date
     * @return Value of property creation_date
     */
    public java.util.Date getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date
     * @param tipo New value of property creation_date
     */
    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }
    /**
     * Getter for property creation_user
     * @return Value of property creation_user
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user
     * @param tipo New value of property creation_user
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    /**
     * Getter for property descripcion
     * @return Value of property descripcion
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion
     * @param tipo New value of property descripcion
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    /**
     * Getter for property last_update
     * @return Value of property last_update
     */
    public java.util.Date getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update
     * @param tipo New value of property last_update
     */
    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }
    /**
     * Getter for property rec_status
     * @return Value of property rec_status
     */
    public java.lang.String getRec_status() {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status
     * @param tipo New value of property rec_status
     */
    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property ntipo.
     * @return Value of property ntipo.
     */
    public java.lang.String getNtipo() {
        return ntipo;
    }
    
    /**
     * Setter for property ntipo.
     * @param ntipo New value of property ntipo.
     */
    public void setNtipo(java.lang.String ntipo) {
        this.ntipo = ntipo;
    }
    
}
