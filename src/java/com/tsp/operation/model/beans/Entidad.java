package com.tsp.operation.model.beans;

/**
 * Bean para la tabla dc.entidad<br/>
 * 2/09/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Entidad {

    private String codSuscriptor;
    private String nombreSuscriptor;
    private String nit;
    private String contrato;
    private String creationUser;
    private String userUpdate;


    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }
    /**
     * Get the value of contrato
     *
     * @return the value of contrato
     */
    public String getContrato() {
        return contrato;
    }

    /**
     * Set the value of contrato
     *
     * @param contrato new value of contrato
     */
    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    /**
     * Get the value of nit
     *
     * @return the value of nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * Set the value of nit
     *
     * @param nit new value of nit
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * Get the value of nombreSuscriptor
     *
     * @return the value of nombreSuscriptor
     */
    public String getNombreSuscriptor() {
        return nombreSuscriptor;
    }

    /**
     * Set the value of nombreSuscriptor
     *
     * @param nombreSuscriptor new value of nombreSuscriptor
     */
    public void setNombreSuscriptor(String nombreSuscriptor) {
        this.nombreSuscriptor = nombreSuscriptor;
    }


    /**
     * Get the value of codSuscriptor
     *
     * @return the value of codSuscriptor
     */
    public String getCodSuscriptor() {
        return codSuscriptor;
    }

    /**
     * Set the value of codSuscriptor
     *
     * @param codSuscriptor new value of codSuscriptor
     */
    public void setCodSuscriptor(String codSuscriptor) {
        this.codSuscriptor = codSuscriptor;
    }


}
