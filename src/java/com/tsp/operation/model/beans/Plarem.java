/*
 * Plarem.java
 *
 * Created on 20 de junio de 2005, 09:49 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  Jm
 */
public class Plarem implements Serializable{
    String numpla;
    String numrem;
    String porcent;
    String porcentaje_contabilizacion;
    String account_code_c;
    String account_code_i;
    String paralaremesa;
    String paralaplanilla;
    
    double vlrpla;
    double vlrrem;
    
    /** Creates a new instance of Plarem */
    public Plarem() {
         String numpla="";
         String numrem="";
         String porcent="";
         String porcentaje_contabilizacion ="";
         String account_code_c = "";
         String account_code_i = "";
         String paralaremesa = "";
         String paralaplanilla = "";
         vlrpla = 0;
         vlrrem = 0;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property porcent.
     * @return Value of property porcent.
     */
    public java.lang.String getPorcent() {
        return porcent;
    }
    
    /**
     * Setter for property porcent.
     * @param porcent New value of property porcent.
     */
    public void setPorcent(java.lang.String porcent) {
        this.porcent = porcent;
    }
    
    /**
     * Getter for property porcentaje_contabilizacion.
     * @return Value of property porcentaje_contabilizacion.
     */
    public java.lang.String getPorcentaje_contabilizacion() {
        return porcentaje_contabilizacion;
    }
    
    /**
     * Setter for property porcentaje_contabilizacion.
     * @param porcentaje_contabilizacion New value of property porcentaje_contabilizacion.
     */
    public void setPorcentaje_contabilizacion(java.lang.String porcentaje_contabilizacion) {
        this.porcentaje_contabilizacion = porcentaje_contabilizacion;
    }
    
    /**
     * Getter for property account_code_c.
     * @return Value of property account_code_c.
     */
    public java.lang.String getAccount_code_c() {
        return account_code_c;
    }
    
    /**
     * Setter for property account_code_c.
     * @param account_code_c New value of property account_code_c.
     */
    public void setAccount_code_c(java.lang.String account_code_c) {
        this.account_code_c = account_code_c;
    }
    
    /**
     * Getter for property account_code_i.
     * @return Value of property account_code_i.
     */
    public java.lang.String getAccount_code_i() {
        return account_code_i;
    }
    
    /**
     * Setter for property account_code_i.
     * @param account_code_i New value of property account_code_i.
     */
    public void setAccount_code_i(java.lang.String account_code_i) {
        this.account_code_i = account_code_i;
    }
    
    /**
     * Getter for property paralaremesa.
     * @return Value of property paralaremesa.
     */
    public java.lang.String getParalaremesa() {
        return paralaremesa;
    }
    
    /**
     * Setter for property paralaremesa.
     * @param paralaremesa New value of property paralaremesa.
     */
    public void setParalaremesa(java.lang.String paralaremesa) {
        this.paralaremesa = paralaremesa;
    }
    
    /**
     * Getter for property paralaplanilla.
     * @return Value of property paralaplanilla.
     */
    public java.lang.String getParalaplanilla() {
        return paralaplanilla;
    }
    
    /**
     * Setter for property paralaplanilla.
     * @param paralaplanilla New value of property paralaplanilla.
     */
    public void setParalaplanilla(java.lang.String paralaplanilla) {
        this.paralaplanilla = paralaplanilla;
    }
    
    /**
     * Getter for property vlrpla.
     * @return Value of property vlrpla.
     */
    public double getVlrpla() {
        return vlrpla;
    }
    
    /**
     * Setter for property vlrpla.
     * @param vlrpla New value of property vlrpla.
     */
    public void setVlrpla(double vlrpla) {
        this.vlrpla = vlrpla;
    }
    
    /**
     * Getter for property vlrrem.
     * @return Value of property vlrrem.
     */
    public double getVlrrem() {
        return vlrrem;
    }
    
    /**
     * Setter for property vlrrem.
     * @param vlrrem New value of property vlrrem.
     */
    public void setVlrrem(double vlrrem) {
        this.vlrrem = vlrrem;
    }
    
}
