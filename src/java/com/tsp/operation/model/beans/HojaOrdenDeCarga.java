
/******************************************************************************** 
 * Nombre clase :                   HojaOrdenDeCarga.java                       *
 * Descripcion :                    Estructura de la Orden De Carga             *
 * Autor :                          LREALES                                     *
 * Fecha :                          11 de mayo de 2006, 01:55 PM                *
 * Version :                        1.0                                         *
 * Copyright :                      Fintravalores S.A.                     *
 ********************************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class HojaOrdenDeCarga {
    
    // 1. Datos Basicos    
    private String dstrct = "";
    private String orden = "";
    private String codciu = "";
    private String remitente = "";
    private String conductor = "";
    private String placa = "";
    private String trailer = "";
    private String contenido = "";
    private String std_job_no = "";
    private String entregar = "";
    private String observacion = "";
    private String precinto1 = "";
    private String precinto2 = "";
    private String precinto3 = "";
    private String precinto4 = "";
    private String precinto5 = "";
    private String fecha_impresion = "";
    private String creation_user = "";
    private String creation_date = "";
    private String user_update = "";
    private String last_update = "";
    private String base = "";
    private String reg_status = "";
    private String precintoc1 = "";
    private String precintoc2 = "";
    private String contenedores = "";
    private String tipocont = "";
    private String tipotrailer = "";
    
    private String dia = "";
    private String mes = "";
    private String ano = "";
    private String empresa = "";
    private String direccion = "";
    private String nomconductor = "";
    private String expced = "";
    private String pase = "";
    private String marca = "";
    private String modelo = "";
    private String motor = "";
    private String chasis = "";
    private String empresaafil = "";
    private String tarjetaoper = "";
    private String color = "";
    private String ciudad = "";
    private String destino = "";
    private String numpla= "";
    private String fecha_cargue = "";
    private String destinatarios;
    private String nombre="";
    private String telefono="";
    
 
    public static HojaOrdenDeCarga load ( ResultSet rs ) throws SQLException {
    
        HojaOrdenDeCarga hojaOrden = new HojaOrdenDeCarga();
        
        hojaOrden.setDstrct( rs.getString("dstrct") );
        hojaOrden.setOrden( rs.getString("orden") );
        hojaOrden.setCodciu( rs.getString("codciu") );
        hojaOrden.setRemitente( rs.getString("codigo") );
        hojaOrden.setConductor( rs.getString("cedula") );
        hojaOrden.setPlaca( rs.getString("placa") );
        hojaOrden.setTrailer( rs.getString("trailer") );
        hojaOrden.setContenido( rs.getString("contenido") );
        hojaOrden.setStd_job_no( rs.getString("std_job_no") );
        hojaOrden.setEntregar( rs.getString("entregar") );        
        hojaOrden.setObservacion(rs.getString("observacion"));
        hojaOrden.setPrecinto1( rs.getString("precinto1") );
        hojaOrden.setPrecinto2( rs.getString("precinto2") );
        hojaOrden.setPrecinto3( rs.getString("precinto3") );
        hojaOrden.setPrecinto4( rs.getString("precinto4") );
        hojaOrden.setPrecinto5( rs.getString("precinto5") );
        hojaOrden.setFecha_impresion( rs.getString("fecha_impresion") );
        hojaOrden.setCreation_user( rs.getString("creation_user") );
        hojaOrden.setCreation_date( rs.getString("creation_date") );
        hojaOrden.setUser_update( rs.getString("user_update") );
        hojaOrden.setLast_update( rs.getString("last_update") );
        hojaOrden.setBase( rs.getString("base") );
        hojaOrden.setReg_status( rs.getString("reg_status") );
        hojaOrden.setPrecintoc1( rs.getString("precintoc1") );
        hojaOrden.setPrecintoc2( rs.getString("precintoc2") );
        hojaOrden.setContenedores( rs.getString("contenedores") );
        hojaOrden.setTipocont( rs.getString("tipocont") );
        hojaOrden.setTipotrailer( rs.getString("tipotrailer") );
        
        return hojaOrden;
    
    }
       
    /**
     * 1. Datos Basicos:
     * Almacena datos basicos de la orden de carga
     * @autor........  LREALES
     * @date.........  11/05/2006
     * @version .....  1.0      
     */ 
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property orden.
     * @return Value of property orden.
     */
    public java.lang.String getOrden() {
        return orden;
    }
    
    /**
     * Setter for property orden.
     * @param orden New value of property orden.
     */
    public void setOrden(java.lang.String orden) {
        this.orden = orden;
    }
    
    /**
     * Getter for property codciu.
     * @return Value of property codciu.
     */
    public java.lang.String getCodciu() {
        return codciu;
    }
    
    /**
     * Setter for property codciu.
     * @param codciu New value of property codciu.
     */
    public void setCodciu(java.lang.String codciu) {
        this.codciu = codciu;
    }
    
    /**
     * Getter for property remitente.
     * @return Value of property remitente.
     */
    public java.lang.String getRemitente() {
        return remitente;
    }
    
    /**
     * Setter for property remitente.
     * @param codigo New value of property remitente.
     */
    public void setRemitente(java.lang.String remitente) {
        this.remitente = remitente;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param cedula New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property trailer.
     * @return Value of property trailer.
     */
    public java.lang.String getTrailer() {
        return trailer;
    }
    
    /**
     * Setter for property trailer.
     * @param trailer New value of property trailer.
     */
    public void setTrailer(java.lang.String trailer) {
        this.trailer = trailer;
    }
    
    /**
     * Getter for property contenido.
     * @return Value of property contenido.
     */
    public java.lang.String getContenido() {
        return contenido;
    }
    
    /**
     * Setter for property contenido.
     * @param contenido New value of property contenido.
     */
    public void setContenido(java.lang.String contenido) {
        this.contenido = contenido;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property entregar.
     * @return Value of property entregar.
     */
    public java.lang.String getEntregar() {
        return entregar;
    }
    
    /**
     * Setter for property entregar.
     * @param entregar New value of property entregar.
     */
    public void setEntregar(java.lang.String entregar) {
        this.entregar = entregar;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property precinto1.
     * @return Value of property precinto1.
     */
    public java.lang.String getPrecinto1() {
        return precinto1;
    }
    
    /**
     * Setter for property precinto1.
     * @param precinto1 New value of property precinto1.
     */
    public void setPrecinto1(java.lang.String precinto1) {
        this.precinto1 = precinto1;
    }
    
    /**
     * Getter for property precinto2.
     * @return Value of property precinto2.
     */
    public java.lang.String getPrecinto2() {
        return precinto2;
    }
    
    /**
     * Setter for property precinto2.
     * @param precinto2 New value of property precinto2.
     */
    public void setPrecinto2(java.lang.String precinto2) {
        this.precinto2 = precinto2;
    }
    
    /**
     * Getter for property precinto3.
     * @return Value of property precinto3.
     */
    public java.lang.String getPrecinto3() {
        return precinto3;
    }
    
    /**
     * Setter for property precinto3.
     * @param precinto3 New value of property precinto3.
     */
    public void setPrecinto3(java.lang.String precinto3) {
        this.precinto3 = precinto3;
    }
    
    /**
     * Getter for property precinto4.
     * @return Value of property precinto4.
     */
    public java.lang.String getPrecinto4() {
        return precinto4;
    }
    
    /**
     * Setter for property precinto4.
     * @param precinto4 New value of property precinto4.
     */
    public void setPrecinto4(java.lang.String precinto4) {
        this.precinto4 = precinto4;
    }
    
    /**
     * Getter for property precinto5.
     * @return Value of property precinto5.
     */
    public java.lang.String getPrecinto5() {
        return precinto5;
    }
    
    /**
     * Setter for property precinto5.
     * @param precinto5 New value of property precinto5.
     */
    public void setPrecinto5(java.lang.String precinto5) {
        this.precinto5 = precinto5;
    }
    
    /**
     * Getter for property fecha_impresion.
     * @return Value of property fecha_impresion.
     */
    public java.lang.String getFecha_impresion() {
        return fecha_impresion;
    }
    
    /**
     * Setter for property fecha_impresion.
     * @param fecha_impresion New value of property fecha_impresion.
     */
    public void setFecha_impresion(java.lang.String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
   
    /**
     * Getter for property precintoc1.
     * @return Value of property precintoc1.
     */
    public java.lang.String getPrecintoc1() {
        return precintoc1;
    }
    
    /**
     * Setter for property precintoc1.
     * @param precintoc1 New value of property precintoc1.
     */
    public void setPrecintoc1(java.lang.String precintoc1) {
        this.precintoc1 = precintoc1;
    }
    
    /**
     * Getter for property precintoc2.
     * @return Value of property precintoc2.
     */
    public java.lang.String getPrecintoc2() {
        return precintoc2;
    }
    
    /**
     * Setter for property precintoc2.
     * @param precintoc2 New value of property precintoc2.
     */
    public void setPrecintoc2(java.lang.String precintoc2) {
        this.precintoc2 = precintoc2;
    }
    
    /**
     * Getter for property contenedores.
     * @return Value of property contenedores.
     */
    public java.lang.String getContenedores() {
        return contenedores;
    }
    
    /**
     * Setter for property contenedores.
     * @param contenedores New value of property contenedores.
     */
    public void setContenedores(java.lang.String contenedores) {
        this.contenedores = contenedores;
    }
    
    /**
     * Getter for property tipocont.
     * @return Value of property tipocont.
     */
    public java.lang.String getTipocont() {
        return tipocont;
    }
    
    /**
     * Setter for property tipocont.
     * @param tipocont New value of property tipocont.
     */
    public void setTipocont(java.lang.String tipocont) {
        this.tipocont = tipocont;
    }
    
    /**
     * Getter for property tipotrailer.
     * @return Value of property tipotrailer.
     */
    public java.lang.String getTipotrailer() {
        return tipotrailer;
    }
    
    /**
     * Setter for property tipotrailer.
     * @param tipotrailer New value of property tipotrailer.
     */
    public void setTipotrailer(java.lang.String tipotrailer) {
        this.tipotrailer = tipotrailer;
    }
    
    /**
     * Getter for property dia.
     * @return Value of property dia.
     */
    public java.lang.String getDia() {
        return dia;
    }
    
    /**
     * Setter for property dia.
     * @param dia New value of property dia.
     */
    public void setDia(java.lang.String dia) {
        this.dia = dia;
    }
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property ano.
     * @return Value of property ano.
     */
    public java.lang.String getAno() {
        return ano;
    }
    
    /**
     * Setter for property ano.
     * @param ano New value of property ano.
     */
    public void setAno(java.lang.String ano) {
        this.ano = ano;
    }
    
    /**
     * Getter for property empresa.
     * @return Value of property empresa.
     */
    public java.lang.String getEmpresa() {
        return empresa;
    }
    
    /**
     * Setter for property empresa.
     * @param empresa New value of property empresa.
     */
    public void setEmpresa(java.lang.String empresa) {
        this.empresa = empresa;
    }
    
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter for property nomconductor.
     * @return Value of property nomconductor.
     */
    public java.lang.String getNomconductor() {
        return nomconductor;
    }
    
    /**
     * Setter for property nomconductor.
     * @param nomconductor New value of property nomconductor.
     */
    public void setNomconductor(java.lang.String nomconductor) {
        this.nomconductor = nomconductor;
    }
    
    /**
     * Getter for property expced.
     * @return Value of property expced.
     */
    public java.lang.String getExpced() {
        return expced;
    }
    
    /**
     * Setter for property expced.
     * @param expced New value of property expced.
     */
    public void setExpced(java.lang.String expced) {
        this.expced = expced;
    }
    
    /**
     * Getter for property pase.
     * @return Value of property pase.
     */
    public java.lang.String getPase() {
        return pase;
    }
    
    /**
     * Setter for property pase.
     * @param pase New value of property pase.
     */
    public void setPase(java.lang.String pase) {
        this.pase = pase;
    }
    
    /**
     * Getter for property marca.
     * @return Value of property marca.
     */
    public java.lang.String getMarca() {
        return marca;
    }
    
    /**
     * Setter for property marca.
     * @param marca New value of property marca.
     */
    public void setMarca(java.lang.String marca) {
        this.marca = marca;
    }
    
    /**
     * Getter for property modelo.
     * @return Value of property modelo.
     */
    public java.lang.String getModelo() {
        return modelo;
    }
    
    /**
     * Setter for property modelo.
     * @param modelo New value of property modelo.
     */
    public void setModelo(java.lang.String modelo) {
        this.modelo = modelo;
    }
    
    /**
     * Getter for property motor.
     * @return Value of property motor.
     */
    public java.lang.String getMotor() {
        return motor;
    }
    
    /**
     * Setter for property motor.
     * @param motor New value of property motor.
     */
    public void setMotor(java.lang.String motor) {
        this.motor = motor;
    }
    
    /**
     * Getter for property chasis.
     * @return Value of property chasis.
     */
    public java.lang.String getChasis() {
        return chasis;
    }
    
    /**
     * Setter for property chasis.
     * @param chasis New value of property chasis.
     */
    public void setChasis(java.lang.String chasis) {
        this.chasis = chasis;
    }
    
    /**
     * Getter for property empresaafil.
     * @return Value of property empresaafil.
     */
    public java.lang.String getEmpresaafil() {
        return empresaafil;
    }
    
    /**
     * Setter for property empresaafil.
     * @param empresaafil New value of property empresaafil.
     */
    public void setEmpresaafil(java.lang.String empresaafil) {
        this.empresaafil = empresaafil;
    }
    
    /**
     * Getter for property tarjetaoper.
     * @return Value of property tarjetaoper.
     */
    public java.lang.String getTarjetaoper() {
        return tarjetaoper;
    }
    
    /**
     * Setter for property tarjetaoper.
     * @param tarjetaoper New value of property tarjetaoper.
     */
    public void setTarjetaoper(java.lang.String tarjetaoper) {
        this.tarjetaoper = tarjetaoper;
    }
    
    /**
     * Getter for property color.
     * @return Value of property color.
     */
    public java.lang.String getColor() {
        return color;
    }
    
    /**
     * Setter for property color.
     * @param color New value of property color.
     */
    public void setColor(java.lang.String color) {
        this.color = color;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property fecha_cargue.
     * @return Value of property fecha_cargue.
     */
    public java.lang.String getFecha_cargue() {
        return fecha_cargue;
    }
    
    /**
     * Setter for property fecha_cargue.
     * @param fecha_cargue New value of property fecha_cargue.
     */
    public void setFecha_cargue(java.lang.String fecha_cargue) {
        this.fecha_cargue = fecha_cargue;
    }
    
    /**
     * Getter for property destinatarios.
     * @return Value of property destinatarios.
     */
    public java.lang.String getDestinatarios() {
        return destinatarios;
    }
    
    /**
     * Setter for property destinatarios.
     * @param destinatarios New value of property destinatarios.
     */
    public void setDestinatarios(java.lang.String destinatarios) {
        this.destinatarios = destinatarios;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public java.lang.String getTelefono() {
        return telefono;
    }
    
    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }
    
} 