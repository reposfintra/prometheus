/*
 * DespachoManual.java
 *
 * Created on 15 de noviembre de 2005, 02:57 PM
 */

package com.tsp.operation.model.beans;
import java.util.*;
import com.tsp.util.Util;
import java.io.*;
import java.sql.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachoManual
{
    
    private String  reg_status="";
    private String dstrct="";
    private String placa="" ;
    private String fecha_despacho="";
    private String fecha_salida="";
    private String conductor="";
    private String  ruta="";
    
    private String descripcionRuta="";
    
    private String  cliente="";
    private String carga="";
    private String fecha_legalisacion ="";
    private String  usuario_legalisa="";
    private String  last_update ="";
    private String  user_update="";
    private String  creation_date="";
    private String creation_user="";
    private String zona="";
    private String nultimopto="";
    private String nproxipto="";
    private String nombreCliente="";
    private String nombreConductor="";
    private int nPlanilla=0;
    private String puestoControl="";
    /**/
    private String justificacion="";
    private String observacion="";
   
    // LREALES - 12 agosto 2006
    private String zona_nombre = "";
    
    /** Creates a new instance of DespachoManual */
    public DespachoManual ()
    {
       
    }
    
    public static DespachoManual load (ResultSet rs)throws SQLException
    {
        
        ////System.out.println("ENTRO LOAD");
        DespachoManual despacho = new DespachoManual ();
        despacho.setReg_status (rs.getString ("reg_status"));
        despacho.setDstrct (rs.getString ("dstrct"));
        despacho.setPlaca (rs.getString ("placa"));
        despacho.setFecha_despacho (rs.getString ("fecha_despacho"));
        despacho.setFecha_salida(rs.getString ("fecha_salida"));
        despacho.setJustificacion(rs.getString ("justificacion"));
        despacho.setObservacion(rs.getString("descripcion"));
        
        despacho.setConductor (rs.getString ("conductor"));
        despacho.setRuta (rs.getString ("ruta"));
        despacho.setCliente (rs.getString ("cliente"));
        despacho.setCarga (rs.getString ("carga"));
        despacho.setFecha_legalisacion (rs.getString ("fecha_legalisacion"));
        despacho.setUsuario_legalisa (rs.getString ("usuario_legalisa"));
        despacho.setLast_update (rs.getString ("last_update"));
        despacho.setUser_update (rs.getString ("user_update"));
        despacho.setCreation_date (rs.getString ("creation_date"));
        despacho.setCreation_user (rs.getString ("creation_user"));
        despacho.setNombreCliente (rs.getString ("nombreCliente"));
        despacho.setNombreConductor (rs.getString ("nombreConductor"));
        despacho.setNPlanilla (rs.getInt("numpla"));
        
        ////System.out.println("SALIO LOAD");
        return despacho;
    }
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status ()
    {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status (java.lang.String reg_status)
    {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct ()
    {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct (java.lang.String dstrct)
    {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca ()
    {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca (java.lang.String placa)
    {
        this.placa = placa;
    }
    
    /**
     * Getter for property fecha_despacho.
     * @return Value of property fecha_despacho.
     */
    public java.lang.String getFecha_despacho ()
    {
        return fecha_despacho;
    }
    
    /**
     * Setter for property fecha_despacho.
     * @param fecha_despacho New value of property fecha_despacho.
     */
    public void setFecha_despacho (java.lang.String fecha_despacho)
    {
        this.fecha_despacho = fecha_despacho;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor ()
    {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor (java.lang.String conductor)
    {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta ()
    {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta (java.lang.String ruta)
    {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente ()
    {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente (java.lang.String cliente)
    {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property carga.
     * @return Value of property carga.
     */
    public java.lang.String getCarga ()
    {
        return carga;
    }
    
    /**
     * Setter for property carga.
     * @param carga New value of property carga.
     */
    public void setCarga (java.lang.String carga)
    {
        this.carga = carga;
    }
    
    /**
     * Getter for property fecha_legalisacion.
     * @return Value of property fecha_legalisacion.
     */
    public java.lang.String getFecha_legalisacion ()
    {
        return fecha_legalisacion;
    }
    
    /**
     * Setter for property fecha_legalisacion.
     * @param fecha_legalisacion New value of property fecha_legalisacion.
     */
    public void setFecha_legalisacion (java.lang.String fecha_legalisacion)
    {
        this.fecha_legalisacion = fecha_legalisacion;
    }
    
    /**
     * Getter for property usuario_legalisa.
     * @return Value of property usuario_legalisa.
     */
    public java.lang.String getUsuario_legalisa ()
    {
        return usuario_legalisa;
    }
    
    /**
     * Setter for property usuario_legalisa.
     * @param usuario_legalisa New value of property usuario_legalisa.
     */
    public void setUsuario_legalisa (java.lang.String usuario_legalisa)
    {
        this.usuario_legalisa = usuario_legalisa;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update ()
    {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update (java.lang.String last_update)
    {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update ()
    {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update)
    {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date ()
    {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date)
    {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user ()
    {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user)
    {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property nombreCliente.
     * @return Value of property nombreCliente.
     */
    public java.lang.String getNombreCliente ()
    {
        return nombreCliente;
    }
    
    /**
     * Setter for property nombreCliente.
     * @param nombreCliente New value of property nombreCliente.
     */
    public void setNombreCliente (java.lang.String nombreCliente)
    {
        this.nombreCliente = nombreCliente;
    }
    
    /**
     * Getter for property nombreConductor.
     * @return Value of property nombreConductor.
     */
    public java.lang.String getNombreConductor ()
    {
        return nombreConductor;
    }
    
    /**
     * Setter for property nombreConductor.
     * @param nombreConductor New value of property nombreConductor.
     */
    public void setNombreConductor (java.lang.String nombreConductor)
    {
        this.nombreConductor = nombreConductor;
    }
    
    /**
     * Getter for property nPlanilla.
     * @return Value of property nPlanilla.
     */
    public int getNPlanilla ()
    {
        return nPlanilla;
    }
    
    /**
     * Setter for property nPlanilla.
     * @param nPlanilla New value of property nPlanilla.
     */
    public void setNPlanilla (int nPlanilla)
    {
        this.nPlanilla = nPlanilla;
    }
    
    /**
     * Getter for property fecha_salida.
     * @return Value of property fecha_salida.
     */
    public java.lang.String getFecha_salida() {
        return fecha_salida;
    }
    
    /**
     * Setter for property fecha_salida.
     * @param fecha_salida New value of property fecha_salida.
     */
    public void setFecha_salida(java.lang.String fecha_salida) {
        this.fecha_salida = fecha_salida;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona() {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona(java.lang.String zona) {
        this.zona = zona;
    }
        
    /**
     * Getter for property descripcionRuta.
     * @return Value of property descripcionRuta.
     */
    public java.lang.String getDescripcionRuta() {
        return descripcionRuta;
    }
    
    /**
     * Setter for property descripcionRuta.
     * @param descripcionRuta New value of property descripcionRuta.
     */
    public void setDescripcionRuta(java.lang.String descripcionRuta) {
        this.descripcionRuta = descripcionRuta;
    }
    
    /**
     * Getter for property nultimopto.
     * @return Value of property nultimopto.
     */
    public java.lang.String getNultimopto() {
        return nultimopto;
    }
    
    /**
     * Setter for property nultimopto.
     * @param nultimopto New value of property nultimopto.
     */
    public void setNultimopto(java.lang.String nultimopto) {
        this.nultimopto = nultimopto;
    }
    
    /**
     * Getter for property nproxipto.
     * @return Value of property nproxipto.
     */
    public java.lang.String getNproxipto() {
        return nproxipto;
    }
    
    /**
     * Setter for property nproxipto.
     * @param nproxipto New value of property nproxipto.
     */
    public void setNproxipto(java.lang.String nproxipto) {
        this.nproxipto = nproxipto;
    }
    
    /**
     * Getter for property justificacion.
     * @return Value of property justificacion.
     */
    public java.lang.String getJustificacion() {
        return justificacion;
    }
    
    /**
     * Setter for property justificacion.
     * @param justificacion New value of property justificacion.
     */
    public void setJustificacion(java.lang.String justificacion) {
        this.justificacion = justificacion;
    }
    
   
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property puestoControl.
     * @return Value of property puestoControl.
     */
    public java.lang.String getPuestoControl() {
        return puestoControl;
    }
    
    /**
     * Setter for property puestoControl.
     * @param puestoControl New value of property puestoControl.
     */
    public void setPuestoControl(java.lang.String puestoControl) {
        this.puestoControl = puestoControl;
    }
    
    /**
     * Getter for property zona_nombre.
     * @return Value of property zona_nombre.
     */
    public java.lang.String getZona_nombre () {
        
        return zona_nombre;
        
    }
    
    /**
     * Setter for property zona_nombre.
     * @param zona_nombre New value of property zona_nombre.
     */
    public void setZona_nombre ( java.lang.String zona_nombre ) {
        
        this.zona_nombre = zona_nombre;
        
    }
    
}