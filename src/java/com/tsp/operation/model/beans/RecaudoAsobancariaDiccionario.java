/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author mcastillo
 */
public class RecaudoAsobancariaDiccionario {
    
    private String tipo_referencia;
    private String codigo;
    private String descripcion;
  
    
    public RecaudoAsobancariaDiccionario() {
    }

    /**
     * @return the tipo_referencia
     */
    public String getTipo_referencia() {
        return tipo_referencia;
    }

    /**
     * @param tipo_referencia the tipo_referencia to set
     */
    public void setTipo_referencia(String tipo_referencia) {
        this.tipo_referencia = tipo_referencia;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
