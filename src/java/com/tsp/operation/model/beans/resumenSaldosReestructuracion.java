/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.util.Date;

/**
 *
 * @author egonzalez
 */
public class resumenSaldosReestructuracion {
    
    private int id;
    private String reg_status;
    private String dstrct;
    private String Negocio;
    private String Estado;
    private String Tipo_neg;
    private String Capital;
    private String Interes;
    private String IntxMora;
    private String GaC;
    private String Sub_Total;
    private String Dcto_capital;
    private String Dcto_interes;
    private String Dcto_intxMora;
    private String Dcto_gac;
    private String Total_Dcto;
    private String Total_Items;
    private int id_rop;
    private String user_update;
    private Date last_update;
    private Date creation_date;
    private String creation_user;

    public resumenSaldosReestructuracion() {
    }

    public resumenSaldosReestructuracion(int id, String reg_status, String dstrct, String Negocio, String Estado, String Tipo_neg, String Capital, String Interes, String IntxMora, String GaC, String Sub_Total, String Dcto_capital, String Dcto_interes, String Dcto_intxmora, String Dcto_gac, String Total_Dcto, String Total_Items, int id_rop, String user_update, Date last_update, Date creation_date, String creation_user) {
        this.id = id;
        this.reg_status = reg_status;
        this.dstrct = dstrct;
        this.Negocio = Negocio;
        this.Estado = Estado;
        this.Tipo_neg = Tipo_neg;
        this.Capital = Capital;
        this.Interes = Interes;
        this.IntxMora = IntxMora;
        this.GaC = GaC;
        this.Sub_Total = Sub_Total;
        this.Dcto_capital = Dcto_capital;
        this.Dcto_interes = Dcto_interes;
        this.Dcto_intxMora = Dcto_intxmora;
        this.Dcto_gac = Dcto_gac;
        this.Total_Dcto = Total_Dcto;
        this.Total_Items = Total_Items;
        this.id_rop = id_rop;
        this.user_update = user_update;
        this.last_update = last_update;
        this.creation_date = creation_date;
        this.creation_user = creation_user;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the Negocio
     */
    public String getNegocio() {
        return Negocio;
    }

    /**
     * @param Negocio the Negocio to set
     */
    public void setNegocio(String Negocio) {
        this.Negocio = Negocio;
    }

    /**
     * @return the Estado
     */
    public String getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    /**
     * @return the Tipo_neg
     */
    public String getTipo_neg() {
        return Tipo_neg;
    }

    /**
     * @param Tipo_neg the Tipo_neg to set
     */
    public void setTipo_neg(String Tipo_neg) {
        this.Tipo_neg = Tipo_neg;
    }

    /**
     * @return the Capital
     */
    public String getCapital() {
        return Capital;
    }

    /**
     * @param Capital the Capital to set
     */
    public void setCapital(String Capital) {
        this.Capital = Capital;
    }

    /**
     * @return the Interes
     */
    public String getInteres() {
        return Interes;
    }

    /**
     * @param Interes the Interes to set
     */
    public void setInteres(String Interes) {
        this.Interes = Interes;
    }

    /**
     * @return the IntxMora
     */
    public String getIntxMora() {
        return IntxMora;
    }

    /**
     * @param IntxMora the IntxMora to set
     */
    public void setIntxMora(String IntxMora) {
        this.IntxMora = IntxMora;
    }

    /**
     * @return the GaC
     */
    public String getGaC() {
        return GaC;
    }

    /**
     * @param GaC the GaC to set
     */
    public void setGaC(String GaC) {
        this.GaC = GaC;
    }

    /**
     * @return the Sub_Total
     */
    public String getSub_Total() {
        return Sub_Total;
    }

    /**
     * @param Sub_Total the Sub_Total to set
     */
    public void setSub_Total(String Sub_Total) {
        this.Sub_Total = Sub_Total;
    }

    /**
     * @return the Dcto_capital
     */
    public String getDcto_capital() {
        return Dcto_capital;
    }

    /**
     * @param Dcto_capital the Dcto_capital to set
     */
    public void setDcto_capital(String Dcto_capital) {
        this.Dcto_capital = Dcto_capital;
    }

    /**
     * @return the Dcto_interes
     */
    public String getDcto_interes() {
        return Dcto_interes;
    }

    /**
     * @param Dcto_interes the Dcto_interes to set
     */
    public void setDcto_interes(String Dcto_interes) {
        this.Dcto_interes = Dcto_interes;
    }

    /**
     * @return the Dcto_intxmora
     */
    public String getDcto_intxmora() {
        return Dcto_intxMora;
    }

    /**
     * @param Dcto_intxmora the Dcto_intxmora to set
     */
    public void setDcto_intxmora(String Dcto_intxmora) {
        this.Dcto_intxMora = Dcto_intxmora;
    }

    /**
     * @return the Dcto_gac
     */
    public String getDcto_gac() {
        return Dcto_gac;
    }

    /**
     * @param Dcto_gac the Dcto_gac to set
     */
    public void setDcto_gac(String Dcto_gac) {
        this.Dcto_gac = Dcto_gac;
    }

    /**
     * @return the Total_Dcto
     */
    public String getTotal_Dcto() {
        return Total_Dcto;
    }

    /**
     * @param Total_Dcto the Total_Dcto to set
     */
    public void setTotal_Dcto(String Total_Dcto) {
        this.Total_Dcto = Total_Dcto;
    }

    /**
     * @return the Total_Items
     */
    public String getTotal_Items() {
        return Total_Items;
    }

    /**
     * @param Total_Items the Total_Items to set
     */
    public void setTotal_Items(String Total_Items) {
        this.Total_Items = Total_Items;
    }

    /**
     * @return the id_rop
     */
    public int getId_rop() {
        return id_rop;
    }

    /**
     * @param id_rop the id_rop to set
     */
    public void setId_rop(int id_rop) {
        this.id_rop = id_rop;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the last_update
     */
    public Date getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the creation_date
     */
    public Date getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    @Override
    public String toString() {
        return "resumenSaldosReestructuracion{" + "id=" + id + ", reg_status=" + reg_status + ", dstrct=" + dstrct + ", Negocio=" + Negocio + ", Estado=" + Estado + ", Tipo_neg=" + Tipo_neg + ", Capital=" + Capital + ", Interes=" + Interes + ", IntxMora=" + IntxMora + ", GaC=" + GaC + ", Sub_Total=" + Sub_Total + ", Dcto_capital=" + Dcto_capital + ", Dcto_interes=" + Dcto_interes + ", Dcto_intxmora=" + Dcto_intxMora + ", Dcto_gac=" + Dcto_gac + ", Total_Dcto=" + Total_Dcto + ", Total_Items=" + Total_Items + ", id_rop=" + id_rop + ", user_update=" + user_update + ", last_update=" + last_update + ", creation_date=" + creation_date + ", creation_user=" + creation_user + '}';
    }

    
       

      
}
