/*
 * CSVRead.java
 *
 * Created on 3 de octubre de 2005, 6:52
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;

/**
 *
 * @author  Mario
 */
public class CSVRead extends BufferedReader{
    
    private List  campos  = null;
    private final String separador = ";";
    
    
    /** Creates a new instance of CSVRead */
    public CSVRead(String filename) throws FileNotFoundException{
        super( 
           new BufferedReader( 
           new FileReader    (new File(filename) )));
        
        campos = new LinkedList();
    }
    
    public void run() throws Exception{
        String buffer = readLine();
        if (buffer !=null){
            String [] camp = buffer.split(separador);
            for (int i=0; i < camp.length ; campos.add( camp[i++] ));
        }
    } 
    
    public int getNumeroCampos(){
        return campos.size();
    }
    
    public String getCampo(int indice){
        return (indice >= 0 && indice < campos.size()) ? campos.get(indice).toString() : "";
    }
    
    public String [] getValores () throws Exception{
        String buffer = readLine();
        return (buffer!=null ? procesarBuffer( buffer ).split(separador) : null );
    }
    
    private String procesarBuffer (String buffer){
        String tmp = buffer.replaceAll( "\"\"", "\"");
        tmp = tmp.replaceAll( separador + "\"" , separador );
        tmp = tmp.replaceAll( "\"" + separador , separador );
        return tmp;
        
    }
    
   /* public static void main(String[] args) throws Exception{
        // TODO code application logic here
        
        CSVRead csv = new CSVRead("c:/libro1.csv");
        csv.run();
        
        ////System.out.println("Numero de Campos :" + csv.getNumeroCampos());
        
        for (int i=0; i<csv.getNumeroCampos() ; i++ ){
            ////System.out.println("Campo ["+ i +"] : " + csv.getCampo(i));
        }
        
        String [] datos = null;
        while (  (datos = csv.getValores()) != null ){
            for (int i = 0; i < datos.length ; i++){
                ////System.out.println( datos[i]);
            }
        }
        
        
        csv.close();
        
    }*/
    
}
