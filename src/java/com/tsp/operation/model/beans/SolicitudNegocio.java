package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudNegocio<br/>
 * 20/12/2011
 * @author ivargas-Geotech
 */
public class SolicitudNegocio {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String nombre;
    private String sector;
    private String subsector;
    private String direccion;
    private String ciudad;
    private String departamento;
    private String barrio;
    private String telefono;
    private String tiempoLocal;
    private String numExpNeg;
    private String tiempoMicroempresario;
    private String numTrabajadores;
    private String tieneSocios;
    private String porcentajeParticipacion;
    private String nombreSocio;
    private String cedulaSocio;
    private String direccionSocio;
    private String telefonoSocio;
    private String tipoActividad;
    private String local;
    private String tieneCamaraComercio;
    private String tipoNegocio;
    private String activos;
    private String pasivos;
    
    public SolicitudNegocio load(ResultSet rs) throws SQLException {
        SolicitudNegocio negocio = new SolicitudNegocio();

        negocio.setNumeroSolicitud(rs.getString("numero_solicitud"));
        negocio.setNombre(rs.getString("nombre"));
        negocio.setDireccion(rs.getString("direccion"));
        negocio.setCiudad(rs.getString("ciudad"));
        negocio.setDepartamento(rs.getString("departamento"));
        negocio.setBarrio(rs.getString("barrio"));
        negocio.setTelefono(rs.getString("telefono"));
        negocio.setNumExpNeg(rs.getString("num_exp_negocio"));
        negocio.setSector(rs.getString("cod_sector"));
        negocio.setSubsector(rs.getString("cod_subsector"));
        negocio.setNumTrabajadores(rs.getString("num_trabajadores"));
        negocio.setTiempoLocal(rs.getString("tiempo_local"));
        negocio.setTiempoMicroempresario(rs.getString("tiempo_microempresario"));
        negocio.setTieneSocios(rs.getString("tiene_socio"));
        negocio.setPorcentajeParticipacion(rs.getString("porcentaje_participacion"));
        negocio.setNombreSocio(rs.getString("nombre_socio"));
        negocio.setCedulaSocio(rs.getString("identificacion_socio"));
        negocio.setDireccionSocio(rs.getString("direccion_socio"));
        negocio.setTelefonoSocio(rs.getString("telefono_socio"));
        negocio.setTipoActividad(rs.getString("tipo_actividad"));
        negocio.setLocal(rs.getString("local"));
        negocio.setTieneCamaraComercio(rs.getString("camara_comercio"));
        negocio.setTipoNegocio(rs.getString("tipo_negocio"));
        negocio.setActivos(rs.getString("activos"));
        negocio.setPasivos(rs.getString("pasivos"));
        
        return negocio;
    }

    /**
     * obtiene el valor de telefono
     *
     * @return el valor de telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * cambia el valor de telefono
     *
     * @param telefono nuevo valor telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * obtiene el valor de barrio
     *
     * @return el valor de barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * cambia el valor de barrio
     *
     * @param barrio nuevo valor barrio
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * obtiene el valor de ciudad
     *
     * @return el valor de ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * cambia el valor de ciudad
     *
     * @param ciudad nuevo valor ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * obtiene el valor de departamento
     *
     * @return el valor de departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * cambia el valor de departamento
     *
     * @param departamento nuevo valor departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * obtiene el valor de direccion
     *
     * @return el valor de tdireccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * cambia el valor de direccion
     *
     * @param direccion nuevo valor direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * obtiene el valor de Nombre
     *
     * @return el valor de Nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * cambia el valor de nombre
     *
     * @param razonSocial nuevo valor nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }


    public String getNumExpNeg() {
        return numExpNeg;
    }

    public void setNumExpNeg(String numExpNeg) {
        this.numExpNeg = numExpNeg;
    }

    public String getNumTrabajadores() {
        return numTrabajadores;
    }

    public void setNumTrabajadores(String numTrabajadores) {
        this.numTrabajadores = numTrabajadores;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSubsector() {
        return subsector;
    }

    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

    public String getTiempoLocal() {
        return tiempoLocal;
    }

    public void setTiempoLocal(String tiempoLocal) {
        this.tiempoLocal = tiempoLocal;
    }

    public String getTiempoMicroempresario() {
        return tiempoMicroempresario;
    }

    public void setTiempoMicroempresario(String tiempoMicroempresario) {
        this.tiempoMicroempresario = tiempoMicroempresario;
    }

    public String getTieneSocios() {
        return tieneSocios;
    }

    public void setTieneSocios(String tieneSocios) {
        this.tieneSocios = tieneSocios;
    }

    public String getPorcentajeParticipacion() {
        return porcentajeParticipacion;
    }

    public void setPorcentajeParticipacion(String porcentajeParticipacion) {
        this.porcentajeParticipacion = porcentajeParticipacion;
    }

    public String getNombreSocio() {
        return nombreSocio;
    }

    public void setNombreSocio(String nombreSocio) {
        this.nombreSocio = nombreSocio;
    }

    public String getCedulaSocio() {
        return cedulaSocio;
    }

    public void setCedulaSocio(String cedulaSocio) {
        this.cedulaSocio = cedulaSocio;
    }

    public String getDireccionSocio() {
        return direccionSocio;
    }

    public void setDireccionSocio(String direccionSocio) {
        this.direccionSocio = direccionSocio;
    }

    public String getTelefonoSocio() {
        return telefonoSocio;
    }

    public void setTelefonoSocio(String telefonoSocio) {
        this.telefonoSocio = telefonoSocio;
    }

    public String getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(String tipoActividad) {
        this.tipoActividad = tipoActividad;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getTieneCamaraComercio() {
        return tieneCamaraComercio;
    }

    public void setTieneCamaraComercio(String tieneCamaraComercio) {
        this.tieneCamaraComercio = tieneCamaraComercio;
    }

    public String getTipoNegocio() {
        return tipoNegocio;
    }

    public void setTipoNegocio(String tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

    public String getActivos() {
        return activos;
    }

    public void setActivos(String activos) {
        this.activos = activos;
    }

    public String getPasivos() {
        return pasivos;
    }

    public void setPasivos(String pasivos) {
        this.pasivos = pasivos;
    }


}
