//*************************************** clase modificada 
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class RegistroAsignacion {

    private String distri_rec;
    private String placa_rec;
    private String fecha_disp_rec;
    private String reg_status_rec;
    private String numpla_rec;
    private String origen_rec;
    private String destino_rec;
    private String tipo_recurso_rec;
    private String recurso_rec;
    private String fecha_asig_rec;
    private float tiempo_rec;
    private String tipo_asig_rec;
    private String cod_cliente_rec;
    private String equipo_aso_rec;
    private String tipo_aso_rec;
    private String distri_req;
    private String estado_req;
    private int num_sec_req;
    private String std_job_no_req;
    private String numpla_req;
    private String fecha_disp_req;
    private String cliente_req;
    private String origen_req;
    private String destino_req;
    private String clase_req;
    private String tipo_rec1_req;
    private String tipo_rec2_req;
    private String tipo_rec3_req;
    private String tipo_rec4_req;
    private String tipo_rec5_req;
    private String recurso1_req;
    private String recurso2_req;
    private String recurso3_req;
    private String recurso4_req;
    private String recurso5_req;
    private String prioridad1_req;
    private String prioridad2_req;
    private String prioridad3_req;
    private String prioridad4_req;
    private String prioridad5_req;
    private String tipo_asing_req;
    private String fecha_asing_req;
    private String nuevafecha_dispo_req;
    private String id_rec_cab_req;
    private String id_rec_tra_req;
    private String fecha_posibleentrega_req;
    private String nuevo_origen;
    private String creation_user;
    private String user_update;
    
    private String std_job_desc_req;
    private String std_job_desc_rec;

    public RegistroAsignacion() {
    }

    public String getNumpla_rec() {
        return numpla_rec;
    }

    public String getOrigen_rec() {
        return origen_rec;
    }

    public String getTipo_rec1_req() {
        return tipo_rec1_req;
    }

    public String getFecha_asig_rec() {
        return fecha_asig_rec;
    }

    public String getPrioridad4_req() {
        return prioridad4_req;
    }

    public String getOrigen_req() {
        return origen_req;
    }

    public String getNuevafecha_dispo_req() {
        return nuevafecha_dispo_req;
    }

    public String getTipo_rec3_req() {
        return tipo_rec3_req;
    }

    public String getTipo_aso_rec() {
        return tipo_aso_rec;
    }

    public String getFecha_disp_req() {
        return fecha_disp_req;
    }

    public String getRecurso4_req() {
        return recurso4_req;
    }

    public String getNumpla_req() {
        return numpla_req;
    }

    public String getRecurso5_req() {
        return recurso5_req;
    }

    public String getRecurso_rec() {
        return recurso_rec;
    }

    public String getPrioridad1_req() {
        return prioridad1_req;
    }

    public String getCod_cliente_rec() {
        return cod_cliente_rec;
    }

    public String getTipo_asing_req() {
        return tipo_asing_req;
    }

    public String getPrioridad3_req() {
        return prioridad3_req;
    }

    public String getNuevo_origen() {
        return nuevo_origen;
    }

    public String getPrioridad5_req() {
        return prioridad5_req;
    }

    public String getTipo_rec2_req() {
        return tipo_rec2_req;
    }

    public String getDestino_rec() {
        return destino_rec;
    }

    public String getEstado_req() {
        return estado_req;
    }

    public String getRecurso1_req() {
        return recurso1_req;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public String getTipo_rec4_req() {
        return tipo_rec4_req;
    }

    public String getClase_req() {
        return clase_req;
    }

    public String getTipo_recurso_rec() {
        return tipo_recurso_rec;
    }

    public String getStd_job_no_req() {
        return std_job_no_req;
    }

    public String getDistri_req() {
        return distri_req;
    }

    public String getReg_status_rec() {
        return reg_status_rec;
    }

    public String getRecurso3_req() {
        return recurso3_req;
    }

    public String getDistri_rec() {
        return distri_rec;
    }

    public int getNum_sec_req() {
        return num_sec_req;
    }

    public String getEquipo_aso_rec() {
        return equipo_aso_rec;
    }

    public String getId_rec_tra_req() {
        return id_rec_tra_req==null?"":id_rec_tra_req;
    }

    public String getTipo_rec5_req() {
        return tipo_rec5_req;
    }

    public String getRecurso2_req() {
        return recurso2_req;
    }

    public String getTipo_asig_rec() {
        return tipo_asig_rec;
    }

    public float getTiempo_rec() {
        return tiempo_rec;
    }

    public String getPrioridad2_req() {
        return prioridad2_req;
    }

    public String getFecha_asing_req() {
        return fecha_asing_req;
    }

    public String getFecha_disp_rec() {
        return fecha_disp_rec;
    }

    public String getId_rec_cab_req() {
        return id_rec_cab_req == null?"":id_rec_cab_req;
    }

    public String getPlaca_rec() {
        return placa_rec;
    }

    public String getFecha_posibleentrega_req() {
        return fecha_posibleentrega_req;
    }

    public String getCliente_req() {
        return cliente_req;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setDestino_req( String destino_req ) {
        this.destino_req = destino_req;
    }

    public void setNumpla_rec( String numpla_rec ) {
        this.numpla_rec = numpla_rec;
    }

    public void setOrigen_rec( String origen_rec ) {
        this.origen_rec = origen_rec;
    }

    public void setTipo_rec1_req( String tipo_rec1_req ) {
        this.tipo_rec1_req = tipo_rec1_req;
    }

    public void setFecha_asig_rec( String fecha_asig_rec ) {
        this.fecha_asig_rec = fecha_asig_rec;
    }

    public void setPrioridad4_req( String prioridad4_req ) {
        this.prioridad4_req = prioridad4_req;
    }

    public void setOrigen_req( String origen_req ) {
        this.origen_req = origen_req;
    }

    public void setNuevafecha_dispo_req( String nuevafecha_dispo_req ) {
        this.nuevafecha_dispo_req = nuevafecha_dispo_req;
    }

    public void setTipo_rec3_req( String tipo_rec3_req ) {
        this.tipo_rec3_req = tipo_rec3_req;
    }

    public void setTipo_aso_rec( String tipo_aso_rec ) {
        this.tipo_aso_rec = tipo_aso_rec;
    }

    public void setFecha_disp_req( String fecha_disp_req ) {
        this.fecha_disp_req = fecha_disp_req;
    }

    public void setRecurso4_req( String recurso4_req ) {
        this.recurso4_req = recurso4_req;
    }

    public void setNumpla_req( String numpla_req ) {
        this.numpla_req = numpla_req;
    }

    public void setRecurso5_req( String recurso5_req ) {
        this.recurso5_req = recurso5_req;
    }

    public void setRecurso_rec( String recurso_rec ) {
        this.recurso_rec = recurso_rec;
    }

    public void setPrioridad1_req( String prioridad1_req ) {
        this.prioridad1_req = prioridad1_req;
    }

    public void setCod_cliente_rec( String cod_cliente_rec ) {
        this.cod_cliente_rec = cod_cliente_rec;
    }

    public void setTipo_asing_req( String tipo_asing_req ) {
        this.tipo_asing_req = tipo_asing_req;
    }

    public void setPrioridad3_req( String prioridad3_req ) {
        this.prioridad3_req = prioridad3_req;
    }

    public void setNuevo_origen( String nuevo_origen ) {
        this.nuevo_origen = nuevo_origen;
    }

    public void setPrioridad5_req( String prioridad5_req ) {
        this.prioridad5_req = prioridad5_req;
    }

    public void setTipo_rec2_req( String tipo_rec2_req ) {
        this.tipo_rec2_req = tipo_rec2_req;
    }

    public void setDestino_rec( String destino_rec ) {
        this.destino_rec = destino_rec;
    }

    public void setEstado_req( String estado_req ) {
        this.estado_req = estado_req;
    }

    public void setRecurso1_req( String recurso1_req ) {
        this.recurso1_req = recurso1_req;
    }

    public void setCreation_user( String creation_user ) {
        this.creation_user = creation_user;
    }

    public void setTipo_rec4_req( String tipo_rec4_req ) {
        this.tipo_rec4_req = tipo_rec4_req;
    }

    public void setClase_req( String clase_req ) {
        this.clase_req = clase_req;
    }

    public void setTipo_recurso_rec( String tipo_recurso_rec ) {
        this.tipo_recurso_rec = tipo_recurso_rec;
    }

    public void setStd_job_no_req( String std_job_no_req ) {
        this.std_job_no_req = std_job_no_req;
    }

    public void setDistri_req( String distri_req ) {
        this.distri_req = distri_req;
    }

    public void setReg_status_rec( String reg_status_rec ) {
        this.reg_status_rec = reg_status_rec;
    }

    public void setRecurso3_req( String recurso3_req ) {
        this.recurso3_req = recurso3_req;
    }

    public void setDistri_rec( String distri_rec ) {
        this.distri_rec = distri_rec;
    }

    public void setNum_sec_req( int num_sec_req ) {
        this.num_sec_req = num_sec_req;
    }

    public void setEquipo_aso_rec( String equipo_aso_rec ) {
        this.equipo_aso_rec = equipo_aso_rec;
    }

    public void setId_rec_tra_req( String id_rec_tra_req ) {
        this.id_rec_tra_req = id_rec_tra_req;
    }

    public void setTipo_rec5_req( String tipo_rec5_req ) {
        this.tipo_rec5_req = tipo_rec5_req;
    }

    public void setRecurso2_req( String recurso2_req ) {
        this.recurso2_req = recurso2_req;
    }

    public void setTipo_asig_rec( String tipo_asig_rec ) {
        this.tipo_asig_rec = tipo_asig_rec;
    }

    public void setTiempo_rec( float tiempo_rec ) {
        this.tiempo_rec = tiempo_rec;
    }

    public void setPrioridad2_req( String prioridad2_req ) {
        this.prioridad2_req = prioridad2_req;
    }

    public void setFecha_asing_req( String fecha_asing_req ) {
        this.fecha_asing_req = fecha_asing_req;
    }

    public void setFecha_disp_rec( String fecha_disp_rec ) {
        this.fecha_disp_rec = fecha_disp_rec;
    }

    public void setId_rec_cab_req( String id_rec_cab_req ) {
        this.id_rec_cab_req = id_rec_cab_req;
    }

    public void setPlaca_rec( String placa_rec ) {
        this.placa_rec = placa_rec;
    }

    public void setFecha_posibleentrega_req( String fecha_posibleentrega_req ) {
        this.fecha_posibleentrega_req = fecha_posibleentrega_req;
    }

    public void setCliente_req( String cliente_req ) {
        this.cliente_req = cliente_req;
    }

    public void setUser_update( String user_update ) {
        this.user_update = user_update;
    }

    public String getDestino_req() {
        return destino_req;
    }

    /**
     * Getter for property std_job_desc_req.
     * @return Value of property std_job_desc_req.
     */
    public java.lang.String getStd_job_desc_req() {
        return std_job_desc_req;
    }    

    /**
     * Setter for property std_job_desc_req.
     * @param std_job_desc_req New value of property std_job_desc_req.
     */
    public void setStd_job_desc_req(java.lang.String std_job_desc_req) {
        this.std_job_desc_req = std_job_desc_req;
    }
    
    /**
     * Getter for property std_job_desc_rec.
     * @return Value of property std_job_desc_rec.
     */
    public java.lang.String getStd_job_desc_rec() {
        return std_job_desc_rec;
    }
    
    /**
     * Setter for property std_job_desc_rec.
     * @param std_job_desc_rec New value of property std_job_desc_rec.
     */
    public void setStd_job_desc_rec(java.lang.String std_job_desc_rec) {
        this.std_job_desc_rec = std_job_desc_rec;
    }
    
}
