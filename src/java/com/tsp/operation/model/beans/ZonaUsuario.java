/*
 * Zona.java
 *
 * Created on 13 de juio de 2005, 09:07 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  Henry
 */
public class ZonaUsuario implements Serializable {
    private String codzona;
    private String nomusuario; 
    private String base;
    private String creation_user;
    private String user_update;
    
    /** Creates a new instance of Zona */
    public static ZonaUsuario load(ResultSet rs)throws SQLException {
        ZonaUsuario zonaUsuario = new ZonaUsuario();  
        zonaUsuario.setCodZona( rs.getString("zona"));
        zonaUsuario.setNomUsuario( rs.getString("nomusuario") );
        zonaUsuario.setBase(rs.getString("base"));
        return zonaUsuario;
    }    
   
    //=====================================================
    //              Metodos de acceso 
    //=====================================================
    
    public void setCodZona(String codzona){    
        this.codzona = codzona;       
    }
    public void setNomUsuario(String nomusuario){       
        this.nomusuario = nomusuario;
    }    
    public String getCodZona(){    
        return this.codzona;
    }
    public String getNomUsuario(){        
        return this.nomusuario;
    }        
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}