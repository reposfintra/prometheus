/*
 * Compania.java
 *
 * Created on 20 de enero de 2005, 04:08 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  KREALES
 */
public class Compania {
    
    /** Creates a new instance of Compania */
    
    public String dstrct;
    public String hora;
    public String description;
    public String default_project;
    public String nit;
    public String nombre_aseguradora;
    public String poliza_aseguradora;
    public String fecha_ven_poliza;
    public String codigo_regional;
    public String codigo_empresa;
    public String rango_inicial;
    public String rango_final;
    public String resolucion;
    public String user_update;
    public float peso_lleno_max;
    public float anticipo_max;
    private String moneda;
    
    public float getpeso_lleno_max(){
        return peso_lleno_max;
    }
    public float getanticipo_max(){
        return anticipo_max;
    }
    
    public String getdstrct(){
        return dstrct;
    }
    public String gethora(){
        return hora;
    }
    public String getdescription(){
        return  description;
    }
    public String getdefault_project(){
        return default_project;
    }
    public String getnit(){
        return nit;
    }
    public String getnombre_aseguradora(){
        return nombre_aseguradora;
    }
    public String getpoliza_aseguradora(){
        return poliza_aseguradora;
    }
    public String getfecha_ven_poliza(){
        return fecha_ven_poliza;
    }
    public String getcodigo_regional(){
        return codigo_regional;
    }
    public String getcodigo_empresa(){
        return codigo_empresa;
    }
    public String getresolucion(){
        return resolucion;
    }
    public String getuser_update(){
        return user_update;
    }
    
    public void sethora(String hora){
        this.hora=hora;
    }
    public void setdstrct(String dstrct){
        this.dstrct=dstrct;
    }
    public void setdescription(String description){
        this.description=description;
    }
    public void setdefault_project(String default_project){
        this.default_project=default_project;
    }
    public void setnit(String nit){
        this.nit=nit;
    }
    public void setnombre_aseguradora(String nombre_aseguradora){
        this.nombre_aseguradora=nombre_aseguradora;
    }
    public void setpoliza_aseguradora(String poliza_aseguradora){
        this.poliza_aseguradora=poliza_aseguradora;
    }
    public void setfecha_ven_poliza(String fecha_ven_poliza){
        this.fecha_ven_poliza=fecha_ven_poliza;
    }
    public void setcodigo_regional(String codigo_regional){
        this.codigo_regional = codigo_regional;
    }
    public void setcodigo_empresa(String codigo_empresa){
        this.codigo_empresa= codigo_empresa;
    }
    public void setresolucion(String resolucion){
        this.resolucion=resolucion;
    }
    public void setuser_update(String user_update){
        this.user_update=user_update;
    }
    public void setpeso_lleno_max(float peso_lleno_max){
        this.peso_lleno_max=peso_lleno_max;
    }
    public void setanticipo_max(float anticipo_max){
        this.anticipo_max=anticipo_max;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }    
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }    
    
}
