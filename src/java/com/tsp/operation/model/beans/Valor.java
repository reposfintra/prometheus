package com.tsp.operation.model.beans;

/**
 * Bean para la tabla dc.valor<br/>
 * 31/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Valor {

    private String tipoPadre;
    private String idPadre;
    private Double valorInicial;
    private Double cupo;
    private Double saldoActual;
    private Double saldoMora;
    private Double cuota;
    private Integer cuotasCanceladas;
    private Integer totalCuotas;
    private Integer maximaMora;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private String calidad;//Calidad del deudor: Codeudor, avalista, etc
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of calidad
     *
     * @return the value of calidad
     */
    public String getCalidad() {
        return calidad;
    }

    /**
     * Set the value of calidad
     *
     * @param calidad new value of calidad
     */
    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of maximaMora
     *
     * @return the value of maximaMora
     */
    public Integer getMaximaMora() {
        return maximaMora;
    }

    /**
     * Set the value of maximaMora
     *
     * @param maximaMora new value of maximaMora
     */
    public void setMaximaMora(int maximaMora) {
        this.maximaMora = maximaMora;
    }

    /**
     * Get the value of totalCuotas
     *
     * @return the value of totalCuotas
     */
    public Integer getTotalCuotas() {
        return totalCuotas;
    }

    /**
     * Set the value of totalCuotas
     *
     * @param totalCuotas new value of totalCuotas
     */
    public void setTotalCuotas(int totalCuotas) {
        this.totalCuotas = totalCuotas;
    }

    /**
     * Get the value of cuotasCanceladas
     *
     * @return the value of cuotasCanceladas
     */
    public Integer getCuotasCanceladas() {
        return cuotasCanceladas;
    }

    /**
     * Set the value of cuotasCanceladas
     *
     * @param cuotasCanceladas new value of cuotasCanceladas
     */
    public void setCuotasCanceladas(int cuotasCanceladas) {
        this.cuotasCanceladas = cuotasCanceladas;
    }

    /**
     * Get the value of cuota
     *
     * @return the value of cuota
     */
    public Double getCuota() {
        return cuota;
    }

    /**
     * Set the value of cuota
     *
     * @param cuota new value of cuota
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * Get the value of saldoMora
     *
     * @return the value of saldoMora
     */
    public Double getSaldoMora() {
        return saldoMora;
    }

    /**
     * Set the value of saldoMora
     *
     * @param saldoMora new value of saldoMora
     */
    public void setSaldoMora(double saldoMora) {
        this.saldoMora = saldoMora;
    }

    /**
     * Get the value of saldoActual
     *
     * @return the value of saldoActual
     */
    public Double getSaldoActual() {
        return saldoActual;
    }

    /**
     * Set the value of saldoActual
     *
     * @param saldoActual new value of saldoActual
     */
    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }

    /**
     * Get the value of cupo
     *
     * @return the value of cupo
     */
    public Double getCupo() {
        return cupo;
    }

    /**
     * Set the value of cupo
     *
     * @param cupo new value of cupo
     */
    public void setCupo(double cupo) {
        this.cupo = cupo;
    }

    /**
     * Get the value of valorInicial
     *
     * @return the value of valorInicial
     */
    public Double getValorInicial() {
        return valorInicial;
    }

    /**
     * Set the value of valorInicial
     *
     * @param valorInicial new value of valorInicial
     */
    public void setValorInicial(double valorInicial) {
        this.valorInicial = valorInicial;
    }

    /**
     * Get the value of idPadre
     *
     * @return the value of idPadre
     */
    public String getIdPadre() {
        return idPadre;
    }

    /**
     * Set the value of idPadre
     *
     * @param idPadre new value of idPadre
     */
    public void setIdPadre(String idPadre) {
        this.idPadre = idPadre;
    }

    /**
     * Get the value of tipoPadre
     *
     * @return the value of tipoPadre
     */
    public String getTipoPadre() {
        return tipoPadre;
    }

    /**
     * Set the value of tipoPadre
     *
     * @param tipoPadre new value of tipoPadre
     */
    public void setTipoPadre(String tipoPadre) {
        this.tipoPadre = tipoPadre;
    }


}
