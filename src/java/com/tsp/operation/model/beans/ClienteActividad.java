/**************************************************************************
 * Nombre:        ClienteActividad.java                     
 * Descripci�n:   Beans de acuerdo especial.               *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:         29 de agosto de 2005, 08:14 AM        *
 * Versi�n:       Java  1.0                                      *
 * Copyright:     Fintravalores S.A. S.A.                                *
 **************************************************************************/


package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;


public class ClienteActividad implements Serializable {
    private String dstrct;
    private String CodCliente;
    private String CodActividad;
    private String TipViaje;
    private int secuencia;
    private String estado;
    private String base;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String descortaact;
    private String deslargaact;
    private String cliente;
    private String destipo_viaje;
    private String remesa;
    private String planilla;
    /** Creates a new instance of ClienteActividad */
    public static ClienteActividad load(ResultSet rs)throws SQLException {
        ClienteActividad clientact = new ClienteActividad();
        clientact.setDstrct(rs.getString("dstrct"));
        clientact.setCodCliente(rs.getString("codcli"));
        clientact.setCodActividad(rs.getString("cod_Actividad"));
        clientact.setTipoViaje(rs.getString("tipoviaje"));
        clientact.setSecuencia(rs.getInt("secuencia"));
        clientact.setEstado(rs.getString("estado"));
        clientact.setBase(rs.getString("base"));
        clientact.setCreation_date(rs.getString("creation_date"));
        clientact.setCreation_user(rs.getString("creation_user"));
        clientact.setLast_update(rs.getString("last_update"));
        clientact.setUser_update(rs.getString("user_update"));
        return clientact;
    }
    
     public void setDstrct(String cia){
        this.dstrct=cia;        
    }
    
    public String getDstrct(){
        return dstrct;
    }
    
    public void setCodCliente(String Cod){
        this.CodCliente = Cod;
    }
    public String getCodCliente(){
        return CodCliente;
    }
    public void setTipoViaje(String tip){
        this.TipViaje = tip;
    }
    public String getTipoViaje(){
        return TipViaje;
    }    
    public void setSecuencia(int s){
        this.secuencia=s;        
    }
    public int getSecuencia(){
        return secuencia;
    }
    public void setCodActividad(String cod){
        this.CodActividad = cod;
    }
    
    public String getCodActividad(){
        return CodActividad;
    }
    
    public void setEstado(String e){
        this.estado=e;
    }
    public String getEstado(){
        return estado;
    }
    public void setBase(String base){
        this.base = base;
    }
    public String getBase(){
        return base;
    }
    
    public void setCreation_user(String cu){
        this.creation_user = cu;
    }
    public String getCreation_user(){
        return creation_user;
    }
    public void setCreation_date(String fec){ 
        this.creation_date = fec;
    }
    public String getCreation_date(){
        return creation_date;
    }
    public void setLast_update(String fec){ 
        this.last_update = fec;
    }
    public String getLast_update(){
        return last_update;
    }
    public void setUser_update(String us){
        this.user_update = us;
    }
    public String getUser_update(){
        return user_update;
    }
    public void setDescortaAct(String des){
        this.descortaact = des;        
    }
    public String getDescortaAct(){
        return descortaact;
    }
    public void setLargaAct(String des){
        this.deslargaact = des;        
    }
    public String getLargaAct(){
        return deslargaact;
    }
    public void setNomCliente(String nom){
        this.cliente = nom;        
    }
    public String getNomCliente(){
        return cliente;
    }
    
    /**
     * Getter for property destipo_viaje.
     * @return Value of property destipo_viaje.
     */
    public java.lang.String getDestipo_viaje() {
        return destipo_viaje;
    }
    
    /**
     * Setter for property destipo_viaje.
     * @param destipo_viaje New value of property destipo_viaje.
     */
    public void setDestipo_viaje(java.lang.String destipo_viaje) {
        this.destipo_viaje = destipo_viaje;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
}
