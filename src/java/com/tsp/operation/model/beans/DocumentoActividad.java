/********************************************************************
 *      Nombre Clase.................   DocumentoActividad.java    
 *      Descripci�n..................   Bean de la tabla tblact_doc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   19.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.Util;
import java.sql.*;

public class DocumentoActividad {
        private String documento;
        private String actividad;
        
        private String usuario_creacion;
        private String fecha_creacion;
        private String usuario_modificacion;
        private String ultima_modificacion;
        private String distrito;
        private String estado;
        private String base;
        
        
        /** Creates a new instance of DocumentoActividad */
        public DocumentoActividad() {
        }
        
        public void Load(ResultSet rs) throws SQLException{
                this.documento = rs.getString("documento");
                this.actividad = rs.getString("actividad");
                
                this.estado = rs.getString("reg_status");                
                this.distrito = rs.getString("dstrct"); 
                this.base = rs.getString("base");
                this.fecha_creacion = rs.getString("creation_date");
                this.usuario_creacion = rs.getString("creation_user");
                this.ultima_modificacion = rs.getString("last_update");
                this.usuario_modificacion = rs.getString("user_update");
        }
        
        /**
         * Getter for property actividad.
         * @return Value of property actividad.
         */
        public java.lang.String getActividad() {
                return actividad;
        }
        
        /**
         * Setter for property actividad.
         * @param actividad New value of property actividad.
         */
        public void setActividad(java.lang.String actividad) {
                this.actividad = actividad;
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }
        
        /**
         * Getter for property distrito.
         * @return Value of property distrito.
         */
        public java.lang.String getDistrito() {
                return distrito;
        }
        
        /**
         * Setter for property distrito.
         * @param distrito New value of property distrito.
         */
        public void setDistrito(java.lang.String distrito) {
                this.distrito = distrito;
        }
        
        /**
         * Getter for property documento.
         * @return Value of property documento.
         */
        public java.lang.String getDocumento() {
                return documento;
        }
        
        /**
         * Setter for property documento.
         * @param documento New value of property documento.
         */
        public void setDocumento(java.lang.String documento) {
                this.documento = documento;
        }
        
        /**
         * Getter for property estado.
         * @return Value of property estado.
         */
        public java.lang.String getEstado() {
                return estado;
        }
        
        /**
         * Setter for property estado.
         * @param estado New value of property estado.
         */
        public void setEstado(java.lang.String estado) {
                this.estado = estado;
        }
        
        /**
         * Getter for property fecha_creacion.
         * @return Value of property fecha_creacion.
         */
        public java.lang.String getFecha_creacion() {
                return fecha_creacion;
        }
        
        /**
         * Setter for property fecha_creacion.
         * @param fecha_creacion New value of property fecha_creacion.
         */
        public void setFecha_creacion(java.lang.String fecha_creacion) {
                this.fecha_creacion = fecha_creacion;
        }
        
        /**
         * Getter for property ultima_modificacion.
         * @return Value of property ultima_modificacion.
         */
        public java.lang.String getUltima_modificacion() {
                return ultima_modificacion;
        }
        
        /**
         * Setter for property ultima_modificacion.
         * @param ultima_modificacion New value of property ultima_modificacion.
         */
        public void setUltima_modificacion(java.lang.String ultima_modificacion) {
                this.ultima_modificacion = ultima_modificacion;
        }
        
        /**
         * Getter for property usuario_creacion.
         * @return Value of property usuario_creacion.
         */
        public java.lang.String getUsuario_creacion() {
                return usuario_creacion;
        }
        
        /**
         * Setter for property usuario_creacion.
         * @param usuario_creacion New value of property usuario_creacion.
         */
        public void setUsuario_creacion(java.lang.String usuario_creacion) {
                this.usuario_creacion = usuario_creacion;
        }
        
        /**
         * Getter for property usuario_modificacion.
         * @return Value of property usuario_modificacion.
         */
        public java.lang.String getUsuario_modificacion() {
                return usuario_modificacion;
        }
        
        /**
         * Setter for property usuario_modificacion.
         * @param usuario_modificacion New value of property usuario_modificacion.
         */
        public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
                this.usuario_modificacion = usuario_modificacion;
        }
        
}
