package com.tsp.operation.model.beans;

/**
 * Bean para la tabla inversionista
 * 25/02/2012
 * @author darrieta
 */
public class Inversionista {

    private String nit;
    private int subcuenta;
    private String nombre_subcuenta;
    private String dstrct;
    private String creation_user;
    private String user_update;
    private double tasa;
    private int ano_base;
    private String rendimiento;
    private String tipo_interes;
    private String retefuente;
    private String reteica;
    private double tasa_nominal;
    private double tasa_diaria;
    private double tasa_ea;
    private int dias_calendario;
    private String nombre_nit;
    private String codcliente;
    private String tipo_inversionista;
    private String pago_automatico;
    private String periodicidad_pago;
     private String fecha_autorizacion_pagos;
     private String genera_documentos;
     private String computa;

    public String getComputa() {
        return computa;
    }

    public void setComputa(String computa) {
        this.computa = computa;
    }



   private int  Ano_base_tasa_diaria;

    public int getAno_base_tasa_diaria() {
        return Ano_base_tasa_diaria;
    }

    public void setAno_base_tasa_diaria(int Ano_base_tasa_diaria) {
        this.Ano_base_tasa_diaria = Ano_base_tasa_diaria;
    }



    public String getGenera_documentos() {
        return genera_documentos;
    }

    public void setGenera_documentos(String genera_documentos) {
        this.genera_documentos = genera_documentos;
    }




     public String getFecha_autorizacion_pagos() {
        return fecha_autorizacion_pagos;
    }

    public void setFecha_autorizacion_pagos(String fecha_autorizacion_pagos) {
        this.fecha_autorizacion_pagos = fecha_autorizacion_pagos;
    }

    public String getPago_automatico() {
        return pago_automatico;
    }

    public void setPago_automatico(String pago_automatico) {
        this.pago_automatico = pago_automatico;
    }

    public String getPeriodicidad_pago() {
        return periodicidad_pago;
    }

    public void setPeriodicidad_pago(String periodicidad_pago) {
        this.periodicidad_pago = periodicidad_pago;
    }


    private double saldo_total;


    private String nit_parentesco;
    private String tipo_parentesco;

    public String getNit_parentesco() {
        return nit_parentesco;
    }

    public void setNit_parentesco(String nit_parentesco) {
        this.nit_parentesco = nit_parentesco;
    }

    public String getTipo_parentesco() {
        return tipo_parentesco;
    }

    public void setTipo_parentesco(String tipo_parentesco) {
        this.tipo_parentesco = tipo_parentesco;
    }


        public String getTipo_inversionista() {
        return tipo_inversionista;
    }

    public void setTipo_inversionista(String tipo_inversionista) {
        this.tipo_inversionista = tipo_inversionista;
    }
    public double getTasa_ea() {
        return tasa_ea;
    }

    public void setTasa_ea(double tasa_ea) {
        this.tasa_ea = tasa_ea;
    }
    public double getTasa_diaria() {
        return tasa_diaria;
    }

    public void setTasa_diaria(double tasa_diaria) {
        this.tasa_diaria = tasa_diaria;
    }

    public double getTasa_nominal() {
        return tasa_nominal;
    }

    public void setTasa_nominal(double tasa_nominal) {
        this.tasa_nominal = tasa_nominal;
    }



        public String getCodcliente() {
        return codcliente;
    }

    public void setCodcliente(String codcliente) {
        this.codcliente = codcliente;
    }

    /**
     * Get the value of reteica
     *
     * @return the value of reteica
     */
    public String getReteica() {
        return reteica;
    }

    /**
     * Set the value of reteica
     *
     * @param reteica new value of reteica
     */
    public void setReteica(String reteica) {
        this.reteica = reteica;
    }

    /**
     * Get the value of retefuente
     *
     * @return the value of retefuente
     */
    public String getRetefuente() {
        return retefuente;
    }

    /**
     * Set the value of retefuente
     *
     * @param retefuente new value of retefuente
     */
    public void setRetefuente(String retefuente) {
        this.retefuente = retefuente;
    }

    /**
     * Get the value of tipo_interes
     *
     * @return the value of tipo_interes
     */
    public String getTipo_interes() {
        return tipo_interes;
    }

    /**
     * Set the value of tipo_interes
     *
     * @param tipo_interes new value of tipo_interes
     */
    public void setTipo_interes(String tipo_interes) {
        this.tipo_interes = tipo_interes;
    }

    /**
     * Get the value of rendimiento
     *
     * @return the value of rendimiento
     */
    public String getRendimiento() {
        return rendimiento;
    }

    /**
     * Set the value of rendimiento
     *
     * @param rendimiento new value of rendimiento
     */
    public void setRendimiento(String rendimiento) {
        this.rendimiento = rendimiento;
    }

    /**
     * Get the value of ano_base
     *
     * @return the value of ano_base
     */
    public int getAno_base() {
        return ano_base;
    }

    /**
     * Set the value of ano_base
     *
     * @param ano_base new value of ano_base
     */
    public void setAno_base(int ano_base) {
        this.ano_base = ano_base;
    }

    /**
     * Get the value of tasa
     *
     * @return the value of tasa
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * Set the value of tasa
     *
     * @param tasa new value of tasa
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }


    /**
     * Get the value of user_update
     *
     * @return the value of user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * Set the value of user_update
     *
     * @param user_update new value of user_update
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * Get the value of creation_user
     *
     * @return the value of creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * Set the value of creation_user
     *
     * @param creation_user new value of creation_user
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * Get the value of dstrct
     *
     * @return the value of dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * Set the value of dstrct
     *
     * @param dstrct new value of dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * Get the value of nombre_subcuenta
     *
     * @return the value of nombre_subcuenta
     */
    public String getNombre_subcuenta() {
        return nombre_subcuenta;
    }

    /**
     * Set the value of nombre_subcuenta
     *
     * @param nombre_subcuenta new value of nombre_subcuenta
     */
    public void setNombre_subcuenta(String nombre_subcuenta) {
        this.nombre_subcuenta = nombre_subcuenta;
    }

    /**
     * Get the value of subcuenta
     *
     * @return the value of subcuenta
     */
    public int getSubcuenta() {
        return subcuenta;
    }

    /**
     * Set the value of subcuenta
     *
     * @param subcuenta new value of subcuenta
     */
    public void setSubcuenta(int subcuenta) {
        this.subcuenta = subcuenta;
    }

    /**
     * Get the value of nit
     *
     * @return the value of nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * Set the value of nit
     *
     * @param nit new value of nit
     */
    public void setNit(String nit) {
        this.nit = nit;
    }


        public String getNombre_nit() {
        return nombre_nit;
    }

    public void setNombre_nit(String nombre_nit) {
        this.nombre_nit = nombre_nit;
    }


        public int getDias_calendario() {
        return dias_calendario;
    }

    public void setDias_calendario(int dias_calendario) {
        this.dias_calendario = dias_calendario;
    }

        public double getSaldo_total() {
        return saldo_total;
    }

    public void setSaldo_total(double saldo_total) {
        this.saldo_total = saldo_total;
    }


}
