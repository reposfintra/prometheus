/********************************************************************
 *      Nombre Clase.................   Banco.java    
 *      Descripci�n..................   Bean del archivo banco
 *      Autor........................   
 *      Fecha........................   1.12.2004
 *      Versi�n......................   
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;
import java.sql.*;
/**
 * Crea una nueva instancia de la clase Banco
 * @author mcelin
 */
public class Banco {
    
    private String Banco;
    private String Codigo_Agencia;
    private String Nombre_Agencia;
    private String descripcion;
    //--- adicionado por fernel------------------------------
    private int    id;
    private String cuenta;
    private String prefix;
    private int    serie_initial;
    private int    serie_finish;
    private int    last_number;
  //ALEJANDRO
    private String codigo_cuenta;
    private String bank_account_no;
    private String dstrct;
    private boolean esDeUsuario = false;
    //Jose
    private String moneda;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String distrito;
    private String base;
    //Tito Andr�s
    private String posbancaria;

     
    /**
     * Setea la propiedad esDeUsuario
     * @param valor Nuevo valor de la propiedad esDeUsuario
     */    
    public void setEsDeUsuario(boolean valor){
        this.esDeUsuario = valor;
    }
    
    

    /**
     * Setea la propiedad Id
     * @param id Nuevo valor de la propiedad id
     */    
    public void setId(int id){
        this.id = id;
    }
    
    /**
     * Obtiene el valor de la propiead id
     * @return Valor de la propiedad id
     */    
    public int getId(){
        return this.id;
    }
    
    /**
     * Setea el valor dela propiedad cuenta
     * @param cuenta Valor de la propiedad cuenta
     */    
    public void setCuenta(String cuenta){
        this.cuenta = cuenta;
    }
    
    /**
     * Setea el valor dela propiedad prefix
     * @param prefix Valor de la propiedad prefix
     */    
    public void setPrefix(String prefix){
        this.prefix = prefix;
    }
    
    /**
     * Setea el valor de la propiedad initial
     * @param initial Valor de la propiedad initial
     */    
    public void setSerieInitial(int initial){
        this.serie_initial = initial;
    }
    
    /**
     * Setea el valor de la propieda serie_finish
     * @param finish Valor de la propiedad serie_finish
     */    
    public void setSerieFinish(int finish){
        this.serie_finish = finish;
    }
    
    /**
     * Setea la propiedad last_number
     * @param ultimo Valor de la propiedad last_number
     */    
    public void setLastNumber(int ultimo){
        this.last_number = ultimo;
    }
    
    
    /**
     * Obtiene el valor de la propiedad cuenta
     * @return valor de la propiedad cuenta
     */    
    public String getCuenta(){
        return this.cuenta;
    }
    
    /**
     * Obtiene el valor de la propieda prefix
     * @return Valor de la propieda prefix
     */    
    public String getPrefix(){
        return this.prefix;
    }
    
    /**
     * Obtiene el valor de la prpiedad serie_initial
     * @return Valor de la prpiedad serie_initial
     */    
    public int getSerieInitial(){
        return this.serie_initial;
    }
    
    /**
     * Obtiene el valor de la propiedad serie_finish
     * @return Valor de la propiedad serie_finish
     */    
    public int getSerieFinish(){
        return this.serie_finish;
    }
    
    /**
     * Obtiene el valor de la propiedad last_number
     * @return Valor de la propiedad last_number
     */    
    public int getLastNumber(){
        return this.last_number;
    }
     
   
    /**
     *
     * @throws Exception
     * @return
     */    
    public String getString() throws Exception{
      String cadena="";
      try{  
        String space="&nbsp"; //-- espacio en HTML
        int spaceBanco     = 15;
        int spaceSucursal  = 15;
        int spaceCuenta    = 15;
        int spaceCheque    = 10;        
        
        cadena+=this.getBanco();
        int longBanco = this.getBanco().length();        
        if(longBanco<spaceBanco)
           for(int i=0;i<=(spaceBanco-longBanco);i++)
               cadena+=space;
        
        cadena+=this.getNombre_Agencia();
        int longAgency = this.getNombre_Agencia().length();        
        if(longAgency<spaceSucursal)
           for(int i=0;i<=(spaceSucursal-longAgency);i++)
               cadena+=space;
        
        cadena+=this.getCuenta();
        int longCuenta = this.getCuenta().length();        
        if(longCuenta<spaceCuenta)
           for(int i=0;i<=(spaceCuenta-longCuenta);i++)
               cadena+=space;   
        
        cadena+=String.valueOf( this.getPrefix() + this.getLastNumber());
        int longCheque = this.getPrefix().length() + String.valueOf(this.getLastNumber()).length();        
        if(longCheque<spaceCheque)
           for(int i=0;i<=(spaceCheque-longCheque);i++)
               cadena+=space;   
      }
      catch(Exception e){ throw new Exception(e.getMessage());}
      return cadena;
   }
    //----------------------------------------------------------
    
    
    
    
    /**
     * Setea las propiedades a partir de una instancia de la clase <CODE>ResultSet</CODE>
     * @param rs Instancia de la clase <CODE>ResultSet</CODE> de donde se cargan los campos
     * @throws SQLException Si presenta un error en la conexi�n con la Base de Datos
     *
     * @return Una instancia de la clase <CODE>Banco</CODE>
     */
    public Banco load(ResultSet rs) throws SQLException {
        Banco datos = new Banco();
        datos.setBanco(rs.getString(1));
        datos.setCodigo_Agencia(rs.getString(2));
        datos.setNombre_Agencia(rs.getString(3));
        return datos;
    }
    
    //Setter
    /**
     * Setea la propiedad Banco
     * @param valor Valor de la propiedad Banco
     */    
    public void setBanco(String valor) {
        this.Banco = valor;
    }
    
    /**
     * Setea la propiedad codigo_agencia
     * @param valor Valor de la propiedad codigo_agencia
     */    
    public void setCodigo_Agencia(String valor) {
        this.Codigo_Agencia = valor;
    }
    
    /**
     * Setea la propiedad nombre_agencia
     * @param valor Valor de la propiedad nombre_agencia
     */    
    public void setNombre_Agencia(String valor) {
        this.Nombre_Agencia = valor;
    }
    //Getter
    /**
     * Obtiene el valor de la propiedad Banco
     * @return Valor de la propiedad Banco
     */    
    public String getBanco() {
        return this.Banco;
    }
    
    /**
     * Obtiene el valor de la propiedad codigo_agencia
     * @return Valor de la propiedad codigo_agencia
     */    
    public String getCodigo_Agencia() {
        return this.Codigo_Agencia;
    }
    
    /**
     * Obtiene el valor de la propiedad nombre_agencia
     * @return Valor de la propiedad nombre_agencia
     */    
    public String getNombre_Agencia() {
        return this.Nombre_Agencia;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
//ALEJANDRO
    /**
     * Setea la propiedad codigo_cuenta
     * @param codigo_cuenta Valor de la propiedad  codigo_cuenta
     */    
    public void setCodigo_cuenta( String codigo_cuenta ) {
        this.codigo_cuenta = codigo_cuenta;
    }

    /**
     * Obtiene el valor de la propiedad codigo_cuenta
     * @return Valor de la propiedad codigo_cuenta
     */    
    public String getCodigo_cuenta() {
        return codigo_cuenta;
    }
    
    /**
     * Setea la propiedad bank_account_no
     * @param valor Valor de la propiedad bank_account_no
     */    
    public void setBank_account_no(String valor){
        this.bank_account_no = valor;
    }
    
    /**
     * Obtiene el valor de la propiedad bank_account_no
     * @return Valor de la propiedad bank_account_no 
     */    
    public String getBank_account_no(){
        return this.bank_account_no;
    }
    
    /**
     * Setea la propiedad dstrct
     * @param dstrct Valor de la propiedad dstrct
     */    
    public void setDstrct(String dstrct){
        this.dstrct = dstrct;
    }
    
    /**
     * Obtiene el valor de la propiedad dstrct
     * @return Valor de la propiedad dstrct
     */    
    public String getDstrct(){
        return this.dstrct;
    }

    /**
     * Getter for property esDeUsuario.
     * @return Value of property esDeUsuario.
     */
    public boolean esDeUsuario() {
        return esDeUsuario;
    }
    
    
    /**
     * Establece si es igual a otro objeto
     * @param otro Objeto a comparar
     * @return true si s�, false si no
     */    
    public boolean equals(Object otro){
        if ( otro instanceof Banco){
            Banco b = (Banco) otro;
            return b.getBanco().equals(this.getBanco()) && b.getBank_account_no().equals(this.getBank_account_no());
        }
        else if ( otro instanceof String ){
            return (this.getBanco()+"/"+this.getBank_account_no()).equals(otro);
        }
        return false;
    }
    
    /**
     * Retorna una cadena de caracteres con el nombre y la sucursal del banco.
     * @return  Una cadena de caracteres con el nombre y la sucursal del banco.
     */    
    public String toString(){
        return this.Banco + ", "+this.getBank_account_no();
    }

    /**
     * Obtiene la propiedad base
     * @return Valor de la propiedad base
     */    
    public java.lang.String getBase() {
        return base;
    }

    /**
     * Establece el valor de la propiedad base
     * @param base Valor de la propiedad base
     */    
    public void setBase(java.lang.String base) {
        this.base = base;
    }

    /**
     * Establece el valor de la propiedad fecha_creacion
     * @return Valor de la propiedad fecha_creacion 
     */    
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * Establece el valor de la propiedad fecha_creacion
     * @param fecha_creacion Valor de la  propiedad fecha_creacion
     */    
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * Obtiene el valor de la propiedad ultima_modificacion
     * @return Valor de la propiedad ultima_modificacion
     */    
    public java.lang.String getUltima_modificacion() {
        return ultima_modificacion;
    }

    /**
     * Establece el valor de la propiedad ultima_modificacion 
     * @param ultima_modificacion Valor de la propiedad ultima_modificacion
     */    
    public void setUltima_modificacion(java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    /**
     * Obtiene el valor de la propiedad usuario_creacion
     * @return Valor de la propiedad usuario_creacion
     */    
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }

    /**
     * Establece el valor de la propiedad usuario_creacion
     * @param usuario_creacion Valor de la propiedad usuario_creacion
     */    
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    /**
     * Obtiene el valor de la propiedad usuario_modificacion
     * @return Valor de la propiedad usuario_modificacion
     */    
    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }

    /**
     * Establece el valor de la propiedad usuario_modificacion
     * @param usuario_modificacion Valor de la propiedad usuario_modificacion
     */    
    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }

    /**
     * Setea el valor de la propiedad distrito
     * @return Valor de la propiedad distrito
     */    
    public java.lang.String getDistrito() {
        return distrito;
    }

    /**
     * Establece el valor de la propiedad distrito
     * @param distrito Valor de la propiedad distrito
     */    
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }

    /**
     * Obtiene el valor de la propieda moneda
     * @return Valor de la propieda moneda
     */    
    public java.lang.String getMoneda() {
        return moneda;
    }

    /**
     * Establece el valor de la propiedad moneda
     * @param moneda Valor de la propiedad moneda
     */    
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Seta las propiedades banco y bank_account_no a partir de una instancia de <code>ResultSet</code>
     * @param rs Instancia de <code>ResultSet</code>
     * @throws Exception si ocurre una excepci�n
     * @return Instancia de <code>Banco</code>
     */    
    public static Banco loadReporteEgreso(ResultSet rs)throws Exception{
        Banco datos = new Banco();
        datos.setBanco(rs.getString("branch_code"));
        datos.setBank_account_no(rs.getString("bank_account_no"));
        return datos;
    }
    
    /**
     * Getter for property posbancaria.
     * @return Value of property posbancaria.
     */
    public java.lang.String getPosbancaria() {
        return posbancaria;
    }
    
    /**
     * Setter for property posbancaria.
     * @param posbancaria New value of property posbancaria.
     */
    public void setPosbancaria(java.lang.String posbancaria) {
        this.posbancaria = posbancaria;
    }
    
}
