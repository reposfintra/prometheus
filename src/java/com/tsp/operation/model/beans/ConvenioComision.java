/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;

/**
 *
 * @author maltamiranda
 */
public class ConvenioComision implements Serializable{

    String reg_status, dstrct, id_convenio, id_comision, nombre, cuenta_comision,
           creation_user, user_update, creation_date,
           last_update;
    double porcentaje_comision;
    private boolean comision_tercero;
    private boolean indicador_contra;
    private String cuenta_contra;
    private double valor_comision;


     public double getValor_comision() {
        return valor_comision;
    }

    public void setValor_comision(double valor_comision) {
        this.valor_comision = valor_comision;
    }

    /**
     * Get the value of cuenta_contra
     *
     * @return the value of cuenta_contra
     */
    public String getCuenta_contra() {
        return cuenta_contra;
    }

    /**
     * Set the value of cuenta_contra
     *
     * @param cuenta_contra new value of cuenta_contra
     */
    public void setCuenta_contra(String cuenta_contra) {
        this.cuenta_contra = cuenta_contra;
    }

    /**
     * Get the value of indicador_contra
     *
     * @return the value of indicador_contra
     */
    public boolean isIndicador_contra() {
        return indicador_contra;
    }

    /**
     * Set the value of indicador_contra
     *
     * @param indicador_contra new value of indicador_contra
     */
    public void setIndicador_contra(boolean indicador_contra) {
        this.indicador_contra = indicador_contra;
    }

    /**
     * Get the value of comision_tercero
     *
     * @return the value of comision_tercero
     */
    public boolean isComision_tercero() {
        return comision_tercero;
    }

    /**
     * Set the value of comision_tercero
     *
     * @param comision_tercero new value of comision_tercero
     */
    public void setComision_tercero(boolean comision_tercero) {
        this.comision_tercero = comision_tercero;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getCuenta_comision() {
        return cuenta_comision;
    }

    public void setCuenta_comision(String cuenta_comision) {
        this.cuenta_comision = cuenta_comision;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getId_comision() {
        return id_comision;
    }

    public void setId_comision(String id_comision) {
        this.id_comision = id_comision;
    }

    public String getId_convenio() {
        return id_convenio;
    }

    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPorcentaje_comision() {
        return porcentaje_comision;
    }

    public void setPorcentaje_comision(double porcentaje_comision) {
        this.porcentaje_comision = porcentaje_comision;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }


}