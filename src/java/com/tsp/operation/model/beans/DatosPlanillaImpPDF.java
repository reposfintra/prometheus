/*************************************************************************************
 * Nombre clase :                   DatosImpresionPlanillaPDF.java                   *
 * Descripcion :                    Clase que maneja los atributos relacionados con  *
 *                                  la impresion de Planillas                        *
 * Autor :                          Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                          16 de diciembre de 2005, 14:45                   *
 * Version :                        1.0                                              *
 * Copyright :                      Fintravalores S.A.                          *
 ************************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;


public class DatosPlanillaImpPDF implements Serializable{
    
    // datos basicos de la planilla
    private String NumeroPlanilla;
    private String FechaPlanilla;
    private String CodOrigen;
    private String DesOrigen;
    private String CodDestino;
    private String DesDestino;
    private String FechaImpresion;
    private String Ruta;
    
    //datos basicos del vehiculo
    private String Placa;
    private String Marca;
    private String Modelo;
    private String Linea;
    private String ModeloRepotenciado;
    private String Serial;
    private String Color;
    private String DimensionCarroceria;
    private String Carroceria;
    private String NumeroEjes;  //configuracion
    private String RegistroNacionalCarga;
    private String PesoVacio;
    private String NumeroPolizaSoat;
    private String Compa�iaSoat;
    private String VecimientoSoat;
    private String PlacaTrailer;
    
    
    //datos propietario del vehiculo
    private String CedulaPropietario;
    private String NombrePropietario;
    private String DireccionPropietario;
    private String TelefonoPropietario;
    private String CiudadPropietario;
    
    //datos tenedor del vehiculo
    private String CedulaTenedor;
    private String NombreTenedor;
    private String DireccionTenedor;
    private String TelefonoTenedor;
    private String CiudadTenedor;
    
    //datos conductor del vehiculo
    private String CedulaConductor;
    private String NombreConductor;
    private String DireccionConductor;
    private String TelefonoConductor;
    private String CiudadConductor;
    //CAT LIC CONDUCCION 
    private String CatLicConductor;
    
    //datos de movimiento
    private double ValorAnticipo;
    private double ValorAnticipo2;
    private String Moneda;
    private String TotalFlete;
    
    private String Despachador;
    //datos remesas
    private List   Remesas;
    
    //datos Compa�ia
    private String CodigoRegional;
    private String CodigoEmpresa;
    private String CodigoNumConsec;
    private String RangoInicial;
    private String RangoFinal;
    private String Resolucion;
    
    //datos Seguros
    private String CiaSeguros;
    private String NumPoliza;
    private String VigenciaPoliza;
    
    //Pago de Saldo
    private String LugarPagoSaldo;
    private String FechaPagoSaldo;
    private String CarguePagadoPor;
    private String DescarguePagadoPor;
    
    //observaciones TextoOC
    private String Observaciones;
    
    //Documento Internos y Contenedores
    private String Contenedores;
    private String DocInternos;
    
    private Vector detalle;
    private Vector copias;
    
    private String leyenda;
    
    //JEscandon 17.01.06
    private String reg_status;
    //JEscandon 26-01-06
    private String soportes;
   private String fechacargue;
    //JEscandon 06-04-06
   private String precinto;

   private String plaviaje;
    
    /** Creates a new instance of DatosImpresionPlanilla */
    public DatosPlanillaImpPDF(){
        
    }    
    
    public static DatosPlanillaImpPDF load(ResultSet rs)throws Exception{
        DatosPlanillaImpPDF datos = new DatosPlanillaImpPDF();
            
        datos.setNumeroPlanilla      (rs.getString(1 ));
        datos.setFechaPlanilla       (rs.getString(2 ));
        datos.setCodOrigen           (rs.getString(3 ));
        datos.setDesOrigen           (rs.getString(4 ));
        datos.setCodDestino          (rs.getString(5 ));
        datos.setDesDestino          (rs.getString(6 ));
        datos.setRuta                (rs.getString(7 ));
                
        datos.setPlaca               (rs.getString(8 ));
        datos.setMarca               (rs.getString(9 ));
        datos.setModelo              (rs.getString(10 ));
        datos.setSerial              (rs.getString(11));
        datos.setColor               (rs.getString(12));
        datos.setDimensionCarroceria (rs.getString(13));
        datos.setNumeroEjes          (rs.getString(14));
        datos.setPesoVacio           (rs.getString(15));
                
        datos.setCedulaPropietario   (rs.getString(16));
        datos.setNombrePropietario   (rs.getString("nomprop"));
        datos.setDireccionPropietario(rs.getString(18));
        datos.setTelefonoPropietario (rs.getString(19));
        datos.setCiudadPropietario   (rs.getString(20));
        
        datos.setCedulaTenedor       (rs.getString(21));
        datos.setNombreTenedor       (rs.getString(22));
        datos.setDireccionTenedor    (rs.getString(23));
        datos.setTelefonoTenedor     (rs.getString(24));
        datos.setCiudadTenedor       (rs.getString(25));

        datos.setCedulaConductor     (rs.getString(26));
        datos.setNombreConductor     (rs.getString(27));
        datos.setDireccionConductor  (rs.getString(28));
        datos.setTelefonoConductor   (rs.getString(29));
        datos.setCiudadConductor     (rs.getString(30));
        
        datos.setFechaImpresion      (rs.getString(31));
        datos.setValorAnticipo       (rs.getDouble(32));
        datos.setValorAnticipo2      (rs.getDouble(33));
        datos.setMoneda              (rs.getString(34));
        datos.setDespachador         (rs.getString(35));
        
        
        return datos;
        
    }
    
    
    /* Funcion que Carga los Datos de CIA y del SEGURO en el Objeto Datos Planilla Impresion */
    public static void loadDatosCIA( ResultSet rs, DatosPlanillaImpPDF datos )throws Exception{
        //Datos SEGURO
        datos.setCiaSeguros(rs.getString("NOMBRE_ASEGURADORA"));
        datos.setNumPoliza(rs.getString("POLIZA_ASEGURADORA"));
        datos.setVigenciaPoliza(rs.getString("FECHA_VEN_POLIZA"));  
        //Datos CIA
        datos.setCodigoRegional(rs.getString("CODIGO_REGIONAL"));
        datos.setCodigoEmpresa(rs.getString("CODIGO_EMPRESA"));
        datos.setRangoInicial(rs.getString("RANGO_INICIAL"));
        datos.setRangoFinal(rs.getString("RANGO_FINAL"));
        datos.setResolucion(rs.getString("RESOLUCION"));
    }
    
   /* Funcion que Carga los Datos de  Planilla  y del Vehiculo en el Objeto Datos Planilla Impresion */
    
    public static void loadDatosPlanilla( ResultSet rs, DatosPlanillaImpPDF datos )throws Exception{
        datos.setNumeroPlanilla(rs.getString("NUMPLA"));
        datos.setFechaPlanilla(rs.getString("FECDSP"));
        datos.setDesOrigen(rs.getString("ORIGEN"));
        datos.setDesDestino(rs.getString("DESTINO"));
        //DATOS VEHICULO
        datos.setPlaca(rs.getString("PLAVEH"));
        datos.setMarca(rs.getString("MARCA"));
        datos.setModelo(rs.getString("MODELO"));
        datos.setSerial(rs.getString("SERIAL"));
        datos.setColor(rs.getString("COLOR"));
        datos.setCarroceria(rs.getString("CARROCERIA"));
        datos.setVecimientoSoat(rs.getString("VENSOAT"));
        datos.setPlacaTrailer(rs.getString("PLATLR"));
        datos.setPesoVacio(rs.getString("PESOVACIO"));
        //DATOS PROPIETARIO
        datos.setCedulaPropietario(rs.getString("CEDPROP"));
        datos.setNombrePropietario(rs.getString("NOMPROP"));
        datos.setDireccionPropietario(rs.getString("DIRPROP"));
        datos.setTelefonoPropietario(rs.getString("TELPROP"));
        datos.setCiudadPropietario(rs.getString("CIUPROP"));
        //DATOS TENEDOR
        datos.setCedulaTenedor(rs.getString("CEDTEN"));
        datos.setNombreTenedor(rs.getString("NOMTEN"));
        datos.setDireccionTenedor(rs.getString("DIRTEN"));
        datos.setTelefonoTenedor(rs.getString("TELTEN"));
        datos.setCiudadTenedor(rs.getString("CIUTEN"));
        //DATOS CONDUCTOR
        datos.setCedulaConductor(rs.getString("CEDCOND"));
        datos.setNombreConductor(rs.getString("NOMCOND"));
        datos.setDireccionConductor(rs.getString("DIRCOND"));
        datos.setCatLicConductor(rs.getString("CATPASE"));
        datos.setCiudadConductor(rs.getString("CIUCOND"));
        //OTROS DATOS
        datos.setFechaImpresion(rs.getString("PRINTER_DATE"));
        datos.setValorAnticipo(rs.getDouble("anticipo_for"));
        datos.setMoneda(rs.getString("MONEDA"));
        datos.setDespachador(rs.getString("DESPACHADOR"));
        datos.setContenedores(rs.getString("CONTENEDORES"));
        datos.setReg_status(rs.getString("REG_STATUS"));
        
        //JEscandon 02-03-06
        datos.setRegistroNacionalCarga(rs.getString("REG_NAL_CARGA"));
        datos.setNumeroPolizaSoat(rs.getString("POLIZASOAT"));
        datos.setCompa�iaSoat(rs.getString("CIASOAT"));
        datos.setNumeroEjes(rs.getString("NOEJES"));
        datos.setFechacargue(rs.getString("fecha_cargue"));     
        datos.setPrecinto(rs.getString("precinto"));
                
         //JEscandon 27-04-07
        datos.setPlaviaje( ( rs.getString("planviaje") != null )?rs.getString("planviaje"):"" );
    }
    
    
    
    //setter
    // planilla
    public void setNumeroPlanilla(String valor){
        this.NumeroPlanilla = valor;
    }
    public void setFechaPlanilla(String valor){
        this.FechaPlanilla = valor;
    }
    public void setCodOrigen(String valor){
        this.CodOrigen = valor;
    }
    public void setDesOrigen(String valor){
        this.DesOrigen = valor;
    }
    public void setCodDestino(String valor){
        this.CodDestino = valor;
    }
    public void setDesDestino(String valor){
        this.DesDestino = valor;
    }
    public void setFechaImpresion(String valor){
        this.FechaImpresion = valor;
    }
    public void setRuta(String valor){
        this.Ruta = valor;
    }
    public void setValorAnticipo(double valor){
        this.ValorAnticipo = valor;
    }
    public void setValorAnticipo2(double valor){
        this.ValorAnticipo2 = valor;
    }
    public void setMoneda(String valor){
        this.Moneda = valor;
    }
    
    
    // vehiculo 
    public void setPlaca(String valor){
        this.Placa = valor;
    }
    public void setMarca(String valor){
        this.Marca = valor;
    }
    public void setModelo(String valor){
        this.Modelo = valor;
    }
    public void setSerial(String valor){
        this.Serial = valor;
    }
    public void setColor(String valor){
        this.Color = valor;
    }
    public void setDimensionCarroceria(String valor){
        this.DimensionCarroceria = valor;
    }
    public void setNumeroEjes(String valor){
        this.NumeroEjes = valor;
    }
    public void setPesoVacio(String valor){
        this.PesoVacio = valor;
    }
    
    //propietario
    public void setCedulaPropietario(String valor){
	this.CedulaPropietario = valor;
    }
    public void setNombrePropietario(String valor){
	this.NombrePropietario = valor;
    }
    public void setDireccionPropietario(String valor){
	this.DireccionPropietario = valor;
    }
    public void setTelefonoPropietario(String valor){
	this.TelefonoPropietario = valor;
    }
    public void setCiudadPropietario(String valor){
	this.CiudadPropietario = valor;
    }

    //tenedor
    public void setCedulaTenedor(String valor){
	this.CedulaTenedor = valor;
    }
    public void setNombreTenedor(String valor){
	this.NombreTenedor = valor;
    }
    public void setDireccionTenedor(String valor){
	this.DireccionTenedor = valor;
    }
    public void setTelefonoTenedor(String valor){
	this.TelefonoTenedor = valor;
    }
    public void setCiudadTenedor(String valor){
	this.CiudadTenedor = valor;
    }
    
    // conductor
    public void setCedulaConductor(String valor){
	this.CedulaConductor = valor;
    }
    public void setNombreConductor(String valor){
	this.NombreConductor = valor;
    }
    public void setDireccionConductor(String valor){
	this.DireccionConductor = valor;
    }
    public void setTelefonoConductor(String valor){
	this.TelefonoConductor = valor;
    }
    public void setCiudadConductor(String valor){
	this.CiudadConductor = valor;
    }
    public void setDespachador(String valor){
	this.Despachador = valor;
    }
    
    public void setRemesas(List valor){
        this.Remesas = valor;
    }
    
    
    // getter

    
    // planilla
    public String getNumeroPlanilla(){
        return this.NumeroPlanilla;
    }
    public String getFechaPlanilla(){
        return this.FechaPlanilla;
    }
    public String getCodOrigen(){
        return this.CodOrigen;
    }
    public String getDesOrigen(){
        return this.DesOrigen;
    }
    public String getCodDestino(){
        return this.CodDestino;
    }
    public String getDesDestino(){
        return this.DesDestino;
    }
    public String getFechaImpresion(){
        return this.FechaImpresion;
    }
    public String getRuta(){
        return this.Ruta;
    }
    public double getValorAnticipo(){
        return this.ValorAnticipo;
    }
    public double getValorAnticipo2(){
        return this.ValorAnticipo2;
    }
    public String getMoneda(){
        return this.Moneda;
    }

    
    // vehiculo 
    public String getPlaca(){
        return  this.Placa;
    }
    public String getMarca(){
        return this.Marca;
    }
    public String getModelo(){
        return this.Modelo;
    }
    public String getSerial(){
        return this.Serial;
    }
    public String getColor(){
        return this.Color;
    }
    public String getDimensionCarroceria(){
        return this.DimensionCarroceria;
    }
    public String getNumeroEjes(){
        return this.NumeroEjes;
    }
    public String getPesoVacio(){
        return this.PesoVacio;
    }
    
    //propietario
    public String getCedulaPropietario(){
	return this.CedulaPropietario;
    }
    public String getNombrePropietario(){
	return this.NombrePropietario;
    }
    public String getDireccionPropietario(){
	return this.DireccionPropietario;
    }
    public String getTelefonoPropietario(){
	return this.TelefonoPropietario;
    }
    public String getCiudadPropietario(){
	return this.CiudadPropietario;
    }

    //tenedor
    public String getCedulaTenedor(){
	return this.CedulaTenedor;
    }
    public String getNombreTenedor(){
	return this.NombreTenedor;
    }
    public String getDireccionTenedor(){
	return this.DireccionTenedor;
    }
    public String getTelefonoTenedor(){
	return this.TelefonoTenedor;
    }
    public String getCiudadTenedor(){
	return this.CiudadTenedor;
    }
    
    // conductor
    public String getCedulaConductor(){
	return this.CedulaConductor;
    }
    public String getNombreConductor(){
	return this.NombreConductor;
    }
    public String getDireccionConductor(){
	return this.DireccionConductor;
    }
    public String getTelefonoConductor(){
	return this.TelefonoConductor;
    }
    public String getCiudadConductor(){
	return this.CiudadConductor;
    }
    public String getDespachador(){
	return this.Despachador;
    }
    
    //remesas
    public List getRemesas(){
        return this.Remesas;
    }
    
    /**
     * Getter for property CatLicConductor.
     * @return Value of property CatLicConductor.
     */
    public java.lang.String getCatLicConductor() {
        return CatLicConductor;
    }
    
    /**
     * Setter for property CatLicConductor.
     * @param CatLicConductor New value of property CatLicConductor.
     */
    public void setCatLicConductor(java.lang.String CatLicConductor) {
        this.CatLicConductor = CatLicConductor;
    }
    
    /**
     * Getter for property VecimientoSoat.
     * @return Value of property VecimientoSoat.
     */
    public java.lang.String getVecimientoSoat() {
        return VecimientoSoat;
    }
    
    /**
     * Setter for property VecimientoSoat.
     * @param VecimientoSoat New value of property VecimientoSoat.
     */
    public void setVecimientoSoat(java.lang.String VecimientoSoat) {
        this.VecimientoSoat = VecimientoSoat;
    }
    
    /**
     * Getter for property CodigoRegional.
     * @return Value of property CodigoRegional.
     */
    public java.lang.String getCodigoRegional() {
        return CodigoRegional;
    }
    
    /**
     * Setter for property CodigoRegional.
     * @param CodigoRegional New value of property CodigoRegional.
     */
    public void setCodigoRegional(java.lang.String CodigoRegional) {
        this.CodigoRegional = CodigoRegional;
    }
    
    /**
     * Getter for property CodigoEmpresa.
     * @return Value of property CodigoEmpresa.
     */
    public java.lang.String getCodigoEmpresa() {
        return CodigoEmpresa;
    }
    
    /**
     * Setter for property CodigoEmpresa.
     * @param CodigoEmpresa New value of property CodigoEmpresa.
     */
    public void setCodigoEmpresa(java.lang.String CodigoEmpresa) {
        this.CodigoEmpresa = CodigoEmpresa;
    }
    
    /**
     * Getter for property CodigoNumConsec.
     * @return Value of property CodigoNumConsec.
     */
    public java.lang.String getCodigoNumConsec() {
        return CodigoNumConsec;
    }
    
    /**
     * Setter for property CodigoNumConsec.
     * @param CodigoNumConsec New value of property CodigoNumConsec.
     */
    public void setCodigoNumConsec(java.lang.String CodigoNumConsec) {
        this.CodigoNumConsec = CodigoNumConsec;
    }
    
    /**
     * Getter for property RangoInicial.
     * @return Value of property RangoInicial.
     */
    public java.lang.String getRangoInicial() {
        return RangoInicial;
    }
    
    /**
     * Setter for property RangoInicial.
     * @param RangoInicial New value of property RangoInicial.
     */
    public void setRangoInicial(java.lang.String RangoInicial) {
        this.RangoInicial = RangoInicial;
    }
    
    /**
     * Getter for property RangoFinal.
     * @return Value of property RangoFinal.
     */
    public java.lang.String getRangoFinal() {
        return RangoFinal;
    }
    
    /**
     * Setter for property RangoFinal.
     * @param RangoFinal New value of property RangoFinal.
     */
    public void setRangoFinal(java.lang.String RangoFinal) {
        this.RangoFinal = RangoFinal;
    }
    
    /**
     * Getter for property Linea.
     * @return Value of property Linea.
     */
    public java.lang.String getLinea() {
        return Linea;
    }
    
    /**
     * Setter for property Linea.
     * @param Linea New value of property Linea.
     */
    public void setLinea(java.lang.String Linea) {
        this.Linea = Linea;
    }
    
    /**
     * Getter for property ModeloRepotenciado.
     * @return Value of property ModeloRepotenciado.
     */
    public java.lang.String getModeloRepotenciado() {
        return ModeloRepotenciado;
    }
    
    /**
     * Setter for property ModeloRepotenciado.
     * @param ModeloRepotenciado New value of property ModeloRepotenciado.
     */
    public void setModeloRepotenciado(java.lang.String ModeloRepotenciado) {
        this.ModeloRepotenciado = ModeloRepotenciado;
    }
    
    /**
     * Getter for property RegistroNacionalCarga.
     * @return Value of property RegistroNacionalCarga.
     */
    public java.lang.String getRegistroNacionalCarga() {
        return RegistroNacionalCarga;
    }
    
    /**
     * Setter for property RegistroNacionalCarga.
     * @param RegistroNacionalCarga New value of property RegistroNacionalCarga.
     */
    public void setRegistroNacionalCarga(java.lang.String RegistroNacionalCarga) {
        this.RegistroNacionalCarga = RegistroNacionalCarga;
    }
    
    /**
     * Getter for property NumeroPolizaSoat.
     * @return Value of property NumeroPolizaSoat.
     */
    public java.lang.String getNumeroPolizaSoat() {
        return NumeroPolizaSoat;
    }
    
    /**
     * Setter for property NumeroPolizaSoat.
     * @param NumeroPolizaSoat New value of property NumeroPolizaSoat.
     */
    public void setNumeroPolizaSoat(java.lang.String NumeroPolizaSoat) {
        this.NumeroPolizaSoat = NumeroPolizaSoat;
    }
    
    /**
     * Getter for property Compa�iaSoat.
     * @return Value of property Compa�iaSoat.
     */
    public java.lang.String getCompa�iaSoat() {
        return Compa�iaSoat;
    }
    
    /**
     * Setter for property Compa�iaSoat.
     * @param Compa�iaSoat New value of property Compa�iaSoat.
     */
    public void setCompa�iaSoat(java.lang.String Compa�iaSoat) {
        this.Compa�iaSoat = Compa�iaSoat;
    }
    
    /**
     * Getter for property PlacaTrailer.
     * @return Value of property PlacaTrailer.
     */
    public java.lang.String getPlacaTrailer() {
        return PlacaTrailer;
    }
    
    /**
     * Setter for property PlacaTrailer.
     * @param PlacaTrailer New value of property PlacaTrailer.
     */
    public void setPlacaTrailer(java.lang.String PlacaTrailer) {
        this.PlacaTrailer = PlacaTrailer;
    }
    
    /**
     * Getter for property TotalFlete.
     * @return Value of property TotalFlete.
     */
    public java.lang.String getTotalFlete() {
        return TotalFlete;
    }
    
    /**
     * Setter for property TotalFlete.
     * @param TotalFlete New value of property TotalFlete.
     */
    public void setTotalFlete(java.lang.String TotalFlete) {
        this.TotalFlete = TotalFlete;
    }
    
    /**
     * Getter for property CiaSeguros.
     * @return Value of property CiaSeguros.
     */
    public java.lang.String getCiaSeguros() {
        return CiaSeguros;
    }
    
    /**
     * Setter for property CiaSeguros.
     * @param CiaSeguros New value of property CiaSeguros.
     */
    public void setCiaSeguros(java.lang.String CiaSeguros) {
        this.CiaSeguros = CiaSeguros;
    }
    
    /**
     * Getter for property NumPoliza.
     * @return Value of property NumPoliza.
     */
    public java.lang.String getNumPoliza() {
        return NumPoliza;
    }
    
    /**
     * Setter for property NumPoliza.
     * @param NumPoliza New value of property NumPoliza.
     */
    public void setNumPoliza(java.lang.String NumPoliza) {
        this.NumPoliza = NumPoliza;
    }
    
    /**
     * Getter for property VigenciaPoliza.
     * @return Value of property VigenciaPoliza.
     */
    public java.lang.String getVigenciaPoliza() {
        return VigenciaPoliza;
    }
    
    /**
     * Setter for property VigenciaPoliza.
     * @param VigenciaPoliza New value of property VigenciaPoliza.
     */
    public void setVigenciaPoliza(java.lang.String VigenciaPoliza) {
        this.VigenciaPoliza = VigenciaPoliza;
    }
    
    /**
     * Getter for property LugarPagoSaldo.
     * @return Value of property LugarPagoSaldo.
     */
    public java.lang.String getLugarPagoSaldo() {
        return LugarPagoSaldo;
    }
    
    /**
     * Setter for property LugarPagoSaldo.
     * @param LugarPagoSaldo New value of property LugarPagoSaldo.
     */
    public void setLugarPagoSaldo(java.lang.String LugarPagoSaldo) {
        this.LugarPagoSaldo = LugarPagoSaldo;
    }
    
    /**
     * Getter for property FechaPagoSaldo.
     * @return Value of property FechaPagoSaldo.
     */
    public java.lang.String getFechaPagoSaldo() {
        return FechaPagoSaldo;
    }
    
    /**
     * Setter for property FechaPagoSaldo.
     * @param FechaPagoSaldo New value of property FechaPagoSaldo.
     */
    public void setFechaPagoSaldo(java.lang.String FechaPagoSaldo) {
        this.FechaPagoSaldo = FechaPagoSaldo;
    }
    
    /**
     * Getter for property CarguePagadoPor.
     * @return Value of property CarguePagadoPor.
     */
    public java.lang.String getCarguePagadoPor() {
        return CarguePagadoPor;
    }
    
    /**
     * Setter for property CarguePagadoPor.
     * @param CarguePagadoPor New value of property CarguePagadoPor.
     */
    public void setCarguePagadoPor(java.lang.String CarguePagadoPor) {
        this.CarguePagadoPor = CarguePagadoPor;
    }
    
    /**
     * Getter for property DescarguePagadoPor.
     * @return Value of property DescarguePagadoPor.
     */
    public java.lang.String getDescarguePagadoPor() {
        return DescarguePagadoPor;
    }
    
    /**
     * Setter for property DescarguePagadoPor.
     * @param DescarguePagadoPor New value of property DescarguePagadoPor.
     */
    public void setDescarguePagadoPor(java.lang.String DescarguePagadoPor) {
        this.DescarguePagadoPor = DescarguePagadoPor;
    }
    
    /**
     * Getter for property Observaciones.
     * @return Value of property Observaciones.
     */
    public java.lang.String getObservaciones() {
        return Observaciones;
    }
    
    /**
     * Setter for property Observaciones.
     * @param Observaciones New value of property Observaciones.
     */
    public void setObservaciones(java.lang.String Observaciones) {
        this.Observaciones = Observaciones;
    }
    
    /**
     * Getter for property Contenedores.
     * @return Value of property Contenedores.
     */
    public java.lang.String getContenedores() {
        return Contenedores;
    }
    
    /**
     * Setter for property Contenedores.
     * @param Contenedores New value of property Contenedores.
     */
    public void setContenedores(java.lang.String Contenedores) {
        this.Contenedores = Contenedores;
    }
    
    /**
     * Getter for property DocInternos.
     * @return Value of property DocInternos.
     */
    public java.lang.String getDocInternos() {
        return DocInternos;
    }
    
    /**
     * Setter for property DocInternos.
     * @param DocInternos New value of property DocInternos.
     */
    public void setDocInternos(java.lang.String DocInternos) {
        this.DocInternos = DocInternos;
    }
    
    /**
     * Getter for property Resolucion.
     * @return Value of property Resolucion.
     */
    public java.lang.String getResolucion() {
        return Resolucion;
    }
    
    /**
     * Setter for property Resolucion.
     * @param Resolucion New value of property Resolucion.
     */
    public void setResolucion(java.lang.String Resolucion) {
        this.Resolucion = Resolucion;
    }
    
    /**
     * Getter for property Carroceria.
     * @return Value of property Carroceria.
     */
    public java.lang.String getCarroceria() {
        return Carroceria;
    }
    
    /**
     * Setter for property Carroceria.
     * @param Carroceria New value of property Carroceria.
     */
    public void setCarroceria(java.lang.String Carroceria) {
        this.Carroceria = Carroceria;
    }
    
    /**
     * Getter for property detalle.
     * @return Value of property detalle.
     */
    public java.util.Vector getDetalle() {
        return detalle;
    }
    
    /**
     * Setter for property detalle.
     * @param detalle New value of property detalle.
     */
    public void setDetalle(java.util.Vector detalle) {
        this.detalle = detalle;
    }
    
    /**
     * Getter for property copias.
     * @return Value of property copias.
     */
    public java.util.Vector getCopias() {
        return copias;
    }
    
    /**
     * Setter for property copias.
     * @param copias New value of property copias.
     */
    public void setCopias(java.util.Vector copias) {
        this.copias = copias;
    }
    
    /**
     * Getter for property leyenda.
     * @return Value of property leyenda.
     */
    public java.lang.String getLeyenda() {
        return leyenda;
    }
    
    /**
     * Setter for property leyenda.
     * @param leyenda New value of property leyenda.
     */
    public void setLeyenda(java.lang.String leyenda) {
        this.leyenda = leyenda;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property soportes.
     * @return Value of property soportes.
     */
    public java.lang.String getSoportes() {
        return soportes;
    }
    
    /**
     * Setter for property soportes.
     * @param soportes New value of property soportes.
     */
    public void setSoportes(java.lang.String soportes) {
        this.soportes = soportes;
    }
    
    /**
     * Getter for property fechacargue.
     * @return Value of property fechacargue.
     */
    public java.lang.String getFechacargue() {
        return fechacargue;
    }
    
    /**
     * Setter for property fechacargue.
     * @param fechacargue New value of property fechacargue.
     */
    public void setFechacargue(java.lang.String fechacargue) {
        this.fechacargue = fechacargue;
    }
    /**
     * Getter for property precinto.
     * @return Value of property precinto.
     */
    public java.lang.String getPrecinto() {
        return precinto;
    }
    
    /**
     * Setter for property precinto.
     * @param precinto New value of property precinto.
     */
    public void setPrecinto(java.lang.String precinto) {
        this.precinto = precinto;
    }    
    
    /**
     * Getter for property plaviaje.
     * @return Value of property plaviaje.
     */
    public java.lang.String getPlaviaje() {
        return plaviaje;
    }
    
    /**
     * Setter for property plaviaje.
     * @param plaviaje New value of property plaviaje.
     */
    public void setPlaviaje(java.lang.String plaviaje) {
        this.plaviaje = plaviaje;
    }
    
}
