/*
 * Comentario.java
 *
 * Created on 10 de febrero de 2005, 02:31 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  AMENDEZ
 *
 *CLASE QUE CONTIENE EL COMENTARIO QUE SE ENCUENTRA AL FNAL DE LOS 
 *ESTRACTOS.
 */
public class Comentario {
    public static final String MSG1 = "EL VALOR DEFINITIVO PUEDE DIFERIR DEL CONSIGNADO EN ESTE PRE-EXTRACTO POR LOS SIGUIENTES CONCEPTOS:"; 
    public static final String MSG2 = "1) RECUPERACION DE ANTICIPOS.";
    public static final String MSG3 = "2) RECUPERACION DE REANTICIPOS."; 
    public static final String MSG4 = "3) CORRECIONES.";
    public static final String MSG5 = "4) AJUSTE DE FLETES POR ANULACION DE OC Y CORRECCIONES.";
    public static final String MSG6 = "5) DESCUENTOS DE COSTOS OPERATIVOS.";
    public static final String MSG7 = "6) ALQUILER Y MANTENIMIENTO DE VANES.";
    
    /** Creates a new instance of Comentario */
    public Comentario() {
    }
    
}
