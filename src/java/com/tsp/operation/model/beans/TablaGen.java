/*
* Nombre        TablaGen.java
* Descripci�n   Clase para el acceso a los datos del reporte de informaci�n al cliente
* Autor         Alejandro Payares
* Fecha         15 de diciembre de 2005, 11:04 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.beans;


/**
 *
 * @author  Alejandro Payares
 */
public class TablaGen implements java.io.Serializable{
    
    /**
     * Holds value of property table_code.
     */
    private String table_code;
    
    private String usuario;
    private String descuenta;
    /**
     * Holds value of property referencia.
     */
    private String referencia;
    
    /**
     * Holds value of property descripcion.
     */
    private String descripcion;
    
    /**
     * Holds value of property table_type.
     */
    private String table_type;
    
    /**
     * Holds value of property oid.
     */
    private String oid;
    
    //diogenes 10-01-2006
    private String dato;
    
    /** Creates a new instance of TablaGen */
    public TablaGen() {
    }
    
    /**
     * Getter for property table_code.
     * @return Value of property table_code.
     */
    public String getTable_code() {
        return this.table_code;
    }
    
    /**
     * Setter for property table_code.
     * @param table_code New value of property table_code.
     */
    public void setTable_code(String table_code) {
        this.table_code = table_code;
    }
    
    /**
     * Getter for property referencia.
     * @return Value of property referencia.
     */
    public String getReferencia() {
        return this.referencia;
    }
    
    /**
     * Setter for property referencia.
     * @param referencia New value of property referencia.
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public String getDescripcion() {
        return this.descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property table_type.
     * @return Value of property table_type.
     */
    public String getTable_type() {
        return this.table_type;
    }
    
    /**
     * Setter for property table_type.
     * @param table_type New value of property table_type.
     */
    public void setTable_type(String table_type) {
        this.table_type = table_type;
    }
    
    /**
     * Carga a memoria los atributos del ResultSet
     * @param el ResultSet de donde se sacar�n los datos.
     * @return El bean tablagen.
     * @throws SQLException si aparece un error en la base de datos
     */
    public static TablaGen load (java.sql.ResultSet rs) throws java.sql.SQLException{
        TablaGen bean = new TablaGen ();
        bean.setDescripcion (rs.getString ("descripcion"));
        bean.setReferencia (rs.getString ("referencia"));
        bean.setTable_code (rs.getString ("table_code"));
        bean.setTable_type (rs.getString ("table_type"));
        bean.setOid (rs.getString ("oid"));
        bean.setDato (rs.getString ("dato"));
        return bean;
    }
    
    /**
     * Getter for property oid.
     * @return Value of property oid.
     */
    public String getOid() {
        return this.oid;
    }
    
    /**
     * Setter for property oid.
     * @param oid New value of property oid.
     */
    public void setOid(String oid) {
        this.oid = oid;
    }
    
    /**
     * Getter for property dato.
     * @return Value of property dato.
     */
    public java.lang.String getDato () {
        return dato;
    }
    
    /**
     * Setter for property dato.
     * @param dato New value of property dato.
     */
    public void setDato (java.lang.String dato) {
        this.dato = dato;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }    
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property descuenta.
     * @return Value of property descuenta.
     */
    public java.lang.String getDescuenta() {
        return descuenta;
    }
    
    /**
     * Setter for property descuenta.
     * @param descuenta New value of property descuenta.
     */
    public void setDescuenta(java.lang.String descuenta) {
        this.descuenta = descuenta;
    }
    
}
