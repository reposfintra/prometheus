/*
 * SJExtraflete.java
 *
 * Created on 15 de julio de 2005, 9:03
 */

package com.tsp.operation.model.beans;
import java.sql.*;
/**
 *
 * @author  Henry
 */
public class SJExtraflete {    
    private String cod_extraflete;
    private String codcli;
    private String std_job_no;
    private double valor_costo;
    private String vf_costo;
    private String moneda_costo;
    private String rembolsable;
    private double valor_ingreso;
    private String moneda_ingreso;
    private String vf_ingreso;
    private String clase_valor;
    private float porc;    
    public String reg_status;
    public String creation_date;
    public String last_update;
    public String creation_user;
    public String base;        
    
    //KAREN
    public String descripcion;
    public String ajuste;
    public String unidades;
    public float cantidad=1;
    public float vlrtotal;
    public boolean selec;
    public String concepto="exf";
    
    //PARA COSTOS REEMBOLSABLES
    public String numpla="";
    public String numrem="";
    public String fecdsp="";
    public String proveedor="";
    public String distrito="";
    public String elemento="";
    public String tipo ="";
    public String aprobador ="";
    public float vlrtotalIng;
    
    
    //NUEVOS CAMPOS COSTOS REEMBOLSABLES
    public String agencia="";
    public String cuenta1="";
    public String cuenta2="";
    public String nit="";
    
    /** Creates a new instance of SJExtraflete */
    public SJExtraflete() {
    }        
    public static SJExtraflete load(ResultSet rs) throws SQLException {
        SJExtraflete sjExtra = new SJExtraflete();
        sjExtra.setCod_extraflete(rs.getString(1));
        sjExtra.setCodcli(rs.getString(2));
        sjExtra.setStd_job_no(rs.getString(3));
        sjExtra.setValor_costo(rs.getDouble(4));
        sjExtra.setMoneda_costo(rs.getString(5));
        sjExtra.setVf_costo(rs.getString(6));
        sjExtra.setReembolsable(rs.getString(7));
        sjExtra.setValor_ingreso(rs.getDouble(8));
        sjExtra.setMoneda_ingreso(rs.getString(9));
        sjExtra.setVf_ingreso(rs.getString(10));
        sjExtra.setClase_valor(rs.getString(11));
        sjExtra.setPorcentaje(rs.getFloat(12));
        return sjExtra;
    }
    //Metodos set
    public void setCod_extraflete(String cod){
        this.cod_extraflete = cod;        
    }
    public void setCodcli(String ds){
        this.codcli = ds;        
    }
    public void setStd_job_no(String std){
        this.std_job_no = std;        
    }
    public void setValor_costo(double val){
        this.valor_costo = val;
    }
    public void setVf_costo(String vf){
        this.vf_costo = vf;
    }
    public void setMoneda_costo(String mon) {
        this.moneda_costo = mon;
    }
    public void setReembolsable(String re) {
        this.rembolsable = re;
    }
    public void setValor_ingreso(double val){
        this.valor_ingreso = val;
    }
    public void setMoneda_ingreso(String mon) {
        this.moneda_ingreso = mon;
    }
     public void setVf_ingreso(String vf){
        this.vf_ingreso = vf;
    }
    public void setClase_valor(String clase) {
         this.clase_valor = clase;
    }
    public void setPorcentaje(float porc){
         this.porc = porc;
    }
    public void setReg_status(String status){
        this.reg_status = status;
    }
    public void setCreation_date(String dia){
        this.creation_date = dia;
    }
    public void setLastUpdate(String las){
        this.last_update = las;
    }
    public void setCreation_user(String user){
        this.creation_user = user;
    }
    public void setBase(String base){
        this.base = base;
    }
    //Metodos Get
    public String getCod_extraflete(){
        return this.cod_extraflete;
    }
    public String getCodcli(){
        return this.codcli;
    }
    public String getStd_job_no(){
        return this.std_job_no;
    }
    public double getValor_costo(){
        return this.valor_costo;        
    }
    public String getMoneda_costo(){
        return this.moneda_costo;
    }
    public String getVf_costo(){
        return this.vf_costo;
    }
    public String getReembolsable(){
        return this.rembolsable;
    }
    public double getValor_ingreso(){
        return this.valor_ingreso;
    }
    public String getMoneda_ingreso(){
        return this.moneda_ingreso;
    }
    public String getVf_ingreso(){
        return this.vf_ingreso;
    }
    public String getClase_valor(){
        return this.clase_valor;
    }
    public float getPorcentaje(){
        return this.porc;
    }
    public String getReg_status(){
        return this.reg_status; 
    }
    public String getCreation_date(){
        return this.creation_date;
    }
    public String getLast_update(){
        return this.last_update;
    }
    public String getCreation_user(){
        return this.creation_user;
    }
    public String getBase(){
        return base;
    }
    
    /**
     * Getter for property ajuste.
     * @return Value of property ajuste.
     */
    public java.lang.String getAjuste() {
        return ajuste;
    }
    
    /**
     * Setter for property ajuste.
     * @param ajuste New value of property ajuste.
     */
    public void setAjuste(java.lang.String ajuste) {
        this.ajuste = ajuste;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property porc.
     * @return Value of property porc.
     */
    public float getPorc() {
        return porc;
    }
    
    /**
     * Setter for property porc.
     * @param porc New value of property porc.
     */
    public void setPorc(float porc) {
        this.porc = porc;
    }
    
    /**
     * Getter for property rembolsable.
     * @return Value of property rembolsable.
     */
    public java.lang.String getRembolsable() {
        return rembolsable;
    }
    
    /**
     * Setter for property rembolsable.
     * @param rembolsable New value of property rembolsable.
     */
    public void setRembolsable(java.lang.String rembolsable) {
        this.rembolsable = rembolsable;
    }
    
    /**
     * Getter for property unidades.
     * @return Value of property unidades.
     */
    public java.lang.String getUnidades() {
        return unidades;
    }
    
    /**
     * Setter for property unidades.
     * @param unidades New value of property unidades.
     */
    public void setUnidades(java.lang.String unidades) {
        this.unidades = unidades;
    }
    
    /**
     * Getter for property cantidad.
     * @return Value of property cantidad.
     */
    public float getCantidad() {
        return cantidad;
    }
    
    /**
     * Setter for property cantidad.
     * @param cantidad New value of property cantidad.
     */
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    /**
     * Getter for property vlrtotal.
     * @return Value of property vlrtotal.
     */
    public float getVlrtotal() {
        return vlrtotal;
    }
    
    /**
     * Setter for property vlrtotal.
     * @param vlrtotal New value of property vlrtotal.
     */
    public void setVlrtotal(float vlrtotal) {
        this.vlrtotal = vlrtotal;
    }
    
    /**
     * Getter for property selec.
     * @return Value of property selec.
     */
    public boolean isSelec() {
        return selec;
    }
    
    /**
     * Setter for property selec.
     * @param selec New value of property selec.
     */
    public void setSelec(boolean selec) {
        this.selec = selec;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property fecdsp.
     * @return Value of property fecdsp.
     */
    public java.lang.String getFecdsp() {
        return fecdsp;
    }
    
    /**
     * Setter for property fecdsp.
     * @param fecdsp New value of property fecdsp.
     */
    public void setFecdsp(java.lang.String fecdsp) {
        this.fecdsp = fecdsp;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property elemento.
     * @return Value of property elemento.
     */
    public java.lang.String getElemento() {
        return elemento;
    }
    
    /**
     * Setter for property elemento.
     * @param elemento New value of property elemento.
     */
    public void setElemento(java.lang.String elemento) {
        this.elemento = elemento;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property aprobador.
     * @return Value of property aprobador.
     */
    public java.lang.String getAprobador() {
        return aprobador;
    }
    
    /**
     * Setter for property aprobador.
     * @param aprobador New value of property aprobador.
     */
    public void setAprobador(java.lang.String aprobador) {
        this.aprobador = aprobador;
    }
    
    /**
     * Getter for property vlrtotalIng.
     * @return Value of property vlrtotalIng.
     */
    public float getVlrtotalIng() {
        return vlrtotalIng;
    }
    
    /**
     * Setter for property vlrtotalIng.
     * @param vlrtotalIng New value of property vlrtotalIng.
     */
    public void setVlrtotalIng(float vlrtotalIng) {
        this.vlrtotalIng = vlrtotalIng;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property cuenta2.
     * @return Value of property cuenta2.
     */
    public java.lang.String getCuenta2() {
        return cuenta2;
    }
    
    /**
     * Setter for property cuenta2.
     * @param cuenta2 New value of property cuenta2.
     */
    public void setCuenta2(java.lang.String cuenta2) {
        this.cuenta2 = cuenta2;
    }
    
    /**
     * Getter for property cuenta1.
     * @return Value of property cuenta1.
     */
    public java.lang.String getCuenta1() {
        return cuenta1;
    }
    
    /**
     * Setter for property cuenta1.
     * @param cuenta1 New value of property cuenta1.
     */
    public void setCuenta1(java.lang.String cuenta1) {
        this.cuenta1 = cuenta1;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
}
