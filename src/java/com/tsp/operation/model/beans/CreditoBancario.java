
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Bean para la tabla fin.credito_bancario
 * @author darrieta - geotech
 */
public class CreditoBancario {
    
    private String nit_banco;
    private String documento;
    private String ref_credito;
    private String linea_credito;
    private double dtf;
    private double puntos_basicos;
    private int periodicidad;
    private double tasa_diaria;
    private double vlr_credito;
    private String fecha_inicial;
    private String fecha_vencimiento;
    private String cupo;
    private String creation_user;
    private String user_update;
    private String dstrct;
    private double saldo_cxp;
    private String nombre_banco;
    private ArrayList<CreditoBancarioDetalle> detalles;
    private double tasa_cobrada;
    private double vlr_intereses;
    private String tipo_dtf;
    private int baseAno;
    private String hc;
    private String cuenta;

    /**
     * Get the value of baseAno
     *
     * @return the value of baseAno
     */
    public int getBaseAno() {
        return baseAno;
    }

    /**
     * Set the value of baseAno
     *
     * @param baseAno new value of baseAno
     */
    public void setBaseAno(int baseAno) {
        this.baseAno = baseAno;
    }

    /**
     * Get the value of tipo_dtf
     *
     * @return the value of tipo_dtf
     */
    public String getTipo_dtf() {
        return tipo_dtf;
    }

    /**
     * Set the value of tipo_dtf
     *
     * @param tipo_dtf new value of tipo_dtf
     */
    public void setTipo_dtf(String tipo_dtf) {
        this.tipo_dtf = tipo_dtf;
    }

    /**
     * Get the value of vlr_intereses
     *
     * @return the value of vlr_intereses
     */
    public double getVlr_intereses() {
        return vlr_intereses;
    }

    /**
     * Set the value of vlr_intereses
     *
     * @param vlr_intereses new value of vlr_intereses
     */
    public void setVlr_intereses(double vlr_intereses) {
        this.vlr_intereses = vlr_intereses;
    }

    /**
     * Get the value of tasa_cobrada
     *
     * @return the value of tasa_cobrada
     */
    public double getTasa_cobrada() {
        return tasa_cobrada;
    }

    /**
     * Set the value of tasa_cobrada
     *
     * @param tasa_cobrada new value of tasa_cobrada
     */
    public void setTasa_cobrada(double tasa_cobrada) {
        this.tasa_cobrada = tasa_cobrada;
    }

    /**
     * Get the value of detalles
     *
     * @return the value of detalles
     */
    public ArrayList<CreditoBancarioDetalle> getDetalles() {
        return detalles;
    }

    /**
     * Set the value of detalles
     *
     * @param detalles new value of detalles
     */
    public void setDetalles(ArrayList<CreditoBancarioDetalle> detalles) {
        this.detalles = detalles;
    }

    /**
     * Get the value of nombre_banco
     *
     * @return the value of nombre_banco
     */
    public String getNombre_banco() {
        return nombre_banco;
    }

    /**
     * Set the value of nombre_banco
     *
     * @param nombre_banco new value of nombre_banco
     */
    public void setNombre_banco(String nombre_banco) {
        this.nombre_banco = nombre_banco;
    }

    /**
     * Get the value of saldo_cxp
     *
     * @return the value of saldo_cxp
     */
    public double getSaldo_cxp() {
        return saldo_cxp;
    }

    /**
     * Set the value of saldo_cxp
     *
     * @param saldo_cxp new value of saldo_cxp
     */
    public void setSaldo_cxp(double saldo_cxp) {
        this.saldo_cxp = saldo_cxp;
    }

    /**
     * Get the value of dstrct
     *
     * @return the value of dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * Set the value of dstrct
     *
     * @param dstrct new value of dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }


    /**
     * Get the value of user_update
     *
     * @return the value of user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * Set the value of user_update
     *
     * @param user_update new value of user_update
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }


    /**
     * Get the value of creation_user
     *
     * @return the value of creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * Set the value of creation_user
     *
     * @param creation_user new value of creation_user
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }


    /**
     * Get the value of cupo
     *
     * @return the value of cupo
     */
    public String getCupo() {
        return cupo;
    }

    /**
     * Set the value of cupo
     *
     * @param cupo new value of cupo
     */
    public void setCupo(String cupo) {
        this.cupo = cupo;
    }


    /**
     * Get the value of fecha_vencimiento
     *
     * @return the value of fecha_vencimiento
     */
    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * Set the value of fecha_vencimiento
     *
     * @param fecha_vencimiento new value of fecha_vencimiento
     */
    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * Get the value of fecha_inicial
     *
     * @return the value of fecha_inicial
     */
    public String getFecha_inicial() {
        return fecha_inicial;
    }

    /**
     * Set the value of fecha_inicial
     *
     * @param fecha_inicial new value of fecha_inicial
     */
    public void setFecha_inicial(String fecha_inicial) {
        this.fecha_inicial = fecha_inicial;
    }


    /**
     * Get the value of vlr_credito
     *
     * @return the value of vlr_credito
     */
    public double getVlr_credito() {
        return vlr_credito;
    }

    /**
     * Set the value of vlr_credito
     *
     * @param vlr_credito new value of vlr_credito
     */
    public void setVlr_credito(double vlr_credito) {
        this.vlr_credito = vlr_credito;
    }


    /**
     * Get the value of tasa_diaria
     *
     * @return the value of tasa_diaria
     */
    public double getTasa_diaria() {
        return tasa_diaria;
    }

    /**
     * Set the value of tasa_diaria
     *
     * @param tasa_diaria new value of tasa_diaria
     */
    public void setTasa_diaria(double tasa_diaria) {
        this.tasa_diaria = tasa_diaria;
    }


    /**
     * Get the value of periodicidad
     *
     * @return the value of periodicidad
     */
    public int getPeriodicidad() {
        return periodicidad;
    }

    /**
     * Set the value of periodicidad
     *
     * @param periodicidad new value of periodicidad
     */
    public void setPeriodicidad(int periodicidad) {
        this.periodicidad = periodicidad;
    }


    /**
     * Get the value of puntos_basicos
     *
     * @return the value of puntos_basicos
     */
    public double getPuntos_basicos() {
        return puntos_basicos;
    }

    /**
     * Set the value of puntos_basicos
     *
     * @param puntos_basicos new value of puntos_basicos
     */
    public void setPuntos_basicos(double puntos_basicos) {
        this.puntos_basicos = puntos_basicos;
    }


    /**
     * Get the value of dtf
     *
     * @return the value of dtf
     */
    public double getDtf() {
        return dtf;
    }

    /**
     * Set the value of dtf
     *
     * @param dtf new value of dtf
     */
    public void setDtf(double dtf) {
        this.dtf = dtf;
    }


    /**
     * Get the value of linea_credito
     *
     * @return the value of linea_credito
     */
    public String getLinea_credito() {
        return linea_credito;
    }

    /**
     * Set the value of linea_credito
     *
     * @param linea_credito new value of linea_credito
     */
    public void setLinea_credito(String linea_credito) {
        this.linea_credito = linea_credito;
    }

    /**
     * Get the value of ref_credito
     *
     * @return the value of ref_credito
     */
    public String getRef_credito() {
        return ref_credito;
    }

    /**
     * Set the value of ref_credito
     *
     * @param ref_credito new value of ref_credito
     */
    public void setRef_credito(String ref_credito) {
        this.ref_credito = ref_credito;
    }

    /**
     * Get the value of documento
     *
     * @return the value of documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * Set the value of documento
     *
     * @param documento new value of documento
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }


    /**
     * Get the value of nit_banco
     *
     * @return the value of nit_banco
     */
    public String getNit_banco() {
        return nit_banco;
    }

    /**
     * Set the value of nit_banco
     *
     * @param nit_banco new value of nit_banco
     */
    public void setNit_banco(String nit_banco) {
        this.nit_banco = nit_banco;
    }

    public CreditoBancario Load(ResultSet rs) throws SQLException{
        nit_banco=rs.getString("nit_banco");
        documento=rs.getString("documento");
        ref_credito=rs.getString("ref_credito");
        linea_credito=rs.getString("linea_credito");
        dtf=rs.getDouble("dtf");
        puntos_basicos=rs.getDouble("puntos_basicos");
        periodicidad=rs.getInt("periodicidad");
        vlr_credito=rs.getDouble("vlr_credito");
        fecha_inicial=rs.getString("fecha_inicial");
        fecha_vencimiento=rs.getString("fecha_vencimiento");
        cupo=rs.getString("cupo");
        tasa_cobrada=rs.getDouble("tasa_cobrada");
        vlr_intereses=rs.getDouble("vlr_interes");
        tipo_dtf=rs.getString("tipo_dtf");
        return this;
    }

    /**
     * @return the hc
     */
    public String getHc() {
        return hc;
    }

    /**
     * @param hc the hc to set
     */
    public void setHc(String hc) {
        this.hc = hc;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }
    
}
