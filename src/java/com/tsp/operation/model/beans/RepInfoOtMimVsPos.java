/***********************************************************************************
 * Nombre clase : ............... RepInfoOtMimVsPos.java                                   *
 * Descripcion :................. clase donde se ghuarda la informacion del reporte                                *
 * Autor :....................... Ing. Ivan Gomez                                  *
 * Fecha :....................... 22 de octubre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.beans;

public class RepInfoOtMimVsPos {
     private String fecha;
     
     private String ot;
     private String docint;
     private String factura;
     private String cantkg;
     private String contenedor;
     private String precinto;
     private String unidades;
     
     private String otpg;
     private String docintpg;
     private String facturapg;
     private String cantkgpg;
     private String contenedorpg;
     private String precintopg;
     private String unidadespg;
     
     private String difdocint;
     private String diffactura;
     private String difcantkg;
     private String difcontenedor;
     private String difprecinto;
     private String difunidades;
     
    
    /**
     * Creates a new instance of RepInfoOtMimVsPos 
     */
    public RepInfoOtMimVsPos() {
    }
 
    public void setInfo(String fecha, String ot, String docint, String factura, String cantkg, String contenedor, String precinto, String unidades,
                        String otpg, String docintpg, String facturapg, String cantkgpg, String contenedorpg, String precintopg, String unidadespg,
                        String difdocint, String diffactura, String difcantkg, String difcontenedor, String difprecinto, String difunidades){
        this.setFecha(fecha);
        this.setOt(ot);
        this.setDocint(docint);
        this.setFactura(factura);
        this.setCantkg(cantkg);
        this.setContenedor(contenedor);
        this.setPrecinto(precinto);
        this.setUnidades(unidades);
        
        this.setOtpg(otpg);
        this.setDocintpg(docintpg);
        this.setFacturapg(facturapg);
        this.setCantkgpg(cantkgpg);
        this.setContenedorpg(contenedorpg);
        this.setPrecintopg(precintopg);
        this.setUnidadespg(unidadespg);
        
        
        this.setDifdocint(difdocint);
        this.setDiffactura(diffactura);
        this.setDifcantkg(difcantkg);
        this.setDifcontenedor(difcontenedor);
        this.setDifprecinto(difprecinto);
        this.setDifunidades(difunidades);
    }    

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getOt() {
        return ot;
    }

    public void setOt(String ot) {
        this.ot = ot;
    }

    public String getDocint() {
        return docint;
    }

    public void setDocint(String docint) {
        this.docint = docint;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getCantkg() {
        return cantkg;
    }

    public void setCantkg(String cantkg) {
        this.cantkg = cantkg;
    }

    public String getContenedor() {
        return contenedor;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }

    public String getPrecinto() {
        return precinto;
    }

    public void setPrecinto(String precinto) {
        this.precinto = precinto;
    }

    public String getUnidades() {
        return unidades;
    }

    public void setUnidades(String unidades) {
        this.unidades = unidades;
    }

    public String getOtpg() {
        return otpg;
    }

    public void setOtpg(String otpg) {
        this.otpg = otpg;
    }

    public String getDocintpg() {
        return docintpg;
    }

    public void setDocintpg(String docintpg) {
        this.docintpg = docintpg;
    }

    public String getFacturapg() {
        return facturapg;
    }

    public void setFacturapg(String facturapg) {
        this.facturapg = facturapg;
    }

    public String getCantkgpg() {
        return cantkgpg;
    }

    public void setCantkgpg(String cantkgpg) {
        this.cantkgpg = cantkgpg;
    }

    public String getContenedorpg() {
        return contenedorpg;
    }

    public void setContenedorpg(String contenedorpg) {
        this.contenedorpg = contenedorpg;
    }

    public String getPrecintopg() {
        return precintopg;
    }

    public void setPrecintopg(String precintopg) {
        this.precintopg = precintopg;
    }

    public String getUnidadespg() {
        return unidadespg;
    }

    public void setUnidadespg(String unidadespg) {
        this.unidadespg = unidadespg;
    }
    
    /**
     * Getter for property difcantkg.
     * @return Value of property difcantkg.
     */
    public java.lang.String getDifcantkg() {
        return difcantkg;
    }
    
    /**
     * Setter for property difcantkg.
     * @param difcantkg New value of property difcantkg.
     */
    public void setDifcantkg(java.lang.String difcantkg) {
        this.difcantkg = difcantkg;
    }
    
    /**
     * Getter for property difcontenedor.
     * @return Value of property difcontenedor.
     */
    public java.lang.String getDifcontenedor() {
        return difcontenedor;
    }
    
    /**
     * Setter for property difcontenedor.
     * @param difcontenedor New value of property difcontenedor.
     */
    public void setDifcontenedor(java.lang.String difcontenedor) {
        this.difcontenedor = difcontenedor;
    }
    
    /**
     * Getter for property difdocint.
     * @return Value of property difdocint.
     */
    public java.lang.String getDifdocint() {
        return difdocint;
    }
    
    /**
     * Setter for property difdocint.
     * @param difdocint New value of property difdocint.
     */
    public void setDifdocint(java.lang.String difdocint) {
        this.difdocint = difdocint;
    }
    
    /**
     * Getter for property diffactura.
     * @return Value of property diffactura.
     */
    public java.lang.String getDiffactura() {
        return diffactura;
    }
    
    /**
     * Setter for property diffactura.
     * @param diffactura New value of property diffactura.
     */
    public void setDiffactura(java.lang.String diffactura) {
        this.diffactura = diffactura;
    }
    
    /**
     * Getter for property difprecinto.
     * @return Value of property difprecinto.
     */
    public java.lang.String getDifprecinto() {
        return difprecinto;
    }
    
    /**
     * Setter for property difprecinto.
     * @param difprecinto New value of property difprecinto.
     */
    public void setDifprecinto(java.lang.String difprecinto) {
        this.difprecinto = difprecinto;
    }
    
    /**
     * Getter for property difunidades.
     * @return Value of property difunidades.
     */
    public java.lang.String getDifunidades() {
        return difunidades;
    }
    
    /**
     * Setter for property difunidades.
     * @param difunidades New value of property difunidades.
     */
    public void setDifunidades(java.lang.String difunidades) {
        this.difunidades = difunidades;
    }
    
}