package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudHijos<br/>
 * 1/03/2011
 * @author ivargas-Geotech
 */
public class SolicitudHijos {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String tipo;
    private String secuencia;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String telefono;
    private String email;
    private String direccion;
    private String edad;
    private String nombre;

    public SolicitudHijos load(ResultSet rs) throws SQLException {
        SolicitudHijos hijo = new SolicitudHijos();
        hijo.setSecuencia(rs.getString("secuencia"));
        hijo.setNumeroSolicitud(rs.getString("numero_solicitud"));
        hijo.setTipo(rs.getString("tipo"));
        hijo.setNombre(rs.getString("nombre"));
        hijo.setEdad(rs.getString("edad"));
        hijo.setDireccion(rs.getString("direccion"));
        hijo.setEmail(rs.getString("email"));
        hijo.setTelefono(rs.getString("telefono"));
        return hijo;
    }

    public String getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * obtiene el valor de edad
     *
     * @return el valor de edad
     */
    public String getEdad() {
        return edad;
    }

    /**
     * cambia el valor de edad
     *
     * @param edad nuevo valor edad
     */
    public void setEdad(String edad) {
        this.edad = edad;
    }

    /**
     * obtiene el valor de nombre
     *
     * @return el valor de nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * cambia el valor de nombre
     *
     * @param telefono nuevo valor nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * obtiene el valor de telefono
     *
     * @return el valor de telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * cambia el valor de telefono
     *
     * @param telefono nuevo valor telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    /**
     * obtiene el valor de direccion
     *
     * @return el valor de tdireccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * cambia el valor de direccion
     *
     * @param direccion nuevo valor direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * obtiene el valor de email
     *
     * @return el valor de email
     */
    public String getEmail() {
        return email;
    }

    /**
     * cambia el valor de email
     *
     * @param email nuevo valor email
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
