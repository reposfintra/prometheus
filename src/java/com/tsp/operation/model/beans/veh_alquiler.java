/*
 * veh_alquiler.java
 *
 * Created on 4 de septiembre de 2005, 09:57 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
/**
 *
 * @author  Jm
 */
public class veh_alquiler {
    private String placa;
    private String std_job_no;
    private String std_job_desc;
    private String distrito;
    private String usuario_creacion;
    private String usuario_modificacion;
    private String fecha_creacion;
    private String fecha_modificacion;
    private String reg_status;
    
    /** Creates a new instance of veh_alquiler */
    public veh_alquiler() {
    }
    
    public static veh_alquiler load(ResultSet rs)throws Exception{
        veh_alquiler datos = new veh_alquiler();
        datos.setPlaca(rs.getString("PLACA"));
        datos.setStd_job_no(rs.getString("STD_JOB_NO"));
        datos.setReg_status(rs.getString("REG_STATUS"));
        datos.setStd_job_desc(rs.getString("STD_JOB_DESC"));
        return datos;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property fecha_modificacion.
     * @return Value of property fecha_modificacion.
     */
    public java.lang.String getFecha_modificacion() {
        return fecha_modificacion;
    }
    
    /**
     * Setter for property fecha_modificacion.
     * @param fecha_modificacion New value of property fecha_modificacion.
     */
    public void setFecha_modificacion(java.lang.String fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property std_job_desc.
     * @return Value of property std_job_desc.
     */
    public java.lang.String getStd_job_desc() {
        return std_job_desc;
    }
    
    /**
     * Setter for property std_job_desc.
     * @param std_job_desc New value of property std_job_desc.
     */
    public void setStd_job_desc(java.lang.String std_job_desc) {
        this.std_job_desc = std_job_desc;
    }
    
}
