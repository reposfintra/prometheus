package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudPersona<br/> 1/03/2011
 *
 * @author ivargas-Geotech
 */
public class SolicitudPersona {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String tipoPersona;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String tipo;
    private String codcli;
    private String identificacion;
    private String tipoId;
    private String fechaExpedicionId;
    private String ciudadExpedicionId;
    private String dptoExpedicionId;
    private String fechaNacimiento;
    private String ciudadNacimiento;
    private String dptoNacimiento;
    private String nivelEstudio;
    private String profesion;
    private String personasACargo;
    private String numHijos;
    private String totalGrupoFamiliar;
    private String estrato;
    private String tiempoResidencia;
    private String primerApellido;
    private String segundoApellido;
    private String primerNombre;
    private String segundoNombre;
    private String ciiu;
    private String fax;
    private String tipoEmpresa;
    private String fechaConstitucion;
    private String representanteLegal;
    private String generoRepresentante;
    private String tipoIdRepresentante;
    private String idRepresentante;
    private String firmadorCheques;
    private String generoFirmador;
    private String tipoIdFirmador;
    private String idFirmador;
    private String genero;
    private String estadoCivil;
    private String direccion;
    private String ciudad;
    private String departamento;
    private String barrio;
    private String tipoVivienda;
    private String telefono;
    private String celular;
    private String email;
    private String nombre;
    private String telefono2;
    private String segundoApellidoCony;
    private String primerNombreCony;
    private String segundoNombreCony;
    private String tipoIdentificacionCony;
    private String identificacionCony;
    private String empresaCony;
    private String direccionEmpresaCony;
    private String telefonoCony;
    private String salarioCony;
    private String cargoCony;
    private String celularCony;
    private String emailCony;
    private String primerApellidoCony;
    private String parentescoDeudorSolidario;
    private boolean enviarExtractoEmail;
    private boolean enviarExtractoCorrespondecia;
    
    public SolicitudPersona load(ResultSet rs) throws SQLException {
        SolicitudPersona persona = new SolicitudPersona();

        persona.setNumeroSolicitud(rs.getString("numero_solicitud"));
        persona.setPrimerApellido(rs.getString("primer_apellido"));
        persona.setSegundoApellido(rs.getString("segundo_apellido"));
        persona.setPrimerNombre(rs.getString("primer_nombre"));
        persona.setSegundoNombre(rs.getString("segundo_nombre"));
        persona.setNombre(rs.getString("nombre"));
        persona.setGenero(rs.getString("genero"));
        persona.setTipoId(rs.getString("tipo_id"));
        persona.setIdentificacion(rs.getString("id_representante"));
        persona.setFechaExpedicionId(rs.getString("fecha_expedicion_id"));
        persona.setCiudadExpedicionId(rs.getString("ciudad_expedicion_id"));
        persona.setDptoExpedicionId(rs.getString("dpto_expedicion_id"));
        persona.setFechaNacimiento(rs.getString("fecha_nacimiento"));
        persona.setCiudadNacimiento(rs.getString("ciudad_nacimiento"));
        persona.setDptoNacimiento(rs.getString("dpto_nacimiento"));
        persona.setEstadoCivil(rs.getString("estado_civil"));
        persona.setNivelEstudio(rs.getString("nivel_estudio"));
        persona.setProfesion(rs.getString("profesion"));
        persona.setPersonasACargo(rs.getString("personas_a_cargo"));
        persona.setNumHijos(rs.getString("num_de_hijos"));
        persona.setTotalGrupoFamiliar(rs.getString("total_grupo_familiar"));
        persona.setTipoPersona(rs.getString("tipo_persona"));
        persona.setTipo(rs.getString("tipo"));
        persona.setCodcli(rs.getString("codcli"));
        persona.setIdentificacion(rs.getString("identificacion"));
        persona.setDireccion(rs.getString("direccion"));
        persona.setCiudad(rs.getString("ciudad"));
        persona.setDepartamento(rs.getString("departamento"));
        persona.setBarrio(rs.getString("barrio"));
        persona.setEstrato(rs.getString("estrato"));
        persona.setTelefono(rs.getString("telefono"));
        persona.setTipoVivienda(rs.getString("tipo_vivienda"));
        persona.setTiempoResidencia(rs.getString("tiempo_residencia"));
        persona.setCelular(rs.getString("celular"));
        persona.setEmail(rs.getString("email"));
        persona.setFax(rs.getString("fax"));
        persona.setFechaConstitucion(rs.getString("fecha_constitucion"));
        persona.setRepresentanteLegal(rs.getString("representante_legal"));
        persona.setGeneroRepresentante(rs.getString("genero_representante"));
        persona.setTipoIdRepresentante(rs.getString("tipo_id_representante"));
        persona.setIdRepresentante(rs.getString("id_representante"));
        persona.setFirmadorCheques(rs.getString("firmador_cheques"));
        persona.setGeneroFirmador(rs.getString("genero_firmador"));
        persona.setTipoIdFirmador(rs.getString("tipo_id_firmador"));
        persona.setIdFirmador(rs.getString("id_firmador"));
        persona.setCiiu(rs.getString("ciiu"));
        persona.setTipoEmpresa(rs.getString("tipo_empresa"));
        persona.setTelefono2(rs.getString("telefono2"));
        persona.setPrimerApellidoCony(rs.getString("primer_apellido_cony"));
        persona.setSegundoApellidoCony(rs.getString("segundo_apellido_cony"));
        persona.setPrimerNombreCony(rs.getString("primer_nombre_cony"));
        persona.setSegundoNombreCony(rs.getString("segundo_nombre_cony"));
        persona.setTipoIdentificacionCony(rs.getString("tipo_id_cony"));
        persona.setIdentificacionCony(rs.getString("id_cony"));
        persona.setEmpresaCony(rs.getString("empresa_cony"));
        persona.setDireccionEmpresaCony(rs.getString("direccion_cony"));
        persona.setTelefonoCony(rs.getString("telefono_cony"));
        persona.setCargoCony(rs.getString("cargo_cony"));
        persona.setSalarioCony(rs.getString("salario_cony"));
        persona.setCelularCony(rs.getString("celular_cony"));
        persona.setEmailCony(rs.getString("email_cony"));
        persona.setEnviarExtractoCorrespondecia(rs.getBoolean("extracto_correspondecia"));
        persona.setEnviarExtractoEmail(rs.getBoolean("extracto_email"));
        persona.setParentescoDeudorSolidario(rs.getString("parentesco_deudor_solidario"));
        return persona;
    }

    public SolicitudPersona() {
    }

    public SolicitudPersona(String regStatus, String dstrct, String numeroSolicitud, String tipoPersona, String creationDate, String creationUser, String lastUpdate, String userUpdate, String tipo, String codcli, String identificacion, String tipoId, String fechaExpedicionId, String ciudadExpedicionId, String dptoExpedicionId, String fechaNacimiento, String ciudadNacimiento, String dptoNacimiento, String nivelEstudio, String profesion, String personasACargo, String numHijos, String totalGrupoFamiliar, String estrato, String tiempoResidencia, String primerApellido, String segundoApellido, String primerNombre, String segundoNombre, String ciiu, String fax, String tipoEmpresa, String fechaConstitucion, String representanteLegal, String generoRepresentante, String tipoIdRepresentante, String idRepresentante, String firmadorCheques, String generoFirmador, String tipoIdFirmador, String idFirmador, String genero, String estadoCivil, String direccion, String ciudad, String departamento, String barrio, String tipoVivienda, String telefono, String celular, String email, String nombre, String telefono2, String segundoApellidoCony, String primerNombreCony, String segundoNombreCony, String tipoIdentificacionCony, String identificacionCony, String empresaCony, String direccionEmpresaCony, String telefonoCony, String salarioCony, String cargoCony, String celularCony, String emailCony, String primerApellidoCony) {
        this.regStatus = regStatus;
        this.dstrct = dstrct;
        this.numeroSolicitud = numeroSolicitud;
        this.tipoPersona = tipoPersona;
        this.creationDate = creationDate;
        this.creationUser = creationUser;
        this.lastUpdate = lastUpdate;
        this.userUpdate = userUpdate;
        this.tipo = tipo;
        this.codcli = codcli;
        this.identificacion = identificacion;
        this.tipoId = tipoId;
        this.fechaExpedicionId = fechaExpedicionId;
        this.ciudadExpedicionId = ciudadExpedicionId;
        this.dptoExpedicionId = dptoExpedicionId;
        this.fechaNacimiento = fechaNacimiento;
        this.ciudadNacimiento = ciudadNacimiento;
        this.dptoNacimiento = dptoNacimiento;
        this.nivelEstudio = nivelEstudio;
        this.profesion = profesion;
        this.personasACargo = personasACargo;
        this.numHijos = numHijos;
        this.totalGrupoFamiliar = totalGrupoFamiliar;
        this.estrato = estrato;
        this.tiempoResidencia = tiempoResidencia;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.ciiu = ciiu;
        this.fax = fax;
        this.tipoEmpresa = tipoEmpresa;
        this.fechaConstitucion = fechaConstitucion;
        this.representanteLegal = representanteLegal;
        this.generoRepresentante = generoRepresentante;
        this.tipoIdRepresentante = tipoIdRepresentante;
        this.idRepresentante = idRepresentante;
        this.firmadorCheques = firmadorCheques;
        this.generoFirmador = generoFirmador;
        this.tipoIdFirmador = tipoIdFirmador;
        this.idFirmador = idFirmador;
        this.genero = genero;
        this.estadoCivil = estadoCivil;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.departamento = departamento;
        this.barrio = barrio;
        this.tipoVivienda = tipoVivienda;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
        this.nombre = nombre;
        this.telefono2 = telefono2;
        this.segundoApellidoCony = segundoApellidoCony;
        this.primerNombreCony = primerNombreCony;
        this.segundoNombreCony = segundoNombreCony;
        this.tipoIdentificacionCony = tipoIdentificacionCony;
        this.identificacionCony = identificacionCony;
        this.empresaCony = empresaCony;
        this.direccionEmpresaCony = direccionEmpresaCony;
        this.telefonoCony = telefonoCony;
        this.salarioCony = salarioCony;
        this.cargoCony = cargoCony;
        this.celularCony = celularCony;
        this.emailCony = emailCony;
        this.primerApellidoCony = primerApellidoCony;
    }

    /**
     * obtiene el valor de telefono
     *
     * @return el valor de telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * cambia el valor de telefono
     *
     * @param telefono nuevo valor telefono
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * obtiene el valor de barrio
     *
     * @return el valor de barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * cambia el valor de barrio
     *
     * @param barrio nuevo valor barrio
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * obtiene el valor de celular
     *
     * @return el valor de celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * cambia el valor de celular
     *
     * @param celular nuevo valor celular
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * obtiene el valor de ciudad
     *
     * @return el valor de ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * cambia el valor de ciudad
     *
     * @param ciudad nuevo valor ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * obtiene el valor de departamento
     *
     * @return el valor de departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * cambia el valor de departamento
     *
     * @param departamento nuevo valor departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * obtiene el valor de direccion
     *
     * @return el valor de tdireccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * cambia el valor de direccion
     *
     * @param direccion nuevo valor direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * obtiene el valor de email
     *
     * @return el valor de email
     */
    public String getEmail() {
        return email;
    }

    /**
     * cambia el valor de email
     *
     * @param email nuevo valor email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * obtiene el valor de primerApellidoCony
     *
     * @return el valor de primerApellidoCony
     */
    public String getPrimerApellidoCony() {
        return primerApellidoCony;
    }

    /**
     * cambia el valor de primerApellidoCony
     *
     * @param primerApellidoCony nuevo valor primerApellidoCony
     */
    public void setPrimerApellidoCony(String primerApellidoCony) {
        this.primerApellidoCony = primerApellidoCony;
    }

    /**
     * obtiene el valor de Nombre
     *
     * @return el valor de Nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * cambia el valor de nombre
     *
     * @param razonSocial nuevo valor nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * obtiene el valor de cargoCony
     *
     * @return el valor de cargoCony
     */
    public String getCargoCony() {
        return cargoCony;
    }

    /**
     * cambia el valor de cargoCony
     *
     * @param cargoCony nuevo valor cargoCony
     */
    public void setCargoCony(String cargoCony) {
        this.cargoCony = cargoCony;
    }

    /**
     * obtiene el valor de celularCony
     *
     * @return el valor de celularCony
     */
    public String getCelularCony() {
        return celularCony;
    }

    /**
     * cambia el valor de celularCony
     *
     * @param celularCony nuevo valor celularCony
     */
    public void setCelularCony(String celularCony) {
        this.celularCony = celularCony;
    }

    /**
     * obtiene el valor de ciiu
     *
     * @return el valor de ciiu
     */
    public String getCiiu() {
        return ciiu;
    }

    /**
     * cambia el valor de ciiu
     *
     * @param ciiu nuevo valor ciiu
     */
    public void setCiiu(String ciiu) {
        this.ciiu = ciiu;
    }

    /**
     * obtiene el valor de ciudadExpedicionId
     *
     * @return el valor de ciudadExpedicionId
     */
    public String getCiudadExpedicionId() {
        return ciudadExpedicionId;
    }

    /**
     * cambia el valor de ciudadExpedicionId
     *
     * @param ciudadExpedicionId nuevo valor ciudadExpedicionId
     */
    public void setCiudadExpedicionId(String ciudadExpedicionId) {
        this.ciudadExpedicionId = ciudadExpedicionId;
    }

    /**
     * obtiene el valor de ciudadNacimiento
     *
     * @return el valor de ciudadNacimiento
     */
    public String getCiudadNacimiento() {
        return ciudadNacimiento;
    }

    /**
     * cambia el valor de ciudadNacimiento
     *
     * @param ciudadNacimiento nuevo valor ciudadNacimiento
     */
    public void setCiudadNacimiento(String ciudadNacimiento) {
        this.ciudadNacimiento = ciudadNacimiento;
    }

    /**
     * obtiene el valor de codcli
     *
     * @return el valor de codcli
     */
    public String getCodcli() {
        return codcli;
    }

    /**
     * cambia el valor de codcli
     *
     * @param codcli nuevo valor codcli
     */
    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    /**
     * obtiene el valor de direccionEmpresaCony
     *
     * @return el valor de direccionEmpresaCony
     */
    public String getDireccionEmpresaCony() {
        return direccionEmpresaCony;
    }

    /**
     * cambia el valor de direccionEmpresaCony
     *
     * @param direccionEmpresaCony nuevo valor direccionEmpresaCony
     */
    public void setDireccionEmpresaCony(String direccionEmpresaCony) {
        this.direccionEmpresaCony = direccionEmpresaCony;
    }

    /**
     * obtiene el valor de dptoExpedicionId
     *
     * @return el valor de dptoExpedicionId
     */
    public String getDptoExpedicionId() {
        return dptoExpedicionId;
    }

    /**
     * cambia el valor de dptoExpedicionId
     *
     * @param dptoExpedicionId nuevo valor dptoExpedicionId
     */
    public void setDptoExpedicionId(String dptoExpedicionId) {
        this.dptoExpedicionId = dptoExpedicionId;
    }

    /**
     * obtiene el valor de dptoNacimiento
     *
     * @return el valor de dptoNacimiento
     */
    public String getDptoNacimiento() {
        return dptoNacimiento;
    }

    /**
     * cambia el valor de dptoNacimiento
     *
     * @param dptoNacimiento nuevo valor dptoNacimiento
     */
    public void setDptoNacimiento(String dptoNacimiento) {
        this.dptoNacimiento = dptoNacimiento;
    }

    /**
     * obtiene el valor de emailCony
     *
     * @return el valor de emailCony
     */
    public String getEmailCony() {
        return emailCony;
    }

    /**
     * cambia el valor de emailCony
     *
     * @param emailCony nuevo valor emailCony
     */
    public void setEmailCony(String emailCony) {
        this.emailCony = emailCony;
    }

    /**
     * obtiene el valor de empresaCony
     *
     * @return el valor de empresaCony
     */
    public String getEmpresaCony() {
        return empresaCony;
    }

    /**
     * cambia el valor de empresaCony
     *
     * @param empresaCony nuevo valor empresaCony
     */
    public void setEmpresaCony(String empresaCony) {
        this.empresaCony = empresaCony;
    }

    /**
     * obtiene el valor de estadoCivil
     *
     * @return el valor de estadoCivil
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * cambia el valor de estadoCivil
     *
     * @param estadoCivil nuevo valor estadoCivil
     */
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    /**
     * obtiene el valor de estrato
     *
     * @return el valor de estrato
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * cambia el valor de estrato
     *
     * @param estrato nuevo valor estrato
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * obtiene el valor de fax
     *
     * @return el valor de fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * cambia el valor de fax
     *
     * @param fax nuevo valor fax
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * obtiene el valor de fechaConstitucion
     *
     * @return el valor de fechaConstitucion
     */
    public String getFechaConstitucion() {
        return fechaConstitucion;
    }

    /**
     * cambia el valor de fechaConstitucion
     *
     * @param fechaConstitucion nuevo valor fechaConstitucion
     */
    public void setFechaConstitucion(String fechaConstitucion) {
        this.fechaConstitucion = fechaConstitucion;
    }

    /**
     * obtiene el valor de fechaExpedicionId
     *
     * @return el valor de fechaExpedicionId
     */
    public String getFechaExpedicionId() {
        return fechaExpedicionId;
    }

    /**
     * cambia el valor de fechaExpedicionId
     *
     * @param fechaExpedicionId nuevo valor fechaExpedicionId
     */
    public void setFechaExpedicionId(String fechaExpedicionId) {
        this.fechaExpedicionId = fechaExpedicionId;
    }

    /**
     * obtiene el valor de fechaNacimiento
     *
     * @return el valor de fechaNacimiento
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * cambia el valor de fechaNacimiento
     *
     * @param fechaNacimiento nuevo valor fechaNacimiento
     */
    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * obtiene el valor de firmadorCheques
     *
     * @return el valor de firmadorCheques
     */
    public String getFirmadorCheques() {
        return firmadorCheques;
    }

    /**
     * cambia el valor de firmadorCheques
     *
     * @param firmadorCheques nuevo valor firmadorCheques
     */
    public void setFirmadorCheques(String firmadorCheques) {
        this.firmadorCheques = firmadorCheques;
    }

    /**
     * obtiene el valor de genero
     *
     * @return el valor de genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * cambia el valor de genero
     *
     * @param genero nuevo valor genero
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * obtiene el valor de generoFirmador
     *
     * @return el valor de generoFirmador
     */
    public String getGeneroFirmador() {
        return generoFirmador;
    }

    /**
     * cambia el valor de generoFirmador
     *
     * @param generoFirmador nuevo valor generoFirmador
     */
    public void setGeneroFirmador(String generoFirmador) {
        this.generoFirmador = generoFirmador;
    }

    /**
     * obtiene el valor de generoRepresentante
     *
     * @return el valor de generoRepresentante
     */
    public String getGeneroRepresentante() {
        return generoRepresentante;
    }

    /**
     * cambia el valor de generoRepresentante
     *
     * @param generoRepresentanter nuevo valor generoRepresentante
     */
    public void setGeneroRepresentante(String generoRepresentante) {
        this.generoRepresentante = generoRepresentante;
    }

    /**
     * obtiene el valor de idFirmador
     *
     * @return el valor de idFirmador
     */
    public String getIdFirmador() {
        return idFirmador;
    }

    /**
     * cambia el valor de idFirmador
     *
     * @param idFirmador nuevo valor idFirmador
     */
    public void setIdFirmador(String idFirmador) {
        this.idFirmador = idFirmador;
    }

    /**
     * obtiene el valor de idRepresentante
     *
     * @return el valor de idRepresentante
     */
    public String getIdRepresentante() {
        return idRepresentante;
    }

    /**
     * cambia el valor de idRepresentante
     *
     * @param idRepresentante nuevo valor idRepresentante
     */
    public void setIdRepresentante(String idRepresentante) {
        this.idRepresentante = idRepresentante;
    }

    /**
     * obtiene el valor de identificacion
     *
     * @return el valor de identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * cambia el valor de identificacion
     *
     * @param identificacion nuevo valor identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * obtiene el valor de identificacionCony
     *
     * @return el valor de identificacionCony
     */
    public String getIdentificacionCony() {
        return identificacionCony;
    }

    /**
     * cambia el valor de identificacionCony
     *
     * @param identificacionCony nuevo valor identificacionCony
     */
    public void setIdentificacionCony(String identificacionCony) {
        this.identificacionCony = identificacionCony;
    }

    /**
     * obtiene el valor de nivelEstudio
     *
     * @return el valor de nivelEstudio
     */
    public String getNivelEstudio() {
        return nivelEstudio;
    }

    /**
     * cambia el valor de nivelEstudio
     *
     * @param nivelEstudio nuevo valor nivelEstudio
     */
    public void setNivelEstudio(String nivelEstudio) {
        this.nivelEstudio = nivelEstudio;
    }

    /**
     * obtiene el valor de numHijos
     *
     * @return el valor de numHijos
     */
    public String getNumHijos() {
        return numHijos;
    }

    /**
     * cambia el valor de numHijos
     *
     * @param numHijos nuevo valor numHijos
     */
    public void setNumHijos(String numHijos) {
        this.numHijos = numHijos;
    }

    /**
     * obtiene el valor de personasACargo
     *
     * @return el valor de personasACargo
     */
    public String getPersonasACargo() {
        return personasACargo;
    }

    /**
     * cambia el valor de personasACargo
     *
     * @param personasACargo nuevo valor personasACargo
     */
    public void setPersonasACargo(String personasACargo) {
        this.personasACargo = personasACargo;
    }

    /**
     * obtiene el valor de primerApellido
     *
     * @return el valor de primerApellido
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * cambia el valor de primerApellido
     *
     * @param primerApellido nuevo valor primerApellido
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     * obtiene el valor de primerNombre
     *
     * @return el valor de primerNombre
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     * cambia el valor de primerNombre
     *
     * @param primerNombre nuevo valor primerNombre
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     * obtiene el valor de primerNombreCony
     *
     * @return el valor de primerNombreCony
     */
    public String getPrimerNombreCony() {
        return primerNombreCony;
    }

    /**
     * cambia el valor de primerNombreCony
     *
     * @param primerNombreCony nuevo valor primerNombreCony
     */
    public void setPrimerNombreCony(String primerNombreCony) {
        this.primerNombreCony = primerNombreCony;
    }

    /**
     * obtiene el valor de profesion
     *
     * @return el valor de profesion
     */
    public String getProfesion() {
        return profesion;
    }

    /**
     * cambia el valor de profesion
     *
     * @param profesion nuevo valor profesion
     */
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    /**
     * obtiene el valor de representanteLegal
     *
     * @return el valor de representanteLegal
     */
    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    /**
     * cambia el valor de representanteLegal
     *
     * @param representanteLegal nuevo valor representanteLegal
     */
    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    /**
     * obtiene el valor de salarioCony
     *
     * @return el valor de salarioCony
     */
    public String getSalarioCony() {
        return salarioCony;
    }

    /**
     * cambia el valor de salarioCony
     *
     * @param salarioCony nuevo valor salarioCony
     */
    public void setSalarioCony(String salarioCony) {
        this.salarioCony = salarioCony;
    }

    /**
     * obtiene el valor de segundoApellido
     *
     * @return el valor de segundoApellido
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     * cambia el valor de segundoApellido
     *
     * @param segundoApellido nuevo valor segundoApellido
     */
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    /**
     * obtiene el valor de segundoApellidoCony
     *
     * @return el valor de segundoApellidoCony
     */
    public String getSegundoApellidoCony() {
        return segundoApellidoCony;
    }

    /**
     * cambia el valor de segundoApellidoCony
     *
     * @param segundoApellidoCony nuevo valor segundoApellidoCony
     */
    public void setSegundoApellidoCony(String segundoApellidoCony) {
        this.segundoApellidoCony = segundoApellidoCony;
    }

    /**
     * obtiene el valor de segundoNombre
     *
     * @return el valor de segundoNombre
     */
    public String getSegundoNombre() {
        return segundoNombre;
    }

    /**
     * cambia el valor de segundoNombre
     *
     * @param segundoNombre nuevo valor segundoNombre
     */
    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    /**
     * obtiene el valor de segundoNombreCony
     *
     * @return el valor de segundoNombreCony
     */
    public String getSegundoNombreCony() {
        return segundoNombreCony;
    }

    /**
     * cambia el valor de segundoNombreCony
     *
     * @param segundoNombreCony nuevo valor segundoNombreCony
     */
    public void setSegundoNombreCony(String segundoNombreCony) {
        this.segundoNombreCony = segundoNombreCony;
    }

    /**
     * obtiene el valor de telefono2
     *
     * @return el valor de telefono2
     */
    public String getTelefono2() {
        return telefono2;
    }

    /**
     * cambia el valor de telefono2
     *
     * @param telefono2 nuevo valor telefono2
     */
    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    /**
     * obtiene el valor de telefonoCony
     *
     * @return el valor de telefonoCony
     */
    public String getTelefonoCony() {
        return telefonoCony;
    }

    /**
     * cambia el valor de telefonoCony
     *
     * @param telefonoCony nuevo valor telefonoCony
     */
    public void setTelefonoCony(String telefonoCony) {
        this.telefonoCony = telefonoCony;
    }

    /**
     * obtiene el valor de tiempoResidencia
     *
     * @return el valor de tiempoResidencia
     */
    public String getTiempoResidencia() {
        return tiempoResidencia;
    }

    /**
     * cambia el valor de tiempoResidencia
     *
     * @param tiempoResidencia nuevo valor tiempoResidencia
     */
    public void setTiempoResidencia(String tiempoResidencia) {
        this.tiempoResidencia = tiempoResidencia;
    }

    /**
     * obtiene el valor de tipo
     *
     * @return el valor de tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * cambia el valor de tipo
     *
     * @param tipo nuevo valor tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * obtiene el valor de tipoEmpresa
     *
     * @return el valor de tipoEmpresa
     */
    public String getTipoEmpresa() {
        return tipoEmpresa;
    }

    /**
     * cambia el valor de tipoEmpresa
     *
     * @param tipoEmpresa nuevo valor tipoEmpresa
     */
    public void setTipoEmpresa(String tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

    /**
     * obtiene el valor de tipoId
     *
     * @return el valor de tipoId
     */
    public String getTipoId() {
        return tipoId;
    }

    /**
     * cambia el valor de tipoId
     *
     * @param tipoId nuevo valor tipoId
     */
    public void setTipoId(String tipoId) {
        this.tipoId = tipoId;
    }

    /**
     * obtiene el valor de tipoIdFirmador
     *
     * @return el valor de tipoIdFirmador
     */
    public String getTipoIdFirmador() {
        return tipoIdFirmador;
    }

    /**
     * cambia el valor de tipoIdFirmador
     *
     * @param tipoIdFirmador nuevo valor tipoIdFirmador
     */
    public void setTipoIdFirmador(String tipoIdFirmador) {
        this.tipoIdFirmador = tipoIdFirmador;
    }

    /**
     * obtiene el valor de tipoIdRepresentante
     *
     * @return el valor de tipoIdRepresentante
     */
    public String getTipoIdRepresentante() {
        return tipoIdRepresentante;
    }

    /**
     * cambia el valor de tipoIdRepresentante
     *
     * @param tipoIdRepresentante nuevo valor tipoIdRepresentante
     */
    public void setTipoIdRepresentante(String tipoIdRepresentante) {
        this.tipoIdRepresentante = tipoIdRepresentante;
    }

    /**
     * obtiene el valor de tipoIdentificaciónCony
     *
     * @return el valor de tipoIdentificaciónCony
     */
    public String getTipoIdentificacionCony() {
        return tipoIdentificacionCony;
    }

    /**
     * cambia el valor de tipoIdentificaciónCony
     *
     * @param tipoIdentificaciónCony nuevo valor tipoIdentificaciónCony
     */
    public void setTipoIdentificacionCony(String tipoIdentificacionCony) {
        this.tipoIdentificacionCony = tipoIdentificacionCony;
    }

    /**
     * obtiene el valor de tipoVivienda
     *
     * @return el valor de tipoVivienda
     */
    public String getTipoVivienda() {
        return tipoVivienda;
    }

    /**
     * cambia el valor de tipoVivienda
     *
     * @param tipoVivienda nuevo valor tipoVivienda
     */
    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    /**
     * obtiene el valor de totalGrupoFamiliar
     *
     * @return el valor de totalGrupoFamiliar
     */
    public String getTotalGrupoFamiliar() {
        return totalGrupoFamiliar;
    }

    /**
     * cambia el valor de totalGrupoFamiliar
     *
     * @param totalGrupoFamiliar nuevo valor totalGrupoFamiliar
     */
    public void setTotalGrupoFamiliar(String totalGrupoFamiliar) {
        this.totalGrupoFamiliar = totalGrupoFamiliar;
    }

    /**
     * obtiene el valor de creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    /**
     * obtiene el valor de tipoPersona
     *
     * @return el valor de tipoPersona
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * cambia el valor de tipoPersona
     *
     * @param tipoPersona nuevo valor tipoPersona
     */
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
    
    public boolean isEnviarExtractoEmail() {
        return enviarExtractoEmail;
    }

    public void setEnviarExtractoEmail(boolean enviarExtractoEmail) {
        this.enviarExtractoEmail = enviarExtractoEmail;
    }

    public boolean isEnviarExtractoCorrespondecia() {
        return enviarExtractoCorrespondecia;
    }

    public void setEnviarExtractoCorrespondecia(boolean enviarExtractoCorrespondecia) {
        this.enviarExtractoCorrespondecia = enviarExtractoCorrespondecia;
    }

    public String getParentescoDeudorSolidario() {
        return parentescoDeudorSolidario;
    }

    public void setParentescoDeudorSolidario(String parentescoDeudorSolidario) {
        this.parentescoDeudorSolidario = parentescoDeudorSolidario;
    }
}
