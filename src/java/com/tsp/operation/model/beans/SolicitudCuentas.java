package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudCuentas<br/>
 * 1/03/2011
 * @author ivargas-Geotech
 */
public class SolicitudCuentas {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String tipo;
    private String consecutivo;
    private String banco;
    private String cuenta;
    private String fechaApertura;
    private String numeroTarjeta;

    public SolicitudCuentas load(ResultSet rs) throws SQLException {
        SolicitudCuentas cuenta = new SolicitudCuentas();
        cuenta.setNumeroSolicitud(rs.getString("numero_solicitud"));
        cuenta.setConsecutivo(rs.getString("consecutivo"));
        cuenta.setBanco(rs.getString("banco"));
        cuenta.setTipo(rs.getString("tipo"));
        cuenta.setCuenta(rs.getString("cuenta"));
        cuenta.setFechaApertura(rs.getString("fecha_apertura"));
        cuenta.setNumeroTarjeta(rs.getString("numero_tarjeta"));

        return cuenta;
    }

    public String getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    /**
     * obtiene el valor de banco
     *
     * @return el valor de banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * cambia el valor de banco
     *
     * @param banco nuevo valor banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * obtiene el valor de tipo
     *
     * @return el valor de tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * cambia el valor de tipo
     *
     * @param tipo nuevo valor tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }
}
