/*
 * PerfilVistaUsuario.java
 *
 * Created on 5 de julio de 2005, 10:31
 */
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  DRIGO
 */

public class PerfilVistaUsuario implements Serializable{
    
    private String codigo;
    private String usuario;
    private String perfil;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    
    public static PerfilVistaUsuario load(ResultSet rs)throws SQLException{
        PerfilVistaUsuario t = new PerfilVistaUsuario();
        t.setCodigo(rs.getString("cod_pvu"));
        t.setUsuario(rs.getString("usuario"));
        t.setPerfil(rs.getString("perfil"));
        t.setCia(rs.getString("cia"));
        t.setRec_status(rs.getString("rec_status"));
        t.setLast_update(rs.getDate("last_update"));
        t.setUser_update(rs.getString("user_update"));
        t.setCreation_date(rs.getDate("creation_date"));
        t.setCreation_user(rs.getString("creation_user"));
        return t;
    }

    public java.lang.String getCia() {
        return cia;
    }
 
    public void setCodigo(java.lang.String cod) {
        this.codigo = cod;
    }
    public java.lang.String getCodigo() {
        return codigo;
    }
 
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }

    public java.lang.String getUsuario() {
        return usuario;
    }

    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }

    public java.util.Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }

    public java.lang.String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }

    public java.lang.String getPerfil() {
        return perfil;
    }

    public void setPerfil(java.lang.String perfil) {
        this.perfil = perfil;
    }

    public java.util.Date getLast_update() {
        return last_update;
    }

    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }

    public java.lang.String getRec_status() {
        return rec_status;
    }

    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }

    public java.lang.String getUser_update() {
        return user_update;
    }

    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}
