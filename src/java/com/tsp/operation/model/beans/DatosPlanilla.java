/*
 * Datosplanilla.java
 *
 * Created on 26 de febrero de 2002, 3:14
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.List;
/**
 *
 * @author  MarioFontalvo
 */
public class DatosPlanilla implements Serializable{
    private String NumeroPla;    
    private String FechaPla;
    private String COrigenPla;
    private String CDestinoPla;
    private String CAgencia;
    private String DOrigenPla;
    private String DDestinoPla;
    private String DAgencia;
    private String PlacaVehiculo;
    private String CedulaConductor;
    private String NumeroRemision;
    private String cia;
    
    /////////////////////////
    private String PesoReal;
    private String Sj;
    private List   Anticipos;
    
    //CAMPOS AGREGADOS POR KAREN REALES
    private String flete;
    private String distrito;
    private String docInterno;
    private String facComercial;
    private String cantidReal;
    private String contenedor;
    private String precinto;
    private String unidades;
    private String trailer;
    private String fechaSalida;
    private String fechaCargue;
    private String fechaDescargue;
    private String horaISalida;
    private String horaICargue;
    private String horaIDescargue;
    private String horaFSalida;
    private String horaFCargue;
    private String horaFDescargue;
    private String remitente;
    private String destinatario;
    private String base;
    private String cf_code ="";
    private String cantPlanilla ="";
    private String cantRemesa ="";
    private String despachador ="";
    private String cedula ="";
    private int porcent =100;
    
    /** Creates a new instance of Datosplanilla */
    public DatosPlanilla() {
    }
    
    public static DatosPlanilla load(ResultSet rs) throws SQLException
    {
        DatosPlanilla       datos = new DatosPlanilla();                        
        datos.setNumeroPlanilla       (rs.getString(1));
        datos.setFechaPlanilla        (rs.getString(2));
        datos.setCodigoOrigenPlanilla (rs.getString(3));
        datos.setCodigoDestinoPlanilla(rs.getString(4));
        datos.setPlacaVehiculo        (rs.getString(5));
        datos.setCedulaConductor      (rs.getString(6));
        datos.setCodigoAgenciaPlanilla(rs.getString(7));
        return datos;
    }     
    
    public static DatosPlanilla load2(ResultSet rs)throws Exception{
        DatosPlanilla       datos = new DatosPlanilla();                        
        datos.setSJ                   (rs.getString(1));
        datos.setNumeroPlanilla       (rs.getString(2));
        datos.setFechaPlanilla        (rs.getString(3));
        datos.setCodigoAgenciaPlanilla(rs.getString(4));
        datos.setPlacaVehiculo        (rs.getString(5));
        datos.setPesoReal             (rs.getString(6));
        datos.setCedulaConductor      (rs.getString(7));   
        datos.setNumeroRemision       (rs.getString(8));
        return datos;        
    }
    
    
    //Setter
    public void setNumeroRemision(String valor){
        this.NumeroRemision = valor;
    }
    public void setNumeroPlanilla(String valor){
        this.NumeroPla = valor;
    }
    public void setFechaPlanilla(String valor){
        this.FechaPla = valor;
    }    
    public void setCodigoOrigenPlanilla(String valor){
        this.COrigenPla = valor;
    }    
    public void setCodigoDestinoPlanilla(String valor){
        this.CDestinoPla = valor;
    }    
    public void setCodigoAgenciaPlanilla(String valor){
        this.CAgencia = valor;
    }        
    public void setDescripcionOrigenPlanilla(String valor){
        this.DOrigenPla = valor;
    }    
    public void setDescripcionDestinoPlanilla(String valor){
        this.DDestinoPla = valor;
    }    
    public void setDescripcionAgenciaPlanilla(String valor){
        this.DAgencia = valor;
    }        
    public void setPlacaVehiculo(String valor){
        this.PlacaVehiculo = valor;
    }        
    public void setCedulaConductor(String valor){
        this.CedulaConductor = valor;
    }
    /////////////////////////////////////
    public void setPesoReal(String valor){
        this.PesoReal = valor;
    }
    public void setSJ(String valor){
        this.Sj = valor;
    }
    public void setAnticipos(List valor){
        this.Anticipos = valor;
    }
    
    //Getter
    public String getNumeroRemision(){
        return this.NumeroRemision;
    }
    public String getNumeroPlanilla(){
        return this.NumeroPla;
    }
    public String getFechaPlanilla(){
        return this.FechaPla;
    }    
    public String getCodigoOrigenPlanilla(){
        return this.COrigenPla;
    }    
    public String getCodigoDestinoPlanilla(){
        return this.CDestinoPla;
    }    
    public String getCodigoAgenciaPlanilla(){
        return this.CAgencia;
    }        
    public String getDescripcionOrigenPlanilla(){
        return this.DOrigenPla;
    }    
    public String getDescripcionDestinoPlanilla(){
        return this.DDestinoPla;
    }    
    public String getDescripcionAgenciaPlanilla(){
        return this.DAgencia;
    }        
    public String getPlacaVehiculo(){
        return this.PlacaVehiculo;
    }        
    public String getCedulaConductor(){
        return this.CedulaConductor;
    }    
    
    /////////////////////////////////////
    public String getPesoReal(){
        return this.PesoReal;
    }
    public String getSJ(){
        return this.Sj;
    }
    public List getAnticipos(){
        return this.Anticipos;
    }
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    //FUNCIONES GENERADAS POR KAREN REALES
    /**
     * Getter for property flete.
     * @return Value of property flete.
     */
    public java.lang.String getFlete() {
        return flete;
    }
    
    /**
     * Setter for property flete.
     * @param flete New value of property flete.
     */
    public void setFlete(java.lang.String flete) {
        this.flete = flete;
    }
    
    /**
     * Getter for property horaFCargue.
     * @return Value of property horaFCargue.
     */
    public java.lang.String getHoraFCargue() {
        return horaFCargue;
    }
    
    /**
     * Setter for property horaFCargue.
     * @param horaFCargue New value of property horaFCargue.
     */
    public void setHoraFCargue(java.lang.String horaFCargue) {
        this.horaFCargue = horaFCargue;
    }
    
    /**
     * Getter for property horaFDescargue.
     * @return Value of property horaFDescargue.
     */
    public java.lang.String getHoraFDescargue() {
        return horaFDescargue;
    }
    
    /**
     * Setter for property horaFDescargue.
     * @param horaFDescargue New value of property horaFDescargue.
     */
    public void setHoraFDescargue(java.lang.String horaFDescargue) {
        this.horaFDescargue = horaFDescargue;
    }
    
    /**
     * Getter for property horaFSalida.
     * @return Value of property horaFSalida.
     */
    public java.lang.String getHoraFSalida() {
        return horaFSalida;
    }
    
    /**
     * Setter for property horaFSalida.
     * @param horaFSalida New value of property horaFSalida.
     */
    public void setHoraFSalida(java.lang.String horaFSalida) {
        this.horaFSalida = horaFSalida;
    }
    
    /**
     * Getter for property horaICargue.
     * @return Value of property horaICargue.
     */
    public java.lang.String getHoraICargue() {
        return horaICargue;
    }
    
    /**
     * Setter for property horaICargue.
     * @param horaICargue New value of property horaICargue.
     */
    public void setHoraICargue(java.lang.String horaICargue) {
        this.horaICargue = horaICargue;
    }
    
    /**
     * Getter for property horaIDescargue.
     * @return Value of property horaIDescargue.
     */
    public java.lang.String getHoraIDescargue() {
        return horaIDescargue;
    }
    
    /**
     * Setter for property horaIDescargue.
     * @param horaIDescargue New value of property horaIDescargue.
     */
    public void setHoraIDescargue(java.lang.String horaIDescargue) {
        this.horaIDescargue = horaIDescargue;
    }
    
    /**
     * Getter for property horaISalida.
     * @return Value of property horaISalida.
     */
    public java.lang.String getHoraISalida() {
        return horaISalida;
    }
    
    /**
     * Setter for property horaISalida.
     * @param horaISalida New value of property horaISalida.
     */
    public void setHoraISalida(java.lang.String horaISalida) {
        this.horaISalida = horaISalida;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property docInterno.
     * @return Value of property docInterno.
     */
    public java.lang.String getDocInterno() {
        return docInterno;
    }
    
    /**
     * Setter for property docInterno.
     * @param docInterno New value of property docInterno.
     */
    public void setDocInterno(java.lang.String docInterno) {
        this.docInterno = docInterno;
    }
    
    /**
     * Getter for property facComercial.
     * @return Value of property facComercial.
     */
    public java.lang.String getFacComercial() {
        return facComercial;
    }
    
    /**
     * Setter for property facComercial.
     * @param facComercial New value of property facComercial.
     */
    public void setFacComercial(java.lang.String facComercial) {
        this.facComercial = facComercial;
    }
    
    /**
     * Getter for property cantidReal.
     * @return Value of property cantidReal.
     */
    public java.lang.String getCantidReal() {
        return cantidReal;
    }
    
    /**
     * Setter for property cantidReal.
     * @param cantidReal New value of property cantidReal.
     */
    public void setCantidReal(java.lang.String cantidReal) {
        this.cantidReal = cantidReal;
    }
    
    /**
     * Getter for property contenedor.
     * @return Value of property contenedor.
     */
    public java.lang.String getContenedor() {
        return contenedor;
    }
    
    /**
     * Setter for property contenedor.
     * @param contenedor New value of property contenedor.
     */
    public void setContenedor(java.lang.String contenedor) {
        this.contenedor = contenedor;
    }
    
    /**
     * Getter for property precinto.
     * @return Value of property precinto.
     */
    public java.lang.String getPrecinto() {
        return precinto;
    }
    
    /**
     * Setter for property precinto.
     * @param precinto New value of property precinto.
     */
    public void setPrecinto(java.lang.String precinto) {
        this.precinto = precinto;
    }
    
    /**
     * Getter for property unidades.
     * @return Value of property unidades.
     */
    public java.lang.String getUnidades() {
        return unidades;
    }
    
    /**
     * Setter for property unidades.
     * @param unidades New value of property unidades.
     */
    public void setUnidades(java.lang.String unidades) {
        this.unidades = unidades;
    }
    
    /**
     * Getter for property trailer.
     * @return Value of property trailer.
     */
    public java.lang.String getTrailer() {
        return trailer;
    }
    
    /**
     * Setter for property trailer.
     * @param trailer New value of property trailer.
     */
    public void setTrailer(java.lang.String trailer) {
        this.trailer = trailer;
    }
    
    /**
     * Getter for property fechaSalida.
     * @return Value of property fechaSalida.
     */
    public java.lang.String getFechaSalida() {
        return fechaSalida;
    }
    
    /**
     * Setter for property fechaSalida.
     * @param fechaSalida New value of property fechaSalida.
     */
    public void setFechaSalida(java.lang.String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }
    
    /**
     * Getter for property fechaDescargue.
     * @return Value of property fechaDescargue.
     */
    public java.lang.String getFechaDescargue() {
        return fechaDescargue;
    }
    
    /**
     * Setter for property fechaDescargue.
     * @param fechaDescargue New value of property fechaDescargue.
     */
    public void setFechaDescargue(java.lang.String fechaDescargue) {
        this.fechaDescargue = fechaDescargue;
    }
    
    /**
     * Getter for property fechaCargue.
     * @return Value of property fechaCargue.
     */
    public java.lang.String getFechaCargue() {
        return fechaCargue;
    }
    
    /**
     * Setter for property fechaCargue.
     * @param fechaCargue New value of property fechaCargue.
     */
    public void setFechaCargue(java.lang.String fechaCargue) {
        this.fechaCargue = fechaCargue;
    }
    
    /**
     * Getter for property remitente.
     * @return Value of property remitente.
     */
    public java.lang.String getRemitente() {
        return remitente;
    }
    
    /**
     * Setter for property remitente.
     * @param remitente New value of property remitente.
     */
    public void setRemitente(java.lang.String remitente) {
        this.remitente = remitente;
    }
    
    /**
     * Getter for property destinatario.
     * @return Value of property destinatario.
     */
    public java.lang.String getDestinatario() {
        return destinatario;
    }
    
    /**
     * Setter for property destinatario.
     * @param destinatario New value of property destinatario.
     */
    public void setDestinatario(java.lang.String destinatario) {
        this.destinatario = destinatario;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property cf_code.
     * @return Value of property cf_code.
     */
    public java.lang.String getCf_code() {
        return cf_code;
    }
    
    /**
     * Setter for property cf_code.
     * @param cf_code New value of property cf_code.
     */
    public void setCf_code(java.lang.String cf_code) {
        this.cf_code = cf_code;
    }
    
    /**
     * Getter for property cantPlanilla.
     * @return Value of property cantPlanilla.
     */
    public java.lang.String getCantPlanilla() {
        return cantPlanilla;
    }
    
    /**
     * Setter for property cantPlanilla.
     * @param cantPlanilla New value of property cantPlanilla.
     */
    public void setCantPlanilla(java.lang.String cantPlanilla) {
        this.cantPlanilla = cantPlanilla;
    }
    
    /**
     * Getter for property cantRemesa.
     * @return Value of property cantRemesa.
     */
    public java.lang.String getCantRemesa() {
        return cantRemesa;
    }
    
    /**
     * Setter for property cantRemesa.
     * @param cantRemesa New value of property cantRemesa.
     */
    public void setCantRemesa(java.lang.String cantRemesa) {
        this.cantRemesa = cantRemesa;
    }
    
    /**
     * Getter for property despachador.
     * @return Value of property despachador.
     */
    public java.lang.String getDespachador() {
        return despachador;
    }
    
    /**
     * Setter for property despachador.
     * @param despachador New value of property despachador.
     */
    public void setDespachador(java.lang.String despachador) {
        this.despachador = despachador;
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula() {
        return cedula;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula(java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Getter for property porcent.
     * @return Value of property porcent.
     */
    public int getPorcent() {
        return porcent;
    }
    
    /**
     * Setter for property porcent.
     * @param porcent New value of property porcent.
     */
    public void setPorcent(int porcent) {
        this.porcent = porcent;
    }
    
}

