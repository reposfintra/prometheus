
/***************************************
 * Nombre Clase ............. FacturasCheques.java
 * Descripci�n  .. . . . . .  Carga las Facturas para generar Cheques
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  13/12/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model.beans;


import java.util.*;


public class FacturasCheques extends CXP_Doc {
    
    private String corrida;
    private String tipoObjecto;
    private double vlrRTFE;
    private double vlrRICA;
    private double vlrRIVA;
    private double vlrFactura;
    private double vlrRet;
    private double vlrDesc;
    private List   docRel;
    
    
    
    
    private String tipo_pago ;
    private String banco_transfer ;
    private String suc_transfer ;
    private String tipo_cuenta ;
    private String no_cuenta ;
    private String cedula_cuenta ;
    private String nombre_cuenta ;
    
    private String codeBancoTransf;
    
    private String oc;
    
    //operez
    private double valor_rtfe;
    private double valor_iva;
    private double valor_riva;
    private double valor_rica;
    
    
    
    // FACTURAS POR CORRIDAS
    
    
    private Hashtable datoProveedor;
    
    
    // Control:
    private boolean   selected;
    private int       idObject;
    private String    estadoCK;
    
    
    
    
    public  Hashtable infoBloque;
    
    private String    monedaBanco;
    
    private double    vlrPagar;
    private double    vlrNetoPago;
    private String    msj;
    
    private String    monedaLocal;
    private String    tipoPago;
    
    
    private double    vlrTotalPagar  = 0;
    private String    monedaTR       = "";
    private double    valorTR        = 0;
    private double    valorTRLocal   = 0;
    private String    bancoTR        = "";
    private String    sucursalTR     = "";
    private String    transferencia  = "";
    
    
    private String    item           = "";
    
    
    
    public FacturasCheques() {
        docRel           = new LinkedList();
        selected         = false;
        estadoCK         = "checked";
        infoBloque       = new Hashtable();
        msj              = "";
        tipoPago         = "";
    }
    
    
    
    
    
    // SET:
    /**
     * M�todos que guardan valores en el objeto
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void  setCorrida  (String val)  {
        this.corrida       = val;
    }
    
    public void setVlrRetencion(double vlr) {
        this.vlrRet = vlr;
    }
    
    public void setVlrFactura(double vlrItems) {
        this.vlrFactura = vlrItems;
    }
    
    public void setDocumnetosRelacionados(List lista){
        this.docRel = lista;
    }
    
    public void setVlrRICA(double vlrRICA) {
        this.vlrRICA = vlrRICA;
    }
    
    public void setVlrRIVA(double vlrRIVA) {
        this.vlrRIVA = vlrRIVA;
    }
    
    public void setVlrRTFE(double vlrRTFE) {
        this.vlrRTFE = vlrRTFE;
    }
    
    
    public void setVlrDescuentos(double vlrDesc) {
        this.vlrDesc = vlrDesc;
    }
    
    public void setTipoObjecto(java.lang.String tipoObjecto) {
        this.tipoObjecto = tipoObjecto;
    }
    
    
    
    
    // GET:
    /**
     * M�todos que retornan  valores
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String  getCorrida() {
        return this.corrida     ;
    }
    
    public double getVlrRetencion() {
        return vlrRet;
    }
    
    public double getVlrFactura() {
        return vlrFactura;
    }
    
    public List getDocumnetosRelacionados(){
        return this.docRel;
    }
    
    
    public double getVlrRICA() {
        return vlrRICA;
    }
    
    public double getVlrRIVA() {
        return vlrRIVA;
    }
    
    public double getVlrRTFE() {
        return vlrRTFE;
    }
    
    public double getVlrDescuentos() {
        return vlrDesc;
    }
    
    public java.lang.String getTipoObjecto() {
        return tipoObjecto;
    }
    
    
    
    
    
    
    /**
     * Getter for property idObject.
     * @return Value of property idObject.
     */
    public int getIdObject() {
        return idObject;
    }
    
    /**
     * Setter for property idObject.
     * @param idObject New value of property idObject.
     */
    public void setIdObject(int idObject) {
        this.idObject = idObject;
    }
    
    
    
    
    
    
    
    
    /**
     * Getter for property selected.
     * @return Value of property selected.
     */
    public boolean isSelected() {
        return selected;
    }
    
    /**
     * Setter for property selected.
     * @param selected New value of property selected.
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    
    
    public void invertirEstado(){
        this.selected = (this.isSelected())?false:true;
    }
    
    
    
    
    
    
    /**
     * Getter for property datoProveedor.
     * @return Value of property datoProveedor.
     */
    public java.util.Hashtable getDatoProveedor() {
        return datoProveedor;
    }
    
    /**
     * Setter for property datoProveedor.
     * @param datoProveedor New value of property datoProveedor.
     */
    public void setDatoProveedor(java.util.Hashtable datoProveedor) {
        this.datoProveedor = datoProveedor;
    }
    
    
    
    
    
    /**
     * Getter for property infoBloque.
     * @return Value of property infoBloque.
     */
    public java.util.Hashtable getInfoBloque() {
        return infoBloque;
    }
    
    /**
     * Setter for property infoBloque.
     * @param infoBloque New value of property infoBloque.
     */
    public void setInfoBloque(java.util.Hashtable infoBloque) {
        this.infoBloque = infoBloque;
    }
    
    
    
    /**
     * Getter for property tipo_pago.
     * @return Value of property tipo_pago.
     */
    public java.lang.String getTipo_pago() {
        return tipo_pago;
    }
    
    /**
     * Setter for property tipo_pago.
     * @param tipo_pago New value of property tipo_pago.
     */
    public void setTipo_pago(java.lang.String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }
    
    
    
    /**
     * Getter for property banco_transfer.
     * @return Value of property banco_transfer.
     */
    public java.lang.String getBanco_transfer() {
        return banco_transfer;
    }
    
    /**
     * Setter for property banco_transfer.
     * @param banco_transfer New value of property banco_transfer.
     */
    public void setBanco_transfer(java.lang.String banco_transfer) {
        this.banco_transfer = banco_transfer;
    }
    
    
    
    /**
     * Getter for property suc_transfer.
     * @return Value of property suc_transfer.
     */
    public java.lang.String getSuc_transfer() {
        return suc_transfer;
    }
    
    /**
     * Setter for property suc_transfer.
     * @param suc_transfer New value of property suc_transfer.
     */
    public void setSuc_transfer(java.lang.String suc_transfer) {
        this.suc_transfer = suc_transfer;
    }
    
    
    
    /**
     * Getter for property tipo_cuenta.
     * @return Value of property tipo_cuenta.
     */
    public java.lang.String getTipo_cuenta() {
        return tipo_cuenta;
    }
    
    /**
     * Setter for property tipo_cuenta.
     * @param tipo_cuenta New value of property tipo_cuenta.
     */
    public void setTipo_cuenta(java.lang.String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }
    
    
    
    /**
     * Getter for property no_cuenta.
     * @return Value of property no_cuenta.
     */
    public java.lang.String getNo_cuenta() {
        return no_cuenta;
    }
    
    /**
     * Setter for property no_cuenta.
     * @param no_cuenta New value of property no_cuenta.
     */
    public void setNo_cuenta(java.lang.String no_cuenta) {
        this.no_cuenta = no_cuenta;
    }
    
    
    
    /**
     * Getter for property cedula_cuenta.
     * @return Value of property cedula_cuenta.
     */
    public java.lang.String getCedula_cuenta() {
        return cedula_cuenta;
    }
    
    /**
     * Setter for property cedula_cuenta.
     * @param cedula_cuenta New value of property cedula_cuenta.
     */
    public void setCedula_cuenta(java.lang.String cedula_cuenta) {
        this.cedula_cuenta = cedula_cuenta;
    }
    
    
    
    /**
     * Getter for property nombre_cuenta.
     * @return Value of property nombre_cuenta.
     */
    public java.lang.String getNombre_cuenta() {
        return nombre_cuenta;
    }
    
    /**
     * Setter for property nombre_cuenta.
     * @param nombre_cuenta New value of property nombre_cuenta.
     */
    public void setNombre_cuenta(java.lang.String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }
    
    
    
    
    /**
     * Getter for property codeBancoTransf.
     * @return Value of property codeBancoTransf.
     */
    public java.lang.String getCodeBancoTransf() {
        return codeBancoTransf;
    }
    
    /**
     * Setter for property codeBancoTransf.
     * @param codeBancoTransf New value of property codeBancoTransf.
     */
    public void setCodeBancoTransf(java.lang.String codeBancoTransf) {
        this.codeBancoTransf = codeBancoTransf;
    }
    
    
    
    /**
     * Getter for property estadoCK.
     * @return Value of property estadoCK.
     */
    public java.lang.String getEstadoCK() {
        return estadoCK;
    }
    
    /**
     * Setter for property estadoCK.
     * @param estadoCK New value of property estadoCK.
     */
    public void setEstadoCK(java.lang.String estadoCK) {
        this.estadoCK = estadoCK;
    }
    
    
    
    /**
     * Getter for property monedaBanco.
     * @return Value of property monedaBanco.
     */
    public java.lang.String getMonedaBanco() {
        return monedaBanco;
    }
    
    /**
     * Setter for property monedaBanco.
     * @param monedaBanco New value of property monedaBanco.
     */
    public void setMonedaBanco(java.lang.String monedaBanco) {
        this.monedaBanco = monedaBanco;
    }
    
    
    
    /**
     * Getter for property vlrPagar.
     * @return Value of property vlrPagar.
     */
    public double getVlrPagar() {
        return vlrPagar;
    }
    
    /**
     * Setter for property vlrPagar.
     * @param vlrPagar New value of property vlrPagar.
     */
    public void setVlrPagar(double vlrPagar) {
        this.vlrPagar = vlrPagar;
    }
    
    
    
    /**
     * Getter for property vlrNetoPago.
     * @return Value of property vlrNetoPago.
     */
    public double getVlrNetoPago() {
        return vlrNetoPago;
    }
    
    /**
     * Setter for property vlrNetoPago.
     * @param vlrNetoPago New value of property vlrNetoPago.
     */
    public void setVlrNetoPago(double vlrNetoPago) {
        this.vlrNetoPago = vlrNetoPago;
    }
    
    /**
     * Getter for property msj.
     * @return Value of property msj.
     */
    public java.lang.String getMsj() {
        return msj;
    }
    
    /**
     * Setter for property msj.
     * @param msj New value of property msj.
     */
    public void setMsj(java.lang.String msj) {
        this.msj = msj;
    }
    
    /**
     * Getter for property monedaLocal.
     * @return Value of property monedaLocal.
     */
    public java.lang.String getMonedaLocal() {
        return monedaLocal;
    }
    
    /**
     * Setter for property monedaLocal.
     * @param monedaLocal New value of property monedaLocal.
     */
    public void setMonedaLocal(java.lang.String monedaLocal) {
        this.monedaLocal = monedaLocal;
    }
    
    /**
     * Getter for property tipoPago.
     * @return Value of property tipoPago.
     */
    public java.lang.String getTipoPago() {
        return tipoPago;
    }
    
    /**
     * Setter for property tipoPago.
     * @param tipoPago New value of property tipoPago.
     */
    public void setTipoPago(java.lang.String tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    /**
     * Getter for property oc.
     * @return Value of property oc.
     */
    public java.lang.String getOc() {
        return oc;
    }
    
    /**
     * Setter for property oc.
     * @param oc New value of property oc.
     */
    public void setOc(java.lang.String oc) {
        this.oc = oc;
    }
    
    /**
     * Getter for property valor_rtfe.
     * @return Value of property valor_rtfe.
     */
    public double getValor_rtfe() {
        return valor_rtfe;
    }
    
    /**
     * Setter for property valor_rtfe.
     * @param valor_rtfe New value of property valor_rtfe.
     */
    public void setValor_rtfe(double valor_rtfe) {
        this.valor_rtfe = valor_rtfe;
    }
    
    /**
     * Getter for property valor_iva.
     * @return Value of property valor_iva.
     */
    public double getValor_iva() {
        return valor_iva;
    }
    
    /**
     * Setter for property valor_iva.
     * @param valor_iva New value of property valor_iva.
     */
    public void setValor_iva(double valor_iva) {
        this.valor_iva = valor_iva;
    }
    
    /**
     * Getter for property valor_riva.
     * @return Value of property valor_riva.
     */
    public double getValor_riva() {
        return valor_riva;
    }
    
    /**
     * Setter for property valor_riva.
     * @param valor_riva New value of property valor_riva.
     */
    public void setValor_riva(double valor_riva) {
        this.valor_riva = valor_riva;
    }
    
    /**
     * Getter for property valor_rica.
     * @return Value of property valor_rica.
     */
    public double getValor_rica() {
        return valor_rica;
    }
    
    /**
     * Setter for property valor_rica.
     * @param valor_rica New value of property valor_rica.
     */
    public void setValor_rica(double valor_rica) {
        this.valor_rica = valor_rica;
    }
    
    /**
     * Getter for property vlrTotalPagar.
     * @return Value of property vlrTotalPagar.
     */
    public double getVlrTotalPagar() {
        return vlrTotalPagar;
    }
    
    /**
     * Setter for property vlrTotalPagar.
     * @param vlrTotalPagar New value of property vlrTotalPagar.
     */
    public void setVlrTotalPagar(double vlrTotalPagar) {
        this.vlrTotalPagar = vlrTotalPagar;
    }
    
    /**
     * Getter for property monedaTR.
     * @return Value of property monedaTR.
     */
    public java.lang.String getMonedaTR() {
        return monedaTR;
    }
    
    /**
     * Setter for property monedaTR.
     * @param monedaTR New value of property monedaTR.
     */
    public void setMonedaTR(java.lang.String monedaTR) {
        this.monedaTR = monedaTR;
    }
    
    /**
     * Getter for property bancoTR.
     * @return Value of property bancoTR.
     */
    public java.lang.String getBancoTR() {
        return bancoTR;
    }
    
    /**
     * Setter for property bancoTR.
     * @param bancoTR New value of property bancoTR.
     */
    public void setBancoTR(java.lang.String bancoTR) {
        this.bancoTR = bancoTR;
    }
    
    /**
     * Getter for property sucursalTR.
     * @return Value of property sucursalTR.
     */
    public java.lang.String getSucursalTR() {
        return sucursalTR;
    }
    
    /**
     * Setter for property sucursalTR.
     * @param sucursalTR New value of property sucursalTR.
     */
    public void setSucursalTR(java.lang.String sucursalTR) {
        this.sucursalTR = sucursalTR;
    }
    
    /**
     * Getter for property valorTR.
     * @return Value of property valorTR.
     */
    public double getValorTR() {
        return valorTR;
    }
    
    /**
     * Setter for property valorTR.
     * @param valorTR New value of property valorTR.
     */
    public void setValorTR(double valorTR) {
        this.valorTR = valorTR;
    }
    
    /**
     * Getter for property transferencia.
     * @return Value of property transferencia.
     */
    public java.lang.String getTransferencia() {
        return transferencia;
    }
    
    /**
     * Setter for property transferencia.
     * @param transferencia New value of property transferencia.
     */
    public void setTransferencia(java.lang.String transferencia) {
        this.transferencia = transferencia;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }    
   
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }    
    
    /**
     * Getter for property valorTRLocal.
     * @return Value of property valorTRLocal.
     */
    public double getValorTRLocal() {
        return valorTRLocal;
    }
    
    /**
     * Setter for property valorTRLocal.
     * @param valorTRLocal New value of property valorTRLocal.
     */
    public void setValorTRLocal(double valorTRLocal) {
        this.valorTRLocal = valorTRLocal;
    }
    
}
