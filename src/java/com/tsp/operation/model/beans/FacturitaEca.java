/*
 * FacturitaEca.java
 * Created on 7 de marzo de 2009, 10:18
 */

package com.tsp.operation.model.beans;

public class FacturitaEca {

  private String  fact_conformada;

    /** Creates a new instance of FacturaGeneral */
    public FacturitaEca() {
    }

    public static FacturitaEca load(java.sql.ResultSet rs)throws java.sql.SQLException{

        FacturitaEca facturitaEca = new FacturitaEca();
        
        facturitaEca.setFact_conformada(rs.getString("facturitaEca") );
        
        return facturitaEca;
    }    

    /**
     * @return the fact_conformada
     */
    public String getFact_conformada() {
        return fact_conformada;
    }

    /**
     * @param fact_conformada the fact_conformada to set
     */
    public void setFact_conformada(String fact_conformada) {
        this.fact_conformada = fact_conformada;
    }


}
