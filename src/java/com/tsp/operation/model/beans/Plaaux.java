package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Plaaux implements Serializable {
    
    private String dstrct;
    private String planilla;
    private float full_weight;
    private float empty_weight;
    private float full_weight_m;
    private float empty_weight_m;
    private float acpm_gln;
    private String acpm_supplier;
    private float ticket_a;
    private float ticket_b;
    private float ticket_c;
    private String ticket_supplier;
    private String ref_1;
    private java.util.Date creation_date;
    private String creation_user;
    private String placa;
    private String acpm_code;
    private String tiket_code;
    private String remision;
    private String sj;
    private String conductor;
    
    public static Plaaux load(ResultSet rs)throws SQLException{
        
        Plaaux plaaux = new Plaaux();
        
        plaaux.setDstrct(rs.getString("dstrct"));
        plaaux.setPlanilla(rs.getString("pla"));
        plaaux.setFull_weight(rs.getFloat("full_weight"));
        plaaux.setEmpty_weight(rs.getFloat("empty_weight"));
        plaaux.setAcpm_gln(rs.getFloat("acpm_gln"));
        plaaux.setAcpm_supplier(rs.getString("acpm_supplier"));
        plaaux.setTicket_a(rs.getFloat("ticket_a"));
        plaaux.setTicket_b(rs.getFloat("ticket_b"));
        plaaux.setTicket_c(rs.getFloat("ticket_c"));
        plaaux.setTicket_supplier(rs.getString("ticket_supplier"));
        plaaux.setRef_1(rs.getString("ref_1"));
        plaaux.setPlaca(rs.getString("plaveh"));
        plaaux.setAcpm_code(rs.getString("acpm_code"));
       // plaaux.setRemision(rs.getString("remision"));
        
        return plaaux;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    public void setAcpm_code(String acpm_code){
        
        this.acpm_code=acpm_code;
    }
    public void setSj(String sj){
        
        this.sj=sj;
    }
    public void setTiket_code(String tiket_code){
        
        this.tiket_code=tiket_code;
    }
    public String getAcpm_code(){
        
        return acpm_code;
    }
    public String getTiket_code(){
        
        return tiket_code;
    }
    
    public void setPlaca(String placa){
        
        this.placa=placa;
    }
    
     public String getRemision(){
        
        return remision;
    }
    public void setRemision(String remision){
        
        this.remision=remision;
    }
    
    public String getPlaca(){
        
        return placa;
    }
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    public String getSj(){
        
        return sj;
    }
    public void	setPlanilla(String planilla){
        
        this.planilla=planilla;
    }
    
    public String getPlanilla(){
        
        return planilla;
    }
    public void setFull_weight_m(float full_weight_m){
        
        this.full_weight_m=full_weight_m;
    }
    
    public float getFull_weight_m(){
        
        return full_weight_m;
    }
    
    public void setEmpty_weight_m(float empty_weight_m){
        
        this.empty_weight_m=empty_weight_m;
    }
    public float getEmpty_weight_m(){
        
        return empty_weight_m;
    }
    
    public void setFull_weight(float full_weight){
        
        this.full_weight=full_weight;
    }
    
    public float getFull_weight(){
        
        return full_weight;
    }
    
    public void setEmpty_weight(float empty_weight){
        
        this.empty_weight=empty_weight;
    }
    
    public float getEmpty_weight(){
        
        return empty_weight;
    }
    
    public void setAcpm_gln(float acpm_gln){
        
        this.acpm_gln=acpm_gln;
    }
    
    public float getAcpm_gln(){
        
        return acpm_gln;
    }
    
    public void setAcpm_supplier(String acpm_supplier){
        
        this.acpm_supplier=acpm_supplier;
    }
    
    public String getAcpm_supplier(){
        
        return acpm_supplier;
    }
    
    public void setTicket_a(float ticket_a){
        
        this.ticket_a=ticket_a;
    }
    
    public float getTicket_a(){
        
        return ticket_a;
    }
    
    public void setTicket_b(float ticket_b){
        
        this.ticket_b=ticket_b;
    }
    
    public float getTicket_b(){
        
        return ticket_b;
    }
    
    public void setTicket_c(float ticket_c){
        
        this.ticket_c=ticket_c;
    }
    
    public float getTicket_c(){
        
        return ticket_c;
    }
    
    public void setTicket_supplier(String ticket_supplier){
        
        this.ticket_supplier=ticket_supplier;
    }
    
    public String getTicket_supplier(){
        
        return ticket_supplier;
    }
    
    public void setRef_1(String ref_1){
        
        this.ref_1=ref_1;
    }
    
    public String getRef_1(){
        
        return ref_1;
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
}











