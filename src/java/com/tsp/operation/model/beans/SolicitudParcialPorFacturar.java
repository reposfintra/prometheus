/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class SolicitudParcialPorFacturar {

    private String id_solicitud;
    private int    parcial;
    private double valor_a_financiar;
    private double intereses;
    private double valor_con_financiacion;




    /** Creates a new instance of SubclientePorFacturar */
    public SolicitudParcialPorFacturar() {
    }


    public static SolicitudParcialPorFacturar load(java.sql.ResultSet rs)throws java.sql.SQLException{

        SolicitudParcialPorFacturar solicitudParcialPorFacturar = new SolicitudParcialPorFacturar();

        solicitudParcialPorFacturar.setId_solicitud(rs.getString("id_solicitud") );
        solicitudParcialPorFacturar.setParcial(rs.getInt("parcial") );
        solicitudParcialPorFacturar.setValor_a_financiar(rs.getDouble("valor_a_financiar") );
        solicitudParcialPorFacturar.setIntereses(rs.getDouble("intereses") );
        solicitudParcialPorFacturar.setValor_con_financiacion(rs.getDouble("valor_con_financiacion") );

        return solicitudParcialPorFacturar;

    }






    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the valor_a_financiar
     */
    public double getValor_a_financiar() {
        return valor_a_financiar;
    }

    /**
     * @param valor_a_financiar the valor_a_financiar to set
     */
    public void setValor_a_financiar(double valor_a_financiar) {
        this.valor_a_financiar = valor_a_financiar;
    }

    /**
     * @return the intereses
     */
    public double getIntereses() {
        return intereses;
    }

    /**
     * @param intereses the intereses to set
     */
    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }

    /**
     * @return the valor_con_financiacion
     */
    public double getValor_con_financiacion() {
        return valor_con_financiacion;
    }

    /**
     * @param valor_con_financiacion the valor_con_financiacion to set
     */
    public void setValor_con_financiacion(double valor_con_financiacion) {
        this.valor_con_financiacion = valor_con_financiacion;
    }



}
