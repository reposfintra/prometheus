/*
 * AutorizacionFlete.java
 *
 * Created on 30 de agosto de 2005, 03:10 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  kreales
 */
public class AutorizacionFlete {
    
    private String reg_status;
    private String dstrct;
    private String item;
    private String agency_id;
    private String planilla;
    private String placa;
    private String pla_owner;
    private String cedcond;
    private String fecha_req;
    private String standard;
    private float nuevoflete;
    private String unidad;
    private String justificacion;
    private String despachador;
    private String creation_user;
    private String autorizador;
    private String email_autorizador;
    private String base;
    private float viejoflete;
    //CAMPOS NECESARIOS PARA ENVIAR UN EMAIL
    private String MyEmail;
    private String subject;
    private String body;
    private String myName;
    private String emailDesp="";
    private String numsol="";
    private boolean autorizada=false;
    /** Creates a new instance of AutorizacionFlete */
    public AutorizacionFlete() {
    }
    
    /**
     * Getter for property agency_id.
     * @return Value of property agency_id.
     */
    public java.lang.String getAgency_id() {
        return agency_id;
    }
    
    /**
     * Setter for property agency_id.
     * @param agency_id New value of property agency_id.
     */
    public void setAgency_id(java.lang.String agency_id) {
        this.agency_id = agency_id;
    }
    
    /**
     * Getter for property autorizador.
     * @return Value of property autorizador.
     */
    public java.lang.String getAutorizador() {
        return autorizador;
    }
    
    /**
     * Setter for property autorizador.
     * @param autorizador New value of property autorizador.
     */
    public void setAutorizador(java.lang.String autorizador) {
        this.autorizador = autorizador;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property cedcond.
     * @return Value of property cedcond.
     */
    public java.lang.String getCedcond() {
        return cedcond;
    }
    
    /**
     * Setter for property cedcond.
     * @param cedcond New value of property cedcond.
     */
    public void setCedcond(java.lang.String cedcond) {
        this.cedcond = cedcond;
    }
    
    /**
     * Getter for property despachador.
     * @return Value of property despachador.
     */
    public java.lang.String getDespachador() {
        return despachador;
    }
    
    /**
     * Setter for property despachador.
     * @param despachador New value of property despachador.
     */
    public void setDespachador(java.lang.String despachador) {
        this.despachador = despachador;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property email_autorizador.
     * @return Value of property email_autorizador.
     */
    public java.lang.String getEmail_autorizador() {
        return email_autorizador;
    }
    
    /**
     * Setter for property email_autorizador.
     * @param email_autorizador New value of property email_autorizador.
     */
    public void setEmail_autorizador(java.lang.String email_autorizador) {
        this.email_autorizador = email_autorizador;
    }
    
    /**
     * Getter for property fecha_req.
     * @return Value of property fecha_req.
     */
    public java.lang.String getFecha_req() {
        return fecha_req;
    }
    
    /**
     * Setter for property fecha_req.
     * @param fecha_req New value of property fecha_req.
     */
    public void setFecha_req(java.lang.String fecha_req) {
        this.fecha_req = fecha_req;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property justificacion.
     * @return Value of property justificacion.
     */
    public java.lang.String getJustificacion() {
        return justificacion;
    }
    
    /**
     * Setter for property justificacion.
     * @param justificacion New value of property justificacion.
     */
    public void setJustificacion(java.lang.String justificacion) {
        this.justificacion = justificacion;
    }
    
    /**
     * Getter for property nuevoflete.
     * @return Value of property nuevoflete.
     */
    public float getNuevoflete() {
        return nuevoflete;
    }
    
    /**
     * Setter for property nuevoflete.
     * @param nuevoflete New value of property nuevoflete.
     */
    public void setNuevoflete(float nuevoflete) {
        this.nuevoflete = nuevoflete;
    }
    
    /**
     * Getter for property pla_owner.
     * @return Value of property pla_owner.
     */
    public java.lang.String getPla_owner() {
        return pla_owner;
    }
    
    /**
     * Setter for property pla_owner.
     * @param pla_owner New value of property pla_owner.
     */
    public void setPla_owner(java.lang.String pla_owner) {
        this.pla_owner = pla_owner;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property standard.
     * @return Value of property standard.
     */
    public java.lang.String getStandard() {
        return standard;
    }
    
    /**
     * Setter for property standard.
     * @param standard New value of property standard.
     */
    public void setStandard(java.lang.String standard) {
        this.standard = standard;
    }
    
    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property body.
     * @return Value of property body.
     */
    public java.lang.String getBody() {
        return body;
    }
    
    /**
     * Setter for property body.
     * @param body New value of property body.
     */
    public void setBody(java.lang.String body) {
        this.body = body;
    }
    
    /**
     * Getter for property MyEmail.
     * @return Value of property MyEmail.
     */
    public java.lang.String getMyEmail() {
        return MyEmail;
    }
    
    /**
     * Setter for property MyEmail.
     * @param MyEmail New value of property MyEmail.
     */
    public void setMyEmail(java.lang.String MyEmail) {
        this.MyEmail = MyEmail;
    }
    
    /**
     * Getter for property myName.
     * @return Value of property myName.
     */
    public java.lang.String getMyName() {
        return myName;
    }
    
    /**
     * Setter for property myName.
     * @param myName New value of property myName.
     */
    public void setMyName(java.lang.String myName) {
        this.myName = myName;
    }
    
    /**
     * Getter for property subject.
     * @return Value of property subject.
     */
    public java.lang.String getSubject() {
        return subject;
    }
    
    /**
     * Setter for property subject.
     * @param subject New value of property subject.
     */
    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }
    
    /**
     * Getter for property viejoflete.
     * @return Value of property viejoflete.
     */
    public float getViejoflete() {
        return viejoflete;
    }
    
    /**
     * Setter for property viejoflete.
     * @param viejoflete New value of property viejoflete.
     */
    public void setViejoflete(float viejoflete) {
        this.viejoflete = viejoflete;
    }
    
    /**
     * Getter for property emailDesp.
     * @return Value of property emailDesp.
     */
    public java.lang.String getEmailDesp() {
        return emailDesp;
    }
    
    /**
     * Setter for property emailDesp.
     * @param emailDesp New value of property emailDesp.
     */
    public void setEmailDesp(java.lang.String emailDesp) {
        this.emailDesp = emailDesp;
    }
    
    /**
     * Getter for property numsol.
     * @return Value of property numsol.
     */
    public java.lang.String getNumsol() {
        return numsol;
    }
    
    /**
     * Setter for property numsol.
     * @param numsol New value of property numsol.
     */
    public void setNumsol(java.lang.String numsol) {
        this.numsol = numsol;
    }
    
    /**
     * Getter for property autorizada.
     * @return Value of property autorizada.
     */
    public boolean isAutorizada() {
        return autorizada;
    }
    
    /**
     * Setter for property autorizada.
     * @param autorizada New value of property autorizada.
     */
    public void setAutorizada(boolean autorizada) {
        this.autorizada = autorizada;
    }
    
}
