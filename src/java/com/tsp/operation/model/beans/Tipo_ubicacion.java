/*
 * Nombre        Ubicacion.java
 * Autor         Ing. Jose de la Rosa
 * Fecha         17 Junio 2005
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Tipo_ubicacion implements Serializable{
    
    private String codigo;
    private String descripcion;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    
    
    /**
     * Metodo <tt>load</tt>, instacioa un tipo de ubicacion
     * @autor : Ing. Jose de la Rosa
     * @param ResultSet
     * @return Tipo_Ubicacion
     * @version : 1.0
     */
    public static Tipo_ubicacion load(ResultSet rs)throws SQLException{
        Tipo_ubicacion t = new Tipo_ubicacion();
        t.setCodigo(rs.getString("codigo"));
        t.setDescripcion(rs.getString("descripcion"));
        t.setCia(rs.getString("cia"));
        t.setRec_status(rs.getString("rec_status"));
        t.setLast_update(rs.getDate("last_update"));
        t.setUser_update(rs.getString("user_update"));
        t.setCreation_date(rs.getDate("creation_date"));
        t.setCreation_user(rs.getString("creation_user"));
        return t;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia
     */ 
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia
     * @param base New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo
     */ 
    public java.lang.String getCodigo() {
        return codigo;
    }

    /**
     * Setter for property codigo
     * @param base New value of property codigo
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date
     */ 
    public java.util.Date getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date
     * @param base New value of property creation_date
     */
    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user
     */ 
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user
     * @param base New value of property creation_user
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion
     */ 
    public java.lang.String getDescripcion() {
        return descripcion;
    }

    /**
     * Setter for property descripcion
     * @param base New value of property descripcion
     */    
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Getter for property last_update.
     * @return Value of property last_update
     */ 
    public java.util.Date getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update
     * @param base New value of property last_update
     */
    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status
     */ 
    public java.lang.String getRec_status() {
        return rec_status;
    }

    /**
     * Setter for property rec_status
     * @param base New value of property cod_cia.
     */    
    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }

    /**
     * Getter for property user_update.
     * @return Value of property user_update
     */ 
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param base New value of property user_update
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
}
