/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class ParmetrosDinamicaTSP {

    private String paso;
    private String filtro_general;
    private String periodo_inicio;
    private String periodo_fin;
    private String tipo_anticipo;
    private String insertLogProcesos;
    private String updateLogProcesos;
    private String nombreLogProceso;
    private String estadoReporte;
    private String tercero;
    private String modulo;
    /**
     * Variable estatica con nombre de query
     */
    public static final String queryHilo = "SQL_DETALLE_MOVIMIENTO_TSP";
    public static final String queryLogProcesos = "SQL_INSERT_LOG_PROCESOS";
    public static final String queryUpLogProcesos = "SQL_UPDATE_LOG_PROCESOS";

    public ParmetrosDinamicaTSP() {
    }

    /**
     * @return the paso
     */
    public String getPaso() {
        return paso;
    }

    /**
     * @param paso the paso to set
     */
    public void setPaso(String paso) {
        this.paso = paso;
    }

    /**
     * @return the filtro_general
     */
    public String getFiltro_general() {
        switch (filtro_general) {
            case "aniocorrido":
                filtro_general = "1";
                break;
            case "anioanterior":
                filtro_general = "2";
                break;
            case "seismeses":
                filtro_general = "3";
                break;
            case "otro":
                filtro_general = "4";
                break;
            case "docemeses":
                filtro_general = "5";
                break;
            case "mespasado":
                filtro_general = "6";
                break;
            case "mespresente":
                filtro_general = "7";
                break;
        }
        return filtro_general;
    }

    /**
     * @param filtro_general the filtro_general to set
     */
    public void setFiltro_general(String filtro_general) {
        this.filtro_general = filtro_general;
    }

    /**
     * @return the periodo_inicio
     */
    public String getPeriodo_inicio() {
        return periodo_inicio;
    }

    /**
     * @param periodo_inicio the periodo_inicio to set
     */
    public void setPeriodo_inicio(String periodo_inicio) {
        this.periodo_inicio = periodo_inicio;
    }

    /**
     * @return the periodo_fin
     */
    public String getPeriodo_fin() {
        return periodo_fin;
    }

    /**
     * @param periodo_fin the periodo_fin to set
     */
    public void setPeriodo_fin(String periodo_fin) {
        this.periodo_fin = periodo_fin;
    }

    /**
     * @return the tipo_anticipo
     */
    public String getTipo_anticipo() {
        return tipo_anticipo;
    }

    /**
     * @param tipo_anticipo the tipo_anticipo to set
     */
    public void setTipo_anticipo(String tipo_anticipo) {
        this.tipo_anticipo = tipo_anticipo;
    }

    /**
     * @return the insertLogProcesos
     */
    public String getInsertLogProcesos() {
        return insertLogProcesos;
    }

    /**
     * @param insertLogProcesos the insertLogProcesos to set
     */
    public void setInsertLogProcesos(String insertLogProcesos) {
        this.insertLogProcesos = insertLogProcesos;
    }

    /**
     * @return the updateLogProcesos
     */
    public String getUpdateLogProcesos() {
        return updateLogProcesos;
    }

    /**
     * @param updateLogProcesos the updateLogProcesos to set
     */
    public void setUpdateLogProcesos(String updateLogProcesos) {
        this.updateLogProcesos = updateLogProcesos;
    }

    public String getNombreLogProceso() {
        return nombreLogProceso;
    }

    public void setNombreLogProceso(String nombreLogProceso) {
        this.nombreLogProceso = nombreLogProceso;
    }

    public String getEstadoReporte() {
        return estadoReporte;
    }

    public void setEstadoReporte(String estadoReporte) {
        this.estadoReporte = estadoReporte;
    }

    public String getTercero() {
        return tercero;
    }

    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

}
