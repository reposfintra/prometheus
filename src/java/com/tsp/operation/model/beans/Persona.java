package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Bean para la tabla persona<br/>
 * 24/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Persona {

    private String tipoIdentificacion;
    private String identificacion;
    private String estadoId;
    private Timestamp fechaExpedicionId;
    private String ciudadId;
    private String departamentoId;
    private String nombre;
    private String nombreCompleto;
    private String primerApellido;
    private String segundoApellido;
    private String nacionalidad;
    private String genero;
    private String estadoCivil;
    private Boolean validada;
    private String edadMin;
    private String edadMax;
    private String creationUser;
    private String userUpdate;
    private String webService;
    private Timestamp ultima_hc;
    private String nitEmpresa;

    public Persona() {
    }

    public Persona(String tipoIdentificacion, String identificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
        this.identificacion = identificacion;
    }

    /**
     * Get the value of ultima_hc
     *
     * @return the value of ultima_hc
     */
    public Timestamp getUltima_hc() {
        return ultima_hc;
    }

    /**
     * Set the value of ultima_hc
     *
     * @param ultima_hc new value of ultima_hc
     */
    public void setUltima_hc(Timestamp ultima_hc) {
        this.ultima_hc = ultima_hc;
    }

    /**
     * Get the value of webService
     *
     * @return the value of webService
     */
    public String getWebService() {
        return webService;
    }

    /**
     * Set the value of webService
     *
     * @param webService new value of webService
     */
    public void setWebService(String webService) {
        this.webService = webService;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of edadMax
     *
     * @return the value of edadMax
     */
    public String getEdadMax() {
        return edadMax;
    }

    /**
     * Set the value of edadMax
     *
     * @param edadMax new value of edadMax
     */
    public void setEdadMax(String edadMax) {
        this.edadMax = edadMax;
    }


    /**
     * Get the value of edadMin
     *
     * @return the value of edadMin
     */
    public String getEdadMin() {
        return edadMin;
    }

    /**
     * Set the value of edadMin
     *
     * @param edadMin new value of edadMin
     */
    public void setEdadMin(String edadMin) {
        this.edadMin = edadMin;
    }

    /**
     * Get the value of validada
     *
     * @return the value of validada
     */
    public Boolean getValidada() {
        return validada;
    }

    /**
     * Set the value of validada
     *
     * @param validada new value of validada
     */
    public void setValidada(boolean validada) {
        this.validada = validada;
    }

    /**
     * Get the value of genero
     *
     * @return the value of genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * Set the value of genero
     *
     * @param genero new value of genero
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * Get the value of estadoCivil
     *
     * @return the value of estadoCivil
     */
    public String getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Set the value of estadoCivil
     *
     * @param estadoCivil new value of estadoCivil
     */
    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }


    /**
     * Get the value of nacionalidad
     *
     * @return the value of nacionalidad
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     * Set the value of nacionalidad
     *
     * @param nacionalidad new value of nacionalidad
     */
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }


    /**
     * Get the value of segundoApellido
     *
     * @return the value of segundoApellido
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     * Set the value of segundoApellido
     *
     * @param segundoApellido new value of segundoApellido
     */
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }


    /**
     * Get the value of primerApellido
     *
     * @return the value of primerApellido
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * Set the value of primerApellido
     *
     * @param primerApellido new value of primerApellido
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }


    /**
     * Get the value of nombreCompleto
     *
     * @return the value of nombreCompleto
     */
    public String getNombreCompleto() {
        return nombreCompleto;
    }

    /**
     * Set the value of nombreCompleto
     *
     * @param nombreCompleto new value of nombreCompleto
     */
    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    /**
     * Get the value of nombre
     *
     * @return the value of nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Set the value of nombre
     *
     * @param nombre new value of nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Get the value of departamentoId
     *
     * @return the value of departamentoId
     */
    public String getDepartamentoId() {
        return departamentoId;
    }

    /**
     * Set the value of departamentoId
     *
     * @param departamentoId new value of departamentoId
     */
    public void setDepartamentoId(String departamentoId) {
        this.departamentoId = departamentoId;
    }

    /**
     * Get the value of ciudadId
     *
     * @return the value of ciudadId
     */
    public String getCiudadId() {
        return ciudadId;
    }

    /**
     * Set the value of ciudadId
     *
     * @param ciudadId new value of ciudadId
     */
    public void setCiudadId(String ciudadId) {
        this.ciudadId = ciudadId;
    }


    /**
     * Get the value of fechaExpedicionId
     *
     * @return the value of fechaExpedicionId
     */
    public Timestamp getFechaExpedicionId() {
        return fechaExpedicionId;
    }

    /**
     * Set the value of fechaExpedicionId
     *
     * @param fechaExpedicionId new value of fechaExpedicionId
     */
    public void setFechaExpedicionId(Timestamp fechaExpedicionId) {
        this.fechaExpedicionId = fechaExpedicionId;
    }

    /**
     * Get the value of estadoId
     *
     * @return the value of estadoId
     */
    public String getEstadoId() {
        return estadoId;
    }

    /**
     * Set the value of estadoId
     *
     * @param estadoId new value of estadoId
     */
    public void setEstadoId(String estadoId) {
        this.estadoId = estadoId;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }
    
    public Persona Load(ResultSet rs) throws SQLException{
        tipoIdentificacion=rs.getString("tipo_identificacion");
        identificacion=rs.getString("identificacion");
        estadoId=rs.getString("estado");
        fechaExpedicionId=rs.getTimestamp("fecha_expedicion_id");
        ciudadId=rs.getString("ciudad_id");
        departamentoId=rs.getString("departamento_id");
        nombre=rs.getString("nombre");
        nombreCompleto=rs.getString("nombre_completo");
        primerApellido=rs.getString("primer_apellido");
        segundoApellido=rs.getString("segundo_apellido");
        nacionalidad=rs.getString("nacionalidad");
        genero=rs.getString("genero");
        estadoCivil=rs.getString("estado_civil");
        validada=rs.getBoolean("validada");
        edadMin=rs.getString("edad_min");
        edadMax=rs.getString("edad_max");
        ultima_hc=rs.getTimestamp("ultima_hc");
        nitEmpresa=rs.getString("nit_empresa");
        return this;
    }

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

}
