package com.tsp.operation.model.beans;

import java.io.*;

public class Remision {
    
 //Atributos   
    private int    id;
    private boolean seleted;
    private boolean printer;
    
    private String distrito;
    private String remision;  
    private String noRemision;
    private String placa;
    private String remesa;
    private double cantidadCarga;
    private String unidadValorizacion;
    private String carga;
    private String origen;
    private String destino;
    private String contrato;
    private String conductor;
    private String nit;
    private String fechaEntrada;
    private String propietario;
    private double cantidadPeajes;
    private double valorPeajes;
    private String fechaCumplido;
    private double cantidadACPM;
    private double valorACPM;
    private String proveedorACPM;
    private double anticipo;
    private double total;
    private String numeroEgreso;
    private String banco;
    private String cuentaBanco;
    private String cheque;
    private String textoOC;
    private String usuario;
    private double valor1;
    private double valor2;
    private String tipoMoneda;
    private String ageBanco;
    private String agencia;
    
    private String prefix;
    private int posicion;
    private String egreso;
    
    private String color;
    private String fechaHoy;
    
    private String proveedorAnticipo;
    private String proveedorPeaje;
    private String monto;
    private String sucAnticipo;
    private String sucACPM;
    private String sucPeaje;
    
 //M�todos
  
    //SET
    
   public void setNoRemision(String no){
       this.noRemision = no;
   }
   
   public void  setSeleted(boolean estado) {
       this.seleted = estado;
   }
   
   public void setPrinter(boolean estado){
       this.printer = estado;
   }
   
   public void setId(int id){
       this.id = id;
   }
   
   public void setAgencia(String age){
     this.agencia=age;   
   }
   
   public void setAgeBanco(String age){
       this.ageBanco=age;
   }
   
  public void setValor1(double valor){
      this.valor1=valor;
  }
  
  public void setValor2(double valor){
      this.valor2=valor;
  }
  
  public void setTipoMoneda(String tipo){
      this.tipoMoneda=tipo;
  }
   public void setNit(String nit){
     this.nit=nit;    
   }
   
    public void  setUsuario(String usuario){
      this.usuario=usuario;  
    }
        
    public void setDistrito(String distrito){
       this.distrito=distrito;    
    }
    
    public void setRemision(String oc){
      this.remision=oc;  
    }
         
    public void setPlaca(String placa){
      this.placa=placa;
    }
    
    public void setConductor(String conductor){
      this.conductor=conductor;
    }
    
    public void setRemesa(String remesa){
      this.remesa=remesa;
    }
   
    public void setCantidadCarga(double cantidadCarga){
      this.cantidadCarga=cantidadCarga;     
    }
   
    public void setUnidadValorizacion(String unidadValorizacion){
        this.unidadValorizacion=unidadValorizacion;
    }
   
    public void setCarga(String carga){
        this.carga=carga;
    }
           
    public void setOrigen(String origen){
        this.origen=origen;
    }
    
    public void setDestino(String destino){
        this.destino=destino;
    }
         
    public void setcontrato(String contrato){
        this.contrato=contrato;
    }
    
    public void setFechaEntrada(String fechaEntrada){
        this.fechaEntrada=fechaEntrada;
    }
   
    public void setPropietario(String propietario){
        this.propietario=propietario;
    }
    
    public void setCantidadPeajes(double cantidadpeajes){
        this.cantidadPeajes=cantidadpeajes;
    }
    
    public void setValorPeajes(double valorPeajes){
       this.valorPeajes=valorPeajes;    
    }
    
    public void setFechaCumplido(String fechaCumplido){
        this.fechaCumplido=fechaCumplido;
    }
    
    public void setcantidadACPM(double cantidadACPM){
        this.cantidadACPM=cantidadACPM;
    }
  
    public void setValorACPM(double valorACPM){
        this.valorACPM=valorACPM;
    }
  
    public void setProveedorACPM(String proveedorACPM){
        this.proveedorACPM=proveedorACPM;
    }
  
    public void setanticipo(double anticipo){
        this.anticipo=anticipo;
    }
  
    public void setTotal(double valor){
        this.total = valor;
    }
    
    public void setNumeroEgreso(String numeroEgreso){
        this.numeroEgreso=numeroEgreso;
    }
    public void setBanco(String banco){
        this.banco=banco;
    }
  
    public void setCuentaBanco(String cuentaBanco){
        this.cuentaBanco=cuentaBanco;
    }
  
    public void setCheque(String cheque){
        this.cheque=cheque;
    }
    
    public void setTextoOc(String textoOC){
        this.textoOC=textoOC;
    }
    
    
    public void setPrefix(String prefijo){
        this.prefix=prefijo;
    }
    
    
    public void setPosicion(int posicion){
        this.posicion=posicion;
    }
    
    
    public void setEgreso(String egreso){
        this.egreso=egreso;
    }
    
    
    public void setColor(String color){
        this.color=color;
    }
    
    public void setFechaHoy(String fecha){
        this.fechaHoy=fecha;
    }
    
    
    public void setProveedorpeaje(String name){
        this.proveedorPeaje = name;
    }
    
       
    public void setMonto(String letras){
        this.monto = letras;
    }
   
    public void setProveedorAnticipo(String name){
        this.proveedorAnticipo = name;
    }
     
     public void setSucursalAnticipo(String sucursal){
         this.sucAnticipo = sucursal;
     }
     
     public void setSucursalACPM(String sucursal){
         this.sucACPM = sucursal;
     }
    
     public void setSucursalPeaje(String sucursal){
         this.sucPeaje = sucursal;
     }
     
     
     
    //GET
     
 public double getTotal(){
      return this.total;
 }
 
 public String getNoRemision(){
      return  this.noRemision ;
 }   
    
  public boolean getSeleted() {
      return  this.seleted;
 }
   
 public boolean getPrinter(){
      return this.printer;
 }
    
  public int getId(){
      return  this.id;
 }
   
    
  public double getValor1(){
      return this.valor1;
  }
  
  public double getValor2(){
      return this.valor2;
  }
  
  public String getTipoMoneda(){
     return this.tipoMoneda;
  }
   public String getNit(){
     return this.nit;    
   }
   
    public String getUsuario(){
      return this.usuario;  
    }
    
    public String getDistrito(){
       return this.distrito;    
    }
    
    public String getRemision(){
      return this.remision;  
    }
        
    public String getPlaca(){
      return this.placa;
    }
    
    public String getConductor(){
      return this.conductor;
    }
   
    public String getRemesa(){
      return this.remesa;
    }
    
    public double getCantidadCarga(){
      return this.cantidadCarga;     
    }
   
    public String getUnidadValorizacion(){
      return this.unidadValorizacion;
    }
   
    public String getCarga(){
      return this.carga;
    }
           
    public String getOrigen(){
       return this.origen;
    }
    
    public String getDestino(){
       return this.destino;
    }
         
    public String getcontrato(){
       return this.contrato;
    }
    
    public String getFechaEntrada(){
       return this.fechaEntrada;
    }
   
    public String getPropietario(){
       return this.propietario;
    }
    
    public double getCantidadPeajes(){
       return this.cantidadPeajes;
    }
    
    public double getValorPeajes(){
      return this.valorPeajes;    
    }
    
    public String getFechaCumplido(){
      return this.fechaCumplido;
    }
    
    public double getcantidadACPM(){
      return this.cantidadACPM;
    }
   
    public double getValorACPM(){
       return this.valorACPM;
    }
  
    public String getProveedorACPM(){
       return this.proveedorACPM;
    }
  
    public double getAnticipo(){     
       return this.anticipo;
    }
      
    
    public String getNumeroEgreso(){
       return this.numeroEgreso;
    }
 
    public String getBanco(){
       return this.banco;
    }
  
    public String getCuentaBanco(){
       return this.cuentaBanco;
    }
  
    public String getCheque(){
       return this.cheque;
    }
   
    public String getTextoOc(){
       return this.textoOC;
    }
   
   public String getAgencia(){
       return this.agencia;   
   }
   
   public String getAgeBanco(){
      return  this.ageBanco;
   }
    
   
  public String getPrefix(){
      return this.prefix;
  }
   
   public int getPosicion(){
      return this.posicion;
  }
   
  public String getEgreso(){
      return this.egreso;
  }
   
  
  
  public String getColor(){
      return this.color;
  }
  
  
  public String getFechaHoy(){
      return this.fechaHoy;
  }
  
  
  public String getProveedorpeaje(){
       return this.proveedorPeaje;
  }
  
  
  public String getMonto(){
       return this.monto;
  }
  
  
    public String getProveedorAnticipo(){
       return this.proveedorAnticipo;
    }
     
     public String getSucursalAnticipo(){
         return this.sucAnticipo ;
     }
     
     public String getSucursalACPM(){
         return this.sucACPM ;
     }
    
     public String getSucursalPeaje(){
         return this.sucPeaje;
     }
  
  
}
