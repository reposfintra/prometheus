
package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;

public class ListadoArchivos implements Serializable{
    private List   Listado;

    public ListadoArchivos() {
        Listado = new LinkedList();
    }
    
        public static ListadoArchivos loadReporteUtilidad ( String Base ) throws Exception {
        
        ListadoArchivos Lista = new ListadoArchivos();
        File f = null;
        f = new File(Base); 
        
        if(! f.exists() ) f.mkdir();
        
        if ( f.isDirectory () ){
            
            File []arc  =  f.listFiles(); 
            File []file =  Ordenar(arc);
            
            for ( int i = 0; i < file.length; i++ ) {
                
              Archivo fs = Archivo.Load( i, file[i] );
              
              if( file[i].getName().toUpperCase().indexOf( "REPORTEUTILIDAD" ) > -1   ) {
                  
                  Lista.Listado.add( fs ); 
                  
              }
              
            } 
            
        }
        
        return Lista;
        
    }
    public static ListadoArchivos Load(String Base) throws Exception{
        ListadoArchivos Lista = new ListadoArchivos();
        File f        = null;
        f = new File(Base); 
        if(! f.exists() ) f.mkdir();
        if (f.isDirectory()){
            File []arc  =  f.listFiles(); 
            File []file =  Ordenar(arc);
            for (int i=0;i<file.length;i++){
              Archivo fs = Archivo.Load(i, file[i] );
              Lista.Listado.add(fs);                    
            } 
        }
        return Lista;
    }
    
    
    
    public static File[] Ordenar(File []arg){
        File temp = null;
        for (int i = 0; i<arg.length;i++)     
            for (int j = i+1; j<arg.length ;j++){
                long fechaJ = arg[j].lastModified();
                long fechaI = arg[i].lastModified();
                if (fechaI < fechaJ ){
                        temp = arg[i];
                        arg[i] = arg[j];
                        arg[j] = temp; 
                }
            }
        return arg;
    }

    
    
    
    
    
    
    
     public static  void create(String Base) throws Exception{
        File f = null;
        f      = new File(Base); 
        if(! f.exists() ) f.mkdir();
    }
    
    
 
    public void delete(String Base)throws Exception{
       try{
        File f        = null;       
        f = new File(Base);
        f.delete();
       }catch(Exception e){ throw new Exception ("Delete-> " + e.getMessage());}
    }
    
    
    
    
    
    public List getListado(){ return Listado;}
    
    
}
