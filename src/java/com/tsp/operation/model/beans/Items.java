/***************************************
    * Nombre Clase ............. MigrarOP266DAO.java
    * Descripci�n  .. . . . . .  Permite Guardar inf. de los Item de facturas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.beans;


import java.io.*;


public class Items {
    
    
    private String  distrito;
    private String  inv_no;
    private String  item_no;
    private String  item_desc;
    private String  valor;
    private String  oc;
    private String  ot;
    private String  wh_tax;
    private String  cuentaContable;
    private String  centroActividad; 
    private String  retencion;
    private double  valorRetencion;    
    private String  impuesto;    
    private double  valorImpuesto;    
    private String  iva;
    private double  valorIva;    
    private String  reteiva;
    private double  valorReteiva;     
    private String  reteica;
    private double  valorReteica;     
    private String  autorizado;
    private String  autorizadoName;
    

    
    
    public Items() {
    }
    
    
     // SET:    
    
    
    public void setDistrito        (String dstrct )  { this.distrito       = dstrct; }    
    public void setFactura         (String inv_no )  { this.inv_no         = inv_no; }    
    public void setItem            (String item   )  { this.item_no        = item;   }    
    public void setItemDesc        (String desc   )  { this.item_desc      = desc;   }    
    public void setValor           (String val    )  { this.valor          = val;    }
    public void setOC              (String val    )  { this.oc             = val;    }
    public void setOT              (String val    )  { this.ot             = val;    }
    public void setWhTax           (String val    )  { this.wh_tax         = val;    }
    public void setAccountCode     (String val    )  { this.cuentaContable  = val;   }    
    public void setProjectNo       (String val    )  { this.centroActividad = val;   }
    
    public void setRetencion       (String code   )  { this.retencion      = code; }
    public void setImpuesto        (String code   )  { this.impuesto       = code; }
    public void setIva             (String code   )  { this.iva            = code; }
    public void setReteIca         (String code   )  { this.reteica        = code; }
    public void setReteIva         (String code   )  { this.reteiva        = code; }
    public void setAutorizado      (String id     )  { this.autorizado     = id;   }
    public void setAutorizadoName  (String name   )  { this.autorizadoName = name; }
    
    public void setValorRetencion  (double val    )  { this.valorRetencion = val; }
    public void setValorImpuesto   (double val    )  { this.valorImpuesto  = val; }
    public void setValorIva        (double val    )  { this.valorIva       = val; }
    public void setValorReteIca    (double val    )  { this.valorReteica   = val; }
    public void setValorReteIva    (double val    )  { this.valorReteiva   = val; }

    
    
    
    
    // GET:
    
   
    public String getDistrito        ()  { return this.distrito;        }    
    public String getFactura         ()  { return this.inv_no;          }    
    public String getItem            ()  { return this.item_no;         }    
    public String getItemDesc        ()  { return this.item_desc;       } 
    public String getValor           ()  { return this.valor;           } 
    public String getOC              ()  { return this.oc;              }
    public String getOT              ()  { return this.ot;              }
    public String getWhTax           ()  { return this.wh_tax;          }
    
    public String getRetencion       ()  { return this.retencion;       }
    public String getImpuesto        ()  { return this.impuesto;        }
    public String getIva             ()  { return this.iva ;            }
    public String getReteIca         ()  { return this.reteica ;        }
    public String getReteIva         ()  { return this.reteiva;         }
    
    public double getValorRetencion  ()  { return this.valorRetencion;  }
    public double getValorImpuesto   ()  { return this.valorImpuesto;   }
    public double getValorIva        ()  { return this.valorIva;        }
    public double getValorReteIca    ()  { return this.valorReteica;    }
    public double getValorReteIva    ()  { return this.valorReteiva;    }
    
    public String getAutorizado      ()  { return this.autorizado;      }
    public String getAutorizadoName  ()  { return this.autorizadoName;  }
    public String getAccountCode     ()  { return this.cuentaContable;  }    
    public String getProjectNo       ()  { return this.centroActividad; }
    

}
