/* * FacturaContratista.java * Created on 27 de mayo de 2009, 17:13 */
package com.tsp.operation.model.beans;
/** * @author  Fintra */
public class FacturaContratista {
    private String valor,factura_contratista,id_contratista,fecha_factura_contratista;
    
    public FacturaContratista() {
        
    }
    public String getValor() {
        return valor;
    }
    public void setValor(String x) {
        this.valor= x;
    }
    public String getFacturaContratista() {
        return factura_contratista;
    }

    public void setFacturaContratista(String x) {
        this.factura_contratista= x;
    } 
    
    public String getIdContratista() {
        return id_contratista;
    }
    public void setIdContratista(String x) {
        this.id_contratista= x;
    }
    
    public String getFecFacContratista() {
        return fecha_factura_contratista;
    }
    public void setFecFacContratista(String x) {
        this.fecha_factura_contratista= x;
    }

    public static FacturaContratista load(java.sql.ResultSet rs)throws java.sql.SQLException{
        FacturaContratista facturaContratista = new FacturaContratista();        
        facturaContratista.setValor( rs.getString("vlr_formula_provintegral") );                                       
        facturaContratista.setFacturaContratista( rs.getString("factura_contratista") );                                       
        facturaContratista.setIdContratista( rs.getString("id_contratista") );                                       
        facturaContratista.setFecFacContratista(rs.getString("fecha_factura_contratista") );                                       
        return facturaContratista;
    }
}
