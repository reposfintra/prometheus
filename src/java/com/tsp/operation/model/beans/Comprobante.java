/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class Comprobante {
    
    private String reg_status;
    private String dstrct;
    private String tipodoc;
    private String numdoc;
    private int    grupo_transaccion;
    private String sucursal;
    private String periodo;
    private String fechadoc;
    private String detalle;
    private String tercero;
    private double total_debito;
    private double total_credito;
    private int    total_items;
    private String moneda;
    private String fecha_aplicacion;
    private String aprobador;    
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String base;    
    private String usuario_aplicacion;
    private String tipo_operacion;
    private String moneda_foranea;
    private double valor_for;
    private String ref_1;
    private String ref_2;


    




    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipodoc
     */
    public String getTipodoc() {
        return tipodoc;
    }

    /**
     * @param tipodoc the tipodoc to set
     */
    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    /**
     * @return the numdoc
     */
    public String getNumdoc() {
        return numdoc;
    }

    /**
     * @param numdoc the numdoc to set
     */
    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    /**
     * @return the grupo_transaccion
     */
    public int getGrupo_transaccion() {
        return grupo_transaccion;
    }

    /**
     * @param grupo_transaccion the grupo_transaccion to set
     */
    public void setGrupo_transaccion(int grupo_transaccion) {
        this.grupo_transaccion = grupo_transaccion;
    }

    /**
     * @return the sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the fechadoc
     */
    public String getFechadoc() {
        return fechadoc;
    }

    /**
     * @param fechadoc the fechadoc to set
     */
    public void setFechadoc(String fechadoc) {
        this.fechadoc = fechadoc;
    }

    /**
     * @return the detalle
     */
    public String getDetalle() {
        return detalle;
    }

    /**
     * @param detalle the detalle to set
     */
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    /**
     * @return the tercero
     */
    public String getTercero() {
        return tercero;
    }

    /**
     * @param tercero the tercero to set
     */
    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    /**
     * @return the total_debito
     */
    public double getTotal_debito() {
        return total_debito;
    }

    /**
     * @param total_debito the total_debito to set
     */
    public void setTotal_debito(double total_debito) {
        this.total_debito = total_debito;
    }

    /**
     * @return the total_credito
     */
    public double getTotal_credito() {
        return total_credito;
    }

    /**
     * @param total_credito the total_credito to set
     */
    public void setTotal_credito(double total_credito) {
        this.total_credito = total_credito;
    }

    /**
     * @return the total_items
     */
    public int getTotal_items() {
        return total_items;
    }

    /**
     * @param total_items the total_items to set
     */
    public void setTotal_items(int total_items) {
        this.total_items = total_items;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the fecha_aplicacion
     */
    public String getFecha_aplicacion() {
        return fecha_aplicacion;
    }

    /**
     * @param fecha_aplicacion the fecha_aplicacion to set
     */
    public void setFecha_aplicacion(String fecha_aplicacion) {
        this.fecha_aplicacion = fecha_aplicacion;
    }

    /**
     * @return the aprobador
     */
    public String getAprobador() {
        return aprobador;
    }

    /**
     * @param aprobador the aprobador to set
     */
    public void setAprobador(String aprobador) {
        this.aprobador = aprobador;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the usuario_aplicacion
     */
    public String getUsuario_aplicacion() {
        return usuario_aplicacion;
    }

    /**
     * @param usuario_aplicacion the usuario_aplicacion to set
     */
    public void setUsuario_aplicacion(String usuario_aplicacion) {
        this.usuario_aplicacion = usuario_aplicacion;
    }

    /**
     * @return the tipo_operacion
     */
    public String getTipo_operacion() {
        return tipo_operacion;
    }

    /**
     * @param tipo_operacion the tipo_operacion to set
     */
    public void setTipo_operacion(String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    /**
     * @return the moneda_foranea
     */
    public String getMoneda_foranea() {
        return moneda_foranea;
    }

    /**
     * @param moneda_foranea the moneda_foranea to set
     */
    public void setMoneda_foranea(String moneda_foranea) {
        this.moneda_foranea = moneda_foranea;
    }

    /**
     * @return the valor_for
     */
    public double getValor_for() {
        return valor_for;
    }

    /**
     * @param valor_for the valor_for to set
     */
    public void setValor_for(double valor_for) {
        this.valor_for = valor_for;
    }

    /**
     * @return the ref_1
     */
    public String getRef_1() {
        return ref_1;
    }

    /**
     * @param ref_1 the ref_1 to set
     */
    public void setRef_1(String ref_1) {
        this.ref_1 = ref_1;
    }

    /**
     * @return the ref_2
     */
    public String getRef_2() {
        return ref_2;
    }

    /**
     * @param ref_2 the ref_2 to set
     */
    public void setRef_2(String ref_2) {
        this.ref_2 = ref_2;
    }
    
}
