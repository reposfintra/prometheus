/*************************************************************************************
 * Nombre clase :                   RemesaImpPDF.java                                *
 * Descripcion :                    Clase que maneja los atributos relacionados con  *
 *                                  la impresion de remesas                          *
 * Autor :                          Ing. Juan Manuel Escandon Perez                  *
 * Fecha :                          6 de enero de 2006, 02:44 PM                     *
 * Version :                        1.0                                              *
 * Copyright :                      Fintravalores S.A.                          *
 ************************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class DatosRemesaImpPDF {
    private String numero_remesa;
    private String oficina_origen;
    private String fecha;
    private String origen;
    private String remitente;
    private String consignatario;
    private String destinatario;
    private String direccion;
    private String ciudad;
    private String numpla;
    private String placas;
    private String conductor;
    private String despachador;
    private String docinternos;
    
    /*18-01-06*/
    private String sellos;
    private String textooc;
    private String distrito;
    private String fiduciaria;
    
    private Vector copias;
    
    /*JEscandon 26-01-05*/
    private String contenedores;
    
    /** Creates a new instance of RemesaImpPDF */
    public DatosRemesaImpPDF() {
    }
    
     /**
     * Metodo loadItem, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo DatosRemesaImpPDF
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static DatosRemesaImpPDF loadItem(ResultSet rs)throws Exception{
        DatosRemesaImpPDF datos = new DatosRemesaImpPDF();
        datos.setOrigen(rs.getString("origen"));
        datos.setFecha(rs.getString("fecha"));
        datos.setNumero_remesa(rs.getString("numrem"));
        datos.setNumpla(rs.getString("numpla"));
        datos.setPlacas(rs.getString("placa"));
        datos.setConductor(rs.getString("cedcon") + " - " + rs.getString("nombre"));
        datos.setRemitente(rs.getString("remitente"));
        datos.setDestinatario(rs.getString("nomdest"));
        datos.setDireccion(rs.getString("direccion"));
        datos.setCiudad(rs.getString("ciudad"));
        datos.setDespachador(rs.getString("despachador"));
        datos.setOficina_origen(rs.getString("agencia"));
        datos.setDistrito(rs.getString("dstrct"));
        datos.setFiduciaria(rs.getString("fiduciaria"));
        datos.setTextooc(rs.getString("texto_remesa"));
        datos.setSellos(rs.getString("precinto"));
        datos.setContenedores(rs.getString("contenedores"));
        return datos;
    }
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property consignatario.
     * @return Value of property consignatario.
     */
    public java.lang.String getConsignatario() {
        return consignatario;
    }
    
    /**
     * Setter for property consignatario.
     * @param consignatario New value of property consignatario.
     */
    public void setConsignatario(java.lang.String consignatario) {
        this.consignatario = consignatario;
    }
    
    /**
     * Getter for property despachador.
     * @return Value of property despachador.
     */
    public java.lang.String getDespachador() {
        return despachador;
    }
    
    /**
     * Setter for property despachador.
     * @param despachador New value of property despachador.
     */
    public void setDespachador(java.lang.String despachador) {
        this.despachador = despachador;
    }
    
    /**
     * Getter for property destinatario.
     * @return Value of property destinatario.
     */
    public java.lang.String getDestinatario() {
        return destinatario;
    }
    
    /**
     * Setter for property destinatario.
     * @param destinatario New value of property destinatario.
     */
    public void setDestinatario(java.lang.String destinatario) {
        this.destinatario = destinatario;
    }
    
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter for property docinternos.
     * @return Value of property docinternos.
     */
    public java.lang.String getDocinternos() {
        return docinternos;
    }
    
    /**
     * Setter for property docinternos.
     * @param docinternos New value of property docinternos.
     */
    public void setDocinternos(java.lang.String docinternos) {
        this.docinternos = docinternos;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property numero_remesa.
     * @return Value of property numero_remesa.
     */
    public java.lang.String getNumero_remesa() {
        return numero_remesa;
    }
    
    /**
     * Setter for property numero_remesa.
     * @param numero_remesa New value of property numero_remesa.
     */
    public void setNumero_remesa(java.lang.String numero_remesa) {
        this.numero_remesa = numero_remesa;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property oficina_origen.
     * @return Value of property oficina_origen.
     */
    public java.lang.String getOficina_origen() {
        return oficina_origen;
    }
    
    /**
     * Setter for property oficina_origen.
     * @param oficina_origen New value of property oficina_origen.
     */
    public void setOficina_origen(java.lang.String oficina_origen) {
        this.oficina_origen = oficina_origen;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property placas.
     * @return Value of property placas.
     */
    public java.lang.String getPlacas() {
        return placas;
    }
    
    /**
     * Setter for property placas.
     * @param placas New value of property placas.
     */
    public void setPlacas(java.lang.String placas) {
        this.placas = placas;
    }
    
    /**
     * Getter for property remitente.
     * @return Value of property remitente.
     */
    public java.lang.String getRemitente() {
        return remitente;
    }
    
    /**
     * Setter for property remitente.
     * @param remitente New value of property remitente.
     */
    public void setRemitente(java.lang.String remitente) {
        this.remitente = remitente;
    }
    
   
    
    /**
     * Getter for property copias.
     * @return Value of property copias.
     */
    public java.util.Vector getCopias() {
        return copias;
    }
    
    /**
     * Setter for property copias.
     * @param copias New value of property copias.
     */
    public void setCopias(java.util.Vector copias) {
        this.copias = copias;
    }
    
    /**
     * Getter for property sellos.
     * @return Value of property sellos.
     */
    public java.lang.String getSellos() {
        return sellos;
    }
    
    /**
     * Setter for property sellos.
     * @param sellos New value of property sellos.
     */
    public void setSellos(java.lang.String sellos) {
        this.sellos = sellos;
    }
    
    /**
     * Getter for property textooc.
     * @return Value of property textooc.
     */
    public java.lang.String getTextooc() {
        return textooc;
    }
    
    /**
     * Setter for property textooc.
     * @param textooc New value of property textooc.
     */
    public void setTextooc(java.lang.String textooc) {
        this.textooc = textooc;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fiduciaria.
     * @return Value of property fiduciaria.
     */
    public java.lang.String getFiduciaria() {
        return fiduciaria;
    }
    
    /**
     * Setter for property fiduciaria.
     * @param fiduciaria New value of property fiduciaria.
     */
    public void setFiduciaria(java.lang.String fiduciaria) {
        this.fiduciaria = fiduciaria;
    }
    
    /**
     * Getter for property contenedores.
     * @return Value of property contenedores.
     */
    public java.lang.String getContenedores() {
        return contenedores;
    }
    
    /**
     * Setter for property contenedores.
     * @param contenedores New value of property contenedores.
     */
    public void setContenedores(java.lang.String contenedores) {
        this.contenedores = contenedores;
    }
    
}
