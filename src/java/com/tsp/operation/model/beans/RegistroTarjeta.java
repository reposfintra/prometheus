/*
 * RegistroTarjeta.java
 *
 * Created on 4 de septiembre de 2005, 09:57 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
/**
 *
 * @author  Jm
 */
public class RegistroTarjeta {
    private String placa;
    private String num_tarjeta;
    private String cedula_conductor;
    private String distrito;
    private String usuario_creacion;
    private String usuario_modificacion;
    private String fecha_creacion;
    private String fecha_modificacion;
    private String reg_status;
    
    /** Creates a new instance of veh_alquiler */
    public RegistroTarjeta() {
        placa                   = "";
        num_tarjeta             = "";
        cedula_conductor        = "";
        distrito                = "";
        usuario_creacion        = "";
        usuario_modificacion    = "";
        fecha_creacion          = "";
        fecha_modificacion      = "";
        reg_status              = "";
    }
    
    public static RegistroTarjeta load(ResultSet rs)throws Exception{
        RegistroTarjeta datos = new RegistroTarjeta();
        datos.setPlaca(rs.getString("placa"));
        datos.setNum_tarjeta(rs.getString("num_tarjeta"));
        datos.setCedula_conductor(rs.getString("cedula"));
        datos.setDistrito(rs.getString("dstrct"));
        datos.setReg_status(rs.getString("reg_status"));
        String fecha = rs.getString("creation_date");
        if( fecha.length() > 18 )
            fecha =  fecha.substring(0,19);
        datos.setFecha_creacion(fecha);
        return datos;
    }
    
    /**
     * Getter for property cedula_ccnductor.
     * @return Value of property cedula_ccnductor.
     */
    public java.lang.String getCedula_conductor() {
        return cedula_conductor;
    }
    
    /**
     * Setter for property cedula_ccnductor.
     * @param cedula_ccnductor New value of property cedula_ccnductor.
     */
    public void setCedula_conductor(java.lang.String cedula_conductor) {
        this.cedula_conductor = cedula_conductor;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property fecha_modificacion.
     * @return Value of property fecha_modificacion.
     */
    public java.lang.String getFecha_modificacion() {
        return fecha_modificacion;
    }
    
    /**
     * Setter for property fecha_modificacion.
     * @param fecha_modificacion New value of property fecha_modificacion.
     */
    public void setFecha_modificacion(java.lang.String fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }
    
    /**
     * Getter for property num_tarjeta.
     * @return Value of property num_tarjeta.
     */
    public java.lang.String getNum_tarjeta() {
        return num_tarjeta;
    }
    
    /**
     * Setter for property num_tarjeta.
     * @param num_tarjeta New value of property num_tarjeta.
     */
    public void setNum_tarjeta(java.lang.String num_tarjeta) {
        this.num_tarjeta = num_tarjeta;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
}
