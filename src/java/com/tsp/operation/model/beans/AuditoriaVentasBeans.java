/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class AuditoriaVentasBeans {

    public String id;
    public String periodo;
    public String planilla;
    public String fecha_envio_fintra;
    public String origen;
    public String destino;
    public String valor_planilla;
    public String valor_neto_anticipo;
    public String valor_descuentos_fintra;
    public String valor_desembolsar;
    public String reanticipo;
    public String fecha_pago_fintra;
    public String fecha_corrida;
    public String id_transportadoras;
    public String transportadora;
    public String id_agencias;
    public String nombre_agencia;
    public String id_conductors;
    public String conductor;
    public String id_propietarios;
    public String cedula_propietario;
    public String propietario;
    public String id_vehiculos;
    public String placa;
    public String cedula_conductor;
    public String sucursal;
    public String num_venta;
    public String fecha_vent;
    public String nombre_eds;
    public String kilometraje;
    public String cant_reg_ventas;
    public String total_venta;
    public String valor_comision_fintra;
    public String diferencia_fechas;
    public String disponible;
    public String producto;
    public String precio_xproducto;
    public String cantidad_suministrada;

    public String getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getFecha_envio_fintra() {
        return fecha_envio_fintra;
    }

    public void setFecha_envio_fintra(String fecha_envio_fintra) {
        this.fecha_envio_fintra = fecha_envio_fintra;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getValor_planilla() {
        return valor_planilla;
    }

    public void setValor_planilla(String valor_planilla) {
        this.valor_planilla = valor_planilla;
    }

    public String getValor_neto_anticipo() {
        return valor_neto_anticipo;
    }

    public void setValor_neto_anticipo(String valor_neto_anticipo) {
        this.valor_neto_anticipo = valor_neto_anticipo;
    }

    public String getValor_descuentos_fintra() {
        return valor_descuentos_fintra;
    }

    public void setValor_descuentos_fintra(String valor_descuentos_fintra) {
        this.valor_descuentos_fintra = valor_descuentos_fintra;
    }

    public String getValor_desembolsar() {
        return valor_desembolsar;
    }

    public void setValor_desembolsar(String valor_desembolsar) {
        this.valor_desembolsar = valor_desembolsar;
    }

    public String getReanticipo() {
        return reanticipo;
    }

    public void setReanticipo(String reanticipo) {
        this.reanticipo = reanticipo;
    }

    public String getFecha_pago_fintra() {
        return fecha_pago_fintra;
    }

    public void setFecha_pago_fintra(String fecha_pago_fintra) {
        this.fecha_pago_fintra = fecha_pago_fintra;
    }

    public String getFecha_corrida() {
        return fecha_corrida;
    }

    public void setFecha_corrida(String fecha_corrida) {
        this.fecha_corrida = fecha_corrida;
    }

    public String getId_transportadoras() {
        return id_transportadoras;
    }

    public void setId_transportadoras(String id_transportadoras) {
        this.id_transportadoras = id_transportadoras;
    }

    public String getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(String transportadora) {
        this.transportadora = transportadora;
    }

    public String getId_agencias() {
        return id_agencias;
    }

    public void setId_agencias(String id_agencias) {
        this.id_agencias = id_agencias;
    }

    public String getNombre_agencia() {
        return nombre_agencia;
    }

    public void setNombre_agencia(String nombre_agencia) {
        this.nombre_agencia = nombre_agencia;
    }

    public String getId_conductors() {
        return id_conductors;
    }

    public void setId_conductors(String id_conductors) {
        this.id_conductors = id_conductors;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public String getId_propietarios() {
        return id_propietarios;
    }

    public void setId_propietarios(String id_propietarios) {
        this.id_propietarios = id_propietarios;
    }

    public String getCedula_propietario() {
        return cedula_propietario;
    }

    public void setCedula_propietario(String cedula_propietario) {
        this.cedula_propietario = cedula_propietario;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getId_vehiculos() {
        return id_vehiculos;
    }

    public void setId_vehiculos(String id_vehiculos) {
        this.id_vehiculos = id_vehiculos;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCedula_conductor() {
        return cedula_conductor;
    }

    public void setCedula_conductor(String cedula_conductor) {
        this.cedula_conductor = cedula_conductor;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getNum_venta() {
        return num_venta;
    }

    public void setNum_venta(String num_venta) {
        this.num_venta = num_venta;
    }

    public String getFecha_vent() {
        return fecha_vent;
    }

    public void setFecha_vent(String fecha_vent) {
        this.fecha_vent = fecha_vent;
    }

    public String getNombre_eds() {
        return nombre_eds;
    }

    public void setNombre_eds(String nombre_eds) {
        this.nombre_eds = nombre_eds;
    }

    public String getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(String kilometraje) {
        this.kilometraje = kilometraje;
    }

    public String getCant_reg_ventas() {
        return cant_reg_ventas;
    }

    public void setCant_reg_ventas(String cant_reg_ventas) {
        this.cant_reg_ventas = cant_reg_ventas;
    }

    public String getTotal_venta() {
        return total_venta;
    }

    public void setTotal_venta(String total_venta) {
        this.total_venta = total_venta;
    }

    public String getValor_comision_fintra() {
        return valor_comision_fintra;
    }

    public void setValor_comision_fintra(String valor_comision_fintra) {
        this.valor_comision_fintra = valor_comision_fintra;
    }

    public String getDiferencia_fechas() {
        return diferencia_fechas;
    }

    public void setDiferencia_fechas(String diferencia_fechas) {
        this.diferencia_fechas = diferencia_fechas;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPrecio_xproducto() {
        return precio_xproducto;
    }

    public void setPrecio_xproducto(String precio_xproducto) {
        this.precio_xproducto = precio_xproducto;
    }

    public String getCantidad_suministrada() {
        return cantidad_suministrada;
    }

    public void setCantidad_suministrada(String cantidad_suministrada) {
        this.cantidad_suministrada = cantidad_suministrada;
    }

}
