/********************************************************************
 *      Nombre Clase.................   ReporteRetroactivo.java
 *      Descripci�n..................   Bean del reporte de retroactivos de carb�n
 *      Autor........................   Ing. Leonardo Parodi Ponce
 *      Fecha........................   14.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;



public class ReporteRetroactivo {
    
    private String numpla;
    private String Placa;
    private java.util.Date FecPla;
    private String propietario;
    private String nit;
    private String ruta;
    private int viajes;
    private String tipo;
    private String std_job_no;
    private double toneladas;
    private double valor_retro;
    
     /**
      * Toma el valor de cada uno de los campos y los asigna.
      * @autor Ing. Leonardo Parodi Ponce
      * @param rs ResultSet de la consulta.
      * @throws SQLException
      * @version 1.1
      */
    public static ReporteRetroactivo load(ResultSet rs)throws SQLException {
        ReporteRetroactivo ReporteRetroactivo = new ReporteRetroactivo();
        ReporteRetroactivo.setNumpla( rs.getString("numpla") );
        ReporteRetroactivo.setPlaca( rs.getString("plaveh") );
        ReporteRetroactivo.setFecPla( rs.getTimestamp("fecpla") );
        ReporteRetroactivo.setNit(rs.getString("nitpro") );
        ReporteRetroactivo.setPropietario(rs.getString("nomprop") );
        ReporteRetroactivo.setRuta(rs.getString("ruta") );
        ReporteRetroactivo.setTipo(rs.getString("tipo") );
        ReporteRetroactivo.setStd_job_no(rs.getString("std_job_no"));
        ReporteRetroactivo.setToneladas(rs.getDouble("pesoreal"));
        ReporteRetroactivo.setValor_retro(rs.getDouble("valor_retro"));
        
        
        return ReporteRetroactivo;
    }
    
    
     /**
     * Setter for property Numpla.
     * @param fecha_finalizada New value of property Numpla.
     */
    public void setNumpla(String Numpla){
        
        this.numpla = Numpla;
        
    }
     /**
     * Setter for property Viajes.
     * @param fecha_finalizada New value of property Viajes.
     */
    public void setViajes(int Viajes){
        
        this.viajes = Viajes;
        
    }
    /**
     * Setter for property placa.
     * @param fecha_finalizada New value of property placa.
     */
    public void setPlaca(String placa){
        
        this.Placa = placa;
        
    }
    /**
     * Setter for property fecpla.
     * @param fecha_finalizada New value of property fecpla.
     */
    public void setFecPla(java.util.Date fecpla){
        
        this.FecPla = fecpla;
        
    }
    
    /**
     * Setter for property Nit.
     * @param fecha_finalizada New value of property Nit.
     */
    public void setNit(String Nit){
        
        this.nit = Nit;
        
    }
    /**
     * Setter for property Propietario.
     * @param fecha_finalizada New value of property Propietario.
     */
    public void setPropietario(String Propietario){
        
        this.propietario = Propietario;
        
    }
    /**
     * Setter for property Ruta.
     * @param fecha_finalizada New value of property Ruta.
     */
    public void setRuta(String Ruta){
        
        this.ruta = Ruta;
        
    }
    /**
     * Setter for property Tipo.
     * @param fecha_finalizada New value of property Tipo.
     */
    public void setTipo(String Tipo){
        
        this.tipo = Tipo;
        
    }
    /**
     * Setter for property Std_job_no.
     * @param fecha_finalizada New value of property Std_job_no.
     */
    public void setStd_job_no(String Std_job_no){
        
        this.std_job_no = Std_job_no;
        
    }
    /**
     * Setter for property Toneladas.
     * @param fecha_finalizada New value of property Toneladas.
     */
    public void setToneladas(double Toneladas){
        
        this.toneladas = Toneladas;
        
    }
    /**
     * Setter for property Valor_retro.
     * @param fecha_finalizada New value of property Valor_retro.
     */
    public void setValor_retro(double Valor_retro){
        
        this.valor_retro = Valor_retro;
        
    }
    
   /**
     * Setter for property numpla.
     * @return Value of property numpla.
     */
    public String getNumpla(){
        
        return numpla;
        
    }
    /**
     * Setter for property viajes.
     * @return Value of property viajes.
     */
    public int getViajes(){
        
        return viajes;
        
    }
    /**
     * Setter for property Placa.
     * @return Value of property Placa.
     */
    public String getPlaca(){
        
        return Placa;
        
    }
    /**
     * Setter for property numpla.
     * @return Value of property numpla.
     */
    public java.util.Date getFecPla(){
        
        return FecPla;
        
    }
    /**
     * Setter for property nit.
     * @return Value of property nit.
     */
    public String getNit(){
        
        return nit;
        
    }
    /**
     * Setter for property propietario.
     * @return Value of property propietario.
     */
    public String getPropietario(){
        
        return propietario;
        
    }
    /**
     * Setter for property ruta.
     * @return Value of property ruta.
     */
    public String getRuta(){
        
        return ruta;
        
    }
    /**
     * Setter for property tipo.
     * @return Value of property tipo.
     */
    public String getTipo(){
        
        return tipo;
        
    }
    /**
     * Setter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public String getStd_job_no(){
        
        return std_job_no;
        
    }
    /**
     * Setter for property toneladas.
     * @return Value of property toneladas.
     */
    public double getToneladas(){
        
        return toneladas;
        
    }
    /**
     * Setter for property valor_retro.
     * @return Value of property valor_retro.
     */
    public double getValor_retro(){
        
        return valor_retro;
        
    }
}
