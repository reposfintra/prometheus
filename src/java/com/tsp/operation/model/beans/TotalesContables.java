/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;



/**
 *
 * @author Alvaro
 */
public class TotalesContables implements Serializable {

    private String descripcion;
    private double valor_debito;
    private double valor_credito;
    private double valor_diferencia;


    public TotalesContables() {

    }

    public static TotalesContables load(ResultSet rs)throws Exception{

        TotalesContables totalesContables = new TotalesContables();
        totalesContables.setDescripcion(rs.getString("descripcion"));
        totalesContables.setValor_debito( rs.getDouble("valor_debito") ) ;
        totalesContables.setValor_credito( rs.getDouble("valor_credito") ) ;
        totalesContables.setValor_diferencia( rs.getDouble("valor_diferencia") ) ;

        return totalesContables;
    }


    

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the valor_debito
     */
    public double getValor_debito() {
        return valor_debito;
    }

    /**
     * @param valor_debito the valor_debito to set
     */
    public void setValor_debito(double valor_debito) {
        this.valor_debito = valor_debito;
    }

    /**
     * @return the valor_credito
     */
    public double getValor_credito() {
        return valor_credito;
    }

    /**
     * @param valor_credito the valor_credito to set
     */
    public void setValor_credito(double valor_credito) {
        this.valor_credito = valor_credito;
    }

    /**
     * @return the valor_diferencia
     */
    public double getValor_diferencia() {
        return valor_diferencia;
    }

    /**
     * @param valor_diferencia the valor_diferencia to set
     */
    public void setValor_diferencia(double valor_diferencia) {
        this.valor_diferencia = valor_diferencia;
    }



}
