 /***********************************************
 * Nombre: Descuento_clase.java
 * Descripci�n: Beans de descuento por clase equipo.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 11:00 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ***********************************************/

package com.tsp.operation.model.beans; 

public class Descuento_clase {
    private String clase_equipo;
    private String codigo_concepto;
    private String frecuencia;
    private String mes_proceso;
    private double valor_mes;
    private float porcentaje_descuento;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String base;
    private String distrito;
    private String rec_status;     
    /** Creates a new instance of Descuento_clase */
    public Descuento_clase () {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property clase_equipo.
     * @return Value of property clase_equipo.
     */
    public java.lang.String getClase_equipo () {
        return clase_equipo;
    }
    
    /**
     * Setter for property clase_equipo.
     * @param clase_equipo New value of property clase_equipo.
     */
    public void setClase_equipo (java.lang.String clase_equipo) {
        this.clase_equipo = clase_equipo;
    }
    
    /**
     * Getter for property codigo_concepto.
     * @return Value of property codigo_concepto.
     */
    public java.lang.String getCodigo_concepto () {
        return codigo_concepto;
    }
    
    /**
     * Setter for property codigo_concepto.
     * @param codigo_concepto New value of property codigo_concepto.
     */
    public void setCodigo_concepto (java.lang.String codigo_concepto) {
        this.codigo_concepto = codigo_concepto;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion () {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion (java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property frecuencia.
     * @return Value of property frecuencia.
     */
    public java.lang.String getFrecuencia () {
        return frecuencia;
    }
    
    /**
     * Setter for property frecuencia.
     * @param frecuencia New value of property frecuencia.
     */
    public void setFrecuencia (java.lang.String frecuencia) {
        this.frecuencia = frecuencia;
    }
    
    /**
     * Getter for property mes_proceso.
     * @return Value of property mes_proceso.
     */
    public java.lang.String getMes_proceso () {
        return mes_proceso;
    }
    
    /**
     * Setter for property mes_proceso.
     * @param mes_proceso New value of property mes_proceso.
     */
    public void setMes_proceso (java.lang.String mes_proceso) {
        this.mes_proceso = mes_proceso;
    }
    
    /**
     * Getter for property porcentaje_descuento.
     * @return Value of property porcentaje_descuento.
     */
    public float getPorcentaje_descuento () {
        return porcentaje_descuento;
    }
    
    /**
     * Setter for property porcentaje_descuento.
     * @param porcentaje_descuento New value of property porcentaje_descuento.
     */
    public void setPorcentaje_descuento (float porcentaje_descuento) {
        this.porcentaje_descuento = porcentaje_descuento;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status () {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status (java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property ultima_modificacion.
     * @return Value of property ultima_modificacion.
     */
    public java.lang.String getUltima_modificacion () {
        return ultima_modificacion;
    }
    
    /**
     * Setter for property ultima_modificacion.
     * @param ultima_modificacion New value of property ultima_modificacion.
     */
    public void setUltima_modificacion (java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion () {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion (java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion () {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property valor_mes.
     * @return Value of property valor_mes.
     */
    public double getValor_mes () {
        return valor_mes;
    }
    
    /**
     * Setter for property valor_mes.
     * @param valor_mes New value of property valor_mes.
     */
    public void setValor_mes (double valor_mes) {
        this.valor_mes = valor_mes;
    }
    
}
