/***********************************************************************************
 * Nombre clase : ............... AnulacionPlanilla.java                           *
 * Descripcion :................. Clase que maneja los atributos relacionados con  *
 *                                el reporte de planillas anuladas                 *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................ 23 de noviembre de 2005, 11:06 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import com.tsp.util.*;


public class AnulacionPlanilla {
    private String numpla;
    private String placa;
    private String remesa;
    private String cliente;
    private String descripcion;
    private String fechaanulacion;
    private String fechacreacion;
    private String diferencia;
    private double valoranticipo;
    private String banco;
    private String sucursal;
    private String agencia;
    private String estadoanticipo;
    private String estadoimpresion;
    private String estadoanulacion;
    private String causaanulacion;
    private String formarecuperacion;
    private String cumplimiento;
    private String fechacumplimiento;
    private String usuario;
    private String discrepancia;
    private String tipodiscrepancia;
    private double valorrecuperar;
    private String fechaproyectada;
    private String ciudad;
    private String fechaimpresionoc;
    private double valoroc;
    private String motivoanulacion;
    private String facturacliente;
    private String estadocheque;
    private String nombreanticipo;
    private String supptopay;
    private String suppliername;
    private String cedulapropietario;
    private String bancopropietario;
    private String sucursalpropietario;
    private double valoranticiporecuperar;
    private double valorforaneoanticipo;
    private String moneda;
    private String fechaanticipo;
    private String tieneanticipo;
    private String chequeanticipo;
        
    private String usuario_anulacion;//AMATURANA 06.02.2007
    private String reanticipo;//AMATURANA 16.03.2007
    private String tipo_recuperacion;
    
    
    /** Creates a new instance of AnulacionPlanilla */
    public AnulacionPlanilla() {
    }
    
    /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo AnulacionPlanilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static AnulacionPlanilla load(ResultSet rs)throws Exception{
        AnulacionPlanilla datos = new AnulacionPlanilla();
        datos.setNumpla(rs.getString("PLANILLA"));
        datos.setPlaca(rs.getString("PLACA"));
        datos.setFechaanulacion(validarFecha(rs.getString("FECANULACION")));
        datos.setFechacreacion(validarFecha(rs.getString("FECDSP")));
        
        String diferencia = rs.getString("DIFERENCIA");
        diferencia = diferencia.replaceAll("days", "dias");
        diferencia = diferencia.replaceAll("day", "dia");
        int i = diferencia.indexOf(".");
        
        if( i >= 0  ){
            diferencia = diferencia.substring(0,i);
        }
        
        datos.setDiferencia(diferencia);
       
        String estadoi = "";
        
        if( rs.getString("ESTADOIMPRESION")!= null && !rs.getString("ESTADOIMPRESION").equals("") ){
            estadoi = "IMPRESO";
        }
        else
            estadoi = "NO IMPRESO";
        
        
        datos.setEstadoimpresion(estadoi);
        
        
        
        String estadoa = (rs.getString("ESTADOANULACION")!=null)?"ANULADO" : "NO ANULADO";
        estadoa = (rs.getString("BANCO")==null)? "NO TIENE ANTICIPO" : estadoa;//AMATURANA
        datos.setEstadoanulacion(estadoa);
        
        double anticipo = rs.getDouble("ANTICIPO");
        
        datos.setUsuario_anulacion(rs.getString("user_anul"));//AMATURANA
        //String estadoant = (anticipo.equals("0.00" ))? "ANULADO" : "NO ANULADO";
        String estadoant = (!datos.getUsuario_anulacion().equals("" ))? "ANULADO" : "NO ANULADO";//AMATURANA
        
        
        if( anticipo == -999999999 ){
            estadoant = "NO TIENE ANTICIPO";
            anticipo  = 0;
        }
        
        datos.setValoranticipo(anticipo);
        
        datos.setEstadoanticipo(estadoant);
        
        datos.setCausaanulacion(rs.getString("CAUSA"));
        datos.setFormarecuperacion(rs.getString("TIPORECUPERACION"));
        String cumplimiento = ( rs.getString("FECHACUMPLIDO")!= null )? "SI" : "NO";
        datos.setCumplimiento(cumplimiento);
        datos.setFechacumplimiento(validarFecha(rs.getString("FECHACUMPLIDO")));
        datos.setUsuario(rs.getString("UANULACION"));
        datos.setBanco(rs.getString("BANCO"));
        datos.setSucursal(rs.getString("SUCURSAL"));
        datos.setAgencia(rs.getString("AGENCIA"));
        datos.setFechaproyectada(validarFecha(rs.getString("fechaproyectada")));
        datos.setCiudad(rs.getString("ciudad"));
        datos.setFechaimpresionoc(validarFecha(rs.getString("fechaimpresionoc")));
        datos.setValoroc(rs.getDouble("valoroc"));
        datos.setMotivoanulacion(rs.getString("motivoanulacion"));
        //datos.setEstadoimpresion(rs.getString("estadoimpresion"));
        datos.setEstadocheque(rs.getString("estadocheque"));
        datos.setNombreanticipo(rs.getString("nombreanticipo"));
        datos.setSupptopay(rs.getString("supptopay"));
        datos.setSuppliername(rs.getString("suppliername"));
        datos.setCedulapropietario(rs.getString("cedulapropietario"));
        datos.setBancopropietario(rs.getString("bancopropietario"));
        datos.setSucursalpropietario(rs.getString("sucursalpropietario"));
        //datos.setValoranticiporecuperar(rs.getString("valoranticiporecuperar"));
        datos.setValorforaneoanticipo(rs.getDouble("valorforaneoanticipo"));
        datos.setMoneda(rs.getString("moneda"));
        datos.setFechaanticipo(validarFecha(rs.getString("fechaanticipo"))); 
        
        datos.setTieneanticipo(rs.getString("tieneanticipo"));
        datos.setChequeanticipo(rs.getString("chequeanticipo"));
        
        datos.setReanticipo(rs.getString("reanticipo"));
        datos.setTipo_recuperacion(rs.getString("tipo_recuperacion_ant"));
        
        return datos;
    }
    
    public static String validarFecha (String fecha){
        
        
        return fecha!=null&&!fecha.equals("0099-01-01")? fecha:"";
        
    }
    
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property causaanulacion.
     * @return Value of property causaanulacion.
     */
    public java.lang.String getCausaanulacion() {
        return causaanulacion;
    }
    
    /**
     * Setter for property causaanulacion.
     * @param causaanulacion New value of property causaanulacion.
     */
    public void setCausaanulacion(java.lang.String causaanulacion) {
        this.causaanulacion = causaanulacion;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property cumplimiento.
     * @return Value of property cumplimiento.
     */
    public java.lang.String getCumplimiento() {
        return cumplimiento;
    }
    
    /**
     * Setter for property cumplimiento.
     * @param cumplimiento New value of property cumplimiento.
     */
    public void setCumplimiento(java.lang.String cumplimiento) {
        this.cumplimiento = cumplimiento;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property diferencia.
     * @return Value of property diferencia.
     */
    public java.lang.String getDiferencia() {
        return diferencia;
    }
    
    /**
     * Setter for property diferencia.
     * @param diferencia New value of property diferencia.
     */
    public void setDiferencia(java.lang.String diferencia) {
        this.diferencia = diferencia;
    }
    
    /**
     * Getter for property estadoanticipo.
     * @return Value of property estadoanticipo.
     */
    public java.lang.String getEstadoanticipo() {
        return estadoanticipo;
    }
    
    /**
     * Setter for property estadoanticipo.
     * @param estadoanticipo New value of property estadoanticipo.
     */
    public void setEstadoanticipo(java.lang.String estadoanticipo) {
        this.estadoanticipo = estadoanticipo;
    }
    
    /**
     * Getter for property estadoanulacion.
     * @return Value of property estadoanulacion.
     */
    public java.lang.String getEstadoanulacion() {
        return estadoanulacion;
    }
    
    /**
     * Setter for property estadoanulacion.
     * @param estadoanulacion New value of property estadoanulacion.
     */
    public void setEstadoanulacion(java.lang.String estadoanulacion) {
        this.estadoanulacion = estadoanulacion;
    }
    

    
    /**
     * Getter for property estadoimpresion.
     * @return Value of property estadoimpresion.
     */
    public java.lang.String getEstadoimpresion() {
        return estadoimpresion;
    }
    
    /**
     * Setter for property estadoimpresion.
     * @param estadoimpresion New value of property estadoimpresion.
     */
    public void setEstadoimpresion(java.lang.String estadoimpresion) {
        this.estadoimpresion = estadoimpresion;
    }
    
    /**
     * Getter for property fechaanulacion.
     * @return Value of property fechaanulacion.
     */
    public java.lang.String getFechaanulacion() {
        return fechaanulacion;
    }
    
    /**
     * Setter for property fechaanulacion.
     * @param fechaanulacion New value of property fechaanulacion.
     */
    public void setFechaanulacion(java.lang.String fechaanulacion) {
        this.fechaanulacion = fechaanulacion;
    }
    
    /**
     * Getter for property fechacreacion.
     * @return Value of property fechacreacion.
     */
    public java.lang.String getFechacreacion() {
        return fechacreacion;
    }
    
    /**
     * Setter for property fechacreacion.
     * @param fechacreacion New value of property fechacreacion.
     */
    public void setFechacreacion(java.lang.String fechacreacion) {
        this.fechacreacion = fechacreacion;
    }
    
    /**
     * Getter for property formarecuperacion.
     * @return Value of property formarecuperacion.
     */
    public java.lang.String getFormarecuperacion() {
        return formarecuperacion;
    }
    
    /**
     * Setter for property formarecuperacion.
     * @param formarecuperacion New value of property formarecuperacion.
     */
    public void setFormarecuperacion(java.lang.String formarecuperacion) {
        this.formarecuperacion = formarecuperacion;
    }
    
   
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property valoranticipo.
     * @return Value of property valoranticipo.
     */
    public double getValoranticipo() {
        return valoranticipo;
    }
    
    /**
     * Setter for property valoranticipo.
     * @param valoranticipo New value of property valoranticipo.
     */
    public void setValoranticipo(double valoranticipo) {
        this.valoranticipo = valoranticipo;
    }
    
  
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property fechacumplimiento.
     * @return Value of property fechacumplimiento.
     */
    public java.lang.String getFechacumplimiento() {
        return fechacumplimiento;
    }
    
    /**
     * Setter for property fechacumplimiento.
     * @param fechacumplimiento New value of property fechacumplimiento.
     */
    public void setFechacumplimiento(java.lang.String fechacumplimiento) {
        this.fechacumplimiento = fechacumplimiento;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property discrepancia.
     * @return Value of property discrepancia.
     */
    public java.lang.String getDiscrepancia() {
        return discrepancia;
    }
    
    /**
     * Setter for property discrepancia.
     * @param discrepancia New value of property discrepancia.
     */
    public void setDiscrepancia(java.lang.String discrepancia) {
        this.discrepancia = discrepancia;
    }
    
    /**
     * Getter for property valorrecuperar.
     * @return Value of property valorrecuperar.
     */
    public double getValorrecuperar() {
        return valorrecuperar;
    }
    
    /**
     * Setter for property valorrecuperar.
     * @param valorrecuperar New value of property valorrecuperar.
     */
    public void setValorrecuperar(double valorrecuperar) {
        this.valorrecuperar = valorrecuperar;
    }
    
    /**
     * Getter for property tipodiscrepancia.
     * @return Value of property tipodiscrepancia.
     */
    public java.lang.String getTipodiscrepancia() {
        return tipodiscrepancia;
    }
    
    /**
     * Setter for property tipodiscrepancia.
     * @param tipodiscrepancia New value of property tipodiscrepancia.
     */
    public void setTipodiscrepancia(java.lang.String tipodiscrepancia) {
        this.tipodiscrepancia = tipodiscrepancia;
    }
    
    /**
     * Getter for property fechaproyectada.
     * @return Value of property fechaproyectada.
     */
    public java.lang.String getFechaproyectada() {
        return fechaproyectada;
    }
    
    /**
     * Setter for property fechaproyectada.
     * @param fechaproyectada New value of property fechaproyectada.
     */
    public void setFechaproyectada(java.lang.String fechaproyectada) {
        this.fechaproyectada = fechaproyectada;
    }
    
   
    
    /**
     * Getter for property fechaimpresionoc.
     * @return Value of property fechaimpresionoc.
     */
    public java.lang.String getFechaimpresionoc() {
        return fechaimpresionoc;
    }
    
    /**
     * Setter for property fechaimpresionoc.
     * @param fechaimpresionoc New value of property fechaimpresionoc.
     */
    public void setFechaimpresionoc(java.lang.String fechaimpresionoc) {
        this.fechaimpresionoc = fechaimpresionoc;
    }
    
    /**
     * Getter for property valoroc.
     * @return Value of property valoroc.
     */
    public double getValoroc() {
        return valoroc;
    }
    
    /**
     * Setter for property valoroc.
     * @param valoroc New value of property valoroc.
     */
    public void setValoroc(double valoroc) {
        this.valoroc = valoroc;
    }
    
    /**
     * Getter for property motivoanulacion.
     * @return Value of property motivoanulacion.
     */
    public java.lang.String getMotivoanulacion() {
        return motivoanulacion;
    }
    
    /**
     * Setter for property motivoanulacion.
     * @param motivoanulacion New value of property motivoanulacion.
     */
    public void setMotivoanulacion(java.lang.String motivoanulacion) {
        this.motivoanulacion = motivoanulacion;
    }
    
    /**
     * Getter for property facturacliente.
     * @return Value of property facturacliente.
     */
    public java.lang.String getFacturacliente() {
        return facturacliente;
    }
    
    /**
     * Setter for property facturacliente.
     * @param facturacliente New value of property facturacliente.
     */
    public void setFacturacliente(java.lang.String facturacliente) {
        this.facturacliente = facturacliente;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property estadocheque.
     * @return Value of property estadocheque.
     */
    public java.lang.String getEstadocheque() {
        return estadocheque;
    }    

    /**
     * Setter for property estadocheque.
     * @param estadocheque New value of property estadocheque.
     */
    public void setEstadocheque(java.lang.String estadocheque) {
        this.estadocheque = estadocheque;
    }    
    
    /**
     * Getter for property nombreanticipo.
     * @return Value of property nombreanticipo.
     */
    public java.lang.String getNombreanticipo() {
        return nombreanticipo;
    }
    
    /**
     * Setter for property nombreanticipo.
     * @param nombreanticipo New value of property nombreanticipo.
     */
    public void setNombreanticipo(java.lang.String nombreanticipo) {
        this.nombreanticipo = nombreanticipo;
    }
    
    /**
     * Getter for property supptopay.
     * @return Value of property supptopay.
     */
    public java.lang.String getSupptopay() {
        return supptopay;
    }
    
    /**
     * Setter for property supptopay.
     * @param supptopay New value of property supptopay.
     */
    public void setSupptopay(java.lang.String supptopay) {
        this.supptopay = supptopay;
    }
    
    /**
     * Getter for property cedulapropietario.
     * @return Value of property cedulapropietario.
     */
    public java.lang.String getCedulapropietario() {
        return cedulapropietario;
    }
    
    /**
     * Setter for property cedulapropietario.
     * @param cedulapropietario New value of property cedulapropietario.
     */
    public void setCedulapropietario(java.lang.String cedulapropietario) {
        this.cedulapropietario = cedulapropietario;
    }
    
    /**
     * Getter for property bancopropietario.
     * @return Value of property bancopropietario.
     */
    public java.lang.String getBancopropietario() {
        return bancopropietario;
    }
    
    /**
     * Setter for property bancopropietario.
     * @param bancopropietario New value of property bancopropietario.
     */
    public void setBancopropietario(java.lang.String bancopropietario) {
        this.bancopropietario = bancopropietario;
    }
    
    /**
     * Getter for property sucursalpropietario.
     * @return Value of property sucursalpropietario.
     */
    public java.lang.String getSucursalpropietario() {
        return sucursalpropietario;
    }
    
    /**
     * Setter for property sucursalpropietario.
     * @param sucursalpropietario New value of property sucursalpropietario.
     */
    public void setSucursalpropietario(java.lang.String sucursalpropietario) {
        this.sucursalpropietario = sucursalpropietario;
    }
    
    /**
     * Getter for property valoranticiporecuperar.
     * @return Value of property valoranticiporecuperar.
     */
    public double getValoranticiporecuperar() {
        return valoranticiporecuperar;
    }
    
    /**
     * Setter for property valoranticiporecuperar.
     * @param valoranticiporecuperar New value of property valoranticiporecuperar.
     */
    public void setValoranticiporecuperar(double valoranticiporecuperar) {
        this.valoranticiporecuperar = valoranticiporecuperar;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property valorforaneoanticipo.
     * @return Value of property valorforaneoanticipo.
     */
    public double getValorforaneoanticipo() {
        return valorforaneoanticipo;
    }
    
    /**
     * Setter for property valorforaneoanticipo.
     * @param valorforaneoanticipo New value of property valorforaneoanticipo.
     */
    public void setValorforaneoanticipo(double valorforaneoanticipo) {
        this.valorforaneoanticipo = valorforaneoanticipo;
    }
    
    /**
     * Getter for property fechaanticipo.
     * @return Value of property fechaanticipo.
     */
    public java.lang.String getFechaanticipo() {
        return fechaanticipo;
    }
    
    /**
     * Setter for property fechaanticipo.
     * @param fechaanticipo New value of property fechaanticipo.
     */
    public void setFechaanticipo(java.lang.String fechaanticipo) {
        this.fechaanticipo = fechaanticipo;
    }
    
    /**
     * Getter for property tieneanticipo.
     * @return Value of property tieneanticipo.
     */
    public java.lang.String getTieneanticipo() {
        return tieneanticipo;
    }
    
    /**
     * Setter for property tieneanticipo.
     * @param tieneanticipo New value of property tieneanticipo.
     */
    public void setTieneanticipo(java.lang.String tieneanticipo) {
        this.tieneanticipo = tieneanticipo;
    }
    
    /**
     * Getter for property suppliername.
     * @return Value of property suppliername.
     */
    public java.lang.String getSuppliername() {
        return suppliername;
    }
    
    /**
     * Setter for property suppliername.
     * @param suppliername New value of property suppliername.
     */
    public void setSuppliername(java.lang.String suppliername) {
        this.suppliername = suppliername;
    }
    
    /**
     * Getter for property chequeanticipo.
     * @return Value of property chequeanticipo.
     */
    public java.lang.String getChequeanticipo() {
        return chequeanticipo;
    }
    
    /**
     * Setter for property chequeanticipo.
     * @param chequeanticipo New value of property chequeanticipo.
     */
    public void setChequeanticipo(java.lang.String chequeanticipo) {
        this.chequeanticipo = chequeanticipo;
    }
    
    /**
     * Getter for property usuario_anulacion.
     * @return Value of property usuario_anulacion.
     */
    public java.lang.String getUsuario_anulacion() {
        return usuario_anulacion;
    }
    
    /**
     * Setter for property usuario_anulacion.
     * @param usuario_anulacion New value of property usuario_anulacion.
     */
    public void setUsuario_anulacion(java.lang.String usuario_anulacion) {
        this.usuario_anulacion = usuario_anulacion;
    }
    
    /**
     * Getter for property reanticipo.
     * @return Value of property reanticipo.
     */
    public java.lang.String getReanticipo() {
        return reanticipo;
    }
    
    /**
     * Setter for property reanticipo.
     * @param reanticipo New value of property reanticipo.
     */
    public void setReanticipo(java.lang.String reanticipo) {
        this.reanticipo = reanticipo;
    }
    
    /**
     * Getter for property tipo_recuperacion.
     * @return Value of property tipo_recuperacion.
     */
    public java.lang.String getTipo_recuperacion() {
        return tipo_recuperacion;
    }
    
    /**
     * Setter for property tipo_recuperacion.
     * @param tipo_recuperacion New value of property tipo_recuperacion.
     */
    public void setTipo_recuperacion(java.lang.String tipo_recuperacion) {
        this.tipo_recuperacion = tipo_recuperacion;
    }
    
    
    
    /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo AnulacionPlanilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static AnulacionPlanilla loadAnticipo(ResultSet rs)throws Exception{
        AnulacionPlanilla datos = new AnulacionPlanilla();
        datos.setNumpla(rs.getString("planilla"));
        datos.setBanco(rs.getString("branch_code"));
        datos.setSucursal(rs.getString("bank_account_no"));
        datos.setChequeanticipo(rs.getString("document"));
        datos.setValorforaneoanticipo(rs.getDouble("vlr_for"));
        
        return datos;
    }
    
}
