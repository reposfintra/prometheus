/*
 * PlanillaTiempo.java
 *
 * Created on 30 de noviembre de 2004, 05:00 PM
 */

package com.tsp.operation.model.beans;
import java.sql.*;

/**
 *
 * @author  Administrador
 */
public class PlanillaTiempo {
      // datos de tbltiempo
      private String Dstrct;
      private String SJ;
      private String CFCode;
      private String NMonicCode;
      private String TimeCode;
      private String TimeDescription;
      private String Sequence;
      private String TimeDelay;
      
      private boolean Asignado; // Asinadoo o NO Asignado
      //  datos de planilla tiempo
      private String Planilla;
      private String DateTimeTraffic; // '0099-01-01 00:00:0'
      private String DelayCode;
      private String Observation;
      
            
    /** Creates a new instance of PlanillaTiempo */
    public PlanillaTiempo() {  
    }
    
    public void load_TBLTiempo(ResultSet rs)throws SQLException{
        this.setDstrct         (rs.getString(1));
        this.setSJ             (rs.getString(2));
        this.setCFCode         (rs.getString(3));
        this.setTimeCode       (rs.getString(4));
        this.setTimeDescription(rs.getString(5));
        this.setSequence       (rs.getString(6)); 
        this.setNMonicCode     (rs.getString(7));
        this.setAsignado(false);
        this.setPlanilla       ("");
        this.setDateTimeTraffic("");
        this.setDelayCode      ("");
        this.setTimeDelay      ("");
        this.setObservation    ("");
    }
    public void load_PlanillaTiempo(ResultSet rs)throws SQLException{
        this.setPlanilla       (rs.getString(1));
        this.setDateTimeTraffic(rs.getString(2));
        this.setDelayCode      (rs.getString(3));
        this.setObservation    (rs.getString(4));
        this.setTimeDelay      (rs.getString(5));
        this.setAsignado(true);
    }
    //setter  
    public void setDstrct(String valor){
        this.Dstrct = valor;
    }
    public void setSJ(String valor){
        this.SJ = valor;
    }
    public void setCFCode(String valor){
        this.CFCode = valor;
    }
    public void setNMonicCode(String valor){
        this.NMonicCode = valor;
    }
    public void setTimeCode(String valor){
        this.TimeCode = valor;
    }
    public void setTimeDescription(String valor){
        this.TimeDescription = valor;
    }
    public void setSequence(String valor){
        this.Sequence = valor;
    }
    public void setPlanilla(String valor){
        this.Planilla = valor;
    }
    public void setDateTimeTraffic(String valor){
        this.DateTimeTraffic = valor;
    }
    public void setDelayCode(String valor){
        this.DelayCode = valor;
    }
    public void setObservation(String valor){
        this.Observation = valor;
    }   
    public void setTimeDelay(String valor){
        this.TimeDelay = valor;
    }   
    
    public void setAsignado(boolean valor){
        this.Asignado = valor;
    }
    
    //getter
    
    public String getDstrct(){
        return this.Dstrct;
    }
    public String getSJ(){
        return this.SJ;
    }
    public String getCFCode(){
        return this.CFCode;
    }
    public String getNMonicCode(){
        return this.NMonicCode;
    }
    public String getTimeCode(){
        return this.TimeCode;
    }
    public String getTimeDescription(){
        return this.TimeDescription;
    }
    public String getSequence(){
        return this.Sequence;
    }
    public String getPlanilla(){
        return this.Planilla;
    }
    public String getDateTimeTraffic(){
        return this.DateTimeTraffic;
    }
    public String getDelayCode(){
        return this.DelayCode;
    }
    public String getTimeDelay(){
        return this.TimeDelay;
    }
    public String  getObservation(){
        return this.Observation;
    }          
    public boolean getAsignado(){
        return this.Asignado;
    }
}
