/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author edgonzalez
 */
public class ArchivosAppMicro {

    private String negocio;
    private int id_archivo;
    private int orden;
    private int id_unid_negocio;
    private String nombre_categoria;
    private String nombre_archivo;
    private String request_id;
    private String archivo_cargado;
    private String nombre_archivo_real;
    private String url;
    private String estado;
    private int target_width;
    private int target_height;
    private int quality;
    private int id_categoria;
    private String nombre_cat;
    private String visible;
    private String icon;
    private String descripcion;


    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public int getId_archivo() {
        return id_archivo;
    }

    public void setId_archivo(int id_archivo) {
        this.id_archivo = id_archivo;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public int getId_unid_negocio() {
        return id_unid_negocio;
    }

    public void setId_unid_negocio(int id_unid_negocio) {
        this.id_unid_negocio = id_unid_negocio;
    }

    public String getNombre_categoria() {
        return nombre_categoria;
    }

    public void setNombre_categoria(String nombre_categoria) {
        this.nombre_categoria = nombre_categoria;
    }

    public String getNombre_archivo() {
        return nombre_archivo;
    }

    public void setNombre_archivo(String nombre_archivo) {
        this.nombre_archivo = nombre_archivo;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getArchivo_cargado() {
        return archivo_cargado;
    }

    public void setArchivo_cargado(String archivo_cargado) {
        this.archivo_cargado = archivo_cargado;
    }

    public String getNombre_archivo_real() {
        return nombre_archivo_real;
    }

    public void setNombre_archivo_real(String nombre_archivo_real) {
        this.nombre_archivo_real = nombre_archivo_real;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getTarget_width() {
        return target_width;
    }

    public void setTarget_width(int target_width) {
        this.target_width = target_width;
    }

    public int getTarget_height() {
        return target_height;
    }

    public void setTarget_height(int target_height) {
        this.target_height = target_height;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getNombre_cat() {
        return nombre_cat;
    }

    public void setNombre_cat(String nombre_cat) {
        this.nombre_cat = nombre_cat;
    } 

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
}
