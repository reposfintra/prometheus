package com.tsp.operation.model.beans;

import java.util.ArrayList;

public class DatosOferta {

    private String id_solicitud;
    private String oferta;
    private String elaboradoPor; 
    private String fechaGeneracion;
    private String fechaCreacion;
    private String aprobadoPor;
    private String ejecutivo;
    private String cliente;
    private String NIC;
    private String consecutivo;
    private String ciudad;
    private String departamento;
    private String representante;
    private String direccion;
    private String NIT;
    private String otras_consideraciones;
    private String tipo_cliente;
    private String telefono;
    private String celular;
    private String valorAgregado;
    private String tipo_solicitud;
    private String oficial;
    private String aviso;
    
    private ArrayList consideraciones;

    public DatosOferta(){
    }

    public String getTipo_solicitud() {
        return tipo_solicitud;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public void setTipo_solicitud(String tipo_solicitud) {
        this.tipo_solicitud = tipo_solicitud;
    }

    public String getNIC() {
        return NIC;
    }

    public void setNIC(String NIC) {
        this.NIC = NIC;
    }

    public String getNIT() {
        return NIT;
    }

    public void setNIT(String NIT) {
        this.NIT = NIT;
    }

    public String getAprobadoPor() {
        return aprobadoPor;
    }

    public void setAprobadoPor(String aprobadoPor) {
        this.aprobadoPor = aprobadoPor;
    }

    public String getAviso() {
        return aviso;
    }

    public void setAviso(String aviso) {
        this.aviso = aviso;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    public ArrayList getConsideraciones() {
        return consideraciones;
    }

    public void setConsideraciones(ArrayList consideraciones) {
        this.consideraciones = consideraciones;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEjecutivo() {
        return ejecutivo;
    }

    public void setEjecutivo(String ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    public String getElaboradoPor() {
        return elaboradoPor;
    }

    public void setElaboradoPor(String elaboradoPor) {
        this.elaboradoPor = elaboradoPor;
    }

    public String getFechaGeneracion() {
        return fechaGeneracion;
    }

    public void setFechaGeneracion(String fecha) {
        this.fechaGeneracion = fecha;
    }

    public String getId_solicitud() {
        return id_solicitud;
    }

    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public String getOferta() {
        return oferta;
    }

    public void setOferta(String oferta) {
        this.oferta = oferta;
    }

    public String getOficial() {
        return oficial;
    }

    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

    public String getOtras_consideraciones() {
        return otras_consideraciones;
    }

    public void setOtras_consideraciones(String otras_consideraciones) {
        this.otras_consideraciones = otras_consideraciones;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipo_cliente() {
        return tipo_cliente;
    }

    public void setTipo_cliente(String tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    public String getValorAgregado() {
        return valorAgregado;
    }

    public void setValorAgregado(String valorAgregado) {
        this.valorAgregado = valorAgregado;
    }
}
