/*
 * PGColumn.java
 *
 * Created on 15 de marzo de 2007, 09:11 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Osvaldo P�rez Ferrer
 * 
 * Clase que representa una columna de una tabla, con sus atributos
 * nombre, definicion, tipo de dato, longitud, cantidad de decimales
 * y si acepta o no NULOS
 */
public class PGColumn {
   
    private String name;
    private String definition;
    private String data_type;
    private int length;
    private int decimal_digits;
    private boolean nullable;
    
    /** Creates a new instance of PGColumn */
    public PGColumn() {
                
    }
    
    /**
     * Getter for property data_type.
     * @return Value of property data_type.
     */
    public java.lang.String getData_type() {
        return data_type;
    }
    
    /**
     * Setter for property data_type.
     * @param data_type New value of property data_type.
     */
    public void setData_type(java.lang.String data_type) {
        this.data_type = data_type;
    }
    
    /**
     * Getter for property definition.
     * @return Value of property definition.
     */
    public java.lang.String getDefinition() {
        return definition;
    }
    
    /**
     * Setter for property definition.
     * @param definition New value of property definition.
     */
    public void setDefinition(java.lang.String definition) {
        this.definition = definition;
    }
    
    /**
     * Getter for property length.
     * @return Value of property length.
     */
    public int getLength() {
        return length;
    }
    
    /**
     * Setter for property length.
     * @param length New value of property length.
     */
    public void setLength(int length) {
        this.length = length;
    }
    
    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public java.lang.String getName() {
        return name;
    }
    
    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }
    
    /**
     * Getter for property decimal_digits.
     * @return Value of property decimal_digits.
     */
    public int getDecimal_digits() {
        return decimal_digits;
    }
    
    /**
     * Setter for property decimal_digits.
     * @param decimal_digits New value of property decimal_digits.
     */
    public void setDecimal_digits(int decimal_digits) {
        this.decimal_digits = decimal_digits;
    }
    
    /**
     * Getter for property nullable.
     * @return Value of property nullable.
     */
    public boolean isNullable() {
        return nullable;
    }    

    /**
     * Setter for property nullable.
     * @param nullable New value of property nullable.
     */
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }    
    
}
