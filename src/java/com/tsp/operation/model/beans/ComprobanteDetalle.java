/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class ComprobanteDetalle {

  private String reg_status ;
  private String dstrct;
  private String tipodoc;
  private String numdoc ;
  private int    grupo_transaccion ;
  private int    transaccion;
  private String periodo ;
  private String cuenta;
  private String auxiliar;
  private String detalle;
  private double valor_debito ;
  private double valor_credito ;
  private String tercero;
  private String documento_interno ;
  private String last_update;
  private String user_update;
  private String creation_date;
  private String creation_user;
  private String base;
  private String tipodoc_rel;
  private String documento_rel;
  private String abc ;
  private double vlr_for;

    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipodoc
     */
    public String getTipodoc() {
        return tipodoc;
    }

    /**
     * @param tipodoc the tipodoc to set
     */
    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    /**
     * @return the numdoc
     */
    public String getNumdoc() {
        return numdoc;
    }

    /**
     * @param numdoc the numdoc to set
     */
    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    /**
     * @return the grupo_transaccion
     */
    public int getGrupo_transaccion() {
        return grupo_transaccion;
    }

    /**
     * @param grupo_transaccion the grupo_transaccion to set
     */
    public void setGrupo_transaccion(int grupo_transaccion) {
        this.grupo_transaccion = grupo_transaccion;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the auxiliar
     */
    public String getAuxiliar() {
        return auxiliar;
    }

    /**
     * @param auxiliar the auxiliar to set
     */
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    /**
     * @return the detalle
     */
    public String getDetalle() {
        return detalle;
    }

    /**
     * @param detalle the detalle to set
     */
    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    /**
     * @return the valor_debito
     */
    public double getValor_debito() {
        return valor_debito;
    }

    /**
     * @param valor_debito the valor_debito to set
     */
    public void setValor_debito(double valor_debito) {
        this.valor_debito = valor_debito;
    }

    /**
     * @return the valor_credito
     */
    public double getValor_credito() {
        return valor_credito;
    }

    /**
     * @param valor_credito the valor_credito to set
     */
    public void setValor_credito(double valor_credito) {
        this.valor_credito = valor_credito;
    }

    /**
     * @return the tercero
     */
    public String getTercero() {
        return tercero;
    }

    /**
     * @param tercero the tercero to set
     */
    public void setTercero(String tercero) {
        this.tercero = tercero;
    }

    /**
     * @return the documento_interno
     */
    public String getDocumento_interno() {
        return documento_interno;
    }

    /**
     * @param documento_interno the documento_interno to set
     */
    public void setDocumento_interno(String documento_interno) {
        this.documento_interno = documento_interno;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the tipodoc_rel
     */
    public String getTipodoc_rel() {
        return tipodoc_rel;
    }

    /**
     * @param tipodoc_rel the tipodoc_rel to set
     */
    public void setTipodoc_rel(String tipodoc_rel) {
        this.tipodoc_rel = tipodoc_rel;
    }

    /**
     * @return the documento_rel
     */
    public String getDocumento_rel() {
        return documento_rel;
    }

    /**
     * @param documento_rel the documento_rel to set
     */
    public void setDocumento_rel(String documento_rel) {
        this.documento_rel = documento_rel;
    }

    /**
     * @return the abc
     */
    public String getAbc() {
        return abc;
    }

    /**
     * @param abc the abc to set
     */
    public void setAbc(String abc) {
        this.abc = abc;
    }

    /**
     * @return the vlr_for
     */
    public double getVlr_for() {
        return vlr_for;
    }

    /**
     * @param vlr_for the vlr_for to set
     */
    public void setVlr_for(double vlr_for) {
        this.vlr_for = vlr_for;
    }




}
