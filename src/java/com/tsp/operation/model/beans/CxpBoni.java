/* * CxpBoni.java * * Created on 29 de julio de 2009, 16:46 */
package com.tsp.operation.model.beans;
/** * * @author  Fintra */
public class CxpBoni {

    private String num_os,cxc_ab,id_orden,factura_boni,vlr_neto,abono,saldo,fec_open;    
    private String sum_tot_prev1,id_accion;    
    private String banco,sucursal,id_contratista,contratista,cantidad_acciones,doc_relacionado,prefactura;
    
    public CxpBoni() {    }    
    
    public String getPrefactura() {
        return prefactura;
    }
    public void setPrefactura(String x) {
        this.prefactura= x;
    }
    public String getCantidadAcciones() {
        return cantidad_acciones;
    }
    public void setCantidadAcciones(String x) {
        this.cantidad_acciones= x;
    }
    public String getIdContratista() {
        return id_contratista;
    }
    public void setIdContratista(String x) {
        this.id_contratista= x;
    }
    public String getContratista() {
        return contratista;
    }
    public void setContratista(String x) {
        this.contratista= x;
    }
    public String getIdAccion() {
        return id_accion;
    }
    public void setIdAccion(String x) {
        this.id_accion= x;
    }
    public String getSumTotPrev1() {
        return sum_tot_prev1;
    }
    public void setSumTotPrev1(String x) {
        this.sum_tot_prev1= x;
    }    
    public String getNumOs() {
        return num_os;
    }
    public void setNumOs(String x) {
        this.num_os= x;
    }
    public String getCxcAb() {
        return cxc_ab;
    }
    public void setCxcAb(String x) {
        this.cxc_ab= x;
    }
    public String getIdOrden() {
        return id_orden;
    }
    public void setIdOrden(String x) {
        this.id_orden= x;
    }
    public String getFacturaBoni() {
        return factura_boni;
    }
    public void setFacturaBoni(String x) {
        this.factura_boni= x;
    }
    public String getVlrNeto() {
        return vlr_neto;
    }
    public void setVlrNeto(String x) {
        this.vlr_neto= x;
    }
    public String getAbono() {
        return abono;
    }
    public void setAbono(String x) {
        this.abono= x;
    }
    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String x) {
        this.saldo= x;
    }
    public String getFecOpen() {
        return fec_open;
    }
    public void setFecOpen(String x) {
        this.fec_open= x;
    }    
    
    public String getBanco() {
        return banco;
    }
    public void setBanco(String x) {
        this.banco= x;
    }
    public String getSucursal() {
        return sucursal;
    }
    public void setSucursal(String x) {
        this.sucursal= x;
    }
    public String getDocRelacionado() {
        return doc_relacionado;
    }
    public void setDocRelacionado(String x) {
        this.doc_relacionado= x;
    }
    
    public static CxpBoni load(java.sql.ResultSet rs)throws java.sql.SQLException{
        CxpBoni cxpBoni = new CxpBoni();
        cxpBoni.setNumOs( rs.getString("num_os") );           
        cxpBoni.setCxcAb( rs.getString("cxc_ab") );             
        cxpBoni.setIdOrden( rs.getString("id_orden") ); 
        cxpBoni.setFacturaBoni( rs.getString("factura_bonificacion") ); 
        cxpBoni.setVlrNeto( rs.getString("vlr_neto") ); 
        cxpBoni.setAbono( rs.getString("vlr_total_abonos") ); 
        cxpBoni.setSaldo( rs.getString("vlr_saldo") ); 
        cxpBoni.setFecOpen( rs.getString("fec_open") );         
        cxpBoni.setSumTotPrev1( rs.getString("sum_total_prev1") );         
        cxpBoni.setBanco(rs.getString("banco") ); 
        cxpBoni.setSucursal(rs.getString("sucursal") ); 
        cxpBoni.setIdAccion(rs.getString("id_accion") ); 
        cxpBoni.setIdContratista(rs.getString("id_contratista") ); 
        cxpBoni.setContratista(rs.getString("descripcion") ); 
        cxpBoni.setCantidadAcciones(rs.getString("cantidad_acciones") ); 
        cxpBoni.setDocRelacionado(rs.getString("documento_relacionado") ); 
        cxpBoni.setPrefactura(rs.getString("prefactura") ); 
        return cxpBoni;
    }  
}

