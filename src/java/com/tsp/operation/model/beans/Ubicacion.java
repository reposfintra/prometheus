/*
 * Nombre        Ubicacion.java
 * Autor         Ing. Jesus Cuesta
 * Fecha         21 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.beans;

public class Ubicacion {
    
    private String cod_ubicacion;
    private String descripcion;
    private String cia;    
    private String tipo;
    private String contacto;
    private String pais;
    private String estado;
    private String ciudad;
    private String direccion;
    private String torigen;
    private String tdestino;
    private String relacion;
    private double tiempo;
    private double distancia;
    private String tel1;
    private String tel2;
    private String tel3;
    private String fax;
    private String dstrct;
    private String base;
    private String creation_user;
    private String user_update;
    
    private String nomcia;
    private String nomtipo;
    private String nomcontacto;
    private String nompais;
    private String nomestado;
    private String nomciudad;
    private String nomtorigen;
    private String nomtdestino;
    
    //sescalante 01.02.06
    private String modalidad;
    
     public Ubicacion() {
    }
     
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }    
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property cod_ubicacion.
     * @return Value of property cod_ubicacion.
     */
    public java.lang.String getCod_ubicacion() {
        return cod_ubicacion;
    }
    
    /**
     * Setter for property cod_ubicacion.
     * @param cod_ubicacion New value of property cod_ubicacion.
     */
    public void setCod_ubicacion(java.lang.String cod_ubicacion) {
        this.cod_ubicacion = cod_ubicacion;
    }
    
    /**
     * Getter for property contacto.
     * @return Value of property contacto.
     */
    public java.lang.String getContacto() {
        return contacto;
    }
    
    /**
     * Setter for property contacto.
     * @param contacto New value of property contacto.
     */
    public void setContacto(java.lang.String contacto) {
        this.contacto = contacto;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter for property distancia.
     * @return Value of property distancia.
     */
    public double getDistancia() {
        return distancia;
    }
    
    /**
     * Setter for property distancia.
     * @param distancia New value of property distancia.
     */
    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property fax.
     * @return Value of property fax.
     */
    public java.lang.String getFax() {
        return fax;
    }
    
    /**
     * Setter for property fax.
     * @param fax New value of property fax.
     */
    public void setFax(java.lang.String fax) {
        this.fax = fax;
    }
    
    /**
     * Getter for property pais.
     * @return Value of property pais.
     */
    public java.lang.String getPais() {
        return pais;
    }
    
    /**
     * Setter for property pais.
     * @param pais New value of property pais.
     */
    public void setPais(java.lang.String pais) {
        this.pais = pais;
    }
    
    /**
     * Getter for property relacion.
     * @return Value of property relacion.
     */
    public java.lang.String getRelacion() {
        return relacion;
    }
    
    /**
     * Setter for property relacion.
     * @param relacion New value of property relacion.
     */
    public void setRelacion(java.lang.String relacion) {
        this.relacion = relacion;
    }
    
    /**
     * Getter for property tel1.
     * @return Value of property tel1.
     */
    public java.lang.String getTel1() {
        return tel1;
    }
    
    /**
     * Setter for property tel1.
     * @param tel1 New value of property tel1.
     */
    public void setTel1(java.lang.String tel1) {
        this.tel1 = tel1;
    }
    
    /**
     * Getter for property tel2.
     * @return Value of property tel2.
     */
    public java.lang.String getTel2() {
        return tel2;
    }
    
    /**
     * Setter for property tel2.
     * @param tel2 New value of property tel2.
     */
    public void setTel2(java.lang.String tel2) {
        this.tel2 = tel2;
    }
    
    /**
     * Getter for property tel3.
     * @return Value of property tel3.
     */
    public java.lang.String getTel3() {
        return tel3;
    }
    
    /**
     * Setter for property tel3.
     * @param tel3 New value of property tel3.
     */
    public void setTel3(java.lang.String tel3) {
        this.tel3 = tel3;
    }
    
    /**
     * Getter for property tiempo.
     * @return Value of property tiempo.
     */
    public double getTiempo() {
        return tiempo;
    }
    
    /**
     * Setter for property tiempo.
     * @param tiempo New value of property tiempo.
     */
    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property torigen.
     * @return Value of property torigen.
     */
    public java.lang.String getTorigen() {
        return torigen;
    }
    
    /**
     * Setter for property torigen.
     * @param torigen New value of property torigen.
     */
    public void setTorigen(java.lang.String torigen) {
        this.torigen = torigen;
    }
    
    /**
     * Getter for property tdestino.
     * @return Value of property tdestino.
     */
    public java.lang.String getTdestino() {
        return tdestino;
    }
    
    /**
     * Setter for property tdestino.
     * @param tdestino New value of property tdestino.
     */
    public void setTdestino(java.lang.String tdestino) {
        this.tdestino = tdestino;
    }
    
    /**
     * Getter for property nomciudad.
     * @return Value of property nomciudad.
     */
    public java.lang.String getNomciudad() {
        return nomciudad;
    }
    
    /**
     * Setter for property nomciudad.
     * @param nomciudad New value of property nomciudad.
     */
    public void setNomciudad(java.lang.String nomciudad) {
        this.nomciudad = nomciudad;
    }
    
    /**
     * Getter for property nomcontacto.
     * @return Value of property nomcontacto.
     */
    public java.lang.String getNomcontacto() {
        return nomcontacto;
    }
    
    /**
     * Setter for property nomcontacto.
     * @param nomcontacto New value of property nomcontacto.
     */
    public void setNomcontacto(java.lang.String nomcontacto) {
        this.nomcontacto = nomcontacto;
    }
    
    /**
     * Getter for property nomcia.
     * @return Value of property nomcia.
     */
    public java.lang.String getNomcia() {
        return nomcia;
    }
    
    /**
     * Setter for property nomcia.
     * @param nomcia New value of property nomcia.
     */
    public void setNomcia(java.lang.String nomcia) {
        this.nomcia = nomcia;
    }
    
    /**
     * Getter for property nomestado.
     * @return Value of property nomestado.
     */
    public java.lang.String getNomestado() {
        return nomestado;
    }
    
    /**
     * Setter for property nomestado.
     * @param nomestado New value of property nomestado.
     */
    public void setNomestado(java.lang.String nomestado) {
        this.nomestado = nomestado;
    }
    
    /**
     * Getter for property nompais.
     * @return Value of property nompais.
     */
    public java.lang.String getNompais() {
        return nompais;
    }
    
    /**
     * Setter for property nompais.
     * @param nompais New value of property nompais.
     */
    public void setNompais(java.lang.String nompais) {
        this.nompais = nompais;
    }
    
    /**
     * Getter for property nomtdestino.
     * @return Value of property nomtdestino.
     */
    public java.lang.String getNomtdestino() {
        return nomtdestino;
    }
    
    /**
     * Setter for property nomtdestino.
     * @param nomtdestino New value of property nomtdestino.
     */
    public void setNomtdestino(java.lang.String nomtdestino) {
        this.nomtdestino = nomtdestino;
    }
    
    /**
     * Getter for property nomtipo.
     * @return Value of property nomtipo.
     */
    public java.lang.String getNomtipo() {
        return nomtipo;
    }
    
    /**
     * Setter for property nomtipo.
     * @param nomtipo New value of property nomtipo.
     */
    public void setNomtipo(java.lang.String nomtipo) {
        this.nomtipo = nomtipo;
    }
    
    /**
     * Getter for property nomtorigen.
     * @return Value of property nomtorigen.
     */
    public java.lang.String getNomtorigen() {
        return nomtorigen;
    }
    
    /**
     * Setter for property nomtorigen.
     * @param nomtorigen New value of property nomtorigen.
     */
    public void setNomtorigen(java.lang.String nomtorigen) {
        this.nomtorigen = nomtorigen;
    }
    
    /**
     * Getter for property modalidad.
     * @return Value of property modalidad.
     */
    public java.lang.String getModalidad() {
        return modalidad;
    }
    
    /**
     * Setter for property modalidad.
     * @param modalidad New value of property modalidad.
     */
    public void setModalidad(java.lang.String modalidad) {
        this.modalidad = modalidad;
    }
    
}
