/**************************************************************************
 * Nombre: ......................ReporteUbicacionVehicular.java           *
 * Descripci�n: .................Beans de ubicacion vehicular.            *
 * Autor:........................Ing. Ivan Herazo                         *
 * Fecha:........................24 de marzo de 2004, 05:12 PM            *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;

/**
 * @author 
 */
public class ReporteUbicacionVehicular implements java.io.Serializable{
//------------------------------------------------------------------------------
  //PROPERTIES
//------------------------------------------------------------------------------
  private  String estado; // Tendr� el mismo valor de la propiedad "observaciones"
  private  String destinatario;
  private  String numPlanilla;
  private  String pedidos;
  private  String documentoInterno;
  private  String origen;
  private  String destino;
  private  String conductor;
  private  String celularConductor;
  private  String numRemesa;
  private  String fechaDespacho;
  private  String fechaPosibleLlegada;
  private  String ultimoReporte;
  private  String tiempoTransito;
  private  String observaciones;
  private  String devoluciones;
  private  String crossDocking;
  private  String equipo;
  private  String pesoReal;
  private Hashtable tablaDeCampos;
//------------------------------------------------------------------------------
  //CONSTRUCTOR
//------------------------------------------------------------------------------
  public ReporteUbicacionVehicular()
  {
    this.estado              = "";
    this.destinatario        = "";
    this.numPlanilla         = "";
    this.pedidos             = "";
    this.documentoInterno    ="";
    this.origen              = "";
    this.destino             = "";
    this.conductor           = "";
    this.celularConductor    = "";
    this.numRemesa           = "";
    this.fechaDespacho       = "";
    this.fechaPosibleLlegada = "";
    this.ultimoReporte       = "";
    this.tiempoTransito      = "";
    this.observaciones       = "";
    this.devoluciones        = "";
    this.crossDocking        = "";
    this.equipo              = "";
    this.pesoReal            = "";
//    this.mvcModel = new Model();
  }
  /*DECLARACION DE SET********************************************/
  public void setEstado(ReporteUbicacionVehicular rpt ) {
        if ( rpt.getUltimoReporte().endsWith("-E") ) {
            this.estado = "Con Entrega"  ;
        } else if (rpt.getUltimoReporte().trim().equals("") || rpt.getUltimoReporte().trim().equals("-")) {
            this.estado = "Por Confirmar Salida"  ;
        }  else {
            this.estado = "En Ruta"  ;
        }
    }
  public void setDestinatario        (String destinatario       ) { this.destinatario        = destinatario;        }
  public void setNumPlanilla         (String numPlanilla        ) { this.numPlanilla         = numPlanilla;         }
  public void setPedidos             (String pedidos            ) { this.pedidos             = pedidos;             }
  public void setDocumentoInterno    (String documentoInterno   ) { this.documentoInterno    = documentoInterno;    }
  public void setOrigen              (String origen             ) { this.origen              = origen;              }
  public void setDestino             (String destino            ) { this.destino             = destino;             }
  public void setConductor           (String conductor          ) { this.conductor           = conductor;           }
  public void setCelularConductor    (String celularConductor   ) { this.celularConductor    = celularConductor;    }
  public void setNumRemesa           (String numRemesa          ) { this.numRemesa           = numRemesa;           }
  public void setFechaDespacho       (String fechaDespacho      ) { this.fechaDespacho       = fechaDespacho;       }
  public void setFechaPosibleLlegada (String fechaPosibleLlegada) { this.fechaPosibleLlegada = fechaPosibleLlegada; }
  public void setUltimoReporte       (String ultimoReporte      ) { this.ultimoReporte       = ultimoReporte;       }
  public void setTiempoTransito      (String tiempoTransito     ) { this.tiempoTransito      = tiempoTransito;      }
  public void setObservaciones       (String observaciones      ) { this.observaciones       = observaciones;       }
  public void setDevoluciones        (String devoluciones       ) { this.devoluciones        = devoluciones;        }
  public void setCrossDocking        (String crossDocking       ) { this.crossDocking        = crossDocking;        }
  public void setEquipo              (String equipo             ) { this.equipo              = equipo;              }
  public void setPesoReal            (String pesoReal           ) { this.pesoReal            = pesoReal;            }
  
   /*DECLARACION DE GET********************************************/
  public String getEstado              ( ) { return this.estado;             }
  public String getDestinatario        ( ) { return this.destinatario;        }
  public String getNumPlanilla         ( ) { return this.numPlanilla;         }
  public String getPedidos             ( ) { return this.pedidos;             }
  public String getDocumentoInterno    ( ) { return this.documentoInterno;    }
  public String getOrigen              ( ) { return this.origen;              }
  public String getDestino             ( ) { return this.destino;             }
  public String getConductor           ( ) { return this.conductor;           }
  public String getCelularConductor    ( ) { return this.celularConductor;    }
  public String getNumRemesa           ( ) { return this.numRemesa;           }
  public String getFechaDespacho       ( ) { return this.fechaDespacho;       }
  public String getFechaPosibleLlegada ( ) { return this.fechaPosibleLlegada; }
  public String getUltimoReporte       ( ) { return this.ultimoReporte;       }
  public String getTiempoTransito      ( ) { return this.tiempoTransito;      }
  public String getObservaciones       ( ) { return this.observaciones;       }
  public String getDevoluciones        ( ) { return this.devoluciones;        }
  public String getCrossDocking        ( ) { return this.crossDocking;        }
  public String getEquipo              ( ) { return this.equipo;              }
  public String getPesoReal            ( ) { return this.pesoReal;            }
   /**
     * Retorna el nombre del destinatario tomando como base el valor del
     * campo "Documento Interno" separados por linea. Luego busca el nombre del
     * destinatario en la tabla REMIDES
     * @return Campo "Documento Interno".
     * @throws SQLException si un error de acceso a base de datos ocurre.
     */
    public String getDestinatarioDiv() throws SQLException {
        String[] vDest = this.getDocumentoInterno().split("/");
        if (vDest.length > 0) {
            StringBuffer cDest = new StringBuffer(0);
            for (int i=0; i < vDest.length; i++ ) {
                String[] vDest2 = vDest[ i ].split(":");
                if (vDest2.length > 1 ) {
                    RemiDest desti = null; //this.mvcModel.getRemidest( vDest2[1] );
                    cDest.append( vDest2[1]+" - " + "<br>"); //+desti.getNombre()
                }
            }
            return cDest.toString();
        } else {
            return "** " + this.destinatario;
        }
    }
    
     /**
     * Retorna el campo "Pedidos" (la factura comercial), pero separada
     * por cada ocurrencia de '/'
     * @return Campo "Pedidos" seccionado.
     */
    public String getPedidosDiv() {
        String[] facturas = this.getPedidos().split("/");
        StringBuffer cFactCial = new StringBuffer(0);
        for (int i=0; i < facturas.length; i++ ) {
            cFactCial.append( facturas[i] + "<br>");
        }
        if( cFactCial.length() == 0 ) {
            cFactCial.append("-");
        }
        return cFactCial.toString(); 
    }
  
  /**
   * Metodo factoria para crear un registro del reporte de la fila activa
   * de un resultado dado
   * @param rs es un resultado dado a partir de la consulta del reporte.
   * @return Un registro del reporte.
   * @throws SQLException si aparece un error de base de datos
   */
  public static ReporteUbicacionVehicular load(ResultSet rs, String [] campos ) throws SQLException
  {
    ReporteUbicacionVehicular rptUbVeh = new ReporteUbicacionVehicular();
    String value = null;
    
    
    value = rs.getString("DESTINATARIO");
    if(value != null) rptUbVeh.setDestinatario( value );
//    if(value != null) rptUbVeh.setDestinatario(value, rs.getString("DOCUINTERNO"));    
    
    value = rs.getString("NUMPLA");
    if(value != null) rptUbVeh.setNumPlanilla(value);
    
    value = rs.getString("PEDIDOS");
    if(value != null) rptUbVeh.setPedidos(value);
    
    value = rs.getString("DOCUINTERNO");
    if(value != null) rptUbVeh.setDocumentoInterno(value);
    
    value = rs.getString("ORINOM");
    if(value != null) rptUbVeh.setOrigen(value);
    
    value = rs.getString("DESNOM");
    if(value != null) rptUbVeh.setDestino(value);
    
    value = rs.getString("NOMCON");
    if(value != null) rptUbVeh.setConductor(value);
    
    value = rs.getString("CELULARCONDUCTOR");
    if(value != null) rptUbVeh.setCelularConductor(value);
        
    value = rs.getString("NUMREM");
    if(value != null) rptUbVeh.setNumRemesa(value);
    
    value = rs.getString("FECDSP");
    if(value != null) rptUbVeh.setFechaDespacho(value);
    
    value = rs.getString("FECHAPOSLLEGADA");
    if(value != null) rptUbVeh.setFechaPosibleLlegada(value);
    
    value = rs.getString("ULTIMOREPORTE");
    if(value != null) rptUbVeh.setUltimoReporte(value);
    
    value = rs.getString("TIEMPOENTRANSITO");
    if(value != null) rptUbVeh.setTiempoTransito(value);
    
    value = rs.getString("ESTADO"); // Posicion 15
    if(value != null) rptUbVeh.setObservaciones(value);
    
    value = rs.getString("DEVOLUCIONES");
    if(value != null) rptUbVeh.setDevoluciones(value);
    
    value = rs.getString("CROSSDOCK");
    if(value != null) rptUbVeh.setCrossDocking(value);
            
    // Preguntar si la propiedad "equipo" corresponde a la placa del veh�culo.
    value = rs.getString("EQUIPO");
    if(value != null) rptUbVeh.setEquipo(value);
            
    value = rs.getString("PESOREAL");
    if(value != null) rptUbVeh.setPesoReal(value);
    
    // Configura la variable especial "estado"
        rptUbVeh.setEstado( rptUbVeh );
        if ( campos != null ){
            rptUbVeh.crearTablaDeCampos(rs, campos);
        }
        
            
    return rptUbVeh;
  }
   public void setInitialization(boolean initialize) throws Exception {
    }
   
   private void crearTablaDeCampos(ResultSet rs, String [] campos) throws SQLException{
        tablaDeCampos = new Hashtable();
        for( int i=0; i<campos.length; i++ ){
            if ( campos[i].equals("estado") ){
                tablaDeCampos.put(campos[i], this.getEstado());
            }
            else {
                String str = rs.getString(campos[i]);
                tablaDeCampos.put(campos[i], str == null?"-":str);
            }
        }
    }
   
    public void establecerValor(String campo, String valor){
        if ( tablaDeCampos != null ){
            tablaDeCampos.put(campo, valor);
        }
    }
    
    public String obtenerValor(String campo){
        if ( tablaDeCampos != null ){
            return ""+tablaDeCampos.get(campo);
        }
        return "campo "+campo+" no existe";
    }
    
    
}