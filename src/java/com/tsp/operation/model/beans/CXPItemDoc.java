/********************************************************************
 *      Nombre Clase.................   CXPItemDoc.java
 *      Descripci�n..................   Bean de la tabla cxp_items_doc
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class CXPItemDoc implements Serializable{
    
        private String reg_status;
        private String dstrct;
	private String proveedor;
	private String tipo_documento;
	private String documento;
	private String descripcion;
        private String item;
        private double vlr;
	private double vlr_me;
	private double vlr_item;
	private double vlr_item_me;
	private String codigo_cuenta;
	private String codigo_abc;
	private String planilla;
	private String last_update;
	private String user_update;
	private String creation_date;
	private String creation_user;
	private String base;        
        private String codcliarea;
        private String tipcliarea;
        private String descliarea;
        //campos para saber si un Item tiene observacion
        boolean banderaRoja;
        boolean banderaVerde;        
        boolean responder;
        private Vector vItems;
        
        private String textoactivo;
        
        private String descconcepto;
        
        //Juan 02-05-2006
        private Vector vCopia;
        private double vlr_total;
        
        //Ivan 10-05-2006
        private String Concepto;
        private String iva;
        private  String[] codigos;
        
        
        //Ivan 21 julio 2006
        private String auxiliar;
        private LinkedList tipo;
        private String tipoSubledger;
        private boolean exiteCuenta; 
        private boolean reqAuxilar;
        
    
        
          //ivan 26 julio 2006
        private String ree;
        private String ref3;
        private String ref4;
        private String agencia;
        
        private boolean errorCuenta;
        
        
        /** mfontalvo ene-22-07 */
        private double vlr_iva;
        private double vlr_riva;
        private double vlr_rica;
        private double vlr_rfte;
        
        private double vlr_iva_me;
        private double vlr_riva_me;
        private double vlr_rica_me;
        private double vlr_rfte_me;
        
        private String porc_riva;
        private String porc_rica;
        private String porc_rfte;
              
        
        
    
    /** Creates a new instance of Documento */
        public CXPItemDoc() {
        }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }  
   
        
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
  
    /**
     * Getter for property banderaRoja.
     * @return Value of property banderaRoja.
     */
    public boolean isBanderaRoja() {
        return banderaRoja;
    }
    
    /**
     * Setter for property banderaRoja.
     * @param banderaRoja New value of property banderaRoja.
     */
    public void setBanderaRoja(boolean banderaRoja) {
        this.banderaRoja = banderaRoja;
    }
    
    /**
     * Getter for property banderaVerde.
     * @return Value of property banderaVerde.
     */
    public boolean isBanderaVerde() {
        return banderaVerde;
    }
    
    /**
     * Setter for property banderaVerde.
     * @param banderaVerde New value of property banderaVerde.
     */
    public void setBanderaVerde(boolean banderaVerde) {
        this.banderaVerde = banderaVerde;
    }
    
    /**
     * Getter for property responder.
     * @return Value of property responder.
     */
    public boolean isResponder() {
        return responder;
    }
    
    /**
     * Setter for property responder.
     * @param responder New value of property responder.
     */
    public void setResponder(boolean responder) {
        this.responder = responder;
    }
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public double getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property codigo_abc.
     * @return Value of property codigo_abc.
     */
    public java.lang.String getCodigo_abc() {
        return codigo_abc;
    }
    
    /**
     * Setter for property codigo_abc.
     * @param codigo_abc New value of property codigo_abc.
     */
    public void setCodigo_abc(java.lang.String codigo_abc) {
        this.codigo_abc = codigo_abc;
    }
    
    /**
     * Getter for property codigo_cuenta.
     * @return Value of property codigo_cuenta.
     */
    public java.lang.String getCodigo_cuenta() {
        return codigo_cuenta;
    }
    
    /**
     * Setter for property codigo_cuenta.
     * @param codigo_cuenta New value of property codigo_cuenta.
     */
    public void setCodigo_cuenta(java.lang.String codigo_cuenta) {
        this.codigo_cuenta = codigo_cuenta;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property vItems.
     * @return Value of property vItems.
     */
    public java.util.Vector getVItems() {
        return vItems;
    }
    
    /**
     * Setter for property vItems.
     * @param vItems New value of property vItems.
     */
    public void setVItems(java.util.Vector vItems) {
        this.vItems = vItems;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property vlr_me.
     * @return Value of property vlr_me.
     */
    public double getVlr_me() {
        return vlr_me;
    }
    
    /**
     * Setter for property vlr_me.
     * @param vlr_me New value of property vlr_me.
     */
    public void setVlr_me(double vlr_me) {
        this.vlr_me = vlr_me;
    }
    
    /**
     * Getter for property textoactivo.
     * @return Value of property textoactivo.
     */
    public java.lang.String getTextoactivo() {
        return textoactivo;
    }
    
    /**
     * Setter for property textoactivo.
     * @param textoactivo New value of property textoactivo.
     */
    public void setTextoactivo(java.lang.String textoactivo) {
        this.textoactivo = textoactivo;
    }
    
    /**
     * Getter for property codcliarea.
     * @return Value of property codcliarea.
     */
    public java.lang.String getCodcliarea () {
        return codcliarea;
    }
    
    /**
     * Setter for property codcliarea.
     * @param codcliarea New value of property codcliarea.
     */
    public void setCodcliarea (java.lang.String codcliarea) {
        this.codcliarea = codcliarea;
    }
    
    /**
     * Getter for property tipcliarea.
     * @return Value of property tipcliarea.
     */
    public java.lang.String getTipcliarea () {
        return tipcliarea;
    }
    
    /**
     * Setter for property tipcliarea.
     * @param tipcliarea New value of property tipcliarea.
     */
    public void setTipcliarea (java.lang.String tipcliarea) {
        this.tipcliarea = tipcliarea;
    }
    
    /**
     * Getter for property descliarea.
     * @return Value of property descliarea.
     */
    public java.lang.String getDescliarea () {
        return descliarea;
    }
    
    /**
     * Setter for property descliarea.
     * @param descliarea New value of property descliarea.
     */
    public void setDescliarea (java.lang.String descliarea) {
        this.descliarea = descliarea;
    }
    
    /**
     * Getter for property descconcepto.
     * @return Value of property descconcepto.
     */
    public java.lang.String getDescconcepto() {
        return descconcepto;
    }
    
    /**
     * Setter for property descconcepto.
     * @param descconcepto New value of property descconcepto.
     */
    public void setDescconcepto(java.lang.String descconcepto) {
        this.descconcepto = descconcepto;
    }
    
    /**
     * Getter for property vCopia.
     * @return Value of property vCopia.
     */
    public java.util.Vector getVCopia() {
        return vCopia;
    }
    
    /**
     * Setter for property vCopia.
     * @param vCopia New value of property vCopia.
     */
    public void setVCopia(java.util.Vector vCopia) {
        this.vCopia = vCopia;
    }
    
    /**
     * Getter for property vlr_total.
     * @return Value of property vlr_total.
     */
    public double getVlr_total() {
        return vlr_total;
    }
    
    /**
     * Setter for property vlr_total.
     * @param vlr_total New value of property vlr_total.
     */
    public void setVlr_total(double vlr_total) {
        this.vlr_total = vlr_total;
    }
    
    /**
     * Getter for property Concepto.
     * @return Value of property Concepto.
     */
    public java.lang.String getConcepto() {
        return Concepto;
    }
    
    /**
     * Setter for property Concepto.
     * @param Concepto New value of property Concepto.
     */
    public void setConcepto(java.lang.String Concepto) {
        this.Concepto = Concepto;
    }
    
    /**
     * Getter for property iva.
     * @return Value of property iva.
     */
    public java.lang.String getIva() {
        return iva;
    }
    
    /**
     * Setter for property iva.
     * @param iva New value of property iva.
     */
    public void setIva(java.lang.String iva) {
        this.iva = iva;
    }
    
    /**
     * Getter for property codigos.
     * @return Value of property codigos.
     */
    public java.lang.String[] getCodigos() {
        return this.codigos;
    }
    
    /**
     * Setter for property codigos.
     * @param codigos New value of property codigos.
     */
    public void setCodigos(java.lang.String[] codigos) {
        this.codigos = codigos;
    }
    
    /**
     * Getter for property reqAuxilar.
     * @return Value of property reqAuxilar.
     */
    public boolean isReqAuxilar() {
        return reqAuxilar;
    }
    
    /**
     * Setter for property reqAuxilar.
     * @param reqAuxilar New value of property reqAuxilar.
     */
    public void setReqAuxilar(boolean reqAuxilar) {
        this.reqAuxilar = reqAuxilar;
    }
    
    /**
     * Getter for property exiteCuenta.
     * @return Value of property exiteCuenta.
     */
    public boolean isExiteCuenta() {
        return exiteCuenta;
    }
    
    /**
     * Setter for property exiteCuenta.
     * @param exiteCuenta New value of property exiteCuenta.
     */
    public void setExiteCuenta(boolean exiteCuenta) {
        this.exiteCuenta = exiteCuenta;
    }
    
    /**
     * Getter for property tipoSubledger.
     * @return Value of property tipoSubledger.
     */
    public java.lang.String getTipoSubledger() {
        return tipoSubledger;
    }
    
    /**
     * Setter for property tipoSubledger.
     * @param tipoSubledger New value of property tipoSubledger.
     */
    public void setTipoSubledger(java.lang.String tipoSubledger) {
        this.tipoSubledger = tipoSubledger;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.util.LinkedList getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.util.LinkedList tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    /**
     * Getter for property ree.
     * @return Value of property ree.
     */
    public java.lang.String getRee() {
        return ree;
    }
    
    /**
     * Setter for property ree.
     * @param ree New value of property ree.
     */
    public void setRee(java.lang.String ree) {
        this.ree = ree;
    }
    
    /**
     * Getter for property ref3.
     * @return Value of property ref3.
     */
    public java.lang.String getRef3() {
        return ref3;
    }
    
    /**
     * Setter for property ref3.
     * @param ref3 New value of property ref3.
     */
    public void setRef3(java.lang.String ref3) {
        this.ref3 = ref3;
    }
    
    /**
     * Getter for property ref4.
     * @return Value of property ref4.
     */
    public java.lang.String getRef4() {
        return ref4;
    }
    
    /**
     * Setter for property ref4.
     * @param ref4 New value of property ref4.
     */
    public void setRef4(java.lang.String ref4) {
        this.ref4 = ref4;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property errorCuenta.
     * @return Value of property errorCuenta.
     */
    public boolean isErrorCuenta() {
        return errorCuenta;
    }
    
    /**
     * Setter for property errorCuenta.
     * @param errorCuenta New value of property errorCuenta.
     */
    public void setErrorCuenta(boolean errorCuenta) {
        this.errorCuenta = errorCuenta;
    }
    
    /**
     * Getter for property vlr_iva.
     * @return Value of property vlr_iva.
     */
    public double getVlr_iva() {
        return vlr_iva;
    }
    
    /**
     * Setter for property vlr_iva.
     * @param vlr_iva New value of property vlr_iva.
     */
    public void setVlr_iva(double vlr_iva) {
        this.vlr_iva = vlr_iva;
    }
    
    /**
     * Getter for property vlr_riva.
     * @return Value of property vlr_riva.
     */
    public double getVlr_riva() {
        return vlr_riva;
    }
    
    /**
     * Setter for property vlr_riva.
     * @param vlr_riva New value of property vlr_riva.
     */
    public void setVlr_riva(double vlr_riva) {
        this.vlr_riva = vlr_riva;
    }
    
    /**
     * Getter for property vlr_rica.
     * @return Value of property vlr_rica.
     */
    public double getVlr_rica() {
        return vlr_rica;
    }
    
    /**
     * Setter for property vlr_rica.
     * @param vlr_rica New value of property vlr_rica.
     */
    public void setVlr_rica(double vlr_rica) {
        this.vlr_rica = vlr_rica;
    }
    
    /**
     * Getter for property vlr_rfte.
     * @return Value of property vlr_rfte.
     */
    public double getVlr_rfte() {
        return vlr_rfte;
    }
    
    /**
     * Setter for property vlr_rfte.
     * @param vlr_rfte New value of property vlr_rfte.
     */
    public void setVlr_rfte(double vlr_rfte) {
        this.vlr_rfte = vlr_rfte;
    }
    
    /**
     * Getter for property vlr_iva_me.
     * @return Value of property vlr_iva_me.
     */
    public double getVlr_iva_me() {
        return vlr_iva_me;
    }
    
    /**
     * Setter for property vlr_iva_me.
     * @param vlr_iva_me New value of property vlr_iva_me.
     */
    public void setVlr_iva_me(double vlr_iva_me) {
        this.vlr_iva_me = vlr_iva_me;
    }
    
    /**
     * Getter for property vlr_riva_me.
     * @return Value of property vlr_riva_me.
     */
    public double getVlr_riva_me() {
        return vlr_riva_me;
    }
    
    /**
     * Setter for property vlr_riva_me.
     * @param vlr_riva_me New value of property vlr_riva_me.
     */
    public void setVlr_riva_me(double vlr_riva_me) {
        this.vlr_riva_me = vlr_riva_me;
    }
    
    /**
     * Getter for property vlr_rica_me.
     * @return Value of property vlr_rica_me.
     */
    public double getVlr_rica_me() {
        return vlr_rica_me;
    }
    
    /**
     * Setter for property vlr_rica_me.
     * @param vlr_rica_me New value of property vlr_rica_me.
     */
    public void setVlr_rica_me(double vlr_rica_me) {
        this.vlr_rica_me = vlr_rica_me;
    }
    
    /**
     * Getter for property vlr_rfte_me.
     * @return Value of property vlr_rfte_me.
     */
    public double getVlr_rfte_me() {
        return vlr_rfte_me;
    }
    
    /**
     * Setter for property vlr_rfte_me.
     * @param vlr_rfte_me New value of property vlr_rfte_me.
     */
    public void setVlr_rfte_me(double vlr_rfte_me) {
        this.vlr_rfte_me = vlr_rfte_me;
    }
    
    /**
     * Getter for property porc_riva.
     * @return Value of property porc_riva.
     */
    public java.lang.String getPorc_riva() {
        return porc_riva;
    }
    
    /**
     * Setter for property porc_riva.
     * @param porc_riva New value of property porc_riva.
     */
    public void setPorc_riva(java.lang.String porc_riva) {
        this.porc_riva = porc_riva;
    }
    
    /**
     * Getter for property porc_rica.
     * @return Value of property porc_rica.
     */
    public java.lang.String getPorc_rica() {
        return porc_rica;
    }
    
    /**
     * Setter for property porc_rica.
     * @param porc_rica New value of property porc_rica.
     */
    public void setPorc_rica(java.lang.String porc_rica) {
        this.porc_rica = porc_rica;
    }
    
    /**
     * Getter for property porc_rfte.
     * @return Value of property porc_rfte.
     */
    public java.lang.String getPorc_rfte() {
        return porc_rfte;
    }
    
    /**
     * Setter for property porc_rfte.
     * @param porc_rfte New value of property porc_rfte.
     */
    public void setPorc_rfte(java.lang.String porc_rfte) {
        this.porc_rfte = porc_rfte;
    }
    
}
