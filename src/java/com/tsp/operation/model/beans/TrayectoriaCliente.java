/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author jpinedo
 */
public class TrayectoriaCliente {


      private String  Tipo_Documento;
  private String  Nro_Documento_Identidad;
  private String  Nombre;


  private String  Codigo_de_Banco;
   private String  banco;
  private String  Codigo_de_Sucursal;
  private String  Numero_de_Cuenta_Exitosas;
  private String  Cantidad_Consultas_Exitosas;
  private double  Valor_Consultas_Exitosas;
  private String  Fecha_Ultima_actualizacion;
  private String  Indicador_Siniestro;
  private String  Accion;
  private String  Fecha;
  private String  Usuario;
  private String  Tel1;
  private String  Tel2;
  private String  ExiCanHis ;
  private String  tipo;
  private String Numero_de_Cheque;
  private String Valor_de_Cheque;
  private String Fecha_Consignacion;
  private String Fecha_Recuperacion;
  private int Dias_Recuperados;
  private String Estado_del_Cheque;

    public int getDias_Recuperados() {
        return Dias_Recuperados;
    }

    public void setDias_Recuperados(int Dias_Recuperados) {
        this.Dias_Recuperados = Dias_Recuperados;
    }

    public String getEstado_del_Cheque() {
        return Estado_del_Cheque;
    }

    public void setEstado_del_Cheque(String Estado_del_Cheque) {
        this.Estado_del_Cheque = Estado_del_Cheque;
    }

    public String getFecha_Consignacion() {
        return Fecha_Consignacion;
    }

    public void setFecha_Consignacion(String Fecha_Consignacion) {
        this.Fecha_Consignacion = Fecha_Consignacion;
    }

    public String getFecha_Recuperacion() {
        return Fecha_Recuperacion;
    }

    public void setFecha_Recuperacion(String Fecha_Recuperacion) {
        this.Fecha_Recuperacion = Fecha_Recuperacion;
    }

    public String getNumero_de_Cheque() {
        return Numero_de_Cheque;
    }

    public void setNumero_de_Cheque(String Numero_de_Cheque) {
        this.Numero_de_Cheque = Numero_de_Cheque;
    }

    public String getValor_de_Cheque() {
        return Valor_de_Cheque;
    }

    public void setValor_de_Cheque(String Valor_de_Cheque) {
        this.Valor_de_Cheque = Valor_de_Cheque;
    }
  
  
    public String getAccion() {
        return Accion;
    }

    public void setAccion(String Accion) {
        this.Accion = Accion;
    }

    public String getCantidad_Consultas_Exitosas() {
        return Cantidad_Consultas_Exitosas;
    }

    public void setCantidad_Consultas_Exitosas(String Cantidad_Consultas_Exitosas) {
        this.Cantidad_Consultas_Exitosas = Cantidad_Consultas_Exitosas;
    }

    public String getCodigo_de_Banco() {
        return Codigo_de_Banco;
    }

    public void setCodigo_de_Banco(String Codigo_de_Banco) {
        this.Codigo_de_Banco = Codigo_de_Banco;
    }

    public String getCodigo_de_Sucursal() {
        return Codigo_de_Sucursal;
    }

    public void setCodigo_de_Sucursal(String Codigo_de_Sucursal) {
        this.Codigo_de_Sucursal = Codigo_de_Sucursal;
    }

    public String getExiCanHis() {
        return ExiCanHis;
    }

    public void setExiCanHis(String ExiCanHis) {
        this.ExiCanHis = ExiCanHis;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getFecha_Ultima_actualizacion() {
        return Fecha_Ultima_actualizacion;
    }

    public void setFecha_Ultima_actualizacion(String Fecha_Ultima_actualizacion) {
        this.Fecha_Ultima_actualizacion = Fecha_Ultima_actualizacion;
    }

    public String getIndicador_Siniestro() {
        return Indicador_Siniestro;
    }

    public void setIndicador_Siniestro(String Indicador_Siniestro) {
        this.Indicador_Siniestro = Indicador_Siniestro;
    }

    public String getNro_Documento_Identidad() {
        return Nro_Documento_Identidad;
    }

    public void setNro_Documento_Identidad(String Nro_Documento_Identidad) {
        this.Nro_Documento_Identidad = Nro_Documento_Identidad;
    }

    public String getNumero_de_Cuenta_Exitosas() {
        return Numero_de_Cuenta_Exitosas;
    }

    public void setNumero_de_Cuenta_Exitosas(String Numero_de_Cuenta_Exitosas) {
        this.Numero_de_Cuenta_Exitosas = Numero_de_Cuenta_Exitosas;
    }

    public String getTel1() {
        return Tel1;
    }

    public void setTel1(String Tel1) {
        this.Tel1 = Tel1;
    }

    public String getTel2() {
        return Tel2;
    }

    public void setTel2(String Tel2) {
        this.Tel2 = Tel2;
    }

    public String getTipo_Documento() {
        return Tipo_Documento;
    }

    public void setTipo_Documento(String Tipo_Documento) {
        this.Tipo_Documento = Tipo_Documento;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public double getValor_Consultas_Exitosas() {
        return Valor_Consultas_Exitosas;
    }

    public void setValor_Consultas_Exitosas(double Valor_Consultas_Exitosas) {
        this.Valor_Consultas_Exitosas = Valor_Consultas_Exitosas;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }


    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

}
