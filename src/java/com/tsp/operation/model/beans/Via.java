 /***************************************
    * Nombre Clase ............. Via.java
    * Descripci�n  .. . . . . .  Estructura de vias para las ruta.
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  11/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.model.beans;

import java.util.*;

public class Via {
    
    private String origen      = "";
    private String destino     = "";
    private String secuencia   = "";
    private String descripcion = "";
    private String codigo      = "";
    private String via         = "";
    private List   puestos     ;
    private String ciudad      = "";
    
    public Via() {
        puestos  = new LinkedList();
    }
    
      
    
    
    public void setPuestos(List list){ this.puestos = list; }
    public List getPuestos()         { return this.puestos; }
    
    
    /**
     * Almacena  el  codigo del origen
     * @autor........  Fernel Villacob
     * @param........  String val: valor del origen
     * @see ......... 
     * @throws ......  
     * @version ..... 1.0      
     */    
     public void setOrigen (String val){ 
        this.origen      = val;
     }
     
     
     
     /**
     * Almacena  el  codigo del destino
     * @autor........  Fernel Villacob
     * @param........  String val: valor del origen
     * @see ......... 
     * @throws ......  
     * @version ..... 1.0      
     */
    public void setDestino     (String val){ 
        this.destino     = val; 
    }
    
    
     /**
      * Almacena  el  codigo del origen
      * @autor........ Fernel Villacob
      * @param........ val: valor del destino
      * @param val
      */
    public void setSecuencia   (String val){ 
        this.secuencia   = val; 
    }
    
    
     /**
      * Almacena  la descripcion
      * @autor........ Fernel Villacob
      * @param val ... valor de la descripcion
      */
    public void setDescripcion (String val){ 
        this.descripcion = val; 
    }
    
         
    /**
      * Almacena  el codigo
      * @autor........ Fernel Villacob
      * @param val ... valor del codigo
      */   
    public void setCodigo (String val){ 
        this.codigo      = val; 
    }
    
    
  /**
   * Almacena  la via
   * @autor........ Fernel Villacob
   * @param val ... valor de la via
   */      
    public void setVia(String val){ 
        this.via         = val; 
    }
    
    
    
    
    
 
    
 /**
  * Retorna el codigo del origen
  * @autor........ Fernel Villacob
  */   
    public String getOrigen (){
        return this.origen      ; 
    }
    
    
    
  /**
  * Retorna el codigo del destino
  * @autor........ Fernel Villacob
  */    
   public String getDestino(){ 
       return this.destino; 
   }
   
   
   
   
  /**
  * Retorna el codigo del origen
  * @autor........ Fernel Villacob
  */   
    public String getSecuencia   (){
        return this.secuencia   ; 
    }
    
    
    
  /**
  * Retorna la descripcion
  * @autor........ Fernel Villacob
  */     
    public String getDescripcion (){ 
        return this.descripcion ; 
    }
    
    
  /**
  * Retorna el codigo 
  * @autor........ Fernel Villacob
  */     
    public String getCodigo(){ 
        return this.codigo ; 
    }
    
    
  /**
  * Retorna la via 
  * @autor........ Fernel Villacob
  */     
    public String getVia(){
        return this.via ; 
    }
   
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }    
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }    
    
}
