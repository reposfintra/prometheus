/*Created on 30 de septiembre de 2005, 03:35 PM*/

package com.tsp.operation.model.beans;

/**@author  fvillacob*/

import java.io.*;

public class Imagen {
    
    private String distrito;
    private String actividad;
    private String tipoDocumento;
    private String Documento;
    //Henry
    private String creation_user;
    
    private String        nameImagen;
    private String        contextType;
    private InputStream   binary;
    
    private String        fileName;
    private String        filePath;
    private String      fecha_creacion;
    
       
    public Imagen() {
    }
    
    
    

    public void setDistrito      (String val)              { this.distrito      = val; }
    public void setActividad     (String val)              { this.actividad     = val; }
    public void setTipoDocumento(String val)              { this.tipoDocumento = val; }
    public void setDocumento     (String val)              { this.Documento     = val; }
    public void setNameImagen    (String val)              { this.nameImagen    = val; }
    public void setContext       (String val)              { this.contextType   = val; }
    public void setBinary        (InputStream val)         { this.binary        = val; }
    public void setFileName      (String val)              { this.fileName      = val; }
    
    
    
    public String      getDistrito       (){ return this.distrito      ; }
    public String      getActividad      (){ return this.actividad     ; }
    public String      getTipoDocumento  (){ return this.tipoDocumento ; }
    public String      getDocumento      (){ return this.Documento     ; }
    public String      getNameImagen     (){ return this.nameImagen    ; }
    public String      getContext        (){ return this.contextType   ; }
    public InputStream getBinary         (){ return this.binary        ; }
    public String      getFileName       (){ return this.fileName      ; }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }    
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    
}
