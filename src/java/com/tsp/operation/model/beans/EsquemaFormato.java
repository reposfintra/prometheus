
package com.tsp.operation.model.beans;

import  java.io.*;


public class EsquemaFormato {
    
    
    private String  distrito;
    private String  tipoDoc;
    private String  doc;
    
    private String  formato;
    private String  campo;
    private String  descripcion;
    private int     orden;
    private int     pInicial;
    private int     pFinal;
    private String  titulo;
    
    
    private String  contenido;
    private String  Remision;
    private String  tipo;
    private String  Centro_C;
    private String  Compresac;
    
    
    public EsquemaFormato() {
        formato     = "";
        campo       = "";
        descripcion = "";
        orden       = 0;
        pInicial    = 0;
        pFinal      = 0;
        titulo      = "";
        contenido   = "";
        tipo        = "";
        distrito    = "";
        tipoDoc     = "";
        doc         = "";
    }
    
    
    
    
    
    /**
     * Getter for property campo.
     * @return Value of property campo.
     */
    public java.lang.String getCampo() {
        return campo;
    }    
    
    /**
     * Setter for property campo.
     * @param campo New value of property campo.
     */
    public void setCampo(java.lang.String campo) {
        this.campo = campo;
    }    
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }    
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property formato.
     * @return Value of property formato.
     */
    public java.lang.String getFormato() {
        return formato;
    }
    
    /**
     * Setter for property formato.
     * @param formato New value of property formato.
     */
    public void setFormato(java.lang.String formato) {
        this.formato = formato;
    }
    
    
    
    
    /**
     * Getter for property longitud.
     * @return Value of property longitud.
     */
    public int getLongitud() {
        return  this.pFinal - this.pInicial + 1;
    }
    
    
    
    
    
    /**
     * Getter for property orden.
     * @return Value of property orden.
     */
    public int getOrden() {
        return orden;
    }
    
    /**
     * Setter for property orden.
     * @param orden New value of property orden.
     */
    public void setOrden(int orden) {
        this.orden = orden;
    }
    
    /**
     * Getter for property pFinal.
     * @return Value of property pFinal.
     */
    public int getPFinal() {
        return pFinal;
    }
    
    /**
     * Setter for property pFinal.
     * @param pFinal New value of property pFinal.
     */
    public void setPFinal(int pFinal) {
        this.pFinal = pFinal;
    }
    
    /**
     * Getter for property pInicial.
     * @return Value of property pInicial.
     */
    public int getPInicial() {
        return pInicial;
    }
    
    /**
     * Setter for property pInicial.
     * @param pInicial New value of property pInicial.
     */
    public void setPInicial(int pInicial) {
        this.pInicial = pInicial;
    }
    
    /**
     * Getter for property titulo.
     * @return Value of property titulo.
     */
    public java.lang.String getTitulo() {
        return titulo;
    }
    
    /**
     * Setter for property titulo.
     * @param titulo New value of property titulo.
     */
    public void setTitulo(java.lang.String titulo) {
        this.titulo = titulo;
    }
    
    /**
     * Getter for property contenido.
     * @return Value of property contenido.
     */
    public java.lang.String getContenido() {
        return contenido;
    }
    
    /**
     * Setter for property contenido.
     * @param contenido New value of property contenido.
     */
    public void setContenido(java.lang.String contenido) {
        this.contenido = contenido;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property tipoDoc.
     * @return Value of property tipoDoc.
     */
    public java.lang.String getTipoDoc() {
        return tipoDoc;
    }
    
    /**
     * Setter for property tipoDoc.
     * @param tipoDoc New value of property tipoDoc.
     */
    public void setTipoDoc(java.lang.String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }
    
    /**
     * Getter for property doc.
     * @return Value of property doc.
     */
    public java.lang.String getDoc() {
        return doc;
    }
    
    /**
     * Setter for property doc.
     * @param doc New value of property doc.
     */
    public void setDoc(java.lang.String doc) {
        this.doc = doc;
    }
    
    /**
     * Getter for property Remision.
     * @return Value of property Remision.
     */
    public java.lang.String getRemision() {
        return Remision;
    }
    
    /**
     * Setter for property Remision.
     * @param Remision New value of property Remision.
     */
    public void setRemision(java.lang.String Remision) {
        this.Remision = Remision;
    }
    
    /**
     * Getter for property Centro_C.
     * @return Value of property Centro_C.
     */
    public java.lang.String getCentro_C() {
        return Centro_C;
    }
    
    /**
     * Setter for property Centro_C.
     * @param Centro_C New value of property Centro_C.
     */
    public void setCentro_C(java.lang.String Centro_C) {
        this.Centro_C = Centro_C;
    }
    
    /**
     * Getter for property Compresac.
     * @return Value of property Compresac.
     */
    public java.lang.String getCompresac() {
        return Compresac;
    }
    
    /**
     * Setter for property Compresac.
     * @param Compresac New value of property Compresac.
     */
    public void setCompresac(java.lang.String Compresac) {
        this.Compresac = Compresac;
    }
    
}
