/*
 * factura.java
 *
 * Created on 12 de noviembre de 2004, 06:53 PM
 */

package com.tsp.operation.model.beans;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
/**
 *
 * @author  AMARTINEZ
 */
public class factura_detalle implements java.io.Serializable,Cloneable{
        
        private String nit;
        private String factura;
        private int item;
        private String concepto;
        private String numero_remesa;
        private String descripcion;
        private String codigo_cuenta_contable;
        private Double cantidad;
        private double valor_unitario;
        private double valor_unitariome;
        private double valor_item;
        private double valor_itemme;
        private Double valor_tasa;
        private String moneda;
        private String fecrem;
        private String unidad;
        private String origen;
        private String destino;
        private String std_job_no;
        private String codtipocarga;
        private String tipo_doc;
        private String nomorigendestino;
        private String documento;
        private String auxiliar;            
        private String plaveh;
        private String monedaOriginal;;
        private LinkedList tiposub; 
        private String aux;
        private String auxliliar;
        private String tiposubledger;
        private boolean incluida = false;
        
        private String tipo_costo;
        private String planilla;
        private String fecha_cost_reemb;
        private boolean estraflete = false;
        private String remesa_costo;
        
        
        
        private double peso_real_cargado;
        private double peso_cobrar;
        private String unit_packed;
        
        public Object clonee() {
            try{
                return super.clone();
            } catch (Exception ex){
                ex.printStackTrace();
                return null;
            }
        }
        public void Load(ResultSet rs) throws SQLException{
            
           // this.nit=rs.getString("nit");
            this.cantidad= new Double((rs.getString("pesoreal")!=null)?rs.getString("pesoreal"):"0");
            this.valor_unitario =rs.getDouble("qty_value");
            this.valor_unitariome =rs.getDouble("qty_value");
            this.unidad= (rs.getString("unit_of_work")!= null)?rs.getString("unit_of_work"):"";
            
            this.descripcion=(rs.getString("descripcion")!=null)?rs.getString("descripcion"):"";
            this.numero_remesa=(rs.getString("numrem")!=null)?rs.getString("numrem"):"";
            this.moneda=(rs.getString("currency")!=null)?rs.getString("currency"):"";
            this.valor_item=rs.getDouble("vlrrem");
            this.valor_itemme=rs.getDouble("vlrrem");
            this.fecrem=(rs.getString("fecrem")!=null)?rs.getString("fecrem"):"";
            this.origen=(rs.getString("orirem")!=null)?rs.getString("orirem"):"";
            this.destino=(rs.getString("desrem")!=null)?rs.getString("desrem"):"";
            this.std_job_no=(rs.getString("std_job_no")!=null)?rs.getString("std_job_no"):"";
            this.codtipocarga= (rs.getString("codtipocarga")!=null)?rs.getString("codtipocarga"):"";
            
            this.codigo_cuenta_contable="1305050301";
           
        }
        
          public void Load2(ResultSet rs) throws SQLException{
            System.out.println("Entro a factura detalle!!!");
            this.factura= rs.getString("documento");
            this.item= rs.getInt("item");
            this.nit=rs.getString("nit");
            this.concepto=rs.getString("concepto");
            this.numero_remesa= rs.getString("numero_remesa");
            this.descripcion=rs.getString("descripcion");
            
            
                        String sql = this.descripcion;
                        String que="";
                        for(int j=0;j<sql.length();j++){
                            String com="";
                            com+=sql.charAt(j);
                            if(com.equals("#")){
                                que=sql.substring(j,sql.length());
                                this.descripcion=descripcion.substring(0,j);
                                break;
                            }
                        }
                        if(!que.equals("")){
                            System.out.println("load2:"+que);
                            this.fecrem=que;
                            
                        }
            System.out.println("descripcion:"+this.descripcion);
            System.out.println("descripcion:"+this.fecrem);
            String auxi = rs.getString("auxiliar");
           
            auxi= auxi.replaceAll(" ", "");
          
            if(!auxi.equals("")){
                System.out.println("auxi:"+auxi);   
                this.tiposubledger=auxi.substring(0,2);
                System.out.println("auxi:"+auxi.substring(0,2));   
                this.auxliliar=auxi.substring(3,auxi.length());
                System.out.println("auxi:"+auxi.substring(3,auxi.length()));   
                this.aux="S";
                
            }
            else{
                this.tiposubledger="";
                this.auxliliar="";
                this.aux="N";
            }
            
            this.codigo_cuenta_contable=rs.getString("codigo_cuenta_contable");
           
            this.cantidad= new Double(rs.getString("cantidad"));
            
            this.valor_unitario =rs.getDouble("valor_unitariome");
            this.valor_unitariome =rs.getDouble("valor_unitariome");
            this.valor_item=rs.getDouble("valor_itemme");
            this.valor_itemme=rs.getDouble("valor_itemme");
            this.valor_tasa=new Double(rs.getString("valor_tasa"));
            this.moneda=rs.getString("moneda");
            
        }
          
          public void LoadCarbon(ResultSet rs) throws SQLException{          
            this.fecrem             =   rs.getString("fecrem");      
            this.cantidad           =   new Double(rs.getString("cantidad"));          
            this.valor_itemme       =   rs.getDouble("valor_itemme");      
                       
        }
        //modificada jm 29-12-2006 
        public void Load3(ResultSet rs) throws SQLException{

            this.numero_remesa=rs.getString("numrem");
            this.fecrem=rs.getString("fecrem");
            this.origen=rs.getString("orirem");
            this.destino=rs.getString("desrem");
            this.cantidad= new Double(rs.getString("cantidad"));
            this.unidad= rs.getString("unit_of_work");
            this.valor_item   = rs.getDouble("valor_item");
            this.valor_itemme = rs.getDouble("valor_itemme");
            this.descripcion=rs.getString("descripcion");
            this.valor_tasa=new Double(rs.getString("valor_tasa"));
            this.valor_unitario   = rs.getDouble("valor_unitario");
            this.valor_unitariome = rs.getDouble("vlrme");
            this.moneda=rs.getString("moneda");
            this.monedaOriginal=rs.getString("currency");                     

        }
        /**
         * Getter for property cantidad.
         * @return Value of property cantidad.
         */
        public java.lang.Double getCantidad() {
            return cantidad;
        }
        
        /**
         * Setter for property cantidad.
         * @param cantidad New value of property cantidad.
         */
        public void setCantidad(java.lang.Double cantidad) {
            this.cantidad = cantidad;
        }
        
        /**
         * Getter for property codigo_cuenta_contable.
         * @return Value of property codigo_cuenta_contable.
         */
        public java.lang.String getCodigo_cuenta_contable() {
            return codigo_cuenta_contable;
        }
        
        /**
         * Setter for property codigo_cuenta_contable.
         * @param codigo_cuenta_contable New value of property codigo_cuenta_contable.
         */
        public void setCodigo_cuenta_contable(java.lang.String codigo_cuenta_contable) {
            this.codigo_cuenta_contable = codigo_cuenta_contable;
        }
        
        /**
         * Getter for property concepto.
         * @return Value of property concepto.
         */
        public java.lang.String getConcepto() {
            return concepto;
        }
        
        /**
         * Setter for property concepto.
         * @param concepto New value of property concepto.
         */
        public void setConcepto(java.lang.String concepto) {
            this.concepto = concepto;
        }
        
        /**
         * Getter for property descripcion.
         * @return Value of property descripcion.
         */
        public java.lang.String getDescripcion() {
            return descripcion;
        }
        
        /**
         * Setter for property descripcion.
         * @param descripcion New value of property descripcion.
         */
        public void setDescripcion(java.lang.String descripcion) {
            this.descripcion = descripcion;
        }
        
        /**
         * Getter for property factura.
         * @return Value of property factura.
         */
        public java.lang.String getFactura() {
            return factura;
        }
        
        /**
         * Setter for property factura.
         * @param factura New value of property factura.
         */
        public void setFactura(java.lang.String factura) {
            this.factura = factura;
        }
        
        /**
         * Getter for property item.
         * @return Value of property item.
         */
        public int getItem() {
            return item;
        }
        
        /**
         * Setter for property item.
         * @param item New value of property item.
         */
        public void setItem(int item) {
            this.item = item;
        }
        
        /**
         * Getter for property moneda.
         * @return Value of property moneda.
         */
        public java.lang.String getMoneda() {
            return moneda;
        }
        
        /**
         * Setter for property moneda.
         * @param moneda New value of property moneda.
         */
        public void setMoneda(java.lang.String moneda) {
            this.moneda = moneda;
        }
        
        /**
         * Getter for property nit.
         * @return Value of property nit.
         */
        public java.lang.String getNit() {
            return nit;
        }
        
        /**
         * Setter for property nit.
         * @param nit New value of property nit.
         */
        public void setNit(java.lang.String nit) {
            this.nit = nit;
        }
        
        /**
         * Getter for property numero_remesa.
         * @return Value of property numero_remesa.
         */
        public java.lang.String getNumero_remesa() {
            return numero_remesa;
        }
        
        /**
         * Setter for property numero_remesa.
         * @param numero_remesa New value of property numero_remesa.
         */
        public void setNumero_remesa(java.lang.String numero_remesa) {
            this.numero_remesa = numero_remesa;
        }
        
        /**
         * Getter for property valor_item.
         * @return Value of property valor_item.
         */
        public double getValor_item() {
            return valor_item;
        }
        
        /**
         * Setter for property valor_item.
         * @param valor_item New value of property valor_item.
         */
        public void setValor_item(double valor_item) {
            this.valor_item = valor_item;
        }
        
        /**
         * Getter for property valor_itemme.
         * @return Value of property valor_itemme.
         */
        public double getValor_itemme() {
            return valor_itemme;
        }
        
        /**
         * Setter for property valor_itemme.
         * @param valor_itemme New value of property valor_itemme.
         */
        public void setValor_itemme(double valor_itemme) {
            this.valor_itemme = valor_itemme;
        }
        
        /**
         * Getter for property valor_tasa.
         * @return Value of property valor_tasa.
         */
        public java.lang.Double getValor_tasa() {
            return valor_tasa;
        }
        
        /**
         * Setter for property valor_tasa.
         * @param valor_tasa New value of property valor_tasa.
         */
        public void setValor_tasa(java.lang.Double valor_tasa) {
            this.valor_tasa = valor_tasa;
        }
        
        /**
         * Getter for property valor_unitario.
         * @return Value of property valor_unitario.
         */
        public double getValor_unitario() {
            return valor_unitario;
        }
        
        /**
         * Setter for property valor_unitario.
         * @param valor_unitario New value of property valor_unitario.
         */
        public void setValor_unitario(double valor_unitario) {
            this.valor_unitario = valor_unitario;
        }
        
        /**
         * Getter for property valor_unitariome.
         * @return Value of property valor_unitariome.
         */
        public double getValor_unitariome() {
            return valor_unitariome;
        }
        
        /**
         * Setter for property valor_unitariome.
         * @param valor_unitariome New value of property valor_unitariome.
         */
        public void setValor_unitariome(double valor_unitariome) {
            this.valor_unitariome = valor_unitariome;
        }
        
        /**
         * Getter for property fecrem.
         * @return Value of property fecrem.
         */
        public java.lang.String getFecrem() {
            return fecrem;
        }
        
        /**
         * Setter for property fecrem.
         * @param fecrem New value of property fecrem.
         */
        public void setFecrem(java.lang.String fecrem) {
            this.fecrem = fecrem;
        }
        
        /**
         * Getter for property unidad.
         * @return Value of property unidad.
         */
        public java.lang.String getUnidad() {
            return unidad;
        }
        
        /**
         * Setter for property unidad.
         * @param unidad New value of property unidad.
         */
        public void setUnidad(java.lang.String unidad) {
            this.unidad = unidad;
        }
        
        /**
         * Getter for property origen.
         * @return Value of property origen.
         */
        public java.lang.String getOrigen() {
            return origen;
        }        
        
        /**
         * Setter for property origen.
         * @param origen New value of property origen.
         */
        public void setOrigen(java.lang.String origen) {
            this.origen = origen;
        }        
        
        /**
         * Getter for property destino.
         * @return Value of property destino.
         */
        public java.lang.String getDestino() {
            return destino;
        }
        
        /**
         * Setter for property destino.
         * @param destino New value of property destino.
         */
        public void setDestino(java.lang.String destino) {
            this.destino = destino;
        }
        
        /**
         * Getter for property std_job_no.
         * @return Value of property std_job_no.
         */
        public java.lang.String getStd_job_no() {
            return std_job_no;
        }
        
        /**
         * Setter for property std_job_no.
         * @param std_job_no New value of property std_job_no.
         */
        public void setStd_job_no(java.lang.String std_job_no) {
            this.std_job_no = std_job_no;
        }
        
        /**
         * Getter for property codtipocarga.
         * @return Value of property codtipocarga.
         */
        public java.lang.String getCodtipocarga() {
            return codtipocarga;
        }
        
        /**
         * Setter for property codtipocarga.
         * @param codtipocarga New value of property codtipocarga.
         */
        public void setCodtipocarga(java.lang.String codtipocarga) {
            this.codtipocarga = codtipocarga;
        }
        
        /**
         * Getter for property tipo_doc.
         * @return Value of property tipo_doc.
         */
        public java.lang.String getTipo_doc() {
            return tipo_doc;
        }
        
        /**
         * Setter for property tipo_doc.
         * @param tipo_doc New value of property tipo_doc.
         */
        public void setTipo_doc(java.lang.String tipo_doc) {
            this.tipo_doc = tipo_doc;
        }
        
        /**
         * Getter for property nomorigendestino.
         * @return Value of property nomorigendestino.
         */
        public java.lang.String getNomorigendestino() {
            return nomorigendestino;
        }
        
        /**
         * Setter for property nomorigendestino.
         * @param nomorigendestino New value of property nomorigendestino.
         */
        public void setNomorigendestino(java.lang.String nomorigendestino) {
            this.nomorigendestino = nomorigendestino;
        }
        
        /**
         * Getter for property documento.
         * @return Value of property documento.
         */
        public java.lang.String getDocumento() {
            return documento;
        }
        
        /**
         * Setter for property documento.
         * @param documento New value of property documento.
         */
        public void setDocumento(java.lang.String documento) {
            this.documento = documento;
        }
        
        /**
         * Getter for property auxiliar.
         * @return Value of property auxiliar.
         */
        public java.lang.String getAuxiliar() {
            return auxiliar;
        }
        
        /**
         * Setter for property auxiliar.
         * @param auxiliar New value of property auxiliar.
         */
        public void setAuxiliar(java.lang.String auxiliar) {
            this.auxiliar = auxiliar;
        }
        
        /**
         * Getter for property monedaOriginal.
         * @return Value of property monedaOriginal.
         */
        public java.lang.String getMonedaOriginal() {
            return monedaOriginal;
        }
        
        /**
         * Setter for property monedaOriginal.
         * @param monedaOriginal New value of property monedaOriginal.
         */
        public void setMonedaOriginal(java.lang.String monedaOriginal) {
            this.monedaOriginal = monedaOriginal;
        }
        
        /**
         * Getter for property plaveh.
         * @return Value of property plaveh.
         */
        public java.lang.String getPlaveh() {
            return plaveh;
        }
        
        /**
         * Setter for property plaveh.
         * @param plaveh New value of property plaveh.
         */
        public void setPlaveh(java.lang.String plaveh) {
            this.plaveh = plaveh;
        }
        
        /**
         * Getter for property tiposub.
         * @return Value of property tiposub.
         */
        public java.util.LinkedList getTiposub() {
            return tiposub;
        }
        
        /**
         * Setter for property tiposub.
         * @param tiposub New value of property tiposub.
         */
        public void setTiposub(java.util.LinkedList tiposub) {
            this.tiposub = tiposub;
        }
        
        /**
         * Getter for property aux.
         * @return Value of property aux.
         */
        public java.lang.String getAux() {
            return aux;
        }
        
        /**
         * Setter for property aux.
         * @param aux New value of property aux.
         */
        public void setAux(java.lang.String aux) {
            this.aux = aux;
        }
        
        /**
         * Getter for property auxliliar.
         * @return Value of property auxliliar.
         */
        public java.lang.String getAuxliliar() {
            return auxliliar;
        }
        
        /**
         * Setter for property auxliliar.
         * @param auxliliar New value of property auxliliar.
         */
        public void setAuxliliar(java.lang.String auxliliar) {
            this.auxliliar = auxliliar;
        }
        
        /**
         * Getter for property tiposubledger.
         * @return Value of property tiposubledger.
         */
        public java.lang.String getTiposubledger() {
            return tiposubledger;
        }
        
        /**
         * Setter for property tiposubledger.
         * @param tiposubledger New value of property tiposubledger.
         */
        public void setTiposubledger(java.lang.String tiposubledger) {
            this.tiposubledger = tiposubledger;
        }
        
        /**
         * Getter for property incluida.
         * @return Value of property incluida.
         */
        public boolean isIncluida() {
            return incluida;
        }
        
        /**
         * Setter for property incluida.
         * @param incluida New value of property incluida.
         */
        public void setIncluida(boolean incluida) {
            this.incluida = incluida;
        }
        
        /**
         * Getter for property tipo_costo.
         * @return Value of property tipo_costo.
         */
        public java.lang.String getTipo_costo() {
            return tipo_costo;
        }
        
        /**
         * Setter for property tipo_costo.
         * @param tipo_costo New value of property tipo_costo.
         */
        public void setTipo_costo(java.lang.String tipo_costo) {
            this.tipo_costo = tipo_costo;
        }
        
        /**
         * Getter for property planilla.
         * @return Value of property planilla.
         */
        public java.lang.String getPlanilla() {
            return planilla;
        }
        
        /**
         * Setter for property planilla.
         * @param planilla New value of property planilla.
         */
        public void setPlanilla(java.lang.String planilla) {
            this.planilla = planilla;
        }
        
        /**
         * Getter for property fecha_cost_reemb.
         * @return Value of property fecha_cost_reemb.
         */
        public java.lang.String getFecha_cost_reemb() {
            return fecha_cost_reemb;
        }
        
        /**
         * Setter for property fecha_cost_reemb.
         * @param fecha_cost_reemb New value of property fecha_cost_reemb.
         */
        public void setFecha_cost_reemb(java.lang.String fecha_cost_reemb) {
            this.fecha_cost_reemb = fecha_cost_reemb;
        }
        
        /**
         * Getter for property estraflete.
         * @return Value of property estraflete.
         */
        public boolean isEstraflete() {
            return estraflete;
        }
        
        /**
         * Setter for property estraflete.
         * @param estraflete New value of property estraflete.
         */
        public void setEstraflete(boolean estraflete) {
            this.estraflete = estraflete;
        }
        
        /**
         * Getter for property remesa_costo.
         * @return Value of property remesa_costo.
         */
        public java.lang.String getRemesa_costo() {
            return remesa_costo;
        }
        
        /**
         * Setter for property remesa_costo.
         * @param remesa_costo New value of property remesa_costo.
         */
        public void setRemesa_costo(java.lang.String remesa_costo) {
            this.remesa_costo = remesa_costo;
        }
        
        /**
         * Getter for property peso_real_cargado.
         * @return Value of property peso_real_cargado.
         */
        public double getPeso_real_cargado() {
            return peso_real_cargado;
        }
        
        /**
         * Setter for property peso_real_cargado.
         * @param peso_real_cargado New value of property peso_real_cargado.
         */
        public void setPeso_real_cargado(double peso_real_cargado) {
            this.peso_real_cargado = peso_real_cargado;
        }
        
        /**
         * Getter for property peso_cobrar.
         * @return Value of property peso_cobrar.
         */
        public double getPeso_cobrar() {
            return peso_cobrar;
        }
        
        /**
         * Setter for property peso_cobrar.
         * @param peso_cobrar New value of property peso_cobrar.
         */
        public void setPeso_cobrar(double peso_cobrar) {
            this.peso_cobrar = peso_cobrar;
        }
        
        /**
         * Getter for property unit_packed.
         * @return Value of property unit_packed.
         */
        public java.lang.String getUnit_packed() {
            return unit_packed;
        }
        
        /**
         * Setter for property unit_packed.
         * @param unit_packed New value of property unit_packed.
         */
        public void setUnit_packed(java.lang.String unit_packed) {
            this.unit_packed = unit_packed;
        }
        
}
