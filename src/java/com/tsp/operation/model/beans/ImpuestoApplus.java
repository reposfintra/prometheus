/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class ImpuestoApplus {

    private ImpuestoContrato iva;
    private ImpuestoContrato retefuenteMat;
    private ImpuestoContrato retefuenteMao;
    private ImpuestoContrato retefuenteOtr;


    /** Creates a new instance of Prefactura */
    public ImpuestoApplus() {

        iva = new ImpuestoContrato();
        retefuenteMat = new ImpuestoContrato();
        retefuenteMao = new ImpuestoContrato();
        retefuenteOtr = new ImpuestoContrato();


        iva.setCodigo_impuesto("");
        iva.setDescripcion("");
        iva.setPorcentaje1(0);


        retefuenteMat.setCodigo_impuesto("");
        retefuenteMat.setDescripcion("");
        retefuenteMat.setPorcentaje1(0);

        retefuenteMao.setCodigo_impuesto("");
        retefuenteMao.setDescripcion("");
        retefuenteMao.setPorcentaje1(0);

        retefuenteOtr.setCodigo_impuesto("");
        retefuenteOtr.setDescripcion("");
        retefuenteOtr.setPorcentaje1(0);

    }

    public void setImpuesto(ImpuestoContrato impuesto,String tipoImpuesto) {

        if (tipoImpuesto.equalsIgnoreCase("IVA")){

            iva.setCodigo_impuesto(impuesto.getCodigo_impuesto());
            iva.setDescripcion(impuesto.getDescripcion());
            iva.setPorcentaje1(impuesto.getPorcentaje1());
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAT")){
            retefuenteMat.setCodigo_impuesto(impuesto.getCodigo_impuesto());
            retefuenteMat.setDescripcion(impuesto.getDescripcion());
            retefuenteMat.setPorcentaje1(impuesto.getPorcentaje1());
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAO")){
            retefuenteMao.setCodigo_impuesto(impuesto.getCodigo_impuesto());
            retefuenteMao.setDescripcion(impuesto.getDescripcion());
            retefuenteMao.setPorcentaje1(impuesto.getPorcentaje1());
        }
        else if (tipoImpuesto.equalsIgnoreCase("ROTR")){
            retefuenteOtr.setCodigo_impuesto(impuesto.getCodigo_impuesto());
            retefuenteOtr.setDescripcion(impuesto.getDescripcion());
            retefuenteOtr.setPorcentaje1(impuesto.getPorcentaje1());
        }

    }

    public String getCodigo(String tipoImpuesto) {

        if (tipoImpuesto.equalsIgnoreCase("IVA")){
            return iva.getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAT")){
            return retefuenteMat.getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAO")){
            return retefuenteMao.getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("ROTR")){
            return retefuenteOtr.getCodigo_impuesto();
        }
        else
            return "";
    }


    public String getDescripcion(String tipoImpuesto) {

        if (tipoImpuesto.equalsIgnoreCase("IVA")){
            return iva.getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAT")){
            return retefuenteMat.getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAO")){
            return retefuenteMao.getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("ROTR")){
            return retefuenteOtr.getDescripcion();
        }
        else
            return "";
    }


    public double getPorcentaje(String tipoImpuesto) {

        if (tipoImpuesto.equalsIgnoreCase("IVA")){
            return iva.getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAT")){
            return retefuenteMat.getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RMAO")){
            return retefuenteMao.getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("ROTR")){
            return retefuenteOtr.getPorcentaje1();
        }
        else
            return 0;
    }




}
