/********************************************************************
 *      Nombre Clase.................   ConsultaUsuarios.java
 *      Descripci�n..................   Bean de la tabla consulta_usuarios
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUsuarios
{
    private String reg_status="";
    private String dstrct="";
    private int codigo=0;
    private String descripcion="";
    private String cselect="";
    private String cfrom="";
    private String cwhere="";
    private String cotros="";
    private String usuario="";
    private String last_update="";
    private String user_update="";
    private String creation_date="";
    private String creation_user="";
    private String base="";
    
    /** Creates a new instance of ConsultaUsuarios */
    public ConsultaUsuarios ()
    {
    }
    
       /** Creates a new instance of Tasa */
    public static  ConsultaUsuarios load(ResultSet rs)throws SQLException {
         ConsultaUsuarios consulta = new  ConsultaUsuarios();
         consulta.setDescripcion (rs.getString ("descripcion"));
         consulta.setCselect (rs.getString ("cselect"));
         consulta.setCfrom (rs.getString("cfrom"));
         consulta.setCwhere (rs.getString ("cwhere"));
         consulta.setCotros (rs.getString ("cotros"));
         consulta.setCodigo (rs.getInt ("codigo"));
         consulta.setCreation_user (rs.getString ("creation_user"));
         consulta.setCreation_date (rs.getString ("creation_date"));
        return consulta;              
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status ()
    {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status (java.lang.String reg_status)
    {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct ()
    {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct (java.lang.String dstrct)
    {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public int getCodigo ()
    {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo (int codigo)
    {
        this.codigo = codigo;
    }
    
   
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion ()
    {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion (java.lang.String descripcion)
    {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario ()
    {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario (java.lang.String usuario)
    {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update ()
    {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update (java.lang.String last_update)
    {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update ()
    {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update)
    {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date ()
    {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date)
    {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user ()
    {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user)
    {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property cselect.
     * @return Value of property cselect.
     */
    public java.lang.String getCselect ()
    {
        return cselect;
    }
    
    /**
     * Setter for property cselect.
     * @param cselect New value of property cselect.
     */
    public void setCselect (java.lang.String cselect)
    {
        this.cselect = cselect;
    }
    
    /**
     * Getter for property cfrom.
     * @return Value of property cfrom.
     */
    public java.lang.String getCfrom ()
    {
        return cfrom;
    }
    
    /**
     * Setter for property cfrom.
     * @param cfrom New value of property cfrom.
     */
    public void setCfrom (java.lang.String cfrom)
    {
        this.cfrom = cfrom;
    }
    
    /**
     * Getter for property cwhere.
     * @return Value of property cwhere.
     */
    public java.lang.String getCwhere ()
    {
        return cwhere;
    }
    
    /**
     * Setter for property cwhere.
     * @param cwhere New value of property cwhere.
     */
    public void setCwhere (java.lang.String cwhere)
    {
        this.cwhere = cwhere;
    }
    
    /**
     * Getter for property cotros.
     * @return Value of property cotros.
     */
    public java.lang.String getCotros ()
    {
        return cotros;
    }
    
    /**
     * Setter for property cotros.
     * @param cotros New value of property cotros.
     */
    public void setCotros (java.lang.String cotros)
    {
        this.cotros = cotros;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
}
