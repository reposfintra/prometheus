/*
 * FileGenerator.java
 *
 * Created on 30 de noviembre de 2004, 10:49 AM
 */

package com.tsp.operation.model.beans.filegeneration;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;

/**
 * Clase b�sica abstracta para generar archivos desde una p�gina JSP.
 * @author  Iv�n Herazo
 */
public abstract class FileGenerator extends ByteArrayOutputStream
{
  // TIPOS DE CONTENIDO MIME
  /** Constante que define el tipo MIME de archivos de texto plano. */
  public static String MIME_TEXT = "text/plain";
  
  /** Constante que define el tipo MIME de archivos pdf. */
  public static String MIME_PDF = "application/pdf";
  
  /** Constante que define el tipo MIME de archivos excel. */
  public static String MIME_MS_EXCEL = "application/vnd.ms-excel";
  
  protected PageContext jspContext;
  protected JspWriter out;
  protected HttpServletResponse response;
  private Hashtable attribute;
  
  private void init(PageContext jspCtx)
  {
    this.jspContext = jspCtx;
    this.attribute  = new Hashtable();
    this.out        = this.jspContext.getOut();
    this.response   = (HttpServletResponse) this.jspContext.getResponse();
    this.attribute.clear();
    this.response.addHeader("Cache-Control", "no-cache");
    this.response.addHeader("Pragma", "No-cache");
    this.response.addDateHeader("Expires", 0);
    this.response.setContentType(FileGenerator.MIME_TEXT);
  }
  
  /** Esta clase no puede instanciarse. */
  protected FileGenerator(PageContext jspCtx) {
    this.init(jspCtx);
  }
  
  /**
   * Obtiene los bytes del archivo que se va a generar.
   * @param bytes Bytes a ser a�adidos.
   */
  public byte [] getBytes() {
    return this.toByteArray();
  }
    
  /**
   * A�ade una cadena de caracteres al archivo que se va a generar.
   * Incluido por conveniencia para generar archivos de texto.
   * @param str Cadena a ser a�adida.
   */
  public void write(String str)
  {
    byte [] bytes = str.getBytes();
    super.write(bytes, 0, bytes.length);
  }
  
  /**
   * Crea un archivo de texto con los datos guardados.
   * Incluido por conveniencia para generar archivos de texto.
   * @param fileName Nombre del archivo.
   * throws IOException Si un error de E/S ocurre.
   */
  protected void createTextFile(String fileName) throws IOException
  {
    // Directorio para logs en LOCALHOST (Solo para depuraci�n).
    File localHostPath = new File("D:\\webapps\\sot\\files\\");
    
    // Directorio para logs en los servidores web (PILOTOWEB, TSPWEB).
    File webDeployPath = new File("/usr/local/tomcat/webapps/sot/files/");
    
    PrintWriter outTxtFile    = null;
    String file = null;
    if( localHostPath.exists() )
      outTxtFile = new PrintWriter(
        new BufferedWriter(new FileWriter(localHostPath.getPath() + "\\" + fileName)), true
      );
    else if( webDeployPath.exists() )
      outTxtFile = new PrintWriter(
        new BufferedWriter(new FileWriter(webDeployPath.getPath() + "/" + fileName)), true
      );
    this.setAttribute("fileUrl", "/sot/files/" + fileName);
    outTxtFile.write( new String(this.getBytes()) );
    outTxtFile.flush();
  }

  /**
   * Env�a los datos del archivo generado.
   * @throws IOException Si ocurre un error de E/S que impide el env�o.
   */
  protected void sendData() throws IOException
  {
    ServletOutputStream servletOut = this.response.getOutputStream();
    this.response.setContentLength(this.size());
    super.writeTo(servletOut);
    servletOut.flush();
  }
  
  /**
   * Inserta espacios.
   * @param count No. de espacios a insertar.
   * @return Cadena con los espacios insertados.
   */
  protected String spaces(int count)
  {
    StringBuffer spaces = new StringBuffer(0);
    for( int idx = 1; idx <= count; idx++ ) spaces.append(" ");
    return spaces.toString();
  }
  
  /**
   * Inserta espacios en HTML (non-breaking spaces)
   * @param count No. de espacios a insertar.
   * @return Cadena con los espacios insertados.
   */
  protected String nbsps(int count)
  {
    StringBuffer spaces = new StringBuffer(0);
    for( int idx = 1; idx <= count; idx++ ) spaces.append(" ");
    return spaces.toString();
  }
  
  /**
   * Inserta saltos de l�nea.
   * @param count No. de saltos de l�nea a insertar.
   * @return Cadena con los saltos de l�nea insertados.
   */
  protected String newLns(int count)
  {
    StringBuffer newLines = new StringBuffer(0);
    String newLnChar = System.getProperty("line.separator");
    for( int idx = 1; idx <= count; idx++ ) newLines.append(newLnChar);
    return newLines.toString();
  }
  
  /**
   * Inserta saltos de l�nea en HTML (&lt;br&gt;)
   * @param count No. de saltos de l�nea a insertar.
   * @return Cadena con los saltos de l�nea insertados.
   */
  protected String brs(int count)
  {
    StringBuffer breakLines = new StringBuffer(0);
    for( int idx = 1; idx <= count; idx++ ) breakLines.append("<br>");
    return breakLines.toString();
  }
  
  /**
   * Ajusta un texto en HTML a un n�mero determinado de espacios.
   * @param text Texto a ajustar.
   * @param spaceCount No. de espacios asignados al campo.
   * @return Texto ajustado.
   */
  protected String fitToNonBrSpaces(String text, int spaceCount)
  {
    String newText = "";
    int textLength = text.length();
    if( spaceCount < textLength )
      newText = text.substring(0, spaceCount);
    else if( spaceCount > textLength )
      newText += text + nbsps(spaceCount - textLength);
    else
      newText = text;
    return newText;
  }
  
  /**
   * Ajusta un texto a un n�mero determinado de espacios.
   * @param text Texto a ajustar.
   * @param spaceCount No. de espacios asignados al campo.
   * @return Texto ajustado.
   */
  protected String fitToSpaces(String text, int spaceCount)
  {
    String newText = "";
    int textLength = text.length();
    if( spaceCount < textLength )
      newText = text.substring(0, spaceCount);
    else if( spaceCount > textLength )
      newText += text + spaces(spaceCount - textLength);
    else
      newText = text;
    return newText;
  }
  
  /**
   * Retorna un atributo previamente guardado.
   * @param name Nombre del atributo.
   * @return Valor del atributo.
   */
  public Object getAttribute(Object name) {
    return this.attribute.get(name);
  }
  
  /**
   * Guarda un atributo para utilizarlo en la generaci�n del archivo.
   * @param name Nombre del atributo.
   * @return Valor del atributo.
   */
  public void setAttribute(Object name, Object value) {
    this.attribute.put(name, value);
  }
  
  /**
   * Asigna el tipo de contenido MIME devuelto al cliente, si la respuesta a�n
   * no ha sido enviada. El tipo especificado puede incluir una especificaci�n
   * de codificaci�n de caracteres (Ej: text/html;charset=UTF-8)<br>
   * Este m�todo puede ser llamado repetitivamente para cambiar el tipo de
   * contenido MIME y la codificaci�n de caracteres.
   * @param mimeType Una cadena de cadena de caracteres que especifica el tipo
   *                 MIME del contenido devuelto como respuesta.
   */
  public void setContentType(String mimeType) {
    this.response.setContentType( mimeType );
  }
  
  /**
   * Prepara la siguiente p�gina a ser impresa.
   * @param preparePage Si es <code>true</code>, la nueva p�gina es preparada.
   *                    si es <code>false</code>, no hace nada.
   * @throws IOException Si un error de E/S ocurre.
   */
  abstract protected void newPage(boolean preparePage) throws IOException;
  
  /**
   * Genera el archivo.
   * Este m�todo debe sobrecargarse en las clases que se deriven de esta
   * para generar el tipo de archivo espec�fico.
   * @throws IOException Si ocurre un error durante la generaci�n del archivo.
   */
  abstract public void generateFile() throws IOException;
}
