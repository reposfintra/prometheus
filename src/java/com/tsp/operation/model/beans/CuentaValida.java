/***********************************************************************
 *      Nombre Clase.................   CuentaValida.java
 *      Descripci�n..................   Bean del programa de CuentaValida,
 *      Autor........................   Ing. Leonardo Parodi Ponce
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ***********************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import java.lang.Integer.*;

public class CuentaValida implements Serializable{
        
        private String clase;
        private String agencia;
        private String unidad;
        private String cliente_area;
        private String elemento;
        private String creation_user;
        private java.util.Date creation_date;
        private String base;
        private String dstrct;
        private java.util.Date last_update;
        private String user_update;
        private String reg_status;
        private String nombreclase;
        private String nombreagencia;
        private String nombreunidad;
        private String nombrecliente_area;
        private String nombreelemento;
        private String cuentaequivalente;
        
       
        
        /** Creates a new instance of CuentaValida */
        public CuentaValida() {
        }
        
        
        public CuentaValida Load(ResultSet rs) throws SQLException{
                CuentaValida cuenta = new CuentaValida();
                cuenta.setClase(rs.getString("clase"));
                cuenta.setAgencia(rs.getString("cod_agencia"));
                cuenta.setUnidad(rs.getString("unidad"));
                cuenta.setCliente_area(rs.getString("cliente_area"));
                cuenta.setElemento(rs.getString("elemento"));
                String clas = rs.getString("nombreclase");
                if (clas!=null){
                        cuenta.setNombreClase(clas);
                }else{
                        cuenta.setNombreClase("");
                }
                String nomagen = rs.getString("nombreagencia");
                if (nomagen!=null){
                        cuenta.setNombreAgencia(nomagen);
                }else{
                        cuenta.setNombreAgencia("");
                }
                String nomauni = rs.getString("nombreunidad");
                if (nomagen!=null){
                        cuenta.setNombreUnidad(nomauni);
                }else{
                        cuenta.setNombreUnidad("");
                }
                String nomacliare = rs.getString("nombrecliente_area");
                if (nomagen!=null){
                        cuenta.setNombreCliente_area(nomacliare);
                }else{
                        cuenta.setNombreCliente_area("");
                }
                String nomelem = rs.getString("nombreelemento");
                if (nomagen!=null){
                        cuenta.setNombreElemento(nomelem);
                }else{
                        cuenta.setNombreElemento("");
                }
                cuenta.setBase(rs.getString("base"));
                cuenta.setCreation_user(rs.getString("creation_user"));
                cuenta.setDstrct(rs.getString("dstrct"));
                cuenta.setLast_update(rs.getTimestamp("last_update"));
                cuenta.setCreation_date(rs.getTimestamp("creation_date"));
                cuenta.setReg_status(rs.getString("reg_status"));
                cuenta.setUser_update(rs.getString("user_update"));
                cuenta.setCuentaEquivalente(rs.getString("cuenta"));
               
                return cuenta;
        }
        
        /**
         * Setter for property cuentaequivalente.
         * @param clase New value of property cuentaequivalente.
         */
        public void setCuentaEquivalente(String Cuenta){
                
                this.cuentaequivalente = Cuenta;
                
        }
        
        /**
         * Getter for property cuentaequivalente.
         * @return Value of property cuentaequivalente.
         */
        public java.lang.String getCuentaEquivalente() {
                return cuentaequivalente;
        }
        
        
        /**
         * Setter for property clase.
         * @param clase New value of property clase.
         */
        public void setNombreClase(String Clase){
                
                this.nombreclase = Clase;
                
        }
        
        /**
         * Getter for property clase.
         * @return Value of property clase.
         */
        public java.lang.String getNombreClase() {
                return nombreclase;
        }
        /**
         * Setter for property unidad.
         * @param unidad New value of property unidad.
         */
        public void setNombreUnidad(String Unidad){
                
                this.nombreunidad = Unidad;
                
        }
        /**
         * Getter for property unidad.
         * @return Value of property unidad.
         */
        public java.lang.String getNombreUnidad() {
                return nombreunidad;
        }
        
        /**
         * Setter for property agencia.
         * @param agencia New value of property agencia.
         */
        public void setNombreAgencia(String Agencia){
                
                this.nombreagencia = Agencia;
                
        }
        /**
         * Getter for property agencia.
         * @return Value of property agencia.
         */
        public java.lang.String getNombreAgencia() {
                return nombreagencia;
        }
        /**
         * Setter for property cliente_area.
         * @param cliente_area New value of property cliente_area.
         */
        public void setNombreCliente_area(String Cliente_area){
                
                this.nombrecliente_area = Cliente_area;
                
        }
        
        /**
         * Getter for property cliente_area.
         * @return Value of property cliente_area.
         */
        public java.lang.String getNombreClienteArea() {
                return nombrecliente_area;
        }
        
        /**
         * Setter for property elemento.
         * @param elemento New value of property elemento.
         */
        public void setNombreElemento(String Elemento){
                
                this.nombreelemento = Elemento;
                
        }
        /**
         * Getter for property elemento.
         * @return Value of property elemento.
         */
        public java.lang.String getNombreElemento() {
                return nombreelemento;
        }
        /**
         * Setter for property clase.
         * @param clase New value of property clase.
         */
        public void setClase(String Clase){
                
                this.clase = Clase;
                
        }
        
        /**
         * Getter for property clase.
         * @return Value of property clase.
         */
        public java.lang.String getClase() {
                return clase;
        }
        /**
         * Setter for property unidad.
         * @param unidad New value of property unidad.
         */
        public void setUnidad(String Unidad){
                
                this.unidad = Unidad;
                
        }
        /**
         * Getter for property unidad.
         * @return Value of property unidad.
         */
        public java.lang.String getUnidad() {
                return unidad;
        }
        
        /**
         * Setter for property agencia.
         * @param agencia New value of property agencia.
         */
        public void setAgencia(String Agencia){
                
                this.agencia = Agencia;
                
        }
        /**
         * Getter for property agencia.
         * @return Value of property agencia.
         */
        public java.lang.String getAgencia() {
                return agencia;
        }
        /**
         * Setter for property cliente_area.
         * @param cliente_area New value of property cliente_area.
         */
        public void setCliente_area(String Cliente_area){
                
                this.cliente_area = Cliente_area;
                
        }
        
        /**
         * Getter for property cliente_area.
         * @return Value of property cliente_area.
         */
        public java.lang.String getClienteArea() {
                return cliente_area;
        }
        
        /**
         * Setter for property elemento.
         * @param elemento New value of property elemento.
         */
        public void setElemento(String Elemento){
                
                this.elemento = Elemento;
                
        }
        /**
         * Getter for property elemento.
         * @return Value of property elemento.
         */
        public java.lang.String getElemento() {
                return elemento;
        }
        
         /**
         * Setter for property creation_date.
         * @param creation_date New value of property creation_date.
         */
        public void setCreation_date(java.util.Date Creation_date){
                
                this.creation_date = Creation_date;
                
        }
        
        /**
         * Setter for property creation_user.
         * @param creation_user New value of property creation_user.
         */
        public void setCreation_user(String Creation_user){
                
                this.creation_user = Creation_user;
                
        }
        
        /**
         * Getter for property creation_date.
         * @return value of property creation_date.
         */
        public java.util.Date getCreation_date(){
                
                return this.creation_date;
                
        }
        
        /**
         * Getter for property creation_user.
         * @return value of property creation_user.
         */
        public String getCreation_user(){
                
                return this.creation_user;
                
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }
        
        /**
         * Getter for property dstrct.
         * @return Value of property dstrct.
         */
        public java.lang.String getDstrct() {
                return dstrct;
        }
        
        /**
         * Setter for property dstrct.
         * @param dstrct New value of property dstrct.
         */
        public void setDstrct(java.lang.String dstrct) {
                this.dstrct = dstrct;
        }
        
        /**
         * Getter for property last_update.
         * @return Value of property last_update.
         */
        public java.util.Date getLast_update() {
                return last_update;
        }
        
        /**
         * Setter for property last_update.
         * @param last_update New value of property last_update.
         */
        public void setLast_update(java.util.Date Last_update) {
                this.last_update = Last_update;
        }
        
        /**
         * Getter for property reg_status.
         * @return Value of property reg_status.
         */
        public java.lang.String getReg_status() {
                return reg_status;
        }
        
        /**
         * Setter for property reg_status.
         * @param reg_status New value of property reg_status.
         */
        public void setReg_status(java.lang.String reg_status) {
                this.reg_status = reg_status;
        }
        
        /**
         * Getter for property user_update.
         * @return Value of property user_update.
         */
        public java.lang.String getUser_update() {
                return user_update;
        }
        
        /**
         * Setter for property user_update.
         * @param user_update New value of property user_update.
         */
        public void setUser_update(java.lang.String user_update) {
                this.user_update = user_update;
        }
        
}
