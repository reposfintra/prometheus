/*
 * Viajes.java
 *
 * Created on 31 de julio de 2005, 10:07
 */

package com.tsp.operation.model.beans;
/**
 *
 * @author  mario
 */

import java.util.*;
import java.lang.Math;


public class ViajesStandar extends ViajesProceso {
    
    private String std_job_no;
    private String std_job_desc;
    
    /*Jescandon 13-12-05*/
    private List ListCostosOperativos;
    private double valor_total_costos;
    
    /** Creates a new instance of Viajes */
    public ViajesStandar() {
        std_job_no     = "";
        std_job_desc   = "";
        ListCostosOperativos = new LinkedList();
        valor_total_costos = 0;
    }
    
    ///////////////////////////////////////////////////////
    // setter
    public void setStdJobNo (String newValue){
        std_job_no = newValue;
    }
    public void setStdJobDesc(String newValue){
        std_job_desc = newValue;
    }
    
    ////////////////////////////////////////////////////////////
    // getter
    public String getStdJobNo (){
        return std_job_no;
    }
    public String getStdJobDesc(){
        return std_job_desc;
    }
    
    public void Calcular(){
        this.CalcularDiferencia();
    }
    
    /**
     * Getter for property ListCostosOperativos.
     * @return Value of property ListCostosOperativos.
     */
    public java.util.List getListCostosOperativos() {
        return ListCostosOperativos;
    }    
    
    /**
     * Setter for property ListCostosOperativos.
     * @param ListCostosOperativos New value of property ListCostosOperativos.
     */
    public void setListCostosOperativos(java.util.List ListCostosOperativos) {
        this.ListCostosOperativos = ListCostosOperativos;
    }    
    
    /**
     * Getter for property valor_total_costos.
     * @return Value of property valor_total_costos.
     */
    public double getValor_total_costos() {
        return valor_total_costos;
    }
    
    /**
     * Setter for property valor_total_costos.
     * @param valor_total_costos New value of property valor_total_costos.
     */
    public void setValor_total_costos(double valor_total_costos) {
        this.valor_total_costos = valor_total_costos;
    }
    
}
