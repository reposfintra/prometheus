/*
 *  Nombre clase    :  BalancePrueba.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 20 de junio de 2006, 11:43 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;

import java.util.*;
import java.sql.*;

public class BPCuentasAlpha extends BPCuentas{
      
    private List ElementoGasto;               
    
    /** Crea una nueva instancia de  BalancePrueba */
    public BPCuentasAlpha() {
        ElementoGasto = new LinkedList();
    }
    
    
    
    public void addElementos(BPElementosGasto e){
        ElementoGasto.add(e);
    }
    
    public void Calcular( int mes ){
        
        if (ElementoGasto!=null && ElementoGasto.size()>0){
            for (int i = 0; i < ElementoGasto.size();i++){
                BPElementosGasto bpg = (BPElementosGasto) ElementoGasto.get(i);
                this.setVMovCredito( mes,   bpg.getVMovCredito(mes));
                this.setVMovDebito(  mes ,  bpg.getVMovDebito(mes));
            }
            CalcularSaldos(mes);
        }
        
    }
    
    /**
     * Getter for property ElementoGasto.
     * @return Value of property ElementoGasto.
     */
    public java.util.List getElementoGasto() {
        return ElementoGasto;
    }
    
    /**
     * Setter for property ElementoGasto.
     * @param ElementoGasto New value of property ElementoGasto.
     */
    public void setElementoGasto(java.util.List ElementoGasto) {
        this.ElementoGasto = ElementoGasto;
    }
    
}
