/*
 * TBLTIEMPO.java
 *
 * Created on 19 de noviembre de 2004, 08:50 AM
 */
package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * @author KREALES
 */
public class Tbltiempo implements Serializable {
    
    private String dstrct;
    private String sj;
    private String cf_code;
    private String time_code;
    private String time_description;
    private String nmonic_code;
    private int secuence;
    private String presentacion;
    private String presentacion_es;
    
    public static Tbltiempo load(ResultSet rs)throws SQLException{
        
        Tbltiempo tbltiempo = new Tbltiempo();
        
        tbltiempo.setDstrct(rs.getString(2));
        tbltiempo.setSj(rs.getString(3));
        tbltiempo.setCf_code(rs.getString(4));
        tbltiempo.setTimeCode(rs.getString(5));
        tbltiempo.setTimeDescription(rs.getString(6));
        tbltiempo.setNmonicCode(rs.getString(7));
        tbltiempo.setSecuence(rs.getInt(8));
        tbltiempo.setPresentacion(rs.getString(9));
        tbltiempo.setPresentacion_es(rs.getString(10));
        
        return tbltiempo;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    
    public void setSj(String sj){
        
        this.sj=sj;
    }
    
    public String getSj(){
        
        return sj;
    }
    public void setCf_code(String cf_code){
        
        this.cf_code=cf_code;
    }
    
    public String getCf_code(){
        
        return cf_code;
    }
    public void setTimeCode(String time_code){
        
        this.time_code=time_code;
    }
    
    public String getTimeCode(){
        
        return time_code;
    }
    public void setTimeDescription(String time_description){
        
        this.time_description=time_description;
    }
    
    public String getTimeDescription(){
        
        return time_description;
    }
    public void setNmonicCode(String nmonic_code){
        
        this.nmonic_code=nmonic_code;
    }
    
    public String getNmonicCode(){
        
        return nmonic_code;
    }
    public void setSecuence(int secuence){
        
        this.secuence=secuence;
    }
    
    public int getSecuence(){
        
        return secuence;
    }
    public void setPresentacion(String presentacion){
        
        this.presentacion=presentacion;
    }
    
    public String getPresentacion(){
        
        return presentacion;
    }
    public void setPresentacion_es(String presentacion_es){
        
        this.presentacion_es=presentacion_es;
    }
    
    public String getPresentacion_es(){
        
        return presentacion_es;
    }
}
