package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * <br/>
 * 25/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class CuentaCartera {

    private Boolean bloqueada;
    private String periodicidad;
    private String comportamiento;
    private Timestamp fechaApertura;
    private Timestamp fechaVencimiento;
    private String numeroObligacion;
    private Timestamp ultimaActualizacion;
    private String entidad;
    private String estado;
    private String estado48;
    private String tipoObligacion;
    private String tipoCuenta;
    private String garante;
    private String formaPago;
    private String codSuscriptor;
    private String positivoNegativo;
    private String oficina;
    private Short mesesPermanencia;
    private String situacionTitular;
    private String estadoOrigen;
    private String tipoContrato;
    private Short ejecucionContrato;
    private String prescripcion;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private String reclamo;
    private Valor valor;
    private String sector;
    private String nitEmpresa;    
    private double valor_inicial;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of sector
     *
     * @return the value of sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * Set the value of sector
     *
     * @param sector new value of sector
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * Get the value of valor
     *
     * @return the value of valor
     */
    public Valor getValor() {
        return valor;
    }

    /**
     * Set the value of valor
     *
     * @param valor new value of valor
     */
    public void setValor(Valor valor) {
        this.valor = valor;
    }

    /**
     * Get the value of reclamo
     *
     * @return the value of reclamo
     */
    public String getReclamo() {
        return reclamo;
    }

    /**
     * Set the value of reclamo
     *
     * @param reclamo new value of reclamo
     */
    public void setReclamo(String reclamo) {
        this.reclamo = reclamo;
    }

    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of prescripcion
     *
     * @return the value of prescripcion
     */
    public String getPrescripcion() {
        return prescripcion;
    }

    /**
     * Set the value of prescripcion
     *
     * @param prescripcion new value of prescripcion
     */
    public void setPrescripcion(String prescripcion) {
        this.prescripcion = prescripcion;
    }

    /**
     * Get the value of ejecucionContrato
     *
     * @return the value of ejecucionContrato
     */
    public Short getEjecucionContrato() {
        return ejecucionContrato;
    }

    /**
     * Set the value of ejecucionContrato
     *
     * @param ejecucionContrato new value of ejecucionContrato
     */
    public void setEjecucionContrato(short ejecucionContrato) {
        this.ejecucionContrato = ejecucionContrato;
    }

    /**
     * Get the value of tipoContrato
     *
     * @return the value of tipoContrato
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Set the value of tipoContrato
     *
     * @param tipoContrato new value of tipoContrato
     */
    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    /**
     * Get the value of estadoOrigen
     *
     * @return the value of estadoOrigen
     */
    public String getEstadoOrigen() {
        return estadoOrigen;
    }

    /**
     * Set the value of estadoOrigen
     *
     * @param estadoOrigen new value of estadoOrigen
     */
    public void setEstadoOrigen(String estadoOrigen) {
        this.estadoOrigen = estadoOrigen;
    }

    /**
     * Get the value of situacionTitular
     *
     * @return the value of situacionTitular
     */
    public String getSituacionTitular() {
        return situacionTitular;
    }

    /**
     * Set the value of situacionTitular
     *
     * @param situacionTitular new value of situacionTitular
     */
    public void setSituacionTitular(String situacionTitular) {
        this.situacionTitular = situacionTitular;
    }

    /**
     * Get the value of mesesPermanencia
     *
     * @return the value of mesesPermanencia
     */
    public Short getMesesPermanencia() {
        return mesesPermanencia;
    }

    /**
     * Set the value of mesesPermanencia
     *
     * @param mesesPermanencia new value of mesesPermanencia
     */
    public void setMesesPermanencia(short mesesPermanencia) {
        this.mesesPermanencia = mesesPermanencia;
    }

    /**
     * Get the value of oficina
     *
     * @return the value of oficina
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * Set the value of oficina
     *
     * @param oficina new value of oficina
     */
    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    /**
     * Get the value of positivoNegativo
     *
     * @return the value of positivoNegativo
     */
    public String getPositivoNegativo() {
        return positivoNegativo;
    }

    /**
     * Set the value of positivoNegativo
     *
     * @param positivoNegativo new value of positivoNegativo
     */
    public void setPositivoNegativo(String positivoNegativo) {
        this.positivoNegativo = positivoNegativo;
    }

    /**
     * Get the value of codSuscriptor
     *
     * @return the value of codSuscriptor
     */
    public String getCodSuscriptor() {
        return codSuscriptor;
    }

    /**
     * Set the value of codSuscriptor
     *
     * @param codSuscriptor new value of codSuscriptor
     */
    public void setCodSuscriptor(String codSuscriptor) {
        this.codSuscriptor = codSuscriptor;
    }

    /**
     * Get the value of formaPago
     *
     * @return the value of formaPago
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * Set the value of formaPago
     *
     * @param formaPago new value of formaPago
     */
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    /**
     * Get the value of garante
     *
     * @return the value of garante
     */
    public String getGarante() {
        return garante;
    }

    /**
     * Set the value of garante
     *
     * @param garante new value of garante
     */
    public void setGarante(String garante) {
        this.garante = garante;
    }

    /**
     * Get the value of tipoCuenta
     *
     * @return the value of tipoCuenta
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * Set the value of tipoCuenta
     *
     * @param tipoCuenta new value of tipoCuenta
     */
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    /**
     * Get the value of tipoObligacion
     *
     * @return the value of tipoObligacion
     */
    public String getTipoObligacion() {
        return tipoObligacion;
    }

    /**
     * Set the value of tipoObligacion
     *
     * @param tipoObligacion new value of tipoObligacion
     */
    public void setTipoObligacion(String tipoObligacion) {
        this.tipoObligacion = tipoObligacion;
    }

    /**
     * Get the value of estado48
     *
     * @return the value of estado48
     */
    public String getEstado48() {
        return estado48;
    }

    /**
     * Set the value of estado48
     *
     * @param estado48 new value of estado48
     */
    public void setEstado48(String estado48) {
        this.estado48 = estado48;
    }


    /**
     * Get the value of entidad
     *
     * @return the value of entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Set the value of entidad
     *
     * @param entidad new value of entidad
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * Get the value of estado
     *
     * @return the value of estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Set the value of estado
     *
     * @param estado new value of estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Get the value of ultimaActualizacion
     *
     * @return the value of ultimaActualizacion
     */
    public Timestamp getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    /**
     * Set the value of ultimaActualizacion
     *
     * @param ultimaActualizacion new value of ultimaActualizacion
     */
    public void setUltimaActualizacion(Timestamp ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }
    /**
     * Get the value of numeroObligacion
     *
     * @return the value of numeroObligacion
     */
    public String getNumeroObligacion() {
        return numeroObligacion;
    }

    /**
     * Set the value of numeroObligacion
     *
     * @param numeroObligacion new value of numeroObligacion
     */
    public void setNumeroObligacion(String numeroObligacion) {
        this.numeroObligacion = numeroObligacion;
    }

    /**
     * Get the value of fechaVencimiento
     *
     * @return the value of fechaVencimiento
     */
    public Timestamp getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Set the value of fechaVencimiento
     *
     * @param fechaVencimiento new value of fechaVencimiento
     */
    public void setFechaVencimiento(Timestamp fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * Get the value of fechaApertura
     *
     * @return the value of fechaApertura
     */
    public Timestamp getFechaApertura() {
        return fechaApertura;
    }

    /**
     * Set the value of fechaApertura
     *
     * @param fechaApertura new value of fechaApertura
     */
    public void setFechaApertura(Timestamp fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    /**
     * Get the value of comportamiento
     *
     * @return the value of comportamiento
     */
    public String getComportamiento() {
        return comportamiento;
    }

    /**
     * Set the value of comportamiento
     *
     * @param comportamiento new value of comportamiento
     */
    public void setComportamiento(String comportamiento) {
        this.comportamiento = comportamiento;
    }

    /**
     * Get the value of periodicidad
     *
     * @return the value of periodicidad
     */
    public String getPeriodicidad() {
        return periodicidad;
    }

    /**
     * Set the value of periodicidad
     *
     * @param periodicidad new value of periodicidad
     */
    public void setPeriodicidad(String periodicidad) {
        this.periodicidad = periodicidad;
    }

    /**
     * Get the value of bloqueada
     *
     * @return the value of bloqueada
     */
    public Boolean getBloqueada() {
        return bloqueada;
    }

    /**
     * Set the value of bloqueada
     *
     * @param bloqueada new value of bloqueada
     */
    public void setBloqueada(boolean bloqueada) {
        this.bloqueada = bloqueada;
    }

    
    public double getValor_Inicial() {
        return valor_inicial;
    }

    public void setValor_Inicial(double valor_inicial) {
        this.valor_inicial = valor_inicial;
    }

    }
