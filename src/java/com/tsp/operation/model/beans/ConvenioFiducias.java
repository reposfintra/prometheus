/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Iris Vargas
 */
public class ConvenioFiducias implements Serializable {

    private String reg_status, dstrct, id_convenio, nit_fiducia, creation_user, user_update, creation_date,
            last_update;
    private String prefijo_end_fiducia, hc_end_fiducia,
            prefijo_dif_fiducia, hc_dif_fiducia, cuenta_dif_fiducia, nombre_fiducia;
    private ArrayList<ConvenioCxcFiducias>convenioCxcFiducias;

    public ConvenioFiducias() {
        this.reg_status = "";
        this.dstrct = "";
        this.id_convenio = "";
        this.nit_fiducia = "";
        this.creation_user = "";
        this.user_update = "";
        this.creation_date = "";
        this.last_update = "";
        this.prefijo_end_fiducia="";
        this.hc_end_fiducia="";
        this.prefijo_dif_fiducia="";
        this.hc_dif_fiducia="";
        this.cuenta_dif_fiducia="";
        this.nombre_fiducia="";

    }

    public String getCuenta_dif_fiducia() {
        return cuenta_dif_fiducia;
    }

    public void setCuenta_dif_fiducia(String cuenta_dif_fiducia) {
        this.cuenta_dif_fiducia = cuenta_dif_fiducia;
    }

    public String getHc_dif_fiducia() {
        return hc_dif_fiducia;
    }

    public void setHc_dif_fiducia(String hc_dif_fiducia) {
        this.hc_dif_fiducia = hc_dif_fiducia;
    }

    public String getHc_end_fiducia() {
        return hc_end_fiducia;
    }

    public void setHc_end_fiducia(String hc_end_fiducia) {
        this.hc_end_fiducia = hc_end_fiducia;
    }

    public String getNit_fiducia() {
        return nit_fiducia;
    }

    public void setNit_fiducia(String nit_fiducia) {
        this.nit_fiducia = nit_fiducia;
    }

    public String getPrefijo_dif_fiducia() {
        return prefijo_dif_fiducia;
    }

    public void setPrefijo_dif_fiducia(String prefijo_dif_fiducia) {
        this.prefijo_dif_fiducia = prefijo_dif_fiducia;
    }

    public String getPrefijo_end_fiducia() {
        return prefijo_end_fiducia;
    }

    public void setPrefijo_end_fiducia(String prefijo_end_fiducia) {
        this.prefijo_end_fiducia = prefijo_end_fiducia;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getId_convenio() {
        return id_convenio;
    }

    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public ArrayList<ConvenioCxcFiducias> getConvenioCxcFiducias() {
        return convenioCxcFiducias;
    }

    public void setConvenioCxcFiducias(ArrayList<ConvenioCxcFiducias> convenioCxcFiducias) {
        this.convenioCxcFiducias = convenioCxcFiducias;
    }

     public String getNombre_fiducia() {
        return nombre_fiducia;
    }

    public void setNombre_fiducia(String nombre_fiducia) {
        this.nombre_fiducia = nombre_fiducia;
    }

}
