/**************************************************************************
 * Nombre: ......................ActividadPlan.java                       *
 * Descripci�n: .................Beans de acuerdo especial.               *
 * Autor:........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:........................12 de septiembre de 2005, 12:02 PM       *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;


public class ActividadPlan implements Serializable {
    private String fecpla;
    private String numpla;
    private String tipoviaje;
    private String origen;
    private String destino;
    private String cod_actividad;
    private String numrem;
    private String codcliente;
    private String cliente;
    private String plaveh;
    private String actividad;
    
    /** Creates a new instance of ActividadPlan */
    public static ActividadPlan load(ResultSet rs)throws SQLException {
        ActividadPlan  actplan = new ActividadPlan ();
        actplan.setNumpla(rs.getString("numpla"));
        actplan.setNumrem(rs.getString("numrem"));
        actplan.setTipoViaje(rs.getString("tipoviaje"));
        actplan.setCodcliente(rs.getString("codcliente"));
        actplan.setCliente(rs.getString("nom_cliente"));
        
        return actplan;
    }
    
    public void setFecpla(String fec){
        this.fecpla = fec;
    }
    public String getFecpla(){
        return fecpla;
    }
    public void setNumpla(String num){
        this.numpla = num;
    }
    public String getNumpla(){
        return numpla;
    }
    public void setTipoViaje(String tipo){
        this.tipoviaje = tipo;
    }
    public String getTipoViaje(){
        return tipoviaje;
    }
    public void setOrigen(String ori){
        this.origen = ori;
    }
    public String getOrigen(){
        return origen;
    }
    public void setDestino(String des){
        this.destino = des;
    }
    public String getDestino(){
        return destino;
    }
    public void setCod_actividad(String cod){
        this.cod_actividad = cod;
    }
    public String getCod_actividad(){
        return cod_actividad;
    }
    public void setNumrem(String num){
        this.numrem = num;
    }
    public String getNumrem(){
        return numrem;
    }
    public void setCodcliente(String codcli){
        this.codcliente = codcli;
    }
    public String getCodcliente(){
        return codcliente;
    }
    public void setCliente(String cli){
        this.cliente = cli;
    }
    public String getCliente (){
        return cliente;
    }
    public void setPlaveh(String pla){
        this.plaveh = pla;
    }
    public String getPlaveh(){
        return plaveh;
    }
    public void setActividad(String act){
        this.actividad = act;
    }
    public String getActividad(){
        return actividad;
    }
}
