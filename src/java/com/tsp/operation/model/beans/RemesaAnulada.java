/*
 * RemesaAnulada.java
 *
 * Created on 18 de julio de 2005, 02:58 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  kreales
 */
public class RemesaAnulada {
    
    private String numero_remesa;
    private String fecha_creacion;
    private String usuario_creacion;
    private String codigo_creacion;
    
    /** Creates a new instance of RemesaAnulada */
    public RemesaAnulada() {
    }
    
    /**
     * Getter for property codigo_creacion.
     * @return Value of property codigo_creacion.
     */
    public java.lang.String getCodigo_creacion() {
        return codigo_creacion;
    }
    
    /**
     * Setter for property codigo_creacion.
     * @param codigo_creacion New value of property codigo_creacion.
     */
    public void setCodigo_creacion(java.lang.String codigo_creacion) {
        this.codigo_creacion = codigo_creacion;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property numero_remesa.
     * @return Value of property numero_remesa.
     */
    public java.lang.String getNumero_remesa() {
        return numero_remesa;
    }
    
    /**
     * Setter for property numero_remesa.
     * @param numero_remesa New value of property numero_remesa.
     */
    public void setNumero_remesa(java.lang.String numero_remesa) {
        this.numero_remesa = numero_remesa;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
}
