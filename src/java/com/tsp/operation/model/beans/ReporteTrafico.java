/*
* Nombre        ReporteTrafico.java
* Descripci�n   Esta clase representa a un reporte de trafico
* Autor         Nestor Parejo
* Fecha         (desconocida)
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

import com.tsp.util.*;

/**
 * Esta clase representa a un reporte de trafico
 * @author Nestor Parejo
 */
public class ReporteTrafico implements java.io.Serializable
{
   private String planilla;
   private String nomciu;
   private String tipo;
   private String fechaReporte;
   private String horaReporte;   
   
   public static ReporteTrafico load(ResultSet rs)
      throws SQLException
   {
      ReporteTrafico reporte = new ReporteTrafico();

      reporte.setPlanilla( rs.getString("PLANILLA") );
      reporte.setTipo( rs.getString("DIRMOTR"), rs.getString("TIPOMTR")  );
      reporte.setFechaReporte( rs.getString("FECHAREPORTE") );
      reporte.setHoraReporte( rs.getString("HORAREPORTE") );
      reporte.setNomCiu( rs.getString("NOMCIU") );
      
      return reporte;
   }

   /**
   * Returns the object as a CSV string
   */
   public String toString()
   {
      StringBuffer sb = new StringBuffer();

      if (getPlanilla() != null)
         sb.append(Util.quote(getPlanilla()));

      sb.append(",");
      if (getNomCiu() != null)
         sb.append(Util.quote(getNomCiu()));

      sb.append(",");
      if (getFechaReporte() != null)
         sb.append(Util.quote(getFechaReporte()));
      
      sb.append(",");
      if (getHoraReporte() != null)
         sb.append(Util.quote(getHoraReporte()));
      
      
      return sb.toString();
   }

   // ===========================================
   //    Property accessor methods
   // ===========================================

   /**
   * Retorna el numero de planilla/oc
   */
   public String getPlanilla()
   {
      return planilla;
   }

   /**
   * Asigna numero de planilla/OC
   * @param newplanilla
   */
   public void setPlanilla(String newPlanilla )
   {
      this.planilla = newPlanilla;
   }

   /**
   * Returns the name.
   */
   public String getNomCiu()
   {
      return nomciu;
   }

   /**
   * Sets the name.
   * @param name the name.
   */
   public void setNomCiu(String nomciu)
   {
      this.nomciu = nomciu;
   }
   
   public String getTipo()
   {
      return tipo;
   }

   public void setTipo(String dirmotr, String tipomtr )
   {
      if ( dirmotr.equals("S") ) {
          this.tipo = "SALIDA";    
      } else if ( dirmotr.equals("B") ) {
          this.tipo = "BODEGA";    
      } else if ( dirmotr.equals("V") ) {
          this.tipo = "VARADO";    
      } else if ( dirmotr.equals("X") ) {
          this.tipo = "ENTREGA PARCIAL";    
      } else if ( tipomtr.equals("E") ) {
          this.tipo = "ENTREGA";    
      } else {   this.tipo = "PASO CONTROL";    
      }
   }   
   
   public String getFechaReporte()
   {
      return fechaReporte;
   }

   public void setFechaReporte(String newFechaReporte)
   {
      this.fechaReporte = newFechaReporte;
   }   

   public String getHoraReporte()
   {
      return horaReporte;
   }

   public void setHoraReporte(String newHoraReporte)
   {
      this.horaReporte = newHoraReporte;
   }   
   
}
