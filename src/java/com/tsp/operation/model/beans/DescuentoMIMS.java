/**
 * Nombre        DescuentoMIMS.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         11 de agosto de 2006, 08:38 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.beans;


public class DescuentoMIMS {
    
    private String tipoRegistro;
    
    private String distrito;
    private String placa;
    private String factura;
    private String factura_ext;
    private String fecha;
    private String estado;
    private String tipo;
    private String banco;
    private String sucursal;
    private double valor;
    private String moneda;
    private String nit;
    private String id_mims;
    private String nombre;
    private String item;
    private String descripcion;
    private String planilla;
    private double valor_item;
    private double retefuente;
    private double iva;
    private double riva;
    private double rica;
    private String account;
    
    private String elemento;
    private String codConcepto;
    private String desConcepto;
    private String asignador;
    private String indicador;
    private String agencia;
    private String cheque;
    private String tipoCuenta;
    
    private String origen;
    private String origenNombre;
    private String destino;
    private String destinoNombre;
    private String cliente;
    private String clienteNombre;
    private String remesa;
    private double tonelaje;
    
    
    /** Crea una nueva instancia de  DescuentoMIMS */
    public DescuentoMIMS() {
    }
    
    /**
     * Getter for property account.
     * @return Value of property account.
     */
    public java.lang.String getAccount() {
        return account;
    }
    
    /**
     * Setter for property account.
     * @param account New value of property account.
     */
    public void setAccount(java.lang.String account) {
        this.account = account;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property factura_ext.
     * @return Value of property factura_ext.
     */
    public java.lang.String getFactura_ext() {
        return factura_ext;
    }
    
    /**
     * Setter for property factura_ext.
     * @param factura_ext New value of property factura_ext.
     */
    public void setFactura_ext(java.lang.String factura_ext) {
        this.factura_ext = factura_ext;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property iva.
     * @return Value of property iva.
     */
    public double getIva() {
        return iva;
    }
    
    /**
     * Setter for property iva.
     * @param iva New value of property iva.
     */
    public void setIva(double iva) {
        this.iva = iva;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property retefuente.
     * @return Value of property retefuente.
     */
    public double getRetefuente() {
        return retefuente;
    }
    
    /**
     * Setter for property retefuente.
     * @param retefuente New value of property retefuente.
     */
    public void setRetefuente(double retefuente) {
        this.retefuente = retefuente;
    }
    
    /**
     * Getter for property rica.
     * @return Value of property rica.
     */
    public double getRica() {
        return rica;
    }
    
    /**
     * Setter for property rica.
     * @param rica New value of property rica.
     */
    public void setRica(double rica) {
        this.rica = rica;
    }
    
    /**
     * Getter for property riva.
     * @return Value of property riva.
     */
    public double getRiva() {
        return riva;
    }
    
    /**
     * Setter for property riva.
     * @param riva New value of property riva.
     */
    public void setRiva(double riva) {
        this.riva = riva;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property valor_item.
     * @return Value of property valor_item.
     */
    public double getValor_item() {
        return valor_item;
    }
    
    /**
     * Setter for property valor_item.
     * @param valor_item New value of property valor_item.
     */
    public void setValor_item(double valor_item) {
        this.valor_item = valor_item;
    }
    
    /**
     * Getter for property elemento.
     * @return Value of property elemento.
     */
    public java.lang.String getElemento() {
        return elemento;
    }
    
    /**
     * Setter for property elemento.
     * @param elemento New value of property elemento.
     */
    public void setElemento(java.lang.String elemento) {
        this.elemento = elemento;
    }
    
    /**
     * Getter for property codConcepto.
     * @return Value of property codConcepto.
     */
    public java.lang.String getCodConcepto() {
        return codConcepto;
    }
    
    /**
     * Setter for property codConcepto.
     * @param codConcepto New value of property codConcepto.
     */
    public void setCodConcepto(java.lang.String codConcepto) {
        this.codConcepto = codConcepto;
    }
    
    /**
     * Getter for property desConcepto.
     * @return Value of property desConcepto.
     */
    public java.lang.String getDesConcepto() {
        return desConcepto;
    }
    
    /**
     * Setter for property desConcepto.
     * @param desConcepto New value of property desConcepto.
     */
    public void setDesConcepto(java.lang.String desConcepto) {
        this.desConcepto = desConcepto;
    }
    
    /**
     * Getter for property asignador.
     * @return Value of property asignador.
     */
    public java.lang.String getAsignador() {
        return asignador;
    }
    
    /**
     * Setter for property asignador.
     * @param asignador New value of property asignador.
     */
    public void setAsignador(java.lang.String asignador) {
        this.asignador = asignador;
    }
    
    /**
     * Getter for property indicador.
     * @return Value of property indicador.
     */
    public java.lang.String getIndicador() {
        return indicador;
    }
    
    /**
     * Setter for property indicador.
     * @param indicador New value of property indicador.
     */
    public void setIndicador(java.lang.String indicador) {
        this.indicador = indicador;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property cheque.
     * @return Value of property cheque.
     */
    public java.lang.String getCheque() {
        return cheque;
    }
    
    /**
     * Setter for property cheque.
     * @param cheque New value of property cheque.
     */
    public void setCheque(java.lang.String cheque) {
        this.cheque = cheque;
    }
    
    /**
     * Getter for property tipoCuenta.
     * @return Value of property tipoCuenta.
     */
    public java.lang.String getTipoCuenta() {
        return tipoCuenta;
    }
    
    /**
     * Setter for property tipoCuenta.
     * @param tipoCuenta New value of property tipoCuenta.
     */
    public void setTipoCuenta(java.lang.String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
    /**
     * Getter for property id_mims.
     * @return Value of property id_mims.
     */
    public java.lang.String getId_mims() {
        return id_mims;
    }
    
    /**
     * Setter for property id_mims.
     * @param id_mims New value of property id_mims.
     */
    public void setId_mims(java.lang.String id_mims) {
        this.id_mims = id_mims;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property origenNombre.
     * @return Value of property origenNombre.
     */
    public java.lang.String getOrigenNombre() {
        return origenNombre;
    }
    
    /**
     * Setter for property origenNombre.
     * @param origenNombre New value of property origenNombre.
     */
    public void setOrigenNombre(java.lang.String origenNombre) {
        this.origenNombre = origenNombre;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property destinoNombre.
     * @return Value of property destinoNombre.
     */
    public java.lang.String getDestinoNombre() {
        return destinoNombre;
    }
    
    /**
     * Setter for property destinoNombre.
     * @param destinoNombre New value of property destinoNombre.
     */
    public void setDestinoNombre(java.lang.String destinoNombre) {
        this.destinoNombre = destinoNombre;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property clienteNombre.
     * @return Value of property clienteNombre.
     */
    public java.lang.String getClienteNombre() {
        return clienteNombre;
    }
    
    /**
     * Setter for property clienteNombre.
     * @param clienteNombre New value of property clienteNombre.
     */
    public void setClienteNombre(java.lang.String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property tonelaje.
     * @return Value of property tonelaje.
     */
    public double getTonelaje() {
        return tonelaje;
    }
    
    /**
     * Setter for property tonelaje.
     * @param tonelaje New value of property tonelaje.
     */
    public void setTonelaje(double tonelaje) {
        this.tonelaje = tonelaje;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property tipoRegistro.
     * @return Value of property tipoRegistro.
     */
    public java.lang.String getTipoRegistro() {
        return tipoRegistro;
    }
    
    /**
     * Setter for property tipoRegistro.
     * @param tipoRegistro New value of property tipoRegistro.
     */
    public void setTipoRegistro(java.lang.String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }
    
}
