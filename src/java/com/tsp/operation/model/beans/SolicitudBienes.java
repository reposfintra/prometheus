package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudBienes<br/>
 * 1/03/2011
 * @author ivargas-Geotech
 */
public class SolicitudBienes {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String tipoPersona;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String tipo;
    private String direccion;
    private String secuencia;
    private String tipoBien;
    private String hipoteca;
    private String aFavorDe;
    private String valorComercial;

    public SolicitudBienes load(ResultSet rs) throws SQLException {
        SolicitudBienes bien = new SolicitudBienes();
        bien.setNumeroSolicitud(rs.getString("numero_solicitud"));
        bien.setTipo(rs.getString("tipo"));
        bien.setSecuencia(rs.getString("secuencia"));
        bien.setTipoBien(rs.getString("tipo_de_bien"));
        bien.setHipoteca(rs.getString("hipoteca"));
        bien.setaFavorDe(rs.getString("a_favor_de"));
        bien.setValorComercial(rs.getString("valor_comercial"));
        bien.setDireccion(rs.getString("direccion"));
        return bien;
    }

    /**
     * obtiene el valor de aFavorDe
     *
     * @return el valor de aFavorDe
     */
    public String getaFavorDe() {
        return aFavorDe;
    }

    /**
     * cambia el valor de aFavorDe
     *
     * @param aFavorDe nuevo valor aFavorDe
     */
    public void setaFavorDe(String aFavorDe) {
        this.aFavorDe = aFavorDe;
    }

    /**
     * obtiene el valor de hipoteca
     *
     * @return el valor de hipoteca
     */
    public String getHipoteca() {
        return hipoteca;
    }

    /**
     * cambia el valor de hipoteca
     *
     * @param hipoteca nuevo valor hipoteca
     */
    public void setHipoteca(String hipoteca) {
        this.hipoteca = hipoteca;
    }

    /**
     * obtiene el valor de secuencia
     *
     * @return el valor de secuencia
     */
    public String getSecuencia() {
        return secuencia;
    }

    /**
     * cambia el valor de secuencia
     *
     * @param secuencia nuevo valor secuencia
     */
    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * obtiene el valor de tipoBien
     *
     * @return el valor de tipoBien
     */
    public String getTipoBien() {
        return tipoBien;
    }

    /**
     * cambia el valor de tipoBien
     *
     * @param tipoBien nuevo valor tipoBien
     */
    public void setTipoBien(String tipoBien) {
        this.tipoBien = tipoBien;
    }

    /**
     * obtiene el valor de valorComercial
     *
     * @return el valor de valorComercial
     */
    public String getValorComercial() {
        return valorComercial;
    }

    /**
     * cambia el valor de valorComercial
     *
     * @param valorComercial nuevo valor valorComercial
     */
    public void setValorComercial(String valorComercial) {
        this.valorComercial = valorComercial;
    }

    /**
     * obtiene el valor de direccion
     *
     * @return el valor de direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * cambia el valor de direccion
     *
     * @param direccion nuevo valor direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * obtiene el valor de tipo
     *
     * @return el valor de tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * cambia el valor de tipo
     *
     * @param tipo nuevo valor tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    /**
     * obtiene el valor de tipoPersona
     *
     * @return el valor de tipoPersona
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * cambia el valor de tipoPersona
     *
     * @param tipoPersona nuevo valor tipoPersona
     */
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
}
