/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class ObligacionesCompra {
    
    private String numero_solicitud;
    private String entidad ;
    private String nit_proveedor;
    private String tipo_cuenta ;
    private String numero_cuenta;
    private double valor_comprar;
    private int secuencia;

    public ObligacionesCompra() {
    }

    
    
    /**
     * @return the numero_solicitud
     */
    public String getNumero_solicitud() {
        return numero_solicitud;
    }

    /**
     * @param numero_solicitud the numero_solicitud to set
     */
    public void setNumero_solicitud(String numero_solicitud) {
        this.numero_solicitud = numero_solicitud;
    }

    /**
     * @return the entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * @param entidad the entidad to set
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getNit_proveedor() {
        return nit_proveedor;
    }

    public void setNit_proveedor(String nit_proveedor) {
        this.nit_proveedor = nit_proveedor;
    }

    
    
    /**
     * @return the tipo_cuenta
     */
    public String getTipo_cuenta() {
        return tipo_cuenta;
    }

    /**
     * @param tipo_cuenta the tipo_cuenta to set
     */
    public void setTipo_cuenta(String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    /**
     * @return the numero_cuenta
     */
    public String getNumero_cuenta() {
        return numero_cuenta;
    }

    /**
     * @param numero_cuenta the numero_cuenta to set
     */
    public void setNumero_cuenta(String numero_cuenta) {
        this.numero_cuenta = numero_cuenta;
    }

    /**
     * @return the valor_comprar
     */
    public double getValor_comprar() {
        return valor_comprar;
    }

    /**
     * @param valor_comprar the valor_comprar to set
     */
    public void setValor_comprar(double valor_comprar) {
        this.valor_comprar = valor_comprar;
    }

    public int getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }
    
    
    
}
