/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class EgresoCabeceraTsp extends EgresoCabecera{

    private double comision;
    private double cuatroxmil;

    private String tipo_documento_ingreso;
    private String num_ingreso;
    private String fecha_creacion_ingreso;


    /** Creates a new instance of OfertaEcaDetalle */
    public EgresoCabeceraTsp() {
    }


    public static EgresoCabeceraTsp load(java.sql.ResultSet rs)throws java.sql.SQLException{

        EgresoCabeceraTsp egresoCabecera = new EgresoCabeceraTsp();

        egresoCabecera.setReg_status( rs.getString("reg_status") ) ;
        egresoCabecera.setDstrct( rs.getString("dstrct") ) ;
        egresoCabecera.setBranch_code( rs.getString("branch_code") ) ;
        egresoCabecera.setBank_account_no( rs.getString("bank_account_no") ) ;
        egresoCabecera.setDocument_no( rs.getString("document_no") ) ;
        egresoCabecera.setNit( rs.getString("nit") ) ;
        egresoCabecera.setPayment_name( rs.getString("payment_name") ) ;
        egresoCabecera.setAgency_id( rs.getString("agency_id") ) ;
        egresoCabecera.setPmt_date ( rs.getString("pmt_date") ) ;
        egresoCabecera.setPrinter_date( rs.getString("printer_date") ) ;
        egresoCabecera.setConcept_code( rs.getString("concept_code") ) ;
        egresoCabecera.setVlr( rs.getDouble("vlr") ) ;
        egresoCabecera.setVlr_for( rs.getDouble("vlr_for") ) ;
        egresoCabecera.setCurrency( rs.getString("currency") ) ;
        egresoCabecera.setLast_update( rs.getString("last_update") ) ;
        egresoCabecera.setUser_update( rs.getString("user_update") ) ;
        egresoCabecera.setCreation_date( rs.getString("creation_date") ) ;
        egresoCabecera.setCreation_user( rs.getString("creation_user") ) ;
        egresoCabecera.setBase( rs.getString("base") ) ;
        egresoCabecera.setTipo_documento( rs.getString("tipo_documento") ) ;
        egresoCabecera.setTasa( rs.getDouble("tasa") ) ;
        egresoCabecera.setFecha_cheque( rs.getString("fecha_cheque") ) ;
        egresoCabecera.setUsuario_impresion( rs.getString("usuario_impresion") ) ;
        egresoCabecera.setUsuario_contabilizacion( rs.getString("usuario_contabilizacion") ) ;
        egresoCabecera.setFecha_contabilizacion( rs.getString("fecha_contabilizacion") ) ;
        egresoCabecera.setFecha_entrega( rs.getString("fecha_entrega") ) ;
        egresoCabecera.setUsuario_envio( rs.getString("usuario_envio") ) ;
        egresoCabecera.setFecha_envio( rs.getString("fecha_envio") ) ;
        egresoCabecera.setUsuario_recibido( rs.getString("usuario_recibido") ) ;
        egresoCabecera.setFecha_recibido( rs.getString("fecha_recibido") ) ;
        egresoCabecera.setUsuario_entrega( rs.getString("usuario_entrega") ) ;
        egresoCabecera.setFecha_registro_entrega( rs.getString("fecha_registro_entrega") ) ;
        egresoCabecera.setFecha_registro_envio( rs.getString("fecha_registro_envio") ) ;
        egresoCabecera.setFecha_registro_recibido( rs.getString("fecha_registro_recibido") ) ;
        egresoCabecera.setTransaccion(rs.getInt("transaccion"));
        egresoCabecera.setNit_beneficiario(rs.getString("nit_beneficiario"));
        egresoCabecera.setFecha_reporte( rs.getString("fecha_reporte") ) ;
        egresoCabecera.setNit_proveedor( rs.getString("nit_proveedor") ) ;
        egresoCabecera.setUsuario_generacion( rs.getString("usuario_generacion") ) ;
        egresoCabecera.setPeriodo( rs.getString("periodo") ) ;
        egresoCabecera.setReimpresion( rs.getString("reimpresion") ) ;
        egresoCabecera.setContabilizable( rs.getString("contabilizable") ) ;
        egresoCabecera.setComision( rs.getDouble("comision") ) ;
        egresoCabecera.setCuatroxmil( rs.getDouble("cuatroxmil") ) ;
        egresoCabecera.setTipo_documento_ingreso( rs.getString("tipo_documento_ingreso") ) ;
        egresoCabecera.setNum_ingreso( rs.getString("num_ingreso") ) ;
        egresoCabecera.setFecha_creacion_ingreso( rs.getString("fecha_creacion_ingreso") ) ;

        return egresoCabecera;

    }



    /**
     * @return the comision
     */
    public double getComision() {
        return comision;
    }

    /**
     * @param comision the comision to set
     */
    public void setComision(double comision) {
        this.comision = comision;
    }

    /**
     * @return the cuatroxmil
     */
    public double getCuatroxmil() {
        return cuatroxmil;
    }

    /**
     * @param cuatroxmil the cuatroxmil to set
     */
    public void setCuatroxmil(double cuatroxmil) {
        this.cuatroxmil = cuatroxmil;
    }

    /**
     * @return the tipo_documento_ingreso
     */
    public String getTipo_documento_ingreso() {
        return tipo_documento_ingreso;
    }

    /**
     * @param tipo_documento_ingreso the tipo_documento_ingreso to set
     */
    public void setTipo_documento_ingreso(String tipo_documento_ingreso) {
        this.tipo_documento_ingreso = tipo_documento_ingreso;
    }

    /**
     * @return the num_ingreso
     */
    public String getNum_ingreso() {
        return num_ingreso;
    }

    /**
     * @param num_ingreso the num_ingreso to set
     */
    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    /**
     * @return the fecha_creacion_ingreso
     */
    public String getFecha_creacion_ingreso() {
        return fecha_creacion_ingreso;
    }

    /**
     * @param fecha_creacion_ingreso the fecha_creacion_ingreso to set
     */
    public void setFecha_creacion_ingreso(String fecha_creacion_ingreso) {
        this.fecha_creacion_ingreso = fecha_creacion_ingreso;
    }


}
