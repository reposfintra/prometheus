/**************************************************************************
 * Nombre: ......................Actividad.java                           *
 * Descripci�n: .................Beans de acuerdo especial.               *
 * Autor:........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:........................24 de agosto de 2005, 03:14 PM           *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class Actividad implements Serializable {
    private String dstrct;
    private String codactividad;
    private String descorta;
    private String deslarga;
    private String fecinicio;
    private String m_fecini;
    private String corta_fecini;
    private String larga_fecini;
    private String fecfin;
    private String m_fecfin;
    private String corta_fecfin;
    private String larga_fecfin;
    private String duracion;
    private String m_duracion;
    private String corta_duracion;
    private String larga_duracion;
    private String documento;
    private String m_documento;
    private String corta_documento;
    private String larga_documento;
    private String tiempodemora;
    private String m_tiempodemora;
    private String corta_tiempodemora;
    private String larga_tiempodemora;
    private String causademora;
    private String m_causademora;
    private String corta_causademora;
    private String larga_causademora;
    private String resdemora;
    private String m_resdemora;
    private String corta_resdemora;
    private String larga_resdemora;
    private String feccierre;
    private String m_feccierre;
    private String corta_feccierre;
    private String larga_feccierre;
    private String observacion;
    private String m_observacion;
    private String corta_observacion;
    private String larga_observacion;
    private String mane_evento;
    private String estado;
    private String base;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    
    
    // nuevos campos 
    private String cantrealizada;
    private String m_cantrealizada;
    private String corta_cantrealizada;
    private String larga_cantrealizada;
    
    private String cantplaneada;
    private String m_cantplaneada;
    private String corta_cantplaneada;
    private String larga_cantplaneada;
    
    private String perfil;
    
    //nuevos 19-12-2005
    private String referencia1;
    private String m_referencia1;
    private String corta_referencia1;
    private String larga_referencia1;
    
    private String referencia2;
    private String m_referencia2;
    private String corta_referencia2;
    private String larga_referencia2;
    
    private String refnumerica1;
    private String m_refnumerica1;
    private String corta_refnumerica1;
    private String larga_refnumerica1;
    
    private String refnumerica2;
    private String m_refnumerica2;
    private String corta_refnumerica2;
    private String larga_refnumerica2;
    private String link;
    
    
    /** Creates a new instance of Actividad */
    public static Actividad load(ResultSet rs)throws SQLException { 
        Actividad actividad =  new Actividad();
        actividad.setDstrct(rs.getString("dstrct"));
        actividad.setCodActividad(rs.getString("cod_actividad"));
        actividad.setDescorta(rs.getString("descorta"));
        actividad.setDesLarga(rs.getString("deslarga"));
        
        actividad.setFecinicio(rs.getString("fecinicio"));
        actividad.setM_Fecinicio(rs.getString("m_fecini"));
        actividad.setCortaFecini(rs.getString("corta_fecini"));
        actividad.setLargaFecini(rs.getString("larga_fecini"));
        
        actividad.setFecfinal(rs.getString("fecfin"));
        actividad.setM_Fecfinal(rs.getString("m_fecfin"));
        actividad.setCortaFecfin(rs.getString("corta_fecfin"));
        actividad.setLargaFecfin(rs.getString("larga_fecfin"));
        
        actividad.setDuracion(rs.getString("duracion"));
        actividad.setM_Duracion(rs.getString("m_duracion"));
        actividad.setCortaDuracion(rs.getString("corta_duracion"));
        actividad.setLargaDuracion(rs.getString("larga_duracion"));
        
        actividad.setDocumento(rs.getString("documento"));
        actividad.setM_Documento(rs.getString("m_documento"));
        actividad.setCortaDocumento(rs.getString("corta_documento"));
        actividad.setLargaDocumento(rs.getString("larga_documento"));
        
        actividad.setTiempodemora(rs.getString("tiempodemora"));
        actividad.setM_Tiempodemora(rs.getString("m_tiempodemora"));
        actividad.setCortaTiempodemora(rs.getString("corta_tiempodemora"));
        actividad.setLargaTiempodemora(rs.getString("larga_tiempodemora"));
        
        actividad.setCausademora(rs.getString("causademora"));
        actividad.setM_Causademora(rs.getString("m_causademora"));
        actividad.setCortaCausademora(rs.getString("corta_causademora"));
        actividad.setLargaCausademora(rs.getString("larga_causademora"));
        
        actividad.setResdemora(rs.getString("resdemora"));
        actividad.setM_Resdemora(rs.getString("m_resdemora"));
        actividad.setCortaResdemora(rs.getString("corta_resdemora"));
        actividad.setLargaResdemora(rs.getString("larga_resdemora"));
        
        actividad.setFeccierre(rs.getString("feccierre")); 
        actividad.setM_Feccierre(rs.getString("m_feccierre"));
        actividad.setCortaFeccierre(rs.getString("corta_feccierre"));
        actividad.setLargaFeccierre(rs.getString("larga_feccierre"));
        
        actividad.setObservacion(rs.getString("observacion"));
        actividad.setM_Observacion(rs.getString("m_observacion"));
        actividad.setCortaObservacion(rs.getString("corta_observacion"));
        actividad.setLargaObservacion(rs.getString("larga_observacion"));
        
        actividad.setMane_evento(rs.getString("mane_evento"));
        actividad.setEstado(rs.getString("estado"));
        actividad.setBase(rs.getString("base"));
        actividad.setCreation_date(rs.getString("creation_date"));
        actividad.setCreation_user(rs.getString("creation_user"));
        actividad.setLast_update(rs.getString("last_update"));
        actividad.setUser_update(rs.getString("user_update"));
        
        actividad.setCantrealizada(rs.getString("cantrealizada"));
        actividad.setM_cantrealizada(rs.getString("m_cantrealizada"));
        actividad.setCorta_cantrealizada(rs.getString("corta_cantrealizada"));
        actividad.setLarga_cantrealizada(rs.getString("larga_cantrealizada"));
        
        actividad.setCantplaneada(rs.getString("cantplaneada"));
        actividad.setM_cantplaneada(rs.getString("m_cantplaneada"));
        actividad.setCorta_cantplaneada(rs.getString("corta_cantplaneada"));
        actividad.setLarga_cantplaneada(rs.getString("larga_cantplaneada"));
        actividad.setPerfil(rs.getString("perfil"));
        
        //Nuevo 19-12-2005
        actividad.setReferencia1( rs.getString("referencia1") );
        actividad.setM_referencia1(rs.getString("m_referencia1"));
        actividad.setCorta_referencia1(rs.getString("corta_referencia1"));
        actividad.setLarga_referencia1(rs.getString("larga_referencia1"));
        
        actividad.setReferencia2( rs.getString("referencia2") );
        actividad.setM_referencia2(rs.getString("m_referencia2"));
        actividad.setCorta_referencia2(rs.getString("corta_referencia2"));
        actividad.setLarga_referencia2(rs.getString("larga_referencia2"));
        
        actividad.setRefnumerica1(rs.getString("refnumerica1"));
        actividad.setM_refnumerica1(rs.getString("m_refnumerica1"));
        actividad.setCorta_refnumerica1(rs.getString("corta_refnumerica1"));
        actividad.setLarga_refnumerica1(rs.getString("larga_refnumerica1"));
        
        actividad.setRefnumerica2(rs.getString("refnumerica2"));
        actividad.setM_refnumerica2(rs.getString("m_refnumerica2"));
        actividad.setCorta_refnumerica2(rs.getString("corta_refnumerica2"));
        actividad.setLarga_refnumerica2(rs.getString("larga_refnumerica2"));
        actividad.setLink(rs.getString("link"));
        return actividad;
    }
    
    public void setDstrct(String cia){
        this.dstrct=cia;        
    }
    
    public String getDstrct(){
        return dstrct;
    }
    
    public void setCodActividad(String cod){
        this.codactividad = cod;
    }
    public String getCodActividad(){
        return codactividad;
    }
    public void setDescorta(String des){
        this.descorta = des;
    }
    public String getDescorta(){
        return descorta;
    }
    public void setDesLarga(String des){
        this.deslarga = des;
    }
    public String getDesLarga(){
        return deslarga;
    }
    public void setFecinicio(String a){
        this.fecinicio=a;
    }
    public String getFecinicio(){
        return fecinicio;
    }
    public void setM_Fecinicio(String a){
        this.m_fecini=a;
    }
    public String getM_Fecinicio(){
        return m_fecini;
    }
    public void setCortaFecini(String a){
        this.corta_fecini=a;
    }
    public String getCortaFecini(){
        return corta_fecini;
    }
    public void setLargaFecini(String a){
        this.larga_fecini=a;        
    }
    public String getLargaFecini(){
        return larga_fecini;
    }
    
    public void setFecfinal(String a){
        this.fecfin=a;
    }
    public String getFecfinal(){
        return fecfin;
    }
    public void setM_Fecfinal(String a){
        this.m_fecfin=a;
    }
    public String getM_Fecfin(){
        return m_fecfin;
    }
    public void setCortaFecfin(String a){
        this.corta_fecfin=a;
    }
    public String getCortaFecfin(){
        return corta_fecfin;
    }
    public void setLargaFecfin(String a){
        this.larga_fecfin=a;        
    }
    public String getLargaFecfin(){
        return larga_fecfin;
    }
    
    
    public void setDuracion(String a){
        this.duracion=a;
    }
    public String getDuracion(){
        return duracion;
    }
    public void setM_Duracion(String a){
        this.m_duracion=a;
    }
    public String getM_Duracion(){
        return m_duracion;
    }
    public void setCortaDuracion(String a){
        this.corta_duracion=a;
    }
    public String getCortaDuracion(){
        return corta_duracion;
    }
    public void setLargaDuracion(String a){
        this.larga_duracion=a;        
    }
    public String getLargaDuracion(){
        return larga_duracion;
    }    
    
    public void setDocumento(String a){
        this.documento=a;
    }
    public String getDocumento(){
        return documento;
    }
    public void setM_Documento(String a){
        this.m_documento=a;
    }
    public String getM_Documento(){
        return m_documento;
    }
    public void setCortaDocumento(String a){
        this.corta_documento=a;
    }
    public String getCortaDocumento(){
        return corta_documento;
    }
    public void setLargaDocumento(String a){
        this.larga_documento=a;        
    }
    public String getLargaDocumento(){
        return larga_documento;
    }
    
    
    public void setTiempodemora(String a){
        this.tiempodemora=a;
    }
    public String getTiempodemora(){
        return tiempodemora;
    }
    public void setM_Tiempodemora(String a){
        this.m_tiempodemora=a;
    }
    public String getM_Tiempodemora(){
        return m_tiempodemora;
    }
    public void setCortaTiempodemora(String a){
        this.corta_tiempodemora=a;
    }
    public String getCortaTiempodemora(){
        return corta_tiempodemora;
    }
    public void setLargaTiempodemora(String a){
        this.larga_tiempodemora=a;        
    }
    public String getLargaTiempodemora(){
        return larga_tiempodemora;
    }
    
    public void setCausademora(String a){
        this.causademora=a;
    }
    public String getCausademora(){
        return causademora;
    }
    public void setM_Causademora(String a){
        this.m_causademora=a;
    }
    public String getM_Causademora(){
        return m_causademora;
    }
    public void setCortaCausademora(String a){
        this.corta_causademora=a;
    }
    public String getCortaCausademora(){
        return corta_causademora;
    }
    public void setLargaCausademora(String a){
        this.larga_causademora=a;        
    }
    public String getLargaCausademora(){
        return larga_causademora;
    }
    
    public void setResdemora(String a){
        this.resdemora=a;
    }
    public String getResdemora(){
        return resdemora;
    }
    public void setM_Resdemora(String a){
        this.m_resdemora=a;
    }
    public String getM_Resdemora(){
        return m_resdemora;
    }
    public void setCortaResdemora(String a){
        this.corta_resdemora=a;
    }
    public String getCortaResdemora(){
        return corta_resdemora;
    }
    public void setLargaResdemora(String a){
        this.larga_resdemora=a;        
    }
    public String getLargaResdemora(){
        return larga_resdemora;
    }
    
    public void setFeccierre(String a){
        this.feccierre=a;
    }
    public String getFeccierre(){
        return feccierre;
    }
    public void setM_Feccierre(String a){
        this.m_feccierre=a;
    }
    public String getM_Feccierre(){
        return m_feccierre;
    }
    public void setCortaFeccierre(String a){
        this.corta_feccierre=a;
    }
    public String getCortaFeccierre(){
        return corta_feccierre;
    }
    public void setLargaFeccierre(String a){
        this.larga_feccierre=a;        
    }
    public String getLargaFeccierre(){
        return larga_feccierre;
    }
    
    public void setObservacion(String a){
        this.observacion=a;
    }
    public String getObservacion(){
        return observacion;
    }
    public void setM_Observacion(String a){
        this.m_observacion=a;
    }
    public String getM_Observacion(){
        return m_observacion;
    }
    public void setCortaObservacion(String a){
        this.corta_observacion=a;
    }
    public String getCortaObservacion(){
        return corta_observacion;
    }
    public void setLargaObservacion(String a){
        this.larga_observacion=a;        
    }
    public String getLargaObservacion(){
        return larga_observacion;
    }
    
    public void setMane_evento(String me){
        this.mane_evento=me;
    }
    public String getMane_evento(){
        return mane_evento;
    }
    public void setEstado(String e){
        this.estado=e;
    }
    public String getEstado(){
        return estado;
    }
    public void setBase(String base){
        this.base = base;
    }
    public String getBase(){
        return base;
    }
    
    public void setCreation_user(String cu){
        this.creation_user = cu;
    }
    public String getCreation_user(){
        return creation_user;
    }
    public void setCreation_date(String fec){ 
        this.creation_date = fec;
    }
    public String getCreation_date(){
        return creation_date;
    }
    public void setLast_update(String fec){ 
        this.last_update = fec;
    }
    public String getLast_update(){
        return last_update;
    }
    public void setUser_update(String us){
        this.user_update = us;
    }
    public String getUser_update(){
        return user_update;
    }
    //Nuevos Campos
    /**
     * Getter for property cantrealizada.
     * @return Value of property cantrealizada.
     */
    public java.lang.String getCantrealizada() {
        return cantrealizada;
    }
    
    /**
     * Setter for property cantrealizada.
     * @param cantrealizada New value of property cantrealizada.
     */
    public void setCantrealizada(java.lang.String cantrealizada) {
        this.cantrealizada = cantrealizada;
    }
    
    /**
     * Getter for property m_cantrealizada.
     * @return Value of property m_cantrealizada.
     */
    public java.lang.String getM_cantrealizada() {
        return m_cantrealizada;
    }
    
    /**
     * Setter for property m_cantrealizada.
     * @param m_cantrealizada New value of property m_cantrealizada.
     */
    public void setM_cantrealizada(java.lang.String m_cantrealizada) {
        this.m_cantrealizada = m_cantrealizada;
    }
    
    /**
     * Getter for property corta_cantrealizada.
     * @return Value of property corta_cantrealizada.
     */
    public java.lang.String getCorta_cantrealizada() {
        return corta_cantrealizada;
    }
    
    /**
     * Setter for property corta_cantrealizada.
     * @param corta_cantrealizada New value of property corta_cantrealizada.
     */
    public void setCorta_cantrealizada(java.lang.String corta_cantrealizada) {
        this.corta_cantrealizada = corta_cantrealizada;
    }
    
    /**
     * Getter for property larga_cantrealizada.
     * @return Value of property larga_cantrealizada.
     */
    public java.lang.String getLarga_cantrealizada() {
        return larga_cantrealizada;
    }
    
    /**
     * Setter for property larga_cantrealizada.
     * @param larga_cantrealizada New value of property larga_cantrealizada.
     */
    public void setLarga_cantrealizada(java.lang.String larga_cantrealizada) {
        this.larga_cantrealizada = larga_cantrealizada;
    }
    
    /**
     * Getter for property cantplaneada.
     * @return Value of property cantplaneada.
     */
    public java.lang.String getCantplaneada() {
        return cantplaneada;
    }
    
    /**
     * Setter for property cantplaneada.
     * @param cantplaneada New value of property cantplaneada.
     */
    public void setCantplaneada(java.lang.String cantplaneada) {
        this.cantplaneada = cantplaneada;
    }
    
    /**
     * Getter for property m_cantplaneada.
     * @return Value of property m_cantplaneada.
     */
    public java.lang.String getM_cantplaneada() {
        return m_cantplaneada;
    }
    
    /**
     * Setter for property m_cantplaneada.
     * @param m_cantplaneada New value of property m_cantplaneada.
     */
    public void setM_cantplaneada(java.lang.String m_cantplaneada) {
        this.m_cantplaneada = m_cantplaneada;
    }
    
    /**
     * Getter for property corta_cantplaneada.
     * @return Value of property corta_cantplaneada.
     */
    public java.lang.String getCorta_cantplaneada() {
        return corta_cantplaneada;
    }
    
    /**
     * Setter for property corta_cantplaneada.
     * @param corta_cantplaneada New value of property corta_cantplaneada.
     */
    public void setCorta_cantplaneada(java.lang.String corta_cantplaneada) {
        this.corta_cantplaneada = corta_cantplaneada;
    }
    
    /**
     * Getter for property larga_cantplaneada.
     * @return Value of property larga_cantplaneada.
     */
    public java.lang.String getLarga_cantplaneada() {
        return larga_cantplaneada;
    }
    
    /**
     * Setter for property larga_cantplaneada.
     * @param larga_cantplaneada New value of property larga_cantplaneada.
     */
    public void setLarga_cantplaneada(java.lang.String larga_cantplaneada) {
        this.larga_cantplaneada = larga_cantplaneada;
    }
    
    /**
     * Getter for property perfil.
     * @return Value of property perfil.
     */
    public java.lang.String getPerfil() {
        return perfil;
    }
    
    /**
     * Setter for property perfil.
     * @param perfil New value of property perfil.
     */
    public void setPerfil(java.lang.String perfil) {
        this.perfil = perfil;
    }
    
    /**
     * Getter for property referencia1.
     * @return Value of property referencia1.
     */
    public java.lang.String getReferencia1() {
        return referencia1;
    }
    
    /**
     * Setter for property referencia1.
     * @param referencia1 New value of property referencia1.
     */
    public void setReferencia1(java.lang.String referencia1) {
        this.referencia1 = referencia1;
    }
    
    /**
     * Getter for property referencia2.
     * @return Value of property referencia2.
     */
    public java.lang.String getReferencia2() {
        return referencia2;
    }
    
    /**
     * Setter for property referencia2.
     * @param referencia2 New value of property referencia2.
     */
    public void setReferencia2(java.lang.String referencia2) {
        this.referencia2 = referencia2;
    }
    
    /**
     * Getter for property refnumerica1.
     * @return Value of property refnumerica1.
     */
    public java.lang.String getRefnumerica1() {
        return refnumerica1;
    }
    
    /**
     * Setter for property refnumerica1.
     * @param refnumerica1 New value of property refnumerica1.
     */
    public void setRefnumerica1(java.lang.String refnumerica1) {
        this.refnumerica1 = refnumerica1;
    }
    
    /**
     * Getter for property refnumerica2.
     * @return Value of property refnumerica2.
     */
    public java.lang.String getRefnumerica2() {
        return refnumerica2;
    }
    
    /**
     * Setter for property refnumerica2.
     * @param refnumerica2 New value of property refnumerica2.
     */
    public void setRefnumerica2(java.lang.String refnumerica2) {
        this.refnumerica2 = refnumerica2;
    }
    
    /**
     * Getter for property m_referencia2.
     * @return Value of property m_referencia2.
     */
    public java.lang.String getM_referencia2() {
        return m_referencia2;
    }
    
    /**
     * Setter for property m_referencia2.
     * @param m_referencia2 New value of property m_referencia2.
     */
    public void setM_referencia2(java.lang.String m_referencia2) {
        this.m_referencia2 = m_referencia2;
    }
    
    /**
     * Getter for property m_refnumerica1.
     * @return Value of property m_refnumerica1.
     */
    public java.lang.String getM_refnumerica1() {
        return m_refnumerica1;
    }
    
    /**
     * Setter for property m_refnumerica1.
     * @param m_refnumerica1 New value of property m_refnumerica1.
     */
    public void setM_refnumerica1(java.lang.String m_refnumerica1) {
        this.m_refnumerica1 = m_refnumerica1;
    }
    
    /**
     * Getter for property m_referencia1.
     * @return Value of property m_referencia1.
     */
    public java.lang.String getM_referencia1() {
        return m_referencia1;
    }
    
    /**
     * Setter for property m_referencia1.
     * @param m_referencia1 New value of property m_referencia1.
     */
    public void setM_referencia1(java.lang.String m_referencia1) {
        this.m_referencia1 = m_referencia1;
    }
    
    /**
     * Getter for property m_refnumerica2.
     * @return Value of property m_refnumerica2.
     */
    public java.lang.String getM_refnumerica2() {
        return m_refnumerica2;
    }
    
    /**
     * Setter for property m_refnumerica2.
     * @param m_refnumerica2 New value of property m_refnumerica2.
     */
    public void setM_refnumerica2(java.lang.String m_refnumerica2) {
        this.m_refnumerica2 = m_refnumerica2;
    }
    
    /**
     * Getter for property corta_referencia1.
     * @return Value of property corta_referencia1.
     */
    public java.lang.String getCorta_referencia1() {
        return corta_referencia1;
    }
    
    /**
     * Setter for property corta_referencia1.
     * @param corta_referencia1 New value of property corta_referencia1.
     */
    public void setCorta_referencia1(java.lang.String corta_referencia1) {
        this.corta_referencia1 = corta_referencia1;
    }
    
    /**
     * Getter for property corta_refnumerica2.
     * @return Value of property corta_refnumerica2.
     */
    public java.lang.String getCorta_refnumerica2() {
        return corta_refnumerica2;
    }
    
    /**
     * Setter for property corta_refnumerica2.
     * @param corta_refnumerica2 New value of property corta_refnumerica2.
     */
    public void setCorta_refnumerica2(java.lang.String corta_refnumerica2) {
        this.corta_refnumerica2 = corta_refnumerica2;
    }
    
    /**
     * Getter for property corta_refnumerica1.
     * @return Value of property corta_refnumerica1.
     */
    public java.lang.String getCorta_refnumerica1() {
        return corta_refnumerica1;
    }
    
    /**
     * Setter for property corta_refnumerica1.
     * @param corta_refnumerica1 New value of property corta_refnumerica1.
     */
    public void setCorta_refnumerica1(java.lang.String corta_refnumerica1) {
        this.corta_refnumerica1 = corta_refnumerica1;
    }
    
    /**
     * Getter for property corta_referencia2.
     * @return Value of property corta_referencia2.
     */
    public java.lang.String getCorta_referencia2() {
        return corta_referencia2;
    }
    
    /**
     * Setter for property corta_referencia2.
     * @param corta_referencia2 New value of property corta_referencia2.
     */
    public void setCorta_referencia2(java.lang.String corta_referencia2) {
        this.corta_referencia2 = corta_referencia2;
    }
    
    /**
     * Getter for property larga_referencia1.
     * @return Value of property larga_referencia1.
     */
    public java.lang.String getLarga_referencia1() {
        return larga_referencia1;
    }
    
    /**
     * Setter for property larga_referencia1.
     * @param larga_referencia1 New value of property larga_referencia1.
     */
    public void setLarga_referencia1(java.lang.String larga_referencia1) {
        this.larga_referencia1 = larga_referencia1;
    }
    
    /**
     * Getter for property larga_referencia2.
     * @return Value of property larga_referencia2.
     */
    public java.lang.String getLarga_referencia2() {
        return larga_referencia2;
    }
    
    /**
     * Setter for property larga_referencia2.
     * @param larga_referencia2 New value of property larga_referencia2.
     */
    public void setLarga_referencia2(java.lang.String larga_referencia2) {
        this.larga_referencia2 = larga_referencia2;
    }
    
    /**
     * Getter for property larga_refnumerica1.
     * @return Value of property larga_refnumerica1.
     */
    public java.lang.String getLarga_refnumerica1() {
        return larga_refnumerica1;
    }
    
    /**
     * Setter for property larga_refnumerica1.
     * @param larga_refnumerica1 New value of property larga_refnumerica1.
     */
    public void setLarga_refnumerica1(java.lang.String larga_refnumerica1) {
        this.larga_refnumerica1 = larga_refnumerica1;
    }
    
    /**
     * Getter for property larga_refnumerica2.
     * @return Value of property larga_refnumerica2.
     */
    public java.lang.String getLarga_refnumerica2() {
        return larga_refnumerica2;
    }
    
    /**
     * Setter for property larga_refnumerica2.
     * @param larga_refnumerica2 New value of property larga_refnumerica2.
     */
    public void setLarga_refnumerica2(java.lang.String larga_refnumerica2) {
        this.larga_refnumerica2 = larga_refnumerica2;
    }
    
    /**
     * Getter for property link.
     * @return Value of property link.
     */
    public java.lang.String getLink () {
        return link;
    }
    
    /**
     * Setter for property link.
     * @param link New value of property link.
     */
    public void setLink (java.lang.String link) {
        this.link = link;
    }
    
}
