/*
 * SendMail.java
 *
 * Created on 27 de diciembre de 2004, 03:55 PM
 */

package com.tsp.operation.model.beans;
import java.io.Serializable;

/**
 *
 * @author  AMENDEZ
 */
public class SendMail implements Serializable{
    
    private String recstatus;
    private String emailcode;
    private String emailfrom;
    private String emailto;
    private String emailcopyto;
    private String emailsubject;
    private String emailbody;
    private String lastupdat;
    private String tipo;
    private String sendername;    
    
    private String nombrearchivo = "";
    /** Creates a new instance of SendMail */
    public SendMail() {
    }
    
    /**
     * Getter for property emailbody.
     * @return Value of property emailbody.
     */
    public java.lang.String getEmailbody() {
        return emailbody;
    }
    
    /**
     * Setter for property emailbody.
     * @param emailbody New value of property emailbody.
     */
    public void setEmailbody(java.lang.String emailbody) {
        this.emailbody = emailbody;
    }
    
    /**
     * Getter for property emailcode.
     * @return Value of property emailcode.
     */
    public java.lang.String getEmailcode() {
        return emailcode;
    }
    
    /**
     * Setter for property emailcode.
     * @param emailcode New value of property emailcode.
     */
    public void setEmailcode(java.lang.String emailcode) {
        this.emailcode = emailcode;
    }
    
    /**
     * Getter for property emailcopyto.
     * @return Value of property emailcopyto.
     */
    public java.lang.String getEmailcopyto() {
        return emailcopyto;
    }
    
    /**
     * Setter for property emailcopyto.
     * @param emailcopyto New value of property emailcopyto.
     */
    public void setEmailcopyto(java.lang.String emailcopyto) {
        this.emailcopyto = emailcopyto;
    }
    
    /**
     * Getter for property emailfrom.
     * @return Value of property emailfrom.
     */
    public java.lang.String getEmailfrom() {
        return emailfrom;
    }
    
    /**
     * Setter for property emailfrom.
     * @param emailfrom New value of property emailfrom.
     */
    public void setEmailfrom(java.lang.String emailfrom) {
        this.emailfrom = emailfrom;
    }
    
    /**
     * Getter for property emailsubject.
     * @return Value of property emailsubject.
     */
    public java.lang.String getEmailsubject() {
        return emailsubject;
    }
    
    /**
     * Setter for property emailsubject.
     * @param emailsubject New value of property emailsubject.
     */
    public void setEmailsubject(java.lang.String emailsubject) {
        this.emailsubject = emailsubject;
    }
    
    /**
     * Getter for property emailto.
     * @return Value of property emailto.
     */
    public java.lang.String getEmailto() {
        return emailto;
    }
    
    /**
     * Setter for property emailto.
     * @param emailto New value of property emailto.
     */
    public void setEmailto(java.lang.String emailto) {
        this.emailto = emailto;
    }
    
    /**
     * Getter for property lastupdat.
     * @return Value of property lastupdat.
     */
    public java.lang.String getLastupdat() {
        return lastupdat;
    }
    
    /**
     * Setter for property lastupdat.
     * @param lastupdat New value of property lastupdat.
     */
    public void setLastupdat(java.lang.String lastupdat) {
        this.lastupdat = lastupdat;
    }
    
    /**
     * Getter for property recstatus.
     * @return Value of property recstatus.
     */
    public java.lang.String getRecstatus() {
        return recstatus;
    }
    
    /**
     * Setter for property recstatus.
     * @param recstatus New value of property recstatus.
     */
    public void setRecstatus(java.lang.String recstatus) {
        this.recstatus = recstatus;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo () {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo (java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property sendername.
     * @return Value of property sendername.
     */
    public java.lang.String getSendername () {
        return sendername;
    }
    
    /**
     * Setter for property sendername.
     * @param sendername New value of property sendername.
     */
    public void setSendername (java.lang.String sendername) {
        this.sendername = sendername;
    }
    
    /**
     * Getter for property nombrearchivo.
     * @return Value of property nombrearchivo.
     */
    public java.lang.String getNombrearchivo() {
        return nombrearchivo;
    }
    
    /**
     * Setter for property nombrearchivo.
     * @param nombrearchivo New value of property nombrearchivo.
     */
    public void setNombrearchivo(java.lang.String nombrearchivo) {
        this.nombrearchivo = nombrearchivo;
    }
    
}
