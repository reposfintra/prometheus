/*
 * DatosDelay.java
 *
 * Created on 30 de noviembre de 2004, 04:17 PM
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
/**
 *
 * @author  Mario Fontalvo
 */
public class DatosDelay implements Serializable{
    private String CodigoDelay;    
    private String DescripcionDelay;
    
    // constructor
    public DatosDelay(){
    }
    
    public static DatosDelay load(ResultSet rs) throws SQLException
    {
        DatosDelay datos = new DatosDelay();
        datos.setCodigoDelay      (rs.getString( 1));
        datos.setDescripcionDelay (rs.getString( 2));
        return datos;
    }      
    
    //Setter
    public void setCodigoDelay(String valor){
        this.CodigoDelay = valor;
    }    
    public void setDescripcionDelay(String valor){
        this.DescripcionDelay = valor;
    }    
    //Getter
    public String getCodigoDelay(){
        return this.CodigoDelay;
    }    
    public String getDescripcionDelay(){
        return this.DescripcionDelay;
    }    
}
