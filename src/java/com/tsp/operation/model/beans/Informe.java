/*
 * Informe.java
 *
 * Created on 1 de abril de 2005, 11:07 AM
 */

package com.tsp.operation.model.beans;

import java.util.*;
/**
 *
 * @author  KREALES
 */
public class Informe {
    
    private String agencia;
    private String fecha;
    private String hora;
    private String fechaInf;
    private String corte;
    private String cliente;
    private String ruta;
    private String remision;
    private String placa;
    private String conductor;
    private float tonelaje;
    private double anticipo;
    private String fechaCump;
    private Vector rutas;
    private String fechai;
    private String fechaf;
    private String egreso;
    private String cedula;
    private String numpla;
    
    //CARGA GENERAL
    private String origen;
    private String destino;
    private String stdjob;
    private float vlrpla;
    private float vlrrem;
    
    //JuanM ReporteDiarioDespacho
    private String despachador;
    private String unidadesdesp;
    private String monedapla;
    private String monedaant;
    private String monedarem;
    private double porcentajemaximoant;
    private String unidad;
    private double cantidadfacturada;
    private double utilidadbruta;
    
    private String estado;
    private String facturacial;
    
    //Reporte Diario de Despacho 15-11-05
    private double vlrcostoflete;
    private String costoflete;
    private double vlringresoflete;
    private String ingresoflete;
    
    //JUAN Reporte Diario de Despacho 29-11-05
    private String documentos;
    private String contenedores;
    private String document;
    
    //Karen 15-03-2006
    private String nomprop;
    private String platlr;
    
    //Jescandon 27-02-07
    private String tipocarga;
    private String vacio;
    private double vlr_rem_base;//Vlr Remesa
    private double vlr_rem_pro;//Vlr Remesa Prorrateado
    private double por_rem_por;//Porcentaje Remesa Prorrateado
    
    private double vlr_pla_base;//Vlr Planilla
    private double vlr_pla_pro;//Vlr Planilla Prorrateado
    private double por_pla_por;//Porcentaje Planilla Prorrateado
    private double por_utilidad_bruta;//Porcentaje utilidad bruta
    
    private boolean flag;

        
    /** Creates a new instance of Informe */
    public Informe() {
        this.flag = true;
    }
    
    public Vector getRutas(){
        return rutas;
    }
    public String getFechai(){
        return fechai;
    }
    public String getCedula(){
        return cedula;
    }
    public String getEgreso(){
        return egreso;
    }
    public String getFechaf(){
        return fechaf;
    }
    public String getAgencia(){
        return agencia;
    }
    public String getFecha(){
        return fecha;
    }
    public String getHora(){
        return hora;
    }
    public String getFechaInf(){
        return fechaInf;
    }
    public String getCorte(){
        return corte;
    }
    public String getCliente(){
        return cliente;
    }
    public String getRuta(){
        return ruta;
    }
    public String getConductor(){
        return conductor;
    }
    public String getRemision(){
        return remision;
    }
    public String getPlaca(){
        return placa;
    }
    public float getTonelaje(){
        return tonelaje;
    }
    public double getAnticipo(){
        return anticipo;
    }
    public String getFechaCump(){
        return fechaCump;
    }
    
    public void setAgencia(String agencia){
        this.agencia=agencia;
    }
    public void setFecha(String fecha){
        this.fecha=fecha;
    }
    public void setHora(String hora){
        this.hora=hora;
    }
    public void setFechaInf(String fechaInf){
        this.fechaInf=fechaInf;
    }
    public void setCorte(String corte){
        this.corte=corte;
    }
    public void setCliente(String cliente){
        this.cliente=cliente;
    }
    public void setRuta(String ruta){
        this.ruta=ruta;
    }
    public void setFechai(String fechai){
        this.fechai=fechai;
    }
    public void setCedula(String cedula){
        this.cedula=cedula;
    }
    public void setFechaf(String fechaf){
        this.fechaf=fechaf;
    }
    public void setConductor(String conductor){
        this.conductor=conductor;
    }
    public void setRemision(String remision){
        this.remision=remision;
    }
    public void setPlaca(String placa){
        this.placa=placa;
    }
    public void setEgreso(String egreso){
        this.egreso=egreso;
    }
    public void setTonelaje(float tonelaje){
        this.tonelaje=tonelaje;
    }
    public void setAnticipo(double anticipo){
        this.anticipo=anticipo;
    }
    public void setFechaCump(String fechaCump ){
        this.fechaCump=fechaCump;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property stdjob.
     * @return Value of property stdjob.
     */
    public java.lang.String getStdjob() {
        return stdjob;
    }
    
    /**
     * Setter for property stdjob.
     * @param stdjob New value of property stdjob.
     */
    public void setStdjob(java.lang.String stdjob) {
        this.stdjob = stdjob;
    }
    
    /**
     * Getter for property vlrpla.
     * @return Value of property vlrpla.
     */
    public float getVlrpla() {
        return vlrpla;
    }
    
    /**
     * Setter for property vlrpla.
     * @param vlrpla New value of property vlrpla.
     */
    public void setVlrpla(float vlrpla) {
        this.vlrpla = vlrpla;
    }
    
    /**
     * Getter for property vlrrem.
     * @return Value of property vlrrem.
     */
    public float getVlrrem() {
        return vlrrem;
    }
    
    /**
     * Setter for property vlrrem.
     * @param vlrrem New value of property vlrrem.
     */
    public void setVlrrem(float vlrrem) {
        this.vlrrem = vlrrem;
    }
    
    /**
     * Getter for property despachador.
     * @return Value of property despachador.
     */
    public java.lang.String getDespachador() {
        return despachador;
    }
    
    /**
     * Setter for property despachador.
     * @param despachador New value of property despachador.
     */
    public void setDespachador(java.lang.String despachador) {
        this.despachador = despachador;
    }
    
    /**
     * Getter for property unidadesdesp.
     * @return Value of property unidadesdesp.
     */
    public java.lang.String getUnidadesdesp() {
        return unidadesdesp;
    }
    
    /**
     * Setter for property unidadesdesp.
     * @param unidadesdesp New value of property unidadesdesp.
     */
    public void setUnidadesdesp(java.lang.String unidadesdesp) {
        this.unidadesdesp = unidadesdesp;
    }
    
    /**
     * Getter for property monedapla.
     * @return Value of property monedapla.
     */
    public java.lang.String getMonedapla() {
        return monedapla;
    }
    
    /**
     * Setter for property monedapla.
     * @param monedapla New value of property monedapla.
     */
    public void setMonedapla(java.lang.String monedapla) {
        this.monedapla = monedapla;
    }
    
    /**
     * Getter for property monedaant.
     * @return Value of property monedaant.
     */
    public java.lang.String getMonedaant() {
        return monedaant;
    }
    
    /**
     * Setter for property monedaant.
     * @param monedaant New value of property monedaant.
     */
    public void setMonedaant(java.lang.String monedaant) {
        this.monedaant = monedaant;
    }
    
    
    /**
     * Getter for property monedarem.
     * @return Value of property monedarem.
     */
    public java.lang.String getMonedarem() {
        return monedarem;
    }
    
    /**
     * Setter for property monedarem.
     * @param monedarem New value of property monedarem.
     */
    public void setMonedarem(java.lang.String monedarem) {
        this.monedarem = monedarem;
    }
    
    /**
     * Getter for property porcentajemaximoant.
     * @return Value of property porcentajemaximoant.
     */
    public double getPorcentajemaximoant() {
        return porcentajemaximoant;
    }
    
    /**
     * Setter for property porcentajemaximoant.
     * @param porcentajemaximoant New value of property porcentajemaximoant.
     */
    public void setPorcentajemaximoant(double porcentajemaximoant) {
        this.porcentajemaximoant = porcentajemaximoant;
    }
    
    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property cantidadfacturada.
     * @return Value of property cantidadfacturada.
     */
    public double getCantidadfacturada() {
        return cantidadfacturada;
    }
    
    /**
     * Setter for property cantidadfacturada.
     * @param cantidadfacturada New value of property cantidadfacturada.
     */
    public void setCantidadfacturada(double cantidadfacturada) {
        this.cantidadfacturada = cantidadfacturada;
    }
    
    /**
     * Getter for property utilidadbruta.
     * @return Value of property utilidadbruta.
     */
    public double getUtilidadbruta() {
        return utilidadbruta;
    }
    
    /**
     * Setter for property utilidadbruta.
     * @param utilidadbruta New value of property utilidadbruta.
     */
    public void setUtilidadbruta(double utilidadbruta) {
        this.utilidadbruta = utilidadbruta;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property facturacial.
     * @return Value of property facturacial.
     */
    public java.lang.String getFacturacial() {
        return facturacial;
    }
    
    /**
     * Setter for property facturacial.
     * @param facturacial New value of property facturacial.
     */
    public void setFacturacial(java.lang.String facturacial) {
        this.facturacial = facturacial;
    }
    
    /**
     * Getter for property costoflete.
     * @return Value of property costoflete.
     */
    public java.lang.String getCostoflete() {
        return costoflete;
    }
    
    /**
     * Setter for property costoflete.
     * @param costoflete New value of property costoflete.
     */
    public void setCostoflete(java.lang.String costoflete) {
        this.costoflete = costoflete;
    }
    
    /**
     * Getter for property ingresoflete.
     * @return Value of property ingresoflete.
     */
    public java.lang.String getIngresoflete() {
        return ingresoflete;
    }
    
    /**
     * Setter for property ingresoflete.
     * @param ingresoflete New value of property ingresoflete.
     */
    public void setIngresoflete(java.lang.String ingresoflete) {
        this.ingresoflete = ingresoflete;
    }
    
    /**
     * Getter for property vlringresoflete.
     * @return Value of property vlringresoflete.
     */
    public double getVlringresoflete() {
        return vlringresoflete;
    }
    
    /**
     * Setter for property vlringresoflete.
     * @param vlringresoflete New value of property vlringresoflete.
     */
    public void setVlringresoflete(double vlringresoflete) {
        this.vlringresoflete = vlringresoflete;
    }
    
    /**
     * Getter for property vlrcostoflete.
     * @return Value of property vlrcostoflete.
     */
    public double getVlrcostoflete() {
        return vlrcostoflete;
    }
    
    /**
     * Setter for property vlrcostoflete.
     * @param vlrcostoflete New value of property vlrcostoflete.
     */
    public void setVlrcostoflete(double vlrcostoflete) {
        this.vlrcostoflete = vlrcostoflete;
    }
    
    /**
     * Getter for property documentos.
     * @return Value of property documentos.
     */
    public java.lang.String getDocumentos() {
        return documentos;
    }
    
    /**
     * Setter for property documentos.
     * @param documentos New value of property documentos.
     */
    public void setDocumentos(java.lang.String documentos) {
        this.documentos = documentos;
    }
    
    /**
     * Getter for property contenedores.
     * @return Value of property contenedores.
     */
    public java.lang.String getContenedores() {
        return contenedores;
    }
    
    /**
     * Setter for property contenedores.
     * @param contenedores New value of property contenedores.
     */
    public void setContenedores(java.lang.String contenedores) {
        this.contenedores = contenedores;
    }
    
    /**
     * Getter for property document.
     * @return Value of property document.
     */
    public java.lang.String getDocument() {
        return document;
    }
    
    /**
     * Setter for property document.
     * @param document New value of property document.
     */
    public void setDocument(java.lang.String document) {
        this.document = document;
    }
    
    /**
     * Getter for property nomprop.
     * @return Value of property nomprop.
     */
    public java.lang.String getNomprop() {
        return nomprop;
    }
    
    /**
     * Setter for property nomprop.
     * @param nomprop New value of property nomprop.
     */
    public void setNomprop(java.lang.String nomprop) {
        this.nomprop = nomprop;
    }
    
    /**
     * Getter for property platlr.
     * @return Value of property platlr.
     */
    public java.lang.String getPlatlr() {
        return platlr;
    }
    
    /**
     * Setter for property platlr.
     * @param platlr New value of property platlr.
     */
    public void setPlatlr(java.lang.String platlr) {
        this.platlr = platlr;
    }
      
    
    /**
     * Getter for property vlr_rem_base.
     * @return Value of property vlr_rem_base.
     */
    public double getVlr_rem_base() {
        return vlr_rem_base;
    }
    
    /**
     * Setter for property vlr_rem_base.
     * @param vlr_rem_base New value of property vlr_rem_base.
     */
    public void setVlr_rem_base(double vlr_rem_base) {
        this.vlr_rem_base = vlr_rem_base;
    }
    
    /**
     * Getter for property vlr_rem_pro.
     * @return Value of property vlr_rem_pro.
     */
    public double getVlr_rem_pro() {
        return vlr_rem_pro;
    }
    
    /**
     * Setter for property vlr_rem_pro.
     * @param vlr_rem_pro New value of property vlr_rem_pro.
     */
    public void setVlr_rem_pro(double vlr_rem_pro) {
        this.vlr_rem_pro = vlr_rem_pro;
    }
    
    /**
     * Getter for property vacio.
     * @return Value of property vacio.
     */
    public java.lang.String getVacio() {
        return vacio;
    }
    
    /**
     * Setter for property vacio.
     * @param vacio New value of property vacio.
     */
    public void setVacio(java.lang.String vacio) {
        this.vacio = vacio;
    }
    
    /**
     * Getter for property por_rem_por.
     * @return Value of property por_rem_por.
     */
    public double getPor_rem_por() {
        return por_rem_por;
    }
    
    /**
     * Setter for property por_rem_por.
     * @param por_rem_por New value of property por_rem_por.
     */
    public void setPor_rem_por(double por_rem_por) {
        this.por_rem_por = por_rem_por;
    }
    
    /**
     * Getter for property vlr_pla_base.
     * @return Value of property vlr_pla_base.
     */
    public double getVlr_pla_base() {
        return vlr_pla_base;
    }
    
    /**
     * Setter for property vlr_pla_base.
     * @param vlr_pla_base New value of property vlr_pla_base.
     */
    public void setVlr_pla_base(double vlr_pla_base) {
        this.vlr_pla_base = vlr_pla_base;
    }
    
    /**
     * Getter for property vlr_pla_pro.
     * @return Value of property vlr_pla_pro.
     */
    public double getVlr_pla_pro() {
        return vlr_pla_pro;
    }
    
    /**
     * Setter for property vlr_pla_pro.
     * @param vlr_pla_pro New value of property vlr_pla_pro.
     */
    public void setVlr_pla_pro(double vlr_pla_pro) {
        this.vlr_pla_pro = vlr_pla_pro;
    }
    
    /**
     * Getter for property por_pla_por.
     * @return Value of property por_pla_por.
     */
    public double getPor_pla_por() {
        return por_pla_por;
    }
    
    /**
     * Setter for property por_pla_por.
     * @param por_pla_por New value of property por_pla_por.
     */
    public void setPor_pla_por(double por_pla_por) {
        this.por_pla_por = por_pla_por;
    }
    
    /**
     * Getter for property tipocarga.
     * @return Value of property tipocarga.
     */
    public java.lang.String getTipocarga() {
        return tipocarga;
    }
    
    /**
     * Setter for property tipocarga.
     * @param tipocarga New value of property tipocarga.
     */
    public void setTipocarga(java.lang.String tipocarga) {
        this.tipocarga = tipocarga;
    }
    
    /**
     * Getter for property por_utilidad_bruta.
     * @return Value of property por_utilidad_bruta.
     */
    public double getPor_utilidad_bruta() {
        return por_utilidad_bruta;
    }
    
    /**
     * Setter for property por_utilidad_bruta.
     * @param por_utilidad_bruta New value of property por_utilidad_bruta.
     */
    public void setPor_utilidad_bruta(double por_utilidad_bruta) {
        this.por_utilidad_bruta = por_utilidad_bruta;
    }
    
    /**
     * Getter for property flag.
     * @return Value of property flag.
     */
    public boolean isFlag() {
        return flag;
    }
    
    /**
     * Setter for property flag.
     * @param flag New value of property flag.
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    
}
