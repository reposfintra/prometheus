package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
public class Email2 implements Serializable{
  private int emailId;
  private String recstatus;
  private String emailcode;
  private String emailfrom;
  private String emailto;
  private String emailcopyto;
  private String emailhiddencopyto;
  private String emailsubject;
  private String emailbody;
  private Timestamp lastupdat;
  private String senderName;
  private String remarks;
  private String tipo;
  private byte[] adjunto;
  private String nombreArchivo;
  private String [] emailtoarray;
  private String [] emailcopytoarray;
  private String [] emailhiddencopytoarray;
  private static ByteArrayOutputStream baos;  
  private String rutaArchivo;
  private String [] nombreArchivos ;


  static {     }
  public Email2()  {
    this.emailId = 0;
    this.recstatus = "A";
    this.emailcode = "";
    this.emailfrom = "";
    this.emailto = "";
    this.emailcopyto = "";
    this.emailhiddencopyto="";
    this.emailsubject = "";
    this.emailbody = "";
    this.lastupdat = null;
    this.senderName = "";
    this.emailtoarray = null;
    this.emailcopytoarray = null;
    this.emailhiddencopytoarray=null;
    this.remarks  = " ";
    this.tipo = "I";
    this.adjunto = null;
    this.nombreArchivo = ""; 
    this.rutaArchivo="";  
  } 
  public String getEmailbody() {
    return emailbody;
  }  
  public void setEmailbody(String emailbody) {
    this.emailbody = emailbody;
  }
  public String getEmailcode() {
    return emailcode;
  }
  public void setEmailcode(String emailcode) {
    this.emailcode = emailcode;
  }
  public String getEmailfrom() {
    return emailfrom;
  }
  public void setEmailfrom(String emailfrom) {
    this.emailfrom = emailfrom;
  }
  public String getEmailsubject() {
    return emailsubject;
  }
  public void setEmailsubject(String emailsubject) {
    this.emailsubject = emailsubject;
  }
  public String getEmailto() {
    return emailto;
  }
  public String getEmailto(int idx) {
    return this.emailtoarray[idx];
  }
  public int getEmailToLength() {
    return this.emailtoarray.length;
  }
  public void setEmailto(String emailto) {
    this.emailto = emailto;
    this.emailtoarray = this.emailto.split(";");
    for( int idx = 0; idx < emailtoarray.length; idx++ )
      this.emailtoarray[idx] = this.emailtoarray[idx].trim();
  }
  public String getEmailcopyto() {
    return emailcopyto;
  }
  public String getEmailcopyto(int idx) {
    return this.emailcopytoarray[idx];
  }
  public int getEmailCopytoLength() {
    return this.emailcopytoarray.length;
  }
  public void setEmailcopyto(String emailcopyto) {
    this.emailcopyto = emailcopyto;
    this.emailcopytoarray = this.emailcopyto.split(";");
    
    if (emailcopyto.trim().equals("") ){
        this.emailcopytoarray =new String[0];
    }
    //.out.println("emailcopyto"+emailcopyto+"emailcopytoarray.length"+emailcopytoarray.length);
    for( int idx = 0; idx < emailcopytoarray.length; idx++ )
      this.emailcopytoarray[idx] = this.emailcopytoarray[idx].trim();
  }
  public Timestamp getLastupdat() {
    return lastupdat;
  }
  public void setLastupdat(Timestamp lastupdat) {
    this.lastupdat = lastupdat;
  }
  public String getRecstatus() {
    return recstatus;
  }
  public void setRecstatus(String recstatus) {
    this.recstatus = recstatus;
  }
  public String getSenderName() {
    return this.senderName;
  }
  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }
  public void setRemarks(String remarks){
      this.remarks = remarks;
  }
  public String getRemarks(){
      return remarks;
  }
  public byte[] getAdjunto()  {
    return this.adjunto;
  }
  public void setAdjunto(byte[] adjunto)  {
    this.adjunto = adjunto;
  }
  public int getEmailId()  {
    return this.emailId;
  }
  public void setEmailId(int emailId)  {      
    this.emailId = emailId;   
  }
  public String getTipo()  {
    return this.tipo;
  }
  public void setTipo(String tipo)  {
    this.tipo = tipo;
  }
  public String getNombreArchivo()  {
    return this.nombreArchivo;
  }
  public void setNombreArchivo(String nombreArchivo)  {
    this.nombreArchivo = nombreArchivo;
  }  
  public String getRutaArchivo() {
    return rutaArchivo;
  }
  public void setRutaArchivo(String rutarchivo) {
    this.rutaArchivo = rutarchivo;
  }  
  public void setEmailHiddencopyto(String emailhiddencopyto2) {
    this.emailhiddencopyto = emailhiddencopyto2;
    this.emailhiddencopytoarray = this.emailhiddencopyto.split(";");
    if (emailhiddencopyto.trim().equals("") ){
        this.emailhiddencopytoarray =new String[0];
    }
    //.out.println("emailhiddencopyto"+emailhiddencopyto+"emailhiddencopytoarray.length"+emailhiddencopytoarray.length);
    for( int idx = 0; idx < emailhiddencopytoarray.length; idx++ )
      this.emailhiddencopytoarray[idx] = this.emailhiddencopytoarray[idx].trim();
  }  
  public String getEmailHiddencopyto(int idx) {
    return this.emailhiddencopytoarray[idx];
  }    
  public int getEmailHiddenCopytoLength() {
    return this.emailhiddencopytoarray.length;
  }

    public String[] getNombreArchivos() {
        return nombreArchivos;
    }

    public void setNombreArchivos(String[] nombreArchivos) {
        this.nombreArchivos = nombreArchivos;
    }

}