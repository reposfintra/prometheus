/***************************************************************************
 * Nombre:        Proveedor.java                                           *
 * Descripci�n:   Beans de acuerdo especial.                               *
 * Autor:         Ing. Tito Maturana                                       *
 * Fecha:         29 de septiembre de 2005, 09:02 PM                       *
 * Versi�n:       Java  1.0                                                *
 * Copyright:     Fintravalores S.A. S.A.                             *
 **************************************************************************/


package com.tsp.operation.model.beans;

public class Proveedor {
        private String nit_afiliado;
        private String c_nit;  
        private String c_idMims;
        private String c_payment_name;
        private String c_branch_code;
        private String c_bank_account;
        private String c_agency_id;
        private String c_tipo_doc;
        private String c_banco_transfer;
        private String c_sucursal_transfer;
        private String c_tipo_cuenta;
        private String c_numero_cuenta;
        private String c_codciudad_cuenta;
        private String c_clasificacion;
        private String c_gran_contribuyente;
        private String c_agente_retenedor;
        private String c_autoretenedor_rfte;
        private String c_autoretenedor_iva;
        private String c_autoretenedor_ica;


        private String usuario_creacion;
        private String fecha_creacion;
        private String usuario_modificacion;
        private String ultima_modificacion;
        private String distrito;
        private String estado;
        private String base;

        private String nombre;
        private int plazo;

        private String Nom1;
        private String Nom2;
        private String Ape1;
        private String Ape2;
        private String expced;
        private String ciudad;
        private String direccion;
        private String telefono;
        private String celular;
        //nuevo campo 02-03-2006
        private String handle_code;
        private String cedula_cuenta;
        private String nombre_cuenta;
        private String pais;
        //nuevo 21-04-2006
        private String veto;
        private String c_hc;
        //nuevo Diogenes
        private String tipo_pago;
        private String c_currency_bank;
        private String currency;

        private String nit_beneficiario;
        private String nom_beneficiario;

        private String cmc;
        private String concept_code;

        private String aprobado;

        //AMATURANA 9.03.2007
        private String retencion_pago;

        private String afil;//20100817
        private String sede;//20100817
        private String regimen;//20100827
        private String codfen;//20100827
        private String tipo_proveedor;


        /** Creates a new instance of Proveedor */
        public Proveedor() {
                this.c_agency_id = "";
                this.c_agente_retenedor = "";
                this.c_autoretenedor_ica = "";
                this.c_autoretenedor_iva = "";
                this.c_autoretenedor_rfte = "";
                this.c_banco_transfer = "";
                this.c_bank_account = "";
                this.c_branch_code = "";
                this.c_clasificacion = "";
                this.c_codciudad_cuenta = "";
                this.c_gran_contribuyente = "";
                this.c_idMims = "";
                this.c_nit = "";
                this.c_numero_cuenta = "";
                this.c_payment_name = "";
                this.c_sucursal_transfer = "";
                this.c_tipo_cuenta = "";
                this.c_tipo_doc = "";
                this.plazo=0;
                this.handle_code="";
                this.cedula_cuenta="";
                this.nombre_cuenta="";
                this.tipo_pago="";
                this.afil="N";//20100817
                this.sede="N";//20100817
                this.regimen="";//20100827
        }

        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }

        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }

        /**
         * Getter for property c_agency_id.
         * @return Value of property c_agency_id.
         */
        public java.lang.String getC_agency_id() {
                return c_agency_id;
        }

        /**
         * Setter for property c_agency_id.
         * @param c_agency_id New value of property c_agency_id.
         */
        public void setC_agency_id(java.lang.String c_agency_id) {
                this.c_agency_id = c_agency_id;
        }

        /**
         * Getter for property c_agente_retenedor.
         * @return Value of property c_agente_retenedor.
         */
        public java.lang.String getC_agente_retenedor() {
                return c_agente_retenedor;
        }

        /**
         * Setter for property c_agente_retenedor.
         * @param c_agente_retenedor New value of property c_agente_retenedor.
         */
        public void setC_agente_retenedor(java.lang.String c_agente_retenedor) {
                this.c_agente_retenedor = c_agente_retenedor;
        }

        /**
         * Getter for property c_autoretenedor_ica.
         * @return Value of property c_autoretenedor_ica.
         */
        public java.lang.String getC_autoretenedor_ica() {
                return c_autoretenedor_ica;
        }

        /**
         * Setter for property c_autoretenedor_ica.
         * @param c_autoretenedor_ica New value of property c_autoretenedor_ica.
         */
        public void setC_autoretenedor_ica(java.lang.String c_autoretenedor_ica) {
                this.c_autoretenedor_ica = c_autoretenedor_ica;
        }

        /**
         * Getter for property c_autoretenedor_iva.
         * @return Value of property c_autoretenedor_iva.
         */
        public java.lang.String getC_autoretenedor_iva() {
                return c_autoretenedor_iva;
        }

        /**
         * Setter for property c_autoretenedor_iva.
         * @param c_autoretenedor_iva New value of property c_autoretenedor_iva.
         */
        public void setC_autoretenedor_iva(java.lang.String c_autoretenedor_iva) {
                this.c_autoretenedor_iva = c_autoretenedor_iva;
        }

        /**
         * Getter for property c_autoretenedor_rfte.
         * @return Value of property c_autoretenedor_rfte.
         */
        public java.lang.String getC_autoretenedor_rfte() {
                return c_autoretenedor_rfte;
        }

        /**
         * Setter for property c_autoretenedor_rfte.
         * @param c_autoretenedor_rfte New value of property c_autoretenedor_rfte.
         */
        public void setC_autoretenedor_rfte(java.lang.String c_autoretenedor_rfte) {
                this.c_autoretenedor_rfte = c_autoretenedor_rfte;
        }

        /**
         * Getter for property c_banco_transfer.
         * @return Value of property c_banco_transfer.
         */
        public java.lang.String getC_banco_transfer() {
                return c_banco_transfer;
        }

        /**
         * Setter for property c_banco_transfer.
         * @param c_banco_transfer New value of property c_banco_transfer.
         */
        public void setC_banco_transfer(java.lang.String c_banco_transfer) {
                this.c_banco_transfer = c_banco_transfer;
        }

        /**
         * Getter for property c_bank_account.
         * @return Value of property c_bank_account.
         */
        public java.lang.String getC_bank_account() {
                return c_bank_account;
        }

        /**
         * Setter for property c_bank_account.
         * @param c_bank_account New value of property c_bank_account.
         */
        public void setC_bank_account(java.lang.String c_bank_account) {
                this.c_bank_account = c_bank_account;
        }

        /**
         * Getter for property c_branch_code.
         * @return Value of property c_branch_code.
         */
        public java.lang.String getC_branch_code() {
                return c_branch_code;
        }

        /**
         * Setter for property c_branch_code.
         * @param c_branch_code New value of property c_branch_code.
         */
        public void setC_branch_code(java.lang.String c_branch_code) {
                this.c_branch_code = c_branch_code;
        }

        /**
         * Getter for property c_clasificacion.
         * @return Value of property c_clasificacion.
         */
        public java.lang.String getC_clasificacion() {
                return c_clasificacion;
        }

        /**
         * Setter for property c_clasificacion.
         * @param c_clasificacion New value of property c_clasificacion.
         */
        public void setC_clasificacion(java.lang.String c_clasificacion) {
                this.c_clasificacion = c_clasificacion;
        }

        /**
         * Getter for property c_codciudad_cuenta.
         * @return Value of property c_codciudad_cuenta.
         */
        public java.lang.String getC_codciudad_cuenta() {
                return c_codciudad_cuenta;
        }

        /**
         * Setter for property c_codciudad_cuenta.
         * @param c_codciudad_cuenta New value of property c_codciudad_cuenta.
         */
        public void setC_codciudad_cuenta(java.lang.String c_codciudad_cuenta) {
                this.c_codciudad_cuenta = c_codciudad_cuenta;
        }

        /**
         * Getter for property c_gran_contribuyente.
         * @return Value of property c_gran_contribuyente.
         */
        public java.lang.String getC_gran_contribuyente() {
                return c_gran_contribuyente;
        }

        /**
         * Setter for property c_gran_contribuyente.
         * @param c_gran_contribuyente New value of property c_gran_contribuyente.
         */
        public void setC_gran_contribuyente(java.lang.String c_gran_contribuyente) {
                this.c_gran_contribuyente = c_gran_contribuyente;
        }

        /**
         * Getter for property c_idMims.
         * @return Value of property c_idMims.
         */
        public java.lang.String getC_idMims() {
                return c_idMims;
        }

        /**
         * Setter for property c_idMims.
         * @param c_idMims New value of property c_idMims.
         */
        public void setC_idMims(java.lang.String c_idMims) {
                this.c_idMims = c_idMims;
        }

        /**
         * Getter for property c_nit.
         * @return Value of property c_nit.
         */
        public java.lang.String getC_nit() {
                return c_nit;
        }

        /**
         * Setter for property c_nit.
         * @param c_nit New value of property c_nit.
         */
        public void setC_nit(java.lang.String c_nit) {
                this.c_nit = c_nit;
        }

        /**
         * Getter for property c_numero_cuenta.
         * @return Value of property c_numero_cuenta.
         */
        public java.lang.String getC_numero_cuenta() {
                return c_numero_cuenta;
        }

        /**
         * Setter for property c_numero_cuenta.
         * @param c_numero_cuenta New value of property c_numero_cuenta.
         */
        public void setC_numero_cuenta(java.lang.String c_numero_cuenta) {
                this.c_numero_cuenta = c_numero_cuenta;
        }

        /**
         * Getter for property c_payment_name.
         * @return Value of property c_payment_name.
         */
        public java.lang.String getC_payment_name() {
                return c_payment_name;
        }

        /**
         * Setter for property c_payment_name.
         * @param c_payment_name New value of property c_payment_name.
         */
        public void setC_payment_name(java.lang.String c_payment_name) {
                this.c_payment_name = c_payment_name;
        }

        /**
         * Getter for property c_sucursal_transfer.
         * @return Value of property c_sucursal_transfer.
         */
        public java.lang.String getC_sucursal_transfer() {
                return c_sucursal_transfer;
        }

        /**
         * Setter for property c_sucursal_transfer.
         * @param c_sucursal_transfer New value of property c_sucursal_transfer.
         */
        public void setC_sucursal_transfer(java.lang.String c_sucursal_transfer) {
                this.c_sucursal_transfer = c_sucursal_transfer;
        }

        /**
         * Getter for property c_tipo_cuenta.
         * @return Value of property c_tipo_cuenta.
         */
        public java.lang.String getC_tipo_cuenta() {
                return c_tipo_cuenta;
        }

        /**
         * Setter for property c_tipo_cuenta.
         * @param c_tipo_cuenta New value of property c_tipo_cuenta.
         */
        public void setC_tipo_cuenta(java.lang.String c_tipo_cuenta) {
                this.c_tipo_cuenta = c_tipo_cuenta;
        }

        /**
         * Getter for property c_tipo_doc.
         * @return Value of property c_tipo_doc.
         */
        public java.lang.String getC_tipo_doc() {
                return c_tipo_doc;
        }

        /**
         * Setter for property c_tipo_doc.
         * @param c_tipo_doc New value of property c_tipo_doc.
         */
        public void setC_tipo_doc(java.lang.String c_tipo_doc) {
                this.c_tipo_doc = c_tipo_doc;
        }

        /**
         * Getter for property distrito.
         * @return Value of property distrito.
         */
        public java.lang.String getDistrito() {
                return distrito;
        }

        /**
         * Setter for property distrito.
         * @param distrito New value of property distrito.
         */
        public void setDistrito(java.lang.String distrito) {
                this.distrito = distrito;
        }

        /**
         * Getter for property estado.
         * @return Value of property estado.
         */
        public java.lang.String getEstado() {
                return estado;
        }

        /**
         * Setter for property estado.
         * @param estado New value of property estado.
         */
        public void setEstado(java.lang.String estado) {
                this.estado = estado;
        }

        /**
         * Getter for property fecha_creacion.
         * @return Value of property fecha_creacion.
         */
        public java.lang.String getFecha_creacion() {
                return fecha_creacion;
        }

        /**
         * Setter for property fecha_creacion.
         * @param fecha_creacion New value of property fecha_creacion.
         */
        public void setFecha_creacion(java.lang.String fecha_creacion) {
                this.fecha_creacion = fecha_creacion;
        }

        /**
         * Getter for property ultima_modificacion.
         * @return Value of property ultima_modificacion.
         */
        public java.lang.String getUltima_modificacion() {
                return ultima_modificacion;
        }

        /**
         * Setter for property ultima_modificacion.
         * @param ultima_modificacion New value of property ultima_modificacion.
         */
        public void setUltima_modificacion(java.lang.String ultima_modificacion) {
                this.ultima_modificacion = ultima_modificacion;
        }

        /**
         * Getter for property usuario_creacion.
         * @return Value of property usuario_creacion.
         */
        public java.lang.String getUsuario_creacion() {
                return usuario_creacion;
        }

        /**
         * Setter for property usuario_creacion.
         * @param usuario_creacion New value of property usuario_creacion.
         */
        public void setUsuario_creacion(java.lang.String usuario_creacion) {
                this.usuario_creacion = usuario_creacion;
        }

        /**
         * Getter for property usuario_modificacion.
         * @return Value of property usuario_modificacion.
         */
        public java.lang.String getUsuario_modificacion() {
                return usuario_modificacion;
        }

        /**
         * Setter for property usuario_modificacion.
         * @param usuario_modificacion New value of property usuario_modificacion.
         */
        public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
                this.usuario_modificacion = usuario_modificacion;
        }

        /**
         * Getter for property nombre.
         * @return Value of property nombre.
         */
        public java.lang.String getNombre() {
                return nombre;
        }

        /**
         * Setter for property nombre.
         * @param nombre New value of property nombre.
         */
        public void setNombre(java.lang.String nombre) {
                this.nombre = nombre;
        }

        /**
         * Getter for property plazo.
         * @return Value of property plazo.
         */
        public int getPlazo () {
            return plazo;
        }

        /**
         * Setter for property plazo.
         * @param plazo New value of property plazo.
         */
        public void setPlazo (int plazo) {
            this.plazo = plazo;
        }

        /**
         * Getter for property handle_code.
         * @return Value of property handle_code.
         */
        public java.lang.String getHandle_code() {
            return handle_code;
        }

        /**
         * Setter for property handle_code.
         * @param handle_code New value of property handle_code.
         */
        public void setHandle_code(java.lang.String handle_code) {
            this.handle_code = handle_code;
        }

        /**
         * Getter for property Nom1.
         * @return Value of property Nom1.
         */
        public java.lang.String getNom1() {
            return Nom1;
        }

        /**
         * Setter for property Nom1.
         * @param Nom1 New value of property Nom1.
         */
        public void setNom1(java.lang.String Nom1) {
            this.Nom1 = Nom1;
        }

        /**
         * Getter for property Nom2.
         * @return Value of property Nom2.
         */
        public java.lang.String getNom2() {
            return Nom2;
        }

        /**
         * Setter for property Nom2.
         * @param Nom2 New value of property Nom2.
         */
        public void setNom2(java.lang.String Nom2) {
            this.Nom2 = Nom2;
        }

        /**
         * Getter for property Ape1.
         * @return Value of property Ape1.
         */
        public java.lang.String getApe1() {
            return Ape1;
        }

        /**
         * Setter for property Ape1.
         * @param Ape1 New value of property Ape1.
         */
        public void setApe1(java.lang.String Ape1) {
            this.Ape1 = Ape1;
        }

        /**
         * Getter for property Ape2.
         * @return Value of property Ape2.
         */
        public java.lang.String getApe2() {
            return Ape2;
        }

        /**
         * Setter for property Ape2.
         * @param Ape2 New value of property Ape2.
         */
        public void setApe2(java.lang.String Ape2) {
            this.Ape2 = Ape2;
        }

        /**
         * Getter for property ciudad.
         * @return Value of property ciudad.
         */
        public java.lang.String getCiudad() {
            return ciudad;
        }

        /**
         * Setter for property ciudad.
         * @param ciudad New value of property ciudad.
         */
        public void setCiudad(java.lang.String ciudad) {
            this.ciudad = ciudad;
        }

        /**
         * Getter for property expced.
         * @return Value of property expced.
         */
        public java.lang.String getExpced() {
            return expced;
        }

        /**
         * Setter for property expced.
         * @param expced New value of property expced.
         */
        public void setExpced(java.lang.String expced) {
            this.expced = expced;
        }

        /**
         * Getter for property direccion.
         * @return Value of property direccion.
         */
        public java.lang.String getDireccion() {
            return direccion;
        }

        /**
         * Setter for property direccion.
         * @param direccion New value of property direccion.
         */
        public void setDireccion(java.lang.String direccion) {
            this.direccion = direccion;
        }

        /**
         * Getter for property telefono.
         * @return Value of property telefono.
         */
        public java.lang.String getTelefono() {
            return telefono;
        }

        /**
         * Setter for property telefono.
         * @param telefono New value of property telefono.
         */
        public void setTelefono(java.lang.String telefono) {
            this.telefono = telefono;
        }

        /**
         * Getter for property celular.
         * @return Value of property celular.
         */
        public java.lang.String getCelular() {
            return celular;
        }

        /**
         * Setter for property celular.
         * @param celular New value of property celular.
         */
        public void setCelular(java.lang.String celular) {
            this.celular = celular;
        }

        /**
         * Getter for property nombre_cuenta.
         * @return Value of property nombre_cuenta.
         */
        public java.lang.String getNombre_cuenta() {
            return nombre_cuenta;
        }

        /**
         * Setter for property nombre_cuenta.
         * @param nombre_cuenta New value of property nombre_cuenta.
         */
        public void setNombre_cuenta(java.lang.String nombre_cuenta) {
            this.nombre_cuenta = nombre_cuenta;
        }

        /**
         * Getter for property cedula_cuenta.
         * @return Value of property cedula_cuenta.
         */
        public java.lang.String getCedula_cuenta() {
            return cedula_cuenta;
        }

        /**
         * Setter for property cedula_cuenta.
         * @param cedula_cuenta New value of property cedula_cuenta.
         */
        public void setCedula_cuenta(java.lang.String cedula_cuenta) {
            this.cedula_cuenta = cedula_cuenta;
        }

        /**
         * Getter for property pais.
         * @return Value of property pais.
         */
        public java.lang.String getPais() {
            return pais;
        }

        /**
         * Setter for property pais.
         * @param pais New value of property pais.
         */
        public void setPais(java.lang.String pais) {
            this.pais = pais;
        }

        /**
         * Getter for property veto.
         * @return Value of property veto.
         */
        public java.lang.String getVeto() {
            return veto;
        }

        /**
         * Setter for property veto.
         * @param veto New value of property veto.
         */
        public void setVeto(java.lang.String veto) {
            this.veto = veto;
        }

        /**
         * Getter for property c_hc.
         * @return Value of property c_hc.
         */
        public java.lang.String getC_hc() {
            return c_hc;
        }

        /**
         * Setter for property c_hc.
         * @param c_hc New value of property c_hc.
         */
        public void setC_hc(java.lang.String c_hc) {
            this.c_hc = c_hc;
        }

        /**
         * Getter for property tipo_pago.
         * @return Value of property tipo_pago.
         */
        public java.lang.String getTipo_pago() {
            return tipo_pago;
        }

        /**
         * Setter for property tipo_pago.
         *
         * @param tipo_pago New value of property tipo_pago.
         */
        public void setTipo_pago(java.lang.String tipo_pago) {
            this.tipo_pago = tipo_pago;
        }

        /**
         * Getter for property c_currency_bank.
         * @return Value of property c_currency_bank.
         */
        public java.lang.String getC_currency_bank() {
            return c_currency_bank;
        }

        /**
         * Setter for property c_currency_bank.
         * @param c_currency_bank New value of property c_currency_bank.
         */
        public void setC_currency_bank(java.lang.String c_currency_bank) {
            this.c_currency_bank = c_currency_bank;
        }

        /**
         * Getter for property currency.
         * @return Value of property currency.
         */
        public java.lang.String getCurrency() {
            return currency;
        }

        /**
         * Setter for property currency.
         * @param currency New value of property currency.
         */
        public void setCurrency(java.lang.String currency) {
            this.currency = currency;
        }

        /**
         * Getter for property nit_beneficiario.
         * @return Value of property nit_beneficiario.
         */
        public java.lang.String getNit_beneficiario() {
            return nit_beneficiario;
        }

        /**
         * Setter for property nit_beneficiario.
         * @param nit_beneficiario New value of property nit_beneficiario.
         */
        public void setNit_beneficiario(java.lang.String nit_beneficiario) {
            this.nit_beneficiario = nit_beneficiario;
        }

        /**
         * Getter for property nom_beneficiario.
         * @return Value of property nom_beneficiario.
         */
        public java.lang.String getNom_beneficiario() {
            return nom_beneficiario;
        }

        /**
         * Setter for property nom_beneficiario.
         * @param nom_beneficiario New value of property nom_beneficiario.
         */
        public void setNom_beneficiario(java.lang.String nom_beneficiario) {
            this.nom_beneficiario = nom_beneficiario;
        }

        /**
         * Getter for property aprobado.
         * @return Value of property aprobado.
         */
        public java.lang.String getAprobado() {
            return aprobado;
        }

        /**
         * Setter for property aprobado.
         * @param aprobado New value of property aprobado.
         */
        public void setAprobado(java.lang.String aprobado) {
            this.aprobado = aprobado;
        }

        /**
         * Getter for property cmc.
         * @return Value of property cmc.
         */
        public java.lang.String getCmc() {
            return cmc;
        }

        /**
         * Setter for property cmc.
         * @param cmc New value of property cmc.
         */
        public void setCmc(java.lang.String cmc) {
            this.cmc = cmc;
        }

        /**
         * Getter for property concept_code.
         * @return Value of property concept_code.
         */
        public java.lang.String getConcept_code() {
            return concept_code;
        }

        /**
         * Setter for property concept_code.
         * @param concept_code New value of property concept_code.
         */
        public void setConcept_code(java.lang.String concept_code) {
            this.concept_code = concept_code;
        }

        /**
         * Getter for property retencion_pago.
         * @return Value of property retencion_pago.
         */
        public java.lang.String getRetencion_pago() {
            return retencion_pago;
        }

        /**
         * Setter for property retencion_pago.
         * @param retencion_pago New value of property retencion_pago.
         */
        public void setRetencion_pago(java.lang.String retencion_pago) {
            this.retencion_pago = retencion_pago;
        }

//------------20100817-------------------//
    public String getAfil() {
        return afil;
    }

    public void setAfil(String afil) {
        this.afil = afil;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }


    public String getCodfen() {
        return codfen;
    }

    public void setCodfen(String codfen) {
        this.codfen = codfen;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    /**
     * Get the value of nit_afiliado
     *
     * @return the value of nit_afiliado
     */
    public String getNit_afiliado() {
        return nit_afiliado;
    }

    /**
     * Set the value of nit_afiliado
     *
     * @param nit_afiliado new value of nit_afiliado
     */
    public void setNit_afiliado(String nit_afiliado) {
        this.nit_afiliado = nit_afiliado;
    }

    public void setTipoProveedor(String tipo_proveedor) {
        this.tipo_proveedor = tipo_proveedor;
    }

    public String getTipoProveedor() {
    return tipo_proveedor;
    }

   


}
