/**
 * Autor : Ing. Roberto Rocha P..
 * Date  : 10 de Julio de 2007
 * Copyrigth Notice : Fintravalores S.A. S.A
 * Version 1.0
-->
<%--
-@(#)
--Descripcion : Bean que maneja los negocios de fenalllco
 **/
package com.tsp.operation.model.beans;

import java.io.Serializable;

public class Negocios implements Serializable, Cloneable {

    private boolean ajuste;
    private String nom_cli;
    private String cod_cli;
    private String nom_cod;
    private String cod_cod;
    private String cod_negocio;
    private String fecha_neg;
    private String cod_tabla;
    private double vr_negocio;
    private int nodocs;
    private double vr_desem;
    private double vr_cust;
    private double vr_aval;
    private String mod_aval;
    private String mod_cust;
    private double por_rem;
    private String mod_rem;
    private String estado;
    private String dist;
    private String creation_date;
    private String creation_user;
    private String fpago;
    private String tneg;
    private String nitp;
    private String bcocode;
    private String bcod;
    private double totpagado;
    private String esta;
    private String obs;
    private String fechatran;
    private String cmc;
    private double totpr;
    private double tdes;
    private String conlet;
    private String bcofid;
    private String numaval;
    private String valor_remesa;
    private String valor_aval;
    private String cnd_aval;
    private String programa;
    private String nombreafil;//rhonalf 2010-06-22
    private String concepto;//rhonalf 2010-06-22
    private double saldoneg;//rhonalf 2010-06-22
    private int cuotas;//rhonalf 2010-06-22
    private int cuotas_ven;//jpinedo 2011-05-22
    private int cuotas_pagas;//jpinedo 2011-05-22
    private int cuotas_vig;//jpinedo 2011-05-22
    private double saldo_ven;//jpinedo 2011-05-22
    private double saldo_pagar;//jpinedo 2011-05-22
    private double valor_cuota;//rhonalf 2010-06-22
    private String fecha_vencimiento;//rhonalf 2010-06-22
    private String factura_vencer;//rhonalf 2010-06-22
    private String nit_tercero; //darrieta 2010-09-23
    private int id_convenio; //darrieta 2010-09-23
    private int id_remesa; //darrieta 2010-10-11
    private String id_sector;//ivargas 2011-04-19
    private String id_subsector;//ivargas 2011-04-19
    private String cuenta_cheque; //ivargas 2011-04-25
    private String tasa;//ivargas 2011-05-11
    private String codigobanco;
    private String pagare;
    private String nuevaTasa;//ivargas 2011-05-11
    private String formulario;//ivargas 2011-07-26
    private String fecha_ap;//jpinedo 2011-02-22
    //borrar despues del montaje de iris fromulario fenalco
    private String cod_estu;
    private String tel;
    private String cel;
    private String direccion;
    private String semestre;
    // --------------------------------------------------
    private String ciclo;//jpinedo 2011-09-14
    private String no_solicitud;//jpinedo 2011-09-14
    private String nom_convenio;//jpinedo 2011-11-04
    private String sector;//jpinedo 2011-11-04
    private String subsector;//jpinedo 2011-11-04
    private String actividad;//ivargas 2011-12-07
    private double valor_central;//ivargas 2012-01-02
    private double porcentaje_cat;//ivargas 2012-01-02
    private double valor_capacitacion;//ivargas 2012-01-02
    private double valor_seguro;//ivargas 2012-01-02
    private String tipoConv;//ivargas 2012-01-02
    private String fecha_liquidacion;//ivargas 2012-01-25
    private String analista;//ivargas 2012-03-23
    private String tipoProceso;//ivargas 2012-03-31
    private String tipo_cuota;//ivargas 2012-06-13
    private int idProvConvenio;//jpinedo 2012-07-11
    private boolean FinanciaAval;//jpinedo 2012-07-13
    private String negocio_rel;//jpinedo 2011-07-16
    private String fecha_factura_aval; //dpaz 2013-03-06
    private int item; 
    private String num_pagare;
    private double valor_fianza;
    private double primera_cuota;
    private String primera_fecha_Vencimiento;
    private double vr_negocio_solicitado;
    private int nodocs_solicitado;
    private double valor_total_poliza;
    private String politica;
    private double valor_renovacion;
    private String analista_asignado;								 
    private String tipo_credito;
    private String agencia;
    private double porc_fin_aval;
    private double porc_dto_aval;
    private double valor_refinanciacion;
    private int nro_docs_ref;
    private String sys_date;
    private String tipo_liq;
    private String asesor_asignado;
    private String preaprobado_ex_cliente;
    private double cuota_aval_mensual;
    private boolean aval_por_cuota;

    public Negocios() {
    }

    public String getTipo_cuota() {
        return tipo_cuota;
    }

    public void setTipo_cuota(String tipo_cuota) {
        this.tipo_cuota = tipo_cuota;
    }

    public String getTipoProceso() {
        return tipoProceso;
    }

    public void setTipoProceso(String tipoProceso) {
        this.tipoProceso = tipoProceso;
    }

    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public String getNuevaTasa() {
        return nuevaTasa;
    }

    public void setNuevaTasa(String nuevaTasa) {
        this.nuevaTasa = nuevaTasa;
    }

    public String getTasa() {
        return tasa;
    }

    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    public String getCodigou() {
        return codigou;
    }

    public void setCodigou(String codigou) {
        this.codigou = codigou;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }
    private String codigou;

    public String getCnd_aval() {
        return cnd_aval;
    }

    public void setCnd_aval(String cnd_aval) {
        this.cnd_aval = cnd_aval;
    }

    public String getCuenta_cheque() {
        return cuenta_cheque;
    }

    public void setCuenta_cheque(String cuenta_cheque) {
        this.cuenta_cheque = cuenta_cheque;
    }

    public String getId_sector() {
        return id_sector;
    }

    public void setId_sector(String id_sector) {
        this.id_sector = id_sector;
    }

    public String getId_subsector() {
        return id_subsector;
    }

    public void setId_subsector(String id_subsector) {
        this.id_subsector = id_subsector;
    }

    /**
     * Getter for property cod_cli.
     * @return Value of property cod_cli.
     */
    public java.lang.String getCod_cli() {
        return cod_cli;
    }

    /**
     * Setter for property cod_cli.
     * @param cod_cli New value of property cod_cli.
     */
    public void setCod_cli(java.lang.String cod_cli) {
        this.cod_cli = cod_cli;
    }

    /**
     * Getter for property fecha_neg.
     * @return Value of property fecha_neg.
     */
    public java.lang.String getFecha_neg() {
        return fecha_neg;
    }

    /**
     * Setter for property fecha_neg.
     * @param fecha_neg New value of property fecha_neg.
     */
    public void setFecha_neg(java.lang.String fecha_neg) {
        this.fecha_neg = fecha_neg;
    }

    /**
     * Getter for property cod_tabla.
     * @return Value of property cod_tabla.
     */
    public java.lang.String getCod_tabla() {
        return cod_tabla;
    }

    /**
     * Setter for property cod_tabla.
     * @param cod_tabla New value of property cod_tabla.
     */
    public void setCod_tabla(java.lang.String cod_tabla) {
        this.cod_tabla = cod_tabla;
    }

    /**
     * Getter for property nodocs.
     * @return Value of property nodocs.
     */
    public int getNodocs() {
        return nodocs;
    }

    /**
     * Setter for property nodocs.
     * @param nodocs New value of property nodocs.
     */
    public void setNodocs(int nodocs) {
        this.nodocs = nodocs;
    }

    /**
     * Getter for property vr_negocio.
     * @return Value of property vr_negocio.
     */
    public double getVr_negocio() {
        return vr_negocio;
    }

    /**
     * Setter for property vr_negocio.
     * @param vr_negocio New value of property vr_negocio.
     */
    public void setVr_negocio(double vr_negocio) {
        this.vr_negocio = vr_negocio;
    }

    /**
     * Getter for property vr_desem.
     * @return Value of property vr_desem.
     */
    public double getVr_desem() {
        return vr_desem;
    }

    /**
     * Setter for property vr_desem.
     * @param vr_desem New value of property vr_desem.
     */
    public void setVr_desem(double vr_desem) {
        this.vr_desem = vr_desem;
    }

    /**
     * Getter for property vr_cust.
     * @return Value of property vr_cust.
     */
    public double getVr_cust() {
        return vr_cust;
    }

    /**
     * Setter for property vr_cust.
     * @param vr_cust New value of property vr_cust.
     */
    public void setVr_cust(double vr_cust) {
        this.vr_cust = vr_cust;
    }

    /**
     * Getter for property vr_aval.
     * @return Value of property vr_aval.
     */
    public double getVr_aval() {
        return vr_aval;
    }

    /**
     * Setter for property vr_aval.
     * @param vr_aval New value of property vr_aval.
     */
    public void setVr_aval(double vr_aval) {
        this.vr_aval = vr_aval;
    }

    /**
     * Getter for property mod_aval.
     * @return Value of property mod_aval.
     */
    public java.lang.String getMod_aval() {
        return mod_aval;
    }

    /**
     * Setter for property mod_aval.
     * @param mod_aval New value of property mod_aval.
     */
    public void setMod_aval(java.lang.String mod_aval) {
        this.mod_aval = mod_aval;
    }

    /**
     * Getter for property mod_cust.
     * @return Value of property mod_cust.
     */
    public java.lang.String getMod_cust() {
        return mod_cust;
    }

    /**
     * Setter for property mod_cust.
     * @param mod_cust New value of property mod_cust.
     */
    public void setMod_cust(java.lang.String mod_cust) {
        this.mod_cust = mod_cust;
    }

    /**
     * Getter for property por_rem.
     * @return Value of property por_rem.
     */
    public double getPor_rem() {
        return por_rem;
    }

    /**
     * Setter for property por_rem.
     * @param por_rem New value of property por_rem.
     */
    public void setPor_rem(double por_rem) {
        this.por_rem = por_rem;
    }

    /**
     * Getter for property mod_rem.
     * @return Value of property mod_rem.
     */
    public java.lang.String getMod_rem() {
        return mod_rem;
    }

    /**
     * Setter for property mod_rem.
     * @param mod_rem New value of property mod_rem.
     */
    public void setMod_rem(java.lang.String mod_rem) {
        this.mod_rem = mod_rem;
    }

    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }

    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * Getter for property fpago.
     * @return Value of property fpago.
     */
    public java.lang.String getFpago() {
        return fpago;
    }

    /**
     * Setter for property fpago.
     * @param fpago New value of property fpago.
     */
    public void setFpago(java.lang.String fpago) {
        this.fpago = fpago;
    }

    /**
     * Getter for property tneg.
     * @return Value of property tneg.
     */
    public java.lang.String getTneg() {
        return tneg;
    }

    /**
     * Setter for property tneg.
     * @param tneg New value of property tneg.
     */
    public void setTneg(java.lang.String tneg) {
        this.tneg = tneg;
    }

    /**
     * Getter for property nom_cli.
     * @return Value of property nom_cli.
     */
    public java.lang.String getNom_cli() {
        return nom_cli;
    }

    /**
     * Setter for property nom_cli.
     * @param nom_cli New value of property nom_cli.
     */
    public void setNom_cli(java.lang.String nom_cli) {
        this.nom_cli = nom_cli;
    }

    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }

    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }

    /**
     * Getter for property nitp.
     * @return Value of property nitp.
     */
    public java.lang.String getNitp() {
        return nitp;
    }

    /**
     * Setter for property nitp.
     * @param nitp New value of property nitp.
     */
    public void setNitp(java.lang.String nitp) {
        this.nitp = nitp;
    }

    /**
     * Getter for property bcocode.
     * @return Value of property bcocode.
     */
    public java.lang.String getBcocode() {
        return bcocode;
    }

    /**
     * Setter for property bcocode.
     * @param bcocode New value of property bcocode.
     */
    public void setBcocode(java.lang.String bcocode) {
        this.bcocode = bcocode;
    }

    /**
     * Getter for property totpagado.
     * @return Value of property totpagado.
     */
    public double getTotpagado() {
        return totpagado;
    }

    /**
     * Setter for property totpagado.
     * @param totpagado New value of property totpagado.
     */
    public void setTotpagado(double totpagado) {
        this.totpagado = totpagado;
    }

    /**
     * Getter for property esta.
     * @return Value of property esta.
     */
    public java.lang.String getEsta() {
        return esta;
    }

    /**
     * Setter for property esta.
     * @param esta New value of property esta.
     */
    public void setEsta(java.lang.String esta) {
        this.esta = esta;
    }

    /**
     * Getter for property obs.
     * @return Value of property obs.
     */
    public java.lang.String getObs() {
        return obs;
    }

    /**
     * Setter for property obs.
     * @param obs New value of property obs.
     */
    public void setObs(java.lang.String obs) {
        this.obs = obs;
    }

    /**
     * Getter for property bcod.
     * @return Value of property bcod.
     */
    public java.lang.String getBcod() {
        return bcod;
    }

    /**
     * Setter for property bcod.
     * @param bcod New value of property bcod.
     */
    public void setBcod(java.lang.String bcod) {
        this.bcod = bcod;
    }

    /**
     * Getter for property cod_negocio.
     * @return Value of property cod_negocio.
     */
    public java.lang.String getCod_negocio() {
        return cod_negocio;
    }

    /**
     * Setter for property cod_negocio.
     * @param cod_negocio New value of property cod_negocio.
     */
    public void setCod_negocio(java.lang.String cod_negocio) {
        this.cod_negocio = cod_negocio;
    }

    /**
     * Getter for property fechatran.
     * @return Value of property fechatran.
     */
    public java.lang.String getFechatran() {
        return fechatran;
    }

    /**
     * Setter for property fechatran.
     * @param fechatran New value of property fechatran.
     */
    public void setFechatran(java.lang.String fechatran) {
        this.fechatran = fechatran;
    }

    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }

    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }

    /**
     * Getter for property totpr.
     * @return Value of property totpr.
     */
    public double getTotpr() {
        return totpr;
    }

    /**
     * Setter for property totpr.
     * @param totpr New value of property totpr.
     */
    public void setTotpr(double totpr) {
        this.totpr = totpr;
    }

    /**
     * Getter for property tdes.
     * @return Value of property tdes.
     */
    public double getTdes() {
        return tdes;
    }

    /**
     * Setter for property tdes.
     * @param tdes New value of property tdes.
     */
    public void setTdes(double tdes) {
        this.tdes = tdes;
    }

    /**
     * Getter for property conlet.
     * @return Value of property conlet.
     */
    public java.lang.String getConlet() {
        return conlet;
    }

    /**
     * Setter for property conlet.
     * @param conlet New value of property conlet.
     */
    public void setConlet(java.lang.String conlet) {
        this.conlet = conlet;
    }

    /**
     * Getter for property bcofid.
     * @return Value of property bcofid.
     */
    public java.lang.String getBcofid() {
        return bcofid;
    }

    /**
     * Setter for property bcofid.
     * @param bcofid New value of property bcofid.
     */
    public void setBcofid(java.lang.String bcofid) {
        this.bcofid = bcofid;
    }

    /**
     * Getter for property numaval.
     * @return Value of property numaval.
     */
    public java.lang.String getNumaval() {
        return numaval;
    }

    /**
     * Setter for property numaval.
     * @param numaval New value of property numaval.
     */
    public void setNumaval(java.lang.String numaval) {
        this.numaval = numaval;
    }

    public java.lang.String getvalor_aval() {
        return valor_aval;
    }

    public java.lang.String getvalor_remesa() {
        return valor_remesa;
    }

    public void setvalor_remesa(java.lang.String valor_remesa1) {
        this.valor_remesa = valor_remesa1;
    }

    public void setvalor_aval(java.lang.String valor_aval1) {
        this.valor_aval = valor_aval1;
    }

    public java.lang.String getPagare() {
        return pagare;
    }

    public void setPagare(java.lang.String pagare1) {
        this.pagare = pagare1;
    }
    ////////////creado por Miguel Altamiranda

    public java.lang.String getCodigoBanco() {
        return codigobanco;
    }

    public void setCodigoBanco(String codigobanco) {
        this.codigobanco = codigobanco;
    }

    public boolean isAjuste() {
        return ajuste;
    }

    /**
     * Setter for property ajuste.
     * @param ajuste New value of property ajuste.
     */
    public void setAjuste(boolean ajuste) {
        this.ajuste = ajuste;
    }

    public String getCod_cod() {
        return cod_cod;
    }

    public void setCod_cod(String cod_cod) {
        this.cod_cod = cod_cod;
    }

    public String getNom_cod() {
        return nom_cod;
    }

    public void setNom_cod(String nom_cod) {
        this.nom_cod = nom_cod;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public int getCuotas() {
        return cuotas;
    }

    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }

    public String getFactura_vencer() {
        return factura_vencer;
    }

    public void setFactura_vencer(String factura_vencer) {
        this.factura_vencer = factura_vencer;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getNombreafil() {
        return nombreafil;
    }

    public void setNombreafil(String nombreafil) {
        this.nombreafil = nombreafil;
    }

    public double getSaldoneg() {
        return saldoneg;
    }

    public void setSaldoneg(double saldoneg) {
        this.saldoneg = saldoneg;
    }

    public double getValor_cuota() {
        return valor_cuota;
    }

    public void setValor_cuota(double valor_cuota) {
        this.valor_cuota = valor_cuota;
    }

    /**
     * Get the value of id_remesa
     *
     * @return the value of id_remesa
     */
    public int getId_remesa() {
        return id_remesa;
    }

    /**
     * Set the value of id_remesa
     *
     * @param id_remesa new value of id_remesa
     */
    public void setId_remesa(int id_remesa) {
        this.id_remesa = id_remesa;
    }

    /**
     * Get the value of id_convenio
     *
     * @return the value of id_convenio
     */
    public int getId_convenio() {
        return id_convenio;
    }

    /**
     * Set the value of id_convenio
     *
     * @param id_convenio new value of id_convenio
     */
    public void setId_convenio(int id_convenio) {
        this.id_convenio = id_convenio;
    }

    /**
     * Get the value of nit_tercero
     *
     * @return the value of nit_tercero
     */
    public String getNit_tercero() {
        return nit_tercero;
    }

    /**
     * Set the value of nit_tercero
     *
     * @param nit_tercero new value of nit_tercero
     */
    public void setNit_tercero(String nit_tercero) {
        this.nit_tercero = nit_tercero;

    }

    public int getCuotas_ven() {
        return cuotas_ven;
    }

    public void setCuotas_ven(int cuotas_ven) {
        this.cuotas_ven = cuotas_ven;
    }

    public int getCuotas_pagas() {
        return cuotas_pagas;
    }

    public void setCuotas_pagas(int cuotas) {
        this.cuotas_pagas = cuotas;
    }

    public int getCuotas_vig() {
        return cuotas_vig;
    }

    public void setCuotas_vig(int cuotas) {
        this.cuotas_vig = cuotas;
    }

    public double getSaldo_ven() {
        return saldo_ven;
    }

    public void setSaldo_ven(double saldo) {
        this.saldo_ven = saldo;
    }

    public double getSaldo_pagar() {
        return saldo_pagar;
    }

    public void setSaldo_pagar(double saldo) {
        this.saldo_pagar = saldo;
    }

    public String getFecha_ap() {
        return fecha_ap;
    }

    public void setFecha_ap(String fecha_ap) {
        this.fecha_ap = fecha_ap;
    }

    public String getcod_est() {
        return cod_estu;
    }

    public void setcod_est(String cod_estu) {
        this.cod_estu = cod_estu;
    }

    public String gettel_est() {
        return tel;
    }

    public void settel_est(String tel) {
        this.tel = tel;
    }

    public String getcel_est() {
        return cel;
    }

    public void setcel_est(String cel) {
        this.cel = cel;
    }

    public String getdireccion_est() {
        return direccion;
    }

    public void setdireccion_est(String direccion) {
        this.direccion = direccion;
    }

    public String getsemestre_est() {
        return semestre;
    }

    public void setsemestre_est(String semestre) {
        this.semestre = semestre;
    }

    public String getFormulario() {
        return formulario;
    }

    public void setFormulario(String formulario) {
        this.formulario = formulario;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getNo_solicitud() {
        return no_solicitud;
    }

    public void setNo_solicitud(String no_solicitud) {
        this.no_solicitud = no_solicitud;
    }

    public String getNom_convenio() {
        return nom_convenio;
    }

    public void setNom_convenio(String nom_convenio) {
        this.nom_convenio = nom_convenio;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSubsector() {
        return subsector;
    }

    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public double getPorcentaje_cat() {
        return porcentaje_cat;
    }

    public void setPorcentaje_cat(double porcentaje_cat) {
        this.porcentaje_cat = porcentaje_cat;
    }

    public double getValor_capacitacion() {
        return valor_capacitacion;
    }

    public void setValor_capacitacion(double valor_capacitacion) {
        this.valor_capacitacion = valor_capacitacion;
    }

    public double getValor_central() {
        return valor_central;
    }

    public void setValor_central(double valor_central) {
        this.valor_central = valor_central;
    }

    public double getValor_seguro() {
        return valor_seguro;
    }

    public void setValor_seguro(double valor_seguro) {
        this.valor_seguro = valor_seguro;
    }

    public String getTipoConv() {
        return tipoConv;
    }

    public void setTipoConv(String tipoConv) {
        this.tipoConv = tipoConv;
    }

    public String getFecha_liquidacion() {
        return fecha_liquidacion;
    }

    public void setFecha_liquidacion(String fecha_liquidacion) {
        this.fecha_liquidacion = fecha_liquidacion;
    }
    
     public String getNegocio_rel() {
        return negocio_rel;
    }

    public void setNegocio_rel(String negocio_rel) {
        this.negocio_rel = negocio_rel;
    }

    public boolean isFinanciaAval() {
        return FinanciaAval;
    }

    public void setFinanciaAval(boolean FinanciaAval) {
        this.FinanciaAval = FinanciaAval;
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }



        public int getIdProvConvenio() {
        return idProvConvenio;
    }

    public void setIdProvConvenio(int idProvConvenio) {
        this.idProvConvenio = idProvConvenio;
    }

    public String getFecha_factura_aval() {
        return fecha_factura_aval;
    }

    public void setFecha_factura_aval(String fecha_factura_aval) {
        this.fecha_factura_aval = fecha_factura_aval;
    }   

    /**
     * @return the item
     */
    public int getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(int item) {
        this.item = item;
    }

    public String getNum_pagare() {
        return num_pagare;
    }

    public void setNum_pagare(String num_pagare) {
        this.num_pagare = num_pagare;
    }

    /**
     * @return the valor_fianza
     */
    public double getValor_fianza() {
        return valor_fianza;
    }

    /**
     * @param valor_fianza the valor_fianza to set
     */
    public void setValor_fianza(double valor_fianza) {
        this.valor_fianza = valor_fianza;
    }
    
    public double getPrimera_cuota() {
        return primera_cuota;
    }
    
    public void setPrimera_cuota(double primera_cuota) {
        this.primera_cuota = primera_cuota;
    }
    
    public String getPrimera_fecha_Vencimiento() {
        return primera_fecha_Vencimiento;
    }
    
    public void setPrimera_fecha_Vencimiento(String primera_fecha_Vencimiento) {
        this.primera_fecha_Vencimiento = primera_fecha_Vencimiento;
    }

    public double getVr_negocio_solicitado() {
        return vr_negocio_solicitado;
    }

    public void setVr_negocio_solicitado(double vr_negocio_solicitado) {
        this.vr_negocio_solicitado = vr_negocio_solicitado;
    }

    public int getNodocs_solicitado() {
        return nodocs_solicitado;
    }

    public void setNodocs_solicitado(int nodocs_solicitado) {
        this.nodocs_solicitado = nodocs_solicitado;
    }
    public double getValor_total_poliza() {
        return valor_total_poliza;
    }

    public void setValor_total_poliza(double valor_total_poliza) {
        this.valor_total_poliza = valor_total_poliza;
    }
    
    public String getPolitica(){   
        return politica;
    }
    
    public void setPolitica (String politica){        
     this.politica=politica;     
    }
    
    public double getValor_renovacion(){        
        return  valor_renovacion;    
    }
    
    public void setValor_renovacion (double valor_renovacion){        
     this.valor_renovacion=valor_renovacion;     
    }

    public String getTipo_credito() {
        return tipo_credito;
    }

    public void setTipo_credito(String tipo_credito) {
        this.tipo_credito = tipo_credito;
    }
        /**
     * @return the analista_asignado
     */
    public String getAnalista_asignado() {
        return analista_asignado;
    }

    /**
     * @param analista_asignado the analista_asignado to set
     */
    public void setAnalista_asignado(String analista_asignado) {
        this.analista_asignado = analista_asignado;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public double getPorc_fin_aval() {
        return porc_fin_aval;
    }

    public void setPorc_fin_aval(double porc_fin_aval) {
        this.porc_fin_aval = porc_fin_aval;
    }

    public double getPorc_dto_aval() {
        return porc_dto_aval;
    }

    public void setPorc_dto_aval(double porc_dto_aval) {
        this.porc_dto_aval = porc_dto_aval;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getValor_remesa() {
        return valor_remesa;
    }

    public void setValor_remesa(String valor_remesa) {
        this.valor_remesa = valor_remesa;
    }

    public String getValor_aval() {
        return valor_aval;
    }

    public void setValor_aval(String valor_aval) {
        this.valor_aval = valor_aval;
    }

    public String getCodigobanco() {
        return codigobanco;
    }

    public void setCodigobanco(String codigobanco) {
        this.codigobanco = codigobanco;
    }

    public String getCod_estu() {
        return cod_estu;
    }

    public void setCod_estu(String cod_estu) {
        this.cod_estu = cod_estu;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public double getValor_refinanciacion() {
        return valor_refinanciacion;
    }

    public void setValor_refinanciacion(double valor_refinanciacion) {
        this.valor_refinanciacion = valor_refinanciacion;
    }

    public int getNro_docs_ref() {
        return nro_docs_ref;
    }

    public void setNro_docs_ref(int nro_docs_ref) {
        this.nro_docs_ref = nro_docs_ref;
    }

    public String getSys_date() {
        return sys_date;
    }

    public void setSys_date(String sys_date) {
        this.sys_date = sys_date;
    }

    public String getTipo_liq() {
        return tipo_liq;
    }

    public void setTipo_liq(String tipo_liq) {
        this.tipo_liq = tipo_liq;
    }

    public String getAsesor_asignado() {
        return asesor_asignado;
    }

    public void setAsesor_asignado(String asesor_asignado) {
        this.asesor_asignado = asesor_asignado;
    }

    public String getPreaprobado_ex_cliente() {
        return preaprobado_ex_cliente;
    }

    public void setPreaprobado_ex_cliente(String preaprobado_ex_cliente) {
        this.preaprobado_ex_cliente = preaprobado_ex_cliente;
    }

    public double getCuota_aval_mensual() {
        return cuota_aval_mensual;
    }

    public void setCuota_aval_mensual(double cuota_aval_mensual) {
        this.cuota_aval_mensual = cuota_aval_mensual;
    }

    public boolean isAval_por_cuota() {
        return aval_por_cuota;
    }

    public void setAval_por_cuota(boolean aval_por_cuota) {
        this.aval_por_cuota = aval_por_cuota;
    }
   
    
}