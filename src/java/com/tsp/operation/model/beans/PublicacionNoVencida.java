 /***********************************************
 * Nombre: PublicacionNoVencida.java
 * Descripci�n: Beans publicaciones no vencidas.
 * Autor: Ing. Jose de la rosa
 * Fecha: 6 de abril de 2006, 02:50 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ***********************************************/

package com.tsp.operation.model.beans;

public class PublicacionNoVencida {
    
    private int opcion;
    private String usuario;
    private String fecha_ingreso;
    private int num_veces;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String base;
    private String distrito;
    private String rec_status;
    private String texto;
    private String tipo;
    private String titulo;
    private String nombre_dir;
    
    /** Creates a new instance of PublicacionNoVencida */
    public PublicacionNoVencida () {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion () {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion (java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property fecha_ingreso.
     * @return Value of property fecha_ingreso.
     */
    public java.lang.String getFecha_ingreso () {
        return fecha_ingreso;
    }
    
    /**
     * Setter for property fecha_ingreso.
     * @param fecha_ingreso New value of property fecha_ingreso.
     */
    public void setFecha_ingreso (java.lang.String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }
    
    /**
     * Getter for property num_veces.
     * @return Value of property num_veces.
     */
    public int getNum_veces () {
        return num_veces;
    }
    
    /**
     * Setter for property num_veces.
     * @param num_veces New value of property num_veces.
     */
    public void setNum_veces (int num_veces) {
        this.num_veces = num_veces;
    }
    
    /**
     * Getter for property opcion.
     * @return Value of property opcion.
     */
    public int getOpcion () {
        return opcion;
    }
    
    /**
     * Setter for property opcion.
     * @param opcion New value of property opcion.
     */
    public void setOpcion (int opcion) {
        this.opcion = opcion;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status () {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status (java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property ultima_modificacion.
     * @return Value of property ultima_modificacion.
     */
    public java.lang.String getUltima_modificacion () {
        return ultima_modificacion;
    }
    
    /**
     * Setter for property ultima_modificacion.
     * @param ultima_modificacion New value of property ultima_modificacion.
     */
    public void setUltima_modificacion (java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario () {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario (java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion () {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion (java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion () {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property texto.
     * @return Value of property texto.
     */
    public java.lang.String getTexto () {
        return texto;
    }
    
    /**
     * Setter for property texto.
     * @param texto New value of property texto.
     */
    public void setTexto (java.lang.String texto) {
        this.texto = texto;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo () {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo (java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property titulo.
     * @return Value of property titulo.
     */
    public java.lang.String getTitulo () {
        return titulo;
    }
    
    /**
     * Setter for property titulo.
     * @param titulo New value of property titulo.
     */
    public void setTitulo (java.lang.String titulo) {
        this.titulo = titulo;
    }
    
    /**
     * Getter for property nombre_dir.
     * @return Value of property nombre_dir.
     */
    public java.lang.String getNombre_dir() {
        return nombre_dir;
    }
    
    /**
     * Setter for property nombre_dir.
     * @param nombre_dir New value of property nombre_dir.
     */
    public void setNombre_dir(java.lang.String nombre_dir) {
        this.nombre_dir = nombre_dir;
    }
    
}
