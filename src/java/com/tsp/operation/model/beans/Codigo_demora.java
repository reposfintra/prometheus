/*
 * Codigo_demora.java
 *
 * Created on 24 de junio de 2005, 10:14 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Jose
 */
public class Codigo_demora implements Serializable{
    private String codigo;
    private String descripcion;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    private String base;
    /** Creates a new instance of Codigo_demora */
    public static Codigo_demora load(ResultSet rs)throws SQLException{
        Codigo_demora c = new Codigo_demora();
        c.setCodigo(rs.getString("codigo"));
        c.setDescripcion(rs.getString("descripcion"));
        c.setCia(rs.getString("cia"));
        c.setRec_status(rs.getString("rec_status"));
        c.setLast_update(rs.getDate("last_update"));
        c.setUser_update(rs.getString("user_update"));
        c.setCreation_date(rs.getDate("creation_date"));
        c.setCreation_user(rs.getString("creation_user"));
        c.setBase(rs.getString("base"));
        return c;
    }

    public java.lang.String getCia() {
        return cia;
    }

    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }

    public java.lang.String getCodigo() {
        return codigo;
    }

    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }

    public java.util.Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }

    public java.lang.String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }

    public java.lang.String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }

    public java.util.Date getLast_update() {
        return last_update;
    }

    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }

    public java.lang.String getRec_status() {
        return rec_status;
    }

    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }

    public java.lang.String getUser_update() {
        return user_update;
    }

    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    } 
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
}
