/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author JM-DESARROLLO
 */
public class perfilRiesgo {
    private int id;
    private double monto_maximo;    
    private double monto_minimo;
    private int cant_max_cred;
    private String descripcion;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String dstrct;
    private String reg_status;
    private String eliminar;
    private String modificar;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the monto_maximo
     */
    public double getMonto_maximo() {
        return monto_maximo;
    }

    /**
     * @param monto_maximo the monto_maximo to set
     */
    public void setMonto_maximo(double monto_maximo) {
        this.monto_maximo = monto_maximo;
    }

    /**
     * @return the monto_minimo
     */
    public double getMonto_minimo() {
        return monto_minimo;
    }

    /**
     * @param monto_minimo the monto_minimo to set
     */
    public void setMonto_minimo(double monto_minimo) {
        this.monto_minimo = monto_minimo;
    }

    /**
     * @return the cant_max_cred
     */
    public int getCant_max_cred() {
        return cant_max_cred;
    }

    /**
     * @param cant_max_cred the cant_max_cred to set
     */
    public void setCant_max_cred(int cant_max_cred) {
        this.cant_max_cred = cant_max_cred;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the eliminar
     */
    public String getEliminar() {
        return eliminar;
    }

    /**
     * @param eliminar the eliminar to set
     */
    public void setEliminar(String eliminar) {
        this.eliminar = eliminar;
    }

    /**
     * @return the modificar
     */
    public String getModificar() {
        return modificar;
    }

    /**
     * @param modificar the modificar to set
     */
    public void setModificar(String modificar) {
        this.modificar = modificar;
    }


    

    
}
