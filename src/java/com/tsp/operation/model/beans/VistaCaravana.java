/*
 * VistaCaravana.java
 *
 * Created on 1 de agosto de 2005, 11:47
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;
import java.sql.*;
/**
 *
 * @author  Henry
 */
public class VistaCaravana implements Serializable {
    
    private String fecpla;
    private String plaveh;
    private String numpla;
    private String numrem;
    private String nomcli;
    private String codcli;
    private String oripla;
    private String despla;
    private String orinom;
    private String desnom;
    private String cedConductor;
    private String nomConductor;
    private String std_job_desc;
    private String std_job_no;
    private boolean seleccionado;
    private String razon_exclusion = "";
    private String ruta_pla;
    private String via;
    private String origen;
    private String destino;
    private String fecinicio;
    
    public VistaCaravana load(ResultSet rs) throws SQLException {
        VistaCaravana v = null;
        v.setPlaveh(rs.getString("plaveh"));
        v.setNumpla(rs.getString("numpla"));
        v.setNomcli(rs.getString("nomcli"));
        v.setCodcli(rs.getString("codcli"));
        v.setOripla(rs.getString("oripla"));
        v.setDespla(rs.getString("despla"));
        v.setOrinom(rs.getString("orinom"));
        v.setDesnom(rs.getString("desnom"));
        v.setNomConductor(rs.getString("nombre"));
        v.setStd_job_desc(rs.getString("std_job_desc"));
        v.setStd_job_no(rs.getString("std_job_no"));
        v.setFecpla(rs.getString("fecpla"));
        return v;
    }
    
    public VistaCaravana() {
    }
    
    /**
     * Getter for property plaveh.
     * @return Value of property plaveh.
     */
    public java.lang.String getPlaveh() {
        return plaveh;
    }
    
    /**
     * Setter for property plaveh.
     * @param plaveh New value of property plaveh.
     */
    public void setPlaveh(java.lang.String plaveh) {
        this.plaveh = plaveh;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property nomcli.
     * @return Value of property nomcli.
     */
    public java.lang.String getNomcli() {
        return nomcli;
    }
    
    /**
     * Setter for property nomcli.
     * @param nomcli New value of property nomcli.
     */
    public void setNomcli(java.lang.String nomcli) {
        this.nomcli = nomcli;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property oripla.
     * @return Value of property oripla.
     */
    public java.lang.String getOripla() {
        return oripla;
    }
    
    /**
     * Setter for property oripla.
     * @param oripla New value of property oripla.
     */
    public void setOripla(java.lang.String oripla) {
        this.oripla = oripla;
    }
    
    /**
     * Getter for property despla.
     * @return Value of property despla.
     */
    public java.lang.String getDespla() {
        return despla;
    }
    
    /**
     * Setter for property despla.
     * @param despla New value of property despla.
     */
    public void setDespla(java.lang.String despla) {
        this.despla = despla;
    }
    
    /**
     * Getter for property orinom.
     * @return Value of property orinom.
     */
    public java.lang.String getOrinom() {
        return orinom;
    }
    
    /**
     * Setter for property orinom.
     * @param orinom New value of property orinom.
     */
    public void setOrinom(java.lang.String orinom) {
        this.orinom = orinom;
    }
    
    /**
     * Getter for property desnom.
     * @return Value of property desnom.
     */
    public java.lang.String getDesnom() {
        return desnom;
    }
    
    /**
     * Setter for property desnom.
     * @param desnom New value of property desnom.
     */
    public void setDesnom(java.lang.String desnom) {
        this.desnom = desnom;
    }
    
    /**
     * Getter for property std_job_desc.
     * @return Value of property std_job_desc.
     */
    public java.lang.String getStd_job_desc() {
        return std_job_desc;
    }
    
    /**
     * Setter for property std_job_desc.
     * @param std_job_desc New value of property std_job_desc.
     */
    public void setStd_job_desc(java.lang.String std_job_desc) {
        this.std_job_desc = std_job_desc;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property nomConductor.
     * @return Value of property nomConductor.
     */
    public java.lang.String getNomConductor() {
        return nomConductor;
    }
    
    /**
     * Setter for property nomConductor.
     * @param nomConductor New value of property nomConductor.
     */
    public void setNomConductor(java.lang.String nomConductor) {
        this.nomConductor = nomConductor;
    }
    
    /**
     * Getter for property fecpla.
     * @return Value of property fecpla.
     */
    public java.lang.String getFecpla() {
        return fecpla;
    }
    
    /**
     * Setter for property fecpla.
     * @param fecpla New value of property fecpla.
     */
    public void setFecpla(java.lang.String fecpla) {
        this.fecpla = fecpla;
    }
    
    /**
     * Getter for property seleccionado.
     * @return Value of property seleccionado.
     */
    public boolean isSeleccionado() {
        return seleccionado;
    }
    
    /**
     * Setter for property seleccionado.
     * @param seleccionado New value of property seleccionado.
     */
    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }
    
    /**
     * Getter for property razon_exclusion.
     * @return Value of property razon_exclusion.
     */
    public java.lang.String getRazon_exclusion() {
        return razon_exclusion;
    }
    
    /**
     * Setter for property razon_exclusion.
     * @param razon_exclusion New value of property razon_exclusion.
     */
    public void setRazon_exclusion(java.lang.String razon_exclusion) {
        this.razon_exclusion = razon_exclusion;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property cedConductor.
     * @return Value of property cedConductor.
     */
    public java.lang.String getCedConductor() {
        return cedConductor;
    }
    
    /**
     * Setter for property cedConductor.
     * @param cedConductor New value of property cedConductor.
     */
    public void setCedConductor(java.lang.String cedConductor) {
        this.cedConductor = cedConductor;
    }
    
    /**
     * Getter for property ruta_pla.
     * @return Value of property ruta_pla.
     */
    public java.lang.String getRuta_pla() {
        return ruta_pla;
    }
    
    /**
     * Setter for property ruta_pla.
     * @param ruta_pla New value of property ruta_pla.
     */
    public void setRuta_pla(java.lang.String ruta_pla) {
        this.ruta_pla = ruta_pla;
    }
    
    /**
     * Getter for property via.
     * @return Value of property via.
     */
    public java.lang.String getVia () {
        return via;
    }
    
    /**
     * Setter for property via.
     * @param via New value of property via.
     */
    public void setVia (java.lang.String via) {
        this.via = via;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen () {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen (java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino () {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino (java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property fecinicio.
     * @return Value of property fecinicio.
     */
    public java.lang.String getFecinicio () {
        return fecinicio;
    }
    
    /**
     * Setter for property fecinicio.
     * @param fecinicio New value of property fecinicio.
     */
    public void setFecinicio (java.lang.String fecinicio) {
        this.fecinicio = fecinicio;
    }
    
}
