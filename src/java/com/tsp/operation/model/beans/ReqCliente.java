//*************************************** clase modificada
/*
 * ReqCliente.java
 *
 * Created on 25 de mayo de 2005, 10:14 PM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  DIOGENES
 */
public class ReqCliente implements Serializable {
    private String dstrct_code;
    private String id_cliente;
    private String origen;
    private String destino;
    private String std_job_no;
    private String fecha_dispo;
    private String nuevafecha_dispo;
    private String tipo_asign;
    private String fecha_asign;
    private String Id_rec_cab;
    private String Id_rec_tra;
    private String clase_req;
    private String numpla;
    private String Tipo_recurso1;
    private String Tipo_recurso2;
    private String Tipo_recurso3;
    private String Tipo_recurso4;
    private String Tipo_recurso5;
    private String recurso1;
    private String recurso2;
    private String recurso3;
    private String recurso4;
    private String recurso5;
    private String estado;
    private String error;
    private String observacion;
    private int num_sec;
    private String Usuario_creacion;
    private String fecha_creacion;
    private String Usuario_actualizacion;
    private String fecha_actualizacion;
    private String fecpla;
    
    /////alpall
    private String prioridad1;
    private String prioridad5;
    private String prioridad2;
    private String prioridad3;
    private String prioridad4;
    private String id_rec_cab;
    private String id_rec_tra;
    private String fecha_posibleentrega;
    //KREALES
    private String agaAsoc="";
    private boolean internacional=false;
    private String paisO="";
    private String paisD="";
    private String origen_ini="";
    private String destino_ini="";
    private String agaAsocDest="";
    private String frontera = "";
    private String stdjob_esp;
    /** Creates a new instance of ReqCliente */
    
    
    public void setcliente(String id_cliente){
        this.id_cliente=id_cliente;
    }
    
    public void setdstrct_code(String dstrct_code){
        this.dstrct_code=dstrct_code;
    }
    
    public void setorigen(String origen){
        this.origen=origen;
    }
    
    public void setdestino(String destino){
        this.destino=destino;
    }
    public void setnum_sec(int num_sec){
        this.num_sec = num_sec;
    }
    public void setstd_job_no(String std_job_no){
        this.std_job_no=std_job_no;
    }
    
    public void setfecha_dispo(String fecha_dispo){
        this.fecha_dispo=fecha_dispo;
    }
    
    public void setnuevafecha_dispo(String nuevafecha_dispo){
        this.nuevafecha_dispo=nuevafecha_dispo;
    }
    
    public void settipo_asign(String tipo_asign){
        this.tipo_asign=tipo_asign;
    }
    
    public void setfecha_asign(String fecha_asign){
        this.fecha_asign=fecha_asign;
    }
    
    public void setId_rec_cab(String Id_rec_cab){
        this.Id_rec_cab=Id_rec_cab;
    }
    public void setId_rec_tra(String Id_rec_tra){
        this.Id_rec_tra=Id_rec_tra;
    }
    public void setclase_req(String clase_req){
        this.clase_req=clase_req;
    }
    public void setnumpla(String numpla){
        this.numpla=numpla;
    }
    public void setTipo_recurso1(String Tipo_recurso1){
        this.Tipo_recurso1=Tipo_recurso1;
    }
    public void setTipo_recurso2(String Tipo_recurso2){
        this.Tipo_recurso2=Tipo_recurso2;
    }
    public void setTipo_recurso3(String Tipo_recurso3){
        this.Tipo_recurso3=Tipo_recurso3;
    }
    public void setTipo_recurso4(String Tipo_recurso4){
        this.Tipo_recurso4=Tipo_recurso4;
    }
    public void setTipo_recurso5(String Tipo_recurso5){
        this.Tipo_recurso5=Tipo_recurso5;
    }
    public void setrecurso1(String recurso1){
        this.recurso1=recurso1;
    }
    public void setrecurso2(String recurso2){
        this.recurso2=recurso2;
    }
    public void setrecurso3(String recurso3){
        this.recurso3=recurso3;
    }
    public void setrecurso4(String recurso4){
        this.recurso4=recurso4;
    }
    public void setrecurso5(String recurso5){
        this.recurso5=recurso5;
    }
    
    public void setestado(String estado){
        this.estado=estado;
    }
    public void setUsuario_creacion(String Usuario_creacion){
        this.Usuario_creacion=Usuario_creacion;
    }
    public void setfecha_creacion(String fecha_creacion){
        this.fecha_creacion=fecha_creacion;
    }
    public void setUsuario_actualizacion(String Usuario_actualizacion){
        this.Usuario_actualizacion=Usuario_actualizacion;
    }
    public void setfecha_actualizacion(String fecha_actualizacion){
        this.fecha_actualizacion=fecha_actualizacion;
    }
    public void setError(String error){
        this.error=error;
    }
    public void setObservacion(String obs){
        this.observacion=obs;
    }
    public void setFecpla(String fecpla){
        this.fecpla=fecpla;
    }
    
    //**************************************************************
    
    public String getcliente(){
        return id_cliente;
    }
    public String getdstrct_code( ){
        return dstrct_code;
    }
    public String getorigen(){
        return origen;
    }
    
    public String getdestino(){
        return destino;
    }
    
    public String getstd_job_no(){
        return std_job_no;
    }
    
    public String getfecha_dispo(){
        return fecha_dispo;
    }
    
    public String getnuevafecha_dispo(){
        return nuevafecha_dispo;
    }
    
    public String gettipo_asign(){
        return tipo_asign;
    }
    
    public String getfecha_asign(){
        return fecha_asign;
    }
    
    public int getnum_sec(){
        return num_sec;
    }
    
    public String getId_rec_cab(){
        return Id_rec_cab;
    }
    public String getId_rec_tra(){
        return Id_rec_tra;
    }
    public String getclase_req(){
        return clase_req;
    }
    public String getnumpla(){
        return numpla;
    }
    public String getTipo_recurso1(){
        return Tipo_recurso1;
    }
    public String getTipo_recurso2(){
        return Tipo_recurso2;
    }
    public String getTipo_recurso3(){
        return Tipo_recurso3;
    }
    public String getTipo_recurso4(){
        return Tipo_recurso4;
    }
    public String getTipo_recurso5(){
        return Tipo_recurso5;
    }
    public String getrecurso1(){
        return recurso1;
    }
    public String getrecurso2(){
        return recurso2;
    }
    public String getrecurso3(){
        return recurso3;
    }
    public String getrecurso4(){
        return recurso4;
    }
    public String getrecurso5(){
        return recurso5;
    }
    
    public String getestado(){
        return estado;
    }
    
    public String getprioridad1(){
        return prioridad1;
    }
    public String getprioridad2(){
        return prioridad2;
    }
    public String getprioridad3(){
        return prioridad3;
    }
    public String getprioridad4(){
        return prioridad4;
    }
    public String getprioridad5(){
        return prioridad5;
    }
    
    public String getUsuario_creacion(){
        return Usuario_creacion;
    }
    public String getfecha_creacion(){
        return fecha_creacion;
    }
    public String getUsuario_actualizacion(){
        return Usuario_actualizacion;
    }
    public String getfecha_actualizacion(){
        return fecha_actualizacion;
    }
    
    //////////////alpall
    public String toString(){
        return "codigo = "+this.getdstrct_code()+",  fechaDisponibilidad="+this.getfecha_dispo()+", origen="+this.getorigen()+", destino="+this.getdestino();
    }
    
    /**
     * setprioridad1
     *
     * @param v String
     */
    public void setprioridad1( String v ) {
        this.prioridad1 = v;
    }
    
    public void setprioridad2( String v ) {
        this.prioridad2 = v;
    }
    
    public void setprioridad3( String v ) {
        this.prioridad3 = v;
    }
    
    public void setprioridad4( String v ) {
        this.prioridad4 = v;
    }
    
    
    public void setprioridad5( String v ) {
        this.prioridad5 = v;
    }
    
    public String getFecha_posibleentrega() {
        return fecha_posibleentrega;
    }
    public String getError(){
        return error;
    }
    
    public String getObservacion(){
        return observacion;
    }
    public String getFecpla(){
        return fecpla;
    }
    
    
    /**
     * setid_rec_cab
     *
     * @param v String
     */
    public void setid_rec_cab( String v ) {
        this.id_rec_cab = v;
    }
    
    /**
     * setid_rec_tra
     *
     * @param v String
     */
    public void setid_rec_tra( String v ) {
        this.id_rec_tra = v;
    }
    
    /**
     * setfecha_posibleentrega
     *
     * @param v String
     */
    public void setfecha_posibleentrega( String v ) {
        fecha_posibleentrega = v;
    }
    
    /**
     * Getter for property agaAsoc.
     * @return Value of property agaAsoc.
     */
    public java.lang.String getAgaAsoc() {
        return agaAsoc;
    }
    
    /**
     * Setter for property agaAsoc.
     * @param agaAsoc New value of property agaAsoc.
     */
    public void setAgaAsoc(java.lang.String agaAsoc) {
        this.agaAsoc = agaAsoc;
    }
    
    /**
     * Getter for property internacional.
     * @return Value of property internacional.
     */
    public boolean isInternacional() {
        return internacional;
    }
    
    /**
     * Setter for property internacional.
     * @param internacional New value of property internacional.
     */
    public void setInternacional(boolean internacional) {
        this.internacional = internacional;
    }
    
    /**
     * Getter for property paisD.
     * @return Value of property paisD.
     */
    public java.lang.String getPaisD() {
        return paisD;
    }
    
    /**
     * Setter for property paisD.
     * @param paisD New value of property paisD.
     */
    public void setPaisD(java.lang.String paisD) {
        this.paisD = paisD;
    }
    
    /**
     * Getter for property paisO.
     * @return Value of property paisO.
     */
    public java.lang.String getPaisO() {
        return paisO;
    }
    
    /**
     * Setter for property paisO.
     * @param paisO New value of property paisO.
     */
    public void setPaisO(java.lang.String paisO) {
        this.paisO = paisO;
    }
    
    /**
     * Getter for property origen_ini.
     * @return Value of property origen_ini.
     */
    public java.lang.String getOrigen_ini() {
        return origen_ini;
    }
    
    /**
     * Setter for property origen_ini.
     * @param origen_ini New value of property origen_ini.
     */
    public void setOrigen_ini(java.lang.String origen_ini) {
        this.origen_ini = origen_ini;
    }
    
    /**
     * Getter for property destino_ini.
     * @return Value of property destino_ini.
     */
    public java.lang.String getDestino_ini() {
        return destino_ini;
    }
    
    /**
     * Setter for property destino_ini.
     * @param destino_ini New value of property destino_ini.
     */
    public void setDestino_ini(java.lang.String destino_ini) {
        this.destino_ini = destino_ini;
    }
    public static ReqCliente copiarRequerimiento( ReqCliente rn ) throws SQLException {
        ReqCliente r = new ReqCliente();
        r.setdstrct_code(rn.getdstrct_code());
        r.setestado( rn.getestado() );
        r.setnum_sec(rn.getnum_sec());
        r.setstd_job_no( rn.getstd_job_no());
        r.setnumpla( rn.getnumpla());
        r.setfecha_dispo(rn.getfecha_dispo());
        r.setcliente(rn.getcliente());
        r.setorigen( rn.getorigen());
        r.setAgaAsoc(rn.getAgaAsoc());
        r.setdestino( rn.getdestino());
        r.setclase_req(rn.getclase_req());
        r.setTipo_recurso1(rn.getTipo_recurso1());
        r.setTipo_recurso2(rn.getTipo_recurso2());
        r.setTipo_recurso3(rn.getTipo_recurso3());
        r.setTipo_recurso4(rn.getTipo_recurso4());
        r.setTipo_recurso5(rn.getTipo_recurso5());
        r.setrecurso1(rn.getrecurso1());
        r.setrecurso2(rn.getrecurso2());
        r.setrecurso3(rn.getrecurso3());
        r.setrecurso4(rn.getrecurso4());
        r.setrecurso5(rn.getrecurso5());
        r.setprioridad1(rn.getprioridad1());
        r.setprioridad2(rn.getprioridad2());
        r.setprioridad3(rn.getprioridad3());
        r.setprioridad4(rn.getprioridad4());
        r.setprioridad5(rn.getprioridad5());
        r.setfecha_asign(rn.getfecha_asign());
        r.setnuevafecha_dispo(rn.getnuevafecha_dispo());
        r.setid_rec_cab(rn.getId_rec_cab());
        r.setid_rec_tra(rn.getId_rec_tra());
        r.setfecha_posibleentrega(rn.getFecha_posibleentrega());
        r.setnuevafecha_dispo(rn.getnuevafecha_dispo());
        r.settipo_asign(rn.gettipo_asign());
        r.setInternacional(rn.isInternacional());
        r.setPaisD(rn.getPaisD());
        r.setPaisO(rn.getPaisO());
        return r;
    }
    
    /**
     * Getter for property agaAsocDest.
     * @return Value of property agaAsocDest.
     */
    public java.lang.String getAgaAsocDest() {
        return agaAsocDest;
    }
    
    /**
     * Setter for property agaAsocDest.
     * @param agaAsocDest New value of property agaAsocDest.
     */
    public void setAgaAsocDest(java.lang.String agaAsocDest) {
        this.agaAsocDest = agaAsocDest;
    }
    
    /**
     * Getter for property frontera.
     * @return Value of property frontera.
     */
    public java.lang.String getFrontera() {
        return frontera;
    }
    
    /**
     * Setter for property frontera.
     * @param frontera New value of property frontera.
     */
    public void setFrontera(java.lang.String frontera) {
        this.frontera = frontera;
    }
    
    /**
     * Getter for property stdjob_esp.
     * @return Value of property stdjob_esp.
     */
    public java.lang.String getStdjob_esp() {
        return stdjob_esp;
    }
    
    /**
     * Setter for property stdjob_esp.
     * @param stdjob_esp New value of property stdjob_esp.
     */
    public void setStdjob_esp(java.lang.String stdjob_esp) {
        this.stdjob_esp = stdjob_esp;
    }
    
}
