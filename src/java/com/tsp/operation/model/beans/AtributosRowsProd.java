/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mmedina
 */
public class AtributosRowsProd {
    private BeanGeneral periodo_anticipo;  

    public AtributosRowsProd() {
    }

    /**
     * @return the periodo_anticipo
     */
    public BeanGeneral getPeriodo_anticipo() {
        return periodo_anticipo;
    }

    /**
     * @param periodo_anticipo the periodo_anticipo to set
     */
    public void setPeriodo_anticipo(BeanGeneral periodo_anticipo) {
        this.periodo_anticipo = periodo_anticipo;
    }
    
}
