 /********************************************************************
 *      Nombre Clase.................   Demora.java    
 *      Descripci�n..................   Representa el archivo demora
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   04.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;
import java.util.*;
import com.tsp.util.Util;

public class Demora {
    private String planilla;
    private String fecha_demora;
    private String sitio_demora;
    private String observacion;
    private String finalizada;
    private String fecha_finalizada;
    private String usuario_finalizacion;
    private String codigo_demora;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String distrito;
    private String estado;
    private String base;
    private Integer duracion;
    private String codigo_jerarquia;
    private Integer id;
    private String fecha_fin_demora;    
    //private 
    
    
    /** Creates a new instance of Demora */
    public Demora() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property codigo_demora.
     * @return Value of property codigo_demora.
     */
    public java.lang.String getCodigo_demora() {
        return codigo_demora;
    }
    
    /**
     * Setter for property codigo_demora.
     * @param codigo_demora New value of property codigo_demora.
     */
    public void setCodigo_demora(java.lang.String codigo_demora) {
        this.codigo_demora = codigo_demora;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property fecha_demora.
     * @return Value of property fecha_demora.
     */
    public java.lang.String getFecha_demora() {
        return fecha_demora;
    }
    
    /**
     * Setter for property fecha_demora.
     * @param fecha_demora New value of property fecha_demora.
     */
    public void setFecha_demora(java.lang.String fecha_demora) {
        this.fecha_demora = fecha_demora;
    }
    
    /**
     * Getter for property fecha_finalizada.
     * @return Value of property fecha_finalizada.
     */
    public java.lang.String getFecha_finalizada() {
        return fecha_finalizada;
    }
    
    /**
     * Setter for property fecha_finalizada.
     * @param fecha_finalizada New value of property fecha_finalizada.
     */
    public void setFecha_finalizada(java.lang.String fecha_finalizada) {
        this.fecha_finalizada = fecha_finalizada;
    }
    
    /**
     * Getter for property finalizada.
     * @return Value of property finalizada.
     */
    public java.lang.String getFinalizada() {
        return finalizada;
    }
    
    /**
     * Setter for property finalizada.
     * @param finalizada New value of property finalizada.
     */
    public void setFinalizada(java.lang.String finalizada) {
        this.finalizada = finalizada;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property sitio_demora.
     * @return Value of property sitio_demora.
     */
    public java.lang.String getSitio_demora() {
        return sitio_demora;
    }
    
    /**
     * Setter for property sitio_demora.
     * @param sitio_demora New value of property sitio_demora.
     */
    public void setSitio_demora(java.lang.String sitio_demora) {
        this.sitio_demora = sitio_demora;
    }
    
    /**
     * Getter for property ultima_modificacion.
     * @return Value of property ultima_modificacion.
     */
    public java.lang.String getUltima_modificacion() {
        return ultima_modificacion;
    }
    
    /**
     * Setter for property ultima_modificacion.
     * @param ultima_modificacion New value of property ultima_modificacion.
     */
    public void setUltima_modificacion(java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property usuario_finalizacion.
     * @return Value of property usuario_finalizacion.
     */
    public java.lang.String getUsuario_finalizacion() {
        return usuario_finalizacion;
    }
    
    /**
     * Setter for property usuario_finalizacion.
     * @param usuario_finalizacion New value of property usuario_finalizacion.
     */
    public void setUsuario_finalizacion(java.lang.String usuario_finalizacion) {
        this.usuario_finalizacion = usuario_finalizacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property duracion.
     * @return Value of property duracion.
     */
    public java.lang.Integer getDuracion() {
        return duracion;
    }
    
    /**
     * Setter for property duracion.
     * @param duracion New value of property duracion.
     */
    public void setDuracion(java.lang.Integer duracion) {
        this.duracion = duracion;
    }
    
    /**
     * Getter for property codigo_jerarquia.
     * @return Value of property codigo_jerarquia.
     */
    public java.lang.String getCodigo_jerarquia() {
        return codigo_jerarquia;
    }
    
    /**
     * Setter for property codigo_jerarquia.
     * @param codigo_jerarquia New value of property codigo_jerarquia.
     */
    public void setCodigo_jerarquia(java.lang.String codigo_jerarquia) {
        this.codigo_jerarquia = codigo_jerarquia;
    }
    
    public String tiempoRestante(){
        Calendar fechaActual = Calendar.getInstance();             
        Calendar fechaDem = Util.crearCalendar(this.fecha_demora);
        ////System.out.println("demora: "+fechaDem);
        long f1ToMilis = fechaActual.getTimeInMillis() ;
        long f2ToMilis = fechaDem.getTimeInMillis();
        ////System.out.println("horas: "+(f1ToMilis - f2ToMilis)/(3600000));
        f2ToMilis += this.duracion.intValue() * 3600000;
        /*Calendar res = Calendar.getInstance();
        res.setTimeInMillis(f1ToMilis - f2ToMilis);
        long horas = res.getTimeInMillis()/(3600000);*/
        long horas = (f2ToMilis - f1ToMilis)/(3600000);
        return String.valueOf(horas);
        //return String.valueOf(fechaActual.getTimeInMillis());
        
    }
    
    /**
     * Getter for property id.
     * @return Value of property id.
     */
    public java.lang.Integer getId() {
            return id;
    }
    
    /**
     * Setter for property id.
     * @param id New value of property id.
     */
    public void setId(java.lang.Integer id) {
            this.id = id;
    }
    
    /**
     * Getter for property fecha_fin_demora.
     * @return Value of property fecha_fin_demora.
     */
    public java.lang.String getFecha_fin_demora() {
            return fecha_fin_demora;
    }
    
    /**
     * Setter for property fecha_fin_demora.
     * @param fecha_fin_demora New value of property fecha_fin_demora.
     */
    public void setFecha_fin_demora(java.lang.String fecha_fin_demora) {
            this.fecha_fin_demora = fecha_fin_demora;
    }

    
}
