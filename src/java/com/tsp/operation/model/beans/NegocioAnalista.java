/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 * Bean para la tabla negocios_trazabilidad<br/>
 * 25/11/2011
 * @author ivargas-Geotech
 */
public class NegocioAnalista {

    private String regStatus;
    private String dstrct;
    private String analista;
    private String fecha;
    private String codNeg;
    private int secuencia;



    public String getAnalista() {
        return analista;
    }

    public void setAnalista(String analista) {
        this.analista = analista;
    }

    public int getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }

    public String getCodNeg() {
        return codNeg;
    }

    public void setCodNeg(String codNeg) {
        this.codNeg = codNeg;
    }

 
    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    
}
