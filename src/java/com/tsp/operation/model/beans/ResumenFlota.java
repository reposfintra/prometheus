/***********************************************************************************
 * Nombre clase : ............... ResumenFlota.java                                *
 * Descripcion :................. Clase que maneja los atributos para el reporte   *
 *                                de Resumen de Flotas Utiliadas                   *
 *                                campos de la tabla flota_utilizada               *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 9 de diciembre de 2005, 08:00 AM                 *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.beans;

public class ResumenFlota {
    
    private String agencia;
    private String vehDisponibles;
    private String vehUtilizados;
    private String remesasDesp;
    private String perUtilidad;
    private String perRetorno;
    private double total;
    
    public ResumenFlota() {
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property perRetorno.
     * @return Value of property perRetorno.
     */
    public java.lang.String getPerRetorno() {
        return perRetorno;
    }
    
    /**
     * Setter for property perRetorno.
     * @param perRetorno New value of property perRetorno.
     */
    public void setPerRetorno(java.lang.String perRetorno) {
        this.perRetorno = perRetorno;
    }
    
    /**
     * Getter for property perUtilidad.
     * @return Value of property perUtilidad.
     */
    public java.lang.String getPerUtilidad() {
        return perUtilidad;
    }
    
    /**
     * Setter for property perUtilidad.
     * @param perUtilidad New value of property perUtilidad.
     */
    public void setPerUtilidad(java.lang.String perUtilidad) {
        this.perUtilidad = perUtilidad;
    }
    
    /**
     * Getter for property remesasDesp.
     * @return Value of property remesasDesp.
     */
    public java.lang.String getRemesasDesp() {
        return remesasDesp;
    }
    
    /**
     * Setter for property remesasDesp.
     * @param remesasDesp New value of property remesasDesp.
     */
    public void setRemesasDesp(java.lang.String remesasDesp) {
        this.remesasDesp = remesasDesp;
    }
    
    /**
     * Getter for property vehDisponibles.
     * @return Value of property vehDisponibles.
     */
    public java.lang.String getVehDisponibles() {
        return vehDisponibles;
    }
    
    /**
     * Setter for property vehDisponibles.
     * @param vehDisponibles New value of property vehDisponibles.
     */
    public void setVehDisponibles(java.lang.String vehDisponibles) {
        this.vehDisponibles = vehDisponibles;
    }
    
    /**
     * Getter for property vehUtilizados.
     * @return Value of property vehUtilizados.
     */
    public java.lang.String getVehUtilizados() {
        return vehUtilizados;
    }
    
    /**
     * Setter for property vehUtilizados.
     * @param vehUtilizados New value of property vehUtilizados.
     */
    public void setVehUtilizados(java.lang.String vehUtilizados) {
        this.vehUtilizados = vehUtilizados;
    }
    
    /**
     * Getter for property total.
     * @return Value of property total.
     */
    public double getTotal() {
        return total;
    }
    
    /**
     * Setter for property total.
     * @param total New value of property total.
     */
    public void setTotal(double total) {
        this.total = total;
    }
    
}
