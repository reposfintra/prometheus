/*
 * Propietario.java
 *
 * Created on 14 de junio de 2005, 10:01 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  sandrameg
 */
public class Propietario {
    
    public String dstrct;
    public String cedula;
    public String p_nombre;
    public String s_nombre;
    public String p_apellido;
    public String s_apellido;
    public String creation_user;
    public String base;
    public String reg_status;
    public String user_update;
    //hojavida
    private String expedicionced;
    private String celular;
    private String telefono;
    private String direccion;
    private String sedepago;
    private String grado_riesgo; 
    private String ciudad;
    
    //Captaciones
    private String frecuencia;
    private double tasa_captacion;
    private double capital;
    private String inicio;
    private double tasa_capital;
    private String frecuencia_capital;
    private String tipo_doc;
    private String documento;
    private String perfil="";
    private int id=0;



    
    //fenalco
    
    private double tasa_fenalco;
    private double custodiacheque;
    private double rem;
    
    
    
    //////gets
    public String getdstrct(){
        return dstrct;
    }
    
    public String getCedula(){
        return cedula;
    }
    
    public String getP_nombre(){
        return p_nombre;
    }
    
    public String getS_nombre(){
        return s_nombre;
    }
    
    public String getP_apellido(){
        return p_apellido;
    }
    
    public String getS_apellido(){
        return s_apellido;
    }
    
    public String getBase(){
        return base;
    }
    
    public String getCreation_user(){
        return creation_user;
    }
    
    public String getUser_update(){
        return user_update;
    }
    
    ///////sets
    public void setdstrct(String dstrct){
        this.dstrct=dstrct;
    }
    
    public void setCedula(String ced){
        this.cedula = ced;
    }
    public void setP_nombre(String pn){
        this.p_nombre = pn;
    }
    
    public void setS_nombre(String sn){
        this.s_nombre = sn;
    }
    
    public void setP_apellido(String pa){
        this.p_apellido = pa;
    }

    public void setS_apellido(String sa){
        this.s_apellido = sa;
    }
    
    public void setBase(String base){
        this.base = base;
    }
    
    public void setCreation_user(String user){
        this.creation_user = user;
    }
    
    public void setUser_update(String user){
        this.user_update = user;
    }
    
    /**
     * Getter for property expedicionced.
     * @return Value of property expedicionced.
     */
    public java.lang.String getExpedicionced() {
        return expedicionced;
    }
    
    /**
     * Setter for property expedicionced.
     * @param expedicionced New value of property expedicionced.
     */
    public void setExpedicionced(java.lang.String expedicionced) {
        this.expedicionced = expedicionced;
    }
    
    /**
     * Getter for property celular.
     * @return Value of property celular.
     */
    public java.lang.String getCelular() {
        return celular;
    }
    
    /**
     * Setter for property celular.
     * @param celular New value of property celular.
     */
    public void setCelular(java.lang.String celular) {
        this.celular = celular;
    }
    
    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public java.lang.String getTelefono() {
        return telefono;
    }
    
    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }
    
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter for property sedepago.
     * @return Value of property sedepago.
     */
    public java.lang.String getSedepago() {
        return sedepago;
    }
    
    /**
     * Setter for property sedepago.
     * @param sedepago New value of property sedepago.
     */
    public void setSedepago(java.lang.String sedepago) {
        this.sedepago = sedepago;
    }
    
    /**
     * Getter for property grado_riesgo.
     * @return Value of property grado_riesgo.
     */
    public java.lang.String getGrado_riesgo() {
        return grado_riesgo;
    }
    
    /**
     * Setter for property grado_riesgo.
     * @param grado_riesgo New value of property grado_riesgo.
     */
    public void setGrado_riesgo(java.lang.String grado_riesgo) {
        this.grado_riesgo = grado_riesgo;
    }
    
    /**
     * Getter for property ciudad.
     * @return Value of property ciudad.
     */
    public java.lang.String getCiudad() {
        return ciudad;
    }
    
    /**
     * Setter for property ciudad.
     * @param ciudad New value of property ciudad.
     */
    public void setCiudad(java.lang.String ciudad) {
        this.ciudad = ciudad;
    }
    
    /**
     * Getter for property frecuencia.
     * @return Value of property frecuencia.
     */
    public java.lang.String getFrecuencia() {
        return frecuencia;
    }
    
    /**
     * Setter for property frecuencia.
     * @param frecuencia New value of property frecuencia.
     */
    public void setFrecuencia(java.lang.String frecuencia) {
        this.frecuencia = frecuencia;
    }
    
    /**
     * Getter for property tasa_captacion.
     * @return Value of property tasa_captacion.
     */
    public double getTasa_captacion() {
        return tasa_captacion;
    }
    
    /**
     * Setter for property tasa_captacion.
     * @param tasa_captacion New value of property tasa_captacion.
     */
    public void setTasa_captacion(double tasa_captacion) {
        this.tasa_captacion = tasa_captacion;
    }
    
    /**
     * Getter for property capital.
     * @return Value of property capital.
     */
    public double getCapital() {
        return capital;
    }
    
    /**
     * Setter for property capital.
     * @param capital New value of property capital.
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }
    
    /**
     * Getter for property inicio.
     * @return Value of property inicio.
     */
    public java.lang.String getInicio() {
        return inicio;
    }
    
    /**
     * Setter for property inicio.
     * @param inicio New value of property inicio.
     */
    public void setInicio(java.lang.String inicio) {
        this.inicio = inicio;
    }
    
    /**
     * Getter for property tasa_capital.
     * @return Value of property tasa_capital.
     */
    public double getTasa_capital() {
        return tasa_capital;
    }
    
    /**
     * Setter for property tasa_capital.
     * @param tasa_capital New value of property tasa_capital.
     */
    public void setTasa_capital(double tasa_capital) {
        this.tasa_capital = tasa_capital;
    }
    
    /**
     * Getter for property frecuencia_capital.
     * @return Value of property frecuencia_capital.
     */
    public java.lang.String getFrecuencia_capital() {
        return frecuencia_capital;
    }
    
    /**
     * Setter for property frecuencia_capital.
     * @param frecuencia_capital New value of property frecuencia_capital.
     */
    public void setFrecuencia_capital(java.lang.String frecuencia_capital) {
        this.frecuencia_capital = frecuencia_capital;
    }
    
    /**
     * Getter for property tipo_doc.
     * @return Value of property tipo_doc.
     */
    public java.lang.String getTipo_doc() {
        return tipo_doc;
    }
    
    /**
     * Setter for property tipo_doc.
     * @param tipo_doc New value of property tipo_doc.
     */
    public void setTipo_doc(java.lang.String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property tasa_fenalco.
     * @return Value of property tasa_fenalco.
     */
    public double getTasa_fenalco() {
        return tasa_fenalco;
    }
    
    /**
     * Setter for property tasa_fenalco.
     * @param tasa_fenalco New value of property tasa_fenalco.
     */
    public void setTasa_fenalco(double tasa_fenalco) {
        this.tasa_fenalco = tasa_fenalco;
    }
    
    /**
     * Getter for property custodiacheque.
     * @return Value of property custodiacheque.
     */
    public double getCustodiacheque() {
        return custodiacheque;
    }
    
    /**
     * Setter for property custodiacheque.
     * @param custodiacheque New value of property custodiacheque.
     */
    public void setCustodiacheque(double custodiacheque) {
        this.custodiacheque = custodiacheque;
    }
    
    /**
     * Getter for property rem.
     * @return Value of property rem.
     */
    public double getRem() {
        return rem;
    }
    
    /**
     * Setter for property rem.
     * @param rem New value of property rem.
     */
    public void setRem(double rem) {
        this.rem = rem;
    }


    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
