/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author Egonzalez
 */
public class ComponentesCosulta {
    
    private int id_consulta;
    private String tipo_componente;
    private String parametro;

    public ComponentesCosulta() {
    }

    /**
     * @return the id_consulta
     */
    public int getId_consulta() {
        return id_consulta;
    }

    /**
     * @param id_consulta the id_consulta to set
     */
    public void setId_consulta(int id_consulta) {
        this.id_consulta = id_consulta;
    }

    /**
     * @return the tipo_componente
     */
    public String getTipo_componente() {
        return tipo_componente;
    }

    /**
     * @param tipo_componente the tipo_componente to set
     */
    public void setTipo_componente(String tipo_componente) {
        this.tipo_componente = tipo_componente;
    }

    /**
     * @return the parametro
     */
    public String getParametro() {
        return parametro;
    }

    /**
     * @param parametro the parametro to set
     */
    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

   
    
    
}
