/*
 * ConsultaSQL.java
 *
 * Created on 21 de abril de 2005, 09:29 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 * Este javabean representa una consulta SQL guardada en la base de datos para
 * su posterior ejecuci�n. El objetivo de esta funcionalidad es tener una plantilla
 * para generar reportes sin tener que realizar un desarrollo nuevo.
 * @author  Ivan Herazo
 */
public class ConsultaSQL implements Serializable {
  private String recordId;
  private String query;
  private String usuarioAsociado;
  private String descripcion;
  private String creador;
  private String fechaCreacion;
  private String baseDeDatos;
  private String[] nombresParametros;
  private String[] nombresCampos;
  private Properties valoresCampos;
  private int conteoColumnas;
  
  /** Crea una nueva instancia de esta clase */
  public ConsultaSQL()
  {
    this.recordId = "";
    this.query = "";
    this.usuarioAsociado = "";
    this.descripcion = "";
    this.creador = "";
    this.fechaCreacion = "";
    this.baseDeDatos = "";
    this.nombresCampos = null;
    this.nombresParametros = null;
    this.valoresCampos = null;
    this.conteoColumnas = 0;
  }
  
  /**
   * Getter for property oid.
   * @return Value of property oid.
   */
  public String getRecordId() {
    return this.recordId;
  }
  
  /**
   * Setter for property oid.
   * @param oid New value of property oid.
   */
  public void setRecordId(String recordId) {
    this.recordId = recordId;
  }
  
  /**
   * Getter for property query.
   * @return Value of property query.
   */
  public String getQuery() {
    return this.query;
  }
  
  /**
   * Setter for property query.
   * @param query New value of property query.
   */
  public void setQuery(String query) {
    this.query = query;
  }
  
  /**
   * Getter for property usuarioAsociado.
   * @return Value of property usuarioAsociado.
   */
  public String getUsuarioAsociado() {
    return this.usuarioAsociado;
  }
  
  /**
   * Setter for property usuarioAsociado.
   * @param usuarioAsociado New value of property usuarioAsociado.
   */
  public void setUsuarioAsociado(String usuarioAsociado) {
    this.usuarioAsociado = usuarioAsociado;
  }
  
  /**
   * Getter for property descripcion.
   * @return Value of property descripcion.
   */
  public String getDescripcion() {
    return this.descripcion;
  }
  
  /**
   * Setter for property descripcion.
   * @param descripcion New value of property descripcion.
   */
  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }
  
  /**
   * Getter for property database.
   * @return Value of property database.
   */
  public String getBaseDeDatos() {
    return this.baseDeDatos;
  }
  
  /**
   * Setter for property database.
   * @param database New value of property database.
   */
  public void setBaseDeDatos(String baseDeDatos) {
    this.baseDeDatos = baseDeDatos;
  }
  
  /**
   * Indexed getter for property nombresParametros.
   * @param index Index of the property.
   * @return Value of the property at <CODE>index</CODE>.
   */
  public String getNombresParametros(int index) {
    return ( this.nombresParametros != null ?
      this.nombresParametros[index] : null
    );
  }
  
  /**
   * Getter for property nombresParametros.
   * @return Value of property nombresParametros.
   */
  public String[] getNombresParametros() {
    return this.nombresParametros;
  }
  
  /**
   * Indexed setter for property nombresParametros.
   * @param index Index of the property.
   * @param nombresParametros New value of the property at <CODE>index</CODE>.
   */
  public void setNombresParametros(int index, String nombresParametros) {
    if( this.nombresParametros != null )
      this.nombresParametros[index] = nombresParametros;
  }
  
  /**
   * Setter for property nombresParametros.
   * @param nombresParametros New value of property nombresParametros.
   */
  public void setNombresParametros(String nombresParams) {
    this.nombresParametros = ( nombresParams.equals("") ?
      null : nombresParams.split(",")
    );
  }
  
  /**
   * Getter for property creador.
   * @return Value of property creador.
   */
  public String getCreador() {
    return this.creador;
  }
  
  /**
   * Setter for property creador.
   * @param creador New value of property creador.
   */
  public void setCreador(String creador) {
    this.creador = creador;
  }
  
  /**
   * Getter for property fechaCreacion.
   * @return Value of property fechaCreacion.
   */
  public String getFechaCreacion() {
    return this.fechaCreacion;
  }
  
  /**
   * Setter for property fechaCreacion.
   * @param fechaCreacion New value of property fechaCreacion.
   */
  public void setFechaCreacion(String fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }
  
  /**
   * Getter for property nombresColumnas.
   * @return Value of property nombresColumnas.
   */
  public String[] getNombresCampos() {
    return this.nombresCampos;
  }
  
  /**
   * Setter for property nombresColumnas.
   * @param fieldNames New value of property nombresColumnas.
   */
  public void setNombresCampos(String[] fieldNames) {
    this.valoresCampos = new Properties();
    this.nombresCampos = fieldNames;
  }
  
  /**
   * Retorna el valor de un campo.
   * @param nombreCampo Nombre del campo.
   * @return Valor del campo.
   */
  public String getValorCampo(final String nombreCampo) {
    String fieldValue = this.valoresCampos.getProperty(nombreCampo);
    return (fieldValue == null ? "" : fieldValue);
  }
  
  /**
   * Asigna el valor de un campo.
   * @param nombreCampo Nombre del campo.
   * @param valorCampo Valor del campo.
   */
  public void setValorCampo(String nombreCampo, String valorCampo) {
    this.valoresCampos.setProperty(nombreCampo, valorCampo);
  }
  
  /**
   * Retorna el n�mero de columnas que tiene el query almacenado en la base
   * de datos. Solo funciona cuando se cargan los resultados del query al
   * ejecutarse (m�todo {@link #loadExecutionResults(java.sql.ResultSet) loadExecutionResults()}).
   * @return N�mero de columnas del query ejecutado.
   */
  public int getConteoColumnas() {
    return this.conteoColumnas;
  }
  
  /**
   * Asigna el n�mero de columnas que tiene el query almacenado en la base
   * de datos. Solo deber�a invocarse cuando se cargan los resultados del query al
   * ejecutarse (m�todo {@link #loadExecutionResults(java.sql.ResultSet) loadExecutionResults()}).
   * @param colCount de columnas del query ejecutado.
   */
  public void setConteoColumnas(int colCount) {
    this.conteoColumnas = colCount;
  }
  
  /**
   * Metodo factor�a que carga en memoria los datos de un registro
   * extra�do de la base de datos.
   * @param rset Registro a cargar.
   * @return Objeto con los datos cargados en memoria.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   */
  public static ConsultaSQL load(final ResultSet rset) throws SQLException {
    String value = null;
    ConsultaSQL sql = new ConsultaSQL();
    value = rset.getString("id");
    if( !rset.wasNull() ) sql.setRecordId(value);
    
    value = rset.getString("usuario");
    if( !rset.wasNull() ) sql.setUsuarioAsociado(value);
    
    value = rset.getString("query");
    if( !rset.wasNull() ) sql.setQuery(value);
    
    value = rset.getString("descripcion");
    if( !rset.wasNull() ) sql.setDescripcion(value);
    
    value = rset.getString("nombresparams");
    if( !rset.wasNull() ) sql.setNombresParametros(value);
    
    value = rset.getString("creador");
    if( !rset.wasNull() ) sql.setCreador(value);
    
    value = rset.getString("fechacreacion");
    if( !rset.wasNull() ) sql.setFechaCreacion(value);
    
    value = rset.getString("basededatos");
    if( !rset.wasNull() ) sql.setBaseDeDatos(value);
    return sql;
  }
  
  /**
   * Metodo factor�a que carga en memoria los datos de un registro del query
   * representado por este objeto, cuando es ejecutado.
   * @param rset Registro a cargar.
   * @return Objeto con los datos cargados en memoria.
   * @throws SQLException Si un error de acceso a la base de datos ocurre.
   */
  public static ConsultaSQL loadExecutionResults(final ResultSet rset)
  throws SQLException {
    int idx = 0;
    ConsultaSQL sql = new ConsultaSQL();
    ResultSetMetaData rsmd = rset.getMetaData();
    int sqlColCount = rsmd.getColumnCount();
    sql.setConteoColumnas( sqlColCount );
    String [] columns = new String[sqlColCount];
    String [] values  = new String[sqlColCount];
    for( idx = 0; idx < columns.length; idx++ )
    {
      columns[idx] = rsmd.getColumnLabel(idx + 1).trim().toUpperCase().replace("_", " ");
      values[idx]  = rset.getString(idx + 1);
      values[idx]  = (rset.wasNull() ? "" : values[idx].trim());
    }
    sql.setNombresCampos( columns );
    for( idx = 0; idx < columns.length; idx++ )
      sql.setValorCampo( columns[idx], values[idx] );
    return sql;
  }
  
  public List<String> getTokensOrdenados()
  {
    List<String> paramTokensList = new LinkedList<String>();
    if( !this.query.equals("") )
    {
      String queryCopy = new String(this.query.toLowerCase().trim());
      while( !queryCopy.equals("") )
      {
        int queryLength     = queryCopy.length();
        int paramTokenIdx   = queryCopy.indexOf("?");
        int paramInTokenIdx = queryCopy.indexOf("<param-in>");
        if( (paramTokenIdx >= 0 && paramInTokenIdx >= 0 &&
             paramTokenIdx < paramInTokenIdx) ||
            (paramTokenIdx >= 0 && paramInTokenIdx < 0) )
        {
          paramTokensList.add("<param>");
          queryCopy = ( queryLength == paramTokenIdx ?
            "" : queryCopy.substring(paramTokenIdx + 1, queryLength)
          );
        }else if( (paramTokenIdx >= 0 && paramInTokenIdx >= 0 &&
                   paramTokenIdx > paramInTokenIdx) ||
                  (paramTokenIdx < 0 && paramInTokenIdx >= 0) ){
          paramTokensList.add("<param-in>");
          queryCopy = ( queryLength == paramInTokenIdx ?
            "" : queryCopy.substring(paramInTokenIdx + 1, queryLength)
          );
        }else
          queryCopy = "";
      }
    }
    return paramTokensList;
  }
}
