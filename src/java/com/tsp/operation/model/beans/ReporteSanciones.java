/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author lcanchila
 */
public class ReporteSanciones {
    
    private String unidad_negocio;
    private String periodo_foto;
    private String cod_negocio;
    private String cedula;
    private String nombre_cliente;
    private String vencimiento_mayor;
    private String fecha_pago_ingreso;
    private String fecha_vencimiento_mayor;
    private String diferencia_pago;
    private double valor_saldo_foto;
    private double interes_mora;
    private double gasto_cobranza;
    private String num_ingreso;
    private double valor_ingreso;
    private double valor_cxc_ingreso;
    private double valor_ixm_ingreso;
    private double valor_gac_ingreso;
    private String cuentas_ixm;
    private String cuentas_gac;
    private String cuenta1;
    private String cuenta2;
    private String cuenta3;
    private String cuenta4;
    private String cuenta5;
    private String cuenta6;

    /**
     * @return the unidad_negocio
     */
    public String getUnidad_negocio() {
        return unidad_negocio;
    }

    /**
     * @param unidad_negocio the unidad_negocio to set
     */
    public void setUnidad_negocio(String unidad_negocio) {
        this.unidad_negocio = unidad_negocio;
    }

    /**
     * @return the periodo_foto
     */
    public String getPeriodo_foto() {
        return periodo_foto;
    }

    /**
     * @param periodo_foto the periodo_foto to set
     */
    public void setPeriodo_foto(String periodo_foto) {
        this.periodo_foto = periodo_foto;
    }

    /**
     * @return the cod_negocio
     */
    public String getCod_negocio() {
        return cod_negocio;
    }

    /**
     * @param cod_negocio the cod_negocio to set
     */
    public void setCod_negocio(String cod_negocio) {
        this.cod_negocio = cod_negocio;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre_cliente
     */
    public String getNombre_cliente() {
        return nombre_cliente;
    }

    /**
     * @param nombre_cliente the nombre_cliente to set
     */
    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    /**
     * @return the vencimiento_mayor
     */
    public String getVencimiento_mayor() {
        return vencimiento_mayor;
    }

    /**
     * @param vencimiento_mayor the vencimiento_mayor to set
     */
    public void setVencimiento_mayor(String vencimiento_mayor) {
        this.vencimiento_mayor = vencimiento_mayor;
    }

    /**
     * @return the fecha_pago_ingreso
     */
    public String getFecha_pago_ingreso() {
        return fecha_pago_ingreso;
    }

    /**
     * @param fecha_pago_ingreso the fecha_pago_ingreso to set
     */
    public void setFecha_pago_ingreso(String fecha_pago_ingreso) {
        this.fecha_pago_ingreso = fecha_pago_ingreso;
    }

    /**
     * @return the fecha_vencimiento_mayor
     */
    public String getFecha_vencimiento_mayor() {
        return fecha_vencimiento_mayor;
    }

    /**
     * @param fecha_vencimiento_mayor the fecha_vencimiento_mayor to set
     */
    public void setFecha_vencimiento_mayor(String fecha_vencimiento_mayor) {
        this.fecha_vencimiento_mayor = fecha_vencimiento_mayor;
    }

    /**
     * @return the diferencia_pago
     */
    public String getDiferencia_pago() {
        return diferencia_pago;
    }

    /**
     * @param diferencia_pago the diferencia_pago to set
     */
    public void setDiferencia_pago(String diferencia_pago) {
        this.diferencia_pago = diferencia_pago;
    }

    /**
     * @return the valor_saldo_foto
     */
    public double getValor_saldo_foto() {
        return valor_saldo_foto;
    }

    /**
     * @param valor_saldo_foto the valor_saldo_foto to set
     */
    public void setValor_saldo_foto(double valor_saldo_foto) {
        this.valor_saldo_foto = valor_saldo_foto;
    }

    /**
     * @return the interes_mora
     */
    public double getInteres_mora() {
        return interes_mora;
    }

    /**
     * @param interes_mora the interes_mora to set
     */
    public void setInteres_mora(double interes_mora) {
        this.interes_mora = interes_mora;
    }

    /**
     * @return the gasto_cobranza
     */
    public double getGasto_cobranza() {
        return gasto_cobranza;
    }

    /**
     * @param gasto_cobranza the gasto_cobranza to set
     */
    public void setGasto_cobranza(double gasto_cobranza) {
        this.gasto_cobranza = gasto_cobranza;
    }

    /**
     * @return the num_ingreso
     */
    public String getNum_ingreso() {
        return num_ingreso;
    }

    /**
     * @param num_ingreso the num_ingreso to set
     */
    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    /**
     * @return the valor_ingreso
     */
    public double getValor_ingreso() {
        return valor_ingreso;
    }

    /**
     * @param valor_ingreso the valor_ingreso to set
     */
    public void setValor_ingreso(double valor_ingreso) {
        this.valor_ingreso = valor_ingreso;
    }

    /**
     * @return the valor_cxc_ingreso
     */
    public double getValor_cxc_ingreso() {
        return valor_cxc_ingreso;
    }

    /**
     * @param valor_cxc_ingreso the valor_cxc_ingreso to set
     */
    public void setValor_cxc_ingreso(double valor_cxc_ingreso) {
        this.valor_cxc_ingreso = valor_cxc_ingreso;
    }

    /**
     * @return the valor_ixm_ingreso
     */
    public double getValor_ixm_ingreso() {
        return valor_ixm_ingreso;
    }

    /**
     * @param valor_ixm_ingreso the valor_ixm_ingreso to set
     */
    public void setValor_ixm_ingreso(double valor_ixm_ingreso) {
        this.valor_ixm_ingreso = valor_ixm_ingreso;
    }

    /**
     * @return the valor_gac_ingreso
     */
    public double getValor_gac_ingreso() {
        return valor_gac_ingreso;
    }

    /**
     * @param valor_gac_ingreso the valor_gac_ingreso to set
     */
    public void setValor_gac_ingreso(double valor_gac_ingreso) {
        this.valor_gac_ingreso = valor_gac_ingreso;
    }

    /**
     * @return the cuentas_ixm
     */
    public String getCuentas_ixm() {
        return cuentas_ixm;
    }

    /**
     * @param cuentas_ixm the cuentas_ixm to set
     */
    public void setCuentas_ixm(String cuentas_ixm) {
        this.cuentas_ixm = cuentas_ixm;
    }

    /**
     * @return the cuentas_gac
     */
    public String getCuentas_gac() {
        return cuentas_gac;
    }

    /**
     * @param cuentas_gac the cuentas_gac to set
     */
    public void setCuentas_gac(String cuentas_gac) {
        this.cuentas_gac = cuentas_gac;
    }

    /**
     * @return the cuenta1
     */
    public String getCuenta1() {
        return cuenta1;
    }

    /**
     * @param cuenta1 the cuenta1 to set
     */
    public void setCuenta1(String cuenta1) {
        this.cuenta1 = cuenta1;
    }

    /**
     * @return the cuenta2
     */
    public String getCuenta2() {
        return cuenta2;
    }

    /**
     * @param cuenta2 the cuenta2 to set
     */
    public void setCuenta2(String cuenta2) {
        this.cuenta2 = cuenta2;
    }

    /**
     * @return the cuenta3
     */
    public String getCuenta3() {
        return cuenta3;
    }

    /**
     * @param cuenta3 the cuenta3 to set
     */
    public void setCuenta3(String cuenta3) {
        this.cuenta3 = cuenta3;
    }

    /**
     * @return the cuenta4
     */
    public String getCuenta4() {
        return cuenta4;
    }

    /**
     * @param cuenta4 the cuenta4 to set
     */
    public void setCuenta4(String cuenta4) {
        this.cuenta4 = cuenta4;
    }

    /**
     * @return the cuenta5
     */
    public String getCuenta5() {
        return cuenta5;
    }

    /**
     * @param cuenta5 the cuenta5 to set
     */
    public void setCuenta5(String cuenta5) {
        this.cuenta5 = cuenta5;
    }

    /**
     * @return the cuenta6
     */
    public String getCuenta6() {
        return cuenta6;
    }

    /**
     * @param cuenta6 the cuenta6 to set
     */
    public void setCuenta6(String cuenta6) {
        this.cuenta6 = cuenta6;
    }
}
