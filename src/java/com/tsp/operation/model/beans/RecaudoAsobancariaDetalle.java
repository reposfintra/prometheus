/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 * Bean para manejar detalle subida archivo recaudo Asobancaria<br/>
 * 30/03/2015
 * @author mcastillo
 */
public class RecaudoAsobancariaDetalle {

    private int id;
    private int id_recaudo;
    private String cod_servicio_rec;
    private int numero_lote;
    private String referencia_factura;
    private double valor_recaudado;
    private String procedencia_pago;
    private String medio_pago;
    private String num_operacion;
    private String num_autorizacion;
    private int cod_entidad_debitada;
    private String cod_sucursal;
    private int secuencia;
    private String causal_devolucion;
    private String encontrado;
    private String procesado_cartera;
    private String causal_devolucion_proc;
    private String negocio;
    private String segunda_referencia_usuario;
    private int tipo_recaudo;
    private String fecha_real_recaudo;
    private String hora_real_recaudo;
    private double valor_recaudo_canje;
    private String numero_cheque;
    private String cod_compensacion_cheque;
    

    public RecaudoAsobancariaDetalle() {
    }
    
     /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_recaudo
     */
    public int getId_recaudo() {
        return id_recaudo;
    }

    /**
     * @param id_recaudo the id_recaudo to set
     */
    public void setId_recaudo(int id_recaudo) {
        this.id_recaudo = id_recaudo;
    }
    
        /**
     * @return the cod_servicio_rec
     */
    public String getCod_servicio_rec() {
        return cod_servicio_rec;
    }

    /**
     * @param cod_servicio_rec the cod_servicio_rec to set
     */
    public void setCod_servicio_rec(String cod_servicio_rec) {
        this.cod_servicio_rec = cod_servicio_rec;
    }

    /**
     * @return the numero_lote
     */
    public int getNumero_lote() {
        return numero_lote;
    }

    /**
     * @param numero_lote the numero_lote to set
     */
    public void setNumero_lote(int numero_lote) {
        this.numero_lote = numero_lote;
    }

    /**
     * @return the referencia_factura
     */
    public String getReferencia_factura() {
        return referencia_factura;
    }

    /**
     * @param referencia_factura the referencia_factura to set
     */
    public void setReferencia_factura(String referencia_factura) {
        this.referencia_factura = referencia_factura;
    }

    /**
     * @return the valor_recaudado
     */
    public double getValor_recaudado() {
        return valor_recaudado;
    }

    /**
     * @param valor_recaudado the valor_recaudado to set
     */
    public void setValor_recaudado(double valor_recaudado) {
        this.valor_recaudado = valor_recaudado;
    }

    /**
     * @return the procedencia_pago
     */
    public String getProcedencia_pago() {
        return procedencia_pago;
    }

    /**
     * @param procedencia_pago the procedencia_pago to set
     */
    public void setProcedencia_pago(String procedencia_pago) {
        this.procedencia_pago = procedencia_pago;
    }

    /**
     * @return the medio_pago
     */
    public String getMedio_pago() {
        return medio_pago;
    }

    /**
     * @param medio_pago the medio_pago to set
     */
    public void setMedio_pago(String medio_pago) {
        this.medio_pago = medio_pago;
    }

    /**
     * @return the num_operacion
     */
    public String getNum_operacion() {
        return num_operacion;
    }

    /**
     * @param num_operacion the num_operacion to set
     */
    public void setNum_operacion(String num_operacion) {
        this.num_operacion = num_operacion;
    }

    /**
     * @return the num_autorizacion
     */
    public String getNum_autorizacion() {
        return num_autorizacion;
    }

    /**
     * @param num_autorizacion the num_autorizacion to set
     */
    public void setNum_autorizacion(String num_autorizacion) {
        this.num_autorizacion = num_autorizacion;
    }

    /**
     * @return the cod_entidad_debitada
     */
    public int getCod_entidad_debitada() {
        return cod_entidad_debitada;
    }

    /**
     * @param cod_entidad_debitada the cod_entidad_debitada to set
     */
    public void setCod_entidad_debitada(int cod_entidad_debitada) {
        this.cod_entidad_debitada = cod_entidad_debitada;
    }

    /**
     * @return the cod_sucursal
     */
    public String getCod_sucursal() {
        return cod_sucursal;
    }

    /**
     * @param cod_sucursal the cod_sucursal to set
     */
    public void setCod_sucursal(String cod_sucursal) {
        this.cod_sucursal = cod_sucursal;
    }

    /**
     * @return the secuencia
     */
    public int getSecuencia() {
        return secuencia;
    }

    /**
     * @param secuencia the secuencia to set
     */
    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * @return the causal_devolucion
     */
    public String getCausal_devolucion() {
        return causal_devolucion;
    }

    /**
     * @param causal_devolucion the causal_devolucion to set
     */
    public void setCausal_devolucion(String causal_devolucion) {
        this.causal_devolucion = causal_devolucion;
    }  

    /**
     * @return the encontrado
     */
    public String getEncontrado() {
        return encontrado;
    }

    /**
     * @param encontrado the encontrado to set
     */
    public void setEncontrado(String encontrado) {
        this.encontrado = encontrado;
    }

    /**
     * @return the procesado_cartera
     */
    public String getProcesado_cartera() {
        return procesado_cartera;
    }

    /**
     * @param procesado_cartera the procesado_cartera to set
     */
    public void setProcesado_cartera(String procesado_cartera) {
        this.procesado_cartera = procesado_cartera;
    }

    /**
     * @return the causal_devolucion_proc
     */
    public String getCausal_devolucion_proc() {
        return causal_devolucion_proc;
    }

    /**
     * @param causal_devolucion_proc the causal_devolucion_proc to set
     */
    public void setCausal_devolucion_proc(String causal_devolucion_proc) {
        this.causal_devolucion_proc = causal_devolucion_proc;
    }

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getSegunda_referencia_usuario() {
        return segunda_referencia_usuario;
    }

    public void setSegunda_referencia_usuario(String segunda_referencia_usuario) {
        this.segunda_referencia_usuario = segunda_referencia_usuario;
    }

    

    public int getTipo_recaudo() {
        return tipo_recaudo;
    }

    public void setTipo_recaudo(int tipo_recaudo) {
        this.tipo_recaudo = tipo_recaudo;
    }

    public String getFecha_real_recaudo() {
        return fecha_real_recaudo;
    }

    public void setFecha_real_recaudo(String fecha_real_recaudo) {
        this.fecha_real_recaudo = fecha_real_recaudo;
    }

    public String getHora_real_recaudo() {
        return hora_real_recaudo;
    }

    public void setHora_real_recaudo(String hora_real_recaudo) {
        this.hora_real_recaudo = hora_real_recaudo;
    }

    public double getValor_recaudo_canje() {
        return valor_recaudo_canje;
    }

    public void setValor_recaudo_canje(double valor_recaudo_canje) {
        this.valor_recaudo_canje = valor_recaudo_canje;
    }

    public String getNumero_cheque() {
        return numero_cheque;
    }

    public void setNumero_cheque(String numero_cheque) {
        this.numero_cheque = numero_cheque;
    }

    public String getCod_compensacion_cheque() {
        return cod_compensacion_cheque;
    }

    public void setCod_compensacion_cheque(String cod_compensacion_cheque) {
        this.cod_compensacion_cheque = cod_compensacion_cheque;
    }

    
}
