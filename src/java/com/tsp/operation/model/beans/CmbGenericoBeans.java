/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Harold Cuello G.
 */
public class CmbGenericoBeans {

    private int idCombo;
    private String DescripcionCombo;
    
    
    public CmbGenericoBeans() {
    }

    /**
     * @param idCombo the idConvenio to set
     */
    public void setIdCmb(int idCombo) {
        this.idCombo = idCombo;
    }

    /**
     * @return the idCombo
     */
    public int getIdCmb() {
        return idCombo;
    }

    /**
     * @param convenio the DescripcionCombo to set
     */
    public void setDescripcionCmb(String DescripcionCombo) {
        this.DescripcionCombo = DescripcionCombo;
    }

    /**
     * @return the DescripcionCombo
     */
    public String getDescripcionCmb() {
        return DescripcionCombo;
    }



}
