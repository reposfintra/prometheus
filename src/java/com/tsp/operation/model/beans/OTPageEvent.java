package com.tsp.operation.model.beans;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*; 
import java.awt.Color;

public class OTPageEvent extends PdfPageEventHelper{

    private PdfWriter writer;
    private Document document;
    private String ruta;

    private Image img1;
    private Image img2;

    public OTPageEvent(PdfWriter w, Document d, String r){
        writer = w;
        document = d;
        ruta = r;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document){
        try {
            Rectangle page = document.getPageSize();

            float[] widths = {0.5f, 0.5f};
            PdfPTable table;
            table = new PdfPTable(widths);
            table.setWidthPercentage(100);

            img1 = Image.getInstance(ruta + "/images/consorcio_logo.png");
            img1.scalePercent(60);
            img2 = Image.getInstance(ruta + "/images/electricaribe_logo.png");
            img2.scalePercent(60);
            celda(img1, table);
            celda(img2, table);
            
            table.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
            table.writeSelectedRows(0, -1, document.leftMargin(), page.getHeight() - document.topMargin() + table.getTotalHeight(), writer.getDirectContent());
           
            /*table = null;

            float[] widths2 = {0.333f, 0.333f, 0.333f};
            table = new PdfPTable(widths2);
            table.setWidthPercentage(100);

            celda("FR-OPAV-002.09", table);
            celda("Version 01", table);

            table.setTotalWidth(page.width() - document.leftMargin() - document.rightMargin());
            table.writeSelectedRows(0, -1, document.leftMargin(), document.bottomMargin(), writer.getDirectContent());*/
        } 
        catch (Exception ex) {
        }        
    }

    public void celda(Image img, PdfPTable tab){
        PdfPCell cell;
        cell = new PdfPCell(img);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(Color.white);
        tab.addCell(cell);
    }

    public void celda(Object val, PdfPTable tab){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val));
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(Color.white);
        tab.addCell(cell);
    }
}

