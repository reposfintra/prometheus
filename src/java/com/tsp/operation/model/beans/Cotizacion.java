/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Rhonalf
 */
public class Cotizacion {

    private String codigo;
    private String fecha;
    private String material;
    private String idAccion;
    private double cantidad;//100226
    private String observacion;
    private String id;//091222

    public Cotizacion(){
        this.cantidad = 0;
        this.codigo = "";
        this.fecha = "0099-01-01 00:00:00";
        this.idAccion = "";
        this.material = "";
        this.observacion = "Ninguna";
        this.id="";//091222
    }

    public void setCodigo(String cod) {
        this.codigo = cod;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setFecha(String dat) {
        this.fecha = dat;
    }

    public String getFecha() {
        return this.fecha;
    }

    public void setMaterial(String mat) {
        this.material = mat;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setAccion(String accion) {
        this.idAccion = accion;
    }

    public String getAccion() {
        return this.idAccion;
    }

    public void setObservacion(String nota) {
        this.observacion = nota;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setCantidad(double cant) {//100226
        this.cantidad = cant;
    }

    public double getCantidad() {//100226
        return this.cantidad;
    }

    public void setId(String idx){
        this.id = idx;
    }

    public String getId(){
        return this.id;
    }

}
