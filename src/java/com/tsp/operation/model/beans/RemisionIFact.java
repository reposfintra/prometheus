/********************************************************************
 *      Nombre Clase.................   RemisionIFact.java    
 *      Descripci�n..................   Bean de la generaci�n del informe
 *                                      de facturaci�n.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   31.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.Util;
import java.sql.*;

public class RemisionIFact {
        private String remision;
        private String placa;
        private String conductor;
        private Double tonelaje;
        private String fecha;
        private String std_job_no;
        private int    viajes;
        private String nomStandar;
        
        /** Creates a new instance of RemisionIFact */
        public RemisionIFact() {
        }
        
        /**
         * Getter for property conductor.
         * @return Value of property conductor.
         */
        public java.lang.String getConductor() {
                return conductor;
        }
        
        /**
         * Setter for property conductor.
         * @param conductor New value of property conductor.
         */
        public void setConductor(java.lang.String conductor) {
                this.conductor = conductor;
        }
        
        /**
         * Getter for property fecha.
         * @return Value of property fecha.
         */
        public java.lang.String getFecha() {
                return fecha;
        }
        
        /**
         * Setter for property fecha.
         * @param fecha New value of property fecha.
         */
        public void setFecha(java.lang.String fecha) {
                this.fecha = fecha;
        }
        
        /**
         * Getter for property placa.
         * @return Value of property placa.
         */
        public java.lang.String getPlaca() {
                return placa;
        }
        
        /**
         * Setter for property placa.
         * @param placa New value of property placa.
         */
        public void setPlaca(java.lang.String placa) {
                this.placa = placa;
        }
        
        /**
         * Getter for property remision.
         * @return Value of property remision.
         */
        public java.lang.String getRemision() {
                return remision;
        }
        
        /**
         * Setter for property remision.
         * @param remision New value of property remision.
         */
        public void setRemision(java.lang.String remision) {
                this.remision = remision;
        }
        
        /**
         * Getter for property tonelaje.
         * @return Value of property tonelaje.
         */
        public java.lang.Double getTonelaje() {
                return tonelaje;
        }
        
        /**
         * Setter for property tonelaje.
         * @param tonelaje New value of property tonelaje.
         */
        public void setTonelaje(java.lang.Double tonelaje) {
                this.tonelaje = tonelaje;
        }
        
        /**
         * Getter for property std_job_no.
         * @return Value of property std_job_no.
         */
        public java.lang.String getStd_job_no() {
                return std_job_no;
        }
        
        /**
         * Setter for property std_job_no.
         * @param std_job_no New value of property std_job_no.
         */
        public void setStd_job_no(java.lang.String std_job_no) {
                this.std_job_no = std_job_no;
        }
        
        /**
         * Getter for property viajes.
         * @return Value of property viajes.
         */
        public int getViajes() {
            return viajes;
        }
        
        /**
         * Setter for property viajes.
         * @param viajes New value of property viajes.
         */
        public void setViajes(int viajes) {
            this.viajes = viajes;
        }
        
        /**
         * Getter for property nomStandar.
         * @return Value of property nomStandar.
         */
        public java.lang.String getNomStandar() {
            return nomStandar;
        }
        
        /**
         * Setter for property nomStandar.
         * @param nomStandar New value of property nomStandar.
         */
        public void setNomStandar(java.lang.String nomStandar) {
            this.nomStandar = nomStandar;
        }
        
}
