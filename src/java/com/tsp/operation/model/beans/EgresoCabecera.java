/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class EgresoCabecera {


  private String reg_status ;
  private String dstrct ;
  private String branch_code ;
  private String bank_account_no ;
  private String document_no ;
  private String nit ;
  private String payment_name ;
  private String agency_id ;
  private String pmt_date  ;
  private String printer_date ;
  private String concept_code ;
  private double vlr ;
  private double vlr_for ;
  private String currency ;
  private String last_update ;
  private String user_update ;
  private String creation_date ;
  private String creation_user ;
  private String base ;
  private String tipo_documento ;
  private double tasa ;
  private String fecha_cheque ;
  private String usuario_impresion ;
  private String usuario_contabilizacion ;
  private String fecha_contabilizacion ;
  private String fecha_entrega ;
  private String usuario_envio ;
  private String fecha_envio ;
  private String usuario_recibido ;
  private String fecha_recibido ;
  private String usuario_entrega ;
  private String fecha_registro_entrega ;
  private String fecha_registro_envio ;
  private String fecha_registro_recibido ;
  private int    transaccion;
  private String nit_beneficiario;
  private String fecha_reporte ;
  private String nit_proveedor ;
  private String usuario_generacion ;
  private String periodo ;
  private String reimpresion ;
  private String contabilizable ;



    /** Creates a new instance of OfertaEcaDetalle */
    public EgresoCabecera() {
    }


    public static EgresoCabecera load(java.sql.ResultSet rs)throws java.sql.SQLException{

        EgresoCabecera egresoCabecera = new EgresoCabecera();

        egresoCabecera.setReg_status( rs.getString("reg_status") ) ;
        egresoCabecera.setDstrct( rs.getString("reg_status") ) ;
        egresoCabecera.setBranch_code( rs.getString("branch_code") ) ;
        egresoCabecera.setBank_account_no( rs.getString("bank_account_no") ) ;
        egresoCabecera.setDocument_no( rs.getString("document_no") ) ;
        egresoCabecera.setNit( rs.getString("nit") ) ;
        egresoCabecera.setPayment_name( rs.getString("payment_name") ) ;
        egresoCabecera.setAgency_id( rs.getString("agency_id") ) ;
        egresoCabecera.setPmt_date ( rs.getString("pmt_date") ) ;
        egresoCabecera.setPrinter_date( rs.getString("printer_date") ) ;
        egresoCabecera.setConcept_code( rs.getString("concept_code") ) ;
        egresoCabecera.setVlr( rs.getDouble("vlr") ) ;
        egresoCabecera.setVlr_for( rs.getDouble("vlr_for") ) ;
        egresoCabecera.setCurrency( rs.getString("currency") ) ;
        egresoCabecera.setLast_update( rs.getString("last_update") ) ;
        egresoCabecera.setUser_update( rs.getString("user_update") ) ;
        egresoCabecera.setCreation_date( rs.getString("creation_date") ) ;
        egresoCabecera.setCreation_user( rs.getString("creation_user") ) ;
        egresoCabecera.setBase( rs.getString("base") ) ;
        egresoCabecera.setTipo_documento( rs.getString("") ) ;
        egresoCabecera.setTasa( rs.getDouble("tasa") ) ;
        egresoCabecera.setFecha_cheque( rs.getString("fecha_cheque") ) ;
        egresoCabecera.setUsuario_impresion( rs.getString("usuario_impresion") ) ;
        egresoCabecera.setUsuario_contabilizacion( rs.getString("usuario_contabilizacion") ) ;
        egresoCabecera.setFecha_contabilizacion( rs.getString("fecha_contabilizacion") ) ;
        egresoCabecera.setFecha_entrega( rs.getString("fecha_entrega") ) ;
        egresoCabecera.setUsuario_envio( rs.getString("usuario_envio") ) ;
        egresoCabecera.setFecha_envio( rs.getString("fecha_envio") ) ;
        egresoCabecera.setUsuario_recibido( rs.getString("usuario_recibido") ) ;
        egresoCabecera.setFecha_recibido( rs.getString("fecha_recibido") ) ;
        egresoCabecera.setUsuario_entrega( rs.getString("usuario_entrega") ) ;
        egresoCabecera.setFecha_registro_entrega( rs.getString("fecha_registro_entrega") ) ;
        egresoCabecera.setFecha_registro_envio( rs.getString("fecha_registro_envio") ) ;
        egresoCabecera.setFecha_registro_recibido( rs.getString("fecha_registro_recibido") ) ;
        egresoCabecera.setTransaccion(rs.getInt("transaccion"));
        egresoCabecera.setNit_beneficiario(rs.getString("nit_beneficiario"));
        egresoCabecera.setFecha_reporte( rs.getString("fecha_reporte") ) ;
        egresoCabecera.setNit_proveedor( rs.getString("nit_proveedor") ) ;
        egresoCabecera.setUsuario_generacion( rs.getString("usuario_generacion") ) ;
        egresoCabecera.setPeriodo( rs.getString("periodo") ) ;
        egresoCabecera.setReimpresion( rs.getString("reimpresion") ) ;
        egresoCabecera.setContabilizable( rs.getString("contabilizable") ) ;
        return egresoCabecera;
        
    }





    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the branch_code
     */
    public String getBranch_code() {
        return branch_code;
    }

    /**
     * @param branch_code the branch_code to set
     */
    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    /**
     * @return the bank_account_no
     */
    public String getBank_account_no() {
        return bank_account_no;
    }

    /**
     * @param bank_account_no the bank_account_no to set
     */
    public void setBank_account_no(String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }

    /**
     * @return the document_no
     */
    public String getDocument_no() {
        return document_no;
    }

    /**
     * @param document_no the document_no to set
     */
    public void setDocument_no(String document_no) {
        this.document_no = document_no;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the payment_name
     */
    public String getPayment_name() {
        return payment_name;
    }

    /**
     * @param payment_name the payment_name to set
     */
    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }

    /**
     * @return the agency_id
     */
    public String getAgency_id() {
        return agency_id;
    }

    /**
     * @param agency_id the agency_id to set
     */
    public void setAgency_id(String agency_id) {
        this.agency_id = agency_id;
    }

    /**
     * @return the pmt_date
     */
    public String getPmt_date() {
        return pmt_date;
    }

    /**
     * @param pmt_date the pmt_date to set
     */
    public void setPmt_date(String pmt_date) {
        this.pmt_date = pmt_date;
    }

    /**
     * @return the printer_date
     */
    public String getPrinter_date() {
        return printer_date;
    }

    /**
     * @param printer_date the printer_date to set
     */
    public void setPrinter_date(String printer_date) {
        this.printer_date = printer_date;
    }

    /**
     * @return the concept_code
     */
    public String getConcept_code() {
        return concept_code;
    }

    /**
     * @param concept_code the concept_code to set
     */
    public void setConcept_code(String concept_code) {
        this.concept_code = concept_code;
    }

    /**
     * @return the vlr
     */
    public double getVlr() {
        return vlr;
    }

    /**
     * @param vlr the vlr to set
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }

    /**
     * @return the vlr_for
     */
    public double getVlr_for() {
        return vlr_for;
    }

    /**
     * @param vlr_for the vlr_for to set
     */
    public void setVlr_for(double vlr_for) {
        this.vlr_for = vlr_for;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the tasa
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * @param tasa the tasa to set
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    /**
     * @return the fecha_cheque
     */
    public String getFecha_cheque() {
        return fecha_cheque;
    }

    /**
     * @param fecha_cheque the fecha_cheque to set
     */
    public void setFecha_cheque(String fecha_cheque) {
        this.fecha_cheque = fecha_cheque;
    }

    /**
     * @return the usuario_impresion
     */
    public String getUsuario_impresion() {
        return usuario_impresion;
    }

    /**
     * @param usuario_impresion the usuario_impresion to set
     */
    public void setUsuario_impresion(String usuario_impresion) {
        this.usuario_impresion = usuario_impresion;
    }

    /**
     * @return the usuario_contabilizacion
     */
    public String getUsuario_contabilizacion() {
        return usuario_contabilizacion;
    }

    /**
     * @param usuario_contabilizacion the usuario_contabilizacion to set
     */
    public void setUsuario_contabilizacion(String usuario_contabilizacion) {
        this.usuario_contabilizacion = usuario_contabilizacion;
    }

    /**
     * @return the fecha_contabilizacion
     */
    public String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    /**
     * @param fecha_contabilizacion the fecha_contabilizacion to set
     */
    public void setFecha_contabilizacion(String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    /**
     * @return the fecha_entrega
     */
    public String getFecha_entrega() {
        return fecha_entrega;
    }

    /**
     * @param fecha_entrega the fecha_entrega to set
     */
    public void setFecha_entrega(String fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }

    /**
     * @return the usuario_envio
     */
    public String getUsuario_envio() {
        return usuario_envio;
    }

    /**
     * @param usuario_envio the usuario_envio to set
     */
    public void setUsuario_envio(String usuario_envio) {
        this.usuario_envio = usuario_envio;
    }

    /**
     * @return the fecha_envio
     */
    public String getFecha_envio() {
        return fecha_envio;
    }

    /**
     * @param fecha_envio the fecha_envio to set
     */
    public void setFecha_envio(String fecha_envio) {
        this.fecha_envio = fecha_envio;
    }

    /**
     * @return the usuario_recibido
     */
    public String getUsuario_recibido() {
        return usuario_recibido;
    }

    /**
     * @param usuario_recibido the usuario_recibido to set
     */
    public void setUsuario_recibido(String usuario_recibido) {
        this.usuario_recibido = usuario_recibido;
    }

    /**
     * @return the fecha_recibido
     */
    public String getFecha_recibido() {
        return fecha_recibido;
    }

    /**
     * @param fecha_recibido the fecha_recibido to set
     */
    public void setFecha_recibido(String fecha_recibido) {
        this.fecha_recibido = fecha_recibido;
    }

    /**
     * @return the usuario_entrega
     */
    public String getUsuario_entrega() {
        return usuario_entrega;
    }

    /**
     * @param usuario_entrega the usuario_entrega to set
     */
    public void setUsuario_entrega(String usuario_entrega) {
        this.usuario_entrega = usuario_entrega;
    }

    /**
     * @return the fecha_registro_entrega
     */
    public String getFecha_registro_entrega() {
        return fecha_registro_entrega;
    }

    /**
     * @param fecha_registro_entrega the fecha_registro_entrega to set
     */
    public void setFecha_registro_entrega(String fecha_registro_entrega) {
        this.fecha_registro_entrega = fecha_registro_entrega;
    }

    /**
     * @return the fecha_registro_envio
     */
    public String getFecha_registro_envio() {
        return fecha_registro_envio;
    }

    /**
     * @param fecha_registro_envio the fecha_registro_envio to set
     */
    public void setFecha_registro_envio(String fecha_registro_envio) {
        this.fecha_registro_envio = fecha_registro_envio;
    }

    /**
     * @return the fecha_registro_recibido
     */
    public String getFecha_registro_recibido() {
        return fecha_registro_recibido;
    }

    /**
     * @param fecha_registro_recibido the fecha_registro_recibido to set
     */
    public void setFecha_registro_recibido(String fecha_registro_recibido) {
        this.fecha_registro_recibido = fecha_registro_recibido;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the nit_beneficiario
     */
    public String getNit_beneficiario() {
        return nit_beneficiario;
    }

    /**
     * @param nit_beneficiario the nit_beneficiario to set
     */
    public void setNit_beneficiario(String nit_beneficiario) {
        this.nit_beneficiario = nit_beneficiario;
    }

    /**
     * @return the fecha_reporte
     */
    public String getFecha_reporte() {
        return fecha_reporte;
    }

    /**
     * @param fecha_reporte the fecha_reporte to set
     */
    public void setFecha_reporte(String fecha_reporte) {
        this.fecha_reporte = fecha_reporte;
    }

    /**
     * @return the nit_proveedor
     */
    public String getNit_proveedor() {
        return nit_proveedor;
    }

    /**
     * @param nit_proveedor the nit_proveedor to set
     */
    public void setNit_proveedor(String nit_proveedor) {
        this.nit_proveedor = nit_proveedor;
    }

    /**
     * @return the usuario_generacion
     */
    public String getUsuario_generacion() {
        return usuario_generacion;
    }

    /**
     * @param usuario_generacion the usuario_generacion to set
     */
    public void setUsuario_generacion(String usuario_generacion) {
        this.usuario_generacion = usuario_generacion;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the reimpresion
     */
    public String getReimpresion() {
        return reimpresion;
    }

    /**
     * @param reimpresion the reimpresion to set
     */
    public void setReimpresion(String reimpresion) {
        this.reimpresion = reimpresion;
    }

    /**
     * @return the contabilizable
     */
    public String getContabilizable() {
        return contabilizable;
    }

    /**
     * @param contabilizable the contabilizable to set
     */
    public void setContabilizable(String contabilizable) {
        this.contabilizable = contabilizable;
    }



}



