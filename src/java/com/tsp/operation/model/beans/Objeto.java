/********************************************************************
 *  Nombre Clase.................   Objeto.java
 *  Descripci�n..................   Bean de la tabla precintos
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   04.01.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
/**
 *
 * @author  EQUIPO12
 */
public class Objeto implements Serializable{
        
        private String pagina;
        private Object objeto;
        
        /** Creates a new instance of Objeto */
        public Objeto() {
        }
        
        /**
         * Getter for property pagina.
         * @return Value of property pagina.
         */
        public java.lang.String getPagina() {
                return pagina;
        }
        
        /**
         * Setter for property pagina.
         * @param pagina New value of property pagina.
         */
        public void setPagina(java.lang.String Pagina) {
                this.pagina = Pagina;
        }
        
        /**
         * Getter for property objeto.
         * @return Value of property objeto.
         */
        public Object getObjeto() {
                return objeto;
        }
        
        /**
         * Setter for property objeto.
         * @param objeto New value of property objeto.
         */
        public void setObjeto(Object Objeto) {
                this.objeto = Objeto;
        }
        
}
