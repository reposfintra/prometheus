package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudVehiculo<br/>
 * 1/03/2011
 * @author ivargas-Geotech
 */
public class SolicitudVehiculo {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String tipoPersona;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String tipo;
    private String secuencia;
    private String marca;
    private String tipoVehiculo;
    private String placa;
    private String modelo;
    private String valorComercial;
    private String cuotaMensual;
    private String pignoradoAFavorDe;

    public SolicitudVehiculo load(ResultSet rs) throws SQLException {
        SolicitudVehiculo vehiculo = new SolicitudVehiculo();
        vehiculo.setNumeroSolicitud(rs.getString("numero_solicitud"));
        vehiculo.setTipo(rs.getString("tipo"));
        vehiculo.setSecuencia(rs.getString("secuencia"));
        vehiculo.setValorComercial(rs.getString("valor_comercial"));
        vehiculo.setPlaca(rs.getString("placa"));
        vehiculo.setModelo(rs.getString("modelo"));
        vehiculo.setCuotaMensual(rs.getString("cuota_mensual"));
        vehiculo.setPignoradoAFavorDe(rs.getString("pignorado_a_favor_de"));
        vehiculo.setTipoVehiculo(rs.getString("tipo_vehiculo"));
        vehiculo.setMarca(rs.getString("marca"));
        return vehiculo;
    }

    /**
     * obtiene el valor de cuotaMensual
     *
     * @return el valor de cuotaMensual
     */
    public String getCuotaMensual() {
        return cuotaMensual;
    }

    /**
     * cambia el valor de cuotaMensual
     *
     * @param cuotaMensual nuevo valor cuotaMensual
     */
    public void setCuotaMensual(String cuotaMensual) {
        this.cuotaMensual = cuotaMensual;
    }

    /**
     * obtiene el valor de marca
     *
     * @return el valor de marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * cambia el valor de marca
     *
     * @param marca nuevo valor marca
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * obtiene el valor de modelo
     *
     * @return el valor de modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * cambia el valor de modelo
     *
     * @param modelo nuevo valor modelo
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * obtiene el valor de pignoradoAFavorDe
     *
     * @return el valor de pignoradoAFavorDe
     */
    public String getPignoradoAFavorDe() {
        return pignoradoAFavorDe;
    }

    /**
     * cambia el valor de pignoradoAFavorDe
     *
     * @param pignoradoAFavorDe nuevo valor pignoradoAFavorDe
     */
    public void setPignoradoAFavorDe(String pignoradoAFavorDe) {
        this.pignoradoAFavorDe = pignoradoAFavorDe;
    }

    /**
     * obtiene el valor de placa
     *
     * @return el valor de placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * cambia el valor de placa
     *
     * @param placa nuevo valor placa
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * obtiene el valor de tipoVehiculo
     *
     * @return el valor de tipoVehiculo
     */
    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    /**
     * cambia el valor de tipoVehiculo
     *
     * @param tipoVehiculo nuevo valor tipoVehiculo
     */
    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    /**
     * obtiene el valor de secuencia
     *
     * @return el valor de secuencia
     */
    public String getSecuencia() {
        return secuencia;
    }

    /**
     * cambia el valor de secuencia
     *
     * @param secuencia nuevo valor secuencia
     */
    public void setSecuencia(String secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * obtiene el valor de valorComercial
     *
     * @return el valor de valorComercial
     */
    public String getValorComercial() {
        return valorComercial;
    }

    /**
     * cambia el valor de valorComercial
     *
     * @param valorComercial nuevo valor valorComercial
     */
    public void setValorComercial(String valorComercial) {
        this.valorComercial = valorComercial;
    }   

    /**
     * obtiene el valor de tipo
     *
     * @return el valor de tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * cambia el valor de tipo
     *
     * @param tipo nuevo valor tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * obtiene el valor de  creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de  creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de  lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de  userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de  dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de  numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de  regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    /**
     * obtiene el valor de tipoPersona
     *
     * @return el valor de tipoPersona
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * cambia el valor de tipoPersona
     *
     * @param tipoPersona nuevo valor tipoPersona
     */
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }
}
