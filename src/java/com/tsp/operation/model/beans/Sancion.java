/*
 * Sancion.java
 *
 * Created on 19 de octubre de 2005, 04:54 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Jose
 */
public class Sancion {
    private double valor;
    private double valor_aprobado;
    private String fecha_aprobacion;
    private String fecha_migracion;
    private String usuario_migracion;
    private String usuario_aprobacion;
    private String tipo_sancion;
    private int cod_sancion;
    private String numpla;
    private String observacion;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String distrito;
    private String base;    
    /** Creates a new instance of Sancion */
    public Sancion() {
    }

    public java.lang.String getBase() {
        return base;
    }

    public void setBase(java.lang.String base) {
        this.base = base;
    }

    public int getCod_sancion() {
        return cod_sancion;
    }

    public void setCod_sancion(int cod_sancion) {
        this.cod_sancion = cod_sancion;
    }

    public java.lang.String getDistrito() {
        return distrito;
    }

    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }

    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public java.lang.String getNumpla() {
        return numpla;
    }

    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }

    public java.lang.String getObservacion() {
        return observacion;
    }

    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }

    public java.lang.String getTipo_sancion() {
        return tipo_sancion;
    }

    public void setTipo_sancion(java.lang.String tipo_sancion) {
        this.tipo_sancion = tipo_sancion;
    }

    public java.lang.String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }

    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }

    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getValor_aprobado() {
        return valor_aprobado;
    }

    public void setValor_aprobado(double valor_aprobado) {
        this.valor_aprobado = valor_aprobado;
    }

    public java.lang.String getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    public void setFecha_aprobacion(java.lang.String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    public java.lang.String getUsuario_aprobacion() {
        return usuario_aprobacion;
    }

    public void setUsuario_aprobacion(java.lang.String usuario_aprobacion) {
        this.usuario_aprobacion = usuario_aprobacion;
    }

    public java.lang.String getFecha_migracion() {
        return fecha_migracion;
    }

    public void setFecha_migracion(java.lang.String fecha_migracion) {
        this.fecha_migracion = fecha_migracion;
    }

    public java.lang.String getUsuario_migracion() {
        return usuario_migracion;
    }

    public void setUsuario_migracion(java.lang.String usuario_migracion) {
        this.usuario_migracion = usuario_migracion;
    }
    
}
