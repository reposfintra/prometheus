/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class reporteColocacionBeans {

    public String id;
    public String periodo;
    public String transportadora;
    public String nombre_agencia;
    public String cedula_propietario;
    public String propietario;
    public String cedula_conductor;
    public String conductor;
    public String placa;
    public String sucursal;
    public String origen;
    public String destino;
    public String planilla;
    public String fecha_venta;
    public String fecha_anticipo;
    public String valor_anticipo;
    public String descuento_fintra;
    public String valor_consignado;
    public String reanticipo;
    public String numero_venta;
    public String nombre_eds;
    public String kilometraje;
    public String producto;
    public String precioxunidad;
    public String cantidad_suministrada;
    public String total_ventas;
    public String disponible;
    public String tiempo_legalizacion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(String transportadora) {
        this.transportadora = transportadora;
    }

    public String getNombre_agencia() {
        return nombre_agencia;
    }

    public void setNombre_agencia(String nombre_agencia) {
        this.nombre_agencia = nombre_agencia;
    }

    public String getCedula_propietario() {
        return cedula_propietario;
    }

    public void setCedula_propietario(String cedula_propietario) {
        this.cedula_propietario = cedula_propietario;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getCedula_conductor() {
        return cedula_conductor;
    }

    public void setCedula_conductor(String cedula_conductor) {
        this.cedula_conductor = cedula_conductor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getFecha_venta() {
        return fecha_venta;
    }

    public void setFecha_venta(String fecha_venta) {
        this.fecha_venta = fecha_venta;
    }

    public String getFecha_anticipo() {
        return fecha_anticipo;
    }

    public void setFecha_anticipo(String fecha_anticipo) {
        this.fecha_anticipo = fecha_anticipo;
    }

    public String getValor_anticipo() {
        return valor_anticipo;
    }

    public void setValor_anticipo(String valor_anticipo) {
        this.valor_anticipo = valor_anticipo;
    }

    public String getDescuento_fintra() {
        return descuento_fintra;
    }

    public void setDescuento_fintra(String descuento_fintra) {
        this.descuento_fintra = descuento_fintra;
    }

    public String getValor_consignado() {
        return valor_consignado;
    }

    public void setValor_consignado(String valor_consignado) {
        this.valor_consignado = valor_consignado;
    }

    public String getReanticipo() {
        return reanticipo;
    }

    public void setReanticipo(String reanticipo) {
        this.reanticipo = reanticipo;
    }

    public String getNumero_venta() {
        return numero_venta;
    }

    public void setNumero_venta(String numero_venta) {
        this.numero_venta = numero_venta;
    }

    public String getNombre_eds() {
        return nombre_eds;
    }

    public void setNombre_eds(String nombre_eds) {
        this.nombre_eds = nombre_eds;
    }

    public String getKilometraje() {
        return kilometraje;
    }

    public void setKilometraje(String kilometraje) {
        this.kilometraje = kilometraje;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPrecioxunidad() {
        return precioxunidad;
    }

    public void setPrecioxunidad(String precioxunidad) {
        this.precioxunidad = precioxunidad;
    }

    public String getCantidad_suministrada() {
        return cantidad_suministrada;
    }

    public void setCantidad_suministrada(String cantidad_suministrada) {
        this.cantidad_suministrada = cantidad_suministrada;
    }

    public String getTotal_ventas() {
        return total_ventas;
    }

    public void setTotal_ventas(String total_ventas) {
        this.total_ventas = total_ventas;
    }

    public String getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    public String getConductor() {
        return conductor;
    }

    public void setConductor(String conductor) {
        this.conductor = conductor;
    }

    public String getTiempo_legalizacion() {
        return tiempo_legalizacion;
    }

    public void setTiempo_legalizacion(String tiempo_legalizacion) {
        this.tiempo_legalizacion = tiempo_legalizacion;
    }

}
