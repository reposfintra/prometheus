/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class TotalExtracto {

    private String nit;
    private int secuencia;
    private double vlr_ext_detalle_calculado;
    private double vlr_ext_detalle_registrado;
    private double vlr_ext_detalle_diferencia;



    public static TotalExtracto load(java.sql.ResultSet rs)throws java.sql.SQLException{

        TotalExtracto totalExtracto = new TotalExtracto();

        totalExtracto.setNit( rs.getString("nit") );
        totalExtracto.setSecuencia(rs.getInt("secuencia") );
        totalExtracto.setVlr_ext_detalle_calculado( rs.getDouble("vlr_ext_detalle_calculado") );
        totalExtracto.setVlr_ext_detalle_registrado( rs.getDouble("vlr_ext_detalle_registrado") );
        totalExtracto.setVlr_ext_detalle_diferencia( rs.getDouble("vlr_ext_detalle_diferencia") );

        return totalExtracto;
    }







    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the secuencia
     */
    public int getSecuencia() {
        return secuencia;
    }

    /**
     * @param secuencia the secuencia to set
     */
    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }

    /**
     * @return the vlr_ext_detalle_calculado
     */
    public double getVlr_ext_detalle_calculado() {
        return vlr_ext_detalle_calculado;
    }

    /**
     * @param vlr_ext_detalle_calculado the vlr_ext_detalle_calculado to set
     */
    public void setVlr_ext_detalle_calculado(double vlr_ext_detalle_calculado) {
        this.vlr_ext_detalle_calculado = vlr_ext_detalle_calculado;
    }

    /**
     * @return the vlr_ext_detalle_registrado
     */
    public double getVlr_ext_detalle_registrado() {
        return vlr_ext_detalle_registrado;
    }

    /**
     * @param vlr_ext_detalle_registrado the vlr_ext_detalle_registrado to set
     */
    public void setVlr_ext_detalle_registrado(double vlr_ext_detalle_registrado) {
        this.vlr_ext_detalle_registrado = vlr_ext_detalle_registrado;
    }

    /**
     * @return the vlr_ext_detalle_diferencia
     */
    public double getVlr_ext_detalle_diferencia() {
        return vlr_ext_detalle_diferencia;
    }

    /**
     * @param vlr_ext_detalle_diferencia the vlr_ext_detalle_diferencia to set
     */
    public void setVlr_ext_detalle_diferencia(double vlr_ext_detalle_diferencia) {
        this.vlr_ext_detalle_diferencia = vlr_ext_detalle_diferencia;
    }


}
