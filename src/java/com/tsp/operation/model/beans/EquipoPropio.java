/******************************************************************
* Nombre ......................EquipoPropio.java
* Descripci�n..................Clase bean para Equipo Propio
* Autor........................Armando Oviedo
* Fecha........................12/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*; 

public class EquipoPropio {
    
    String regStatus;
    String dstrct;
    String lastUpdate;
    String userUpdate;
    String creationDate;
    String creationUser;
    String base;
    String trailer;
    String claseequipo;    
    
    /** Creates a new instance of EquipoPropio */
    public EquipoPropio() {
    }
    
    /**
     * M�todo que retorna un objeto EquipoPropio cargado previamente de un resultset
     * @autor.......Armando Oviedo          
     * @throws......SQLException
     * @version.....1.0.
     * @param.......ResultSet rs
     * @return.......objeto EquipoPropio
     **/ 
    public EquipoPropio loadResultSet(ResultSet rs) throws SQLException{
        EquipoPropio de = new EquipoPropio();
        de.setBase(rs.getString("base"));
        de.setTrailer(rs.getString("trailer"));        
        de.setCreationDate(rs.getString("creation_date"));
        de.setCreationUser(rs.getString("creation_user"));
        de.setClaseEquipo(rs.getString("equipopropio"));
        de.setDstrct(rs.getString("dstrct"));
        de.setLastUpdate(rs.getString("last_update"));
        de.setRegStatus(rs.getString("reg_status"));
        de.setUserUpdate(rs.getString("user_update"));
        return de;
    }
    public void setRegStatus(String regStatus){
        this.regStatus = regStatus;
    }
    public void setDstrct(String dstrct){
        this.dstrct = dstrct;
    }
    public void setLastUpdate(String lastUpdate){
        this.lastUpdate = lastUpdate;
    }
    public void setUserUpdate(String userUpdate){
        this.userUpdate = userUpdate;
    }
    public void setCreationDate(String creationDate){
        this.creationDate = creationDate;
    }
    public void setCreationUser(String creationUser){
        this.creationUser = creationUser;
    }
    public void setBase(String base){
        this.base = base;
    }
    public void setTrailer(String trailer){
        this.trailer = trailer;
    }
    public void setClaseEquipo(String claseequipo){
        this.claseequipo = claseequipo;
    }    
    public String getRegStatus(){
        return this.regStatus;
    }
    public String getDstrct(){
        return this.dstrct;
    }
    public String getLastUpdate(){
        return this.lastUpdate;
    }
    public String getUserUpdate(){
        return this.userUpdate;
    }
    public String getCreationDate(){
        return this.creationDate;
    }
    public String getCreationUser(){
        return this.creationUser;
    }
    public String getBase(){
        return this.base;
    }
    public String getTrailer(){
        return this.trailer;
    }
    public String getClaseEquipo(){
        return this.claseequipo;
    }    
}
