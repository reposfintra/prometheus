/**************************************************************************
 * Nombre: ......................Ingreso.java                           *
 * Descripci�n: .................Beans de acuerdo especial.               *
 * Autor:........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:........................8 de mayo de 2006, 06:17 PM              *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*; 
import java.sql.*;
import java.util.*;
import java.io.Serializable;
 

public class Ingreso implements Serializable {
    private String reg_status;
    private String dstrct;
    private String codcli;
    private String nitcli;
    private String num_ingreso;
    private String concepto;
    private String tipo_ingreso;
    private String fecha_consignacion;
    private String fecha_ingreso;
    private String branch_code;
    private String bank_account_no;
    private String codmoneda;
    private String fecha_impresion;
    private String agencia_ingreso;
    private String descripcion_ingreso;
    private String periodo;
    private double vlr_ingreso;
    private double vlr_ingreso_me;
    private int transaccion;
    private int transaccion_anulacion;
    private String fecha_contabilizacion;
    private String fecha_anulacion_contabilizacion;
    private String fecha_anulacion;
    private double vlr_tasa;
    private double tasaDolBol;
    private String fecha_tasa;
    private int cant_item;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String base;
    private String tipo_documento;
    private String nomCliente;
    private String nro_consignacion;
    private String periodo_anulacion;
    private String abc;
    
    private String destipo;
    private double vlr_saldo_ing;
    
    private String cuenta;
    private String auxiliar;
    private String tipo_aux;
    private LinkedList tipos; 
    private String cuenta_banco;
    private String pagina;    
     /** mfontalvo 2007-0206 */
    private String cuenta_cliente;   
    private String cmc;
    
    private int nro_extracto;
    /**
     * Getter for property cuenta_cliente.
     * @return Value of property cuenta_cliente.
     */
    public java.lang.String getCuenta_cliente() {
        return cuenta_cliente;
    }
    
    /**
     * Setter for property cuenta_cliente.
     * @param cuenta_cliente New value of property cuenta_cliente.
     */
    public void setCuenta_cliente(java.lang.String cuenta_cliente) {
        this.cuenta_cliente = cuenta_cliente;
    }
    
    /** Creates a new instance of ingreso */
    public Ingreso() {
    }
    
    /**
     * Getter for property agencia_ingreso.
     * @return Value of property agencia_ingreso.
     */
    public java.lang.String getAgencia_ingreso() {
        return agencia_ingreso;
    }
    
    /**
     * Setter for property agencia_ingreso.
     * @param agencia_ingreso New value of property agencia_ingreso.
     */
    public void setAgencia_ingreso(java.lang.String agencia_ingreso) {
        this.agencia_ingreso = agencia_ingreso;
    }
    
    /**
     * Getter for property bank_account_no.
     * @return Value of property bank_account_no.
     */
    public java.lang.String getBank_account_no() {
        return bank_account_no;
    }
    
    /**
     * Setter for property bank_account_no.
     * @param bank_account_no New value of property bank_account_no.
     */
    public void setBank_account_no(java.lang.String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property branch_code.
     * @return Value of property branch_code.
     */
    public java.lang.String getBranch_code() {
        return branch_code;
    }
    
    /**
     * Setter for property branch_code.
     * @param branch_code New value of property branch_code.
     */
    public void setBranch_code(java.lang.String branch_code) {
        this.branch_code = branch_code;
    }
    
   
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property codmoneda.
     * @return Value of property codmoneda.
     */
    public java.lang.String getCodmoneda() {
        return codmoneda;
    }
    
    /**
     * Setter for property codmoneda.
     * @param codmoneda New value of property codmoneda.
     */
    public void setCodmoneda(java.lang.String codmoneda) {
        this.codmoneda = codmoneda;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property descripcion_ingreso.
     * @return Value of property descripcion_ingreso.
     */
    public java.lang.String getDescripcion_ingreso() {
        return descripcion_ingreso;
    }
    
    /**
     * Setter for property descripcion_ingreso.
     * @param descripcion_ingreso New value of property descripcion_ingreso.
     */
    public void setDescripcion_ingreso(java.lang.String descripcion_ingreso) {
        this.descripcion_ingreso = descripcion_ingreso;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_anulacion.
     * @return Value of property fecha_anulacion.
     */
    public java.lang.String getFecha_anulacion() {
        return fecha_anulacion;
    }
    
    /**
     * Setter for property fecha_anulacion.
     * @param fecha_anulacion New value of property fecha_anulacion.
     */
    public void setFecha_anulacion(java.lang.String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }
    
    /**
     * Getter for property fecha_anulacion_contabilizacion.
     * @return Value of property fecha_anulacion_contabilizacion.
     */
    public java.lang.String getFecha_anulacion_contabilizacion() {
        return fecha_anulacion_contabilizacion;
    }
    
    /**
     * Setter for property fecha_anulacion_contabilizacion.
     * @param fecha_anulacion_contabilizacion New value of property fecha_anulacion_contabilizacion.
     */
    public void setFecha_anulacion_contabilizacion(java.lang.String fecha_anulacion_contabilizacion) {
        this.fecha_anulacion_contabilizacion = fecha_anulacion_contabilizacion;
    }
    
    
    
    /**
     * Getter for property fecha_contabilizacion.
     * @return Value of property fecha_contabilizacion.
     */
    public java.lang.String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }
    
    /**
     * Setter for property fecha_contabilizacion.
     * @param fecha_contabilizacion New value of property fecha_contabilizacion.
     */
    public void setFecha_contabilizacion(java.lang.String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }
    
    /**
     * Getter for property fecha_impresion.
     * @return Value of property fecha_impresion.
     */
    public java.lang.String getFecha_impresion() {
        return fecha_impresion;
    }
    
    /**
     * Setter for property fecha_impresion.
     * @param fecha_impresion New value of property fecha_impresion.
     */
    public void setFecha_impresion(java.lang.String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }
    
    /**
     * Getter for property fecha_ingreso.
     * @return Value of property fecha_ingreso.
     */
    public java.lang.String getFecha_ingreso() {
        return fecha_ingreso;
    }
    
    /**
     * Setter for property fecha_ingreso.
     * @param fecha_ingreso New value of property fecha_ingreso.
     */
    public void setFecha_ingreso(java.lang.String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }
    
    /**
     * Getter for property fecha_tasa.
     * @return Value of property fecha_tasa.
     */
    public java.lang.String getFecha_tasa() {
        return fecha_tasa;
    }
    
    /**
     * Setter for property fecha_tasa.
     * @param fecha_tasa New value of property fecha_tasa.
     */
    public void setFecha_tasa(java.lang.String fecha_tasa) {
        this.fecha_tasa = fecha_tasa;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property nitcli.
     * @return Value of property nitcli.
     */
    public java.lang.String getNitcli() {
        return nitcli;
    }
    
    /**
     * Setter for property nitcli.
     * @param nitcli New value of property nitcli.
     */
    public void setNitcli(java.lang.String nitcli) {
        this.nitcli = nitcli;
    }
    
    
    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }
    
    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tipo_ingreso.
     * @return Value of property tipo_ingreso.
     */
    public java.lang.String getTipo_ingreso() {
        return tipo_ingreso;
    }
    
    /**
     * Setter for property tipo_ingreso.
     * @param tipo_ingreso New value of property tipo_ingreso.
     */
    public void setTipo_ingreso(java.lang.String tipo_ingreso) {
        this.tipo_ingreso = tipo_ingreso;
    }
    
    
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property vlr_ingreso.
     * @return Value of property vlr_ingreso.
     */
    public double getVlr_ingreso() {
        return vlr_ingreso;
    }    
  
    /**
     * Setter for property vlr_ingreso.
     * @param vlr_ingreso New value of property vlr_ingreso.
     */
    public void setVlr_ingreso(double vlr_ingreso) {
        this.vlr_ingreso = vlr_ingreso;
    }    
    
    /**
     * Getter for property vlr_ingreso_me.
     * @return Value of property vlr_ingreso_me.
     */
    public double getVlr_ingreso_me() {
        return vlr_ingreso_me;
    }
    
    /**
     * Setter for property vlr_ingreso_me.
     * @param vlr_ingreso_me New value of property vlr_ingreso_me.
     */
    public void setVlr_ingreso_me(double vlr_ingreso_me) {
        this.vlr_ingreso_me = vlr_ingreso_me;
    }
    
    /**
     * Getter for property vlr_tasa.
     * @return Value of property vlr_tasa.
     */
    public double getVlr_tasa() {
        return vlr_tasa;
    }
    
    /**
     * Setter for property vlr_tasa.
     * @param vlr_tasa New value of property vlr_tasa.
     */
    public void setVlr_tasa(double vlr_tasa) {
        this.vlr_tasa = vlr_tasa;
    }
    
    /**
     * Getter for property transaccion.
     * @return Value of property transaccion.
     */
    public int getTransaccion() {
        return transaccion;
    }
    
    /**
     * Setter for property transaccion.
     * @param transaccion New value of property transaccion.
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }
    
    /**
     * Getter for property cant_item.
     * @return Value of property cant_item.
     */
    public int getCant_item() {
        return cant_item;
    }
    
    /**
     * Setter for property cant_item.
     * @param cant_item New value of property cant_item.
     */
    public void setCant_item(int cant_item) {
        this.cant_item = cant_item;
    }
    
    /**
     * Getter for property transaccion_anulacion.
     * @return Value of property transaccion_anulacion.
     */
    public int getTransaccion_anulacion() {
        return transaccion_anulacion;
    }
    
    /**
     * Setter for property transaccion_anulacion.
     * @param transaccion_anulacion New value of property transaccion_anulacion.
     */
    public void setTransaccion_anulacion(int transaccion_anulacion) {
        this.transaccion_anulacion = transaccion_anulacion;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property num_ingreso.
     * @return Value of property num_ingreso.
     */
    public String getNum_ingreso() {
        return num_ingreso;
    }
    
    /**
     * Setter for property num_ingreso.
     * @param num_ingreso New value of property num_ingreso.
     */
    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }
    
    /**
     * Getter for property nomCliente.
     * @return Value of property nomCliente.
     */
    public java.lang.String getNomCliente() {
        return nomCliente;
    }
    
    /**
     * Setter for property nomCliente.
     * @param nomCliente New value of property nomCliente.
     */
    public void setNomCliente(java.lang.String nomCliente) {
        this.nomCliente = nomCliente;
    }
    
    /**
     * Getter for property fecha_consignacion.
     * @return Value of property fecha_consignacion.
     */
    public java.lang.String getFecha_consignacion() {
        return fecha_consignacion;
    }
    
    /**
     * Setter for property fecha_consignacion.
     * @param fecha_consignacion New value of property fecha_consignacion.
     */
    public void setFecha_consignacion(java.lang.String fecha_consignacion) {
        this.fecha_consignacion = fecha_consignacion;
    }
    
    /**
     * Getter for property nro_consignacion.
     * @return Value of property nro_consignacion.
     */
    public java.lang.String getNro_consignacion() {
        return nro_consignacion;
    }
    
    /**
     * Setter for property nro_consignacion.
     * @param nro_consignacion New value of property nro_consignacion.
     */
    public void setNro_consignacion(java.lang.String nro_consignacion) {
        this.nro_consignacion = nro_consignacion;
    }
    
    /**
     * Getter for property destipo.
     * @return Value of property destipo.
     */
    public java.lang.String getDestipo() {
        return destipo;
    }    

    /**
     * Setter for property destipo.
     * @param destipo New value of property destipo.
     */
    public void setDestipo(java.lang.String destipo) {
        this.destipo = destipo;
    }
    
    /**
     * Getter for property vlr_saldo_ing.
     * @return Value of property vlr_saldo_ing.
     */
    public double getVlr_saldo_ing() {
        return vlr_saldo_ing;
    }
    
    /**
     * Setter for property vlr_saldo_ing.
     * @param vlr_saldo_ing New value of property vlr_saldo_ing.
     */
    public void setVlr_saldo_ing(double vlr_saldo_ing) {
        this.vlr_saldo_ing = vlr_saldo_ing;
    }
    
    /**
     * Getter for property periodo_anulacion.
     * @return Value of property periodo_anulacion.
     */
    public java.lang.String getPeriodo_anulacion() {
        return periodo_anulacion;
    }
    
    /**
     * Setter for property periodo_anulacion.
     * @param periodo_anulacion New value of property periodo_anulacion.
     */
    public void setPeriodo_anulacion(java.lang.String periodo_anulacion) {
        this.periodo_anulacion = periodo_anulacion;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    /**
     * Getter for property tipos.
     * @return Value of property tipos.
     */
    public java.util.LinkedList getTipos() {
        return tipos;
    }
    
    /**
     * Setter for property tipos.
     * @param tipos New value of property tipos.
     */
    public void setTipos(java.util.LinkedList tipos) {
        this.tipos = tipos;
    }
    
    /**
     * Getter for property tipo_aux.
     * @return Value of property tipo_aux.
     */
    public java.lang.String getTipo_aux() {
        return tipo_aux;
    }
    
    /**
     * Setter for property tipo_aux.
     * @param tipo_aux New value of property tipo_aux.
     */
    public void setTipo_aux(java.lang.String tipo_aux) {
        this.tipo_aux = tipo_aux;
    }
    
    /**
     * Getter for property cuenta_banco.
     * @return Value of property cuenta_banco.
     */
    public java.lang.String getCuenta_banco() {
        return cuenta_banco;
    }
    
    /**
     * Setter for property cuenta_banco.
     * @param cuenta_banco New value of property cuenta_banco.
     */
    public void setCuenta_banco(java.lang.String cuenta_banco) {
        this.cuenta_banco = cuenta_banco;
    }
    
    /**
     * Getter for property tasaDolBol.
     * @return Value of property tasaDolBol.
     */
    public double getTasaDolBol() {
        return tasaDolBol;
    }
    
    /**
     * Setter for property tasaDolBol.
     * @param tasaDolBol New value of property tasaDolBol.
     */
    public void setTasaDolBol(double tasaDolBol) {
        this.tasaDolBol = tasaDolBol;
    }
    
    /**
     * Getter for property abc.
     * @return Value of property abc.
     */
    public java.lang.String getAbc() {
        return abc;
    }
    
    /**
     * Setter for property abc.
     * @param abc New value of property abc.
     */
    public void setAbc(java.lang.String abc) {
        this.abc = abc;
    }
    
    /**
     * Getter for property pagina.
     * @return Value of property pagina.
     */
    public java.lang.String getPagina() {
        return pagina;
    }
    
    /**
     * Setter for property pagina.
     * @param pagina New value of property pagina.
     */
    public void setPagina(java.lang.String pagina) {
        this.pagina = pagina;
    }
    
    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }
    
    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }

    /**
     * @return the nro_extracto
     */
    public int getNro_extracto() {
        return nro_extracto;
    }

    /**
     * @param nro_extracto the nro_extracto to set
     */
    public void setNro_extracto(int nro_extracto) {
        this.nro_extracto = nro_extracto;
    }
    
}
