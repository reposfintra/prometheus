/*
 * Nombre        UbicacionVehicularPropietario.java
 * Descripci�n
 * Autor         Ivan Dario Gomez Vanegas
 * Fecha         1 de febrero de 2006, 16:16
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.model.beans;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.*;

public class UbicacionVehicularPropietario implements java.io.Serializable {
    // Objeto utilizado para logs.
    static transient Logger logger = Logger.getLogger(UbicacionVehicularPropietario.class);
    
    //------------------------------------------------------------------------------
    //PROPERTIES
    //------------------------------------------------------------------------------
    private String estado;
    private String destinatario;
    private String numPlanilla;
    private String pedidos;
    private String documentoInterno;
    private String origen;
    private String destino;
    private String conductor;
    private String celularConductor;
    private String numRemesa;
    private String fechaDespacho;
    private String fechaPosibleLlegada;
    private String ultimoReporte;
    private String tiempoTransito;
    private String observaciones;
    private String devoluciones;
    private String crossDocking;
    private String equipo;
    private String pesoReal;
    private String zonaDisponibilidad;
    private String descripcionOT;
    private String grupoEquipoCabezote;
    private String nomCli;
    private String tipo;
    private String capacidad;
    private String trailer;
    private String descripcionTrailer;
    private String f_d;
    private String tipoCarga;
    private String tipoDeViaje;
    private String grupoEquipoTrailer;
    private String origenOT;
    private String destinoOT;
    private Hashtable tablaDeCampos;
    
    /**
     * Holds value of property saldoPlanilla.
     */
    private String saldoPlanilla;
    
    //------------------------------------------------------------------------------
    //CONSTRUCTOR
    //------------------------------------------------------------------------------
    /**
     * Crea una instancia de esta clase
     */    
    public UbicacionVehicularPropietario() {
        this.estado = "";
        this.destinatario = "";
        this.numPlanilla = "";
        this.pedidos = "";
        this.documentoInterno = "";
        this.origen = "";
        this.destino = "";
        this.conductor = "";
        this.celularConductor = "";
        this.numRemesa = "";
        this.fechaDespacho = "";
        this.fechaPosibleLlegada = "";
        this.ultimoReporte = "";
        this.tiempoTransito = "";
        this.observaciones = "";
        this.devoluciones = "";
        this.crossDocking = "";
        this.equipo = "";
        this.pesoReal = "";
        this.zonaDisponibilidad = "";
        this.origenOT = "";
        this.destinoOT = "";
        this.descripcionOT = "";
        this.grupoEquipoCabezote = "";
        this.grupoEquipoTrailer = "";
        this.nomCli = "";
        this.tipo = "";
        this.capacidad = "";
        this.trailer = "";
        this.descripcionTrailer = "";
        this.f_d = "";
        this.tipoCarga = "";
        this.tipoDeViaje = "";
    }
    
    
    /**
     * Crea un hashtable con los pares clave = campos[n] y valor = rs.getString(campos[n]).
     * Ademas filtra que no se vayan datos nulos y los reemplaza con el caracter (gui�n '-').
     * @param rs el ResultSet de donde se sacar�n los datos.
     * @param campos Los campos del reporte
     * @throws SQLException Si algun error ocurre en el acceso a la base de datos.
     * @autor Alejandro Payares
     */    
    private void crearTablaDeCampos(ResultSet rs, String [] campos) throws SQLException{
        tablaDeCampos = new Hashtable();
        for( int i=0; i<campos.length; i++ ){
            if ( campos[i].equals("estado") ){
                tablaDeCampos.put(campos[i], this.getEstado());
            }
            else {
                String str = rs.getString(campos[i]);
                tablaDeCampos.put(campos[i], str == null?"-":str);
            }
        }
    }
    
    /**
     * Cambia el valor asociado al campo dado en el Hashtable
     * @param campo el nombre del campo a cambiar
     * @param valor el nuevo valor del campo
     * @autor Alejandro Payares
     */    
    public void establecerValor(String campo, String valor){
        if ( tablaDeCampos != null ){
            tablaDeCampos.put(campo, valor);
        }
    }
    
    /**
     * Devuelve el valor correspondiente al nombre campo dado.
     * @param campo el nombre del campo
     * @return El valor del campo dado enel Hashtable
     * @autor Alejandro Payares
     */    
    public String obtenerValor(String campo){
        if ( tablaDeCampos != null ){
            return ""+tablaDeCampos.get(campo);
        }
        return "campo "+campo+" no existe";
    }
    
    //------------------------------------------------------------------------------
    //BEAN PATTERNS (GETTER AND SETTER METHODS FOR PROPERTIES)
    //------------------------------------------------------------------------------
    /**<PRE>
     *       Retorna los valores de los campos que hacen parte del encabezado
     *       del reporte.
     * </PRE>
     * @return Fields of the report header.
     */
  /*public Hashtable getReportHeader()
  {
    Hashtable headerValues = new Hashtable();
    headerValues.clear();
    String nombreCliente = (request.getParameter("nombreCliente") == null)?"":request.getParameter("nombreCliente");
    String fechaini      = (request.getParameter("fechaini")      == null)?"":request.getParameter("fechaini");
    String estadoViajes  = (request.getParameter("estadoViajes")  == null)?"":request.getParameter("estadoViajes");
           estadoViajes  = (estadoViajes.equals("RUTA") ? "En Ruta" :
                           (estadoViajes.equals("PORCONF") ? "Por Confirmar Salida" :
                           (estadoViajes.equals("CONENTREGA") ? "Con Entrega" : "TODOS")));
   
    String placasTrailers = (request.getParameter("placasTrailers") == null )?"":request.getParameter("placasTrailers");
    String tipoBusqueda   = (request.getParameter("tipoBusqueda")   == null )?"":request.getParameter("tipoBusqueda");
    String planillas      = (request.getParameter("planillas")      == null )?"":request.getParameter("planillas");
    String listaTipoViaje = (request.getParameter("listaTipoViaje") == null )?"":request.getParameter("listaTipoViaje");
    String fechafin       = (request.getParameter("fechafin")       == null )?"":request.getParameter("fechafin");
   
   
    if( !placasTrailers.equals("") )
      headerValues.put( "placasTrailers", placasTrailers );
    headerValues.put( "tipoBusqueda", tipoBusqueda );
   
    if(fechaini != null && !fechaini.equals("")){
      headerValues.put( "fechaInicial", fechaini );
      headerValues.put( "fechaFinal",   fechafin);
    }
   
    if(planillas!=null &&  !planillas.equals("") )
      headerValues.put( "planillas", planillas );
   
    headerValues.put( "estadoViajes",   estadoViajes );
    headerValues.put( "listaTipoViaje", listaTipoViaje );
   
    return headerValues;
  }*/
    
    public static UbicacionVehicularPropietario load(ResultSet rs) throws SQLException {
        return load(rs,null); 
    }
    
    /**
     * Metodo factoria para crear un registro del reporte de la fila activa
     * de un resultado dado
     * @return Un registro del reporte.
     * @param campos los nombres de los campos del reporte.
     * @param rs es un resultado dado a partir de la consulta del reporte.
     * @throws SQLException si aparece un error de base de datos
     */
    public static UbicacionVehicularPropietario load(ResultSet rs, String [] campos) throws SQLException {
        UbicacionVehicularPropietario rptUbVeh = new UbicacionVehicularPropietario();
        String value = null;
        
        value = rs.getString("DESTINATARIO");
        if(value != null) rptUbVeh.setDestinatario(value, rs.getString("DOCUINTERNO"));
        
        value = rs.getString("NUMPLA");
        if(value != null) rptUbVeh.setNumPlanilla(value);
        
        value = rs.getString("PEDIDOS");
        if(value != null) rptUbVeh.setPedidos(value);
        
        value = rs.getString("DOCUINTERNO");
        if(value != null) rptUbVeh.setDocumentoInterno(value);
        
        value = rs.getString("ORINOM");
        if(value != null) rptUbVeh.setOrigen(value);
        
        value = rs.getString("DESNOM");
        if(value != null) rptUbVeh.setDestino(value);
        
        value = rs.getString("ORIREM");
        if(value != null) rptUbVeh.setOrigenOT(value);
        
        value = rs.getString("DESREM");
        if(value != null) rptUbVeh.setDestinoOT(value);
        
        value = rs.getString("NOMCON");
        if(value != null) rptUbVeh.setConductor(value);
        
        value = rs.getString("CELULARCONDUCTOR");
        if(value != null) rptUbVeh.setCelularConductor(value);
        
        value = rs.getString("NUMREM");
        if(value != null) rptUbVeh.setNumRemesa(value);
        
        value = rs.getString("FECDSP");
        if(value != null) rptUbVeh.setFechaDespacho(value);
        
        value = rs.getString("FECHAPOSLLEGADA");
        if(value != null) rptUbVeh.setFechaPosibleLlegada(value);
        
        value = rs.getString("ULTIMOREPORTE");
        if(value != null) rptUbVeh.setUltimoReporte(value);
        
        value = rs.getString("TIEMPOENTRANSITO");
        if(value != null) rptUbVeh.setTiempoTransito(value);
        
        value = rs.getString("OBSERVACION"); // Posicion 15
        if(value != null) rptUbVeh.setObservaciones(value);
        
       
        // Preguntar si la propiedad "equipo" corresponde a la placa del veh�culo.
        value = rs.getString("EQUIPO");
        if(value != null) rptUbVeh.setEquipo(value);
        
        value = rs.getString("PESOREAL");
        if(value != null) rptUbVeh.setPesoReal(value);
        
        value = rs.getString("ZONADISPONIBILIDAD");
        if(value != null) rptUbVeh.setZonaDisponibilidad(value);
        
        value = rs.getString("DESCRIPCION");
        if(value != null) rptUbVeh.setDescripcionOT(value);
        
        value = rs.getString("WGEQUIPO");
        if(value != null) rptUbVeh.setGrupoEquipoCabezote(value);
        
        value = rs.getString("NOMCLI");
        if(value != null) rptUbVeh.setNomCli(value);
        
        value = rs.getString("CAPACIDAD");
        if(value != null) rptUbVeh.setCapacidad(value);
        
        value = rs.getString("TIPO");
        if (value != null) rptUbVeh.setTipo(value);
        
        value = rs.getString("desctipocarga");
        if (value != null) rptUbVeh.setTipoCarga(value);
        
        value = rs.getString("saldo");
        if (value != null) rptUbVeh.setSaldoPlanilla(value);
        
        //               logger.info("*** Grupo Equipo: " + rptUbVeh.getGrupoEquipo() );
        
        // Configura la variable especial "estado"
        rptUbVeh.setEstado( rptUbVeh );
        if ( campos != null ){
            rptUbVeh.crearTablaDeCampos(rs, campos);
        }
        return rptUbVeh;  
    }
    
    /**
     * <PRE>
     *         Initializa las propiedades del bean con los par�metros de p�gina.
     * </PRE>
     * @param initialize Si es <code>true</code>, el bean es initializado;
     *                   <code>false</code> en caso contrario.
     * @throws Exception Si ocurre un error durante la inicializaci�n.
     */
    public void setInitialization(boolean initialize) throws Exception {
    }
    
    /**
     * Getter for property destinatario.
     * @return Value of property destinatario.
     */
    public String getDestinatario() {
        return this.destinatario;
    }
    
    /**
     * Setter for property destinatario.
     * @param nuevoDestinatario New value of property destinatario.
     */
    public void setDestinatario(String newDestinatario, String DocuInterno) throws SQLException {
        String[] vDest = DocuInterno.split("/");
        StringBuffer cDest = new StringBuffer(0);
        if (vDest.length > 0) {
            for (int i=0; i < vDest.length; i++ ) {
                String[] vDest2 = vDest[ i ].split(":");
                if (vDest2.length > 1 ) {
                    RemiDest desti = null; //this.mvcModel.getRemiDest( vDest2[1] );
                    if (desti == null ) {
                        cDest.append( vDest2[1]+" - null " + "<br>");
                    } else {
                        cDest.append( desti.getNombre() + "<br>");
                    }
                }
            }
        }
        if ( cDest.length() <= 0 ) {
            cDest.append( newDestinatario );
        }
        this.destinatario = cDest.toString();
    }
    
    /**
     * Retorna el nombre del destinatario tomando como base el valor del
     * campo "Documento Interno" separados por linea. Luego busca el nombre del
     * destinatario en la tabla REMIDES
     * @return Campo "Documento Interno".
     * @throws SQLException si un error de acceso a base de datos ocurre.
     */
    public String getDestinatarioDiv() throws SQLException {
        String[] vDest = this.getDocumentoInterno().split("/");
        if (vDest.length > 0) {
            StringBuffer cDest = new StringBuffer(0);
            for (int i=0; i < vDest.length; i++ ) {
                String[] vDest2 = vDest[ i ].split(":");
                if (vDest2.length > 1 ) {
                    RemiDest desti = null; //this.mvcModel.getRemiDest( vDest2[1] );
                    cDest.append( vDest2[1]+" - " + "<br>"); //+desti.getNombre()
                }
            }
            return cDest.toString();
        } else {
            return "** " + this.destinatario;
        }
    }
    
    /**
     * Getter for property numPlanilla.
     * @return Value of property numPlanilla.
     */
    public String getNumPlanilla() {
        return this.numPlanilla;
    }
    
    /**
     * Setter for property numPlanilla.
     * @param nuevoNumPlanilla New value of property numPlanilla.
     */
    public void setNumPlanilla(String nuevoNumPlanilla) {
        this.numPlanilla = nuevoNumPlanilla;
    }
    
    /**
     * Retorna el valor del campo "Pedidos" (la factura comercial).
     * @return Value of property pedidos.
     */
    public String getPedidos() {
        return this.pedidos;
    }
    
    /**
     * Retorna el campo "Pedidos" (la factura comercial), pero separada
     * por cada ocurrencia de '/'
     * @return Campo "Pedidos" seccionado.
     */
    public String getPedidosDiv() {
        String[] facturas = this.getPedidos().split("/");
        StringBuffer cFactCial = new StringBuffer(0);
        for (int i=0; i < facturas.length; i++ ) {
            cFactCial.append( facturas[i] + "<br>");
        }
        if( cFactCial.length() == 0 ) {
            cFactCial.append("-");
        }
        return cFactCial.toString();
    }
    
    /**
     * Setter for property pedidos.
     * @param nuevoPedidos New value of property pedidos.
     */
    public void setPedidos(String nuevoPedidos) {
        this.pedidos = nuevoPedidos;
    }
    
    /**
     * Getter for property documentoInterno.
     * @return Value of property documentoInterno.
     */
    public String getDocumentoInterno() {
        return this.documentoInterno;
    }
    
    /**
     * Setter for property documentoInterno.
     * @param nuevoDocumentoInterno New value of property documentoInterno.
     */
    public void setDocumentoInterno(String nuevoDocumentoInterno) {
        this.documentoInterno = nuevoDocumentoInterno ;
    }
    
    /**
     * Retorna el valor del campo "Documento Interno" separados por linea y
     * sin el codigo del destinatario
     * @return Campo "Documento Interno".
     */
    public String getDocumentoInternoDiv() {
        String[] vDocInt = this.getDocumentoInterno().split("/");
        StringBuffer cDocInt = new StringBuffer(0);
        for (int i=0; i < vDocInt.length; i++ ) {
            String[] vDocInt2 = vDocInt[ i ].split(":");
            if (vDocInt2.length > 0 ) {
                cDocInt.append( vDocInt2[0] + "<br>");
            }
        }
        return cDocInt.toString();
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public String getOrigen() {
        return this.origen;
    }
    
    /**
     * Setter for property origen.
     * @param nuevoOrigen New value of property origen.
     */
    public void setOrigen(String nuevoOrigen) {
        this.origen = nuevoOrigen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public String getDestino() {
        return this.destino;
    }
    
    /**
     * Setter for property destino.
     * @param nuevoDestino New value of property destino.
     */
    public void setDestino(String nuevoDestino) {
        this.destino = nuevoDestino;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public String getConductor() {
        return this.conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param nuevoConductor New value of property conductor.
     */
    public void setConductor(String nuevoConductor) {
        this.conductor = nuevoConductor;
    }
    
    /**
     * Getter for property celularConductor.
     * @return Value of property celularConductor.
     */
    public String getCelularConductor() {
        return this.celularConductor;
    }
    
    /**
     * Setter for property celularConductor.
     * @param nuevoCelularConductor New value of property celularConductor.
     */
    public void setCelularConductor(String nuevoCelularConductor) {
        this.celularConductor = nuevoCelularConductor;
    }
    
    /**
     * Getter for property numRemesa.
     * @return Value of property numRemesa.
     */
    public String getNumRemesa() {
        return this.numRemesa;
    }
    
    /**
     * Setter for property numRemesa.
     * @param nuevoNumRemesa New value of property numRemesa.
     */
    public void setNumRemesa(String nuevoNumRemesa) {
        this.numRemesa = nuevoNumRemesa;
    }
    
    /**
     * Getter for property fechaDespacho.
     * @return Value of property fechaDespacho.
     */
    public String getFechaDespacho() {
        return this.fechaDespacho;
    }
    
    /**
     * Setter for property fechaDespacho.
     * @param nuevaFechaDespacho New value of property fechaDespacho.
     */
    public void setFechaDespacho(String nuevaFechaDespacho) {
        this.fechaDespacho = nuevaFechaDespacho;
    }
    
    /**
     * Getter for property fechaPosibleLlegada.
     * @return Value of property fechaPosibleLlegada.
     */
    public String getFechaPosibleLlegada() {
        return this.fechaPosibleLlegada;
    }
    
    /**
     * Setter for property fechaPosibleLlegada.
     * @param nuevaFechaPosibleLlegada New value of property fechaPosibleLlegada.
     */
    public void setFechaPosibleLlegada(String nuevaFechaPosibleLlegada) {
        this.fechaPosibleLlegada = nuevaFechaPosibleLlegada;
    }
    
    /**
     * Getter for property ultimoReporte.
     * @return Value of property ultimoReporte.
     */
    public String getUltimoReporte() {
        return this.ultimoReporte;
    }
    
    /**
     * Setter for property ultimoReporte.
     * @param nuevoUltimoReporte New value of property ultimoReporte.
     */
    public void setUltimoReporte(String nuevoUltimoReporte) {
        this.ultimoReporte = nuevoUltimoReporte;
    }
    
    /**
     * Getter for property tiempoTransito.
     * @return Value of property tiempoTransito.
     */
    public String getTiempoTransito() {
        return this.tiempoTransito;
    }
    
    /**
     * Setter for property tiempoTransito.
     * @param nuevoTiempoTransito New value of property tiempoTransito.
     */
    public void setTiempoTransito(String nuevoTiempoTransito) {
        this.tiempoTransito = nuevoTiempoTransito;
    }
    
    /**
     * Getter for property observaciones.
     * @return Value of property observaciones.
     */
    public String getObservaciones() {
        return this.observaciones;
    }
    
    /**
     * Setter for property observaciones.
     * @param nuevoObservaciones New value of property observaciones.
     */
    public void setObservaciones(String nuevoObservaciones) {
        this.observaciones = nuevoObservaciones;
    }
    
    /**
     * Getter for property devoluciones.
     * @return Value of property devoluciones.
     */
    public String getDevoluciones() {
        return this.devoluciones;
    }
    
    /**
     * Setter for property devoluciones.
     * @param nuevasDevoluciones New value of property devoluciones.
     */
    public void setDevoluciones(String nuevasDevoluciones) {
        this.devoluciones = nuevasDevoluciones;
    }
    
    /**
     * Getter for property crossDocking.
     * @return Value of property crossDocking.
     */
    public String getCrossDocking() {
        return this.crossDocking;
    }
    
    /**
     * Setter for property crossDocking.
     * @param nuevoCrossDocking New value of property crossDocking.
     */
    public void setCrossDocking(String nuevoCrossDocking) {
        this.crossDocking = nuevoCrossDocking;
    }
    
    /**
     * Getter for property equipo.
     * @return Value of property equipo.
     */
    public String getEquipo() {
        return this.equipo;
    }
    
    /**
     * Setter for property equipo.
     * @param nuevoEquipo New value of property equipo.
     */
    public void setEquipo(String nuevoEquipo) {
        this.equipo = nuevoEquipo;
    }
    
    /**
     * Getter for property pesoReal.
     * @return Value of property pesoReal.
     */
    public String getPesoReal() {
        return this.pesoReal;
    }
    
    /**
     * Setter for property pesoReal.
     * @param nuevoPesoReal New value of property pesoReal.
     */
    public void setPesoReal(String nuevoPesoReal) {
        this.pesoReal = nuevoPesoReal;
    }
    
    /**
     * Getter para capturar la zona de disponibilidad.
     * La zona de disponibilidad no es mas que las agencia
     * asociada al destino de la OC
     * @return La propiedad -> zonaDisponibilidad.
     */
    public String getZonaDisponibilidad() {
        return this.zonaDisponibilidad;
    }
    
    /**
     * Setter para zona de disponibilidad.
     * @param zonaDisponibilidad de la propiedad zonaDisponibilidad.
     */
    public void setZonaDisponibilidad(String nuevaZona) {
        this.zonaDisponibilidad = nuevaZona;
    }
    
    /**
     * Getter para capturar la descripcion de la OT.
     * La descripcion de la OT contiene el cliente y la carga
     * y esta asociada a la remesa.
     * @return La propiedad -> zonaDisponibilidad.
     */
    public String getDescripcionOT() {
        return this.descripcionOT;
    }
    
    /**
     * Setter para la descripcion de la OT o remesa.
     * @param nuevaDescripcion de la propiedad descripcionOT.
     */
    public void setDescripcionOT(String nuevaDescripcion) {
        this.descripcionOT = nuevaDescripcion;
    }
    
    /**
     * Getter para capturar el grupo al que pertenece el equipo.
     * Esta informacion esta contenida en la tabla PLACA y se refiere
     * a euipos como cabezote, trailer o contenedores.
     * @return La propiedad -> grupoEquipo.
     */
    public String getGrupoEquipoCabezote() {
        return this.grupoEquipoCabezote;
    }
    
    /**
     * Setter para el grupo del equipo
     * @param nuevoGrupo de la propiedad grupoEquipo.
     */
    public void setGrupoEquipoCabezote(String nuevoGrupo) {
        this.grupoEquipoCabezote = nuevoGrupo;
    }
    
    public String getNomCli() {
        return this.nomCli;
    }
    
    public void setNomCli(String nuevoCliente) {
        this.nomCli = nuevoCliente;
    }
    
    public void setEstado(UbicacionVehicularPropietario rpt ) {
        if ( rpt.getUltimoReporte().endsWith("-E") ) {
            this.estado = "Con Entrega"  ;
        } else if (rpt.getUltimoReporte().trim().equals("") || rpt.getUltimoReporte().trim().equals("-")) {
            this.estado = "Por Confirmar Salida"  ;
        }  else {
            this.estado = "En Ruta"  ;
        }
    }
    
    public String getEstado() {
        return this.estado;
    }
    
    public String getTipo() {
        return this.tipo;
    }
    
    public void setTipo(String nuevoTipo) {
        this.tipo = nuevoTipo;
    }
    
    public String getCapacidad() {
        return this.capacidad;
    }
    
    public void setCapacidad(String nuevoCapacidad) {
        this.capacidad = nuevoCapacidad;
    }
    
    public String getTrailer() {
        return this.trailer;
    }
    
    public void setTrailer(String nuevoTrailer) {
        this.trailer = nuevoTrailer;
    }
    
    public String getDescripcionTrailer() {
        return this.descripcionTrailer;
    }
    
    public void setDescripcionTrailer(String nuevaDescripcionTrailer ) {
        this.descripcionTrailer = nuevaDescripcionTrailer;
    }
    
    public String getF_D() {
        return this.f_d;
    }
    
    public void setF_D(String nuevoF_D) {
        this.f_d = nuevoF_D;
    }
    
    /**
     * Getter for property tipoDeViaje.
     * @return Value of property tipoDeViaje.
     */
    public String getTipoDeViaje() {
        return this.tipoDeViaje;
    }
    
    /**
     * Setter for property tipoDeViaje.
     * @param tipoDeViaje New value of property tipoDeViaje.
     */
    public void setTipoDeViaje(String tipoDeViaje) {
        this.tipoDeViaje = tipoDeViaje;
    }
    
    /**
     * Getter for property tipoCarga.
     * @return Value of property tipoCarga.
     */
    public java.lang.String getTipoCarga() {
        return tipoCarga;
    }
    
    /**
     * Setter for property tipoCarga.
     * @param tipoCarga New value of property tipoCarga.
     */
    public void setTipoCarga(java.lang.String tipoCarga) {
        this.tipoCarga = tipoCarga;
    }
    
    /**
     * Getter for property grupoEquipoTrailer.
     * @return Value of property grupoEquipoTrailer.
     */
    public String getGrupoEquipoTrailer() {
        return this.grupoEquipoTrailer;
    }
    
    /**
     * Setter for property grupoEquipoTrailer.
     * @param grupoEquipoTrailer New value of property grupoEquipoTrailer.
     */
    public void setGrupoEquipoTrailer(String grupoEquipoTrailer) {
        this.grupoEquipoTrailer = grupoEquipoTrailer;
    }
    
    /**
     * Getter for property origenOt.
     * @return Value of property origenOt.
     */
    public String getOrigenOT() {
        return this.origenOT;
    }
    
    /**
     * Setter for property origenOt.
     * @param origenOt New value of property origenOt.
     */
    public void setOrigenOT(String origenOT) {
        this.origenOT = origenOT;
    }
    
    /**
     * Getter for property destinoOt.
     * @return Value of property destinoOt.
     */
    public String getDestinoOT() {
        return this.destinoOT;
    }
    
    /**
     * Setter for property destinoOt.
     * @param destinoOt New value of property destinoOt.
     */
    public void setDestinoOT(String destinoOT) {
        this.destinoOT = destinoOT;
    }
    
    /**
     * Getter for property saldoPlanilla.
     * @return Value of property saldoPlanilla.
     */
    public String getSaldoPlanilla() {
        return this.saldoPlanilla;
    }
    
    /**
     * Setter for property saldoPlanilla.
     * @param saldoPlanilla New value of property saldoPlanilla.
     */
    public void setSaldoPlanilla(String saldoPlanilla) {
        this.saldoPlanilla = saldoPlanilla;
    }
    
}