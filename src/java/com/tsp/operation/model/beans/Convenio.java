/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author maltamiranda
 */
public class Convenio implements Serializable{


    private String reg_status, dstrct, id_convenio, nombre, descripcion, nit_convenio,
            nit_tercero, cuenta_interes,
            cuenta_custodia, prefijo_negocio, prefijo_cxp, cuenta_cxp, hc_cxp,
            cuota_gmf, prefijo_nc_gmf, cuenta_gmf, cuenta_gmf2,
            prefijo_nc_aval, cuenta_aval, prefijo_diferidos, cuenta_diferidos,
            hc_diferidos, creation_user, user_update, creation_date, last_update ,agencia;
    private boolean factura_tercero, descuenta_gmf, descuenta_aval, mediador_aval, aval_tercero;
    private double valor_custodia, tasa_interes;
    private double porc_gmf, porc_gmf2;//2010-09-27
    private ArrayList<ConvenioCxc> convenioCxc;
    private ArrayList<ConveniosRemesas> conveniosRemesas;
    private ArrayList<ConvenioComision> convenioComision;
    private String impuesto;
    private String cuenta_ajuste;
    private String prefijo_endoso, hc_endoso;
    private ArrayList<ConvenioFiducias> convenioFiducias;
    private String nit_mediador, nit_central, nit_capacitador, nit_asegurador;
    private boolean central, capacitacion, seguro, cat;
    private String tipo, prefijo_cxc_interes, prefijo_cxp_central, cuenta_central, cuenta_com_central,
            prefijo_cxc_cat, cuenta_cat, cuenta_capacitacion, cuenta_seguro, cuenta_com_seguro, plazo_maximo;
    private double valor_central, valor_com_central, porcentaje_cat, valor_capacitacion, valor_seguro,
            porcentaje_com_seguro, monto_minimo, monto_maximo;
    private boolean redescuento;
    private boolean aval_anombre, cxp_avalista;
    private String nit_anombre, prefijo_cxc_aval, hc_cxc_aval, cuenta_cxc_aval, cctrl_db_cxc_aval, cctrl_cr_cxc_aval, cctrl_iva_cxc_aval,
            prefijo_cxp_avalista, hc_cxp_avalista, cuenta_cxp_avalista, prefijo_cxp_fianza, hc_cxp_fianza, cuenta_cuota_administracion,prefijo_cuota_admin,hc_cuota_admin,cta_cuota_admin_diferido;
    private Convenio convenio;
    private ArrayList<CargosFijosConvenios> cargos_fijos_convenios;
    private String id_sucursal;
    private double tasa_compra_cartera;
    private Unidad_Negocio unidad_negocio ;
    
    private double tasa_max_fintra;
    private double tasa_sic_EA;

    public double getTasa_sic_EA() {
        return tasa_sic_EA;
    }

    public void setTasa_sic_EA(double tasa_sic_EA) {
        this.tasa_sic_EA = tasa_sic_EA;
    }

    public double getTasa_Max_Fintra() {
        return tasa_max_fintra;
    }

    public void setTasa_Max_Fintra(double tasa_max_fintra) {
        this.tasa_max_fintra = tasa_max_fintra;
    }
    

    public String getHc_cuota_admin() {
        return hc_cuota_admin;
    }

    public void setHc_cuota_admin(String hc_cuota_admin) {
        this.hc_cuota_admin = hc_cuota_admin;
    }

    public String getPrefijo_cuota_admin() {
        return prefijo_cuota_admin;
    }

    public void setPrefijo_cuota_admin(String prefijo_cuota_admin) {
        this.prefijo_cuota_admin = prefijo_cuota_admin;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    
    public boolean isCat() {
        return cat;
    }

    public void setCat(boolean cat) {
        this.cat = cat;
    }  
    
    public boolean isRedescuento() {
        return redescuento;
    }

    public void setRedescuento(boolean redescuento) {
        this.redescuento = redescuento;
    }
    
    public boolean isAval_anombre() {
        return aval_anombre;
    }

    public void setAval_anombre(boolean aval_anombre) {
        this.aval_anombre = aval_anombre;
    }

    public String getCctrl_db_cxc_aval() {
        return cctrl_db_cxc_aval;
    }

    public void setCctrl_db_cxc_aval(String cctrl_db_cxc_aval) {
        this.cctrl_db_cxc_aval = cctrl_db_cxc_aval;
    }

    public String getCctrl_iva_cxc_aval() {
        return cctrl_iva_cxc_aval;
    }

    public void setCctrl_iva_cxc_aval(String cctrl_iva_cxc_aval) {
        this.cctrl_iva_cxc_aval = cctrl_iva_cxc_aval;
    }

    public String getCuenta_cxc_aval() {
        return cuenta_cxc_aval;
    }

    public void setCuenta_cxc_aval(String cuenta_cxc_aval) {
        this.cuenta_cxc_aval = cuenta_cxc_aval;
    }

    public String getCuenta_cxp_avalista() {
        return cuenta_cxp_avalista;
    }

    public void setCuenta_cxp_avalista(String cuenta_cxp_avalista) {
        this.cuenta_cxp_avalista = cuenta_cxp_avalista;
    }

    public boolean isCxp_avalista() {
        return cxp_avalista;
    }

    public void setCxp_avalista(boolean cxp_avalista) {
        this.cxp_avalista = cxp_avalista;
    }

    public String getHc_cxc_aval() {
        return hc_cxc_aval;
    }

    public void setHc_cxc_aval(String hc_cxc_aval) {
        this.hc_cxc_aval = hc_cxc_aval;
    }

    public String getHc_cxp_avalista() {
        return hc_cxp_avalista;
    }

    public void setHc_cxp_avalista(String hc_cxp_avalista) {
        this.hc_cxp_avalista = hc_cxp_avalista;
    }

    public String getNit_anombre() {
        return nit_anombre;
    }

    public void setNit_anombre(String nit_anombre) {
        this.nit_anombre = nit_anombre;
    }

    public String getPrefijo_cxc_aval() {
        return prefijo_cxc_aval;
    }

    public void setPrefijo_cxc_aval(String prefijo_cxc_aval) {
        this.prefijo_cxc_aval = prefijo_cxc_aval;
    }

    public String getPrefijo_cxp_avalista() {
        return prefijo_cxp_avalista;
    }

    public void setPrefijo_cxp_avalista(String prefijo_cxp_avalista) {
        this.prefijo_cxp_avalista = prefijo_cxp_avalista;
    }

    public String getCctrl_cr_cxc_aval() {
        return cctrl_cr_cxc_aval;
    }

    public void setCctrl_cr_cxc_aval(String cctrl_cr_cxc_aval) {
        this.cctrl_cr_cxc_aval = cctrl_cr_cxc_aval;
    }

    /**
     * Get the value of cuenta_ajuste
     *
     * @return the value of cuenta_ajuste
     */
    public String getCuenta_ajuste() {
        return cuenta_ajuste;
    }

    /**
     * Set the value of cuenta_ajuste
     *
     * @param cuenta_ajuste new value of cuenta_ajuste
     */
    public void setCuenta_ajuste(String cuenta_ajuste) {
        this.cuenta_ajuste = cuenta_ajuste;
    }

    /**
     * Get the value of impuesto
     *
     * @return the value of impuesto
     */
    public String getImpuesto() {
        return impuesto;
    }

    /**
     * Set the value of impuesto
     *
     * @param impuesto new value of impuesto
     */
    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public void addConvenioCxc(ConvenioCxc c) {
        convenioCxc.add(c);
    }

    public void addConveniosRemesas(ConveniosRemesas c) {
        conveniosRemesas.add(c);
    }

    public void addConvenioComision(ConvenioComision c) {
        convenioComision.add(c);
    }

    public Convenio() {
        convenioCxc=new ArrayList();
        conveniosRemesas=new ArrayList();
        convenioComision=new ArrayList();
        this.reg_status = "";
        this.dstrct = "";
        this.id_convenio = "";
        this.nombre = "";
        this.descripcion = "";
        this.nit_convenio = "";
        this.nit_tercero = "";
        this.cuenta_interes = "";
        this.cuenta_custodia = "";
        this.prefijo_negocio = "";
        this.prefijo_cxp = "";
        this.cuenta_cxp = "";
        this.hc_cxp = "";
        this.cuota_gmf = "";
        this.prefijo_nc_gmf = "";
        this.cuenta_gmf = "";
        this.prefijo_nc_aval = "";
        this.cuenta_aval = "";
        this.prefijo_diferidos = "";
        this.cuenta_diferidos = "";
        this.hc_diferidos = "";
        this.creation_user = "";
        this.user_update = "";
        this.creation_date = "";
        this.last_update = "";
        this.factura_tercero = false;
        this.descuenta_gmf = false;
        this.descuenta_aval = false;
        this.valor_custodia = 0;
        this.tasa_interes = 0;
        this.id_sucursal="";
        this.tasa_compra_cartera=0;
    }

    public Convenio(ArrayList <ConvenioCxc> convenioCxc,ArrayList <ConveniosRemesas> conveniosRemesas,ArrayList <ConvenioComision> convenioComision,ArrayList <ConvenioFiducias> convenioFiducia) {
        this.convenioCxc=convenioCxc;
        this.conveniosRemesas=conveniosRemesas;
        this.convenioComision=convenioComision;
        this.convenioFiducias=convenioFiducia;
        this.reg_status = "";
        this.dstrct = "";
        this.id_convenio = "";
        this.nombre = "";
        this.descripcion = "";
        this.nit_convenio = "";
        this.nit_tercero = "";
        this.cuenta_interes = "";
        this.cuenta_custodia = "";
        this.prefijo_negocio = "";
        this.prefijo_cxp = "";
        this.cuenta_cxp = "";
        this.hc_cxp = "";
        this.cuota_gmf = "";
        this.prefijo_nc_gmf = "";
        this.cuenta_gmf = "";
        this.prefijo_nc_aval = "";
        this.cuenta_aval = "";
        this.prefijo_diferidos = "";
        this.cuenta_diferidos = "";
        this.hc_diferidos = "";
        this.creation_user = "";
        this.user_update = "";
        this.creation_date = "";
        this.last_update = "";
        this.factura_tercero = false;
        this.descuenta_gmf = false;
        this.descuenta_aval = false;
        this.valor_custodia = 0;
        this.tasa_interes = 0;
        this.id_sucursal="";
        this.tasa_compra_cartera=0;
    }

    public String getCuenta_gmf2() {
        return cuenta_gmf2;
    }

    public void setCuenta_gmf2(String cuenta_gmf2) {
        this.cuenta_gmf2 = cuenta_gmf2;
    }

    public double getPorc_gmf2() {
        return porc_gmf2;
    }

    public void setPorc_gmf2(double porc_gmf2) {
        this.porc_gmf2 = porc_gmf2;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getCuenta_aval() {
        return cuenta_aval;
    }

    public void setCuenta_aval(String cuenta_aval) {
        this.cuenta_aval = cuenta_aval;
    }

    public String getCuenta_custodia() {
        return cuenta_custodia;
    }

    public void setCuenta_custodia(String cuenta_custodia) {
        this.cuenta_custodia = cuenta_custodia;
    }

    public String getCuenta_cxp() {
        return cuenta_cxp;
    }

    public void setCuenta_cxp(String cuenta_cxp) {
        this.cuenta_cxp = cuenta_cxp;
    }

    public String getCuenta_diferidos() {
        return cuenta_diferidos;
    }

    public void setCuenta_diferidos(String cuenta_diferidos) {
        this.cuenta_diferidos = cuenta_diferidos;
    }

    public String getCuenta_gmf() {
        return cuenta_gmf;
    }

    public void setCuenta_gmf(String cuenta_gmf) {
        this.cuenta_gmf = cuenta_gmf;
    }

    public String getCuenta_interes() {
        return cuenta_interes;
    }

    public void setCuenta_interes(String cuenta_interes) {
        this.cuenta_interes = cuenta_interes;
    }

    public String getCuota_gmf() {
        return cuota_gmf;
    }

    public void setCuota_gmf(String cuota_gmf) {
        this.cuota_gmf = cuota_gmf;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isDescuenta_aval() {
        return descuenta_aval;
    }

    public void setDescuenta_aval(boolean descuenta_aval) {
        this.descuenta_aval = descuenta_aval;
    }

    public boolean isDescuenta_gmf() {
        return descuenta_gmf;
    }

    public void setDescuenta_gmf(boolean descuenta_gmf) {
        this.descuenta_gmf = descuenta_gmf;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public boolean isFactura_tercero() {
        return factura_tercero;
    }

    public void setFactura_tercero(boolean factura_tercero) {
        this.factura_tercero = factura_tercero;
    }

    public String getHc_cxp() {
        return hc_cxp;
    }

    public void setHc_cxp(String hc_cxp) {
        this.hc_cxp = hc_cxp;
    }

    public String getHc_diferidos() {
        return hc_diferidos;
    }

    public void setHc_diferidos(String hc_diferidos) {
        this.hc_diferidos = hc_diferidos;
    }

    public String getId_convenio() {
        return id_convenio;
    }

    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getNit_convenio() {
        return nit_convenio;
    }

    public void setNit_convenio(String nit_convenio) {
        this.nit_convenio = nit_convenio;
    }

    public String getNit_tercero() {
        return nit_tercero;
    }

    public void setNit_tercero(String nit_tercero) {
        this.nit_tercero = nit_tercero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrefijo_cxp() {
        return prefijo_cxp;
    }

    public void setPrefijo_cxp(String prefijo_cxp) {
        this.prefijo_cxp = prefijo_cxp;
    }

    public String getPrefijo_diferidos() {
        return prefijo_diferidos;
    }

    public void setPrefijo_diferidos(String prefijo_diferidos) {
        this.prefijo_diferidos = prefijo_diferidos;
    }

    public String getPrefijo_nc_aval() {
        return prefijo_nc_aval;
    }

    public void setPrefijo_nc_aval(String prefijo_nc_aval) {
        this.prefijo_nc_aval = prefijo_nc_aval;
    }

    public String getPrefijo_nc_gmf() {
        return prefijo_nc_gmf;
    }

    public void setPrefijo_nc_gmf(String prefijo_nc_gmf) {
        this.prefijo_nc_gmf = prefijo_nc_gmf;
    }

    public String getPrefijo_negocio() {
        return prefijo_negocio;
    }

    public void setPrefijo_negocio(String prefijo_negocio) {
        this.prefijo_negocio = prefijo_negocio;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public double getTasa_interes() {
        return tasa_interes;
    }

    public void setTasa_interes(double tasa_interes) {
        this.tasa_interes = tasa_interes;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public double getValor_custodia() {
        return valor_custodia;
    }

    public void setValor_custodia(double valor_custodia) {
        this.valor_custodia = valor_custodia;
    }

    public ArrayList<ConvenioCxc> getConvenioCxc() {
        return convenioCxc;
    }

    public void setConvenioCxc(ArrayList<ConvenioCxc> convenioCxc) {
        this.convenioCxc = convenioCxc;
    }

    public ArrayList<ConveniosRemesas> getConveniosRemesas() {
        return conveniosRemesas;
    }

    public void setConveniosRemesas(ArrayList<ConveniosRemesas> conveniosRemesas) {
        this.conveniosRemesas = conveniosRemesas;
    }

    public ArrayList<ConvenioComision> getConvenioComision() {
        return convenioComision;
    }

    public void setConvenioComision(ArrayList<ConvenioComision> convenioComision) {
        this.convenioComision = convenioComision;
    }

    public double getPorc_gmf() {
        return porc_gmf;
    }

    public void setPorc_gmf(double porc_gmf) {
        this.porc_gmf = porc_gmf;
    }

     public String getHc_endoso() {
        return hc_endoso;
    }

    public void setHc_endoso(String hc_endoso) {
        this.hc_endoso = hc_endoso;
    }

    public String getPrefijo_endoso() {
        return prefijo_endoso;
    }

    public void setPrefijo_endoso(String prefijo_endoso) {
        this.prefijo_endoso = prefijo_endoso;
    }

    public boolean isMediador_aval() {
        return mediador_aval;
    }

    public void setMediador_aval(boolean mediador_aval) {
        this.mediador_aval = mediador_aval;
    }

    public String getNit_mediador() {
        return nit_mediador;
    }

    public void setNit_mediador(String nit_mediador) {
        this.nit_mediador = nit_mediador;
    }

    public ConvenioCxc buscarTituloValor(String tipoTitulo) {
        ConvenioCxc tituloValor = null;
        for (int i = 0; i < convenioCxc.size(); i++) {
            ConvenioCxc convCxc = this.convenioCxc.get(i);
            if (tipoTitulo.equals(convCxc.getTitulo_valor())) {
                tituloValor = convCxc;
                break;
            }
        }
        return tituloValor;
    }

    public ConveniosRemesas buscarRemesa(int id_remesa) {
        ConveniosRemesas remesa = null;
        for (int i = 0; i < conveniosRemesas.size(); i++) {
            ConveniosRemesas convRemesa = this.conveniosRemesas.get(i);
            if (String.valueOf(id_remesa).equals(convRemesa.getId_remesa())) {
                remesa = convRemesa;
                break;
            }
        }
        return remesa;
    }
    
     public boolean isAval_tercero() {
        return aval_tercero;
    }

    public void setAval_tercero(boolean aval_tercero) {
        this.aval_tercero = aval_tercero;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isCapacitacion() {
        return capacitacion;
    }

    public void setCapacitacion(boolean capacitacion) {
        this.capacitacion = capacitacion;
    }

    public boolean isCentral() {
        return central;
    }

    public void setCentral(boolean central) {
        this.central = central;
    }

    public String getCuenta_capacitacion() {
        return cuenta_capacitacion;
    }

    public void setCuenta_capacitacion(String cuenta_capacitacion) {
        this.cuenta_capacitacion = cuenta_capacitacion;
    }

    public String getCuenta_cat() {
        return cuenta_cat;
    }

    public void setCuenta_cat(String cuenta_cat) {
        this.cuenta_cat = cuenta_cat;
    }

    public String getCuenta_central() {
        return cuenta_central;
    }

    public void setCuenta_central(String cuenta_central) {
        this.cuenta_central = cuenta_central;
    }

    public String getCuenta_com_central() {
        return cuenta_com_central;
    }

    public void setCuenta_com_central(String cuenta_com_central) {
        this.cuenta_com_central = cuenta_com_central;
    }

    public String getCuenta_com_seguro() {
        return cuenta_com_seguro;
    }

    public void setCuenta_com_seguro(String cuenta_com_seguro) {
        this.cuenta_com_seguro = cuenta_com_seguro;
    }

    public String getCuenta_seguro() {
        return cuenta_seguro;
    }

    public void setCuenta_seguro(String cuenta_seguro) {
        this.cuenta_seguro = cuenta_seguro;
    }

    public double getMonto_maximo() {
        return monto_maximo;
    }

    public void setMonto_maximo(double monto_maximo) {
        this.monto_maximo = monto_maximo;
    }

    public double getMonto_minimo() {
        return monto_minimo;
    }

    public void setMonto_minimo(double monto_minimo) {
        this.monto_minimo = monto_minimo;
    }

    public String getNit_asegurador() {
        return nit_asegurador;
    }

    public void setNit_asegurador(String nit_asegurador) {
        this.nit_asegurador = nit_asegurador;
    }

    public String getNit_capacitador() {
        return nit_capacitador;
    }

    public void setNit_capacitador(String nit_capacitador) {
        this.nit_capacitador = nit_capacitador;
    }

    public String getNit_central() {
        return nit_central;
    }

    public void setNit_central(String nit_central) {
        this.nit_central = nit_central;
    }

    public String getPlazo_maximo() {
        return plazo_maximo;
    }

    public void setPlazo_maximo(String plazo_maximo) {
        this.plazo_maximo = plazo_maximo;
    }

    public double getPorcentaje_cat() {
        return porcentaje_cat;
    }

    public void setPorcentaje_cat(double porcentaje_cat) {
        this.porcentaje_cat = porcentaje_cat;
    }

    public double getPorcentaje_com_seguro() {
        return porcentaje_com_seguro;
    }

    public void setPorcentaje_com_seguro(double porcentaje_com_seguro) {
        this.porcentaje_com_seguro = porcentaje_com_seguro;
    }

    public String getPrefijo_cxc_cat() {
        return prefijo_cxc_cat;
    }

    public void setPrefijo_cxc_cat(String prefijo_cxc_cat) {
        this.prefijo_cxc_cat = prefijo_cxc_cat;
    }

    public String getPrefijo_cxc_interes() {
        return prefijo_cxc_interes;
    }

    public void setPrefijo_cxc_interes(String prefijo_cxc_interes) {
        this.prefijo_cxc_interes = prefijo_cxc_interes;
    }

    public String getPrefijo_cxp_central() {
        return prefijo_cxp_central;
    }

    public void setPrefijo_cxp_central(String prefijo_cxp_central) {
        this.prefijo_cxp_central = prefijo_cxp_central;
    }

    public boolean isSeguro() {
        return seguro;
    }

    public void setSeguro(boolean seguro) {
        this.seguro = seguro;
    }

   
    public double getValor_capacitacion() {
        return valor_capacitacion;
    }

    public void setValor_capacitacion(double valor_capacitacion) {
        this.valor_capacitacion = valor_capacitacion;
    }

    public double getValor_central() {
        return valor_central;
    }

    public void setValor_central(double valor_central) {
        this.valor_central = valor_central;
    }

    public double getValor_com_central() {
        return valor_com_central;
    }

    public void setValor_com_central(double valor_com_central) {
        this.valor_com_central = valor_com_central;
    }

    public double getValor_seguro() {
        return valor_seguro;
    }

    public void setValor_seguro(double valor_seguro) {
        this.valor_seguro = valor_seguro;
    }

    public Convenio load(ResultSet rs) throws SQLException {
        convenio.setReg_status(rs.getString("reg_status"));
        convenio.setDstrct(rs.getString("dstrct"));
        convenio.setId_convenio(rs.getString("id_convenio"));
        convenio.setNombre(rs.getString("nombre"));
        convenio.setDescripcion(rs.getString("descripcion"));
        convenio.setNit_convenio(rs.getString("nit_convenio"));
        convenio.setNit_tercero(rs.getString("nit_tercero"));
        convenio.setCuenta_interes(rs.getString("cuenta_interes"));
        convenio.setCuenta_custodia(rs.getString("cuenta_custodia"));
        convenio.setPrefijo_negocio(rs.getString("prefijo_negocio"));
        convenio.setPrefijo_cxp(rs.getString("prefijo_cxp"));
        convenio.setCuenta_cxp(rs.getString("cuenta_cxp"));
        convenio.setHc_cxp(rs.getString("hc_cxp"));
        convenio.setCuota_gmf(rs.getString("cuota_gmf"));
        convenio.setPrefijo_nc_gmf(rs.getString("prefijo_nc_gmf"));
        convenio.setCuenta_gmf(rs.getString("cuenta_gmf"));
        convenio.setCuenta_gmf2(rs.getString("cuenta_gmf2"));
        convenio.setPrefijo_nc_aval(rs.getString("prefijo_nc_aval"));
        convenio.setCuenta_aval(rs.getString("cuenta_aval"));
        convenio.setPrefijo_diferidos(rs.getString("prefijo_diferidos"));
        convenio.setCuenta_diferidos(rs.getString("cuenta_diferidos"));
        convenio.setHc_diferidos(rs.getString("hc_diferidos"));
        convenio.setCreation_user(rs.getString("creation_user"));
        convenio.setUser_update(rs.getString("user_update"));
        convenio.setCreation_date(rs.getString("creation_date"));
        convenio.setLast_update(rs.getString("last_update"));
        convenio.setFactura_tercero(rs.getBoolean("factura_tercero"));
        convenio.setDescuenta_gmf(rs.getBoolean("descuenta_gmf"));
        convenio.setDescuenta_aval(rs.getBoolean("descuenta_aval"));
        convenio.setValor_custodia(rs.getDouble("valor_custodia"));
        convenio.setTasa_interes(rs.getDouble("tasa_interes"));
        convenio.setPorc_gmf(rs.getDouble("porcentaje_gmf"));//2010-09-27
        convenio.setPorc_gmf2(rs.getDouble("porcentaje_gmf2"));
        convenio.setImpuesto(rs.getString("impuesto"));
        convenio.setCuenta_ajuste(rs.getString("cuenta_ajuste"));
        convenio.setPrefijo_endoso(rs.getString("prefijo_endoso"));
        convenio.setHc_endoso(rs.getString("hc_endoso"));
        convenio.setMediador_aval(rs.getBoolean("intermediario_aval"));
        convenio.setNit_mediador(rs.getString("nit_mediador"));
        convenio.setAval_tercero(rs.getBoolean("aval_tercero"));
        convenio.setCentral(rs.getBoolean("central"));
        convenio.setCapacitacion(rs.getBoolean("capacitacion"));
        convenio.setSeguro(rs.getBoolean("seguro"));
        convenio.setNit_central(rs.getString("nit_central"));
        convenio.setNit_capacitador(rs.getString("nit_capacitador"));
        convenio.setNit_asegurador(rs.getString("nit_asegurador"));
        convenio.setPrefijo_cxc_interes(rs.getString("prefijo_cxc_interes"));
        convenio.setPrefijo_cxp_central(rs.getString("prefijo_cxp_central"));
        convenio.setCuenta_central(rs.getString("cuenta_central"));
        convenio.setCuenta_com_central(rs.getString("cuenta_com_central"));
        convenio.setPrefijo_cxc_cat(rs.getString("prefijo_cxc_cat"));
        convenio.setCuenta_cat(rs.getString("cuenta_cat"));
        convenio.setCuenta_capacitacion(rs.getString("cuenta_capacitacion"));
        convenio.setCuenta_seguro(rs.getString("cuenta_seguro"));
        convenio.setCuenta_com_seguro(rs.getString("cuenta_com_seguro"));
        convenio.setValor_central(rs.getDouble("valor_central"));
        convenio.setValor_com_central(rs.getDouble("valor_com_central"));
        convenio.setPorcentaje_cat(rs.getDouble("porcentaje_cat"));
        convenio.setValor_capacitacion(rs.getDouble("valor_capacitacion"));
        convenio.setValor_seguro(rs.getDouble("valor_seguro"));
        convenio.setPorcentaje_com_seguro(rs.getDouble("porcentaje_com_seguro"));
        convenio.setMonto_minimo(rs.getDouble("monto_minimo"));
        convenio.setMonto_maximo(rs.getDouble("monto_maximo"));
        convenio.setPlazo_maximo(rs.getString("plazo_maximo"));
        convenio.setTipo(rs.getString("tipo"));
        convenio.setRedescuento(rs.getBoolean("redescuento"));
        convenio.setAval_anombre(rs.getBoolean("aval_anombre"));
        convenio.setCxp_avalista(rs.getBoolean("cxp_avalista"));
        convenio.setNit_anombre(rs.getString("nit_anombre"));
        convenio.setPrefijo_cxc_aval(rs.getString("prefijo_cxc_aval"));
        convenio.setHc_cxc_aval(rs.getString("hc_cxc_aval"));
        convenio.setCuenta_cxc_aval(rs.getString("cuenta_cxc_aval"));
        convenio.setCctrl_db_cxc_aval(rs.getString("cctrl_db_cxc_aval"));
        convenio.setCctrl_cr_cxc_aval(rs.getString("cctrl_cr_cxc_aval"));
        convenio.setCctrl_iva_cxc_aval(rs.getString("cctrl_iva_cxc_aval"));
        convenio.setPrefijo_cxp_avalista(rs.getString("prefijo_cxp_avalista"));
        convenio.setHc_cxp_avalista(rs.getString("hc_cxp_avalista"));
        convenio.setCuenta_cxp_avalista(rs.getString("cuenta_cxp_avalista"));
        convenio.setCat(rs.getBoolean("cat")); 
        convenio.setPrefijo_cxp_fianza(rs.getString("prefijo_cxp_fianza"));
        convenio.setHc_cxp_fianza(rs.getString("hc_cxp_fianza"));
        convenio.setCuenta_cuota_administracion(rs.getString("cuenta_cuota_administracion"));
        convenio.setAgencia(rs.getString("agencia"));
        convenio.setPrefijo_cuota_admin(rs.getString("prefijo_cuota_administracion_diferido"));
        convenio.setHc_cuota_admin(rs.getString("hc_cuota_admin"));
        convenio.setCta_cuota_admin_diferido(rs.getString("cta_cuota_admin_diferido"));
        convenio.setId_Sucursal(rs.getString("id_sucursal"));
        convenio.setTasa_Compra_cartera(rs.getDouble("tasa_compra_cartera"));
        convenio.setTasa_sic_EA(rs.getDouble("tasa_sic_ea"));
        convenio.setTasa_Max_Fintra(rs.getDouble("tasa_max_fintra"));

        return convenio;
    }

    public String getId_Sucursal() {
        return id_sucursal;
    }

    public void setId_Sucursal(String id_sucursal) {
        this.id_sucursal = id_sucursal;
    }
    
    public double getTasa_Compra_cartera() {
        return tasa_compra_cartera;
    }

    public void setTasa_Compra_cartera(double tasa_compra_cartera) {
        this.tasa_compra_cartera = tasa_compra_cartera;
    }
    
    
    public Convenio getConvenio() {
        return convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }
    
     public ArrayList<ConvenioFiducias> getConvenioFiducias() {
        return convenioFiducias;
    }

    public void setConvenioFiducias(ArrayList<ConvenioFiducias> convenioFiducias) {
        this.convenioFiducias = convenioFiducias;
    }

    /**
     * @return the prefijo_cxp_fianza
     */
    public String getPrefijo_cxp_fianza() {
        return prefijo_cxp_fianza;
    }

    /**
     * @param prefijo_cxp_fianza the prefijo_cxp_fianza to set
     */
    public void setPrefijo_cxp_fianza(String prefijo_cxp_fianza) {
        this.prefijo_cxp_fianza = prefijo_cxp_fianza;
    }

    /**
     * @return the hc_cxp_fianza
     */
    public String getHc_cxp_fianza() {
        return hc_cxp_fianza;
    }

    /**
     * @param hc_cxp_fianza the hc_cxp_fianza to set
     */
    public void setHc_cxp_fianza(String hc_cxp_fianza) {
        this.hc_cxp_fianza = hc_cxp_fianza;
    }

    /**
     * @return the cuenta_cuota_administracion
     */
    public String getCuenta_cuota_administracion() {
        return cuenta_cuota_administracion;
    }

    /**
     * @param cuenta_cuota_administracion the cuenta_cuota_administracion to set
     */
    public void setCuenta_cuota_administracion(String cuenta_cuota_administracion) {
        this.cuenta_cuota_administracion = cuenta_cuota_administracion;
    }

    /**
     * @return the cta_cuota_admin_diferido
     */
    public String getCta_cuota_admin_diferido() {
        return cta_cuota_admin_diferido;
    }

    /**
     * @param cta_cuota_admin_diferido the cta_cuota_admin_diferido to set
     */
    public void setCta_cuota_admin_diferido(String cta_cuota_admin_diferido) {
        this.cta_cuota_admin_diferido = cta_cuota_admin_diferido;
    }

    /**
     * @return the cargos_fijos_convenios
     */
    public ArrayList<CargosFijosConvenios> getCargos_fijos_convenios() {
        return cargos_fijos_convenios;
    }

    /**
     * @param cargos_fijos_convenios the cargos_fijos_convenios to set
     */
    public void setCargos_fijos_convenios(ArrayList<CargosFijosConvenios> cargos_fijos_convenios) {
        this.cargos_fijos_convenios = cargos_fijos_convenios;
    }

    public String getId_sucursal() {
        return id_sucursal;
    }

    public void setId_sucursal(String id_sucursal) {
        this.id_sucursal = id_sucursal;
    }

    public double getTasa_compra_cartera() {
        return tasa_compra_cartera;
    }

    public void setTasa_compra_cartera(double tasa_compra_cartera) {
        this.tasa_compra_cartera = tasa_compra_cartera;
    }

    public Unidad_Negocio getUnidad_negocio() {
        return unidad_negocio;
    }

    public void setUnidad_negocio(Unidad_Negocio unidad_negocio) {
        this.unidad_negocio = unidad_negocio;
    }
    
}
