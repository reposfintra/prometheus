/*
 * ProConBen.java
 *
 * Created on 21 de diciembre de 2004, 04:49 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  mcelin
 */
public class ProConBen {
    
    private String Placa;
    private String Pais;
    private String NombreP;
    private String DireccionP;
    private String NombreC;
    private String DireccionC;
    private String Telefono;
    private String Beneficiario;
    private String Banco;
    private String Cuenta;
    private String Moneda;
    
    /** Creates a new instance of ProConBen */
    public ProConBen() {
    }
    
    //Setter
    public void setPlaca(String valor) {
        this.Placa = valor;
    }
    
    public void setPais(String valor) {
        this.Pais = valor;
    }
    
    public void setNombreP(String valor) {
        this.NombreP = valor;
    }
    
    public void setDirteccionP(String valor) {
        this.DireccionP = valor;
    }
    
    public void setNombreC(String valor) {
        this.NombreC = valor;
    }
    
    public void setDireccionC(String valor) {
        this.DireccionC = valor;
    }
    
    public void setTelefono(String valor) {
        this.Telefono = valor;
    }
    
    public void setBeneficiario(String valor) {
        this.Beneficiario = valor;
    }
    
    public void setBanco(String valor) {
        this.Banco = valor;
    }
    
    public void setCuenta(String valor) {
        this.Cuenta = valor;
    }
    
    public void setMoneda(String valor) {
        this.Moneda = valor;
    }
    
    //Getter
    public String getPlaca(){
        return this.Placa;
    }
    
    public String getPais(){
        return this.Pais;
    }
    
    public String getNombreP(){
        return this.NombreP;
    }
    
    public String getDireccionP(){
        return this.DireccionP;
    }
    
    public String getNombreC(){
        return this.NombreC;
    }
    
    public String getDireccionC(){
        return this.DireccionC;
    }
    
    public String getTelefono(){
        return this.Telefono;
    }
    
    public String getBeneficiario(){
        return this.Beneficiario;
    }
    
    public String getBanco(){
        return this.Banco;
    }
    
    public String getCuenta(){
        return this.Cuenta;
    }
    
    public String getMoneda(){
        return this.Moneda;
    }        
}
