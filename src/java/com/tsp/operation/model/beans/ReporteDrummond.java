/**************************************************************************
 * Nombre clase: ReporteDrummondService.java                               *
 * Descripci�n: Clase que maneja los GET y los SET de las consultas de los *
 * reportes del cliente  DRUNMONND                                         *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/

package com.tsp.operation.model.beans;

/**
 *
 * @author  Igomez
 */
public class ReporteDrummond {
    private String Despacho;
    private String Remision;
    private String FecDespacho;
    private String Origen;
    private String Destino;
    private String DO;
    private String PO;
    private String TipoVehiculo;
    private String Color;
    private String Placa;
    private String Conductor;
    private String Cedula;
    private String Celular;
    private String OC;
    
    
    /** Creates a new instance of ReporteDrummond */
    public ReporteDrummond() {
      Despacho     =" ";
      Remision     =" ";
      FecDespacho  =" ";
      Origen       =" ";
      Destino      =" ";
      DO           =" ";
      PO           =" ";
      TipoVehiculo =" ";
      Color        =" ";
      Placa        =" ";
      Conductor    =" ";
      Cedula       =" ";
      Celular      =" ";
      OC           =" ";

    }
   /// Declaracion de SET------------------------------------------------------------------------- 
    public void setDespacho    (String newDespacho     )  {  this.Despacho     = newDespacho;    }
    public void setRemision    (String newRemision     )  {  this.Remision     = newRemision;    }
    public void setFecDespacho (String newFecDespacho  )  {  this.FecDespacho  = newFecDespacho; }
    public void setOrigen      (String newOrigen       )  {  this.Origen       = newOrigen;      }
    public void setDestino     (String newDestino      )  {  this.Destino      = newDestino;     }
    public void setDO          (String newDO           )  {  this.DO           = newDO;          }
    public void setPO          (String newPO           )  {  this.PO           = newPO;          }
    public void setTipoVehiculo(String newTipoVehiculo )  {  this.TipoVehiculo = newTipoVehiculo;}
    public void setColor       (String newColor        )  {  this.Color        = newColor;       }
    public void setPlaca       (String newPlaca        )  {  this.Placa        = newPlaca;       }
    public void setConductor   (String newConductor    )  {  this.Conductor    = newConductor;   }
    public void setCedula      (String newCedula       )  {  this.Cedula       = newCedula;      }
    public void setCelular     (String newCelular      )  {  this.Celular      = newCelular;     }
    public void setOC          (String newOC           )  {  this.OC           = newOC;          }
    
    // Declartacion de GET -----------------------------------------------------------------------
    public String getDespacho    ()  {  return this.Despacho;    }
    public String getRemision    ()  {  return this.Remision;    }
    public String getFecDespacho ()  {  return this.FecDespacho; }
    public String getOrigen      ()  {  return this.Origen;      }
    public String getDestino     ()  {  return this.Destino;     }
    public String getDO          ()  {  return this.DO;          }
    public String getPO          ()  {  return this.PO;          }
    public String getTipoVehiculo()  {  return this.TipoVehiculo;}
    public String getColor       ()  {  return this.Color;       }
    public String getPlaca       ()  {  return this.Placa;       }
    public String getConductor   ()  {  return this.Conductor;   }
    public String getCedula      ()  {  return this.Cedula;      }
    public String getCelular     ()  {  return this.Celular;     }
    public String getOC          ()  {  return this.OC;          }
    
}
