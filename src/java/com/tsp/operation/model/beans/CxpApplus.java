/* * CxpApplus.java * * Created on 23 de julio de 2009, 16:14 */

package com.tsp.operation.model.beans;

/** * * @author  Fintra */
public class CxpApplus {
    private String num_os,cxc_ap,id_orden,factura_app,vlr_neto,abono,saldo,fec_open;
    
    private String base_cxp_app,iva_cxp_app,iva_factor_finv_cxp_app,iva_finv_cxp_app,esquema_comision,
        retencion_cxp_app,sum_tot_prev1,doc_relacionado;
    
    private String banco,sucursal;
    
    public CxpApplus() {    }
    public String getBaseCxpApp() {
        return base_cxp_app;
    }
    public void setBaseCxpApp(String x) {
        this.base_cxp_app= x;
    }
    public String getDocRelacionado() {
        return doc_relacionado;
    }
    public void setDocRelacionado(String x) {
        this.doc_relacionado= x;
    }
    public String getIvaCxpApp() {
        return iva_cxp_app;
    }
    public void setIvaCxpApp(String x) {
        this.iva_cxp_app= x;
    }
    public String getIvaFactorFinvCxpApp() {
        return iva_factor_finv_cxp_app;
    }
    public void setIvaFactorFinvCxpApp(String x) {
        this.iva_factor_finv_cxp_app= x;
    }
    public String getIvaFinvCxpApp() {
        return iva_finv_cxp_app;
    }
    public void setIvaFinvCxpApp(String x) {
        this.iva_finv_cxp_app= x;
    }
    public String getEsquemaComision() {
        return esquema_comision;
    }
    public void setEsquemaComision(String x) {
        this.esquema_comision= x;
    }
    public String getRetencionCxpApp() {
        return retencion_cxp_app;
    }
    public void setRetencionCxpApp(String x) {
        this.retencion_cxp_app= x;
    }
    public String getSumTotPrev1() {
        return sum_tot_prev1;
    }
    public void setSumTotPrev1(String x) {
        this.sum_tot_prev1= x;
    }
    
    public String getNumOs() {
        return num_os;
    }
    public void setNumOs(String x) {
        this.num_os= x;
    }
    public String getCxcAp() {
        return cxc_ap;
    }
    public void setCxcAp(String x) {
        this.cxc_ap= x;
    }
    public String getIdOrden() {
        return id_orden;
    }
    public void setIdOrden(String x) {
        this.id_orden= x;
    }
    public String getFacturaApp() {
        return factura_app;
    }
    public void setFacturaApp(String x) {
        this.factura_app= x;
    }
    public String getVlrNeto() {
        return vlr_neto;
    }
    public void setVlrNeto(String x) {
        this.vlr_neto= x;
    }
    public String getAbono() {
        return abono;
    }
    public void setAbono(String x) {
        this.abono= x;
    }
    public String getSaldo() {
        return saldo;
    }
    public void setSaldo(String x) {
        this.saldo= x;
    }
    public String getFecOpen() {
        return fec_open;
    }
    public void setFecOpen(String x) {
        this.fec_open= x;
    }
    
    
    public String getBanco() {
        return banco;
    }
    public void setBanco(String x) {
        this.banco= x;
    }
    public String getSucursal() {
        return sucursal;
    }
    public void setSucursal(String x) {
        this.sucursal= x;
    }
    
    public static CxpApplus load(java.sql.ResultSet rs)throws java.sql.SQLException{
        CxpApplus cxpApplus = new CxpApplus();
        cxpApplus.setNumOs( rs.getString("num_os") );           
        cxpApplus.setCxcAp( rs.getString("cxc_ap") );             
        cxpApplus.setIdOrden( rs.getString("id_orden") ); 
        cxpApplus.setFacturaApp( rs.getString("factura_app") ); 
        cxpApplus.setVlrNeto( rs.getString("vlr_neto") ); 
        cxpApplus.setAbono( rs.getString("vlr_total_abonos") ); 
        cxpApplus.setSaldo( rs.getString("vlr_saldo") ); 
        cxpApplus.setFecOpen( rs.getString("fec_open") ); 
        cxpApplus.setBaseCxpApp( rs.getString("vlr_1") ); 
        cxpApplus.setIvaCxpApp( rs.getString("vlr_2") ); 
        cxpApplus.setIvaFactorFinvCxpApp( rs.getString("vlr_3") ); 
        cxpApplus.setIvaFinvCxpApp( rs.getString("vlr_4") ); 
        cxpApplus.setRetencionCxpApp( rs.getString("vlr_5") ); 
        cxpApplus.setEsquemaComision( rs.getString("esquema_comision") ); 
        cxpApplus.setSumTotPrev1( rs.getString("sum_total_prev1") ); 
        
        cxpApplus.setBanco(rs.getString("banco") ); 
        cxpApplus.setSucursal(rs.getString("sucursal") ); 
        cxpApplus.setDocRelacionado(rs.getString("documento_relacionado") ); 
        
        return cxpApplus;
    }  
    
    
}




 
    
 
    
  
    
 

