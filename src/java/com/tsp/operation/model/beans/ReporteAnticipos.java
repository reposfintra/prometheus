/********************************************************************
 *      Nombre Clase.................   ReporteAnticipos.java
 *      Descripci�n..................   Bean del reporte de anticipos
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   03.11.2005
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.beans;

import java.util.*;
import com.tsp.util.Util;
import java.sql.*;

public class ReporteAnticipos {
    private String remision;
    private String placa;
    private String fecha;
    private String nitprop;
    private String nomprop;
    private String proveedor;
    private String galon;
    private Double vr_galon;
    private Double peajes;
    private Double efectivo;
    private Double total;
    private String puerto;
    private String provanticipo;
    
    /** Creates a new instance of ReporteAnticipos */
    public ReporteAnticipos() {
    }
    
      
    /**
         * Toma el valor de cada uno de los campos y los asigna.
         * @autor Tito Andr�s Maturana
         * @param rs ResultSet de la consulta.
         * @throws SQLException
         * @version 1.1
         */        
        public void Load(ResultSet rs) throws SQLException{
                this.setEfectivo(Double.valueOf(String.valueOf(Util.redondear(rs.getDouble("efectivo"),0))));
                this.setFecha(rs.getString("fecdsp"));
                this.setGalon(rs.getString("acpm_gln"));
                this.setNitprop(rs.getString("nitpro"));
                this.setNomprop(rs.getString("nomprop"));
                this.setPeajes(Double.valueOf(String.valueOf(Util.redondear(rs.getDouble("peajes"),0))));
                this.setPlaca(rs.getString("plaveh"));
                this.setProveedor(rs.getString("proveedor"));
                this.setRemision(rs.getString("remision"));
                this.setTotal(Double.valueOf(String.valueOf(Util.redondear(rs.getDouble("total"),0))));
                this.setVr_galon(Double.valueOf(String.valueOf(rs.getDouble("vr_acpm"))));
                // ricardo
                this.setPuerto(rs.getString("puerto"));
                this.setProvanticipo(rs.getString("provanticipo"));
                
        }
    //ricardo    
    
    /**
     * Setter for property puerto.
     * @param puerto New value of property puerto.
     */
    public void setPuerto(String dato){
        this.puerto = dato;
    }
    
    /**
     * Getter for property puerto.
     * @return Value of property puerto.
     */
    public String getPuerto(){
        return this.puerto;
    }
    
    /**
     * Setter for property provanticipo.
     * @param provanticipo New value of property provanticipo.
     */
    public void setProvAnticipo(String dato){
        this.provanticipo = dato;
    }
    
    /**
     * Getter for property provanticipo.
     * @return Value of property provanticipo.
     */
    public String getProvAnticipo(){
        return this.provanticipo;
    }
    
    /**
     * Getter for property efectivo.
     * @return Value of property efectivo.
     */
    public java.lang.Double getEfectivo() {
        return efectivo;
    }
    
    /**
     * Setter for property efectivo.
     * @param efectivo New value of property efectivo.
     */
    public void setEfectivo(java.lang.Double efectivo) {
        this.efectivo = efectivo;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property galon.
     * @return Value of property galon.
     */
    public java.lang.String getGalon() {
        return galon;
    }
    
    /**
     * Setter for property galon.
     * @param galon New value of property galon.
     */
    public void setGalon(java.lang.String galon) {
        this.galon = galon;
    }
    
    /**
     * Getter for property nitprop.
     * @return Value of property nitprop.
     */
    public java.lang.String getNitprop() {
        return nitprop;
    }
    
    /**
     * Setter for property nitprop.
     * @param nitprop New value of property nitprop.
     */
    public void setNitprop(java.lang.String nitprop) {
        this.nitprop = nitprop;
    }
    
    /**
     * Getter for property nomprop.
     * @return Value of property nomprop.
     */
    public java.lang.String getNomprop() {
        return nomprop;
    }
    
    /**
     * Setter for property nomprop.
     * @param nomprop New value of property nomprop.
     */
    public void setNomprop(java.lang.String nomprop) {
        this.nomprop = nomprop;
    }
    
    /**
     * Getter for property peajes.
     * @return Value of property peajes.
     */
    public java.lang.Double getPeajes() {
        return peajes;
    }
    
    /**
     * Setter for property peajes.
     * @param peajes New value of property peajes.
     */
    public void setPeajes(java.lang.Double peajes) {
        this.peajes = peajes;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
    /**
     * Getter for property total.
     * @return Value of property total.
     */
    public java.lang.Double getTotal() {
        return total;
    }
    
    /**
     * Setter for property total.
     * @param total New value of property total.
     */
    public void setTotal(java.lang.Double total) {
        this.total = total;
    }
    
    /**
     * Getter for property vr_galon.
     * @return Value of property vr_galon.
     */
    public java.lang.Double getVr_galon() {
        return vr_galon;
    }
    
    /**
     * Setter for property vr_galon.
     * @param vr_galon New value of property vr_galon.
     */
    public void setVr_galon(java.lang.Double vr_galon) {
        this.vr_galon = vr_galon;
    }
    
        /**
         * Getter for property provanticipo.
         * @return Value of property provanticipo.
         */
        public java.lang.String getProvanticipo() {
            return provanticipo;
        }
        
        /**
         * Setter for property provanticipo.
         * @param provanticipo New value of property provanticipo.
         */
        public void setProvanticipo(java.lang.String provanticipo) {
            this.provanticipo = provanticipo;
        }
    
}
