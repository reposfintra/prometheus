/*
 * Aval.java
 * Created on 1 de mayo de 2009, 15:52
 */

package com.tsp.operation.model.beans;

import java.sql.ResultSet;
 // @author  navi
 import java.io.Serializable;
public class Aval implements Serializable{    
    private String SeccionalAfiliado,ConsecutivoAfiliado,SucursalAfiliado,NitFinanciera,CodigoServicio,NumAval,TituloValor,FechaAutorizacion
    ,HoraAutorizacion,NitAfiliado,TipoDoc,NumDoc,NombreGirador,TelGirador,CodigoBanco,CodigoSucursal,NumeroCuenta,ValorTitulo,FechaConsignacion,
    CoberturaConsulta,CodigoDepto,CodigoCiudad,FechaInicial,FechaFinal,FechaProceso,HoraProceso,IndicadorRatificado,CodigoIata,Compa�iaVende,TipoVenta,
    NumLinea,FechaSistema,UsuarioSistema,RegStatus,Archivo,FechaArchivo,Aprobado,FechaAprobacion,Num;
    private String nombre_afiliado;
    
    public Aval() {
        
    }    
    public void setSeccionalAfiliado(String x){
        this.SeccionalAfiliado = x;
    }    
    public String getSeccionalAfiliado(){
        return this.SeccionalAfiliado;
    }
    public void setConsecutivoAfiliado(String x){
        this.ConsecutivoAfiliado = x;
    }    
    public String getConsecutivoAfiliado(){
        return this.ConsecutivoAfiliado;
    }
    public void setSucursalAfiliado(String x){
        this.SucursalAfiliado = x;
    }    
    public String getSucursalAfiliado(){
        return this.SucursalAfiliado;
    }
    public void setNitFinanciera(String x){
        this.NitFinanciera = x;
    }    
    public String getNitFinanciera(){
        return this.NitFinanciera;
    }
    public void setCodigoServicio(String x){
        this.CodigoServicio = x;
    }    
    public String getCodigoServicio(){
        return this.CodigoServicio;
    }
    public void setNumAval(String x){
        this.NumAval = x;
    }    
    public String getNumAval(){
        return this.NumAval;
    }
    public void setTituloValor(String x){
        this.TituloValor = x;
    }    
    public String getTituloValor(){
        return this.TituloValor;
    }
    public void setFechaAutorizacion(String x){
        this.FechaAutorizacion = x;
    }    
    public String getFechaAutorizacion(){
        return this.FechaAutorizacion;
    }
    public void setHoraAutorizacion(String x){
        this.HoraAutorizacion = x;
    }    
    public String getHoraAutorizacion(){
        return this.HoraAutorizacion;
    }
    public void setNitAfiliado(String x){
        this.NitAfiliado = x;
    }    
    public String getNitAfiliado(){
        return this.NitAfiliado;
    }
    public void setTipoDoc(String x){
        this.TipoDoc = x;
    }    
    public String getTipoDoc(){
        return this.TipoDoc;
    }
    public void setNumDoc(String x){
        this.NumDoc = x;
    }    
    public String getNumDoc(){
        return this.NumDoc;
    }
    public void setNombreGirador(String x){
        this.NombreGirador = x;
    }    
    public String getNombreGirador(){
        return this.NombreGirador;
    }
    public void setTelGirador(String x){
        this.TelGirador = x;
    }    
    public String getTelGirador(){
        return this.TelGirador;
    }
    public void setCodigoBanco(String x){
        this.CodigoBanco = x;
    }    
    public String getCodigoBanco(){
        return this.CodigoBanco;
    }
    public void setCodigoSucursal(String x){
        this.CodigoSucursal = x;
    }    
    public String getCodigoSucursal(){
        return this.CodigoSucursal;
    }
    public void setNumeroCuenta(String x){
        this.NumeroCuenta = x;
    }    
    public String getNumeroCuenta(){
        return this.NumeroCuenta;
    }
    public void setValorTitulo(String x){
        this.ValorTitulo = x;
    }    
    public String getValorTitulo(){
        return this.ValorTitulo;
    }
    public void setFechaConsignacion(String x){
        this.FechaConsignacion = x;
    }    
    public String getFechaConsignacion(){
        return this.FechaConsignacion;
    }
    public void setCoberturaConsulta(String x){
        this.CoberturaConsulta = x;
    }    
    public String getCoberturaConsulta(){
        return this.CoberturaConsulta;
    }
    public void setCodigoDepto(String x){
        this.CodigoDepto = x;
    }    
    public String getCodigoDepto(){
        return this.CodigoDepto;
    }
    public void setCodigoCiudad(String x){
        this.CodigoCiudad = x;
    }    
    public String getCodigoCiudad(){
        return this.CodigoCiudad;
    }
    public void setFechaInicial(String x){
        this.FechaInicial = x;
    }    
    public String getFechaInicial(){
        return this.FechaInicial;
    }
    public void setFechaFinal(String x){
        this.FechaFinal = x;
    }    
    public String getFechaFinal(){
        return this.FechaFinal;
    }
    public void setFechaProceso(String x){
        this.FechaProceso = x;
    }    
    public String getFechaProceso(){
        return this.FechaProceso;
    }
    public void setHoraProceso(String x){
        this.HoraProceso = x;
    }    
    public String getHoraProceso(){
        return this.HoraProceso;
    }
    public void setIndicadorRatificado(String x){
        this.IndicadorRatificado = x;
    }    
    public String getIndicadorRatificado(){
        return this.IndicadorRatificado;
    }
    public void setCodigoIata(String x){
        this.CodigoIata = x;
    }    
    public String getCodigoIata(){
        return this.CodigoIata;
    }
    public void setCompa�iaVende(String x){
        this.Compa�iaVende = x;
    }    
    public String getCompa�iaVende(){
        return this.Compa�iaVende;
    }
    public void setTipoVenta(String x){
        this.TipoVenta = x;
    }    
    public String getTipoVenta(){
        return this.TipoVenta;
    }
    public void setNumLinea(String x){
        this.NumLinea = x;
    }    
    public String getNumLinea(){
        return this.NumLinea;
    }
    public void setFechaSistema(String x){
        this.FechaSistema = x;
    }    
    public String getFechaSistema(){
        return this.FechaSistema;
    }
    public void setUsuarioSistema(String x){
        this.UsuarioSistema = x;
    }    
    public String getUsuarioSistema(){
        return this.UsuarioSistema;
    }
    public void setRegStatus(String x){
        this.RegStatus = x;
    }    
    public String getRegStatus(){
        return this.RegStatus;
    }
    public void setArchivo(String x){
        this.Archivo = x;
    }    
    public String getArchivo(){
        return this.Archivo;
    }
    public void setFechaArchivo(String x){
        this.FechaArchivo = x;
    }    
    public String getFechaArchivo(){
        return this.FechaArchivo;
    }
    public void setAprobado(String x){
        this.Aprobado = x;
    }    
    public String getAprobado(){
        return this.Aprobado;
    }
    public void setFechaAprobacion(String x){
        this.FechaAprobacion = x;
    }    
    public String getFechaAprobacion(){
        return this.FechaAprobacion;
    }
    public void setNum(String x){
        this.Num = x;
    }    
    public String getNum(){
        return this.Num;
    }
    
    public void setNombreAfiliado(String x){
        this.nombre_afiliado = x;
    }    
    public String getNombreAfiliado(){
        return this.nombre_afiliado;
    }
    
    private String reset(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    public void Load(ResultSet r) throws Exception{
       
        this.setSeccionalAfiliado(r.getString("scc_afi"));
        this.setConsecutivoAfiliado(r.getString("con_afi"));
        this.setSucursalAfiliado(r.getString("suc_afi"));
        this.setNitFinanciera(r.getString("fin_nit"));
        this.setCodigoServicio(r.getString("ser_cod"));
        this.setNumAval(r.getString("no_aval"));
        this.setTituloValor(r.getString("tit_val"));
        this.setFechaAutorizacion(r.getString("fec_aut"));
        this.setHoraAutorizacion(r.getString("hor_aut"));
        this.setNitAfiliado(r.getString("nit_afi"));
        this.setTipoDoc(r.getString("tip_doc"));
        this.setNumDoc(r.getString("num_doc"));
        this.setNombreGirador(r.getString("nom_gir"));
        this.setTelGirador(r.getString("tel_gir"));
        this.setCodigoBanco(r.getString("cod_bco"));
        this.setCodigoSucursal(r.getString("cod_suc"));
        this.setNumeroCuenta(r.getString("nro_cta"));
        this.setValorTitulo(r.getString("vlr_tit"));
        this.setFechaConsignacion(r.getString("fec_con"));
        this.setCoberturaConsulta(r.getString("cob_con"));
        this.setCodigoDepto(r.getString("cod_dpt"));
        this.setCodigoCiudad(r.getString("cod_ciu"));
        this.setFechaInicial(r.getString("fec_ini"));
        this.setFechaFinal(r.getString("fec_fin"));
        this.setFechaProceso(r.getString("fec_pro"));
        this.setHoraProceso(r.getString("hor_pro"));
        this.setIndicadorRatificado(r.getString("ind_rat"));
        this.setCodigoIata(r.getString("cod_iata"));
        this.setCompa�iaVende(r.getString("cod_cia"));
        this.setTipoVenta(r.getString("cod_vta"));
        this.setNumLinea(r.getString("nro_lin"));
        this.setFechaSistema(r.getString("creation_date"));
        this.setUsuarioSistema(r.getString("creation_user"));
        this.setRegStatus(r.getString("reg_status"));
        this.setArchivo(r.getString("archivo"));
        this.setFechaArchivo(r.getString("creation_date_archivo"));
        this.setAprobado(r.getString("aprobado"));
        this.setFechaAprobacion(r.getString("fecha_aprobacion"));  
        this.setNum(r.getString("num")); 
        this.setNombreAfiliado(reset(r.getString("nombre_afiliado")));          
    }
}
