/**************************************************************************
 * Nombre:        UsuarioAprobacion.java                     
 * Descripci�n:   Beans de usuario que aprueba la tabla.              
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   
 * Fecha:         11 de enero de 2006, 05:24 PM       
 * Versi�n:       Java  1.0                                      
 * Copyright:     Fintravalores S.A. S.A.                               
 **************************************************************************/



package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  dbastidas
 */
public class UsuarioAprobacion implements Serializable {
    private String reg_status;
    private String dstrct;
    private String id_agencia;
    private String tabla;
    private String usuario_aprobacion;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String base;
    private String id_agenciaAct;
    private String tablaAct;
    private String usuario_aprobacionAct;
    //nuevo
    private String nomagencia; 
    /** Creates a new instance of UsuarioAprobacion */
    public UsuarioAprobacion() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
     
    /**
     * Getter for property id_agencia.
     * @return Value of property id_agencia.
     */
    public java.lang.String getId_agencia() {
        return id_agencia;
    }
    
    /**
     * Setter for property id_agencia.
     * @param id_agencia New value of property id_agencia.
     */
    public void setId_agencia(java.lang.String id_agencia) {
        this.id_agencia = id_agencia;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property tabla.
     * @return Value of property tabla.
     */
    public java.lang.String getTabla() {
        return tabla;
    }
    
    /**
     * Setter for property tabla.
     * @param tabla New value of property tabla.
     */
    public void setTabla(java.lang.String tabla) {
        this.tabla = tabla;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property usuario_aprobacion.
     * @return Value of property usuario_aprobacion.
     */
    public java.lang.String getUsuario_aprobacion() {
        return usuario_aprobacion;
    }
    
    /**
     * Setter for property usuario_aprobacion.
     * @param usuario_aprobacion New value of property usuario_aprobacion.
     */
    public void setUsuario_aprobacion(java.lang.String usuario_aprobacion) {
        this.usuario_aprobacion = usuario_aprobacion;
    }
    
    /**
     * Getter for property nomagencia.
     * @return Value of property nomagencia.
     */
    public java.lang.String getNomagencia() {
        return nomagencia;
    }
    
    /**
     * Setter for property nomagencia.
     * @param nomagencia New value of property nomagencia.
     */
    public void setNomagencia(java.lang.String nomagencia) {
        this.nomagencia = nomagencia;
    }
    
    /**
     * Getter for property id_agenciaAct.
     * @return Value of property id_agenciaAct.
     */
    public java.lang.String getId_agenciaAct() {
        return id_agenciaAct;
    }
    
    /**
     * Setter for property id_agenciaAct.
     * @param id_agenciaAct New value of property id_agenciaAct.
     */
    public void setId_agenciaAct(java.lang.String id_agenciaAct) {
        this.id_agenciaAct = id_agenciaAct;
    }
    
    /**
     * Getter for property tablaAct.
     * @return Value of property tablaAct.
     */
    public java.lang.String getTablaAct() {
        return tablaAct;
    }
    
    /**
     * Setter for property tablaAct.
     * @param tablaAct New value of property tablaAct.
     */
    public void setTablaAct(java.lang.String tablaAct) {
        this.tablaAct = tablaAct;
    }
    
    /**
     * Getter for property usuario_aprobacionAct.
     * @return Value of property usuario_aprobacionAct.
     */
    public java.lang.String getUsuario_aprobacionAct() {
        return usuario_aprobacionAct;
    }
    
    /**
     * Setter for property usuario_aprobacionAct.
     * @param usuario_aprobacionAct New value of property usuario_aprobacionAct.
     */
    public void setUsuario_aprobacionAct(java.lang.String usuario_aprobacionAct) {
        this.usuario_aprobacionAct = usuario_aprobacionAct;
    }
    
}
