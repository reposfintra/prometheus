/********************************************************************
 *      Nombre Clase.................   RetrasoSalida.java
 *      Descripci�n..................   Bean de la tabla retraso_salida
 *      Autor........................   David Lamadrid
 *      Fecha........................   13.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.beans;

/**
 *
 * @author  dlamadrid
 */
public class RetrasoSalida
{
    
  private String reg_status ="";
  private String dstrct ="";
  private String tipo ="";
  private String planilla ="";
  private String causa ="";
  private String descripcion ="";
  private String c_actual ="";
  private String c_nuevo ="";
  private String last_update ="";
  private String user_update ="";
  private String creation_date ="";
  private String creation_user ="";
  private String base ="";
   private String cedulaC ="";
    /** Creates a new instance of RetrasoSalida */
    public RetrasoSalida ()
    {
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status ()
    {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status (java.lang.String reg_status)
    {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct ()
    {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct (java.lang.String dstrct)
    {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo ()
    {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo (java.lang.String tipo)
    {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla ()
    {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla (java.lang.String planilla)
    {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa ()
    {
        return causa;
    }
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa (java.lang.String causa)
    {
        this.causa = causa;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion ()
    {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion (java.lang.String descripcion)
    {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property c_actual.
     * @return Value of property c_actual.
     */
    public java.lang.String getC_actual ()
    {
        return c_actual;
    }
    
    /**
     * Setter for property c_actual.
     * @param c_actual New value of property c_actual.
     */
    public void setC_actual (java.lang.String c_actual)
    {
        this.c_actual = c_actual;
    }
    
    /**
     * Getter for property c_nuevo.
     * @return Value of property c_nuevo.
     */
    public java.lang.String getC_nuevo ()
    {
        return c_nuevo;
    }
    
    /**
     * Setter for property c_nuevo.
     * @param c_nuevo New value of property c_nuevo.
     */
    public void setC_nuevo (java.lang.String c_nuevo)
    {
        this.c_nuevo = c_nuevo;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update ()
    {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update (java.lang.String last_update)
    {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update ()
    {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update)
    {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date ()
    {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date)
    {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user ()
    {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user)
    {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase ()
    {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base)
    {
        this.base = base;
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedulaC ()
    {
        return cedulaC;
    }
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedulaC (java.lang.String cedula)
    {
        this.cedulaC = cedula;
    }
    
}
