package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Stdjobcosto implements Serializable {
    
    private String dstrct;
    private String sj;
    private String cf_code;
    private String ft_code;
    private String ind_trip;
    private String ind_tariff;
    private float cost;
    private float unit_cost;
    private String unit_transp;
    private String currency;
    private float peso_lleno_maximo;
    private float porcentaje_maximo_anticipo;
    private java.util.Date creation_date;
    private String creation_user;
    private String ruta;
    private float max_valor;
    private String  max_unidad;
    
    public static Stdjobcosto load(ResultSet rs)throws SQLException{
        
        Stdjobcosto stdjobcosto = new Stdjobcosto();
        
        stdjobcosto.setDstrct(rs.getString("dstrct"));
        stdjobcosto.setSj(rs.getString("sj"));
        stdjobcosto.setCf_code(rs.getString("cf_code"));
        stdjobcosto.setFt_code(rs.getString("ft_code"));
        stdjobcosto.setInd_trip(rs.getString("ind_trip"));
        stdjobcosto.setInd_tariff(rs.getString("ind_tariff"));
        stdjobcosto.setCost(rs.getFloat("cost"));
        stdjobcosto.setUnit_cost(rs.getFloat("unit_cost"));
        stdjobcosto.setUnit_transp(rs.getString("unit_transp"));
        stdjobcosto.setCurrency(rs.getString("currency"));
        stdjobcosto.setPeso_lleno_maximo(rs.getFloat("Peso_lleno_maximo"));
        stdjobcosto.setPorcentaje_maximo_anticipo(rs.getFloat("Porcentaje_maximo_anticipo"));
        stdjobcosto.setCreation_date(rs.getTimestamp("creation_date"));
        stdjobcosto.setCreation_user(rs.getString("creation_user"));
        
        return stdjobcosto;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    
     public void setRuta(String ruta){
        
        this.ruta=ruta;
    }
    
    public String getRuta(){
        
        return ruta;
    }
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    
    public void setSj(String sj){
        
        this.sj=sj;
    }
    
    public String getSj(){
        
        return sj;
    }
    
    public void setCf_code(String cf_code){
        
        this.cf_code=cf_code;
    }
    
    public String getCf_code(){
        
        return cf_code;
    }
    
    public void setFt_code(String ft_code){
        
        this.ft_code=ft_code;
    }
    
    public String getFt_code(){
        
        return ft_code;
    }
    
    public void setInd_trip(String ind_trip){
        
        this.ind_trip=ind_trip;
    }
    
    public String getInd_trip(){
        
        return ind_trip;
    }
    
    public void setInd_tariff(String ind_tariff){
        
        this.ind_tariff=ind_tariff;
    }
    
    public String getInd_tariff(){
        
        return ind_tariff;
    }
    
    public void setCost(float cost){
        
        this.cost=cost;
    }
    
    public float getCost(){
        
        return cost;
    }
    
    public void setUnit_cost(float unit_cost){
        
        this.unit_cost=unit_cost;
    }
    
    public float getUnit_cost(){
        
        return unit_cost;
    }
    
    public void setUnit_transp(String unit_transp){
        
        this.unit_transp=unit_transp;
    }
    
    public String getUnit_transp(){
        
        return unit_transp;
    }
    
    public void setCurrency(String currency){
        
        this.currency=currency;
    }
    
    public String getCurrency(){
        
        return currency;
    }
    
    public void setPeso_lleno_maximo(float peso_lleno_maximo){
        
        this.peso_lleno_maximo=peso_lleno_maximo;
    }
    
    public float getPeso_lleno_maximo(){
        
        return peso_lleno_maximo;
    }
    
    public void setPorcentaje_maximo_anticipo(float porcentaje_maximo_anticipo){
        
        this.porcentaje_maximo_anticipo=porcentaje_maximo_anticipo;
    }
    
    public float getporcentaje_maximo_anticipo(){
        
        return porcentaje_maximo_anticipo;
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
    
    /**
     * Getter for property max_valor.
     * @return Value of property max_valor.
     */
    public float getMax_valor() {
        return max_valor;
    }
    
    /**
     * Setter for property max_valor.
     * @param max_valor New value of property max_valor.
     */
    public void setMax_valor(float max_valor) {
        this.max_valor = max_valor;
    }
    
    /**
     * Getter for property max_unidad.
     * @return Value of property max_unidad.
     */
    public java.lang.String getMax_unidad() {
        return max_unidad;
    }
    
    /**
     * Setter for property max_unidad.
     * @param max_unidad New value of property max_unidad.
     */
    public void setMax_unidad(java.lang.String max_unidad) {
        this.max_unidad = max_unidad;
    }
    
}


