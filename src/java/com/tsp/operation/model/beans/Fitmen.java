/******************************************************************
* Nombre                        Fitmen.java
* Descripci�n                   Clase bean para la tabla fitmen
* Autor                         ricardo rosero
* Fecha                         06/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  rrosero
 */
public class Fitmen implements java.io.Serializable {
    //atributos
    private String platlr;
    private String numpla;
    private String numpla_ant;
    private String feccum;
    private String feccum_ant;
    private String fecdsp;
    private String oripla;
    private String despla;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String reg_status;
    private String dstrct;
    private String base;
    private String plaveh;
    private String agcplao;
    private String agcplad;
    
    
    /** Creates a new instance of Fitmen */
    /**
    * load
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ ResulSet rs
    * @throws ....... SQLException
    * @version ...... 1.0
    */
    public static Fitmen load(ResultSet rs) throws SQLException {
        Fitmen fit = new Fitmen();
        
        fit.setPlatlr(rs.getString("platlr"));
        fit.setNumpla(rs.getString("numpla"));
        fit.setPlaveh(rs.getString("plaveh"));
        fit.setFecdsp(rs.getString("fecdsp"));
        fit.setOripla(rs.getString("oripla"));
        fit.setDespla(rs.getString("despla"));
        fit.setAgcplao(rs.getString("agcplao"));
        fit.setAgcplad(rs.getString("agcplad"));
        fit.setNumpla_ant(rs.getString("numpla_ant"));
        fit.setFeccum(rs.getString("feccum"));
        fit.setFeccum_ant(rs.getString("feccum_ant"));
        fit.setLast_update(rs.getString("last_update"));
        fit.setUser_update(rs.getString("user_update"));
        fit.setCreation_date(rs.getString("creation_date"));
        fit.setCreation_user(rs.getString("creation_user"));
        fit.setReg_status(rs.getString("reg_status"));
        fit.setDstrct(rs.getString("dstrct"));
        fit.setBase(rs.getString("base"));
        
        return fit;
    }
    
    /**
    * load2
    * @autor: ....... Ing. Ricardo Rosero
    * @param ........ ResulSet rs
    * @throws ....... SQLException
    * @version ...... 1.0
    */
    public static Fitmen load2(ResultSet rs) throws SQLException {
        Fitmen fit = new Fitmen();
        
        fit.setPlatlr(rs.getString("platlr"));
        fit.setNumpla(rs.getString("numpla"));
        fit.setPlaveh(rs.getString("plaveh"));
        fit.setFecdsp(rs.getString("fecdsp"));
        fit.setOripla(rs.getString("oripla"));
        fit.setDespla(rs.getString("despla"));
        fit.setAgcplao(rs.getString("agcplao"));
        fit.setAgcplad(rs.getString("agcplad"));
        fit.setNumpla_ant(rs.getString("numpla_ant"));
        fit.setFeccum(rs.getString("feccum"));
        fit.setFeccum_ant(rs.getString("feccum_ant"));
        
        return fit;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property despla.
     * @return Value of property despla.
     */
    public java.lang.String getDespla() {
        return despla;
    }
    
    /**
     * Setter for property despla.
     * @param despla New value of property despla.
     */
    public void setDespla(java.lang.String despla) {
        this.despla = despla;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property feccum.
     * @return Value of property feccum.
     */
    public java.lang.String getFeccum() {
        return feccum;
    }
    
    /**
     * Setter for property feccum.
     * @param feccum New value of property feccum.
     */
    public void setFeccum(java.lang.String feccum) {
        this.feccum = feccum;
    }
    
    /**
     * Getter for property feccum_ant.
     * @return Value of property feccum_ant.
     */
    public java.lang.String getFeccum_ant() {
        return feccum_ant;
    }
    
    /**
     * Setter for property feccum_ant.
     * @param feccum_ant New value of property feccum_ant.
     */
    public void setFeccum_ant(java.lang.String feccum_ant) {
        this.feccum_ant = feccum_ant;
    }
    
    /**
     * Getter for property fecdsp.
     * @return Value of property fecdsp.
     */
    public java.lang.String getFecdsp() {
        return fecdsp;
    }
    
    /**
     * Setter for property fecdsp.
     * @param fecdsp New value of property fecdsp.
     */
    public void setFecdsp(java.lang.String fecdsp) {
        this.fecdsp = fecdsp;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property oripla.
     * @return Value of property oripla.
     */
    public java.lang.String getOripla() {
        return oripla;
    }
    
    /**
     * Setter for property oripla.
     * @param oripla New value of property oripla.
     */
    public void setOripla(java.lang.String oripla) {
        this.oripla = oripla;
    }
    
    /**
     * Getter for property platlr.
     * @return Value of property platlr.
     */
    public java.lang.String getPlatlr() {
        return platlr;
    }
    
    /**
     * Setter for property platlr.
     * @param plaveh New value of property platlr.
     */
    public void setPlatlr(java.lang.String platlr) {
        this.platlr = platlr;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property numpla_ant.
     * @return Value of property numpla_ant.
     */
    public java.lang.String getNumpla_ant() {
        return numpla_ant;
    }
    
    /**
     * Setter for property numpla_ant.
     * @param numpla_ant New value of property numpla_ant.
     */
    public void setNumpla_ant(java.lang.String numpla_ant) {
        this.numpla_ant = numpla_ant;
    }
    
    /**
     * Getter for property plaveh.
     * @return Value of property plaveh.
     */
    public java.lang.String getPlaveh() {
        return plaveh;
    }
    
    /**
     * Setter for property plaveh.
     * @param plaveh New value of property plaveh.
     */
    public void setPlaveh(java.lang.String plaveh) {
        this.plaveh = plaveh;
    }
    
    /**
     * Getter for property agcplad.
     * @return Value of property agcplad.
     */
    public java.lang.String getAgcplad() {
        return agcplad;
    }
    
    /**
     * Setter for property agcplad.
     * @param agcplad New value of property agcplad.
     */
    public void setAgcplad(java.lang.String agcplad) {
        this.agcplad = agcplad;
    }
    
    /**
     * Getter for property agcplao.
     * @return Value of property agcplao.
     */
    public java.lang.String getAgcplao() {
        return agcplao;
    }
    
    /**
     * Setter for property agcplao.
     * @param agcplao New value of property agcplao.
     */
    public void setAgcplao(java.lang.String agcplao) {
        this.agcplao = agcplao;
    }
    
}
