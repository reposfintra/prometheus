/*
 * Movrem.java
 *
 * Created on 24 de abril de 2007, 07:56 AM
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  E
 */
public class Movrem  implements Serializable {
     
     private String numrem;
     private double vlrrem;
     private double vlrrem2;
     private double pesoreal; 
     private double qty_value;
     private double tasa;
     
     private double vlrrem_aj;
     private double vlrrem2_aj;
     private double pesoreal_aj; 
     private double qty_value_aj;
     private double tasa_aj;
     private boolean tiene_movimiento = false;
     
    /** Creates a new instance of Movrem */
    public Movrem() {
    }
    
    /**
     * Getter for property pesoreal.
     * @return Value of property pesoreal.
     */
    public double getPesoreal() {
        return pesoreal;
    }
    
    /**
     * Setter for property pesoreal.
     * @param pesoreal New value of property pesoreal.
     */
    public void setPesoreal(double pesoreal) {
        this.pesoreal = pesoreal;
    }
    
    /**
     * Getter for property pesoreal_aj.
     * @return Value of property pesoreal_aj.
     */
    public double getPesoreal_aj() {
        return pesoreal_aj;
    }
    
    /**
     * Setter for property pesoreal_aj.
     * @param pesoreal_aj New value of property pesoreal_aj.
     */
    public void setPesoreal_aj(double pesoreal_aj) {
        this.pesoreal_aj = pesoreal_aj;
    }
    
    /**
     * Getter for property qty_value.
     * @return Value of property qty_value.
     */
    public double getQty_value() {
        return qty_value;
    }
    
    /**
     * Setter for property qty_value.
     * @param qty_value New value of property qty_value.
     */
    public void setQty_value(double qty_value) {
        this.qty_value = qty_value;
    }
    
    /**
     * Getter for property qty_value_aj.
     * @return Value of property qty_value_aj.
     */
    public double getQty_value_aj() {
        return qty_value_aj;
    }
    
    /**
     * Setter for property qty_value_aj.
     * @param qty_value_aj New value of property qty_value_aj.
     */
    public void setQty_value_aj(double qty_value_aj) {
        this.qty_value_aj = qty_value_aj;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
    
    /**
     * Getter for property tasa_aj.
     * @return Value of property tasa_aj.
     */
    public double getTasa_aj() {
        return tasa_aj;
    }
    
    /**
     * Setter for property tasa_aj.
     * @param tasa_aj New value of property tasa_aj.
     */
    public void setTasa_aj(double tasa_aj) {
        this.tasa_aj = tasa_aj;
    }
    
    /**
     * Getter for property vlrrem.
     * @return Value of property vlrrem.
     */
    public double getVlrrem() {
        return vlrrem;
    }
    
    /**
     * Setter for property vlrrem.
     * @param vlrrem New value of property vlrrem.
     */
    public void setVlrrem(double vlrrem) {
        this.vlrrem = vlrrem;
    }
    
    /**
     * Getter for property vlrrem2.
     * @return Value of property vlrrem2.
     */
    public double getVlrrem2() {
        return vlrrem2;
    }
    
    /**
     * Setter for property vlrrem2.
     * @param vlrrem2 New value of property vlrrem2.
     */
    public void setVlrrem2(double vlrrem2) {
        this.vlrrem2 = vlrrem2;
    }
    
    /**
     * Getter for property vlrrem2_aj.
     * @return Value of property vlrrem2_aj.
     */
    public double getVlrrem2_aj() {
        return vlrrem2_aj;
    }
    
    /**
     * Setter for property vlrrem2_aj.
     * @param vlrrem2_aj New value of property vlrrem2_aj.
     */
    public void setVlrrem2_aj(double vlrrem2_aj) {
        this.vlrrem2_aj = vlrrem2_aj;
    }
    
    /**
     * Getter for property vlrrem_aj.
     * @return Value of property vlrrem_aj.
     */
    public double getVlrrem_aj() {
        return vlrrem_aj;
    }
    
    /**
     * Setter for property vlrrem_aj.
     * @param vlrrem_aj New value of property vlrrem_aj.
     */
    public void setVlrrem_aj(double vlrrem_aj) {
        this.vlrrem_aj = vlrrem_aj;
    }
    
    /**
     * Getter for property tiene_movimiento.
     * @return Value of property tiene_movimiento.
     */
    public boolean isTiene_movimiento() {
        return tiene_movimiento;
    }
    
    /**
     * Setter for property tiene_movimiento.
     * @param tiene_movimiento New value of property tiene_movimiento.
     */
    public void setTiene_movimiento(boolean tiene_movimiento) {
        this.tiene_movimiento = tiene_movimiento;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }
    
}
