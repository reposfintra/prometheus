  /***************************************
    * Nombre Clase ............. Ajuste.java
    * Descripci�n  .. . . . . .  Obtiene datos para generar ajustes de planillas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  05/12/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.beans;



import com.tsp.util.Utility;
import java.util.*;



public class Ajuste {
    
    private String  distrito          = "";
    private String  planilla          = "";
    private String  placa             = ""; 
    private String  propietario       = "";
    private String  namePropietario   = "";
    private String  conductor         = "";
    private String  nameConductor     = "";
    private String  estado            = "";
    private String  origen            = "";
    private String  destino           = "";  
    private String  factura           = "";    
    private double  valor;
    private String  moneda            = "";    
    private String  base              = "";
    
    
    private List    movimientos;
    private String  monedaLocal;
    
    private double  vlrOCML;
    
    private double  vlrPlanilla;
    private double  vlrPlanillaMonOC;
    private double  neto;
    
    
    
    public Ajuste() {
        movimientos = new LinkedList();
    }
    
    
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property nameConductor.
     * @return Value of property nameConductor.
     */
    public java.lang.String getNameConductor() {
        return nameConductor;
    }
    
    /**
     * Setter for property nameConductor.
     * @param nameConductor New value of property nameConductor.
     */
    public void setNameConductor(java.lang.String nameConductor) {
        this.nameConductor = nameConductor;
    }
    
    /**
     * Getter for property namePropietario.
     * @return Value of property namePropietario.
     */
    public java.lang.String getNamePropietario() {
        return namePropietario;
    }
    
    /**
     * Setter for property namePropietario.
     * @param namePropietario New value of property namePropietario.
     */
    public void setNamePropietario(java.lang.String namePropietario) {
        this.namePropietario = namePropietario;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property propietario.
     * @return Value of property propietario.
     */
    public java.lang.String getPropietario() {
        return propietario;
    }
    
    /**
     * Setter for property propietario.
     * @param propietario New value of property propietario.
     */
    public void setPropietario(java.lang.String propietario) {
        this.propietario = propietario;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property movimientos.
     * @return Value of property movimientos.
     */
    public java.util.List getMovimientos() {
        return movimientos;
    }
    
    /**
     * Setter for property movimientos.
     * @param movimientos New value of property movimientos.
     */
    public void setMovimientos(java.util.List movimientos) {
        this.movimientos = movimientos;
    }
    
    /**
     * Getter for property monedaLocal.
     * @return Value of property monedaLocal.
     */
    public java.lang.String getMonedaLocal() {
        return monedaLocal;
    }
    
    /**
     * Setter for property monedaLocal.
     * @param monedaLocal New value of property monedaLocal.
     */
    public void setMonedaLocal(java.lang.String monedaLocal) {
        this.monedaLocal = monedaLocal;
    }
    
    /**
     * Getter for property vlrPlanilla.
     * @return Value of property vlrPlanilla.
     */
    public double getVlrPlanilla() {
        return vlrPlanilla;
    }
    
    /**
     * Setter for property vlrPlanilla.
     * @param vlrPlanilla New value of property vlrPlanilla.
     */
    public void setVlrPlanilla(double vlrPlanilla) {
        this.vlrPlanilla = vlrPlanilla;
    }
    
    /**
     * Getter for property vlrPlanillaMonOC.
     * @return Value of property vlrPlanillaMonOC.
     */
    public double getVlrPlanillaMonOC() {
        return vlrPlanillaMonOC;
    }
    
    /**
     * Setter for property vlrPlanillaMonOC.
     * @param vlrPlanillaMonOC New value of property vlrPlanillaMonOC.
     */
    public void setVlrPlanillaMonOC(double vlrPlanillaMonOC) {
        this.vlrPlanillaMonOC = vlrPlanillaMonOC;
    }
    
    /**
     * Getter for property vlrOCML.
     * @return Value of property vlrOCML.
     */
    public double getVlrOCML() {
        return vlrOCML;
    }
    
    /**
     * Setter for property vlrOCML.
     * @param vlrOCML New value of property vlrOCML.
     */
    public void setVlrOCML(double vlrOCML) {
        this.vlrOCML = vlrOCML;
    }
    
    /**
     * Getter for property neto.
     * @return Value of property neto.
     */
    public double getNeto() {
        return neto;
    }
    
    /**
     * Setter for property neto.
     * @param neto New value of property neto.
     */
    public void setNeto(double neto) {
        this.neto = neto;
    }
    
}
