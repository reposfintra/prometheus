/********************************************************************
 *      Nombre Clase.................   RepUtilidad.java
 *      Descripci�n..................   Bean del reporte de utilidad
 *      Autor........................   Ing. Tito Andr�s Maturana De La Cruz
 *      Fecha........................   20 de febrero de 2006, 04:33 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.beans;

import java.sql.*;

/**
 *
 * @author  Ing. Tito Andr�s Maturana De La Cruz
 */
public class RepUtilidad {
    private String agencia;
    private String fecdsp;
    private String cliente;
    private String oripla;
    private String despla;
    private String descripcion;
    private String fecrem;
    private double vlrpla;
    private double vlrrem;
    private String placa;
    private String vacio;
    private String estandar;
    private String planilla;
    private String remesa;
    private double unidadesdsp;
    private String periodorem;
    private String periodopla;
    private String porcentaje;
    private String desrem;
    private String orirem;
    private String dstrct;
    private String usuario;
    private String periodo;
    
    
    /** Crea una nueva instancia de  RepUtilidad */
    public RepUtilidad() {
    }
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void Load(ResultSet rs) throws SQLException{
        // = rs.getString("");
        this.agencia = rs.getString("agcpla");
        this.fecdsp = rs.getString("fecdsp");
        this.vlrpla = rs.getDouble("vlrpla");
        this.vlrrem = rs.getDouble("vlrrem");
        this.cliente = rs.getString("cliente");
        this.descripcion = rs.getString("descripcion");
        this.despla = rs.getString("despla");
        this.estandar = rs.getString("std_job_no");
        this.fecrem = rs.getString("fecrem");
        this.oripla = rs.getString("oripla");
        this.placa = rs.getString("plaveh");
        this.planilla = rs.getString("numpla");
        this.porcentaje = rs.getString("porcent");
        this.remesa = rs.getString("numrem");
        this.unidadesdsp = rs.getDouble("pesoreal");        
        this.orirem = rs.getString("orirem");
        this.desrem = rs.getString("desrem");
        this.vacio = rs.getString("tipoviaje");
        this.dstrct = rs.getString("dstrct");
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property despla.
     * @return Value of property despla.
     */
    public java.lang.String getDespla() {
        return despla;
    }
    
    /**
     * Setter for property despla.
     * @param despla New value of property despla.
     */
    public void setDespla(java.lang.String despla) {
        this.despla = despla;
    }
    
    /**
     * Getter for property estandar.
     * @return Value of property estandar.
     */
    public java.lang.String getEstandar() {
        return estandar;
    }
    
    /**
     * Setter for property estandar.
     * @param estandar New value of property estandar.
     */
    public void setEstandar(java.lang.String estandar) {
        this.estandar = estandar;
    }
    
    /**
     * Getter for property fecdsp.
     * @return Value of property fecdsp.
     */
    public java.lang.String getFecdsp() {
        return fecdsp;
    }
    
    /**
     * Setter for property fecdsp.
     * @param fecdsp New value of property fecdsp.
     */
    public void setFecdsp(java.lang.String fecdsp) {
        this.fecdsp = fecdsp;
    }
    
    /**
     * Getter for property fecrem.
     * @return Value of property fecrem.
     */
    public java.lang.String getFecrem() {
        return fecrem;
    }
    
    /**
     * Setter for property fecrem.
     * @param fecrem New value of property fecrem.
     */
    public void setFecrem(java.lang.String fecrem) {
        this.fecrem = fecrem;
    }
    
    /**
     * Getter for property oripla.
     * @return Value of property oripla.
     */
    public java.lang.String getOripla() {
        return oripla;
    }
    
    /**
     * Setter for property oripla.
     * @param oripla New value of property oripla.
     */
    public void setOripla(java.lang.String oripla) {
        this.oripla = oripla;
    }
    
    /**
     * Getter for property periodopla.
     * @return Value of property periodopla.
     */
    public java.lang.String getPeriodopla() {
        return periodopla;
    }
    
    /**
     * Setter for property periodopla.
     * @param periodopla New value of property periodopla.
     */
    public void setPeriodopla(java.lang.String periodopla) {
        this.periodopla = periodopla;
    }
    
    /**
     * Getter for property periodorem.
     * @return Value of property periodorem.
     */
    public java.lang.String getPeriodorem() {
        return periodorem;
    }
    
    /**
     * Setter for property periodorem.
     * @param periodorem New value of property periodorem.
     */
    public void setPeriodorem(java.lang.String periodorem) {
        this.periodorem = periodorem;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property porcentaje.
     * @return Value of property porcentaje.
     */
    public java.lang.String getPorcentaje() {
        return porcentaje;
    }
    
    /**
     * Setter for property porcentaje.
     * @param porcentaje New value of property porcentaje.
     */
    public void setPorcentaje(java.lang.String porcentaje) {
        this.porcentaje = porcentaje;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
    /**
     * Getter for property vacio.
     * @return Value of property vacio.
     */
    public java.lang.String getVacio() {
        return vacio;
    }
    
    /**
     * Setter for property vacio.
     * @param vacio New value of property vacio.
     */
    public void setVacio(java.lang.String vacio) {
        this.vacio = vacio;
    }
    
    /**
     * Getter for property vlrpla.
     * @return Value of property vlrpla.
     */
    public double getVlrpla() {
        return vlrpla;
    }
    
    /**
     * Setter for property vlrpla.
     * @param vlrpla New value of property vlrpla.
     */
    public void setVlrpla(double vlrpla) {
        this.vlrpla = vlrpla;
    }
    
    /**
     * Getter for property vlrrem.
     * @return Value of property vlrrem.
     */
    public double getVlrrem() {
        return vlrrem;
    }
    
    /**
     * Setter for property vlrrem.
     * @param vlrrem New value of property vlrrem.
     */
    public void setVlrrem(double vlrrem) {
        this.vlrrem = vlrrem;
    }
    
    /**
     * Getter for property desrem.
     * @return Value of property desrem.
     */
    public java.lang.String getDesrem() {
        return desrem;
    }
    
    /**
     * Setter for property desrem.
     * @param desrem New value of property desrem.
     */
    public void setDesrem(java.lang.String desrem) {
        this.desrem = desrem;
    }
    
    /**
     * Getter for property orirem.
     * @return Value of property orirem.
     */
    public java.lang.String getOrirem() {
        return orirem;
    }
    
    /**
     * Setter for property orirem.
     * @param orirem New value of property orirem.
     */
    public void setOrirem(java.lang.String orirem) {
        this.orirem = orirem;
    }
    
    /**
     * Getter for property unidadesdsp.
     * @return Value of property unidadesdsp.
     */
    public double getUnidadesdsp() {
        return unidadesdsp;
    }
    
    /**
     * Setter for property unidadesdsp.
     * @param unidadesdsp New value of property unidadesdsp.
     */
    public void setUnidadesdsp(double unidadesdsp) {
        this.unidadesdsp = unidadesdsp;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }
    
    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }
    
}
