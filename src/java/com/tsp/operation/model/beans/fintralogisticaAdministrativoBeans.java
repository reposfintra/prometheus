/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author mariana
 */
public class fintralogisticaAdministrativoBeans {
     public String reanticipo;
     public String venta;
     public String planilla;
     private String fecha_anticipo;
     private String fecha_venta;
     public String fecha_corrida;
     public String cxc_corrida;
     public String documento;
     public String referencia1;
     public String tipdoc_ultimoingreso;
     public String numingreso_ultimoingreso;
     public String empresa;
     public String respuesta;

    public String getReanticipo() {
        return reanticipo;
    }

    public void setReanticipo(String reanticipo) {
        this.reanticipo = reanticipo;
    }

    public String getVenta() {
        return venta;
    }

    public void setVenta(String venta) {
        this.venta = venta;
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public String getFecha_corrida() {
        return fecha_corrida;
    }

    public void setFecha_corrida(String fecha_corrida) {
        this.fecha_corrida = fecha_corrida;
    }

    public String getCxc_corrida() {
        return cxc_corrida;
    }

    public void setCxc_corrida(String cxc_corrida) {
        this.cxc_corrida = cxc_corrida;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getReferencia1() {
        return referencia1;
    }

    public void setReferencia1(String referencia1) {
        this.referencia1 = referencia1;
    }

    public String getTipdoc_ultimoingreso() {
        return tipdoc_ultimoingreso;
    }

    public void setTipdoc_ultimoingreso(String tipdoc_ultimoingreso) {
        this.tipdoc_ultimoingreso = tipdoc_ultimoingreso;
    }

    public String getNumingreso_ultimoingreso() {
        return numingreso_ultimoingreso;
    }

    public void setNumingreso_ultimoingreso(String numingreso_ultimoingreso) {
        this.numingreso_ultimoingreso = numingreso_ultimoingreso;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    /**
     * @return the fecha_anticipo
     */
    public String getFecha_anticipo() {
        return fecha_anticipo;
    }

    /**
     * @param fecha_anticipo the fecha_anticipo to set
     */
    public void setFecha_anticipo(String fecha_anticipo) {
        this.fecha_anticipo = fecha_anticipo;
    }

    /**
     * @return the fecha_venta
     */
    public String getFecha_venta() {
        return fecha_venta;
    }

    /**
     * @param fecha_venta the fecha_venta to set
     */
    public void setFecha_venta(String fecha_venta) {
        this.fecha_venta = fecha_venta;
    }
     
     
}
