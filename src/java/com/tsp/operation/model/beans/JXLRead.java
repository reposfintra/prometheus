/*
 * JXLRead.java
 *
 * Created on 24 de septiembre de 2005, 9:55
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import jxl.*;


/**
 *
 * @author  Mario
 */
public class JXLRead {
    
    Workbook wb;
    Sheet    sheet; 
    
    /** Creates a new instance of JXLRead */
    public JXLRead() {
    }
    
    public JXLRead(String file)throws Exception{
        wb = Workbook.getWorkbook(new File( file ));
    }

    public void abrirLibro(String file)throws Exception{
        wb = Workbook.getWorkbook(new File( file ));
    }
    
    public void obtenerHoja(String hoja) throws Exception{
        if (wb==null)
            throw new Exception("No se pudo obtener la hoja, primero deber� abrir el libro");
        sheet = wb.getSheet(hoja); 
    }
    
    public void cerrarLibro() throws Exception{
        wb.close();
    }
    
    public Sheet obtenerHoja() throws Exception{
        return sheet;        
    }    
    
    public int numeroFilas()throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo obtener el numero de filas de la hoja, primero deber� cargar una hoja existente");
        return sheet.getRows();        
    }
    
    public String obtenerDato (int fila, int columna) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo obtener el el valor de la celda, primero deber� cargar una hoja existente");
        return sheet.getCell(columna, fila).getContents();
    }
    
    public Cell obtenerCelda (int fila, int columna) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo obtener el el valor de la celda, primero deber� cargar una hoja existente");
        return sheet.getCell(columna, fila);
    }    
    
    public String [] getValores (int fila) throws Exception {
        if (sheet==null)
            throw new Exception("No se pudo obtener datos de la hoja, primero deber� cargar una hoja existente");
        
        String [] datos = null;
        
        if ( fila < sheet.getRows() ){
            Cell [] celdas = sheet.getRow( fila );
            datos = new String [celdas.length];
            for (int i = 0; i< celdas.length ; i++ ){
                datos[i] = celdas[i].getContents();
            }
        }
        return datos;       
    }
    
    /**
     * Metodo para obtener fecha excel sin importar formato
     * @param celda
     * @return
     */
    public String getFechaCellExcel(Cell celda){
        DateCell dCell = (DateCell) celda;
        TimeZone gmtZone = TimeZone.getTimeZone("GMT");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(gmtZone);
        System.out.println(sdf.format(dCell.getDate()));
        return sdf.format(dCell.getDate());
    }
    
    /**
     * Metodo para obtener numero de columnas excel
     * @return
     * @throws Exception
     */
    public int numeroColumnas() throws Exception {
        if (sheet == null) {
            throw new Exception("No se pudo obtener el numero de columnas de la hoja, primero deber� cargar una hoja existente");
        }
        return sheet.getColumns();
    }
    
    /**
     * Metodo para obtener hoja por el indice 
     * @param index
     * @throws Exception
     */
    public void obtenerHoja(int index) throws Exception{
        if (wb==null)
            throw new Exception("No se pudo obtener la hoja, primero deber� abrir el libro");
        sheet = wb.getSheet(index); 
    }
    
    /**
     * Metodo para obtener arreglo con los nombres de las hojas de excel
     * @return
     * @throws Exception
     */
    public String [] obtenerNombresHojas() throws Exception{
        if (wb==null)
            throw new Exception("No se pudo obtener la hoja, primero deber� abrir el libro");
        return wb.getSheetNames();
    }


    
}
