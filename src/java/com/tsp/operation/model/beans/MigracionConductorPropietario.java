
package com.tsp.operation.model.beans;


public class MigracionConductorPropietario {
    
    private String cedula;
    private String idMims;
    private String nombre1="NN";
    private String nombre2;
    private String apellido1="NN";
    private String apellido2;
    private String direccion;
    private String ciudad;
    private String posicion;
    private String tipo;
    private String genero;
    private String fechaNacimiento;
    private String fechaIngreso;
    private String texto1;
    private String texto2;
    
    
    public MigracionConductorPropietario() {}
    
    //SET
    
    public void setCedula(String cedula){
        this.cedula = cedula;
    }
    
    public void setIdMims(String id){
        this.idMims = id;
    }
    
    public void setNombre1(String nombre){
        this.nombre1= nombre;
    }
    
    public void setNombre2(String nombre){
        this.nombre2= nombre;
    }
    
    public void setApellido1(String apellido){
        this.apellido1 = apellido;
    }
    
    public void setApellido2(String apellido2){
        this.apellido2 = apellido2;
    }
    
    public void setDireccion(String dir){
        this.direccion = dir;
    }
    
    public void setCiudad(String ciuda){
        this.ciudad = ciuda;
    }
    
    public void setPosicion(String pos){
        this.posicion = pos;
    }
    
    public void setTipo(String type){
        this.tipo = type;
    }
    
    public void setGenero(String genero){
        this.genero = genero;
    }
    
    public void setFechaNacimiento(String fecha){
        this.fechaNacimiento = fecha;
    }
    
    public void setFechaIngreso(String fecha){
        this.fechaIngreso = fecha;
    }
    
    public void setTexto1(String texto){
        this.texto1 = texto;
    }
    
    public void setTexto2(String texto){
        this.texto2 = texto;
    }
    
    //GET
    
    
     
    public String getCedula(){
        return this.cedula;
    }
    
    public String getIdMims(){
        return this.idMims;
    }
    
    public String getNombre1(){
        return this.nombre1;
    }
    
    public String getNombre2(){
        return this.nombre2;
    }
    
    public String getApellido1(){
        return this.apellido1;
    }
    
    public String getApellido2(){
        return this.apellido2;
    }
    
    public String getDireccion(){
        return this.direccion;
    }
    
    public String getCiudad(){
        return this.ciudad;
    }
    
    public String getPosicion(){
        return this.posicion;
    }
    
    public String getTipo(){
        return this.tipo;
    }
    
    public String getGenero(){
        return this.genero;
    }
    
    public String getFechaNacimiento(){
        return this.fechaNacimiento;
    }
    
    public String getFechaIngreso(){
        return this.fechaIngreso;
    }
    
    public String getTexto1(){
        return this.texto1;
    }
    
    public String getTexto2(){
        return this.texto2;
    }
    
     
    public void format(){
        this.idMims ="";
        this.apellido1="";
        this.apellido2="";
        this.cedula="";
        this.ciudad="";
        this.direccion="";
        this.fechaIngreso="";
        this.fechaNacimiento="";
        this.genero="";
        this.nombre1="";
        this.nombre2="";
        this.texto1="";
        this.texto2="";
        this.tipo="";        
    }
}
