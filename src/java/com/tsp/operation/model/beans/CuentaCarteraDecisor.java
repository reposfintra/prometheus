/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 *
 * @author agutierrez
 */
public class CuentaCarteraDecisor {
    
    private Boolean bloqueada;
    private String entidad;
    private String numero;
    private Timestamp fechaApertura;
    private Timestamp fechaVencimiento;
    private String comportamiento;
    private String formaPago;
    private String probabilidadIncumplimiento;
    private String clasificacion;
    private String situacionTitular;
    private String oficina;
    private String ciudad;
    private String codigoDaneCiudad;
    private String codSuscriptor;
    private String tipoIdentificacion;
    private String identificacion;
    private String sector;
    private Boolean calificacionHD;
    private String tipoCuenta;
    private String tipoObligacion;
    private String tipoContrato;
    private Short ejecucionContrato;
    private Short mesesPermanencia;
    private String  calidadDeudor; 
    private String garantia;
    private String periodicidad;
    private Timestamp ultimaActualizacion;
    private String estado;
    private String estadoOrigen;
    private String creationUser;
    private String userUpdate;
    private String reclamo;
    private Valor valor;
    private String nitEmpresa;

    public CuentaCarteraDecisor() {
    }

    /**
     * @return the bloqueada
     */
    public Boolean getBloqueada() {
        return bloqueada;
    }

    /**
     * @param bloqueada the bloqueada to set
     */
    public void setBloqueada(Boolean bloqueada) {
        this.bloqueada = bloqueada;
    }

    /**
     * @return the entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * @param entidad the entidad to set
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the fechaApertura
     */
    public Timestamp getFechaApertura() {
        return fechaApertura;
    }

    /**
     * @param fechaApertura the fechaApertura to set
     */
    public void setFechaApertura(Timestamp fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    /**
     * @return the fechaVencimiento
     */
    public Timestamp getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(Timestamp fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the comportamiento
     */
    public String getComportamiento() {
        return comportamiento;
    }

    /**
     * @param comportamiento the comportamiento to set
     */
    public void setComportamiento(String comportamiento) {
        this.comportamiento = comportamiento;
    }

    /**
     * @return the formaPago
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * @param formaPago the formaPago to set
     */
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    /**
     * @return the probabilidadIncumplimiento
     */
    public String getProbabilidadIncumplimiento() {
        return probabilidadIncumplimiento;
    }

    /**
     * @param probabilidadIncumplimiento the probabilidadIncumplimiento to set
     */
    public void setProbabilidadIncumplimiento(String probabilidadIncumplimiento) {
        this.probabilidadIncumplimiento = probabilidadIncumplimiento;
    }

    /**
     * @return the clasificacion
     */
    public String getClasificacion() {
        return clasificacion;
    }

    /**
     * @param clasificacion the clasificacion to set
     */
    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    /**
     * @return the situacionTitular
     */
    public String getSituacionTitular() {
        return situacionTitular;
    }

    /**
     * @param situacionTitular the situacionTitular to set
     */
    public void setSituacionTitular(String situacionTitular) {
        this.situacionTitular = situacionTitular;
    }

    /**
     * @return the oficina
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * @param oficina the oficina to set
     */
    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the codigoDaneCiudad
     */
    public String getCodigoDaneCiudad() {
        return codigoDaneCiudad;
    }

    /**
     * @param codigoDaneCiudad the codigoDaneCiudad to set
     */
    public void setCodigoDaneCiudad(String codigoDaneCiudad) {
        this.codigoDaneCiudad = codigoDaneCiudad;
    }

    /**
     * @return the codSuscriptor
     */
    public String getCodSuscriptor() {
        return codSuscriptor;
    }

    /**
     * @param codSuscriptor the codSuscriptor to set
     */
    public void setCodSuscriptor(String codSuscriptor) {
        this.codSuscriptor = codSuscriptor;
    }

    /**
     * @return the tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the calificacionHD
     */
    public Boolean getCalificacionHD() {
        return calificacionHD;
    }

    /**
     * @param calificacionHD the calificacionHD to set
     */
    public void setCalificacionHD(Boolean calificacionHD) {
        this.calificacionHD = calificacionHD;
    }

    /**
     * @return the tipoCuenta
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * @param tipoCuenta the tipoCuenta to set
     */
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    /**
     * @return the tipoObligacion
     */
    public String getTipoObligacion() {
        return tipoObligacion;
    }

    /**
     * @param tipoObligacion the tipoObligacion to set
     */
    public void setTipoObligacion(String tipoObligacion) {
        this.tipoObligacion = tipoObligacion;
    }

    /**
     * @return the tipoContrato
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * @param tipoContrato the tipoContrato to set
     */
    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    /**
     * @return the ejecucionContrato
     */
    public Short getEjecucionContrato() {
        return ejecucionContrato;
    }

    /**
     * @param ejecucionContrato the ejecucionContrato to set
     */
    public void setEjecucionContrato(Short ejecucionContrato) {
        this.ejecucionContrato = ejecucionContrato;
    }

    /**
     * @return the mesesPermanencia
     */
    public Short getMesesPermanencia() {
        return mesesPermanencia;
    }

    /**
     * @param mesesPermanencia the mesesPermanencia to set
     */
    public void setMesesPermanencia(Short mesesPermanencia) {
        this.mesesPermanencia = mesesPermanencia;
    }

    /**
     * @return the calidadDeudor
     */
    public String getCalidadDeudor() {
        return calidadDeudor;
    }

    /**
     * @param calidadDeudor the calidadDeudor to set
     */
    public void setCalidadDeudor(String calidadDeudor) {
        this.calidadDeudor = calidadDeudor;
    }

    /**
     * @return the garantia
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * @param garantia the garantia to set
     */
    public void setGarantia(String garantia) {
        this.garantia = garantia;
    }

    /**
     * @return the periodicidad
     */
    public String getPeriodicidad() {
        return periodicidad;
    }

    /**
     * @param periodicidad the periodicidad to set
     */
    public void setPeriodicidad(String periodicidad) {
        this.periodicidad = periodicidad;
    }

    /**
     * @return the ultimaActualizacion
     */
    public Timestamp getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    /**
     * @param ultimaActualizacion the ultimaActualizacion to set
     */
    public void setUltimaActualizacion(Timestamp ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the estadoOrigen
     */
    public String getEstadoOrigen() {
        return estadoOrigen;
    }

    /**
     * @param estadoOrigen the estadoOrigen to set
     */
    public void setEstadoOrigen(String estadoOrigen) {
        this.estadoOrigen = estadoOrigen;
    }

    /**
     * @return the creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * @param creationUser the creationUser to set
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * @return the userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * @param userUpdate the userUpdate to set
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * @return the reclamo
     */
    public String getReclamo() {
        return reclamo;
    }

    /**
     * @param reclamo the reclamo to set
     */
    public void setReclamo(String reclamo) {
        this.reclamo = reclamo;
    }

    /**
     * @return the valor
     */
    public Valor getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(Valor valor) {
        this.valor = valor;
    }

    /**
     * @return the nitEmpresa
     */
    public String getNitEmpresa() {
        return nitEmpresa;
    }

    /**
     * @param nitEmpresa the nitEmpresa to set
     */
    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }
    
     
    
    
}
