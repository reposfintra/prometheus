/******************************************************************************************
 * Nombre clase : ............... FlotaUtilizada.java                                     *
 * Descripcion :................. Bean utilizado para el reporte de extracto. Información *
 *                                propietario                                             *
 * Autor :....................... Ing. Henry A.Osorio González                            *
 * Fecha :....................... 01 de enero de 2006, 08:00 AM                           *
 * Version :..................... 1.0                                                     *
 * Copyright :................... Fintravalores S.A.                                 *
 ******************************************************************************************/

package com.tsp.operation.model.beans;

import java.util.*;
import java.io.*;

public class EncPropietario implements Serializable{
    
    private String fecha;
    private String corrida;
    private String propietario;
    private String nombre;
    private String fechaCumplido; 
    private String tipo_pago;
    private String banco_transfer;
    private String suc_transfer;
    private String tipo_cuenta;
    private String no_cuenta;
    private String cedula_cuenta;
    private String nombre_cuenta;
    private Vector facturas;
    
    /** Creates a new instance of EncPropietario */
    public EncPropietario() {
    }
    
    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }
    
    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }
    
    /**
     * Getter for property facturas.
     * @return Value of property facturas.
     */
    public java.util.Vector getFacturas() {
        return facturas;
    }
    
    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.util.Vector facturas) {
        this.facturas = facturas;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property propietario.
     * @return Value of property propietario.
     */
    public java.lang.String getPropietario() {
        return propietario;
    }
    
    /**
     * Setter for property propietario.
     * @param propietario New value of property propietario.
     */
    public void setPropietario(java.lang.String propietario) {
        this.propietario = propietario;
    }
    
    /**
     * Getter for property fechaCumplido.
     * @return Value of property fechaCumplido.
     */
    public java.lang.String getFechaCumplido() {
        return fechaCumplido;
    }
    
    /**
     * Setter for property fechaCumplido.
     * @param fechaCumplido New value of property fechaCumplido.
     */
    public void setFechaCumplido(java.lang.String fechaCumplido) {
        this.fechaCumplido = fechaCumplido;
    }
    
    /**
     * Getter for property tipo_pago.
     * @return Value of property tipo_pago.
     */
    public java.lang.String getTipo_pago () {
        return tipo_pago;
    }
    
    /**
     * Setter for property tipo_pago.
     * @param tipo_pago New value of property tipo_pago.
     */
    public void setTipo_pago (java.lang.String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }
    
    /**
     * Getter for property banco_transfer.
     * @return Value of property banco_transfer.
     */
    public java.lang.String getBanco_transfer () {
        return banco_transfer;
    }
    
    /**
     * Setter for property banco_transfer.
     * @param banco_transfer New value of property banco_transfer.
     */
    public void setBanco_transfer (java.lang.String banco_transfer) {
        this.banco_transfer = banco_transfer;
    }
    
    /**
     * Getter for property suc_transfer.
     * @return Value of property suc_transfer.
     */
    public java.lang.String getSuc_transfer () {
        return suc_transfer;
    }
    
    /**
     * Setter for property suc_transfer.
     * @param suc_transfer New value of property suc_transfer.
     */
    public void setSuc_transfer (java.lang.String suc_transfer) {
        this.suc_transfer = suc_transfer;
    }
    
    /**
     * Getter for property tipo_cuenta.
     * @return Value of property tipo_cuenta.
     */
    public java.lang.String getTipo_cuenta () {
        return tipo_cuenta;
    }
    
    /**
     * Setter for property tipo_cuenta.
     * @param tipo_cuenta New value of property tipo_cuenta.
     */
    public void setTipo_cuenta (java.lang.String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }
    
    /**
     * Getter for property no_cuenta.
     * @return Value of property no_cuenta.
     */
    public java.lang.String getNo_cuenta () {
        return no_cuenta;
    }
    
    /**
     * Setter for property no_cuenta.
     * @param no_cuenta New value of property no_cuenta.
     */
    public void setNo_cuenta (java.lang.String no_cuenta) {
        this.no_cuenta = no_cuenta;
    }
    
    /**
     * Getter for property cedula_cuenta.
     * @return Value of property cedula_cuenta.
     */
    public java.lang.String getCedula_cuenta () {
        return cedula_cuenta;
    }
    
    /**
     * Setter for property cedula_cuenta.
     * @param cedula_cuenta New value of property cedula_cuenta.
     */
    public void setCedula_cuenta (java.lang.String cedula_cuenta) {
        this.cedula_cuenta = cedula_cuenta;
    }
    
    /**
     * Getter for property nombre_cuenta.
     * @return Value of property nombre_cuenta.
     */
    public java.lang.String getNombre_cuenta () {
        return nombre_cuenta;
    }
    
    /**
     * Setter for property nombre_cuenta.
     * @param nombre_cuenta New value of property nombre_cuenta.
     */
    public void setNombre_cuenta (java.lang.String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }
    
}
