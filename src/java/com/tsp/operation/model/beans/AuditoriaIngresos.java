/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author lcanchila
 */
public class AuditoriaIngresos {
    private String negocio;
    private String cedula;
    private String nombre_cliente;
    private String vencimiento_mayor;
    private String fecha_pago_ingreso;
    private String fecha_vencimiento_mayor;
    private String diferencia_pago;
    private double valor_saldo_foto;
    private double interes_mora;
    private double gasto_cobranza;
    private String num_ingreso;
    private double valor_ingreso;
    private double valor_cxc_ingreso; 
    private double G16252145;
    private double G94350302;
    private double GI010010014205;
    private double GI010130014205;
    private double I16252147;
    private double I94350301;
    private double II010010014170;
    private double II010130024170;
    private double I010140014170;
    private double I010140014205;
    private double I28150530;
    private double I28150531;
    private double valor_ixm_ingreso;
    private double valor_gac_ingreso;
    private String convenio;
    private double valor_asignado;
    private double porc_item;
    private String periodo;
    private double valor_vencido;
    private double valor_xvencer;
    private int cantidad;
    private double valor_pagos;
    private String unidad_negocio;
    private String afiliado;
    private String fecha_aprobacion;
    private String fecha_desembolso;
    private String periodo_desembolso;
    private String total_desembolsado;
    private double saldo_porvencer;
    private String cuota;
    private String cuotas_vencidas;
    private String analista;
    private String asesor_comercial;
    private String cobrador_telefonico;
    private String cobrador_campo;
    private String fecha_ultimo_pago;
    private String tramo_anterior;
    private String direccion;
    private String telefono;
    private String celular;
    private String email;
    private String estrato;
    private String ocupacion;
    private String departamento;
    private String ciudad;
    private String municipio;
    private String barrio;
    private String nombre_empresa;
    private String cargo;
    private double colocacion;
    private double saldo;
    private String vencimiento_mayor_maximo;
    private String plazo;
    private int cuentas_caidas;
    private int cuentas_debido;
    private double valor_debido;
    private double valor_cuota;
    private double valor_recaudo;
    private double valor_recaudo_debido;
    private double cartera_con_sancion;
    private String caida;

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre_cliente
     */
    public String getNombre_cliente() {
        return nombre_cliente;
    }

    /**
     * @param nombre_cliente the nombre_cliente to set
     */
    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    /**
     * @return the vencimiento_mayor
     */
    public String getVencimiento_mayor() {
        return vencimiento_mayor;
    }

    /**
     * @param vencimiento_mayor the vencimiento_mayor to set
     */
    public void setVencimiento_mayor(String vencimiento_mayor) {
        this.vencimiento_mayor = vencimiento_mayor;
    }

    /**
     * @return the fecha_pago_ingreso
     */
    public String getFecha_pago_ingreso() {
        return fecha_pago_ingreso;
    }

    /**
     * @param fecha_pago_ingreso the fecha_pago_ingreso to set
     */
    public void setFecha_pago_ingreso(String fecha_pago_ingreso) {
        this.fecha_pago_ingreso = fecha_pago_ingreso;
    }

    /**
     * @return the fecha_vencimiento_mayor
     */
    public String getFecha_vencimiento_mayor() {
        return fecha_vencimiento_mayor;
    }

    /**
     * @param fecha_vencimiento_mayor the fecha_vencimiento_mayor to set
     */
    public void setFecha_vencimiento_mayor(String fecha_vencimiento_mayor) {
        this.fecha_vencimiento_mayor = fecha_vencimiento_mayor;
    }

    /**
     * @return the diferencia_pago
     */
    public String getDiferencia_pago() {
        return diferencia_pago;
    }

    /**
     * @param diferencia_pago the diferencia_pago to set
     */
    public void setDiferencia_pago(String diferencia_pago) {
        this.diferencia_pago = diferencia_pago;
    }

    /**
     * @return the valor_saldo_foto
     */
    public double getValor_saldo_foto() {
        return valor_saldo_foto;
    }

    /**
     * @param valor_saldo_foto the valor_saldo_foto to set
     */
    public void setValor_saldo_foto(double valor_saldo_foto) {
        this.valor_saldo_foto = valor_saldo_foto;
    }

    /**
     * @return the interes_mora
     */
    public double getInteres_mora() {
        return interes_mora;
    }

    /**
     * @param interes_mora the interes_mora to set
     */
    public void setInteres_mora(double interes_mora) {
        this.interes_mora = interes_mora;
    }

    /**
     * @return the gasto_cobranza
     */
    public double getGasto_cobranza() {
        return gasto_cobranza;
    }

    /**
     * @param gasto_cobranza the gasto_cobranza to set
     */
    public void setGasto_cobranza(double gasto_cobranza) {
        this.gasto_cobranza = gasto_cobranza;
    }

    /**
     * @return the num_ingreso
     */
    public String getNum_ingreso() {
        return num_ingreso;
    }

    /**
     * @param num_ingreso the num_ingreso to set
     */
    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    /**
     * @return the valor_ingreso
     */
    public double getValor_ingreso() {
        return valor_ingreso;
    }

    /**
     * @param valor_ingreso the valor_ingreso to set
     */
    public void setValor_ingreso(double valor_ingreso) {
        this.valor_ingreso = valor_ingreso;
    }

    /**
     * @return the valor_cxc_ingreso
     */
    public double getValor_cxc_ingreso() {
        return valor_cxc_ingreso;
    }

    /**
     * @param valor_cxc_ingreso the valor_cxc_ingreso to set
     */
    public void setValor_cxc_ingreso(double valor_cxc_ingreso) {
        this.valor_cxc_ingreso = valor_cxc_ingreso;
    }

    /**
     * @return the G16252145
     */
    public double getG16252145() {
        return G16252145;
    }

    /**
     * @param G16252145 the G16252145 to set
     */
    public void setG16252145(double G16252145) {
        this.G16252145 = G16252145;
    }

    /**
     * @return the G94350302
     */
    public double getG94350302() {
        return G94350302;
    }

    /**
     * @param G94350302 the G94350302 to set
     */
    public void setG94350302(double G94350302) {
        this.G94350302 = G94350302;
    }

    /**
     * @return the GI010010014205
     */
    public double getGI010010014205() {
        return GI010010014205;
    }

    /**
     * @param GI010010014205 the GI010010014205 to set
     */
    public void setGI010010014205(double GI010010014205) {
        this.GI010010014205 = GI010010014205;
    }

    /**
     * @return the GI010130014205
     */
    public double getGI010130014205() {
        return GI010130014205;
    }

    /**
     * @param GI010130014205 the GI010130014205 to set
     */
    public void setGI010130014205(double GI010130014205) {
        this.GI010130014205 = GI010130014205;
    }

    /**
     * @return the I16252147
     */
    public double getI16252147() {
        return I16252147;
    }

    /**
     * @param I16252147 the I16252147 to set
     */
    public void setI16252147(double I16252147) {
        this.I16252147 = I16252147;
    }

    /**
     * @return the I94350301
     */
    public double getI94350301() {
        return I94350301;
    }

    /**
     * @param I94350301 the I94350301 to set
     */
    public void setI94350301(double I94350301) {
        this.I94350301 = I94350301;
    }

    /**
     * @return the II010010014170
     */
    public double getII010010014170() {
        return II010010014170;
    }

    /**
     * @param II010010014170 the II010010014170 to set
     */
    public void setII010010014170(double II010010014170) {
        this.II010010014170 = II010010014170;
    }

    /**
     * @return the II010130024170
     */
    public double getII010130024170() {
        return II010130024170;
    }

    /**
     * @param II010130024170 the II010130024170 to set
     */
    public void setII010130024170(double II010130024170) {
        this.II010130024170 = II010130024170;
    }

    public double getI28150530() {
        return I28150530;
    }

    public void setI28150530(double I28150530) {
        this.I28150530 = I28150530;
    }

    public double getI28150531() {
        return I28150531;
    }

    public void setI28150531(double I28150531) {
        this.I28150531 = I28150531;
    }

    /**
     * @return the valor_ixm_ingreso
     */
    public double getValor_ixm_ingreso() {
        return valor_ixm_ingreso;
    }

    /**
     * @param valor_ixm_ingreso the valor_ixm_ingreso to set
     */
    public void setValor_ixm_ingreso(double valor_ixm_ingreso) {
        this.valor_ixm_ingreso = valor_ixm_ingreso;
    }

    /**
     * @return the valor_gac_ingreso
     */
    public double getValor_gac_ingreso() {
        return valor_gac_ingreso;
    }

    /**
     * @param valor_gac_ingreso the valor_gac_ingreso to set
     */
    public void setValor_gac_ingreso(double valor_gac_ingreso) {
        this.valor_gac_ingreso = valor_gac_ingreso;
    }

    /**
     * @return the convenio
     */
    public String getConvenio() {
        return convenio;
    }

    /**
     * @param convenio the convenio to set
     */
    public void setConvenio(String convenio) {
        this.convenio = convenio;
    }

    /**
     * @return the valor_asignado
     */
    public double getValor_asignado() {
        return valor_asignado;
    }

    /**
     * @param valor_asignado the valor_asignado to set
     */
    public void setValor_asignado(double valor_asignado) {
        this.valor_asignado = valor_asignado;
    }

    /**
     * @return the porc_item
     */
    public double getPorc_item() {
        return porc_item;
    }

    /**
     * @param porc_item the porc_item to set
     */
    public void setPorc_item(double porc_item) {
        this.porc_item = porc_item;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the valor_vencido
     */
    public double getValor_vencido() {
        return valor_vencido;
    }

    /**
     * @param valor_vencido the valor_vencido to set
     */
    public void setValor_vencido(double valor_vencido) {
        this.valor_vencido = valor_vencido;
    }

    /**
     * @return the valor_xvencer
     */
    public double getValor_xvencer() {
        return valor_xvencer;
    }

    /**
     * @param valor_xvencer the valor_xvencer to set
     */
    public void setValor_xvencer(double valor_xvencer) {
        this.valor_xvencer = valor_xvencer;
    }

    /**
     * @return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the valor_pagos
     */
    public double getValor_pagos() {
        return valor_pagos;
    }

    /**
     * @param valor_pagos the valor_pagos to set
     */
    public void setValor_pagos(double valor_pagos) {
        this.valor_pagos = valor_pagos;
    }

    /**
     * @return the unidad_negocio
     */
    public String getUnidad_negocio() {
        return unidad_negocio;
    }

    /**
     * @param unidad_negocio the unidad_negocio to set
     */
    public void setUnidad_negocio(String unidad_negocio) {
        this.unidad_negocio = unidad_negocio;
    }

    /**
     * @return the afiliado
     */
    public String getAfiliado() {
        return afiliado;
    }

    /**
     * @param afiliado the afiliado to set
     */
    public void setAfiliado(String afiliado) {
        this.afiliado = afiliado;
    }

    /**
     * @return the fecha_aprobacion
     */
    public String getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    /**
     * @param fecha_aprobacion the fecha_aprobacion to set
     */
    public void setFecha_aprobacion(String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    /**
     * @return the fecha_desembolso
     */
    public String getFecha_desembolso() {
        return fecha_desembolso;
    }

    /**
     * @param fecha_desembolso the fecha_desembolso to set
     */
    public void setFecha_desembolso(String fecha_desembolso) {
        this.fecha_desembolso = fecha_desembolso;
    }

    /**
     * @return the periodo_desembolso
     */
    public String getPeriodo_desembolso() {
        return periodo_desembolso;
    }

    /**
     * @param periodo_desembolso the periodo_desembolso to set
     */
    public void setPeriodo_desembolso(String periodo_desembolso) {
        this.periodo_desembolso = periodo_desembolso;
    }

    /**
     * @return the total_desembolsado
     */
    public String getTotal_desembolsado() {
        return total_desembolsado;
    }

    /**
     * @param total_desembolsado the total_desembolsado to set
     */
    public void setTotal_desembolsado(String total_desembolsado) {
        this.total_desembolsado = total_desembolsado;
    }

    /**
     * @return the saldo_porvencer
     */
    public double getSaldo_porvencer() {
        return saldo_porvencer;
    }

    /**
     * @param saldo_porvencer the saldo_porvencer to set
     */
    public void setSaldo_porvencer(double saldo_porvencer) {
        this.saldo_porvencer = saldo_porvencer;
    }

    /**
     * @return the cuota
     */
    public String getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(String cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the cuotas_vencidas
     */
    public String getCuotas_vencidas() {
        return cuotas_vencidas;
    }

    /**
     * @param cuotas_vencidas the cuotas_vencidas to set
     */
    public void setCuotas_vencidas(String cuotas_vencidas) {
        this.cuotas_vencidas = cuotas_vencidas;
    }

    /**
     * @return the analista
     */
    public String getAnalista() {
        return analista;
    }

    /**
     * @param analista the analista to set
     */
    public void setAnalista(String analista) {
        this.analista = analista;
    }

    /**
     * @return the asesor_comercial
     */
    public String getAsesor_comercial() {
        return asesor_comercial;
    }

    /**
     * @param asesor_comercial the asesor_comercial to set
     */
    public void setAsesor_comercial(String asesor_comercial) {
        this.asesor_comercial = asesor_comercial;
    }

    /**
     * @return the cobrador_telefonico
     */
    public String getCobrador_telefonico() {
        return cobrador_telefonico;
    }

    /**
     * @param cobrador_telefonico the cobrador_telefonico to set
     */
    public void setCobrador_telefonico(String cobrador_telefonico) {
        this.cobrador_telefonico = cobrador_telefonico;
    }

    /**
     * @return the cobrador_campo
     */
    public String getCobrador_campo() {
        return cobrador_campo;
    }

    /**
     * @param cobrador_campo the cobrador_campo to set
     */
    public void setCobrador_campo(String cobrador_campo) {
        this.cobrador_campo = cobrador_campo;
    }

    /**
     * @return the fecha_ultimo_pago
     */
    public String getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    /**
     * @param fecha_ultimo_pago the fecha_ultimo_pago to set
     */
    public void setFecha_ultimo_pago(String fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    /**
     * @return the tramo_anterior
     */
    public String getTramo_anterior() {
        return tramo_anterior;
    }

    /**
     * @param tramo_anterior the tramo_anterior to set
     */
    public void setTramo_anterior(String tramo_anterior) {
        this.tramo_anterior = tramo_anterior;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the estrato
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * @param estrato the estrato to set
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * @return the ocupacion
     */
    public String getOcupacion() {
        return ocupacion;
    }

    /**
     * @param ocupacion the ocupacion to set
     */
    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    /**
     * @return the departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * @param departamento the departamento to set
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the municipio
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     * @param municipio the municipio to set
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    /**
     * @return the barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * @param barrio the barrio to set
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * @return the nombre_empresa
     */
    public String getNombre_empresa() {
        return nombre_empresa;
    }

    /**
     * @param nombre_empresa the nombre_empresa to set
     */
    public void setNombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    /**
     * @return the cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * @return the colocacion
     */
    public double getColocacion() {
        return colocacion;
    }

    /**
     * @param colocacion the colocacion to set
     */
    public void setColocacion(double colocacion) {
        this.colocacion = colocacion;
    }

    /**
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    /**
     * @return the vencimiento_mayor_maximo
     */
    public String getVencimiento_mayor_maximo() {
        return vencimiento_mayor_maximo;
    }

    /**
     * @param vencimiento_mayor_maximo the vencimiento_mayor_maximo to set
     */
    public void setVencimiento_mayor_maximo(String vencimiento_mayor_maximo) {
        this.vencimiento_mayor_maximo = vencimiento_mayor_maximo;
    }

    /**
     * @return the plazo
     */
    public String getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the cuentas_caidas
     */
    public int getCuentas_caidas() {
        return cuentas_caidas;
    }

    /**
     * @param cuentas_caidas the cuentas_caidas to set
     */
    public void setCuentas_caidas(int cuentas_caidas) {
        this.cuentas_caidas = cuentas_caidas;
    }

    /**
     * @return the cuentas_debido
     */
    public int getCuentas_debido() {
        return cuentas_debido;
    }

    /**
     * @param cuentas_debido the cuentas_debido to set
     */
    public void setCuentas_debido(int cuentas_debido) {
        this.cuentas_debido = cuentas_debido;
    }

    /**
     * @return the valor_debido
     */
    public double getValor_debido() {
        return valor_debido;
    }

    /**
     * @param valor_debido the valor_debido to set
     */
    public void setValor_debido(double valor_debido) {
        this.valor_debido = valor_debido;
    }

    /**
     * @return the valor_cuota
     */
    public double getValor_cuota() {
        return valor_cuota;
    }

    /**
     * @param valor_cuota the valor_cuota to set
     */
    public void setValor_cuota(double valor_cuota) {
        this.valor_cuota = valor_cuota;
    }

    /**
     * @return the valor_recaudo
     */
    public double getValor_recaudo() {
        return valor_recaudo;
    }

    /**
     * @param valor_recaudo the valor_recaudo to set
     */
    public void setValor_recaudo(double valor_recaudo) {
        this.valor_recaudo = valor_recaudo;
    }

    /**
     * @return the valor_recaudo_debido
     */
    public double getValor_recaudo_debido() {
        return valor_recaudo_debido;
    }

    /**
     * @param valor_recaudo_debido the valor_recaudo_debido to set
     */
    public void setValor_recaudo_debido(double valor_recaudo_debido) {
        this.valor_recaudo_debido = valor_recaudo_debido;
    }

    /**
     * @return the cartera_con_sancion
     */
    public double getCartera_con_sancion() {
        return cartera_con_sancion;
    }

    /**
     * @param cartera_con_sancion the cartera_con_sancion to set
     */
    public void setCartera_con_sancion(double cartera_con_sancion) {
        this.cartera_con_sancion = cartera_con_sancion;
    }

    /**
     * @return the caida
     */
    public String getCaida() {
        return caida;
    }

    /**
     * @param caida the caida to set
     */
    public void setCaida(String caida) {
        this.caida = caida;
    }

    /**
     * @return the I010140014170
     */
    public double getI010140014170() {
        return I010140014170;
    }

    /**
     * @param I010140014170 the I010140014170 to set
     */
    public void setI010140014170(double I010140014170) {
        this.I010140014170 = I010140014170;
    }

    /**
     * @return the I010140014205
     */
    public double getI010140014205() {
        return I010140014205;
    }

    /**
     * @param I010140014205 the I010140014205 to set
     */
    public void setI010140014205(double I010140014205) {
        this.I010140014205 = I010140014205;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}
