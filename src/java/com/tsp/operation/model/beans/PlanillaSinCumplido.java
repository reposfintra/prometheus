/************************************************************************************
 * Nombre clase : ................ PlanillaSinCumplido.java                         *
 * Descripcion :.................. Clase que maneja los atributos relacionados con  *
 *                                el reporte de planillas Sin Cumplido              *
 * Autor :........................ Ing. David Velasquez Gonzalez 
 * Modified:......................Ing. Enrique De Lavalle
 * Fecha :........................ 24 de noviembre de 2006, 3:00 pm                 *
 * Version :...................... 1.0                                              *
 * Copyright :.................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
/**
 *
 * @author  David
 */
public class PlanillaSinCumplido {
    
    private String numpla;
    private String generador_oc;
    private String ot;
    private double valor_ot;
    private String fecha_gen;
    private String placa;
    private String fecha_ent;
    private String sede;
    private String origen;
    private String destino;
    private String agencia_dest;
    private String tipoviaje;
    private String discrepancia;
    private String cliente;
    private String cod_cliente;
    private String no_factura;
    private double valor_fact;
    private double anticipo;
    private String Fecha_anticipo;
    private double valor_oc;
    private String vacio;
    private int dias;
    private double calificacion;
    private String agencia;
    private String asociada;
    private String clasificacion;
    private String ruta_pla; 
    private int contaroc;
    private String pais;
    private String contarDia = "0";
    private String fechareporte;
    private int diasDeGen;
    private String codOrig;
    private String codDest;
   
    
    /** Creates a new instance of PlanillaSinCumplido */
    public PlanillaSinCumplido() {
    }
    
    public static PlanillaSinCumplido load(ResultSet rs)throws Exception{
        PlanillaSinCumplido datos = new PlanillaSinCumplido();
        datos.setNumpla(rs.getString("NUMPLA"));
        datos.setGenerador_oc(rs.getString("GENERADOR_OC"));
        datos.setOt(rs.getString("REMESA"));
        datos.setValor_ot(rs.getDouble("VALOR_OT"));
        datos.setFecha_gen(rs.getString("FECHA_GENERACION"));
        datos.setPlaca(rs.getString("PLACA"));
        datos.setFecha_ent((!rs.getString("FECHA_ENTREGA").equals("0099-01-01 00:00:00"))?rs.getString("FECHA_ENTREGA"):"");
        datos.setSede(rs.getString("SEDE_PAGO"));
        datos.setOrigen(rs.getString("ORIGEN"));
        datos.setDestino(rs.getString("DESTINO"));
        datos.setTipoviaje(rs.getString("TIPOVIAJE"));
        datos.setCliente(rs.getString("NOMBRE_CLIENTE"));
        datos.setCod_cliente(rs.getString("CODIGO"));
        datos.setNo_factura(rs.getString("NO_FACTURA"));
        datos.setValor_fact(rs.getDouble("FACTURA"));
        datos.setAnticipo(rs.getDouble("VALOR_ANTICIPO"));
        //datos.setFecha_anticipo(rs.getString("FECHA_ANTICIPO"));
        datos.setFecha_anticipo((!rs.getString("FECHA_ANTICIPO").equals("0099-01-01 00:00:00"))?rs.getString("FECHA_ANTICIPO"):"");
        datos.setValor_oc(rs.getDouble("VALOR"));
        datos.setVacio((rs.getString("VACIO").equals("VAC"))?"VACIO":"LLENO");
        datos.setDias(rs.getInt("DIF"));
        datos.setAgencia(rs.getString("AGENCIA"));
        datos.setRuta_pla(rs.getString("ruta_pla"));
        datos.setPais(rs.getString("paises"));    
        datos.setCodOrig(rs.getString("cod_orig"));
        datos.setCodDest(rs.getString("cod_dest"));
        
        if(!datos.getCod_cliente().equals("") && datos.getCod_cliente()!=null){
            if(datos.getCod_cliente().equals("000079") && datos.getCodOrig().equals("XU")){
                datos.setAgencia_dest("PUERTO TEJADA");
            }
            else if (datos.getCod_cliente().equals("000734") || datos.getCod_cliente().equals("000369")){
                datos.setAgencia_dest("XOM MOBIL");
            }
            else if (datos.getCod_cliente().equals("000448") && (datos.getCodDest().equals("CU") || datos.getCodDest().equals("BU"))){
                datos.setAgencia_dest("PEREIRA");
            }
            else{
                datos.setAgencia_dest(rs.getString("ASOCIADA"));
            }
        }
        if(datos.getTipoviaje()!=null && !datos.getTipoviaje().equals("")){
            String tipoviaje = datos.getTipoviaje();
            if(tipoviaje.equals("RM") || tipoviaje.equals("DM") || tipoviaje.equals("RE")){
                datos.setTipoviaje("INTERNACIONAL");
            }
            else if(tipoviaje.equals("DC") || tipoviaje.equals("RC")){
                datos.setTipoviaje("URBANO");
            }
            else if(tipoviaje.equals("NA")){
                datos.setTipoviaje("NACIONAL");
            }
        }
        
        datos.setFechareporte(rs.getString("FECHAREPORTE"));
        datos.setDiasDeGen(rs.getInt("dias_gen"));  
      
        
        return datos;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla() {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla(java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property generador_oc.
     * @return Value of property generador_oc.
     */
    public java.lang.String getGenerador_oc() {
        return generador_oc;
    }
    
    /**
     * Setter for property generador_oc.
     * @param generador_oc New value of property generador_oc.
     */
    public void setGenerador_oc(java.lang.String generador_oc) {
        this.generador_oc = generador_oc;
    }
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt() {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt(java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property valor_ot.
     * @return Value of property valor_ot.
     */
    public double getValor_ot() {
        return valor_ot;
    }
    
    /**
     * Setter for property valor_ot.
     * @param valor_ot New value of property valor_ot.
     */
    public void setValor_ot(double valor_ot) {
        this.valor_ot = valor_ot;
    }
    
    /**
     * Getter for property fecha_gen.
     * @return Value of property fecha_gen.
     */
    public java.lang.String getFecha_gen() {
        return fecha_gen;
    }
    
    /**
     * Setter for property fecha_gen.
     * @param fecha_gen New value of property fecha_gen.
     */
    public void setFecha_gen(java.lang.String fecha_gen) {
        this.fecha_gen = fecha_gen;
    }
    
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property fecha_ent.
     * @return Value of property fecha_ent.
     */
    public java.lang.String getFecha_ent() {
        return fecha_ent;
    }
    
    /**
     * Setter for property fecha_ent.
     * @param fecha_ent New value of property fecha_ent.
     */
    public void setFecha_ent(java.lang.String fecha_ent) {
        this.fecha_ent = fecha_ent;
    }
    
    /**
     * Getter for property sede.
     * @return Value of property sede.
     */
    public java.lang.String getSede() {
        return sede;
    }
    
    /**
     * Setter for property sede.
     * @param sede New value of property sede.
     */
    public void setSede(java.lang.String sede) {
        this.sede = sede;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
 
    /**
     * Getter for property tipoviaje.
     * @return Value of property tipoviaje.
     */
    public java.lang.String getTipoviaje() {
        return tipoviaje;
    }
    
    /**
     * Setter for property tipoviaje.
     * @param tipoviaje New value of property tipoviaje.
     */
    public void setTipoviaje(java.lang.String tipoviaje) {
        this.tipoviaje = tipoviaje;
    }
    
    /**
     * Getter for property discrepancia.
     * @return Value of property discrepancia.
     */
    public java.lang.String getDiscrepancia() {
        return discrepancia;
    }
    
    /**
     * Setter for property discrepancia.
     * @param discrepancia New value of property discrepancia.
     */
    public void setDiscrepancia(java.lang.String discrepancia) {
        this.discrepancia = discrepancia;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property cod_cliente.
     * @return Value of property cod_cliente.
     */
    public java.lang.String getCod_cliente() {
        return cod_cliente;
    }
    
    /**
     * Setter for property cod_cliente.
     * @param cod_cliente New value of property cod_cliente.
     */
    public void setCod_cliente(java.lang.String cod_cliente) {
        this.cod_cliente = cod_cliente;
    }
    
    /**
     * Getter for property anticipo.
     * @return Value of property anticipo.
     */
    public double getAnticipo() {
        return anticipo;
    }
    
    /**
     * Setter for property anticipo.
     * @param anticipo New value of property anticipo.
     */
    public void setAnticipo(double anticipo) {
        this.anticipo = anticipo;
    }
    
    /**
     * Getter for property Fecha_anticipo.
     * @return Value of property Fecha_anticipo.
     */
    public java.lang.String getFecha_anticipo() {
        return Fecha_anticipo;
    }
    
    /**
     * Setter for property Fecha_anticipo.
     * @param Fecha_anticipo New value of property Fecha_anticipo.
     */
    public void setFecha_anticipo(java.lang.String Fecha_anticipo) {
        this.Fecha_anticipo = Fecha_anticipo;
    }
    
    /**
     * Getter for property valor_oc.
     * @return Value of property valor_oc.
     */
    public double getValor_oc() {
        return valor_oc;
    }
    
    /**
     * Setter for property valor_oc.
     * @param valor_oc New value of property valor_oc.
     */
    public void setValor_oc(double valor_oc) {
        this.valor_oc = valor_oc;
    }
    
    /**
     * Getter for property vacio.
     * @return Value of property vacio.
     */
    public java.lang.String getVacio() {
        return vacio;
    }
    
    /**
     * Setter for property vacio.
     * @param vacio New value of property vacio.
     */
    public void setVacio(java.lang.String vacio) {
        this.vacio = vacio;
    }
    
    /**
     * Getter for property dias.
     * @return Value of property dias.
     */
    public int getDias() {
        return dias;
    }
    
    /**
     * Setter for property dias.
     * @param dias New value of property dias.
     */
    public void setDias(int dias) {
        this.dias = dias;
    }
    
    /**
     * Getter for property calificacion.
     * @return Value of property calificacion.
     */
    public double getCalificacion() {
        return calificacion;
    }    
   
    /**
     * Setter for property calificacion.
     * @param calificacion New value of property calificacion.
     */
    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }    
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }    
   
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }    
    
    /**
     * Getter for property agencia_dest.
     * @return Value of property agencia_dest.
     */
    public java.lang.String getAgencia_dest() {
        return agencia_dest;
    }
    
    /**
     * Setter for property agencia_dest.
     * @param agencia_dest New value of property agencia_dest.
     */
    public void setAgencia_dest(java.lang.String agencia_dest) {
        this.agencia_dest = agencia_dest;
    }
    
    /**
     * Getter for property asociada.
     * @return Value of property asociada.
     */
    public java.lang.String getAsociada() {
        return asociada;
    }
    
    /**
     * Setter for property asociada.
     * @param asociada New value of property asociada.
     */
    public void setAsociada(java.lang.String asociada) {
        this.asociada = asociada;
    }
    
    /**
     * Getter for property no_factura.
     * @return Value of property no_factura.
     */
    public java.lang.String getNo_factura() {
        return no_factura;
    }
    
    /**
     * Setter for property no_factura.
     * @param no_factura New value of property no_factura.
     */
    public void setNo_factura(java.lang.String no_factura) {
        this.no_factura = no_factura;
    }
    
    /**
     * Getter for property valor_fact.
     * @return Value of property valor_fact.
     */
    public double getValor_fact() {
        return valor_fact;
    }
    
    /**
     * Setter for property valor_fact.
     * @param valor_fact New value of property valor_fact.
     */
    public void setValor_fact(double valor_fact) {
        this.valor_fact = valor_fact;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.lang.String getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(java.lang.String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property ruta_pla.
     * @return Value of property ruta_pla.
     */
    public java.lang.String getRuta_pla() {
        return ruta_pla;
    }
    
    /**
     * Setter for property ruta_pla.
     * @param ruta_pla New value of property ruta_pla.
     */
    public void setRuta_pla(java.lang.String ruta_pla) {
        this.ruta_pla = ruta_pla;
    }
    
    /**
     * Getter for property contaroc.
     * @return Value of property contaroc.
     */
    public int getContaroc() {
        return contaroc;
    }
    
    /**
     * Setter for property contaroc.
     * @param contaroc New value of property contaroc.
     */
    public void setContaroc(int contaroc) {
        this.contaroc = contaroc;
    }
    
    /**
     * Getter for property contarDia.
     * @return Value of property contarDia.
     */
    public java.lang.String getContarDia() {
        return contarDia;
    }    
    
    /**
     * Setter for property contarDia.
     * @param contarDia New value of property contarDia.
     */
    public void setContarDia(java.lang.String contarDia) {
        this.contarDia = contarDia;
    }    
    
    /**
     * Getter for property pais.
     * @return Value of property pais.
     */
    public java.lang.String getPais() {
        return pais;
    }    
    
    /**
     * Setter for property pais.
     * @param pais New value of property pais.
     */
    public void setPais(java.lang.String pais) {
        this.pais = pais;
    }
    
    /**
     * Getter for property fechareporte.
     * @return Value of property fechareporte.
     */
    public java.lang.String getFechareporte() {
        return fechareporte;
    }
    
    /**
     * Setter for property fechareporte.
     * @param fechareporte New value of property fechareporte.
     */
    public void setFechareporte(java.lang.String fechareporte) {
        this.fechareporte = fechareporte;
    }
    
    /**
     * Getter for property diasDeGen.
     * @return Value of property diasDeGen.
     */
    public int getDiasDeGen() {
        return diasDeGen;
    }
    
    /**
     * Setter for property diasDeGen.
     * @param diasDeGen New value of property diasDeGen.
     */
    public void setDiasDeGen(int diasDeGen) {
        this.diasDeGen = diasDeGen;
    }
    
    /**
     * Getter for property codOrig.
     * @return Value of property codOrig.
     */
    public java.lang.String getCodOrig() {
        return codOrig;
    }    
 
    /**
     * Setter for property codOrig.
     * @param codOrig New value of property codOrig.
     */
    public void setCodOrig(java.lang.String codOrig) {
        this.codOrig = codOrig;
    }    
  
    /**
     * Getter for property codDest.
     * @return Value of property codDest.
     */
    public java.lang.String getCodDest() {
        return codDest;
    }    
    
    /**
     * Setter for property codDest.
     * @param codDest New value of property codDest.
     */
    public void setCodDest(java.lang.String codDest) {
        this.codDest = codDest;
    }
    
}
