
package com.tsp.operation.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;


public class Nit {
    
    private String estado;
    private String nit;
    private String idMims;
    private String name;
    private String address;
    private String ag_code;
    private String dpt_code;
    private String country_code;
    private String phone;
    private String cellular;
    private String e_mail;
    private String sex;
    private String fechaNacido;
    private String pnombre;
    private String snombre;
    private String papellido;
    private String sapellido;
    private String base;
    private String dstrct;
    private String user;
    
    public Nit() {
    }
    
    
    //SET
    
    public void setEstado(String state){
       this.estado=state; 
    }
    
    public void setNit(String nit){
        this.nit = nit;
    }
    
    public void setIdMims(String id){
        this.idMims = id;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setAddress(String address){
        this.address = address;
    }
    
    public void setAgCode(String agency){
        this.ag_code = agency;
    }
    
    public void setDptCode(String code){
        this.dpt_code = code;
    }
    
    public void setCountryCode(String country){
        this.country_code=country;
    }
    
    public void setPhone(String phone){
        this.phone = phone;
    }
    
    public void setCellular(String cellular){
        this.cellular = cellular;
    }
    
    public void setEMail(String mail){
        this.e_mail = mail;
    }
    
    public void setSex(String sexo){
        this.sex = sexo;
    }

    public void setFechaNacido(String fecha){
      this.fechaNacido=fecha;
    }
    
    
    //GET
    
     //--- nit
    
    public String getEstado(){
       return this.estado; 
    }
    
    
    public String getNit(){
        return this.nit;
    }
    
    public String getIdMims(){
        return this.idMims;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public String getAgCode(){
        return this.ag_code;
    }
    
    public String getDptCode(){
        return this.dpt_code;
    }
    
    public String getCountryCode(){
        return this.country_code;
    }
    
    public String getPhone(){
        return this.phone;
    }
    
    public String getCellular(){
        return this.cellular;
    }
    
    public String getEMail(){
        return this.e_mail;
    }
        
    public String getSex(){
       return this.sex;
    }
    
    public String getFechaNacido(){
        return this.fechaNacido;
    }
     
    
    
  //--- formación de objetos
    public static Nit getObject(HttpServletRequest request){
        Nit objeto = new Nit(); 
        String estado  = request.getParameter("estadoNit");
        String nit     = request.getParameter("identificacion");
        String idMims  = request.getParameter("identificacion");
        String nombre  = request.getParameter("nombre");
        String direccion    = request.getParameter("direccion");
        String agencia      = request.getParameter("ciudad");
        String departamento = request.getParameter("departamento");
        String pais      = request.getParameter("pais");
        String telefono  = request.getParameter("telefono");
        String celular   = request.getParameter("celular");
        String mail      = request.getParameter("email");        
        String sex            = request.getParameter("sexo");
        String fechaNacido    = request.getParameter("fechaNacimiento");
        
       //--- creamos el objeto 
        objeto.setEstado(estado);
        objeto.setNit(nit);
        objeto.setIdMims(idMims);
        objeto.setName(nombre);
        objeto.setAddress(direccion);
        objeto.setAgCode(agencia);
        objeto.setDptCode(departamento);
        objeto.setCountryCode(pais);
        objeto.setPhone(telefono);
        objeto.setCellular(celular);
        objeto.setEMail(mail);
        objeto.setSex(sex);
        objeto.setFechaNacido(Util.formatoFechaPostgres(fechaNacido) );
        
       return objeto;
    }
    
    /**
     * Getter for property papellido.
     * @return Value of property papellido.
     */
    public java.lang.String getPapellido() {
        return papellido;
    }
    
    /**
     * Setter for property papellido.
     * @param papellido New value of property papellido.
     */
    public void setPapellido(java.lang.String papellido) {
        this.papellido = papellido;
    }
    
    /**
     * Getter for property pnombre.
     * @return Value of property pnombre.
     */
    public java.lang.String getPnombre() {
        return pnombre;
    }
    
    /**
     * Setter for property pnombre.
     * @param pnombre New value of property pnombre.
     */
    public void setPnombre(java.lang.String pnombre) {
        this.pnombre = pnombre;
    }
    
    /**
     * Getter for property sapellido.
     * @return Value of property sapellido.
     */
    public java.lang.String getSapellido() {
        return sapellido;
    }
    
    /**
     * Setter for property sapellido.
     * @param sapellido New value of property sapellido.
     */
    public void setSapellido(java.lang.String sapellido) {
        this.sapellido = sapellido;
    }
    
    /**
     * Getter for property snombre.
     * @return Value of property snombre.
     */
    public java.lang.String getSnombre() {
        return snombre;
    }
    
    /**
     * Setter for property snombre.
     * @param snombre New value of property snombre.
     */
    public void setSnombre(java.lang.String snombre) {
        this.snombre = snombre;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property user.
     * @return Value of property user.
     */
    public java.lang.String getUser() {
        return user;
    }
    
    /**
     * Setter for property user.
     * @param user New value of property user.
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }
    
}
