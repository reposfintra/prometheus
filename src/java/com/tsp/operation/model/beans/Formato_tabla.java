/***********************************************
 * Nombre: Formato_tabla.java
 * Descripci�n: Beans de formato de tablas.
 * Autor: Ing. Jose de la rosa
 * Fecha: 25 de noviembre de 2006, 10:06 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ***********************************************/
package com.tsp.operation.model.beans;

/**
 *
 * @author  EQUIPO13
 */
public class Formato_tabla implements Cloneable {
    
    private String formato;
    private String nombre_formato;
    private String tabla;
    private String campo_tabla;
    private String campo_jsp;
    private String titulo;
    private String tipo_campo;
    private String validar;
    private String tabla_val;
    private String campo_val;
    private String primaria;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String base;
    private String distrito;
    private String rec_status;
    private int orden;
    private double longitud;
    private String valor_campo;
    private String tipo_campo_equivalente;
    
    /** Creates a new instance of Formato_tabla */
    public Formato_tabla () {
    }
    
    /**
     * Getter for property campo_jsp.
     * @return Value of property campo_jsp.
     */
    public java.lang.String getCampo_jsp () {
        return campo_jsp;
    }
    
    /**
     * Setter for property campo_jsp.
     * @param campo_jsp New value of property campo_jsp.
     */
    public void setCampo_jsp (java.lang.String campo_jsp) {
        this.campo_jsp = campo_jsp;
    }
    
    /**
     * Getter for property campo_tabla.
     * @return Value of property campo_tabla.
     */
    public java.lang.String getCampo_tabla () {
        return campo_tabla;
    }
    
    /**
     * Setter for property campo_tabla.
     * @param campo_tabla New value of property campo_tabla.
     */
    public void setCampo_tabla (java.lang.String campo_tabla) {
        this.campo_tabla = campo_tabla;
    }
    
    /**
     * Getter for property campo_val.
     * @return Value of property campo_val.
     */
    public java.lang.String getCampo_val () {
        return campo_val;
    }
    
    /**
     * Setter for property campo_val.
     * @param campo_val New value of property campo_val.
     */
    public void setCampo_val (java.lang.String campo_val) {
        this.campo_val = campo_val;
    }
    
    /**
     * Getter for property formato.
     * @return Value of property formato.
     */
    public java.lang.String getFormato () {
        return formato;
    }
    
    /**
     * Setter for property formato.
     * @param formato New value of property formato.
     */
    public void setFormato (java.lang.String formato) {
        this.formato = formato;
    }
    
    /**
     * Getter for property primaria.
     * @return Value of property primaria.
     */
    public String getPrimaria () {
        return primaria;
    }
    
    /**
     * Setter for property primaria.
     * @param primaria New value of property primaria.
     */
    public void setPrimaria (String primaria) {
        this.primaria = primaria;
    }
    
    /**
     * Getter for property tabla.
     * @return Value of property tabla.
     */
    public java.lang.String getTabla () {
        return tabla;
    }
    
    /**
     * Setter for property tabla.
     * @param tabla New value of property tabla.
     */
    public void setTabla (java.lang.String tabla) {
        this.tabla = tabla;
    }
    
    /**
     * Getter for property tabla_val.
     * @return Value of property tabla_val.
     */
    public java.lang.String getTabla_val () {
        return tabla_val;
    }
    
    /**
     * Setter for property tabla_val.
     * @param tabla_val New value of property tabla_val.
     */
    public void setTabla_val (java.lang.String tabla_val) {
        this.tabla_val = tabla_val;
    }
    
    /**
     * Getter for property tipo_campo.
     * @return Value of property tipo_campo.
     */
    public java.lang.String getTipo_campo () {
        return tipo_campo;
    }
    
    /**
     * Setter for property tipo_campo.
     * @param tipo_campo New value of property tipo_campo.
     */
    public void setTipo_campo (java.lang.String tipo_campo) {
        this.tipo_campo = tipo_campo;
    }
    
    /**
     * Getter for property titulo.
     * @return Value of property titulo.
     */
    public java.lang.String getTitulo () {
        return titulo;
    }
    
    /**
     * Setter for property titulo.
     * @param titulo New value of property titulo.
     */
    public void setTitulo (java.lang.String titulo) {
        this.titulo = titulo;
    }
    
    /**
     * Getter for property validar.
     * @return Value of property validar.
     */
    public String getValidar () {
        return validar;
    }
    
    /**
     * Setter for property validar.
     * @param validar New value of property validar.
     */
    public void setValidar (String validar) {
        this.validar = validar;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion () {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion (java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion () {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion (java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion () {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property ultima_modificacion.
     * @return Value of property ultima_modificacion.
     */
    public java.lang.String getUltima_modificacion () {
        return ultima_modificacion;
    }
    
    /**
     * Setter for property ultima_modificacion.
     * @param ultima_modificacion New value of property ultima_modificacion.
     */
    public void setUltima_modificacion (java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status () {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status (java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property nombre_formato.
     * @return Value of property nombre_formato.
     */
    public java.lang.String getNombre_formato () {
        return nombre_formato;
    }
    
    /**
     * Setter for property nombre_formato.
     * @param nombre_formato New value of property nombre_formato.
     */
    public void setNombre_formato (java.lang.String nombre_formato) {
        this.nombre_formato = nombre_formato;
    }
    
    /**
     * Getter for property orden.
     * @return Value of property orden.
     */
    public int getOrden () {
        return orden;
    }
    
    /**
     * Setter for property orden.
     * @param orden New value of property orden.
     */
    public void setOrden (int orden) {
        this.orden = orden;
    }
    
    /**
     * Getter for property longitud.
     * @return Value of property longitud.
     */
    public double getLongitud () {
        return longitud;
    }
    
    /**
     * Setter for property longitud.
     * @param longitud New value of property longitud.
     */
    public void setLongitud (double longitud) {
        this.longitud = longitud;
    }
    
    /**
     * Getter for property valor_campo.
     * @return Value of property valor_campo.
     */
    public java.lang.String getValor_campo () {
        return valor_campo;
    }
    
    /**
     * Setter for property valor_campo.
     * @param valor_campo New value of property valor_campo.
     */
    public void setValor_campo (java.lang.String valor_campo) {
        this.valor_campo = valor_campo;
    }
    
    /**
     * Getter for property tipo_campo_equivalente.
     * @return Value of property tipo_campo_equivalente.
     */
    public java.lang.String getTipo_campo_equivalente () {
        return tipo_campo_equivalente;
    }
    
    /**
     * Setter for property tipo_campo_equivalente.
     * @param tipo_campo_equivalente New value of property tipo_campo_equivalente.
     */
    public void setTipo_campo_equivalente (java.lang.String tipo_campo_equivalente) {
        this.tipo_campo_equivalente = tipo_campo_equivalente;
    }
    
    public Object clone() {
        try{
            return super.clone();
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }    
    
}
