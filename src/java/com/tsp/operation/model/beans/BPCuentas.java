/*
 *  Nombre clase    :  BalancePrueba.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 20 de junio de 2006, 11:43 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;

import java.util.*;
import java.sql.*;

public class BPCuentas {
    
    private String Elemento;
    private String DescripcionElemento = "";
    
    private String Unidad;
    private String DescripcionUnidad = "";
    
    private String Codigo;
    private String Descripcion;
    
    /*Totales Cuentas*/
    private double SaldoAnterior;
    private double MovDebito;
    private double MovCredito;
    private double SaldoActual;
    
    private double auxSaldoAnterior;
    private double auxSaldoActual;
    
    /*Datos*/
    private double [] VMovDebito    = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    private double [] VMovCredito   = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    
    
    private String ano;
    private String mes;
    
    /** Crea una nueva instancia de  BalancePrueba */
    public BPCuentas() {
    }
    
    
    
    
    public static BPCuentas Load( ResultSet rs, int tipo ) throws Exception{
        BPCuentas datos = new BPCuentas();
        datos.setAno(  rs.getString("anio")  );
        datos.setCodigo(rs.getString("cuenta"));
        datos.setDescripcion(rs.getString("nombre_largo"));
        datos.setAuxSaldoActual(rs.getDouble("saldoact"));
        datos.setAuxSaldoAnterior(rs.getDouble("saldoant"));        
        for( int i = 1 ; i < 13; i++){
            String indice = ( i<10 )?"0"+i:String.valueOf(i);
            datos.setVMovCredito(i, rs.getDouble("movcre"+indice));
            datos.setVMovDebito(i, rs.getDouble("movdeb"+indice));
        }
        if( tipo == 1 ){
            String elemento = (rs.getString("elemento")!=null)?rs.getString("elemento"):"";
            datos.setDescripcionElemento(elemento);
            String unidad  = (rs.getString("unidad")!=null)?rs.getString("unidad"):"";
            datos.setDescripcionUnidad(unidad);        
        }
        else{
            datos.setDescripcionElemento(" ");
            datos.setDescripcionUnidad(" ");   
        }
            
        return datos;
        
    }
    
    public void setVMovDebito(int mes, double newValue){
        VMovDebito [mes - 1] = newValue;
    }
    
    
    public void setVMovCredito(int mes, double newValue){
        VMovCredito [mes - 1] = newValue;
    }
    
    public double getVMovDebito(int mes){
        return VMovDebito[mes - 1];
    }
    
    public double getVMovCredito(int mes){
        return VMovCredito[mes - 1];
    }
    
    /**
     * Getter for property Codigo.
     * @return Value of property Codigo.
     */
    public java.lang.String getCodigo() {
        return Codigo;
    }
    
    /**
     * Setter for property Codigo.
     * @param Codigo New value of property Codigo.
     */
    public void setCodigo(java.lang.String Codigo) {
        this.Codigo = Codigo;
    }
    
    /**
     * Getter for property Descripcion.
     * @return Value of property Descripcion.
     */
    public java.lang.String getDescripcion() {
        return Descripcion;
    }
    
    /**
     * Setter for property Descripcion.
     * @param Descripcion New value of property Descripcion.
     */
    public void setDescripcion(java.lang.String Descripcion) {
        this.Descripcion = Descripcion;
    }
    
    /**
     * Getter for property MovCredito.
     * @return Value of property MovCredito.
     */
    public double getMovCredito() {
        return MovCredito;
    }
    
    /**
     * Setter for property MovCredito.
     * @param MovCredito New value of property MovCredito.
     */
    public void setMovCredito(double MovCredito) {
        this.MovCredito = MovCredito;
    }
    
    /**
     * Getter for property MovDebito.
     * @return Value of property MovDebito.
     */
    public double getMovDebito() {
        return MovDebito;
    }
    
    /**
     * Setter for property MovDebito.
     * @param MovDebito New value of property MovDebito.
     */
    public void setMovDebito(double MovDebito) {
        this.MovDebito = MovDebito;
    }
    
    /**
     * Getter for property SaldoActual.
     * @return Value of property SaldoActual.
     */
    public double getSaldoActual() {
        return SaldoActual;
    }
    
    /**
     * Setter for property SaldoActual.
     * @param SaldoActual New value of property SaldoActual.
     */
    public void setSaldoActual(double SaldoActual) {
        this.SaldoActual = SaldoActual;
    }
    
    /**
     * Getter for property SaldoAnterior.
     * @return Value of property SaldoAnterior.
     */
    public double getSaldoAnterior() {
        return SaldoAnterior;
    }
    
    /**
     * Setter for property SaldoAnterior.
     * @param SaldoAnterior New value of property SaldoAnterior.
     */
    public void setSaldoAnterior(double SaldoAnterior) {
        this.SaldoAnterior = SaldoAnterior;
    }
    
    /**
     * Getter for property ano.
     * @return Value of property ano.
     */
    public java.lang.String getAno() {
        return ano;
    }
    
    /**
     * Setter for property ano.
     * @param ano New value of property ano.
     */
    public void setAno(java.lang.String ano) {
        this.ano = ano;
    }
    
    
     public void CalcularSaldos( int mes ){     
        SaldoAnterior   = 0.0;
        SaldoActual     = 0.0;
        
        //Saldo Anterior
        for (int i=0 ;i < mes-1 ; i++){
            SaldoAnterior += (VMovDebito[i] - VMovCredito[i]);                                   
        }
        SaldoAnterior += auxSaldoAnterior;
        
        //Saldo Actual
        for (int i = 0; i< mes ; i++ ){
            SaldoActual += (VMovDebito[i] - VMovCredito[i]);
        }
        
        SaldoActual += auxSaldoAnterior;
        
        MovDebito   = VMovDebito[mes - 1];
        MovCredito  = VMovCredito[mes - 1];
    }
    
     /**
      * Getter for property Elemento.
      * @return Value of property Elemento.
      */
     public java.lang.String getElemento() {
         return Elemento;
     }
     
     /**
      * Setter for property Elemento.
      * @param Elemento New value of property Elemento.
      */
     public void setElemento(java.lang.String Elemento) {
         this.Elemento = Elemento;
     }
     
     /**
      * Getter for property DescripcionElemento.
      * @return Value of property DescripcionElemento.
      */
     public java.lang.String getDescripcionElemento() {
         return DescripcionElemento;
     }
     
     /**
      * Setter for property DescripcionElemento.
      * @param DescripcionElemento New value of property DescripcionElemento.
      */
     public void setDescripcionElemento(java.lang.String DescripcionElemento) {
         this.DescripcionElemento = DescripcionElemento;
     }
     
     /**
      * Getter for property Unidad.
      * @return Value of property Unidad.
      */
     public java.lang.String getUnidad() {
         return Unidad;
     }
     
     /**
      * Setter for property Unidad.
      * @param Unidad New value of property Unidad.
      */
     public void setUnidad(java.lang.String Unidad) {
         this.Unidad = Unidad;
     }
     
     /**
      * Getter for property DescripcionUnidad.
      * @return Value of property DescripcionUnidad.
      */
     public java.lang.String getDescripcionUnidad() {
         return DescripcionUnidad;
     }
     
     /**
      * Setter for property DescripcionUnidad.
      * @param DescripcionUnidad New value of property DescripcionUnidad.
      */
     public void setDescripcionUnidad(java.lang.String DescripcionUnidad) {
         this.DescripcionUnidad = DescripcionUnidad;
     }
     
     /**
      * Getter for property mes.
      * @return Value of property mes.
      */
     public java.lang.String getMes() {
         return mes;
     }
     
     /**
      * Setter for property mes.
      * @param mes New value of property mes.
      */
     public void setMes(java.lang.String mes) {
         this.mes = mes;
     }
     
     /**
      * Getter for property auxSaldoAnterior.
      * @return Value of property auxSaldoAnterior.
      */
     public double getAuxSaldoAnterior() {
         return auxSaldoAnterior;
     }
     
     /**
      * Setter for property auxSaldoAnterior.
      * @param auxSaldoAnterior New value of property auxSaldoAnterior.
      */
     public void setAuxSaldoAnterior(double auxSaldoAnterior) {
         this.auxSaldoAnterior = auxSaldoAnterior;
     }
     
     /**
      * Getter for property auxSaldoActual.
      * @return Value of property auxSaldoActual.
      */
     public double getAuxSaldoActual() {
         return auxSaldoActual;
     }
     
     /**
      * Setter for property auxSaldoActual.
      * @param auxSaldoActual New value of property auxSaldoActual.
      */
     public void setAuxSaldoActual(double auxSaldoActual) {
         this.auxSaldoActual = auxSaldoActual;
     }
     
}
