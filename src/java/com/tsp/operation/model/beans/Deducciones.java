/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author maltamiranda
 */
public class Deducciones implements Serializable{


    private String id, reg_status, dstrct, descripcion, modalidad,id_convenio, unid_negocio,creation_date, creation_user, last_update, user_update;
    private double desembolso_inicial,desembolso_final, valor_cobrar, perc_cobrar;
    private Deducciones Deducciones;

    public String getid() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getreg_status() {
        return reg_status;
    }

    public void setreg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getdstrct() {
        return dstrct;
    }

    public void setdstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    
    public String descripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }  
    
    public String getmodalidad() {
        return modalidad;
    }
    public void setmodalidad(String modalidad) {
        this.modalidad = modalidad;
    }  
    
    public String getid_convenio() {
        return id_convenio;
    }
    public void setid_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    } 
    public String getunid_negocio() {
        return unid_negocio;
    }
    public void setunid_negocio(String unid_negocio) {
        this.unid_negocio = unid_negocio;
    }
    public String getcreation_date() {
        return creation_date;
    }
    public void setcreation_date(String creation_date) {
        this.creation_date = creation_date;
    }
    public String getcreation_usero() {
        return creation_user;
    }
    public void setcreation_user(String creation_user) {
        this.creation_user = creation_user;
    }
    public String getlast_updateo() {
        return last_update;
    }
    public void setlast_update(String last_update) {
        this.last_update = last_update;
    }
    public String getuser_update() {
        return user_update;
    }
    public void setuser_update(String user_update) {
        this.user_update = user_update;
    }
    
     public Double getdesembolso_inicial() {
        return desembolso_inicial;
    }
    public void setdesembolso_inicial(Double desembolso_inicial) {
        this.desembolso_inicial = desembolso_inicial;
    }
     public Double getdesembolso_final() {
        return desembolso_final;
    }
    public void setdesembolso_final(Double desembolso_final) {
        this.desembolso_final = desembolso_final;
    }
     public Double getvalor_cobrar() {
        return valor_cobrar;
    }
    public void setvalor_cobrar(Double valor_cobrar) {
        this.valor_cobrar = valor_cobrar;
    }
     public Double getperc_cobrar() {
        return perc_cobrar;
    }
    public void setperc_cobrar(Double perc_cobrar) {
        this.perc_cobrar = perc_cobrar;
    }


    public Deducciones() {
        this.id="";
        this.reg_status="";
        this.dstrct="";
        this.descripcion="";
        this.modalidad="";
        this.id_convenio="";
        this.unid_negocio="";
        this.creation_date="";
        this.creation_user="";
        this.last_update="";
        this.user_update="";
        this.desembolso_inicial=0;
        this.desembolso_final=0;
        this.valor_cobrar=0;
        this.perc_cobrar=0;
    }

   

    

    public Deducciones load(ResultSet rs) throws SQLException {
        Deducciones.setid(rs.getString("id"));
        Deducciones.setreg_status(rs.getString("reg_status"));
        Deducciones.setdstrct(rs.getString("dstrct"));
        Deducciones.setdescripcion(rs.getString("descripcion"));
        Deducciones.setmodalidad(rs.getString("modalidad"));
        Deducciones.setunid_negocio(rs.getString("unid_negocio"));
        Deducciones.setcreation_date(rs.getString("creation_date"));
        Deducciones.setcreation_user(rs.getString("creation_user"));
        Deducciones.setlast_update(rs.getString("last_update"));
        Deducciones.setuser_update(rs.getString("user_update"));
        Deducciones.setdesembolso_inicial(rs.getDouble("desembolso_inicial"));
        Deducciones.setdesembolso_final(rs.getDouble("desembolso_final"));
        Deducciones.setvalor_cobrar(rs.getDouble("valor_cobrar"));
        Deducciones.setperc_cobrar(rs.getDouble("perc_cobrar"));
        
        

        return Deducciones;
    }

 

}
