/********************************************************************
 *      Nombre Clase.................   Turnos.java
 *      Descripci�n..................   Bean de la tabla turnos
 *      Autor........................   David Lamadrid
 *      Fecha........................   28.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.model.beans;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  dlamadrid
 */
public class Turnos {
    
    private String reg_status;
    private String dstrct;
    private String usuario_turno ;
    private String fecha_turno;
    private String h_entrada;
    private String h_salida;
    private String zona;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String base;
    private String nombreUsuario;
    private String nombreZona;
    private Vector nombresZonas;
    /** Creates a new instance of Turnos */
    public Turnos () {
    }
    
     public static Turnos load(ResultSet rs,Vector v)throws SQLException{
        Turnos turno = new Turnos();
        turno.setUsuario_turno (rs.getString ("usuario_turno"));
        turno.setFecha_turno (rs.getString ("fecha_turno"));
        turno.setH_entrada (rs.getString ("h_entrada"));
        turno.setH_salida (rs.getString ("h_salida"));
        turno.setZona (rs.getString ("zona"));
        turno.setNombreUsuario (rs.getString ("nombre"));
        turno.setNombresZonas (v);
        //turno.setNombreZona (rs.getString ("desczona"));
        return turno;
    }
   
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status () {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status (java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct () {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct (java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property usuario_turno.
     * @return Value of property usuario_turno.
     */
    public java.lang.String getUsuario_turno () {
        return usuario_turno;
    }
    
    /**
     * Setter for property usuario_turno.
     * @param usuario_turno New value of property usuario_turno.
     */
    public void setUsuario_turno (java.lang.String usuario_turno) {
        this.usuario_turno = usuario_turno;
    }
    
    /**
     * Getter for property fecha_turno.
     * @return Value of property fecha_turno.
     */
    public java.lang.String getFecha_turno () {
        return fecha_turno;
    }
    
    /**
     * Setter for property fecha_turno.
     * @param fecha_turno New value of property fecha_turno.
     */
    public void setFecha_turno (java.lang.String fecha_turno) {
        this.fecha_turno = fecha_turno;
    }
    
    /**
     * Getter for property h_entrada.
     * @return Value of property h_entrada.
     */
    public java.lang.String getH_entrada () {
        return h_entrada;
    }
    
    /**
     * Setter for property h_entrada.
     * @param h_entrada New value of property h_entrada.
     */
    public void setH_entrada (java.lang.String h_entrada) {
        this.h_entrada = h_entrada;
    }
    
    /**
     * Getter for property h_salida.
     * @return Value of property h_salida.
     */
    public java.lang.String getH_salida () {
        return h_salida;
    }
    
    /**
     * Setter for property h_salida.
     * @param h_salida New value of property h_salida.
     */
    public void setH_salida (java.lang.String h_salida) {
        this.h_salida = h_salida;
    }
    
    /**
     * Getter for property zona.
     * @return Value of property zona.
     */
    public java.lang.String getZona () {
        return zona;
    }
    
    /**
     * Setter for property zona.
     * @param zona New value of property zona.
     */
    public void setZona (java.lang.String zona) {
        this.zona = zona;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update () {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update (java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update () {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date () {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user () {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property nombreUsuario.
     * @return Value of property nombreUsuario.
     */
    public java.lang.String getNombreUsuario () {
        return nombreUsuario;
    }
    
    /**
     * Setter for property nombreUsuario.
     * @param nombreUsuario New value of property nombreUsuario.
     */
    public void setNombreUsuario (java.lang.String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    
    /**
     * Getter for property nombreZona.
     * @return Value of property nombreZona.
     */
    public java.lang.String getNombreZona () {
        return nombreZona;
    }
    
    /**
     * Setter for property nombreZona.
     * @param nombreZona New value of property nombreZona.
     */
    public void setNombreZona (java.lang.String nombreZona) {
        this.nombreZona = nombreZona;
    }
    
    /**
     * Getter for property nombresZonas.
     * @return Value of property nombresZonas.
     */
    public java.util.Vector getNombresZonas () {
        return nombresZonas;
    }
    
    /**
     * Setter for property nombresZonas.
     * @param nombresZonas New value of property nombresZonas.
     */
    public void setNombresZonas (java.util.Vector nombresZonas) {
        this.nombresZonas = nombresZonas;
    }
    
}
