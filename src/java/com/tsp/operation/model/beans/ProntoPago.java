/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class ProntoPago {

    private String tipo_operacion;
    private String numero_operacion;
    private String numero_transferencia;
    private String nit;
    private String nombre_propietario;
    private String tcta_transferencia;
    private String banco_transferencia;
    private String cuenta_transferencia;


    private double vlr_extracto;
    private double porcentaje;
    private double vlr_descuento;
    private double vlr_neto;
    private double vlr_combancaria;
    private double vlr_consignacion;
    private double vlr_consignacion_calculada;
    private double vlr_ajuste;
    private String fecha_transferencia;
    
    private double vlr_ext_detalle_calculado;
    private double vlr_ext_detalle_registrado;
    private double vlr_ext_detalle_diferencia;
    private double vlr_diferencia_ext_ant;
    private String cuenta_contable_banco;


    private String cuenta_os;
    private String dbcr_os;
    private String sigla_comprobante_os;
    private String cmc;
    private String periodo_os;
    private int grupo_transaccion;
    private String banco;
    private String sucursal;
    private String factura_cxp;


    private String agency_id;
    private String planilla;
    private String supplier;
    private String tipo_cuenta;
    private String cedcon;
    private String concept_code;
    private int id;
    private String cuenta_anticipo;

    private String creation_date;
    private String con_ant_tercero;

    private String transferido;



    
    

    public static ProntoPago load(java.sql.ResultSet rs)throws java.sql.SQLException{

        ProntoPago prontoPago = new ProntoPago();

        prontoPago.setNumero_operacion( rs.getString("numero_operacion") );
        prontoPago.setNumero_transferencia(rs.getString("numero_transferencia") );
        prontoPago.setNit(rs.getString("nit") );
        prontoPago.setNombre_propietario( rs.getString("nombre_propietario") );
        prontoPago.setTcta_transferencia( rs.getString("tcta_transferencia") );
        prontoPago.setVlr_extracto( rs.getDouble("vlr_extracto") );
        prontoPago.setPorcentaje( rs.getDouble("porcentaje") );
        prontoPago.setVlr_descuento( rs.getDouble("vlr_descuento") );
        prontoPago.setVlr_neto( rs.getDouble("vlr_neto") );
        prontoPago.setVlr_combancaria( rs.getDouble("vlr_combancaria") );
        prontoPago.setVlr_consignacion( rs.getDouble("vlr_consignacion") );
        prontoPago.setVlr_consignacion_calculada( rs.getDouble("vlr_consignacion_calculada") );
        prontoPago.setVlr_ajuste( rs.getDouble("vlr_ajuste") );
        prontoPago.setFecha_transferencia( rs.getString("fecha_transferencia") );
        prontoPago.setBanco_transferencia( rs.getString("banco_transferencia") );
        prontoPago.setCuenta_transferencia( rs.getString("cuenta_transferencia") );
        
        prontoPago.setAgency_id(rs.getString("agency_id"));
        prontoPago.setPlanilla(rs.getString("planilla"));
        prontoPago.setSupplier(rs.getString("supplier"));
        prontoPago.setTipo_cuenta(rs.getString("tipo_cuenta"));
        prontoPago.setCedcon(rs.getString("cedcon"));
        prontoPago.setConcept_code(rs.getString("concept_code"));
        prontoPago.setId(rs.getInt("id"));
        prontoPago.setCreation_date(rs.getString("creation_date"));
        prontoPago.setCon_ant_tercero(rs.getString("con_ant_tercero")); 
        prontoPago.setTransferido(rs.getString("transferido"));

        // Inicializadas en blanco

        prontoPago.setTipo_operacion( "" );
        prontoPago.setVlr_ext_detalle_calculado( 0.00 );
        prontoPago.setVlr_ext_detalle_registrado( 0.00 );
        prontoPago.setVlr_ext_detalle_diferencia( 0.00 );
        prontoPago.setVlr_diferencia_ext_ant( 0.00 );
        prontoPago.setCuenta_contable_banco("");
        prontoPago.setCuenta_os("");
        prontoPago.setDbcr_os("");
        prontoPago.setSigla_comprobante_os("");
        prontoPago.setCmc("");
        prontoPago.setPeriodo_os("");
        prontoPago.setGrupo_transaccion(0);
        prontoPago.setBanco("");
        prontoPago.setSucursal("");
        prontoPago.setFactura_cxp("");
        prontoPago.setCuenta_anticipo("");

        return prontoPago;
    }




    /**
     * @return the numero_operacion
     */
    public String getNumero_operacion() {
        return numero_operacion;
    }

    /**
     * @param numero_operacion the numero_operacion to set
     */
    public void setNumero_operacion(String numero_operacion) {
        this.numero_operacion = numero_operacion;
    }

    /**
     * @return the numero_transferencia
     */
    public String getNumero_transferencia() {
        return numero_transferencia;
    }

    /**
     * @param numero_transferencia the numero_transferencia to set
     */
    public void setNumero_transferencia(String numero_transferencia) {
        this.numero_transferencia = numero_transferencia;
    }

    /**
     * @return the nombre_propietario
     */
    public String getNombre_propietario() {
        return nombre_propietario;
    }

    /**
     * @param nombre_propietario the nombre_propietario to set
     */
    public void setNombre_propietario(String nombre_propietario) {
        this.nombre_propietario = nombre_propietario;
    }



    /**
     * @return the sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the vlr_extracto
     */
    public double getVlr_extracto() {
        return vlr_extracto;
    }

    /**
     * @param vlr_extracto the vlr_extracto to set
     */
    public void setVlr_extracto(double vlr_extracto) {
        this.vlr_extracto = vlr_extracto;
    }

    /**
     * @return the porcentaje
     */
    public double getPorcentaje() {
        return porcentaje;
    }

    /**
     * @param porcentaje the porcentaje to set
     */
    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

    /**
     * @return the vlr_descuento
     */
    public double getVlr_descuento() {
        return vlr_descuento;
    }

    /**
     * @param vlr_descuento the vlr_descuento to set
     */
    public void setVlr_descuento(double vlr_descuento) {
        this.vlr_descuento = vlr_descuento;
    }

    /**
     * @return the vlr_neto
     */
    public double getVlr_neto() {
        return vlr_neto;
    }

    /**
     * @param vlr_neto the vlr_neto to set
     */
    public void setVlr_neto(double vlr_neto) {
        this.vlr_neto = vlr_neto;
    }

    /**
     * @return the vlr_combancaria
     */
    public double getVlr_combancaria() {
        return vlr_combancaria;
    }

    /**
     * @param vlr_combancaria the vlr_combancaria to set
     */
    public void setVlr_combancaria(double vlr_combancaria) {
        this.vlr_combancaria = vlr_combancaria;
    }

    /**
     * @return the vlr_consignacion
     */
    public double getVlr_consignacion() {
        return vlr_consignacion;
    }

    /**
     * @param vlr_consignacion the vlr_consignacion to set
     */
    public void setVlr_consignacion(double vlr_consignacion) {
        this.vlr_consignacion = vlr_consignacion;
    }

    /**
     * @return the vlr_consignacion_calculada
     */
    public double getVlr_consignacion_calculada() {
        return vlr_consignacion_calculada;
    }

    /**
     * @param vlr_consignacion_calculada the vlr_consignacion_calculada to set
     */
    public void setVlr_consignacion_calculada(double vlr_consignacion_calculada) {
        this.vlr_consignacion_calculada = vlr_consignacion_calculada;
    }

    /**
     * @return the vlr_ajuste
     */
    public double getVlr_ajuste() {
        return vlr_ajuste;
    }

    /**
     * @param vlr_ajuste the vlr_ajuste to set
     */
    public void setVlr_ajuste(double vlr_ajuste) {
        this.vlr_ajuste = vlr_ajuste;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the vlr_ext_detalle_calculado
     */
    public double getVlr_ext_detalle_calculado() {
        return vlr_ext_detalle_calculado;
    }

    /**
     * @param vlr_ext_detalle_calculado the vlr_ext_detalle_calculado to set
     */
    public void setVlr_ext_detalle_calculado(double vlr_ext_detalle_calculado) {
        this.vlr_ext_detalle_calculado = vlr_ext_detalle_calculado;
    }

    /**
     * @return the vlr_ext_detalle_registrado
     */
    public double getVlr_ext_detalle_registrado() {
        return vlr_ext_detalle_registrado;
    }

    /**
     * @param vlr_ext_detalle_registrado the vlr_ext_detalle_registrado to set
     */
    public void setVlr_ext_detalle_registrado(double vlr_ext_detalle_registrado) {
        this.vlr_ext_detalle_registrado = vlr_ext_detalle_registrado;
    }

    /**
     * @return the vlr_ext_detalle_diferencia
     */
    public double getVlr_ext_detalle_diferencia() {
        return vlr_ext_detalle_diferencia;
    }

    /**
     * @param vlr_ext_detalle_diferencia the vlr_ext_detalle_diferencia to set
     */
    public void setVlr_ext_detalle_diferencia(double vlr_ext_detalle_diferencia) {
        this.vlr_ext_detalle_diferencia = vlr_ext_detalle_diferencia;
    }

    /**
     * @return the vlr_diferencia_ext_ant
     */
    public double getVlr_diferencia_ext_ant() {
        return vlr_diferencia_ext_ant;
    }

    /**
     * @param vlr_diferencia_ext_ant the vlr_diferencia_ext_ant to set
     */
    public void setVlr_diferencia_ext_ant(double vlr_diferencia_ext_ant) {
        this.vlr_diferencia_ext_ant = vlr_diferencia_ext_ant;
    }

    /**
     * @return the tipo_operacion
     */
    public String getTipo_operacion() {
        return tipo_operacion;
    }

    /**
     * @param tipo_operacion the tipo_operacion to set
     */
    public void setTipo_operacion(String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    /**
     * @return the cuenta_contable_banco
     */
    public String getCuenta_contable_banco() {
        return cuenta_contable_banco;
    }

    /**
     * @param cuenta_contable_banco the cuenta_contable_banco to set
     */
    public void setCuenta_contable_banco(String cuenta_contable_banco) {
        this.cuenta_contable_banco = cuenta_contable_banco;
    }

    /**
     * @return the cuenta_os
     */
    public String getCuenta_os() {
        return cuenta_os;
    }

    /**
     * @param cuenta_os the cuenta_os to set
     */
    public void setCuenta_os(String cuenta_os) {
        this.cuenta_os = cuenta_os;
    }

    /**
     * @return the dbcr_os
     */
    public String getDbcr_os() {
        return dbcr_os;
    }

    /**
     * @param dbcr_os the dbcr_os to set
     */
    public void setDbcr_os(String dbcr_os) {
        this.dbcr_os = dbcr_os;
    }

    /**
     * @return the sigla_comprobante_os
     */
    public String getSigla_comprobante_os() {
        return sigla_comprobante_os;
    }

    /**
     * @param sigla_comprobante_os the sigla_comprobante_os to set
     */
    public void setSigla_comprobante_os(String sigla_comprobante_os) {
        this.sigla_comprobante_os = sigla_comprobante_os;
    }

    /**
     * @return the cmc
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc the cmc to set
     */
    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo_os() {
        return periodo_os;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo_os(String periodo_os) {
        this.periodo_os = periodo_os;
    }

    /**
     * @return the grupo_transaccion
     */
    public int getGrupo_transaccion() {
        return grupo_transaccion;
    }

    /**
     * @param grupo_transaccion the grupo_transaccion to set
     */
    public void setGrupo_transaccion(int grupo_transaccion) {
        this.grupo_transaccion = grupo_transaccion;
    }

    /**
     * @return the banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * @return the fecha_transferencia
     */
    public String getFecha_transferencia() {
        return fecha_transferencia;
    }

    /**
     * @param fecha_transferencia the fecha_transferencia to set
     */
    public void setFecha_transferencia(String fecha_transferencia) {
        this.fecha_transferencia = fecha_transferencia;
    }

    /**
     * @return the tcta_transferencia
     */
    public String getTcta_transferencia() {
        return tcta_transferencia;
    }

    /**
     * @param tcta_transferencia the tcta_transferencia to set
     */
    public void setTcta_transferencia(String tcta_transferencia) {
        this.tcta_transferencia = tcta_transferencia;
    }

    /**
     * @return the banco_transferencia
     */
    public String getBanco_transferencia() {
        return banco_transferencia;
    }

    /**
     * @param banco_transferencia the banco_transferencia to set
     */
    public void setBanco_transferencia(String banco_transferencia) {
        this.banco_transferencia = banco_transferencia;
    }

    /**
     * @return the cuenta_transferencia
     */
    public String getCuenta_transferencia() {
        return cuenta_transferencia;
    }

    /**
     * @param cuenta_transferencia the cuenta_transferencia to set
     */
    public void setCuenta_transferencia(String cuenta_transferencia) {
        this.cuenta_transferencia = cuenta_transferencia;
    }

    /**
     * @return the agency_id
     */
    public String getAgency_id() {
        return agency_id;
    }

    /**
     * @param agency_id the agency_id to set
     */
    public void setAgency_id(String agency_id) {
        this.agency_id = agency_id;
    }

    /**
     * @return the planilla
     */
    public String getPlanilla() {
        return planilla;
    }

    /**
     * @param planilla the planilla to set
     */
    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    /**
     * @return the supplier
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * @param supplier the supplier to set
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * @return the tipo_cuenta
     */
    public String getTipo_cuenta() {
        return tipo_cuenta;
    }

    /**
     * @param tipo_cuenta the tipo_cuenta to set
     */
    public void setTipo_cuenta(String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    /**
     * @return the cedcon
     */
    public String getCedcon() {
        return cedcon;
    }

    /**
     * @param cedcon the cedcon to set
     */
    public void setCedcon(String cedcon) {
        this.cedcon = cedcon;
    }

    /**
     * @return the concept_code
     */
    public String getConcept_code() {
        return concept_code;
    }

    /**
     * @param concept_code the concept_code to set
     */
    public void setConcept_code(String concept_code) {
        this.concept_code = concept_code;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the factura_cxp
     */
    public String getFactura_cxp() {
        return factura_cxp;
    }

    /**
     * @param factura_cxp the factura_cxp to set
     */
    public void setFactura_cxp(String factura_cxp) {
        this.factura_cxp = factura_cxp;
    }

    /**
     * @return the cuenta_anticipo
     */
    public String getCuenta_anticipo() {
        return cuenta_anticipo;
    }

    /**
     * @param cuenta_anticipo the cuenta_anticipo to set
     */
    public void setCuenta_anticipo(String cuenta_anticipo) {
        this.cuenta_anticipo = cuenta_anticipo;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the con_ant_tercero
     */
    public String getCon_ant_tercero() {
        return con_ant_tercero;
    }

    /**
     * @param con_ant_tercero the con_ant_tercero to set
     */
    public void setCon_ant_tercero(String con_ant_tercero) {
        this.con_ant_tercero = con_ant_tercero;
    }

    /**
     * @return the transferido
     */
    public String getTransferido() {
        return transferido;
    }

    /**
     * @param transferido the transferido to set
     */
    public void setTransferido(String transferido) {
        this.transferido = transferido;
    }


}




