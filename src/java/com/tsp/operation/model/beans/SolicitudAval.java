package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Bean para la tabla SolicitudAval<br/> 1/03/2011
 *
 * @author ivargas-Geotech
 */
public class SolicitudAval {

    private String regStatus;
    private String dstrct;
    private String numeroSolicitud;
    private String fechaConsulta;
    private String valorSolicitado;
    private String agente;
    private String afiliado;
    private String codigo;
    private String numeroAprobacion;
    private String estadoSol;
    private String tipoPersona;
    private String valorAprobado;
    private String tipoNegocio;
    private String numTipoNegocio;
    private String banco;
    private String sucursal;
    private String numChequera;
    private String creationDate;
    private String creationUser;
    private String lastUpdate;
    private String userUpdate;
    private String codNegocio;
    private String actividadNegocio;
    private String idConvenio;
    private String producto;
    private String servicio;
    private String ciudadMatricula;
    private String valorProducto;
    private String tipoConv;
    private String asesor;
    private String sector;
    private String subsector;
    private String plazo;
    private String plazoPrCuota;
    private String ciudadCheque;
    private String mod_formulario = "";

    private boolean cat;
    private String renovacion;
    private String fecha_primera_cuota = "0099-01-01 00:00:00";
    private String codNegocioRenovado;
    private String preAprobadoMicro;

    private String tipo_cliente;
    private String score_buro;
    private String score_lisim;
    private String score_total;
    private String accion_sugerida;
    private String capacidad_endeudamiento;

    private String altura_actual_titular;
    private String altura_hist_titular;
    private String altura_actual_codeudor;
    private String altura_hist_codeudor;
    private String cuotas_pendientes;

    private String vr_negocio;
    private String fianza;
    private String ocupacion;

    // Campos destino cr�dito
    private boolean capitalDeTrabajo;
    private boolean activoFijo;
    private boolean CompraCartera;
    private String cuotaMaxima;
    private String destinoCredito;

    private double valor_renovacion;
    private String politica;
    private String nit_fondo;
    private int producto_fondo;
    private int cobertura_fondo;


    public SolicitudAval load(ResultSet rs) throws SQLException {
        SolicitudAval solicitud = new SolicitudAval();
        solicitud.setNumeroSolicitud(rs.getString("numero_solicitud"));
        solicitud.setFechaConsulta(rs.getString("fecha_consulta"));
        solicitud.setValorSolicitado(rs.getString("valor_solicitado"));
        solicitud.setAgente(rs.getString("agente"));
        solicitud.setAfiliado(rs.getString("afiliado"));
        solicitud.setCodigo(rs.getString("codigo"));
        solicitud.setNumeroAprobacion(rs.getString("numero_aprobacion"));
        solicitud.setValorAprobado(rs.getString("valor_aprobado"));
        solicitud.setTipoNegocio(rs.getString("tipo_negocio"));
        solicitud.setNumTipoNegocio(rs.getString("num_tipo_negocio"));
        solicitud.setBanco(rs.getString("banco"));
        solicitud.setSucursal(rs.getString("sucursal"));
        solicitud.setNumChequera(rs.getString("num_chequera"));
        solicitud.setTipoPersona(rs.getString("tipo_persona"));
        solicitud.setEstadoSol(rs.getString("estado_sol"));
        solicitud.setCodNegocio(rs.getString("cod_neg"));
        solicitud.setActividadNegocio(rs.getString("actividad") != null ? rs.getString("actividad") : "");
        solicitud.setIdConvenio(rs.getString("id_convenio"));
        solicitud.setProducto(rs.getString("producto"));
        solicitud.setServicio(rs.getString("servicio"));
        solicitud.setCiudadMatricula(rs.getString("ciudad_matricula"));
        solicitud.setValorProducto(rs.getString("valor_producto"));
        solicitud.setAsesor(rs.getString("asesor"));
        solicitud.setSector(rs.getString("cod_sector"));
        solicitud.setSubsector(rs.getString("cod_subsector"));
        solicitud.setPlazo(rs.getString("plazo"));
        solicitud.setPlazoPrCuota(rs.getString("plazo_pr_cuota"));
        solicitud.setCiudadCheque(rs.getString("ciudad_cheque"));
        solicitud.setCreationDate(rs.getString("creation_date"));
        solicitud.setMod_formulario(rs.getString("mod_formulario"));
        solicitud.setTipo_cliente(rs.getString("tipo_cliente"));
        solicitud.setScore_buro(rs.getString("score_buro"));
        solicitud.setScore_lisim(rs.getString("score_lisim"));
        solicitud.setScore_total(rs.getString("score_total"));
        solicitud.setAccion_sugerida(rs.getString("accion_sugerida"));
        solicitud.setCapacidad_endeudamiento(rs.getString("capacidad_endeudamiento"));
        solicitud.setAltura_actual_titular(rs.getString("altura_mora_actual_titular"));
        solicitud.setAltura_hist_titular(rs.getString("altura_mora_history_titular"));
        solicitud.setAltura_actual_codeudor(rs.getString("altura_mora_actual_codeudor"));
        solicitud.setAltura_hist_codeudor(rs.getString("altura_mora_history_codeudor"));
        solicitud.setCuotas_pendientes(rs.getString("cuotas_pendientes"));
        solicitud.setFianza(rs.getString("fianza"));
        solicitud.setOcupacion(rs.getString("ocupacion"));
        solicitud.setCompraCartera(rs.getBoolean("compra_cartera"));
        solicitud.setActivoFijo(rs.getBoolean("activo_fijo"));
        solicitud.setCapitalDeTrabajo(rs.getBoolean("capital_trabajo"));
        solicitud.setDestinoCredito(rs.getString("destino_credito"));
        solicitud.setCuotaMaxima(rs.getString("cuota_maxima"));
        solicitud.setValor_renovacion(rs.getDouble("valor_renovacion"));
        solicitud.setPolitica(rs.getString("politica"));
        solicitud.setNit_fondo(rs.getString("nit_fondo"));
        solicitud.setCobertura_fondo(rs.getInt("cobertura_fondo"));
        solicitud.setProducto_fondo(rs.getInt("producto_fondo"));

        return solicitud;
    }

    public String getNit_fondo() {
        return nit_fondo;
    }

    public void setNit_fondo(String nit_fondo) {
        this.nit_fondo = nit_fondo;
    }

    public int getProducto_fondo() {
        return producto_fondo;
    }

    public void setProducto_fondo(int producto_fondo) {
        this.producto_fondo = producto_fondo;
    }

    public int getCobertura_fondo() {
        return cobertura_fondo;
    }

    public void setCobertura_fondo(int cobertura_fondo) {
        this.cobertura_fondo = cobertura_fondo;
    }

    public String getTipo_cliente() {
        return tipo_cliente;
    }

    public void setTipo_cliente(String tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    public String getScore_buro() {
        return score_buro;
    }

    public void setScore_buro(String score_buro) {
        this.score_buro = score_buro;
    }

    public String getScore_lisim() {
        return score_lisim;
    }

    public void setScore_lisim(String score_lisim) {
        this.score_lisim = score_lisim;
    }

    public String getScore_total() {
        return score_total;
    }

    public void setScore_total(String score_total) {
        this.score_total = score_total;
    }

    public String getAccion_sugerida() {
        return accion_sugerida;
    }

    public void setAccion_sugerida(String accion_sugerida) {
        this.accion_sugerida = accion_sugerida;
    }

    public String getCapacidad_endeudamiento() {
        return capacidad_endeudamiento;
    }

    public void setCapacidad_endeudamiento(String capacidad_endeudamiento) {
        this.capacidad_endeudamiento = capacidad_endeudamiento;
    }

    public String getCodNegocio() {
        return codNegocio;
    }

    public void setCodNegocio(String codNegocio) {
        this.codNegocio = codNegocio;
    }

    /**
     * obtiene el valor de tarjeta
     *
     *
     *
     *
     * /**
     * obtiene el valor de creationDate
     *
     * @return el valor de creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * cambia el valor de creationDate
     *
     * @param creationDate nuevo valor creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * obtiene el valor de creationUser
     *
     * @return el valor de creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * cambia el valor de creationUser
     *
     * @param creationUser nuevo valor creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * obtiene el valor de lastUpdate
     *
     * @return el valor de lastUpdate
     */
    public String getLastUpdate() {
        return lastUpdate;
    }

    /**
     * cambia el valor de lastUpdate
     *
     * @param lastUpdate nuevo valor lastUpdate
     */
    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * obtiene el valor de numChequera
     *
     * @return el valor de numChequera
     */
    public String getNumChequera() {
        return numChequera;
    }

    /**
     * cambia el valor de numChequera
     *
     * @param numChequera nuevo valor numChequera
     */
    public void setNumChequera(String numChequera) {
        this.numChequera = numChequera;
    }

    /**
     * obtiene el valor de numTipoNegocio
     *
     * @return el valor de numTipoNegocio
     */
    public String getNumTipoNegocio() {
        return numTipoNegocio;
    }

    /**
     * cambia el valor de numTipoNegocio
     *
     * @param numTipoNegocio nuevo valor numTipoNegocio
     */
    public void setNumTipoNegocio(String numTipoNegocio) {
        this.numTipoNegocio = numTipoNegocio;
    }

    /**
     * obtiene el valor de tipoNegocio
     *
     * @return el valor de tipoNegocio
     */
    public String getTipoNegocio() {
        return tipoNegocio;
    }

    /**
     * cambia el valor de tipoNegocio
     *
     * @param tipoNegocio nuevo valor tipoNegocio
     */
    public void setTipoNegocio(String tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }

    /**
     * obtiene el valor de userUpdate
     *
     * @return el valor de userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * cambia el valor de userUpdate
     *
     * @param userUpdate nuevo valor userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * obtiene el valor de valorAprobado
     *
     * @return el valor de valorAprobado
     */
    public String getValorAprobado() {
        return valorAprobado;
    }

    /**
     * cambia el valor de valorAprobado
     *
     * @param valorAprobado nuevo valor valorAprobado
     */
    public void setValorAprobado(String valorAprobado) {
        this.valorAprobado = valorAprobado;
    }

    /**
     * obtiene el valor de afiliado
     *
     * @return el valor de afiliado
     */
    public String getAfiliado() {
        return afiliado;
    }

    /**
     * cambia el valor de afiliado
     *
     * @param afiliado nuevo valor afiliado
     */
    public void setAfiliado(String afiliado) {
        this.afiliado = afiliado;
    }

    /**
     * obtiene el valor de agente
     *
     * @return el valor de agente
     */
    public String getAgente() {
        return agente;
    }

    /**
     * cambia el valor de agente
     *
     * @param agente nuevo valor agente
     */
    public void setAgente(String agente) {
        this.agente = agente;
    }

    /**
     * obtiene el valor de banco
     *
     * @return el valor de banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * cambia el valor de banco
     *
     * @param banco nuevo valor banco
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * obtiene el valor de codigo
     *
     * @return el valor de codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * cambia el valor de codigo
     *
     * @param codigo nuevo valor codigo
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * obtiene el valor de dstrct
     *
     * @return el valor de dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * cambia el valor de dstrct
     *
     * @param dstrct nuevo valor dstrct
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * obtiene el valor de estadoSol
     *
     * @return el valor de estadoSol
     */
    public String getEstadoSol() {
        return estadoSol;
    }

    /**
     * cambia el valor de estadoSol
     *
     * @param estadoSol nuevo valor estadoSol
     */
    public void setEstadoSol(String estadoSol) {
        this.estadoSol = estadoSol;
    }

    /**
     * obtiene el valor de fechaConsulta
     *
     * @return el valor de fechaConsulta
     */
    public String getFechaConsulta() {
        return fechaConsulta;
    }

    /**
     * cambia el valor de fechaConsulta
     *
     * @param fechaConsulta nuevo valor fechaConsulta
     */
    public void setFechaConsulta(String fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    /**
     * obtiene el valor de numeroAprobacion
     *
     * @return el valor de numeroAprobacion
     */
    public String getNumeroAprobacion() {
        return numeroAprobacion;
    }

    /**
     * cambia el valor de numeroAprobacion
     *
     * @param numeroAprobacion nuevo valor numeroAprobacion
     */
    public void setNumeroAprobacion(String numeroAprobacion) {
        this.numeroAprobacion = numeroAprobacion;
    }

    /**
     * obtiene el valor de numeroSolicitud
     *
     * @return el valor de numeroSolicitud
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * cambia el valor de numeroSolicitud
     *
     * @param numeroSolicitud nuevo valor numeroSolicitud
     */
    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    /**
     * obtiene el valor de regStatus
     *
     * @return el valor de regStatus
     */
    public String getRegStatus() {
        return regStatus;
    }

    /**
     * cambia el valor de regStatus
     *
     * @param regStatus nuevo valor regStatus
     */
    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    /**
     * obtiene el valor de sucursal
     *
     * @return el valor de sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * cambia el valor de Sucursal
     *
     * @param sucursal nuevo valor Sucursal
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * obtiene el valor de tipoPersona
     *
     * @return el valor de tipoPersona
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * cambia el valor de tipoPersona
     *
     * @param tipoPersona nuevo valor tipoPersona
     */
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    /**
     * obtiene el valor de valorSolicitado
     *
     * @return el valor de valorSolicitado
     */
    public String getValorSolicitado() {
        return valorSolicitado;
    }

    /**
     * cambia el valor de valorSolicitado
     *
     * @param valorSolicitado nuevo valor valorSolicitado
     */
    public void setValorSolicitado(String valorSolicitado) {
        this.valorSolicitado = valorSolicitado;
    }

    public String getActividadNegocio() {
        return actividadNegocio;
    }

    public void setActividadNegocio(String actividadNegocio) {
        this.actividadNegocio = actividadNegocio;
    }

    public String getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(String idConvenio) {
        this.idConvenio = idConvenio;
    }

    public String getCiudadMatricula() {
        return ciudadMatricula;
    }

    public void setCiudadMatricula(String ciudadMatricula) {
        this.ciudadMatricula = ciudadMatricula;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getValorProducto() {
        return valorProducto;
    }

    public void setValorProducto(String valorProducto) {
        this.valorProducto = valorProducto;
    }

    public String getTipoConv() {
        return tipoConv;
    }

    public void setTipoConv(String tipoConv) {
        this.tipoConv = tipoConv;
    }

    public String getAsesor() {
        return asesor;
    }

    public void setAsesor(String asesor) {
        this.asesor = asesor;
    }

    public boolean isCat() {
        return cat;
    }

    public void setCat(boolean cat) {
        this.cat = cat;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getPlazoPrCuota() {
        return plazoPrCuota;
    }

    public void setPlazoPrCuota(String plazoPrCuota) {
        this.plazoPrCuota = plazoPrCuota;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSubsector() {
        return subsector;
    }

    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

    public String getCiudadCheque() {
        return ciudadCheque;
    }

    public void setCiudadCheque(String ciudadCheque) {
        this.ciudadCheque = ciudadCheque;
    }

    public String getMod_formulario() {
        return mod_formulario;
    }

    public void setMod_formulario(String mod_formulario) {
        this.mod_formulario = mod_formulario;
    }

    /**
     * @return the renovacion
     */
    public String getRenovacion() {
        return renovacion;
    }

    /**
     * @param renovacion the renovacion to set
     */
    public void setRenovacion(String renovacion) {
        this.renovacion = renovacion;
    }

    /**
     * @return the fecha_primera_cuota
     */
    public String getFecha_primera_cuota() {
        return fecha_primera_cuota;
    }

    /**
     * @param fecha_primera_cuota the fecha_primera_cuota to set
     */
    public void setFecha_primera_cuota(String fecha_primera_cuota) {
        this.fecha_primera_cuota = fecha_primera_cuota;
    }

    /**
     * @return the codNegocioRenovado
     */
    public String getCodNegocioRenovado() {
        return codNegocioRenovado;
    }

    /**
     * @param codNegocioRenovado the codNegocioRenovado to set
     */
    public void setCodNegocioRenovado(String codNegocioRenovado) {
        this.codNegocioRenovado = codNegocioRenovado;
    }

    /**
     * @return the preAprobadoMicro
     */
    public String getPreAprobadoMicro() {
        return preAprobadoMicro;
    }

    /**
     * @param preAprobadoMicro the preAprobadoMicro to set
     */
    public void setPreAprobadoMicro(String preAprobadoMicro) {
        this.preAprobadoMicro = preAprobadoMicro;
    }

    /**
     * @return the altura_actual_titular
     */
    public String getAltura_actual_titular() {
        return altura_actual_titular;
    }

    /**
     * @param altura_actual_titular the altura_actual_titular to set
     */
    public void setAltura_actual_titular(String altura_actual_titular) {
        this.altura_actual_titular = altura_actual_titular;
    }

    /**
     * @return the altura_hist_titular
     */
    public String getAltura_hist_titular() {
        return altura_hist_titular;
    }

    /**
     * @param altura_hist_titular the altura_hist_titular to set
     */
    public void setAltura_hist_titular(String altura_hist_titular) {
        this.altura_hist_titular = altura_hist_titular;
    }

    /**
     * @return the altura_actual_codeudor
     */
    public String getAltura_actual_codeudor() {
        return altura_actual_codeudor;
    }

    /**
     * @param altura_actual_codeudor the altura_actual_codeudor to set
     */
    public void setAltura_actual_codeudor(String altura_actual_codeudor) {
        this.altura_actual_codeudor = altura_actual_codeudor;
    }

    /**
     * @return the altura_hist_codeudor
     */
    public String getAltura_hist_codeudor() {
        return altura_hist_codeudor;
    }

    /**
     * @param altura_hist_codeudor the altura_hist_codeudor to set
     */
    public void setAltura_hist_codeudor(String altura_hist_codeudor) {
        this.altura_hist_codeudor = altura_hist_codeudor;
    }

    /**
     * @return the cuotas_pendientes
     */
    public String getCuotas_pendientes() {
        return cuotas_pendientes;
    }

    /**
     * @param cuotas_pendientes the cuotas_pendientes to set
     */
    public void setCuotas_pendientes(String cuotas_pendientes) {
        this.cuotas_pendientes = cuotas_pendientes;
    }

    /**
     * @return the vr_negocio
     */
    public String getVr_negocio() {
        return vr_negocio;
    }

    /**
     * @param vr_negocio the vr_negocio to set
     */
    public void setVr_negocio(String vr_negocio) {
        this.vr_negocio = vr_negocio;
    }

    /**
     * @return the fianza
     */
    public String getFianza() {
        return fianza;
    }

    /**
     * @param fianza the fianza to set
     */
    public void setFianza(String fianza) {
        this.fianza = fianza;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public boolean isCapitalDeTrabajo() {
        return capitalDeTrabajo;
    }

    public void setCapitalDeTrabajo(boolean CapitalDeTrabajo) {
        this.capitalDeTrabajo = CapitalDeTrabajo;
    }
    
    public String getDestinoCredito() {
        return destinoCredito;
    }

    public void setDestinoCredito(String destinoCredito) {
        this.destinoCredito = destinoCredito;
    }

    public boolean isActivoFijo() {
        return activoFijo;
    }

    public void setActivoFijo(boolean activoFijo) {
        this.activoFijo = activoFijo;
    }

    public boolean isCompraCartera() {
        return CompraCartera;
    }

    public void setCompraCartera(boolean CompraCartera) {
        this.CompraCartera = CompraCartera;
    }

    public String getCuotaMaxima() {
        return cuotaMaxima;
    }

    public void setCuotaMaxima(String cuotaMaxima) {
        this.cuotaMaxima = cuotaMaxima;
    }

    public double getValor_renovacion() {
        return valor_renovacion;
    }

    public void setValor_renovacion(double valor_renovacion) {
        this.valor_renovacion = valor_renovacion;
    }

    public String getPolitica() {
        return politica;
    }

    public void setPolitica(String politica) {
        this.politica = politica;
    }

   
    
    
    
    }
