package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * Bean para la tabla dc.direccion<br/>
 * 1/09/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class Direccion {

    private int id;
    private String tipo;
    private String direccion;
    private String fuente;
    private Timestamp creacion;
    private Timestamp actualizacion;
    private int numReportes;
    private String estrato;
    private String probabilidadEntrega;
    private String ciudad;
    private String departamento;
    private String pais;
    private String nuevaNomenclatura;
    private String entidad;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;


    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of entidad
     *
     * @return the value of entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Set the value of entidad
     *
     * @param entidad new value of entidad
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * Get the value of nuevaNomenclatura
     *
     * @return the value of nuevaNomenclatura
     */
    public String getNuevaNomenclatura() {
        return nuevaNomenclatura;
    }

    /**
     * Set the value of nuevaNomenclatura
     *
     * @param nuevaNomenclatura new value of nuevaNomenclatura
     */
    public void setNuevaNomenclatura(String nuevaNomenclatura) {
        this.nuevaNomenclatura = nuevaNomenclatura;
    }

    /**
     * Get the value of pais
     *
     * @return the value of pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * Set the value of pais
     *
     * @param pais new value of pais
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * Get the value of departamento
     *
     * @return the value of departamento
     */
    public String getDepartamento() {
        return departamento;
    }

    /**
     * Set the value of departamento
     *
     * @param departamento new value of departamento
     */
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    /**
     * Get the value of ciudad
     *
     * @return the value of ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Set the value of ciudad
     *
     * @param ciudad new value of ciudad
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }


    /**
     * Get the value of probabilidadEntrega
     *
     * @return the value of probabilidadEntrega
     */
    public String getProbabilidadEntrega() {
        return probabilidadEntrega;
    }

    /**
     * Set the value of probabilidadEntrega
     *
     * @param probabilidadEntrega new value of probabilidadEntrega
     */
    public void setProbabilidadEntrega(String probabilidadEntrega) {
        this.probabilidadEntrega = probabilidadEntrega;
    }

    /**
     * Get the value of estrato
     *
     * @return the value of estrato
     */
    public String getEstrato() {
        return estrato;
    }

    /**
     * Set the value of estrato
     *
     * @param estrato new value of estrato
     */
    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    /**
     * Get the value of numReportes
     *
     * @return the value of numReportes
     */
    public int getNumReportes() {
        return numReportes;
    }

    /**
     * Set the value of numReportes
     *
     * @param numReportes new value of numReportes
     */
    public void setNumReportes(int numReportes) {
        this.numReportes = numReportes;
    }


    /**
     * Get the value of actualizacion
     *
     * @return the value of actualizacion
     */
    public Timestamp getActualizacion() {
        return actualizacion;
    }

    /**
     * Set the value of actualizacion
     *
     * @param actualizacion new value of actualizacion
     */
    public void setActualizacion(Timestamp actualizacion) {
        this.actualizacion = actualizacion;
    }


    /**
     * Get the value of creacion
     *
     * @return the value of creacion
     */
    public Timestamp getCreacion() {
        return creacion;
    }

    /**
     * Set the value of creacion
     *
     * @param creacion new value of creacion
     */
    public void setCreacion(Timestamp creacion) {
        this.creacion = creacion;
    }

    /**
     * Get the value of fuente
     *
     * @return the value of fuente
     */
    public String getFuente() {
        return fuente;
    }

    /**
     * Set the value of fuente
     *
     * @param fuente new value of fuente
     */
    public void setFuente(String fuente) {
        this.fuente = fuente;
    }


    /**
     * Get the value of direccion
     *
     * @return the value of direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Set the value of direccion
     *
     * @param direccion new value of direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Get the value of tipo
     *
     * @return the value of tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set the value of tipo
     *
     * @param tipo new value of tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }


}
