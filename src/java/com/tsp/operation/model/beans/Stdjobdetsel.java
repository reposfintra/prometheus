package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Stdjobdetsel implements Serializable {
    
    private String dstrct;
    private String sj;
    private String sj_desc;
    private String originator_id;
    private String orig_priority;
    private String maint_type;
    private String work_group;
    private String unit_of_work;
    private float vlr_freight;
    private String currency;
    private float qty_standard;
    private float peso_lleno_max;
    private String project_no;
    private String origin_code;
    private String destination_code;
    private String origin_name;
    private String destination_name;
    private String ind_tariff;
    private String ind_printer_pla;
    private String ind_printer_rem;
    private String ind_printer_pmt;
    private String ind_printer_delivery;
    private java.util.Date creation_date;
    private String creation_user;
    private String cliente;
    private String codcliente;
    private String wo_type;
    private String ag_cliente;
    private boolean esta_std;
    private String porcentaje_ant;
    private boolean bloquea_despacho = false;
    private double vlr_pes; 
    private float unidades;
    private int  clasificacion=5;
    private float vlr_mercancia=0;
    String account_code_i="";
    String account_code_c="";
    private String tipoCarga;
    private String pagador;
    
    
    public static Stdjobdetsel load(ResultSet rs)throws SQLException{
        
        Stdjobdetsel stdjobdetsel = new Stdjobdetsel();
        if(rs.getString(2)!=null)
            stdjobdetsel.setDstrct(rs.getString(2));
        if(rs.getString(3)!=null)
            stdjobdetsel.setSj(rs.getString(3));
        if(rs.getString(4)!=null)
            stdjobdetsel.setSj_desc(rs.getString(4));
        if(rs.getString(6)!=null)
            stdjobdetsel.setOriginator_id(rs.getString(6));
        if(rs.getString(7)!=null)
            stdjobdetsel.setOrig_priority(rs.getString(7));
        if(rs.getString(8)!=null)
            stdjobdetsel.setMaint_type(rs.getString(8));
        if(rs.getString(9)!=null)
            stdjobdetsel.setWork_group(rs.getString(9));
        if(rs.getString(10)!=null)
            stdjobdetsel.setUnit_of_work(rs.getString(10));
        
        stdjobdetsel.setVlr_freight(rs.getFloat(11));
        
        if(rs.getString(12)!=null)
            stdjobdetsel.setCurrency(rs.getString(12));
        
        stdjobdetsel.setQty_standard(rs.getFloat(13));
        
        if(rs.getString(14)!=null)
            stdjobdetsel.setProject_no(rs.getString(14));
        if(rs.getString(15)!=null)
            stdjobdetsel.setOrigin_code(rs.getString(15));
        if(rs.getString(16)!=null)
            stdjobdetsel.setDestination_code(rs.getString(16));
        if(rs.getString(17)!=null)
            stdjobdetsel.setOrigin_name(rs.getString(17));
        if(rs.getString(18)!=null)
            stdjobdetsel.setDestination_name(rs.getString(18));
        if(rs.getString(19)!=null)
            stdjobdetsel.setInd_tariff(rs.getString(19));
        if(rs.getString(20)!=null)
            stdjobdetsel.setInd_printer_pla(rs.getString(20));
        if(rs.getString(21)!=null)
            stdjobdetsel.setInd_printer_rem(rs.getString(21));
        if(rs.getString(22)!=null)
            stdjobdetsel.setInd_printer_pmt(rs.getString(22));
        if(rs.getString(23)!=null)
            stdjobdetsel.setInd_printer_delivery(rs.getString(23));
        if(rs.getTimestamp(26)!=null)
            stdjobdetsel.setCreation_date(rs.getTimestamp(26));
        if(rs.getString(27)!=null)
            stdjobdetsel.setCreation_user(rs.getString(27));
        
        //stdjobdetsel.setCliente(rs.getString("customer_id"));
        return stdjobdetsel;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    
    public void setEstaStd(boolean esta_std){
        
        this.esta_std=esta_std;
    }
    
    public boolean getEstaStd(){
        
        return esta_std;
    }
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    public void setAg_cliente(String ag_cliente){
        
        this.ag_cliente=ag_cliente;
    }
    
    public String getAg_cliente(){
        
        return ag_cliente;
    }
    public void setWoType(String wotype){
        
        this.wo_type=wotype;
    }
    
    public String getWoType(){
        
        return wo_type;
    }
    public void setCliente(String cliente){
        
        this.cliente=cliente;
    }
    
    public String getCliente(){
        
        return cliente;
    }
    
    public void setCodCliente(String codcliente){
        
        this.codcliente=codcliente;
    }
    
    public String getCodCliente(){
        
        return codcliente;
    }
    
    public void setSj(String sj){
        
        this.sj=sj;
    }
    
    public String getSj(){
        
        return sj;
    }
    
    public void setSj_desc(String sj_desc){
        
        this.sj_desc=sj_desc;
    }
    
    public String getSj_desc(){
        
        return sj_desc;
    }
    
    public void setOriginator_id(String originator_id){
        
        this.originator_id=originator_id;
    }
    
    public String getOriginator_id(){
        
        return originator_id;
    }
    
    public void setOrig_priority(String orig_priority){
        
        this.orig_priority=orig_priority;
    }
    
    public String getOrig_priority(){
        
        return orig_priority;
    }
    
    public void setMaint_type(String maint_type){
        
        this.maint_type=maint_type;
    }
    
    public String getMaint_type(){
        
        return maint_type;
    }
    
    public void setWork_group(String work_group){
        
        this.work_group=work_group;
    }
    
    public String getWork_group(){
        
        return work_group;
    }
    
    public void setUnit_of_work(String unit_of_work){
        
        this.unit_of_work=unit_of_work;
    }
    
    public String getUnit_of_work(){
        
        return unit_of_work;
    }
    
    public void setVlr_freight(float vlr_freight){
        
        this.vlr_freight=vlr_freight;
    }
    
    public float getVlr_freight(){
        
        return vlr_freight;
    }
    
    public void setCurrency(String currency){
        
        this.currency=currency;
    }
    
    public String getCurrency(){
        
        return currency;
    }
    
    public void setQty_standard(float qty_standard){
        
        this.qty_standard=qty_standard;
    }
    
    public float getQty_standard(){
        
        return qty_standard;
    }
    
    public void setPeso(float peso){
        
        this.peso_lleno_max=peso;
    }
    
    public float getPeso(){
        
        return peso_lleno_max;
    }
    
    public void setProject_no(String project_no){
        
        this.project_no=project_no;
    }
    
    public String getProject_no(){
        
        return project_no;
    }
    
    public void setOrigin_code(String origin_code){
        
        this.origin_code=origin_code;
    }
    
    public String getOrigin_code(){
        
        return origin_code;
    }
    
    public void setDestination_code(String destination_code){
        
        this.destination_code=destination_code;
    }
    
    public String getDestination_code(){
        
        return destination_code;
    }
    
    public void setOrigin_name(String origin_name){
        
        this.origin_name=origin_name;
    }
    
    public String getOrigin_name(){
        
        return origin_name;
    }
    
    public void setDestination_name(String destination_name){
        
        this.destination_name=destination_name;
    }
    
    public String getDestination_name(){
        
        return destination_name;
    }
    
    public void setInd_tariff(String ind_tariff){
        
        this.ind_tariff=ind_tariff;
    }
    
    public String getInd_tariff(){
        
        return ind_tariff;
    }
    
    public void setInd_printer_pla(String ind_printer_pla){
        
        this.ind_printer_pla=ind_printer_pla;
    }
    
    public String getInd_printer_pla(){
        
        return ind_printer_pla;
    }
    
    public void setInd_printer_rem(String ind_printer_rem){
        
        this.ind_printer_rem=ind_printer_rem;
    }
    
    public String getInd_printer_rem(){
        
        return ind_printer_rem;
    }
    
    public void setInd_printer_pmt(String ind_printer_pmt){
        
        this.ind_printer_pmt=ind_printer_pmt;
    }
    
    public String getInd_printer_pmt(){
        
        return ind_printer_pmt;
    }
    
    public void setInd_printer_delivery(String ind_printer_delivery){
        
        this.ind_printer_delivery=ind_printer_delivery;
    }
    
    public String getInd_printer_delivery(){
        
        return ind_printer_delivery;
    }
    
    public void setCreation_date(java.util.Date creation_date){
        
        this.creation_date=creation_date;
    }
    
    public java.util.Date getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
    
    /**
     * Getter for property porcentaje_ant.
     * @return Value of property porcentaje_ant.
     */
    public java.lang.String getPorcentaje_ant() {
        return porcentaje_ant;
    }
    
    /**
     * Setter for property porcentaje_ant.
     * @param porcentaje_ant New value of property porcentaje_ant.
     */
    public void setPorcentaje_ant(java.lang.String porcentaje_ant) {
        this.porcentaje_ant = porcentaje_ant;
    }
    
    /**
     * Getter for property bloquea_despacho.
     * @return Value of property bloquea_despacho.
     */
    public boolean isBloquea_despacho() {
        return bloquea_despacho;
    }
    
    /**
     * Setter for property bloquea_despacho.
     * @param bloquea_despacho New value of property bloquea_despacho.
     */
    public void setBloquea_despacho(boolean bloquea_despacho) {
        this.bloquea_despacho = bloquea_despacho;
    }
    
    /**
     * Getter for property vlr_pes.
     * @return Value of property vlr_pes.
     */
    public double getVlr_pes() {
        return vlr_pes;
    }
    
    /**
     * Setter for property vlr_pes.
     * @param vlr_pes New value of property vlr_pes.
     */
    public void setVlr_pes(double vlr_pes) {
        this.vlr_pes = vlr_pes;
    }
    
    /**
     * Getter for property unidades.
     * @return Value of property unidades.
     */
    public float getUnidades() {
        return unidades;
    }
    
    /**
     * Setter for property unidades.
     * @param unidades New value of property unidades.
     */
    public void setUnidades(float unidades) {
        this.unidades = unidades;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public int getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(int clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property vlr_mercancia.
     * @return Value of property vlr_mercancia.
     */
    public float getVlr_mercancia() {
        return vlr_mercancia;
    }
    
    /**
     * Setter for property vlr_mercancia.
     * @param vlr_mercancia New value of property vlr_mercancia.
     */
    public void setVlr_mercancia(float vlr_mercancia) {
        this.vlr_mercancia = vlr_mercancia;
    }
    
    /**
     * Getter for property account_code_i.
     * @return Value of property account_code_i.
     */
    public java.lang.String getAccount_code_i() {
        return account_code_i;
    }
    
    /**
     * Setter for property account_code_i.
     * @param account_code_i New value of property account_code_i.
     */
    public void setAccount_code_i(java.lang.String account_code_i) {
        this.account_code_i = account_code_i;
    }
    
    /**
     * Getter for property account_code_c.
     * @return Value of property account_code_c.
     */
    public java.lang.String getAccount_code_c() {
        return account_code_c;
    }
    
    /**
     * Setter for property account_code_c.
     * @param account_code_c New value of property account_code_c.
     */
    public void setAccount_code_c(java.lang.String account_code_c) {
        this.account_code_c = account_code_c;
    }
    
    /**
     * Getter for property tipoCarga.
     * @return Value of property tipoCarga.
     */
    public java.lang.String getTipoCarga() {
        return tipoCarga;
    }
    
    /**
     * Setter for property tipoCarga.
     * @param tipoCarga New value of property tipoCarga.
     */
    public void setTipoCarga(java.lang.String tipoCarga) {
        this.tipoCarga = tipoCarga;
    }
    
    /**
     * Getter for property pagador.
     * @return Value of property pagador.
     */
    public java.lang.String getPagador() {
        return pagador;
    }
    
    /**
     * Setter for property pagador.
     * @param pagador New value of property pagador.
     */
    public void setPagador(java.lang.String pagador) {
        this.pagador = pagador;
    }
    
}





