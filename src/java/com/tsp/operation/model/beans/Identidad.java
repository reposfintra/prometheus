/**************************************************************************
 * Nombre:        Identidad.java
 * Descripci�n:   Beans de Identidad
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         16 de julio de 2005, 06:38 PM
 * Versi�n:       Java  1.0                    
 * Copyright:     Fintravalores S.A. S.A. 
 **************************************************************************/


package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  DIOGENES
 */
public class Identidad implements Serializable {
    private String estado;
    private String cedula;
    private String id_mims;
    private String direccion;
    private String codciu;
    private String coddpto;
    private String codpais;
    private String telefono;
    private String celular;
    private String e_mail;
    private java.util.Date fechaultact;
    private String usuario;
    private java.util.Date fechacrea;
    private String usuariocrea;
    private String sexo;
    private String fechanac;
    private String base;
    private String telefono1;
    //private String cod_cia;
    private String cargo;
    //private String ubicacion;
    private String cia;
    private String ref1;
    private String tipo_iden;
    private String observacion;
    
    private String nom1;
    private String nom2;
    private String ape1;
    private String ape2;
    private String pais1;
    private String pais2;
    private String area1;
    private String area2;
    private String num1;
    private String num2;
    
    //campos nuevos
    private String est_civil;
    private String libmilitar;
    private String lugarnac;
    private String barrio;
    private String expced;
    private String senalparti;
    private String nomnemo; 
    private String nombre;
    
    private String clasificacion;
    
    //Dbastidas 18.01.06
    private String aprobado;
    
    //Tito
    private String estado_name;
    private String pais_name;
    private String ciudad_name;
    private String veto;

    /** Creates a new instance of Identidad */
    public static  Identidad load(ResultSet rs)throws SQLException {
        Identidad identidad = new Identidad();
        identidad.setEstado( rs.getString("estado") );
        identidad.setCedula(rs.getString("cedula"));
        identidad.setId_mims((rs.getString("id_mims")!=null)?rs.getString("id_mims"):"");
        identidad.setNombre((rs.getString("nombre")!=null)?rs.getString("nombre"):"");
        identidad.setNom1((rs.getString("nombre1")!=null)?rs.getString("nombre1"):"");
        identidad.setNom2((rs.getString("nombre2")!=null)?rs.getString("nombre2"):"");
        identidad.setApe1((rs.getString("apellido1")!=null)?rs.getString("apellido1"):"");
        identidad.setApe2((rs.getString("apellido2")!=null)?rs.getString("apellido2"):"");
        identidad.setDireccion(rs.getString("direccion"));
        identidad.setCodciu( (rs.getString("codciu")!=null)? rs.getString("codciu"): "NR" );
        identidad.setCoddpto((rs.getString("coddpto")!=null)? rs.getString("coddpto") : "NR");
        identidad.setCodpais((rs.getString("codpais")!=null)? rs.getString("codpais"): "NR");
        try{
            identidad.setTelefono(rs.getString("telefono"));
        }catch(Exception e){}
        identidad.setCelular(rs.getString("celular"));
        identidad.setE_mail(rs.getString("e_mail"));
        identidad.setFechaultact(rs.getDate("fechaultact"));
        identidad.setUsuario(rs.getString("usuario"));
        identidad.setFechacrea(rs.getDate("fechacrea"));
        identidad.setUsuariocrea(rs.getString("usuariocrea"));
        identidad.setSexo(rs.getString("sexo"));
        identidad.setFechanac(""+rs.getDate("fechanac"));
        identidad.setBase(rs.getString("base"));
        identidad.setTelefono1(rs.getString("telefono1"));
        //identidad.setCod_cia(rs.getString("cod_cia"));
        identidad.setCargo(rs.getString("cargo"));
        //identidad.setUbicacion(rs.getString("ubicacion"));
        identidad.setCia(rs.getString("cia"));
        identidad.setRef1(rs.getString("ref1"));
        identidad.setTipo_iden(rs.getString("tipo_iden"));
        identidad.setObservacion(rs.getString("observacion"));
        //nuevos campos
        identidad.setEst_civil(rs.getString("est_civil"));
        identidad.setLugarnac(rs.getString("lugarnac"));
        identidad.setBarrio(rs.getString("barrio"));
        identidad.setLibmilitar(rs.getString("libmilitar"));
        identidad.setExpced(rs.getString("expced"));
        identidad.setSenalParticular(rs.getString("senalparti"));
        identidad.setNomnemo(rs.getString("nemotecnico"));
        identidad.setClasificacion(rs.getString("clasificacion"));
        identidad.setVeto(rs.getString("veto") != null ? rs.getString("veto") :""); 
        return identidad;
        
    }
    /**
     * Setter for property estado.
     * @param estado New value of property nombre.
     */
    public void setEstado (String estado){
        this.estado=estado;
        
    }
    public String getEstado ( ){
        return estado;
    }
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula (String cedula){
        this.cedula=cedula;
        
    }
    public String getCedula ( ){
        return cedula;
    }
    public void setId_mims (String id_mims){
        
        this.id_mims=id_mims;
        
    }
    public String getId_mims ( ){
        return id_mims;
    }
    public void setNom1 (String nombre){
        this.nom1=nombre;
    }
    public void setNom2 (String nombre){
        this.nom2=nombre;
    }
    public void setApe1 (String ape){
        this.ape1=ape;
    }
    public void setApe2 (String ape){
        this.ape2=ape;
    }
    
   
    public String getNom1 ( ){
        return nom1;
    }
   
    public String getNom2 ( ){
        return nom2;
    }
    
    public String getApe1 ( ){
        return ape1;
    }
   
    public String getApe2 ( ){
        return ape2;
    }
    public void setDireccion (String direccion){
        this.direccion=direccion;
        
    }
    public String getDireccion ( ){
        return direccion;
    }
    public void setCodciu (String codciu){
        this.codciu=codciu;
        
    }
    public String getCodciu ( ){
        return codciu;
    }
    public void setCoddpto (String coddpto){
        this.coddpto=coddpto;
        
    }
    public String getCoddpto ( ){
        return coddpto;
    }
    public void setCodpais (String codpais){
        this.codpais=codpais;
        
    }
    public String getCodpais ( ){
        return codpais;
    }

    public void setTelefono (String telefono) throws Exception{
        int ini=0,con=0,j=1;
        try{
            for (int i=0 ; i<= telefono.length() ; i++ ){
                 if ( telefono.substring(i,j).equalsIgnoreCase(" ") ){
                     if(con == 0 ){
                        this.pais1 = telefono.substring(ini,i);
                        ini=j;                        
                        con++;                        
                     }
                     else if(con == 1 ){
                        this.area1 = telefono.substring(ini,i);
                        ini=j;
                        con++;   
                        this.num1= telefono.substring(ini ,telefono.length() );
                        i=telefono.length();
                     }
                 }
                 j++;
            }
        }catch(Exception e){
            this.num1=telefono;
            this.area1="";
            this.pais1="";
        }        
        this.telefono=telefono;
    }
    public String getTelefono ( ){
        return telefono;
    }
    public String getPais1 ( ){
        return pais1;
    }
    public String getPais2 ( ){
        return pais2;
    }
    public String getArea1 ( ){
        return area1;
    }
    public String getArea2 ( ){
        return area2;
    }
    public String getnum1 ( ){
        return num1;
    }
    public String getnum2 ( ){
        return num2;
    }
    public void setCelular (String celular){
        this.celular=celular;
        
    }
    public String getCelular ( ){
        return celular;
    }
    public void setE_mail (String e_mail){
        this.e_mail=e_mail;
        
    }
    public String getE_mail ( ){
        return e_mail;
    }
    public void setFechaultact(java.util.Date fechaultact){
        
        this.fechaultact = fechaultact;
        
    }
    
    public java.util.Date getFechaultact( ){
        
        return fechaultact;
        
    }
    public void setUsuario (String usuario){
        this.usuario=usuario;
        
    }
    public String getUsuario ( ){
        return usuario;
    }
    public void setFechacrea(java.util.Date fechacrea){
        
        this.fechacrea = fechacrea;
        
    }
    
    public java.util.Date getFechacrea( ){
        
        return fechacrea;
        
    }
    public void setUsuariocrea (String usuariocrea){
        this.usuariocrea=usuariocrea;
        
    }
    public String getUsuariocrea ( ){
        return usuariocrea;
    }
    public void setSexo (String sexo){
        this.sexo=sexo;
        
    }
    public String getSexo ( ){
        return sexo;
    }
    public void setFechanac (String fechanac){
        this.fechanac=fechanac;
        
    }
    public String getFechanac ( ){
        return fechanac;
    }
    public void setBase (String base){
        this.base=base;
        
    }
    public String getBase ( ){
        return base;
    }
    public void setTelefono1 (String telefono1){
        int ini=0,con=0,j=1;
        if (telefono1.length()>5){
           for (int i=0 ; i<= telefono1.length() ; i++ ){
             if ( telefono1.substring(i,j).equalsIgnoreCase(" ") ){
                    if(con == 0 ){
                        this.pais2 = telefono1.substring(ini,i);
                        ini=j;                        
                        con++;                        
                    }
                    else if(con == 1 ){
                        this.area2 = telefono1.substring(ini,i);
                        ini=j;
                        con++;   
                        this.num2= telefono1.substring(ini ,telefono1.length() );
                        i=telefono1.length();
                    }
             }
             j++;
          }
        }
        else{
            this.pais2=" ";
            this.area2=" ";
            this.num2=" ";
        }
        this.telefono1=telefono1;
    }
    public String getTelefono1 ( ){
        return telefono1;
    }
    /*public void setCod_cia (String cod_cia){
        this.cod_cia=cod_cia;
        
    }
    public String getCod_cia ( ){
        return cod_cia;
    }*/
    public void setCargo (String cargo){
        this.cargo=cargo;
        
    }
    public String getCargo ( ){
        return cargo;
    }
    /*public void setUbicacion (String ubicacion){
        this.ubicacion=ubicacion;
        
    }
    public String getUbicacion ( ){
        return ubicacion;
    }*/
    public void setCia (String cia){
        this.cia=cia;
        
    }
    public String getCia ( ){
        return cia;
    }
    public void setRef1 (String ref1){
        this.ref1=ref1;
        
    }
    public String getRef1 ( ){
        return ref1;
    }
    public void setTipo_iden (String tipo_iden){
        this.tipo_iden=tipo_iden;
        
    }
    public String getTipo_iden ( ){
        return tipo_iden;
    }
    public void setObservacion (String observacion){
        this.observacion=observacion;
        
    }
    public String getObservacion ( ){
        return observacion;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }    
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    public void setEst_civil(String est){
        this.est_civil=est;
    }
    public String getEst_civil(){
        return est_civil;
    }
    public void setLibmilitar(String nro){
        this.libmilitar=nro;
    }
    public String getLibmilitar(){
        return libmilitar;
    }
    public void setLugarnac(String lugar){
        this.lugarnac = lugar;
    }
    public String getLugarnac(){
        return lugarnac;
    }
    public void setBarrio(String barrio){
        this.barrio = barrio;
    }
    public String getBarrio(){
        return barrio;
    }
    public void setExpced(String expced){
        this.expced = expced;
    }
    public String getExpced(){
        return expced;
    }
    public String getSenalParticular(){
       return this.senalparti;
    }
    public void setSenalParticular(String sign){
        this.senalparti = sign;
    }
    
    /**
     * Getter for property nomnemo.
     * @return Value of property nomnemo.
     */
    public java.lang.String getNomnemo() {
        return nomnemo;
    }
    
    /**
     * Setter for property nomnemo.
     * @param nomnemo New value of property nomnemo.
     */
    public void setNomnemo(java.lang.String nomnemo) {
        this.nomnemo = nomnemo;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.lang.String getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(java.lang.String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property aprobado.
     * @return Value of property aprobado.
     */
    public java.lang.String getAprobado() {
        return aprobado;
    }    
    
    /**
     * Setter for property aprobado.
     * @param aprobado New value of property aprobado.
     */
    public void setAprobado(java.lang.String aprobado) {
        this.aprobado = aprobado;
    }
    
    /**
     * Getter for property ciudad_name.
     * @return Value of property ciudad_name.
     */
    public java.lang.String getCiudad_name() {
        return ciudad_name;
    }
    
    /**
     * Setter for property ciudad_name.
     * @param ciudad_name New value of property ciudad_name.
     */
    public void setCiudad_name(java.lang.String ciudad_name) {
        this.ciudad_name = ciudad_name;
    }
    
    /**
     * Getter for property pais_name.
     * @return Value of property pais_name.
     */
    public java.lang.String getPais_name() {
        return pais_name;
    }
    
    /**
     * Setter for property pais_name.
     * @param pais_name New value of property pais_name.
     */
    public void setPais_name(java.lang.String pais_name) {
        this.pais_name = pais_name;
    }
    
    /**
     * Getter for property estado_name.
     * @return Value of property estado_name.
     */
    public java.lang.String getEstado_name() {
        return estado_name;
    }
    
    /**
     * Setter for property estado_name.
     * @param estado_name New value of property estado_name.
     */
    public void setEstado_name(java.lang.String estado_name) {
        this.estado_name = estado_name;
    }
    
    /**
     * Getter for property veto.
     * @return Value of property veto.
     */
    public java.lang.String getVeto() {
        return veto;
    }
    
    /**
     * Setter for property veto.
     * @param veto New value of property veto.
     */
    public void setVeto(java.lang.String veto) {
        this.veto = veto;
    }
    
}