/*
 * Nombre        Contacto.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         24 de febrero de 2005, 10:42 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class Publicacion implements Serializable {
    
    private String titulo;
    private String fecha;
    private String descripcion;    
    private int numeroVeces;
    private int id_opcion;
    
    //jose
    private String usuario;
    private String fecha_ingreso;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String base;
    private String distrito;
    private String rec_status;
    private String tipo;
    
    /** Creates a new instance of Contacto */
    
    public static Publicacion load(ResultSet rs)throws SQLException {
        Publicacion pu = new Publicacion();  
        pu.setTitulo(rs.getString("descripcion") );
        pu.setFecha(rs.getString("creation_date"));
        pu.setDescripcion(rs.getString("texto"));
        pu.setNumeroVeces(rs.getInt("num_veces_revision"));
        pu.setId_opcion(rs.getInt("id_opcion"));
        return pu;
    }
    
    public static Publicacion load2(ResultSet rs)throws SQLException {
        Publicacion pu = new Publicacion();  
        pu.setTitulo ( rs.getString("descripcion")!=null?rs.getString("descripcion"):"" );
        pu.setFecha ( rs.getString("creation_date")!=null?rs.getString("creation_date"):"" );
        pu.setDescripcion ( rs.getString("texto")!=null?rs.getString("texto"):"" );
        pu.setId_opcion ( rs.getInt("id_opcion") );
        return pu;
    }    
    
    /**
     * Getter for property titulo.
     * @return Value of property titulo.
     */
    public java.lang.String getTitulo() {
        return titulo;
    }
    
    /**
     * Setter for property titulo.
     * @param titulo New value of property titulo.
     */
    public void setTitulo(java.lang.String titulo) {
        this.titulo = titulo;
    }
    
   
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property numeroVeces.
     * @return Value of property numeroVeces.
     */
    public int getNumeroVeces() {
        return numeroVeces;
    }
    
    /**
     * Setter for property numeroVeces.
     * @param numeroVeces New value of property numeroVeces.
     */
    public void setNumeroVeces(int numeroVeces) {
        this.numeroVeces = numeroVeces;
    }
    
    /**
     * Getter for property id_opcion.
     * @return Value of property id_opcion.
     */
    public int getId_opcion() {
        return id_opcion;
    }
    
    /**
     * Setter for property id_opcion.
     * @param id_opcion New value of property id_opcion.
     */
    public void setId_opcion(int id_opcion) {
        this.id_opcion = id_opcion;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario () {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario (java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property fecha_ingreso.
     * @return Value of property fecha_ingreso.
     */
    public java.lang.String getFecha_ingreso () {
        return fecha_ingreso;
    }
    
    /**
     * Setter for property fecha_ingreso.
     * @param fecha_ingreso New value of property fecha_ingreso.
     */
    public void setFecha_ingreso (java.lang.String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion () {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion (java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion () {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion (java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion () {
        return usuario_modificacion;
    }
    
    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion (java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
    /**
     * Getter for property ultima_modificacion.
     * @return Value of property ultima_modificacion.
     */
    public java.lang.String getUltima_modificacion () {
        return ultima_modificacion;
    }
    
    /**
     * Setter for property ultima_modificacion.
     * @param ultima_modificacion New value of property ultima_modificacion.
     */
    public void setUltima_modificacion (java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito () {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito (java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property rec_status.
     * @return Value of property rec_status.
     */
    public java.lang.String getRec_status () {
        return rec_status;
    }
    
    /**
     * Setter for property rec_status.
     * @param rec_status New value of property rec_status.
     */
    public void setRec_status (java.lang.String rec_status) {
        this.rec_status = rec_status;
    }
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo () {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo (java.lang.String tipo) {
        this.tipo = tipo;
    }
    
}
