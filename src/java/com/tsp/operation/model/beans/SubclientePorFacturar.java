/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class SubclientePorFacturar {


    private String id_solicitud;
    private int    parcial;
    private String id_cliente;
    private String nit;
    private String nombre;
    private double val_cuota_inicial;
    private int    periodo;
    private String nic;
    private int    meses_mora;
    private String fecha_financiacion;
    private String tipo_punto;
    private String clase_dtf;
    private double punto;
    private double dtf_semanal;
    private double val_sin_iva;
    private double porcentaje_base;
    private double base_1;
    private double porcentaje_incremento;
    private double porcentaje_extemporaneo;
    private double val_extemporaneo_1;
    private double base_2;
    private double base_3;
    private double iva;
    private double iva_base;
    private double val_extemporaneo_2;
    private double subtotal_iva;
    private double val_a_financiar;
    private double intereses;
    private double val_con_financiacion;
    private double cuota_pago;
    private double porcentaje_interes;
    private String factura_cliente;
    private String simbolo_variable;
    private String observacion;
    private String fecha_factura;


    /** Creates a new instance of SubclientePorFacturar */
    public SubclientePorFacturar() {
    }


    public static SubclientePorFacturar load(java.sql.ResultSet rs)throws java.sql.SQLException{
        
        
        SubclientePorFacturar subclientePorFacturar = new SubclientePorFacturar();
        
        subclientePorFacturar.setId_solicitud(rs.getString("id_solicitud") );
        subclientePorFacturar.setParcial(rs.getInt("parcial") );
        subclientePorFacturar.setId_cliente(rs.getString("id_cliente") );
        subclientePorFacturar.setNit(rs.getString("nit") );
        subclientePorFacturar.setNombre(rs.getString("nombre") );
        subclientePorFacturar.setVal_cuota_inicial(rs.getDouble("val_cuota_inicial") );
        subclientePorFacturar.setPeriodo(rs.getInt("periodo") );
        subclientePorFacturar.setNic(rs.getString("nic") );
        subclientePorFacturar.setMeses_mora(rs.getInt("meses_mora") );
        subclientePorFacturar.setFecha_financiacion(rs.getString("fecha_financiacion") );
        subclientePorFacturar.setTipo_punto(rs.getString("tipo_punto") );
        subclientePorFacturar.setClase_dtf(rs.getString("clase_dtf") );
        subclientePorFacturar.setPunto(rs.getDouble("punto") );
        subclientePorFacturar.setDtf_semanal(rs.getDouble("dtf_semanal") );
        subclientePorFacturar.setVal_sin_iva(rs.getDouble("val_sin_iva") );
        subclientePorFacturar.setPorcentaje_base(rs.getDouble("porcentaje_base") );
        subclientePorFacturar.setBase_1(rs.getDouble("base_1") );
        subclientePorFacturar.setPorcentaje_incremento(rs.getDouble("porcentaje_incremento") );
        subclientePorFacturar.setPorcentaje_extemporaneo(rs.getDouble("porcentaje_extemporaneo") );
        subclientePorFacturar.setVal_extemporaneo_1(rs.getDouble("val_extemporaneo_1") );
        subclientePorFacturar.setBase_2(rs.getDouble("base_2") );
        subclientePorFacturar.setBase_3(rs.getDouble("base_3") );
        subclientePorFacturar.setIva(rs.getDouble("iva") );
        subclientePorFacturar.setIva_base(rs.getDouble("iva_base") );
        subclientePorFacturar.setVal_extemporaneo_2(rs.getDouble("val_extemporaneo_2") );
        subclientePorFacturar.setSubtotal_iva(rs.getDouble("subtotal_iva") );
        subclientePorFacturar.setVal_a_financiar(rs.getDouble("val_a_financiar") );
        subclientePorFacturar.setIntereses(rs.getDouble("intereses") );
        subclientePorFacturar.setVal_con_financiacion(rs.getDouble("val_con_financiacion") );
        subclientePorFacturar.setCuota_pago(rs.getDouble("cuota_pago") );
        subclientePorFacturar.setPorcentaje_interes(rs.getDouble("porcentaje_interes") );
        subclientePorFacturar.setFactura_cliente(rs.getString("factura_cliente") );
        subclientePorFacturar.setSimbolo_variable(rs.getString("simbolo_variable") );
        subclientePorFacturar.setObservacion(rs.getString("observacion") );
        subclientePorFacturar.setFecha_factura(rs.getString("fecha_factura") );
        
        return subclientePorFacturar;
        
        
    }

        





    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the val_cuota_inicial
     */
    public double getVal_cuota_inicial() {
        return val_cuota_inicial;
    }

    /**
     * @param val_cuota_inicial the val_cuota_inicial to set
     */
    public void setVal_cuota_inicial(double val_cuota_inicial) {
        this.val_cuota_inicial = val_cuota_inicial;
    }

    /**
     * @return the periodo
     */
    public int getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the nic
     */
    public String getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(String nic) {
        this.nic = nic;
    }

    /**
     * @return the meses_mora
     */
    public int getMeses_mora() {
        return meses_mora;
    }

    /**
     * @param meses_mora the meses_mora to set
     */
    public void setMeses_mora(int meses_mora) {
        this.meses_mora = meses_mora;
    }

    /**
     * @return the fecha_financiacion
     */
    public String getFecha_financiacion() {
        return fecha_financiacion;
    }

    /**
     * @param fecha_financiacion the fecha_financiacion to set
     */
    public void setFecha_financiacion(String fecha_financiacion) {
        this.fecha_financiacion = fecha_financiacion;
    }

    /**
     * @return the tipo_punto
     */
    public String getTipo_punto() {
        return tipo_punto;
    }

    /**
     * @param tipo_punto the tipo_punto to set
     */
    public void setTipo_punto(String tipo_punto) {
        this.tipo_punto = tipo_punto;
    }

    /**
     * @return the clase_dtf
     */
    public String getClase_dtf() {
        return clase_dtf;
    }

    /**
     * @param clase_dtf the clase_dtf to set
     */
    public void setClase_dtf(String clase_dtf) {
        this.clase_dtf = clase_dtf;
    }

    /**
     * @return the punto
     */
    public double getPunto() {
        return punto;
    }

    /**
     * @param punto the punto to set
     */
    public void setPunto(double punto) {
        this.punto = punto;
    }

    /**
     * @return the dtf_semanal
     */
    public double getDtf_semanal() {
        return dtf_semanal;
    }

    /**
     * @param dtf_semanal the dtf_semanal to set
     */
    public void setDtf_semanal(double dtf_semanal) {
        this.dtf_semanal = dtf_semanal;
    }

    /**
     * @return the val_sin_iva
     */
    public double getVal_sin_iva() {
        return val_sin_iva;
    }

    /**
     * @param val_sin_iva the val_sin_iva to set
     */
    public void setVal_sin_iva(double val_sin_iva) {
        this.val_sin_iva = val_sin_iva;
    }

    /**
     * @return the porcentaje_base
     */
    public double getPorcentaje_base() {
        return porcentaje_base;
    }

    /**
     * @param porcentaje_base the porcentaje_base to set
     */
    public void setPorcentaje_base(double porcentaje_base) {
        this.porcentaje_base = porcentaje_base;
    }

    /**
     * @return the base_1
     */
    public double getBase_1() {
        return base_1;
    }

    /**
     * @param base_1 the base_1 to set
     */
    public void setBase_1(double base_1) {
        this.base_1 = base_1;
    }

    /**
     * @return the porcentaje_incremento
     */
    public double getPorcentaje_incremento() {
        return porcentaje_incremento;
    }

    /**
     * @param porcentaje_incremento the porcentaje_incremento to set
     */
    public void setPorcentaje_incremento(double porcentaje_incremento) {
        this.porcentaje_incremento = porcentaje_incremento;
    }

    /**
     * @return the porcentaje_extemporaneo
     */
    public double getPorcentaje_extemporaneo() {
        return porcentaje_extemporaneo;
    }

    /**
     * @param porcentaje_extemporaneo the porcentaje_extemporaneo to set
     */
    public void setPorcentaje_extemporaneo(double porcentaje_extemporaneo) {
        this.porcentaje_extemporaneo = porcentaje_extemporaneo;
    }

    /**
     * @return the val_extemporaneo_1
     */
    public double getVal_extemporaneo_1() {
        return val_extemporaneo_1;
    }

    /**
     * @param val_extemporaneo_1 the val_extemporaneo_1 to set
     */
    public void setVal_extemporaneo_1(double val_extemporaneo_1) {
        this.val_extemporaneo_1 = val_extemporaneo_1;
    }

    /**
     * @return the base_2
     */
    public double getBase_2() {
        return base_2;
    }

    /**
     * @param base_2 the base_2 to set
     */
    public void setBase_2(double base_2) {
        this.base_2 = base_2;
    }

    /**
     * @return the base_3
     */
    public double getBase_3() {
        return base_3;
    }

    /**
     * @param base_3 the base_3 to set
     */
    public void setBase_3(double base_3) {
        this.base_3 = base_3;
    }

    /**
     * @return the iva
     */
    public double getIva() {
        return iva;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(double iva) {
        this.iva = iva;
    }

    /**
     * @return the iva_base
     */
    public double getIva_base() {
        return iva_base;
    }

    /**
     * @param iva_base the iva_base to set
     */
    public void setIva_base(double iva_base) {
        this.iva_base = iva_base;
    }

    /**
     * @return the val_extemporaneo_2
     */
    public double getVal_extemporaneo_2() {
        return val_extemporaneo_2;
    }

    /**
     * @param val_extemporaneo_2 the val_extemporaneo_2 to set
     */
    public void setVal_extemporaneo_2(double val_extemporaneo_2) {
        this.val_extemporaneo_2 = val_extemporaneo_2;
    }

    /**
     * @return the subtotal_iva
     */
    public double getSubtotal_iva() {
        return subtotal_iva;
    }

    /**
     * @param subtotal_iva the subtotal_iva to set
     */
    public void setSubtotal_iva(double subtotal_iva) {
        this.subtotal_iva = subtotal_iva;
    }

    /**
     * @return the val_a_financiar
     */
    public double getVal_a_financiar() {
        return val_a_financiar;
    }

    /**
     * @param val_a_financiar the val_a_financiar to set
     */
    public void setVal_a_financiar(double val_a_financiar) {
        this.val_a_financiar = val_a_financiar;
    }

    /**
     * @return the intereses
     */
    public double getIntereses() {
        return intereses;
    }

    /**
     * @param intereses the intereses to set
     */
    public void setIntereses(double intereses) {
        this.intereses = intereses;
    }

    /**
     * @return the val_con_financiacion
     */
    public double getVal_con_financiacion() {
        return val_con_financiacion;
    }

    /**
     * @param val_con_financiacion the val_con_financiacion to set
     */
    public void setVal_con_financiacion(double val_con_financiacion) {
        this.val_con_financiacion = val_con_financiacion;
    }

    /**
     * @return the cuota_pago
     */
    public double getCuota_pago() {
        return cuota_pago;
    }

    /**
     * @param cuota_pago the cuota_pago to set
     */
    public void setCuota_pago(double cuota_pago) {
        this.cuota_pago = cuota_pago;
    }

    /**
     * @return the porcentaje_interes
     */
    public double getPorcentaje_interes() {
        return porcentaje_interes;
    }

    /**
     * @param porcentaje_interes the porcentaje_interes to set
     */
    public void setPorcentaje_interes(double porcentaje_interes) {
        this.porcentaje_interes = porcentaje_interes;
    }

    /**
     * @return the factura_cliente
     */
    public String getFactura_cliente() {
        return factura_cliente;
    }

    /**
     * @param factura_cliente the factura_cliente to set
     */
    public void setFactura_cliente(String factura_cliente) {
        this.factura_cliente = factura_cliente;
    }

    /**
     * @return the simbolo_variable
     */
    public String getSimbolo_variable() {
        return simbolo_variable;
    }

    /**
     * @param simbolo_variable the simbolo_variable to set
     */
    public void setSimbolo_variable(String simbolo_variable) {
        this.simbolo_variable = simbolo_variable;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the fecha_factura
     */
    public String getFecha_factura() {
        return fecha_factura;
    }

    /**
     * @param fecha_factura the fecha_factura to set
     */
    public void setFecha_factura(String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }
}