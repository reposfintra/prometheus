/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.util.Date;

/**
 *
 * @author egonzalez
 */
public class ExtractoPagoInicialDetalle {
    
    private int id_rop;
    private int id_conceptos_recaudo;
    private String descripcion;
    private String cuota;
    private int dias_vencidos;
    private Date fecha_factura_padre;
    private Date fecha_vencimiento_padre;
    private Date fecha_ultimo_pago;
    private int items;
    private double valor_concepto;
    private double valor_descuento;
    private double valor_ixm;
    private double valor_descuento_ixm;
    private double valor_gac;
    private double valor_descuento_gac;
    private double valor_abono;
    private double valor_saldo;
    private Date creation_date;
    private String creation_user;
    private String negocio;

    public ExtractoPagoInicialDetalle() {
    }

    public ExtractoPagoInicialDetalle(int id_rop, int id_conceptos_recaudo, String descripcion, String cuota, int dias_vencidos, Date fecha_factura_padre, Date fecha_vencimiento_padre, Date fecha_ultimo_pago, int items, double valor_concepto, double valor_descuento, double valor_ixm, double valor_descuento_ixm, double valor_gac, double valor_descuento_gac, double valor_abono, double valor_saldo, Date creation_date, String creation_user, String negocio) {
        this.id_rop = id_rop;
        this.id_conceptos_recaudo = id_conceptos_recaudo;
        this.descripcion = descripcion;
        this.cuota = cuota;
        this.dias_vencidos = dias_vencidos;
        this.fecha_factura_padre = fecha_factura_padre;
        this.fecha_vencimiento_padre = fecha_vencimiento_padre;
        this.fecha_ultimo_pago = fecha_ultimo_pago;
        this.items = items;
        this.valor_concepto = valor_concepto;
        this.valor_descuento = valor_descuento;
        this.valor_ixm = valor_ixm;
        this.valor_descuento_ixm = valor_descuento_ixm;
        this.valor_gac = valor_gac;
        this.valor_descuento_gac = valor_descuento_gac;
        this.valor_abono = valor_abono;
        this.valor_saldo = valor_saldo;
        this.creation_date = creation_date;
        this.creation_user = creation_user;
        this.negocio = negocio;
    }

    /**
     * @return the id_rop
     */
    public int getId_rop() {
        return id_rop;
    }

    /**
     * @param id_rop the id_rop to set
     */
    public void setId_rop(int id_rop) {
        this.id_rop = id_rop;
    }

    /**
     * @return the id_conceptos_recaudo
     */
    public int getId_conceptos_recaudo() {
        return id_conceptos_recaudo;
    }

    /**
     * @param id_conceptos_recaudo the id_conceptos_recaudo to set
     */
    public void setId_conceptos_recaudo(int id_conceptos_recaudo) {
        this.id_conceptos_recaudo = id_conceptos_recaudo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the cuota
     */
    public String getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(String cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the dias_vencidos
     */
    public int getDias_vencidos() {
        return dias_vencidos;
    }

    /**
     * @param dias_vencidos the dias_vencidos to set
     */
    public void setDias_vencidos(int dias_vencidos) {
        this.dias_vencidos = dias_vencidos;
    }

    /**
     * @return the fecha_factura_padre
     */
    public Date getFecha_factura_padre() {
        return fecha_factura_padre;
    }

    /**
     * @param fecha_factura_padre the fecha_factura_padre to set
     */
    public void setFecha_factura_padre(Date fecha_factura_padre) {
        this.fecha_factura_padre = fecha_factura_padre;
    }

    /**
     * @return the fecha_vencimiento_padre
     */
    public Date getFecha_vencimiento_padre() {
        return fecha_vencimiento_padre;
    }

    /**
     * @param fecha_vencimiento_padre the fecha_vencimiento_padre to set
     */
    public void setFecha_vencimiento_padre(Date fecha_vencimiento_padre) {
        this.fecha_vencimiento_padre = fecha_vencimiento_padre;
    }

    /**
     * @return the fecha_ultimo_pago
     */
    public Date getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    /**
     * @param fecha_ultimo_pago the fecha_ultimo_pago to set
     */
    public void setFecha_ultimo_pago(Date fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    /**
     * @return the items
     */
    public int getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(int items) {
        this.items = items;
    }

    /**
     * @return the valor_concepto
     */
    public double getValor_concepto() {
        return valor_concepto;
    }

    /**
     * @param valor_concepto the valor_concepto to set
     */
    public void setValor_concepto(double valor_concepto) {
        this.valor_concepto = valor_concepto;
    }

    /**
     * @return the valor_descuento
     */
    public double getValor_descuento() {
        return valor_descuento;
    }

    /**
     * @param valor_descuento the valor_descuento to set
     */
    public void setValor_descuento(double valor_descuento) {
        this.valor_descuento = valor_descuento;
    }

    /**
     * @return the valor_ixm
     */
    public double getValor_ixm() {
        return valor_ixm;
    }

    /**
     * @param valor_ixm the valor_ixm to set
     */
    public void setValor_ixm(double valor_ixm) {
        this.valor_ixm = valor_ixm;
    }

    /**
     * @return the valor_descuento_ixm
     */
    public double getValor_descuento_ixm() {
        return valor_descuento_ixm;
    }

    /**
     * @param valor_descuento_ixm the valor_descuento_ixm to set
     */
    public void setValor_descuento_ixm(double valor_descuento_ixm) {
        this.valor_descuento_ixm = valor_descuento_ixm;
    }

    /**
     * @return the valor_gac
     */
    public double getValor_gac() {
        return valor_gac;
    }

    /**
     * @param valor_gac the valor_gac to set
     */
    public void setValor_gac(double valor_gac) {
        this.valor_gac = valor_gac;
    }

    /**
     * @return the valor_descuento_gac
     */
    public double getValor_descuento_gac() {
        return valor_descuento_gac;
    }

    /**
     * @param valor_descuento_gac the valor_descuento_gac to set
     */
    public void setValor_descuento_gac(double valor_descuento_gac) {
        this.valor_descuento_gac = valor_descuento_gac;
    }

    /**
     * @return the valor_abono
     */
    public double getValor_abono() {
        return valor_abono;
    }

    /**
     * @param valor_abono the valor_abono to set
     */
    public void setValor_abono(double valor_abono) {
        this.valor_abono = valor_abono;
    }

    /**
     * @return the valor_saldo
     */
    public double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo(double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    /**
     * @return the creation_date
     */
    public Date getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the negocio
     */
    public String getNegocio() {
        return negocio;
    }

    /**
     * @param negocio the negocio to set
     */
    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }
    
    
    
    
    
}
