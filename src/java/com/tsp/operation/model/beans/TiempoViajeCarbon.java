/***********************************************************************************
 * Nombre clase : ............... TiempoViajeCarbon.java                           *
 * Descripcion :................. Clase que maneja los atributos relacionados      *
 *                                al objeto TiempoViajeCarbon (Reporte  XLS)       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 19 de noviembre de 2005, 09:45 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class TiempoViajeCarbon implements Serializable{
    private String remision;
    private String fec_ingre;
    private String placa;
    private String trayler;
    private String cedcon;
    private String conductor;
    private String cedpro;
    private String propietario;
    private String lleno_pto;
    private String vacio_pto;
    private String neto_pto;
    private String lleno_min;
    private String vacio_min;
    private String neto_min;
    private String dif_pesos;
    private String ruta;
    private String inicio_viaje;
    private String llegada_mina;
    private String entrada_cargue;
    private String salida_mina;
    private String llegada_y;
    private String entrada_puerto;
    private String entrada_descarga;
    private String salida_descar;
    private String salida_puerto;
    private String ruta2;
    private int rangohllegada;
    private String last_update;

    
    
    /** Creates a new instance of TiempoViajeCarbon */
    public TiempoViajeCarbon() {
    }
    
    /**
     * Getter for property remision.
     * @return Value of property remision.
     */
    public java.lang.String getRemision() {
        return remision;
    }
    
    /**
     * Setter for property remision.
     * @param remision New value of property remision.
     */
    public void setRemision(java.lang.String remision) {
        this.remision = remision;
    }
    
    /**
     * Getter for property fec_ingre.
     * @return Value of property fec_ingre.
     */
    public java.lang.String getFec_ingre() {
        return fec_ingre;
    }
    
    /**
     * Setter for property fec_ingre.
     * @param fec_ingre New value of property fec_ingre.
     */
    public void setFec_ingre(java.lang.String fec_ingre) {
        this.fec_ingre = fec_ingre;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property trayler.
     * @return Value of property trayler.
     */
    public java.lang.String getTrayler() {
        return trayler;
    }
    
    /**
     * Setter for property trayler.
     * @param trayler New value of property trayler.
     */
    public void setTrayler(java.lang.String trayler) {
        this.trayler = trayler;
    }
    
    /**
     * Getter for property cedcon.
     * @return Value of property cedcon.
     */
    public java.lang.String getCedcon() {
        return cedcon;
    }
    
    /**
     * Setter for property cedcon.
     * @param cedcon New value of property cedcon.
     */
    public void setCedcon(java.lang.String cedcon) {
        this.cedcon = cedcon;
    }
    
    /**
     * Getter for property conductor.
     * @return Value of property conductor.
     */
    public java.lang.String getConductor() {
        return conductor;
    }
    
    /**
     * Setter for property conductor.
     * @param conductor New value of property conductor.
     */
    public void setConductor(java.lang.String conductor) {
        this.conductor = conductor;
    }
    
    /**
     * Getter for property cedpro.
     * @return Value of property cedpro.
     */
    public java.lang.String getCedpro() {
        return cedpro;
    }
    
    /**
     * Setter for property cedpro.
     * @param cedpro New value of property cedpro.
     */
    public void setCedpro(java.lang.String cedpro) {
        this.cedpro = cedpro;
    }
    
    /**
     * Getter for property propietario.
     * @return Value of property propietario.
     */
    public java.lang.String getPropietario() {
        return propietario;
    }
    
    /**
     * Setter for property propietario.
     * @param propietario New value of property propietario.
     */
    public void setPropietario(java.lang.String propietario) {
        this.propietario = propietario;
    }
    
    /**
     * Getter for property lleno_pto.
     * @return Value of property lleno_pto.
     */
    public java.lang.String getLleno_pto() {
        return lleno_pto;
    }
    
    /**
     * Setter for property lleno_pto.
     * @param lleno_pto New value of property lleno_pto.
     */
    public void setLleno_pto(java.lang.String lleno_pto) {
        this.lleno_pto = lleno_pto;
    }
    
    /**
     * Getter for property vacio_pto.
     * @return Value of property vacio_pto.
     */
    public java.lang.String getVacio_pto() {
        return vacio_pto;
    }
    
    /**
     * Setter for property vacio_pto.
     * @param vacio_pto New value of property vacio_pto.
     */
    public void setVacio_pto(java.lang.String vacio_pto) {
        this.vacio_pto = vacio_pto;
    }
    
    /**
     * Getter for property neto_pto.
     * @return Value of property neto_pto.
     */
    public java.lang.String getNeto_pto() {
        return neto_pto;
    }
    
    /**
     * Setter for property neto_pto.
     * @param neto_pto New value of property neto_pto.
     */
    public void setNeto_pto(java.lang.String neto_pto) {
        this.neto_pto = neto_pto;
    }
    
    /**
     * Getter for property lleno_min.
     * @return Value of property lleno_min.
     */
    public java.lang.String getLleno_min() {
        return lleno_min;
    }
    
    /**
     * Setter for property lleno_min.
     * @param lleno_min New value of property lleno_min.
     */
    public void setLleno_min(java.lang.String lleno_min) {
        this.lleno_min = lleno_min;
    }
    
    /**
     * Getter for property vacio_min.
     * @return Value of property vacio_min.
     */
    public java.lang.String getVacio_min() {
        return vacio_min;
    }
    
    /**
     * Setter for property vacio_min.
     * @param vacio_min New value of property vacio_min.
     */
    public void setVacio_min(java.lang.String vacio_min) {
        this.vacio_min = vacio_min;
    }
    
    /**
     * Getter for property neto_min.
     * @return Value of property neto_min.
     */
    public java.lang.String getNeto_min() {
        return neto_min;
    }
    
    /**
     * Setter for property neto_min.
     * @param neto_min New value of property neto_min.
     */
    public void setNeto_min(java.lang.String neto_min) {
        this.neto_min = neto_min;
    }
    
    /**
     * Getter for property dif_pesos.
     * @return Value of property dif_pesos.
     */
    public java.lang.String getDif_pesos() {
        return dif_pesos;
    }
    
    /**
     * Setter for property dif_pesos.
     * @param dif_pesos New value of property dif_pesos.
     */
    public void setDif_pesos(java.lang.String dif_pesos) {
        this.dif_pesos = dif_pesos;
    }
    
    /**
     * Getter for property ruta.
     * @return Value of property ruta.
     */
    public java.lang.String getRuta() {
        return ruta;
    }
    
    /**
     * Setter for property ruta.
     * @param ruta New value of property ruta.
     */
    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    /**
     * Getter for property inicio_viaje.
     * @return Value of property inicio_viaje.
     */
    public java.lang.String getInicio_viaje() {
        return inicio_viaje;
    }
    
    /**
     * Setter for property inicio_viaje.
     * @param inicio_viaje New value of property inicio_viaje.
     */
    public void setInicio_viaje(java.lang.String inicio_viaje) {
        this.inicio_viaje = inicio_viaje;
    }
    
    /**
     * Getter for property llegada_mina.
     * @return Value of property llegada_mina.
     */
    public java.lang.String getLlegada_mina() {
        return llegada_mina;
    }
    
    /**
     * Setter for property llegada_mina.
     * @param llegada_mina New value of property llegada_mina.
     */
    public void setLlegada_mina(java.lang.String llegada_mina) {
        this.llegada_mina = llegada_mina;
    }
    
    /**
     * Getter for property entrada_cargue.
     * @return Value of property entrada_cargue.
     */
    public java.lang.String getEntrada_cargue() {
        return entrada_cargue;
    }
    
    /**
     * Setter for property entrada_cargue.
     * @param entrada_cargue New value of property entrada_cargue.
     */
    public void setEntrada_cargue(java.lang.String entrada_cargue) {
        this.entrada_cargue = entrada_cargue;
    }
    
    /**
     * Getter for property salida_mina.
     * @return Value of property salida_mina.
     */
    public java.lang.String getSalida_mina() {
        return salida_mina;
    }
    
    /**
     * Setter for property salida_mina.
     * @param salida_mina New value of property salida_mina.
     */
    public void setSalida_mina(java.lang.String salida_mina) {
        this.salida_mina = salida_mina;
    }
    
    /**
     * Getter for property llegada_y.
     * @return Value of property llegada_y.
     */
    public java.lang.String getLlegada_y() {
        return llegada_y;
    }
    
    /**
     * Setter for property llegada_y.
     * @param llegada_y New value of property llegada_y.
     */
    public void setLlegada_y(java.lang.String llegada_y) {
        this.llegada_y = llegada_y;
    }
    
    /**
     * Getter for property entrada_puerto.
     * @return Value of property entrada_puerto.
     */
    public java.lang.String getEntrada_puerto() {
        return entrada_puerto;
    }
    
    /**
     * Setter for property entrada_puerto.
     * @param entrada_puerto New value of property entrada_puerto.
     */
    public void setEntrada_puerto(java.lang.String entrada_puerto) {
        this.entrada_puerto = entrada_puerto;
    }
    
    /**
     * Getter for property entrada_descarga.
     * @return Value of property entrada_descarga.
     */
    public java.lang.String getEntrada_descarga() {
        return entrada_descarga;
    }
    
    /**
     * Setter for property entrada_descarga.
     * @param entrada_descarga New value of property entrada_descarga.
     */
    public void setEntrada_descarga(java.lang.String entrada_descarga) {
        this.entrada_descarga = entrada_descarga;
    }
    
    /**
     * Getter for property salida_descar.
     * @return Value of property salida_descar.
     */
    public java.lang.String getSalida_descar() {
        return salida_descar;
    }
    
    /**
     * Setter for property salida_descar.
     * @param salida_descar New value of property salida_descar.
     */
    public void setSalida_descar(java.lang.String salida_descar) {
        this.salida_descar = salida_descar;
    }
    
    /**
     * Getter for property salida_puerto.
     * @return Value of property salida_puerto.
     */
    public java.lang.String getSalida_puerto() {
        return salida_puerto;
    }
    
    /**
     * Setter for property salida_puerto.
     * @param salida_puerto New value of property salida_puerto.
     */
    public void setSalida_puerto(java.lang.String salida_puerto) {
        this.salida_puerto = salida_puerto;
    }
    
   
    /**
     * Getter for property ruta2.
     * @return Value of property ruta2.
     */
    public java.lang.String getRuta2() {
        return ruta2;
    }
    
    /**
     * Setter for property ruta2.
     * @param ruta2 New value of property ruta2.
     */
    public void setRuta2(java.lang.String ruta2) {
        this.ruta2 = ruta2;
    }
    
    /**
     * Getter for property rangohllegada.
     * @return Value of property rangohllegada.
     */
    public int getRangohllegada() {
        return rangohllegada;
    }
    
    /**
     * Setter for property rangohllegada.
     * @param rangohllegada New value of property rangohllegada.
     */
    public void setRangohllegada(int rangohllegada) {
        this.rangohllegada = rangohllegada;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
}
