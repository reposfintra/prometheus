/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author Harold Cuello G.
 */
public class BeansGenericoRta {

    public BeansGenericoRta() {
    }
    
    //-------------------------------------------------------------
    private int id;
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    //-------------------------------------------------------------
    private String descripcion;
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    //-------------------------------------------------------------
    private String resultado;
    
    public void setResultado(String Resultado) {
        this.resultado = resultado;
    }

    public String getResultado() {
        return resultado;
    }

    //-------------------------------------------------------------
    private String DescYear;
    
    public void setDescYear(String DescYear) {
        this.DescYear = DescYear;
    }
    
    public String getDescYear() {
        return DescYear;
    }
    
    //-------------------------------------------------------------
    private String DescAcumulado;
    
    public void setDescAcumulado(String DescAcumulado) {
        this.DescAcumulado = DescAcumulado;
    }
    
    public String getDescAcumulado() {
        return DescAcumulado;
    }

    
    //-------------------------------------------------------------
    @Override
    public String toString() {
        return "BeansGenericoRta{" + "id=" + id + ", descripcion=" + descripcion + ", resultado=" + resultado + '}';
    }
    
}
