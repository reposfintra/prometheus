/*
 * Sql.java
 *
 * Created on 23 de agosto de 2005, 09:23 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.model.beans;
import java.io.*;

/**
 *
 * @author amendez
 */
public class Sql implements Serializable{
    private String desc = "";
    private String sql = "";
    private String db = "";
    private String name = "";
    private java.lang.String params[];
     
    /** Creates a new instance of Sql */
    public Sql() {
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    } 

    public String[] getParams() {
        return params;
    }

    public void setParams(String[] params) {
        this.params = params;
    }
}
