/*
 * Caravana.java
 *
 * Created on 20 de diciembre de 2004, 0:42
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  Armando
 */
public class Caravana implements Serializable {
    
    private String codigo;
    private String planilla;
    private String usuario;
    private String cia;
    private String creation_date;
    
    
    ///henry
    
    private String dstrct;
    private String agencia;
    private int numcaravana;
    private String numpla;
    private String fecinicio;
    private String fecfin;
    private int caravanaorig;
    private String regstatus;
    private String last_update;
    private String user_update;
    // private String creation_date;
    private String creation_user;
    private String base;
    private String numrem;
    private String razon_exclusion;
    
    //jose
    private String origen;
    private String destino;
    
    /** Creates a new instance of Caravana */
    public Caravana () {
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo () {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo (String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla () {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla (String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario () {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario (String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia () {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia (java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date () {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date (java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    ////HENRY
    public static Caravana load (ResultSet rs)throws SQLException {
        Caravana c = null;
        VistaCaravana v = new VistaCaravana ();
        c.setNumcaravana (rs.getInt ("numcaravana"));
        c.setAgencia (rs.getString ("Agencia"));
        c.setDstrct (rs.getString ("dstrct"));
        c.setNumpla (rs.getString ("numpla"));
        c.setCaravanaorig (rs.getInt ("caravanaorig"));
        c.setFecinicio (rs.getString ("fecinicio"));
        c.setFecfin (rs.getString ("fecfin"));
        c.setLast_update (rs.getString ("last_update"));
        c.setRegstatus (rs.getString ("reg_status"));
        c.setCreation_date (rs.getString ("creation_date"));
        c.setCreation_user (rs.getString ("creation_user"));
        c.setUser_update (rs.getString ("user_update"));
        c.setBase (rs.getString ("base"));
        return c;
    }
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase () {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase (java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user () {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user (java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update () {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update (java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update () {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update (java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property regstatus.
     * @return Value of property regstatus.
     */
    public java.lang.String getRegstatus () {
        return regstatus;
    }
    
    /**
     * Setter for property regstatus.
     * @param regstatus New value of property regstatus.
     */
    public void setRegstatus (java.lang.String regstatus) {
        this.regstatus = regstatus;
    }
    
    /**
     * Getter for property caravanaorig.
     * @return Value of property caravanaorig.
     */
    public int getCaravanaorig () {
        return caravanaorig;
    }
    
    /**
     * Setter for property caravanaorig.
     * @param caravanaorig New value of property caravanaorig.
     */
    public void setCaravanaorig (int caravanaorig) {
        this.caravanaorig = caravanaorig;
    }
    
    /**
     * Getter for property fecfin.
     * @return Value of property fecfin.
     */
    public java.lang.String getFecfin () {
        return fecfin;
    }
    
    /**
     * Setter for property fecfin.
     * @param fecfin New value of property fecfin.
     */
    public void setFecfin (java.lang.String fecfin) {
        this.fecfin = fecfin;
    }
    
    /**
     * Getter for property fecinicio.
     * @return Value of property fecinicio.
     */
    public java.lang.String getFecinicio () {
        return fecinicio;
    }
    
    /**
     * Setter for property fecinicio.
     * @param fecinicio New value of property fecinicio.
     */
    public void setFecinicio (java.lang.String fecinicio) {
        this.fecinicio = fecinicio;
    }
    
    /**
     * Getter for property numpla.
     * @return Value of property numpla.
     */
    public java.lang.String getNumpla () {
        return numpla;
    }
    
    /**
     * Setter for property numpla.
     * @param numpla New value of property numpla.
     */
    public void setNumpla (java.lang.String numpla) {
        this.numpla = numpla;
    }
    
    /**
     * Getter for property numcaravana.
     * @return Value of property numcaravana.
     */
    public int getNumcaravana () {
        return numcaravana;
    }
    
    /**
     * Setter for property numcaravana.
     * @param numcaravana New value of property numcaravana.
     */
    public void setNumcaravana (int numcaravana) {
        this.numcaravana = numcaravana;
    }
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia () {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia (java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct () {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct (java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property razon_exclusion.
     * @return Value of property razon_exclusion.
     */
    public java.lang.String getRazon_exclusion () {
        return razon_exclusion;
    }
    
    /**
     * Setter for property razon_exclusion.
     * @param razon_exclusion New value of property razon_exclusion.
     */
    public void setRazon_exclusion (java.lang.String razon_exclusion) {
        this.razon_exclusion = razon_exclusion;
    }
    
    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem () {
        return numrem;
    }
    
    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem (java.lang.String numrem) {
        this.numrem = numrem;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen () {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen (java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino () {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino (java.lang.String destino) {
        this.destino = destino;
    }
    
}
