/**************************************************************************
 * Nombre:        Extracto.java                     
 * Descripci�n:   Beans de acuerdo especial.               *
 * Autor:         Ing. Julio Barros   *
 * Fecha:         20 de noviembre de 2006, 07:50 AM        *
 * Versi�n:       Java  1.0                                      *
 * Copyright:     Fintravalores S.A. S.A.                                *
 **************************************************************************/


package com.tsp.operation.model.beans;

public class Extracto {
    
    private String reg_status;
    private String dstrct;
    private String fecha;
    private String nit;
    private float vlr_pp;
    private float vlr_ppa;
    private String currency;
    private String banco="";
    private String sucursal;
    private String usuario_aprobacion;
    private String fecha_aprobacion;
    private String cedula_trans; 
    private String nombre_trans; 
    private String banco_trans; 
    private String cuenta_trans; 
    private String tipo_cuenta_trans; 
    private String tipo_pago; 
    private String creation_user; 
    private String creation_date; 
    private String user_update; 
    private String last_update; 
    private String base;

    private int secuencia;    
    
    /** Creates a new instance of ProntoPagoImpreso */
    public Extracto() {
    }
    
    // set's
    public void setReg_status(String reg_status){
        this.reg_status=reg_status;
    }
    
    public void  setDstrct( String dstrct ){
        this.dstrct=dstrct;
    }
    
    public void  setFecha( String fecha ){ 
        this.fecha=fecha;
    }
    
    public void  setNit( String nit ){ 
        this.nit=nit;
    }
    
    public void  setVlr_pp( float vlr_pp ){ 
        this.vlr_pp=vlr_pp;
    }
    
    public void  setVlr_ppa( float vlr_ppa ){ 
        this.vlr_ppa=vlr_ppa;
    }
    
    public void  setCurrency( String currency ){  
        this.currency=currency;
    }

    public void  setBanco( String banco){  
        this.banco=banco;
    }
    
    public void  setSucursal( String sucursal ){  
        this.sucursal=sucursal;
    }
    
    public void  setUsuario_aprobacion( String usuario_aprobacion ){  
        this.usuario_aprobacion=usuario_aprobacion;
    }
    
    public void  setFecha_aprobacion( String fecha_aprobacion ){  
        this.fecha_aprobacion=fecha_aprobacion;
    }
    
    public void  setCedula_trans( String cedula_trans ){  
        this.cedula_trans=cedula_trans;
    } 
    
    public void  setNombre_trans( String nombre_trans ){  
        this.nombre_trans=nombre_trans;
    } 
    
    public void  setBanco_trans( String banco_trans ){  
        this.banco_trans=banco_trans;
    } 
    
    public void  setCuenta_trans( String cuenta_trans ){  
        this.cuenta_trans=cuenta_trans;
    } 
    
    public void  setTipo_cuenta_trans( String tipo_cuenta_trans ){  
        this.tipo_cuenta_trans=tipo_cuenta_trans;
    } 
    
    public void  setTipo_pago( String tipo_pago){  
        this.tipo_pago=tipo_pago;
    } 
    
    public void  setCreation_user( String tipo_pago ){  
        this.tipo_pago=tipo_pago;
    } 
    
    public void  setCreation_date( String creation_date ){  
        this.creation_date=creation_date;
    } 
    
    public void  setUser_update( String user_update ){  
        this.user_update=user_update;
    } 
    
    public void  setLast_update( String last_update){  
        this.last_update=last_update;
    }
    
    public void  setBase( String base){  
        this.base=base;
    }
    
    //get's
    
    public String getReg_status(){
        return this.reg_status;
    }
    
    public String  getDstrct(){
        return this.dstrct;
    }
    
    public String  getFecha(){ 
        return this.fecha;
    }
    
    public String  getNit(){ 
        return this.nit;
    }
    
    public float  getVlr_pp(){ 
        return this.vlr_pp;
    }
    
    public float  getVlr_ppa(){ 
        return this.vlr_ppa;
    }
    
    public String  getCurrency(){ 
        return this.currency;
    }

    public String  getBanco(){ 
        return this.banco;
    }
    
    public String  getSucursal(){ 
        return this.sucursal;
    }
    
    public String  getUsuario_aprobacion(){ 
        return this.usuario_aprobacion;
    }
    
    public String  getFecha_aprobacion(){ 
        return this.fecha_aprobacion;
    }
    
    public String  getCedula_trans(){ 
        return this.cedula_trans;
    } 
    
    public String  getNombre_trans(){ 
        return this.nombre_trans;
    } 
    
    public String  getBanco_trans(){ 
        return this.banco_trans;
    } 
    
    public String  getCuenta_trans(){ 
        return this.cuenta_trans;
    } 
    
    public String  getTipo_cuenta_trans(){ 
        return this.tipo_cuenta_trans;
    } 
    
    public String  getTipo_pago(){ 
        return this.tipo_pago;
    } 
    
    public String  getCreation_user(){ 
        return this.tipo_pago;
    } 
    
    public String  getCreation_date(){ 
        return this.creation_date;
    } 
    
    public String  getUser_update(){ 
        return this.user_update;
    } 
    
    public String  getLast_update(){ 
        return this.last_update;
    }
    
    public String  getBase(){ 
        return this.base;
    }
    
    /**
     * Getter for property secuencia.
     * @return Value of property secuencia.
     */
    public int getSecuencia() {
        return secuencia;
    }
    
    /**
     * Setter for property secuencia.
     * @param secuencia New value of property secuencia.
     */
    public void setSecuencia(int secuencia) {
        this.secuencia = secuencia;
    }
    
}
