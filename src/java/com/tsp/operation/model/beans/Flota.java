 /***********************************************************************************
 * Nombre clase : ............... Flota.java                                       *
 * Descripcion :................. Clase que maneja los atributos relacionados con  *
 *                                la tabla Flota                                   *
 * Autor :....................... Ing. Juan Manuel Escandon Perez                  *
 * Fecha :........................   5 de diciembre de 2005, 05:26 PM              *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;

public class Flota {
    private String dstrct;
    private String reg_status;
    private String base;
    private String std_job_no;
    private String placa;
    private String descripcion;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_actualizacion;
    private String fecha_actualizacion;
    /** Creates a new instance of Flota */
    public Flota() {
    }
    
     /**
     * Metodo Load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo Flota
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static Flota load(ResultSet rs)throws Exception{
        Flota datos = new Flota();
        datos.setReg_status(rs.getString("reg_status"));
        datos.setDescripcion(rs.getString("std_job_desc")!=null?rs.getString("std_job_desc"):"");
        datos.setStd_job_no(rs.getString("std_job_no")!=null?rs.getString("std_job_no"):" ");
        datos.setPlaca(rs.getString("placa"));
        return datos;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_actualizacion.
     * @return Value of property fecha_actualizacion.
     */
    public java.lang.String getFecha_actualizacion() {
        return fecha_actualizacion;
    }
    
    /**
     * Setter for property fecha_actualizacion.
     * @param fecha_actualizacion New value of property fecha_actualizacion.
     */
    public void setFecha_actualizacion(java.lang.String fecha_actualizacion) {
        this.fecha_actualizacion = fecha_actualizacion;
    }
    
    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }
    
    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }
    
    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }
    
    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property usuario_actualizacion.
     * @return Value of property usuario_actualizacion.
     */
    public java.lang.String getUsuario_actualizacion() {
        return usuario_actualizacion;
    }
    
    /**
     * Setter for property usuario_actualizacion.
     * @param usuario_actualizacion New value of property usuario_actualizacion.
     */
    public void setUsuario_actualizacion(java.lang.String usuario_actualizacion) {
        this.usuario_actualizacion = usuario_actualizacion;
    }
    
    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }
    
    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
}
