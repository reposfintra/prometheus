/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Iris Vargas
 */
public class AfiliadoConvenio {

    String idProvConvenio;
    String nitProveedor;
    String idConvenio;
    String codSector;
    String codSubsector;
    String porcentajeAfiliado;
    String cuentaComision;
    String valorCobertura;
    String porcCoberturaFlotante;
    String tasaInteres;
    String valorCustodia;
    String comision;
    String usuario;
    ArrayList <AfiliadoConvenioRangos> rangos;

    public void load(ResultSet rs) throws SQLException {
        this.idProvConvenio=rs.getString("id_prov_convenio");
        this.nitProveedor = rs.getString("nit_proveedor");
        this.idConvenio = rs.getString("id_convenio");
        this.codSector = rs.getString("cod_sector");
        this.codSubsector = rs.getString("cod_subsector");
        this.porcentajeAfiliado = rs.getString("porcentaje_afiliado");
        this.cuentaComision = rs.getString("cuenta_comision");
        this.valorCobertura = rs.getString("valor_cobertura");
        this.porcCoberturaFlotante =rs.getString("porc_cobertura_flotante");
        this.tasaInteres = rs.getString("tasa_interes");
        this.valorCustodia = rs.getString("valor_custodia");
        this.comision = rs.getString("comision");
        this.rangos = new ArrayList <AfiliadoConvenioRangos> ();
    }


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getIdProvConvenio() {
        return idProvConvenio;
    }

    public void setIdProvConvenio(String idProvConvenio) {
        this.idProvConvenio = idProvConvenio;
    }

    public ArrayList<AfiliadoConvenioRangos> getRangos() {
        return rangos;
    }

    public void setRangos(ArrayList<AfiliadoConvenioRangos> rangos) {
        this.rangos = rangos;
    }

    public String getCodSector() {
        return codSector;
    }

    public void setCodSector(String codSector) {
        this.codSector = codSector;
    }

    public String getCodSubsector() {
        return codSubsector;
    }

    public void setCodSubsector(String codSubsector) {
        this.codSubsector = codSubsector;
    }

    public String getComision() {
        return comision;
    }

    public void setComision(String comision) {
        this.comision = comision;
    }

    public String getCuentaComision() {
        return cuentaComision;
    }

    public void setCuentaComision(String cuentaComision) {
        this.cuentaComision = cuentaComision;
    }

    public String getIdConvenio() {
        return idConvenio;
    }

    public void setIdConvenio(String idConvenio) {
        this.idConvenio = idConvenio;
    }

    public String getNitProveedor() {
        return nitProveedor;
    }

    public void setNitProveedor(String nitProveedor) {
        this.nitProveedor = nitProveedor;
    }

    public String getPorcCoberturaFlotante() {
        return porcCoberturaFlotante;
    }

    public void setPorcCoberturaFlotante(String porcCoberturaFlotante) {
        this.porcCoberturaFlotante = porcCoberturaFlotante;
    }

    public String getPorcentajeAfiliado() {
        return porcentajeAfiliado;
    }

    public void setPorcentajeAfiliado(String porcentajeAfiliado) {
        this.porcentajeAfiliado = porcentajeAfiliado;
    }   

    public String getTasaInteres() {
        return tasaInteres;
    }

    public void setTasaInteres(String tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    public String getValorCobertura() {
        return valorCobertura;
    }

    public void setValorCobertura(String valorCobertura) {
        this.valorCobertura = valorCobertura;
    }

    public String getValorCustodia() {
        return valorCustodia;
    }

    public void setValorCustodia(String valorCustodia) {
        this.valorCustodia = valorCustodia;
    }
}
