/*
 * Nombre        Tramo_Ticket.java
 * Autor         Ing Jesus Cuestas
 * Modificado    Ing Sandra Escalante   
 * Fecha         25 de junio de 2005, 10:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.model.beans;

public class Tramo_Ticket {
    
    private String cia;
    private String origen;
    private String destino;
    private String ticket_id;
    private float cantidad;
    private String usuario;
    private String base;
    private String dtiquete;
    /** Creates a new instance of Tramo_Ticket */
    public Tramo_Ticket() {
    }
    
    
    
    
    /**
     * Getter for property cia.
     * @return Value of property cia.
     */
    public java.lang.String getCia() {
        return cia;
    }
    
    /**
     * Setter for property cia.
     * @param cia New value of property cia.
     */
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property ticket_id.
     * @return Value of property ticket_id.
     */
    public java.lang.String getTicket_id() {
        return ticket_id;
    }
    
    /**
     * Setter for property ticket_id.
     * @param ticket_id New value of property ticket_id.
     */
    public void setTicket_id(java.lang.String ticket_id) {
        this.ticket_id = ticket_id;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property cantidad.
     * @return Value of property cantidad.
     */
    public float getCantidad() {
        return cantidad;
    }
    
    /**
     * Setter for property cantidad.
     * @param cantidad New value of property cantidad.
     */
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property dtiquete.
     * @return Value of property dtiquete.
     */
    public java.lang.String getDtiquete() {
        return dtiquete;
    }
    
    /**
     * Setter for property dtiquete.
     * @param dtiquete New value of property dtiquete.
     */
    public void setDtiquete(java.lang.String dtiquete) {
        this.dtiquete = dtiquete;
    }
    
}
