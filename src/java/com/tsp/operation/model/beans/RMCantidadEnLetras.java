/***************************************
    * Nombre Clase ............. RMCantidadEnLetras.java
    * Descripci�n  .. . . . . .  Permite expresar el Letra valores
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  26/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.beans;



public class RMCantidadEnLetras {
    
    /** Creates a new instance of CantidadEnLetras */
    public RMCantidadEnLetras() {
    }
    
    private static String[] xDos  = new String[]{"U|NO","UN","DOS","TRES","CUA|TRO","CIN|CO",
                               "SEIS","SIE|TE","O|CHO","NUE|VE","DIEZ","ON|CE","DO|CE","TRE|CE",
                               "CA|TOR|CE","QUIN|CE","DIE|CI|SEIS","DIE|CI|SIE|TE","DIE|CI|O|CHO",
                               "DIE|CI|NUE|VE","VEIN|TE","VEIN|TI|UN","VEIN|TI|DOS","VEIN|TI|TRES",
                               "VEIN|TI|CUA|TRO","VEIN|TI|CIN|CO","VEIN|TI|SEIS","VEINTI|SIE|TE",
                               "VEIN|TI|O|CHO","VEIN|TI|NUE|VE"};

    private static String[] xUno  = new String[]{"CIEN","CIEN|TO","DOS|CIEN|TOS","TRES|CIEN|TOS",
                               "CUA|TRO|CIEN|TOS","QUI|NIEN|TOS","SEIS|CIEN|TOS","SE|TE|CIEN|TOS",
                               "O|CHO|CIEN|TOS","NO|VE|CIEN|TOS"};

    private static String[] xDiez = new String[]{"VEIN|TI|U|NO","","","TREIN|TA","CUA|REN|TA","CIN|CUEN|TA",
                               "SE|SEN|TA","SE|TEN|TA","O|CHEN|TA","NO|VEN|TA" };

    private static String[] xEsp = new String[]{"Y","MIL","MI|LLON","MI|LLO|NES","BI|LLON","BI|LLO|NES"};
    private static String prefijo_inicio = " ", sufijo_enteros = "PE|SOS",prefijo_tipo_moneda =" ML",
                          prefijo_decimales = "CON", sufijo_decimales = "|/100.-",
                          sufijo_final = " ", palabra_cero = "CE|RO";

    private static char   caracter_proteccion = 42;
    
    private static char   caracter_relleno    = ' ';

    private static int[]  renglones = new int[] { 100 };
    
    private static int    caracteres_max_linea = 78;

    private static char[][] datos;

    private static double importe , divisor , xtres, cantidad;

    private static int    tres, dos , uno, paso, decena, unidad, haymillones,cant_decimales = 2,
                          genero_unidad = 0 , renglon , ajuste , posicion, posicion_corte,
                          silaba , decimales ;

    private static boolean traduce_decimales = false;
    
    
    private static char  ecaracter_proteccion = ' ';
    
    
    
    
    
    
    
    /**
     * Metodo que setea renglones
     * @parameter .... int[] xrenglones
     * @version ...... 1.0
     */  

    public static void setRenglones(int[] xrenglones )    {
        renglones = xrenglones;
    }

    
    
    /**
     * Metodo que setea genero_unidad
     * @parameter .... int xgenero_unidad 
     * @version ...... 1.0
     */  
    public static void setGenero_unidad(int xgenero_unidad )    {
        genero_unidad = xgenero_unidad;
    }

    
    
    /**
     * Metodo que setea cant_decimales
     * @parameter .... int xcant_decimales 
     * @version ...... 1.0
     */  
    public static void setCantidad_decimales(int xcant_decimales)    {
        cant_decimales = xcant_decimales;
    }

    
    /**
     * Metodo que setea prefijo_inicio
     * @parameter .... String xprefijo_inicio 
     * @version ...... 1.0
     */  
    public static void setPrefijo_inicio(String xprefijo_inicio)    {
        prefijo_inicio = xprefijo_inicio;
    }

    
    /**
     * Metodo que setea sufijo_final
     * @parameter .... String xsufijo_final 
     * @version ...... 1.0
     */  
    public static void setSufijo_final(String xsufijo_final)    {
        sufijo_final = xsufijo_final;
    }

    
    /**
     * Metodo que setea sufijo_enteros
     * @parameter .... String xsufijo_enteros
     * @version ...... 1.0
     */  
    public static void setSufijo_enteros(String xsufijo_enteros)    {
        sufijo_enteros = xsufijo_enteros;
    }

    
    
    /**
     * Metodo que setea prefijo_decimales
     * @parameter .... String xprefijo_decimales
     * @version ...... 1.0
     */  
    public static void setPrefijo_decimales(String xprefijo_decimales)  {
        prefijo_decimales = xprefijo_decimales;
    }

      
    
    /**
     * Metodo que setea sufijo_decimales
     * @parameter .... String xsufijo_decimales
     * @version ...... 1.0
     */  
    public static void setSufijo_decimales(String xsufijo_decimales)    {
        sufijo_decimales = xsufijo_decimales;
    }

        
    
    /**
     * Metodo que setea caracter_proteccion
     * @parameter .... char xcaracter_proteccion
     * @version ...... 1.0
     */  
    public static void setCaracter_proteccion(char xcaracter_proteccion)    {
        caracter_proteccion = xcaracter_proteccion;
    }

    
    /**
     * Metodo que setea traduce_decimales
     * @parameter .... boolean xtraduce_decimales
     * @version ...... 1.0
     */  
    public static void setTraduce_decimales(boolean xtraduce_decimales) {
        traduce_decimales = xtraduce_decimales;
    }

    
     /**
     * Metodo que setea palabra_cero
     * @parameter .... String xpalabra_cero
     * @version ...... 1.0
     */  
    public static void setPalabra_cero(String xpalabra_cero) {
        palabra_cero = xpalabra_cero;
    }

    
        
    
    
    /**
     * Metodo que genera el Texto
     * @parameter .... double importe, int qdecimales
     * @version ...... 1.0
     */ 
    private static String[] getTexto(double importe, int qdecimales) {
        int[] renglones = new int[]{ caracteres_max_linea , caracteres_max_linea  };
        RMCantidadEnLetras.setRenglones(renglones);

    decimales = qdecimales;

    divisor   = 1.00E12;

    haymillones = 0;

    if ( cant_decimales > 3 )  traduce_decimales = false;

    ajuste = 10;

    for ( int i=1; i < cant_decimales; i++ )    {
        ajuste = ajuste * 10;
    }

    if ( cant_decimales == 0 ) ajuste = 0;

    datos = new char[renglones.length][renglones[0]];

   
    for ( int x=0; x < renglones.length; x++ )  {
        datos[x] = new char[renglones[x]];
        for ( int y=0; y < renglones[x]; y++ )  {
            datos[x][y] = caracter_relleno;
        }
    }

    xDos[0] = "U|N";//ACA FUE EL CAMBIO
    xDiez[0] = "VEIN|TI|UN";//ACA FUE EL CAMBIO
    if ( genero_unidad == 2 )   {
        xDos[0] = "UN";
        xDiez[0] = "VEIN|TI|UN";
    }
    if ( genero_unidad == 1 )   {
        xDos[0] = "U|NA";
        xDiez[0] = "VEIN|TI|U|NA";
    }

    renglon = posicion = 0;


    for ( paso = 0; paso < 5; paso++ ) {
        xtres  = ( importe / divisor );
        tres   = (int) xtres;
        importe = importe - (double)(tres * divisor);
        divisor = divisor / 1000;
        if ( tres > 0 ) traducir(tres);
    }

    if ( ( palabra_cero.length() > 0 ) && renglon == 0 && posicion == 0 )   {
        if ( prefijo_inicio.length() > 0 ) pasarTexto(prefijo_inicio);
        pasarTexto(palabra_cero);
    }

    if (( sufijo_enteros.length() > 0 ) && ( ( renglon > 0 ) || ( posicion > prefijo_inicio.length())))
        pasarTexto(sufijo_enteros + prefijo_tipo_moneda );

    tres = (int) ( importe * ajuste );

    if ( decimales > 0 ) tres = decimales;

    if ( tres > 0 && ( prefijo_decimales.length() > 0 ) && ( renglon != 0 || posicion != 0))
        pasarTexto(prefijo_decimales);

    if ( tres > 0 && traduce_decimales )    {
        paso = 5;
        traducir(tres);
    }

    if ( tres > 0 && !traduce_decimales )  pasarTexto(("" + tres + ""));

    if ( tres > 0 && ( sufijo_decimales.length() > 0 )) pasarTexto(sufijo_decimales);

    if ( sufijo_final.length() > 0 ) pasarTexto(sufijo_final);

    
  
    String[] texto = new String[datos.length];

    for ( int i=0; i < datos.length; i++ )  {
        texto[i] = new String(datos[i]);
    }


    return texto ;
    }

    
    
    
    
    
    
    
    /**
     * Metodo que inicializa silaba y actualiza posicion_corte
     * @version ...... 1.0
     */ 
    private static void iniciarSilaba() {
        posicion_corte = posicion;
        silaba = 0;
    }
    
    
   /**
     * Metodo que paseCaracter
     * @parameter .... char caracter
     * @version ...... 1.0
     */ 
    private static void paseCaracter(char caracter)  {
        datos[renglon][posicion] = caracter;
        silaba++;
        posicion++;
    }

    
    /**
     * Metodo que sumeRenglon
     * @version ...... 1.0
     */ 
    private static void sumeRenglon()   {
        renglon++;
        posicion = 0;
        iniciarSilaba();
    }


    
    /**
     * Metodo que pasarTexto
     * @parameter .... String palabra
     * @version ...... 1.0
     */ 
    private static void pasarTexto(String palabra) {

        char[] desglose = palabra.toCharArray();

        if (posicion > 0 && ( posicion < ( renglones[renglon] -1 )) &&
            ( desglose[0] != 124 ))  {
            datos[renglon][posicion] = 32;
            posicion ++;
        } else

        if (posicion > 0 && ( posicion == ( renglones[renglon] -1 )) &&
            ( desglose[0] != 124 ))  {
            datos[renglon][posicion] = 32;
            sumeRenglon();
        }

        iniciarSilaba();

        for ( int i=0; i < desglose.length; i++ ) {

        if ( desglose[i] == 124 && i > 0  && ( posicion == ( renglones[renglon] -1 )))   {
            datos[renglon][posicion] = 45;
            sumeRenglon();
        } else

        if ( desglose[i] == 124 && i > 0  && ( posicion < ( renglones[renglon] -1 )))   {
           iniciarSilaba();
        } else

        if ( desglose[i] != 124 && ( posicion < ( renglones[renglon] -1 )))   {
           paseCaracter(desglose[i]);
        } else

        if ( (desglose[i] != 124) && ( posicion == ( renglones[renglon] -1 ))
             && ( i == ( desglose.length - 1)) )  {
           paseCaracter(desglose[i]);
           sumeRenglon();
        } else

        if ( desglose[i] != 124 && ( posicion == ( renglones[renglon] -1 ))
             && ( i < ( desglose.length - 1)))   {

        posicion = posicion_corte;
        datos[renglon][posicion] = 45;
        posicion++;

        if ( posicion <  renglones[renglon]  ) {
            for ( posicion=posicion; posicion < renglones[renglon]; posicion++ )   {
            datos[renglon][posicion] = 45;
            }
        }
        int xsilaba = silaba;
        sumeRenglon();

        for ( int z = (i - xsilaba); z < ( i + 1 ); z++ )  {
                datos[renglon][posicion] = desglose[z];
                posicion++;
                }
           }
        }
    }

    
    
     /**
     * Metodo que traducir
     * @parameter .... int mil
     * @version ...... 1.0
     */ 
    private static void traducir(int mil)    {

        
        if ( renglon == 0 && posicion == 0 && prefijo_inicio.length() > 0 )
            pasarTexto(prefijo_inicio);
        uno  =  mil / 100;
        dos  =  mil - ( uno * 100 );
        decena = dos / 10;
        unidad = dos - ( decena * 10 );
        
         
        if ( mil == 100 )  pasarTexto(xUno[0]);
        else if ( uno > 0 ) pasarTexto(xUno[uno]);

      
        if( dos==1 && unidad>0 && paso<4)
            pasarTexto(xDos[unidad]);
        
        
        
        if ( (dos > 1 && dos < 30 ) && ( paso != 4 || dos != 21 ))  pasarTexto(xDos[dos]);
        if ( paso == 4 && dos == 21 )   pasarTexto(xDiez[0]);
        if ( paso == 4 && dos == 1 )    pasarTexto(xDos[0]);

        if ( dos > 29 ) {
            pasarTexto(xDiez[decena]);
            if ( unidad > 0 )   {
            pasarTexto(xEsp[0]);
            if ( paso != 4 || unidad != 1 ) pasarTexto(xDos[unidad]);
            if ( paso == 4 && unidad == 1 ) pasarTexto(xDos[0]);
            }
        }
        
        if ( paso == 0 && mil>0 )    {
             pasarTexto(xEsp[1]);
        }
        
        if ( paso == 0 && mil == 1 )    {
             pasarTexto(xEsp[4]);
        }
        if ( paso == 0 && mil > 1 )     {
            pasarTexto(xEsp[5]);
        }
        if (( paso == 2 && mil > 1 ) || haymillones == 1 )  {
            pasarTexto(xEsp[3]);
            haymillones = 0;
        }
        
        if ( paso == 1 && mil > 0 )     {
            pasarTexto( xEsp[1]);
            haymillones = 1;
        }
        if (( paso == 2 && mil == 1 && haymillones == 0 ))  {
            pasarTexto(xEsp[2]);
        }
    
        if ( paso == 3 && mil > 0 )     {
            pasarTexto(  xEsp[1]);
        }
        
        
    }
    
    
    
     /**
     * Metodo que getTexto
     * @parameter .... String cantidad
     * @version ...... 1.0
     */ 
    public static String[] getTexto(String cantidad) {

      String parte_entera = ((cantidad.substring(0,cantidad.indexOf('.'))) + ".0");
      cantidad = cantidad.substring(cantidad.indexOf('.') + 1 );
      if ( cantidad.length() > cant_decimales ) cantidad = cantidad.substring(0,cant_decimales);
      if ( cantidad.length() < cant_decimales )    {
          for ( int i = cantidad.length(); i < cant_decimales; i++ )   {
              cantidad = cantidad + "0";
          }
      }

      
      
     /**
     * Metodo que getTexto
     * @parameter .... Double.parseDouble(parte_entera),Integer.parseInt(cantidad)
     * @version ...... 1.0
     */ 
    return getTexto( Double.parseDouble(parte_entera),Integer.parseInt(cantidad) );
    }

    
     /**
     * Metodo que getTexto
     * @parameter .... double cantidad
     * @version ...... 1.0
     */ 
    public static String[] getTexto(double cantidad) {
         return getTexto(cantidad,0);
    }

    
    
    
    
    
    /**
     * Metodo que getTexto
     * @parameter .... String cantidad
     * @version ...... 1.0
     */ 
    public static String[] getTexto(String cantidad, String moneda) {
        sufijo_enteros = getNombreMoneda(moneda);
        return getTexto(cantidad);      
    }
    
    
    /**
     * Metodo que getTexto
     * @parameter .... double cantidad
     * @version ...... 1.0
     */ 
    public static String[] getTexto(double cantidad, String moneda) {
         sufijo_enteros = getNombreMoneda(moneda);
         return getTexto(cantidad,0);
    }
    
    
    public static String getNombreMoneda(String moneda){
        String name = "";
        if(moneda.equals("PES"))  name = "PE|SOS";
        if(moneda.equals("BOL"))  name = "BOLIVAR|ES";
        if(moneda.equals("DOL"))  name = "DOLAR|ES";      
        return name;
    }
    
    
    
    
    
     /**
     * Metodo void main
     * @parameter .... String[] args
     * @version ...... 1.0
     */ 
    public static void main(String[] args) {

        double cantidad = 4444444;
        String moneda   = "BOL";
        String texto[]  = RMCantidadEnLetras.getTexto(cantidad, moneda);
        for ( int i = 0 ; i < texto.length; i++ )   {
            ////System.out.println(texto[i] );
        }
        
    }

    
    

    
}
