/***************************************
 * Nombre Clase ............. Cheque.java
 * Descripci�n  .. . . . . .  Generamos Los Cheques a pagar
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  14/12/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.model.beans;



import java.util.*;
import com.tsp.util.*;


public class Cheque {
    
    private String  distrito      = "";
    private String  numero        = "";
    private String  corrida       = "";
    private String  banco         = "";
    private String  sucursal      = "";
    private String  cuentaBanco   = "";
    private String  moneda        = "";
    
    private String  beneficiario  = "";
    private String  nombre        = "";
    private double  monto         = 0;
    private String  montoEscrito  = "";
    private String  ano           = "";
    private String  mes           = "";
    private String  dia           = "";
    
    
    private String  comentario    = "";
    private boolean impreso       = false;
    private String  agencia       = "";
    
    
    private List    facturas;
    private List    esquema;
    private Series  serie;
    
    private double  page;
    private double  topeDetalle;
    private int     contDetalle;
    
    private double  vlrPagar     = 0;
    
    
    private int     lastNumber;
    
    private String  monedaLocal;
    
    
    private String nom_beneficiario;
    private String nit_beneficiario;
    
    private List esquemaLaser;
    private String nitProveedor;
    
    //Jescandon 24 Febrero
    private String nit;
    private String pmt_date;
    private String concept_code;
    private String printer_date;
    
    private double vlr;
    private double vlr_for;
    private String creation_user;
    private String base;
    private String tipo_documento;
    private String fecha_cheque;
    private String usuario_impresion;
    private double tasa;
    private String reimpresion;
    private String id_precheque;
    
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property pmt_date.
     * @return Value of property pmt_date.
     */
    public java.lang.String getPmt_date() {
        return pmt_date;
    }
    
    /**
     * Setter for property pmt_date.
     * @param pmt_date New value of property pmt_date.
     */
    public void setPmt_date(java.lang.String pmt_date) {
        this.pmt_date = pmt_date;
    }
    
    /**
     * Getter for property concept_code.
     * @return Value of property concept_code.
     */
    public java.lang.String getConcept_code() {
        return concept_code;
    }
    
    /**
     * Setter for property concept_code.
     * @param concept_code New value of property concept_code.
     */
    public void setConcept_code(java.lang.String concept_code) {
        this.concept_code = concept_code;
    }
    
    /**
     * Getter for property printer_date.
     * @return Value of property printer_date.
     */
    public java.lang.String getPrinter_date() {
        return printer_date;
    }
    
    /**
     * Setter for property printer_date.
     * @param printer_date New value of property printer_date.
     */
    public void setPrinter_date(java.lang.String printer_date) {
        this.printer_date = printer_date;
    }
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public double getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }
    
    /**
     * Getter for property vlr_for.
     * @return Value of property vlr_for.
     */
    public double getVlr_for() {
        return vlr_for;
    }
    
    /**
     * Setter for property vlr_for.
     * @param vlr_for New value of property vlr_for.
     */
    public void setVlr_for(double vlr_for) {
        this.vlr_for = vlr_for;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property fecha_cheque.
     * @return Value of property fecha_cheque.
     */
    public java.lang.String getFecha_cheque() {
        return fecha_cheque;
    }
    
    /**
     * Setter for property fecha_cheque.
     * @param fecha_cheque New value of property fecha_cheque.
     */
    public void setFecha_cheque(java.lang.String fecha_cheque) {
        this.fecha_cheque = fecha_cheque;
    }
    
    /**
     * Getter for property usuario_impresion.
     * @return Value of property usuario_impresion.
     */
    public java.lang.String getUsuario_impresion() {
        return usuario_impresion;
    }
    
    /**
     * Setter for property usuario_impresion.
     * @param usuario_impresion New value of property usuario_impresion.
     */
    public void setUsuario_impresion(java.lang.String usuario_impresion) {
        this.usuario_impresion = usuario_impresion;
    }
    
    /**
     * Getter for property tasa.
     * @return Value of property tasa.
     */
    public double getTasa() {
        return tasa;
    }
    
    /**
     * Setter for property tasa.
     * @param tasa New value of property tasa.
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
    
    /**
     * Getter for property reimpresion.
     * @return Value of property reimpresion.
     */
    public java.lang.String getReimpresion() {
        return reimpresion;
    }
    
    /**
     * Setter for property reimpresion.
     * @param reimpresion New value of property reimpresion.
     */
    public void setReimpresion(java.lang.String reimpresion) {
        this.reimpresion = reimpresion;
    }
    
    
    public Cheque() {
        topeDetalle  = 11;
        page         = 600;
        esquema      = new LinkedList();
        facturas     = new LinkedList();
        serie        = null;
        impreso      = false;
    }
    
    /**
     * M�todos que calcula la cantidad total de detalle, esto para el espacio disponible para las facturas.
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public int getContDetalle(){
        contDetalle = 0;
        if(this.facturas!=null){
            contDetalle  = facturas.size();
            for(int i=0;i<this.facturas.size();i++){
                FacturasCheques fact = (FacturasCheques)this.facturas.get(i);
                contDetalle  += fact.getDocumnetosRelacionados().size();
                
            }
        }
        return contDetalle;
    }
    
    
    
    
    
    
    
    
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property ano.
     * @return Value of property ano.
     */
    public java.lang.String getAno() {
        return ano;
    }
    
    /**
     * Setter for property ano.
     * @param ano New value of property ano.
     */
    public void setAno(java.lang.String ano) {
        this.ano = ano;
    }
    
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    /**
     * Getter for property beneficiario.
     * @return Value of property beneficiario.
     */
    public java.lang.String getBeneficiario() {
        return beneficiario;
    }
    
    /**
     * Setter for property beneficiario.
     * @param beneficiario New value of property beneficiario.
     */
    public void setBeneficiario(java.lang.String beneficiario) {
        this.beneficiario = beneficiario;
    }
    
    /**
     * Getter for property comentario.
     * @return Value of property comentario.
     */
    public java.lang.String getComentario() {
        return comentario;
    }
    
    /**
     * Setter for property comentario.
     * @param comentario New value of property comentario.
     */
    public void setComentario(java.lang.String comentario) {
        this.comentario = comentario;
    }
    
    /**
     * Getter for property corrida.
     * @return Value of property corrida.
     */
    public java.lang.String getCorrida() {
        return corrida;
    }
    
    /**
     * Setter for property corrida.
     * @param corrida New value of property corrida.
     */
    public void setCorrida(java.lang.String corrida) {
        this.corrida = corrida;
    }
    
    /**
     * Getter for property cuentaBanco.
     * @return Value of property cuentaBanco.
     */
    public java.lang.String getCuentaBanco() {
        return cuentaBanco;
    }
    
    /**
     * Setter for property cuentaBanco.
     * @param cuentaBanco New value of property cuentaBanco.
     */
    public void setCuentaBanco(java.lang.String cuentaBanco) {
        this.cuentaBanco = cuentaBanco;
    }
    
    /**
     * Getter for property dia.
     * @return Value of property dia.
     */
    public java.lang.String getDia() {
        return dia;
    }
    
    /**
     * Setter for property dia.
     * @param dia New value of property dia.
     */
    public void setDia(java.lang.String dia) {
        this.dia = dia;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property esquema.
     * @return Value of property esquema.
     */
    public java.util.List getEsquema() {
        return esquema;
    }
    
    /**
     * Setter for property esquema.
     * @param esquema New value of property esquema.
     */
    public void setEsquema(java.util.List esquema) {
        this.esquema = esquema;
    }
    
    /**
     * Getter for property facturas.
     * @return Value of property facturas.
     */
    public java.util.List getFacturas() {
        return facturas;
    }
    
    /**
     * Setter for property facturas.
     * @param facturas New value of property facturas.
     */
    public void setFacturas(java.util.List facturas) {
        this.facturas = facturas;
    }
    
    /**
     * Getter for property impreso.
     * @return Value of property impreso.
     */
    public boolean isImpreso() {
        return impreso;
    }
    
    /**
     * Setter for property impreso.
     * @param impreso New value of property impreso.
     */
    public void setImpreso(boolean impreso) {
        this.impreso = impreso;
    }
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property monto.
     * @return Value of property monto.
     */
    public double getMonto() {
        return monto;
    }
    
    /**
     * Setter for property monto.
     * @param monto New value of property monto.
     */
    public void setMonto(double monto) {
        this.monto = monto;
    }
    
    /**
     * Getter for property montoEscrito.
     * @return Value of property montoEscrito.
     */
    public java.lang.String getMontoEscrito() {
        return montoEscrito;
    }
    
    /**
     * Setter for property montoEscrito.
     * @param montoEscrito New value of property montoEscrito.
     */
    public void setMontoEscrito(java.lang.String montoEscrito) {
        this.montoEscrito = montoEscrito;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property numero.
     * @return Value of property numero.
     */
    public java.lang.String getNumero() {
        return numero;
    }
    
    /**
     * Setter for property numero.
     * @param numero New value of property numero.
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property serie.
     * @return Value of property serie.
     */
    public com.tsp.operation.model.beans.Series getSerie() {
        return serie;
    }
    
    /**
     * Setter for property serie.
     * @param serie New value of property serie.
     */
    public void setSerie(com.tsp.operation.model.beans.Series serie) {
        this.serie = serie;
    }
    
    /**
     * Getter for property page.
     * @return Value of property page.
     */
    public double getPage() {
        return page;
    }
    
    /**
     * Setter for property page.
     * @param page New value of property page.
     */
    public void setPage(double page) {
        this.page = page;
    }
    
    /**
     * Getter for property topeDetalle.
     * @return Value of property topeDetalle.
     */
    public double getTopeDetalle() {
        return topeDetalle;
    }
    
    /**
     * Setter for property topeDetalle.
     * @param topeDetalle New value of property topeDetalle.
     */
    public void setTopeDetalle(double topeDetalle) {
        this.topeDetalle = topeDetalle;
    }
    
    /**
     * Getter for property vlrPagar.
     * @return Value of property vlrPagar.
     */
    public double getVlrPagar() {
        return vlrPagar;
    }
    
    /**
     * Setter for property vlrPagar.
     * @param vlrPagar New value of property vlrPagar.
     */
    public void setVlrPagar(double vlrPagar) {
        this.vlrPagar = vlrPagar;
    }
    
    
    
    
    
    /**
     * Getter for property lastNumber.
     * @return Value of property lastNumber.
     */
    public int getLastNumber() {
        return lastNumber;
    }
    
    /**
     * Setter for property lastNumber.
     * @param lastNumber New value of property lastNumber.
     */
    public void setLastNumber(int lastNumber) {
        this.lastNumber = lastNumber;
    }
    
    
    
    /**
     * Getter for property monedaLocal.
     * @return Value of property monedaLocal.
     */
    public java.lang.String getMonedaLocal() {
        return monedaLocal;
    }
    
    /**
     * Setter for property monedaLocal.
     * @param monedaLocal New value of property monedaLocal.
     */
    public void setMonedaLocal(java.lang.String monedaLocal) {
        this.monedaLocal = monedaLocal;
    }
    
    /**
     * Getter for property nom_beneficiario.
     * @return Value of property nom_beneficiario.
     */
    public java.lang.String getNom_beneficiario() {
        return nom_beneficiario;
    }
    
    /**
     * Setter for property nom_beneficiario.
     * @param nom_beneficiario New value of property nom_beneficiario.
     */
    public void setNom_beneficiario(java.lang.String nom_beneficiario) {
        this.nom_beneficiario = nom_beneficiario;
    }
    
    /**
     * Getter for property nit_beneficiario.
     * @return Value of property nit_beneficiario.
     */
    public java.lang.String getNit_beneficiario() {
        return nit_beneficiario;
    }
    
    /**
     * Setter for property nit_beneficiario.
     * @param nit_beneficiario New value of property nit_beneficiario.
     */
    public void setNit_beneficiario(java.lang.String nit_beneficiario) {
        this.nit_beneficiario = nit_beneficiario;
    }
    
    /**
     * Getter for property esquemaLaser.
     * @return Value of property esquemaLaser.
     */
    public java.util.List getEsquemaLaser() {
        return esquemaLaser;
    }
    
    /**
     * Setter for property esquemaLaser.
     * @param esquemaLaser New value of property esquemaLaser.
     */
    public void setEsquemaLaser(java.util.List esquemaLaser) {
        this.esquemaLaser = esquemaLaser;
    }
    
    /**
     * Getter for property nitProveedor.
     * @return Value of property nitProveedor.
     */
    public java.lang.String getNitProveedor() {
        return nitProveedor;
    }
    
    /**
     * Setter for property nitProveedor.
     * @param nitProveedor New value of property nitProveedor.
     */
    public void setNitProveedor(java.lang.String nitProveedor) {
        this.nitProveedor = nitProveedor;
    }
    
    /**
     * Getter for property id_precheque.
     * @return Value of property id_precheque.
     */
    public java.lang.String getId_precheque() {
        return id_precheque;
    }
    
    /**
     * Setter for property id_precheque.
     * @param id_precheque New value of property id_precheque.
     */
    public void setId_precheque(java.lang.String id_precheque) {
        this.id_precheque = id_precheque;
    }
    
     /**
     * M�todos que define esquema de impresion
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String formatEsquema( EsquemaCheque  esquema ){
        String valor = "";
        String campo = esquema.getDescripcion().toUpperCase().trim();
        if(campo.equals("BENEFICIARIO"))
            valor = this.getNom_beneficiario();
        else if(campo.equals("A�O"))
            valor = this.getAno();
        else if(campo.equals("MES")){
            if( esquema.getBanco().toUpperCase().equals("NACIONAL CREDIT")){
                valor = com.tsp.util.Util.NombreMes( Integer.parseInt( this.getMes() ) );
            }
            else{
                valor = this.getMes();
            }
        }

        else if(campo.equals("DIA"))
            valor = this.getDia();
        else if(campo.equals("VALOR"))
            valor =  Util.customFormat( this.getVlrPagar() );
        else if(campo.equals("MONTO")){

            String[] vector   = RMCantidadEnLetras.getTexto(  this.getVlrPagar() , this.getMoneda()   );
            for(int i=0;i<vector.length;i++)
                valor += vector[i] + "<BR>";

        }


        return valor.toUpperCase();
    }
     public String formatEsquemaCMS( EsquemaCheque  esquema){
        String valor = "";
        String campo = esquema.getCampo().toUpperCase().trim();
        if(campo.equals("BENEFICIARIO"))
            valor = this.getNom_beneficiario();
        else if(campo.equals("A�O"))
            valor = this.getAno();
        else if(campo.equals("MES"))
            valor = this.getMes();
        else if(campo.equals("DIA"))
            valor = this.getDia();
        else if(campo.equals("VALOR"))
            valor =  Util.customFormat( this.getVlrPagar() );
        else if(campo.equals("MONTO")){
            
            String[] vector   = RMCantidadEnLetras.getTexto(  this.getVlrPagar() , this.getMoneda()   );
            for(int i=0;i<vector.length;i++)
                valor += vector[i] + "<BR>";
            
        }
        
        
        return valor.toUpperCase();
    }
    
    
}
