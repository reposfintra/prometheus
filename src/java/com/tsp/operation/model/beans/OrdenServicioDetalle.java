/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author Alvaro
 */
public class OrdenServicioDetalle {

  private String reg_status;
  private String dstrct;
  private String tipo_operacion;
  private String numero_operacion;
  private String tipo_documento;
  private String documento;
  private String proveedor;
  private String clase;
  private double valor_calculado;
  private String placa;
  private String last_update;
  private String user_update;
  private String creation_date;
  private String creation_user;
  private String factura_cxc;

  

    /** Creates a new instance of OrdenServicio */
    public OrdenServicioDetalle() {
    }

    public static OrdenServicioDetalle load(java.sql.ResultSet rs)throws java.sql.SQLException{

      OrdenServicioDetalle ordenServicioDetalle = new OrdenServicioDetalle();

      ordenServicioDetalle.setReg_status(rs.getString( "reg_status")) ;
      ordenServicioDetalle.setDstrct(rs.getString( "dstrct")) ;
      ordenServicioDetalle.setTipo_operacion(rs.getString( "tipo_operacion")) ;
      ordenServicioDetalle.setNumero_operacion(rs.getString( "numero_operacion")) ;

      ordenServicioDetalle.setTipo_documento(rs.getString( "tipo_documento")) ;

      ordenServicioDetalle.setDocumento(rs.getString( "documento")) ;

      ordenServicioDetalle.setProveedor(rs.getString( "proveedor")) ;
      ordenServicioDetalle.setClase(rs.getString( "clase")) ;
      ordenServicioDetalle.setValor_calculado(rs.getDouble( "valor_calculado")) ;
      ordenServicioDetalle.setPlaca(rs.getString( "placa")) ;
      ordenServicioDetalle.setLast_update(rs.getString( "last_update")) ;
      ordenServicioDetalle.setUser_update(rs.getString( "user_update")) ;
      ordenServicioDetalle.setCreation_date(rs.getString( "creation_date")) ;
      ordenServicioDetalle.setCreation_user(rs.getString( "creation_user")) ;
      ordenServicioDetalle.setFactura_cxc(rs.getString("factura_cxc"));

      return ordenServicioDetalle;
    }

  
  
  
  
  
    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_operacion
     */
    public String getTipo_operacion() {
        return tipo_operacion;
    }

    /**
     * @param tipo_operacion the tipo_operacion to set
     */
    public void setTipo_operacion(String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    /**
     * @return the numero_operacion
     */
    public String getNumero_operacion() {
        return numero_operacion;
    }

    /**
     * @param numero_operacion the numero_operacion to set
     */
    public void setNumero_operacion(String numero_operacion) {
        this.numero_operacion = numero_operacion;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the proveedor
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the clase
     */
    public String getClase() {
        return clase;
    }

    /**
     * @param clase the clase to set
     */
    public void setClase(String clase) {
        this.clase = clase;
    }

    /**
     * @return the valor_calculado
     */
    public double getValor_calculado() {
        return valor_calculado;
    }

    /**
     * @param valor_calculado the valor_calculado to set
     */
    public void setValor_calculado(double valor_calculado) {
        this.valor_calculado = valor_calculado;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the factura_cxc
     */
    public String getFactura_cxc() {
        return factura_cxc;
    }

    /**
     * @param factura_cxc the factura_cxc to set
     */
    public void setFactura_cxc(String factura_cxc) {
        this.factura_cxc = factura_cxc;
    }


}
