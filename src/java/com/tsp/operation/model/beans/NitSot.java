package com.tsp.operation.model.beans;

import java.io.*;

public class NitSot implements Serializable{
    
    private String estado = "";
    private String cedula = "";
    private String id_mims = "";
    private String nombre = "";
    private String direccion = "";
    private String codciu = "";
    private String coddpto = "";
    private String codpais = "";
    private String telefono = "";
    private String cellular = "";
    private String e_mail = "";
    private String sexo = "";
    private String fechanac = "";
    private String fechaultact = "";
    private String usuario = "";
    private String fechacrea = "";
    private String usuariocrea = "";
    
    private String nombre1;
    private String nombre2;
    private String apellido1;
    private String apellido2;
    private String direccion_oficina;
    private String e_mail2;
 /**
     * Get the value of e_mail2
     *
     * @return the value of e_mail2
     */
    public String getE_mail2() {
        return e_mail2;
    }

    /**
     * Set the value of e_mail2
     *
     * @param e_mail2 new value of e_mail2
     */
    public void setE_mail2(String e_mail2) {
        this.e_mail2 = e_mail2;
    }

    /**
     * Get the value of direccion_oficina
     *
     * @return the value of direccion_oficina
     */
    public String getDireccion_oficina() {
        return direccion_oficina;
    }

    /**
     * Set the value of direccion_oficina
     *
     * @param direccion_oficina new value of direccion_oficina
     */
    public void setDireccion_oficina(String direccion_oficina) {
        this.direccion_oficina = direccion_oficina;
    }

    /**
     * Get the value of apellido2
     *
     * @return the value of apellido2
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * Set the value of apellido2
     *
     * @param apellido2 new value of apellido2
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     * Get the value of apellido1
     *
     * @return the value of apellido1
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * Set the value of apellido1
     *
     * @param apellido1 new value of apellido1
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     * Get the value of nombre2
     *
     * @return the value of nombre2
     */
    public String getNombre2() {
        return nombre2;
    }

    /**
     * Set the value of nombre2
     *
     * @param nombre2 new value of nombre2
     */
    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    /**
     * Get the value of nombre1
     *
     * @return the value of nombre1
     */
    public String getNombre1() {
        return nombre1;
    }

    /**
     * Set the value of nombre1
     *
     * @param nombre1 new value of nombre1
     */
    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }
    
    public NitSot() {
    }
    
    /**
     * Getter for property cedula.
     * @return Value of property cedula.
     */
    public java.lang.String getCedula() {
        return cedula;
    }    
    
    /**
     * Setter for property cedula.
     * @param cedula New value of property cedula.
     */
    public void setCedula(java.lang.String cedula) {
        this.cedula = cedula;
    }
    
    /**
     * Getter for property cellular.
     * @return Value of property cellular.
     */
    public java.lang.String getCellular() {
        return cellular;
    }
    
    /**
     * Setter for property cellular.
     * @param cellular New value of property cellular.
     */
    public void setCellular(java.lang.String cellular) {
        this.cellular = cellular;
    }
    
    /**
     * Getter for property codciu.
     * @return Value of property codciu.
     */
    public java.lang.String getCodciu() {
        return codciu;
    }
    
    /**
     * Setter for property codciu.
     * @param codciu New value of property codciu.
     */
    public void setCodciu(java.lang.String codciu) {
        this.codciu = codciu;
    }
    
    /**
     * Getter for property coddpto.
     * @return Value of property coddpto.
     */
    public java.lang.String getCoddpto() {
        return coddpto;
    }
    
    /**
     * Setter for property coddpto.
     * @param coddpto New value of property coddpto.
     */
    public void setCoddpto(java.lang.String coddpto) {
        this.coddpto = coddpto;
    }
    
    /**
     * Getter for property codpais.
     * @return Value of property codpais.
     */
    public java.lang.String getCodpais() {
        return codpais;
    }
    
    /**
     * Setter for property codpais.
     * @param codpais New value of property codpais.
     */
    public void setCodpais(java.lang.String codpais) {
        this.codpais = codpais;
    }
    
    /**
     * Getter for property direccion.
     * @return Value of property direccion.
     */
    public java.lang.String getDireccion() {
        return direccion;
    }
    
    /**
     * Setter for property direccion.
     * @param direccion New value of property direccion.
     */
    public void setDireccion(java.lang.String direccion) {
        this.direccion = direccion;
    }
    
    /**
     * Getter for property e_mail.
     * @return Value of property e_mail.
     */
    public java.lang.String getE_mail() {
        return e_mail;
    }
    
    /**
     * Setter for property e_mail.
     * @param e_mail New value of property e_mail.
     */
    public void setE_mail(java.lang.String e_mail) {
        this.e_mail = e_mail;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
        return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
        this.estado = estado;
    }
    
    /**
     * Getter for property fechacrea.
     * @return Value of property fechacrea.
     */
    public java.lang.String getFechacrea() {
        return fechacrea;
    }
    
    /**
     * Setter for property fechacrea.
     * @param fechacrea New value of property fechacrea.
     */
    public void setFechacrea(java.lang.String fechacrea) {
        this.fechacrea = fechacrea;
    }
    
    /**
     * Getter for property fechanac.
     * @return Value of property fechanac.
     */
    public java.lang.String getFechanac() {
        return fechanac;
    }
    
    /**
     * Setter for property fechanac.
     * @param fechanac New value of property fechanac.
     */
    public void setFechanac(java.lang.String fechanac) {
        this.fechanac = fechanac;
    }
    
    /**
     * Getter for property fechaultact.
     * @return Value of property fechaultact.
     */
    public java.lang.String getFechaultact() {
        return fechaultact;
    }
    
    /**
     * Setter for property fechaultact.
     * @param fechaultact New value of property fechaultact.
     */
    public void setFechaultact(java.lang.String fechaultact) {
        this.fechaultact = fechaultact;
    }
    
    /**
     * Getter for property id_mims.
     * @return Value of property id_mims.
     */
    public java.lang.String getId_mims() {
        return id_mims;
    }
    
    /**
     * Setter for property id_mims.
     * @param id_mims New value of property id_mims.
     */
    public void setId_mims(java.lang.String id_mims) {
        this.id_mims = id_mims;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
     
    /**
     * Getter for property telefono.
     * @return Value of property telefono.
     */
    public java.lang.String getTelefono() {
        return telefono;
    }
    
    /**
     * Setter for property telefono.
     * @param telefono New value of property telefono.
     */
    public void setTelefono(java.lang.String telefono) {
        this.telefono = telefono;
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property usuariocrea.
     * @return Value of property usuariocrea.
     */
    public java.lang.String getUsuariocrea() {
        return usuariocrea;
    }
    
    /**
     * Setter for property usuariocrea.
     * @param usuariocrea New value of property usuariocrea.
     */
    public void setUsuariocrea(java.lang.String usuariocrea) {
        this.usuariocrea = usuariocrea;
    }
    
    /**
     * Getter for property sexo.
     * @return Value of property sexo.
     */
    public java.lang.String getSexo() {
        return sexo;
    }
    
    /**
     * Setter for property sexo.
     * @param sexo New value of property sexo.
     */
    public void setSexo(java.lang.String sexo) {
        this.sexo = sexo;
    }
    
}
