/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.beans;

/**
 * Bean para el Query negocios relacionados Automotor<br/>
 * 13/03/2015
 * @author mcastillo
 */
public class NegocioAsociacion {
    
    private String negasoc;
    private String cedula;
    private String fecha_negocio;
    private String tipo_neg;
    private double vr_negocio;
    private double valor_saldo;
    private String estado;

    public NegocioAsociacion() {
    }
    /**
     * @return the negasoc
     */
    public String getNegasoc() {
        return negasoc;
    }

    /**
     * @param negasoc the negasoc to set
     */
    public void setNegasoc(String negasoc) {
        this.negasoc = negasoc;
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the fecha_negocio
     */
    public String getFecha_negocio() {
        return fecha_negocio;
    }

    /**
     * @param fecha_negocio the fecha_negocio to set
     */
    public void setFecha_negocio(String fecha_negocio) {
        this.fecha_negocio = fecha_negocio;
    }
    
     /**
     * @return the tipo_neg
     */
    public String getTipo_neg() {
        return tipo_neg;
    }

    /**
     * @param tipo_neg the tipo_neg to set
     */
    public void setTipo_neg(String tipo_neg) {
        this.tipo_neg = tipo_neg;
    }

    /**
     * @return the vr_negocio
     */
    public double getVr_negocio() {
        return vr_negocio;
    }

    /**
     * @param vr_negocio the vr_negocio to set
     */
    public void setVr_negocio(double vr_negocio) {
        this.vr_negocio = vr_negocio;
    }

    /**
     * @return the valor_saldo
     */
    public double getValor_saldo() {
        return valor_saldo;
    }

    /**
     * @param valor_saldo the valor_saldo to set
     */
    public void setValor_saldo(double valor_saldo) {
        this.valor_saldo = valor_saldo;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

   
   
    
}
