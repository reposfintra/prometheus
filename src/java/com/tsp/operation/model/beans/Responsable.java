/*
 * Responsable.java
 *
 * Created on 28 de octubre de 2005, 04:08 PM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  Jose
 */
public class Responsable {
    private String codigo;
    private String descripcion;
    private String usuario_creacion;
    private String fecha_creacion;
    private String usuario_modificacion;
    private String ultima_modificacion;
    private String base;
    private String rec_status;
    /** Creates a new instance of Responsable */
    public Responsable() {
    }

    public java.lang.String getBase() {
        return base;
    }

    public void setBase(java.lang.String base) {
        this.base = base;
    }

    public java.lang.String getCodigo() {
        return codigo;
    }

    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }

    public java.lang.String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }

    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public java.lang.String getRec_status() {
        return rec_status;
    }

    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }

    public java.lang.String getUltima_modificacion() {
        return ultima_modificacion;
    }

    public void setUltima_modificacion(java.lang.String ultima_modificacion) {
        this.ultima_modificacion = ultima_modificacion;
    }

    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }

    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }

    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }
    
}
