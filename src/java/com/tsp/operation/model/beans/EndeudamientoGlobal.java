package com.tsp.operation.model.beans;

import java.sql.Timestamp;

/**
 * <br/>
 * 25/08/2011<br/>
 * @author darrieta - GEOTECH SOLUTIONS S.A.
 */
public class EndeudamientoGlobal {

    private String calificacion;
    private Double saldoPendiente;
    private String tipoCredito;
    private String moneda;
    private Integer numeroCreditos;
    private Timestamp fechaReporte;
    private String entidad;
    private String garantia;
    private String tipoIdentificacion;
    private String identificacion;
    private String creationUser;
    private String userUpdate;
    private int numConsumo;
    private double totalConsumo;
    private int numMicrocredito;
    private double totalMicrocredito;
    private int numComercial;
    private double totalComercial;
    private int numHipotecario;
    private double totalHipotecario;
    private String nitEmpresa;

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    /**
     * Get the value of totalHipotecario
     *
     * @return the value of totalHipotecario
     */
    public double getTotalHipotecario() {
        return totalHipotecario;
    }

    /**
     * Set the value of totalHipotecario
     *
     * @param totalHipotecario new value of totalHipotecario
     */
    public void setTotalHipotecario(double totalHipotecario) {
        this.totalHipotecario = totalHipotecario;
    }

    /**
     * Get the value of numHipotecario
     *
     * @return the value of numHipotecario
     */
    public int getNumHipotecario() {
        return numHipotecario;
    }

    /**
     * Set the value of numHipotecario
     *
     * @param numHipotecario new value of numHipotecario
     */
    public void setNumHipotecario(int numHipotecario) {
        this.numHipotecario = numHipotecario;
    }

    /**
     * Get the value of totalComercial
     *
     * @return the value of totalComercial
     */
    public double getTotalComercial() {
        return totalComercial;
    }

    /**
     * Set the value of totalComercial
     *
     * @param totalComercial new value of totalComercial
     */
    public void setTotalComercial(double totalComercial) {
        this.totalComercial = totalComercial;
    }

    /**
     * Get the value of numComercial
     *
     * @return the value of numComercial
     */
    public int getNumComercial() {
        return numComercial;
    }

    /**
     * Set the value of numComercial
     *
     * @param numComercial new value of numComercial
     */
    public void setNumComercial(int numComercial) {
        this.numComercial = numComercial;
    }

    /**
     * Get the value of totalMicrocredito
     *
     * @return the value of totalMicrocredito
     */
    public double getTotalMicrocredito() {
        return totalMicrocredito;
    }

    /**
     * Set the value of totalMicrocredito
     *
     * @param totalMicrocredito new value of totalMicrocredito
     */
    public void setTotalMicrocredito(double totalMicrocredito) {
        this.totalMicrocredito = totalMicrocredito;
    }

    /**
     * Get the value of numMicrocredito
     *
     * @return the value of numMicrocredito
     */
    public int getNumMicrocredito() {
        return numMicrocredito;
    }

    /**
     * Set the value of numMicrocredito
     *
     * @param numMicrocredito new value of numMicrocredito
     */
    public void setNumMicrocredito(int numMicrocredito) {
        this.numMicrocredito = numMicrocredito;
    }

    /**
     * Get the value of totalConsumo
     *
     * @return the value of totalConsumo
     */
    public double getTotalConsumo() {
        return totalConsumo;
    }

    /**
     * Set the value of totalConsumo
     *
     * @param totalConsumo new value of totalConsumo
     */
    public void setTotalConsumo(double totalConsumo) {
        this.totalConsumo = totalConsumo;
    }

    /**
     * Get the value of numConsumo
     *
     * @return the value of numConsumo
     */
    public int getNumConsumo() {
        return numConsumo;
    }

    /**
     * Set the value of numConsumo
     *
     * @param numConsumo new value of numConsumo
     */
    public void setNumConsumo(int numConsumo) {
        this.numConsumo = numConsumo;
    }


    /**
     * Get the value of userUpdate
     *
     * @return the value of userUpdate
     */
    public String getUserUpdate() {
        return userUpdate;
    }

    /**
     * Set the value of userUpdate
     *
     * @param userUpdate new value of userUpdate
     */
    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    /**
     * Get the value of creationUser
     *
     * @return the value of creationUser
     */
    public String getCreationUser() {
        return creationUser;
    }

    /**
     * Set the value of creationUser
     *
     * @param creationUser new value of creationUser
     */
    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    /**
     * Get the value of identificacion
     *
     * @return the value of identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Set the value of identificacion
     *
     * @param identificacion new value of identificacion
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Get the value of tipoIdentificacion
     *
     * @return the value of tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * Set the value of tipoIdentificacion
     *
     * @param tipoIdentificacion new value of tipoIdentificacion
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * Get the value of garantia
     *
     * @return the value of garantia
     */
    public String getGarantia() {
        return garantia;
    }

    /**
     * Set the value of garantia
     *
     * @param garantia new value of garantia
     */
    public void setGarantia(String garantia) {
        this.garantia = garantia;
    }

    /**
     * Get the value of entidad
     *
     * @return the value of entidad
     */
    public String getEntidad() {
        return entidad;
    }

    /**
     * Set the value of entidad
     *
     * @param entidad new value of entidad
     */
    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    /**
     * Get the value of fechaReporte
     *
     * @return the value of fechaReporte
     */
    public Timestamp getFechaReporte() {
        return fechaReporte;
    }

    /**
     * Set the value of fechaReporte
     *
     * @param fechaReporte new value of fechaReporte
     */
    public void setFechaReporte(Timestamp fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    /**
     * Get the value of numeroCreditos
     *
     * @return the value of numeroCreditos
     */
    public Integer getNumeroCreditos() {
        return numeroCreditos;
    }

    /**
     * Set the value of numeroCreditos
     *
     * @param numeroCreditos new value of numeroCreditos
     */
    public void setNumeroCreditos(int numeroCreditos) {
        this.numeroCreditos = numeroCreditos;
    }

    /**
     * Get the value of moneda
     *
     * @return the value of moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * Set the value of moneda
     *
     * @param moneda new value of moneda
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * Get the value of tipoCredito
     *
     * @return the value of tipoCredito
     */
    public String getTipoCredito() {
        return tipoCredito;
    }

    /**
     * Set the value of tipoCredito
     *
     * @param tipoCredito new value of tipoCredito
     */
    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    /**
     * Get the value of saldoPendiente
     *
     * @return the value of saldoPendiente
     */
    public Double getSaldoPendiente() {
        return saldoPendiente;
    }

    /**
     * Set the value of saldoPendiente
     *
     * @param saldoPendiente new value of saldoPendiente
     */
    public void setSaldoPendiente(double saldoPendiente) {
        this.saldoPendiente = saldoPendiente;
    }


    /**
     * Get the value of calificacion
     *
     * @return the value of calificacion
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     * Set the value of calificacion
     *
     * @param calificacion new value of calificacion
     */
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

}
