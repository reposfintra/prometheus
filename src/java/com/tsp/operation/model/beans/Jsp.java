/*
 * Jsp.java
 *
 * Created on 10 de julio de 2005, 12:01
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  DRIGO
 */

public class Jsp implements Serializable{
    
    private String codigo;
    private String nombre;
    private String ruta;
    private String descripcion;
    private String cia;
    private String rec_status;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    
    public static Jsp load(ResultSet rs)throws SQLException{
        Jsp t = new Jsp();
        t.setNombre(rs.getString("codigo"));
        t.setNombre(rs.getString("nombre"));
        t.setDescripcion(rs.getString("descripcion"));
        t.setDescripcion(rs.getString("ruta"));
        t.setCia(rs.getString("cia"));
        t.setRec_status(rs.getString("rec_status"));
        t.setLast_update(rs.getDate("last_update"));
        t.setUser_update(rs.getString("user_update"));
        t.setCreation_date(rs.getDate("creation_date"));
        t.setCreation_user(rs.getString("creation_user"));
        return t;
    }
    
    public void loadCampos(ResultSet rs) throws SQLException{
        this.setNombre(rs.getString("codigo"));
        this.setNombre(rs.getString("nombre"));
        this.setDescripcion(rs.getString("descripcion"));
        this.setDescripcion(rs.getString("ruta"));
        this.setCia(rs.getString("cia"));
        this.setRec_status(rs.getString("rec_status"));
        this.setLast_update(rs.getDate("last_update"));
        this.setUser_update(rs.getString("user_update"));
        this.setCreation_date(rs.getDate("creation_date"));
        this.setCreation_user(rs.getString("creation_user"));
    }

    public java.lang.String getCia() {
        return cia;
    }
 
    public void setCia(java.lang.String cia) {
        this.cia = cia;
    }

    public java.lang.String getCodigo() {
        return codigo;
    }
    public java.lang.String getNombre() {
        return nombre;
    }

    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    public java.lang.String getRuta() {
        return ruta;
    }

    public void setRuta(java.lang.String ruta) {
        this.ruta = ruta;
    }
    
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }

    public java.util.Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(java.util.Date creation_date) {
        this.creation_date = creation_date;
    }

    public java.lang.String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }

    public java.lang.String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }

    public java.util.Date getLast_update() {
        return last_update;
    }

    public void setLast_update(java.util.Date last_update) {
        this.last_update = last_update;
    }

    public java.lang.String getRec_status() {
        return rec_status;
    }

    public void setRec_status(java.lang.String rec_status) {
        this.rec_status = rec_status;
    }

    public java.lang.String getUser_update() {
        return user_update;
    }

    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
}
