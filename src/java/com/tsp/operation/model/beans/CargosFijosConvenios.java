/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class CargosFijosConvenios {
    
   private String  id_convenio ;
   private String  nombre_concepto;
   private String  descripcion ;
   private String  prefijo;
   private String  cuenta_diferido;
   private String  hc_diferido ;
   private String  cuenta ;
   private String  tipo_calculo ;
   private double  valor ;
   private boolean activo;
   private String aplica_iva;
   private String esdiferido;
   

    public CargosFijosConvenios() {
    }

    /**
     * @return the id_convenio
     */
    public String getId_convenio() {
        return id_convenio;
    }

    /**
     * @param id_convenio the id_convenio to set
     */
    public void setId_convenio(String id_convenio) {
        this.id_convenio = id_convenio;
    }

    /**
     * @return the nombre_concepto
     */
    public String getNombre_concepto() {
        return nombre_concepto;
    }

    /**
     * @param nombre_concepto the nombre_concepto to set
     */
    public void setNombre_concepto(String nombre_concepto) {
        this.nombre_concepto = nombre_concepto;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the prefijo
     */
    public String getPrefijo() {
        return prefijo;
    }

    /**
     * @param prefijo the prefijo to set
     */
    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    /**
     * @return the cuenta_diferido
     */
    public String getCuenta_diferido() {
        return cuenta_diferido;
    }

    /**
     * @param cuenta_diferido the cuenta_diferido to set
     */
    public void setCuenta_diferido(String cuenta_diferido) {
        this.cuenta_diferido = cuenta_diferido;
    }

    /**
     * @return the hc_diferido
     */
    public String getHc_diferido() {
        return hc_diferido;
    }

    /**
     * @param hc_diferido the hc_diferido to set
     */
    public void setHc_diferido(String hc_diferido) {
        this.hc_diferido = hc_diferido;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the tipo_calculo
     */
    public String getTipo_calculo() {
        return tipo_calculo;
    }

    /**
     * @param tipo_calculo the tipo_calculo to set
     */
    public void setTipo_calculo(String tipo_calculo) {
        this.tipo_calculo = tipo_calculo;
    }

    /**
     * @return the valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * @return the activo
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * @return the aplica_iva
     */
    public String getAplica_iva() {
        return aplica_iva;
    }

    /**
     * @param aplica_iva the aplica_iva to set
     */
    public void setAplica_iva(String aplica_iva) {
        this.aplica_iva = aplica_iva;
    }

    public String getEsdiferido() {
        return esdiferido;
    }

    public void setEsdiferido(String esdiferido) {
        this.esdiferido = esdiferido;
    }
    
    
   
}
