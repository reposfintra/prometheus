/*
 * InfoPlanilla.java
 *
 * Created on 23 de agosto de 2005, 08:19 AM
 */

package com.tsp.operation.model.beans;

/**
 *
 * @author  mfontalvo
 */
public class InfoPlanilla {
    private String planilla, vehiculo, fecha, cliente, nombreConductor, cedulaConductor;
    private String origen, destino;
    private String propietario;
    private String remesa;
    
    /** Creates a new instance of InfoPlanilla */
    public InfoPlanilla() {
    }
    
    /**
     * Getter for property cedulaConductor.
     * @return Value of property cedulaConductor.
     */
    public java.lang.String getCedulaConductor() {
        return cedulaConductor;
    }
    
    /**
     * Setter for property cedulaConductor.
     * @param cedulaConductor New value of property cedulaConductor.
     */
    public void setCedulaConductor(java.lang.String cedulaConductor) {
        this.cedulaConductor = cedulaConductor;
    }
    
    /**
     * Getter for property cliente.
     * @return Value of property cliente.
     */
    public java.lang.String getCliente() {
        return cliente;
    }
    
    /**
     * Setter for property cliente.
     * @param cliente New value of property cliente.
     */
    public void setCliente(java.lang.String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Getter for property destino.
     * @return Value of property destino.
     */
    public java.lang.String getDestino() {
        return destino;
    }
    
    /**
     * Setter for property destino.
     * @param destino New value of property destino.
     */
    public void setDestino(java.lang.String destino) {
        this.destino = destino;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property nombreConductor.
     * @return Value of property nombreConductor.
     */
    public java.lang.String getNombreConductor() {
        return nombreConductor;
    }
    
    /**
     * Setter for property nombreConductor.
     * @param nombreConductor New value of property nombreConductor.
     */
    public void setNombreConductor(java.lang.String nombreConductor) {
        this.nombreConductor = nombreConductor;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property vehiculo.
     * @return Value of property vehiculo.
     */
    public java.lang.String getVehiculo() {
        return vehiculo;
    }
    
    /**
     * Setter for property vehiculo.
     * @param vehiculo New value of property vehiculo.
     */
    public void setVehiculo(java.lang.String vehiculo) {
        this.vehiculo = vehiculo;
    }
    /**
     * Getter for property propietario.
     * @return Value of property propietario.
     */
    public java.lang.String getPropietario() {
        return propietario;
    }
    
    /**
     * Setter for property propietario.
     * @param propietario New value of property propietario.
     */
    public void setPropietario(java.lang.String propietario) {
        this.propietario = propietario;
    }
    
    /**
     * Getter for property remesa.
     * @return Value of property remesa.
     */
    public java.lang.String getRemesa() {
        return remesa;
    }
    
    /**
     * Setter for property remesa.
     * @param remesa New value of property remesa.
     */
    public void setRemesa(java.lang.String remesa) {
        this.remesa = remesa;
    }
    
}
