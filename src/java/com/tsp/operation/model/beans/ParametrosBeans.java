/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.beans;

/**
 *
 * @author egonzalez
 */
public class ParametrosBeans {
  
    private String stringParameter1;
    private String stringParameter2;
    private String stringParameter3;
    private String stringParameter4;
    private String stringParameter5;
    private String stringParameter6;
    private String stringParameter7;
    private String stringParameter8;
    private String stringParameter9;
    private String stringParameter10;
    private String stringParameter11;
    
    public ParametrosBeans() {
    }

    public ParametrosBeans(String stringParameter1, String stringParameter2, String stringParameter3, String stringParameter4, String stringParameter5, String stringParameter6, String stringParameter7, String stringParameter8, String stringParameter9, String stringParameter10, String stringParameter11) {
        this.stringParameter1 = stringParameter1;
        this.stringParameter2 = stringParameter2;
        this.stringParameter3 = stringParameter3;
        this.stringParameter4 = stringParameter4;
        this.stringParameter5 = stringParameter5;
        this.stringParameter6 = stringParameter6;
        this.stringParameter7 = stringParameter7;
        this.stringParameter8 = stringParameter8;
        this.stringParameter9 = stringParameter9;
        this.stringParameter10 = stringParameter10;
        this.stringParameter11 = stringParameter11;
    }

    /**
     * @return the stringParameter1
     */
    public String getStringParameter1() {
        return stringParameter1;
    }

    /**
     * @param stringParameter1 the stringParameter1 to set
     */
    public void setStringParameter1(String stringParameter1) {
        this.stringParameter1 = stringParameter1;
    }

    /**
     * @return the stringParameter2
     */
    public String getStringParameter2() {
        return stringParameter2;
    }

    /**
     * @param stringParameter2 the stringParameter2 to set
     */
    public void setStringParameter2(String stringParameter2) {
        this.stringParameter2 = stringParameter2;
    }

    /**
     * @return the stringParameter3
     */
    public String getStringParameter3() {
        return stringParameter3;
    }

    /**
     * @param stringParameter3 the stringParameter3 to set
     */
    public void setStringParameter3(String stringParameter3) {
        this.stringParameter3 = stringParameter3;
    }

    /**
     * @return the stringParameter4
     */
    public String getStringParameter4() {
        return stringParameter4;
    }

    /**
     * @param stringParameter4 the stringParameter4 to set
     */
    public void setStringParameter4(String stringParameter4) {
        this.stringParameter4 = stringParameter4;
    }

    /**
     * @return the stringParameter5
     */
    public String getStringParameter5() {
        return stringParameter5;
    }

    /**
     * @param stringParameter5 the stringParameter5 to set
     */
    public void setStringParameter5(String stringParameter5) {
        this.stringParameter5 = stringParameter5;
    }

    /**
     * @return the stringParameter6
     */
    public String getStringParameter6() {
        return stringParameter6;
    }

    /**
     * @param stringParameter6 the stringParameter6 to set
     */
    public void setStringParameter6(String stringParameter6) {
        this.stringParameter6 = stringParameter6;
    }

    /**
     * @return the stringParameter7
     */
    public String getStringParameter7() {
        return stringParameter7;
    }

    /**
     * @param stringParameter7 the stringParameter7 to set
     */
    public void setStringParameter7(String stringParameter7) {
        this.stringParameter7 = stringParameter7;
    }

    /**
     * @return the stringParameter8
     */
    public String getStringParameter8() {
        return stringParameter8;
    }

    /**
     * @param stringParameter8 the stringParameter8 to set
     */
    public void setStringParameter8(String stringParameter8) {
        this.stringParameter8 = stringParameter8;
    }

    /**
     * @return the stringParameter9
     */
    public String getStringParameter9() {
        return stringParameter9;
    }

    /**
     * @param stringParameter9 the stringParameter9 to set
     */
    public void setStringParameter9(String stringParameter9) {
        this.stringParameter9 = stringParameter9;
    }

    /**
     * @return the stringParameter10
     */
    public String getStringParameter10() {
        return stringParameter10;
    }

    /**
     * @param stringParameter10 the stringParameter10 to set
     */
    public void setStringParameter10(String stringParameter10) {
        this.stringParameter10 = stringParameter10;
    }

    public String getStringParameter11() {
        return stringParameter11;
    }

    public void setStringParameter11(String stringParameter11) {
        this.stringParameter11 = stringParameter11;
    }
    
    

}

