/*
 * DatosDescuentosImp.java
 *
 * Created on 18 de diciembre de 2004, 7:26
 */

package com.tsp.operation.model.beans;
import java.io.*;
import java.sql.*;

/**
 *
 * @author  Mario Fontalvo
 */
public class DatosDescuentosImp implements Serializable{
    
    /** Creates a new instance of DatosDescuentosImp */
    private String Codigo;   
    private String Concepto;    
    private String Valor;
    private String Moneda;
    private String Tipo;
    
    //Codigo Migracion Proveedor
    private String MigracionConcepto;
    private String MigracionProveedor;
    private String Valor2;
    private String creacion_movpla ="";
    
    private String documento;
    
    public DatosDescuentosImp() {
    }
    
    public static DatosDescuentosImp load(ResultSet rs) throws Exception{
        DatosDescuentosImp datos = new DatosDescuentosImp();
        datos.setCodigo  (rs.getString(1));
        datos.setConcepto(rs.getString(2));
        datos.setValor   (rs.getString(3));
        datos.setMoneda  (rs.getString(4));
        datos.setTipo    (rs.getString(5));
        return datos;
    }
    
    public static DatosDescuentosImp load2(ResultSet rs) throws Exception{
        DatosDescuentosImp datos = new DatosDescuentosImp();
        datos.setCodigo           (rs.getString(1));
        datos.setConceptoMigracion(rs.getString(2));
        datos.setValor            (rs.getString(3));
        datos.setValor2           (rs.getString(4));
        datos.setMoneda           (rs.getString(5));
        return datos;
    }    
    //setter
    public void setCodigo (String valor){
        this.Codigo = valor;        
    }
    public void setConcepto (String valor){
        this.Concepto = valor;        
    }
    public void setValor (String valor){
        this.Valor = valor;        
    }
    public void setValor2 (String valor){
        this.Valor2 = valor;        
    }
    public void setMoneda (String valor){
        this.Moneda = valor;        
    }
    public void setTipo (String valor){
        this.Tipo = valor;        
    }
    ///////////////////////////////////
    public void setProveedorMigracion (String valor){
        this.MigracionProveedor = valor;        
    }
    public void setConceptoMigracion (String valor){
        this.MigracionConcepto = valor;        
    }
    
    //getter
    public String getCodigo (){
        return this.Codigo ;
    }
    public String getConcepto (){
        return this.Concepto ;
    }
    public String getValor (){
        return this.Valor ;
    }
    public String getValor2 (){
        return this.Valor2 ;
    }    
    public String getMoneda (){
        return this.Moneda ;
    }
    public String getTipo (){
        return this.Tipo ;
    }
    //////////////////////////////////////
    public String getProveedorMigracion (){
        return this.MigracionProveedor;        
    }
    public String getConceptoMigracion (){
        return this.MigracionConcepto;        
    }
    
    /**
     * Getter for property creacion_movpla.
     * @return Value of property creacion_movpla.
     */
    public java.lang.String getCreacion_movpla() {
        return creacion_movpla;
    }
    
    /**
     * Setter for property creacion_movpla.
     * @param creacion_movpla New value of property creacion_movpla.
     */
    public void setCreacion_movpla(java.lang.String creacion_movpla) {
        this.creacion_movpla = creacion_movpla;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
}
