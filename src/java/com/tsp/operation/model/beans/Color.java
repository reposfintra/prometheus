/*
 * Color.java
 *
 * Created on 9 de noviembre de 2004, 01:39 PM
 */

package com.tsp.operation.model.beans;

import java.io.Serializable;
/**
 *
 * @author  AMENDEZ
 */
public class Color implements Serializable{
    
    String codigo;
    String descripcion;
    /** Creates a new instance of Color */
    public Color() {
    }
    
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    
    public String getCodigo(){
        return this.codigo;
    }
    
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    
    public String getDescripcion(){
        return this.descripcion;
    }
    
}
