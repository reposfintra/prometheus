/*
 * Moneda.java
 *
 * Created on 29 de diciembre de 2004, 09:43 AM
 */

package com.tsp.operation.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
/**
 *
 * @author  KREALES
 */
public class Moneda {
    
    private String change_date;
    private String local_currency;
    private String for_currency;
    private float rate;
    private float cambio;
    
    //henry
    private String codmoneda;
    private String nommoneda;
    
    //Diogenes
    private String inicial;
    private String estado;
    private java.util.Date last_update;
    private String user_update;
    private java.util.Date creation_date;
    private String creation_user;
    
    /** Creates a new instance of Moneda */
    public static Moneda load(ResultSet rs)throws SQLException {
        Moneda m = new Moneda();
        m.setChange_date(rs.getString("change_date"));
        m.setFor_currency(rs.getString("for_currency"));
        m.setLocal_currency(rs.getString("local_currency"));
        m.setRate(rs.getFloat("rate"));
        
        return m;
    }
    
    public String getChange_date(){
        return this.change_date;
    }
    public void setChange_date(String change_date){
        this.change_date=change_date;
    }
    
    public String getLocal_currency(){
        return this.local_currency;
    }
    public void setLocal_currency(String local_currency){
        this.local_currency=local_currency;
    }
    
    public String getFor_currency(){
        return this.for_currency;
    }
    public void setFor_currency(String for_currency){
        this.for_currency=for_currency;
    }
    
    public float getRate(){
        return this.rate;
    }
    public void setRate(float rate){
        this.rate = rate;
    }
    
    public float getCambio(){
        return this.cambio;
    }
    public void setCambio(float cambio){
        this.cambio = cambio;
    }
    public void setCodMoneda(String cod){
        this.codmoneda = cod;
    }    
    public String getNomMoneda(){
        return this.nommoneda;
    }
    public void setNomMoneda(String nom){
        this.nommoneda = nom;
    }    
    public String getCodMoneda(){
        return this.codmoneda;
    }
    
    //Diogenes
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.util.Date getLast_update() {
            return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.util.Date last_update) {
            this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
            return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
            this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.util.Date getCreation_date() {
            return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.util.Date creation_date) {
            this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
            return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
            this.creation_user = creation_user;
    }
    
    /**
     * Getter for property inicial.
     * @return Value of property inicial.
     */
    public java.lang.String getInicial() {
            return inicial;
    }
    
    /**
     * Setter for property inicial.
     * @param inicial New value of property inicial.
     */
    public void setInicial(java.lang.String inicial) {
            this.inicial = inicial;
    }
    
    /**
     * Getter for property estado.
     * @return Value of property estado.
     */
    public java.lang.String getEstado() {
            return estado;
    }
    
    /**
     * Setter for property estado.
     * @param estado New value of property estado.
     */
    public void setEstado(java.lang.String estado) {
            this.estado = estado;
    }
}
