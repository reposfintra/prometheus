/*
 *  Nombre clase    :  BalancePrueba.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 20 de junio de 2006, 11:43 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.model.beans;

import java.util.*;
import java.sql.*;

public class BPElementosGasto extends BPCuentas{
      
    private List UnidadesNegocio;               
    
    /** Crea una nueva instancia de  BalancePrueba */
    public BPElementosGasto() {
        UnidadesNegocio = new LinkedList();
    }
    
    
    
    public void addUnidades(BPUnidadesNegocio u){
        UnidadesNegocio.add(u);
    }
    
    public void Calcular( int mes ){
        
        if (UnidadesNegocio!=null && UnidadesNegocio.size()>0){
            for (int i = 0; i < UnidadesNegocio.size();i++){
                BPUnidadesNegocio bpu = (BPUnidadesNegocio) UnidadesNegocio.get(i);
                this.setVMovCredito( mes,   bpu.getVMovCredito(mes));
                this.setVMovDebito(  mes ,  bpu.getVMovDebito(mes));
            }
            CalcularSaldos(mes);
        }
        
    }
    
    /**
     * Getter for property UnidadesNegocio.
     * @return Value of property UnidadesNegocio.
     */
    public java.util.List getUnidadesNegocio() {
        return UnidadesNegocio;
    }
    
    /**
     * Setter for property UnidadesNegocio.
     * @param UnidadesNegocio New value of property UnidadesNegocio.
     */
    public void setUnidadesNegocio(java.util.List UnidadesNegocio) {
        this.UnidadesNegocio = UnidadesNegocio;
    }
    
}
