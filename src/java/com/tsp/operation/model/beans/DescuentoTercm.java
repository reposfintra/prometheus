/**************************************************************************
 * Nombre clase: DescuentoTercm.java                                      *
 * Descripci�n: Clase que maneja los get y los set                        *
 * Autor: Ing. Ivan Dario Gomez Vanegas                                   *
 * Fecha: Created on 23 de diciembre de 2005, 08:20 AM                    *
 * Versi�n: Java 1.0                                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 *************************************************************************/


package com.tsp.operation.model.beans;

/**
 *
 * @author  Ivan Dario Gomez
 */
public class DescuentoTercm {
    private String OC;
    private String Boleta;
    private String Fecha;
    private String Placa;
    private String ValorPost;
    private int    Cedula;
    private int    Valor;
    private String ValorDisp;
    private String Nombre;
    private String Prefactura;
    /** Creates a new instance of DescuentoTercm */
    public DescuentoTercm() {
        OC         = " ";
        Boleta     = " ";
        Fecha      = " ";
        Placa      = " ";
        ValorPost  = " ";
        Cedula     = 0;
        Valor      = 0;
        ValorDisp  = " ";
        Nombre     = " ";
        Prefactura = " ";
        
    }
    /******************** Declaracion de SET************************************/
    public void setOC        (String newOC        )  {  this.OC         = newOC;            }
    public void setBoleta    (String newBoleta    )  {  this.Boleta     = newBoleta;        }
    public void setFecha     (String newFecha     )  {  this.Fecha      = newFecha;         }
    public void setPlaca     (String newPlaca     )  {  this.Placa      = newPlaca;         }
    public void setValorPost (String newValorPost )  {  this.ValorPost  = newValorPost;     }
    public void setCedula    (int newCedula       )  {  this.Cedula     = newCedula;        }
    public void setValor     (int newValor        )  {  this.Valor      = newValor;         }
    public void setValorDisp (String newValorDisp )  {  this.ValorDisp  = newValorDisp;     }
    public void setNombre    (String newNombre    )  {  this.Nombre     = newNombre;        }
    public void setPrefactura(String newPrefactura)  {  this.Prefactura = newPrefactura;    }
    
    /******************** Declaracion de GET************************************/
    public String getOC        ()  {  return this.OC;         }
    public String getBoleta    ()  {  return this.Boleta;     }
    public String getFecha     ()  {  return this.Fecha;      }
    public String getPlaca     ()  {  return this.Placa;      }
    public String getValorPost ()  {  return this.ValorPost;  }
    public int    getCedula    ()  {  return this.Cedula;     }
    public int    getValor     ()  {  return this.Valor;      }
    public String getValorDisp ()  {  return this.ValorDisp;  }
    public String getNombre    ()  {  return this.Nombre;     }
    public String getPrefactura()  {  return this.Prefactura; }
}
