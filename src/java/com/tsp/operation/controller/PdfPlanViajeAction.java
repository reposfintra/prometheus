
/*****************************************************************************
 * Nombre clase :                   PdfPlanViajeAction.java              *
 * Descripcion :                    Clase que maneja los eventos             *
 *                                  relacionados con el programa de          *
 *                                  Impresion de plan de Viaje    *
 * Autor :                          Ing. ivan dario gomez          *
 * Fecha :                          16 de marzo de 2006, 01:30 PM            *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.operation.controller;


import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.pdf.*;
import java.io.*;

public class PdfPlanViajeAction extends Action{
    
    /** Creates a new instance of HojaControlViajeAction */
    public PdfPlanViajeAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
           // HttpSession session =    request.getSession();
            PlanDeViaje planViaje        = model.PlanViajeSvc.getPlan();
            
            String ruta = application.getRealPath("/");
            File xslt = new File(ruta + "Templates/planViaje.xsl");
            File pdf = new File(ruta + "pdf/pv.pdf");
            
            HojaPlanViajePDF Plan_viajes  = new HojaPlanViajePDF(); 
            Plan_viajes.generarPDF(xslt, pdf, planViaje);
            
            String next = "/pdf/pv.pdf";
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en PLAN VIAJES PDF .....\n"+e.getMessage());
        }
    }
    
}

