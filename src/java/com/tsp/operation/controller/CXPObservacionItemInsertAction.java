/***********************************************************************************
 * Nombre clase : ............... CXPObservacionItemInsertAction.java                            
 * Descripcion :................. Clase que maneja Las Acciones de las Observaciones
 * Autor :....................... Ing. Henry A.Osorio González                     
 * Fecha :.......................  Created on 10 de octubre de 2005, 11:17 AM                 
 * Version :..................... 1.0                                              
 * Copyright :................... Fintravalores S.A.                          
 ***********************************************************************************/



package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  hosorio
 */
public class CXPObservacionItemInsertAction extends Action{
    
    /** Creates a new instance of CXPBuscarFacturas */
    public CXPObservacionItemInsertAction() {
    }
     public void run() throws javax.servlet.ServletException, InformationException {
        String next="/jsp/cxpagar/facturas/obsAutorizador.jsp?msg=";
        HttpSession session = request.getSession();
        String aprobador = (request.getParameter("aprobador")!=null)?request.getParameter("aprobador"):"";
        String pagador = (request.getParameter("pagador")!=null)?request.getParameter("pagador"):"";
        String registrador = (request.getParameter("registrador")!=null)?request.getParameter("registrador"):"";
        String opc = (request.getParameter("opc")!=null)?request.getParameter("opc"):"";
        CXPItemDoc items = (CXPItemDoc) session.getAttribute("item");
        CXPObservacionItem observacion = new CXPObservacionItem(); 
        String usu1 ="",usu2="";
        if(!pagador.equals(""))
             usu1 = "PAGADOR";            
         else if(!registrador.equals(""))
             usu2 = "REGISTRADOR";
        try{
            String TextoActivo = model.cxpObservacionItemService.obtenerTextoActivo(items); 
            Usuario usuario = (Usuario) session.getAttribute("Usuario");                 
            observacion.setDstrct(items.getDstrct());
            observacion.setProveedor(items.getProveedor());
            observacion.setTipo_documento(items.getTipo_documento());
            observacion.setDocumento(items.getDocumento());
            observacion.setItem(items.getItem());
            observacion.setObservacion_autorizador(aprobador);
            observacion.setFecha_ob_autorizador(Util.getFechaActual_String(6));
            observacion.setUsuario_ob_autorizador(usuario.getLogin());     
            observacion.setTextoactivo (items.getTextoactivo ());
            observacion.setObservacion_pagador(pagador);
            if(opc.equals("INSERT")){ 
                if(TextoActivo.equals("")){
                    observacion.setCreation_user(usuario.getLogin());
                    model.cxpObservacionItemService.insertarObservacionItem(observacion,usuario.getLogin ()); 
                    model.cxpDocService.sumarNumeroObsAutorizador(items);
                    next+="Observación agregada exitosamente&TextActivo=R";
                    model.cxpObservacionItemService.buscarObservacionItem(items);
                    session.setAttribute("observacion",model.cxpObservacionItemService.getCXPObservacionItem());
                }else{
                    model.cxpObservacionItemService.updateObservacion(observacion,usuario.getLogin ());
                    next+="Observacion modificada exitosamente&TextActivo=R";
                    //System.out.println("Texto: "+observacion.getTextoactivo());
                    if(!TextoActivo.equals("R")){
                        model.cxpDocService.sumarNumeroObsAutorizador(items);
                    }
                    model.cxpObservacionItemService.buscarObservacionItem(items);
                    session.setAttribute("observacion",model.cxpObservacionItemService.getCXPObservacionItem());
                }
               
            }else if(opc.equals("UPDATE")){
                model.cxpObservacionItemService.updateObservacion(observacion,usuario.getLogin ());
                next+="Observacion modificada exitosamente&TextActivo=R";
                //System.out.println("Texto: "+observacion.getTextoactivo());
                if(!TextoActivo.equals("R")){
                    model.cxpDocService.sumarNumeroObsAutorizador(items);
                }
                model.cxpObservacionItemService.buscarObservacionItem(items);
                session.setAttribute("observacion",model.cxpObservacionItemService.getCXPObservacionItem());

                
            }else if(opc.equals("CIERRE")){
                observacion.setCierre_observacion(Util.getFechaActual_String(6));               
                model.cxpObservacionItemService.cerrarObservcacion(observacion);                
                next+="Cierre de observaciones exitoso";
                
                
            }else if(opc.equals("UPDATEPAGADOR")){
                model.cxpObservacionItemService.updateObservacionPagador(observacion, usuario.getLogin());
                next="/jsp/cxpagar/facturas/RespObsPagador.jsp?msg=Observacion modificada exitosamente";
                if(TextoActivo.equals("R")){
                    model.cxpDocService.sumarNumeroObsPagadorRegistrador(items);
                }
                model.cxpObservacionItemService.buscarObservacionItem(items);
                session.setAttribute("observacion",model.cxpObservacionItemService.getCXPObservacionItem());
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }         
         // Redireccionar a la página indicada.
        this.dispatchRequest(next);
    }
    
}
