/*
 * StdjobplkcostoDelete.java
 *
 * Created on 9 de marzo de 2005, 05:35 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class StdjobplkcostoDeleteAction extends Action{
    
    /** Creates a new instance of StdjobplkcostoDelete */
    public StdjobplkcostoDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next                 =   "/tarifas_diferenciales/sjgrupDelete.jsp";
        String grupo                =   request.getParameter("grupo").toUpperCase();
                
        try{
            
            if(request.getParameter("buscar")!=null){
                
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    request.setAttribute("std", model.stdjobplkcostoService.getStd());
                }
                
            }
            else{
                String dstrct               =   request.getParameter("distrito").toUpperCase();
                String activacion           =   request.getParameter("activacion");
                
                HttpSession session         =   request.getSession();
                Usuario usuario             =   (Usuario) session.getAttribute("Usuario");
                String user_update          =   usuario.getLogin();
                
                request.setAttribute("error", "#99cc99");
                Stdjobplkcosto stdjobplkcosto = new Stdjobplkcosto();
                stdjobplkcosto.setDstrct(dstrct);
                stdjobplkcosto.setGroup_code(grupo);
                stdjobplkcosto.setActivation_date(activacion);
                stdjobplkcosto.setCreation_user(usuario.getLogin());
                
                
                model.stdjobplkcostoService.setStd(stdjobplkcosto);
                model.stdjobplkcostoService.delete();
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
