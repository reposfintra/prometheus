/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class CaravanaExistPlanViajeAddAction extends Action {
  static Logger logger = Logger.getLogger(CaravanaPlanViajeAddAction.class);
  
  /**
   * Executes the action
   */
  public void run() throws ServletException, InformationException {
      String next = "";
      String retorno = null;
    
      HttpSession session = request.getSession();
      Usuario usuario = (Usuario)session.getAttribute("Usuario");
      Calendar fecha = Calendar.getInstance();
      SimpleDateFormat FFormat = new SimpleDateFormat("yyyyMMdd");
      SimpleDateFormat TFormat = new SimpleDateFormat("HHmm");
      String last_mod_date = FFormat.format(fecha.getTime());
      String last_mod_time = TFormat.format(fecha.getTime());
      PlanViaje planVj = (PlanViaje)session.getAttribute("planVj");
      PlanViaje tmp = null;
      Caravana caravana = null;
      String carNo = (String)session.getAttribute("caravana");
      NitSot conductor = null;
      NitSot propietario = null;
      
      planVj.setCia(request.getParameter("cia"));
      planVj.setPlanilla(request.getParameter("planilla"));
      planVj.setPlaca(request.getParameter("placa"));
      planVj.setProducto(request.getParameter("producto"));
      planVj.setTrailer(request.getParameter("trailer"));
      planVj.setContenedor(request.getParameter("contenedor"));
      planVj.setCodtipocarga(request.getParameter("codtipocarga"));
      
      planVj.setFecha(request.getParameter("fecha"));
      planVj.setDestinatario(request.getParameter("destinatario"));
      planVj.setRuta(request.getParameter("ruta"));
      planVj.setCedcon(request.getParameter("cedcon"));
      
      planVj.setRadio(request.getParameter("radio"));
      planVj.setCelular(request.getParameter("celular"));
      planVj.setAvantel(request.getParameter("avantel"));
      planVj.setTelefono(request.getParameter("telefono"));
      planVj.setCazador(request.getParameter("cazador"));
      planVj.setMovil(request.getParameter("movil"));
      planVj.setOtro(request.getParameter("otro"));
      
      planVj.setNomfam(request.getParameter("nomfam"));
      planVj.setPhonefam(request.getParameter("phonefam"));
      planVj.setNitpro(request.getParameter("nitpro"));
      planVj.setComentario1(request.getParameter("comentario1"));
      planVj.setComentario2(request.getParameter("comentario2"));
      planVj.setComentario3(request.getParameter("comentario3"));
      planVj.setLast_mod_date(last_mod_date);
      planVj.setLast_mod_time(last_mod_time);
      planVj.setLast_mod_user(usuario.getLogin());
      planVj.setUsuario(usuario.getLogin());
      
      retorno = request.getParameter("retorno");
      if (retorno == null)
          retorno = "N";
      
      planVj.setRetorno(retorno);
      
      try {
          
          //SE ONTIENE LA INFORMACION COMUN DE LA CARAVANA
          tmp = model.caravanaService.getCaravanaInfoComun(carNo);
          planVj.setAl1(tmp.getAl1());
          planVj.setAt1(tmp.getAt1());
          planVj.setAl2(tmp.getAl2());
          planVj.setAt2(tmp.getAt2());
          planVj.setAl3(tmp.getAl3());
          planVj.setAt3(tmp.getAt3());
          planVj.setPl1(tmp.getPl1());
          planVj.setPt1(tmp.getPt1());
          planVj.setPl2(tmp.getPl2());
          planVj.setPt2(tmp.getPt2());
          planVj.setPl3(tmp.getPl3());
          planVj.setPt3(tmp.getPt3());
          planVj.setQlo(tmp.getQlo());
          planVj.setQto(tmp.getQto());
          planVj.setQld(tmp.getQld());
          planVj.setQtd(tmp.getQtd());
          planVj.setQl1(tmp.getQl1());
          planVj.setQt1(tmp.getQt1());
          planVj.setQl2(tmp.getQl2());
          planVj.setQt2(tmp.getQt2());
          planVj.setQl3(tmp.getQl3());
          planVj.setQt3(tmp.getQt3());
          planVj.setTl1(tmp.getTl1());
          planVj.setTt1(tmp.getTt1());
          planVj.setTl2(tmp.getTl2());
          planVj.setTt2(tmp.getTt2());
          planVj.setTl3(tmp.getTl3());
          planVj.setTt3(tmp.getTt3());
        //busca el nit del conductor para crearlo o actualizarlo
        if (model.nitService.searchNit(planVj.getCedcon())){
          conductor = model.nitService.getNit();
          conductor.setNombre(request.getParameter("nomcon"));
          conductor.setDireccion(request.getParameter("dircon"));
          conductor.setTelefono(request.getParameter("phonecon"));
          conductor.setCodciu(request.getParameter("ciucon"));
          model.nitService.updateNit(conductor);
        }
        else{
          conductor = new NitSot();
          conductor.setCedula(request.getParameter("cedcon"));
          conductor.setNombre(request.getParameter("nomcon"));
          conductor.setDireccion(request.getParameter("dircon"));
          conductor.setTelefono(request.getParameter("phonecon"));
          conductor.setCodciu(request.getParameter("ciucon"));
          conductor.setCellular("");
          conductor.setCoddpto("");
          conductor.setCodpais("");
          conductor.setE_mail("");
          conductor.setFechanac("0099-01-01");
          conductor.setId_mims("");
          conductor.setSexo("");
          conductor.setUsuariocrea("");
          conductor.setUsuario("");
          model.nitService.insertNit(conductor);
        }
        
        //busca el nit del propetrio para crearlo o actualizarlo
        if (model.nitService.searchNit(planVj.getNitpro())){
          propietario = model.nitService.getNit();
          propietario.setNombre(request.getParameter("nompro"));
          propietario.setDireccion(request.getParameter("dirpro"));
          propietario.setTelefono(request.getParameter("phonepro"));
          propietario.setCodciu(request.getParameter("ciupro"));
          model.nitService.updateNit(propietario);
        }
        else{
          propietario = new NitSot();
          propietario.setCedula(request.getParameter("nitpro"));
          propietario.setNombre(request.getParameter("nompro"));
          propietario.setDireccion(request.getParameter("dirpro"));
          propietario.setTelefono(request.getParameter("phonepro"));
          propietario.setCodciu(request.getParameter("ciupro"));
          propietario.setCellular("");
          propietario.setCoddpto("");
          propietario.setCodpais("");
          propietario.setE_mail("");
          propietario.setFechanac("0099-01-01");
          propietario.setId_mims("");
          propietario.setSexo("");
          propietario.setUsuariocrea("");
          propietario.setUsuario("");
          model.nitService.insertNit(propietario);
        }
        
        caravana = new Caravana();
        caravana.setPlanilla(planVj.getPlanilla());
        caravana.setUsuario(usuario.getLogin());
        caravana.setCodigo(carNo);
        caravana.setCia(planVj.getCia());
        
        //verifica si al momento grabar el plan de viaje ha sido creado por otro usuario
        if (!model.planViajeService.planViajeExist(planVj.getCia(), planVj.getPlanilla())){
          model.planViajeService.createPlanVj(planVj);
          model.caravanaService.crearCaravana(caravana);
        }
        else{
          model.planViajeService.updatePlanVj(planVj);
        }
        
        model.caravanaService.listarPvjCaravana(carNo);
        session.removeAttribute("planVj");
        session.removeAttribute("caravana");
        
        logger.info(usuario.getNombre() + " Cre� CARAVANA No = " + caravana + " con Planilla = " + planVj.getPlanilla());
      }
      catch (Exception e){
        throw new ServletException(e.getMessage());
      }
      next = "/caravana/PantallaModCaravana.jsp";
      // Redireccionar a la p�gina indicada.
      this.dispatchRequest(next);
  }
}
