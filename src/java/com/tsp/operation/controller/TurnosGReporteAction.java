/********************************************************************
 *      Nombre Clase.................   TurnosGReporteAction.java
 *      Descripci�n..................   Action para Acceder al modulo de listas de turnos por usuario en trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class TurnosGReporteAction extends Action{
    
    /** Creates a new instance of TurnosGReporteAction */
    public TurnosGReporteAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        try {
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String userlogin=""+usuario.getLogin ();  
            String usuario_turno=""+request.getParameter ("usuario").toUpperCase();
            String Fecini = request.getParameter("fechai");
            String Fecfin  = request.getParameter("fechaf");
            model.turnosService.turnosPorCodigo (usuario_turno,Fecini,Fecfin); 
            next="/jsp/trafico/turnos/reporte1.jsp?marco=no&fechai=" + Fecini + "&fechaf" + Fecfin;    
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
            
        }
        this.dispatchRequest (next);
    }
}
