/*******************************************************************
 * Nombre clase: UbicacionVehicularReturnAction.java
 * Descripci�n: Accion para anular un acuerdo especial a la bd.
 * Autor: Ing. Ivan Gomez
 * Fecha: 12 de abril de 2004, 11:11 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author  iherazo
 */
public class UbicacionVehicularReturnAction extends Action
{
  /**
   * Ejecuta la acci�n. Esta en particular, busca las devoluciones hechas por
   * un cliente en el reporte de ubicaci�n vehicular y las muestra.
   * @throws IOException Si no se puede abrir la vista solicitada por esta
   *         accion, la cual corresponde a una p�gina jsp.
   */
  public void run() throws ServletException
  {
    try
    {
      model.UbicacionVehicularClientesSvc.UbicacionVehicularReturn( request.getParameter("planilla") );
    }catch(SQLException SQLE){
      throw new ServletException(SQLE.getMessage());
    }
    
    this.dispatchRequest("/jsp/sot/reports/UbicVehDevoluciones.jsp");
  }
}
