/*************************************************************************
 * Nombre ......................ActividadIngresarAction.java             *
 * Descripci�n..................Clase Action para insetar actividad      *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................26 de agosto de 2005, 09:12 AM           * 
 * Versi�n......................1.0                                      * 
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/ 

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

 
public class ActividadIngresarAction extends Action {
    
    /** Creates a new instance of ActividadIngresarAction */
    public ActividadIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/actividad/actividad.jsp?mensaje=MsgAgregado";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0;
        
        Actividad act = new Actividad();
        act.setCodActividad(request.getParameter("codactividad").toUpperCase());
        act.setDescorta(request.getParameter("descorta").toUpperCase());
        act.setDesLarga(request.getParameter("deslarga").toUpperCase());
        
        act.setFecinicio((request.getParameter("fecini")!=null)?request.getParameter("fecini"):"N");
        act.setM_Fecinicio((request.getParameter("m_fecini")!=null)?request.getParameter("m_fecini"):"N");
        act.setCortaFecini((request.getParameter("cortafecini")!=null)?request.getParameter("cortafecini").toUpperCase():"");
        act.setLargaFecini((request.getParameter("largafecini")!=null)?request.getParameter("largafecini").toUpperCase():"");
        
        
        act.setFecfinal((request.getParameter("fecfin")!=null)?request.getParameter("fecfin"):"N");
        act.setM_Fecfinal((request.getParameter("m_fecfin")!=null)?request.getParameter("m_fecfin"):"N");
        act.setCortaFecfin((request.getParameter("cortafecfin")!=null)?request.getParameter("cortafecfin").toUpperCase():"");
        act.setLargaFecfin((request.getParameter("largafecfin")!=null)?request.getParameter("largafecfin").toUpperCase():"");
        
        act.setDuracion((request.getParameter("duracion")!=null)?request.getParameter("duracion"):"N");
        act.setM_Duracion((request.getParameter("m_duracion")!=null)?request.getParameter("m_duracion"):"N");
        act.setCortaDuracion((request.getParameter("cortaduracion")!=null)?request.getParameter("cortaduracion").toUpperCase():"");
        act.setLargaDuracion((request.getParameter("largaduracion")!=null)?request.getParameter("largaduracion").toUpperCase():"");
        
        act.setDocumento((request.getParameter("documento")!=null)?request.getParameter("documento"):"N");
        act.setM_Documento((request.getParameter("m_documento")!=null)?request.getParameter("m_documento"):"N");
        act.setCortaDocumento((request.getParameter("cortadocumento")!=null)?request.getParameter("cortadocumento").toUpperCase():"");
        act.setLargaDocumento((request.getParameter("largadocumento")!=null)?request.getParameter("largadocumento").toUpperCase():"");
        
        act.setTiempodemora((request.getParameter("tiempodemora")!=null)?request.getParameter("tiempodemora"):"N");
        act.setM_Tiempodemora((request.getParameter("m_tiempodemora")!=null)?request.getParameter("m_tiempodemora"):"N");
        act.setCortaTiempodemora((request.getParameter("cortatiempodemora")!=null)?request.getParameter("cortatiempodemora").toUpperCase():"");
        act.setLargaTiempodemora((request.getParameter("largatiempodemora")!=null)?request.getParameter("largatiempodemora").toUpperCase():"");
        
        act.setCausademora((request.getParameter("causademora")!=null)?request.getParameter("causademora"):"N");
        act.setM_Causademora((request.getParameter("m_causademora")!=null)?request.getParameter("m_causademora"):"N");
        act.setCortaCausademora((request.getParameter("cortacausademora")!=null)?request.getParameter("cortacausademora").toUpperCase():"");
        act.setLargaCausademora((request.getParameter("largacausademora")!=null)?request.getParameter("largacausademora").toUpperCase():"");
        
        act.setResdemora((request.getParameter("resdemora")!=null)?request.getParameter("resdemora"):"N");
        act.setM_Resdemora((request.getParameter("m_resdemora")!=null)?request.getParameter("m_resdemora"):"N");
        act.setCortaResdemora((request.getParameter("cortaresdemora")!=null)?request.getParameter("cortaresdemora").toUpperCase():"");
        act.setLargaResdemora((request.getParameter("largaresdemora")!=null)?request.getParameter("largaresdemora").toUpperCase():"");
        
        act.setFeccierre((request.getParameter("feccierre")!=null)?request.getParameter("feccierre"):"N");
        act.setM_Feccierre((request.getParameter("m_feccierre")!=null)?request.getParameter("m_feccierre"):"N");
        act.setCortaFeccierre((request.getParameter("cortafeccierre")!=null)?request.getParameter("cortafeccierre").toUpperCase():"");
        act.setLargaFeccierre((request.getParameter("largafeccierre")!=null)?request.getParameter("largafeccierre").toUpperCase():"");
        
        act.setObservacion((request.getParameter("observacion")!=null)?request.getParameter("observacion"):"N");
        act.setM_Observacion((request.getParameter("m_observacion")!=null)?request.getParameter("m_observacion"):"N");
        act.setCortaObservacion((request.getParameter("cortaobservacion")!=null)?request.getParameter("cortaobservacion").toUpperCase():"");
        act.setLargaObservacion((request.getParameter("largaobservacion")!=null)?request.getParameter("largaobservacion").toUpperCase():"");
        
        //ojo
        act.setCantrealizada((request.getParameter("cantrealizada")!=null)?request.getParameter("cantrealizada"):"N");
        act.setM_cantrealizada((request.getParameter("m_cantrealizada")!=null)?request.getParameter("m_cantrealizada"):"N");
        act.setCorta_cantrealizada((request.getParameter("cortacantrealizada")!=null)?request.getParameter("cortacantrealizada").toUpperCase():"");
        act.setLarga_cantrealizada((request.getParameter("largacantrealizada")!=null)?request.getParameter("largacantrealizada").toUpperCase():"");
        
        act.setCantplaneada((request.getParameter("cantplaneada")!=null)?request.getParameter("cantplaneada"):"N");
        act.setM_cantplaneada((request.getParameter("m_cantplaneada")!=null)?request.getParameter("m_cantplaneada"):"N");
        act.setCorta_cantplaneada((request.getParameter("cortacantplaneada")!=null)?request.getParameter("cortacantplaneada").toUpperCase():"");
        act.setLarga_cantplaneada((request.getParameter("largacantplaneada")!=null)?request.getParameter("largacantplaneada").toUpperCase():"");
        
        //nuevos campos 19-12-2005
        act.setReferencia1((request.getParameter("referencia1")!=null)?request.getParameter("referencia1"):"N");
        act.setM_referencia1((request.getParameter("m_referencia1")!=null)?request.getParameter("m_referencia1"):"N");
        act.setCorta_referencia1((request.getParameter("cortareferencia1")!=null)?request.getParameter("cortareferencia1").toUpperCase():"");
        act.setLarga_referencia1((request.getParameter("largareferencia1")!=null)?request.getParameter("largareferencia1").toUpperCase():"");
        
        act.setReferencia2((request.getParameter("referencia2")!=null)?request.getParameter("referencia2"):"N");
        act.setM_referencia2((request.getParameter("m_referencia2")!=null)?request.getParameter("m_referencia2"):"N");
        act.setCorta_referencia2((request.getParameter("cortareferencia2")!=null)?request.getParameter("cortareferencia2").toUpperCase():"");
        act.setLarga_referencia2((request.getParameter("largareferencia2")!=null)?request.getParameter("largareferencia2").toUpperCase():"");
        
        act.setRefnumerica1((request.getParameter("refnumerica1")!=null)?request.getParameter("refnumerica1"):"N");
        act.setM_refnumerica1((request.getParameter("m_refnumerica1")!=null)?request.getParameter("m_refnumerica1"):"N");
        act.setCorta_refnumerica1((request.getParameter("cortarefnumerica1")!=null)?request.getParameter("cortarefnumerica1").toUpperCase():"");
        act.setLarga_refnumerica1((request.getParameter("largarefnumerica1")!=null)?request.getParameter("largarefnumerica1").toUpperCase():"");
        
        act.setRefnumerica2((request.getParameter("refnumerica2")!=null)?request.getParameter("refnumerica2"):"N");
        act.setM_refnumerica2((request.getParameter("m_refnumerica2")!=null)?request.getParameter("m_refnumerica2"):"N");
        act.setCorta_refnumerica2((request.getParameter("cortarefnumerica2")!=null)?request.getParameter("cortarefnumerica2").toUpperCase():"");
        act.setLarga_refnumerica2((request.getParameter("largarefnumerica2")!=null)?request.getParameter("largarefnumerica2").toUpperCase():"");
        act.setLink((request.getParameter("linkimg")!=null)?request.getParameter("linkimg"):"N");
        //**
        
        act.setPerfil(request.getParameter("perfil"));
        act.setMane_evento(request.getParameter("m_evento"));
        act.setCreation_user(usuario.getLogin());
        act.setUser_update(usuario.getLogin());
        act.setBase(usuario.getBase());
        act.setDstrct(usuario.getCia());
        act.setLast_update(now);
        act.setCreation_date(now);
        
        try{
            try{
                model.actividadSvc.insertarActividad(act);
            }catch (SQLException e){
                sw=1;
            } 
            if(sw==1){
                if (model.actividadSvc.existeActividadAnulada(act.getCodActividad(),act.getDstrct())){
                    act.setEstado("");
                    model.actividadSvc.modificarActividad(act);                
                }            
                else
                    next = "/jsp/trafico/actividad/actividad.jsp?mensaje=MsgErrorIng"; 
            }
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
