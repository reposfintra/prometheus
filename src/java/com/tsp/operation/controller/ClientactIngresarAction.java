/*************************************************************************
 * Nombre                       ClientactInsertarAction.java             *
 * Descripci�n                  Clase Action para insetar cliente        *
 *                              actividad                                *
 * Autor.                       Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha                        29 de agosto de 2005, 12:54 PM           * 
 * Versi�n                      1.0                                      * 
 * Coyright                     Transportes Sanchez Polo S.A.            *
 *************************************************************************/ 


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes
 */
public class ClientactIngresarAction extends Action  {
    
    /** Creates a new instance of ClientactInsertarAction */
    public ClientactIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/clienteactividad/clienteactividad.jsp?mensaje=MsgAgregado";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0,a=0;
        
        ClienteActividad clientact = new ClienteActividad();
        clientact.setCodActividad(request.getParameter("actividad"));
        clientact.setCodCliente(request.getParameter("cliente"));
        clientact.setTipoViaje(request.getParameter("tipo").toUpperCase());
        clientact.setSecuencia(Integer.parseInt(request.getParameter("secuencia")));
        clientact.setCreation_user(usuario.getLogin());
        clientact.setUser_update(usuario.getLogin());
        clientact.setBase(usuario.getBase());
        clientact.setDstrct(usuario.getCia());
        clientact.setLast_update(now);
        clientact.setCreation_date(now);
        try{
            if (!model.clientactSvc.existeSecuencia(usuario.getCia(),request.getParameter("cliente"),Integer.parseInt(request.getParameter("secuencia")), request.getParameter("tipo").toUpperCase())  ){
                try{
                    model.clientactSvc.insertarClientAct(clientact);
                }catch (SQLException e){
                    sw=1;
                }
                if(sw==1){
                    if ( model.clientactSvc.existeClientActAnulada( clientact.getCodCliente(),clientact.getCodActividad(),clientact.getTipoViaje(),usuario.getCia()) ){
                        clientact.setEstado("");
                        model.clientactSvc.modificarClientAct(clientact);
                    }
                    else
                        next = "/jsp/trafico/clienteactividad/clienteactividad.jsp?mensaje=MsgErrorIng";
                }
            }
            else{
                next = "/jsp/trafico/clienteactividad/clienteactividad.jsp?mensaje=Existe";
            }
            model.clientactSvc.listarActiClient(usuario.getCia(), clientact.getCodCliente());
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
