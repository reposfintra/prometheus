/***************************************
 * Nombre Clase ............. LiberarFacturasXCorridasAction.java
 * Descripci�n  .. . . . . .  Maneja eventos para la liberaci�n de facturas No aprobadas para pago dentro de una corrida.
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  27/03/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/





package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.HLiberacionFacturas;


public class LiberarFacturasXCorridasAction extends Action{
    
 
    
    public void run() throws ServletException, InformationException {
        
        try{
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");    
                String[] corrida = request.getParameterValues("corridas");
                
        
                String   next          = "/jsp/cxpagar/corridas/LiberarFacturasXCorrida.jsp?msj=";
                String   msj           = "";
              
               
                String evento          = request.getParameter("evento");
                
                if(evento!=null){
                   if( evento.equals("FREE") ){
                       
                        HLiberacionFacturas  hilo = new HLiberacionFacturas();
                        hilo.start( model, usuario, corrida);
                        msj="Proceso de Liberaci�n de facturas en  corrida se ha iniciado...";
                                
                   }
                    
                }else{                
                   model.LiberarFacturasSvc.setTreemap(new TreeMap());
                   model.LiberarFacturasSvc.getCorridasParaLiberar((String) session.getAttribute("Distrito"),usuario.getLogin());
                }
                

                next+= msj;
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response); 
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
        
        
    }
    
}
