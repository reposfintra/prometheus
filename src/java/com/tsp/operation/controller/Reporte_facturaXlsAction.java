/**
 * Nombre       Reporte_facturaXlsAction.java 
 * Autor        Ing Jose de la Rosa
 * Modificar    Ing Sandra Escalante
 * Fecha        18/10/2005
 * versi�n      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 **/

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;


public class Reporte_facturaXlsAction extends Action{
    
    /** Creates a new instance of Reporte_facturaXlsAction */
    public Reporte_facturaXlsAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/cxpagar/facturas/ReporteFacturas.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String agencia = usuario.getId_agencia();
        String fchi = (String) request.getParameter("fechaInicio");
        String fchf = (String) request.getParameter("fechaFinal");
        
        try{
            if (model.cxpDocService.getEnproceso()){
                next += "?msg=Hay un proceso en ejecucion. Una vez terminado podra realizar la generacion de un nuevo reporte...<br>Para realizar el seguimiento del actual proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
            }
            else {
                model.cxpDocService.setEnproceso();
                ReporteFacturasCreadasXLS hilo = new ReporteFacturasCreadasXLS(model);
                hilo.start(usuario.getLogin(),distrito, agencia, fchi, fchf);
                next += "?msg=La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
            }
        }catch (Exception ex){
            throw new ServletException("Error en Reporte_facturaXlsAction......\n"+ex.getMessage());
        }
        this.dispatchRequest(next);         
    }
}
