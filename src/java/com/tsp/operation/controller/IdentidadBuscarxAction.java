/*************************************************************************
 * Nombre:        IdentidadBuscarxAction.java         
 * Descripci�n:   Clase Action para buscar identidad    
 * Autor:         Ing. Diogenes Antonio Bastidas Morales  
 * Fecha:         20 de julio de 2005, 08:34 AM                            
 * Versi�n        1.0                                      
 * Coyright:      Transportes Sanchez Polo S.A.            
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class IdentidadBuscarxAction extends Action{
    
    /** Creates a new instance of IdentidadBuscarxAction */
    public IdentidadBuscarxAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String doc = request.getParameter("c_documento");
        String nom = request.getParameter("c_nombre").toUpperCase();
        String cia = request.getParameter("c_cia");
        try {
            model.identidadService.listarIdentidadXBusq(doc+"%", nom+"%", cia+"%");
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
