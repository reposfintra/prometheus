/********************************************************************
 *      Nombre Clase.................   RelacionGrupoSubgrupoPlacaSearchAction.java
 *      Descripci�n..................   Busca las Relaciones entre las placas, grupos y subgrupos
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  EQUIPO12
 */
public class RelacionGrupoSubgrupoPlacaSearchAction extends Action {
        
        /** Creates a new instance of RelacionGrupoSubgrupoPlacaSearchAction */
        public RelacionGrupoSubgrupoPlacaSearchAction() {
        }
         public void run() throws ServletException, InformationException {
                RelacionGrupoSubgrupoPlaca relacion = new RelacionGrupoSubgrupoPlaca();
                String  next="";
                //Info del usuario
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String placa = request.getParameter("placa");
                String grupo = request.getParameter("grupo");
                String subgrupo = request.getParameter("subgrupo");
                String user = usuario.getLogin();
                String base = usuario.getBase();
                String dstrct = usuario.getDstrct();
                
                
                
                try {
                        List relaciones = model.relaciongruposubgrupoplaca.ConsultaRelacion(placa, subgrupo, grupo);
                        request.setAttribute("relaciones", relaciones);
                        next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlacaList.jsp";
                        
                }catch(SQLException e){
                        throw new ServletException(e.getMessage());
                }
                this.dispatchRequest(next);
        }
}
