/********************************************************************
 *      Nombre Clase.................   ReimpresionAction.java
 *      Descripci�n..................   Action que se encarga de manejar los eventos para la reimpresion
 *      Autor........................   Ivan Gomez
 *      Fecha........................   22.02.2007
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  Ivan Gomez
 */
public class ReimpresionAction  extends Action {
    
    /** Creates a new instance of FactursCargarbancosAction */
    public ReimpresionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            
            //ivan 21 julio 2006
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String next = "/jsp/cxpagar/cheques/";
            String opcion     = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
            String banco      =  (request.getParameter("c_banco")!=null)?request.getParameter("c_banco"):"";
            String c_sucursal =  (request.getParameter("c_sucursal")!=null)?request.getParameter("c_sucursal"):"";
            String Cinicial   = (request.getParameter("Cinicial")!=null)?request.getParameter("Cinicial"):""; 
            String Cfinal     = (request.getParameter("Cfinal")!=null)?request.getParameter("Cfinal"):""; 
            
            if(opcion.equals("SUCURSALES")){
                model.servicioBanco.loadSusursales(banco);
                next+="FiltroReimpresion.jsp";
                
            }else if(opcion.equals("BUSCAR_CHEQUES")){
                
                model.ReimpresionSvc.buscarCheques(usuario.getDstrct(), banco, c_sucursal, Cinicial, Cfinal);   
                if(model.ReimpresionSvc.getListaCheques().size()>0){
                    next+="ListadoReimpresion.jsp";
                }else{
                    next+="FiltroReimpresion.jsp?msg=No se encontraron registros";
                }
            }else if(opcion.equals("UPDATE")){
                next += "ListadoReimpresion.jsp?msg=Cheques actualizados exitosamente";
                String[]Selec = request.getParameterValues("chequesSelec");
                List listado = model.ReimpresionSvc.getListaCheques();
                List ChequesNoSelec = new LinkedList();
                List ChequesSelec = new LinkedList();
                Iterator it = listado.iterator();
                while(it.hasNext()){
                    Egreso eg = (Egreso) it.next();
                    boolean esta = false;
                    if(Selec != null){
                        for(int i=0;i<Selec.length;i++){
                            if(eg.getDocument_no().equals(Selec[i])){
                                ChequesSelec.add(eg);
                                esta = true;
                                eg.setReimpresion(true);
                                break;
                            }
                        }
                        if(!esta){
                            ChequesNoSelec.add(eg);
                            eg.setReimpresion(false);
                        }
                    }else{
                        ChequesNoSelec.add(eg);
                        eg.setReimpresion(false);
                    }
                }
                
                model.ReimpresionSvc.updateCheques(ChequesSelec, ChequesNoSelec);
                
            }
            
            
            
            ////System.out.println("next en Filtro"+next);
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}

