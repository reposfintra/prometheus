package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.services.GestionSolicitudAiresAAEService;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.Email2;
import com.tsp.operation.model.beans.NegocioAnalista;
import com.tsp.operation.model.beans.NegocioTrazabilidad;
import com.tsp.operation.model.beans.Negocios;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.SolicitudDocumentos;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.EmailSendingEngineService;
import com.tsp.operation.model.services.GestionCondicionesService;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.operation.model.services.NegocioTrazabilidadService;
import com.tsp.operation.model.services.NegociosGenService;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * Controlador para el programa de manejo de la trazabilidad del negocio
 * @author ivargas - geotech
 */
public class NegocioTrazabilidadAction extends Action {

    HttpSession session;
    Usuario usuario;
    String next = "";
    NegocioTrazabilidadService trazaService;
    NegociosGenService negserv;
    GestionSolicitudAvalService gsaserv;
    GestionCondicionesService gcondserv;
    private int fila = 0;
    POIWrite xls;
    private SimpleDateFormat fmt;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    String   rutaInformes;
    String iduser;
    String   nombre;
    
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            trazaService = new NegocioTrazabilidadService(usuario.getBd());
            negserv = new NegociosGenService(usuario.getBd());
            gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            gcondserv = new GestionCondicionesService(usuario.getBd());
            String opcion = request.getParameter("opcion");
            boolean redirect = true;
            String strRespuesta = "";
            iduser = usuario.getLogin();

            if (opcion.equals("GRABAR_TRAZA")) {
                strRespuesta = grabarTraza();
                redirect = false;
            } else if (opcion.equals("CARGARLISTA")) {
                strRespuesta = cargarLista();
                redirect = false;
            } else if (opcion.equals("GRABAR_REFERECIA")) {
                next = guardarArchivoEnDirectorio();
                redirect = true;
            }
            else if (opcion.equals("PERFECCIONAR")) {
                next = perfeccionar();
                redirect = true;
            }else if (opcion.equals("BUSCAR_NEGOCIOS_REL")) {
                buscarNegociosRelAut();
                redirect = false;
            }else if (opcion.equals("ACTUALIZAR_NEGOCIO_REL")) {
                actualizarNegocioRel();
                redirect = false;
            }else if (opcion.equals("LISTAR_NEGOCIOS_STANDBY")) {
                //String unidad_negocio = null ;
                buscarNegociosStandBy();
                redirect = false;
            }else if (opcion.equals("REGRESAR_ACT_NEGOCIO_STANDBY")) {
                actualizarNegocioStandBy();
                redirect = false;
            }else if (opcion.equals("LISTAR_CONFIG_CAUSALES_STANDBY")){
                buscarConfigCausalesStandBy();
                redirect = false;
            }else if (opcion.equals("INSERT_CAUSAL_STANDBY")){
                insertarCausalStandBy();
                redirect = false;
            }else if (opcion.equals("UPDATE_CAUSAL_STANDBY")){
                actualizarCausalStandBy();
                redirect = false;
            }else if (opcion.equals("ACTIVAR_INACTIVAR_CAUSAL_STANDBY")){
                activaInactivaCausalStandBy();
                redirect = false;
            } else if (opcion.equals("RELIQUIDAR_NEGOCIOS_FINTRA")){
               strRespuesta =  this.updateNegociosLiquidacion();
                redirect = false;
            } else if (opcion.equals("AGREGAR_COMENTARIO_STANDBY")) {
                agregarComentarioStandBy();
                redirect = false;            
            } else if (opcion.equals("DEVOLVER_DECISION")) {
                strRespuesta =  this.devolver_decision();
                redirect = false;
            }else if (opcion.equals("EXPORTAR_NEGOCIO_STAMBY")) {
                exportarExcelNegocioStamby();
                redirect = false;
            }

            if (redirect) {
                this.dispatchRequest(next);
            } else {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-store");
                response.setDateHeader("Expires", 0);
                response.getWriter().print(strRespuesta);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error en NegociosTrazabilidadAction: " + e.getMessage());
        }

    }

    public String guardarArchivo() {
        String negocio = "";
        String form = "";
        String act = "";
        String coment = "";
        String concepto = "";

        try {

            String carpeta = "images/multiservicios";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("ruta") + "/" + carpeta + "/" + usuario.getLogin();//se establece la ruta de la imagen

            File carpetica = new File(directorioArchivos);

            deleteDirectory(carpetica);

            this.createDir(directorioArchivos);
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                String fieldtem = "", valortem = "";


                while (it.hasNext()) {
                    FileItem fileItem = (FileItem) it.next();
                    if (fileItem.isFormField()) {

                        fieldtem = fileItem.getFieldName();
                        valortem = fileItem.getString();
                        if (fieldtem.equals("negocio")) {
                            negocio = valortem;
                        }
                        if (fieldtem.equals("form")) {
                            form = valortem;
                        }
                        if (fieldtem.equals("act")) {
                            act = valortem;
                        }
                        if (fieldtem.equals("refcomentarios")) {
                            coment = valortem;
                        }
                        if (fieldtem.equals("refconcepto")) {
                            concepto = valortem;
                        }

                    }
                    if (!(fileItem.isFormField())) {
                        String nombreArchivo = fileItem.getName();
                        boolean sw2=false;
                        if(!nombreArchivo.equals("")){
                        String[] nombrearreglo = nombreArchivo.split("\\\\");
                        String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];
                        nombre_archivo = directorioArchivos + "/" + nombreArchivoremix;

                        File archivo = new File(nombre_archivo);
                        fileItem.write(archivo);

                         sw2=archivo.exists();
                        }else{
                            sw2=true;
                        }
                        if (sw2) {
                            
                            TransaccionService tService = new TransaccionService(usuario.getBd());
                            tService.crearStatement();
                            NegocioTrazabilidad sw = trazaService.buscarTraza(Integer.parseInt(form), act);
                            boolean ok = true;
                            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                            negtraza.setActividad(act);
                            negtraza.setNumeroSolicitud(form);
                            negtraza.setUsuario(usuario.getLogin());
                            negtraza.setCausal("");
                            negtraza.setComentarios(coment);
                            negtraza.setDstrct(usuario.getDstrct());
                            negtraza.setConcepto(concepto);
                            negtraza.setCodNeg(negocio);
                            next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&redi=S";
                            if (concepto.equals("DEVOLVER_RAD")||(concepto.equals("DEVOLVER_SOL"))&&(!model.Negociossvc.getTipoProceso(negocio).equals("DSR"))) {
                                if (sw == null) {
                                    if(concepto.equals("DEVOLVER_RAD"))
                                    {
                                       act = "LIQ";
                                    }
                                    if(concepto.equals("DEVOLVER_SOL"))
                                    {
                                        act = "SOL";
                                        tService.getSt().addBatch(trazaService.updateEstadoForm(form, "B"));
                                    }                                                                                                             
                                    
                                } else {
                                    ok = false;
                                }
                            }


                            if (ok) {

                                if(!nombreArchivo.equals("")){
                                    guardarArchivoEnBd(negocio, directorioArchivos, "", "negocio");
                                }
                                try {    
                                    
                                    if (act.equals("REF")) {
                                        if (!trazaService.tieneAnalista(negocio)) {
                                            ArrayList<String> analistas = trazaService.buscarAnalistas();
                                            if (analistas.size() > 0) {
                                                String analista = "";
                                                int sec = trazaService.secUltimoAnalistaProducto(form);
                                                if (sec == analistas.size()) {
                                                    analista = analistas.get(0);
                                                    sec = 1;
                                                } else {
                                                    analista = analistas.get(sec);
                                                    sec = sec + 1;
                                                }
                                                NegocioAnalista neg_analista = new NegocioAnalista();
                                                neg_analista.setDstrct(usuario.getDstrct());
                                                neg_analista.setCodNeg(negocio);
                                                neg_analista.setAnalista(analista);
                                                neg_analista.setSecuencia(sec);
                                                tService.getSt().addBatch(trazaService.insertNegocioAnalista(neg_analista));
                                            }
                                        }
                                    }




                                    tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                                    tService.getSt().addBatch(trazaService.updateActividad(negocio, act));
                                    
                                    //Aqui vamos a validar si si el negocio tine el titulo valor digitado.
                                    boolean validaDocs = true;
                                    if (model.Negociossvc.getTipoProceso(negocio).equals("DSR")) {
                                        ArrayList<SolicitudDocumentos> lista_docs = gsaserv.buscarDocsSolicitud(Integer.parseInt(form));
                                        //Vamos a validar que existan los documentos
                                        int contador = 0;
                                        if (!lista_docs.isEmpty()) {

                                            for (int i = 0; i < lista_docs.size(); i++) {
                                                SolicitudDocumentos solicitudDocumentos = lista_docs.get(i);

                                                if (solicitudDocumentos.getNumTitulo().equals("")
                                                        || solicitudDocumentos.getNumTitulo().equals("0")
                                                        || solicitudDocumentos.getNumTitulo().length() < 3) {

                                                    contador++;
                                                }
                                            }

                                            validaDocs = (contador > 0) ? false : true;
                                        } else {

                                            validaDocs = false;
                                        }
                                    }

                                    
                                    //Ejecutamos todos los cambios en la BD.

                                    if (validaDocs) {

                                        tService.execute();

                                    }else {
                                        next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&msg="
                                                + "<span class='fila'>Error: El Negocio no tiene registrado el numero de cheque.</span>";
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw new ServletException("Error en NegocioTrazabilidadAction: " + e.getMessage());
                                }
                            } else {
                                next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&msg=" + "<span class='fila'>Error: No se puede efectuar una segunda devolucion</span>";
                            }
                        }
                    }

                }
            }
        } catch (Exception ee) {
            System.out.println("error:" + ee.toString() + "__" + ee.getMessage());
        }
        return next;
    }



        public String perfeccionar() {
        String negocio = "";
        String form = "";
        String act = "";
        String coment = "";
        String concepto = "";
        String causal = "";
        String rutaArchivo = "";
        String nombre_archivo = "";

        try {
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioArchivos = rb.getString("rutaImagenes");//se establece la ruta de la imagen
            String folderName = "fintracredit/";

            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();               
                String fieldtem = "", valortem = "";


                while (it.hasNext()) {
                    FileItem fileItem = (FileItem) it.next();
                    if (fileItem.isFormField()) {

                        fieldtem = fileItem.getFieldName();
                        valortem = fileItem.getString();
                        if (fieldtem.equals("negocio")) {
                            negocio = valortem;
                        }
                        if (fieldtem.equals("form")) {
                            form = valortem;
                        }
                        if (fieldtem.equals("act")) {
                            act = valortem;
                        }
                        if (fieldtem.equals("perfcomentarios")) {
                            coment = valortem;
                        }
                        if (fieldtem.equals("perfconcepto")) {
                            concepto = valortem;
                        }
                        if (fieldtem.equals("perfcausal")) {
                            causal = valortem;
                        }

                    }
                    if (!(fileItem.isFormField())) {
                        String nombreArchivo = cleanAcentos(fileItem.getName());
                        boolean sw2=false, swExiste=false;
                        if(!nombreArchivo.equals("")){
                        String[] nombrearreglo = nombreArchivo.split("\\\\");
                        String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];                       
                        
                        nombre_archivo= nombreArchivoremix;                    
                      
                        rutaArchivo = directorioArchivos + folderName + negocio;
                        File f = new File(rutaArchivo);
                        if(! f.exists() ){
                           folderName="fintracredit1/";
                           this.createDir(directorioArchivos+folderName);
                        }
                        rutaArchivo = directorioArchivos + folderName + negocio;
                        this.createDir(rutaArchivo);

                        File archivo = new File(rutaArchivo + "/" + nombre_archivo);                   
                       
                        swExiste = archivo.exists();
                        fileItem.write(archivo);

                        sw2=archivo.exists();
                        }else{
                            sw2=true;
                        }
                        if (sw2) {

                            TransaccionService tService = new TransaccionService(usuario.getBd());
                            tService.crearStatement();
                            NegocioTrazabilidad sw = trazaService.buscarTraza(Integer.parseInt(form), act);
                            boolean ok = true;
                            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                            negtraza.setActividad(act);
                            negtraza.setNumeroSolicitud(form);
                            negtraza.setUsuario(usuario.getLogin());
                            negtraza.setCausal(causal);
                            negtraza.setComentarios(coment);
                            negtraza.setDstrct(usuario.getDstrct());
                            negtraza.setConcepto(concepto);
                            negtraza.setCodNeg(negocio);
                            next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&redi=S";
                            if ((!model.Negociossvc.getTipoProceso(negocio).equals("DSR"))) {
                                if (sw == null) {
                                    if (concepto.equals("DEVOLVER_LIQ")) {
                                        act = "SOL";
                                        tService.getSt().addBatch(trazaService.updateEstadoForm(form, "B"));
                                    }
                                   
                                    if (concepto.equals("RELIQUIDAR")) {
                                        act = "SOL";
                                    }
                                } else {
                                      if (concepto.equals("RELIQUIDAR")||concepto.equals("DEVOLVER_LIQ")) {
                                    ok = false;
                                    }
                                }
                                 if (concepto.equals("RECHAZAD")) {
                                        tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "R"));
                                    }
                                    if (concepto.equals("DESISTIDO")) {
                                        tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "D"));
                                    }
                            }

                            if (ok) {

                                if(!nombreArchivo.equals("")){
                                    String accion = "";
                                    accion =  (swExiste) ? "Actualizar" : "";
                                     guardarRegistroEnBd(negocio, directorioArchivos+ folderName, nombre_archivo, accion, "negocio");
                                }
                                try {
                                     if (concepto.equals("CONTINUA")) {
                                         tService.getSt().addBatch(negserv.actualiza_perfeccionamiento(negocio,"N"));
                                    }
                                   
                                    tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                                    tService.getSt().addBatch(trazaService.updateActividad(negocio, act));
                                    tService.execute();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw new ServletException("Error en NegocioTrazabilidadAction: " + e.getMessage());
                                }
                            } else {
                                next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&msg=" + "<span class='fila'>Error: No se puede efectuar una segunda devolucion</span>";
                            }
                        }
                    }

                }
            }
        } catch (Exception ee) {
            System.out.println("error:" + ee.toString() + "__" + ee.getMessage());
        }
        return next;
    }


    public String insertImagen(ByteArrayInputStream bfin, int longitud, Dictionary fields, String id, String tipito) throws ServletException {
        String estado = "";
        String fileName = (String) fields.get("archivo");
        try {
            model.negociosApplusService.insertImagen(bfin, longitud, fields, id, tipito);
            estado = "Archivo " + fileName + " ha sido guardado <br>";

        } catch (Exception e) {
            System.out.println("errorrcito" + e.toString() + "__" + e.getMessage());
            estado = "No se pudo guardar la imagen " + fileName + "<br>" + e.getMessage();
        }
        return estado;
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    static public boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    public void guardarArchivoEnBd(String negocio, String namecarpeta, String id, String tipo) {
        try {

            Dictionary fields = new Hashtable();
            fields.put("actividad", "023");
            fields.put("tipoDocumento", "031");
            fields.put("documento", negocio);
            fields.put("agencia", "OP");
            fields.put("usuario", usuario.getLogin());

            File file = new File(namecarpeta);

            String comentario = "";
            if (file.exists()) {
                File[] arc = file.listFiles();
                for (int i = 0; i < arc.length; i++) {

                    if (!arc[i].isDirectory()) {

                        String name = arc[i].getName();
                        FileInputStream in = new FileInputStream(file + "/" + name);
                        ByteArrayOutputStream out = new ByteArrayOutputStream();
                        ByteArrayInputStream bfin;
                        int input = in.read();
                        byte[] savedFile = null;
                        while (input != -1) {
                            out.write(input);
                            input = in.read();
                        }
                        in.close();
                        out.close();
                        savedFile = out.toByteArray();
                        bfin = new ByteArrayInputStream(savedFile);
                        fields.put("archivo", name);
                        // Insertar imagen
                        comentario += this.insertImagen(bfin, savedFile.length, fields, id, tipo);

                    }
                }
            }

        } catch (Exception ee) {
            System.out.println("error" + ee.toString() + "_" + ee.getMessage());
        }
    }

    /**
     * Graba la trazabilidad de una de las etapas del proceso
     * @return pagina de redireccion o mensaje de error
     * @throws SQLException 
     */
    public String grabarTraza() throws Exception {
        String negocio = request.getParameter("negocio");
        String form = request.getParameter("form");
        String act = request.getParameter("act") != null ? request.getParameter("act") : "";
        String coment = request.getParameter("coment") != null ? request.getParameter("coment") : "";
        String coment_stby = request.getParameter("coment_stby") != null ? request.getParameter("coment_stby") : "";
        String causa = request.getParameter("causa") != null ? request.getParameter("causa") : "";
        String concep = request.getParameter("concep") != null ? request.getParameter("concep") : "";
        String tipo_proceso= model.Negociossvc.getTipoProceso(negocio);
        String perfeccionar = request.getParameter("perfeccionar") != null ? request.getParameter("perfeccionar") : "N";
        String ruta = "";
        boolean ok = true;
        boolean swFor = false;

        try {
            String conceptoTraza = model.Negociossvc.obtenerUltimoConceptoTraza(negocio);
            if(conceptoTraza.equals("DEVOLVER")){   
               if(concep.equals("FORMALIZ") || concep.equals("RECHAZAD"))
                   swFor = true;
            }
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            negtraza.setActividad(act);
//            negtraza.setActividad((concep.equals("STANDBY") && act.equals("ANA"))?"REF":act);
            negtraza.setNumeroSolicitud(form);
            negtraza.setUsuario(usuario.getLogin());
            negtraza.setCausal(causa);
            negtraza.setComentarios(coment);
            negtraza.setDstrct(usuario.getDstrct());
            negtraza.setConcepto(concep);
            negtraza.setCodNeg(negocio);
            negtraza.setComentario_standby(coment_stby);
            if (conceptoTraza.equals("DEVOLVER") && swFor == false) {
                ruta = "<span class='fila'>Error: No se puede devolver el negocio porque ya fue devuelto en transferencia</span>";
            } else {
                if (act.equals("REF")) {
                    // return "/jsp/fenalco/avales/solicitud_aval_buscar.jsp?vista=8";
                    ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=10";
                } else if (act.equals("RAD")) {
                    ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=6";
                } else if (act.equals("ANA")) {
                    ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=7";
                } else if (act.equals("DEC")) {
                    ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=8";
                } else if (act.equals("FOR")) {
                    ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=9";
                }

                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                NegocioTrazabilidad sw = trazaService.buscarTraza(Integer.parseInt(form), act);
                if (concep.equals("ZONGRISO") || concep.equals("DEVOLVER")) {
                    if (sw == null) {
                        if (act.equals("FOR")) {
                            act = "REG";
                        } else {
                            act = "SOL";
                            tService.getSt().addBatch(trazaService.updateEstadoForm(form, "B"));
                        }
                    } else {
                        ok = false;
                    }
                }

                if (concep.equals("DEVOLVER_RAD")) {
                    if (sw == null) {
                        if (act.equals("ANA") || act.equals("FOR")) {
                            act = "LIQ";
                        } else {
                            act = "SOL";
                            tService.getSt().addBatch(trazaService.updateEstadoForm(form, "B"));
                        }
                    } else {
                        ok = false;
                    }
                }

            //devolver un negocio desde la instancia de Formalizacion � Radicacion a Liquidacion
                //cuando es devuelto desde formalizacion se realiza todo el proceso de la fabrica.
                if (concep.equals("DEVOLVER_LIQ")) {
                    if (sw == null) {

                        if (act.equals("FOR") || act.equals("RAD")) {
                            act = "LIQ";
                            tService.getSt().addBatch(trazaService.updateEstadoForm(form, "Q"));
                            tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "Q"));
                        }
                    } else {
                        ok = false;
                    }
                }

                //falta revisar el procedimiento a seguir una vez es devuelto el negocio.
                if (concep.equals("DEVOLVER_SOL")) {
                    if (sw == null) {
                        if (act.equals("FOR") || act.equals("RAD")) {
                            act = "SOL";
                            tService.getSt().addBatch(trazaService.updateEstadoForm(form, "B"));
                        }
                    } else {
                        ok = false;
                    }
                }

                if (concep.equals("RELIQUIDAR")) {
                    if (sw == null) {
                        act = "SOL";
                    } else {
                        ok = false;
                    }
                }

                if (concep.equals("ZONGRILI") && tipo_proceso.equals("PSR")) {
                    ok = false;
                }

                if (concep.equals("ZONGRILI")) {
                    if (sw == null || act.equals("FOR")) {
                        act = "LIQ";
                        if (!act.equals("FOR")) {
                            tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "Q"));
                        }
                        tService.getSt().addBatch(trazaService.updateEstadoForm(form, "Q"));
                    } else {
                        ok = false;
                    }
                }
                Negocios neg = null;
                String tipo_conv = "";
                if (act.equals("DEC")) {
                    neg = negserv.buscarNegocio(negocio);
                    if (concep.equals("RECHAZAD")) {
                        tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "R"));
                        tService.getSt().addBatch(trazaService.updateEstadoForm(form, "R"));
                    } else {
                        String num_aval = "";
                        Convenio convenio = model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), neg.getId_convenio() + "");
                        if (convenio.getTipo().equals("Consumo") && convenio.isAval_anombre()) {
                            if (!model.Negociossvc.isNegocioAval(neg.getCod_negocio())) {
                                //numero de aprobacionde aval se modifica por agencia.
                                if (neg.getCod_negocio().substring(0, 2).equals("FA")) {
                                    num_aval = model.serieGeneralService.getSerie(usuario.getCia(), "OP", "NUMERO_AVAL_AN").getUltimo_prefijo_numero();
                                    model.serieGeneralService.setSerie(usuario.getCia(), "OP", "NUMERO_AVAL_AN"); //Incrementa el numero de serie
                                } else if (neg.getCod_negocio().substring(0, 2).equals("FB")) {
                                    num_aval = model.serieGeneralService.getSerie(usuario.getCia(), "CG", "NUMERO_AVAL_AN").getUltimo_prefijo_numero();
                                    model.serieGeneralService.setSerie(usuario.getCia(), "CG", "NUMERO_AVAL_AN"); //Incrementa el numero de serie
                                }
                            // num_aval=  model.serieGeneralService.getSerie(usuario.getCia(), usuario.getId_agencia(), "NUMERO_AVAL_AN").getUltimo_prefijo_numero();
                                //  model.serieGeneralService.setSerie(usuario.getCia(), usuario.getId_agencia(), "NUMERO_AVAL_AN"); //Incrementa el numero de serie
                            }
                        } else {
                            num_aval = negserv.UpCP("NUMERO_AVAL");
                        }
                        
                        if (!concep.equals("STANDBY")) {
                            tService.getSt().addBatch(negserv.avalarNeg(negocio, num_aval, usuario.getLogin()));
                            tService.getSt().addBatch(negserv.avalarForm(negocio, num_aval, usuario.getLogin()));
                        }
                        
                        if (perfeccionar.equals("S")) {
                            tService.getSt().addBatch(negserv.actualiza_perfeccionamiento(negocio, perfeccionar));
                        }
                        tipo_conv = model.gestionConveniosSvc.getTipoConvenio(neg.getId_convenio() + "");
                        if (tipo_proceso.equals("PSR") || tipo_proceso.equals("DSR")) {
                            if (tipo_conv.equals("Consumo")) {
                                ArrayList<SolicitudDocumentos> lisdoc = gsaserv.buscarDocsSolicitud(Integer.parseInt(form));
                                String id_prov_conv = gcondserv.codigoCv(neg.getId_convenio() + "", neg.getSector(), neg.getSubsector());
                                ArrayList<BeanGeneral> rangos = gcondserv.rangosConvenio(id_prov_conv.equals("") ? "0" : id_prov_conv, neg.getTneg(), neg.getFpago(), false);

                                for (int i = 0; i < lisdoc.size(); i++) {
                                    double comision_aval = 0;
                                    int dias = Util.fechasDiferenciaEnDias(Util.convertiraDate(neg.getFecha_neg()), Util.convertiraDate(lisdoc.get(i).getFecha()));
                                    for (int j = 0; j < rangos.size(); j++) {
                                        if (dias >= Integer.parseInt(rangos.get(j).getValor_01()) && dias <= Integer.parseInt(rangos.get(j).getValor_02())) {
                                            comision_aval = Double.parseDouble(lisdoc.get(i).getValor()) * (Double.parseDouble(rangos.get(j).getValor_03()) / 100);
                                            j = rangos.size();
                                        }
                                    }
                                    if (comision_aval != 0) {
                                        lisdoc.get(i).setComisionAval(comision_aval);
                                        lisdoc.get(i).setUserUpdate(usuario.getLogin());
                                        tService.getSt().addBatch(gsaserv.updateComisionAvalDoc(lisdoc.get(i)));
                                    } else {
                                        ok = false;
                                        ruta = "RANGO";
                                        i = lisdoc.size();
                                    }
                                }
                            }
                            tService.getSt().addBatch(model.Negociossvc.admit(usuario.getLogin(), "", negocio, num_aval, "0", "", "A"));
                            tService.getSt().addBatch(trazaService.updateEstadoForm(form, "A"));
                        }
                    }
                }

                if ((act.equals("FOR") || act.equals("ANA") || act.equals("RAD")) && concep.equals("RECHAZAD")) {
                    tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "R"));
                    tService.getSt().addBatch(trazaService.updateEstadoForm(form, "R"));
                }

                //desistir un negocio desde radicacion.
                if (concep.equals("DESISTIDO")) {
                    // NegocioTrazabilidad sw1 = trazaService.buscarTraza(Integer.parseInt(form), "FOR");
                    if (act.equals("RAD")) {
                        tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "D"));
                    }
                    /*else if (sw1 != null && !sw1.getConcepto().equals("DEVOLVER LIQUIDACION")) {
                     act = "DEC";
                     }*/
                }

            //validar cuando un negocio viene de formalizacion a radicacion
                //para dejarlo en la misma instancia uan vez radicado. 
                if (concep.equals("CONTINUA") && act.equals("RAD")) {
                    NegocioTrazabilidad sw1 = trazaService.buscarTraza(Integer.parseInt(form), "FOR");
                    if (sw1 != null) {

                        act = sw1.getConcepto().equals("DEVOLVER LIQUIDACION") ? "RAD" : "DEC";

                        //Si la activida es DEC cambiamos el estado del negocio a V
                        if (act.equals("DEC")) {
                            tService.getSt().addBatch(trazaService.updateEstadoNeg(negocio, "V"));
                        }

                    } else {
                        //si no viene de la actividad de formalizacion buscamos la ultima traza para ubicarlo en la activida correspondiente
                        NegocioTrazabilidad sw2 = trazaService.buscarUltimaActivida(Integer.parseInt(form));
                        if (sw2.getActividad().equals("DEC")) {

                            act = sw2.getConcepto().equals("APROBADO") ? "DEC" : "ANA";

                        } else if (sw2.getActividad().equals("ANA")) {

                            act = sw2.getConcepto().equals("PRE APROBADO") ? "ANA" : "REF";

                        } else if (sw2.getActividad().equals("REF")) {

                            act = sw2.getConcepto().equals("CONTINUAR") ? "REF" : "RAD";

                        } else if (sw2.getActividad().equals("LIQ")) {
                            NegocioTrazabilidad sw3 = trazaService.buscarTraza(Integer.parseInt(form), "REF");
                            if (sw3 != null) {
                                act = "RAD";
                            }

                        }

                    }

                }

                //metodo para salvar el montaje de iris para proyecto aires.
                if (act.equals("RAD")) {

                    tipo_conv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "";
                    if (tipo_conv.equals("Multiservicio")) {
                        GestionSolicitudAiresAAEService sasrv = new GestionSolicitudAiresAAEService();
                        int tot = request.getParameter("tot") != null ? Integer.parseInt(request.getParameter("tot")) : 0;
                        String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
                        tService.getSt().addBatch(sasrv.eliminarGarantias(id_solicitud));
                        for (int i = 0; i < tot; i++) {
                            BeanGeneral garantia = new BeanGeneral();
                            garantia.setValor_01(i + "");
                            garantia.setValor_02(request.getParameter("equipo" + i) != null ? request.getParameter("equipo" + i) : "");
                            garantia.setValor_03(request.getParameter("formulario" + i) != null ? request.getParameter("formulario" + i) : "");
                            garantia.setValor_04(request.getParameter("garantia" + i) != null ? request.getParameter("garantia" + i) : "");
                            garantia.setValor_05(id_solicitud);
                            garantia.setCreation_user(usuario.getLogin());
                            tService.getSt().addBatch(sasrv.insertarGarantiaEquipo(garantia));
                        }
                    }

                }

                //validar radicacion negocio microcredito cuando  devuelto de formalizacion a radicacion
                if (act.equals("RAD") && negocio.substring(0, 2).equals("MC")) {
                    //consultamos si viene de formalizacion.
                    NegocioTrazabilidad sw3 = trazaService.buscarTraza(Integer.parseInt(form), "FOR");
                    if (sw3 != null) {
                        act = "DEC";
                    }

                }
                
                if (concep.equals("STANDBY")) {
                    act = "STBY";
                }

                tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                tService.getSt().addBatch(trazaService.updateActividad(negocio, act));
                
                if (ok) {
                    tService.execute();
                    if (concep.equals("STANDBY")) {
                        neg = negserv.buscarNegocio(negocio);   
                        String id_und_negocio = trazaService.getUnidadConvenio(neg.getId_convenio());
                        String cliente = trazaService.getNombreCliente(negocio);
                        String receiversType = (id_und_negocio.equals("1")) ? "Comerciales_Micro" : (id_und_negocio.equals("22")) ? "Comerciales_Libranza" : "Comerciales_Fenalco"; 
                        String causal = trazaService.getCausalStandBy(causa);
                        String MyMessage = "Negocio No " +negocio+ " a nombre del cliente " + cliente + " fue colocado en STANDBY con causal " +causal+ " por el usuario " +usuario.getLogin()+ " debido a: "+coment;
                        String mailsToSend = trazaService.getCorreosToSendStandBy(receiversType);                      
                        String asunto = "Cambio estado Negocio " +negocio+  " a StandBy";
                        Thread hiloEmail = new Thread(new EmailSenderService(trazaService, "STANDBY", MyMessage, mailsToSend, asunto, usuario.getLogin()), "hilo");
                        hiloEmail.start();                         
                    }
                } else {
                    if (ruta.equals("RANGO")) {
                        ruta = "<span class='fila'>Error: No existen codiciones de aval para el afiliado con los rangos necesarios</span>";

                    } else if (!model.Negociossvc.getTipoProceso(negocio).equals("PSR")) {

                        ruta = "<span class='fila'>Error: No se puede efectuar una segunda devoluci&oacute;n</span>";
                    } else {
                        ruta = "<span class='fila'>Error: Los negocios Posfechados sin redescuento no se pueden devolver a la actividad de liquidacion</span>";
                    }
                }
                if (act.equals("DEC") && tipo_conv.equals("Multiservicio") && ok) {
                    String correosMT = gsaserv.nombreTablagen("CORREODEC", "MT");
                    String mensajeMT = "CODIGO DE NEGOCIO: " + neg.getCod_negocio() + "\n";
                    mensajeMT += "NOMBRE DEL CLIENTE: " + neg.getNom_cli() + "\n";
                    mensajeMT += "CEDULA DEL CLIENTE: " + neg.getCod_cli() + "\n";
                    mensajeMT += "VALOR DEL NEGOCIO: " + neg.getVr_negocio() + "\n";
                    mensajeMT += "FECHA DEL NEGOCIO: " + neg.getFecha_neg() + "\n";
                    mensajeMT += "Le Informamos que el negocio fue " + gsaserv.nombreTablagen("CONCEPNEG", concep) + "";
                    Email2 emailData = null;
                    emailData = new Email2();
                    emailData.setEmailId(12345);
                    emailData.setRecstatus("");
                    emailData.setEmailcode("emailcode");
                    emailData.setEmailfrom("informacion@fintra.co");
                    emailData.setEmailto(correosMT);
                    emailData.setEmailcopyto("");
                    emailData.setEmailHiddencopyto("");
                    emailData.setEmailsubject("Negocio " + neg.getCod_negocio() + " Decidido");
                    emailData.setEmailbody(mensajeMT);
                    emailData.setLastupdat(new Timestamp(System.currentTimeMillis()));
                    emailData.setSenderName("sender name");
                    emailData.setRemarks("remark2");
                    emailData.setTipo("E");
                    emailData.setRutaArchivo(ruta);
                    emailData.setNombreArchivo("");

                    EmailSendingEngineService emailSendingEngineService = new EmailSendingEngineService();
                    emailSendingEngineService.send(emailData);
                    emailSendingEngineService = null;
                }
            }
        } catch (SQLException e) {
            return "<span class='fila'>Error :" + e.getMessage() + "</span>";
        }

        return ruta;
    }

    /**
     * visualiza el listado de una tabla en un select
     */
    public String cargarLista() {
        String valor = request.getParameter("valor") != null ? request.getParameter("valor") : "";
        String id = request.getParameter("id") != null ? request.getParameter("id") : "";
        String tabla = request.getParameter("tabla") != null ? request.getParameter("tabla") : "";
        ArrayList lista = null;
        String cadenaWrite = "";
        try {
            lista = trazaService.listadotablagen(tabla, valor);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<select id='" + id + "' name='" + id + "' style='width:20em;'>";
        if (lista.isEmpty()) {
            cadenaWrite += "<option value=''>...</option>";
        } else {
            String[] dato1 = null;
            for (int i = 0; i < lista.size(); i++) {
                dato1 = ((String) lista.get(i)).split(";_;");
                cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
            }
        }
        cadenaWrite += "</select>";
        return cadenaWrite;

    }
    
     /**
     * Muestra listado de negocios de tipo automotor relacionados para un cliente
     */
    private void buscarNegociosRelAut() {

        try {
            String cliente =request.getParameter("cliente")!= null ? request.getParameter("cliente"):"";
            String negocio =request.getParameter("negocio")!= null ? request.getParameter("negocio"):"";
            ArrayList listNegociosRel =trazaService.buscarNegocioRelAut(cliente,negocio);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listNegociosRel) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(NegocioTrazabilidadAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
    
     /**
     * Actualiza campo negocio relacionado de la tabla negocios de acuerdo al tipo(Seguro o GPS)
     */
    private void actualizarNegocioRel() throws Exception {
        String json = "";
        TransaccionService tservice = new TransaccionService(usuario.getBd());
        try {
            String cod_negocio = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg"):"";
            String cod_neg_rel = request.getParameter("neg_rel") != null ? request.getParameter("neg_rel"):"";
            String tipo_neg_rel= request.getParameter("neg_type")!= null ? request.getParameter("neg_type"):"";
            
            tservice.crearStatement();
            tservice.getSt().addBatch(trazaService.actualizarNegocioRel(cod_negocio, cod_neg_rel, tipo_neg_rel));
            tservice.getSt().addBatch(trazaService.actualizarCicloNegocioRel(cod_negocio, cod_neg_rel));
            tservice.execute();
            json = "{\"respuesta\":\"OK\"}";           
            
        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
        }finally {
                this.printlnResponse(json, "application/json;");
                tservice.closeAll();
        }

    }
    
    public String guardarArchivoEnDirectorio() {
        String negocio = "";
        String form = "";
        String act = "";
        String coment = "";
        String coment_stby = "";
        String concepto = "";
        String causal = "";
        String rutaArchivo = "";
        String nombre_archivo = "";
        
        try {
          
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes");//se establece la ruta de la imagen
            String folderName = "fintracredit/";

            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();             
                String fieldtem = "", valortem = "";


                while (it.hasNext()) {
                    FileItem fileItem = (FileItem) it.next();
                    if (fileItem.isFormField()) {

                        fieldtem = fileItem.getFieldName();
                        valortem = fileItem.getString();
                        if (fieldtem.equals("negocio")) {
                            negocio = valortem;
                        }
                        if (fieldtem.equals("form")) {
                            form = valortem;
                        }
                        if (fieldtem.equals("act")) {
                            act = valortem;
                        }
                        if (fieldtem.equals("refcomentarios")) {
                            coment = valortem;
                        }
                        if (fieldtem.equals("refconcepto")) {
                            concepto = valortem;
                        }
                        
                        if (fieldtem.equals("refcausal")) {
                            causal = valortem;
                        }
                        if (fieldtem.equals("refcomentarios_stby")) {
                            coment_stby = valortem;
                        }

                    }
                    if (!(fileItem.isFormField())) {
                        String nombreArchivo = cleanAcentos(fileItem.getName());
                        boolean sw2=false, swExiste=false;
                        if(!nombreArchivo.equals("")){
                        String[] nombrearreglo = nombreArchivo.split("\\\\");
                        String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];                       
                        nombre_archivo= nombreArchivoremix;
                        rutaArchivo = directorioArchivos + folderName + negocio;
                        File f = new File(rutaArchivo);
                        if(! f.exists() ){
                           folderName="fintracredit1/";
                           this.createDir(directorioArchivos+folderName);
                        }                        
                        rutaArchivo = directorioArchivos + folderName + negocio;
                        this.createDir(rutaArchivo);

                        File archivo = new File(rutaArchivo + "/" + nombre_archivo);                   
                        
                        swExiste = archivo.exists();                        
                        fileItem.write(archivo);

                         sw2=archivo.exists();
                        }else{
                            sw2=true;
                        }
                        if (sw2) {
                            
                            TransaccionService tService = new TransaccionService(usuario.getBd());
                            tService.crearStatement();
                            NegocioTrazabilidad sw = trazaService.buscarTraza(Integer.parseInt(form), act);
                            boolean ok = true;
                            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                            negtraza.setActividad(act);
                            negtraza.setNumeroSolicitud(form);
                            negtraza.setUsuario(usuario.getLogin());
                            negtraza.setCausal(causal);
                            negtraza.setComentarios(coment);
                            negtraza.setDstrct(usuario.getDstrct());
                            negtraza.setConcepto(concepto);
                            negtraza.setCodNeg(negocio);
                            negtraza.setComentario_standby(coment_stby);
                            next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&redi=S";
                            if (concepto.equals("DEVOLVER_RAD")||(concepto.equals("DEVOLVER_SOL"))&&(!model.Negociossvc.getTipoProceso(negocio).equals("DSR"))) {
                                if (sw == null) {
                                    if(concepto.equals("DEVOLVER_RAD"))
                                    {
                                       act = "LIQ";
                                    }
                                    if(concepto.equals("DEVOLVER_SOL"))
                                    {
                                        act = "SOL";
                                        tService.getSt().addBatch(trazaService.updateEstadoForm(form, "B"));
                                    }                                                                                                             
                                    
                                } else {
                                    ok = false;
                                }
                            }


                            if (ok) {

                                if(!nombreArchivo.equals("")){
                                    String accion = "";
                                    accion = (swExiste) ? "Actualizar" : "";
                                    guardarRegistroEnBd(negocio, directorioArchivos+ folderName, nombre_archivo, accion, "negocio");
                                }
                                try {    
                                    
                                    if (act.equals("REF")) {
                                        if (!trazaService.tieneAnalista(negocio)) {
                                            ArrayList<String> analistas = trazaService.buscarAnalistas();
                                            if (analistas.size() > 0) {
                                                String analista = "";
                                                int sec = trazaService.secUltimoAnalistaProducto(form);
                                                if (sec == analistas.size()) {
                                                    analista = analistas.get(0);
                                                    sec = 1;
                                                } else {
                                                    analista = analistas.get(sec);
                                                    sec = sec + 1;
                                                }
                                                NegocioAnalista neg_analista = new NegocioAnalista();
                                                neg_analista.setDstrct(usuario.getDstrct());
                                                neg_analista.setCodNeg(negocio);
                                                neg_analista.setAnalista(analista);
                                                neg_analista.setSecuencia(sec);
                                                tService.getSt().addBatch(trazaService.insertNegocioAnalista(neg_analista));
                                            }
                                        }
                                    }

                                    if (concepto.equals("STANDBY")) {
                                        act = "STBY";
                                    }



                                    tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                                    tService.getSt().addBatch(trazaService.updateActividad(negocio, act));
                                    
                                    //Aqui vamos a validar si si el negocio tine el titulo valor digitado.
                                    boolean validaDocs = true;
                                    if (model.Negociossvc.getTipoProceso(negocio).equals("DSR")) {
                                        ArrayList<SolicitudDocumentos> lista_docs = gsaserv.buscarDocsSolicitud(Integer.parseInt(form));
                                        //Vamos a validar que existan los documentos
                                        int contador = 0;
                                        if (!lista_docs.isEmpty()) {

                                            for (int i = 0; i < lista_docs.size(); i++) {
                                                SolicitudDocumentos solicitudDocumentos = lista_docs.get(i);

                                                if (solicitudDocumentos.getNumTitulo().equals("")
                                                        || solicitudDocumentos.getNumTitulo().equals("0")
                                                        || solicitudDocumentos.getNumTitulo().length() < 3) {

                                                    contador++;
                                                }
                                            }

                                            validaDocs = (contador > 0) ? false : true;
                                        } else {

                                            validaDocs = false;
                                        }
                                    }

                                    
                                    //Ejecutamos todos los cambios en la BD.

                                    if (validaDocs) {

                                        tService.execute();
                                        if(concepto.equals("STANDBY")){     
                                            Negocios neg = null;
                                            neg = negserv.buscarNegocio(negocio);                                           
                                            String id_und_negocio = trazaService.getUnidadConvenio(neg.getId_convenio());
                                            String unidad_negocio = trazaService.getUnidadNegocio(neg.getId_convenio());
                                            String cliente = trazaService.getNombreCliente(negocio);
                                            String receiversType = (id_und_negocio.equals("1")) ? "Comerciales_Micro" : (id_und_negocio.equals("22")) ? "Comerciales_Libranza" : "Comerciales_Fenalco"; 
                                            String desccausal = trazaService.getCausalStandBy(causal);
                                            String MyMessage = "Negocio No " + negocio + " a nombre del cliente " + cliente + " fue colocado en STANDBY con causal " + desccausal + " por el usuario " + usuario.getLogin() + " debido a: " + coment;
                                            String mailsToSend = trazaService.getCorreosToSendStandBy(receiversType);
                                            String asunto = "Cambio estado Negocio " + negocio + " a StandBy perteneciente a la linea de negocio " +unidad_negocio;
                                            Thread hiloEmail = new Thread(new EmailSenderService(trazaService, "STANDBY", MyMessage, mailsToSend, asunto, usuario.getLogin()), "hilo");
                                            hiloEmail.start();                                                           
                                        }
                                       

                                    }else {
                                        next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&msg="
                                                + "<span class='fila'>Error: El Negocio no tiene registrado el numero de cheque.</span>";
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw new ServletException("Error en NegocioTrazabilidadAction: " + e.getMessage());
                                }
                            } else {
                                next = "/jsp/fenalco/negocios/Conceptos.jsp?negocio=" + negocio + "&form=" + form + "&act=" + act + "&msg=" + "<span class='fila'>Error: No se puede efectuar una segunda devolucion</span>";
                            }
                        }
                    }

                }
            }
        } catch (Exception ee) {
            System.out.println("error:" + ee.toString() + "__" + ee.getMessage());
        }
        return next;
    }
    
    public void guardarRegistroEnBd(String negocio, String namecarpeta, String nomarchivo, String id, String tipo) {
        try {

            Dictionary fields = new Hashtable();
            fields.put("actividad", "023");
            fields.put("tipoDocumento", "031");
            fields.put("documento", negocio);
            fields.put("agencia", "OP");
            fields.put("usuario", usuario.getLogin());
            fields.put("archivo", nomarchivo );
            
            this.insertRegistroArchivo(namecarpeta, fields,id,tipo);           

        } catch (Exception ee) {
            System.out.println("error" + ee.toString() + "_" + ee.getMessage());
        }
    }
    
    public String insertRegistroArchivo(String ruta,  Dictionary fields,String idxx3,String tipito) throws ServletException{
        String estado   = "";
        String fileName = (String)fields.get("archivo");
       
        try{

        model.negociosApplusService.insertRegistroArchivo(ruta,fields,idxx3,tipito,"");
        estado = "Archivo "+ fileName +" ha sido guardado <br>";

        }catch(Exception e){
            System.out.println("errorrcito"+e.toString()+"__"+e.getMessage());
         estado =  "Error al insertar registro en BD "+  fileName  +"<br>" + e.getMessage() ;
        }
        return estado;
  }
    
    public static String cleanAcentos(String texto) {   
      
          String original = "��������������u���������������������':";
          String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcCu   ";

	        if (texto != null) {
	            //Recorro la cadena y remplazo los caracteres originales por aquellos sin acentos

	            for (int i = 0; i < original.length(); i++ ) {

	                texto = texto.replace(original.charAt(i), ascii.charAt(i));

	            }

	          //Establezco todos los caracteres a min�scula.
	          texto = texto.toLowerCase();

	        }    
      
	        return texto;
    }
    
    /**
     * Muestra listado de negocios en estado de StandBy
     */
    private void buscarNegociosStandBy() {

        try {    
            //String unidad_negocio  = request.getParameter("unidad_negocio").replaceAll("-_-", "#");
            String unidad_negocio = (request.getParameter("unidad_negocio") != null) ? request.getParameter("unidad_negocio"): "";
            String json;
            json = trazaService.buscarNegociosStandBy(unidad_negocio);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(NegocioTrazabilidadAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Actualiza campo actividad de la tabla negocios. Lo devuelve a la actividad previa al estado de StandBy
     */
    private void actualizarNegocioStandBy() throws Exception {
        String json = "";
        TransaccionService tservice = new TransaccionService(usuario.getBd());
        try {
            String negocio = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            String num_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String cod_actividad = request.getParameter("cod_actividad") != null ? request.getParameter("cod_actividad") : "";
            //String actividad = request.getParameter("actividad") != null ? request.getParameter("actividad") : "";
            String actividad = request.getParameter("actividad") != null ? request.getParameter("actividad").substring(0,3) : "";
            String comment = request.getParameter("comentario") != null ? request.getParameter("comentario") : "";
            String usuario_stby = request.getParameter("usuario_stby") != null ? request.getParameter("usuario_stby") : "";
            
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            negtraza.setActividad(actividad);
            negtraza.setNumeroSolicitud(num_solicitud);
            negtraza.setUsuario(usuario.getLogin());
            negtraza.setCausal("");
            negtraza.setComentarios(comment);
            negtraza.setDstrct(usuario.getDstrct());
            negtraza.setConcepto("DEV_STANDBY");
            negtraza.setCodNeg(negocio);
            
            tservice.crearStatement();
            tservice.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
            tservice.getSt().addBatch(trazaService.updateActividad(negocio, cod_actividad));        
            tservice.execute();
            //Envio de Email
            Negocios neg = null;
            neg = negserv.buscarNegocio(negocio);    
            String id_und_negocio = trazaService.getUnidadConvenio(neg.getId_convenio());
            String cliente = trazaService.getNombreCliente(negocio);
            String receiversType = (id_und_negocio.equals("1")) ? "Coordinadores_Micro" : (id_und_negocio.equals("22")) ? "Coordinadores_Libranza" : "Coordinadores_Fenalco"; 
            String MyMessage = "Negocio No " + negocio + " a nombre del cliente " + cliente + " fue devuelto a fabrica en etapa " + actividad + " por el usuario " + usuario.getLogin() + ". Observaciones: " + comment + ", favor seguir con el debido proceso.";
            String asunto = "Devoluci�n de negocio " + negocio + " a fabrica des de estado de StandBy";
            String mailsToSend = trazaService.getCorreosToSendStandBy(receiversType);
            String mailasesor = trazaService.getMailUser(usuario_stby);
            Thread hiloEmail = new Thread(new EmailSenderService(trazaService, "STANDBY", MyMessage, mailsToSend+","+mailasesor, asunto, usuario.getLogin()),"hilo");     
            hiloEmail.start();      
            json = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            this.printlnResponse(json, "application/json;");
            tservice.closeAll();
        }

    }
    
    /**
     * Muestra configuracion de causales de StandBy
     */
    private void buscarConfigCausalesStandBy() {

        try {    
            String json;
            json = trazaService.buscarConfigCausalesStandBy();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(NegocioTrazabilidadAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Inserta una nueva causal de StandBy
     */
    private void insertarCausalStandBy() throws Exception {
        String json = "{}";    
        try {
            String nombre = (request.getParameter("nombre") != null) ? request.getParameter("nombre"): "";
            String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion"): "";
            json = trazaService.insertCausalStandBy(nombre, descripcion, usuario);      
        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        }finally{
            this.printlnResponse(json, "application/json;");
        }
    }

    /**
     * Actualiza el plazo para la causal de StandBy
    */
    private void actualizarCausalStandBy() throws Exception {
        String json = "{}";      
        try {
            String codigo = (request.getParameter("codigo") != null) ? request.getParameter("codigo"): "";
            String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion"): "";                   
            json = trazaService.actualizarCausalStandBy(codigo, descripcion, usuario);           
        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        }finally{
            this.printlnResponse(json, "application/json;");
        }
    }
    
    /**
     * Activa o Inactiva la causal de StandBy
     */
    private void activaInactivaCausalStandBy() throws Exception {
        String json = "{}";    
        try {
            String codigo = (request.getParameter("codigo") != null) ? request.getParameter("codigo"): "";
            String estado = (request.getParameter("estadoCausal") != null) ? request.getParameter("estadoCausal"): "";
            json = trazaService.activaInactivaCausalStandBy(codigo, estado, usuario);           
        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        }finally{
            this.printlnResponse(json, "application/json;");
        }
    }

    private String updateNegociosLiquidacion() throws Exception {
        /* negocio  act  coment  causa concep  perfeccionar  coment_stby;*/
        String negocio = request.getParameter("negocio");
        String act = request.getParameter("act") != null ? request.getParameter("act") : "";
        String coment = request.getParameter("coment") != null ? request.getParameter("coment") : "";
        String causa = request.getParameter("causa") != null ? request.getParameter("causa") : "";
        String concep = request.getParameter("concep") != null ? request.getParameter("concep") : "";
        String coment_stby = request.getParameter("coment_stby") != null ? request.getParameter("coment_stby") : "";
        String form = request.getParameter("form");
        String ruta = "";
        String estado_neg="";
        String actividad="SOL";
        
        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            
            negtraza.setNumeroSolicitud(form);
            negtraza.setActividad(act);
            negtraza.setUsuario(usuario.getLogin());
            negtraza.setCausal(causa);
            negtraza.setComentarios(coment);
            negtraza.setDstrct(usuario.getDstrct());
            negtraza.setConcepto(concep);
            negtraza.setCodNeg(negocio);
            negtraza.setComentario_standby(coment_stby);
            
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            
            if (act.equals("DEC")){
                estado_neg="E";
                ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=8";
            }else{
                estado_neg="F";
                ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=9";
            }
            
        tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
        tService.getSt().addBatch(trazaService.updateActividadnegocio(negocio,estado_neg,actividad));
        
         tService.execute();
         
           return ruta;
         
    }

    private void agregarComentarioStandBy() throws Exception {
        String numeroSolicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud"): "";
        String codigoNegocio = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg"): "";        
        String actividad = request.getParameter("actividad") != null ? request.getParameter("actividad").substring(0, 3) : "";
        String comentario = request.getParameter("comentario") != null ? request.getParameter("comentario") : "";
        String json = "{\"error\":\"No se encontr� el causal de la anterior traza\"}", causal = trazaService.obtenerCausalUltimoStandBy(numeroSolicitud, codigoNegocio, actividad);
        
        if (causal != null) {
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            negtraza.setActividad(actividad);
            negtraza.setNumeroSolicitud(numeroSolicitud);
            negtraza.setUsuario(usuario.getLogin());
            negtraza.setCausal(causal);
            negtraza.setComentarios("");
            negtraza.setComentario_standby(comentario);
            negtraza.setDstrct(usuario.getDstrct());
            negtraza.setConcepto("STANDBY");
            negtraza.setCodNeg(codigoNegocio);

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            try {
                tservice.crearStatement();
                tservice.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                tservice.execute();
                json = "{\"respuesta\":\"OK\"}";
            } catch (SQLException ex) {
                json = "{\"error\":\"" + ex.getMessage() + "\"}";
            } finally {
                tservice.closeAll();
            }
        }
        this.printlnResponse(json, "application/json;");
    }
    
    private String devolver_decision() throws Exception {
        /* negocio  act  coment  causa concep  perfeccionar  coment_stby;*/
        String negocio = request.getParameter("negocio");
        String act = request.getParameter("act") != null ? request.getParameter("act") : "";
        String coment = request.getParameter("coment") != null ? request.getParameter("coment") : "";
        String causa = request.getParameter("causa") != null ? request.getParameter("causa") : "";
        String concep = request.getParameter("concep") != null ? request.getParameter("concep") : "";
        String coment_stby = request.getParameter("coment_stby") != null ? request.getParameter("coment_stby") : "";
        String form = request.getParameter("form");
        String ruta = "";
        String estado_neg="P";
        String actividad="ANA";
        
        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            
            negtraza.setNumeroSolicitud(form);
            negtraza.setActividad(act);
            negtraza.setUsuario(usuario.getLogin());
            negtraza.setCausal(causa);
            negtraza.setComentarios(coment);
            negtraza.setDstrct(usuario.getDstrct());
            negtraza.setConcepto(concep);
            negtraza.setCodNeg(negocio);
            negtraza.setComentario_standby(coment_stby);
            
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            
                ruta = "/controller?estado=Negocios&accion=Ver&op=5&vista=9";
                        
        tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
        tService.getSt().addBatch(trazaService.updateActividadnegocio(negocio,estado_neg,actividad));
        
         tService.execute();
         
           return ruta;
         
    }
    
    
     private void exportarExcelNegocioStamby() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("NegociosStby", "Reporte");
            String[] cabecera = {"Id Cliente", "Cliente", "Afiliado", "Negocio", "Id Solicitud", "Valor Negocio", "Valor Desembolso", "Fecha Negocio", "Fecha Desembolso", "Cod. Actividad", "Actividad", "Usuario", "Fecha StandBy", "Causal", "D�as Expira", "Tiempo Restante", "Comentarios", "Asesor"};

            short[] dimensiones = new short[]{
                5000, 5000, 8000, 7000, 7000, 5000, 5000, 5000, 5000, 5000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("id_cliente").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cliente").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("afiliado").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("numero_solicitud").getAsString(), letra);
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("vr_negocio").getAsString()), letra);  
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("vr_desembolso").getAsString()), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_desembolso").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cod_actividad_anterior").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("actividad").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("usuario_standby").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_standby").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("causal_standby").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("dias_exp").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("dias_restantes").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("comentarios").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("asesor").getAsString(), letra);                            
               
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd");
            url = request.getContextPath() + "/exportar/migracion/" + iduser.toUpperCase() + "/NegociosStby" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
     
     public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + iduser;
            File archivo = new File( rutaInformes );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }
     
     private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd");
            this.crearArchivo(nameFileParcial + fmt.format( new Date() ) +".xls", titulo);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
     
     private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 2;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }
     
     private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
     
     private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
     
     private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + iduser + "/" + nameFile;
            xls.nuevoLibro( rutaInformes + "/" + nameFile );
            header         = xls.nuevoEstilo("Tahoma", 10, true  , false, "text"  , HSSFColor.GREEN.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER, 2);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

   }
    
}
