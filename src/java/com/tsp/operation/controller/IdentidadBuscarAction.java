/*************************************************************************
 * Nombre:        IdentidadBuscarAction.java
 * Descripci�n:   Clase Action para Buscar Identidad
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         20 de julio de 2005, 05:56 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;


public class IdentidadBuscarAction extends Action{
    
    /** Creates a new instance of IdentidadBuscarAction */
    public IdentidadBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina")+"?men=OK";
        
        String ced = request.getParameter("ced");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            model.identidadService.buscarIdentidad(ced, usuario.getCia());
            model.vetoSvc.buscarCausaVeto(ced, "N");
            Identidad iden = model.identidadService.obtIdentidad();
            model.tdocumentoService.ListarTipoDocumentos();
            model.paisservice.listarpaises();
            model.ciudadService.searchTreMapCiudades();
            model.estadoservice.listarEstado(iden.getCodpais());
            model.ciudadservice.listarCiudades(iden.getCodpais(), iden.getCoddpto());
            model.tablaGenService.BuscarDatosTreemap("FUENTEREP");
            if(model.tablaGenService.obtenerInformacionDato( "USRVETONIT",usuario.getLogin()) !=null){
                session.setAttribute("USRIDEN","S");
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        ////System.out.println(next);
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
