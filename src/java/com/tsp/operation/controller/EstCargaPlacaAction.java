/******************************************************************
* Nombre                        EstCargaPlacaAction.java
* Descripci�n                   Clase Action para la tabla est_carga_placa
* Autor                         ricardo rosero
* Fecha                         13/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.lang.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 * @Clase ........ EstCargaPlacaAction.java
 * @Autor ........ Ricardo Rosero
 * @Fecha ........ 25 de noviembre de 2005, 10:32 AM
 * @version ...... 1.0
 * @Copyrigth .... Transporte sanchez Polo S.A.
 */
public class EstCargaPlacaAction extends Action {
    private Properties dbProps;
    
    /** Creates a new instance of EstCargaPlaca_Action */
    public EstCargaPlacaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String Mensaje = "";
        
        String next = "";
        
        HttpSession session = request.getSession();
        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            //System.out.println("entro2");
            InputStream is = PoolManager.getInstance().getClass().getResourceAsStream("com/tsp/operation/model/periodos.properties");
            dbProps = new Properties();
            
            //dbProps.load(is);
            dbProps.setProperty("clase1", Integer.toString(1));
            dbProps.setProperty("clase2", Integer.toString(2));
            dbProps.setProperty("clase3", Integer.toString(3));
            dbProps.setProperty("clase4", Integer.toString(4));
            dbProps.setProperty("clase5", Integer.toString(5));
            dbProps.setProperty("periodo1", request.getParameter("periodo1"));
            dbProps.setProperty("periodo2", request.getParameter("periodo2"));
            dbProps.setProperty("periodo3", request.getParameter("periodo3"));
            dbProps.setProperty("periodo4", request.getParameter("periodo4"));
            dbProps.setProperty("periodo5", request.getParameter("periodo5"));
            
            
            File file = new File(application.getRealPath("/WEB-INF/classes/com/tsp/operation/model/"),"periodos.properties");
            FileOutputStream ostream = new FileOutputStream(file);
            dbProps.store(ostream,"Periodos del sistema");
            ostream.close();
            Mensaje = "Proceso ejecutado satisfactoriamente";
            next="/jsp/masivo/estadisticas/est_car_placa.jsp?Mensaje="+Mensaje;
        }
        catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
            
        }
        
        this.dispatchRequest(next);
        
    }
}