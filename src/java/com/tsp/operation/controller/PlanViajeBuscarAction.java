   /***************************************
    * Nombre Clase ............. PlanViajeBuscarAction.java
    * Descripci�n  .. . . . . .  Busqueda plan de viaje de la planilla
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  07/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.*;



public class PlanViajeBuscarAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
  
    public void run() throws ServletException, InformationException  {
        
        try{
            model.PlanViajeSvc.init();
            String next            = "/jsp/trafico/planviaje/frmPlanViaje.jsp";
            String distrito        = request.getParameter("txtDistrito").trim().toUpperCase();
            String planilla        = request.getParameter("txtPlanilla").trim().toUpperCase();
            String opcion          = request.getParameter("rdOpcion");
            PlanDeViaje planViaje  = new PlanDeViaje();
            PlanDeViaje planAux    = null;
            String comentario      = "";
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");//AMATURANA 21.12.2006
            boolean permiso = false;
            
            
            
           if(opcion!=null){
                 opcion = opcion.toUpperCase();
                 model.PlanViajeSvc.formatBtn(opcion);
                 planAux = model.PlanViajeSvc.search_planViaje(planilla,distrito);      // Verificamos que ya este creado
                 
                 //************ AMATURANA 21.12.2006 *****************
                 
                 String dpto = usuario.getDpto();
                 
                 logger.info("Login: " + usuario.getLogin() + " - Dpto: " + dpto );
                 
                 if( dpto.equals("traf") || model.rmtService.permitirPlanViaje(planilla) ) {
                     logger.info("El usuario: " + usuario.getLogin() + " puede crear/modificar plan de viaje.");
                     //***************************************************
                     if(planAux == null){
                         
                         planAux = model.PlanViajeSvc.formar_planViaje(planilla,distrito);  // Formamos de la inf. de la oc
                         model.PlanViajeSvc.formatBtn("SAVE");
                         comentario = "El plan de Viaje para la planilla "+ planilla +" aun No ha sido creado ";
                         
                     }
                     else{
                         
                         if( opcion.equals("SAVE") )
                             model.PlanViajeSvc.formatBtn("SEARCH");
                         
                         
                         if (  planAux.getEstado().equals("C")  )
                             model.PlanViajeSvc.formatBtn("DELETE");
                     }
                     
                     permiso = true;
                 }
            }
            
            if(planAux==null){
                next = "/jsp/trafico/planviaje/planViaje.jsp";
                comentario = "La planilla " + planilla + ", No se encuentra registrada...";
            }
            else
               planViaje   = planAux;
            
            if( !permiso ){
                next = "/jsp/trafico/planviaje/planViaje.jsp";
                comentario = "El Plan de Viaje para la planilla " + planilla + "<br> no puede ser creado/modificado. Tiene seguimiento en tr�fico. <br>Solo los usuarios de Tr�fico pueden hacerlo...";
            }
            
            
            model.PlanViajeSvc.setPlan(planViaje);
            request.setAttribute("planViajeObject", planViaje );
            
            
            next+= "?msj="+ comentario + "&rdOpcion=" + opcion;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);

            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
    }
    
    
    
}
