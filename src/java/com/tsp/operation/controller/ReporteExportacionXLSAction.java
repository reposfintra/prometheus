/******************************************************************************
 *      Nombre Clase.................   ReporteExportacionAction.java
 *      Descripci�n..................   Genera el reporte de Importaci�n-Exportaci�n
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   26.08.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteExportacionXLSAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteExportacionXLSAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String nit = request.getParameter("nit");     
                
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        String pag  = "/jsp/masivo/reportes/ReporteExportacion.jsp?msg=Se ha iniciado la generaci�n de los reportes seleccionados.";
        String next = pag;
        
        String[] docs = request.getParameterValues("LOV");
        
        if ( docs!= null ){
            logger.info("DOCUMENTOS: " + docs.length);
            for( int i=0; i<docs.length; i++){
                logger.info("GENERAR: " + docs[i]);
            }
            
            try{
                /**
                 * Generamos el reporte
                 */
                ReporteExportacionMabeTh hilo = new ReporteExportacionMabeTh();
                hilo.start(usuario.getLogin(), docs);
                
            }catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
        } else {
            next  = "/jsp/masivo/reportes/ReporteExportacion.jsp?msg=No selecciono ning�n reporte.";
        }
        
        this.dispatchRequest(next);
    }
    
}
