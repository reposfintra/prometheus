/*
 * SubirArchivoAction.java
 *
 * Created on 22 de febrero de 2005, 02:11 PM
 */

package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class SubirArchivoAction extends Action{
    
    /** Creates a new instance of SubirArchivoAction */
    public SubirArchivoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/Sincronizacion/Mensaje.jsp";
        String archivo = request.getParameter("archivo");
        File f = new File("/exportar/masivo/Sincronizacion/servidor/recibido/"+archivo);
        if(f.exists()){
            ProcesoSubirJournal psj = new ProcesoSubirJournal();
            psj.start(model, archivo);
        }
        
        this.dispatchRequest(next);
    }
    
    
}
