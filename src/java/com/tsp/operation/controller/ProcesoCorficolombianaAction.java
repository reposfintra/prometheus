/*
 * ProcesoCorficolombianaAction.java
 *
 * Created on 28 de julio de 2008, 09:12 AM
 */
package com.tsp.operation.controller;

import com.tsp.finanzas.contab.model.DAO.CmcDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionNegociosDAO;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import com.tsp.operation.model.services.ChequeXFacturaService;
import com.tsp.util.Util;

/**
 *
 * @author  Tmolina
 */
public class ProcesoCorficolombianaAction extends Action {

    private String[] facturas;
    private int items;//los items del comprobante;
    private double interes;
    List<Negocios> lista = new LinkedList<Negocios>();
    ContabilizacionNegociosDAO dao;
    public List listitems;

    /** Creates a new instance of ProcesoCorficolombianaAction */
    public ProcesoCorficolombianaAction() {
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");    //Info Usuario
        facturas = request.getParameterValues("facturasC");//Array de las Facturas a endozar a Corficolombiana
        String fecha_actual = Util.getFechaActual_String(4);
        dao = new ContabilizacionNegociosDAO(usuario.getBd());
        List<String> negocios = new LinkedList();
        SeriesService serie = new SeriesService(usuario.getBd());
        com.tsp.finanzas.contab.model.beans.Cmc cmcob = new com.tsp.finanzas.contab.model.beans.Cmc();
        String docType = "";
        double temp;
        String msj = "";
        String nit_fiducia = request.getParameter("nit_fiducia") == null ? "" : request.getParameter("nit_fiducia");
        String prefijo_fiducia = "";
        String hc_fiducia = "";
        String next = "/jsp/corficolombiana/procesoCorficolombianaMsgError.jsp?marco=no";//error

        try {


            //Facturas q se van a endozar a Corficolombiana

            TransaccionService tService = new TransaccionService(usuario.getBd());
            ChequeXFacturaService md = new ChequeXFacturaService(usuario.getBd());
            String monedaLocal = md.getMonedaLocal(usuario.getDstrct());

            //Factura nueva
            factura fac = new factura();  //Objeto factura nueva
            fac.setTipo_documento("FAC");  // tipo_documento

            //Proceso de creacion de la Nota de Ajuste para cada uno de las facturas
            Ingreso ing = new Ingreso();
            ing.setReg_status("");
            ing.setDstrct(usuario.getDstrct());
            ing.setTipo_documento("ICA");
            ing.setConcepto("FE");
            ing.setTipo_ingreso("C");
            ing.setFecha_consignacion(fecha_actual);
            ing.setFecha_ingreso(fecha_actual);
            ing.setBranch_code("");
            ing.setBank_account_no("");
            ing.setCodmoneda(monedaLocal);
            ing.setBase(usuario.getBase());
            ing.setAgencia_ingreso(usuario.getId_agencia());
            ing.setPeriodo("000000");
            ing.setDescripcion_ingreso("");
            ing.setVlr_tasa(1);
            ing.setFecha_tasa(fecha_actual);
            ing.setCant_item(1);
            ing.setTransaccion(0);
            ing.setTransaccion_anulacion(0);
            ing.setCreation_user(usuario.getLogin());
            ing.setCreation_date(Util.fechaActualTIMESTAMP());
            ing.setLast_update(Util.fechaActualTIMESTAMP());
            ing.setUser_update(usuario.getLogin());
            ing.setNro_consignacion("");
            ing.setTasaDolBol(0);
            ing.setAbc("");

            //Campos Comunes en Ingreso_detalle
            Ingreso_detalle ingDetalle = new Ingreso_detalle(); //Detalle del Ingreso
            ingDetalle.setDistrito(usuario.getDstrct());
            ingDetalle.setTipo_doc("FAC");
            ingDetalle.setItem(1);
            ingDetalle.setTipo_documento("ICA");
            ingDetalle.setCodigo_reteica("");
            ingDetalle.setValor_reteica(0);
            ingDetalle.setValor_reteica_me(0);
            ingDetalle.setValor_diferencia(0);
            ingDetalle.setCreation_user(usuario.getLogin());
            ingDetalle.setCreation_date(Util.fechaActualTIMESTAMP());
            ingDetalle.setBase(usuario.getBase());
            ingDetalle.setCodigo_retefuente("");
            ingDetalle.setValor_retefuente(0);
            ingDetalle.setValor_retefuente_me(0);
            ingDetalle.setTipo_aux("RD");
            ingDetalle.setValor_tasa(1);


            model.facturaService.inicializarNits();
            for (int i = 0; i < facturas.length; i++) {

                String[] fact = facturas[i].split(";");
                String id_convenio = fact[7];
                GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());

                if (!id_convenio.equals("null")) {
                    Convenio convenio = convdao.buscar_convenio(usuario.getBd(), id_convenio);
                    for (int k = 0; k < convenio.getConvenioCxc().size(); k++) {
                        String prefijo = serie.obtenerPrefix(convenio.getConvenioCxc().get(k).getPrefijo_factura());
                        if (prefijo.equals(fact[0].substring(0, 2))) {
                            cmcob = new com.tsp.finanzas.contab.model.beans.Cmc();
                            cmcob.setDstrct(usuario.getDstrct());
                            cmcob.setTipodoc(fact[8]);
                            cmcob.setCmc(convenio.getConvenioCxc().get(k).getHc_cxc());
                            CmcDAO cmcdao = new CmcDAO(usuario.getBd());
                            cmcdao.setCmc(cmcob);
                            cmcdao.obtenerCmcDoc();
                            String cuentacmc = cmcdao.getCmc().getCuenta();
                            for (int j = 0; j < convenio.getConvenioCxc().get(k).getConvenioCxcFiducias().size(); j++) {
                                if (convenio.getConvenioCxc().get(k).getConvenioCxcFiducias().get(j).getNit_fiducia().equals(nit_fiducia)) {
                                    fac.setCmc(convenio.getConvenioCxc().get(k).getConvenioCxcFiducias().get(j).getHc_cxc_fiducia()); // cmc
                                    fac.setCcuenta(convenio.getConvenioCxc().get(k).getConvenioCxcFiducias().get(j).getCuenta_cxc_fiducia());
                                    docType = serie.obtenerPrefix(convenio.getConvenioCxc().get(k).getConvenioCxcFiducias().get(j).getPrefijo_cxc_fiducia());
                                    ing.setCuenta(convenio.getConvenioCxc().get(k).getConvenioCxcFiducias().get(j).getCuenta_cxc_fiducia());
                                }
                            }
                            ing.setCmc(convenio.getConvenioCxc().get(k).getHc_cxc());
                            ingDetalle.setCuenta(cuentacmc);
                            k = convenio.getConvenioCxc().size();
                        }
                    }
                    for (int k = 0; k < convenio.getConvenioFiducias().size(); k++) {
                        if (convenio.getConvenioFiducias().get(k).getNit_fiducia().equals(nit_fiducia)) {
                            prefijo_fiducia = convenio.getConvenioFiducias().get(k).getPrefijo_dif_fiducia();
                            hc_fiducia = convenio.getConvenioFiducias().get(k).getHc_dif_fiducia();
                        }
                    }
                    tService.crearStatement();
                    temp = Double.valueOf(fact[3]).doubleValue();

                    fac.setNit(fact[2]); //nit
                    fac.setCodcli(fact[1]); //codcli
                    fac.setFecha_factura(fact[4]); //fecha_factura original
                    fac.setDescripcion(fact[5]);  //descripcion
                    fac.setValor_factura(temp);  //valor_factura
                    fac.setDocumento(docType + fact[0].substring(2));

                    tService.getSt().addBatch(model.procesoCorficolombianaService.endozarFacturas(fact[0]));
                    
                    ArrayList<String> listaQuerys=model.facturaService.ingresarNewFactura(fac, fact[0], usuario.getLogin());
            
                    tService= getAddBatch(tService, listaQuerys);
                 
                    ing.setAuxiliar("RD-" + fact[2]);

                    //Ingreso
                    ing.setCodcli(fact[1]);
                    ing.setNitcli(fact[2]);
                    ing.setVlr_ingreso(temp);
                    ing.setVlr_ingreso_me(temp);
                    tService.getSt().addBatch(model.ingresoService.insertarNewIngreso(ing));

                    //Ingreso Detalle

                    ingDetalle.setNit_cliente(fact[2]);

                    ingDetalle.setValor_ingreso(temp);
                    ingDetalle.setValor_ingreso_me(temp);
                    ingDetalle.setDocumento(fact[0]);
                    ingDetalle.setFactura(fact[0]);
                    ingDetalle.setAuxiliar(fact[2]);
                    ingDetalle.setFecha_factura(fact[4]);
                    ingDetalle.setDescripcion_factura(fact[5]);
                    ingDetalle.setValor_saldo_factura_me(temp);
                    tService.getSt().addBatch(model.ingreso_detalleService.insertarNewIngresoDetalle(ingDetalle));
                    tService.getSt().addBatch(model.ingresoService.incrementarSerie("ICAC")); //Incrementa el numero de ingreso
                    tService.getSt().addBatch(model.ingreso_detalleService.updateSaldoFactura(temp, temp, fact[0], usuario.getDstrct(), "FAC"));
                    //Ejecucion del query


                    /****************************** Generacion de Comprobante por cada negocio ****************************************************/
                    //1. OBTENER LA LISTA DE NEGOCIOS
                    //2. BUSCAR CADA UNO DE LOS NEGOCIOS Y SUS DETALLES
                    ArrayList<String> error = null; 
                    if (!negocios.contains(fact[6])) {

                        if (!model.procesoCorficolombianaService.existeComprobante(fact[6])) {
                          
                            error=new ArrayList<String>();
                            listitems = new LinkedList();
                            String periodo = fecha_actual.substring(0, 7).replaceAll("-", "");
                            Comprobantes comprobante = (Comprobantes) model.procesoCorficolombianaService.buscarNegocio(fact[6], usuario.getDstrct());
                            comprobante.setAbc("N/A");
                            comprobante.setBase(usuario.getBase());
                            comprobante.setDocrelacionado(comprobante.getNumdoc());
                            comprobante.setTdoc_rel("NEG");
                            comprobante.setTipodoc("PI");
                            comprobante.setNumdoc(comprobante.getTipodoc() + comprobante.getNumdoc().substring(2));
                            comprobante.setMoneda(monedaLocal);
                            comprobante.setSucursal("BQ");
                            comprobante.setFechadoc(fecha_actual);//fecha del comprobante
                            comprobante.setPeriodo(periodo);//periodo del comprobante preguntar que fecha debe quedar el comprobante
                            comprobante.setTipo_operacion("001");
                            comprobante.setUsuario(usuario.getLogin());
                            comprobante.setAprobador(usuario.getLogin());
                            comprobante.setTipo("C");

                            cmcob = new com.tsp.finanzas.contab.model.beans.Cmc();
                            cmcob.setDstrct(usuario.getDstrct());
                            cmcob.setTipodoc(comprobante.getTipodoc());
                            cmcob.setCmc(hc_fiducia);
                            CmcDAO cmcdao = new CmcDAO(usuario.getBd());
                            cmcdao.setCmc(cmcob);
                            cmcdao.obtenerCmcDoc();
                            String cuenta = cmcdao.getCmc().getCuenta();
                            interes = dao.getSumaInteres(comprobante.getDocrelacionado());

                            itemscopy(comprobante, cuenta, interes, "C");
                            itemscopy(comprobante, convenio.getCuenta_interes(), interes, "D");

                            comprobante.setTotal_items(2);
                            comprobante.setTotal_debito(interes);
                            comprobante.setTotal_credito(interes);
                            comprobante.setItems(listitems);
                            error = model.procesoCorficolombianaService.insertar(comprobante, usuario.getLogin());
                            if (!error.isEmpty()) {
                                
                                if ( nit_fiducia.equals("8001408878") ) {
                                    tService=getAddBatch(tService, error);
                       
                                    tService.getSt().addBatch(model.procesoCorficolombianaService.activarProcesoIntereses(serie.obtenerPrefix(prefijo_fiducia), prefijo_fiducia, convenio.getPrefijo_diferidos(),fact[6], usuario.getLogin()));
                                    tService.getSt().addBatch(model.procesoCorficolombianaService.anularProcesoIntereses(convenio.getPrefijo_diferidos(),fact[6], usuario.getLogin()));
                                    //buscar comprobantes si existen crear comprobantes de reversion
                                    ArrayList<String> reversion = model.procesoCorficolombianaService.comprobantesReversion(fact[6], convenio, usuario, nit_fiducia);
                                    if (!reversion.isEmpty()) {
                                       tService= getAddBatch(tService, reversion);
                                        //tService.getSt().addBatch(reversion);
                                    }
                                    error = null;
                                }  
                                error=null;
                                
                            } else {
                                msj += "Error lista vacia comprobantes" + "\n";
                            }
                        }
                        negocios.add(fact[6]);
                    }
                    if (error ==null) {
                        tService.execute();
                    }

                    //
                } else {
                    msj += fact[0] + " la factura no tiene convenio\n";
                }

            }


            if (msj.equals("")) {
                msj = "Proceso Exitoso";
            }
            next = "/jsp/corficolombiana/procesoCorficolombianaMsgExito.jsp?marco=no&msj=" + msj;

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }

    public void itemscopy(Comprobantes neg, String parm, double parm2, String tipo) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getTercero());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getDocrelacionado());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setTdoc_rel(neg.getTdoc_rel());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(dao.getAccountDES(parm));
            if (tipo.equals("D")) {
                item.setTotal_credito(0.0);
                item.setTotal_debito(parm2);
            } else {
                item.setTotal_credito(parm2);
                item.setTotal_debito(0.0);
            }
            item.setAuxiliar("");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }
}
