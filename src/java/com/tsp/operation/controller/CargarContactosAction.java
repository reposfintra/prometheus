/*
 * CargarContactosAction.java
 *
 * Created on 15 de julio de 2005, 10:58
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Rodrigo
 */
public class CargarContactosAction extends Action{
    
    /** Creates a new instance of Campos_jspInsertAction */
    public CargarContactosAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=ok";
        this.dispatchRequest(next);
    }
    
}