
/**
* Autor  : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
**/
package com.tsp.operation.controller;

import com.tsp.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Proveedor;

//librerias pdf
import com.lowagie.text.*;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Font;
import com.tsp.operation.model.DAOS.RolesDAO;





import java.text.SimpleDateFormat;
import java.lang.*;



import java.io.*;
import java.sql.*;
import java.util.*;


public class NegociosVerAction  extends Action{
    
   //protected HttpServletRequest request;

          //jpinedo
      String  url_logo="fintrapdf.gif";


      Calendar c = new GregorianCalendar();
      String dia = Integer.toString(c.get(Calendar.DATE));
      String mes = Integer.toString(c.get(Calendar.MONTH));
      String annio = Integer.toString(c.get(Calendar.YEAR));
      NegociosGenService negserv;
      EgresoService egserv;
      Proveedor prov = new Proveedor();
      ProveedorService ps;
      NitSot objnit = new NitSot();
      NitDAO nitDAO;
	  //Constantes para identificar los roles de los analistas
      private final String ADMINISTRADOR = "1";
    public NegociosVerAction () {
    }
    
    
    
    
    public void run() throws ServletException, InformationException
    { 
       HttpSession session  = request.getSession();
       Usuario usuario = (Usuario)session.getAttribute("Usuario");
       
       negserv = new NegociosGenService (usuario.getBd());
       egserv = new EgresoService (usuario.getBd());
       nitDAO = new NitDAO(usuario.getBd());
       ps= new ProveedorService(usuario.getBd());
       
       String op=request.getParameter ("op"); 
       String ckest=null,est="",ckdate=null,fi="",ff="",cknita=null,nita="",cknoma=null,noma="",cknitc=null,nitc="",cknomc=null,nomc="",cknitcod=null,nitcod="",cknomcod=null,nomcod="";
       String ckfap=null,fi1="",ff1="",ckfde=null,fi2="",ff2="", exportar="";
       String ckcodxc=null,codxc="", vista="";
       String next="/jsp/fenalco/negocios/ver_negocios.jsp";
       Vector fp=new Vector();
       String mensaje="";
       boolean redirect = true;
        String respuesta="";
       if(op.equals("3"))
       {
                fp=(Vector)session.getAttribute("vneg");
                ListadoNegociosTh hilo = new ListadoNegociosTh();  
                hilo.start(model,fp, usuario);  
                next = "/jsp/fenalco/negocios/analisisCarteraMsg.jsp";
       }
       if(op.equals("2"))
       {
           next="/jsp/fenalco/negocios/ver_negocios.jsp";   
           ckfap=request.getParameter ("ckfap"); 
           fi1=(request.getParameter ("fecini2") != null )?request.getParameter ("fecini2").toUpperCase():"";
           ff1=(request.getParameter ("fecfin2") != null )?request.getParameter ("fecfin2").toUpperCase():"";
           ckfde=request.getParameter ("ckfde"); 
           fi2=(request.getParameter ("fecini3") != null )?request.getParameter ("fecini3").toUpperCase():"";
           ff2=(request.getParameter ("fecfin3") != null )?request.getParameter ("fecfin3").toUpperCase():"";
           ckest=request.getParameter ("ckestado"); 
           est=(request.getParameter ("estados") != null )?request.getParameter ("estados").toUpperCase():"";
           ckdate=request.getParameter ("ckdate"); 
           fi=(request.getParameter ("fecini") != null )?request.getParameter ("fecini").toUpperCase():"";
           ff=(request.getParameter ("fecfin") != null )?request.getParameter ("fecfin").toUpperCase():"";
           cknitc=request.getParameter ("cknitc");
           nitc=(request.getParameter ("nitc") != null )?request.getParameter ("nitc").toUpperCase():"";
           cknomc=request.getParameter ("cknomc");
           nomc=(request.getParameter ("nomc") != null )?request.getParameter ("nomc").toUpperCase():"";
           cknitcod=request.getParameter ("cknitcod");
           nitcod=(request.getParameter ("nitcod") != null )?request.getParameter ("nitcod").toUpperCase():"";
           cknomcod=request.getParameter ("cknomcod");
           nomcod=(request.getParameter ("nomcod") != null )?request.getParameter ("nomcod").toUpperCase():"";

           vista=(request.getParameter ("vista") != null )?request.getParameter ("vista"):"";
           if(vista.equals("2")){
               next="/jsp/fenalco/negocios/ver_negocios.jsp?vista=13";           }
           int sw=0;
           session.setAttribute("afil", nitc);
           String user=usuario.getLogin().toUpperCase();

            try{
                model.Negociossvc.listneg2(ckest,est,ckdate,fi,ff,ckfap,fi1,ff1,ckfde,fi2,ff2,cknitc,nitc,cknomc,nomc,cknitcod,nitcod,cknomcod,nomcod,ckcodxc,user, vista);
            }
            catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
       }
      if(op.equals("1"))
           {
               next="/jsp/fenalco/negocios/ver_negocios.jsp";
               vista=(request.getParameter ("vista") != null )?request.getParameter ("vista"):"";
               int sw=0;
               ckest=request.getParameter ("ckestado");
               est=(request.getParameter ("estados") != null )?request.getParameter ("estados").toUpperCase():"";

               ckdate=request.getParameter ("ckdate");
               fi=(request.getParameter ("fecini") != null )?request.getParameter ("fecini").toUpperCase():"";
               ff=(request.getParameter ("fecfin") != null )?request.getParameter ("fecfin").toUpperCase():"";

               ckfap=request.getParameter ("ckfap");
               fi1=(request.getParameter ("fecini2") != null )?request.getParameter ("fecini2").toUpperCase():"";
               ff1=(request.getParameter ("fecfin2") != null )?request.getParameter ("fecfin2").toUpperCase():"";

               ckfde=request.getParameter ("ckfde");
               fi2=(request.getParameter ("fecini3") != null )?request.getParameter ("fecini3").toUpperCase():"";
               ff2=(request.getParameter ("fecfin3") != null )?request.getParameter ("fecfin3").toUpperCase():"";

               cknita=request.getParameter ("cknita");
               nita=(request.getParameter ("nita") != null )?request.getParameter ("nita").toUpperCase():"";
               cknoma=request.getParameter ("cknoma");
               noma=(request.getParameter ("noma") != null )?request.getParameter ("noma").toUpperCase():"";
               cknitc=request.getParameter ("cknitc");
               nitc=(request.getParameter ("nitc") != null )?request.getParameter ("nitc").toUpperCase():"";
               cknomc=request.getParameter ("cknomc");
               nomc=(request.getParameter ("nomc") != null )?request.getParameter ("nomc").toUpperCase():"";
               cknitcod=request.getParameter ("cknitcod");
               nitcod=(request.getParameter ("nitcod") != null )?request.getParameter ("nitcod").toUpperCase():"";
               cknomcod=request.getParameter ("cknomcod");
               nomcod=(request.getParameter ("nomcod") != null )?request.getParameter ("nomcod").toUpperCase():"";
               ckcodxc=request.getParameter ("ckcodxc");
               codxc=(request.getParameter ("codxc") != null )?request.getParameter ("codxc").toUpperCase():"";
               session.setAttribute("afil", nitc);
                exportar=(request.getParameter ("exportar") != null )?request.getParameter ("exportar"):"off";
               //System.out.println("Entro en la opcion de ver"+ckest);

                try{

                    model.Negociossvc.listneg(ckest,est,ckdate,fi,ff,ckfap,fi1,ff1,ckfde,fi2,ff2,cknita,nita,cknoma,noma,cknitc,nitc,cknomc,nomc,cknitcod,nitcod,cknomcod,nomcod,ckcodxc,codxc,vista);

                    if(exportar.equals("on"))
                    {
                    Vector listado =  model.Negociossvc.getListNeg();
                    ListadoNegociosTh hilo = new ListadoNegociosTh();
                    hilo.start(model,listado, usuario);
                    mensaje="Se ha iniciado la exportaci�n del reporte en formato MS Excel.";
                    next = "/jsp/fenalco/negocios/ConsultarNegociosClientes.jsp?mensaje="+mensaje;
                    }
                }
                catch (Exception e){
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                   // throw new ServletException(e.getMessage());
                }
           }
           if(op.equals("4"))
           {
                 redirect = false;
                 fp=(Vector)session.getAttribute("vneg");
                 respuesta="";
                 try
                 {


                      String nit =  request.getParameter("documento")!=null ? request.getParameter("documento") : "";
                      String fecha =  request.getParameter("fecha")!=null ? request.getParameter("fecha") : "";
                      String enviar_email =  request.getParameter("email")!=null ? request.getParameter("email") : "";

                      prov=ps.obtenerProveedor(nit , "");
                      objnit =nitDAO.searchNit(nit);




                     if(this.exportarReportePdf(usuario.getLogin(), fp,nit,fecha))
                     {
                         respuesta=Utility.getIcono(request.getContextPath(), 5)+ " PDF Generado Exitosamente.  ";
                         if(enviar_email.equals("S"))
                         {
                              String  directorio = this.directorioArchivo(usuario.getLogin());
                              String ruta=directorio+"/";
                             String  archivo="ReportePago"+prov.getC_payment_name()+".pdf";

                             if(this.envia_correo("reportedepago@fintravalores.com",objnit.getE_mail(), "asesor.comercial3@fintravalores.com;asistente.administrativo@fintravalores.com", "ReportePago", this.getMensaje(), ruta, archivo))
                             {
                                respuesta=Utility.getIcono(request.getContextPath(), 6)+"Se Envio  Reporte De Pago Por Correo Electronico Con Exito.  ";
                             }
                             else
                             {
                                 respuesta=Utility.getIcono(request.getContextPath(), 2)+"Error al momento de Enviar Reporte De Pago Por Correo Electronico ";
                             }

                         }

                     }
                     else
                     {
                      respuesta=Utility.getIcono(request.getContextPath(), 2)+"Error Generando PDF.  ";
                     }


                 }
                 catch (Exception e)
                 {
                     System.out.println("Error al redireccionar o escribir respuesta: "+e.toString());
                     respuesta="Error al momento de Generar PDF";
                     e.printStackTrace();
                 }


        }
            if (op.equals("5")) {
               String user=usuario.getLogin().toUpperCase();

		     /* Modificacion para asigancion automatica.
               Autor: Roberto Parra(JM) inicio.
               Descripcion: Buscar los perfiles del usuario que inicio sesion  
               y se identifica si alguno de estos es director de riesgos. */
             String rol="";
 
             try{
              RolesDAO Rol = new RolesDAO();
              rol =  Rol.ObtenerRol(user);

           }catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
             
           /* Modificacion para asigancion automatica
            Autor: Roberto Parra(JM) Fin */
               
               /*Validar si los usuarios son fenalco atlantico o bolivar
                *fenalco atlantico : true
                *fenalco bolivar: false
                */												
                String condicional = "";								   
				 /*
                //Este bloque se comento para el desarrollo del requerimiento de asignacion automatica
                  Comentado por: Roberto Parra(JM).
                boolean fenalco;
                if (model.Negociossvc.permisosFenalco(usuario.getLogin(), "PER_FENALC")) {

                    //validar permisos fenalco atlantico
                    fenalco = model.Negociossvc.permisosFenalco(usuario.getLogin(), "PERMISOSFA");
                    //aqui vamos agreagar una condicion
                    condicional = (fenalco == true) ? " AND conv.id_convenio in  "
                            + "(SELECT id_convenio FROM convenios  "
                            + "where id_convenio not in (7,8,15,21,10,11,12,13,6,14) and reg_status !='A') " : " AND conv.id_convenio in  "
                            + "(SELECT id_convenio FROM convenios  "
                            + "where id_convenio in (7,8,15,21,10,11,12,13,6,14) and reg_status !='A') ";
                } else {
                    //validar permisos fenalco bolivar
                    fenalco = model.Negociossvc.permisosFenalco(usuario.getLogin(), "PERMISOSFB");
                    //aqui vamos agreagar una condicion
                    condicional = (fenalco == true) ? " AND conv.id_convenio in  "
                            + "(SELECT id_convenio FROM convenios  "
                            + "where id_convenio  in (30,31,32,34) and reg_status !='A') " : "AND conv.id_convenio in  "
                            + "(SELECT id_convenio FROM convenios  "
                            + "where id_convenio  in (0) and reg_status !='A') ";
                }
               //validar si puede ver todos los convenios
                boolean todos;
                todos = model.Negociossvc.permisosFenalco(usuario.getLogin(),"TODOSCONV");
                if(todos){
                    condicional="";
                }
                  */
            try {
                vista = request.getParameter("vista") != null ? request.getParameter("vista") : "0";
                String filtroCupos=request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
                String remp = "";
                String tab = "";
                if (vista.equals("6")) {//radicacion
                    tab = "negocios n";
                    int permisos =Integer.parseInt(model.Negociossvc.permisosRAD(usuario.getLogin(), "TIPO_RAD"));
                    
                    switch (permisos){
                        case 0:
                            //Sin negocios
                            remp += " AND 1=0  ";
                           break;
                        case 1:
                            //fintra
                            remp += " AND ((conv.aval_tercero AND (n.estado_neg ='P' OR n.estado_neg ='V'))"
                                    + " OR ( not conv.aval_tercero AND ( n.estado_neg ='P' OR n.estado_neg ='Q' "
                                    + "OR n.estado_neg ='V' ) ) ) and ( ( n.actividad='REG' AND (n.tipo_proceso='PSR' "
                                    + "AND conv.tipo='Multiservicio')) or  ( n.tipo_proceso='PCR'  AND n.actividad='LIQ')  ) "
                                    + "AND conv.id_convenio in (SELECT id_convenio FROM convenios  "
                                    + "where id_convenio  in (3,4,5,7,8,14,15,16,17,18,20,21) and reg_status !='A')";
                            
                           
                           break;
                        case 2:
                            //hay que cambiar los convenios de piloto y productivo para fenalco bolivar
                            remp += " AND ((conv.aval_tercero AND (n.estado_neg ='P' OR n.estado_neg ='V'))"
                                    + " OR ( not conv.aval_tercero AND ( n.estado_neg ='P' OR n.estado_neg ='Q' "
                                    + "OR n.estado_neg ='V' ) ) ) and ( ( n.actividad='REG' AND (n.tipo_proceso='PSR' "
                                    + "AND conv.tipo='Multiservicio')) or  ( n.tipo_proceso='PCR'  AND n.actividad='LIQ')  ) "
                                    + "AND conv.id_convenio in (SELECT id_convenio FROM convenios  "
                                    + "where id_convenio  in (30,31,32,34) and reg_status !='A') ";                            
                            break;
                        case 3:
                            //microcredito
                            remp += " AND ((conv.aval_tercero AND (n.estado_neg ='P' OR n.estado_neg ='V'))"
                                    + " OR ( not conv.aval_tercero AND ( n.estado_neg ='P' OR n.estado_neg ='Q' "
                                    + "OR n.estado_neg ='V' ) ) ) and ( ( n.actividad='REG' AND (n.tipo_proceso='PSR' "
                                    + "AND conv.tipo='Multiservicio')) or  ( n.tipo_proceso='PCR'  AND n.actividad='LIQ')  ) "
                                    + "AND conv.id_convenio in (SELECT id_convenio FROM convenios  "
                                    + "where id_convenio  in (10,11,12,13) and reg_status !='A') ";                            
                            break;
                        case 4:
                            //todos
                            remp += " AND ((conv.aval_tercero AND (n.estado_neg ='P' OR n.estado_neg ='V')) OR "
                                    + "( not conv.aval_tercero AND ( n.estado_neg ='P' OR n.estado_neg ='Q' OR "
                                    + "n.estado_neg ='V' ) ) ) and ( ( n.actividad='REG' AND (n.tipo_proceso='PSR' "
                                    + "AND conv.tipo='Multiservicio')) or  ( n.tipo_proceso='PCR'  AND n.actividad='LIQ')  )  ";
                           break;
                    }
                    
                }
                if (vista.equals("7")) {//analisis
                    tab = "negocios n";
		 //En caso de que el analista no sea administrador solo podr� ver los negocios que tenga asignados
                    if(!rol.equals(ADMINISTRADOR)){
                      condicional="AND n.analista_asignado='"+user+"'";
                    }	
                    remp += " AND  (( (not conv.aval_tercero AND ( n.estado_neg ='P' OR n.estado_neg ='Q'  ) )  AND n.actividad='REF' AND  n.tipo_proceso='PCR') OR  ( n.tipo_proceso='PSR' AND n.actividad='REF' AND n.estado_neg ='P' )  OR ( n.tipo_proceso='DSR' AND n.actividad='REF' AND n.estado_neg ='P' ) OR (NOT conv.cat and conv.tipo='Microcredito' and n.estado_neg='P' and n.actividad='RAD') OR (conv.tipo='Libranza' and n.estado_neg='P' and n.actividad ='REF')) "+condicional;
                }
                if (vista.equals("8")) { // decision
                    tab = "negocios n inner join perfries_por_usuarios ppu on ppu.idusuario='"+usuario.getLogin()+"' "
                            + " /*inner join tablagen t on (t.table_type='USUAMONTO' and t.table_code='"+usuario.getLogin()+"')*/";
                   
                    /* Se agrega condicion para en caso de que hayan negocios que no cumplan con los rangos 
                   que puede decidir el analista no se le mostraran*/
                    remp += "and n.vr_negocio>ppu.monto_minimo_decision and n.vr_negocio<=ppu.monto_decision "
                            + "AND (( (not conv.aval_tercero AND ( n.estado_neg ='P' OR n.estado_neg ='Q'  ) )  AND n.actividad='ANA' /*AND n.vr_negocio>t.referencia::numeric AND n.vr_negocio<=t.dato::numeric*/ AND  n.tipo_proceso='PCR') "
                            + "OR ( n.tipo_proceso='PSR'  AND n.estado_neg ='P'  AND n.actividad='ANA' /*AND n.vr_negocio>t.referencia::numeric AND n.vr_negocio<=t.dato::numeric*/) "
                            + "OR ( n.tipo_proceso='DSR'  AND n.estado_neg ='P'  AND n.actividad='ANA' /*AND n.vr_negocio>t.referencia::numeric AND n.vr_negocio<=t.dato::numeric*/)) "+ condicional ;
                }
                if (vista.equals("9")) { // formalizacion
                    tab = "negocios n";
                    
                    int permisos =Integer.parseInt(model.Negociossvc.permisosRAD(usuario.getLogin(), "TIPO_FOR"));
                    
                    switch (permisos){
                        case 0:
                            remp += "AND  (conv.aval_tercero AND n.estado_neg ='V' AND "
                                    + "(n.actividad='REF' OR n.actividad='DEC' and n.pend_perfeccionamiento!='S' )"
                                    + " or (n.actividad='PERF' and estado_neg not in('R','D') )) OR "
                                    + "(( not conv.aval_tercero AND  n.tipo_proceso='PCR'  AND n.estado_neg ='V' ) AND  "
                                    + "(n.actividad='DEC' and n.pend_perfeccionamiento!='S' ) or (n.actividad='PERF' and estado_neg not in('R','D') ) )"
                                   // + "(select coalesce(sum(valor_comprar),0) from solicitud_obligaciones_comprar where numero_solicitud = (select numero_solicitud from solicitud_aval where cod_neg = n.cod_neg)) = 0 
                                    +"AND conv.id_convenio not in (38)";
                                    break; 
                        case 1: 
                             remp += "AND ( (conv.aval_tercero AND n.estado_neg ='V' AND "
                                    + "(n.actividad='REF' OR n.actividad='DEC' and n.pend_perfeccionamiento!='S' )"
                                    + " or (n.actividad='PERF' and estado_neg not in('R','D') )) OR "
                                    + "(( not conv.aval_tercero AND  n.tipo_proceso='PCR'  AND n.estado_neg ='V' ) AND  "
                                    + "(n.actividad='DEC' and n.pend_perfeccionamiento!='S' ) or "
                                    + "(n.actividad='PERF' and estado_neg not in('R','D') ) )) AND "
                                    + "conv.id_convenio in (SELECT id_convenio FROM convenios where id_convenio  not in (30,31,32,34) and reg_status !='A') AND "
                                    + "(select coalesce(sum(valor_comprar),0) from solicitud_obligaciones_comprar where numero_solicitud = (select numero_solicitud from solicitud_aval where cod_neg = n.cod_neg)) = 0";
                            
                            break;
                        case 2: 
                            
                            remp += "AND ( (conv.aval_tercero AND n.estado_neg ='V' AND "
                                    + "(n.actividad='REF' OR n.actividad='DEC' and n.pend_perfeccionamiento!='S' )"
                                    + " or (n.actividad='PERF' and estado_neg not in('R','D') )) OR "
                                    + "(( not conv.aval_tercero AND  n.tipo_proceso='PCR'  AND n.estado_neg ='V' ) AND  "
                                    + "(n.actividad='DEC' and n.pend_perfeccionamiento!='S' ) or "
                                    + "(n.actividad='PERF' and estado_neg not in('R','D') ) ))"
                                    + "AND conv.id_convenio in (SELECT id_convenio FROM convenios where id_convenio  in (30,31,32,34) and reg_status !='A') AND "
                                    + "(select coalesce(sum(valor_comprar),0) from solicitud_obligaciones_comprar where numero_solicitud = (select numero_solicitud from solicitud_aval where cod_neg = n.cod_neg)) = 0";
                            
                            break;
                    }
                    
                   
                }
                if (vista.equals("10")) { // referenciacion
                    tab = "negocios n";
					                    if(!rol.equals(ADMINISTRADOR)){
                      condicional="AND n.analista_asignado='"+user+"'";
                    }
                    remp += " AND (( n.tipo_proceso='PCR' AND ( (conv.aval_tercero AND ( n.estado_neg ='V' OR n.estado_neg ='P')  AND n.actividad='RAD') OR ( not conv.aval_tercero AND (n.estado_neg ='P' OR n.estado_neg ='Q' ) AND n.actividad='RAD' AND (conv.tipo='Consumo' OR (conv.cat and conv.tipo='Microcredito'  ))))) OR ( n.tipo_proceso='PSR' AND ( (conv.tipo='Multiservicio' AND  n.actividad='RAD') OR (n.actividad='SOL' AND conv.tipo='Consumo') ) AND n.estado_neg ='P'  ) OR ( conv.tipo='Consumo' AND n.tipo_proceso='DSR' AND n.estado_neg ='P' AND (n.actividad='SOL' or n.actividad='RAD')) OR ( conv.tipo='Libranza' AND n.estado_neg ='P' AND n.actividad='RAD')) "+condicional;
                }
                if (vista.equals("12")) {// generacion volantes de pago
                    tab = "negocios n";
                    remp += " AND  (conv.tipo='Microcredito'AND n.estado_neg ='T' AND replace(substring(f_desem,1,7 ),'-','') = '"+filtroCupos+"' ) ";
                }
                if (vista.equals("14")) {//indemnizacion
                    tab = "negocios n INNER JOIN solicitud_aval sa ON (n.cod_neg=sa.cod_neg) INNER JOIN solicitud_documentos sd ON (sa.numero_solicitud =sd.numero_solicitud)";
                    remp += " AND  (not conv.redescuento AND n.estado_neg ='A' AND sd.est_indemnizacion='VIG') ";
                }

                if (vista.equals("16")) {//Excepci�n a pol�tica
                    tab = "negocios n  ";// se quito esta linea INNER JOIN solicitud_aval sa ON (n.cod_neg=sa.cod_neg) porque en el xml ya esta 
                    remp += " and (conv.redescuento AND conv.aval_anombre "
                            + "and  n.estado_neg ='R'  ) "
                            + "and substring(n.creation_date,1,7)=substring(now(),1,7) "
                            + "and (validaCausalRechazo((select p.identificacion from solicitud_aval s, solicitud_persona p where  "
                            + "(tipo='S')and  s.numero_solicitud=p.numero_solicitud and"
                            + " s.cod_neg=n.cod_neg order by tipo asc limit 1),'LIMITE MAXIMO DE ENDEUDAMIENTO') "
                            + "OR n.cod_neg in (SELECT distinct cod_neg FROM negocios_trazabilidad where concepto ='RECHAZAD' and actividad ='LIQ' AND  causal ='21' ))";
                }
                if (vista.equals("17")) {//perfeccionammiento
                    tab = "negocios n";
                    remp += " AND n.estado_neg ='V'   AND n.pend_perfeccionamiento='S'  and n.actividad='DEC'"
                            +" AND (select coalesce(sum(valor_comprar),0) from solicitud_obligaciones_comprar where numero_solicitud = (select numero_solicitud from solicitud_aval where cod_neg = n.cod_neg)) = 0";
                }

                if (vista.equals("18")) {//radicacion afiliados
                    tab = "negocios n";
                    remp += "  AND (((conv.aval_tercero AND (n.estado_neg ='P' OR n.estado_neg ='V')) OR ( not conv.aval_tercero AND ( n.estado_neg ='P' OR n.estado_neg ='Q' OR n.estado_neg ='V' ) ) ) and              ( ( n.actividad='REG' AND (n.tipo_proceso='PSR' AND conv.tipo='Multiservicio')) or  ( n.tipo_proceso='PCR'  AND n.actividad='LIQ')  ))              and (n.nit_tercero in (select pc.nit_proveedor from usuario_prov_convenio upc join prov_convenio pc on "
                            + " upc.id_prov_convenio = pc.id_prov_convenio where upc.reg_status != 'A'and upc.idusuario = '"+user+"'))";
                }
                next = "/jsp/fenalco/negocios/ver_negocios.jsp?vista=" + vista;
                model.Negociossvc.listneg(tab, remp);

            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }


      try
      {
        if(redirect==false)
        {
            this.escribirResponse(respuesta);
        }
        else
        {
            this.dispatchRequest(next);
        }
      }
      catch (Exception e)
      {
          System.out.println("Error al redireccionar o escribir respuesta: "+e.toString());
          e.printStackTrace();
      }

    }







    protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }











    /**********************************Pdf Reporte Pagos***********************************************************/
    //jpinedo
    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons +/* "_" + fmt.format(new Date()) +*/ "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }


     private String directorioArchivo(String user) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;

    }








 /***********************************************carta pdf anticipos ************************************************************/
protected boolean exportarReportePdf(String userlogin,Vector  vn,String nit,String fecha) throws Exception {


            vn=this.getGrupoFecDesembolso(vn, nit, fecha);
            double vlr=negserv.Total_pagado(vn);


            Negocios  neg = (Negocios)vn.get(0);





           boolean generado = true;
            String directorio = "";String documento_detalle="";
            ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "ReportePago"+prov.getC_payment_name(), "pdf");
            Font fuente = new Font(Font.HELVETICA, 9);
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));

            documento.setFooter(this.getMyFooter());
            documento.open();
            documento.newPage();
            Image img= Image.getInstance(rb.getString("ruta")+"/images/"+url_logo);
            img.scaleToFit(200, 200);
            documento.add(img);
            documento.add(Chunk.NEWLINE);
            documento.add(new Paragraph("802.022.016-1",fuenteB));
            documento.add(Chunk.NEWLINE);
            Paragraph p= new Paragraph("REPORTE DE PAGOS",fuenteB);
            p.setAlignment(Element.ALIGN_CENTER);
            documento.add(p);
            documento.add(Chunk.NEWLINE);

           /*----------------------------------datos_afiliado--------------------------------------*/
            PdfPTable tabla_datos_pro = this.DatosAfiliado(prov,vn);
            tabla_datos_pro.setWidthPercentage(100);
            documento.add(tabla_datos_pro);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);

           /*----------------------------------lista_negocios--------------------------------------*/
            p= new Paragraph("DETALLE DE PAGOS",fuenteB);
            p.setAlignment(Element.ALIGN_LEFT);
            documento.add(p);
            documento.add(Chunk.NEWLINE);
            PdfPTable tabla_negocios = this.TrasferenciasNegocios(vn);
            tabla_negocios.setWidthPercentage(100);
            documento.add(tabla_negocios);
            documento.add(Chunk.NEWLINE);

            PdfPTable tabla_total = this.Total(vlr);
            tabla_total.setWidthPercentage(100);
            documento.add(tabla_total);
            documento.add(Chunk.NEWLINE);documento.add(Chunk.NEWLINE);

            /*if(neg.getBcocode().equals("FCM VPL COLPATR") || neg.getBcocode().equals("FID VALOR PLUS") )
            {
            p= new Paragraph("ORIGUEN DE TRANSFERENCIA:          FIDUCIARIA CORFICOLOMBIANA",fuenteB);
            p.setAlignment(Element.ALIGN_LEFT);
            documento.add(p);
            }*/
            documento.close();


        }
   catch (Exception e)
        {
            generado = false;
            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        System.out.println("fin elaboracion pdf solicitud de aval");
        //Envio de Correo

        return generado;
    }
   /*********************************************************************************************************************/


   private Document createDoc() {
        Document doc = new Document(PageSize.LETTER, 60, 40, 40, 40);//
        doc.addAuthor ("Fintra S.A");
        doc.addSubject ("Trasferencias");
        return doc;
    }






    /************************************** DATOS AFILIADO***************************************************/
  protected PdfPTable DatosAfiliado(Proveedor prv,Vector  vn) throws DocumentException {
        Negocios ng= (Negocios)vn.get(0);
        String fecha=ng.getFechatran().substring(0, 18);
        PdfPTable tabla_temp = new PdfPTable(4);

        double vlr=negserv.Total_pagado(vn);
      float[] medidaCeldas = {0.170f, 0.500f,0.170f,0.160f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("AFILIADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getC_payment_name(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("NIT", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getC_nit(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);




        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TITULAR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getNombre_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("NIT TITULAR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getCedula_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("BANCO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(prv.getC_banco_transfer(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("TIPO CTA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(prv.getC_tipo_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("CUENTA BANCARIA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(prv.getC_numero_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("FECHA DE PAGO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(fecha.substring(8, 10)+" de "+Utility.NombreMes(Integer.parseInt(fecha.substring(5, 7)))+" de "+fecha.substring(0, 4), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("TOTAL PAGADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(Utility.customFormat(this.Total_pagado(vn)), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

          return tabla_temp;
    }

    /************************************** trasferencias***************************************************/
  protected PdfPTable TrasferenciasNegocios( Vector   vn) throws DocumentException {

        double vlr=0,ip=0;
         PdfPTable tabla_temp = new PdfPTable(5);
        try
        {

        Negocios neg = new Negocios();


      float[] medidaCeldas = {0.05f, 0.135f, 0.160f, 0.500f, 0.200f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 9);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();

        // columna 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("N�", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Negocio", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Id Cliente", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Cliente", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

       for (int i=0;i<vn.size();i++)
       {
            neg = (Negocios)vn.get(i);
            neg.setVr_desem(egserv.getVlr(neg.getCod_negocio()));


            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(""+(i+1), fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(neg.getCod_negocio(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(neg.getCod_cli(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(neg.getNom_cli(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(neg.getVr_desem()), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);



        }
      }
        catch (Exception e)
        {

            System.out.println("error al generar pdf solicitud de aval: " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }











  /************************************** tabla total***************************************************/
  protected PdfPTable Total( double vlr  ) throws DocumentException {


        PdfPTable tabla_temp = new PdfPTable(5);
        float[] medidaCeldas = {0.05f, 0.135f, 0.160f, 0.500f, 0.200f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(3);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL PAGADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }






        public HeaderFooter getMyFooter() throws ServletException
        {
        try {
             Phrase p = new Phrase();
            Font fuente = new Font(Font.HELVETICA, 9);
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
             p.clear();


             p.add(new Paragraph("Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia\n",fuenteB));
             p.add(new Paragraph("PBX: 57 5 3679900 FAX: 57 5 3679906\n",fuenteB));
             p.add(new Paragraph("www.fintravalores.com\n",fuenteB));
             HeaderFooter header = new HeaderFooter(p, false);
             header.setBorder(Rectangle.NO_BORDER);
             header.setAlignment(Paragraph.ALIGN_CENTER);

             return header;
         } catch (Exception ex) {
            throw new ServletException("Header Error");
         }
     }



            /**jpinedo
     * devuelve un vector con los negocios trasferido en una misma hora filtrado por afiliado
     * @param documento La factura a buscar
     * @return Listado con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    public Vector getGrupoFecDesembolso(Vector vn,String nit,String fecha_desm) throws Exception{
        Vector tem =new Vector();
        try {

            for(int i=0;i<vn.size();i++)
            {
                Negocios  neg = (Negocios)vn.get(i);
                if(neg.getBcod().equals(nit) && neg.getFechatran().substring(0, 18).equals(fecha_desm))
                {
                    tem.add(neg);
                }
                neg=null;
            }


        }
        catch (Exception e) {
            throw new Exception("Error en observacionesFact en GestionCarteraService.java: "+e.toString());
        }
        return tem;
    }





      public double Total_pagado( Vector vn  ) throws DocumentException {

      double vlr=0;
       Negocios neg = new Negocios();

        for (int i=0;i<vn.size();i++)
        {
            neg = (Negocios)vn.get(i);
             vlr=vlr+neg.getVr_desem();
        }
       return vlr;
    }

    public boolean envia_correo(String from,String to,String copia,String asunto,String msg,String ruta,String archivo)
    {
     boolean enviado=false;
     try
       {


         /************************* Envio de Correo*************************/
           Email2 emailData = null;
           String ahora = new java.util.Date().toString();
            emailData=new Email2();
            emailData.setEmailId(12345);
            emailData.setRecstatus("A");//A
            emailData.setEmailcode( "emailcode");
            emailData.setEmailfrom(from);
            emailData.setEmailto(to );
            emailData.setEmailcopyto(copia);
            emailData.setEmailHiddencopyto("");
            emailData.setEmailsubject(asunto);//"WebServiceMultiple_Fintra2" );
            emailData.setEmailbody(msg);
            emailData.setLastupdat(new Timestamp(System.currentTimeMillis()) );
            emailData.setSenderName( "sender name" );
            emailData.setRemarks("remark2");
            emailData.setTipo("E");
            emailData.setRutaArchivo(ruta);
            emailData.setNombreArchivo(archivo);


          
              EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService();
           enviado= emailSendingEngineService.send(emailData);
            emailSendingEngineService=null;//091206
            /*************************Fin Envio de Correo*************************/
           }
           catch(Exception e)
           {

               System.out.println("Error  "+e.getMessage());
           }

     return enviado;
    }



    public String getMensaje()
    {
        String mensaje="<span style='font-family: ; font-size:14px; line-height:25px '; >"
                + "Estimado Cliente. </br>Adjunto le estamos enviando el soporte de pago de los negocios desembolsados correspondienten a los clientes aqu&iacute; relacionados.<br />"
                + "Favor no responder este mensaje. Este se genera de manera autom&aacute;tica. En caso de requerir comunicarse con nosotros, favor conta&aacute;ctenos en los tel&eacute;fonos (5) 3679901 Ext, 1131 -1112."
                + "</span>";

        return mensaje;
    }


}

