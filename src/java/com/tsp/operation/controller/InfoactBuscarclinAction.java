/*****************************************************************************
 * Nombre ......................InfoactBuscarclinAction.java                 *
 * Descripci�n..................Clase Action para buscar cliente actividad   *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales       *
 * Fecha........................12 de septiembre de 2005, 01:09 PM           * 
 * Versi�n......................1.0                                          * 
 * Coyright.....................Transportes Sanchez Polo S.A.                *
 ****************************************************************************/
 
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class InfoactBuscarclinAction extends Action{
    
    /** Creates a new instance of InfoactBuscarclinAction */
    public InfoactBuscarclinAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String cod = request.getParameter("cliente");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String numpla = request.getParameter("numpla").toUpperCase();
        boolean sw = false;
        try{
            model.actplanService.buscarInfoplanilla(numpla);  
            sw = model.actplanService.BuscarCLientexPlanilla(numpla);
            if (sw){
                model.clientactSvc.setPla(model.actplanService.getPlan());
                next="/jsp/trafico/actividad/Infoactividad/ClientesxPlan.jsp";
            }
            else{
                next="/jsp/trafico/actividad/Infoactividad/BuscarClientexPlan.jsp?men=false";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        ////System.out.println(next);
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
