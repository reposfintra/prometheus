/*
 * FormularioBuscarAction.java
 *
 * Created on 30 de noviembre de 2006, 08:51 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  dbastidas
 */
public class FormularioBuscarAction extends Action {
    
    /** Creates a new instance of FormularioBuscarAction */
    public FormularioBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String formato = request.getParameter("formato");
        String tabla = "";
        try{
            Vector campos = model.formato_tablaService.getVecbusqueda();
            for(int i=0; i < campos.size() ; i++){
                Formato_tabla reg = (Formato_tabla) campos.get(i);
                tabla = reg.getTabla();
                if( reg.getTipo_campo().equals("DO") || reg.getTipo_campo().equals("INT") ){
                    reg.setValor_campo( request.getParameter(reg.getCampo_tabla())!=null?request.getParameter(reg.getCampo_tabla()).toUpperCase():"0" );
                }else{
                    reg.setValor_campo( request.getParameter(reg.getCampo_tabla())!=null?request.getParameter(reg.getCampo_tabla()).toUpperCase():"" );
                }
            }
            model.formato_tablaService.camposFormulario(formato);
            
            //agrego los campos de para factura migracion
            if(tabla.equals("factura_migracion")){
                System.out.println("Agrega los campos");
                Formato_tabla reg = new Formato_tabla();
                reg.setTabla(tabla);
                reg.setCampo_tabla("formato");
                reg.setTipo_campo("TXT");
                reg.setValor_campo(formato);
                reg.setPrimaria("S");
                reg.setValidar("N");
                campos.add(reg);
                //district
                reg = new Formato_tabla();
                reg.setTabla(tabla);
                reg.setCampo_tabla("dstrct");
                reg.setTipo_campo("TXT");
                reg.setValor_campo(usuario.getDstrct());
                reg.setPrimaria("S");
                reg.setValidar("N");
                campos.add(reg);
            }
            
            if(!model.formularioService.buscarRegistro( campos, model.formato_tablaService.getVecCampos()) ){
                model.formato_tablaService.setVecbusqueda(model.formularioService.eliminarCamposFacturaMigracion(campos));
                next="/jsp/general/form_tabla/BuscarDatos.jsp?mensaje=La busqueda no arrojo resultados";
            }
            if(tabla.equals("factura_migracion")){
                model.formato_tablaService.setVecCampos(model.formularioService.eliminarCamposFacturaMigracion(model.formato_tablaService.getVecCampos()));
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
