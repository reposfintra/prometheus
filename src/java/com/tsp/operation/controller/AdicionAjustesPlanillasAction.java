  /***************************************
    * Nombre Clase ............. AdicionAjustesPlanillasAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para aplicar ajuste a la planills
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  02/12/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.controller;





import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;



public class AdicionAjustesPlanillasAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {
        
        try{
             
         
               HttpSession session    = request.getSession();
               Usuario     usuario    = (Usuario)session.getAttribute("Usuario");
               String      user       = usuario.getLogin();
               String      distrito   = usuario.getDstrct();
               String      agencia    = usuario.getId_agencia();

               String      next       = "/colpapel/AdicionExtrafletes.jsp";
               String      msj        = "";


               String      evento      = request.getParameter("evento");  

               if( evento!= null ){
               
                    if(evento.equals("BUSCAR")){                        
                        
                        boolean hayTasa = model.tasaService.isTasaHoy( distrito );
                        if( hayTasa ){
                           
                             String oc = request.getParameter("oc");
                             model.AdicionAjustesPlanillasSvc.datosPlanillas(distrito, oc );
                             model.AdicionAjustesPlanillasSvc.loadExtrafletes();
                             model.AdicionAjustesPlanillasSvc.loadMonedas();
                             Ajuste    ajuste = model.AdicionAjustesPlanillasSvc.getAjuste();
                             if( ajuste==null )
                                  msj = "No se encontraron datos de la planilla. La planilla no existe  o est� anulada.";
                            
                        }else
                            next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();
                           
                           
                    } 
                    
                    
                    if(evento.equals("APLICAR")){
                          String extraflete  = request.getParameter("ajuste");
                          double valor       = Double.parseDouble( request.getParameter("valor") );
                          String moneda      = request.getParameter("moneda");
                       
                       // Adicionamos Ajuste:
                          msj = model.AdicionAjustesPlanillasSvc.agregarAjuste(distrito, user,  agencia, extraflete, valor, moneda);
                          
                       // Refrescamos lista de Movimientos:
                          Ajuste    ajuste  = model.AdicionAjustesPlanillasSvc.getAjuste();
                          List      mov     = model.AdicionAjustesPlanillasSvc.loadMovimientos(ajuste.getDistrito(), ajuste.getPlanilla() );
                          ajuste.setMovimientos( mov );
                          
                       // Valor planilla Moneda Local
                          double    vlrPlanilla = model.AdicionAjustesPlanillasSvc.getValorPlanilla( mov, ajuste.getValor(), ajuste.getMoneda(),  ajuste.getMonedaLocal() );
                          ajuste.setVlrPlanilla( vlrPlanilla );
                          
                          
                       // Saldo:
                          double vlrSaldo = model.AdicionAjustesPlanillasSvc.getValorSaldo( mov );
                          ajuste.setNeto( ajuste.getVlrPlanilla() + vlrSaldo );
                 
                 
                       // Valor planilla moneda OC
                       /*   double  vlrPlanillaMOC = model.AdicionAjustesPlanillasSvc.convert( vlrPlanilla, ajuste.getMonedaLocal(), ajuste.getMoneda(), ajuste.getMonedaLocal() );
                          ajuste.setVlrPlanillaMonOC( vlrPlanillaMOC );*/
                          
                    }  
                   
                    
               }               
               
               
               
              next += "?msj=" + msj ; 
               
              RequestDispatcher rd = application.getRequestDispatcher( next );
              if(rd == null)
                 throw new Exception("No se pudo encontrar "+ next);
              rd.forward(request, response); 
              
                            
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }    
        
        
    }
    
}
