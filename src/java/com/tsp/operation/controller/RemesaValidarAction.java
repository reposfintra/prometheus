/*
 * RemesaValidarAction.java
 *
 * Created on 15 de diciembre de 2004, 09:52 AM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class RemesaValidarAction extends Action{
    static Logger logger = Logger.getLogger(RemesaValidarAction.class);
    /** Creates a new instance of RemesaValidarAction */
    public RemesaValidarAction() {
    }
    public void buscarLista(String remesa)throws ServletException, InformationException {
        try{
            List planillas= model.remesaService.buscarPlanillas(remesa);
            List lplanillas= new LinkedList();
            Iterator it=planillas.iterator();
            while (it.hasNext()){
                Planilla pla = (Planilla) it.next();
                String nomruta= model.planillaService.getRutas(pla.getNumpla());
                pla.setCodruta(pla.getRuta_pla());
                pla.setRuta_pla(nomruta);
                lplanillas.add(pla);
            }
            request.setAttribute("planillas",lplanillas);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    public void run() throws ServletException, InformationException {
        
        String ruta = request.getParameter("rutaP");
        String sj = request.getParameter("standard");
        String planilla = request.getParameter("planilla");
        String next="/colpapel/agregarRemesaValidado.jsp";
        int sw=0;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        request.setAttribute("standar","#99cc99");
        request.setAttribute("porcent","#99cc99");
        request.setAttribute("ruta","#99cc99");
        request.setAttribute("toneladas","#99cc99");
        request.setAttribute("anticipo","#99cc99");
        
        if(request.getParameter("existe")!=null)
            next="/colpapel/RemesaExistente.jsp";
        
        if(request.getParameter("relacionar")!=null)
            next="/colpapel/relacionar.jsp";
        
        try{
            request.setAttribute("cfacturar","filaresaltada");
            //MAXIMOS VALORES EN TABLAGEN
            double maximo_valor =0;
            model.tablaGenService.obtenerRegistro("MAXCANSJ",request.getParameter("standard"), "");
            if(model.tablaGenService.getTblgen()!=null){
                try{
                    maximo_valor =Double.parseDouble(model.tablaGenService.getTblgen().getReferencia());
                    System.out.println("Maximo valor "+maximo_valor);
                }catch(NumberFormatException ne){
                    System.out.println("No es un numero "+model.tablaGenService.getTblgen().getReferencia());
                    maximo_valor = 0;
                }
            }
            
            logger.info("Empezamos a validar la remesa..");
            
            //SE BUSCA LA REMESA.
            if(request.getParameter("remesa")!=null){
                model.remesaService.buscaRemesa(request.getParameter("remesa"));
                if(model.remesaService.getRemesa()!=null){
                    logger.info("Encontro la remesa");
                    Remesa rem=model.remesaService.getRemesa();
                    String nombreR="";
                    String remitentes[]= rem.getRemitente().split(",");
                    for(int i=0; i<remitentes.length; i++){
                        nombreR=model.remidestService.getNombre(remitentes[i])+",";
                    }
                    String nombreD="";
                    String destinatarios[]= rem.getDestinatario().split(",");
                    for(int i=0; i<destinatarios.length; i++){
                        nombreD=model.remidestService.getNombre(destinatarios[i])+",";
                    }
                    
                    rem.setRemitente(nombreR);
                    rem.setDestinatario(nombreD);
                    request.setAttribute("remesa",rem);
                    
                    this.buscarLista(request.getParameter("remesa"));
                }
                
                
            }
            //SE BUSCA LA LISTA DE PLANILLAS QUE ESTAN RELACIONADAS CON LA REMESA
            
            if(request.getParameter("planilla")!=null){
                logger.info("ENTRO A LA OPCION DE PLANILLA..");
                model.planillaService.bucarColpapel(request.getParameter("planilla"));
                if(model.planillaService.getPlanilla()!=null){
                    
                    logger.info("Encontro la planilla...");
                    Planilla pla = model.planillaService.getPlanilla();
                    String nomruta= model.planillaService.getRutas(pla.getNumpla());
                    pla.setCodruta(pla.getRuta_pla());
                    pla.setRuta_pla(nomruta);
                    request.setAttribute("planilla",pla);
                    
                    model.movplaService.buscaMovpla(request.getParameter("planilla"),"01");
                    if(model.movplaService.getMovPla()!=null){
                        
                        Movpla movpla= model.movplaService.getMovPla();
                        request.setAttribute("movpla",movpla);
                    }
                    List remesas = model.planillaService.buscarRemesas(request.getParameter("planilla"));
                    request.setAttribute("remesas" , remesas);
                }
            }
            
            //SE UTILIZA SOLO CUANDO SE VA A CAMBIAR LA REMESA A UNA LISTA DE PLANILLAS
            Iterator it;
            int sw1=0;
            if(request.getParameter("varias")!=null){
                logger.info("ENTRO A LA OPCION DE VARIAS..");
                //BUSCO ALGUNA PLANILLA CON LA RUTA DIFERENTE A LA DE LA REMESA
                List plaerror = new LinkedList();
                if(session.getAttribute("plaerror")!=null){
                    logger.info("ENCONTRE LAS PLANILLAS ERROR EN LA SESION..");
                    plaerror= (List)session.getAttribute("plaerror");
                    /*it=plaerror.iterator();
                    while (it.hasNext()){
                        Planilla plani = (Planilla) it.next();
                        if(!model.stdjobcostoService.estaRuta(plani.getRuta_pla(), sj)){
                            sw1=1;
                        }
                    }*/
                }
                
                //ESTO SE HACE CUANDO LA REMESA EXISTE
                if(request.getParameter("lista")==null){
                    logger.info("ENTRO A LA OPCION DE LISTA..");
                    next="/colpapel/agregarListaPlanilla.jsp";
                    if(request.getAttribute("remesa")!=null){
                        logger.info("ENTRO A LA OPCION DE REMESA NO NULA..");
                        Remesa rem = (Remesa) request.getAttribute("remesa");
                        this.buscarLista(rem.getNumrem());
                    }
                    
                    if(sw1!=0){
                        request.setAttribute("standar","#ff0000");
                    }
                    else{
                        logger.info("VOY A AGREGAR DE UN LA REMESA..");
                        next="/colpapel/mostrarRemesa.jsp";
                        it=plaerror.iterator();
                        List plaagre = new LinkedList();
                        while (it.hasNext()){
                            Planilla plani = (Planilla) it.next();
                            logger.info("Relacionar la remesa "+request.getParameter("remesa")+" con la planilla "+plani.getNumpla());
                            if(!model.remplaService.estaRempla(plani.getNumpla(),request.getParameter("remesa"))){
                                RemPla rempla= new RemPla();
                                rempla.setPlanilla(plani.getNumpla());
                                rempla.setRemesa(request.getParameter("remesa"));
                                rempla.setDstrct(usuario.getDstrct());
                                //Agrego la Remesa-Planilla
                                model.tService.crearStatement();
                                model.tService.getSt().addBatch(model.remplaService.insertRemesa(rempla,usuario.getBase()));
                                plaagre.add(plani);
                            }
                            
                            
                        }
                        logger.info("Tama�o de la lista planilla "+plaagre.size());
                        model.tService.execute();
                        request.setAttribute("planillas",plaagre);
                    }
                }
                //ESTO SE HACE CUANDO LA REMESA ES NUEVA
                
                else{
                    if(sw1!=0){
                        request.setAttribute("standar","#ff0000");
                        next="/colpapel/agregarRemesaListaError.jsp";
                    }
                    else{
                        next="/colpapel/agregarRemesaListaValidado.jsp";
                    }
                }
            }
            //CUANDO ES SOLO UNA PLANILLA
            else{
                java.util.Enumeration enum1;
                String parametro;
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                int cant = 0;
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    if(parametro.indexOf("por")==0){
                        if(!parametro.equals("por"+request.getParameter("anular"))){
                            cant = Integer.parseInt(request.getParameter(parametro))+cant;
                        }
                    }
                }
                if(cant>100){
                    sw=1;
                    request.setAttribute("porcent","#ff0000");
                }
                
                //SE VALIDA LA CANTIDAD A FACTURAR
                float cfacturar =0;
                if(request.getParameter("cfacturar")!=null){
                    if(!request.getParameter("cfacturar").equals(""))
                        cfacturar = Float.parseFloat(request.getParameter("cfacturar"));
                }
                String uw = request.getParameter("uw")!=null? request.getParameter("uw"):"";
                double maxton=maximo_valor==0?50:maximo_valor;
                double maxmt3=maximo_valor==0?100:maximo_valor;
                double maxkl=maximo_valor==0?55000:maximo_valor;
                double maxgl=maximo_valor==0?500000:maximo_valor;
                double maxvi=maximo_valor==0?1:maximo_valor;
                
                if(uw.indexOf("T")==0){//toneladas
                    if(cfacturar>maxton){
                        sw=1;
                        request.setAttribute("cfacturar","filaroja");
                    }
                }
                if(uw.indexOf("M")==0){//MT3
                    if(cfacturar>maxmt3){
                        sw=1;
                        request.setAttribute("cfacturar","filaroja");
                    }
                }
                if(uw.indexOf("L")==0){//KILO
                    if(cfacturar>maxkl){
                        sw=1;
                        request.setAttribute("cfacturar","filaroja");
                    }
                }
                if(uw.indexOf("G")==0){//GALONES
                    if(cfacturar>maxgl){
                        sw=1;
                        request.setAttribute("cfacturar","filaroja");
                    }
                }
                if(uw.indexOf("V")==0 || uw.indexOf("W")==0){//VIAJES
                    if(cfacturar>maxvi){
                        sw=1;
                        request.setAttribute("cfacturar","filaroja");
                    }
                }
                
                if (sw==1){
                    next="/colpapel/agregarRemesaError.jsp";
                    if(request.getParameter("existe")!=null){
                        next="/colpapel/RemesaExistente.jsp";
                    }
                }
                else{
                    if(request.getParameter("existe")!=null || request.getParameter("relacionar")!=null ){
                        if(!model.remplaService.estaRempla(planilla,request.getParameter("remesa"))){
                            RemPla rempla= new RemPla();
                            rempla.setPlanilla(planilla);
                            rempla.setRemesa(request.getParameter("remesa"));
                            rempla.setDstrct(usuario.getDstrct());
                            //Agrego la Remesa-Planilla
                            model.tService.crearStatement();
                            model.tService.getSt().addBatch(model.remplaService.insertRemesa(rempla,usuario.getBase()));
                            model.tService.execute();
                            request.setAttribute("remesa" , null);
                            request.setAttribute("planilla",null);
                            request.setAttribute("standar","");
                        }
                    }
                    
                    
                }
            }
            Calendar mesActual = Calendar.getInstance();
            Date fecha_mes = mesActual.getTime();
            java.text.SimpleDateFormat s =  new java.text.SimpleDateFormat("yyyy-MM-dd");
            String fecha = s.format(fecha_mes);
            
            String monedaCia="PES";
            model.stdjobdetselService.searchStdJob(request.getParameter("standard"));
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                logger.info("Standard Job Det Set encontrado");
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                String moneda=stdjobdetsel.getCurrency();
                //se busca la tasa para la remesa
                if(!moneda.equals(monedaCia)){
                    
                    try{
                        model.tasaService.buscarValorTasa(monedaCia,moneda,monedaCia,fecha);
                    }catch(Exception et){
                        throw new ServletException(et.getMessage());
                    }
                    Tasa tasa = model.tasaService.obtenerTasa();
                    if(tasa==null){
                        logger.info("NO EXISTE TASA ");
                        String advertencias = " No se encontro tasa de cambio para la moneda "+moneda;
                        request.setAttribute("mensaje",advertencias);
                        sw=1;
                        request.setAttribute("cfacturar","filaroja");
                        next="/colpapel/agregarRemesaError.jsp";
                        
                    }
                    
                    
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
