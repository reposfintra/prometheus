/********************************************************************
 *      Nombre Clase.................   ReporteNitPlacaUpdatesAction.java
 *      Descripci�n..................   Genera el reporte de retroactivos actualizaciones
 *                                      de las tablas Nit y Placa
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   20.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteNitPlacaUpdatesAction extends Action{
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteNitPlacaUpdatesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter("FechaI");
        String fechaF = request.getParameter("FechaF");
        
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/hvida/reportes/ReporteNitPlacaUpdates.jsp?msg=" +
                "El Reporte se ha generado exitosamente en ReporteActualizacionesNitPlaca_" +
                FechaFormated1 + ".xls";
        
        try{
            
            model.repNitPlacasUpSvc.generarReporte(fechaI.substring(2).replaceAll("-",""), 
                    fechaF.substring(2).replaceAll("-",""));
            
            Vector cols_placa = model.placaService.obtenerNombresCamposPlaca();
            Vector placas = model.repNitPlacasUpSvc.getPlacas();
            Vector nits = model.repNitPlacasUpSvc.getNits();
            Vector cols_nit = model.identidadService.obtenerNombresCamposNit();
            
            ReporteNitPlacaUpdatesTh hilo = new ReporteNitPlacaUpdatesTh();
            
            hilo.start(placas, cols_placa, nits, cols_nit, usuario.getLogin(), fechaI, fechaF);
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
