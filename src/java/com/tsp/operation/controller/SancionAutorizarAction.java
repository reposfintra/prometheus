/***********************************************
 * Nombre clase: SancionAutorizarAction.java
 * Descripci�n: Accion para aprobar una sanci�n a la bd.
 * Autor: Jose de la rosa
 * Fecha: 09 de noviembre de 2005, 12:01 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class SancionAutorizarAction extends Action{
    
    /** Creates a new instance of SancionAutorizarAction */
    public SancionAutorizarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String next = "/jsp/cumplidos/sancion/autorizarSancion.jsp?reload=ok";
        double vr_autorizado = Double.parseDouble (request.getParameter ("c_valor_autorizado"));
        int cod_sancion = Integer.parseInt (request.getParameter ("c_cod_sancion"));
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        try{
            Sancion san = new Sancion ();
            san.setNumpla (numpla);
            san.setCod_sancion (cod_sancion);
            san.setValor_aprobado (vr_autorizado);
            san.setUsuario_aprobacion (usuario.getLogin ());
            model.sancionService.autorizarSancion (san);
            model.sancionService.searchSancion (numpla,cod_sancion);
            next = com.tsp.util.Util.LLamarVentana (next, "Autorizar Sanci�n");
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        
        this.dispatchRequest (next);
    }
    
}
