package com.tsp.operation.controller;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.services.ClientesVerService;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.GestionCondicionesService;
import com.tsp.operation.model.services.GestionConveniosService;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import javax.servlet.http.HttpSession;
import com.tsp.util.*;


//librerias pdf

import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import com.tsp.operation.model.*;
import com.tsp.util.Util;

//librerias pdf

import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.Barcode;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.sun.org.apache.bcel.internal.generic.AALOAD;
import com.tsp.opav.model.DAOS.ApplusDAO;
import com.tsp.operation.model.services.NegocioTrazabilidadService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.Phaser;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author geotech
 */
public class LiquidadorNegociosAction extends Action {

    Usuario usuario = null;
    private String next;
    ClientesVerService clvsrv;
    boolean sw_ver_saldos=false;
   boolean redirect = false;
   double valorCodigoConAval;
   String FechaPrimeraCuota;
   String mensaje = "";
   ArrayList<DocumentosNegAceptado> liqui =  new ArrayList();
   //ArrayList<Negocios> facturas = new ArrayList();

    public LiquidadorNegociosAction(HttpServletRequest request, Usuario user, String negocio, String fechapr) throws Exception {
        this.request=request;
        this.usuario=user;
        model=new Model(user.getBd());
        model.Negociossvc.setNegocio(model.Negociossvc.buscarNegocio(negocio)); 
        model.Negociossvc.setLiquidacion(model.Negociossvc.buscarDetallesNegocio(negocio)); 
        this.FechaPrimeraCuota=fechapr;
    }

    public LiquidadorNegociosAction() {
    }
    
    

    @Override
    public void run() throws ServletException, InformationException {

        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            clvsrv = new ClientesVerService(usuario.getBd());
            String perfil = clvsrv.getPerfil(usuario.getLogin());
            //sw_ver_saldos=clvsrv.ispermitted(perfil, "60");
            String opcion = request.getParameter("opcion");
            boolean redirect = false;
            String strRespuesta = "";

            next = "/jsp/fenalco/liquidadores/resultados_liquidador.jsp";

            try {
                if (opcion.equals("busConvUsuario")) {
                    strRespuesta = obtenerConveniosUsuario();
                    this.printlnResponse(strRespuesta, "text/plain");
                } else if (opcion.equals("buscarForm")) {
                    strRespuesta = obtenerForm();
                    this.printlnResponse(strRespuesta, "text/plain");
                } else if (opcion.equals("validartasa")) {
                    strRespuesta = validarTasa();
                    this.printlnResponse(strRespuesta, "text/plain");
                } else if (opcion.equals("buscarTitVlrConv")) {
                    strRespuesta = obtenerTitulosValorConvenio();
                    this.printlnResponse(strRespuesta, "text/plain");
                } else if (opcion.equals("buscarCiclosTercero")) {
                    strRespuesta = obtenerCiclosTercero();
                    this.printlnResponse(strRespuesta, "text/plain");
                } else if (opcion.equals("calcularLiquidacion")) {
                    calcularLiquidacion();
                    redirect = true;
                } else if (opcion.equals("Ejecutar")) {
                    consultarNegocio();
                    redirect = true;
                } else if (opcion.equals("busSectUsuario")) {
                    strRespuesta = obtenerSectoresUsuario();
                    this.printlnResponse(strRespuesta, "text/plain");
                } else if (opcion.equals("getLiquidacion")) {
                    this.getliquidacion();
                } else if (opcion.equals("financiarAval")) {
                    this.financiarAval();
                } else if (opcion.equals("exportarPlanPagos")) {
                    this.exportarPlanPagos();
                } else if (opcion.equals("ExportarPlanPagosFintra")) {
                    this.ExportarPlanPagos_Fintra();
                }                
                else if (opcion.equals("calcularLiquidacionConvenio")) {
                    LiquidacionConvenio();
                    redirect = true;
                } else if (opcion.equals("calcularReliquidacion")) {
                    this.calcularReliquidacion();
                    redirect = true;
                } else if (opcion.equals("reactivar_negocio")) {
                    this.reactivar_negocio();
                } else if (opcion.equals("valida_preaprobados")) {
                    this.ValidaPreaprobado();
                } else if (opcion.equals("buscarRenovacion")) {
                    strRespuesta = calcularRenovacion();
                    redirect = false;
                    this.printlnResponse(strRespuesta, "text/plain");
                    System.out.println("enviamos esta :" + strRespuesta);

                } else if (opcion.equals("existeConfigFianza")) {
                    strRespuesta = existeConfiguracionFianza();
                    this.printlnResponse(strRespuesta, "text/plain;");
                } else if (opcion.equals("obtenerFechaPago")) {
                    strRespuesta = obtenerFechaUltimoPago();
                    this.printlnResponse(strRespuesta, "text/plain;");
                } else if (opcion.equals("buscarPreaprobado")) {
                    strRespuesta=this.buscarPreaprobadoform();
                    this.printlnResponse(strRespuesta, "text/plain;");
                }

                if (redirect == true) {
                    this.dispatchRequest(next);
                }

            } catch (Exception e) {
                throw new ServletException(e.getMessage(), e);
            }
        } catch (Exception ex) {
            Logger.getLogger(LiquidadorNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
            this.dispatchRequest(next);
        }
    }

    private void consultarNegocio(){

        String nomcli = (String)request.getParameter("nomcli");
        String nit=(String)request.getParameter("nit");
        String codneg=(String)request.getParameter("cod_neg");
        String vista=request.getParameter("vista")!=null?request.getParameter("vista"):"";
        String form=request.getParameter("form")!=null?request.getParameter("form"):"";
        HttpSession session = request.getSession();
        session.setAttribute("codrech",codneg);
        String pagarex=request.getParameter("pagarex");
        String tipoconv=request.getParameter("tipoconv")!=null&&!request.getParameter("tipoconv").equals("null")?request.getParameter("tipoconv"):"Consumo";
        String estado=request.getParameter("state");
        String valor_total_poliza=request.getParameter("valor_total_poliza");

        next          = "/jsp/fenalco/negocios/negocios_liquidador.jsp";
        next += "?Nit=" + nit + "&nomcli=" + nomcli + "&pagarex=" + pagarex + "&vista=" + vista+"&form="+form+"&tipoconv="+tipoconv+"&state="+estado+"&valor_total_poliza="+valor_total_poliza;

    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }

    private String obtenerConveniosUsuario()throws Exception{
        String nitafil = request.getParameter("nitprov") != null ? request.getParameter("nitprov") : "0";
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        boolean pasa = true;
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        String cadenatabla = "<div style='overlay: auto;'><table width='100%' style='border-collapse: collapse;'>"
                + "<thead>"
                + "<tr class='subtitulo1'>"
                + "<th>Convenio</th>"
                + "<th>Sector</th>"
                + "<th>Subsector</th>"
                + "</tr>"
                + "</thead></tbody>";
        try {
            lista = gserv.obtenerConveniosProvUsuario(nitafil, usuario.getLogin());
        } catch (Exception e) {
            pasa = false;
            System.out.println("Error al buscar datos: " + e.toString());
        }
        if (pasa == true && lista.size() > 0) {
            for (int i = 0; i < lista.size(); i++) {
                bean = lista.get(i);
                cadenatabla += "<tr style='cursor:pointer;' class='filaazul' onclick='colocar(\"" + bean.getValor_02() + "\",\"" + bean.getValor_03() + "\",\"" + bean.getValor_04() + "\",\"" +
                                bean.getValor_05() + "\",\"" + bean.getValor_06() + "\",\"" + bean.getValor_07() +
                                "\",\"" + bean.getValor_08() + "\",\"" + bean.getValor_09() + "\",\"" + bean.getValor_01() + "\");cerrarDiv();'>"
                            + "<td>" + bean.getValor_05() + "</td>"
                            + "<td>" + bean.getValor_06() + "</td>"
                            + "<td>" + bean.getValor_07() + "</td>"
                            + "</tr>";
                bean = null;
            }
        } else {
            cadenatabla += "<tr class='filaazul' align='center' style='cursor:pointer;' onclick='cerrarDiv();'>"
                    + "<td colspan='3'>No se encontraron registros</td></tr>";
        }
        cadenatabla += "</tbody></table></div>";

        return cadenatabla;
    }

     private String obtenerSectoresUsuario()throws Exception{
        String nitafil = request.getParameter("nitprov") != null ? request.getParameter("nitprov") : "0";
        String convenio = request.getParameter("convenio") != null ? request.getParameter("convenio") : "0";
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        boolean pasa = true;
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        String cadenatabla = "<div style='overlay: auto;'><table width='100%' style='border-collapse: collapse;'>"
                + "<thead>"
                + "<tr class='subtitulo1'>"
                + "<th>Sector</th>"
                + "<th>Subsector</th>"
                + "</tr>"
                + "</thead></tbody>";
        try {
            lista = gserv.obtenerSectoresProvUsuario(nitafil, usuario.getLogin(), convenio);
        } catch (Exception e) {
            pasa = false;
            System.out.println("Error al buscar datos: " + e.toString());
        }
        if (pasa == true && lista.size() > 0) {
            for (int i = 0; i < lista.size(); i++) {
                bean = lista.get(i);
                cadenatabla += "<tr style='cursor:pointer;' class='filaazul' onclick='colocar2(\"" + bean.getValor_02() + "\",\"" + bean.getValor_03() + "\",\"" + bean.getValor_04() + "\",\"" +
                                bean.getValor_05() + "\",\"" + bean.getValor_01()  + "\");cerrarDiv();'>"
                            + "<td>" + bean.getValor_04() + "</td>"
                            + "<td>" + bean.getValor_05() + "</td>"
                            + "</tr>";
                bean = null;
            }
        } else {
            cadenatabla += "<tr class='filaazul' align='center' style='cursor:pointer;' onclick='cerrarDiv();'>"
                    + "<td colspan='3'>No se encontraron registros</td></tr>";
        }
        cadenatabla += "</tbody></table></div>";

        return cadenatabla;
    }

    private String obtenerForm()throws Exception{
        String nitafil = request.getParameter("nitprov") != null ? request.getParameter("nitprov") : "0";
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        boolean pasa = true;
        GestionSolicitudAvalService gserv = new GestionSolicitudAvalService(usuario.getBd());
        String cadenatabla = "<div style='overlay: auto;'><table width='100%' style='border-collapse: collapse;'>"
                + "<thead>"
                + "<tr class='subtitulo1'>"
                + "<th>Cedula</th>"
                + "<th>Nombre</th>"
                + "<th>No Formulario</th>"
                + "<th>Valor Solicitado</th>"
                + "</tr>"
                + "</thead></tbody>";
        try {
            lista = gserv.obtenerFormProv(nitafil);
        } catch (Exception e) {
            pasa = false;
            System.out.println("Error al buscar datos: " + e.toString());
        }
        if (pasa == true && lista.size() > 0) {
            for (int i = 0; i < lista.size(); i++) {
                bean = lista.get(i);
                cadenatabla += "<tr style='cursor:pointer;' class='filaazul' onclick='datosform(\"" + bean.getValor_03() + "\",\"" + bean.getValor_01()
                        + "\",\"" + bean.getValor_04() +"\",\"" + bean.getValor_05() + "\",\"" + bean.getValor_06()  +"\",\"" + bean.getValor_07()
                        + "\",\"" + bean.getValor_08()+ "\",\"" + bean.getValor_09()+ "\",\"" + bean.getValor_10()+ "\",\"" + bean.getValor_11()+"_"+bean.getValor_16()
                        + "\",\"" + bean.getValor_12()+ "\",\""  +  bean.getValor_13()+ "\",\""  +  bean.getValor_14()+ "\",\""  +  bean.getValor_15()+ "\",\""  +  bean.getValor_17()+ "\",\""  +  bean.getValor_18()+ "\");cerrarDiv();'>"
                            + "<td>" + bean.getValor_01() + "</td>"
                            + "<td>" + bean.getValor_02() + "</td>"
                            + "<td>" + bean.getValor_03() + "</td>"
                            + "<td>" + bean.getValor_04() + "</td>"
                            + "</tr>";
                bean = null;
            }
        } else {
            cadenatabla += "<tr class='filaazul' align='center' style='cursor:pointer;' onclick='cerrarDiv();'>"
                    + "<td colspan='4'>No se encontraron registros</td></tr>";
        }
        cadenatabla += "</tbody></table></div>";

        return cadenatabla;
    }
    private String validarTasa() throws Exception {
        String solc = request.getParameter("solc") != null ? request.getParameter("solc") : "0";
        String idprovconv = request.getParameter("idprovconv") != null ? request.getParameter("idprovconv") : "0";
        GestionSolicitudAvalService gserv = new GestionSolicitudAvalService(usuario.getBd());
        SolicitudAval s = gserv.buscarSolicitud(Integer.parseInt(solc));
        String cadena = "S";
        if (s!=null && s.getCodNegocio() != null) {
            Propietario p = new Propietario();
            p = model.gestionConveniosSvc.buscarProvConvenio(Integer.parseInt(idprovconv));
            Negocios neg = model.Negociossvc.buscarNegocio(s.getCodNegocio());
            
            /*se utiliza un operador ternario para validar que los negocios de micro credito
            que no tienen convenio con fenalco no entren a validar la tasa. segun el analisis 
            realizado cuando la var P sea nula es porque son negocios de microcredito
            ya que estos son riesgo propio*/
            
            if (Double.parseDouble(neg.getTasa()) != (p == null ? Double.parseDouble(neg.getTasa()) : p.getTasa_fenalco())) {
                if (neg.getNuevaTasa() != null) {
                    if (neg.getNuevaTasa().equals("N")) {
                        cadena = "N";
                    }
                } else {
                    cadena = "E";
                }
            }
        }
        return cadena;
    }

    private String obtenerTitulosValorConvenio()throws Exception{
        String idConvenio = request.getParameter("idConvenio");
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        ArrayList<String> lista = gserv.titulosValorConvenio(idConvenio);
        String strHtml = "<option value='' selected>...</option>";
        if(lista!=null && lista.size()>0){
            String row[] = null;
            for(int i=0;i<lista.size();i++){
                row = (lista.get(i)).split(";_;");
                strHtml += "<option value="+ row[0] +"_"+row[2]+">"+ row[1] +"</option>";
            }
        }

        return strHtml;
    }

    private String obtenerCiclosTercero()throws Exception{
        String nitTercero = request.getParameter("nitTercero");
        ArrayList listaCiclos = model.tablaGenService.obtenerRegistroDato("CICLOTERCE", nitTercero);
        String strHtml = "<option value='' selected>...</option>";
        if(listaCiclos!=null && listaCiclos.size()>0){
            for (int i = 0; i < listaCiclos.size(); i++) {
                TablaGen tciclo = (TablaGen)listaCiclos.get(i);
                strHtml += "<option value="+tciclo.getReferencia()+">"+tciclo.getDescripcion()+"</option>";
            }
        }
        return strHtml;
    }


            public void getliquidacion() throws Exception {
            double  tcuota=0; double tcapital =0;
            double  tseguro=0;double tcustodia =0;
            double tinteres =0;double tremesa =0;
            double tcuota_manejo =0;
            ArrayList<DocumentosNegAceptado> itemsLiquidaciom = new ArrayList();
              int op_liquidacion = Integer.parseInt(request.getParameter("tipo"));

            if (op_liquidacion==1) {
                itemsLiquidaciom = model.Negociossvc.getLiquidacion();
            } else {
                itemsLiquidaciom = model.Negociossvc.getLiquidacionAval();
            }
            for (int i = 0; i < itemsLiquidaciom.size(); i++) {

                tcuota += itemsLiquidaciom.get(i).getValor();
                tcapital += itemsLiquidaciom.get(i).getCapital();
                tseguro += itemsLiquidaciom.get(i).getSeguro();
                tinteres += itemsLiquidaciom.get(i).getInteres();
                tcustodia += itemsLiquidaciom.get(i).getCustodia();
                tremesa+= itemsLiquidaciom.get(i).getRemesa();
                tcuota_manejo+= itemsLiquidaciom.get(i).getCuota_manejo();
            }





        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(itemsLiquidaciom) + ",\"userdata\":{\"capital\":"+tcapital+",\"interes\":"+tinteres+",\"custodia\":"+tcustodia+",\"seguro\":"+tseguro+",\"remesa\":"+tremesa+",\"cuota_manejo\":"+tcuota_manejo+",\"valor\":"+tcuota+",\"name\":\"Total\"}}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }




        public void exportarPlanPagos() throws Exception {
        String resp = "";
        boolean generado = true;
        int op_icono = 8;
        String url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/plan_de_pagos" + ".pdf";
        generado =this.exportarPlanPagosPdf(usuario.getLogin());
        op_icono = 8;
        if (generado) {
            resp = Utility.getIcono(request.getContextPath(), 5) + "Plan De Pagos Generado Con Exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), op_icono) + " <a href='" + url + "' target=_blank >Ver Plan De Pagos</a></td>";
        }
        this.printlnResponse(resp, "text/plain");

    }
        public void ExportarPlanPagos_Fintra() throws Exception {
        String resp = "";
        boolean generado = true;
        int op_icono = 8;
        String url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/f_plan_de_pagos" + ".pdf";
        int numero_solicitud = Integer.parseInt(request.getParameter("numsolc"));
        generado =this.generarPlanPagosNegocios(usuario.getLogin(),numero_solicitud);
//        generado =this.exportarPlanPagosFintra(usuario.getLogin());
        op_icono = 8;
        if (generado) {
            resp = Utility.getIcono(request.getContextPath(), 5) + "Plan De Pagos Generado Con Exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), op_icono) + " <a href='" + url + "' target=_blank >Ver Plan De Pagos</a></td>";
        }
        this.printlnResponse(resp, "text/plain");

    }

    public void reactivar_negocio() throws Exception {
          Vector batch = new Vector();
        ApplusDAO bd = new ApplusDAO(usuario.getBd());
        String resp = "";
        String cod_neg = request.getParameter("cod_neg");
        String numero_solicitud = request.getParameter("numero_solicitud");
        boolean generado = true;



        NegocioTrazabilidadService nt_service = new NegocioTrazabilidadService(usuario.getBd());
        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
        negtraza.setActividad("LIQ");
        negtraza.setNumeroSolicitud(numero_solicitud);
        negtraza.setUsuario(usuario.getLogin());
        negtraza.setCausal("");
        negtraza.setComentarios("Negocio: Continua Proceso");
        negtraza.setDstrct(usuario.getDstrct());
        negtraza.setCodNeg(cod_neg);
        negtraza.setConcepto("");
        negtraza.setCausal("22");
        batch.add(nt_service.updateEstadoNeg(cod_neg, "P"));
        batch.add(nt_service.updateEstadoForm(numero_solicitud, "P"));
        batch.add(nt_service.insertNegocioTrazabilidad(negtraza));
        bd.ejecutarSQL(batch);
        resp = Utility.getIcono(request.getContextPath(), 5) + "Negocio Reactivado Con Exito.<br/><br/>";
        this.printlnResponse(resp, "text/plain");

    }



        public void financiarAval() throws Exception {

        HttpSession session  = request.getSession();
        GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());
        //obtener negocio inicial
        Negocios neg=model.Negociossvc.getNegocio();
        if(!neg.isFinanciaAval())
        {
        Negocios negAval= (Negocios)neg.clone();
        negAval.setVr_negocio(neg.getVr_aval());
        neg.setVr_aval(0);//
        neg.setFinanciaAval(true);
        model.Negociossvc.setNegocio(neg);
        Convenio convenio = convenioserv.buscar_convenio(usuario.getBd(), neg.getId_convenio() + "");
        Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
        model.Negociossvc.calcularLiquidacionNegocioAval(negAval, usuario, impuesto);
        model.Negociossvc.setNegocioAval(negAval);
        negAval.setVr_aval(0);//
        negAval.setFinanciaAval(false);
        negAval.setVr_desem(negAval.getVr_negocio());
        negAval.setTotpagado( model.Negociossvc.getLiquidacionAval().get(0).getValor() * negAval.getNodocs());
        negAval.setVr_cust(0);
        }
        else
        {
             neg=model.Negociossvc.getNegocio();
             Negocios negAval=model.Negociossvc.getNegocioAval();
             neg.setFinanciaAval(false);
             neg.setVr_aval(negAval.getVr_negocio());
             model.Negociossvc.setNegocio(neg);
             model.Negociossvc.setNegocioAval(null);
        }



        /*ArrayList<DocumentosNegAceptado> itemsLiquidaciom = new ArrayList();
        itemsLiquidaciom = model.Negociossvc.getLiquidacion();*/


        redirect = false;
    }


        public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }


       private void calcularLiquidacion() throws Exception {
           try{
            GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());
            Convenio convenio = convenioserv.buscar_convenio(usuario.getBd(),request.getParameter("convid"));
            if(convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre())
            {
                LiquidacionRedescuento();
                next = "/jsp/fenalco/liquidadores/ResultadoLiquidacionNegocio.jsp";
            }
         else
            {
               Liquidacion ();
            }
           }
           catch (Exception e) {
           mensaje=e.getMessage();
           next = "/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?msj="+mensaje;
           throw new Exception("Error: en parametrizacion");

        }



    }





       private void calcularReliquidacion() throws Exception {
           try{

                this.ReliquidacionRedescuento();
                next = "/jsp/fenalco/liquidadores/ResultadoLiquidacionNegocio.jsp";
           }
           catch (Exception e) {
           mensaje=e.getMessage();
           next = "/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?msj="+mensaje;
           throw new Exception("Error: en parametrizacion");

        }



    }

        private void LiquidacionRedescuento() throws Exception {

        HttpSession session  = request.getSession();
        Negocios neg = new Negocios();
        GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());
        String tipo_liq = request.getParameter("tipo_liq");

        String numero_solicitud = (request.getParameter("numsolc")!=null && request.getParameter("numsolc").equals(""))?null:request.getParameter("numsolc");
        String fechaItem = request.getParameter("forma_pago");
        String tasa=request.getParameter("tasa")==null?"S":request.getParameter("tasa");///revisar con iris
        String tituloValor[] = request.getParameter("cmbTituloValor").split("_");
        String fechai = (String) request.getParameter("fechainicio");
        String fechaNegocio=(String) request.getParameter("fechaNegocio");
        int idProvConvenio = Integer.parseInt(request.getParameter("hidIdProvConvenio"));
        String banco = request.getParameter("cmbBanco");
        String ciudadCh = request.getParameter("txtCiudadCheque");
    
        String tneg = request.getParameter("cmbTituloValor").split("_")[0];
        neg.setId_sector(request.getParameter("sectid"));
        neg.setNo_solicitud(request.getParameter("numsolc"));
        neg.setNodocs(Integer.parseInt(request.getParameter("txtNumCuotas")));
        neg.setTneg(tituloValor[0]);
        Propietario Propietario = new Propietario();
        Propietario = model.gestionConveniosSvc.buscarProvConvenio(idProvConvenio);
        neg.setDist(usuario.getDstrct());
        neg.setTasa(Propietario.getTasa_fenalco() +"");
        neg.setFecha_neg(fechaNegocio);// neg.setFecha_neg(request.getParameter("fechainicio"));
        neg.setIdProvConvenio(idProvConvenio);
        neg.setVr_negocio(Math.rint(Double.parseDouble(request.getParameter("txtValor"))));
        neg.setMod_rem((tituloValor[1].equals("t")?"0":"1"));
        neg.setId_convenio(Integer.parseInt(request.getParameter("convid")));
        neg.setId_subsector(request.getParameter("subsectid"));
        neg.setCod_cli(request.getParameter("nit"));
        neg.setNit_tercero(request.getParameter("cmbAfiliado"));
        neg.setNitp(request.getParameter("cmbAfiliado"));
        neg.setFpago(fechaItem);
        neg.setFecha_liquidacion(fechai);//neg.setFecha_liquidacion(neg.getFecha_neg());
        Convenio convenio = convenioserv.buscar_convenio(usuario.getBd(), neg.getId_convenio() + "");
        Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
        neg.setValor_seguro(convenio.getValor_seguro());
        neg.setvalor_remesa("0");

            //Si se selecciono una sede guardar el nit del afiliado
            Proveedor proveedor = model.proveedorService.buscarNitAfiliado(neg.getNit_tercero());
            if (proveedor == null) {
                throw new Exception("Error: no existe el proveedor");
            }
            if (proveedor.getSede().equals("S")) {
                neg.setNit_tercero(proveedor.getNit_afiliado());
            }
           // calculo segurro debe ser por porcentaje
            convenio.getValor_seguro();
            neg.setValor_seguro(0);
           if(neg.getMod_rem().equals("0")) //Obtener datos de la remesa
           {
            ConveniosRemesas cr = new ConveniosRemesas();
            cr.setDstrct(usuario.getDstrct());
            cr.setId_convenio(String.valueOf(neg.getId_convenio()));
            cr.setCiudad_sede(proveedor.getCiudad());
            cr.setBanco_titulo(banco);
            cr.setCiudad_titulo(ciudadCh);
            ConveniosRemesas Objremesa = model.gestionConveniosSvc.buscarRemesa(cr);

            if(Objremesa==null)
            {
             throw new Exception("Error: No existe condicione sde remesas para esos parametros");
            }

            neg.setPor_rem(Objremesa.getPorcentaje_remesa());
            neg.setId_remesa(Integer.parseInt(Objremesa.getId_remesa()));

            if(Objremesa!=null)
            {
           //calcular remesa
            double exp= 1-( Math.pow((1 + (Propietario.getTasa_fenalco()/100)),-neg.getNodocs()));
            double vremesa= (neg.getPor_rem()/100) * (neg.getVr_negocio()/ (exp/(Propietario.getTasa_fenalco()/100)));

            vremesa=vremesa+neg.getVr_cust()+neg.getValor_seguro();
            neg.setvalor_remesa(Math.round(vremesa)+"");
               }

            neg.setVr_cust(Propietario.getCustodiacheque());
            }
            model.Negociossvc.calcularLiquidacionNegocio(neg, usuario, impuesto);//calcular liquidacion
            model.Negociossvc.setNegocio(neg);
            //Enviar valores a la interfaz
            request.setAttribute("idProvConvenio", idProvConvenio);
            request.setAttribute("Custodia_Cheque", 0);
            request.setAttribute("banco", banco);
            request.setAttribute("ciudadCh", ciudadCh);
            request.setAttribute("tipo_liq", tipo_liq);
            request.setAttribute("fechai", fechai);
            request.setAttribute("numsolc",numero_solicitud );
            request.setAttribute("tipo_negocio", neg.getTneg());
            request.setAttribute("act_liq", request.getParameter("act_liq"));

    }



    private void Liquidacion() throws Exception {

        HttpSession session  = request.getSession();
        String nit=request.getParameter("nit");
        session.setAttribute("nitcm", nit);

        String tipo_liq = request.getParameter("tipo_liq");
        boolean mostrarTodo = false;
        double valor_operation = 0;
        double custodiax = 0;
        String numero_solicitud = (request.getParameter("txtSolicitud")!=null && request.getParameter("txtSolicitud").equals(""))?null:request.getParameter("txtSolicitud");
        String ciclo = request.getParameter("cmbCiclo");
        String prop = request.getParameter("cmbTipoCliente");
        String Aux = "";
        String tasa=request.getParameter("tasa")==null?"S":request.getParameter("tasa");
        if (numero_solicitud != null) {
            Aux = "&numero_solicitud=" + numero_solicitud + "&ciclo=" + ciclo + "&prop=" + prop;
        }


        String ParNumero_Cheques = (String) request.getParameter("txtNumCuotas");
        String ParValor_Negocio = (String) request.getParameter("txtValor");
        String Forma_Pago = (String) request.getParameter("forma_pago");
        String tituloValor[] = request.getParameter("cmbTituloValor").split("_");
        String tipo_negocio = tituloValor[0];
        String Estado_Remesa = (tituloValor[1].equals("t")?"0":"1");
        String fechai = (String) request.getParameter("fechainicio");
        String propietario = request.getParameter("cmbAfiliado");
        String numero_form=request.getParameter("numsolc");
        String aval = request.getParameter("aval")==null?"0":request.getParameter("aval");
        String remesas = "1";
        String custodia = request.getParameter("custodia");
        String facturaTercero = request.getParameter("hidFacturaTercero");
        int idProvConvenio = Integer.parseInt(request.getParameter("hidIdProvConvenio"));
        String idConvenio = request.getParameter("convid");
        String banco = request.getParameter("cmbBanco");
        String ciudadCh = request.getParameter("txtCiudadCheque");
        String colorfila = "";
        String idRemesa = "0";
        String idsector=request.getParameter("sectid");
        String idsubsector=request.getParameter("subsectid");
        String tipoCalculo = request.getParameter("tipoCalculo")==null?"liquidador":request.getParameter("tipoCalculo");
        String parametros = "";
        double comisionTercero=0;
        double porrem = 0;

        //Crea un string con los parametros recibidos para ser reutilizado cuando se calcule el aval y la remesa
        for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
             String param = (String)e.nextElement();
             if(!param.equals("aval") && !param.equals("remesas")){
                 parametros += "&"+param+"="+request.getParameter(param);
             }
         }

        if(tipoCalculo.equals("calculoAval")){
            if (aval.equals("0")) {
                aval = "1";
            } else {
                aval = "0";
            }
        }
        if (Forma_Pago == null || Forma_Pago.equals("")) {
            Forma_Pago = model.FenalcoFintraSvc.obtenerPrimeraFechaPago(ciclo, fechai);
        }

        int Numero_Cheques = Integer.parseInt(ParNumero_Cheques);
        boolean esPropietario;
        if (facturaTercero.equals("t")) {
            if (prop.equals("30")) {
                esPropietario = true;
            } else {
                esPropietario = false;
            }
        } else {
            esPropietario = false;
        }

        //Si se selecciono una sede guardar el nit del afiliado
        Proveedor proveedor = model.proveedorService.buscarNitAfiliado(propietario);
        if (proveedor == null) {
            throw new Exception("Error: no existe el proveedor");
        }
        if (proveedor.getSede().equals("S")) {
            propietario = proveedor.getNit_afiliado();
        }
        //buscar convenio
        GestionConveniosDAO convenioDao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convenioDao.buscar_convenio(usuario.getBd(),idConvenio);
        //buscar la comision del tercero
        for (int i = 0; i < convenio.getConvenioComision().size(); i++) {
            ConvenioComision convComision = convenio.getConvenioComision().get(i);
            if(convComision.isComision_tercero()){
                comisionTercero=convComision.getPorcentaje_comision();
                break;
            }
        }
        Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
        if(impuesto==null){
             throw new Exception("Error: el convenio "+convenio.getId_convenio()+" no tiene impuesto vigente");
        }
        Propietario Propietario = new Propietario();
        Propietario = model.gestionConveniosSvc.buscarProvConvenio(idProvConvenio);
        if (Propietario == null) {
            throw new Exception("Error: no existe el registro en prov_convenio");
        }
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        int plazoPrimeraCuota = Integer.parseInt(Forma_Pago);
        if(numero_solicitud != null) {
            plazoPrimeraCuota = Integer.parseInt(prop);
        }
        ArrayList Aval = gserv.buscarRangosAval(idProvConvenio, tipo_negocio, plazoPrimeraCuota, esPropietario);
        if (Aval == null || Aval.size() == 0) {
            mensaje ="Error: No hay condiciones de aval para estos parametros";
            throw new Exception("Error: no esta registrado el aval para los parametros ingresados");
        }

        double[] diasremix = new double[Numero_Cheques + 2];

        double Valor_Negocio = Double.parseDouble(ParValor_Negocio);

        double Tasa = Propietario.getTasa_fenalco() / 100;
        if(tasa.equals("N")){
            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            SolicitudAval s = gsaserv.buscarSolicitud(Integer.parseInt(numero_form));
            Negocios neg = model.Negociossvc.buscarNegocio(s.getCodNegocio());
            Tasa=Double.parseDouble(neg.getTasa())/100;
        }
        double Custodia_Cheque = Propietario.getCustodiacheque();

        if(Estado_Remesa.equals("0")){
            //OBTENER PORCENTAJE REMESA
            ConveniosRemesas cr = new ConveniosRemesas();
            cr.setDstrct(usuario.getDstrct());
            cr.setId_convenio(idConvenio);
            cr.setCiudad_sede(proveedor.getCiudad());
            cr.setBanco_titulo(banco);
            cr.setCiudad_titulo(ciudadCh);
            ConveniosRemesas remesa = model.gestionConveniosSvc.buscarRemesa(cr);
            if (remesa == null) {
                 mensaje ="Error: No hay condiciones de aval para estos parametros";
                throw new Exception("Error: no esta registrado el valor de la remesa para los parametros ingresados");
            }else{
                if (remesa.isGenera_remesa()) {
                    porrem = remesa.getPorcentaje_remesa() / 100;
                    idRemesa = remesa.getId_remesa();
                }else{
                    Estado_Remesa="1";
                }
            }
            cr = null;
        }

        java.util.Calendar Fecha_Sugerida = null;
        java.util.Calendar Fecha_Negocio = Util.crearCalendarDate(fechai);
        java.util.Calendar Fecha_Sugerida_2 = null;
        java.util.Calendar Fecha_Sugerida_3 = null;

        int dia_valido = 0;
        int dia = 0;
        int mes = 0;
        int ano = 0;
        int dia_s = 0;
        int mes_s = 0;
        int ano_s = 0;

        dia = Integer.parseInt(fechai.substring(8, 10));
        mes = Integer.parseInt(fechai.substring(5, 7));
        ano = Integer.parseInt(fechai.substring(0, 4));
        Fecha_Negocio.set(ano, mes - 1, dia);

        double Formula = 0;
        double Suma = 0;
        double Valor_Cheque = 0;
        double Valor_Cheque_X = 0;
        double Desembolso = 0;
        double Valor_Capital = 0;
        double Valor_Interes = 0;
        double Valor_Remesa = 0;
        double Saldo_Final = 0;
        double Saldo_Inicial = 0;
        double Valor_Doc = 0;
        double Formulas[];
        double ValorNum = 0;
        double TotalAvalComisionTerc = 0;
        double ValorAval = 0;
        double ValorAvalIva = 0;
        double TotalAval = 0;
        double ValorComisionTercero=0;
        double ValorComisionTerceroIva=0;
        double valor_1 = 0;
        double valor_2 = 0;
        double valor_3 = 0;
        double valor_4 = 0;
        double dias = 0;
        Suma = 0;
        double lim = 11655;
        double TotalRemesa_x = 0;

        String Filas_Tabla_Liquidacion = "";
        String Filas_Tabla_Liquidacion_2 = "";
        String Dias_Primer_Cheque = Forma_Pago;
        Saldo_Final = 0;

        Formula = (((Math.pow((1 + Tasa), 12)) - 1));

        Formulas = new double[Numero_Cheques + 1];

        ArrayList chequesx = new ArrayList();
        chequeCartera chequeCarterax = new chequeCartera();
        for (int fila = 1; fila <= Numero_Cheques; fila++) {//se recorrera cada cheque para armarle su fila

            chequeCarterax = new chequeCartera();

            if (fila % 2 == 0) {
                colorfila = "filagris";
            } else {
                colorfila = "filaazul";
            }

            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "<tr class='" + colorfila + "'>";
            //Numero del documento
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <td>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <p align='center'>" + String.valueOf(fila) + "</p>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    </td>";

            chequeCarterax.setItem("" + fila);

            // Fechas
            if (fila == 1) {

                if (!Forma_Pago.equals("30")) {
                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                } else {

                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
                }

            } else {

                if (!Forma_Pago.equals("45")) {

                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                } else {

                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                }

            }

            if (!Forma_Pago.equals("30")) {
                if (fila == 1) {
                    dia = Fecha_Sugerida.get(Calendar.DATE);
                    mes = Fecha_Sugerida.get(Calendar.MONTH);
                    ano = Fecha_Sugerida.get(Calendar.YEAR);
                }

            }

            dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
            mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
            ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);

            if (mes_s == 12) {
                ano_s = ano_s - 1;
            }
            if (mes_s == 0) {
                mes_s = 1;
            } else if (mes_s != 1) {
                mes_s = mes_s + 1;

            } else if (mes_s == 1) {
                mes_s = mes_s = 2;
            }
            int j = 0;

            while (model.FenalcoFintraSvc.buscarFestivo(ano_s + "-" + mes_s + "-" + dia_s)) {

                mes_s = mes_s - 1;
                Fecha_Sugerida_2.set(ano_s, mes_s, dia_s);
                Fecha_Sugerida_2.add(Calendar.DAY_OF_YEAR, 1);

                dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
                mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
                ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
                mes_s = mes_s + 1;


                j = 1;

            }

            if (j == 1) {
                if (mes_s != 0) {
                    Fecha_Sugerida_2.set(ano_s, mes_s - 1, dia_s);
                } else {
                    Fecha_Sugerida_2.set(ano_s, 1, dia_s);
                }
            }
            if (fila != 1) {
                if (mes_s != 12) {
                    Fecha_Sugerida_3.set(ano_s, mes_s - 1, dia_s);
                }
            } else {
                Fecha_Sugerida_3.set(ano_s, mes_s, dia_s);
            }
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <td>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <p align='right' >" + Util.ObtenerFechaCompleta(Fecha_Sugerida) + "</p>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    </td>";

            String diaremix = "", mesremix = "";
            if (Fecha_Sugerida.get(java.util.Calendar.MONTH) < 9) {
                mesremix = "0" + (Fecha_Sugerida.get(java.util.Calendar.MONTH) + 1);
            } else {
                mesremix = "" + (Fecha_Sugerida.get(java.util.Calendar.MONTH) + 1);
            }
            if (Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH) < 10) {
                diaremix = "0" + Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH);
            } else {
                diaremix = "" + Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH);
            }

            String fechitaremix = (Fecha_Sugerida.get(java.util.Calendar.YEAR)) + "-" + mesremix + "-" + diaremix;

            chequeCarterax.setFechaCheque("" + fechitaremix);

            if (fila != 1) {
                dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
            } else {
                dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
            }

            if (mostrarTodo) {
                Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <td>";
                Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <p align='right' >" + dias + "</p>";
                Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    </td>";
            }

            diasremix[fila - 1] = dias;

            chequeCarterax.setDias("" + dias);

            valor_1 = (dias / 365);
            valor_2 = (1 + Formula);
            valor_3 = Math.pow(valor_2, valor_1);
            valor_4 = ((1 / valor_3));

            double val = Util.redondear(valor_4, 9);
            Formulas[fila] = val;

            Suma = Suma + Formulas[fila];
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "</tr>";
            if (j == 1) {

                Fecha_Sugerida_2.set(ano_s, mes_s + 1, dia_s);
                dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
                mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
                ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
            }
            chequesx.add(chequeCarterax);
        }
        Valor_Cheque = Util.redondear((Valor_Negocio / (Suma)), 0);

        //Valor_Cheque = Suma;
        custodia = "1";
        //aval 0: sin aval. //custodia 0: sin custodia //Estado_Remesa 1: sin remesa //Estado_Remesa 0 y remesa 0: con remesa el establecimiento. //Estado_Remesa 0 y remesa 1: con remesa el cliente.

        if (custodia.equals("1") && aval.equals("1") && Estado_Remesa.equals("1")) { //remesas.equals("0")){//__a//si hay custodia y hay aval y no hay remesa //creo que ok

            for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {//se calculara el valor inicial del aval

                if (diasremix[fila] > ((fila + 1) * 30)) {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                } else {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                }

                ValorAval = (Valor_Cheque * ValorNum);
                ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1()/100));
                ValorComisionTercero = (Valor_Cheque * (comisionTercero/100));
                ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1()/100));
                TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva)+(ValorComisionTercero + ValorComisionTerceroIva);
                TotalAval=TotalAval+ValorAval+ValorAvalIva;
            }

            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);//custodia
            valor_operation = Valor_Negocio;//valor operacion inicial
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));//valor segundo del cheque
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));

            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation) >= 0.5) {//se calculara el valor del aval,el valor del cheque
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalAval=0;
                TotalAvalComisionTerc = 0;
                for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                    if (diasremix[fila] > ((fila + 1) * 30)) {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                    } else {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                    }

                    ValorAval = (Valor_Cheque * ValorNum);
                    ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1()/100));
                    ValorComisionTercero = (Valor_Cheque * (comisionTercero/100));
                    ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1()/100));
                    TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva)+(ValorComisionTercero + ValorComisionTerceroIva);
                    TotalAval=TotalAval+ValorAval+ValorAvalIva;
                }
            }

        }
        //aval 0: sin aval. //custodia 0: sin custodia //Estado_Remesa 1: sin remesa //Estado_Remesa 0 y remesa 0: con remesa el establecimiento. //Estado_Remesa 0 y remesa 1: con remesa el cliente.

        //si custodia asumida por cliente(1) se suma a valor del cheque , sino no
        if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1")) {//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma));// + (Custodia_Cheque));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma));// + (Custodia_Cheque));
        }

        //si custodia asumida por cliente(1) se suma a valor del cheque , sino no
        if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1") && (1 == 2)) {//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma) + (Custodia_Cheque));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma) + (Custodia_Cheque));
        }

        if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1") && (1 == 2)) {//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok
            custodiax = 0;
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma));//+ (Custodia_Cheque));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma));//+ (Custodia_Cheque));
        }

        if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el cliente

            for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {//_

                if (diasremix[fila] > ((fila + 1) * 30)) {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                } else {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                }

                ValorAval = (Valor_Cheque * ValorNum);
                ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1()/100));
                ValorComisionTercero = (Valor_Cheque * (comisionTercero/100));
                ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1()/100));
                TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva)+(ValorComisionTercero + ValorComisionTerceroIva);
                TotalAval=TotalAval+ValorAval+ValorAvalIva;
            }
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            valor_operation = Valor_Negocio + TotalAvalComisionTerc + TotalRemesa_x;
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));

            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }

            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation + TotalRemesa_x) >= 0.5) {
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc + TotalRemesa_x;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalAval=0;
                TotalAvalComisionTerc = 0;
                for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                    if (diasremix[fila] > ((fila + 1) * 30)) {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                    } else {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                    }

                    ValorAval = (Valor_Cheque * ValorNum);
                    ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1()/100));
                    ValorComisionTercero = (Valor_Cheque * (comisionTercero/100));
                    ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1()/100));
                    TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva)+(ValorComisionTercero + ValorComisionTerceroIva);
                    TotalAval=TotalAval+ValorAval+ValorAvalIva;
                }
                TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
                if (TotalRemesa_x < (lim * Numero_Cheques)) {
                    TotalRemesa_x = (lim * Numero_Cheques);
                }
            }
        }//endif

        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {//caso ok a//si hay custodia y no hay aval y la remesa la asume el cliente

            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            TotalAval=0;
            TotalAvalComisionTerc = 0;
            valor_operation = Valor_Negocio + TotalAvalComisionTerc + TotalRemesa_x;
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation + TotalRemesa_x) >= 1.0) {
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc + TotalRemesa_x;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
                if (TotalRemesa_x < (lim * Numero_Cheques)) {
                    TotalRemesa_x = (lim * Numero_Cheques);
                }
            }
        }

        if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento

            for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                if (diasremix[fila] > ((fila + 1) * 30)) {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                } else {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                }

                    ValorAval = (Valor_Cheque * ValorNum);
                    ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1()/100));
                    ValorComisionTercero = (Valor_Cheque * (comisionTercero/100));
                    ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1()/100));
                    TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva)+(ValorComisionTercero + ValorComisionTerceroIva);
                    TotalAval=TotalAval+ValorAval+ValorAvalIva;
            }
            TotalRemesa_x = 0;
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            valor_operation = Valor_Negocio + TotalAvalComisionTerc + TotalRemesa_x;
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));

            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation + TotalRemesa_x) >= 1.0) {
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc + TotalRemesa_x;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalAval=0;
                TotalAvalComisionTerc = 0;
                for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                    if (diasremix[fila] > ((fila + 1) * 30)) {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                    } else {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                    }

                    ValorAval = (Valor_Cheque * ValorNum);
                    ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1()/100));
                    ValorComisionTercero = (Valor_Cheque * (comisionTercero/100));
                    ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1()/100));
                    TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva)+(ValorComisionTercero + ValorComisionTerceroIva);
                    TotalAval=TotalAval+ValorAval+ValorAvalIva;
                }
            }
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
        }//endif

        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y no hay aval y la remesa la asume el establecimiento
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma));
            TotalAval=0;
            TotalAvalComisionTerc = 0;
            TotalRemesa_x = (Valor_Cheque + Custodia_Cheque) * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
        }

        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0")) && (1 == 2)) {//caso ok a//si hay custodia y no hay aval y la remesa la asume el establecimiento
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = Math.round(((Valor_Negocio) / Suma) + (Custodia_Cheque));
            Valor_Cheque_X = Math.round(((Valor_Negocio) / Suma) + (Custodia_Cheque));
            //////////////////////////////////////////////////////////////////////////////////////
            TotalAval=0;
            TotalAvalComisionTerc = 0;
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
        }

        if ((Valor_Cheque * porrem) < lim) {//si el valor de la remesa es menor que el limite
            Valor_Remesa = lim;//valor_remesa
        } else {//si el valor de la remesa es mayor o igual que el limite
            Valor_Remesa = Valor_Cheque * porrem;//valor_remesa
        }

        //si custodia es asumida por establecimiento se resta a desembolso , sino no
        //si aval es asumido por establecimiento se ignora, sino se suma a desembolso
        //si remesa es asumida por establecimiento se resta a desembolso , si no no
        //ojo con remesa

        if (aval.equals("0")) {//si no hay aval
            Desembolso = Valor_Negocio;
        }
        if (aval.equals("1")) {//si hay aval
            Desembolso = Valor_Negocio + TotalAvalComisionTerc;
        }

        if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento
            Desembolso = Desembolso - TotalRemesa_x;
        }
        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento
            Desembolso = Desembolso - TotalRemesa_x;
        }

        for (int fila = 1; fila <= Numero_Cheques; fila++) {//para cada cheque
            chequeCartera chequeCarterax2 = (chequeCartera) chequesx.get(fila - 1);
            if (fila % 2 == 0) {
                colorfila = "filagris";
            } else {
                colorfila = "filaazul";
            }

            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "<tr class='" + colorfila + "'>";

            //Valor de los intereses
            Valor_Capital = (Valor_Cheque_X * Formulas[fila]);//capitalito
            Valor_Interes = (Valor_Cheque_X - Valor_Capital);//interesito
            Valor_Doc = Math.round(Valor_Capital + Valor_Interes);//valor_cheque
            if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1")) {
                Valor_Capital = (Valor_Cheque_X * Formulas[fila]) + Custodia_Cheque;//capitalito
                Valor_Interes = (Valor_Cheque_X - Valor_Capital) + Custodia_Cheque;//interesito
                Valor_Doc = Math.round(Valor_Capital + Valor_Interes)/*+Custodia_Cheque*/;//valor_cheque
            }

            if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {
                Valor_Capital = (Valor_Cheque_X * Formulas[fila]) + Custodia_Cheque;//capitalito
                Valor_Interes = (Valor_Cheque_X - Valor_Capital) + Custodia_Cheque;//interesito
                Valor_Doc = Math.round(Valor_Capital + Valor_Interes)/*+Custodia_Cheque*/;//valor_cheque
            }

            if (fila == 1) {
                //si el aval lo asume el cliente se le suma al saldo, sino no
                Saldo_Inicial = Math.round(Valor_Negocio + TotalAvalComisionTerc);//saldo inicial
                if (aval.equals("1")) {//si hay aval
                    Saldo_Inicial = /*Math.round*/ (Valor_Negocio + TotalAvalComisionTerc);//saldo inicial
                }

                if (aval.equals("0")) {//si no hay aval
                    Saldo_Inicial = Math.round(Valor_Negocio + TotalAvalComisionTerc);//saldo inicial
                }

                Saldo_Inicial = Saldo_Inicial + Math.round(TotalRemesa_x);

                if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {
                    Saldo_Inicial = Saldo_Inicial + TotalRemesa_x;
                }

                if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {
                    Saldo_Inicial = Saldo_Inicial + TotalRemesa_x;
                }

            } else {
                Saldo_Inicial = Math.round(Saldo_Final);//nuevo saldo inicial
            }

            Saldo_Final = Math.round(Saldo_Inicial - Valor_Capital);//saldo final

            if(sw_ver_saldos) {
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >" + String.valueOf(Util.customFormat(Saldo_Inicial)) + "</p>";
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            }
            chequeCarterax2.setSaldoInicial("" + Saldo_Inicial);

            if (mostrarTodo) {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='center' >" + String.valueOf(Util.customFormat(Valor_Capital)) + "" + "" + "</p>";///
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            }

            chequeCarterax2.setValorCapital("" + Valor_Capital);
            double valor_aval99 = 0;
            String porci = "0";
            if (mostrarTodo) {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='center' >" + String.valueOf(Util.customFormat(Valor_Interes)) + "</p>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
                if (Aval != null) {
                    valor_aval99 = Valor_Doc * ((impuesto.getPorcentaje1()/100)+1)* (Double.parseDouble("" + Aval.get(fila))) / 100;
                    porci = ("" + valor_aval99) + "_" + Aval.get(fila);
                }
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='center' >" + porci + "</p>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            } else {
                if (Aval != null) {
                    valor_aval99 = Valor_Doc * ((impuesto.getPorcentaje1()/100)+1)* (Double.parseDouble("" + Aval.get(fila))) / 100;
                    porci = ("" + valor_aval99) + "_" + Aval.get(fila);
                }
            }

            chequeCarterax2.setValorInteres("" + Valor_Interes);
            chequeCarterax2.setNoAval("" + valor_aval99);

            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >" + String.valueOf(Util.customFormat(Valor_Doc)) + "</p>";//clave
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";

            chequeCarterax2.setValor("" + Valor_Doc);
            if(sw_ver_saldos) {
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
            if (fila == Numero_Cheques) {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >0.00</p>";

            } else {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >" + String.valueOf(Util.customFormat(Saldo_Final)) + "</p>";

            }

            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            }


            //////////////
            if (fila == Numero_Cheques)
            {
                chequeCarterax2.setSaldoFinal("0");
            }
            else
            {
                chequeCarterax2.setSaldoFinal("" + Saldo_Final);
            }





            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "</tr>";

        }

        //Enviar valores a la interfaz
        request.setAttribute("numsolc", numero_form);
        request.setAttribute("tipo_liq", tipo_liq);
        request.setAttribute("Valor_Negocio", Valor_Negocio);
        request.setAttribute("Numero_Cheques", Numero_Cheques);
        request.setAttribute("Desembolso", Desembolso);
        request.setAttribute("Valor_Doc", Valor_Doc);
        request.setAttribute("Fecha_Negocio", Fecha_Negocio);
        request.setAttribute("custodia", custodia);
        request.setAttribute("aval", aval);
        request.setAttribute("Estado_Remesa", Estado_Remesa);
        request.setAttribute("remesas", remesas);
        request.setAttribute("tipo_negocio", tipo_negocio);
        request.setAttribute("ParNumero_Cheques", ParNumero_Cheques);
        request.setAttribute("ParValor_Negocio", ParValor_Negocio);
        request.setAttribute("Forma_Pago", Forma_Pago);
        request.setAttribute("fechai", fechai);
        request.setAttribute("propietario", propietario);
        request.setAttribute("idProvConvenio", idProvConvenio);
        request.setAttribute("convenio", idConvenio);
        request.setAttribute("banco", banco);
        request.setAttribute("ciudadCh", ciudadCh);
        request.setAttribute("facturaTercero", facturaTercero);
        request.setAttribute("numero_solicitud", numero_solicitud);
        request.setAttribute("ciclo", ciclo);
        request.setAttribute("prop", prop);
        request.setAttribute("Aux", Aux);
        request.setAttribute("Tasa", Tasa);
        request.setAttribute("Custodia_Cheque", Custodia_Cheque);
        request.setAttribute("porrem", porrem);
        request.setAttribute("TotalAval", TotalAval);
        request.setAttribute("TotalRemesa_x", TotalRemesa_x);
        request.setAttribute("idRemesa", idRemesa);
        request.setAttribute("chequesx", chequesx);
        request.setAttribute("parametros", parametros);
        request.setAttribute("Filas_Tabla_Liquidacion", Filas_Tabla_Liquidacion);
        request.setAttribute("Filas_Tabla_Liquidacion_2", Filas_Tabla_Liquidacion_2);
        request.setAttribute("idsect", idsector);
        request.setAttribute("idsubsect", idsubsector);
        request.setAttribute("act_liq", request.getParameter("act_liq"));

    }







    /***********************************************plan de pagos************************************************************/
    public boolean exportarPlanPagosPdf(String userlogin) throws Exception {



        boolean generado = true;
        String directorio = "";
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "plan_de_pagos", "pdf");
            Font fuente = new Font(Font.HELVETICA, 10);
            Font fuenteB = new Font(Font.HELVETICA, 12, Font.BOLD);
            Document documento = null;
            documento = this.createDoc("H");
            PdfWriter Pdfriter = PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.open();
            Paragraph p = new Paragraph("PLAN DE PAGOS", fuenteB);
            p.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(p);
            documento.add(Chunk.NEWLINE);


            /*----------------------------------Datos generales --------------------------------------*/
            PdfPTable tabla_datos = this.DatosGenerales();
            tabla_datos.setHorizontalAlignment(tabla_datos.ALIGN_CENTER);
            tabla_datos.setWidthPercentage(85);
            documento.add(tabla_datos);
            documento.add(Chunk.NEWLINE);

            /*----------------------------------liquidaciones--------------------------------------*/
            PdfPTable tabla_liquidacion = liquidacion();
            tabla_liquidacion.setWidthPercentage(100);
            documento.add(tabla_liquidacion);
            documento.add(Chunk.NEWLINE);

            Negocios neg= model.Negociossvc.getNegocio();
            if(!neg.isFinanciaAval() && !model.Negociossvc.isNegocioAval(neg.getCod_negocio()))
            {

            documento.add(phtml("<span class =nota>La primera cuota es mas alta debido a que esta incluye el valor correspondiente a la comision cobrada por Fenalco por concepto de aval</span> "));
            }
            
            

            documento.add(phtml("<span class =nota ><b>Nota Importante:</b>En caso de incurrir en mora, la entidad podr� cobrar intereses y gastos de cobranza, de acuerdo con lo establecido por ley.<br/>"
                    + "	Para mayor informaci�n, puede comunicarse a los telefonos_______________ <br/>Ext. ____; correo electronico ______________________;<br/>"
                    + "Direccion: _______________________,<br/><br/></span>"));
                    //+ "____________________________<br/>"
                    //+ "Firma de Aceptaci�n<br/>"
                    //+ "<br/><br/><br/></span> "));
            //documento.add(new Paragraph(new Phrase("Cordialmente,", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("__________________________________", fuente)));
            documento.add(new Paragraph(new Phrase("Firma de Aceptaci�n", fuente)));
            
            
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(phtml("<span class =nota > C�digo para pago Cuota con Aval <br/></span>"));
            
            
            
            String cod_neg = neg.getCod_negocio();
            //facturas= model.Negociossvc.obtenerValorPimeraCuota(cod_neg);
            liqui= model.Negociossvc.getLiquidacion();
            
            float[] widths2 = {0.34f, 0.18f, 0.26f, 0.22f};
            PdfPTable thead = new PdfPTable(widths2); 
            PdfPTable thead2 = new PdfPTable(widths2); 
            String consneg = neg.getCod_negocio().substring(2);
            String ceros = "00000000";
            consneg = consneg.length() == 8 ? consneg : ceros.substring(0, 8-consneg.length())+consneg;
            PdfPCell celda = new PdfPCell();
            PdfPCell celda2 = new PdfPCell();
            Barcode128 uccEan128 = new Barcode128();
            uccEan128.setCodeType(Barcode.CODE128_UCC);
            PdfContentByte cb = Pdfriter.getDirectContent();
            
            //Codigo de barras primera cuota
            if (!neg.isFinanciaAval()) {
                celda.setBorderWidth(0);
                String valorP = String.valueOf(Math.round(valorCodigoConAval));
                valorP = valorP.length() == 8 ? valorP : ceros.substring(0, 8 - valorP.length()) + valorP;
                uccEan128.setCode("(415)" + model.tablaGenService.obtenerTablaGen("FITRAGS1", "01").getDato()
                        + "(8020)" + consneg + "(3900)" + valorP.replace(".", "")
                        + "(96)" + FechaPrimeraCuota.replaceAll("-", ""));
                uccEan128.setBarHeight(48f);
                uccEan128.setSize(8f);
                celda.addElement(uccEan128.createImageWithBarcode(cb, java.awt.Color.BLACK,
                        java.awt.Color.BLACK));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                celda.setColspan(4);
                thead.addCell(celda);
                celda = new PdfPCell();
                celda.setBorderWidth(0);
                celda.setPhrase(new Phrase(" ", fuente));
                celda.setColspan(4);
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                thead.addCell(celda);
                documento.add(thead);
            }

            
            documento.add(phtml("<span class =nota > C�digo para pago <br/></span>"));      
                     
                        
            //Codigo de barras cuotas restantes
            celda2.setBorderWidth(0);
            String valor = ((int) liqui.get(1).getValor() + "").replace(".", "");
            int ultimafecha = (neg.getNodocs()-1);
            valor = valor.length() == 8 ? valor : ceros.substring(0, 8-valor.length())+valor;
            
            uccEan128.setCode("(415)" + model.tablaGenService.obtenerTablaGen("FITRAGS1", "01").getDato()
                    + "(8020)" + consneg  + "(3900)" + valor
                    + "(96)" + liqui.get(ultimafecha).getFecha().substring(0, 10).replaceAll("-", ""));
            uccEan128.setBarHeight(48f);
            uccEan128.setSize(8f);
            
            celda2.addElement(uccEan128.createImageWithBarcode(cb, java.awt.Color.BLACK,
                    java.awt.Color.BLACK));

            celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda2.setColspan(4);
            thead2.addCell(celda2);
            celda2 = new PdfPCell();
            celda2.setBorderWidth(0);
            celda2.setPhrase(new Phrase(" ", fuente));
            celda2.setColspan(4);
            celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            thead2.addCell(celda2);

            documento.add(thead2);
            
            documento.close();

        } catch (Exception e) {
            generado = false;
            System.out.println("error al generar plan de pagos pdf : " + e.toString());
            e.printStackTrace();
        }


        return generado;
    }


        private Document createDoc(String op) {///hora horizontal
        int alto=842; int ancho=595;
        if (op.equals("V"))//vertical
        {
          alto =ancho;
          ancho=842;
        }

         Rectangle pageSize = new Rectangle(ancho, alto);

        Document doc = new Document(pageSize, 60, 40, 40, 40);//
        doc.addSubject("Plan de pgos");

        return doc;
    }


          public String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons +/* "_" + fmt.format(new Date()) +*/ "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    public String directorioArchivo(String user) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }


     /************************************** DATOS Inversionistas***************************************************/
     protected PdfPTable DatosGenerales() throws DocumentException, SQLException, Exception {
      int numero_solicitud = Integer.parseInt(request.getParameter("numsolc"));
      GestionSolicitudAvalService  gservice = new GestionSolicitudAvalService (this.usuario.getBd());
      NitDAO nitDAO = new NitDAO(this.usuario.getBd());
      NitSot objnit = new NitSot();
      Negocios neg= model.Negociossvc.getNegocio();
      SolicitudPersona sp = gservice.buscarPersona(numero_solicitud, "S");
      objnit =nitDAO.searchNit(neg.getNit_tercero());
        String fecha = Util.getFechahoy();
        PdfPTable tabla_temp = new PdfPTable(2);
        java.awt.Color fondo = new java.awt.Color(42, 136, 200); 
        java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);

        Font fuente_header = new Font(Font.HELVETICA, 12, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
        //double vlr=this.Total_pagado(lista);
        double vlr = 10;
        float[] medidaCeldas = {0.300f, 0.520f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 10);
        Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Informac�on Titular", fuente_header));
        celda_temp.setBackgroundColor(fondo);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setColspan(2);
        tabla_temp.addCell(celda_temp);


        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Cliente", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(sp.getNombre().toUpperCase(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("No ID", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(sp.getIdentificacion(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Establecimiento de Comercio:", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(objnit.getNombre().toUpperCase(),fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setVerticalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        CiudadService cs = new CiudadService(usuario.getBd());


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Fecha de Negocio", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(neg.getFecha_neg().substring(0,10),fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


                celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Referencia de Pago", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(neg.getCod_negocio(),fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);



        tabla_temp.setWidthPercentage(80);



        return tabla_temp;
    }




        /************************************** Movimientos***************************************************/
    protected PdfPTable liquidacion() throws DocumentException {

        double vlr = 0, ip = 0;
        PdfPTable tabla_temp = new PdfPTable(3);
        try {

            java.awt.Color fondo = new java.awt.Color(42, 136, 200);
            java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);
            float[] medidaCeldas = {0.100f, 0.100f, 0.100f};
            tabla_temp.setWidths(medidaCeldas);

            Font fuente = new Font(Font.HELVETICA, 10);
            Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
            Font fuente_header = new Font(Font.HELVETICA, 10, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            PdfPCell celda_temp = new PdfPCell();
            celda_temp = new PdfPCell();

            // columna 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Cuota", fuente_header));
            celda_temp.setBackgroundColor(fondo);

            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Fecha de Pago", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);


            celda_temp.setPhrase(new Phrase("Pago Mensual", fuente_header));

            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);


            double total=0;

            ArrayList<DocumentosNegAceptado> itemsLiquidaciom = new ArrayList();
            itemsLiquidaciom= model.Negociossvc.getLiquidacion();
            Negocios neg= model.Negociossvc.getNegocio();
            String fecha = null;
            String dia = null;


            if (neg.isFinanciaAval()|| model.Negociossvc.isNegocioAval(neg.getCod_negocio())) {


                if (neg.isFinanciaAval()){
                Negocios negAval=model.Negociossvc.buscarNegocioAval(neg.getCod_negocio());/// se busca el negocio de aval del negocio original
                model.Negociossvc.setLiquidacionAval(model.Negociossvc.buscarDetallesNegocio(negAval.getCod_negocio()));
                }
                else
                {
                  Negocios negRel=model.Negociossvc.buscarNegocio(neg.getCod_negocio());
                  model.Negociossvc.setLiquidacionAval(model.Negociossvc.buscarDetallesNegocio(negRel.getCod_negocio()));
                }


                ArrayList<DocumentosNegAceptado> itemsLiquidacionAval = new ArrayList();
                itemsLiquidacionAval= model.Negociossvc.getLiquidacionAval();
                for (int i = 0; i < itemsLiquidaciom.size(); i++) {
                    DocumentosNegAceptado items = (DocumentosNegAceptado) itemsLiquidaciom.get(i);
                    DocumentosNegAceptado itemsAval = (DocumentosNegAceptado) itemsLiquidacionAval.get(i);

                    total=total+items.getValor()+itemsAval.getValor();


                    celda_temp = new PdfPCell();
                    celda_temp.setPhrase(new Phrase(items.getItem(), fuenteB));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);


                    celda_temp = new PdfPCell();


                    if (i == 0) {
                        fecha = itemsLiquidaciom.get(i).getFecha().substring(0, 10);
                        dia = itemsLiquidaciom.get(0).getFecha().substring(8, 10);
                    } else {
                        fecha = nextFecha(fecha, dia);
                    }

                    
                    celda_temp.setPhrase(new Phrase(fecha, fuente));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);

                    celda_temp = new PdfPCell();
                    celda_temp.setPhrase(new Phrase(Util.customFormat(items.getValor()+itemsAval.getValor()), fuente));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);

                }
            } else {
                    total=Double.parseDouble(neg.getvalor_aval());                    
                    for (int i = 0; i < itemsLiquidaciom.size(); i++) {
                    DocumentosNegAceptado items = (DocumentosNegAceptado) itemsLiquidaciom.get(i);
                    total=total+items.getValor();

                    celda_temp = new PdfPCell();
                    celda_temp.setPhrase(new Phrase(items.getItem(), fuente));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);

                    if (i == 0) {
                        fecha = itemsLiquidaciom.get(i).getFecha().substring(0, 10);
                        dia = itemsLiquidaciom.get(0).getFecha().substring(8, 10);
                    } else {
                        fecha = nextFecha(fecha, dia);
                    }

                    celda_temp.setPhrase(new Phrase(fecha, fuente));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);



                    celda_temp = new PdfPCell();
                    if ( sumarAval(items.getDias(), itemsLiquidaciom) ) { //condicion_anterior: Integer.parseInt(items.getItem()) == 1
                        valorCodigoConAval = items.getValor()+Double.parseDouble(neg.getvalor_aval());
                        FechaPrimeraCuota = fecha;
                        celda_temp.setPhrase(new Phrase(Util.customFormat(items.getValor()+Double.parseDouble(neg.getvalor_aval())), fuente));
                    }else{
                    celda_temp.setPhrase(new Phrase(Util.customFormat(items.getValor()), fuente));
                    }

                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);

            }

            }

                    celda_temp = new PdfPCell();
                    celda_temp.setPhrase(new Phrase("Total", fuenteB));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    celda_temp.setColspan(2);
                    tabla_temp.addCell(celda_temp);





                    celda_temp = new PdfPCell();
                    celda_temp.setPhrase(new Phrase(Util.customFormat(total), fuente));
                    celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                    tabla_temp.addCell(celda_temp);
        } catch (Exception e) {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }




     public Paragraph phtml(String texto) throws IOException
    {
            //Creas un p�rrafo:
            Paragraph parrafo = new Paragraph();
            //Estableces el tipo de alineaci�n:
            parrafo.setAlignment(Element.ALIGN_JUSTIFIED);
            //As� se alinear� todo lo que tengas en el p�rrfo. Si el texto, por ejemplo se toma de un HTML, que puede estar formateado y con varios p�rrafos, todos los p�rrafos que tenga el HTML se justifican:
            ArrayList htmlObjs = HTMLWorker.parseToList(new StringReader(texto),this.stylehtml());
            for (int k = 0; k < htmlObjs.size(); ++k) {
                Element elementHtml = (Element) htmlObjs.get(k);
                //Vamos a�adiendo el elemento HTML al p�rrafo anterior:
                parrafo.add(htmlObjs.get(k));
            }   //A�adimos el p�rrafo al PDF:

            return parrafo;

    }

    public StyleSheet stylehtml() throws IOException
    {

            StyleSheet  styles = new StyleSheet();
            styles.loadTagStyle("body", "face", "times new roman");
            styles.loadTagStyle("body", "size", "14px");
            styles.loadStyle("nota", "size", "8px");
            return  styles;

    }


     private void LiquidacionConvenio() throws Exception {

        GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
        HttpSession session = request.getSession();
        String cod_neg = request.getParameter("cod_neg");
        int id_convenio = Integer.parseInt(request.getParameter("id_convenio") != null ? request.getParameter("id_convenio") : "0");
        Negocios negocio = model.Negociossvc.buscarNegocio(cod_neg);
        negocio.setId_convenio(id_convenio);
        SolicitudAval bean_sol = gsaserv.buscarSolicitud(negocio.getCod_negocio());
        session.setAttribute("nitcm", negocio.getCod_cli());
        String tipo_liq = "tipo_liq";
        boolean mostrarTodo = false;
        double valor_operation = 0;
        double custodiax = 0;
        String tipoCalculo = request.getParameter("tipoCalculo") != null ? request.getParameter("tipoCalculo") : "";

        String numero_solicitud = negocio.getNo_solicitud();
        String ciclo = negocio.getCiclo();
        String prop = request.getParameter("cmbTipoCliente");
        String Aux = "";
        String tasa = negocio.getTasa();
        if (numero_solicitud != null) {
            Aux = "&numero_solicitud=" + numero_solicitud + "&ciclo=" + ciclo + "&prop=" + prop;
        }


        String ParNumero_Cheques = "" + negocio.getNodocs();
        String ParValor_Negocio = "" + negocio.getVr_negocio();
        String Forma_Pago = negocio.getFpago();
        // String tituloValor[] = request.getParameter("cmbTituloValor").split("_");
        String tipo_negocio = negocio.getTneg();
        String Estado_Remesa = negocio.getMod_rem();
        String fechai = negocio.getFecha_neg();
        String propietario = negocio.getNit_tercero();
        String numero_form = bean_sol.getNumeroSolicitud();
        String aval = request.getParameter("aval") != null ? request.getParameter("aval") : negocio.getMod_aval();
        String remesas = "1";
        String custodia = negocio.getMod_cust();
        String facturaTercero = "hidFacturaTercero";
        int idProvConvenio = (negocio.getIdProvConvenio());
        String idConvenio = "" + negocio.getId_convenio();
        String banco = (negocio.getBcod()!=null?negocio.getBcod():"0");
        String ciudadCh = (negocio.getBcocode()!=null?negocio.getBcocode():"0");
        String colorfila = "";
        String idRemesa = "0";
        String idsector = negocio.getSector();
        String idsubsector = negocio.getSubsector();
        // String tipoCalculo = request.getParameter("tipoCalculo")==null?"liquidador":request.getParameter("tipoCalculo");
        String parametros = "";
        double comisionTercero = 0;
        double porrem = 0;

        //Crea un string con los parametros recibidos para ser reutilizado cuando se calcule el aval y la remesa
        for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
            String param = (String) e.nextElement();
            if (!param.equals("aval") && !param.equals("remesas")) {
                parametros += "&" + param + "=" + request.getParameter(param);
            }
        }

        if (tipoCalculo.equals("calculoAval")) {
            if (aval.equals("0")) {
                aval = "1";
            } else {
                aval = "0";
            }
        }
        if (Forma_Pago == null || Forma_Pago.equals("")) {
            Forma_Pago = model.FenalcoFintraSvc.obtenerPrimeraFechaPago(ciclo, fechai);
        }

        int Numero_Cheques = Integer.parseInt(ParNumero_Cheques);
        boolean esPropietario;
        if (facturaTercero.equals("t")) {
            if (prop.equals("30")) {
                esPropietario = true;
            } else {
                esPropietario = false;
            }
        } else {
            esPropietario = false;
        }

        //Si se selecciono una sede guardar el nit del afiliado
        Proveedor proveedor = model.proveedorService.buscarNitAfiliado(propietario);
        if (proveedor == null) {
            throw new Exception("Error: no existe el proveedor");
        }
        if (proveedor.getSede().equals("S")) {
            propietario = proveedor.getNit_afiliado();
        }
        //buscar convenio
        GestionConveniosDAO convenioDao = new GestionConveniosDAO(usuario.getBd());
        Convenio convenio = convenioDao.buscar_convenio(usuario.getBd(), idConvenio);
        //buscar la comision del tercero
        for (int i = 0; i < convenio.getConvenioComision().size(); i++) {
            ConvenioComision convComision = convenio.getConvenioComision().get(i);
            if (convComision.isComision_tercero()) {
                comisionTercero = convComision.getPorcentaje_comision();
                break;
            }
        }
        Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
        if (impuesto == null) {
            throw new Exception("Error: el convenio " + convenio.getId_convenio() + " no tiene impuesto vigente");
        }
        Propietario Propietario = new Propietario();
        Propietario = model.gestionConveniosSvc.buscarProvConvenio(negocio.getNit_tercero(), idConvenio, idsector, idsubsector);

        if (Propietario == null) {
            throw new Exception("Error: no existe el registro en prov_convenio");
        }
        idProvConvenio = Integer.parseInt(Propietario.getInicio());
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        int plazoPrimeraCuota = Integer.parseInt(Forma_Pago);
        if (numero_solicitud != null) {
            plazoPrimeraCuota = Integer.parseInt(prop);
        }
        ArrayList Aval = gserv.buscarRangosAval(idProvConvenio, tipo_negocio, plazoPrimeraCuota, esPropietario);
        if (Aval == null || Aval.size() == 0) {
            mensaje = "Error: No hay condiciones de aval para estos parametros";
            throw new Exception("Error: no esta registrado el aval para los parametros ingresados");
        }

        double[] diasremix = new double[Numero_Cheques + 2];

        double Valor_Negocio = Double.parseDouble(ParValor_Negocio);

        double Tasa = Propietario.getTasa_fenalco() / 100;
        if (tasa.equals("N")) {
            SolicitudAval s = gsaserv.buscarSolicitud(Integer.parseInt(numero_form));
            Negocios neg = model.Negociossvc.buscarNegocio(s.getCodNegocio());
            Tasa = Double.parseDouble(neg.getTasa()) / 100;
        }
        double Custodia_Cheque = Propietario.getCustodiacheque();

        if (Estado_Remesa.equals("0")) {
            //OBTENER PORCENTAJE REMESA
            ConveniosRemesas cr = new ConveniosRemesas();
            cr.setDstrct(usuario.getDstrct());
            cr.setId_convenio(idConvenio);
            cr.setCiudad_sede(proveedor.getCiudad());
            cr.setBanco_titulo(banco);
            cr.setCiudad_titulo(ciudadCh);
            ConveniosRemesas remesa = model.gestionConveniosSvc.buscarRemesa(cr);
            if (remesa == null) {
                mensaje = "Error: No hay condiciones de aval para estos parametros";
                throw new Exception("Error: no esta registrado el valor de la remesa para los parametros ingresados");
            } else {
                if (remesa.isGenera_remesa()) {
                    porrem = remesa.getPorcentaje_remesa() / 100;
                    idRemesa = remesa.getId_remesa();
                } else {
                    Estado_Remesa = "1";
                }
            }
            cr = null;
        }

        java.util.Calendar Fecha_Sugerida = null;
        java.util.Calendar Fecha_Negocio = Util.crearCalendarDate(fechai);
        java.util.Calendar Fecha_Sugerida_2 = null;
        java.util.Calendar Fecha_Sugerida_3 = null;

        int dia_valido = 0;
        int dia = 0;
        int mes = 0;
        int ano = 0;
        int dia_s = 0;
        int mes_s = 0;
        int ano_s = 0;

        dia = Integer.parseInt(fechai.substring(8, 10));
        mes = Integer.parseInt(fechai.substring(5, 7));
        ano = Integer.parseInt(fechai.substring(0, 4));
        Fecha_Negocio.set(ano, mes - 1, dia);

        double Formula = 0;
        double Suma = 0;
        double Valor_Cheque = 0;
        double Valor_Cheque_X = 0;
        double Desembolso = 0;
        double Valor_Capital = 0;
        double Valor_Interes = 0;
        double Valor_Remesa = 0;
        double Saldo_Final = 0;
        double Saldo_Inicial = 0;
        double Valor_Doc = 0;
        double Formulas[];
        double ValorNum = 0;
        double TotalAvalComisionTerc = 0;
        double ValorAval = 0;
        double ValorAvalIva = 0;
        double TotalAval = 0;
        double ValorComisionTercero = 0;
        double ValorComisionTerceroIva = 0;
        double valor_1 = 0;
        double valor_2 = 0;
        double valor_3 = 0;
        double valor_4 = 0;
        double dias = 0;
        Suma = 0;
        double lim = 11655;
        double TotalRemesa_x = 0;

        String Filas_Tabla_Liquidacion = "";
        String Filas_Tabla_Liquidacion_2 = "";
        String Dias_Primer_Cheque = Forma_Pago;
        Saldo_Final = 0;

        Formula = (((Math.pow((1 + Tasa), 12)) - 1));

        Formulas = new double[Numero_Cheques + 1];

        ArrayList chequesx = new ArrayList();
        chequeCartera chequeCarterax = new chequeCartera();
        for (int fila = 1; fila <= Numero_Cheques; fila++) {//se recorrera cada cheque para armarle su fila

            chequeCarterax = new chequeCartera();

            if (fila % 2 == 0) {
                colorfila = "filagris";
            } else {
                colorfila = "filaazul";
            }

            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "<tr class='" + colorfila + "'>";
            //Numero del documento
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <td>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <p align='center'>" + String.valueOf(fila) + "</p>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    </td>";

            chequeCarterax.setItem("" + fila);

            // Fechas
            if (fila == 1) {

                if (!Forma_Pago.equals("30")) {
                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                } else {

                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);
                }

            } else {

                if (!Forma_Pago.equals("45")) {

                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes, ano, Forma_Pago);

                } else {

                    Fecha_Sugerida = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_2 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                    Fecha_Sugerida_3 = Util.TraerFecha(fila, dia, mes - 1, ano, Forma_Pago);

                }

            }

            if (!Forma_Pago.equals("30")) {
                if (fila == 1) {
                    dia = Fecha_Sugerida.get(Calendar.DATE);
                    mes = Fecha_Sugerida.get(Calendar.MONTH);
                    ano = Fecha_Sugerida.get(Calendar.YEAR);
                }

            }

            dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
            mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
            ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);

            if (mes_s == 12) {
                ano_s = ano_s - 1;
            }
            if (mes_s == 0) {
                mes_s = 1;
            } else if (mes_s != 1) {
                mes_s = mes_s + 1;

            } else if (mes_s == 1) {
                mes_s = mes_s = 2;
            }
            int j = 0;

            while (model.FenalcoFintraSvc.buscarFestivo(ano_s + "-" + mes_s + "-" + dia_s)) {

                mes_s = mes_s - 1;
                Fecha_Sugerida_2.set(ano_s, mes_s, dia_s);
                Fecha_Sugerida_2.add(Calendar.DAY_OF_YEAR, 1);

                dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
                mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
                ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
                mes_s = mes_s + 1;


                j = 1;

            }

            if (j == 1) {
                if (mes_s != 0) {
                    Fecha_Sugerida_2.set(ano_s, mes_s - 1, dia_s);
                } else {
                    Fecha_Sugerida_2.set(ano_s, 1, dia_s);
                }
            }
            if (fila != 1) {
                if (mes_s != 12) {
                    Fecha_Sugerida_3.set(ano_s, mes_s - 1, dia_s);
                }
            } else {
                Fecha_Sugerida_3.set(ano_s, mes_s, dia_s);
            }
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <td>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <p align='right' >" + Util.ObtenerFechaCompleta(Fecha_Sugerida) + "</p>";
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    </td>";

            String diaremix = "", mesremix = "";
            if (Fecha_Sugerida.get(java.util.Calendar.MONTH) < 9) {
                mesremix = "0" + (Fecha_Sugerida.get(java.util.Calendar.MONTH) + 1);
            } else {
                mesremix = "" + (Fecha_Sugerida.get(java.util.Calendar.MONTH) + 1);
            }
            if (Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH) < 10) {
                diaremix = "0" + Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH);
            } else {
                diaremix = "" + Fecha_Sugerida.get(java.util.Calendar.DAY_OF_MONTH);
            }

            String fechitaremix = (Fecha_Sugerida.get(java.util.Calendar.YEAR)) + "-" + mesremix + "-" + diaremix;

            chequeCarterax.setFechaCheque("" + fechitaremix);

            if (fila != 1) {
                dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
            } else {
                dias = Util.diasTranscurridos(Fecha_Negocio, Fecha_Sugerida_2);
            }

            if (mostrarTodo) {
                Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <td>";
                Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    <p align='right' >" + dias + "</p>";
                Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "    </td>";
            }

            diasremix[fila - 1] = dias;

            chequeCarterax.setDias("" + dias);

            valor_1 = (dias / 365);
            valor_2 = (1 + Formula);
            valor_3 = Math.pow(valor_2, valor_1);
            valor_4 = ((1 / valor_3));

            double val = Util.redondear(valor_4, 9);
            Formulas[fila] = val;

            Suma = Suma + Formulas[fila];
            Filas_Tabla_Liquidacion = Filas_Tabla_Liquidacion + "</tr>";
            if (j == 1) {

                Fecha_Sugerida_2.set(ano_s, mes_s + 1, dia_s);
                dia_s = Fecha_Sugerida_2.get(Calendar.DATE);
                mes_s = Fecha_Sugerida_2.get(Calendar.MONTH);
                ano_s = Fecha_Sugerida_2.get(Calendar.YEAR);
            }
            chequesx.add(chequeCarterax);
        }
        Valor_Cheque = Util.redondear((Valor_Negocio / (Suma)), 0);

        //Valor_Cheque = Suma;
        custodia = "1";
        //aval 0: sin aval. //custodia 0: sin custodia //Estado_Remesa 1: sin remesa //Estado_Remesa 0 y remesa 0: con remesa el establecimiento. //Estado_Remesa 0 y remesa 1: con remesa el cliente.

        if (custodia.equals("1") && aval.equals("1") && Estado_Remesa.equals("1")) { //remesas.equals("0")){//__a//si hay custodia y hay aval y no hay remesa //creo que ok

            for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {//se calculara el valor inicial del aval

                if (diasremix[fila] > ((fila + 1) * 30)) {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                } else {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                }

                ValorAval = (Valor_Cheque * ValorNum);
                ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1() / 100));
                ValorComisionTercero = (Valor_Cheque * (comisionTercero / 100));
                ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1() / 100));
                TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva) + (ValorComisionTercero + ValorComisionTerceroIva);
                TotalAval = TotalAval + ValorAval + ValorAvalIva;
            }

            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);//custodia
            valor_operation = Valor_Negocio;//valor operacion inicial
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));//valor segundo del cheque
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));

            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation) >= 0.5) {//se calculara el valor del aval,el valor del cheque
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalAval = 0;
                TotalAvalComisionTerc = 0;
                for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                    if (diasremix[fila] > ((fila + 1) * 30)) {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                    } else {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                    }

                    ValorAval = (Valor_Cheque * ValorNum);
                    ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1() / 100));
                    ValorComisionTercero = (Valor_Cheque * (comisionTercero / 100));
                    ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1() / 100));
                    TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva) + (ValorComisionTercero + ValorComisionTerceroIva);
                    TotalAval = TotalAval + ValorAval + ValorAvalIva;
                }
            }

        }
        //aval 0: sin aval. //custodia 0: sin custodia //Estado_Remesa 1: sin remesa //Estado_Remesa 0 y remesa 0: con remesa el establecimiento. //Estado_Remesa 0 y remesa 1: con remesa el cliente.

        //si custodia asumida por cliente(1) se suma a valor del cheque , sino no
        if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1")) {//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma));// + (Custodia_Cheque));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma));// + (Custodia_Cheque));
        }

        //si custodia asumida por cliente(1) se suma a valor del cheque , sino no
        if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1") && (1 == 2)) {//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma) + (Custodia_Cheque));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma) + (Custodia_Cheque));
        }

        if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1") && (1 == 2)) {//remesas.equals("0")){//_//si hay custodia y no hay aval y no hay remesa //creo que ok
            custodiax = 0;
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma));//+ (Custodia_Cheque));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma));//+ (Custodia_Cheque));
        }

        if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el cliente

            for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {//_

                if (diasremix[fila] > ((fila + 1) * 30)) {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                } else {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                }

                ValorAval = (Valor_Cheque * ValorNum);
                ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1() / 100));
                ValorComisionTercero = (Valor_Cheque * (comisionTercero / 100));
                ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1() / 100));
                TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva) + (ValorComisionTercero + ValorComisionTerceroIva);
                TotalAval = TotalAval + ValorAval + ValorAvalIva;
            }
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            valor_operation = Valor_Negocio + TotalAvalComisionTerc + TotalRemesa_x;
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));

            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }

            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation + TotalRemesa_x) >= 0.5) {
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc + TotalRemesa_x;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalAval = 0;
                TotalAvalComisionTerc = 0;
                for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                    if (diasremix[fila] > ((fila + 1) * 30)) {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                    } else {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                    }

                    ValorAval = (Valor_Cheque * ValorNum);
                    ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1() / 100));
                    ValorComisionTercero = (Valor_Cheque * (comisionTercero / 100));
                    ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1() / 100));
                    TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva) + (ValorComisionTercero + ValorComisionTerceroIva);
                    TotalAval = TotalAval + ValorAval + ValorAvalIva;
                }
                TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
                if (TotalRemesa_x < (lim * Numero_Cheques)) {
                    TotalRemesa_x = (lim * Numero_Cheques);
                }
            }
        }//endif

        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {//caso ok a//si hay custodia y no hay aval y la remesa la asume el cliente

            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            TotalAval = 0;
            TotalAvalComisionTerc = 0;
            valor_operation = Valor_Negocio + TotalAvalComisionTerc + TotalRemesa_x;
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation + TotalRemesa_x) >= 1.0) {
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc + TotalRemesa_x;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
                if (TotalRemesa_x < (lim * Numero_Cheques)) {
                    TotalRemesa_x = (lim * Numero_Cheques);
                }
            }
        }

        if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento

            for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                if (diasremix[fila] > ((fila + 1) * 30)) {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                } else {
                    ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                }

                ValorAval = (Valor_Cheque * ValorNum);
                ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1() / 100));
                ValorComisionTercero = (Valor_Cheque * (comisionTercero / 100));
                ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1() / 100));
                TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva) + (ValorComisionTercero + ValorComisionTerceroIva);
                TotalAval = TotalAval + ValorAval + ValorAvalIva;
            }
            TotalRemesa_x = 0;
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            valor_operation = Valor_Negocio + TotalAvalComisionTerc + TotalRemesa_x;
            Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));

            while ((Valor_Negocio + custodiax + TotalAvalComisionTerc - valor_operation + TotalRemesa_x) >= 1.0) {
                valor_operation = Valor_Negocio + custodiax + TotalAvalComisionTerc + TotalRemesa_x;
                Valor_Cheque = /*Math.round*/ (((valor_operation) / Suma));
                Valor_Cheque_X = /*Math.round*/ (((valor_operation) / Suma));
                TotalAval = 0;
                TotalAvalComisionTerc = 0;
                for (int fila = 0; fila <= Numero_Cheques - 1; fila++) {

                    if (diasremix[fila] > ((fila + 1) * 30)) {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila + 1))) / 100;
                    } else {
                        ValorNum = (Double.parseDouble("" + Aval.get(fila))) / 100;
                    }

                    ValorAval = (Valor_Cheque * ValorNum);
                    ValorAvalIva = (ValorAval * (impuesto.getPorcentaje1() / 100));
                    ValorComisionTercero = (Valor_Cheque * (comisionTercero / 100));
                    ValorComisionTerceroIva = (ValorComisionTercero * (impuesto.getPorcentaje1() / 100));
                    TotalAvalComisionTerc = TotalAvalComisionTerc + (ValorAval + ValorAvalIva) + (ValorComisionTercero + ValorComisionTerceroIva);
                    TotalAval = TotalAval + ValorAval + ValorAvalIva;
                }
            }
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
        }//endif

        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y no hay aval y la remesa la asume el establecimiento
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = /*Math.round*/ (((Valor_Negocio) / Suma));
            Valor_Cheque_X = /*Math.round*/ (((Valor_Negocio) / Suma));
            TotalAval = 0;
            TotalAvalComisionTerc = 0;
            TotalRemesa_x = (Valor_Cheque + Custodia_Cheque) * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
        }

        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0")) && (1 == 2)) {//caso ok a//si hay custodia y no hay aval y la remesa la asume el establecimiento
            custodiax = 0;
            custodiax = (Custodia_Cheque * Numero_Cheques);
            Valor_Cheque = Math.round(((Valor_Negocio) / Suma) + (Custodia_Cheque));
            Valor_Cheque_X = Math.round(((Valor_Negocio) / Suma) + (Custodia_Cheque));
            //////////////////////////////////////////////////////////////////////////////////////
            TotalAval = 0;
            TotalAvalComisionTerc = 0;
            TotalRemesa_x = Valor_Cheque * porrem * Numero_Cheques;//valor_remesa
            if (TotalRemesa_x < (lim * Numero_Cheques)) {
                TotalRemesa_x = (lim * Numero_Cheques);
            }
        }

        if ((Valor_Cheque * porrem) < lim) {//si el valor de la remesa es menor que el limite
            Valor_Remesa = lim;//valor_remesa
        } else {//si el valor de la remesa es mayor o igual que el limite
            Valor_Remesa = Valor_Cheque * porrem;//valor_remesa
        }

        //si custodia es asumida por establecimiento se resta a desembolso , sino no
        //si aval es asumido por establecimiento se ignora, sino se suma a desembolso
        //si remesa es asumida por establecimiento se resta a desembolso , si no no
        //ojo con remesa

        if (aval.equals("0")) {//si no hay aval
            Desembolso = Valor_Negocio;
        }
        if (aval.equals("1")) {//si hay aval
            Desembolso = Valor_Negocio + TotalAvalComisionTerc;
        }

        if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento
            Desembolso = Desembolso - TotalRemesa_x;
        }
        if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {//caso ok a//si hay custodia y hay aval y la remesa la asume el establecimiento
            Desembolso = Desembolso - TotalRemesa_x;
        }

        for (int fila = 1; fila <= Numero_Cheques; fila++) {//para cada cheque
            chequeCartera chequeCarterax2 = (chequeCartera) chequesx.get(fila - 1);
            if (fila % 2 == 0) {
                colorfila = "filagris";
            } else {
                colorfila = "filaazul";
            }

            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "<tr class='" + colorfila + "'>";

            //Valor de los intereses
            Valor_Capital = (Valor_Cheque_X * Formulas[fila]);//capitalito
            Valor_Interes = (Valor_Cheque_X - Valor_Capital);//interesito
            Valor_Doc = Math.round(Valor_Capital + Valor_Interes);//valor_cheque
            if (custodia.equals("1") && aval.equals("0") && Estado_Remesa.equals("1")) {
                Valor_Capital = (Valor_Cheque_X * Formulas[fila]) + Custodia_Cheque;//capitalito
                Valor_Interes = (Valor_Cheque_X - Valor_Capital) + Custodia_Cheque;//interesito
                Valor_Doc = Math.round(Valor_Capital + Valor_Interes)/*+Custodia_Cheque*/;//valor_cheque
            }

            if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("0"))) {
                Valor_Capital = (Valor_Cheque_X * Formulas[fila]) + Custodia_Cheque;//capitalito
                Valor_Interes = (Valor_Cheque_X - Valor_Capital) + Custodia_Cheque;//interesito
                Valor_Doc = Math.round(Valor_Capital + Valor_Interes)/*+Custodia_Cheque*/;//valor_cheque
            }

            if (fila == 1) {
                //si el aval lo asume el cliente se le suma al saldo, sino no
                Saldo_Inicial = Math.round(Valor_Negocio + TotalAvalComisionTerc);//saldo inicial
                if (aval.equals("1")) {//si hay aval
                    Saldo_Inicial = /*Math.round*/ (Valor_Negocio + TotalAvalComisionTerc);//saldo inicial
                }

                if (aval.equals("0")) {//si no hay aval
                    Saldo_Inicial = Math.round(Valor_Negocio + TotalAvalComisionTerc);//saldo inicial
                }

                Saldo_Inicial = Saldo_Inicial + Math.round(TotalRemesa_x);

                if (custodia.equals("1") && aval.equals("0") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {
                    Saldo_Inicial = Saldo_Inicial + TotalRemesa_x;
                }

                if (custodia.equals("1") && aval.equals("1") && (Estado_Remesa.equals("0") && remesas.equals("1"))) {
                    Saldo_Inicial = Saldo_Inicial + TotalRemesa_x;
                }

            } else {
                Saldo_Inicial = Math.round(Saldo_Final);//nuevo saldo inicial
            }

            Saldo_Final = Math.round(Saldo_Inicial - Valor_Capital);//saldo final

            if (sw_ver_saldos) {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >" + String.valueOf(Util.customFormat(Saldo_Inicial)) + "</p>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            }
            chequeCarterax2.setSaldoInicial("" + Saldo_Inicial);

            if (mostrarTodo) {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='center' >" + String.valueOf(Util.customFormat(Valor_Capital)) + "" + "" + "</p>";///
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            }

            chequeCarterax2.setValorCapital("" + Valor_Capital);
            double valor_aval99 = 0;
            String porci = "0";
            if (mostrarTodo) {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='center' >" + String.valueOf(Util.customFormat(Valor_Interes)) + "</p>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
                if (Aval != null) {
                    valor_aval99 = Valor_Doc * ((impuesto.getPorcentaje1() / 100) + 1) * (Double.parseDouble("" + Aval.get(fila))) / 100;
                    porci = ("" + valor_aval99) + "_" + Aval.get(fila);
                }
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='center' >" + porci + "</p>";
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            } else {
                if (Aval != null) {
                    valor_aval99 = Valor_Doc * ((impuesto.getPorcentaje1() / 100) + 1) * (Double.parseDouble("" + Aval.get(fila))) / 100;
                    porci = ("" + valor_aval99) + "_" + Aval.get(fila);
                }
            }

            chequeCarterax2.setValorInteres("" + Valor_Interes);
            chequeCarterax2.setNoAval("" + valor_aval99);

            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >" + String.valueOf(Util.customFormat(Valor_Doc)) + "</p>";//clave
            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";

            chequeCarterax2.setValor("" + Valor_Doc);
            if (sw_ver_saldos) {
                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <td>";
                if (fila == Numero_Cheques) {
                    Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >0.00</p>";

                } else {
                    Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    <p align='right' >" + String.valueOf(Util.customFormat(Saldo_Final)) + "</p>";

                }

                Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "    </td>";
            }


            //////////////
            if (fila == Numero_Cheques) {
                chequeCarterax2.setSaldoFinal("0");
            } else {
                chequeCarterax2.setSaldoFinal("" + Saldo_Final);
            }





            Filas_Tabla_Liquidacion_2 = Filas_Tabla_Liquidacion_2 + "</tr>";

        }

        //Enviar valores a la interfaz
        request.setAttribute("numsolc", numero_form);
        request.setAttribute("tipo_liq", tipo_liq);
        request.setAttribute("Valor_Negocio", Valor_Negocio);
        request.setAttribute("Numero_Cheques", Numero_Cheques);
        request.setAttribute("Desembolso", Desembolso);
        request.setAttribute("Valor_Doc", Valor_Doc);
        request.setAttribute("Fecha_Negocio", Fecha_Negocio);
        request.setAttribute("custodia", custodia);
        request.setAttribute("aval", aval);
        request.setAttribute("Estado_Remesa", Estado_Remesa);
        request.setAttribute("remesas", remesas);
        request.setAttribute("tipo_negocio", tipo_negocio);
        request.setAttribute("ParNumero_Cheques", ParNumero_Cheques);
        request.setAttribute("ParValor_Negocio", ParValor_Negocio);
        request.setAttribute("Forma_Pago", Forma_Pago);
        request.setAttribute("fechai", fechai);
        request.setAttribute("propietario", propietario);
        request.setAttribute("idProvConvenio", idProvConvenio);
        request.setAttribute("convenio", idConvenio);
        request.setAttribute("banco", banco);
        request.setAttribute("ciudadCh", ciudadCh);
        request.setAttribute("facturaTercero", facturaTercero);
        request.setAttribute("numero_solicitud", numero_solicitud);
        request.setAttribute("ciclo", ciclo);
        request.setAttribute("prop", prop);
        request.setAttribute("Aux", Aux);
        request.setAttribute("Tasa", Tasa);
        request.setAttribute("Custodia_Cheque", Custodia_Cheque);
        request.setAttribute("porrem", porrem);
        request.setAttribute("TotalAval", TotalAval);
        request.setAttribute("TotalRemesa_x", TotalRemesa_x);
        request.setAttribute("idRemesa", idRemesa);
        request.setAttribute("chequesx", chequesx);
        request.setAttribute("parametros", parametros);
        request.setAttribute("Filas_Tabla_Liquidacion", Filas_Tabla_Liquidacion);
        request.setAttribute("Filas_Tabla_Liquidacion_2", Filas_Tabla_Liquidacion_2);
        request.setAttribute("idsect", idsector);
        request.setAttribute("idsubsect", idsubsector);
        request.setAttribute("opcion", "1");

        parametros = "";

    }

    private void ReliquidacionRedescuento() throws Exception {

        HttpSession session = request.getSession();
        GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
        String cod_neg = request.getParameter("cod_neg");
        Negocios neg = model.Negociossvc.buscarNegocio(cod_neg);
        neg.setFinanciaAval(false);
        neg.setVr_negocio(Math.rint(Double.parseDouble(request.getParameter("monto_sujerido"))));
        neg.setBcod(neg.getBcod() != null ? neg.getBcod() : "");
        neg.setBcocode(neg.getBcocode() != null ? neg.getBcocode() : "");
        GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());
        String tipo_liq = request.getParameter("tipo_liq");
        SolicitudAval bean_sol = gsaserv.buscarSolicitud(neg.getCod_negocio());
        neg.setId_sector(bean_sol.getSector());
        neg.setId_subsector(bean_sol.getSubsector());


        Propietario Propietario = new Propietario();
        Propietario = model.gestionConveniosSvc.buscarProvConvenio(neg.getNit_tercero(), String.valueOf(neg.getId_convenio()), neg.getSector(), neg.getSubsector());
        neg.setTasa(Propietario.getTasa_fenalco() + "");
        neg.setIdProvConvenio(Propietario.getId());
        neg.setFecha_liquidacion(neg.getFecha_neg());
        Convenio convenio = convenioserv.buscar_convenio(usuario.getBd(), neg.getId_convenio() + "");

        Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
        neg.setValor_seguro(convenio.getValor_seguro());
        neg.setvalor_remesa("0");
        //Si se selecciono una sede guardar el nit del afiliado
        Proveedor proveedor = model.proveedorService.buscarNitAfiliado(neg.getNit_tercero());
        if (proveedor == null) {
            throw new Exception("Error: no existe el proveedor");
        }
        if (proveedor.getSede().equals("S")) {
            neg.setNit_tercero(proveedor.getNit_afiliado());
        }
        // calculo segurro debe ser por porcentaje
        convenio.getValor_seguro();
        neg.setValor_seguro(0);
        if (neg.getMod_rem().equals("0")) //Obtener datos de la remesa
        {
            ConveniosRemesas cr = new ConveniosRemesas();
            cr.setDstrct(usuario.getDstrct());
            cr.setId_convenio(String.valueOf(neg.getId_convenio()));
            cr.setCiudad_sede(proveedor.getCiudad());
            cr.setBanco_titulo(neg.getBcod());
            cr.setCiudad_titulo(neg.getBcocode());


            ConveniosRemesas Objremesa = model.gestionConveniosSvc.buscarRemesa(cr);

            if (Objremesa == null) {
                throw new Exception("Error: No existe condicione sde remesas para esos parametros");
            }

            neg.setPor_rem(Objremesa.getPorcentaje_remesa());
            neg.setId_remesa(Integer.parseInt(Objremesa.getId_remesa()));

            if (Objremesa != null) {
                //calcular remesa
                double exp = 1 - (Math.pow((1 + (Propietario.getTasa_fenalco() / 100)), -neg.getNodocs()));
                double vremesa = (neg.getPor_rem() / 100) * (neg.getVr_negocio() / (exp / (Propietario.getTasa_fenalco() / 100)));

                vremesa = vremesa + neg.getVr_cust() + neg.getValor_seguro();
                neg.setvalor_remesa(vremesa + "");
            }

            neg.setVr_cust(Propietario.getCustodiacheque());
        }
        model.Negociossvc.calcularLiquidacionNegocio(neg, usuario, impuesto);//calcular liquidacion

        model.Negociossvc.setNegocio(neg);
        //Enviar valores a la interfaz
        request.setAttribute("idProvConvenio", neg.getIdProvConvenio());
        request.setAttribute("Custodia_Cheque", 0);
        request.setAttribute("banco", neg.getBcod());
        request.setAttribute("ciudadCh", neg.getBcocode());
        request.setAttribute("tipo_liq", tipo_liq);
        request.setAttribute("fechai", neg.getFecha_neg());
        request.setAttribute("numsolc", bean_sol.getNumeroSolicitud());
        request.setAttribute("tipo_negocio", neg.getTneg());
        request.setAttribute("opcion", "2");
    }
    
public void ValidaPreaprobado() throws Exception {

      
        String nit = request.getParameter("nit");
        String convenio = request.getParameter("convenio");      
        GestionSolicitudAvalService gservice = new GestionSolicitudAvalService(usuario.getBd());
        model.tablaGenService.buscarDatos("CONVENIO", "FENALCO_ATLANTICO");
        TablaGen tConvenio = model.tablaGenService.getTblgen();
        if(tConvenio.getReferencia().equals(convenio))
        {
        boolean resp = gservice.ValidarClientePreaprobado(nit);
        this.printlnResponse(String.valueOf(resp), "text/plain");
       }
}

/*
 *Valida cual item es el que tiene menor numero de dias y a ese se le sumara el valor del aval.
 */
    private boolean sumarAval(String dias, ArrayList<DocumentosNegAceptado> items) {
        boolean menor = true;
        try {
            int diasItem = Integer.parseInt(dias);
            for (int i = 0; i < items.size(); i++) {
                DocumentosNegAceptado itemActual = (DocumentosNegAceptado) items.get(i);
                int diasItemActual = Integer.parseInt(itemActual.getDias());
                if (diasItem > diasItemActual) {
                    menor = false;
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return menor;
    }

    /*
     *Devuelve la fecha de pago siguiente para la cuota(item) que se esta proccesando en ese momento.
     */
    private String nextFecha(String fecha, String dia) {
        String fechaSplit[] = fecha.split("-");
        String anio = fechaSplit[0];
        String mes = fechaSplit[1];        

        if (!mes.equals("12")) {
            int m = Integer.parseInt(mes) + 1;
            mes = (m > 9) ? String.valueOf(m) : "0" + String.valueOf(m);
        } else {
            int a = Integer.parseInt(anio) + 1;
            anio = String.valueOf(a);
            mes = "01";
        }

        if (dia.equals("31") && (mes.equals("04") || mes.equals("06") || mes.equals("09") || mes.equals("11"))) {
            dia = "30";
        }

        if (mes.equals("02") && (dia.equals("29") || dia.equals("30") || dia.equals("31"))) {
            int a = Integer.parseInt(anio);
            GregorianCalendar bisiesto = new GregorianCalendar();
            dia = bisiesto.isLeapYear(a) ? "29" : "28";
        }

        return anio + "-" + mes + "-" + dia;
    }

     private String calcularRenovacion(){
         String resta="";
        try {
     


           // String numero_solicitud = request.getParameter("num_sol");
            String identificacion = request.getParameter("identificacion");


            //aqui validamos que sea una renovacion el formulario digitado..
            GestionSolicitudAvalService gsas = new GestionSolicitudAvalService(usuario.getBd());

           // boolean sw = gsas.isRenovacion(numero_solicitud);
            System.out.println("antes de entrar....");
                   
            
            
                /*validamos que cumpla las politicas de renovacion
                 *solo para los creditos que tinen el 70% de la obligacion
                 * cumplidad en cartera.... */

                ArrayList<BeanGeneral> listRe = gsas.validarRenovacion(identificacion);

                if (!listRe.isEmpty()) {
                    System.out.println("entrar....");
                   
                    BeanGeneral bean = listRe.get(0);
                    String cod_neg = bean.getValor_03();
                    int nrs_documento = Integer.parseInt(bean.getValor_02());
                    int docPagos= Integer.parseInt(bean.getValor_07());
                    int vrl_negAnterior = Integer.parseInt(bean.getValor_01());//valor negocio
                    int vrl_cancelado = Integer.parseInt(bean.getValor_04());//capital pagado
                    String aldia = bean.getValor_05();//dice si esta al  dia con la cartera 
                    String fecha_pc = bean.getValor_06();
                    
                    double valorPagado = vrl_negAnterior * 0.70;
                    int cuotasPendientes = nrs_documento - docPagos;
                    double cuotasPagadas = nrs_documento * 0.70; 

                    //if ((vrl_cancelado >= valorPagado) && cuotasPendientes <= 3) {
                    //if (((nrs_documento <= 14) && (docPagos >= cuotasPagadas)) || ((cuotasPendientes <= 4) && (nrs_documento >= 15)))  {    
                    if (((nrs_documento <= 8 && nrs_documento >= 5) && (cuotasPendientes <= 2)) || ((nrs_documento <= 13 && nrs_documento >= 9) && (cuotasPendientes <= 3)) || (nrs_documento >= 14 && cuotasPendientes <= 4)){  
                        if (nrs_documento == docPagos) {
                            aldia = "S";
                            fecha_pc ="0099-01-01";
                        }
                        
                        int saldo = vrl_negAnterior - vrl_cancelado;
                        double valor_new_neg;
                        valor_new_neg = Math.round((vrl_negAnterior * 1.5) - saldo);
                        
                        resta = Util.FormatoMilesSinDecimal(valor_new_neg).replaceAll(",", "") + ";_;" + aldia + ";_;" + fecha_pc +";_;" + cod_neg ;
                       
                        System.out.println(resta);
                   
                    } else {
                        resta = "N";
                        System.out.println("No cumple con la renovacion de este form...");
                    }

                } else {
                    resta = "N";
                    System.out.println("No ha pagado una form eliminamos el formulario...");
                }

           

        } catch (Exception ex) {
            Logger.getLogger(LiquidadorNegMicrocreditoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    return resta;
    
    }
     
   private String existeConfiguracionFianza(){
       String resp = "";
       try {
           String id_convenio = request.getParameter("idConvenio") != null ? request.getParameter("idConvenio") : "";
           String plazo = request.getParameter("plazo") != null ? request.getParameter("plazo") : "0";
           if (model.Negociossvc.existeValorFianza(id_convenio, Integer.parseInt(plazo))) {
               resp = "SI";
           } else {
               resp = "NO";
           }
       } catch (Exception ex) {
           Logger.getLogger(LiquidadorNegMicrocreditoAction.class.getName()).log(Level.SEVERE, null, ex);
       }
     return resp;
   }
   
    private String obtenerFechaUltimoPago(){
       String resp = "";
       try {
           String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : ""; 
           resp = model.Negociossvc.obtenerUltimaFechaPago(identificacion);          
       } catch (Exception ex) {
           Logger.getLogger(LiquidadorNegMicrocreditoAction.class.getName()).log(Level.SEVERE, null, ex);
       }
     return resp;
   }

    private Document createDoc() {
        Document doc = new Document(PageSize.LETTER, 52, 50, 20, 0);

        return doc;
    }
    
    private String buscarPreaprobadoform() {
        String resp = "N";
        try {
            String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
            BeanGeneral bg = model.Negociossvc.buscarPreaprobadoform(identificacion);
            if (!bg.getValor_01().equals("") && !bg.getValor_02().equals("") && !bg.getValor_03().equals("") && !bg.getValor_04().equals("") && !bg.getValor_05().equals("") && !bg.getValor_06().equals("")) {
                resp = bg.getValor_01() + ";_;" + bg.getValor_02() + ";_;" + bg.getValor_03() + ";_;" + bg.getValor_04() + ";_;" + bg.getValor_05() + ";_;" + bg.getValor_06();
            }
        } catch (Exception ex) {
            Logger.getLogger(LiquidadorNegMicrocreditoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resp;
    }
    
    /**
     * Nuevo metodo que imprime el plan de pago de un credito
     * @param userlogin
     * @return
     * @throws Exception
     */
    public boolean exportarPlanPagosFintra(String userlogin) throws Exception {



        boolean generado = true;
        String directorio = "";
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "f_plan_de_pagos", "pdf");
            Font fuente = new Font(Font.HELVETICA, 10);
            Font fuenteB = new Font(Font.HELVETICA, 12, Font.BOLD);
            Document documento = null;
            documento = this.createDoc("H");
            PdfWriter Pdfriter = PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.open();
            Paragraph p = new Paragraph("PLAN DE PAGOS", fuenteB);
            p.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(p);
            documento.add(Chunk.NEWLINE);


            /*----------------------------------Datos generales --------------------------------------*/
            PdfPTable tabla_datos = this.DatosGenerales();
            tabla_datos.setHorizontalAlignment(tabla_datos.ALIGN_CENTER);
            tabla_datos.setWidthPercentage(85);
            documento.add(tabla_datos);
            documento.add(Chunk.NEWLINE);

            /*----------------------------------liquidaciones--------------------------------------*/
            PdfPTable tabla_liquidacion =liquidacionFintra();
            tabla_liquidacion.setWidthPercentage(100);
            documento.add(tabla_liquidacion);
            documento.add(Chunk.NEWLINE);

            Negocios neg= model.Negociossvc.getNegocio();
//            if(!neg.isFinanciaAval() && !model.Negociossvc.isNegocioAval(neg.getCod_negocio()))
//            {
//
//            documento.add(phtml("<span class =nota>La primera cuota es mas alta debido a que esta incluye el valor correspondiente a la comision cobrada por Fenalco por concepto de aval</span> "));
//            }
        
            documento.add(new Paragraph(new Phrase("Nota Importante:", fuenteB)));
            documento.add(new Paragraph(new Phrase("En caso de incurrir en mora, la entidad podr� cobrar intereses y gastos de cobranza, de acuerdo con lo establecido por ley.", fuente)));
            documento.add(new Paragraph(new Phrase("Para mayor informaci�n, puede comunicarse a los telefonos: ___________________ Ext. ______,", fuente)));
            documento.add(new Paragraph(new Phrase("Correo electronico: ______________________,", fuente)));
            documento.add(new Paragraph(new Phrase("Direccion: _______________________", fuente)));
                    //+ "____________________________<br/>"
                    //+ "Firma de Aceptaci�n<br/>"
                    //+ "<br/><br/><br/></span> "));
            //documento.add(new Paragraph(new Phrase("Cordialmente,", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("__________________________________", fuente)));
            documento.add(new Paragraph(new Phrase("Firma de Aceptaci�n", fuente)));
            
            
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(phtml("<center><span class =nota > C�digo para pago de cuota </span></center><br/>"));
            
            
            
            String cod_neg = neg.getCod_negocio();
            //facturas= model.Negociossvc.obtenerValorPimeraCuota(cod_neg);
            liqui= model.Negociossvc.getLiquidacion();
            
            float[] widths2 = {0.34f, 0.18f, 0.26f, 0.22f};
            PdfPTable thead = new PdfPTable(widths2); 
            PdfPTable thead2 = new PdfPTable(widths2); 
            String consneg = neg.getCod_negocio().substring(2);
            String ceros = "00000000";
            consneg = consneg.length() == 8 ? consneg : ceros.substring(0, 8-consneg.length())+consneg;
            PdfPCell celda = new PdfPCell();
            PdfPCell celda2 = new PdfPCell();
            Barcode128 uccEan128 = new Barcode128();
            uccEan128.setCodeType(Barcode.CODE128_UCC);
            PdfContentByte cb = Pdfriter.getDirectContent();
            
            
                                
                        
            //Codigo de barras cuotas restantes
            celda2.setBorderWidth(0);
            String valor = ((int) liqui.get(1).getValor() + "").replace(".", "");
            int ultimafecha = (neg.getNodocs()-1);
            valor = valor.length() == 8 ? valor : ceros.substring(0, 8-valor.length())+valor;
            
            uccEan128.setCode("(415)" + model.tablaGenService.obtenerTablaGen("FITRAGS1", "01").getDato()
                    + "(8020)" + consneg  + "(3900)" + valor
                    + "(96)" + liqui.get(ultimafecha).getFecha().substring(0, 10).replaceAll("-", ""));
            uccEan128.setBarHeight(48f);
            uccEan128.setSize(8f);
            
            celda2.addElement(uccEan128.createImageWithBarcode(cb, java.awt.Color.BLACK,
                    java.awt.Color.BLACK));

            celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda2.setColspan(4);
            thead2.addCell(celda2);
            celda2 = new PdfPCell();
            celda2.setBorderWidth(0);
            celda2.setPhrase(new Phrase(" ", fuente));
            celda2.setColspan(4);
            celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            thead2.addCell(celda2);

            documento.add(thead2);
            
            documento.close();

        } catch (Exception e) {
            generado = false;
            System.out.println("error al generar plan de pagos pdf : " + e.toString());
            e.printStackTrace();
        }


        return generado;
    }
    
    /**
     *
     * @return
     * @throws DocumentException
     */
    protected PdfPTable liquidacionFintra() throws DocumentException {

        double vlr = 0, ip = 0;
        PdfPTable tabla_temp = new PdfPTable(3);
        try {

            java.awt.Color fondo = new java.awt.Color(42, 136, 200);
            java.awt.Color color_fuente_header = new java.awt.Color(0xFF, 0xFF, 0xFF);
            float[] medidaCeldas = {0.100f, 0.100f, 0.100f};
            tabla_temp.setWidths(medidaCeldas);

            Font fuente = new Font(Font.HELVETICA, 10);
            Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
            Font fuente_header = new Font(Font.HELVETICA, 10, Font.BOLD);
            fuente_header.setColor(color_fuente_header);
            PdfPCell celda_temp = new PdfPCell();
            celda_temp = new PdfPCell();

            // columna 1
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Cuota", fuente_header));
            celda_temp.setBackgroundColor(fondo);

            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase("Fecha de Pago", fuente_header));
            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);


            celda_temp.setPhrase(new Phrase("Pago Mensual", fuente_header));

            celda_temp.setBackgroundColor(fondo);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);


            double total=0;

            ArrayList<DocumentosNegAceptado> itemsLiquidaciom = new ArrayList();
            itemsLiquidaciom= model.Negociossvc.getLiquidacion();
            Negocios neg= model.Negociossvc.getNegocio();
            String fecha = null;
            String dia = null;

                for (int i = 0; i < itemsLiquidaciom.size(); i++) {
                DocumentosNegAceptado items = (DocumentosNegAceptado) itemsLiquidaciom.get(i);
                total=total+items.getValor();

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(items.getItem(), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

                if (i == 0) {
                    fecha = itemsLiquidaciom.get(i).getFecha().substring(0, 10);
                    dia = itemsLiquidaciom.get(0).getFecha().substring(8, 10);
                } else {
                    fecha = nextFecha(fecha, dia);
                }

                celda_temp.setPhrase(new Phrase(fecha, fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);



                celda_temp = new PdfPCell();
//                if ( sumarAval(items.getDias(), itemsLiquidaciom) ) { //condicion_anterior: Integer.parseInt(items.getItem()) == 1
//                    valorCodigoConAval = items.getValor()+Double.parseDouble(neg.getvalor_aval());
//                    FechaPrimeraCuota = fecha;
//                    celda_temp.setPhrase(new Phrase(Util.customFormat(items.getValor()+Double.parseDouble(neg.getvalor_aval())), fuente));
//                }else{
//                    celda_temp.setPhrase(new Phrase(Util.customFormat(items.getValor()), fuente));
//                }
                
                 celda_temp.setPhrase(new Phrase(Util.customFormat(items.getValor()), fuente));

                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);

            }

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Total", fuenteB));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                celda_temp.setColspan(2);
                tabla_temp.addCell(celda_temp);
                
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase(Util.customFormat(total), fuente));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tabla_temp.addCell(celda_temp);
        } catch (Exception e) {

            System.out.println("error al generar pdf : " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }

    private boolean generarPlanPagosNegocios(String login, int numero_solicitud) throws Exception {
        boolean generado = true;
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        JsonObject infoCabecera = gserv.getInfoPlanDePagos(numero_solicitud);
        JsonArray infoDetalle = gserv.getDetallePlanDePagos(numero_solicitud);
        JsonObject totalDetalle = gserv.getTotalDetallePlanDePagos(numero_solicitud);
        String ruta_destino = "";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String rutaOrigenPdf = rb.getString("rutaInformes");
        DecimalFormat formatea = new DecimalFormat("###,###.##");

        Font fuente = new Font(Font.HELVETICA, 7);
        Font fuente2 = new Font(Font.HELVETICA, 7,Font.BOLD);
        PdfReader reader=null;
        if (!infoCabecera.toString().equals("{}")) {
            ruta_destino = this.directorioArchivo(login, "f_plan_de_pagos", "pdf");
            
            if(infoCabecera.get("id_unidad_negocio").getAsInt()==1){
               reader = new PdfReader(rutaOrigenPdf + "/PLAN_PAGOS_MICRO.pdf");
            }else if(infoCabecera.get("id_unidad_negocio").getAsInt()==31){
               reader = new PdfReader(rutaOrigenPdf + "/PLAN_PAGOS_EDU.pdf");
            }
           
            if (infoCabecera.get("nro_docs").getAsInt()<=20) {
//                stamper.insertPage(2, reader.getPageSizeWithRotation(1));
                reader.selectPages("1-1");
            }
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(ruta_destino));
            Phrase phrase = null;
            PdfContentByte over = stamper.getOverContent(1);
            ColumnText ct = new ColumnText(over);
//          ******Primera Linea****** 
            ct.setSimpleColumn(70, 455, 600, 10);
            phrase = new Phrase(infoCabecera.get("nomcli").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

            ct.setSimpleColumn(360, 455, 700, 10);
            phrase = new Phrase(infoCabecera.get("cod_cli").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

            ct.setSimpleColumn(610, 455, 800, 10);
            phrase = new Phrase(infoCabecera.get("cod_neg").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

//          ******Segunda Linea******
            ct.setSimpleColumn(80, 443, 600, 10);
            phrase = new Phrase(infoCabecera.get("descripcion").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

            ct.setSimpleColumn(375, 443, 700, 10);
            phrase = new Phrase(infoCabecera.get("nro_docs").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

            ct.setSimpleColumn(612, 443, 800, 10);
            phrase = new Phrase(infoCabecera.get("base_liquidacion").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

//          *****Tercera Linea****** 
            ct.setSimpleColumn(110, 431, 600, 10);
            phrase = new Phrase("$"+formatea.format(infoCabecera.get("vr_negocio").getAsDouble()), fuente);
            ct.setText(phrase);
            ct.go();
            
             if(infoCabecera.get("id_unidad_negocio").getAsInt()==1){
                 
                ct.setSimpleColumn(348, 431, 700, 10);
               phrase = new Phrase(infoCabecera.get("cuotas_aval").getAsString(), fuente);
               ct.setText(phrase);
               ct.go();

               ct.setSimpleColumn(610, 431, 800, 10);
               phrase = new Phrase(infoCabecera.get("tasa_aval").getAsString(), fuente);
               ct.setText(phrase);
               ct.go();
             }
            

//          *****Cuarta Linea****** 
                      
            if(infoCabecera.get("id_unidad_negocio").getAsInt()==1){
                
                ct.setSimpleColumn(102, 420, 600, 10);
                phrase = new Phrase("$"+formatea.format(infoCabecera.get("aval_financiado").getAsDouble()), fuente);
                ct.setText(phrase);
                ct.go();
                
                ct.setSimpleColumn(370, 420, 700, 10);
            }else if(infoCabecera.get("id_unidad_negocio").getAsInt()==31){
                ct.setSimpleColumn(95, 420, 600, 10);
                phrase = new Phrase("$"+formatea.format(infoCabecera.get("valor_aval").getAsDouble()), fuente);
                ct.setText(phrase);
                ct.go();
                
                ct.setSimpleColumn(370, 431, 700, 10);  
            }
            
            phrase = new Phrase(infoCabecera.get("fecha_finalizacion").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();
            
            ct.setSimpleColumn(620, 431, 800, 10);
            if(infoCabecera.get("id_unidad_negocio").getAsInt()==1){
            ct.setSimpleColumn(620, 420, 800, 10);
            }
            phrase = new Phrase(infoCabecera.get("plazo_credito_dias").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

//          *****Quinta Linea****** 
            ct.setSimpleColumn(125, 409, 600, 10);
            phrase = new Phrase(infoCabecera.get("f_desem").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();
            
            ct.setSimpleColumn(365, 420, 700, 10);
            if(infoCabecera.get("id_unidad_negocio").getAsInt()==1){
                ct.setSimpleColumn(365, 409, 700, 10);
            }
            phrase = new Phrase(infoCabecera.get("tasa").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

            ct.setSimpleColumn(603, 420, 800, 10);
            if(infoCabecera.get("id_unidad_negocio").getAsInt()==1){
                ct.setSimpleColumn(603, 409, 800, 10);
            }
            phrase = new Phrase(infoCabecera.get("tasa_mora").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

            //          *****Sexta Linea****** 
            ct.setSimpleColumn(135, 397, 600, 10);
            phrase = new Phrase(infoCabecera.get("sistema_amortizacion").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

            //          *****Septima Linea****** 
            ct.setSimpleColumn(110, 386, 600, 10);
            phrase = new Phrase(infoCabecera.get("asesor").getAsString(), fuente);
            ct.setText(phrase);
            ct.go();

//          ******Octava Linea****** 
//            ct.setSimpleColumn(80, 346, 600, 10);
//            phrase = new Phrase("$"+formatea.format(infoCabecera.get("vr_desembolso").getAsDouble()), fuente);
//            ct.setText(phrase);
//            ct.go();
//
//            ct.setSimpleColumn(250, 346, 700, 10);
//            phrase = new Phrase("$"+formatea.format(infoCabecera.get("total_interes").getAsDouble()), fuente);
//            ct.setText(phrase);
//            ct.go();
//
//            ct.setSimpleColumn(430, 346, 800, 10);
//            phrase = new Phrase("$"+formatea.format(infoCabecera.get("total_seguro").getAsDouble()), fuente);
//            ct.setText(phrase);
//            ct.go();

//          ******Detalle Liquidacion******
            PdfPTable detalle = new PdfPTable(12);
            PdfPTable detalle2 = new PdfPTable(12);
            double aval_financiado=infoCabecera.get("aval_financiado").getAsDouble();
            if(aval_financiado==0){
             detalle = new PdfPTable(10);
             detalle2 = new PdfPTable(10);
            }
            Font fontCabeceraDetalle = new Font(Font.HELVETICA, 8, Font.BOLD);
            fontCabeceraDetalle.setColor(java.awt.Color.WHITE);
            java.awt.Color fondo = new java.awt.Color(3, 187, 221);  //03bbdd hexa
            java.awt.Color fondoOragen = new java.awt.Color(250,173,25);  //faad19 hexa
            PdfPCell cell = new PdfPCell();

            phrase = new Phrase("No. cuotas", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);

            phrase = new Phrase("Fecha limite de pago", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);

            phrase = new Phrase("Periodo de cuotas (dias)", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);
            
            phrase = new Phrase("Capital", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);
            
            phrase = new Phrase("Inter�s corriente", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);
            
            phrase = new Phrase("Seguro de vida deudor", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);

            phrase = new Phrase("Comisi�n Mipyme", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);

            phrase = new Phrase("Intermediaci�n Financiera", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);
            
            phrase = new Phrase("Saldo Capital", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);
            
            if(aval_financiado>0){

            phrase = new Phrase("Capital Aval", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);

            phrase = new Phrase("Inter�s Aval", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondo);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);

//            phrase = new Phrase("Saldo Capital Aval", fontCabeceraDetalle);
//            cell = new PdfPCell(phrase);
//            cell.setBorder(Rectangle.BOX);
//            cell.setBackgroundColor(fondo);
//            cell.setBorderColor(fondo);
//            cell.setVerticalAlignment(Element.ALIGN_CENTER);
//            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//            detalle.addCell(cell);
//            detalle2.addCell(cell);
            }
            
            phrase = new Phrase("Valor cuota", fontCabeceraDetalle);
            cell = new PdfPCell(phrase);
            cell.setBorder(Rectangle.BOX);
            cell.setBackgroundColor(fondoOragen);
            cell.setBorderColor(fondo);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            detalle.addCell(cell);
            detalle2.addCell(cell);
            

            
                        
            if (infoDetalle != null) {

                for (JsonElement det : infoDetalle) {

                    JsonObject objectcode = det.getAsJsonObject();

                    if (objectcode.get("item").getAsInt() <= 20) {

                        phrase = new Phrase(objectcode.get("item").getAsString(), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(objectcode.get("fecha").getAsString(), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(objectcode.get("dias").getAsString(), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);
                        
                        phrase = new Phrase(formatea.format(objectcode.get("capital").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("interes").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("seguro").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("cat").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("cuota_manejo").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("saldo_final").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);
                        
                        if(aval_financiado>0){
                        phrase = new Phrase(formatea.format(objectcode.get("capital_aval").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("interes_aval").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

//                        phrase = new Phrase(formatea.format(objectcode.get("valor_aval").getAsDouble()), fuente);
//                        cell = new PdfPCell(phrase);
//                        cell.setBorder(Rectangle.BOX);
//                        cell.setBorderColor(fondo);
//                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        detalle.addCell(cell);
                        
                        }
                        
                        phrase = new Phrase(formatea.format(objectcode.get("valor").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);


                    } else if (objectcode.get("item").getAsInt() >= 21) {
                        
                        phrase = new Phrase(objectcode.get("item").getAsString(), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(objectcode.get("fecha").getAsString(), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(objectcode.get("dias").getAsString(), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);
                       

                        phrase = new Phrase(formatea.format(objectcode.get("capital").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("interes").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("seguro").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("cat").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("cuota_manejo").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("saldo_final").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);
                        
                        if(aval_financiado>0){
                        phrase = new Phrase(formatea.format(objectcode.get("capital_aval").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(objectcode.get("interes_aval").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

//                        phrase = new Phrase(formatea.format(objectcode.get("valor_aval").getAsDouble()), fuente);
//                        cell = new PdfPCell(phrase);
//                        cell.setBorder(Rectangle.BOX);
//                        cell.setBorderColor(fondo);
//                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
//                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                        detalle2.addCell(cell);
                        
                        }                        
                        
                         phrase = new Phrase(formatea.format(objectcode.get("valor").getAsDouble()), fuente);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);
                    }

                }

            }
            
            if (infoCabecera.get("nro_docs").getAsInt() <= 20) {

                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);
                        
                        phrase = new Phrase(formatea.format(totalDetalle.get("capital").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("interes").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("seguro").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("cat").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("cuota_manejo").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);
                        
                        if(aval_financiado>0){
                        phrase = new Phrase(formatea.format(totalDetalle.get("capital_aval").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("interes_aval").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);

                        }
                        
                        phrase = new Phrase(formatea.format(totalDetalle.get("valor").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle.addCell(cell);


                    } else if (infoCabecera.get("nro_docs").getAsInt() >= 21) {
                        
                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);
                       

                        phrase = new Phrase(formatea.format(totalDetalle.get("capital").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("interes").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("seguro").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("cat").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("cuota_manejo").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase("", fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);
                        
                        if(aval_financiado>0){
                        phrase = new Phrase(formatea.format(totalDetalle.get("capital_aval").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        phrase = new Phrase(formatea.format(totalDetalle.get("interes_aval").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);

                        }                        
                        
                         phrase = new Phrase(formatea.format(totalDetalle.get("valor").getAsDouble()), fuente2);
                        cell = new PdfPCell(phrase);
                        cell.setBorder(Rectangle.BOX);
                        cell.setBorderColor(fondo);
                        cell.setVerticalAlignment(Element.ALIGN_CENTER);
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        detalle2.addCell(cell);
                    }
            
            
            over = stamper.getOverContent(1);
            ct = new ColumnText(over);
            ct.setSimpleColumn(10, 350, 800, 10);
            ct.addElement(detalle);
            ct.go();
            
            Barcode128 uccEan128 = new Barcode128();
            uccEan128.setCodeType(Barcode.CODE128_UCC);
            uccEan128.setCode("(415)" + infoCabecera.get("referencia_1").getAsString() + "(8020)" + infoCabecera.get("referencia_2").getAsString()+ "(3900)" + infoCabecera.get("referencia_3").getAsString()+ "(96)" + infoCabecera.get("referencia_4").getAsString());
            uccEan128.setBarHeight(45f);
            uccEan128.setSize(8f);
            ct.setSimpleColumn(390,565,750,70);   
            ct.addElement(uccEan128.createImageWithBarcode(over, java.awt.Color.BLACK,java.awt.Color.BLACK));
            ct.go();
            
            if(infoCabecera.get("nro_docs").getAsInt()>=21){
                over = stamper.getOverContent(2);
                ct = new ColumnText(over);
                ct.setSimpleColumn(10, 455, 800, 10);
                ct.addElement(detalle2);
                ct.go();
            }
            
            

            
            stamper.close();
            reader.close();
        }
        return generado;
    }

}
