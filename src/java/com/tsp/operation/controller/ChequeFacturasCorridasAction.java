/***************************************
 * Nombre Clase ............. ChequeXFacturaAction.java
 * Descripci�n  .. . . . . .  Genera el Cheque para una factura
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  23/12/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;



public class ChequeFacturasCorridasAction extends Action{
    
    private String PREFIJO_LASER = "LASER ";
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        
        try{
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String   next          = "/jsp/cxpagar/impresion/ChequesFacturaXCorrida.jsp";
            String   msj           = "";
            String   evento        = request.getParameter("evento");
            String   laser         = request.getParameter("laser")!=null? request.getParameter("laser") : "";
            
            if( evento !=null ){
                
                //-- SELECCION DE FILTROS POR CORRIDA, BANCO O SUCURSAL
                
                if ( evento.equals("FILTER") ){
                    next          = "/jsp/cxpagar/impresion/FiltroChequesFacturaXCorrida.jsp";
                    model.ChequeFactXCorridasSvc.loadBancos( usuario.getDstrct(), usuario.getId_agencia() );
                    model.ChequeFactXCorridasSvc.loadSucursales(usuario.getDstrct(), usuario.getId_agencia() );
                }
                
                
                
                //-- CARGAR ARBOL CON FILTROS APLICADOS
                
                if ( evento.equals("LOAD") ){
                    
                    
                    model.ChequeFactXCorridasSvc.loadRequest(request);
                    if( model.ChequeFactXCorridasSvc.existeCorridasConFacturasPago(usuario.getDstrct(), usuario.getId_agencia() ) ){
                        model.ChequeFactXCorridasSvc.buscarFacturasAprobadasFiltro( usuario.getDstrct(), usuario.getId_agencia() );
                    }else{
                        request.setAttribute("msg", "No existen datos para la Corrida " + model.ChequeFactXCorridasSvc.getCorrida() + " con banco " +  model.ChequeFactXCorridasSvc.getBanco()  +" "+   model.ChequeFactXCorridasSvc.getSucursal() + ", Verifique que est� aprobada"  );//Osvaldo
                    }
                    
                    String puede_seleccionar = usuario.getId_agencia().equals("OP")? "SI" : "NO";//Osvaldo
                    request.setAttribute("puede_seleccionar", puede_seleccionar );//Osvaldo
                    
                }
                
                //--- FILTROS DE BLOQUES: EXPAND COLAPSE
                if ( evento.equals("FILTRO") ){
                    String llave = request.getParameter("llave");
                    model.ChequeFactXCorridasSvc.filtro(llave);
                    
                    
                }
                
                
                //--- SELECCIONAR GRUPOS
                if ( evento.equals("SELECTION") ){
                    String  id     = request.getParameter("id");
                    String  ck     = request.getParameter("ck");
                    
                    String rango   = request.getParameter("rango");
                    
                    if( rango == null ){
                        
                        model.ChequeFactXCorridasSvc.filtroSeleccion(id,ck);
                        
                    }else{
                        
                        int ini = Integer.parseInt( request.getParameter("ini") );
                        int fin = Integer.parseInt( request.getParameter("fin") );
                        
                        String id_bloque = "BL_PROVEEDOR_0_0_0_";
                        
                        model.ChequeFactXCorridasSvc.filtroSeleccion( "BL_SUCURSAL_0_0_0","");
                        
                        if( ini > fin ){
                            int temp = ini;
                            ini = fin;
                            fin = temp;
                        }
                        
                        ini = ini-1;
                        fin = fin-1;
                        
                        int cont = ini;
                        while( cont <= fin ){                            
                            model.ChequeFactXCorridasSvc.filtroSeleccion( ( id_bloque+String.valueOf(cont) ),ck);
                            cont++;
                        }
                        
                    }
                }
                
                
                //--- FORMA LOS CHEQUES A PARTIR DE LAS FACTURAS SELECCIONADAS
                if ( evento.equals("FORMARCHEQUES") ){
                    model.ChequeFactXCorridasSvc.loadFacturasSeleccionadas();
                    msj  = model.ChequeFactXCorridasSvc.formarCheques();
                    model.ChequeFactXCorridasSvc.loadNextBloqueCheques();
                    next = "/jsp/cxpagar/impresion/viewChequesFacturaXCorrida.jsp?msj=";
                }
                
                
                //--- ACTUALIZA LA BD Y CARGA EL PROXIMO BLOQUE DE CHEQUES:
                if ( evento.equals("NEXT") ){
                    
                    
                    // Actualiza bd:
                    String sql =  model.ChequeFactXCorridasSvc.updateBloqueCheque( usuario.getLogin() );
                    model.tService.crearStatement();
                    model.tService.getSt().addBatch(sql);
                    model.tService.execute();
                    
                    
                    // Carga proximo bloque:
                    model.ChequeFactXCorridasSvc.loadNextBloqueCheques();
                    model.ChequeFactXCorridasSvc.setEstadoBtnRegresar(false);
                    if ( model.ChequeFactXCorridasSvc.getBloque()!=null  )
                        next = "/jsp/cxpagar/impresion/viewChequesFacturaXCorrida.jsp?msj=";
                    else
                        model.ChequeFactXCorridasSvc.buscarFacturasAprobadasFiltro( usuario.getDstrct(), usuario.getId_agencia() );
                    
                }
                
                /*
                if( evento.equals("LASER") ){
                 
                    String banco = request.getParameter("banco");
                 
                    List  esquema   = model.ImprimirChequeSvc.getEsquema( usuario.getDstrct(), PREFIJO_LASER + banco );
                    if( esquema != null && esquema.size() > 0 ){
                        next = "/jsp/cxpagar/impresion/Cheques.jsp?laser="+laser+"&msj=";
                    }else{
                        msj = "No hay esquema de impresi�n L�ser para el banco "+ banco;
                        next = "/jsp/cxpagar/impresion/viewChequeXFactura.jsp?laser=error&msj=";
                    }
                 
                }
                 */
                
            }
            
            next+= msj;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        } catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
        
    }
    
}
