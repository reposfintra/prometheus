/*
 * DespachoInsertAction.java
 *
 * Created on 28 de marzo de 2005, 09:39 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class GenerarPadreAction extends Action{
    
    
    /** Creates a new instance of DespachoInsertAction */
    public GenerarPadreAction() {
    }
    
    public void run() throws ServletException,InformationException {
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String numpadre="";
        String next="";
        String descripcion = request.getParameter("texto");
        String no_rem= request.getParameter("remesa").toUpperCase();
        String no_rem2= request.getParameter("numrem").toUpperCase();
        try{
            model.tService.crearStatement();
            //Se asigna una serie a la planilla.
            model.seriesService.obtenerSerie( "005", usuario);
            if(model.seriesService.getSerie()!=null){
                Series serie = model.seriesService.getSerie();
                model.tService.getSt().addBatch(model.seriesService.asignarSerie(serie,usuario));
                numpadre=serie.getPrefix()+serie.getLast_number();
            }
            
            Remesa r = new Remesa();
            r.setNumRem(numpadre);
            r.setDescripcion(descripcion);
            r.setCia(usuario.getDstrct());
            r.setUsuario(usuario.getLogin());
            model.tService.getSt().addBatch(model.remesaService.insertRemPadre(usuario.getBase(),r));
            model.tService.execute();
            
            model.remesaService.buscaRemesa(no_rem);
            if(model.remesaService.getRemesa()!=null){
                Remesa rem=model.remesaService.getRemesa();
                String nombreR="";
                String remitentes[]= rem.getRemitente().split(",");
                for(int i=0; i<remitentes.length; i++){
                    nombreR=model.remidestService.getNombre(remitentes[i])+",";
                }
                String nombreD="";
                String destinatarios[]= rem.getDestinatario().split(",");
                for(int i=0; i<destinatarios.length; i++){
                    nombreD=model.remidestService.getNombre(destinatarios[i])+",";
                }
                
                rem.setRemitente(nombreR);
                rem.setDestinatario(nombreD);
                request.setAttribute("remesa",rem);                
            }            
            
            next="/colpapel/agregarOTpadre.jsp?numpadre="+numpadre+"&generado=si&numrem="+no_rem2;
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
        
    }
}
