/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * GestionCondicionesAction.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.operation.controller;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.ExcelApplication;
import java.io.File;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpSession;
/**
 *
 * @author rhonalf
 */
public class GestionCondicionesAction extends Action {

    @Override
    public void run(){
        String next = "/jsp/fenalco/avales/";
        String opcion = "";
        String cadenatabla = "";
        boolean redirect = false;
        GestionCondicionesService gserv = null;
        try {
            opcion = request.getParameter("opcion")!=null ? request.getParameter("opcion") : "";
            gserv = new GestionCondicionesService(((Usuario) request.getSession().getAttribute("Usuario")).getBd());
            if(opcion.equals("buscacli")){
                String texto = request.getParameter("texto")!=null ? request.getParameter("texto") : "";
                String filtro = request.getParameter("filtro")!=null ? request.getParameter("filtro") : "nit";
                ArrayList<Proveedor> lista = null;
                //gserv = new GestionCondicionesService();
                boolean pasa = true;
                //cadenatabla = "<option value='' selected>...</option>";
                try {
                    lista = gserv.buscarProv(texto, filtro);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al buscar datos: "+e.toString());
                    e.printStackTrace();
                }
                if(pasa==true && lista.size()>0){
                    Proveedor prov = null;
                    for (int i = 0; i < lista.size(); i++) {
                        prov = lista.get(i);
                        cadenatabla += "<option value='"+ prov.getC_nit() +"'>"+ prov.getC_payment_name() +"</option>";
                        prov = null;
                    }
                }
                else{
                    cadenatabla = "<option value='' selected>...</option>";
                }
            }
            else if(opcion.equals("busconv")){
                //gserv = new GestionCondicionesService();
                String nitafil = request.getParameter("nitprov")!=null ? request.getParameter("nitprov") : "0";
                ArrayList<BeanGeneral> lista = null;
                BeanGeneral bean = null;
                boolean pasa = true;
                cadenatabla = "<div style='height: 400px; overlay: auto;'><table width='100%' style='border-collapse: collapse;'>" +
                                        "<thead>" +
                                            "<tr class='subtitulo1'>" +
                                                "<th>Convenio</th>" +
                                                "<th>Sector</th>" +
                                                "<th>Subsector</th>" +
                                            "</tr>" +
                                        "</thead></tbody>";
                try {
                    lista = gserv.conveniosProv(nitafil);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al buscar datos: "+e.toString());
                    e.printStackTrace();
                }
                if(pasa==true && lista.size()>0){
                    for (int i = 0; i < lista.size(); i++) {
                        bean = lista.get(i);
                        //modificado darrieta 08/09/2010
                        cadenatabla += "<tr style='cursor:pointer;' class='filaazul' onclick='colocar(\"" + bean.getValor_02() + "\",\"" + bean.getValor_03() + "\",\"" + bean.getValor_04() + "\",\"" +
                                bean.getValor_05() + "\",\"" + bean.getValor_06() + "\",\"" + bean.getValor_07() +
                                "\",\"" + bean.getValor_08() + "\",\"" + bean.getValor_09() + "\",\"" + bean.getValor_01() + "\");cerrarDiv();'>" +

                                                    "<td>"+ bean.getValor_05() +"</td>" +
                                                    "<td>"+ gserv.nomSector(bean.getValor_03()) +"</td>" +
                                                    "<td>"+ gserv.nomSubSector(bean.getValor_03(),bean.getValor_04()) +"</td>" +
                                                  "</tr>";
                        bean = null;
                    }
                }
                else{
                    cadenatabla += "<tr class='filaazul' align='center' style='cursor:pointer;' onclick='cerrarDiv();'>" +
                                                "<td colspan='3'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
            }
            else if(opcion.equals("buscran")){
                String conv = request.getParameter("conv")!=null ? request.getParameter("conv") : "0";
                String tipotit = request.getParameter("tipotit")!=null ? request.getParameter("tipotit") : "0";
                String plazoprimer = request.getParameter("plazoprimer")!=null ? request.getParameter("plazoprimer") : "0";
                boolean prop = request.getParameter("prop")!=null ? Boolean.parseBoolean(request.getParameter("prop")) : false;
                String vista = request.getParameter("vista")!=null ? request.getParameter("vista") : "1";
                ArrayList<BeanGeneral> lista = null;
                BeanGeneral bean = null;
                boolean pasa = true;
                cadenatabla = "<table border='0' width='100%' id='tavales'>" +
                                            "<tr>" +
                                                "<td align='left' width='20%' class='fila'>&nbsp;</td>" +
                                                "<td align='left' width='35%' class='fila'>&nbsp;Inicio</td>" +
                                                "<td align='left' width='15%' class='fila'>&nbsp;Fin</td>" +
                                                "<td align='left' width='25%' class='fila'>&nbsp;Porcentaje</td>" +
                                            "</tr>";
                try {
                    lista = gserv.rangosConvenio(conv, tipotit, plazoprimer, prop);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al buscar datos: "+e.toString());
                    e.printStackTrace();
                }
                String disabled = vista.equals("0")? "":"readonly";
                if(pasa==true && lista.size()>0){
                    String aucadenatabla = "";
                    if(vista.equals("0")){
                        aucadenatabla = "<img alt='+' style='cursor: pointer' onclick='TDAgregarRegistro(\"tavales\",\"avales\")'" +
                                " src='"+ request.getContextPath() +"/images/botones/iconos/mas.gif' width='12' height='12'>" +
                                "&nbsp;<img alt='-'style='cursor: pointer' onclick='TDEliminarRegistro(this,\"tavales\")'" +
                                " src='"+ request.getContextPath() +"/images/botones/iconos/menos1.gif' width='12' height='12'>";
                    }
                    for (int i = 0; i < lista.size(); i++) {
                        bean = lista.get(i);
                        cadenatabla += "<tr>" +
                                                    "<td align='center' class='fila'>"+ aucadenatabla +
                                                    "</td>" +
                                                    "<td align='left' class='fila'>" +
                                                        "<input onkeyup='soloNumeros(this.id);' type='text' id=\"riavales"+(i+1)+"\" name='riavales"+(i+1)+"' value='"+ bean.getValor_01() +"' "+  disabled+">" +
                                                    "</td>" +
                                                    "<td align='left' class='fila'>" +
                                                        "<input  onkeyup='soloNumeros(this.id);validateValueAnt('"+(i+1)+"');' onblur='validateValue('"+(i+1)+"');validateValueAnt('"+(i+1)+"');' type='text' id=\"rfavales"+(i+1)+"\" name='rfavales"+(i+1)+"' value='"+ bean.getValor_02() +"' "+  disabled+">" +
                                                    "</td>" +
                                                    "<td align='left' class='fila'>" +
                                                        "<input onkeyup='soloNumeros(this.id);this.value = formatNumber(this.value);" +
                                                        "' type='text' id=\"pavales"+(i+1)+"\" name='pavales"+(i+1)+"' value='"+ bean.getValor_03() +"' "+  disabled+">" +
                                                    "</td>" +
                                                  "</tr>";
                        bean = null;
                    }
                }
                else{
                    String cadaux = "";
                    if(vista.equals("0")){
                        cadaux = "<img alt='+' style='cursor: pointer' onclick='TDAgregarRegistro(\"tavales\",\"avales\")'" +
                                                    " src='"+ request.getContextPath() +"/images/botones/iconos/mas.gif' width='12' height='12'>" +
                                                    "&nbsp;<img alt='-'style='cursor: pointer' onclick='TDEliminarRegistro(this,\"tavales\")'" +
                                                    " src='"+ request.getContextPath() +"/images/botones/iconos/menos1.gif' width='12' height='12'>";
                    }
                    cadenatabla += "<tr class='filaazul' align='center'>" +
                                                "<td align='center' class='fila'>" +
                                                    cadaux +
                                                "</td>" +
                                                "<td align='left' class='fila'>" +
                                                    "<input onkeyup='soloNumeros(this.id);' type='text' id='riavales1' name='riavales1' "+  disabled+" value='1' readonly>" +
                                                "</td>" +
                                                "<td align='left' class='fila'>" +
                                                    "<input  onkeyup='soloNumeros(this.id);' onblur='validateValue(\"1\");' type='text' id='rfavales1' name='rfavales1' "+  disabled+">" +
                                                "</td>" +
                                                "<td align='left' class='fila'>" +
                                                    "<input onkeyup='soloNumeros(this.id);this.value = formatNumber(this.value);" +
                                                    "' type='text' id='pavales1' name='pavales1' "+  disabled+">" +
                                                "</td>" +
                                              "</tr>";
                }
                cadenatabla += "</tbody></table></div>";
            }
            else if(opcion.equals("export")){
                String conv = request.getParameter("conv")!=null ? (!(request.getParameter("conv").equals(""))?request.getParameter("conv"):"0") : "0";
                String tipotit = request.getParameter("tipotit")!=null ? (!(request.getParameter("tipotit").equals(""))?request.getParameter("tipotit"):"0") : "0";
                String plazoprimer = request.getParameter("plazoprimer")!=null ? (!(request.getParameter("plazoprimer").equals(""))?request.getParameter("plazoprimer"):"0") : "0";
                boolean prop = request.getParameter("prop")!=null ? Boolean.parseBoolean(request.getParameter("prop")) : false;
                ArrayList<BeanGeneral> lista = null;
                boolean pasa = true;
                try {
                    lista = gserv.rangosConvenio(conv, tipotit, plazoprimer, prop);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al buscar datos: "+e.toString());
                    e.printStackTrace();
                }
                if(pasa==true && lista.size()>0){
                    cadenatabla = "Archivo generado sin errores. Revisa el directorio de archivos.";
                    try {
                        this.fabricarDoc(lista);
                    }
                    catch (Exception e) {
                        cadenatabla = "Ocurrio un error al procesar la peticion";
                        System.out.println("Error al exportar las condiciones: "+e.toString());
                        e.printStackTrace();
                    }
                }
                else{
                    cadenatabla = "No se encontraron registros de rangos al procesar la peticion";
                }
            }
            else if(opcion.equals("update")){
                redirect = true;
                next += "CondicionesFenalco.jsp?vista=0";
                ArrayList<String> listarangos = null;
                int filas = request.getParameter("filas")!=null ? Integer.parseInt(request.getParameter("filas")) : 0;
                String diasini = "";
                String diasfin = "";
                String porc = "";
                String conv = request.getParameter("conv")!=null ? (!(request.getParameter("conv").equals(""))?request.getParameter("conv"):"0") : "0";
                String tipotit = request.getParameter("tipotit")!=null ? (!(request.getParameter("tipotit").equals(""))?request.getParameter("tipotit"):"0") : "0";
                String plazoprimer = request.getParameter("plazoprimer")!=null ? (!(request.getParameter("plazoprimer").equals(""))?request.getParameter("plazoprimer"):"0") : "0";
                boolean prop = request.getParameter("prop")!=null ? (!(request.getParameter("prop").equals(""))?Boolean.parseBoolean(request.getParameter("prop")):false) : false;
                String sect = request.getParameter("sect")!=null ? (!(request.getParameter("sect").equals(""))?request.getParameter("sect"):"0") : "0";
                String subsect = request.getParameter("subsect")!=null ? (!(request.getParameter("subsect").equals(""))?request.getParameter("subsect"):"0") : "0";
                if(filas>0){
                    listarangos = new ArrayList<String>();
                    String codcv = "";
                    String codigocond = "";
                    try {
                        /*codcv = gserv.codigoCv(conv, sect, subsect);*/
                        codcv = request.getParameter("hidIdProvConvenio")!=null ? (!(request.getParameter("hidIdProvConvenio").equals(""))?request.getParameter("hidIdProvConvenio"):"0") : "0";
                        codigocond = gserv.codigoCond(codcv, tipotit, plazoprimer, prop);
                    }
                    catch (Exception e) {
                        request.setAttribute("msg", "No se actualizaron filas");
                        System.out.println("Error al buscar codigo: "+e.toString());
                        e.printStackTrace();
                    }
                    HttpSession session = request.getSession();
                    Usuario usuario = (Usuario)session.getAttribute("Usuario");
                    for (int i = 0; i < filas; i++) {
                        diasini = request.getParameter("riavales"+(i+1))!=null ? request.getParameter("riavales"+(i+1)) : "0";
                        diasfin = request.getParameter("rfavales"+(i+1))!=null ? request.getParameter("rfavales"+(i+1)) : "0";
                        porc = request.getParameter("pavales"+(i+1))!=null ? request.getParameter("pavales"+(i+1)) : "0";
                        if(!diasini.equals("0") && !diasfin.equals("0") && !porc.equals("0")) {
                            listarangos.add(diasini+";_;"+diasfin+";_;"+porc);
                        }
                    }
                    if(codigocond.equals("")){
                        try {
                            gserv.insertHeaderConv(codcv, tipotit, plazoprimer, prop, usuario.getLogin());
                            codigocond = gserv.codigoCond(codcv, tipotit, plazoprimer, prop);
                        }
                        catch (Exception e) {
                            request.setAttribute("msg", "No se actualizaron filas");
                            System.out.println("Error al buscar codigo: "+e.toString());
                            e.printStackTrace();
                        }
                    }
                    try {
                        gserv.modificarRangos(listarangos,codigocond,usuario.getLogin());
                        request.setAttribute("msg", "Registros actualizados");
                    }
                    catch (Exception e) {
                        request.setAttribute("msg", "No se actualizaron filas");
                        System.out.println("error: "+e.toString());
                        e.printStackTrace();
                    }
                }
                else{
                    //next += "&msg=No se actualizaron filas";
                    request.setAttribute("msg", "No se actualizaron filas");
                }
            }
            else{
                redirect = true;
                String vista = request.getParameter("vista")!=null ? request.getParameter("vista") : "1";
                next += "CondicionesFenalco.jsp?vista="+vista;
            }
        }
        catch (Exception e) {
            System.out.println("Error en GestionCondicionesAction.java: "+e.toString());
            e.printStackTrace();
        }
        finally {
            try {
                if(redirect==false){
                    this.escribirResponse(cadenatabla);
                }
                else{
                    this.dispatchRequest(next);
                }
            }
            catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta");
                e.printStackTrace();
            }
        }
    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user,String cons,String extension) throws Exception{
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            ruta = ruta + "/" + "Condiones_aval_" + cons +"_" + fmt.format( new Date() ) +"."+extension;
        }
        catch (Exception e) {
            throw new Exception("Error al generar el directorio: "+e.toString());
        }
        return ruta;
    }

    /**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception{
        ExcelApplication excel = new ExcelApplication();
        try{
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short)1, true, (short)12);
            excel.createFont("Subtitulo", "Verdana", (short)0, true, (short)10);
            excel.createFont("Contenido", "Verdana", (short)0, false, (short)10);

            excel.createColor((short)11, (byte)255, (byte)255, (byte)255);//Blanco
            excel.createColor((short)9, (byte)26, (byte)126, (byte)0);//Verde dark
            excel.createColor((short)10, (byte)37, (byte)69, (byte)255);//Azul oscuro

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short)10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short)9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@");

        }
        catch(Exception e){
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return excel;
    }

    private void fabricarDoc(ArrayList<BeanGeneral> lista) throws Exception{
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            ExcelApplication excel = this.instanciar("condiciones");
            String conv = request.getParameter("convtext")!=null ? (!(request.getParameter("convtext").equals(""))?request.getParameter("convtext"):"0") : "0";
            String afil = request.getParameter("afil")!=null ? (!(request.getParameter("afil").equals(""))?request.getParameter("afil"):"0") : "0";
            String sect = request.getParameter("secttext")!=null ? (!(request.getParameter("secttext").equals(""))?request.getParameter("secttext"):"0") : "0";
            String subsect = request.getParameter("subsecttext")!=null ? (!(request.getParameter("subsecttext").equals(""))?request.getParameter("subsecttext"):"0") : "0";
            String tipotit = request.getParameter("tipotit")!=null ? (!(request.getParameter("tipotit").equals(""))?request.getParameter("tipotit"):"0") : "0";
            String plazoprimer = request.getParameter("plazoprimer")!=null ? (!(request.getParameter("plazoprimer").equals(""))?request.getParameter("plazoprimer"):"0") : "0";
            boolean prop = request.getParameter("prop")!=null ? Boolean.parseBoolean(request.getParameter("prop")) : false;
            BeanGeneral bean = null;
            String ruta = this.directorioArchivo(usuario.getLogin(), conv, "xls");
            excel.setDataCell(0, 0, "Afiliado");
            excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            Proveedor prov = null;
            GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
            prov = gserv.buscarProv(afil, "nit").get(0);
            excel.setDataCell(0, 1, prov.getC_payment_name());
            excel.setCellStyle(0, 1, excel.getStyle("estilo3"));
            excel.setDataCell(1, 0, "Convenio");
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, conv);
            excel.setCellStyle(1, 1, excel.getStyle("estilo3"));
            excel.setDataCell(2, 0, "Sector");
            excel.setCellStyle(2, 0, excel.getStyle("estilo2"));
            excel.setDataCell(2, 1, sect);
            excel.setCellStyle(2, 1, excel.getStyle("estilo3"));
            excel.setDataCell(3, 0, "Subsector");
            excel.setCellStyle(3, 0, excel.getStyle("estilo2"));
            excel.setDataCell(3, 1, subsect);
            excel.setCellStyle(3, 1, excel.getStyle("estilo3"));
            excel.setDataCell(4, 0, "Tipo Titulo Valor");
            excel.setCellStyle(4, 0, excel.getStyle("estilo2"));
            excel.setDataCell(4, 1, tipotit);
            excel.setCellStyle(4, 1, excel.getStyle("estilo3"));
            excel.setDataCell(5, 0, "Plazo primer titulo");
            excel.setCellStyle(5, 0, excel.getStyle("estilo2"));
            excel.setDataCell(5, 1, plazoprimer);
            excel.setCellStyle(5, 1, excel.getStyle("estilo3"));
            excel.setDataCell(6, 0, "Propietario");
            excel.setCellStyle(6, 0, excel.getStyle("estilo2"));
            excel.setDataCell(6, 1, prop==true?"Si":"No");
            excel.setCellStyle(6, 1, excel.getStyle("estilo3"));
            excel.setDataCell(8, 0, "Inicio");
            excel.setCellStyle(8, 0, excel.getStyle("estilo2"));
            excel.setDataCell(8, 1, "Fin");
            excel.setCellStyle(8, 1, excel.getStyle("estilo2"));
            excel.setDataCell(8, 2, "Porcentaje");
            excel.setCellStyle(8, 2, excel.getStyle("estilo2"));
            int act = 9;
            for (int i = 0; i < lista.size(); i++) {
                bean = lista.get(i);
                excel.setDataCell(act, 0, bean.getValor_01());
                excel.setCellStyle(act, 0, excel.getStyle("estilo3"));
                excel.setDataCell(act, 1, bean.getValor_02());
                excel.setCellStyle(act, 1, excel.getStyle("estilo3"));
                excel.setDataCell(act, 2, bean.getValor_03());
                excel.setCellStyle(act, 2, excel.getStyle("estilo3"));
                act++;
            }
            excel.saveToFile(ruta);
        }
        catch (Exception e) {
            throw new Exception("Error al fabricar el doc.: "+e.toString());
        }
    }

}
