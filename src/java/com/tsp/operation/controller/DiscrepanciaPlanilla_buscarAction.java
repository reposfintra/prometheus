/************************************************************************
 * Nombre clase: DiscrepanciaPlanilla_buscarAction.java
 * Descripci�n: Accion para buscar una discrepancia dada una planilla.
 * Autor: Jose de la rosa
 * Fecha: 3 de noviembre de 2005, 10:38 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class DiscrepanciaPlanilla_buscarAction extends Action {
    /** Creates a new instance of DiscrepanciaPlanilla_buscarAction */
    public DiscrepanciaPlanilla_buscarAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String numpla = request.getParameter ("numpla").toUpperCase ();
        String tipo = request.getParameter ("tipo").toUpperCase ();
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String next = "";
        try {
            if( model.demorasSvc.existePlanilla (numpla) ) {
                if(tipo.equals ("D")){
                    model.discrepanciaService.listDiscrepanciaPlanilla (numpla,distrito);
                    next = "/jsp/cumplidos/discrepancia/discrepanciaListaCierre.jsp";
                }
                else{
                    next = "/jsp/cumplidos/discrepancia/DiscrepanciaListaCierreGeneral.jsp?c_numpla=" + numpla;
                    model.discrepanciaService.listDiscrepanciaGeneral(numpla,distrito);
                }
            }
            else {
                next = "/jsp/cumplidos/discrepancia/buscarDiscrepanciaPlanilla.jsp?msg=La planilla no tiene discrepancias asociadas";
            }
            
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
