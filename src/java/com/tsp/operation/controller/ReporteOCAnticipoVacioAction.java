/***************************************************************************
 * Nombre clase : ............... ReporteOCAnticipoAction.java             *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de OC con anticipos sin tr�fico  *
 * Autor :.......................  Ing. Enrique De Lavalle                 *
 * Fecha :........................ 23 de noviembre de 2006, 11:36 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;
 
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.*;
/**
 *
 * @author  edelavalle
 */
public class ReporteOCAnticipoVacioAction extends Action{
    
    
    public ReporteOCAnticipoVacioAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            /*
             *Declaracion de variables
             */
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";           
            String fechainicial           = (request.getParameter("fechaini")!=null)?request.getParameter("fechaini"):"";
            String fechafinal             = (request.getParameter("fechafin")!=null)?request.getParameter("fechafin"):"";
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");            
            String dstrct                 = user.getDstrct();            
            
            String next                   = "";
            String Mensaje                = "";
           
            
            /*
             *Manejo de eventos
             */
            
            if (Opcion.equals("Listado")){
                ////System.out.println("dstrct: "+dstrct+" fechainicial: "+fechainicial+" fechafinal: "+fechafinal);
                model.reporteOCVacioAnticipoService.List_oc_vacias(fechainicial, fechafinal, dstrct);
                next = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/reporteOcVacios.jsp?fechainicial="+fechainicial+"&fechafinal="+fechafinal;//Mostrar
            }
             if(Opcion.equals("Excel")){
                ReporteOCAnticipoVaciosTh hilo = new ReporteOCAnticipoVaciosTh();
                hilo.start(model.reporteOCVacioAnticipoService.getListVacios(),fechainicial,fechafinal,user.getDstrct(),user.getLogin());
                 Mensaje = "El reporte se ha generado correctamente";
                next = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/FiltroReporteOcVacios.jsp?Mensaje="+Mensaje;
                
            }
            if (Opcion.equals("Regresar")){
                  next = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/FiltroReporteOcVacios.jsp";
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteOCAnticipoVacioActions .....\n"+e.getMessage());
        }
        
    }
    
}
