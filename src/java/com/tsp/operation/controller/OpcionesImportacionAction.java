/*************************************************************
 * Nombre      ............... OpcionesImportacion.java
 * Descripcion ............... Opciones Generales de Importacion
 * Autor       ............... mfontalvo
 * Fecha       ............... Octubre - 07 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.operation.controller;


import com.tsp.operation.model.threads.HImportaciones;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 * Clase de Opciones de Importacion
 */

public class OpcionesImportacionAction extends Action {
    
    /**
     * Session actual
     */
    HttpSession session = null;
    
    /**
     * Usuario en session
     */
    Usuario     usuario = null;    
    
    /** Crea una nueva unstacia de OpcionesImportacion */
    public OpcionesImportacionAction() {
    }
    
    /**
     * Procedimiento general del Action
     * @throws ServletException .
     * @throws InformationException .
     */    
    public void run() throws ServletException, InformationException  {
        
        try{

            session = request.getSession(); 
            usuario = (Usuario)session.getAttribute("Usuario");        
                   
            
            String next = "/jsp/general/importacion/";
            
            String Opcion    = request.getParameter("Opcion");

            String formato   = Util.coalesce( ((String) request.getParameter("formato")), "").trim().toUpperCase();
            String ntabla    = request.getParameter("ntabla");
            String dtabla    = request.getParameter("dtabla");
            
            String ctab    = request.getParameter("ctab");
            String fcon    = request.getParameter("fcon");
            String bpna    = request.getParameter("bpna");
            String adat    = request.getParameter("adat");
            String titulo  = request.getParameter("titulo");
            String evento  = request.getParameter("evento");
            String ins     = request.getParameter("insert");
            String upd     = request.getParameter("update");
            
            String [] campos  = request.getParameterValues("LOVCMP");
            String [] tipos   = request.getParameterValues("LOVEST");
            String [] extras  = request.getParameterValues("LOVEXT");
            String [] def     = request.getParameterValues("LOVDEF");
            String [] tdef    = request.getParameterValues("LOVTDF");
            String [] vdef    = request.getParameterValues("LOVVAL");
            String [] alias   = request.getParameterValues("LOVALIAS");
            String [] valid   = request.getParameterValues("LOVVALIDACION");
            String [] insert  = request.getParameterValues("LOVINSERCION");
            String [] update  = request.getParameterValues("LOVUPDATE");
            String [] lvalid  = request.getParameterValues("LOV_OBSVALIDACION");
            String [] linsert = request.getParameterValues("LOV_OBSINSERCION");
            
            String [] formatos = request.getParameterValues("formatos");
            
            if (Opcion!=null){
                
                
                ////////////////////////////////////////////////////
                
                if (Opcion.equals("Estructura-Grabar")){
                    model.ImportacionSvc.agregarEstructura(campos, tipos, def, tdef, vdef, alias, valid, insert, update, lvalid, linsert, formato, ntabla, dtabla, usuario.getLogin());
                    model.ImportacionSvc.Buscar(ntabla);
                    next += "imp_estructura.jsp?Mensaje=Su tabla a sido agregada";
                }
                
                else if (Opcion.equals("Estructura-Modificar")){
                    model.ImportacionSvc.modificarEstructura(campos, tipos, def, tdef ,vdef , alias, valid, insert, update, lvalid, linsert, formato, ntabla, dtabla, usuario.getLogin());
                    model.ImportacionSvc.Buscar(formato);
                    next += "imp_estructura.jsp?Mensaje=Modificacion procesada.";
                }
                
                else if (Opcion.equals("Estructura-Buscar")){
                    model.ImportacionSvc.Buscar(formato);
                    next += "imp_estructura.jsp";
                    
                    if (model.ImportacionSvc.getDatos()==null){
                        next += "?Mensaje=El formato indicado no esta registrado en las tablas de Importaciones.";
                    }
                }
                
                else if (Opcion.equals("Estructura-Reset")){
                    model.ImportacionSvc.ResetDato();
                    next += "imp_estructura.jsp";
                }   
                
                else if (Opcion.equals("Estructura-Incluir")){
                    next += "imp_estructura.jsp";
                    
                    model.ImportacionSvc.Buscar(formato);
                    if (model.ImportacionSvc.getDatos()==null) {
                        if (model.ImportacionSvc.Incluir(formato, ntabla, usuario.getLogin())) {
                            model.ImportacionSvc.Buscar(formato);                        
                        }
                        else{
                            next += "?Mensaje=" +
                              "La tabla indicada no se pudo incluir como una tabla de Migracion," +
                              "por favor verifique su consistencia en la base de datos";
                        }
                    } else {
                        next += "?Mensaje=El foramto ya esta registrado en la base de datos.";
                        model.ImportacionSvc.ResetDato();
                    }
                } 
                else if (Opcion.equals("Estructura-Recargar")){
                    next += 
                        "imp_estructura.jsp?Mensaje=" +
                        "Se ha hecho una nueva extraccion de los campos de la base de datos " +
                        "si usted poseia parametros de importacion para los campos presione " +
                        "sobre el boton modificar para mantener los cambios ";
                    model.ImportacionSvc.Recargar(usuario.getLogin());
                    
                }                
                
                else if (Opcion.equals("Estructura-Eliminar")){                    
                    model.ImportacionSvc.eliminar ( new String [] { formato } );
                    model.ImportacionSvc.ResetDato();
                    next += "imp_estructura.jsp?Mensaje=Formato de eliminado de las tablas de migracion.";
                } 
                
                else if (Opcion.equals("Estructura-FormatoArchivo")){
                    try{
                        ConstruirFormatoArchivo(model.ImportacionSvc.getDatos());
                        next += "imp_estructura.jsp?Archivo=ok&Mensaje=Archivo Generado con exito.";
                    }
                    catch (Exception e){
                        next += "imp_estructura.jsp?Mensaje=No se pudo generar el archivo.";
                    }
                }                 
                
                ////////////////////////////////////////////////////

                else if (Opcion.equals("Parametros-Grabar")){
                    model.ImportacionSvc.agregarParametros(formato, ntabla, dtabla, ctab, fcon, adat, bpna, ins, upd, usuario.getLogin());
                    model.ImportacionSvc.Buscar(ntabla);
                    next += "imp_parametros.jsp?Mensaje=Parametros grabados.";
                }
                
                else if (Opcion.equals("Parametros-Modificar")){
                    System.out.println("formato**** "+formato);
                    System.out.println("tabla *****"+ctab);
                    System.out.println("descripcion****"+fcon);
                    model.ImportacionSvc.modificarParametros(formato, ntabla, dtabla, ctab, fcon, adat, bpna, titulo, evento, ins, upd, usuario.getLogin());
                    model.ImportacionSvc.Buscar(formato);
                    next += "imp_parametros.jsp?Mensaje=Parametros modificados.";
                }
                
                else if (Opcion.equals("Parametros-Buscar")){
                    model.ImportacionSvc.Buscar(formato);
                    next += "imp_parametros.jsp";
                }   
                
                else if (Opcion.equals("Parametros-Reset")){
                    model.ImportacionSvc.ResetDato();
                    next += "parametros.jsp";
                }                
                
                ////////////////////////////////////////////////////
                
                else if (Opcion.equals("Listado")){
                    model.ImportacionSvc.listadoGeneral();
                    next += "imp_listado.jsp";
                }  
                
                else if (Opcion.equals("Eliminar")){
                    model.ImportacionSvc.eliminar ( formatos );
                    model.ImportacionSvc.listadoGeneral();
                    next += "imp_listado.jsp?Mensaje=Formatos Eliminados de las tablas de migracion.";
                }
                
                else if (Opcion.equals("Parametros")){
                    next += "parametros.jsp";
                }
                
                else if (Opcion.equals("ListarTablas")){
                    model.ImportacionSvc.ListarTablas();
                    next += "imp_tablas.jsp";
                }                
                
                ////////////////////////////////////////////////////
                
                
                else if (Opcion.equals("Run-Importacion-Auto")){
                    next += "imp_upload.jsp";
                    HImportaciones hilo = new HImportaciones();
                    hilo.start( model, usuario, "", generarConstImportacion(), "Insercion");
                    
                }
                
            }            
            ////System.out.println("------------------VALOR DEL NEXT:"+next);
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new Exception ("no se pudo encontrar " + next );
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException ("Error en OpcionesImportacionAction ...\n" + ex.getMessage());            
        }
    }
    
    /**
     * Genera la lista de contantes de importacion
     * @autor mfontalvo
     * @throws Exception .
     * @return Retorna una lista de valores contastantes de importacion basados en la session
     */    
    private TreeMap generarConstImportacion() throws Exception {  
        
        TreeMap constante = new TreeMap();
        try{
            
            constante.put("_DSTRCT_SESSION_", (String) session.getAttribute("Distrito"));
            constante.put("_USER_SESSION_"  , usuario.getLogin());
            constante.put("_USER_AGENCY_"   , usuario.getId_agencia());
            constante.put("_TODAY_"         , com.tsp.util.Util.getFechaActual_String(6));
            constante.put("_BASE_"          , usuario.getBase());
            
        }catch (Exception ex){
            throw new Exception ("Error en generarConstImportacion OpcionesImportacionDirecta" + ex.getMessage());
        }
        return constante;
        
    }
    
    
    
    /**
     * Procedimiento para construir el formato del archivo de excel para la tabla
     * de importacion actual.
     * @autor mfontalvo
     * @throws Exception .
     */  
    private void ConstruirFormatoArchivo (ImportacionParametros tabla) throws Exception {
        try{
            
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");           
            java.io.File path = new java.io.File ( ruta + "/exportar/migracion/" + usuario.getLogin() );
            path.mkdirs();
            
            com.tsp.operation.model.beans.POIWrite xls = 
                   new  com.tsp.operation.model.beans.POIWrite();
            
            xls.nuevoLibro(path.getAbsolutePath() + "/" + tabla.getFormato() + "_FORMATO.xls");
            
            
            HSSFCellStyle titulo1 = xls.nuevoEstilo("Tahoma", 9 , true    , false, "text"  , HSSFColor.LIME.index   , HSSFColor.YELLOW.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Tahoma", 9 , true    , false, "text"  , HSSFColor.YELLOW.index , HSSFColor.GREEN.index  , HSSFCellStyle.ALIGN_CENTER);
            //HSSFCellStyle titulo3 = xls.nuevoEstilo("Tahoma", 9 , true    , false, "text"  , HSSFColor.BLACK.index  , HSSFColor.SKY_BLUE.index  , HSSFCellStyle.ALIGN_CENTER);
            
            
            
            xls.obtenerHoja("BASE");
            
            if (tabla.getListaCampos()!=null && !tabla.getListaCampos().isEmpty()){
                
                Iterator it = tabla.getListaCampos().iterator();
                while ( it.hasNext() ){
                    ImportacionEstructura campo = (ImportacionEstructura) it.next();
                    xls.adicionarCelda(
                    0,
                    Integer.parseInt(campo.getSecuencia()),
                    campo.getAlias(),
                    (campo.getDefault().equals("S")?titulo1:titulo2)
                    );
                }
            }
            
            xls.cerrarLibro();
        }catch ( Exception ex ){
            throw new Exception ("Error en ConstruirFormatoArchivo [OpcionesImportacionAction] ...\n" + ex.getMessage());
        }
        
    }
        
    
}
