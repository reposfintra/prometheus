/********************************************************************
 *      Nombre Clase.................   ReportePlacaNitFotoAction.java
 *      Descripci�n..................   Genera el reporte de las fotos de Placas y nits
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   06.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  EQUIPO12
 */
public class ReportePlacaNitFotoAction extends Action{
        
        /** Creates a new instance of ReportePlacaNitFotoAction */
        public ReportePlacaNitFotoAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter("FechaI");
        String fechaF = request.getParameter("FechaF");
        String agencia = (request.getParameter("agencia").equals("NADA") ?"%":request.getParameter("agencia"));
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        ////System.out.println("agencia = "+agencia+"  FechaI = "+fechaI+" FechaF = "+fechaF);
        
        //Pr�xima vista
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/hvida/reportes/ReportePlacaNitFoto.jsp?msg=" +
                "El Reporte se ha generado exitosamente en ReportePlacaNitFotos_" +
                FechaFormated1 + ".xls";
        
        try{
            ReportePlacaNitXLS hilo = new ReportePlacaNitXLS(); 
            hilo.start(fechaI, fechaF, usuario, model, agencia);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
        
}
