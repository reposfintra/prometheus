/*******************************************************************
 * Nombre clase: OCargaSearchAction.java
 * Descripci�n: Accion para realizar la buscqueda de una orden de carga
 * Autor: Ing. Karen Reales
 * Fecha: 12 de mayo 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OCargaListarAction extends Action{
    static Logger logger = Logger.getLogger(OCargaListarAction.class);
    
    public OCargaListarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        /**
         *
         * Usuario en Sesion
         */
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        /**
         *
         * Inicializacion Variable tipo String
         */
        String next="/jsp/masivo/ordencargue/listaOCargue.jsp";
        String cliente = request.getParameter("cliente").toUpperCase();
        String user = request.getParameter("usuario").toUpperCase();
        String agencia = request.getParameter("agency").toUpperCase().equalsIgnoreCase("NADA")?"":request.getParameter("agency").toUpperCase();
        
        try{
            
            model.imprimirOrdenService.filtrar(user, agencia, cliente);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
