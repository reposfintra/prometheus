/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.UsuarioAdmonDAO;
import com.tsp.operation.model.DAOS.impl.UsuarioAdmonImpl;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.UsuarioService;
import com.tsp.util.Util;
import javax.servlet.ServletException;

/**
 *
 * @author egonzalez
 */
public class GestionUsuariosAdmonUserAction extends Action {

    private final int CARGA_COMBO = 0;
    private final int CREAR_USUARIO = 1;
    private final int ACTUALIZAR_USUARIO = 2;
    private final int BUSCAR_PERFILES = 3;
    private final int EXISTE_USUARIO = 4;
    private final int PERFILES_FENALCO = 5;
    private final int EXISTE_USUARIO_APP = 6;
    
    private UsuarioAdmonDAO dao = null;
           private String txtResp = "";
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            dao = new UsuarioAdmonImpl();
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGA_COMBO:
                    cargaCombo();
                    break;
                case CREAR_USUARIO:
                    crear();
                    break;
                case ACTUALIZAR_USUARIO:
                    actualizar();
                    break;
                case BUSCAR_PERFILES:
                    buscar();
                    break;
                case EXISTE_USUARIO:
                    UsuarioService us = new UsuarioService();
                    us.buscaUsuario(request.getParameter("usuario"));
                                     txtResp = "{\"mensaje\":\""+((us.getUsuario() != null) ? "Id usuario existe" : "OK" )+"\"}";
                    break;
                case PERFILES_FENALCO:
                    buscarPerfilesFenalco();
                    break;
                case EXISTE_USUARIO_APP:
                   txtResp =  "{\"mensaje\":\""+((buscarUserApp().equals("Ok"))  ? "Ok" : txtResp )+"\"}";
                    break;       
                default:
                    break;
            }
        } catch(Exception exc) {
            txtResp = "{\"error\":\""+exc.getMessage()+"\"}";
        } finally {
            try {
                response.setContentType("application/json; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(txtResp);
            } catch (Exception e) { }
        }
    }
    
    private void cargaCombo() {
        String query = request.getParameter("query");
        String filtro = request.getParameter("filtro");
        txtResp = new Gson().toJson(dao.cargarCombo(query, filtro));        
    }

    private void crear() throws Exception{
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario", ((Usuario)request.getSession().getAttribute("Usuario")).getLogin());
        
        try {
            info.addProperty("claveencr",
                    Util.encript(this.application.getInitParameter("deskey"),
                                info.get("basico").getAsJsonObject().get("clave").getAsString()) );
        }catch(Exception e) {}
        txtResp = new Gson().toJson(dao.crear(info));
    }
    
    private void actualizar() {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario", ((Usuario)request.getSession().getAttribute("Usuario")).getLogin());
        
        try {
            info.addProperty("claveencr",
                    (info.get("basico").getAsJsonObject().get("cambio_clave").getAsBoolean())
                    ? Util.encript(this.application.getInitParameter("deskey"),
                                info.get("basico").getAsJsonObject().get("clave").getAsString())
                    : info.get("basico").getAsJsonObject().get("clave").getAsString());
        }catch(Exception e) {}
        txtResp = new Gson().toJson(dao.Acturalizar(info));
    }
    
    private void buscar() {
        String dstrct = request.getParameter("distrito");
        String usuario = request.getParameter("usuario");
        txtResp = new Gson().toJson(dao.buscar(dstrct, usuario));
    }
    
     private String  buscarUserApp() {
        String usuario = request.getParameter("usuario");
        String resp  = dao.buscarAppm(usuario);
         if (!resp.equals("Ok")) {
              txtResp ="Usuario "+resp +" ya existe en la App";
         } else {
               txtResp ="Ok";
         }
       
        return txtResp;
    }
    
    public JsonObject cargaInicial(String usuario) {
        if (dao == null) dao = new UsuarioAdmonImpl();
        return dao.cargaInicial(usuario);
    }

    private void buscarPerfilesFenalco() {
        String dstrct = request.getParameter("distrito");
        String usuario = request.getParameter("usuario");
        txtResp = new Gson().toJson(dao.buscarPerfilesFenalco(dstrct, usuario));
    }
}
