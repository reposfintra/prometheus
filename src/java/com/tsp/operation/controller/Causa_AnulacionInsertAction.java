/*
 * Codigo_discrepanciaInsertAction.java
 *
 * Created on 28 de junio de 2005, 01:41 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Causa_AnulacionInsertAction extends Action{
    
    /** Creates a new instance of Codigo_discrepanciaInsertAction */
    public Causa_AnulacionInsertAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "/jsp/masivo/causa_anulacion/causa_anulacionInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");   
        String dstrct = usuario.getDstrct();
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        String base = usuario.getBase();
        int sw=0;
        try{
            Causas_Anulacion c_a = new Causas_Anulacion();
            c_a.setDescripcion(descripcion);
            c_a.setCodigo(codigo);
            c_a.setDistrito(dstrct.toUpperCase());
            c_a.setUsuario(usuario.getLogin().toUpperCase());
            c_a.setBase(base);
            model.causas_anulacionService.setCausa_anulacion(c_a);
            try{
                model.causas_anulacionService.insert();
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.causas_anulacionService.exist(codigo)){
                    model.causas_anulacionService.update();                    
                    request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                }
                else{
                    request.setAttribute("mensaje","Error la causa de anulacion ya existe!");
                }
            }
            else{
                request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
