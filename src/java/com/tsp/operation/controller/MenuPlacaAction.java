/*
 * MenuArchivoTXTAction.java
 *
 * Created on 21 de diciembre de 2004, 08:57 AM
 */

package com.tsp.operation.controller;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import com.tsp.operation.model.*;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  mcelin
 */
public class MenuPlacaAction extends Action {
    
    /** Creates a new instance of MenuSeriesAction */
    public MenuPlacaAction() {
    }
    
    public void run() throws ServletException, InformationException {                 
        try {                                                          
            final String next = "/migracion/placa.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException("Error en la Accion [MenuPlacaAction]...\n"+e.getMessage());
        }
     }    
}

