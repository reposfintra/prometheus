/*
 * BuscarRutasAction.java
 *
 * Created on 29 de junio de 2005, 04:19 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jcuesta
 */
public class BuscarRutasAction extends Action{
    
    /** Creates a new instance of BuscarRutasAction */
    public BuscarRutasAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=ok";
        
        String pagina = request.getParameter("pagina");
        HttpSession session = request.getSession();        
        try{
            //HACEMOS EL SW
            int sw = Integer.parseInt(request.getParameter("sw"));
            
            if(sw==1){
                model.rutaService.listarRutas();
            }
            else if(sw==2){
                String origen = !request.getParameter("c_origen").equals("") ? request.getParameter("c_origen").toUpperCase():"%";
                String destino = !request.getParameter("c_destino").equals("") ? request.getParameter("c_destino").toUpperCase():"%";
                if (destino.length()==1){
                    destino+="%";
                }
                if (origen.length()==1){
                    origen+="%";
                }
                Ruta r = new Ruta();                
                r.setOrigen(origen);
                r.setDestino(destino);
                model.rutaService.setRuta(r);
                model.rutaService.consultarRutas();
            }
            else if(sw==3){
                String cia = request.getParameter("cia");
                String origen = request.getParameter("origen");
                String destino = request.getParameter("destino");
                String secuencia = request.getParameter("secuencia");
                model.tramoService.buscarTramo(cia, origen, destino);
                model.rutaService.buscarRuta(cia, origen, destino,secuencia);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
