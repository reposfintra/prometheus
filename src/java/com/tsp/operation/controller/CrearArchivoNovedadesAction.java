/*
 * CrearArchivoNovedadesAction.java
 *
 * Created on 11 de abril de 2005, 08:36 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  KREALES
 */
public class CrearArchivoNovedadesAction extends Action{
    
    /** Creates a new instance of CrearArchivoNovedadesAction */
    public CrearArchivoNovedadesAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String fecha = request.getParameter("fechai");
        String base = request.getParameter("base");
        String mes = fecha.substring(5,7);
        String dia = fecha.substring(8,10);
        String mensaje ="";
        try{
            HNovedadesClipper hnc = new HNovedadesClipper();
            hnc.start(model,mes+dia,usuario.getId_agencia(),fecha,base, usuario.getLogin());
            mensaje = "Su archivo se esta creando en la ruta /exportar/migracion/"+usuario.getLogin()+"/CC"+base.toUpperCase()+fecha.replaceAll("-","")+".txt";
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        String next="/informes/FechasArchivo.jsp?mensaje="+mensaje;
        this.dispatchRequest(next);
    }
    
}
