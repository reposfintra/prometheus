
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.*;
import com.tsp.operation.model.threads.*;


/**
 * Action para comunicar la interfaz JSP
 * con las consultas de base de Datos
 *
 * @Clase ........ ReemplazoChequesAction.java
 * @Autor ........ Ing. Juan Manuel Escand�n P�rez
 * @Fecha ........ 2007-02-17
 * @version ...... 1.0
 * @Copyrigth .... Transporte sanchez Polo S.A.
 */

public class ReemplazoChequesAction extends Action {
    
    /**
     * Constructor de la Clase
     */
    public ReemplazoChequesAction() {
    }
    
    /**
     * Metodo Principal de la Clase
     * @see com.tsp.operation.model.egresoService
     * @throws ServletException .
     * @throws InformationException .
     */
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            
            HttpSession session = (HttpSession) request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            String Opcion       = getParameter("Opcion"     , null);
            String fltBanco     = getParameter("fltBanco"   , "");
            String fltSucursal  = getParameter("fltSucursal", "");
            String fltRangoini  = getParameter("fltRangoini", "");
            String fltRangofin  = getParameter("fltRangofin", "");
            
            String []LOV        = request.getParameterValues("LOV");
            String Mensaje      = "";
            
            String fltDistrito  = usuario.getDstrct();
            
            
            String [] codigos  = request.getParameterValues("codigos");
            String [] fechas   = request.getParameterValues("fechas");
            
            
            if (Opcion!=null){
                
                if (Opcion.equals("Filtrar")){
                    model.egresoService.buscarChequesRangoFechas( fltDistrito, fltBanco, fltSucursal, fltRangoini, fltRangofin);
                    session.setAttribute("fltBanco"   , fltBanco);
                    session.setAttribute("fltSucursal", fltSucursal);
                    session.setAttribute("fltRangoini", fltRangoini);
                    session.setAttribute("fltRangofin", fltRangofin);
                    
                    List listado       =  model.egresoService.getListaCheques();
                    if( listado == null || listado.size() == 0 ){
                        Mensaje = "No Hay datos para los parametros de busqueda";
                    }
                }
                
                else if (Opcion.equals("Actualizar")){
                    String sql                  = "";
                    boolean flag                = true;
                    List    lista_errores       = new LinkedList();
                    List    lista_insertados    = new LinkedList();
                    LogReemplazoCheque  log     = new LogReemplazoCheque();
                    
                    for(int i=0;i<LOV.length;i++){
                        sql                     = "";
                        String  aux []          = LOV[i].split(",");
                        String  banco           = aux[0];
                        String  sucursal        = aux[1];
                        String  cheque          = aux[2];
                        
                        String  banco_reemplazo = "";
                        String  suc_reemplazo   = "";
                        String  concept_code    = ""; //Concepto Movpla
                        String  concepto        = ""; //CXP o ANT para manejo de Serie
                        String  id_serie        = "";//
                        
                        if( aux.length > 3 ) concept_code = aux[3];
                        
                        if( aux.length > 4 ){
                            banco_reemplazo  = aux[4];
                            suc_reemplazo    = aux[5];
                        }
                        else{
                            banco_reemplazo = banco;
                            suc_reemplazo   = sucursal;
                        }
                        
                        
                        
                        /*Objetos Tabla gen*/
                        model.tablaGenService.buscarDatos( "TCHEQUES", concept_code );
                        TablaGen tg =   model.tablaGenService.getTblgen();
                        
                        if ( tg != null ) concepto    = tg.getDato();
                        
                        //Seteo de Objeto
                        Egreso inf              = new Egreso();
                        inf.setDocument_no      (   cheque          );
                        inf.setBranch_code      (   banco           );
                        inf.setBank_account_no  (   sucursal        );
                        inf.setBanco_reemplazo  (   banco_reemplazo );
                        inf.setSuc_reemplazo    (   suc_reemplazo   );
                        
                        //Seteo de Variables
                        
                        /*Anulacion del registro en egreso*/
                        sql         +=  model.ChequeXFacturaSvc.anularEgreso(fltDistrito, banco, sucursal, cheque, usuario.getLogin() );
                        
                        sql         +=  model.ChequeXFacturaSvc.anularChequeDetalles(fltDistrito, banco, sucursal, cheque, usuario.getLogin() );
                        
                        /*Insertar Registro en anulacion_egreso*/
                        String causa        = "";
                        String observacion  = "";
                        String trecuperacion= "";
                        sql         +=  model.ChequeXFacturaSvc.insertAnulacion_egreso(fltDistrito, banco, sucursal, cheque, usuario.getLogin(), causa, observacion, trecuperacion, usuario.getBase(), "R" );
                        
                        /*Insertar registro en egreso*/
                        String numchk       = model.ChequeXFacturaSvc.obtenerNumeroChqComp( fltDistrito, banco_reemplazo, suc_reemplazo, concepto, usuario.getId_agencia(), usuario.getLogin() );
                        
                        inf.setCheque_reemplazo(numchk);
                        
                        if( numchk.equals("")){
                            lista_errores.add(inf);
                            flag                = false;
                            break;
                        }
                        else{
                            Cheque auxobj    = new Cheque();
                            
                            /*ID serie para actualizar*/
                            Series serie     = model.ChequeXFacturaSvc.obtenerSerieConcepto( fltDistrito, banco_reemplazo, suc_reemplazo, concepto, usuario.getId_agencia(), usuario.getLogin() );                                                        
                            id_serie         = String.valueOf(serie.getId());
                            
                            
                            String numchkaux = model.ChequeXFacturaSvc.obtenerNumeroChq(fltDistrito, banco_reemplazo , suc_reemplazo );
                            auxobj.setUsuario_impresion(usuario.getLogin());
                            auxobj.setLastNumber(Integer.parseInt(numchkaux));
                            auxobj.setDistrito(fltDistrito);
                            auxobj.setBanco(banco_reemplazo);
                            auxobj.setSucursal(suc_reemplazo);
                            
                            sql                += model.ChequeXFacturaSvc.ActualizarSerieConcepto( id_serie, usuario.getLogin() );
                            //sql              += model.ChequeXFacturaSvc.ActualizarSerie(fltDistrito, banco_reemplazo, suc_reemplazo, auxobj  );
                        }//Fin de Validacion
                        
                        
                        Cheque obj       =  model.ChequeXFacturaSvc.getEgreso(fltDistrito, banco, sucursal, cheque );
                        obj.setReimpresion  ("S");
                        obj.setBanco        ( banco_reemplazo );
                        obj.setSucursal     ( suc_reemplazo   );
                        
                        Banco bank       =  model.servicioBanco.obtenerBanco( fltDistrito, banco, sucursal );
                        if( bank != null ){
                            obj.setAgencia( bank.getCodigo_Agencia() );
                        }
                        
                        sql              += model.ChequeXFacturaSvc.insertEgreso(obj, usuario.getLogin(), numchk);
                        
                        /*Insertar un registro en egreso_det*/
                        Cheque objdet   = obj;
                        sql             += model.ChequeXFacturaSvc.insertEgresoDet(objdet, usuario.getLogin(), numchk );
                        
                        //Ejecucion Batch
                        if( flag ){
                            TransaccionService  scv = new TransaccionService(usuario.getBd());
                            scv.crearStatement();
                            scv.getSt().addBatch(sql);
                            scv.execute();
                            model.egresoService.buscarChequesRangoFechas( fltDistrito, fltBanco, fltSucursal, fltRangoini, fltRangofin);
                            lista_insertados.add(inf);//Agrega a Lista de insertado                            
                            //Actualizar serie de cheque**********************
                        }
                    }//Fin del For
                    
                    log.start(  usuario.getLogin(), lista_errores, lista_insertados );
                    Mensaje = "Consulte el log de procesos , para ver el estado del proceso de Reemplazo de cheques";
                }
                
            }
            
            
            String next = "/cheques/reemplazochq.jsp?Mensaje="+Mensaje;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException("Error en  : ReemplazoChequesAction  ....\n" + e.getMessage());
        }
    }
    
    /**
     * Metodo que toma una variable del objeto request
     * en caso de ser nula este en lugar de devolver el
     * valor de la variable devolvera otro valor por defecto
     * @autor ...... Mario Fontalvo
     * @param name . Indica el nombre de la Variable
     * @param otro . Este Indica el valor por defecto que retornara la funcion en caso de ser nulas
     * @return ..... devulve el valor de una variable en caso de ser diferente de null si no devuelve el valor por defecto
     */
    
    public String getParameter(String name, String otro){
        return (request.getParameter(name)!=null?request.getParameter(name):otro);
    }
    
    
}