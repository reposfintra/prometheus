/********************************************************************
 *      Nombre Clase.................   ReporteVehiculosVaradosAction.java
 *      Descripci�n..................   Genera el reporte de oportunidad
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   07.01.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteVehiculosVaradosRefreshAction extends Action{
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteVehiculosVaradosRefreshAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String plaveh = (request.getParameter("plaveh")!=null)? request.getParameter("plaveh") : "";
        String cliente = (request.getParameter("clientes")!=null)? request.getParameter("clientes") : "";  
        String orig = (request.getParameter("origenselec")!=null)? request.getParameter("origenselec") : "";
        //String dest = (request.getParameter("destino")!=null)? request.getParameter("destino") : "";
        String agencia = (request.getParameter("agencias")!=null)? request.getParameter("agencias") : "";
        String fechai = (request.getParameter("FechaI")!=null)? request.getParameter("FechaI") : "";
        String fechaf = (request.getParameter("FechaF")!=null)? request.getParameter("FechaF") : "";
        
        String next = "/jsp/trafico/reportes/ReporteVehiculosDemorados.jsp?&plaveh=" + plaveh;
        
        request.setAttribute("origensel", orig);
        request.setAttribute("codcli", cliente);
        request.setAttribute("codagc", agencia);
        request.setAttribute("fechai", fechai);
        request.setAttribute("fechaf", fechaf);
        request.setAttribute("plaveh", plaveh);
        
        try{            
            model.agenciaService.loadAgencias();
            model.clienteService.setTreeMapClientes();
            model.tramoService.loadOrigenes();
            //model.tramoService.loadDestinos(orig);
            model.viaService.listarDestinos(orig);
            model.zonaService.loadZonas();
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
