/***************
 * Nombre:        IngresoMiscelaneoEventoAction.java
 * Descripci�n:   Clase Action para controlar los eventos del
 *                y las suscursales del banco
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         Created on 24 de junio de 2006, 04:29 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 ****************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

import com.tsp.pdf.*;

public class IngresoMiscelaneoRegistrarAction extends Action {
    
    /** Creates a new instance of IngresoMiscelaneoEventoAction */
    public IngresoMiscelaneoRegistrarAction() {
        
    }
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        String mon_local = "";
        String mon_ingreso = request.getParameter("moneda");
        double valor_con = Double.parseDouble(request.getParameter("valor"));
        String fecha_actual = Util.getFechaActual_String(4);
        String num_ingreso = "";
        boolean sw = true;
        
        String opcion = ( request.getParameter("opcion") != null )?request.getParameter("opcion"):"";
        try{
            
            if( opcion.equals("") ){
                Ingreso ing = new Ingreso();
                ing.setDstrct(usuario.getDstrct());
                ing.setReg_status("");
                ing.setCodcli("");
                ing.setNitcli(request.getParameter("identificacion"));
                ing.setConcepto(request.getParameter("concepto"));
                ing.setTipo_ingreso("M");
                ing.setFecha_consignacion(request.getParameter("fecha"));
                ing.setFecha_ingreso(fecha_actual);
                ing.setBranch_code(request.getParameter("banco"));
                String sucursal = request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"";
                String vec[] = sucursal.split("/");
                if( vec != null && vec.length > 1 )
                    sucursal = vec[0];
                ing.setBank_account_no(sucursal);
                ing.setCodmoneda(mon_ingreso);
                ing.setAgencia_ingreso(usuario.getId_agencia());
                ing.setPeriodo("000000");
                ing.setCant_item(0);
                ing.setBase(usuario.getBase());
                ing.setTransaccion(0);
                ing.setTransaccion_anulacion(0);
                ing.setTipo_documento(request.getParameter("tipodoc"));
                ing.setCreation_user(usuario.getLogin());
                ing.setCreation_date( Util.fechaActualTIMESTAMP());
                ing.setLast_update(Util.fechaActualTIMESTAMP());
                ing.setUser_update(usuario.getLogin());
                ing.setDescripcion_ingreso(request.getParameter("descripcion"));
                ing.setNro_consignacion(request.getParameter("nro_consignacion"));
                ing.setCuenta("");
                ing.setAuxiliar("");
                ing.setAbc("");
                mon_local =  (String) session.getAttribute("Moneda");
                
                Tasa tasa = model.tasaService.buscarValorTasa( mon_local, mon_ingreso, mon_local, request.getParameter("fecha"));
                if(tasa!=null){
                    ing.setVlr_ingreso(tasa.getValor_tasa() * valor_con );
                    ing.setVlr_ingreso_me(valor_con);
                    ing.setVlr_tasa(tasa.getValor_tasa());
                    ing.setFecha_tasa(tasa.getFecha());
                }
                else{
                    sw = false;
                }
                
                if(sw){
                    Banco ban = model.servicioBanco.obtenerBanco( ing.getDstrct(), ing.getBranch_code(), ing.getBank_account_no());
                    //valido cuando es ingreso y la moneda del ingreso sea igual a la moneda del banco
                    if( ban.getMoneda().equals( ing.getCodmoneda() ) ){
                        if(!model.ingresoService.isTempmis()){
                            num_ingreso = model.ingresoService.buscarSerie( request.getParameter("tipodoc")+"M" );
                            if(!num_ingreso.equals("") ){
                                ing.setNum_ingreso(num_ingreso);//buscar de la serie
                                TransaccionService tService = new TransaccionService(usuario.getBd());
                                tService.crearStatement();
                                tService.getSt().addBatch( model.ingresoService.insertarIngreso(ing) );
                                tService.execute();
                                model.ingresoService.setTempmis(true);
                                model.ingresoService.setNro_ingmis(num_ingreso);
                                next+="?msg="+model.ingresoService.getNro_ingmis()+"&msg2="+request.getParameter("nombre");
                            }
                            else{
                                this.dispatchRequest(next+"?msg=serie");
                            }
                        }
                    }
                    else{
                        request.setAttribute("mensaje","La moneda "+ ing.getCodmoneda()+" no pertenece al banco "+ing.getBranch_code()+" "+ing.getBank_account_no());
                        model.ingresoService.setTemporal(false);
                    }
                }
                else{
                    next+="?msg=error";
                }
            }
            
            if( opcion.equals("imp_all") ){
                
                IngresoMPDF remision = new IngresoMPDF();
                
                remision.RemisionPlantilla();
                
                remision.crearCabecera();
                
                String tipo     = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                
                String numing   = ( request.getParameter("numingreso") != null )?request.getParameter("numingreso"):"";
                
                remision.crearRemision( model, usuario.getDstrct() , tipo , numing , "ALL", usuario.getLogin() );
                
                remision.generarPDF();
                
                next = "/pdf/IngresoMPDF.pdf";
                
            }
            
            this.dispatchRequest(next);
        }catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
}
