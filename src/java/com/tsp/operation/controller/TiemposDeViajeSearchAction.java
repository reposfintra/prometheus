/*
 * TiemposDeViajeSearchAction.java
 *
 * Created on 29 de marzo de 2004, 02:36 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;


/** @author  fvillacob*/


public class TiemposDeViajeSearchAction extends Action {
    static Logger logger = Logger.getLogger(TiemposDeViajeSearchAction.class);
    
    public void run() throws  ServletException {
        String next="/jsp/trafico/tiempo_viaje/TiemposDeViaje.jsp";
        String tipoviaje = request.getParameter("op");
        String agency = request.getParameter("agency");
        String fec1= request.getParameter("feci");
        String fec2 = request.getParameter("fecf");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String mensaje = "Su reporte esta generandose, para mas informacion consulte el estado del proceso" +
        " y encuentre el resultado en el Directorio de Archivos.";
        try{
            Vector agencias = new Vector();
            if(agency.equalsIgnoreCase("NADA")){
                model.agenciaService.cargarAgencias();
                Vector agenciasBean = model.agenciaService.getVectorAgencias();
                for(int i =0; i<agenciasBean.size();i++){
                    Agencia a = (Agencia) agenciasBean.elementAt(i);
                    logger.info("Se agrega la agencia "+a.getId_agencia());
                    agencias.add(a.getId_agencia());
                }
            }
            else{
                agencias.add(agency);
            }
            logger.info("Cantidad de agencias encontradas "+agencias.size());
            HReporteTViajesEntrega hilo = new HReporteTViajesEntrega();
            hilo.start(tipoviaje,fec1,fec2,agencias,usuario.getLogin());
            
            next = next + "?mensaje="+mensaje;
        }catch(Exception e){
            throw new ServletException("[TiemposDeViajeSearchAction] "+e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
