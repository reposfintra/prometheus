/*************************************************************************
 * Nombre:        RetroactivoAnular.java                                *
 * Descripci�n:   Clase Action para anular retroactivo                  *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales                *
 * Fecha:         6 de febrero de 2006, 11:12 PM                        *
 * Versi�n        1.0                                                   * 
 * Coyright:      Transportes Sanchez Polo S.A.                         *
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

 
public class RetroactivoAnularAction extends Action {
    
    /** Creates a new instance of RetroactivoAnular */
    public RetroactivoAnularAction() {
    }
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0;
        
        Retroactivo retact = new Retroactivo();
        retact.setFecha1(request.getParameter("fechai"));
        retact.setFecha2(request.getParameter("fechaf"));
        retact.setStd_job_no(request.getParameter("stdjob"));
        retact.setRuta(request.getParameter("ruta"));
        retact.setCreation_user(usuario.getLogin());
        retact.setUser_update(usuario.getLogin());

        try{
            model.retroactivoService.anularRetroactivo(retact);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
