/******************************************************************************
 *      Nombre Clase.................   SerieDocAnuladoManagerAction
 *      Descripci�n..................   Serie de documentos anulados
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   19.03.2007
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import com.tsp.operation.model.threads.*;




import org.apache.log4j.*;

public class SerieDocAnuladoManagerAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    private CXP_Doc facturas = new CXP_Doc();
    /** Creates a new instance of DocumentoInsertAction */
    public SerieDocAnuladoManagerAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String opc = request.getParameter( "opc" );
        //Pr�xima vista
        
        String next  = "";
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        
        try{
            
            request.setAttribute("tdoc", model.tablaGenService.loadTableType("TDOC"));
            request.setAttribute("tcanul", model.tablaGenService.loadTableType("TCAUSANUL"));

            switch( Integer.parseInt(opc) ){
                case 1:     
                    /* Inserci�n */
                    next = "/jsp/cxpagar/seriedoc_anul/insert.jsp";
                    
                    SerieDocAnulado obj = new SerieDocAnulado();
                    obj.loadRequest(request);
                    obj.setDstrct(user.getDstrct());
                    obj.setBase(user.getBase());
                    obj.setCreation_user(user.getLogin());
                    obj.setUser_update(obj.getCreation_user());
                    obj.setReg_status("");
                    
                    model.docAnuladoSvc.setDoc(obj);
                    String reg_status = model.docAnuladoSvc.existe();
                    if( reg_status == null ){
                        model.docAnuladoSvc.insertar();
                        next += "?msg=El registro se ha ingresado exitosamente.";
                    } else if ( reg_status.equals("A") ){
                        model.docAnuladoSvc.actualizar();
                        next += "?msg=El registro se ha ingresado exitosamente.";
                    } else {
                        request.setAttribute("tipo_doc", obj.getTipo_documento());
                        request.setAttribute("nodoc", obj.getNo_documento());
                        request.setAttribute("ref1", obj.getReferencia_1());
                        request.setAttribute("ref2", obj.getReferencia_2());
                        request.setAttribute("ref3", obj.getReferencia_3());
                        request.setAttribute("causa_anul", obj.getCausa_anulacion());
                        request.setAttribute("obs", obj.getObservacion());
                        next += "?msg=El registro ya existe.";
                    }          
                    
                    break;
                case 2:
                    /* Consulta */
                    String fechai = this.validarRequest(request.getParameter("FechaI"));
                    String fechaf = this.validarRequest(request.getParameter("FechaF"));
                    String login = this.validarRequest(request.getParameter("user"));
                    obj = null;
                    obj = new SerieDocAnulado();
                    obj.loadRequest(request);
                    obj.setDstrct(user.getDstrct());
                    
                    model.docAnuladoSvc.setDoc(obj);
                    model.docAnuladoSvc.search(fechai, fechaf, login);
                    logger.info("? B�squeda. Registros encontrados: " + model.docAnuladoSvc.getDocumentos().size());                    
                    next = "/jsp/cxpagar/seriedoc_anul/consulta.jsp";
                    if( model.docAnuladoSvc.getDocumentos()!=null && model.docAnuladoSvc.getDocumentos().size()==0 )
                        next += "?msg. No se encontraron resultados.";
                    break;
                case 3:
                    /* Cargar el objeto seleccionado */
                    obj = null;
                    obj = new SerieDocAnulado();
                    obj.loadRequest(request);
                    model.docAnuladoSvc.setDoc(obj);
                    model.docAnuladoSvc.obtener();
                    request.setAttribute("Obj", model.docAnuladoSvc.getDoc());
                    next = "/jsp/cxpagar/seriedoc_anul/update.jsp";
                    break;
                case 4:
                    /* Modificar */
                    obj = null;
                    obj = new SerieDocAnulado();
                    obj.loadRequest(request);
                    obj.setReg_status("");
                    obj.setUser_update(user.getLogin());
                    model.docAnuladoSvc.setDoc(obj);
                    model.docAnuladoSvc.actualizar();
                    model.docAnuladoSvc.obtener();
                    request.setAttribute("Obj", model.docAnuladoSvc.getDoc());
                    next = "/jsp/cxpagar/seriedoc_anul/update.jsp?msg=Registro actualizado exitosamente.&mod=ok";
                    break;
                case 5:
                    /* Anular */
                    obj = null;
                    obj = new SerieDocAnulado();
                    obj.loadRequest(request);
                    obj.setReg_status("A");
                    obj.setUser_update(user.getLogin());
                    model.docAnuladoSvc.setDoc(obj);
                    model.docAnuladoSvc.actualizar();
                    request.setAttribute("Obj", model.docAnuladoSvc.getDoc());
                    next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
                    break;
                case 6:
                    /* Exportar */
                    SerieDocAnuladosTh hilo = new SerieDocAnuladosTh();
                    hilo.start(model, model.docAnuladoSvc.getDocumentos(), user);
                    next = "/jsp/cxpagar/reportes/ReporteFacturasMsg.jsp?msg=Se ha iniciado la exportaci�n a MS Esxcel. Favor consultar el log de procesos para ver el estado del reporte";
                    break;
            }
            
            
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
    /**
     * Valida los campos enviados a trav�s de un request
     */
    private String validarRequest(String str){
        if( str==null )
            return "";
        else
            return str;
    }
}
