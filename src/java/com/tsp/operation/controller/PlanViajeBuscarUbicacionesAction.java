   /***************************************
    * Nombre Clase ............. PlanViajeBuscarUbicacionesAction.java
    * Descripci�n  .. . . . . .  Busqueda plan de viaje de la planilla
    * Autor  . . . . . . . . . . ANDRES MATURANA
    * Fecha . . . . . . . . . .  23/03/2007
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.*;



public class PlanViajeBuscarUbicacionesAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private static String TIPO_PARQUEADERO = "PAR";
    private static String TIPO_PERNOTADERO = "PER";
    private static String TIPO_TANQUEO     = "GAS";
    private static String TIPO_RESTAURANTE = "RES";
    
    public void run() throws ServletException, InformationException  {
        
        try{
            model.PlanViajeSvc.init();
            String next            = "/jsp/trafico/planviaje/frmPlanViaje.jsp";
            String distrito        = request.getParameter("distrito").trim().toUpperCase();
            String planilla        = request.getParameter("planilla").trim().toUpperCase();
            String opcion          = request.getParameter("rdOpcion");
            PlanDeViaje planViaje  = null;
            PlanDeViaje planAux    = null;
            String comentario      = "";
            String via = request.getParameter("viaSelec");
            
            opcion = opcion.toUpperCase();
            model.PlanViajeSvc.formatBtn(opcion);
              
            String productos      = request.getParameter("productos");
            String fecha          = request.getParameter("fechaPlan");
            String radio          = request.getParameter("radio");
            String avantel        = request.getParameter("avantel");
            String cazador        = request.getParameter("cazador");
            String celular        = request.getParameter("celular");
            String telefono       = request.getParameter("telefono");
            String movil          = request.getParameter("movil");
            String otros          = request.getParameter("otros");
            String nameFamiliar   = request.getParameter("nameFamiliar");
            String telFamiliar    = request.getParameter("telFamiliar");
            String comentario1    = request.getParameter("comentario1");
            String cbRetorno      = request.getParameter("cbRetorno");
            
            logger.info("? planilla: " + planilla);
            logger.info("? distrito: " + distrito);
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            boolean permiso = true;
            
            List listAlimentacion = new LinkedList();
            List listPernotacion  = new LinkedList();
            List listParqueadero  = new LinkedList();
            List listTanqueo      = new LinkedList();
            
            planViaje = model.PlanViajeSvc.search_planViaje(planilla,distrito);
            if( planViaje != null )
                permiso = false;
            String ruta = model.viaService.getRuta(via);
            planAux = model.PlanViajeSvc.formar_planViaje(planilla,distrito);
            
            planAux.setProducto(productos);
            planAux.setFechaPlanViaje(fecha);
            planAux.setRadio(radio);
            planAux.setAvantel(avantel);
            planAux.setCazador(cazador);
            planAux.setCelular(celular);
            planAux.setTelefono(telefono);
            planAux.setMovil(movil);
            planAux.setOtros(otros);
            planAux.setNameFamiliar(nameFamiliar);
            planAux.setTelFamiliar(telFamiliar);
            planAux.setComentario1(comentario1);
            planAux.setRetorno(cbRetorno!=null ? cbRetorno : "N");
            //planAux
            
            if( !via.equals("") ){
                logger.info("? via: " + via + " - ruta: " + ruta);
                planAux.setRuta(via);
                
                listAlimentacion = model.PlanViajeSvc.searchUbicacion( ruta, this.TIPO_RESTAURANTE );
                listPernotacion  = model.PlanViajeSvc.searchUbicacion( ruta, this.TIPO_PERNOTADERO );
                listParqueadero  = model.PlanViajeSvc.searchUbicacion( ruta, this.TIPO_PARQUEADERO );
                listTanqueo      = model.PlanViajeSvc.searchUbicacion( ruta, this.TIPO_TANQUEO     );
                
                planAux.setListAlimentacion( listAlimentacion );
                planAux.setListParqueadero( listParqueadero );
                planAux.setListPernotacion( listPernotacion );
                planAux.setListTanqueo( listTanqueo );
            }
            
            if( planViaje == null ) {                
                model.PlanViajeSvc.formatBtn("SAVE");
            } else {
                if( opcion.equals("SAVE") )
                    model.PlanViajeSvc.formatBtn("SEARCH");
                if (  planAux.getEstado().equals("C")  )
                    model.PlanViajeSvc.formatBtn("DELETE");
            }
            model.PlanViajeSvc.setPlan(planAux);
            request.setAttribute("planViajeObject", planAux );
            
            if( permiso ){
                comentario = "El plan de Viaje para la planilla "+ planilla +" aun No ha sido creado ";
                next+= "?msj="+ comentario;
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);

            
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
    }
    
    
    
}
