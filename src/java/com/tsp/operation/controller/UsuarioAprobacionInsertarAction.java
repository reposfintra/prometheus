/*****************************************************************
 * Nombre:        UsuarioAprobacionInsertarAction.java
 * Descripci�n:   Clase Action para insertar Usuario aprobacion    
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   
 * Fecha:         18 de enero de 2006, 05:11 PM                                
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 ********************************************************************/



package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UsuarioAprobacionInsertarAction extends Action{
    
    /** Creates a new instance of UsuarioAprobacionInsertarAction */
    public UsuarioAprobacionInsertarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/general/usuarioAprobacion/usuarioAprobacion.jsp";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0;
        
        UsuarioAprobacion usuapro = new UsuarioAprobacion();
        usuapro.setId_agencia(request.getParameter("agencia"));
        usuapro.setTabla(request.getParameter("tabla").toUpperCase());
        usuapro.setUsuario_aprobacion(request.getParameter("usuario"));
        usuapro.setUser_update(usuario.getLogin());
        usuapro.setLast_update(now);
        usuapro.setCreation_user(usuario.getLogin());
        usuapro.setCreation_date(now);
        usuapro.setDstrct(usuario.getDstrct());
        usuapro.setBase(usuario.getBase());

        try{
            model.usuaprobacionService.insertarUsuario(usuapro);
            next = next+"?mensaje=Usuario aprobacion ingresado exitosamente!";
        }catch (SQLException e){
            sw=1;
        }
        if(sw==1){
            next = next+"?mensaje=Usuario aprobacion ya existe!";
        }
        this.dispatchRequest(next);
    }
    
}
