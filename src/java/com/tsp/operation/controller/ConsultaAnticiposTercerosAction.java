  /***************************************
    * Nombre Clase ............. ConsultaAnticiposTercerosAction.java
    * Descripci?n  .. . . . . .  Maneja los eventos para la consulta de anticipos
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  23/08/2006
    * versi?n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.controller;





import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Usuario;






public class ConsultaAnticiposTercerosAction extends Action{


    public void run() throws ServletException, InformationException {


         try{
                HttpSession session     = request.getSession();
                Usuario     usuario     = (Usuario)session.getAttribute("Usuario");
                String      user        = usuario.getLogin();
                String      distrito    = usuario.getDstrct();
                String      proveedor   = model.AnticiposPagosTercerosSvc.getProveedorUser(user);


                String      next        = "/jsp/finanzas/fintra/FiltrosConsulta.jsp?x=1";
                String      msj         = "";

                String     evento       =  request.getParameter("evento");

                  NitSot pr= new NitSot();
                  NitDAO nitDAO = new NitDAO();
                  boolean redirect=true;
                if(evento!=null){

                     if( evento.equals("CONSULTAR")){

                            String ckAgencia      =  request.getParameter("ckAgencia");
                            String Agencia        =  request.getParameter("Agencia");
                            String ckPropietario  =  request.getParameter("ckPropietario");
                            String Propietario    =  request.getParameter("Propietario");
                            String ckPlanilla     =  request.getParameter("ckPlanilla");
                            String Planilla       =  request.getParameter("Planilla");
                            String ckPlaca        =  request.getParameter("ckPlaca");
                            String Placa          =  request.getParameter("Placa");
                            String ckConductor    =  request.getParameter("ckConductor");
                            String Conductor      =  request.getParameter("Conductor");
                            String ckReanticipo   =  request.getParameter("ckReanticipo");
                            String reanticipo     =  request.getParameter("reanticipo");
                            String ckFechas       =  request.getParameter("ckFechas");
                            String fechaIni       =  request.getParameter("fechaInicio");
                            String fechaFin       =  request.getParameter("fechaFinal");

                            String ckLiquidacion     =  request.getParameter("ckLiquidacion");
                            String Liquidacion       =  request.getParameter("Liquidacion");
                            String ckTransferencia   =  request.getParameter("ckTransferencia");
                            String Transferencia     =  request.getParameter("Transferencia");
                            String ckFactura         =  request.getParameter("ckFactura");
                            String Factura           =  request.getParameter("Factura");




                              String exp_excel           =  request.getParameter("op_excel")!= null ? request.getParameter("op_excel"):"";


                              if(exp_excel.equals("on"))
                               {
                                   HReporteConsultaAnticipos  hilo =  new HReporteConsultaAnticipos();
                                   hilo.start(model, usuario,"consulta",
                                           distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca,
                                           Placa, ckConductor, Conductor,  ckReanticipo, reanticipo,  ckFechas, fechaIni, fechaFin,
                                           ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura);

                                   next  = "/jsp/finanzas/fintra/FiltrosConsulta.jsp?x=1";
                                   msj   = "Se est? generando su archivo en Excel...";

                               }
                                 else
                               {



                            model.ConsultaAnticiposTercerosSvc.searchAnticipos(distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca, Placa, ckConductor, Conductor,  ckReanticipo, reanticipo,  ckFechas, fechaIni, fechaFin,  ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura );
                            List lista = model.ConsultaAnticiposTercerosSvc.getLista();
                            if(lista.size()>0)
                            {

                             next        = "/jsp/finanzas/fintra/ListaFiltrosConsulta.jsp?x=1";



                            }
                            else
                            {
                               msj = "No se encontraron registros para dichos parametros de busqueda...";
                            }


                            }

                     }


                     if( evento.equals("ACTUALIZAR")){
                            model.ConsultaAnticiposTercerosSvc.refresh();
                            List lista = model.ConsultaAnticiposTercerosSvc.getLista();
                            if(lista.size()>0)
                               next        = "/jsp/finanzas/fintra/ListaFiltrosConsulta.jsp?x=1";
                            else
                               msj = "No se encontraron registros para dichos parametros de busqueda...";
                     }



                      if( evento.equals("EXCEL"))
                      {

                            HReporteConsultaAnticipos  hilo =  new HReporteConsultaAnticipos();
                            hilo.start(model, usuario);


                            next  = "/jsp/finanzas/fintra/ListaFiltrosConsulta.jsp?x=1";
                            msj   = "Se est? generando su archivo en Excel...";
                     }

                     if( evento.equals("PDF")){

                         redirect = false;

                         String nit =  request.getParameter("documento")!=null ? request.getParameter("documento") : "";
                         String fecha =  request.getParameter("fecha")!=null ? request.getParameter("fecha") : "";
                          String enviar_email =  request.getParameter("email")!=null ? request.getParameter("email") : "";

                         String dia = fecha.substring(8,10);
                         String mes = fecha.substring(5, 7);
                         String annio = fecha.substring(0, 4);
                         double vlr=0;

                         /******************************************************************************/
                         List  lista     = model.ConsultaAnticiposTercerosSvc.getLista();
                         lista=model.AnticiposPagosTercerosSvc.getGrupoFecTransferencia(lista, nit, fecha);
                         pr =nitDAO.searchNit(nit);



                      if(model.AnticiposPagosTercerosSvc.exportarReportePdf(usuario.getLogin(), lista,nit,fecha))
                     {
                         msj=Utility.getIcono(request.getContextPath(), 5)+ " PDF Generado Exitosamente.  ";
                        if(enviar_email.equals("S"))
                         {
                              String  directorio = model.AnticiposPagosTercerosSvc.directorioArchivo(usuario.getLogin());
                              String ruta=directorio+"/";
                              String  archivo="ReportePago"+pr.getNombre()+".pdf";

                             if(model.AnticiposPagosTercerosSvc.envia_correo("reportedepago@fintravalores.com",pr.getE_mail(), "", "ReportePago", this.getMensaje(), ruta, archivo))
                             {
                                msj=Utility.getIcono(request.getContextPath(), 6)+"Se Envio  Reporte De Pago Por Correo Electronico Con Exito.  ";
                             }
                             else
                             {
                                 msj=Utility.getIcono(request.getContextPath(), 2)+"Error al momento de Enviar Reporte De Pago Por Correo Electronico ";
                             }

                         }

                     }
                     else
                     {
                      msj=Utility.getIcono(request.getContextPath(), 2)+"Error Generando PDF.  ";
                     }



                     }


                }

                else
                {
               model.AnticiposPagosTercerosSvc.loadAgencias();
             }








        if(redirect==false)
        {
            this.escribirResponse(msj);
        }
        else
        {
            next += "&msj="+ msj;
            this.dispatchRequest(next);
        }



                 }
                 catch (Exception e)
                {
                     throw new ServletException(e.getMessage());
                }







    }









    protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }



        public String getMensaje()
    {
        String mensaje="<span style='font-family: ; font-size:14px; line-height:25px '; >"
                + "Estimado Cliente. </br>Adjunto le estamos enviando el soporte de pago de los negocios desembolsados correspondienten a los clientes aqu&iacute; relacionados.<br />"
                + "Favor no responder este mensaje. Este se genera de manera autom&aacute;tica. En caso de requerir comunicarse con nosotros, favor conta&aacute;ctenos en los tel&eacute;fonos (5) 3679901 Ext, 1131 -1112."
                + "</span>";

        return mensaje;
    }

}
