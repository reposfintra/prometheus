/*
 * ConsultarPlanillaAction.java
 *
 * Created on 3 de enero de 2005, 12:36 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class ConsultarPlanillaAction extends Action{
    
    /** Creates a new instance of ConsultarPlanillaAction  */
    public ConsultarPlanillaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String placa = request.getParameter("placa").toUpperCase();
        String nit = request.getParameter("nit");
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");
        String cedcon = request.getParameter("cedcon");
        String ori = request.getParameter("origen").toUpperCase();
        String des = request.getParameter("destino").toUpperCase();
        String agencia = request.getParameter("agencia");
        String despachador = request.getParameter("despachador").toUpperCase();
        String anulada= "";
        String feccum = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String next="/consultas/consultasRedirect.jsp?planilla=ok&general=ok";
        
        //System.out.println("CORREMOS EL PROCESO DE BUSCAR UNA PLANILLA---");
        
        if(request.getParameter("Anulada")!=null)
            anulada = "A";
        if (request.getParameter("cumplida")!=null)
            feccum = "20";
        try{
            //HENRY 20-01-2006
            if(request.getParameter("general")!=null){
                next="/consultas/consultasRedirect.jsp?planilla=ok&general=ok";
            }
            
            if(request.getParameter("normal")!=null){
                next="/consultas/consultasRedirect.jsp?planilla=ok&normal=ok";
                //next = Util.LLamarVentana (next, "Datos Consulta Viajes");
            }
            String numpla =request.getParameter("numpla")!=null?request.getParameter("numpla").toUpperCase():"";
            
            if(numpla.equals("")){
                //System.out.println("Buscar planilla con los valores:  placa "+placa+" fecha ini: "+ fechaini+" fecha fin: "+ fechafin+" fecha cum "+ feccum+" anulada "+ anulada+ " nit " +nit+" conductor "+cedcon);
                model.planillaService.consultaPlanilla(placa, fechaini, fechafin, feccum, anulada, nit, cedcon,usuario.getBase(), despachador, ori, des, agencia);
            }
            else{
                //System.out.println("El numero de la planilla es "+numpla);
                if(model.planillaService.cantPlanilla(numpla)<=1){
                    //next="/controller?estado=ConsultaOCNormal&accion=Buscar&tipo=1&distrito=TSP&numeroOC="+numpla.toUpperCase()+"&numeroOT=NINGUNA";
                }
            }
           
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
