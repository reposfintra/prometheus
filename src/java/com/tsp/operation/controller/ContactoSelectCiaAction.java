/*
 * ContactoSelectCiaAction.java
 *
 * Created on 23 de agosto de 2005, 12:39 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  INTEL
 */
public class ContactoSelectCiaAction extends Action {
    
    /** Creates a new instance of ContactoSelectCiaAction */
    public ContactoSelectCiaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        try{
            HttpSession session = request.getSession();
            session.setAttribute("frase", request.getParameter("frase"));
            if (request.getParameter("tipo") != null){
                model.identidadService.busquedaxCedula(request.getParameter("frase").toUpperCase());        
                next = "/jsp/trafico/contacto/seleccionarCto.jsp?mostrar=OK";
            }
            else {
                model.identidadService.busquedaxNit(request.getParameter("frase").toUpperCase());        
                next = "/jsp/trafico/contacto/seleccionarCia.jsp?mostrar=OK";
            }
            
            if (model.identidadService.obtVecIdentidad().size() == 0){
                next = "/jsp/trafico/mensaje/ErrorBusq.jsp?ruta=/jsp/trafico/contacto/" + request.getParameter("pagina") + "?mostrar=NO";
            }
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
