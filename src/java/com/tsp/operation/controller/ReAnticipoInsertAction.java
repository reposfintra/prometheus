/*
 * ReAnticipoInsertAction.java
 *
 * Created on 28 de diciembre de 2004, 10:21 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class ReAnticipoInsertAction extends Action {
    //2009-09-02
    static Logger logger = Logger.getLogger(ReAnticipoInsertAction.class);
    /** Creates a new instance of ReAnticipoInsertAction */
    public ReAnticipoInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        request.setAttribute("error","#99cc99");
        String next="/reanticipo/incioReAnticipo.jsp";
        String sj = request.getParameter("standard");
        String moneda = "";
        String planilla= request.getParameter("planilla").toUpperCase();
        String nit ="";
        String placa = "";
        int sw=0;
        float anticipo = Float.parseFloat(request.getParameter("anticipo"));
        float saldo = Float.parseFloat(request.getParameter("saldo"));
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        float max_ant = Float.parseFloat(request.getParameter("antres"));
        String numpla ="";
        String mensaje="Seleccione la planilla a la que se va a hacer el reanticipo";
        String agencia = request.getParameter("agency")!=null?request.getParameter("agency"):usuario.getId_agencia();
        agencia = agencia.equals("NADA")?usuario.getId_agencia():agencia;
        String usuarioA = request.getParameter("usuario")!=null?request.getParameter("usuario"):usuario.getLogin();
        String cadena_banco[]=request.getParameter("banco_usuario")!=null?request.getParameter("banco_usuario").split("/"):request.getParameter("banco").split("/");
        double valor= anticipo;
        String monedaCia="PES";
        double vlrtasa=1;
        try{
            if(valor==0){
                mensaje ="Error El valor del reanticipo debe ser mayor a 0";
                sw=1;
            }
            if(model.planillaService.getPlanilla()!=null){
                model.planillaService.buscarPlanillaAnticipo(planilla);
                if(model.planillaService.getPlanilla()!=null){
                    Planilla pla = model.planillaService.getPlanilla();
                    String a [] = pla.getNitpro().split(" - ");
                    if(a.length>0){
                        nit = a[0];
                    }
                    else
                        nit="";
                    placa = pla.getPlaveh();
                    numpla = pla.getNumpla();
                    valor= anticipo;
                    Banco b = model.servicioBanco.obtenerBanco(usuario.getDstrct(), cadena_banco[0],cadena_banco[1]);
                    if(b!=null){
                        moneda = b.getMoneda();
                        logger.info("Encontre el banco "+moneda);
                    }
                    
                    if(!monedaCia.equals(moneda)){
                        logger.info("Moneda del banco "+moneda);
                        
                        Date hoy = new Date();
                        java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd");
                        String fecha_cambio = s.format(hoy);
                        
                        try{
                            model.tasaService.buscarValorTasa(monedaCia,moneda,monedaCia,fecha_cambio);
                        }catch(Exception et){
                            throw new ServletException(et.getMessage());
                        }
                        Tasa tasa = model.tasaService.obtenerTasa();
                        if(tasa!=null){
                            
                            vlrtasa= tasa.getValor_tasa();
                            logger.info("Tasa "+vlrtasa);
                        }
                        else{
                            sw=1;
                            mensaje ="Error: No se encontro tasa de cambio para la moneda "+moneda+" en la fecha "+fecha_cambio;
                        }
                        
                    }
                    
                    if(valor>max_ant){
                        if(usuarioA.equals("")){
                            sw=1;
                            mensaje ="Error: Debe seleccionar el usuario al que va a imprimir el cheque.";
                        }else{
                            if(!usuario.isPuedeReanticipo()){
                                request.setAttribute("error","#cc0000");
                                sw=1;
                                mensaje ="Error El valor excede el anticipo maximo permitido para esta planilla";
                            }
                            else{
                                if(valor > saldo){
                                    request.setAttribute("error","#cc0000");
                                    sw=1;
                                    mensaje ="Error el reanticipo excede el saldo de la planilla.";
                                }
                            }
                        }
                    }
                    
                }
                
                if(sw==1){
                    next="/reanticipo/incioReAnticipo.jsp?reant="+anticipo;
                    
                }
                else{
                    model.planillaService.buscarPlanillaAnticipo(numpla);
                    Planilla pla = model.planillaService.getPlanilla();
                    
                    
                    String proveedorAnt="";
                    model.ciaService.buscarCia(usuario.getDstrct());
                    if(model.ciaService.getCompania()!=null){
                        proveedorAnt = model.ciaService.getCompania().getnit();
                    }
                    
                    String banco =cadena_banco[0];
                    String cuenta=cadena_banco[1];
                    model.tService.crearStatement();
                    Movpla movpla= new Movpla();
                    String beneficiario="";
                    //BUSCAMOS EL NIT DEL BENEFICIARIO
                    if(request.getParameter("beneficiario")!=null){
                        if(request.getParameter("beneficiario").equals("C")){
                            beneficiario = pla.getCedcon();
                        }
                        else{
                            beneficiario = model.movplaService.buscarBeneficiarioCheque(request.getParameter("beneficiario"),placa);
                        }
                    }
                    
                    if(beneficiario.equals("")){
                        //SE OBTIENE LA AGENCIA ASOCIADA AL ORIGEN DEL STANDARD
                        model.ciudadService.buscarCiudad(pla.getOripla());
                        Ciudad c = model.ciudadService.obtenerCiudad();
                        beneficiario =pla.getCedcon();
                        if(c!=null){
                            String agenciaC = c.getAgAsoc();
                            
                            //SE BUSCA EL PAIS DE LA AGENCIA ASOCIADA
                            model.ciudadService.buscarCiudad(agenciaC);
                            c = model.ciudadService.obtenerCiudad();
                            if(c!=null){
                                String pais = c.getPais()!=null?c.getPais():"";
                                if(pais.equals("VE")){
                                    beneficiario=nit;
                                }
                            }
                        }
                        
                    }
                    float ant2 = anticipo;
                    Vector anticipoProv=model.anticiposService.getAnticiposProv();
                    boolean antProve=false;
                    if(anticipoProv!=null){
                        anticipo = 0;
                        for(int i=0; i<anticipoProv.size(); i++){
                            Anticipos ant = (Anticipos) anticipoProv.elementAt(i);
                            if(ant.getValor()>0){
                                antProve=true;
                                String provee[] = ant.getProveedor().split("/");
                                movpla.setAgency_id(usuario.getId_agencia());
                                movpla.setCurrency(moneda);
                                movpla.setDstrct(usuario.getDstrct());
                                movpla.setPla_owner(nit);
                                movpla.setPlanilla(numpla);
                                movpla.setSupplier(placa);
                                movpla.setVlr((float)com.tsp.util.Util.redondear(ant.getValor()*vlrtasa,0));
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(ant.getAnticipo_code());
                                movpla.setAp_ind("S");
                                movpla.setProveedor(provee.length>0?provee[0]:"");
                                movpla.setCreation_user(usuario.getLogin());
                                movpla.setVlr_for(ant.getValor());
                                movpla.setSucursal(provee.length>1?provee[1]:"");
                                movpla.setBanco(banco);
                                movpla.setCuenta(cuenta);
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
                                movpla.setReanticipo("Y");
                                movpla.setBeneficiario(beneficiario);
//                                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            }
                        }
                        
                    }
                    if(ant2>0 && !antProve){
                        movpla.setAgency_id(agencia);
                        movpla.setCurrency(moneda);
                        movpla.setDstrct(usuario.getDstrct());
                        movpla.setPla_owner(nit);
                        movpla.setPlanilla(planilla);
                        movpla.setSupplier(placa);
                        movpla.setVlr((float)com.tsp.util.Util.redondear(ant2*vlrtasa,0));
                        movpla.setVlr_for(ant2);
                        movpla.setDocument_type("001");
                        movpla.setConcept_code("01");
                        movpla.setAp_ind("S");
                        movpla.setProveedor(proveedorAnt);
                        movpla.setSucursal("");
                        movpla.setCreation_user(usuarioA);
                        movpla.setBanco(banco);
                        movpla.setCuenta(cuenta);
                        movpla.setReanticipo("Y");
                        movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+15));
                        movpla.setBeneficiario(beneficiario);
//                        model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                        
                        
                        
                    }
                    movpla.setCreation_user(usuario.getLogin());
                    model.tService.getSt().addBatch(model.movplaService.insertAutReanticipo());
                    model.tService.execute();
                    mensaje="Se gener� con exito el reanticipo a la planilla "+numpla;
                    
                    model.planillaService.setPlanilla(null);
                }
            }
            
            
            
            next="/reanticipo/incioReAnticipo.jsp?mensaje="+mensaje;
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
