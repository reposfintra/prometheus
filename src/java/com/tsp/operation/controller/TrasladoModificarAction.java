/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class TrasladoModificarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public TrasladoModificarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String banco_Destino = request.getParameter ("bancoDes")!=null?request.getParameter ("bancoDes"):"";
        String banco_Origen = (request.getParameter("bancoOri")!=null)?request.getParameter ("bancoOri"):"";
        String SucBancoOri = (request.getParameter("SucBancoOri")!=null)?request.getParameter ("SucBancoOri"):"";
        String SucBancoDes = (request.getParameter("SucBancoDes")!=null)?request.getParameter ("SucBancoDes"):"";
        String valor = (request.getParameter("valor")!=null)?request.getParameter ("valor"):"";
        String fecha = (request.getParameter("fecha")!=null)?request.getParameter ("fecha"):"0099-01-01";
        String comprobante = (request.getParameter("comprobante")!=null)?request.getParameter ("comprobante"):"";
        Vector datos = new Vector();
          
        String next = "/jsp/finanzas/contab/comprobante/traslados/traslado_modificar.jsp?comprobante="+comprobante;
        //System.out.println("comprobante trasmodificar  "+comprobante);
        HttpSession session = request.getSession ();
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
       
           /********************************************************************/
         try{
              if ( opcion.equals("1") ){
                model.trasladoService.BusquedaTrasladoModificar(banco_Origen, banco_Destino, SucBancoOri, SucBancoDes, valor,fecha);
                datos = model.trasladoService.getVector();
                if ( datos.size () <= 0 ) {    
                     next = next + "&msg=Su busqueda no arrojo resultados!";
                
                }  
           } 
          
           /***************************************************************************/        
             String agencia = "%";
             String target = "sucursales";
            List ListaAgencias = model.ciudadService.ListarAgencias();
            TreeMap tm = new TreeMap();
            if(ListaAgencias.size()>0) {
                Iterator It3 = ListaAgencias.iterator();
                while(It3.hasNext()) {
                    Ciudad  datos2 =  (Ciudad) It3.next();
                    tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
               }
            }
             if( target.length()!=0 && target.compareTo("sucursales")==0 ){
                 model.servicioBanco.loadSucursalesAgenciaDes(agencia, banco_Destino, (String) session.getAttribute("Distrito")); 
                 TreeMap SucBanco = model.servicioBanco.getSucursalDes(); 
                 
                 /**************************************
                  tremap de origen                       */
                 model.servicioBanco.loadSucursalesAgencia(agencia, banco_Origen, (String) session.getAttribute("Distrito"));
                 TreeMap SucBanco1 = model.servicioBanco.getSucursal();
               
             }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
   
               
          
             
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en la busqueda  de TrasladoBuscarModificarAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
