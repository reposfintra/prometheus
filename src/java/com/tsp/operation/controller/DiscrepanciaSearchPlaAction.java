/************************************************************************
 * Nombre clase: DiscrepanciaSearchPlaAction.java
 * Descripci�n: Accion para la busqueda de discrepancias dada una planilla.
 * Autor: Rodrigo Salazar
 * Fecha: 7 de octubre de 2005, 03:46 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  Rodrigo Salazar
 */
public class DiscrepanciaSearchPlaAction extends Action {
    
    /** Creates a new instance of DiscrepanciaSearchPlaAction */
    public DiscrepanciaSearchPlaAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        String planilla = (String) request.getParameter ("c_planilla");
        try {
            next="/jsp/cumplidos/discrepancia/DiscrepanciaListar.jsp";
            model.discrepanciaService.searchDetalleDiscrepancia ("","",planilla,"","","");
            next = Util.LLamarVentana (next, "Listar Discrepancia");
            
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
    
}
