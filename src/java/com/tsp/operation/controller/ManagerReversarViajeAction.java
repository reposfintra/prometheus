/*
 * ManagerReversarViajeAction.java
 *
 * Created on 9 de octubre de 2006, 06:20 PM
 */

/**
 * Nombre        ManagerReversarViajeAction.java
 * Descripci�n   Maneja las acciones de reversion de viajes
 * Autor         Ivan Dario Gomez
 * Fecha         9 de octubre de 2006, 06:20 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 **/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import com.tsp.util.UtilFinanzas;
import java.util.*;
import java.io.*;
/**
 *
 * @author  Ivan Gomez
 */
public class ManagerReversarViajeAction extends Action {
    
    /** Creates a new instance of ManagerReversarViajeAction */
    public ManagerReversarViajeAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next    = "/jsp/trafico/ReversarViaje/ReversarViaje.jsp";
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String accion = request.getParameter( "paso" );
            String numpla = request.getParameter( "numpla" );
            numpla = (numpla!=null)? numpla : "";
            String Mensaje = "";            
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            request.setAttribute( "mostrarInfo", "ok" );
            if( accion.equalsIgnoreCase( "1" ) ){
                model.planillaService.getInfoPlanilla( numpla );
                Planilla pla = model.planillaService.getPlanilla();
                if( pla != null ){
                    pla.setRuta_pla( model.planillaService.getRutas( numpla ) );                    
                }else{
                    request.setAttribute( "mensaje", "No existe el n�mero de la planilla." );
                }
                
            }else if( accion.equalsIgnoreCase( "2" ) ){                
                if( model.ReversarViajeTraficoSvc.existePlanillaIngresoTrafico( numpla ) ){
                    request.setAttribute( "mensaje", "No se puede realizar la reversi�n de la entrega, la planilla existe en ingreso trafico." );
                }else if( !model.ReversarViajeTraficoSvc.existePlanillaTrafico( numpla ) ){
                    request.setAttribute( "mensaje", "No se puede realizar la reversi�n de la entrega, la planilla no se encuentra en tr�fico." );
                }else if( !model.ReversarViajeTraficoSvc.esPlanillaFinalizada( numpla ) ){
                    request.setAttribute( "mensaje", "No se puede realizar la reversi�n de la entrega, la planilla en reporte movimiento tr�fico no esta finalizada." );
                }else{
                    
                    model.ReversarViajeTraficoSvc.obtenerReporteTrafico( numpla );                    
                    model.ReversarViajeTraficoSvc.insertarCopiaIngresoTrafico();                     
                    request.setAttribute( "mostrarInfo", null );
                    request.setAttribute( "mensaje", "Reversi�n realizada exitosamente." );
                    
                }
            }
            
        }catch( Exception ex ){
            throw new ServletException ( "Error en ManagerReversarViajeAction ...\n" + ex.getMessage() );
        }
        this.dispatchRequest( next );
    }
    
}