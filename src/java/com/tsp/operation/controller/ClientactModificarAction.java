/*************************************************************************
 * Nombre                       ClientactModificarAction.java            *
 * Descripci�n                  Clase Action para modificar              *
 * Autor.                       Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha                        30 de agosto de 2005, 08:07 AM           * 
 * Versi�n                      1.0                                      * 
 * Coyright                     Transportes Sanchez Polo S.A.            *
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes
 */
public class ClientactModificarAction extends Action {
    
    /** Creates a new instance of ClientactModificarAction */
    public ClientactModificarAction() {
    }
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/clienteactividad/clienteactividadMod.jsp";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0,a=0;
        int sec =Integer.parseInt(request.getParameter("sec"));
        ClienteActividad clientact = new ClienteActividad();
        clientact.setEstado("");
        clientact.setCodActividad(request.getParameter("actividad"));
        clientact.setCodCliente(request.getParameter("cliente"));
        clientact.setTipoViaje(request.getParameter("tipo").toUpperCase());
        clientact.setSecuencia(Integer.parseInt(request.getParameter("secuencia")));
        clientact.setCreation_user(usuario.getLogin());
        clientact.setUser_update(usuario.getLogin());
        clientact.setBase(usuario.getBase());
        clientact.setDstrct(usuario.getCia());
        clientact.setLast_update(now);
        clientact.setCreation_date(now);
        try{
            if( sec != Integer.parseInt(request.getParameter("secuencia")) ){
                if (!model.clientactSvc.existeSecuencia(usuario.getCia(),request.getParameter("cliente"),Integer.parseInt(request.getParameter("secuencia")), request.getParameter("tipo").toUpperCase() ) ){
                    model.clientactSvc.modificarClientAct(clientact);
                    next+="?mensaje=Modificado&reload=OK";
                }
                else{
                    next+="?mensaje=Existe";
                }
            }
            else{
                model.clientactSvc.modificarClientAct(clientact);
                next+="?mensaje=Modificado&reload=OK";
            }
            model.clientactSvc.buscarClientAct(clientact.getCodCliente(), clientact.getCodActividad(), clientact.getTipoViaje(), clientact.getDstrct());
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
