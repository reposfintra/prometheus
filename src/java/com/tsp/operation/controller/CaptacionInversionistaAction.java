package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.exceptions.InformationException;
import com.tsp.finanzas.contab.model.beans.ComprobanteFacturas;
import com.tsp.operation.model.NitDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Inversionista;
import com.tsp.operation.model.beans.MovimientosCaptaciones;
import com.tsp.operation.model.beans.NitSot;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Ingreso;
import com.tsp.operation.model.beans.Banco;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.CXPItemDoc;
import com.tsp.operation.model.beans.CXP_Doc;

import com.tsp.operation.model.beans.Ingreso_detalle;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.operation.model.beans.Sms;
import com.tsp.operation.model.beans.TablaGen;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.operation.model.services.CaptacionInversionistaService;
import com.tsp.operation.model.services.ChequeXFacturaService;
import com.tsp.operation.model.services.Smservice;
import com.tsp.operation.model.services.Tipo_impuestoService;
import com.tsp.operation.model.threads.HArchivosTransferenciasCaptaciones;
import java.io.IOException;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import com.tsp.util.Util;
import com.tsp.util.UtilFinanzas;
import com.tsp.util.Utility;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Vector;
import org.jfree.chart.JFreeChart;

/**
 * Action para el nuevo programa de captaciones
 * 23/02/2012
 * @author darrieta
 */
public class CaptacionInversionistaAction extends Action {

    HttpSession session;
    Usuario usuario;
    String next = "";
    /**
     * Opcion para obtener los ultimos movimeintos
     */
    private final int OBTENER_ULTIMOS_MOVIMIENTOS = 6;
    /**
     * Opcion para guardar un movimeinto
     */
    private final int GUARDAR_MOVIMIENTO = 7;
    /**
     * Opcion para obtener los ultimos movimeintos
     */
    private final int RETIROS_X_CONFIRMAR = 8;
    /**
     * Opcion para obtener los ultimos movimeintos
     */
    private final int CONFIRMAR_RETIROS = 9;
    /**
     * Opcion para obtener los ultimos movimeintos
     */
    private final int RETIROS_X_APROBAR = 10;
    /**
     * APROBAR RETIROS
     */
    private final int APROBAR_RETIROS = 11;
    /**
     * APROBAR RETIROS
     */
    private final int CONSULTAR_CUENTAS_TRANSFERENCIAS = 12;
    /**
     * Opcion para obtener los retiros por transferir
     */
    private final int RETIROS_X_TRANSFERIR = 13;
    /**
     * Opcion para obtener los retiros por transferir
     */
    private final int TRANSFERIR_RETIROS = 14;
    private final int CONSULTAR_SALDOS = 15;
    private final int CONSULTAR_INVERSIONISTAS = 16;
    /**
     * Opcion para obtener los movimientos de un mes especifico
     */
    private final int CONSULTAR_MOVIMIENTOS_MES = 17;
    private final int EXPORTAR_PDF_MES = 18;
    private final int MOVIMIENTOS_X_CAUSACION = 19;
    private final int CALCULAR_INTERES_CAUSACION = 20;
    private final int CAUSAR_INTERESES = 21;
    private final int CONSULTAR_INFORMACION_CIERRE_SOCIOS = 22;
    private final int GENERAR_GRAFICO_CIERRE = 23;
    private final int INFORME_CIERRE_PDF = 24;
    private final int ELIMINAR_CUENTAS_TRANSFERENCIA = 25;
    private final int CONFIRMAR_AUTORIZAR_PAGOS = 26;
    private final int AUTORIZAR_PAGOS = 27;
    private final int REPORTE_INVERSIONISTAS = 28;
    private final int  EXPORTAR_INFORME_CONSOLIDADO = 29;
    private final int CONSULTAR_INFORMACION_CIERRE_PARTICULAR = 30;
    private final int CONSULTAR_INFORMACION_DETALLE_INVERSIONISTA = 31;
    private final int CONSULTAR_TOTAL_INVERSIONISTA_MES = 32;
    private final int INSERTAR_GRAFICO = 33;
  



    @Override
    public void run() throws ServletException, InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String opcion = request.getParameter("opcion") == null ? "" : request.getParameter("opcion");
            boolean redirect = true;
            String strRespuesta = "";
            next = "/jsp/inversionistas/crearInversionista.jsp";

            if (opcion.equals("CONSULTAR_INVERSIONISTA")) {
                consultarCedula();
                redirect = false;
            } else if (opcion.equals("GUARDAR_INVERSIONISTA")) {
                guardarInversionista();
                redirect = true;
            } else if (opcion.equals("CARGAR_SUBCUENTAS")) {
                cargarSubcuentas();
                redirect = false;

            } else if (opcion.equals("GUARDAR_DATOS_LIQUIDACION")) {
                guardarDatosLiquidacion();
            } else if (opcion.equals("CARGAR_SUBCUENTA_INVERSIONISTA")) {
                cargarDatosSubcuenta();
                redirect = false;
            }

            int op = (request.getParameter("op") != null) ? Integer.parseInt(request.getParameter("op").toString()) : -1;

            switch (op) {
                case OBTENER_ULTIMOS_MOVIMIENTOS:
                    this.consultarUltimosMovimientos();
                    redirect = false;
                    break;
                case GUARDAR_MOVIMIENTO:
                    this.guardarMovimiento();
                    redirect = false;
                    break;

                case RETIROS_X_CONFIRMAR:
                    consultarRetirosXconfirmar();
                    redirect = false;
                    break;

                case CONFIRMAR_RETIROS:
                    this.confirmarRetiros();
                    redirect = false;
                    break;
                case RETIROS_X_APROBAR:
                    consultarRetirosXaprobar();
                    redirect = false;
                    break;

                case APROBAR_RETIROS:
                    this.aprobarRetiros();
                    redirect = false;
                    break;
                case CONSULTAR_CUENTAS_TRANSFERENCIAS:
                    this.consultarCuentasTransferencias();
                    redirect = false;
                    break;

                case RETIROS_X_TRANSFERIR:
                    consultarRetirosXtransferir();
                    redirect = false;
                    break;

                case TRANSFERIR_RETIROS:
                    this.transferirRetiros();
                    redirect = false;
                    break;

                case CONSULTAR_SALDOS:
                    this.consultarSaldos();
                    redirect = false;
                    break;

                case CONSULTAR_INVERSIONISTAS:
                    this.consultarInversionistas();
                    redirect = false;
                    break;

                case CONSULTAR_MOVIMIENTOS_MES:
                    this.consultarMovimientosMes();
                    redirect = false;
                    break;

                case EXPORTAR_PDF_MES:
                    this.exportar();
                    redirect = false;
                    break;

                case MOVIMIENTOS_X_CAUSACION:
                    this.consultarMovimeintosXCausar();
                    redirect = false;
                    break;

                case CALCULAR_INTERES_CAUSACION:
                    this.calcularInteresCausacion();
                    redirect = false;
                    break;

                case CAUSAR_INTERESES:
                    this.CausarIntereses();
                    redirect = false;
                    break;

                case CONSULTAR_INFORMACION_CIERRE_SOCIOS:
                    this.consultariInformacionCierreSocios();
                    redirect = false;
                    break;

                case GENERAR_GRAFICO_CIERRE:
                    this.generarGraficoCierre();
                    redirect = false;
                    break;

                case INFORME_CIERRE_PDF:
                    this.exportarInformeCierre();
                    redirect = false;
                    break;

                case ELIMINAR_CUENTAS_TRANSFERENCIA:
                    this.eliminarCuentasTransferencias();
                    redirect = false;
                    break;

                case CONFIRMAR_AUTORIZAR_PAGOS:
                    this.confirmarModificacionPagos();
                    redirect = false;
                    break;


                case AUTORIZAR_PAGOS:
                    this.actualizarDatosPagosAutomaticos();
                    redirect = false;
                    break;

                case REPORTE_INVERSIONISTAS:
                    this.consultarReportesInversionistas();
                    redirect = false;
                    break;

               case EXPORTAR_INFORME_CONSOLIDADO:
                    this.exportarInformeConsolidado();
                    redirect = false;
                    break;
                   
               case CONSULTAR_INFORMACION_CIERRE_PARTICULAR:
                    this.consultariInformacionCierreParticular();
                    redirect = false;
                    break;
                   
               case CONSULTAR_INFORMACION_DETALLE_INVERSIONISTA:
                    this.consultariInformacionDetalleInversionista();
                    redirect = false;
                    break;   
                   
              case CONSULTAR_TOTAL_INVERSIONISTA_MES:
                    this.consultarTotalInversionistaMes();
                    redirect = false;
                    break;  
                  
               case INSERTAR_GRAFICO:
                    this.insertarGrafico();
                    redirect = false;
                    break;         
                   
               
            }
            if (redirect) {
                this.dispatchRequest(next);
            }
            /*  else {
            response.setContentType("text/plain");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.getWriter().print(strRespuesta);
            }*/

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error en CaptacionInversionistaAction: " + e.getMessage());
        }
    }

    public void consultarCedula() throws Exception {
        String respuesta = "ok";
        String nit = request.getParameter("inversionista");
        model.nitService.searchNit(nit);
        if (model.nitService.getNit() != null) {
            if (!model.nitService.getNit().getEstado().equals("I")) {
                Proveedor prov = model.proveedorService.obtenerProveedorPorNit(nit);
                if (prov != null) {
                    if (!model.clienteService.existeClienteXNit(nit)) {
                        respuesta = "El inversionista no se encuentra registrado como cliente";
                    }
                } else {
                    respuesta = "El inversionista no se encuentra registrado como proveedor";
                }
            } else {
                respuesta = "La cedula no esta activa";
            }
        } else {
            respuesta = "La cedula no existe";
        }
       // this.printlnResponse(respuesta, "text/plain");
         response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-store");
                response.setDateHeader("Expires", 0);
                response.getWriter().print(respuesta);
    }

    /**
     * guarda la informacion del inversionista y las subcuentas
     * @throws Exception
     */
    public void guardarInversionista() throws Exception {
        try {
            CaptacionInversionistaService svc = new CaptacionInversionistaService(usuario.getBd());
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            //actualizar el nit
            NitSot nit = new NitSot();
            nit.setCedula(request.getParameter("cedula"));
            nit.setE_mail2(request.getParameter("e_mail2"));
            nit.setDireccion_oficina(request.getParameter("direccion_oficina"));
            tService.getSt().addBatch(svc.updateNit(nit));

            if (request.getParameter("tipo").equals("NUEVO")) {
                Inversionista inversionista = new Inversionista();
                inversionista.setNit(request.getParameter("cedula"));
                inversionista.setSubcuenta(0);
                inversionista.setNombre_subcuenta("");
                inversionista.setCreation_user(usuario.getLogin());
                inversionista.setDstrct(usuario.getDstrct());

                 inversionista.setNit_parentesco(request.getParameter("nit_parentesco"));
                 inversionista.setTipo_parentesco(request.getParameter("tipo_parentesco"));
                 inversionista.setTipo_inversionista(request.getParameter("tipo_inversionista"));
                tService.getSt().addBatch(svc.insertarInversionista(inversionista));
            }
            int cont = 0;
            if (request.getParameter("tipo").equals("EDITAR")) {
                cont = svc.consultarMaxSubcuenta(request.getParameter("cedula"), usuario.getDstrct());
            }
            int max = Integer.parseInt(request.getParameter("max"));
            for (int i = 0; i <= max; i++) {
                if (request.getParameter("nombre_subcuenta" + i) != null) {
                    Inversionista subcuenta = new Inversionista();
                    subcuenta.setNit_parentesco(request.getParameter("nit_parentesco"));
                    subcuenta.setTipo_parentesco(request.getParameter("tipo_parentesco"));
                    subcuenta.setTipo_inversionista(request.getParameter("tipo_inversionista"));
                    subcuenta.setNit(request.getParameter("cedula"));
                    subcuenta.setNombre_subcuenta(request.getParameter("nombre_subcuenta" + i));
                    subcuenta.setDstrct(usuario.getDstrct());
                    if (request.getParameter("subcuenta" + i) == null) {
                        subcuenta.setSubcuenta(++cont);
                        subcuenta.setCreation_user(usuario.getLogin());
                        tService.getSt().addBatch(svc.insertarInversionista(subcuenta));
                    } else {
                        //editar
                        subcuenta.setUser_update(usuario.getLogin());
                        subcuenta.setSubcuenta(Integer.parseInt(request.getParameter("subcuenta" + i)));
                        String[] split = svc.editarInversionista(subcuenta).split(";");
                        for (int j = 0; j < split.length; j++) {
                            tService.getSt().addBatch(split[j]);       
                        }                      
                    }
                }
            }
            tService.execute();
            next += "?inversionista=&mensaje=Se ha guardado el inversionista";
        } catch (Exception exception) {
            throw exception;
        }
    }

    public void cargarSubcuentas() throws SQLException, Exception {
        CaptacionInversionistaService svc = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<Inversionista> subcuentas = svc.consultarSubcuentas(request.getParameter("inversionista"), usuario.getDstrct());
        String options = "<option value=''>Seleccione</option>";

        for (int i = 0; i < subcuentas.size(); i++) {
            options += "<option value='" + subcuentas.get(i).getSubcuenta() + "'>" + subcuentas.get(i).getNombre_subcuenta() + "</option>";
        }

        this.printlnResponse(options, "text/plain");

    }

    public void guardarDatosLiquidacion() throws Exception {
        try {
            CaptacionInversionistaService svc = new CaptacionInversionistaService(usuario.getBd());
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            Inversionista subcuenta = new Inversionista();
            subcuenta.setNit(request.getParameter("inversionista"));
            subcuenta.setSubcuenta(Integer.parseInt(request.getParameter("subcuenta")));
            subcuenta.setUser_update(usuario.getLogin());
            subcuenta.setDstrct(usuario.getDstrct());
            subcuenta.setTasa(Double.parseDouble(request.getParameter("tasa")));
            subcuenta.setAno_base(Integer.parseInt(request.getParameter("ano_base")));
            subcuenta.setRendimiento(request.getParameter("rendimiento").equals("0") ? request.getParameter("rendimiento_dias") : request.getParameter("rendimiento"));
            subcuenta.setTipo_interes(request.getParameter("intereses"));
            subcuenta.setRetefuente(request.getParameter("rfte"));
            subcuenta.setReteica(request.getParameter("rica"));
            subcuenta.setTasa_nominal(Double.parseDouble(request.getParameter("tasa_nominal")));
            subcuenta.setTasa_diaria(Double.parseDouble(request.getParameter("tasa_diaria")));
            subcuenta.setAno_base_tasa_diaria(Integer.parseInt(request.getParameter("ano_base_td")));
            subcuenta.setTasa_ea(Double.parseDouble(request.getParameter("tasa_ea")));
            subcuenta.setDias_calendario(Integer.parseInt(request.getParameter("dias_calendario")));
            subcuenta.setPago_automatico(request.getParameter("pago_automatico"));
            subcuenta.setPeriodicidad_pago(request.getParameter("periodicidad_pago"));
            subcuenta.setGenera_documentos(request.getParameter("genera_documentos")!=null ? "S":"N");
            tService.getSt().addBatch(svc.guardarDatosLiquidacion(subcuenta));
            tService.execute();
            next = "/jsp/inversionistas/crearLiquidadorCaptaciones.jsp?mensaje=Se han guardado los cambios";
        } catch (Exception exception) {
            throw exception;
        }
    }

    private void cargarDatosSubcuenta() throws Exception {
        String xml = "";
        CaptacionInversionistaService svc = new CaptacionInversionistaService(usuario.getBd());
        Inversionista subcuenta = new Inversionista();


        subcuenta = svc.consultarSubcuentaInversionista("FINV", request.getParameter("inversionista"), Integer.parseInt(request.getParameter("subcuenta")));

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<datos>";
        xml += "<tasa>" + subcuenta.getTasa() + "</tasa>";
        xml += "<retefuente>" + subcuenta.getRetefuente() + "</retefuente>";
        xml += "<retefuente>" + subcuenta.getRetefuente() + "</retefuente>";
        xml += "<reteica>" + subcuenta.getReteica() + "</reteica>";
        xml += "<tasa_ea>" + subcuenta.getTasa_ea() + "</tasa_ea>";
        xml += "<tasa_nominal>" + subcuenta.getTasa_nominal() + "</tasa_nominal>";
        xml += "<tasa_diaria>" + subcuenta.getTasa_diaria() + "</tasa_diaria>";
        xml += "<ano_base>" + subcuenta.getAno_base() + "</ano_base>";
        xml += "<ano_base_td>" + subcuenta.getAno_base_tasa_diaria() + "</ano_base_td>";
        xml += "<rendimiento>" + subcuenta.getRendimiento() + "</rendimiento>";
        xml += "<tipo_interes>" + subcuenta.getTipo_interes() + "</tipo_interes>";
        xml += "<dias_calendario>" + subcuenta.getDias_calendario() + "</dias_calendario>";
        xml += "<pago_automatico>" + subcuenta.getPago_automatico() + "</pago_automatico>";
        xml += "<periodicidad_pago>" + subcuenta.getPeriodicidad_pago() + "</periodicidad_pago>";
        xml += "<genera_documentos>" + (subcuenta.getGenera_documentos().equals("S")? true:false) + "</genera_documentos>";
        xml += "</datos>";
        this.printlnResponse(xml, "text/xml");

    }

    public void consultarUltimosMovimientos() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String subcuenta = request.getParameter("subcuenta");
        String nit = request.getParameter("inversionista");
        ArrayList<MovimientosCaptaciones> movimientos = cis.consultarUltimosMovimientos(usuario.getDstrct(), nit, subcuenta, "S");// consultar ultimo movimientos del mes

        Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), nit, Integer.parseInt(subcuenta));// consultar ultimo movimientos del mes
        if (movimientos.size() == 0) {
            movimientos = cis.consultarUltimosMovimientos(usuario.getDstrct(), nit, subcuenta, "N");// consultar ultimo movimiento
        }

        if (movimientos.size() == 0) {
            MovimientosCaptaciones movimiento = new MovimientosCaptaciones();
            movimiento.setFecha(Util.getFechahoy());
            movimientos.add(movimiento);
        } else {
            //tienen movimientos
            //se crea el movimiento hasta la fecha de hoy

            ///calcular
            movimientos.add(cis.calculosMovimientoFinal(inversionista, movimientos));
        }
        session.setAttribute("lista_movimientos", movimientos);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movimientos) + "}";
        this.printlnResponse(json, "application/json;");

    }

    public void guardarMovimiento() throws Exception {
        String resp = " ";
        try {

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            double valor = Double.parseDouble(request.getParameter("valor").replace(",", ""));
            String fecha = request.getParameter("fecha") == null ? Util.getFechahoy() : request.getParameter("fecha");
            String banco = request.getParameter("banco") == null ? "" : request.getParameter("banco");
            String sucursal = request.getParameter("sucursal") == null ? "" : request.getParameter("sucursal");
            String nit = request.getParameter("inversionista") == null ? "" : request.getParameter("inversionista");
            int codsubcuenta = Integer.parseInt(request.getParameter("subcuenta"));
            String tipo_movimiento = request.getParameter("tipo_movimiento") == null ? "" : request.getParameter("tipo_movimiento");
            String tipo_transferencia = request.getParameter("tipo_transferencia") == null ? "" : request.getParameter("tipo_transferencia");

            String guardar_datos_transferencia = request.getParameter("sw_datos_transferencia") == null ? "" : request.getParameter("sw_datos_transferencia");

            //datos trasnferencia electronica
            String cuenta = request.getParameter("cuenta") == null ? "" : request.getParameter("cuenta");
            String titular_cuenta = request.getParameter("titular_cuenta") == null ? "" : request.getParameter("titular_cuenta");
            String nit_cuenta = request.getParameter("nit_cuenta") == null ? "" : request.getParameter("nit_cuenta");
            String tipo_cuenta = request.getParameter("tipo_cuenta") == null ? "" : request.getParameter("tipo_cuenta");
            String concepto_transaccion = request.getParameter("concepto_transaccion") == null ? "" : request.getParameter("concepto_transaccion");

            //datos trasferencia cheque
            String nombre_beneficiario = request.getParameter("nombre_beneficiario") == null ? "" : request.getParameter("nombre_beneficiario");
            String nit_beneficiario = request.getParameter("nit_beneficiario") == null ? "" : request.getParameter("nit_beneficiario");
            String cheque_primer_beneficiario = request.getParameter("cheque_primer_beneficiario") == null ? "" : request.getParameter("cheque_primer_beneficiario");
            String cheque_cruzado = request.getParameter("cheque_cruzado") == null ? "" : request.getParameter("cheque_cruzado");
            //datos trasferencia interna
            String nombre_beneficiario_captacion = request.getParameter("nombre_beneficiario_capt") == null ? "" : request.getParameter("nombre_beneficiario_capt");

            CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
            ArrayList<MovimientosCaptaciones> movimientos = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_movimientos");
            MovimientosCaptaciones movimiento_final = movimientos.get(movimientos.size() - 1);

            Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), nit, codsubcuenta);
            /// setiar movimiento
            movimiento_final.setNit_cuenta(nit_cuenta);
            movimiento_final.setTitular_cuenta(titular_cuenta);
            movimiento_final.setTipo_cuenta(tipo_cuenta);
            movimiento_final.setCuenta(cuenta);
            movimiento_final.setNombre_beneficiario(nombre_beneficiario);
            movimiento_final.setNit_beneficiario(nit_beneficiario);
            movimiento_final.setCheque_cruzado(cheque_cruzado);
            movimiento_final.setCheque_primer_beneficiario(cheque_primer_beneficiario);
            movimiento_final.setNombre_beneficiario_captacion(nombre_beneficiario_captacion);
            movimiento_final.setFecha(fecha);
            movimiento_final.setTasa_ea(inversionista.getTasa_ea());
            movimiento_final.setSubcuenta(inversionista.getSubcuenta());
            movimiento_final.setTipo_movimiento(tipo_movimiento);
            movimiento_final.setNit(inversionista.getNit());
            movimiento_final.setDstrct(usuario.getDstrct());
            movimiento_final.setBanco(banco);
            movimiento_final.setSucursal(sucursal);
            movimiento_final.setCreation_user(usuario.getLogin());
            movimiento_final.setTipo_transferencia(tipo_transferencia);
            movimiento_final.setConcepto_transaccion(concepto_transaccion);

            if (movimiento_final.getTipo_movimiento().equals("I")) {
                movimiento_final.setConsignacion(valor);
            } else {// si es retiro
                movimiento_final.setRetiro(valor);
                movimiento_final.setEstado("P");
                // opcion para guardar datos transferencia
                if(!movimiento_final.getTipo_transferencia().equals("I"))
                {
                if(guardar_datos_transferencia.equals("true") && cis.ExisteDatosTrans(movimiento_final))
                 tService.getSt().addBatch(cis.guardarDatosTransferencia(movimiento_final));
                }
            }


            if (inversionista.getTipo_interes().equals("C")) {//calcular saldo final
                movimiento_final.setSubtotal(movimiento_final.getSaldo_inicial() + movimiento_final.getValor_intereses() - movimiento_final.getValor_retefuente() - movimiento_final.getValor_reteica());
                movimiento_final.setSaldo_final(movimiento_final.getSubtotal() + movimiento_final.getConsignacion());//no se resta el retiro se restan cuando se genera el pago
            } else {
                if (inversionista.getTipo_interes().equals("S")) {
                    movimiento_final.setSaldo_final(movimiento_final.getConsignacion());
                }
            }

            tService.getSt().addBatch(cis.guardarMovimiento(movimiento_final));


            if (movimiento_final.getTipo_movimiento().equals("I") && (inversionista.getGenera_documentos().equals("S"))) {
                /// se crea el ingreso
               ArrayList<String> listSQL = crearIngreso(inversionista, movimiento_final);
               for (String sql : listSQL) {
                tService.getSt().addBatch(sql);
               }
            }
           tService.execute();


            if (movimientos.size() > 1)//validar que tiene mas de un movimiento
            {
                ///reliquidar
                movimientos = cis.getMovimientosRecalcular(movimiento_final);//se obtienen los movimientos a recalcular
                if (movimientos.size() > 1)//validar que tiene movimientos posteriores a este para recalcular(inicialmente la lista carga 1 movimiento anterior y el movimiento actual)
                {
                    movimientos = cis.RecalcularMovimientos(inversionista, movimientos);
                    cis.updateMovimientos(movimientos);
                }
            }

            //if es retiro enviar correo
            if(movimiento_final.getTipo_movimiento().equals("RP") || movimiento_final.getTipo_movimiento().equals("RT"))
            {
             NitDAO nitDAO = new NitDAO(usuario.getBd());
             NitSot objnit = new NitSot();
             objnit =nitDAO.searchNit(nit);
             Smservice smsservice = new Smservice(usuario.getBd());
             Sms  sms = new Sms();
             sms.setCell(objnit.getTelefono());
             sms.setSMS("Fintra ha recibido su solicitud de retiro de inversion por valor de $" + Util.customFormat(movimiento_final.getRetiro()) + ", en breve estaremos contactandolo para su confirmacion. www.fintra.co");
             
           // cis.enviarCorreo("comercial@fintra.co","jpinedo@fintra.co;apabon@fintra.co", "", "SOLICITUD RETIRO INVERSIONISTA", this.getMensajeInterno(objnit,inversionista,movimiento_final,"S"), "", "");
           // cis.enviarCorreo("comercial@fintra.co","jpinedo@fintra.co;<apabon@fintra.co>", "", "SOLICITUD RETIRO INVERSIONISTA", this.getMensajeInversionista(objnit,inversionista,movimiento_final,"S"), "", "");
            smsservice.envia_SMS_HTTP(sms);
            }
            resp = Utility.getIcono(request.getContextPath(), 5) + "Movimiento registrado con exito.";
            session.removeAttribute("lista_movimientos");

        } catch (Exception exception) {
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }

    public void confirmarRetiros() throws Exception {
        String resp = " ";
        try {

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
            ArrayList<MovimientosCaptaciones> movimientos = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_retiros_x_confirmar");
            String retirostem = request.getParameter("retiros");
            String[] trasnsacciones = retirostem.split(",");
            movimientos = cis.getRetirosChequeados(movimientos, trasnsacciones);
            int numItem = 1;
            model.tablaGenService.buscarDatos("CAP_CUENTA", "INGRESO_CAPTACIONES");
            TablaGen tCuenta = model.tablaGenService.getTblgen();
            for (int i = 0; i < movimientos.size(); i++) {

                MovimientosCaptaciones retiro = movimientos.get(i);
                Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), retiro.getNit(), retiro.getSubcuenta());
                retiro.setEstado("C");

                if(inversionista.getGenera_documentos().equals("S"))
                {
                    //se crea la cxp
                    Vector vItems = new Vector();
                    CXP_Doc factura = new CXP_Doc();
                    factura = this.crearCxp(retiro);
                    CXPItemDoc item = crearDetalleCXP(retiro.getNit(), "FAP", retiro.getNo_transaccion(), factura.getDescripcion(), retiro.getRetiro(), tCuenta.getReferencia(), numItem++);
                    vItems.add(item);
                    ArrayList<String> listQuery = model.cxpDocService.insertarCXPDoc_SQL(factura, vItems, new Vector(), usuario.getId_agencia(), "");
                    for (String sql : listQuery) {
                        tService.getSt().addBatch(sql);
                    }
                }
                 
                tService.getSt().addBatch(cis.confirmarRetiro(retiro));
            }
            tService.execute();
            resp = Utility.getIcono(request.getContextPath(), 5) + "Se Confirmaron " + movimientos.size() + " retiros con exito.";
            session.removeAttribute("lista_retiros_x_confirmar");
        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error.";
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }



    public void actualizarDatosPagosAutomaticos() throws Exception {
        String resp = " ";
        try {

            TransaccionService tService = new TransaccionService(usuario.getBd());

            CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
            String nit = request.getParameter("nit");
            String codsubcuenta = request.getParameter("subcuenta");

             Inversionista subcuenta = cis.consultarSubcuentaInversionista(usuario.getDstrct(), nit, Integer.parseInt(codsubcuenta));
            subcuenta.setPago_automatico(request.getParameter("pago_automatico"));
            subcuenta.setPeriodicidad_pago(request.getParameter("periodicidad_pago"));

            tService.crearStatement();
            tService.getSt().addBatch(cis.actualizarDatosPagosAutomaticos(subcuenta));
            tService.execute();
            resp = Utility.getIcono(request.getContextPath(), 5) + "Cambios realizado con exito.";

        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error."+exception.getMessage();
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }


    public void transferirRetiros() throws Exception {
        String resp = " ";
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
            ArrayList<MovimientosCaptaciones> movimientos_x_trans = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_retiros_x_transferir");
            String retirostem = request.getParameter("retiros");
            String[] trasnsacciones = retirostem.split(",");
            movimientos_x_trans = cis.getRetirosChequeados(movimientos_x_trans, trasnsacciones);
            String bco = ((String) request.getParameter("banco"));
            String[] bn;
            bn = bco.split(",");
            model.tablaGenService.buscarDatos("CAP_CUENTA", "INGRESO_CAPTACIONES");
            TablaGen tCuenta = model.tablaGenService.getTblgen();
            for (int i = 0; i < movimientos_x_trans.size(); i++) {
                Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), movimientos_x_trans.get(i).getNit(), movimientos_x_trans.get(i).getSubcuenta());
                MovimientosCaptaciones retiro = movimientos_x_trans.get(i);
                retiro.setEstado("T");
                retiro.setFecha(Util.getFechahoy());
                tService.getSt().addBatch(cis.confirmarRetiro(retiro));//se actualiza el retiro como transferido
                tService.getSt().addBatch(cis.actualizaFechaRetiro(retiro));// se actualiza la fecha del retiro
                tService.execute();

                ArrayList<MovimientosCaptaciones> movimientos_x_recalcular = cis.getMovimientosRecalcularRetiros(movimientos_x_trans.get(i));//se obtienen los movimientos a recalcular
                if (movimientos_x_recalcular.size() > 1)//validar que tiene movimientos posteriores a este para recalcular(inicialmente la lista carga 1 movimiento anterior y el movimiento actual)
                {   //Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), movimientos_x_trans.get(i).getNit(), movimientos_x_recalcular.get(i).getSubcuenta());
                    movimientos_x_recalcular = cis.RecalcularMovimientos(inversionista, movimientos_x_recalcular);//recalcular todos lso movimientos posteriores al retiro
                    cis.updateMovimientos(movimientos_x_recalcular);///se actualiza el retiro y recalculad
                    retiro = cis.getRetiroRecalculado(movimientos_x_recalcular, retiro);
                    ///generar comprobante del retiro total
                }


                  String nbanco = bn[0];
                 if(bn[0].equals("BANCOLOMBIAPAB")){
                           nbanco = "BANCOLOMBIA";
                 }
                retiro.setBanco(nbanco);//setiar banco
                retiro.setSucursal(bn[1]);//setiar la sucursal
                tService.crearStatement();


                if(inversionista.getGenera_documentos().equals("S"))
                {
                tService.getSt().addBatch(cis.crearEgresoRetiro(retiro));//cabecera egreso
                tService.getSt().addBatch(cis.crearEgresoDetalleRetiro(retiro));//detalle egreso
                tService.getSt().addBatch(cis.actualizarCxp_retiro(retiro));// cxp egreso
                tService.getSt().addBatch(model.ingresoService.incrementarSerie("EGRESO")); //Incrementa el numero de ingreso

                if(retiro.getTipo_movimiento().equals("RT") && inversionista.getGenera_documentos().equals("S"))
                {
                ArrayList<String> listQuery = this.crearComprobanteCierre(retiro, inversionista);// se genera comprobante
                for (String sql : listQuery) {
                    tService.getSt().addBatch(sql);
                }
                }
                }

            }


            //pendiente recalcular movimientos nuevamente

           tService.execute();

            /// enviar correo


            for (int i = 0; i < movimientos_x_trans.size(); i++) {

             Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), movimientos_x_trans.get(i).getNit(), movimientos_x_trans.get(i).getSubcuenta());
             MovimientosCaptaciones retiro = movimientos_x_trans.get(i);
             NitDAO nitDAO = new NitDAO(usuario.getBd());
             NitSot objnit = new NitSot();
             objnit =nitDAO.searchNit(retiro.getNit());
           //  cis.enviarCorreo("comercial@fintra.co",objnit.getE_mail()+";jpinedo@fintra.co;jesus.pinedo@hotmail.com;ajpabon@fintra.co", "", "RETIRO INVERSION FINTRA S.A.", this.getMensajeInversionista(objnit,inversionista,retiro,"T"), "", "");
             //// envio de sms
             Smservice smsservice = new Smservice(usuario.getBd());
             Sms  sms = new Sms();            
             sms.setCell(objnit.getTelefono());
             sms.setSMS(this.getMensajeSMSInversionista(objnit, inversionista, retiro));

            }
           ///generar archivo plano

            HArchivosTransferenciasCaptaciones hilo = new HArchivosTransferenciasCaptaciones();
            String codb = "";
            String msj = "";
            int sw = 0;
            if (bn[0].equals("BANCOLOMBIA")) {
                codb = "7B";
            } else if (bn[0].equals("BCO OCCIDENTE")) {
                codb = "23";
            } else if (bn[0].equals("BANCOLOMBIAPAB")) {
                codb = "7B";
            } else {
                msj = "No existen formatos para este banco";
                sw = 1;
            }
            if (sw == 0) {
                msj = "El archivo para transferir al banco " + bn[0] + " est&aacute; siendo generado....";
                hilo.start(model, usuario, trasnsacciones, "802022016", "FINTRA", codb, bn[0], bn[2], bn[1]);
            }
            hilo.join();
            bn = null;
            bco = null;
            ///reliquidar
             /*   for (int i = 0; i < movimientos_x_trans.size(); i++) {


            }
             */

            resp = Utility.getIcono(request.getContextPath(), 5) + "Se Transfirieron " + movimientos_x_trans.size() + " retiros con exito.";
            session.removeAttribute("lista_retiros_x_transferir");
        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error." + exception.getMessage();
            this.printlnResponse(resp, "text/plain");
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }

    public void CausarIntereses() throws Exception {
        String resp = " ";
        ArrayList<String> listQuery = new ArrayList<String>();
        ArrayList<String> listQuery2 = new ArrayList<String>();
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
            ArrayList<MovimientosCaptaciones> lista_x_causar = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_movimientos_x_causar");
            String mov_tem = request.getParameter("movimientos");
            String[] trasnsacciones = mov_tem.split(",");
            lista_x_causar = cis.getRetirosChequeados(lista_x_causar, trasnsacciones);
            int numItem = 1;
            model.tablaGenService.buscarDatos("CAP_CUENTA", "INGRESO_CAPTACIONES");
            TablaGen tCuenta = model.tablaGenService.getTblgen();
            /* ----------------------------------------------------------- */
            for (int i = 0; i < lista_x_causar.size(); i++) {
                Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), lista_x_causar.get(i).getNit(), lista_x_causar.get(i).getSubcuenta());

                MovimientosCaptaciones movimeinto_cierre = cis.CalcularMovimientoCierre(inversionista, lista_x_causar.get(i));
                movimeinto_cierre.setFecha(movimeinto_cierre.getFecha_causacion());
                movimeinto_cierre.setRetiro(0);
                tService.getSt().addBatch(cis.guardarMovimiento(movimeinto_cierre));
                if (inversionista.getGenera_documentos().equals("S")) {
                    listQuery = this.crearComprobanteCierre(movimeinto_cierre, inversionista);// se genera comprobante
                    for (String sql1 : listQuery) {
                        tService.getSt().addBatch(sql1);
                    }
                }
                if (inversionista.getPago_automatico().equals("S"))//valida si tiene pago automatico
                {
                    /// se vailda si aplica fecha de pago
                    if (cis.getPagoIntereses(movimeinto_cierre)) {

                        // se crea el retiro RI
                        ArrayList<MovimientosCaptaciones> lista = new ArrayList<MovimientosCaptaciones>();
                        lista.add(movimeinto_cierre);
                        MovimientosCaptaciones movimeinto_retiro_ri = new MovimientosCaptaciones();
                        movimeinto_retiro_ri = cis.calculosMovimientoRetiroRI(inversionista, movimeinto_cierre);
                        movimeinto_retiro_ri.setEstado("C");
                        Vector vItems = new Vector();
                        CXP_Doc factura = new CXP_Doc();
                        factura = this.crearCxp(movimeinto_retiro_ri);
                        CXPItemDoc item = crearDetalleCXP(movimeinto_retiro_ri.getNit(), "FAP", movimeinto_retiro_ri.getNo_transaccion(), factura.getDescripcion(), movimeinto_retiro_ri.getRetiro(), tCuenta.getReferencia(), numItem++);
                        vItems.add(item);
                        listQuery2 = model.cxpDocService.insertarCXPDoc_SQL(factura, vItems, new Vector(), usuario.getId_agencia(), "");  // se crea la cxp pendiente por transferir
                        for (String sql2 : listQuery2) {
                            tService.getSt().addBatch(sql2);
                        }
                        tService.getSt().addBatch(cis.confirmarRetiro(movimeinto_retiro_ri));
                        // se marca la fecha de ultimopago

                    }
                }
            }

            tService.execute();
            resp = Utility.getIcono(request.getContextPath(), 5) + "Se Causaron " + lista_x_causar.size() + " retiros con exito.";
            session.removeAttribute("lista_movimientos_x_causar");
        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error." + exception.getMessage();
            this.printlnResponse(resp, "text/plain");
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }

    public void aprobarRetiros() throws Exception {
        String resp = " ";
        try {

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
            ArrayList<MovimientosCaptaciones> movimientos = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_retiros_x_aprobar");
            String retirostem = request.getParameter("retiros");
            String[] trasnsacciones = retirostem.split(",");
            movimientos = cis.getRetirosChequeados(movimientos, trasnsacciones);

            for (int i = 0; i < movimientos.size(); i++) {
                MovimientosCaptaciones retiro = movimientos.get(i);
                retiro.setEstado("A");
                tService.getSt().addBatch(cis.confirmarRetiro(retiro));
                tService.getSt().addBatch(cis.aprobarCxp_retiro(usuario.getLogin(), usuario.getDstrct(), "FAP", retiro.getNo_transaccion(), retiro.getNit()));
            }
            tService.execute();
            resp = Utility.getIcono(request.getContextPath(), 5) + "Se Aprobaron " + movimientos.size() + " retiros con exito.";
            session.removeAttribute("lista_retiros_x_aprobar");
        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error.";
            this.printlnResponse(resp, "text/plain");
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }



     public void eliminarCuentasTransferencias() throws Exception {
        String resp = "";
        try {

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
            ArrayList<MovimientosCaptaciones> movimientos = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_datos_transferencia");
            String cuentas = request.getParameter("cuentas_chequeadas");
            String[] cuentas_chequeadas = cuentas.split(",");
            movimientos = cis.getRetirosChequeados(movimientos, cuentas_chequeadas);
            for (int i = 0; i < movimientos.size(); i++) {
                MovimientosCaptaciones movimiento = movimientos.get(i);
                cis.eliminarCuentasTransferencias(movimiento);
            }
           // tService.execute();
           resp = Utility.getIcono(request.getContextPath(), 5) + "Se Eliminaron " + movimientos.size() + " retiros con exito.";
           Gson gson = new Gson();
           String json = gson.toJson(resp);
           this.printlnResponse(json, "application/json;");
          //  session.removeAttribute("lista_datos_transferencia");
        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error.";
            this.printlnResponse(resp, "text/plain");
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    //Proceso de creacion del ingreso para cada consignacion
    public ArrayList<String> crearIngreso(Inversionista subcuenta, MovimientosCaptaciones movimiento) throws Exception {
        ArrayList<String>  sql = new ArrayList<String>();
        SerieGeneral serie = model.serieGeneralService.getSerie(usuario.getCia(),usuario.getId_agencia() , "INGC");
        String ni= serie.getUltimo_prefijo_numero();
        model.tablaGenService.buscarDatos("CAP_CUENTA", "INGRESO_CAPTACIONES");
        TablaGen tCuenta = model.tablaGenService.getTblgen();

        //Se consulta la informacion de los bancos
        Banco bancoIngreso = model.servicioBanco.obtenerBanco(usuario.getDstrct(), request.getParameter("banco"), request.getParameter("sucursal"));
        ChequeXFacturaService md = new ChequeXFacturaService(usuario.getBd());
        String monedaLocal = md.getMonedaLocal(usuario.getDstrct());
        Ingreso ing = new Ingreso();
        ing.setReg_status("");
        ing.setDstrct(usuario.getDstrct());
        ing.setTipo_documento("ING");
        ing.setNum_ingreso(ni);
        ing.setCodcli(subcuenta.getCodcliente());
        ing.setNitcli(subcuenta.getNit());
        ing.setConcepto("FE");
        ing.setTipo_ingreso("C");
        ing.setFecha_consignacion(movimiento.getFecha());
        ing.setFecha_ingreso(movimiento.getFecha());
        ing.setBranch_code(request.getParameter("banco"));
        ing.setBank_account_no(request.getParameter("sucursal"));
        ing.setCodmoneda(monedaLocal);
        ing.setBase(usuario.getBase());
        ing.setAgencia_ingreso(usuario.getId_agencia());
        ing.setPeriodo("000000");
        ing.setDescripcion_ingreso("Ingreso de inversi&oacute;n del titular " + subcuenta.getNombre_nit());
        ing.setVlr_tasa(1);
        ing.setFecha_tasa(movimiento.getFecha());
        ing.setCant_item(1);
        ing.setTransaccion(0);
        ing.setTransaccion_anulacion(0);
        ing.setCreation_user(usuario.getLogin());
        ing.setCreation_date(Util.fechaActualTIMESTAMP());
        ing.setLast_update(Util.fechaActualTIMESTAMP());
        ing.setUser_update(usuario.getLogin());
        ing.setNro_consignacion("");
        ing.setTasaDolBol(0);
        ing.setAbc("");
        ing.setCuenta(bancoIngreso.getCodigo_cuenta());
        ing.setCmc("");
        ing.setVlr_ingreso(movimiento.getConsignacion());
        ing.setVlr_ingreso_me(movimiento.getConsignacion());

        //Campos Comunes en Ingreso_detalle
        Ingreso_detalle ingDetalle = new Ingreso_detalle(); //Detalle del Ingreso
        ingDetalle.setDistrito(usuario.getDstrct());
        ingDetalle.setTipo_doc("FAC");
        ingDetalle.setItem(1);
        ingDetalle.setTipo_documento("ING");
        ingDetalle.setValor_ingreso(movimiento.getConsignacion());
        ingDetalle.setValor_ingreso_me(movimiento.getConsignacion());
        ingDetalle.setNumero_ingreso(ni);
        ingDetalle.setCodigo_reteica("");
        ingDetalle.setValor_reteica(0);
        ingDetalle.setValor_reteica_me(0);
        ingDetalle.setValor_diferencia(0);
        ingDetalle.setCreation_user(usuario.getLogin());
        ingDetalle.setCreation_date(Util.fechaActualTIMESTAMP());
        ingDetalle.setBase(usuario.getBase());
        ingDetalle.setCodigo_retefuente("");
        ingDetalle.setValor_retefuente(0);
        ingDetalle.setValor_retefuente_me(0);
        ingDetalle.setTipo_aux("RD");
        ingDetalle.setValor_tasa(1);
        ingDetalle.setFactura("");
        ingDetalle.setCuenta(tCuenta.getReferencia());///pendiente
        ingDetalle.setDocumento("");
        ingDetalle.setAuxiliar("");
        ingDetalle.setFecha_factura("0099-01-01");
        ingDetalle.setNit_cliente(subcuenta.getNit());
        sql.add(model.ingresoService.insertarIngreso(ing));
        sql.add(model.ingreso_detalleService.insertarIngresoDetalle(ingDetalle));
        model.serieGeneralService.setSerie(usuario.getCia(),usuario.getId_agencia() , "INGC"); //Incrementa el numero de ingreso

        return sql;

    }


        public void consultarReportesInversionistas() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<Inversionista> inversionistas = cis.reportesInversionistas(usuario.getDstrct());// consultar ultimo movimientos del mes

             for(int i=0;i<inversionistas.size();i++){
                   inversionistas.get(i).setSaldo_total(cis.getSaldoTotal(inversionistas.get(i)));
        }

        session.setAttribute("lista_inversionistas", inversionistas);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(inversionistas) + "}";
        this.printlnResponse(json, "application/json;");

    }



    public void consultarRetirosXconfirmar() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<MovimientosCaptaciones> movimientos = cis.consultarRetirosXconfirmar(usuario.getDstrct());// consultar ultimo movimientos del mes
        session.setAttribute("lista_retiros_x_confirmar", movimientos);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movimientos) + "}";
        this.printlnResponse(json, "application/json;");

    }

    public void consultariInformacionCierreSocios() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String periodo = request.getParameter("periodo");
        ArrayList<MovimientosCaptaciones> movSocios = cis.getInformeCierreSocio(usuario.getDstrct(), periodo);
      
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movSocios) + "}";
        this.printlnResponse(json, "application/json;");
        
       
        
     }
    
     public void consultariInformacionCierreParticular() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String periodo = request.getParameter("periodo");
        ArrayList<MovimientosCaptaciones> movOtros = cis.getInformeCierreOtros(usuario.getDstrct(), periodo);
        
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movOtros) + "}";
        this.printlnResponse(json, "application/json;");
        
    }
     
     public void insertarGrafico() throws Exception{
        String periodo = request.getParameter("periodo");
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        ArrayList<MovimientosCaptaciones> movimientos = cis.getInformeCierreInversionistas(usuario.getDstrct(), periodo);
        String url = rb.getString("ruta") + "/exportar/migracion/" + "informe" + periodo + ".jpg";
       
        if (movimientos.size() > 0) {
             JFreeChart grafica = cis.createChartBarra(cis.createDatasetBarra(movimientos));
             try {
                 cis.crearImgGrafico(grafica, url, 800, 500, 0.90);
             } catch (Exception e) {

                 e.printStackTrace();
             }
         }
     }
     
     public void consultariInformacionDetalleInversionista() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String periodo = request.getParameter("periodo");
        String nit = request.getParameter("nit");
        ArrayList<MovimientosCaptaciones> detalleInversionista = cis.getInformeDetalleInversionista(usuario.getDstrct(),periodo,nit);
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(detalleInversionista) + "}";
        this.printlnResponse(json, "application/json;");
        
    }
     
    public void consultarRetirosXtransferir() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<MovimientosCaptaciones> movimientos = cis.consultarRetirosXtransferir(usuario.getDstrct());// consultar ultimo movimientos del mes
        session.setAttribute("lista_retiros_x_transferir", movimientos);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movimientos) + "}";
        this.printlnResponse(json, "application/json;");
    }

    public void consultarInversionistas() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<Inversionista> inversionistas = cis.consultarInversionistas(usuario.getDstrct());

        for(int i=0;i<inversionistas.size();i++){
                   inversionistas.get(i).setSaldo_total(cis.getSaldoTotal(inversionistas.get(i)));
        }

        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(inversionistas) + "}";
        this.printlnResponse(json, "application/json;");

    }

    public void consultarMovimientosMes() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String nit = request.getParameter("nit") == null ? "" : request.getParameter("nit");
        String subcuenta = request.getParameter("subcuenta");
        String periodo = request.getParameter("periodo");
        Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), nit, Integer.parseInt(subcuenta));
        ArrayList<MovimientosCaptaciones> movimientos = cis.consultarMovimientosMes(usuario.getDstrct(), nit, subcuenta, periodo);
       /* if (movimientos.size() > 0) {
            movimientos.add(this.calculosMovimientoFinal(inversionista, movimientos));
        }*/
        session.setAttribute("lista_movimientos_mes", movimientos);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movimientos) + "}";
        this.printlnResponse(json, "application/json;");


    }

    public void exportar() throws Exception {
        String resp = "";
        boolean generado = true;
        int op_icono = 0;
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String nit = request.getParameter("nit") == null ? "" : request.getParameter("nit");
        String subcuenta = request.getParameter("subcuenta");
        String formato = request.getParameter("formato");
        String periodo = request.getParameter("periodo");



        Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), nit, Integer.parseInt(subcuenta));
        ArrayList<MovimientosCaptaciones> movimientos = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_movimientos_mes");

          String url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/" + inversionista.getNit()+"_"+inversionista.getSubcuenta()+ periodo + "." + formato;


        if (formato.equals("pdf")) {
            generado = cis.exportarExtractoPdf(usuario.getLogin(), inversionista, movimientos, periodo);
            op_icono = 8;
        } else {
            generado = cis.exportarExtractoXls(usuario.getLogin(), inversionista, movimientos, periodo);
            op_icono = 7;
        }

        if (generado) {
            resp = Utility.getIcono(request.getContextPath(), 5) + "Extracto generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), op_icono) + " <a href='" + url + "' >Ver Extracto</a></td>";
        }

        this.printlnResponse(resp, "text/plain");

    }




        public void exportarInformeConsolidado() throws Exception {
        String resp = "";
        boolean generado = true;
        int op_icono = 0;
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String formato = request.getParameter("formato");

        ArrayList<Inversionista> lista_inversionistas = (ArrayList<Inversionista>) session.getAttribute("lista_inversionistas");

        String url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/informeConsolidado." + formato;


        if (formato.equals("pdf")) {
            generado = cis.exportarInformeConsolidadoPdf(usuario, lista_inversionistas);
            op_icono = 8;
        } else {
           generado = cis.exportarInformeConsolidadoXls(usuario,lista_inversionistas);
            op_icono = 7;
        }

        if (generado) {
            resp = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), op_icono) + " <a href='" + url + "' >Ver Informe</a></td>";
        }

        this.printlnResponse(resp, "text/plain");

    }

    public void exportarInformeCierre() throws Exception {
         String resp = "";
        try
        {
        boolean generado = true;
        int op_icono = 0;
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String periodo = request.getParameter("periodo");
        ArrayList<MovimientosCaptaciones> movimientos = cis.getInformeCierreInversionistas(usuario.getDstrct(), periodo);

        String url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/" + "informe" + periodo + ".pdf";

        generado = cis.exportarInformePdf(usuario.getLogin(), movimientos, periodo);
        op_icono = 8;
        if (generado) {
            resp = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), op_icono) + " <a href='" + url + "' >Ver Informe</a></td>";
        } else {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error.<br/><br/>";

        }

        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error.";
            this.printlnResponse(resp, "text/plain");
            throw exception;
        }


        this.printlnResponse(resp, "text/plain");

    }

    public void consultarRetirosXaprobar() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<MovimientosCaptaciones> movimientos = cis.consultarRetirosXaprobar(usuario.getDstrct());// consultar ultimo movimientos del mes
        session.setAttribute("lista_retiros_x_aprobar", movimientos);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movimientos) + "}";
        this.printlnResponse(json, "application/json;");

    }

    public void consultarMovimeintosXCausar() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<MovimientosCaptaciones> movimientos = cis.getMovimientosCausacion(usuario.getDstrct());// consultar ultimo movimientos del mes
        double intereses = 0;
        ArrayList<MovimientosCaptaciones> lista_final = new ArrayList<MovimientosCaptaciones>();

        for (int i = 0; i < movimientos.size(); i++) {
            Inversionista subcuenta = cis.consultarSubcuentaInversionista(usuario.getDstrct(), movimientos.get(i).getNit(), movimientos.get(i).getSubcuenta());
            movimientos.get(i).setFecha_causacion(Util.getFechahoy());
            intereses = cis.getInteresesCausacion(subcuenta, movimientos.get(i));
            movimientos.get(i).setValor_intereses(intereses);

            intereses = 0;
        }
        session.setAttribute("lista_movimientos_x_causar", movimientos);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movimientos) + "}";
        this.printlnResponse(json, "application/json;");
    }

    public void calcularInteresCausacion() throws Exception {
        String xml = "";
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        ArrayList<MovimientosCaptaciones> movimientos = new ArrayList<MovimientosCaptaciones>();
        movimientos = (ArrayList<MovimientosCaptaciones>) session.getAttribute("lista_movimientos_x_causar");
        //ArrayList<MovimientosCaptaciones> lista_final= new  ArrayList<MovimientosCaptaciones> ();
        //lista_final  =  (ArrayList<MovimientosCaptaciones>)session.getAttribute("lista_final_x_causar") ;


        double intereses = 0;
        int posicion = 0;
        movimientos.get(0).setValor_intereses(intereses);
        String no_transaccion = request.getParameter("no_transaccion");
        String fecha_causacion = request.getParameter("fecha_causacion");
        int codsubcuenta = Integer.parseInt(request.getParameter("subcuenta"));
        String nit = request.getParameter("nit");
        Inversionista subcuenta = cis.consultarSubcuentaInversionista(usuario.getDstrct(), nit, codsubcuenta);

        for (int i = 0; i < movimientos.size(); i++) {//se calcula interees con la nueva fecha para el registro indicado en la lista
            if (movimientos.get(i).getNo_transaccion().equals(no_transaccion)) {
                movimientos.get(i).setFecha_causacion(fecha_causacion);
                intereses = cis.getInteresesCausacion(subcuenta, movimientos.get(i));
                movimientos.get(i).setValor_intereses(intereses);

            }
        }

        session.setAttribute("lista_movimientos_x_causar", movimientos);
        //  session.setAttribute("lista_final_x_causar", lista_final);


        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<datos>";
        xml += "<intereses>" + intereses + "</intereses>";
        xml += "<fecha>" + fecha_causacion + "</fecha>";
        xml += "</datos>";
        this.printlnResponse(xml, "text/xml");
    }

    public CXP_Doc crearCxp(MovimientosCaptaciones movimiento) throws SQLException {
        Proveedor proveedor = model.proveedorService.obtenerProveedorPorNit(movimiento.getNit());
        CXP_Doc factura = new CXP_Doc();

        //   String monedaUsuario = (String)session.getAttribute("Moneda");
        factura.setDstrct(usuario.getDstrct());
        factura.setProveedor(movimiento.getNit());
        factura.setTipo_documento("FAP");
        factura.setDocumento(movimiento.getNo_transaccion());
        factura.setDescripcion("CXP Por retiro de inversionista " + proveedor.getNombre());
        factura.setAgencia(usuario.getId_agencia());
        factura.setId_mims("");
        factura.setFecha_documento(Util.getFechahoy());
        factura.setTipo_documento_rel("");
        factura.setDocumento_relacionado("");
        factura.setFecha_aprobacion("0099-01-01 00:00:00");
        factura.setUsuario_aprobacion("");
        factura.setClase_documento_rel("4");
        factura.setAprobador("JGOMEZ");
        factura.setFecha_vencimiento(Util.fechaFinal(Util.getFechahoy(), 30));
        factura.setUltima_fecha_pago("0099-01-01 00:00:00");
        factura.setBanco(proveedor.getC_branch_code());
        factura.setSucursal(proveedor.getC_bank_account());
         factura.setMoneda("PES");
        factura.setMoneda_banco("PES");
        factura.setHandle_code("PN");
        //factura.setTasa(valor_tasa);
        factura.setUsuario_contabilizo("");
        factura.setFecha_contabilizacion("0099-01-01 00:00:00");
        factura.setUsuario_anulo("");
        factura.setFecha_anulacion("0099-01-01 00:00:00");
        factura.setFecha_contabilizacion_anulacion("0099-01-01 00:00:00");
        factura.setObservacion("");
        factura.setNum_obs_autorizador(0);
        factura.setNum_obs_pagador(0);
        factura.setNum_obs_registra(0);
        factura.setCreation_user(usuario.getLogin());
        factura.setUser_update(usuario.getLogin());
        factura.setBase(usuario.getBase());
        factura.setVlr_neto(movimiento.getRetiro());
        factura.setVlr_total_abonos(0);
        factura.setVlr_saldo(movimiento.getRetiro());
        factura.setVlr_neto_me(movimiento.getRetiro());
        factura.setVlr_total_abonos_me(0);
        factura.setVlr_saldo_me(movimiento.getRetiro());
        return factura;
    }

    private CXPItemDoc crearDetalleCXP(String proveedor, String tipoDocumento, String documento, String descripcion, double valor, String cuenta, int numItem) {
        CXPItemDoc item = new CXPItemDoc();
        item.setDstrct(usuario.getDstrct());
        item.setProveedor(proveedor);
        item.setTipo_documento(tipoDocumento);
        item.setDocumento(documento);
        item.setItem("00" + numItem);
        item.setDescripcion(descripcion);
        item.setConcepto("");
        item.setVlr(valor);
        item.setVlr_me(valor);
        item.setCodigo_cuenta(cuenta);
        item.setCodigo_abc("");
        item.setPlanilla("");
        item.setCodcliarea("");
        item.setTipcliarea("");
        item.setCreation_user(usuario.getLogin());
        item.setUser_update(usuario.getLogin());
        item.setBase(usuario.getBase());
        item.setAuxiliar("");
        item.setTipoSubledger("");
        Vector vImpuestosPorItem = new Vector();
        item.setVItems(vImpuestosPorItem);
        return item;
    }

    public void consultarCuentasTransferencias() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String tipo_transferencia = request.getParameter("tipo_transferencia");
        String nit = request.getParameter("inversionista");
        ArrayList<MovimientosCaptaciones> movimientos = cis.consultarCuentasTransferencias(usuario.getDstrct(), nit,tipo_transferencia);// consultar ultimo movimientos del mes
        session.setAttribute("lista_datos_transferencia", movimientos);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(movimientos) + "}";
        this.printlnResponse(json, "application/json;");

    }

    public void consultarSaldos() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String nit = request.getParameter("nit");


        ArrayList<MovimientosCaptaciones> saldo_subcuentas = new ArrayList<MovimientosCaptaciones>();
        ArrayList<MovimientosCaptaciones> cuentas_inversionistas = cis.consultarSubcuentasMovimientosInversionista(usuario.getDstrct(), nit);// consultar ultimo movimientos del mes

        for (int i = 0; i < cuentas_inversionistas.size(); i++) {
            ArrayList<MovimientosCaptaciones> movimientos_subcuenta =
                    cis.consultarUltimosMovimientos(usuario.getDstrct(), cuentas_inversionistas.get(i).getNit(), String.valueOf(cuentas_inversionistas.get(i).getSubcuenta()), "N");// consultar ultimo movimiento
                    Inversionista inversionista = cis.consultarSubcuentaInversionista(usuario.getDstrct(), nit, cuentas_inversionistas.get(i).getSubcuenta());
                    saldo_subcuentas.add(cis.calculosMovimientoFinal(inversionista, movimientos_subcuenta));
        }


        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(saldo_subcuentas) + "}";
        this.printlnResponse(json, "application/json;");

    }

   


 /**
     * Genera el sql para un comprobante
     * @param credito
     * @throws SQLException
     * @throws Exception
     */
    public ArrayList<String> crearComprobanteCierre(MovimientosCaptaciones movimiento, Inversionista inversionista) throws SQLException, Exception {
        ArrayList<String> listSql = new ArrayList<String>();
        try {
            //Se instancia el model de contabilidad
            com.tsp.finanzas.contab.model.Model modelContab = new com.tsp.finanzas.contab.model.Model(usuario.getBd());

            String tipodoc = "CDIAR";

            String[] monedaBase = modelContab.GrabarComprobanteSVC.getMoneda(usuario.getCia()).split("-");
            Hashtable DatosTipoDoc = modelContab.GrabarComprobanteSVC.buscarDatosTipoDoc(usuario.getDstrct(), tipodoc);
            java.sql.Timestamp ffinal = Util.ConvertiraTimestamp(movimiento.getFecha().substring(0, 10) + " 00:00:00");

            String periodo =(movimiento.getFecha().substring(0,4)+movimiento.getFecha().substring(5,7));
            String numdoc = DatosTipoDoc.get("prefijo") + periodo.substring(2) + UtilFinanzas.rellenar((String) DatosTipoDoc.get("serie_act"), "0", 4);
            String concepto = "COMPROBANTE CIERRE " + movimiento.getNo_transaccion();


            double diferencia = (Math.round(movimiento.getValor_intereses())+Math.round(movimiento.getIntereses_acomulados())) - Math.round(movimiento.getValor_retefuente()) - Math.round(movimiento.getValor_reteica());
            ComprobanteFacturas comprobante = new ComprobanteFacturas();

            comprobante.setDstrct(usuario.getDstrct());
            comprobante.setTipodoc(tipodoc);
            comprobante.setNumdoc(numdoc);
            comprobante.setSucursal(usuario.getId_agencia());
            comprobante.setPeriodo(periodo);
            comprobante.setFechadoc(movimiento.getFecha());
            comprobante.setDetalle(concepto);
            comprobante.setTercero(movimiento.getNit());
            comprobante.setTipo("C");
            comprobante.setTotal_debito(Math.round(movimiento.getValor_intereses())+Math.round(movimiento.getIntereses_acomulados()));
            comprobante.setTotal_credito(Math.round(movimiento.getValor_intereses())+Math.round(movimiento.getIntereses_acomulados()));
            comprobante.setMoneda(monedaBase[0]);
            comprobante.setAprobador(usuario.getLogin());
            comprobante.setBase(monedaBase[1]);
            comprobante.setDocumento_interno((String) DatosTipoDoc.get("codigo_interno"));
            comprobante.setOrigen("COMPROBANTE");
            comprobante.setTipo_operacion("009");

            ArrayList items = new ArrayList();

            //Generar los items del comprobante
            Tipo_impuestoService TimpuestoSvc = new Tipo_impuestoService(usuario.getBd());

            Tipo_impuesto impuestos_rfte = TimpuestoSvc.buscarImpuestoxCodigo(movimiento.getDstrct(), inversionista.getRetefuente());
            Tipo_impuesto impuestos_rica = TimpuestoSvc.buscarImpuestoxCodigo(movimiento.getDstrct(), inversionista.getReteica());
            //items total intereses
            model.tablaGenService.buscarDatos("CAP_CUENTA", "INTERESES_CAPTACIONES");
            TablaGen tCuenta = model.tablaGenService.getTblgen();
            ComprobanteFacturas comprodet = this.crearComprodet(movimiento, comprobante, tCuenta.getReferencia(), Math.round((movimiento.getValor_intereses())+Math.round(movimiento.getIntereses_acomulados())), 0, "Intereses");
            items.add(comprodet);
            //items retefuente
            comprodet = this.crearComprodet(movimiento, comprobante, impuestos_rfte.getCod_cuenta_contable(), 0, Math.round(movimiento.getValor_retefuente()), impuestos_rfte.getDescripcion());
            items.add(comprodet);
            //items reteica
            comprodet = this.crearComprodet(movimiento, comprobante, impuestos_rica.getCod_cuenta_contable(), 0, Math.round(movimiento.getValor_reteica()), impuestos_rica.getDescripcion());
            items.add(comprodet);
            //items diferencia
            model.tablaGenService.buscarDatos("CAP_CUENTA", "CUENTA_CAPTACIONES");
            TablaGen tCuenta_ci = model.tablaGenService.getTblgen();
            comprodet = this.crearComprodet(movimiento, comprobante, tCuenta_ci.getReferencia(), 0, diferencia, comprobante.getDetalle());
            items.add(comprodet);

            comprobante.setItems(items);
            comprobante.setTotal_items(4);
            modelContab.GrabarComprobanteSVC.setComprobante(comprobante);

            ArrayList listaContable = new ArrayList();
            listaContable.add(comprobante);
            if (modelContab.GrabarComprobanteSVC.existeDocumento(usuario.getDstrct(), tipodoc, numdoc)) {
                throw new Exception("Este numero de documento generado ya existe dentro de la base de datos");
            } else {
                listSql = modelContab.ContabilizacionFacSvc.generarSQLComprobante(comprobante, usuario.getLogin());
                String msgError = modelContab.ContabilizacionFacSvc.getESTADO();

                if (!msgError.equals("")) {
                    throw new Exception(msgError);
                } else {
                    modelContab.GrabarComprobanteSVC.UpdateSerie(usuario.getDstrct(), tipodoc);
                }
            }
        } catch (Exception exception) {
            throw exception;
        }
        return listSql;
    }



    /**
     * Genera un detalle de comprobante
     * @param cbDetalle
     * @param comprobante
     * @param codContable
     * @param debito
     * @param credito
     * @return bean ComprobanteFacturas con los datos del comprobante
     * @throws SQLException
     * @throws Exception
     */
    public ComprobanteFacturas crearComprodet(MovimientosCaptaciones movimiento, ComprobanteFacturas comprobante, String codContable, double debito, double credito, String detalle) throws SQLException, Exception {
        //Se instancia el model de contabilidad
        com.tsp.finanzas.contab.model.Model modelContab = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
        modelContab.GrabarComprobanteSVC.buscarCuenta(usuario.getDstrct(), codContable);
        boolean Existe = modelContab.GrabarComprobanteSVC.isExisteCuenta();
        ComprobanteFacturas comprodet = new ComprobanteFacturas();
        comprodet.setDstrct(usuario.getDstrct());
        comprodet.setTipodoc(comprobante.getTipodoc());
        comprodet.setNumdoc(comprobante.getNumdoc());
        comprodet.setPeriodo(comprobante.getPeriodo());
        comprodet.setCuenta(codContable);
        comprodet.setAuxiliar("");
        comprodet.setDetalle(detalle);
        comprodet.setAbc("");
        comprodet.setTdoc_rel("FAP");
        comprodet.setNumdoc_rel(movimiento.getNo_transaccion());
        comprodet.setTotal_debito(debito);
        comprodet.setTotal_credito(credito);
        comprodet.setTercero(movimiento.getNit());
        comprodet.setBase(usuario.getBase());
        comprodet.setDocumento_interno(comprobante.getDocumento_interno());
        comprodet.setExisteCuenta(Existe);
        comprodet.setTipo("D");
        comprodet.setOrigen("COMPROBANTE");
        Vector tipSubledger = null;
        if (!Existe) {
            throw new Exception("Los numeros de cuentas parametrizados para el comprobante no existen en la base de datos o no pueden ser usadas dentro de este modulo");
        } else {
            Hashtable cuenta = modelContab.GrabarComprobanteSVC.getCuenta();
            if (cuenta.get("modulo1").toString().equalsIgnoreCase("N")) {
                comprodet.setExisteCuenta(false);
                throw new Exception("Los numeros de cuentas parametrizados para el comprobante no existen en la base de datos o no pueden ser usadas dentro de este modulo");
            }
            modelContab.subledgerService.buscarCodigosCuenta(codContable);
            tipSubledger = modelContab.subledgerService.getCodigos();
        }
        comprodet.setCodSubledger(tipSubledger);

        return comprodet;
    }

    public void generarGraficoCierre() throws IOException {
        response.setContentType("image/jpeg");
        // this.printlnResponse(next, next);
        java.io.OutputStream salida = response.getOutputStream();
        //  JFreeChart grafica = createChart(cis.createDataset());
        int ancho = 650;
        int alto = 450;
        //      ChartUtilities.writeChartAsJPEG(salida, grafica, ancho, alto);

        salida.close();
    }



        public String getMensajeInterno(NitSot infoNit,Inversionista inversionista,MovimientosCaptaciones movimiento,String op)
    {
        String mensaje = "";


            mensaje = "<span style='font-family: ; font-size:14px; line-height:25px '; >"
                    + "Se ha generado solicitud de retiro inversionista a nombre de " + inversionista.getNombre_subcuenta() + " por valor de $ " + Util.customFormat(movimiento.getRetiro()) + "</br>Consultar la opci&oacute;n Transacciones por Confirmar para proceder<br />"
                    + "</span>";
        return mensaje;
    }


    public String getMensajeInversionista(NitSot infoNit,Inversionista inversionista,MovimientosCaptaciones movimiento,String op)
    {
        String mensaje = "";
        String datos_retiro="";

        if (op.equals("S")) {
            mensaje = "<b>RETIRO DE INVERSI&Oacute;N</b><br/><br/>"
                    + "<span style='font-family:'font-size:14px; line-height:25px' >Estimado  Inversionista,<br/>"
                    + "Le  notificamos que el retiro solicitado, ha sido recibido para su desembolso, motivo por el cual lo  estaremos contactando v&iacute;a telef&oacute;nica para su confirmaci&oacute;n.<br />"
                    + "A continuaci&oacute;n le presentamos el detalle de su solicitud:";
                    if(movimiento.getTipo_transferencia().equals("E"))
                    {
                        datos_retiro=
                    "<br/><b>Titular:</b>"+ movimiento.getTitular_cuenta() + "<br/>"
                    + "<b>Nit Titular:</b>  "+ movimiento.getNit_cuenta()+ "  <br/>"
                    + "<b>No Cuenta:</b> "+ movimiento.getCuenta() + "   <br/>"
                    + "<b>Tipo de Cuenta : "+ movimiento.getTipo_cuenta() + "</b>    <br/>"
                    + "<b>Valor:</b> $" + Util.customFormat(movimiento.getRetiro()) + " <br/>"
                    + "<b>Fecha:</b> $" + movimiento.getFecha() + " <br/>";
                   }
                    if(movimiento.getTipo_transferencia().equals("C"))
                    {
                        datos_retiro=
                    "<br/><b>Nit Beneficiario:</b>"+ movimiento.getNit_beneficiario() + "<br/>"
                    + "<b>Nombre Beneficiario:</b>  "+ movimiento.getNombre_beneficiario()+ "  <br/>"
                    + "<b>Cheque Cruzado:</b> "+ movimiento.getCheque_cruzado() + "   <br/>"
                    + "<b>Primer Beneficiario : "+ movimiento.getCheque_primer_beneficiario()+ "</b>    <br/>"
                    + "<b>Valor:</b> $" + Util.customFormat(movimiento.getRetiro()) + " <br/>"
                     + "<b>Fecha:</b> $" + movimiento.getFecha() + " <br/>";
                   }
                    mensaje = mensaje +datos_retiro;
                     mensaje = mensaje
                    + " <br/><br />Agradecemos de antemano su atenci&oacute;n, cualquier inquietud podr&aacute; comunicarse con nosotros al 367 99 00 Ext. 1214, 1212, 1211 y all&iacute; recibir&aacute; la asesor&iacute;a necesaria. Recuerde que desde nuestra p&aacute;gina web www.fintra.co, podr&aacute; realizar consulta de sus movimientos."
                    +"<br/><br/>Cordialmente,<br/>"
                    +"<br/><b>Departamento de Tesorer&iacute;a</b><br/>";

        }

        if (op.equals("T")) {
            mensaje = "<b>RETIRO DE INVERSI&Oacute;N</b><br/><br/>"
                    + "<span style='font-family:'font-size:14px; line-height:25px' >Estimado  Inversionista,<br/>"
                    + "Le notificamos que el monto de inversi&oacute;n solicitado fue desembolsado. A continuaci&oacute;n le presentamos el resumen de la operaci&oacute;n:";

                    if(movimiento.getTipo_transferencia().equals("Cheque"))
                    {
                    datos_retiro= "<table width='1200'  ><tr style='background-color:#063; color:#FFF'>"
                    +"<td width='116' align='center'><b>Modalidad</b></td>"
                    + "<td width='428' align='center'><b>Nombre Beneficiario</b></td> "
                    + "<td width='117' align='center'><b>Identificaci&oacute;n</b></td>"
                    +"<td width='118' align='center'><b>Cheque Cruzado</b></td>"
                    + "<td width='111' align='center'><b>Primer Beneficiario</b></td>"
                    + "<td width='107' align='center'><b>Valor</b></td>"
                    +"</tr><tr align='center'>2"
                     + "<td>"+ movimiento.getTipo_transferencia()+ "</td>"
                    + "<td>"+ movimiento.getNombre_beneficiario()
                    + "</td> <td>"+movimiento.getNit_beneficiario()+ "</td>"
                    +"<td>"+ movimiento.getCheque_cruzado()+ "</td>"
                    + "<td>"+ movimiento.getCheque_primer_beneficiario()+ "</td>"
                    +"<td align='right'>$" + Util.customFormat(movimiento.getRetiro()) + "</td>  </tr></table>";
                    }

                    if(movimiento.getTipo_transferencia().equals("Electronica"))
                    {
                    datos_retiro= "<table width='1200' ><tr style='background-color:#063; color:#FFF'>"
                    +"<td width='116' align='center'><b>Modalidad</b></td>"
                    + "<td width='428' align='center'><b>Nombre  del Titular</b></td>"
                    + "<td width='117' align='center'><b>Identificaci&oacute;n</b></td>"
                    +"<td width='157' align='center'><b>Entidad  Bancaria</b></td>"
                    +"<td width='118' align='center'><b>No.  de Cuenta</b></td>"
                    + "<td width='111' align='center'><b>Tipo de Cuenta</b></td>"
                    + "<td width='107' align='center'><b>Valor</b></td>"
                    +"</tr>"
                    + "<tr align='center'><td>"+ movimiento.getTipo_transferencia()+ "</td>"
                    + "<td>"+ movimiento.getTitular_cuenta()
                    + "</td> <td>"+ movimiento.getNit_cuenta()+ "</td>"
                    +"<td>"+ movimiento.getBanco()+ "</td>"
                    + "<td>"+ movimiento.getCuenta()+ "</td>"
                    + " <td>"+ movimiento.getTipo_cuenta()+ "</td>"
                    +"<td align='right'>$" + Util.customFormat(movimiento.getRetiro()) + "</td>  </tr></table>";
                    }
                     mensaje = mensaje +datos_retiro;
                     mensaje = mensaje
                    + "<br/><br />Agradecemos de antemano su atenci&oacute;n, cualquier inquietud podr&aacute; comunicarse con nosotros al 367 99 00 Ext. 1214, 1212, 1211 y all&iacute; recibir&aacute; la asesor&iacute;a necesaria. Recuerde que desde nuestra p&aacute;gina web www.fintra.co, podr&aacute; realizar consulta de sus movimientos."
                    +"<br/><br/>Cordialmente,<br/>"
                    +"<br/><b>Departamento de Tesorer&iacute;a</b><br/>";
        }

        return mensaje;
    }








        public String getMensajeSMSInversionista(NitSot infoNit,Inversionista inversionista,MovimientosCaptaciones movimiento)
       {
        String mensaje = "";
        String datos_retiro="";



            mensaje = "Fintra efectu� transferencia, por valor $" + Util.customFormat(movimiento.getRetiro()) + ""
                    + "xxxxx por concepto de Retiro de Inversion. www.fintra.co";

                    if(movimiento.getTipo_transferencia().equals("Electronica"))
                    {
                        datos_retiro=" en BCO "+movimiento.getBanco() + " Cuenta "+movimiento.getTipo_cuenta()+" "+ movimiento.getCuenta();
                    }
                    if(movimiento.getTipo_transferencia().equals("Cheque"))
                    {
                        datos_retiro= " a " + movimiento.getNombre_beneficiario()+ " ";
                    }

                    if(movimiento.getTipo_transferencia().equals("Interna"))
                    {
                        datos_retiro= " a " + movimiento.getNombre_beneficiario_captacion()+ " ";
                    }
                    mensaje = mensaje.replaceAll("xxxxx", datos_retiro) ;
         return mensaje;

        }





        public void confirmarModificacionPagos() throws Exception {
        String resp = " ";
        try {
            int periodicidad_pago = Integer.parseInt(request.getParameter("periodicidad_pago"));
            String hoy = Util.getFechahoy();
            String fecha_pagos= Util.fechaMasDias(hoy,periodicidad_pago);
            resp =  fecha_pagos;
            int a=Integer.parseInt(fecha_pagos.substring(0, 4));
            int m = Integer.parseInt(fecha_pagos.substring(5, 7));
            resp= Util.NombreMes(m)+" de "+a;
        } catch (Exception exception) {
            resp = Utility.getIcono(request.getContextPath(), 1) + "Error. "+exception.getMessage();
            this.printlnResponse(resp, "text/plain");
            throw exception;
        }
        this.printlnResponse(resp, "text/plain");

    }

    private void consultarTotalInversionistaMes() throws Exception {
        CaptacionInversionistaService cis = new CaptacionInversionistaService(usuario.getBd());
        String periodo = request.getParameter("periodo");
        BeanGeneral totalInversionista = cis.clasificacionInversionistasMes(usuario.getDstrct(),periodo);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(totalInversionista) + "}";
        this.printlnResponse(json, "application/json;");
    }



}
