package com.tsp.operation.controller;

import javax.servlet.http.*;
import javax.servlet.*;
import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.lang.reflect.*;
import java.util.Vector;
import java.net.URL;
import java.io.*;
import java.util.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;


public class RetornarVacioAction extends Action implements java.io.Serializable {
    static Logger logger = Logger.getLogger(ColpapelInsertAction.class);
    public RetornarVacioAction() {
    }
    
    private static Properties dbProps = new Properties();
    
    /**
     * run
     */
    public void run() throws ServletException {
        try {
            logger.info("ENTRO EN LA OPCION DE RETORNAR VACIO...");
            ResourceBundle  dbProps = ResourceBundle.getBundle("com/tsp/operation/model/StoreInf");
            logger.info("YA CARGUE EL DBPROPS...");
            HttpSession session = request.getSession();
            Usuario usuario = ( Usuario ) session.getAttribute( "Usuario" );
            String fechaInicio ="0099-01-01 00:00";
            String fechaFin = "now()";
            if(request.getParameter( "fechai" )!=null &&  request.getParameter( "fechaf" )!=null){
                fechaInicio = request.getParameter( "fechai" ).equals("")?"0099-01-01 00:00":request.getParameter( "fechai" );
                fechaFin = request.getParameter( "fechaf" ).equals("")?"now()":request.getParameter( "fechaf" );
            }
            logger.info("Fecha i..."+fechaInicio);
            logger.info("Fecha f..."+fechaFin);
            String user =usuario.getLogin();
            String asignado="";
            String[] asignados;
            String parametro;
            
            java.util.Enumeration enum1;
            
            /*
             * Obtenemos todos los registros seleccionados en el formulario
             * para retornar.
             */
            enum1 = request.getParameterNames();
            
            while (enum1.hasMoreElements()) {
                
                parametro = (String) enum1.nextElement();
                if(parametro.indexOf("check")>=0){
                    logger.info("Chequeado..."+request.getParameter(parametro));
                    asignado=request.getParameter(parametro)+";"+asignado;
                }
                if(parametro.indexOf("agencia_")>=0){
                    logger.info("Se buscan las agencias de: "+asignado);
                    logger.info(parametro);
                }
            }
            if(!asignado.equals("")){
                logger.info("Total asignados "+asignado);
                asignados= asignado.split(";");
                
                logger.info("Cantidad de asignados "+asignados.length);
                
            /*
             *Obtenemos los datos del standard guardado en properties
             */
                String sj = dbProps.getString("Stdjob_req_no");
                String sj_desc = dbProps.getString("Stdjob_req_desc");
                
                logger.info("Standard "+sj);
                logger.info("Descripcion "+sj_desc);
           /*
            *Creamos el requerimiento con los datos del standard job de vacios que se encuentra
            *en el properties y con origen y destino de la placa.
            */
                ReqCliente reqcliente= new ReqCliente();
                reqcliente.setdstrct_code("FINV");
                reqcliente.setnum_sec(1);
                reqcliente.setstd_job_no(sj);
                reqcliente.setnumpla("") ;
                reqcliente.setcliente(sj) ;
                reqcliente.setorigen("") ;
                reqcliente.setdestino("") ;
                reqcliente.setclase_req("") ;
                reqcliente.setTipo_recurso1("") ;
                reqcliente.setTipo_recurso2("") ;
                reqcliente.setTipo_recurso3("") ;
                reqcliente.setTipo_recurso4("") ;
                reqcliente.setTipo_recurso5("") ;
                reqcliente.setrecurso1("") ;
                reqcliente.setrecurso2("") ;
                reqcliente.setrecurso3("") ;
                reqcliente.setrecurso4("") ;
                reqcliente.setrecurso5("") ;
                reqcliente.setprioridad1("") ;
                reqcliente.setprioridad2("") ;
                reqcliente.setprioridad3("") ;
                reqcliente.setprioridad4("") ;
                reqcliente.setprioridad5("") ;
                reqcliente.setUsuario_creacion(user) ;
                reqcliente.setfecha_creacion("");
                reqcliente.setUsuario_actualizacion(user);
                reqcliente.settipo_asign("M");
                //reqcliente.getfecha_actualizacion();
                
            /*
             * Recorremos los registros a asignar
             */
                model.rdSvc.setAsignados(new Vector());
                
                for ( int i = 0; i < asignados.length; i++ ) {
                    
                    int numrec =Integer.parseInt(asignados[i]);
                    logger.info("Retornando recurso: "+request.getParameter("R"+numrec+"placa"));
                    
                /*
                 *Obtenemos el recurso.
                 */
                    String dstrct = request.getParameter("R"+numrec+"dstrct");
                    String placa = request.getParameter("R"+numrec+"placa");
                    String fecha_disp = request.getParameter("R"+numrec+"fecha_disp");
                    Recursosdisp recurso = model.regAsigService.obtenerRecursoDeRegistro(dstrct, placa, fecha_disp);
                    logger.info("Fecha de disponibilidad "+recurso.getaFecha_disp());
                    reqcliente.setfecha_dispo(""+recurso.getaFecha_disp()) ;
                    reqcliente.setfecha_creacion(""+recurso.getaFecha_disp()) ;
                /*
                 *Seteamos el valor del destino seleccionado en el origen del requerimiento
                 */
                    logger.info("Origen  "+recurso.getDestino());
                    reqcliente.setorigen(recurso.getDestino()) ;
                    
                /*
                 *Seteamos el valor del origen del recurso en el destino del requerimiento
                 */
                    logger.info("Destino "+request.getParameter("agencia_"+numrec));
                    
                    reqcliente.setdestino(request.getParameter("agencia_"+numrec)) ;
                    model.reqclienteServices.llenarReqcliente(reqcliente);
                    
                /*
                 *Se realiza la asignacion del recurso con el requerimiento creado.
                 */
                    model.rdSvc.realizarAsignacion(recurso, null, reqcliente, "M");
                    
                }
                Vector asignadosV = model.rdSvc.getAsignados();
                
                logger.info("Asignando "+asignadosV.size()+" registros...");
                
                for(int i=0; i<asignadosV.size(); i++){
                    model.regAsigService.guardarRegistroAsignacion((RegistroAsignacion)asignadosV.elementAt(i));
                }
                model.regAsigService.insertarRegistrosNoAsignados();
                model.ipredoSvc.LlenarAgasoc();
            }
            logger.info("Voy a Buscar");
            String tipo = "SINFECHA";
            if(request.getParameter("fecha").equalsIgnoreCase("Si")){
                tipo = "FECHAS";
            }
            logger.info("Tipo :"+tipo);
            logger.info("Agencia :"+request.getParameter("agencia"));
            logger.info("Fecha1 :"+request.getParameter("fechai"));
            logger.info("Fecha2 :"+request.getParameter("fechaf"));
            model.ipredoSvc.llenarInforme(request.getParameter("fechai"),request.getParameter("fechaf"),request.getParameter("agencia"),tipo);
            
            this.dispatchRequest("/reportePlaneacion/mostrarEditorDeReporte.jsp?mostrar=ok");
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
            throw new ServletException("Error en RetornarVacioAction:\n"+ex.getMessage());
        }
    }
}