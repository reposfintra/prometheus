/******************************************************************************
 * Nombre clase :                   CXP_DocAnularAction.java                  *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de Anular    *
 *                                  un Documento de Cuentas por Pagar Action  *
 * Autor :                          Leonardo Parody                           *
 * Fecha Creado :                   19 de octubre de 2005, 04:15 PM           *
 * Modificado por:                  LREALES                                   *
 * Fecha Modificado:                17 de mayo de 2006, 08:15 AM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
//logger
import org.apache.log4j.Logger;

public class FactRecurrAnularAction extends Action{
    
    Logger log = Logger.getLogger(this.getClass());
        
    /** Creates a new instance of CXP_DocAnularAction */
    public FactRecurrAnularAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario )session.getAttribute( "Usuario" );
        String user = usuario.getLogin().toUpperCase();
        String distrito = usuario.getDstrct().toUpperCase();
        String flag = request.getParameter( "flag" );
        String titulo="";
        CXP_Doc cXP_Docu = new CXP_Doc();
        CXP_Doc cXP_Docu2 = new CXP_Doc();
        CXP_Doc cXP_Docu3 = new CXP_Doc();
        
        boolean sw = false;
        boolean sw2 = false;
        
        String c_documento = ( request.getParameter( "c_documento" ) != null )?request.getParameter( "c_documento" ).toUpperCase() :"";
        String tipo_documento = ( request.getParameter( "tipo_documento" ) != null )?request.getParameter( "tipo_documento" ).toUpperCase() :"";
        String c_proveedor = ( request.getParameter( "c_proveedor" ) != null )?request.getParameter( "c_proveedor" ).toUpperCase() :"";
        String documento = ( request.getParameter( "documento" ) != null )?request.getParameter( "documento" ) :"";
        String descripcion = ( request.getParameter( "descripcion" ) != null )?request.getParameter( "descripcion" ) :"";
        String r = ( request.getParameter( "r" ) != null )?request.getParameter( "r" ) :"";
        
        String validar = "";
        
        try{
            
            String nombre = model.factrecurrService.SQL_Nombre_Proveedor( distrito, c_proveedor );
            
            if ( flag.equals( "mostrar" ) ){
                
                sw = model.factrecurrService.ExisteCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                
                if ( sw == true ){
                    
                    cXP_Docu = model.factrecurrService.ConsultarCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                                      
                    request.setAttribute( "CXP", cXP_Docu );
                           
                    if ( cXP_Docu.getCheque().equals( "" ) ){
                        
                        if ( cXP_Docu.getCorrida().equals( "" ) ){
                            
                            if ( cXP_Docu.getVlr_saldo() == 0 ){
                                
                                next = "/jsp/cxpagar/facturasrec/Mensaje.jsp?msg=La Factura no puede ser anulada! Por estar cancelada!!";
                            
                            } else if ( ( cXP_Docu.getVlr_saldo() < cXP_Docu.getVlr_neto() ) && ( cXP_Docu.getVlr_saldo() != 0 ) ){
                                
                                next = "/jsp/cxpagar/facturasrec/Mensaje.jsp?msg=La Factura no puede ser anulada! Por ser abonada!!";
                            
                            } else {
                                
                                if ( !cXP_Docu.getTipo_documento_rel().equals( "" ) && !cXP_Docu.getDocumento_relacionado().equals( "" ) ) {
                                    
                                    sw2 = model.factrecurrService.ExisteCXP_Doc( distrito, c_proveedor, cXP_Docu.getTipo_documento_rel(), cXP_Docu.getDocumento_relacionado() );
                                    
                                    if ( !sw2 ) {
                                        
                                        r = "false";
                                        next = "/jsp/cxpagar/facturasrec/anularfactura.jsp?flag=detalle";
                                        
                                    } else {
                                        
                                        r = "true";
                                        next = "/jsp/cxpagar/facturasrec/anularfactura.jsp?flag=detalle";
                                        
                                    }                                    
                                    
                                } else {
                                    
                                    r = "false";                                                                       
                                    next = "/jsp/cxpagar/facturasrec/anularfactura.jsp?flag=detalle";
                                    
                                }                                
                                
                            }
                            
                        } else {
                            
                            next = "/jsp/cxpagar/facturasrec/Mensaje.jsp?msg=La Factura no puede ser anulada! Por encontrarse en una corrida!!";
                        
                        }
                        
                    } else {
                        
                        next = "/jsp/cxpagar/facturasrec/Mensaje.jsp?msg=La Factura no puede ser anulada! Por poseer un cheque!!";
                    
                    }
                    
                } else{
                    
                    next = "/jsp/cxpagar/facturasrec/Mensaje.jsp?msg=La Factura Digitada NO EXISTE!!";
                    
                }
                
            } else if ( flag.equals( "anular" ) ){
                
                log.info("INSTRUCCION: ANULAR FATURA");
                
                if ( r.equals( "true" ) ) {
                    
                    cXP_Docu = model.factrecurrService.ConsultarCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                    cXP_Docu2 = model.factrecurrService.ConsultarCXP_Doc( distrito, c_proveedor, cXP_Docu.getTipo_documento_rel(), cXP_Docu.getDocumento_relacionado() );
                    
                    //NOTA DEBITO
                    if ( cXP_Docu.getTipo_documento().equals( "036" ) ){
                        
                        cXP_Docu2.setVlr_total_abonos( cXP_Docu2.getVlr_total_abonos() - cXP_Docu.getVlr_neto() );
                        cXP_Docu2.setVlr_saldo( cXP_Docu2.getVlr_saldo() + cXP_Docu.getVlr_neto() );
                        
                        cXP_Docu2.setVlr_total_abonos_me( cXP_Docu2.getVlr_total_abonos_me() - cXP_Docu.getVlr_neto_me() );
                        cXP_Docu2.setVlr_saldo_me( cXP_Docu2.getVlr_saldo_me() + cXP_Docu.getVlr_neto_me() );
                        
                        validar = "anular";
                        
                    } 
                    //NOTA CREDITO
                    else if ( cXP_Docu.getTipo_documento().equals( "035" ) ){
                        
                        cXP_Docu2.setVlr_total_abonos( cXP_Docu2.getVlr_total_abonos() + cXP_Docu.getVlr_neto() );
                        cXP_Docu2.setVlr_saldo( cXP_Docu2.getVlr_saldo() - cXP_Docu.getVlr_neto() );
                        
                        cXP_Docu2.setVlr_total_abonos_me( cXP_Docu2.getVlr_total_abonos_me() + cXP_Docu.getVlr_neto_me() );
                        cXP_Docu2.setVlr_saldo_me( cXP_Docu2.getVlr_saldo_me() - cXP_Docu.getVlr_neto_me() );
                        
                        validar = "anular";
                        
                    }
                    //FACTURA NORMAL
                    else{
                                                
                        validar = "anular";
                    
                    }
                    
                } else if ( r.equals( "false" ) ) {      
                    
                    validar = "anular";
                                    
                } else{
                    
                    next = "/jsp/cxpagar/facturasrec/Mensaje.jsp?msg=Error en el proceso de anulacion de la factura!"; 
                
                }
                
                if ( validar.equals( "anular" ) ){
                    
                    cXP_Docu3.setTipo_documento( tipo_documento );
                    cXP_Docu3.setDocumento( c_documento );
                    cXP_Docu3.setProveedor( c_proveedor );
                    cXP_Docu3.setDstrct( distrito );   
                    cXP_Docu3.setUser_update( user );

                    model.factrecurrService.AnularCXP_Doc( cXP_Docu3 );

                    next = "/jsp/cxpagar/facturasrec/Mensaje.jsp?msg=La Factura Fue Anulada Exitosamente!";
                
                }
                
            }
            
            session.setAttribute ( "nombre", nombre );
            session.setAttribute ( "r", r );
            
        } catch ( Exception e ){
            
            throw new ServletException( e.getMessage() );
            
        }
        
        this.dispatchRequest( next );
        
     }
    
}