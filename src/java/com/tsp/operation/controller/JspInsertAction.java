/********************************************************************
 *      Nombre Clase.................   JspInsertAction.java    
 *      Descripci�n..................   Inserta un campo en el archivo de campos_jsp.
 *      Autor........................   Ing. Rodrigo Salazar
 *      Fecha........................   10.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import org.apache.log4j.*;
/**
 *
 * @author  rodrigo
 */


public class JspInsertAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of JspInsertAction */
    public JspInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/permisos/jsp/JspInsertar.jsp?mensaje=Pagina ya existe";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");      
        String codigo = request.getParameter("c_codigo");
        String pagina = request.getParameter("c_pagina");
        String ruta = request.getParameter("c_ruta");
        String descripcion = request.getParameter("c_descripcion"); 
        //String unidad = request.getParameter("c_unidad");
        try{
            ////System.out.println(codigo);            
            Jsp jsp = new Jsp();
            jsp.setDescripcion(descripcion);
            jsp.setCodigo(codigo);
            jsp.setNombre(pagina);
            jsp.setRuta(ruta);
            jsp.setCia(usuario.getCia().toUpperCase());
            jsp.setUser_update(usuario.getLogin().toUpperCase());
            jsp.setCreation_user(usuario.getLogin().toUpperCase());
            ////System.out.println("Action"+jsp.getDescripcion());
            
            String reg_status = model.jspService.existeJsp(jsp.getCodigo());
            
            if( reg_status == null  ){
                model.jspService.insertJsp(jsp);
                next = "/jsp/trafico/permisos/jsp/JspInsertar.jsp?mensaje=Pagina guardada";
            } else if ( reg_status.equals("A") ){
                model.jspService.modificarJsp(jsp);
                next = "/jsp/trafico/permisos/jsp/JspInsertar.jsp?mensaje=Pagina guardada";
            }
            
            
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
