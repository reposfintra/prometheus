/*
 * CaravanaBisquedaAction.java
 *
 * Created on 04 de Agosto de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

public class CaravanaBusquedaAction extends Action{
    
    
    static Logger logger = Logger.getLogger(CaravanaBusquedaAction.class);
    
    public void run() throws ServletException {
        String error = "";
        logger.info("CARAVANA CTRL TRAFICO: " + request.getParameter("caravana"));
        String next = "/jsp/trafico/caravana/caravana.jsp?reload=&agregar=visible";
        String next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?reload=&agregar=visible";
        String numC = (request.getParameter("numC")!=null)?request.getParameter("numC"):"";
        String next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?reload=&agregar=visible&escolta=visible&first=2&caravana="+numC;
        String  numpla = (request.getParameter("planilla")!=null)?request.getParameter("planilla").toUpperCase():"";
        String checkbox = "";
        String err = "";
        String caravana = "";
        String pag = (request.getParameter("pag")!=null)?request.getParameter("pag"):"";
        
        String plamanual = request.getParameter("plamanual")==null ? "" : request.getParameter("plamanual");
        logger.info("NumPla: " + numpla + " ::: PlaManual: " + plamanual);
        //Modificacion 16 ene 2006
        if (pag.equals("trafico")) {
            next = "/jsp/trafico/caravana/caravana.jsp?reload=&agregar=visible";
            if (model.caravanaSVC.getVectorCaravanaActual()!=null){
                if(model.caravanaSVC.getVectorCaravanaActual().size()>0) {
                    model.caravanaSVC.limpiarVectorActualPlanillas();
                }
            }
        }
        try{
            /* Andr�s Maturana */
            caravana = (request.getParameter("caravana")==null ? "" : request.getParameter("caravana"));
            if( caravana.trim().length()!=0 && !caravana.equals("0")){//AMATURANA
                logger.info("GO MODIFICAR CARAVANA: " + caravana);
                String fecF = model.caravanaSVC.getFechaFinCaravana(Integer.parseInt(caravana));
                String next4 = "/jsp/trafico/5a/modificarCaravana.jsp?first=1&fecF=" + fecF + "&caravana=" + caravana;
                this.dispatchRequest(next4);
            } else {
                
                logger.info("Planilla Manual: Existe::>" +  (model.caravanaSVC.existePlanillaManual(numpla)) + " - PlaManual::> " + (plamanual.compareTo("true")==0) );
                logger.info("Planilla Normal: Existe::>" +  (model.caravanaSVC.existaPlanilla(numpla)) + " - PlaManual::> " + (plamanual.compareTo("false")==0) );
                if (!numpla.equals("")){
                    /*validacion de existencia para asignacion escolta*/                        
                    boolean isAsignada = false;
                    if (pag.equals("escolta"))
                        isAsignada = model.caravanaSVC.existaPlanillaCaravana(numpla);
                    else
                        isAsignada = model.caravanaSVC.existaPlanillaEnCaravana(numpla);
                    if(model.caravanaSVC.existaPlanilla(numpla) && plamanual.compareTo("true")!=0 ){                        
                        if (!isAsignada){
                            model.caravanaSVC.getVehiculosXPlanilla(numpla);
                            VistaCaravana vc = model.caravanaSVC.getVehiculoPlanilla();
                            if (!model.caravanaSVC.existaVehiculoCaravana(vc)) {
                                //Cargando los cvectores de las rutas
                                logger.info("Ruta de la planilla: "+vc.getVia());
                                model.caravanaSVC.cargarVectorPCXRutaPla(vc.getRuta_pla());
                                if(model.caravanaSVC.getVectorCaravanaActual().size()>0) {
                                    Vector vec = model.caravanaSVC.getVectorCaravanaActual();
                                    int posicion_menor = 0;
                                    int tama�o_via = 1000;
                                    for(int i=0; i< vec.size(); i++){
                                        VistaCaravana car = (VistaCaravana) vec.elementAt( i );
                                        if( tama�o_via > car.getVia().length() ){
                                            tama�o_via = car.getVia().length();
                                            posicion_menor = i;
                                        }
                                    }
                                    VistaCaravana vis = (VistaCaravana) vec.elementAt( posicion_menor );
                                    
                                    //  if(model.caravanaSVC.validarOrigenDestino( vc, vis.getVia()) ){
                                    if (pag.equals("mod"))
                                        vc.setSeleccionado(true);
                                    model.caravanaSVC.agregarVectorCaravanaActual(vc);
                                    //} else
                                    //  err = "&msg=RUTER";
                                }
                                else{
                                    if (pag.equals("mod"))
                                        vc.setSeleccionado(true);
                                    model.caravanaSVC.agregarVectorCaravanaActual(vc);
                                }
                            }
                            next = "/jsp/trafico/caravana/caravana.jsp?agregar=visible"+err;
                            next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?agregar=visible"+err;
                            next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?agregar=visible&escolta=visible&first=mod&caravana="+numC+err;
                        } else  {
                            next = "/jsp/trafico/caravana/caravana.jsp?msg=ERRORE";
                            next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?msg=ERRORE";
                            next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?msg=ERRORE&agregar=visible&escolta=visible&first=2&caravana="+numC;
                        }
                    } else if ( model.caravanaSVC.existePlanillaManual(numpla) && plamanual.compareTo("true")==0 ) {
                        /* ANDRES MATURANA */
                        logger.info("................ CONGRATS ES PLA MANUAL");
                        if (!isAsignada){
                            model.caravanaSVC.getVehiculoXPlanillaManual(numpla);
                            VistaCaravana vc = model.caravanaSVC.getVehiculoPlanilla();
                            if (!model.caravanaSVC.existaVehiculoCaravana(vc)) {
                                //Cargando los cvectores de las rutas
                                logger.info("Ruta de la planilla: "+vc.getVia());
                                model.caravanaSVC.cargarVectorPCXRutaPla(vc.getRuta_pla());
                                if(model.caravanaSVC.getVectorCaravanaActual().size()>0) {
                                    Vector vec = model.caravanaSVC.getVectorCaravanaActual();
                                    int posicion_menor = 0;
                                    int tama�o_via = 1000;
                                    for(int i=0; i< vec.size(); i++){
                                        VistaCaravana car = (VistaCaravana) vec.elementAt( i );
                                        if( tama�o_via > car.getVia().length() ){
                                            tama�o_via = car.getVia().length();
                                            posicion_menor = i;
                                        }
                                    }
                                    VistaCaravana vis = (VistaCaravana) vec.elementAt( posicion_menor );
                                    
                                    //  if(model.caravanaSVC.validarOrigenDestino( vc, vis.getVia()) ){
                                    if (pag.equals("mod"))
                                        vc.setSeleccionado(true);
                                    model.caravanaSVC.agregarVectorCaravanaActual(vc);
                                    //} else
                                    //  err = "&msg=RUTER";
                                }
                                else{
                                    if (pag.equals("mod"))
                                        vc.setSeleccionado(true);
                                    model.caravanaSVC.agregarVectorCaravanaActual(vc);
                                }
                            }
                            next = "/jsp/trafico/caravana/caravana.jsp?agregar=visible"+err;
                            next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?agregar=visible"+err;
                            next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?agregar=visible&escolta=visible&first=mod&caravana="+numC+err;
                        } else  {
                            next = "/jsp/trafico/caravana/caravana.jsp?msg=ERRORE";
                            next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?msg=ERRORE";
                            next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?msg=ERRORE&agregar=visible&escolta=visible&first=2&caravana="+numC;
                        }
                    } else {
                        next = "/jsp/trafico/caravana/caravana.jsp?msg=ERRORB";
                        next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?msg=ERRORB";
                        next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?msg=ERRORB&agregar=visible&escolta=visible&first=2&caravana="+numC;
                        if (model.caravanaSVC.getVectorCaravanaActual().size()>0) {
                            next = "/jsp/trafico/caravana/caravana.jsp?msg=ERRORB&agregar=visible";
                            next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?msg=ERRORB&agregar=visible&escolta=visible&first=2&caravana="+numC;
                            next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?msg=ERRORE";
                        }
                    }
                } else {
                    boolean esprimero = false;
                    int posprimero = 0;
                    Vector datos = model.caravanaSVC.getVectorVehiculos();
                    for (int i=0; i<datos.size(); i++){
                        checkbox = (request.getParameter("fila"+i)!=null)?request.getParameter("fila"+i):"";
                        //System.out.println("CHECKBOX "+i+":"+checkbox);
                        if (!checkbox.equals("")){
                            VistaCaravana vc = (VistaCaravana)datos.elementAt(Integer.parseInt(checkbox));
                            //model.caravanaSVC.modificarVectorVehiculos(Integer.parseInt(checkbox),true);
                            if(!esprimero){
                                posprimero = Integer.parseInt(checkbox);
                                esprimero = true;
                            }
                            vc.setSeleccionado(true);
                            if (!model.caravanaSVC.existaVehiculoCaravana(vc))
                                if(model.caravanaSVC.getVectorCaravanaActual().size()>0) {
                                    Vector vec = model.caravanaSVC.getVectorCaravanaActual();
                                    VistaCaravana vis = (VistaCaravana) datos.elementAt( posprimero );
                                    if(model.caravanaSVC.validarOrigenDestino( vc, vis.getVia()) ){
                                        if (pag.equals("mod"))
                                            vc.setSeleccionado(true);
                                        model.caravanaSVC.agregarVectorCaravanaActual(vc);
                                    }else {
                                        next = "/jsp/trafico/caravana/caravana.jsp?msg=RUTER&agregar=visible";
                                        next3 = "/jsp/trafico/caravana/modificarCaravana.jsp?msg=RUTER&agregar=visible&escolta=visible&first=2&caravana="+numC;
                                    }
                                }
                                else{
                                    if (pag.equals("mod"))
                                        vc.setSeleccionado(true);
                                    model.caravanaSVC.agregarVectorCaravanaActual(vc);
                                }
                        }
                    }
                }
                if (model.caravanaSVC.getVectorCaravanaActual().size()>1 && pag.equals("escolta")) {
                    error = "&error=MP1";
                    model.caravanaSVC.borrarPosicionMayorUnoVectorActualPlanillas();
                }
                if (model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual().size()>1 && pag.equals("escolta")) {
                    error = "&error=ME1";
                    model.escoltaVehiculoSVC.borrarPosicionMayorUnoVectorEscoltaActual();
                }
            }
            //numC
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        ////System.out.println("PAGINA :"+pag+"NEXT3:"+next3);
        logger.info("NEXT2 : " + (next2+error));
        logger.info("NEXT2 : " + (next2+error));
        if (pag.equals("escolta") && caravana.equals("0"))
            this.dispatchRequest("/jsp/trafico/caravana/asignarEscolta.jsp?msg="+error);
        else if (pag.equals("escolta"))
            this.dispatchRequest(next2+error);
        else if (pag.equals("mod"))
            this.dispatchRequest(next3);
        else if (!pag.equals("mod") && !pag.equals("escolta"))
            this.dispatchRequest(next);
    }
}
