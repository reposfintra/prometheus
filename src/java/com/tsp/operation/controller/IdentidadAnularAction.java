/*************************************************************************
 * Nombre:        IdentidadAnularAction.java            
 * Descripci�n:   Clase Action para anular Identidad    
 * Autor:         Ing. Diogenes Antonio Bastidas Morales  
 * Fecha:         20 de julio de 2005, 09:06 PM                             
 * Versi�n        1.0                                      
 * Coyright:      Transportes Sanchez Polo S.A.            
 *************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class IdentidadAnularAction extends Action{
    
    /** Creates a new instance of IdentidadAnularAction */
    public IdentidadAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        String ced = request.getParameter("cedula");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
     
        try{
            Identidad ident = new Identidad(); 
            ident.setCedula(ced);            
            ident.setUsuario(usuario.getLogin());         
            model.identidadService.anularIdentidad(ident);
        }
        catch (Exception e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);

    }
    
}
