/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AutorizarFleteAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public AutorizarFleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/colpapel/autorizarCambioFlete.jsp";
        String planilla = request.getParameter("numpla");
        /*try{
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }*/
        this.dispatchRequest(next);
        
        
    }
    
    
}
