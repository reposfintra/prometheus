/*
 * ChequeNoImpAnular.java
 *
 * Created on 22 de junio de 2005, 06:14 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIOGENES
 */
public class ChequenoimpAnularAction extends Action  {
    
    /** Creates a new instance of ChequeNoImpAnular */
    public ChequenoimpAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/cheques/mostrarChequeNoImpre.jsp";
        String planilla = request.getParameter("planilla");
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            if ( ! model.movplaService.AnticipoAnulado(planilla) ){
                model.movplaService.BuscarChequeNoImpreso(planilla);
                if(model.movplaService.getMovPla()==null){
                    next = "/cheques/buscarChequeNoImpre.jsp?estado=no";                
                }
            }
            else
                next = "/cheques/buscarChequeNoImpre.jsp?estado=anu";                
            
             
            if( request.getParameter("anular")!=null){
                Movpla mp = model.movplaService.getMovPla();
                mp.setCreation_user(usuario.getLogin());
                model.movplaService.setMovPla(mp);
                model.movplaService.anularChequeNoImpreso();              
                next = "/cheques/buscarChequeNoImpre.jsp?estado=ok";
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
