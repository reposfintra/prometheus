/*
 * InsertarZonaAction.java
 *
 * Created on 13 de junio de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;

public class InsertarZonaAction extends Action{
    
    /** Creates a new instance of InsertarZonaAction */
    public InsertarZonaAction() {
    }
    
    public void run() throws ServletException {
        String codigo = (request.getParameter("codigo").toUpperCase());
        String nombre = (request.getParameter("nombre").toUpperCase());
        String next = "/jsp/trafico/zona/zona.jsp?mensaje=";        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        try{ 
            if (model.zonaService.existeZona(codigo)){
                next = "/jsp/trafico/zona/zona.jsp?mensaje=ErrorA";
            }
            else{
                Zona zona = new Zona();
                zona.setCodZona(codigo);
                zona.setNomZona(nombre);     
                zona.setBase(usuario.getBase());
                zona.setCreation_user(usuario.getLogin());
                model.zonaService.insertarZona(zona);       
                next = next+"Agregado";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
