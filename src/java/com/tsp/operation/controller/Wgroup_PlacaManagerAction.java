/***************************************************************************
 * Nombre clase :                 Wgroup_PlacaManagerAction.java           *
 * Descripcion :                  Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                mantenimiento de la tabla wgroup_placa   *
 * Autor :                        Ing. Juan Manuel Escandon Perez          *
 * Fecha :                        13 de enero de 2006, 04:24 PM            *
 * Version :                      1.0                                      *
 * Copyright :                    Fintravalores S.A.                  *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
public class Wgroup_PlacaManagerAction extends Action{
    private static String wg;
    private static String pl;
    /** Creates a new instance of Wgroup_PlacaManagerAction */
    public Wgroup_PlacaManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String wgroup                 = (request.getParameter("wgroup")!=null)?request.getParameter("wgroup"):"";
            String placa                  = (request.getParameter("placa")!=null)?request.getParameter("placa"):"";
            String original               = (request.getParameter("original")!=null)?request.getParameter("original"):"";
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String dstrct                 = user.getDstrct();
            
            String []LOV                  = request.getParameterValues("LOV");
            
            String Mensaje = "";
            
            if (Opcion.equals("Guardar")){
                model.placaService.buscaPlaca(placa);
                if(model.placaService.getPlaca()!=null){
                    if(!model.wgroup_placaSvc.Existe(wgroup, placa)){
                        model.wgroup_placaSvc.Insert(wgroup, placa, usuario, dstrct);
                        Mensaje = "El Registro ha sido agregado";
                    }
                    else{
                        Mensaje = "El Registro ya existe";
                    }
                }
                else{
                    Mensaje = "La placa no existe";
                }
            }
            
            if (Opcion.equals("Modificar")){
                this.Conversion(original);
                model.wgroup_placaSvc.Update(wgroup, placa, wg, pl);
                model.wgroup_placaSvc.ReiniciarDato();
                Mensaje = "El Registro ha sido modificado";
            }
            
            if (Opcion.equals("Nuevo"))
                model.wgroup_placaSvc.ReiniciarDato();
            
            if (Opcion.equals("Ocultar Lista")){
                model.wgroup_placaSvc.ReiniciarLista();
            }
            
            if (Opcion.equals("Anular")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.wgroup_placaSvc.Status("A", wg, pl);
                }
                Mensaje = "El Registro ha sido anulado";
            }
            
            if (Opcion.equals("Activar")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.wgroup_placaSvc.Status("", wg, pl);
                }
                Mensaje = "El Registro ha sido activado ";
            }
            
            if (Opcion.equals("Eliminar")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.wgroup_placaSvc.Delete( wg, pl);
                }
                Mensaje = "El Registro ha sido eliminado ";
            }
            
            if(Opcion.equals("Seleccionar")){
                this.Conversion(original);
                model.wgroup_placaSvc.Search(wg, pl);
            }
            
            if (Opcion.equals("Listado")){
                model.wgroup_placaSvc.List();
                if( model.wgroup_placaSvc.getList().size() == 0 )
                    Mensaje = "No hay registros en la tabla";
            }
            
            if ("Listado|Modificar|Guardar|Eliminar|Anular|Activar".indexOf(Opcion) != -1) {
                model.wgroup_placaSvc.List();
            }
            
            String next ="/jsp/masivo/workgroup_placa/wgroup_placa.jsp?Mensaje="+Mensaje;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en Wgroup_PlacaManagerAction .....\n"+e.getMessage());
        }
    }
    
    public static void Conversion(String f){
        String vF [] = f.split(",");
        wg = vF[0];
        pl = vF[1];
    }
    
}
