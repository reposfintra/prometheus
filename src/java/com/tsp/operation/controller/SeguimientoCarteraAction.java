package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.ReporteSanciones;
import com.tsp.operation.model.beans.SeguimientoCarteraBeans;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.SeguimientoCarteraService;
import com.tsp.util.Utility;
import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


public class SeguimientoCarteraAction extends Action{
    
    SeguimientoCarteraService rqs;
    HttpSession session;
    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String   rutaInformes;
    String   nombre;
    String iduser;
    String dbName;
   
    public  SeguimientoCarteraAction(){}
    
    public void run() throws ServletException, InformationException {

        String evento = "";
        String respuesta = "";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //String user = (String) session.getAttribute("Usuario");
        //String Dstrct = (String) session.getAttribute("Distrito");
        
        String dstrct = usuario.getDstrct();
        iduser = usuario.getLogin();
        dbName = usuario.getBd();
        rqs = new SeguimientoCarteraService(usuario.getBd());
        //String control = (request.getParameter("control") != null) ? request.getParameter("control") : "";
        
        try{
            
            evento = request.getParameter("evento");
            
            int proceso = Integer.parseInt(request.getParameter("proceso") != null ? request.getParameter("proceso") : "0");
            int tiporeq = Integer.parseInt(request.getParameter("tiporeq") != null ? request.getParameter("tiporeq") : "0");
            String asunto = request.getParameter("asunto") != null ? request.getParameter("asunto") : "";
            String descripcion = request.getParameter("descripcion") != null ? request.getParameter("descripcion") : "";

            //response.getWriter().println(evento);
            
            if ( evento.equals("INSERT") ) {
                
                try{
                    
                    //respuesta =  rqs.InsertarRequisicion(dstrct, tiporeq, proceso, iduser,asunto, descripcion);
                    //response.getWriter().println(respuesta);
                    
                }catch(Exception e){
                    
                    e.printStackTrace();
                    
                }             
                
            }else if ( evento.equals("DELETE") ) {
                
            }else if ( evento.equals("UPDATE") ) {    

                try{
                    
                    //respuesta =  rqs.ActualizarRequisicion(dstrct, tiporeq, proceso, iduser, asunto, descripcion);
                    //response.getWriter().println(respuesta);
                    
                }catch(Exception e){
                    
                    e.printStackTrace();
                    
                }             
                
            } else if (evento.equals("detallefac")) {
                //aqui metemos lo nuevo..
             
                String unidad_negocio=request.getParameter("unid_negocio");
                int periodo_foto =Integer.parseInt(request.getParameter("periodo"));
                String negocio=request.getParameter("negocio");
                String cuota=request.getParameter("cuota");
                listarDetallesCartera(unidad_negocio, periodo_foto, negocio,cuota);
            
            } else if (evento.equals("detallepago")){
               
                int periodo_foto =Integer.parseInt(request.getParameter("periodo"));
                String negocio=request.getParameter("negocio");
                String num_ingreso=request.getParameter("num_ingreso");
                listarDetallesPagos(periodo_foto, negocio, num_ingreso);
         
            } else if (evento.equals("detallepagotodo")){
               
                int periodo_foto =Integer.parseInt(request.getParameter("periodo"));
                String negocio=request.getParameter("negocio");
                String num_ingreso=request.getParameter("num_ingreso");
                listarDetallesPagosTodos(periodo_foto, negocio, num_ingreso);
            
            } else if (evento.equals("exportarTodo")){
                String periodo = request.getParameter("periodo");
                String undNeg = request.getParameter("undNeg");
                boolean generado = false;
                String url = "";
                String resp = "";
                ArrayList<SeguimientoCarteraBeans> lista = null;
                //SeguimientoCarteraService rqservice = new SeguimientoCarteraService();
                try {
                    this.generarRUTA();
                    this.crearLibro("DetalleCarteraUnidadNegocio_", "DETALLE CARTERA POR UNIDAD DE NEGOCIO");

                    // encabezado
                    String[] cabecera;
                    short[] dimensiones;
                    if(undNeg.equals("22")){
                        cabecera = new String[]{
                             "UND NEGOCIO", "PERIODO FOTO", "CEDULA", "NOMBRE CLIENTE", "PAGADURIA", "DIRECCION", "BARRIO", "CIUDAD", "TELEFONO", "CELULAR", "NEGOCIO", "CONVENIO", "CUOTA", "VR. ASIGNADO", "FECHA VENCIMIENTO", "PERIODO VCTO", "VENCIMIENTO MAYOR", "DIAS VENCIDOS", "DIA PAGO", "STATUS", "STATUS VENCIMIENTO", "DEBIDO COBRAR", "RECAUDOS FIDUCIA", "RECAUDOS FENALCO", "RECAUDOS", "AGENTE"
                        };

                        dimensiones = new short[]{
                             6000, 4000, 5000, 10000, 8000, 10000, 8000, 3000, 5000, 4000, 4000, 3000, 3000, 4000, 4000, 4000, 5000, 4000, 4000, 3000, 4000, 4000, 5000, 5000, 5000, 5000, 5000
                        };
                    }else{
                        cabecera = new String[]{
                            "UND NEGOCIO", "PERIODO FOTO", "CEDULA", "NOMBRE CLIENTE", "DIRECCION", "BARRIO", "CIUDAD", "TELEFONO", "CELULAR", "NEGOCIO", "CONVENIO", "CUOTA", "VR. ASIGNADO", "FECHA VENCIMIENTO", "PERIODO VCTO", "VENCIMIENTO MAYOR", "DIAS VENCIDOS", "DIA PAGO", "STATUS", "STATUS VENCIMIENTO", "DEBIDO COBRAR", "RECAUDOS FIDUCIA", "RECAUDOS FENALCO", "RECAUDOS", "AGENTE"
                        };

                        dimensiones = new short[]{
                            6000, 4000, 5000, 10000, 10000, 8000, 3000, 5000, 4000, 4000, 3000, 3000, 4000, 4000, 4000, 5000, 4000, 4000, 3000, 4000, 4000, 5000, 5000, 5000, 5000, 5000
                        };
                    }
                    this.generaTitulos(cabecera, dimensiones);
                    lista = rqs.obtenerDetalleCarteraUndNegocio(periodo,undNeg,iduser);
                    generado = generaArchivoCartera(lista);
                    this.cerrarArchivo();
                    if (generado) {
                        fmt = new SimpleDateFormat("yyyMMdd");
                        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/DetalleCarteraUnidadNegocio_" + fmt.format(new Date()) + ".xls";
                        resp = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
                    }
                    this.printlnResponse(resp, "text/plain");

                } catch (Exception e) {
                    e.printStackTrace();
                    throw new SQLException("ERROR DURANTE LA CREACION DEL REPORTE DE CARTERA. \n " + e.getMessage());
                }
            } else if(evento.equals("exportarTabla")){
                String unidad_negocio = request.getParameter("unidad_negocio");
                String periodo_foto = request.getParameter("periodo_foto");
                String aldia = request.getParameter("aldia");
                String avencer = request.getParameter("avencer");
                String vencido = request.getParameter("vencido");
                String SuperHaving = request.getParameter("SuperHaving");
                String StatusVcto = request.getParameter("StatusVcto");
                String DayMonth = request.getParameter("DayMonth");
                String UserLogin = request.getParameter("UserLogin");
                String Query = "SQL_CARGAR_SEGUIMIENTO_CARTERA";
                String item = "";
                boolean generado = false;
                String url = "";
                String resp1 = "";
                ArrayList<SeguimientoCarteraBeans> lista = null;
                //SeguimientoCarteraService rqservice = new SeguimientoCarteraService();
                try {
                    this.generarRUTA();
                    this.crearLibro("ReporteSeguimientoCartera_", "REPORTE SEGUIMIENTO CARTERA");

                    // encabezado      
                    String[] cabecera;
                    short[] dimensiones;
                    if(unidad_negocio.equals("22")){
                        cabecera = new String[] {
                            "CEDULA", "NOMBRE CLIENTE", "PAGADURIA", "DIRECCION", "BARRIO", "CIUDAD", "TELEFONO", "CELULAR", "NEGOCIO", "CONVENIO", "VENCIMIENTO MAYOR", "DIA PAGO", "VALOR SALDO", "DEBIDO COBRAR", "RECAUDOS", "% CUMPLIMIENTO", "VALOR A PAGAR", "FECHA ULT. COMPROMISO", "REESTRUCTURACION", "JURIDICA"
                        };

                        dimensiones = new short[]{
                            4000, 10000, 8000, 10000, 8000, 4000, 5000, 5000, 3000, 3000, 6000, 3000, 4000, 4000, 4000, 4000, 4000, 7000, 5000, 5000
                        };
                    }else{
                        cabecera = new String[]{
                            "CEDULA","NOMBRE CLIENTE","DIRECCION", "BARRIO", "CIUDAD", "TELEFONO", "CELULAR", "NEGOCIO", "CONVENIO","VENCIMIENTO MAYOR","DIA PAGO","VALOR SALDO", "DEBIDO COBRAR", "RECAUDOS", "% CUMPLIMIENTO","VALOR A PAGAR", "FECHA ULT. COMPROMISO","REESTRUCTURACION", "JURIDICA"
                        };

                        dimensiones = new short[]{
                            4000, 10000, 10000, 8000, 4000, 5000, 5000, 3000, 3000, 6000, 3000, 4000, 4000, 4000, 4000, 4000,7000, 5000, 5000
                        };
                    }
                    this.generaTitulos(cabecera, dimensiones);
                    lista = rqs.SCarteraCargarSeguimientoBeans(Query, unidad_negocio, Integer.parseInt(periodo_foto), aldia, avencer, vencido, SuperHaving, StatusVcto, item, UserLogin, DayMonth);
                    generado = generaArchivoTabla(lista);
                    this.cerrarArchivo();
                    if (generado) {
                        fmt = new SimpleDateFormat("yyyMMdd");
                        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ReporteSeguimientoCartera_" + fmt.format(new Date()) + ".xls";
                        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
                    }
                    this.printlnResponse(resp1, "text/plain");

                } catch (Exception e) {
                    e.printStackTrace();
                    throw new SQLException("ERROR DURANTE EXPORTACION DE LA TABLA DE CARTERA. \n " + e.getMessage());
                }
            
            }else if(evento.equals("generarSancionesFenalco")){
                String[] periodo =  request.getParameter("periodo").split(",");
                String[] und_negocio = request.getParameter("uneg").split(",");
                ArrayList<ReporteSanciones> lista = new  ArrayList<ReporteSanciones>();
                for(int i=0; i < periodo.length; i++){
                    for(int j=0; j< und_negocio.length; j++){
                        lista.addAll(rqs.generarSancionesFenalco(periodo[i],und_negocio[j]));
                    }
                }
                Gson gson = new Gson();
                String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
                this.printlnResponse(json, "application/json;");
            }else if(evento.equals("generarSancionesMicro")){
                String[] periodo =  request.getParameter("periodo").split(",");
                String und_negocio = request.getParameter("uneg");
                ArrayList<ReporteSanciones> lista = new  ArrayList<ReporteSanciones>();
                for(int i=0; i < periodo.length; i++){
                    lista.addAll(rqs.generarSancionesMicro(periodo[i],und_negocio));
                }
                Gson gson = new Gson();
                String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
                this.printlnResponse(json, "application/json;");   
            }else if(evento.equals("exportarSanciones")){
                String[] periodo =  request.getParameter("periodo").split(",");
                String[] und_negocio = request.getParameter("uneg").split(",");
                ArrayList<ReporteSanciones> lista = new  ArrayList<ReporteSanciones>();
                boolean generado = false;
                String url = "";
                String resp1 = "";
                try {
                    this.generarRUTA();
                    this.crearLibro("ReporteSanciones_", "REPORTE DE SANCIONES");
                    
                    if (und_negocio[0].equals("1")) {
                        String[] cabecera = {
                            "UNIDAD NEGOCIO", "PERIODO", "COD NEGOCIO", "CEDULA", "NOMBRE CLIENTE", "VENCIMIENTO MAYOR", "FECHA PAGO INGRESO", "FECHA VENC. MAYOR", "DIFERENCIA PAGO", "VR. SALDO FOTO",
                            "INTERES MORA", "GASTO COBRANZA", "NUMERO INGRESO", "VR. INGRESO", "VR. CXC INGRESO", "GI010130014205", "GI010010014205", "I010130024170", "I010010014170", "VR. INTERES X MORA INGRESO", "VR. GASTO COBRANZA INGRESO"
                        };
                        short[] dimensiones = new short[]{
                            6000, 4000, 4000, 5000, 9000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
                        };
                        this.generaTitulos(cabecera, dimensiones);
                        for (int i = 0; i < periodo.length; i++) {
                            for (int j = 0; j < und_negocio.length; j++) {
                                lista.addAll(rqs.generarSancionesMicro(periodo[i], und_negocio[j]));
                            }
                        }
                        generado = generaArchivoSancionesMicro(lista);
                    } else {
                        String[] cabecera = {
                            "UNIDAD NEGOCIO", "PERIODO", "COD NEGOCIO", "CEDULA", "NOMBRE CLIENTE", "VENCIMIENTO MAYOR", "FECHA PAGO INGRESO", "FECHA VENC. MAYOR", "DIFERENCIA PAGO", "VR. SALDO FOTO",
                            "INTERES MORA", "GASTO COBRANZA", "NUMERO INGRESO", "VR. INGRESO", "VR. CXC INGRESO", "G16252145", "G94350302", "GI010010014205", "I16252147", "I94350301", "II010010014170", "VR. INTERES X MORA INGRESO", "VR. GASTO COBRANZA INGRESO"
                        };
                        short[] dimensiones = new short[]{
                            6000, 4000, 4000, 5000, 9000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
                        };
                        this.generaTitulos(cabecera, dimensiones);
                        for (int i = 0; i < periodo.length; i++) {
                            for (int j = 0; j < und_negocio.length; j++) {
                                lista.addAll(rqs.generarSancionesFenalco(periodo[i], und_negocio[j]));
                            }
                        }
                        generado = generaArchivoSancionesFenalco(lista);
                    }
                    
                    
                    

                    this.cerrarArchivo();
                    if (generado) {
                        fmt = new SimpleDateFormat("yyyMMdd");
                        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ReporteSanciones_" + fmt.format(new Date()) + ".xls";
                        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
                    }
                    this.printlnResponse(resp1, "text/plain");
                }catch (Exception e) {
                    e.printStackTrace();
                    throw new SQLException("ERROR DURANTE EXPORTACION DE SANCIONES. \n " + e.getMessage());
                }
            }else if(evento.equals("cargarAgentesReporte")){               
                cargarAgentesReporte();
            }else if(evento.equals("verCompromisos")){               
                cargarCompromisosPago();
            }else if(evento.equals("exportarCompromisos")){
                exportarExcelCompromisos();
            }else if(evento.equals("cargarAsesores")){               
                cargarAsesoresComerciales();
            }else if (evento.equals("cargarEstadosRecibo")) {
                cargarEstadosRecibo(false);
            }else if (evento.equals("cargarEstadosReciboAll")) {
                cargarEstadosRecibo(true);
            }else if (evento.equals("cargarTiposRecaudo")) {
                cargarTiposRecaudo();
            }else if (evento.equals("getNextRCNumber")) {
                obtenerNextRCNumber();
            }else if (evento.equals("guardarRC")) {
                guardarReciboCaja();
            }else if (evento.equals("anularRC")) {
                anularReciboCaja();
            }else if (evento.equals("cargarInfoReciboCaja")) {
                cargarInfoRecibo();           
            }else if (evento.equals("cargarRecibosCaja")) {
                cargarRecibosCaja();
            }else if (evento.equals("exportarRecibosCaja")) {
                exportarExcelRecibosCaja();
            }else if (evento.equals("cambiarEstadoRC")) {
                cambiarEstadoReciboCaja(usuario.getBd());
            }else if (evento.equals("autorizadoAplicarRC")) {
                isAllowToApplyRC();
            }else if (evento.equals("obtenerNombreCliente")) {
                obtenerNombreCliente();
            }else if (evento.equals("reasignarAsesor")) {
                reasignarAsesor(usuario.getBd());
            }else if (evento.equals("updateCommentRC")) {
                actualizarCommentRC();
            } else if (evento.equals("cargarNegociosCliente")) {
                cargarNegociosCliente();
            }
        }catch(Exception e){
        
        e.printStackTrace();
        
        }        
        

    }
    
    
    
    private void listarDetallesCartera(String unidad_negocio, int periodo_foto, String negocio,String cuota) throws Exception {
         
    //SeguimientoCarteraService rqservice = new SeguimientoCarteraService();
    ArrayList listaConsolidados = rqs.SCarteraDetalleCuentaBeans2(unidad_negocio, periodo_foto, negocio,cuota);
    Gson gson;
        gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(listaConsolidados) + "}";
        this.printlnResponse(json, "application/json;");

    }
    
    private void listarDetallesPagos(int periodo_foto, String negocio,String num_ingreso) throws Exception {
         
    //SeguimientoCarteraService rqservice = new SeguimientoCarteraService();
    ArrayList listaConsolidados = rqs.SCarteraDetallePagosBeans2(periodo_foto, negocio, num_ingreso);
    Gson gson;
        gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(listaConsolidados) + "}";
        this.printlnResponse(json, "application/json;");

    }
    
    private void listarDetallesPagosTodos(int periodo_foto, String negocio,String num_ingreso) throws Exception {
         
    //SeguimientoCarteraService rqservice = new SeguimientoCarteraService();
    ArrayList listaConsolidados = rqs.SCarteraDetallePagosBeansTodos(periodo_foto, negocio, num_ingreso);
    Gson gson;
        gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(listaConsolidados) + "}";
        this.printlnResponse(json, "application/json;");

    }
    
    
    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    public void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
    
    
    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd");
            this.crearArchivo(nameFileParcial + fmt.format( new Date() ) +".xls", titulo);

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 2;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private boolean generaArchivoCartera(List lista) throws Exception {
        boolean generado = true;
        try{
           // Inicia el proceso de listar a excel
           SeguimientoCarteraBeans cartera = new SeguimientoCarteraBeans();
           Iterator it = lista.iterator();
           while (it.hasNext()){

               cartera = (SeguimientoCarteraBeans)it.next();
               fila++;
               int col = 0;
               xls.adicionarCelda(fila  , col++ , cartera.getUndNegocio(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getPeriodoFoto(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getCedula(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getNombreCliente(), letra  );
               if(cartera.getConvenio().equals("38")) xls.adicionarCelda(fila  , col++ , cartera.getPagaduria(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getDireccion(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getBarrio(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getCiudad(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getTelefono(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getTelContacto(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getNegocio(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getConvenio(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getCuota(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getValorAsignado(), dinero  );
               xls.adicionarCelda(fila  , col++ , cartera.getFechaVencimiento(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getPeriodo(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getVencimientoMayor(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getDiasVencidos(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getDiaPago(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getStatus(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getStatus_vencimiento(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getDebidoCobrar(), dinero  );
               xls.adicionarCelda(fila  , col++ , cartera.getRecaudoxcuotaFiducia(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getRecaudoxcuotaFenal(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getRecaudoxCuota(), dinero  );
               xls.adicionarCelda(fila  , col++ , cartera.getAgente(), letra  );
              
           }

        }catch (Exception ex){
            generado = false;
           ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
        return generado;
    }

    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + iduser + "/" + nameFile;
            xls.nuevoLibro( rutaInformes + "/" + nameFile );
            header         = xls.nuevoEstilo("Tahoma", 10, true  , false, "text"  , HSSFColor.GREEN.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER, 2);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

   }
    
    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + iduser;
            File archivo = new File( rutaInformes );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }
    
    private boolean generaArchivoTabla(List lista) throws Exception {
        boolean generado = true;
        try{
           // Inicia el proceso de listar a excel
           SeguimientoCarteraBeans cartera = new SeguimientoCarteraBeans();
           Iterator it = lista.iterator();
           while (it.hasNext()){

               cartera = (SeguimientoCarteraBeans)it.next();
               fila++;
               int col = 0;
               xls.adicionarCelda(fila  , col++ , cartera.getCedula(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getNombreCliente(), letra  );
               if(cartera.getConvenio().equals("38"))   xls.adicionarCelda(fila  , col++ , cartera.getPagaduria(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getDireccion(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getBarrio(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getCiudad(), letra  );              
               xls.adicionarCelda(fila  , col++ , cartera.getTelefono(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getTelContacto(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getNegocio(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getConvenio(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getVencimientoMayor(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getDiaPago(), letra  );
               xls.adicionarCelda(fila  , col++ , cartera.getValorAsignado(), dinero  );
               xls.adicionarCelda(fila  , col++ , cartera.getDebidoCobrar(), dinero  );
               xls.adicionarCelda(fila  , col++ , cartera.getRecaudoxCuota(), dinero  );
               xls.adicionarCelda(fila  , col++ , cartera.getCumplimiento(), porcentaje  );
               xls.adicionarCelda(fila  , col++ , cartera.getValoraPagar(), dinero  );
               xls.adicionarCelda(fila, col++, cartera.getFechaUltimoCompromiso(), porcentaje);
               xls.adicionarCelda(fila, col++, cartera.getReestructuracion(), letra);
               xls.adicionarCelda(fila, col++, cartera.getJuridica(), letra);
               
           }
          
        }catch (Exception ex){
            generado = false;
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
        return generado;
    }
    
    private boolean generaArchivoSancionesMicro(List lista) throws Exception {
        boolean generado = true;
        try{
           // Inicia el proceso de listar a excel
           ReporteSanciones sanciones = new ReporteSanciones();
           Iterator it = lista.iterator();
           while (it.hasNext()){

               sanciones = (ReporteSanciones)it.next();
               fila++;
               int col = 0;
               xls.adicionarCelda(fila  , col++ , sanciones.getUnidad_negocio(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getPeriodo_foto(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCod_negocio(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCedula(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getNombre_cliente(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getVencimiento_mayor(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getFecha_pago_ingreso(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getFecha_vencimiento_mayor(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getDiferencia_pago(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_saldo_foto(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getInteres_mora(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getGasto_cobranza(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getNum_ingreso(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_ingreso(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_cxc_ingreso(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta1(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta2(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta3(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta4(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_ixm_ingreso(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_gac_ingreso(), dinero  );
               
               
           }
          
        }catch (Exception ex){
            generado = false;
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
        return generado;
    }
    
    private boolean generaArchivoSancionesFenalco(List lista) throws Exception {
        boolean generado = true;
        try{
           // Inicia el proceso de listar a excel
           ReporteSanciones sanciones = new ReporteSanciones();
           Iterator it = lista.iterator();
           while (it.hasNext()){

               sanciones = (ReporteSanciones)it.next();
               fila++;
               int col = 0;
               xls.adicionarCelda(fila  , col++ , sanciones.getUnidad_negocio(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getPeriodo_foto(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCod_negocio(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCedula(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getNombre_cliente(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getVencimiento_mayor(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getFecha_pago_ingreso(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getFecha_vencimiento_mayor(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getDiferencia_pago(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_saldo_foto(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getInteres_mora(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getGasto_cobranza(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getNum_ingreso(), letra  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_ingreso(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_cxc_ingreso(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta1(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta2(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta3(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta4(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta5(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getCuenta6(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_ixm_ingreso(), dinero  );
               xls.adicionarCelda(fila  , col++ , sanciones.getValor_gac_ingreso(), dinero  );
               
               
           }
          
        }catch (Exception ex){
            generado = false;
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
        return generado;
    }
    
    private void cargarCompromisosPago() {
                 
        try{
            String fechaini = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechafin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_agente = rqs.tipoAgenteCartera(iduser);
            String agente_campo = request.getParameter("agente") != null ? request.getParameter("agente") : ""; 
            String domicilio = request.getParameter("domicilio") != null ? request.getParameter("domicilio") : "";             
            String json = rqs.cargarCompromisosPago(fechaini, fechafin, agente_campo, tipo_agente, domicilio, iduser);          
            this.printlnResponse(json, "application/json;"); 
           
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    
     private void exportarExcelCompromisos() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("CompromisosPago_", "Reporte");
            String[] cabecera = {"Negocio", "Cedula", "Nombre Cliente", "Dirección", "Barrio", "Ciudad",
                "Fecha a Pagar", "Valor a Pagar", "Observación","Rango Mora","Linea negocio"};

            short[] dimensiones = new short[]{
                5000, 5000, 8000, 7000, 5000, 5000, 5000, 3000, 8000, 5000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cedula").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("direccion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("barrio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("ciudad").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_pagar").getAsString(), letra);              
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_pagar").getAsString()), letra);              
                xls.adicionarCelda(fila, col++, objects.get("observacion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("rango_mora").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("linea_negocio").getAsString(), letra);
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd");
            url = request.getContextPath() + "/exportar/migracion/" + iduser.toUpperCase() + "/CompromisosPago_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    private void cargarAgentesReporte() {
                 
        try{           
            String tipo_usuario = rqs.tipoAgenteCartera(iduser);            
            String Query = (tipo_usuario.equals("coordinador")) ? "SQL_AGENTES_CARTERA":"SQL_AGENTES_CARTERA_POR_USUARIO";    
            String sWhere = (tipo_usuario.equals("coordinador")) ? "": iduser;   
            ArrayList listaAgentes =  rqs.GetComboGenericoStr(Query,"id","descripcion", sWhere);         
            Gson gson = new Gson();
            String json = gson.toJson(listaAgentes);          
            this.printlnResponse(json, "application/json;");            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void cargarAsesoresComerciales() {

        try {            
            if(iduser.equals("JDVALLE")){
            String Query =  "SQL_AGENTES_COBRO_CTG";
            String sWhere =  "";
            ArrayList listaAgentes = rqs.GetComboGenericoStr(Query, "id", "descripcion", sWhere);
            Gson gson = new Gson();
            String json = gson.toJson(listaAgentes);
            this.printlnResponse(json, "application/json;");
        }else{
            String Query =  "SQL_AGENTES_COBRO";
            String sWhere =  "";
            ArrayList listaAgentes = rqs.GetComboGenericoStr(Query, "id", "descripcion", sWhere);
            Gson gson = new Gson();
            String json = gson.toJson(listaAgentes);
            this.printlnResponse(json, "application/json;");
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    private void cargarEstadosRecibo(boolean cargarTodos) {                 
        try{ 
            String Query = (cargarTodos) ?  "SQL_ESTADOS_RECIBO_CAJA_ALL" : "SQL_ESTADOS_RECIBO_CAJA";
            ArrayList listaEstadosRecibo = rqs.GetComboGenericoStr(Query,"id","descripcion", "");         
            Gson gson = new Gson();
            String json = gson.toJson(listaEstadosRecibo);          
            this.printlnResponse(json, "application/json;");            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void cargarTiposRecaudo() {
                 
        try{           
      
            String Query = "SQL_TIPOS_RECAUDO";    
            String sWhere = "";   
            ArrayList listaTiposRecaudo =  rqs.GetComboGenericoStr(Query,"id","descripcion", sWhere);         
            Gson gson = new Gson();
            String json = gson.toJson(listaTiposRecaudo);          
            this.printlnResponse(json, "application/json;");            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void guardarReciboCaja() {
        try {             
            String resp = "{}";
            String accion = request.getParameter("action") != null ? request.getParameter("action") : ""; 
            if (accion.equals("insertar")){
                String consecutivo_inicial = request.getParameter("consecutivo_inicial") != null ? request.getParameter("consecutivo_inicial") : "";
                String consecutivo_final = request.getParameter("consecutivo_final") != null ? request.getParameter("consecutivo_final") : "";
                String fecha_entrega = request.getParameter("fecha_entrega") != null ? request.getParameter("fecha_entrega") : "";
                String asesor = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
                String area = request.getParameter("area") != null ? request.getParameter("area") : "";
                String estado_recibo = request.getParameter("estado_recibo") != null ? request.getParameter("estado_recibo") : "";
                TransaccionService tService = new TransaccionService(dbName);
                tService.crearStatement();
                for (int i = Integer.parseInt(consecutivo_inicial); i <= Integer.parseInt(consecutivo_final); i++) {
                    tService.getSt().addBatch(rqs.guardarReciboCaja(i,fecha_entrega,asesor,area,estado_recibo, iduser));                                    
                } 
                tService.execute();                
                rqs.actualizaConsecutivoRC(); 
                resp = "{\"respuesta\":\"OK\"}";
            }else{
                String consecutivo = request.getParameter("consecutivo") != null ? request.getParameter("consecutivo") : "";
                String fecha_entrega = request.getParameter("fecha_entrega") != null ? request.getParameter("fecha_entrega") : "";
                String asesor = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
                String area = request.getParameter("area") != null ? request.getParameter("area") : "";
                String estado_recibo = request.getParameter("estado_recibo") != null ? request.getParameter("estado_recibo") : "";
                String id_cliente = request.getParameter("id_cliente") != null ? request.getParameter("id_cliente") : "";
                String fecha_recibido = request.getParameter("fecha_recibido") != null ? request.getParameter("fecha_recibido") : "";
                String nombre = request.getParameter("nombre") != null ? request.getParameter("nombre") : "";
                String tipo_recaudo = request.getParameter("tipo_recaudo") != null ? request.getParameter("tipo_recaudo") : "";
                String valor_recaudo = request.getParameter("valor_recaudo") != null ? request.getParameter("valor_recaudo") : "0";
                String cod_neg = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "0";
                resp = rqs.actualizarRecibosCaja(consecutivo,fecha_entrega,asesor,area,estado_recibo,fecha_recibido,id_cliente,nombre,tipo_recaudo,valor_recaudo, cod_neg,iduser); 
            }
//          resp = (accion.equals("insertar")) ? rqs.guardarReciboCaja(consecutivo,fecha_entrega,asesor,area,estado_recibo,fecha_recibido,id_cliente,nombre,tipo_recaudo,valor_recaudo,iduser): rqs.actualizarRecibosCaja(consecutivo,fecha_entrega,asesor,area,estado_recibo,fecha_recibido,id_cliente,nombre,tipo_recaudo,valor_recaudo,iduser);                   
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void anularReciboCaja() {
        try {
            String consecutivo = request.getParameter("consecutivo") != null ? request.getParameter("consecutivo") : ""; 
            String resp = "{}";
            if (rqs.existe_Recibo_Caja(consecutivo, "")){
               resp = rqs.anularReciboCaja(consecutivo);  
            }else{
               resp = "{\"respuesta\":\"NUMERO DE RECIBO NO EXISTE. NO ES POSIBLE ANULAR\"}";
            }                 
            this.printlnResponse(resp, "application/json;");            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     private void cargarInfoRecibo() {
                 
        try{
            String num_recibo = request.getParameter("num_recibo") != null ? request.getParameter("num_recibo") : "";
            String json = "{}";
            if (rqs.existe_Recibo_Caja(num_recibo, " AND id_estado_recibo IN('ANULADO','APLICADO')")){
              json = "{\"respuesta\":\"NUMERO DE RECIBO INGRESADO HA SIDO " + rqs.fn_get_Estado_Recibo_Caja(num_recibo,"")+ ". VERIFIQUE EN MODULO DE CONSULTA\"}";
            }else{
                json = rqs.cargarInfoRecibo(num_recibo); 
            }
                    
            this.printlnResponse(json, "application/json;"); 
           
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void cargarRecibosCaja() {
                 
        try{
            if(iduser.equals("JDVALLE")){
            String asesor = iduser;
            String fechaini = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechafin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String estado_recibo = request.getParameter("estado_recibo") != null ? request.getParameter("estado_recibo") : "";
            String num_recibo = request.getParameter("num_recibo") != null ? request.getParameter("num_recibo") : "";
            String json = rqs.cargarRecibosCaja(asesor, fechaini, fechafin, estado_recibo, num_recibo);          
            this.printlnResponse(json, "application/json;"); 
        }else{
            String asesor = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
            String fechaini = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechafin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String estado_recibo = request.getParameter("estado_recibo") != null ? request.getParameter("estado_recibo") : "";
            String num_recibo = request.getParameter("num_recibo") != null ? request.getParameter("num_recibo") : "";
            String json = rqs.cargarRecibosCaja(asesor, fechaini, fechafin, estado_recibo, num_recibo);          
            this.printlnResponse(json, "application/json;"); 
            }  
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void cambiarEstadoReciboCaja(String Database) {   
        String estado_recibo = (request.getParameter("estado_recibo") != null) ? request.getParameter("estado_recibo") : "";     
        String listado[] = request.getParameter("listado").split(",");
       
        try {
            TransaccionService tService = new TransaccionService(Database);
            tService.crearStatement();
            for (int i = 0; i < listado.length; i++) {
                tService.getSt().addBatch(rqs.cambiarEstadoReciboCaja(listado[i], estado_recibo, iduser));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
    
    private void obtenerNextRCNumber() {
        try {
            String resp = "{}";           
            resp = rqs.get_Next_Recibo_Caja();
            this.printlnResponse(resp, "application/json;");      
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void isAllowToApplyRC() {
        try {       
            String resp = "{}";
            if (rqs.isAllowToApplyRC(iduser)) {
                resp = "{\"respuesta\":\"SI\"}";
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void obtenerNombreCliente() {
        try {
            String resp = "{}";
            String id_cliente = (request.getParameter("id_cliente") != null) ? request.getParameter("id_cliente") : "";     
            resp = rqs.get_Nombre_Cliente(id_cliente);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void reasignarAsesor(String Database) {   
        String asesor = (request.getParameter("asesor") != null) ? request.getParameter("asesor") : "";     
        String listado[] = request.getParameter("listado").split(",");
       
        try {
            TransaccionService tService = new TransaccionService(Database);
            tService.crearStatement();
            for (int i = 0; i < listado.length; i++) {
                tService.getSt().addBatch(rqs.reasignarAsesor(listado[i], asesor, iduser));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    } 
    
    private void actualizarCommentRC() {
        try {
            String consecutivo = request.getParameter("consecutivo") != null ? request.getParameter("consecutivo") : "";
            String comment = request.getParameter("comment") != null ? request.getParameter("comment") : "";
            String resp = "{}";           
            resp = rqs.actualizaComentarioRC(consecutivo, comment, iduser);          
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void exportarExcelRecibosCaja() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("RecibosCaja_", "Reporte");
            String[] cabecera = {"Consecutivo", "Fecha Entrega", "Asesor", "Area", "Observacion", "Fecha Recibido", "Id Cliente",
                "Nombre Cliente", "Negocio", "Tipo Recaudo", "Valor Recaudo"};

            short[] dimensiones = new short[]{
                5000, 5000, 8000, 7000, 7000, 5000, 5000, 5000, 5000, 5000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("num_recibo").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_entrega").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("asesor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("area").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("estado_recibo").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_recibido").getAsString(), letra);  
                xls.adicionarCelda(fila, col++, objects.get("id_cliente").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_cliente").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cod_neg").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("tipo_recaudo").getAsString(), letra);
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_recaudo").getAsString()), letra);              
               
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd");
            url = request.getContextPath() + "/exportar/migracion/" + iduser.toUpperCase() + "/RecibosCaja_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    private void cargarNegociosCliente() {
        try {
            String idCliente = request.getParameter("idCliente") != null ? request.getParameter("idCliente") : "";
            String resp = rqs.cargarNegociosCliente(idCliente);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

