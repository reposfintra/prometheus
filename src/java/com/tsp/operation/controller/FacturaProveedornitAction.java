
/********************************************************************
 *      Nombre Clase.................   FacturaProveedornitAction.java
 *      Descripci�n..................   Action que se encarga de generar un vector con los resgitros de proveedores por numero de nit
 *      Autor........................   David Lamadrid
 *      Fecha........................   12 de octubre de 2005, 06:36 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaProveedornitAction extends Action
{
        
        /** Creates a new instance of FacturaProveedornitAction */
        public FacturaProveedornitAction ()
        {
        }
        
        public void run () throws ServletException, InformationException
        {
                try
                {
                        
                        String tipo_documento =""+ request.getParameter ("tipo_documento");
                        String documento =""+ request.getParameter ("documento");
                        //System.out.println("DOCUMENTO "+documento);
                        String proveedor =""+ request.getParameter ("proveedor");
                        String tipo_documento_rel =""+ request.getParameter ("tipo_documento_rel");
                        String documento_relacionado =""+ request.getParameter ("documento_relacionado");
                        String fecha_documento=""+request.getParameter ("fecha_documento");
                        String banco =""+ request.getParameter ("c_banco");
                        double vlr_neto=0;
                        int plazo=0;
                        int num_items=0;
                        try
                        {
                                num_items  = Integer.parseInt (""+ request.getParameter ("num_items"));
                                //System.out.println("plazo"+request.getParameter ("plazo"));
                                vlr_neto  = Double.parseDouble (""+ request.getParameter ("vlr_neto"));
                                plazo = Integer.parseInt (""+ request.getParameter ("plazo"));
                        }
                        catch(java.lang.NumberFormatException e)
                        {
                                vlr_neto=0;
                                plazo=0;
                                num_items=1;
                        }
                        
                        Vector vItems = new Vector ();
                        //System.out.println("Numero de Items " + num_items);
                        for (int i=1;i <= num_items; i++)
                        {
                                CXPItemDoc item = new CXPItemDoc ();
                                String descripcion_i=""+ request.getParameter ("descripcion_i"+i);
                                String codigo_cuenta=""+ request.getParameter ("codigo_cuenta"+i);
                                String codigo_abc=""+ request.getParameter ("codigo_abc"+i);
                                String planilla=""+ request.getParameter ("planilla"+i);
                                double valor =0;
                                try
                                {
                                        valor=Double.parseDouble (""+ request.getParameter ("valor1"+i));
                                }
                                catch(java.lang.NumberFormatException e)
                                {
                                        valor=0;
                                }
                                //System.out.println("descripcion_i "+descripcion_i);
                                //System.out.println("codigo_cuenta "+codigo_cuenta);
                                //System.out.println("codigo_abc "+codigo_abc);
                                //System.out.println("planilla"+planilla);
                                //System.out.println("valor "+ valor);
                                
                                item.setDescripcion (descripcion_i);
                                item.setCodigo_cuenta (codigo_cuenta);
                                item.setCodigo_abc (codigo_abc);
                                item.setPlanilla (planilla);
                                item.setVlr_me (valor);
                                
                                
                                Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos ();
                                Vector vImpuestosPorItem= new Vector ();
                                for(int x=0;x<vTipoImp.size ();x++)
                                {
                                        CXPImpItem impuestoItem = new CXPImpItem ();
                                        String cod_impuesto = ""+ request.getParameter ("impuesto"+x+""+i);
                                        impuestoItem.setCod_impuesto (cod_impuesto);
                                        //System.out.println("impuesto"+cod_impuesto);
                                        vImpuestosPorItem.add (impuestoItem);
                                }
                                item.setVItems ( vImpuestosPorItem);
                                vItems.add (item);
                        }
                        
                        model.cxpItemDocService.setVecCxpItemsDoc (vItems);
                        
                        String sucursal =""+ request.getParameter ("c_sucursal");
                        String moneda=""+ request.getParameter ("moneda");
                        String descripcion =""+ request.getParameter ("descripcion");
                        String observacion =""+ request.getParameter ("observacion");
                        String usuario_aprobacion=""+request.getParameter ("usuario_aprobacion");
                        
                        
                        Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit (proveedor);
                        int b =0;
                        if(o_proveedor != null)
                        {
                                //System.out.println(" Id Mins "+o_proveedor.getC_idMims ());
                                //System.out.println(" Beneficiario "+o_proveedor.getC_branch_code ());
                                //System.out.println(" Sucursal "+o_proveedor.getC_bank_account ());
                                //System.out.println(" Id Mins "+o_proveedor.getC_payment_name ());
                                b=1;
                        }
                        
                        if (b!=1)
                        {
                                proveedor="";
                        }
                        
                        CXP_Doc factura = new CXP_Doc ();
                        factura.setTipo_documento (tipo_documento);
                        factura.setDocumento (documento);
                        factura.setProveedor (proveedor);
                        factura.setTipo_documento_rel (tipo_documento_rel);
                        factura.setDocumento_relacionado (documento_relacionado);
                        factura.setFecha_documento (fecha_documento);
                        factura.setBanco (banco);
                        factura.setVlr_neto (vlr_neto);
                        factura.setMoneda (moneda);
                        factura.setSucursal (sucursal);
                        factura.setDescripcion (descripcion);
                        factura.setObservacion (observacion);
                        factura.setUsuario_aprobacion (usuario_aprobacion);
                        factura.setPlazo (plazo);
                        model.cxpDocService.setFactura (factura);
                        
                        //System.out.println("seteo la factura");
                        String next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp?";
                        //System.out.println("next en Filtro"+next);
                        
                        RequestDispatcher rd = application.getRequestDispatcher (next);
                        if(rd == null)
                                throw new Exception ("No se pudo encontrar "+ next);
                        rd.forward (request, response);
                        
                        
                }
                catch(Exception e)
                {
                        throw new ServletException ("Accion:"+ e.getMessage ());
                }
                
        }
}
