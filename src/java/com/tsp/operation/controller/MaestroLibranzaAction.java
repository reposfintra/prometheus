/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.MaestroLibranzaDAO;
import com.tsp.operation.model.DAOS.impl.MaestroLibranzaImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.ExcelApiUtil;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author egonzalez
 */
public class MaestroLibranzaAction extends Action {

    Usuario usuario = null;
    String reponseJson = "{}";
    private MaestroLibranzaDAO dao;
    private final int CARGAR_SALARIO_MINIMO = 1;
    private final int GUARDAR_SALARIO_MINIMO = 2;
    private final int ACTUALIZAR_SALARIO_MINIMO = 3;
    private final int CAMBIAR_ESTADO_SALARIO_MINIMO = 4;
    private final int CARGAR_GRILLA_PAGADURIAS = 5;
    private final int GUARDAR_PAGADURIAS = 6;
    private final int ACTUALIZAR_PAGADURIAS = 7;
    private final int ACTIVAR_INACTIVAR_PAGADURIAS = 8;
    private final int CARGAR_OCUPACION_LABORAL = 9;
    private final int GUARDAR_OCUPACION_LABORAL = 10;
    private final int ACTUALIZAR_OCUPACION_LABORAL = 11;
    private final int CAMBIAR_OCUPACION_LABORAL = 12;
    private final int CARGAR_DESCUENTOS_LEY = 13;
    private final int GUARDAR_DESCUENTOS_LEY = 14;
    private final int ACTUALIZAR_DESCUENTOS_LEY = 15;
    private final int CAMBIAR_DESCUENTOS_LEY = 16;
    private final int CARGAR_COMBO_OCUPACION_LABORAL = 17;
    private final int CARGAR_EXTRAPRIMA_LIBRANZA = 18;
    private final int GUARDAR_EXTRAPRIMA_LIBRANZA = 19;
    private final int ACTUALIZAR_EXTRAPRIMA_LIBRANZA = 20;
    private final int CAMBIAR_EXTRAPRIMA_LIBRANZA = 21;
    private final int CARGAR_OP_BANCARIAS_LIBRANZA = 22;
    private final int GUARDAR_OP_BANCARIAS_LIBRANZA = 23;
    private final int ACTUALIZAR_OP_BANCARIAS_LIBRANZA = 24;
    private final int CAMBIAR_OP_BANCARIAS_LIBRANZA = 25;
    private final int CARGAR_COMBO_TIPO_DOC = 26;
    private final int CARGAR_DEDUCCIONES_LIBRANZA = 27;
    private final int GUARDAR_DEDUCCIONES_LIBRANZA = 28;
    private final int ACTUALIZAR_DEDUCCIONES_LIBRANZA = 29;
    private final int CAMBIAR_DEDUCCIONES_LIBRANZA = 30;
    private final int CARGAR_COMBO_HC = 31;
    private final int CARGAR_CUENTA_HC = 32;
    private final int CARGAR_COMBO_OP_BANCARIAS_LIBRANZA = 33;
    private final int CARGAR_CONFIGURACION_LIBRANZA = 34;
    private final int CARGAR_FIRMAS_REGISTRADAS = 35;
    private final int CARGAR_COMBO_PAGADURIAS = 36;
    private final int CARGAR_COMBO_CONVENIOS = 37;
    private final int GUARDAR_CONFIG_LIBRANZA = 38;
    private final int ACTUALIZAR_CONFIG_LIBRANZA = 39;
    private final int ACTIVAR_INACTIVAR_CONFIG_LIBRANZA = 40;
    private final int GUARDAR_FIRMA_REGISTRADA = 41;
    private final int ACTUALIZAR_FIRMA_REGISTRADA = 42;
    private final int ACTIVAR_INACTIVAR_FIRMA_REGISTRADA = 43;
    private final int CARGAR_COMBO_CONCEPTO = 44;
    private final int CARGAR_INFORMACION_NEGOCIO = 45;
    private final int FORMALIZAR_NEGOCIO_LIBRANZA = 46;
    private final int CARGAR_COMBO_TIPO_OPERACION_LIBRANZA = 47;
    private final int CALCULAR_TASA_ANUAL = 48;
    private final int CARGAR_PERFECCIONAR_COMPRA_CARTERA = 49;
    private final int PERFECCIONAMIENTO_COMPRA_CARTERA = 50;
    private final int CARGAR_DEDUCCIONES_MICROCREDITO = 51;
    private final int CARGAR_COMBO_OCUPACION_LABORAL_MICRO = 52;
    private final int CAMBIAR_DEDUCCIONES_MICROCREDITO = 53;
    private final int ACTUALIZAR_DEDUCCIONES_MICROCREDITO = 54;
    private final int GUARDAR_DEDUCCIONES_MICROCREDITO = 55;
    private final int CARGAR_ENTIDADES_COMPRA_CARTERA = 56;
    private final int CARGAR_PROVEEDORES_ENTIDADES = 57;
    private final int CAMBIAR_ESTADO_ENTIDADES = 58;
    private final int GUARDAR_ENTIDADES_CC = 59;
    private final int CARGAR_OBLIGACIONES_COMPRA = 60;
    private final int GUARDAR_OBLIGACIONES_COMPRA = 61;
    private final int VERIFICAR_NIT = 62;
    private final int UPDATE_OBLIGACIONES_COMPRA = 63;
    private final int CARGAR_CHEQUE_COMPRA_CARTERA = 64;
    private final int CARGAR_PAZYSALVO = 65;
    private final int AUDITORIA_LIBRANZAS = 66;
    private final int LISTAR_DOCUMENTOS_CARGADOS = 67;
    private final int MOSTRAR_DOCUMENTO_CARGADO = 68;
    private final int CARGAR_EMPRESAS_ASOCIADAS = 69;
    private final int GUARDAR_EMPRESA_ASOCIADA = 70;
    private final int ACTUALIZAR_EMPRESA_ASOCIADA = 71;
    private final int ACTIVAR_INACTIVAR_EMPRESA_ASOCIADA = 72;
    private final int LISTAR_CLIENTES_LIBRANZA = 73;
    private final int EXPORTAR_EXCEL_CLIENTES = 74;
    private final int CARGAR_COMBO_CONVENIOS_PAGADURIA = 75;
    private final int CARGAR_CONVENIOS = 76;
    private final int CARGAR_FORMULARIO = 77;
    private final int CARGAR_CONVENIOS_LIBRANZA = 78;
    private final int CARGAR_EMPRESAS_LIBRANZA = 79;
    private final int CARGAR_TIPO_CUOTA = 80;
    private final int CARGAR_TIPO_TITULO_VALOR = 81;
    private final int RELIQUIDACION_LIBRANZA = 82;
    private final int INFORMACION_RELIQUIDACION_LIBRANZA = 83;
    private final int RELIQUIDAR_LIBRANZA = 84;
    private final int CALCULO_FECHA_PRIMERA_CUOTA_RELIQUIDACION = 85;
    private final int ACTUALIZAR_TASA_NEGOCIO = 86;
    

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new MaestroLibranzaImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_SALARIO_MINIMO:
                    CargarSalarioMinimo();
                    break;
                case GUARDAR_SALARIO_MINIMO:
                    guardarSalarioMinimo();
                    break;
                case ACTUALIZAR_SALARIO_MINIMO:
                    actualizarSalarioMinimo();
                    break;
                case CAMBIAR_ESTADO_SALARIO_MINIMO:
                    cambiarEstadoSalarioMinimo();
                    break;
                case CARGAR_GRILLA_PAGADURIAS:
                    cargarPagadurias();
                    break;
                case GUARDAR_PAGADURIAS:
                    guardarPagadurias();
                    break;
                case ACTUALIZAR_PAGADURIAS:
                    actualizarPagadurias();
                    break;
                case ACTIVAR_INACTIVAR_PAGADURIAS:
                    activaInactivaPagadurias();
                    break;
                case CARGAR_OCUPACION_LABORAL:
                    CargarOcupacionLaboral();
                    break;
                case GUARDAR_OCUPACION_LABORAL:
                    guardarOcupacionLaboral();
                    break;
                case ACTUALIZAR_OCUPACION_LABORAL:
                    actualizarOcupacionLaboral();
                    break;
                case CAMBIAR_OCUPACION_LABORAL:
                    cambiarEstadoOcupacionLaboral();
                    break;
                case CARGAR_DESCUENTOS_LEY:
                    CargarDescuentoLey();
                    break;
                case GUARDAR_DESCUENTOS_LEY:
                    guardarDescuentoLey();
                    break;
                case ACTUALIZAR_DESCUENTOS_LEY:
                    actualizarDescuentoLey();
                    break;
                case CAMBIAR_DESCUENTOS_LEY:
                    cambiarEstadoDescuentoLey();
                    break;
                case CARGAR_COMBO_OCUPACION_LABORAL:
                    cargarComboOcupacionLab();
                    break;
                case CARGAR_EXTRAPRIMA_LIBRANZA:
                    CargarExtraprimaLibranza();
                    break;
                case GUARDAR_EXTRAPRIMA_LIBRANZA:
                    guardarExtraprimaLibranza();
                    break;
                case ACTUALIZAR_EXTRAPRIMA_LIBRANZA:
                    actualizarExtraprimaLibranza();
                    break;
                case CAMBIAR_EXTRAPRIMA_LIBRANZA:
                    cambiarEstadoExtraprimaLibranza();
                    break;
                case CARGAR_OP_BANCARIAS_LIBRANZA:
                    CargarOpBancariaLibranza();
                    break;
                case GUARDAR_OP_BANCARIAS_LIBRANZA:
                    guardarOpBancariaLibranza();
                    break;
                case ACTUALIZAR_OP_BANCARIAS_LIBRANZA:
                    actualizarOpBancariaLibranza();
                    break;
                case CAMBIAR_OP_BANCARIAS_LIBRANZA:
                    cambiarEstadoOpBancariaLibranza();
                    break;
                case CARGAR_COMBO_TIPO_DOC:
                    cargarComboTipodoc();
                    break;
                case CARGAR_DEDUCCIONES_LIBRANZA:
                    CargardDeduccionesLibranza();
                    break;
                case GUARDAR_DEDUCCIONES_LIBRANZA:
                    guardarDeduccionesLibranza();
                    break;
                case ACTUALIZAR_DEDUCCIONES_LIBRANZA:
                    actualizarDeduccionesLibranza();
                    break;
                case CAMBIAR_DEDUCCIONES_LIBRANZA:
                    cambiarEstadoDeduccionesLibranza();
                    break;
                case CARGAR_COMBO_HC:
                    cargarComboHC();
                    break;
                case CARGAR_CUENTA_HC:
                    CargarCuentaHC();
                    break;
                case CARGAR_COMBO_OP_BANCARIAS_LIBRANZA:
                    cargarComboOpBancariaLibranza();
                    break;
                case CARGAR_CONFIGURACION_LIBRANZA:
                    cargarConfigLibranza();
                    break;
                case CARGAR_FIRMAS_REGISTRADAS:
                    cargarFirmasRegistradas();
                    break;
                case CARGAR_COMBO_PAGADURIAS:
                    cargarComboPagadurias();
                    break;
                case CARGAR_COMBO_CONVENIOS:
                    cargarComboConvenios();
                    break;
                case GUARDAR_CONFIG_LIBRANZA:
                    guardarConfigLibranza();
                    break;
                case ACTUALIZAR_CONFIG_LIBRANZA:
                    actualizarConfigLibranza();
                    break;
                case ACTIVAR_INACTIVAR_CONFIG_LIBRANZA:
                    activaInactivaConfigLibranza();
                    break;
                case GUARDAR_FIRMA_REGISTRADA:
                    guardarFirmaRegistrada();
                    break;
                case ACTUALIZAR_FIRMA_REGISTRADA:
                    actualizarFirmaRegistrada();
                    break;
                case ACTIVAR_INACTIVAR_FIRMA_REGISTRADA:
                    activaInactivaFirmaRegistrada();
                    break;
                case CARGAR_COMBO_CONCEPTO:
                    cargarComboConcepto();
                    break;
                case CARGAR_INFORMACION_NEGOCIO:
                    CargarInformacionNegocio();
                    break;
                case FORMALIZAR_NEGOCIO_LIBRANZA:
                    formalizarLibranza();
                    break;
                case CARGAR_COMBO_TIPO_OPERACION_LIBRANZA:
                    cargarComboTipoOpBancariaLibranza();
                    break;
                case CALCULAR_TASA_ANUAL:
                    calcularTasaAnual();
                    break;
                case CARGAR_PERFECCIONAR_COMPRA_CARTERA:
                    cargarperfeccionarCompraCartera();
                    break;
                case PERFECCIONAMIENTO_COMPRA_CARTERA:
                    perfeccionamientoCompraCartera();
                    break;
                case CARGAR_DEDUCCIONES_MICROCREDITO:
                    CargardDeduccionesMicrocredito();
                    break;
                case CARGAR_COMBO_OCUPACION_LABORAL_MICRO:
                    cargarComboOcupacionLabMicro();
                    break;
                case CAMBIAR_DEDUCCIONES_MICROCREDITO:
                    cambiarEstadoDeduccionesMicrocredito();
                    break;
                case ACTUALIZAR_DEDUCCIONES_MICROCREDITO:
                    actualizarDeduccionesMicrocredito();
                    break;
                case GUARDAR_DEDUCCIONES_MICROCREDITO:
                    guardarDeduccionesMicrocredito();
                    break;
                case CARGAR_ENTIDADES_COMPRA_CARTERA:
                    CargardEntidadesCompraCartera();
                    break;
                case CARGAR_PROVEEDORES_ENTIDADES:
                    cargarProveedoresEntidades();
                    break;
                case CAMBIAR_ESTADO_ENTIDADES:
                    CambiarEstadoEntidadesCompraCartera();
                    break;
                case GUARDAR_ENTIDADES_CC:
                    guardarEntidadesCompraCartera();
                    break;
                case CARGAR_OBLIGACIONES_COMPRA:
                    CargarObligacionesCompra();
                    break;
                case GUARDAR_OBLIGACIONES_COMPRA:
                    guardarObligacionesCompra();
                    break;
                case VERIFICAR_NIT:
                    verificarNit();
                    break;
                case UPDATE_OBLIGACIONES_COMPRA:
                    actualizarObligacionesCompra();
                    break;
                case CARGAR_CHEQUE_COMPRA_CARTERA:
                    cargarCHequeCompraCartera();
                    break;
                case CARGAR_PAZYSALVO:
                    cargarPazySalvo();
                    break;
                case AUDITORIA_LIBRANZAS:
                    auditoriaLibranzas();
                    break;
                case LISTAR_DOCUMENTOS_CARGADOS:
                    this.getNombresArchivos();
                    break;    
                case MOSTRAR_DOCUMENTO_CARGADO:
                    this.almacenarArchivoEnCarpetaUsuario();
                    break;
                case CARGAR_EMPRESAS_ASOCIADAS:
                    cargarEmpresasAsociadas();
                    break;
                case GUARDAR_EMPRESA_ASOCIADA:
                    guardarEmpresaAsociada();
                    break;
                case ACTUALIZAR_EMPRESA_ASOCIADA:
                    actualizarEmpresaAsociada();
                    break;
                case ACTIVAR_INACTIVAR_EMPRESA_ASOCIADA:
                    activaInactivaEmpresaAsociada();
                    break;
                case LISTAR_CLIENTES_LIBRANZA:
                    CargarClientesLibranza();
                    break;
                case EXPORTAR_EXCEL_CLIENTES:
                    this.exportarExcelClientes();
                    break;
                case CARGAR_COMBO_CONVENIOS_PAGADURIA:
                    cargarComboConveniosPagaduria();
                    break;
                case CARGAR_CONVENIOS:
                    cargarComboConveniosReliquidacion();
                    break;
                case CARGAR_FORMULARIO:
                    cargarFormularioReliquidacion();
                    break;
                case CARGAR_CONVENIOS_LIBRANZA:
                    cargarConveniosLibranza();
                    break;
                case CARGAR_EMPRESAS_LIBRANZA:
                    cargarempresasLibranza();
                    break;
                case CARGAR_TIPO_CUOTA:
                    cargarTipoCuota();
                    break;
                case CARGAR_TIPO_TITULO_VALOR:
                    cargarTipoTituloValor();
                    break;
                case RELIQUIDACION_LIBRANZA:
                    reliquidacionLibranza();
                    break;
                case INFORMACION_RELIQUIDACION_LIBRANZA:
                    informacionReliquidacionLibranza();
                    break;
                case RELIQUIDAR_LIBRANZA:
                    reliquidarLibranza();
                    break;
                case CALCULO_FECHA_PRIMERA_CUOTA_RELIQUIDACION:
                    calculo_fecha_primera_cuota_reliquidacion();
                    break;
                case ACTUALIZAR_TASA_NEGOCIO:
                    actualizarTasaNegocio();
                    break;
                  
               
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CargarSalarioMinimo() {
        try {
            this.printlnResponseAjax(dao.CargarSalarioMinimo(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarSalarioMinimo() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String ano = request.getParameter("ano");
            String salario_minimo_diario = request.getParameter("smd");
            String salario_minimo_mensual = request.getParameter("smm");
            String variacion_anual = request.getParameter("va");
            this.dao.guardarSalarioMinimo(usuario, ano, salario_minimo_diario, salario_minimo_mensual, variacion_anual);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarSalarioMinimo() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String ano = request.getParameter("ano");
            String salario_minimo_diario = request.getParameter("smd");
            String salario_minimo_mensual = request.getParameter("smm");
            String variacion_anual = request.getParameter("va");
            this.dao.actualizarSalarioMinimo(usuario, ano, salario_minimo_diario, salario_minimo_mensual, variacion_anual, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoSalarioMinimo() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoSalarioMinimo(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    private void cargarPagadurias() {
        try {
            String json = dao.cargarPagadurias();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarPagadurias() {
        try {
            String documento = request.getParameter("documento");
            String digito_verificacion = request.getParameter("digito_verificacion");
            String razon_social = request.getParameter("razon_social");
            String ciudad = request.getParameter("ciudad");
            String direccion = request.getParameter("direccion");
            String telefono = request.getParameter("telefono");
            String email = request.getParameter("email");
            String verificacion = request.getParameter("verificacion");
            String resp = "{}";
            if (dao.existeValorEnPagadurias("documento", documento, "0")) {
                resp = "{\"error\":\" No es posible actualizar, ya existe una pagaduria con ese documento\"}";
            } else if (dao.existeValorEnPagadurias("razon_social", razon_social, "0")) {
                resp = "{\"error\":\" No es posible actualizar, ya existe una pagaduria con dicha razon social\"}";
            } else {
                if (verificacion.equals("NO EXISTE")) {
                    dao.guardarProveedor(documento, razon_social, ciudad, digito_verificacion, usuario);
                }
                resp = dao.guardarPagadurias(razon_social, documento, digito_verificacion, ciudad, direccion, telefono, email, usuario.getLogin(), usuario.getDstrct());
            }
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarPagadurias() {
        try {
            String documento = request.getParameter("documento");
            String digito_verificacion = request.getParameter("digito_verificacion");
            String razon_social = request.getParameter("razon_social");
            String ciudad = request.getParameter("ciudad");
            String direccion = request.getParameter("direccion");
            String telefono = request.getParameter("telefono");
            String email = request.getParameter("email");
            String id = request.getParameter("id");
            String resp = "{}";
            if (dao.existeValorEnPagadurias("documento", documento, id)) {
                resp = "{\"error\":\" No es posible actualizar, ya existe una pagaduria con ese documento\"}";
            } else if (dao.existeValorEnPagadurias("razon_social", razon_social, id)) {
                resp = "{\"error\":\" No es posible actualizar, ya existe una pagaduria con dicha razon social\"}";
            } else {
                resp = dao.actualizarPagadurias(id, razon_social, documento, digito_verificacion, ciudad, direccion, telefono, email, usuario.getLogin());
            }
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaPagadurias() {
        try {
            String idPagaduria = request.getParameter("id");
            String resp = dao.activaInactivaPagadurias(idPagaduria, usuario.getLogin());
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CargarOcupacionLaboral() {
        try {
            this.printlnResponseAjax(dao.CargarOcupacionLaboral(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarOcupacionLaboral() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String descripcion = request.getParameter("descripcion");
            this.dao.guardarOcupacionLaboral(usuario, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarOcupacionLaboral() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = request.getParameter("descripcion");
            this.dao.actualizarOcupacionLaboral(usuario, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoOcupacionLaboral() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoOcupacionLaboral(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void CargarDescuentoLey() {
        try {
            this.printlnResponseAjax(dao.CargarDescuentoLey(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarDescuentoLey() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            String descripcion = request.getParameter("descripcion");
            String smlv_inicial = request.getParameter("smlv_inicial");
            String smlv_final = request.getParameter("smlv_final");
            String totaldesc = request.getParameter("totaldesc");
            this.dao.guardarDescuentoLey(usuario, ocupacion_laboral, descripcion, smlv_inicial, smlv_final, totaldesc);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarDescuentoLey() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            String descripcion = request.getParameter("descripcion");
            String smlv_inicial = request.getParameter("smlv_inicial");
            String smlv_final = request.getParameter("smlv_final");
            String totaldesc = request.getParameter("totaldesc");
            this.dao.actualizarDescuentoLey(usuario, ocupacion_laboral, descripcion, smlv_inicial, smlv_final, totaldesc, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoDescuentoLey() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoDescuentoLey(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void cargarComboOcupacionLab() {
        try {
            this.printlnResponseAjax(dao.cargarComboOcupacionLab(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboOcupacionLabMicro() {
        try {
            this.printlnResponseAjax(dao.cargarComboOcupacionLabMicro(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void CargarExtraprimaLibranza() {
        try {
            this.printlnResponseAjax(dao.CargarExtraprimaLibranza(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarExtraprimaLibranza() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            String descripcion = request.getParameter("descripcion");
            String edad_inicial = request.getParameter("edad_inicial");
            String edad_final = request.getParameter("edad_final");
            String perc_extraprima = request.getParameter("perc_extraprima");
            this.dao.guardarExtraprimaLibranza(usuario, ocupacion_laboral, descripcion, edad_inicial, edad_final, perc_extraprima);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarExtraprimaLibranza() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            String descripcion = request.getParameter("descripcion");
            String edad_inicial = request.getParameter("edad_inicial");
            String edad_final = request.getParameter("edad_final");
            String perc_extraprima = request.getParameter("perc_extraprima");
            this.dao.actualizarExtraprimaLibranza(usuario, ocupacion_laboral, descripcion, edad_inicial, edad_final, perc_extraprima, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoExtraprimaLibranza() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoExtraprimaLibranza(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void CargarOpBancariaLibranza() {
        try {
            this.printlnResponseAjax(dao.CargarOpBancariaLibranza(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarOpBancariaLibranza() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String tipo_documento = request.getParameter("tipo_documento");
            String descripcion = request.getParameter("descripcion");
            String cmc = request.getParameter("cmc");
            String cuenta_cxp = request.getParameter("cuenta_cxp");
            String cuenta_detalle = request.getParameter("cuenta_detalle");
            String tipo_operacion = request.getParameter("tipo_operacion");
            this.dao.guardarOpBancariaLibranza(usuario, tipo_documento, descripcion, cmc, cuenta_cxp, cuenta_detalle, tipo_operacion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarOpBancariaLibranza() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String tipo_documento = request.getParameter("tipo_documento");
            String descripcion = request.getParameter("descripcion");
            String cmc = request.getParameter("cmc");
            String cuenta_cxp = request.getParameter("cuenta_cxp");
            String cuenta_detalle = request.getParameter("cuenta_detalle");
            String tipo_operacion = request.getParameter("tipo_operacion");
            this.dao.actualizarOpBancariaLibranza(usuario, tipo_documento, descripcion, cmc, cuenta_cxp, cuenta_detalle, id, tipo_operacion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoOpBancariaLibranza() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoOpBancariaLibranza(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void cargarComboTipodoc() {
        try {
            this.printlnResponseAjax(dao.cargarComboTipodoc(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboHC() {
        try {
            String tipodoc = request.getParameter("tipodoc");
            this.printlnResponseAjax(dao.cargarComboHC(tipodoc), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void CargarCuentaHC() {
        try {
            String cmc = request.getParameter("cmc");
            String tipodoc = request.getParameter("tipodoc");
            this.printlnResponseAjax(dao.CargarCuentaHC(cmc, tipodoc), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CargardDeduccionesLibranza() {
        try {
            this.printlnResponseAjax(dao.CargardDeduccionesLibranza(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CargardDeduccionesMicrocredito() {
        try {
            this.printlnResponseAjax(dao.CargardDeduccionesMicrocredito(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarDeduccionesLibranza() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String operacion_bancaria = request.getParameter("operacion_bancaria");
            String descripcion = request.getParameter("descripcion");
            String desembolso_inicial = request.getParameter("desembolso_inicial");
            String desembolso_final = request.getParameter("desembolso_final");
            String valor_cobrar = request.getParameter("valor_cobrar");
            String perc_cobrar = request.getParameter("perc_cobrar");
            String n_xmil = request.getParameter("n_xmil");
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            this.dao.guardarDeduccionesLibranza(usuario, operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, ocupacion_laboral);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void guardarDeduccionesMicrocredito() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String operacion_bancaria = request.getParameter("operacion_bancaria");
            String descripcion = request.getParameter("descripcion");
            String desembolso_inicial = request.getParameter("desembolso_inicial");
            String desembolso_final = request.getParameter("desembolso_final");
            String valor_cobrar = request.getParameter("valor_cobrar");
            String perc_cobrar = request.getParameter("perc_cobrar");
            String n_xmil = request.getParameter("n_xmil");
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            this.dao.guardarDeduccionesMicrocredito(usuario, operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, ocupacion_laboral);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarDeduccionesLibranza() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String operacion_bancaria = request.getParameter("operacion_bancaria");
            String descripcion = request.getParameter("descripcion");
            String desembolso_inicial = request.getParameter("desembolso_inicial");
            String desembolso_final = request.getParameter("desembolso_final");
            String valor_cobrar = request.getParameter("valor_cobrar");
            String perc_cobrar = request.getParameter("perc_cobrar");
            String n_xmil = request.getParameter("n_xmil");
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            this.dao.actualizarDeduccionesLibranza(usuario, operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, ocupacion_laboral, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarDeduccionesMicrocredito() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String operacion_bancaria = request.getParameter("operacion_bancaria");
            String descripcion = request.getParameter("descripcion");
            String desembolso_inicial = request.getParameter("desembolso_inicial");
            String desembolso_final = request.getParameter("desembolso_final");
            String valor_cobrar = request.getParameter("valor_cobrar");
            String perc_cobrar = request.getParameter("perc_cobrar");
            String n_xmil = request.getParameter("n_xmil");
            String ocupacion_laboral = request.getParameter("ocupacion_laboral");
            this.dao.actualizarDeduccionesMicrocredito(usuario, operacion_bancaria, descripcion, desembolso_inicial, desembolso_final, valor_cobrar, perc_cobrar, n_xmil, ocupacion_laboral, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarComboOpBancariaLibranza() {
        try {
            String operacion = request.getParameter("operacion");
            this.printlnResponseAjax(dao.cargarComboOpBancariaLibranza(operacion), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cambiarEstadoDeduccionesLibranza() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoDeduccionesLibranza(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void cambiarEstadoDeduccionesMicrocredito() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoDeduccionesMicrocredito(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    private void cargarConfigLibranza() {
        try {
            String json = dao.cargarConfigLibranza();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarFirmasRegistradas() {
        try {
            String idConfLibranza = request.getParameter("id_config_libranza");
            String json = dao.cargarFirmasRegistradas(idConfLibranza);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarConfigLibranza() {
        try {
            String nom_conv_pagaduria = (request.getParameter("nom_conv_pagaduria") != null) ? request.getParameter("nom_conv_pagaduria") : "";
            String id_convenio = (request.getParameter("id_convenio") != null) ? request.getParameter("id_convenio") : "";
            String id_pagaduria = (request.getParameter("id_pagaduria") != null) ? request.getParameter("id_pagaduria") : "";
            String id_ocupacion_laboral = (request.getParameter("id_ocupacion_laboral") != null) ? request.getParameter("id_ocupacion_laboral") : "";
            String tasa_mensual = (request.getParameter("tasa_mensual") != null) ? request.getParameter("tasa_mensual") : "0";
            String tasa_anual = (request.getParameter("tasa_anual") != null) ? request.getParameter("tasa_anual") : "0";
            String tasa_renovacion = (request.getParameter("tasa_renovacion") != null) ? request.getParameter("tasa_renovacion") : "0";
            String monto_minimo = (request.getParameter("monto_minimo") != null) ? request.getParameter("monto_minimo") : "0";
            String monto_maximo = (request.getParameter("monto_maximo") != null) ? request.getParameter("monto_maximo") : "0";
            String plazo_minimo = (request.getParameter("plazo_minimo") != null) ? request.getParameter("plazo_minimo") : "0";
            String plazo_maximo = (request.getParameter("plazo_maximo") != null) ? request.getParameter("plazo_maximo") : "0";
            String colchon = (request.getParameter("colchon") != null) ? request.getParameter("colchon") : "0";
            String factor_seguro = (request.getParameter("factor_seguro") != null) ? request.getParameter("factor_seguro") : "0";
            String porc_descuento = (request.getParameter("porc_descuento") != null) ? request.getParameter("porc_descuento") : "0";
            String dia_ent_novedades = (request.getParameter("dia_ent_novedades") != null) ? request.getParameter("dia_ent_novedades") : "";
            String dia_pago = (request.getParameter("dia_pago") != null) ? request.getParameter("dia_pago") : "";
            String periodo_gracia = (request.getParameter("periodo_gracia") != null) ? request.getParameter("periodo_gracia") : "0";
            String requiere_anexo = (request.getParameter("requiere_anexo") != null) ? request.getParameter("requiere_anexo") : "N";
            String resp = "{}";
            resp = dao.guardarConfigLibranza(nom_conv_pagaduria, id_convenio, id_pagaduria, id_ocupacion_laboral, tasa_mensual, tasa_anual, tasa_renovacion, monto_minimo, monto_maximo, plazo_minimo, plazo_maximo, colchon, factor_seguro, porc_descuento, dia_ent_novedades, dia_pago, periodo_gracia, requiere_anexo, usuario.getLogin(), usuario.getDstrct());

            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarConfigLibranza() {
        try {
            String nom_conv_pagaduria = (request.getParameter("nom_conv_pagaduria") != null) ? request.getParameter("nom_conv_pagaduria") : "";
            String id_convenio = (request.getParameter("id_convenio") != null) ? request.getParameter("id_convenio") : "";
            String id_pagaduria = (request.getParameter("id_pagaduria") != null) ? request.getParameter("id_pagaduria") : "";
            String id_ocupacion_laboral = (request.getParameter("id_ocupacion_laboral") != null) ? request.getParameter("id_ocupacion_laboral") : "";
            String tasa_mensual = (request.getParameter("tasa_mensual") != null) ? request.getParameter("tasa_mensual") : "0";
            String tasa_anual = (request.getParameter("tasa_anual") != null) ? request.getParameter("tasa_anual") : "0";
            String tasa_renovacion = (request.getParameter("tasa_renovacion") != null) ? request.getParameter("tasa_renovacion") : "0";
            String monto_minimo = (request.getParameter("monto_minimo") != null) ? request.getParameter("monto_minimo") : "0";
            String monto_maximo = (request.getParameter("monto_maximo") != null) ? request.getParameter("monto_maximo") : "0";
            String plazo_minimo = (request.getParameter("plazo_minimo") != null) ? request.getParameter("plazo_minimo") : "0";
            String plazo_maximo = (request.getParameter("plazo_maximo") != null) ? request.getParameter("plazo_maximo") : "0";
            String colchon = (request.getParameter("colchon") != null) ? request.getParameter("colchon") : "0";
            String factor_seguro = (request.getParameter("factor_seguro") != null) ? request.getParameter("factor_seguro") : "0";
            String porc_descuento = (request.getParameter("porc_descuento") != null) ? request.getParameter("porc_descuento") : "0";
            String dia_ent_novedades = (request.getParameter("dia_ent_novedades") != null) ? request.getParameter("dia_ent_novedades") : "";
            String dia_pago = (request.getParameter("dia_pago") != null) ? request.getParameter("dia_pago") : "";
            String periodo_gracia = (request.getParameter("periodo_gracia") != null) ? request.getParameter("periodo_gracia") : "0";
            String requiere_anexo = (request.getParameter("requiere_anexo") != null) ? request.getParameter("requiere_anexo") : "N";
            String id = request.getParameter("id");
            String resp = "{}";
            resp = dao.actualizarConfigLibranza(id, nom_conv_pagaduria, id_convenio, id_pagaduria, id_ocupacion_laboral, tasa_mensual, tasa_anual, tasa_renovacion, monto_minimo, monto_maximo, plazo_minimo, plazo_maximo, colchon, factor_seguro, porc_descuento, dia_ent_novedades, dia_pago, periodo_gracia, requiere_anexo, usuario.getLogin());
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaConfigLibranza() {
        try {
            String idConfigLibranza = request.getParameter("id");
            String resp = dao.activaInactivaConfigLibranza(idConfigLibranza, usuario.getLogin());
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarFirmaRegistrada() {
        try {
            String id_config_libranza = (request.getParameter("id_config_libranza") != null) ? request.getParameter("id_config_libranza") : "";
            String nombre = (request.getParameter("nombre") != null) ? request.getParameter("nombre") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String telefono = request.getParameter("telefono");
            String email = request.getParameter("email");
            String resp = "{}";
            resp = dao.guardarFirmaRegistrada(id_config_libranza, nombre, documento, telefono, email, usuario.getLogin(), usuario.getDstrct());
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarFirmaRegistrada() {
        try {
            String id_config_libranza = (request.getParameter("id_config_libranza") != null) ? request.getParameter("id_config_libranza") : "";
            String nombre = (request.getParameter("nombre") != null) ? request.getParameter("nombre") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String telefono = request.getParameter("telefono");
            String email = request.getParameter("email");
            String id = request.getParameter("id");
            String resp = "{}";
            resp = dao.actualizarFirmaRegistrada(id, id_config_libranza, nombre, documento, telefono, email, usuario.getLogin());
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaFirmaRegistrada() {
        try {
            String idPagaduria = request.getParameter("id");
            String resp = dao.activaInactivaFirmaRegistrada(idPagaduria, usuario.getLogin());
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarComboPagadurias() {
        try {
            this.printlnResponseAjax(dao.cargarComboPagadurias(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboConvenios() {
        try {
            this.printlnResponseAjax(dao.cargarComboConvenios(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboConcepto() {
        try {
            this.printlnResponseAjax(dao.cargarComboConcepto(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void CargarInformacionNegocio() {
        try {
            String negocio = (request.getParameter("cod_neg") != null) ? request.getParameter("cod_neg") : "";
            this.printlnResponseAjax(dao.CargarInformacionNegocio(negocio), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void formalizarLibranza() {
        try {
            String negocio = (request.getParameter("negocio") != null) ? request.getParameter("negocio") : "";
            String concepto = (request.getParameter("concepto") != null) ? request.getParameter("concepto") : "";
            String coment = (request.getParameter("coment") != null) ? request.getParameter("coment") : "";
            this.printlnResponseAjax(dao.formalizarLibranza(negocio, usuario, concepto, coment), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarComboTipoOpBancariaLibranza() {
        try {
            this.printlnResponseAjax(dao.cargarComboTipoOpBancariaLibranza(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void calcularTasaAnual() {
        try {
            String tasa_mensual = (request.getParameter("tasa_mensual") != null) ? request.getParameter("tasa_mensual") : "";
            this.printlnResponseAjax(dao.calcularTasaAnual(tasa_mensual), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarperfeccionarCompraCartera() {
        try {
            String json = dao.cargarperfeccionarCompraCartera();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void perfeccionamientoCompraCartera() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String negocio = (request.getParameter("negocio") != null) ? request.getParameter("negocio") : "";
            String coment = (request.getParameter("coment") != null) ? request.getParameter("coment") : "";
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.perfeccionamientoCompraCarteraTrazabilidad(negocio, usuario, coment));
            tService.getSt().addBatch(dao.perfeccionamientoCompraCarteraNegocios(negocio, usuario));
            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void CargardEntidadesCompraCartera() {
        try {
            this.printlnResponseAjax(dao.CargardEntidadesCompraCartera(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarProveedoresEntidades() {
        try {
            String nombre = request.getParameter("nombre") == null ? "" : request.getParameter("nombre");
            String json = dao.cargarProveedoresEntidades(nombre);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void CambiarEstadoEntidadesCompraCartera() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.CambiarEstadoEntidadesCompraCartera(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void guardarEntidadesCompraCartera() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String nombre = request.getParameter("nombre");
            String nit = request.getParameter("nit");
            String digver = request.getParameter("digver");

            this.dao.guardarEntidadesCompraCartera(usuario, nombre, nit, digver);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void CargarObligacionesCompra() {
        try {
            String numero_solicitud = (request.getParameter("numero_solicitud") != null) ? request.getParameter("numero_solicitud") : "";
            this.printlnResponseAjax(dao.CargarObligacionesCompra(numero_solicitud), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarObligacionesCompra() {
        String json = "";
        try {
            String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
            String numero_solicitud = (request.getParameter("numero_solicitud") != null) ? request.getParameter("numero_solicitud") : "";
            String diferencia = (request.getParameter("diferencia") != null) ? request.getParameter("diferencia") : "";
            json = "{\"respuesta\":\"" + this.dao.guardarObligacionesCompra(usuario, operacion, numero_solicitud, diferencia) + "\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void verificarNit() {
        try {
            String documento = request.getParameter("documento");
            String resp = dao.verificarNit(documento);
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actualizarObligacionesCompra() {
        String json = "";
        try {
            JsonArray obj = (JsonArray) (new JsonParser()).parse(request.getParameter("info"));
            json = this.dao.actualizarObligacionesCompra(usuario, obj);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void cargarCHequeCompraCartera() {
        try {
            String doc_rel = (request.getParameter("doc_rel") != null) ? request.getParameter("doc_rel") : "";
            String json = dao.cargarCHequeCompraCartera(doc_rel);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarPazySalvo() {
        String respuesta = "{}";
        String documento = "";
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes") + "gestionadministrativa/pazysalvo/";//se establece la ruta del archivo

            //this.createDir(directorioArchivos );
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                String fieldtem = "", valortem = "";

                while (it.hasNext()) {

                    FileItem fileItem = (FileItem) it.next();

                    if ((fileItem.isFormField())) {

                        fieldtem = fileItem.getFieldName();
                        valortem = fileItem.getString();

                        if (fieldtem.equals("documento")) {
                            documento = valortem;
                        }

                    }

                    if (!(fileItem.isFormField())) {

                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                        if (fileItem.getName() != "" && !fileItem.getName().isEmpty() && fileItem.getSize() > 0) {

                            String nombreArchivo = fileItem.getName();

                            String[] nombrearreglo = nombreArchivo.split("\\\\");
                            String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];
                            String rutaArchivo = directorioArchivos + documento;
                            nombre_archivo = nombreArchivoremix;

                            this.createDir(rutaArchivo);

                            File archivo = new File(rutaArchivo + "/" + nombre_archivo);

                            fileItem.write(archivo);
                        }

                    }

                }
            }
            dao.actualizarReferencia(documento);
            respuesta = "{\"respuesta\":\"OK\"}";

        } catch (Exception ee) {
            respuesta = "{\"respuesta\":\"" + ee.getMessage() + "\"}";
            //respuesta = "{\"respuesta\":\"" + ee.getMessage() + "\"}";
            System.out.println("eroror:" + ee.toString() + "__" + ee.getMessage());
        } finally {
            try {
                this.printlnResponseAjax(respuesta, "application/json;");
            } catch (Exception ex) {
                Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    
    private void auditoriaLibranzas() {
        try {
            String fechaini = request.getParameter("fecha_ini") != null ? request.getParameter("fecha_ini") : "";
            String fechafin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            this.printlnResponseAjax(dao.auditoriaLibranzas(fechaini, fechafin), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void getNombresArchivos() {
        java.util.List lista = null;
         Gson gson = new Gson();
        try {
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/pazysalvo/";
            this.printlnResponseAjax(gson.toJson(dao.searchNombresArchivos(rutaOrigen, documento)),"application/json");
        } catch (Exception e) {
            System.out.println("errorrr::" + e.toString() + "__" + e.getMessage());
        }       
    }

    public void almacenarArchivoEnCarpetaUsuario() throws Exception {
        try {
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? request.getParameter("nomarchivo") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");            
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/pazysalvo/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";
            if (dao.almacenarArchivoEnCarpetaUsuario(documento, ("" + rutaOrigen+documento), ("" + rutaDestino), nomarchivo)) {
                this.printlnResponseAjax("{\"respuesta\":\"SI\",\"login\":\"" + usuario.getLogin() + "\"}","application/json");
            } else {
                this.printlnResponseAjax("{\"respuesta\":\"NO\"}","application/json");
            }           
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    private void cargarEmpresasAsociadas() {
        try {
            String idPagaduria = request.getParameter("id_pagaduria");
            String json = dao.cargarEmpresasPagaduria(idPagaduria);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void guardarEmpresaAsociada() {
        try {
            String id_pagaduria = (request.getParameter("id_pagaduria") != null) ? request.getParameter("id_pagaduria") : "";
            String razon_social = (request.getParameter("razon_social") != null) ? request.getParameter("razon_social") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String digito_verificacion = (request.getParameter("digito_verificacion") != null) ? request.getParameter("digito_verificacion") : "";
            String telefono =  (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";
            String direccion =  (request.getParameter("direccion") != null) ? request.getParameter("direccion") : "";
            String resp = "{}";
            resp = dao.guardarEmpresaPagaduria(id_pagaduria, razon_social, documento, digito_verificacion, telefono, direccion, usuario);
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarEmpresaAsociada() {
        try {
            String id_pagaduria = (request.getParameter("id_pagaduria") != null) ? request.getParameter("id_pagaduria") : "";
            String razon_social = (request.getParameter("razon_social") != null) ? request.getParameter("razon_social") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String digito_verificacion = (request.getParameter("digito_verificacion") != null) ? request.getParameter("digito_verificacion") : "";
            String telefono =  (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";
            String direccion =  (request.getParameter("direccion") != null) ? request.getParameter("direccion") : "";
            String id = request.getParameter("id");
            String resp = "{}";
            resp = dao.actualizarEmpresaPagaduria(id, id_pagaduria, razon_social, documento, digito_verificacion, telefono, direccion, usuario);
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaEmpresaAsociada() {
        try {
            String idEmpresaPagaduria = request.getParameter("id");
            String resp = dao.activaInactivaEmpresaPagaduria(idEmpresaPagaduria, usuario);
            this.printlnResponseAjax(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void CargarClientesLibranza() {
        try {
            String fechaini = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechafin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String id_convenio = request.getParameter("id_convenio") != null ? request.getParameter("id_convenio") : "";
            String json = dao.cargarClientesLibranza(fechaini,fechafin,id_convenio);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     private void exportarExcelClientes() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Clientes_libranza_", titulo = "REPORTE CLIENTES LIBRANZA";

        cabecera = new String[]{"Id", "Fecha", "C�dula", "Nombres", "Apellidos", "Celular", "Empresa", "Convenio", "Monto", "Cuotas", "Plazo", "Viabilidad"};

        dimensiones = new short[]{
            3000, 5000, 5000, 7000, 7000, 6000, 7000, 8000, 6000, 3000, 3000, 3000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        this.printlnResponseAjax( apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request), "text/plain;");   
    }   
     
    public void cargarComboConveniosPagaduria() {
        try {
            this.printlnResponseAjax(dao.cargarComboConveniosPagaduria(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(MaestroLibranzaAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarComboConveniosReliquidacion() {
         try {
            this.printlnResponseAjax(dao.cargarComboConveniosReliquidacion(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarFormularioReliquidacion() {
        try {
            String busqueda = request.getParameter("busqueda") != null ? request.getParameter("busqueda") : "";
            String dato = request.getParameter("dato") != null ? request.getParameter("dato") : "";
            this.printlnResponseAjax(dao.cargarFormularioReliquidacion(busqueda, dato), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarConveniosLibranza() {
       try {
            String cliente = request.getParameter("cliente") != null ? request.getParameter("cliente") : "";
            this.printlnResponseAjax(dao.cargarConveniosLibranza(cliente), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarempresasLibranza() {
       try {
            String id_pagaduria = request.getParameter("id_pagaduria") != null ? request.getParameter("id_pagaduria") : "";
            this.printlnResponseAjax(dao.cargarempresasLibranza(id_pagaduria), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void cargarTipoCuota() {
       try {
            this.printlnResponseAjax(dao.cargarTipoCuota(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarTipoTituloValor() {
        try {
            this.printlnResponseAjax(dao.cargarTipoTituloValor(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void reliquidacionLibranza() {
        String json = "";
        try {
            json = request.getParameter("info") != null ? request.getParameter("info") : "{}";
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.reliquidacionLibranza(usuario, obj), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void informacionReliquidacionLibranza() {
        try {
            String valor_desembolso = request.getParameter("valor_desembolso") != null ? request.getParameter("valor_desembolso") : "";
            String numero_cuotas = request.getParameter("numero_cuotas") != null ? request.getParameter("numero_cuotas") : "";
            String tipo_cuota = request.getParameter("tipo_cuota") != null ? request.getParameter("tipo_cuota") : "";
            String fecha_calculo = request.getParameter("fecha_calculo") != null ? request.getParameter("fecha_calculo") : "";
            String fecha_primera_cuota = request.getParameter("fecha_primera_cuota") != null ? request.getParameter("fecha_primera_cuota") : "";
            int formulario = request.getParameter("formulario") != null ? Integer.parseInt(request.getParameter("formulario")): 0;
            
            this.printlnResponseAjax(dao.informacionReliquidacionLibranza(valor_desembolso, numero_cuotas, tipo_cuota, fecha_calculo, fecha_primera_cuota,formulario), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void reliquidarLibranza() {
         try {
            String valor_desembolso = request.getParameter("valor_desembolso") != null ? request.getParameter("valor_desembolso") : "";
            String numero_cuotas = request.getParameter("numero_cuotas") != null ? request.getParameter("numero_cuotas") : "";
            String tipo_cuota = request.getParameter("tipo_cuota") != null ? request.getParameter("tipo_cuota") : "";
            String fecha_calculo = request.getParameter("fecha_calculo") != null ? request.getParameter("fecha_calculo") : "";
            String fecha_primera_cuota = request.getParameter("fecha_primera_cuota") != null ? request.getParameter("fecha_primera_cuota") : "";
            String formulario = request.getParameter("formulario") != null ? request.getParameter("formulario") : "";
            String valorFianza = request.getParameter("valorFianza") != null ? request.getParameter("valorFianza") : "";
            this.printlnResponseAjax(dao.reliquidarLibranza(valor_desembolso, numero_cuotas, tipo_cuota, fecha_calculo, fecha_primera_cuota,formulario,usuario,valorFianza), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculo_fecha_primera_cuota_reliquidacion() {
        try {
            String formulario = request.getParameter("formulario") != null ? request.getParameter("formulario") : "";
            this.printlnResponseAjax(dao.calculo_fecha_primera_cuota_reliquidacion(formulario), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarTasaNegocio() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String cod_neg = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            String tasa_convenio = request.getParameter("tasa_convenio") != null ? request.getParameter("tasa_convenio") : "";

            this.dao.actualizarTasaNegocio(cod_neg, tasa_convenio);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

}
