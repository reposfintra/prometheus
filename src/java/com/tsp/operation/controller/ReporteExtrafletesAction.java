/********************************************************************
 *      Nombre Clase.................   ReporteExtrafletesAction.java
 *      Descripci�n..................   Genera el reporte de extrafletes y costos reembolsables
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   03.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteExtrafletesAction extends Action{
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteExtrafletesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter("FechaI");
        String fechaF = request.getParameter("FechaF");      
        String cliente = request.getParameter("clientes");  
        String tipo = request.getParameter("tipo");
        String doc = request.getParameter("document");
        String costos = request.getParameter("costos");
        boolean ver_costos = costos==null ? false : true;
                
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        String next = "/jsp/masivo/reportes/ReporteExtrafletes.jsp?msg=" +
                "El Reporte de Extrafletes y Costos Reembolsables se ha generado exitosamente.";
        
        try{
            
            model.planillaService.reporteExtrafletes(fechaI, fechaF, cliente, tipo, doc);
            Vector reporte = model.planillaService.getReporte();
            
            if( reporte.size() == 0){
                next = "/jsp/masivo/reportes/ReporteExtrafletes.jsp?msg=" +
                        "La consulta no gener� ning�n resultado.";
            }                
            
            ReporteExtrafletesTh hilo = new ReporteExtrafletesTh();
            hilo.start(model, reporte, usuario.getLogin(), fechaI, fechaF, ver_costos);            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
