/*
 * PlanillasSinPlanViajeAction.java
 *
 * Created on 12 de junio de 2006, 04:46 PM
 */
/***********************************************************************************
 * Nombre clase : ............... PlanillasSinPlanViajeAction.java                 *
 * Descripcion :................. Action para llamar al m�todo                     * 
                                  getPlanillasSinPlanviaje de  PlanillaService     *
 * Autor :....................... Osvaldo P�rez Ferrer                             *
 * Fecha :........................ 12 de Junio de 2006                             *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Osvaldo
 */
public class PlanillasSinPlanViajeAction extends Action{
    
    /** Creates a new instance of PlanillasSinPlanViajeAction */
    public PlanillasSinPlanViajeAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/planviaje/planillasSinPlanViaje.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        Vector planillas = null;
        try{                        
            String agencia = usuario.getId_agencia();
            String nagencia = model.ciudadService.buscarNomCiudadxCodigo(agencia);
            planillas = model.planillaService.getPlanillasSinPlanviaje(agencia);  
            
            request.setAttribute("planillas", planillas);
            request.setAttribute("nagencia", nagencia);
            
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
