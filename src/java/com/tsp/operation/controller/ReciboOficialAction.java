/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ReciboOficialDAO;
import com.tsp.operation.model.DAOS.impl.ReciboOficialImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.ReporteDatacredito;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.GenerarPlanoCentralRiesgo;
import com.tsp.util.Utility;
import java.io.File;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author jpacosta
 */
public class ReciboOficialAction extends Action{
    private final int CARGAR_ROP = 0;
    private final int CARGAR_DETALLES = 1;
    private final int CARGAR_UNIDADES = 2;
    private final int CARGAR_HISTORICO_REPORTES = 3;
    private final int EXPORTAR_INFORME = 4;
    private final int CARGAR_PREVISUALIZACION_REPORTADOS = 5;
    private final int EXPORTAR_PREVISUALIZACION_REPORTADOS = 6;
    private final int GUARDAR_PREVISUALIZACION = 7;
    private final int LIMPIAR_REPORTE = 8;
    private final int ELIMINAR_NEGOCIO = 9;
    private final int GENERAR_PLANO_REPORTE_DATACREDITO = 10;
    private final int REFRESCAR_REPORTE_DATACREDITO = 11;
    private final int CARGAR_UNIDADES_DATACREDITO = 12;
    private final int DETALLE_ROP = 13;
    private final int ACTUALIZAR_IF = 14;
    private final int GENERAR_PLANO_ASOBANCARIA = 15;
    private final int GENERAR_PLANO_ASOBANCARIA_EFECTY = 16;
    private ReciboOficialDAO dao;
    private String txtResp;
    private String tipo;                //"application/json" | "text/plain"
    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String   rutaInformes;
    String   nombre;
    Usuario usuario = null;
            
    
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session     = request.getSession();
            usuario     = (Usuario)session.getAttribute("Usuario");
            tipo = "application/json";
            this.txtResp = "";
            dao = new ReciboOficialImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_ROP:
                    this.cargarROP();
                    break;
                case CARGAR_DETALLES:
                    this.cargarDetalles();
                    break;
                case CARGAR_UNIDADES:
                    this.cargarUnidades();
                    break;
                case  CARGAR_HISTORICO_REPORTES:
                    this.cargarHistoricoReportes();
                    break;
                case  EXPORTAR_INFORME:
                    this.exportarInforme();
                    break;  
                case  CARGAR_PREVISUALIZACION_REPORTADOS:
                    this.previsualizarReportados();
                    break;    
                case  EXPORTAR_PREVISUALIZACION_REPORTADOS:
                    this.ExportarPrevisualizacion();
                    break;      
                case  GUARDAR_PREVISUALIZACION:
                    this.guardarPrevisualizacion();
                    break;          
                case  LIMPIAR_REPORTE:
                    this.limpiarReporte();
                    break;              
                case  ELIMINAR_NEGOCIO:
                    this.eliminarNegocio();
                    break;   
                case  GENERAR_PLANO_REPORTE_DATACREDITO:
                    this.generarPlanoReporteDatacredito();
                    break;       
                case  REFRESCAR_REPORTE_DATACREDITO:
                    this.refrescarReporteDatacredito();
                    break; 
                case  CARGAR_UNIDADES_DATACREDITO:
                    this.cargarUnidadesDatacredito();
                    break;     
                case DETALLE_ROP: 
                    this.buscarDetalleRop();
                    break;
                case ACTUALIZAR_IF: 
                    this.actualizarIF();
                    break;
                case GENERAR_PLANO_ASOBANCARIA: 
                    this.genPlanoAso();
                    break;
                case GENERAR_PLANO_ASOBANCARIA_EFECTY: 
                    this.genPlanoAsoEfecty();
                    break;    
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.responser();
        }
    }
    
    private void cargarROP() {
        try {
            String documento = request.getParameter("documento");
            String periodo =  request.getParameter("periodo");
            String unegocio = request.getParameter("uneg");
            String negocio = request.getParameter("negocio");
            String cedula = request.getParameter("cedula");
            String valor = request.getParameter("valor");
            txtResp = dao.cargarROP(documento, periodo, unegocio, negocio, cedula, valor);
        } catch(Exception e) {
            txtResp = "{mensaje:"+e.getMessage()+"}";
        }
    }
    
    private void cargarDetalles() {
        try {
            String idRop = (request.getParameter("idRop"));
            txtResp = dao.cargarDetalles(idRop);
        } catch(Exception e) {
            txtResp = "{mensaje:"+e.getMessage()+"}";
        }
    }
    
    private void cargarUnidades() {
        try {
            txtResp = dao.cargarUnidades();
        } catch(Exception e) {
            txtResp = "{mensaje:"+e.getMessage()+"}";
        }
    }
    
    /** 
     * envia respuesta al cliente
     **/
    private void responser() {
        try {
            response.setContentType(this.tipo + "; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(txtResp);
        } catch (Exception e) {
        }
    }   

    private void cargarHistoricoReportes() {
        try {
            String periodo =  request.getParameter("periodo");
            String unegocio = request.getParameter("uneg");
            String negocio = request.getParameter("negocio");
            String cedula = request.getParameter("cedula");
            txtResp = dao.cargarHistoricoPeticiones(periodo, unegocio, negocio, cedula);
        } catch(Exception e) {
            txtResp = "{mensaje:"+e.getMessage()+"}";
        }
    }

    private void exportarInforme() throws SQLException {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        String periodo =  request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String negocio = request.getParameter("negocio");
        String cedula = request.getParameter("cedula");
        boolean generado = false;
        String url = "";
        int op_icono = 0;
        String resp = "";
        
        try {
            this.generarRUTA();   // Ya generada en proceso anterior
            this.crearLibro("ReportadosDatacredito_", "REPORTADOS CENTRALES DE RIESGO ");

            // encabezado
            String[] cabecera = {
                "Und Negocio", "Periodo", "Tipo Id Cliente", "Identificacion", "Nombre Cliente","Situacion Titular","Cod Negocio", "Fecha Apertura", "Fecha Vencimiento", "Fecha Corte Proceso", "Dias Mora", "Novedad", "Min Dias Mora", "Vr. Desembolso", "Saldo Deuda","Saldo Mora","Cuota Mensual",
                "Numero Cuotas","Cuotas Canceladas","Cuotas en Mora","Fecha Limite Pago","Ultimo Pago","Ciudad Radicacion","Cod Dane Radicacion","Ciudad Residencia","Cod Dane Residencia","Dpto Residencia","Direccion Residencia","Telefono Residencia","Ciudad Laboral","Cod Dane Laboral","Departamento Laboral",
                "Direccion Laboral","Telefono Laboral","Ciudad Correspondencia","Cod Dane Correspondencia","Direccion Correspondencia","Correo Electronico","Celular Solicitante","Tipo"
            };

            short[] dimensiones = new short[]{
                4000, 2000, 2000, 3000, 7000, 2000, 3000, 3000, 3000, 3000,2000, 2000, 2000, 3000, 3000, 3000, 3000, 2000, 2000, 2000,
                3000, 3000, 3000, 2000, 3000, 2000, 3000, 4000, 2000, 3000,2000, 3000, 4000, 2000, 4000, 2000, 4000, 4000, 3000, 2000
            };

            this.generaTitulos(cabecera, dimensiones);
            lista = dao.listaReportadosDatacredito(periodo, unegocio, negocio, cedula);
            generado = generaArchivoHistorico(lista);
            System.out.println("exportar "+generado);
            this.cerrarArchivo();
            if (generado) {
                fmt= new SimpleDateFormat("yyyMMdd");
                url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ReportadosDatacredito_"+fmt.format( new Date() )+".xls"; 
                resp = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
        }
            this.printlnResponse(resp, "text/plain");

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL REPORTADOS DATACREDITO. \n " + e.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd");
            this.crearArchivo(nameFileParcial + fmt.format( new Date() ) +".xls", titulo);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }

   private void generaTitulos(String [] cabecera, short[] dimensiones) throws Exception {
       
        try{

            fila = 6;
            
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                if ( i < dimensiones.length )
                    xls.cambiarAnchoColumna(i, dimensiones[i] );
            }

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }

    private boolean generaArchivoHistorico(List lista) throws Exception {
        boolean generado = true;
        try{

           // Inicia el proceso de listar a excel

           BeanGeneral bean = new BeanGeneral();
           Iterator it = lista.iterator();
           while (it.hasNext()){

               bean = (BeanGeneral)it.next();

               fila++;
               int col = 0;

               xls.adicionarCelda(fila  , col++ , bean.getValor_01(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_02(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_03(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_04(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_05(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_06(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_07(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_08(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_09(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_10(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_11(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_12(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_13(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_14(), dinero  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_15(), dinero  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_16(), dinero  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_17(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_18(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_19(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_20(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_21(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_22(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_23(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_24(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_25(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_26(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_27(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_28(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_29(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_30(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_31(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_32(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_33(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_34(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_35(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_36(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_37(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_38(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_39(), letra  );
               xls.adicionarCelda(fila  , col++ , bean.getValor_40(), letra  );
           }

        }catch (Exception ex){
            generado = false;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
        return generado;
    }

    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo2);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo2 );
            xls.adicionarCelda(2,0, "USUARIO", titulo2);
            xls.adicionarCelda(2,1, usuario.getNombre() , titulo2);


        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( rutaInformes + "/" + nameFile );
            header         = xls.nuevoEstilo("Tahoma", 10, true  , false, "text"  , HSSFColor.DARK_BLUE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

   }
    
    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( rutaInformes );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }
    
    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void previsualizarReportados(){
        String json="{}";
        try {
            String periodo =  request.getParameter("periodo");
            String unegocio = request.getParameter("uneg");
            String fecha = request.getParameter("fecha");
            int rangoIni = Integer.parseInt(request.getParameter("rangoIni"));
            int rangoFin = Integer.parseInt(request.getParameter("rangoFin"));
            String nomNeg = request.getParameter("nomNeg");
            ArrayList<ReporteDatacredito> lista = new  ArrayList<ReporteDatacredito>();
            lista = dao.cargarPrevisualizarReportados(periodo,unegocio,fecha,rangoIni,rangoFin,nomNeg,"Visualizar");
            Gson gson = new Gson();
            json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";          
        } catch (Exception ex) {
            json = "{\"page\":1,\"rows\":" + ex.getMessage() + "}";     
            Logger.getLogger(ReciboOficialAction.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                Logger.getLogger(ReciboOficialAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }

    private void ExportarPrevisualizacion() throws SQLException {
        boolean generado = false;
        String url = "";
        String resp = "";

        try {
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            if (!json.equals("")) {
                Gson gson = new Gson();
                Type tipoListaFacturas;
                tipoListaFacturas = new TypeToken<ArrayList<ReporteDatacredito>>() {
                }.getType();
                ArrayList<ReporteDatacredito> lista = gson.fromJson(json, tipoListaFacturas);

                this.generarRUTA();
                this.crearLibro("PrevisualizacionReporteDatacredito_", "PREVISUALIZACION REPORTADOS CENTRALES DE RIESGO");

                // encabezado
                String[] cabecera = {
                    "Und Negocio", "Periodo Anterior", "Periodo", "Tipo Id Anterior", "Tipo Id", "Identificacion Anterior", "Identificacion", "Nombre Anterior", "Nombre", "Situacion Titular", "Cod Negocio", "Fecha Apertura Anterior", "Fecha Apertura", "Fecha Vencimiento Anterior", "Fecha Vencimiento", "Fecha Corte Proceso", "Dias Mora", "Novedad Anterior", "Novedad", "Min Dias Mora", "Vr. Desembolso", "Saldo Deuda", "Saldo Mora Anterior", "Saldo Mora", "Cuota Mensual",
                    "Numero Cuotas", "Cuotas Canceladas", "Cuotas en Mora", "Fecha Limite Pago", "Ultimo Pago", "Ciudad Radicacion", "Cod Dane Radicacion", "Ciudad Residencia", "Cod Dane Residencia", "Dpto Residencia", "Direccion Residencia", "Telefono Residencia", "Ciudad Laboral", "Cod Dane Laboral", "Departamento Laboral",
                    "Direccion Laboral", "Telefono Laboral", "Ciudad Correspondencia", "Cod Dane Correspondencia", "Direccion Correspondencia", "Correo Electronico", "Celular Solicitante", "Tipo"
                };

                short[] dimensiones = new short[]{
                    4000, 2000, 2000, 2000, 2000, 3000, 3000, 7000, 7000, 2000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 2000, 2000, 2000, 3000, 3000, 3000, 3000, 3000, 2000, 2000, 2000,
                    3000, 3000, 3000, 2000, 3000, 2000, 3000, 4000, 2000, 3000, 2000, 3000, 4000, 2000, 4000, 4000, 2000, 4000, 4000, 3000, 2000
                };

                this.generaTitulos(cabecera, dimensiones);
                generado = generaArchivoPrevisualizacion(lista);
                System.out.println("exportar " + generado);
                this.cerrarArchivo();
                if (generado) {
                    fmt = new SimpleDateFormat("yyyMMdd");
                    url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/PrevisualizacionReporteDatacredito_" + fmt.format(new Date()) + ".xls";
                    resp = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                            + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
                }
                this.printlnResponse(resp, "text/plain");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL REPORTADOS DATACREDITO. \n " + e.getMessage());
        }
    }

    private void guardarPrevisualizacion() throws SQLException {
        String periodo = request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String listaNeg[] = null;
        String codNeg = "";
        String PeriodoAnterior = "";
        BeanGeneral bg = new BeanGeneral();
        listaNeg = request.getParameter("listado").split(";_;");
        String[] vec = new String[2];
        try {
            String mes = periodo.substring(4, 6);
            if (mes.equals("01")){
                PeriodoAnterior = Integer.parseInt(periodo.substring(0,4))-1+"12";
            }else{
                PeriodoAnterior = String.valueOf(Integer.parseInt(periodo) - 1);
            }
           TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < listaNeg.length; i++) {
                vec = listaNeg[i].split(",");
                bg.setValor_01(PeriodoAnterior);
                bg.setValor_02(unegocio);
                bg.setValor_03(vec[0]); //codigo del negocio
                bg.setValor_04(vec[1]); // cedula
                
                tService.getSt().addBatch(dao.guardarReportados(bg,usuario.getLogin()));
            }
            tService.execute();
            this.printlnResponse("OK", "application/text;");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR GUARDANDO REPORTADOS DATACREDITO. \n " + e.getMessage());
        }

    }
    
    private boolean generaArchivoPrevisualizacion(List lista) throws Exception {
        boolean generado = true;
        try{

           // Inicia el proceso de listar a excel

           ReporteDatacredito rep = new ReporteDatacredito();
           Iterator it = lista.iterator();
           while (it.hasNext()){

               rep = (ReporteDatacredito)it.next();

               fila++;
               int col = 0;

               xls.adicionarCelda(fila  , col++ , rep.getUnd_negocio(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_periodo(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getPeriodo(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_tipo_id(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getTipo_identificacion(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_identificacion(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getIdentificacion(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_nombre(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getNombre(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getSituacion_titular(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getNegocio(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_fecha_apertura(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getFecha_apertura(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_fecha_ven(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getFecha_vencimiento(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getFecha_corte_proceso(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getDias_mora(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_novedad(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getNovedad(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getMin_dias_mora(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getDesembolso(), dinero  );
               xls.adicionarCelda(fila  , col++ , rep.getSaldo_deuda(), dinero  );
               xls.adicionarCelda(fila  , col++ , rep.getHr_saldo_mora(), dinero  );
               xls.adicionarCelda(fila  , col++ , rep.getSaldo_mora(), dinero  );
               xls.adicionarCelda(fila  , col++ , rep.getCuota_mensual(), dinero  );
               xls.adicionarCelda(fila  , col++ , rep.getNumero_cuotas(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCuotas_canceladas(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCuotas_mora(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getFecha_limite_pago(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getUltimo_pago(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCiudad_radicacion(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCod_dane_radicacion(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCiudad_residencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCod_dane_residencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getDepartamento_residencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getDireccion_residencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getTelefono_residencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCiudad_laboral(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCod_dane_laboral(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getDepartamento_laboral(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getDireccion_laboral(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getTelefono_laboral(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCiudad_correspondencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCod_dane_correspondencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getDireccion_correspondencia(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCorreo_electronico(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getCelular_solicitante(), letra  );
               xls.adicionarCelda(fila  , col++ , rep.getTipo(), letra  );
              
           }

        }catch (Exception ex){
            generado = false;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
        return generado;
    }

    private void limpiarReporte() {
        String periodo = request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        
        dao.limpiarReporte(periodo,unegocio);
        
    }

    private void eliminarNegocio() throws SQLException {
        String periodo = request.getParameter("periodo_reg");
        String unegocio = request.getParameter("negocio");
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.anularNegocioHistorico(periodo,unegocio,usuario.getLogin()));
            tService.getSt().addBatch(dao.anularNegocioReportado(periodo,unegocio,usuario.getLogin()));
            tService.execute();
            this.printlnResponse("OK", "application/text;");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR GUARDANDO REPORTADOS DATACREDITO. \n " + e.getMessage());
        }
        
    }

    private void generarPlanoReporteDatacredito() throws SQLException {
        String periodo =  request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String fecha = request.getParameter("fecha");
        String nuevafecha = fecha.replace("-", "");
        nuevafecha = nuevafecha.trim();
        int rangoIni = Integer.parseInt(request.getParameter("rangoIni"));
        int rangoFin = Integer.parseInt(request.getParameter("rangoFin"));
        String accion = "";
        GenerarPlanoCentralRiesgo  hilo = new GenerarPlanoCentralRiesgo();
        String codUnidadCR = "";
        String resultado = "";
       
        try {
            codUnidadCR = dao.obtenerCodigoUnidNegocioCR(unegocio);
            if(dao.generadoReporte(periodo,unegocio)){
                accion = "Visualizar";
                resultado = dao.obtenerReporteDatacredito(periodo,unegocio,fecha,rangoIni,rangoFin,accion,"S");
            }else{
                accion = "Generar";
                resultado = dao.obtenerReporteDatacredito(periodo,unegocio,fecha,rangoIni,rangoFin,accion,"N");
            }
            
            int cantReg = dao.obtenerCantidadReportados(periodo,unegocio);
            hilo.start(model,resultado,usuario,codUnidadCR+nuevafecha,codUnidadCR,cantReg,nuevafecha);
            if(accion.equals("Generar")){
                dao.insertarReporteHistorico(periodo,unegocio,fecha,rangoIni,rangoFin);
            }
            this.printlnResponse("OK", "application/text;");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR GUARDANDO REPORTADOS DATACREDITO. \n " + e.getMessage());
        }
    }

    private void refrescarReporteDatacredito() throws Exception {
        String periodo =  request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String fecha = request.getParameter("fecha");
        int rangoIni = Integer.parseInt(request.getParameter("rangoIni"));
        int rangoFin = Integer.parseInt(request.getParameter("rangoFin"));
        String nomNeg = request.getParameter("nomNeg");
        ArrayList<ReporteDatacredito> lista = new  ArrayList<ReporteDatacredito>();
        lista = dao.obtenerNoReportados(periodo,unegocio,fecha,rangoIni,rangoFin,nomNeg,"Visualizar");
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }
    
    private void cargarUnidadesDatacredito() {
        try {
            txtResp = dao.cargarUnidadesDatacredito();
        } catch(Exception e) {
            txtResp = "{mensaje:"+e.getMessage()+"}";
        }
    }
    
    
     private void buscarDetalleRop() throws Exception {
        int idRop = Integer.parseInt(request.getParameter("id_rop"));
        String negocio = request.getParameter("negocio");
      
        ArrayList<BeanGeneral> lista = dao.buscarDetalle(idRop, negocio);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }
     
      private void actualizarIF() throws Exception {
       
        String negocio = request.getParameter("cod_neg");
        String fecha_actual = request.getParameter("fecha_actual");
        double valor_if = Double.parseDouble(request.getParameter("valor_if"));
        String fechaif = request.getParameter("fechaif");      
       
        String json =dao.actualizarIFecha(fechaif, fecha_actual, valor_if, negocio);
   
        this.printlnResponse(json, "application/json;");
    }
     
    private void genPlanoAso() throws Exception {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        String json =dao.generarPlanoAsobancaria(info);
   
        this.printlnResponse(json, "application/json;");
    }
    
    private void genPlanoAsoEfecty() throws Exception {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        String json =dao.genPlanoAsoEfecty(info);
   
        this.printlnResponse(json, "application/json;");
    }

}
