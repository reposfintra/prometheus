/********************************************************************
 *      Nombre Clase.................   WorkGrpStdJobInsertAction.java
 *      Descripci�n..................   Actualiza el el archivo wgroup_stadard
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class WorkGrpStdJobInsertAction extends Action{
    
    public WorkGrpStdJobInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {        
               
        String[] stds = request.getParameterValues("c_stdSelec");
        String[] wgs = request.getParameterValues("c_wgSelec");
        
        String next="/jsp/masivo/workgroup_std/WorkGrpStdJInsert.jsp?msg=Relaci�n Work Groups - Standard Jobs realizada exitosamente.";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String cia = (String) session.getAttribute("Distrito");

	WgroupStd wg = new WgroupStd();
        
        wg.setBase(usuario.getBase());
        wg.setCreation_user(usuario.getLogin());
        wg.setDstrct(cia);
                
        try{
            
            for( int i=0; i<stds.length; i++){
                wg.setStd_job_no(stds[i]);
                
                if( wgs!=null) {
                    for( int j=0; j<wgs.length; j++){
                        String wgroup = wgs[j].substring(0, wgs[j].length()-1);
                        wg.setWork_group(wgroup);
                        
                        String reg_status = model.wgroup_stdjobSvc.existe(wgroup, stds[i]);
                        if( reg_status==null ){
                            model.wgroup_stdjobSvc.setWg(wg);
                            model.wgroup_stdjobSvc.ingresar();
                        } else if ( reg_status.compareTo("A")==0 ) {
                            model.wgroup_stdjobSvc.reg_statusUpdate( "", wg.getCreation_user(), wgroup, stds[i]);
                        }
                        
                    }
                    
                    model.wgroup_stdjobSvc.workGroups(stds[i]);
                    Vector vec = model.wgroup_stdjobSvc.getVector();
                    
                    for( int j=0; j<vec.size(); j++){
                        Hashtable ht = new Hashtable();
                        ht = (Hashtable) vec.elementAt(j);
                        String codigo = (String) ht.get("codigo");
                        
                        boolean esta = false;
                        for( int k=0; k<wgs.length; k++){
                            String wgroup = wgs[k].substring(0, wgs[k].length()-1);
                            
                            if( wgroup.compareTo(codigo)==0 ){
                                esta = true;
                            }
                            //////System.out.println(".......... (" + j + "," + k + ") comparando: " + codigo + " con " + wgroup + ", esta? " + esta);
                        }
                        
                        if(!esta){
                            model.wgroup_stdjobSvc.reg_statusUpdate("A", usuario.getLogin(), codigo, stds[i]);
                        }
                    }
                } else {
                    model.wgroup_stdjobSvc.workGroups(stds[i]);
                    Vector vec = model.wgroup_stdjobSvc.getVector();
                    
                    for( int j=0; j<vec.size(); j++){
                        Hashtable ht = new Hashtable();
                        ht = (Hashtable) vec.elementAt(j);
                        String codigo = (String) ht.get("codigo");
                        model.wgroup_stdjobSvc.reg_statusUpdate("A", usuario.getLogin(), codigo, stds[i]);
                    }                    
                }
                
            }
            
            request.setAttribute("cliente", null);
            request.setAttribute("origen", null);
            request.setAttribute("destino", null);
            
            model.wgroup_stdjobSvc.setVarCamposJS("var CamposJS = [];");
            model.wgroup_stdjobSvc.setVarCamposJStd("var CamposStd = [];");
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
