/***********************************************
 * Nombre clase: PublicacionEjecutarAction.java
 * Descripci�n: Accion para ingresar una discrepancia a la bd.
 * Autor: Jose de la rosa
 * Fecha: 18 de abril de 2006, 02:52 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import com.tsp.util.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

public class PublicacionEjecutarAction extends Action{
    private String procesoName;
    private String des;
    com.tsp.operation.model.Model model = new com.tsp.operation.model.Model ();
    /** Creates a new instance of PublicacionEjecutarAction */
    public PublicacionEjecutarAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        this.procesoName = " Publicaci�n ";
        this.des = " Proceso Automatico de Publicaciones ";
        String next="/"+request.getParameter ("carpeta")+"/"+request.getParameter ("pagina");
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String perfil = (String) session.getAttribute ("Perfil");
        String sw = request.getParameter ("sw");
        String fecha = com.tsp.util.Util.fechaActualTIMESTAMP ();
        try{
            if( sw.equals ("True") ){
                model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, usuario.getLogin ());
                Vector consulta = new Vector ();
                // Elimino las publicaciones
                consulta.add ( model.publicacionService.eliminarPublicacionesNoVencidas () );
                // Obtengo las publicaciones q no se han leido
                model.publicacionService.obtenerPublicacionesActuales ( model.publicacionService.fechaCorte () );
                Vector vec = model.publicacionService.getVecPublicaciones ();
                for(int i = 0; i< vec.size ();i++){
                    Publicacion p = (Publicacion) vec.elementAt (i);
                    Publicacion publi = new Publicacion ();
                    publi.setUsuario_creacion ( p.getUsuario_creacion () );
                    publi.setFecha_creacion ( p.getFecha_creacion () );
                    publi.setId_opcion ( p.getId_opcion () );
                    publi.setNumeroVeces ( 0 );
                    publi.setUsuario ( p.getUsuario () );
                    publi.setDistrito ( p.getDistrito () );
                    publi.setBase ( p.getBase () );
                    // Ingreso las publicaciones no vencidas
                    consulta.add ( model.publicacionService.insertarPublicacionNoVencida ( publi ) );
                }
                consulta.add ( model.publicacionService.actualizarPublicacionUsuarios () );
                model.despachoService.insertar ( consulta );
                model.publicacionService.setFechaUltimoCorte ( fecha );
                next += "?msg=Se ejecuto el proceso exitosamente";
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), usuario.getLogin (), "PROCESO EXITOSO");
            }
            else{
                model.publicacionService.historialPublicaciones ( usuario.getLogin () );
                Vector p = model.publicacionService.getVecDatos();
                
                request.setAttribute ("publicaciones", p );
                
                if ( p != null ){                    
                    Vector pp = (Vector)p.get(0);
                    Publicacion a = (Publicacion)pp.get(0);                    
                }
                
                
            }
        }catch (Exception e) {
            next += "?msg=Error durante el proceso, consulte en el LOG de proceso para conocer el error";
            try{
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),usuario.getLogin (),"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),usuario.getLogin (),"ERROR :");
                }catch(Exception p){    }
            }
        }
        this.dispatchRequest (next);
    }
    
}
