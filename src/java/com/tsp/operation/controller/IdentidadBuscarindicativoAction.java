/*************************************************************************
 * Nombre:        IdentidadBuscarindicativoAction.java            
 * Descripci�n:   Clase Action para buscar indicativo de la ciudad    
 * Autor:         Ing. Diogenes Antonio Bastidas Morales  
 * Fecha:         28 de noviembre de 2005, 04:15 PM                             
 * Versi�n        1.0                                      
 * Coyright:      Transportes Sanchez Polo S.A.            
 *************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class IdentidadBuscarindicativoAction extends Action{
    
    /** Creates a new instance of IdentidadBuscarindicativoAction */
    public IdentidadBuscarindicativoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String op = request.getParameter("opcion");
        String next = "/" + request.getParameter("carpeta") + "/" + request.getParameter("pagina") + "?ind=";
        HttpSession session = request.getSession();
        String pais = request.getParameter("pais");
        String codp;
        int ind;
        try {
            if (pais.equals("CO")) {
                codp = "57";
            } else if (pais.equals("VE")) {
                codp = "58";
            } else if (pais.equals("EC")) {
                codp = "593";
            } else if (pais.equals("EU")) {
                codp = "001";
            } else {
                codp = "0";
            }
            ind = model.ciudadService.buscarIndicativoCiudad(request.getParameter("ciudad"));
            next = next + "" + ind + "&codp=" + codp;
            
            if (op == null) { //Si no se env�a ninguna opcion, se envian peticiones desde registrarTelefono.jsp
                this.dispatchRequest(next);
            } else if (op.equals("obtenerIndicativo")) {
                response.setContentType("application/json");
                ServletOutputStream out;
                try {
                    String json = String.format("{\"indicativoPais\":\"%s\",\"indicativoCiudad\":\"%s\"}", codp, ind);

                    out = response.getOutputStream();
                    out.print(json);
                    out.close();
                } catch (IOException ex) {
                    System.err.println("Error al responder la petici�n de buscar indicativos identidad");
                }
            }
        } catch (SQLException e) {
            throw new ServletException(e.getMessage());
        }        
    }

}
