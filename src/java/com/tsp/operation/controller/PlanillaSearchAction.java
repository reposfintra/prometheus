/*
 * PlanillaSearchAction.java
 *
 * Created on 30 de noviembre de 2004, 10:15 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class PlanillaSearchAction extends Action{
    
    static Logger logger = Logger.getLogger(PlanillaSearchAction.class);
    String nombreR="";
    String nombreD="";
    /** Creates a new instance of PlanillaSearchAction */
    public PlanillaSearchAction() {
    }
    
    public String buscarStandard(String pla)throws ServletException{
        String sj="";
        try{
            model.remplaService.buscaRemPla(pla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rempla = model.remplaService.getRemPla();
                String no_rem= rempla.getRemesa();
                model.remesaService.buscaRemesa(no_rem);
                if(model.remesaService.getRemesa()!=null){
                    Remesa rem=model.remesaService.getRemesa();
                    sj=rem.getStdJobNo();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        return sj;
    }
    public void buscarNombres(String remitente, String destinatario)throws ServletException {
        try{
            String remitentes[]= remitente.split(",");
            for(int i=0; i<remitentes.length; i++){
                nombreR=model.remidestService.getNombre(remitentes[i])+",";
            }
            
            String destinatarios[]= destinatario.split(",");
            for(int i=0; i<destinatarios.length; i++){
                nombreD=model.remidestService.getNombre(destinatarios[i])+",";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/anularDespacho/buscarPlanilla.jsp";
        String planilla= request.getParameter("planilla").toUpperCase();
        model.remesaService.setRemesa(null);
        model.remesaService.setRm(null);
        model.planillaService.setPlanilla(null);
        String usuario_genero="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        boolean tieneTrafico = false;
        String base="";
        boolean remCumplida=false;
        
        try{
            if(request.getParameter("modificar")!=null){
                next="/colpapel/InicioModificar.jsp?mensaje=No se encontro la planilla "+planilla;
            }
            if(!model.planillaService.tieneFactura(planilla) || request.getParameter("nremesa")!=null){
                
                TreeMap t = new TreeMap();
                
                model.stdjobdetselService.setCiudadesDest(t);
                model.stdjobdetselService.setCiudadesOri(t);
                model.stdjobdetselService.setStdjobTree(t);
                
                
                if(!planilla.equals("")){
                    
                    if(request.getParameter("modificar")==null){
                        
                        if(request.getParameter("colpapel")!=null){
                            
                            next="/colpapel/anularDespacho.jsp?entro=ok";
                            model.planillaService.bucarColpapel(planilla);
                            
                            if(model.planillaService.getPlanilla()!=null){
                                model.movplaService.buscarCheques(planilla);
                            }
                        }
                        else{
                            model.planillaService.bucaPlanilla(planilla);
                        }
                        
                        
                        if(model.planillaService.getPlanilla()!=null){
                            
                            Planilla pla = model.planillaService.getPlanilla();
                            
                            //SE BUSCAN TODOS LOS REPORTES DE ESTA PLANILLA EN TRAFICO
                            tieneTrafico=model.rmtService.tieneReporte(pla.getNumpla());
                            base = pla.getBase();
                            
                            usuario_genero = pla.getDespachador();
                            String nomruta= model.planillaService.getRutas(pla.getNumpla());
                            pla.setCodruta(pla.getRuta_pla());
                            pla.setRuta_pla(nomruta);
                            planilla = pla.getNumpla();
                            model.planillaService.setPlanilla(pla);
                            
                            request.setAttribute("planilla",pla);
                            
                            List listTabla = model.tbltiempoService.getTblTiempos(this.buscarStandard(planilla));
                            request.setAttribute("fechas",listTabla);
                            
                            model.movplaService.buscaMovpla(planilla,"01");
                            if(model.movplaService.getMovPla()!=null){
                                Movpla movpla= model.movplaService.getMovPla();
                                request.setAttribute("movpla",movpla);
                            }
                            
                            model.planillaService.bucaPlaaux(planilla);
                            if(model.planillaService.getPlaaux()!=null){
                                Plaaux plaaux= model.planillaService.getPlaaux();
                                request.setAttribute("plaaux", plaaux);
                            }
                            
                            model.remplaService.buscaRemPla(planilla);
                            if(model.remplaService.getRemPla()!=null){
                                RemPla rempla = model.remplaService.getRemPla();
                                String no_rem= rempla.getRemesa();
                                model.remesaService.buscaRemesa(no_rem);
                                if(model.remesaService.getRemesa()!=null){
                                    Remesa rem=model.remesaService.getRemesa();
                                    remCumplida = rem.getReg_status().equals("C");
                                    this.buscarNombres(rem.getRemitente(), rem.getDestinatario());
                                    rem.setRemitente(nombreR);
                                    rem.setDestinatario(nombreD);
                                    request.setAttribute("remesa",rem);
                                    
                                    List planillas= model.remesaService.buscarPlanillas(no_rem);
                                    request.setAttribute("planillas",planillas);
                                    
                                    List lremesas = new LinkedList();
                                    List remesas = model.planillaService.buscarRemesas(planilla);
                                    Iterator it=remesas.iterator();
                                    while (it.hasNext()){
                                        rem = (Remesa) it.next();
                                        this.buscarNombres(rem.getRemitente(), rem.getDestinatario());
                                        rem.setRemitente(nombreR);
                                        rem.setDestinatario(nombreD);
                                        lremesas.add(rem);
                                    }
                                    model.remesaService.setRemesasList(lremesas);
                                    
                                    request.setAttribute("remesas" , lremesas);
                                    
                                    
                                }
                            }
                            
                        }
                        //AQUI VAN LAS CONDICIONES PARA ANULAR
                        boolean sw = false;
                        String mensaje = "";
                        if(request.getParameter("nremesa")==null){
                            if(usuario_genero.equalsIgnoreCase(usuario.getLogin()) || usuario.getId_agencia().equalsIgnoreCase("OP") ){
                                sw = true;
                            }
                            else{
                                model.planillaService.setPlanilla(null);
                                model.remesaService.setRemesa(null);
                                model.remesaService.setRm(null);
                                model.movplaService.setLista(null);
                                mensaje ="La planilla que selecciono para anular no fue creada por " +
                                " usted, fue creada por : "+usuario_genero+". Por lo tanto no puede anularla.";
                                
                                next="/colpapel/anularDespacho.jsp?mensaje="+mensaje;
                            }
                            model.tablaGenService.obtenerRegistro("USUARIOANU",usuario.getLogin(), "");
                            
                            if(tieneTrafico && request.getParameter("nremesa")==null && model.tablaGenService.getTblgen()==null){
                                model.planillaService.setPlanilla(null);
                                model.remesaService.setRemesa(null);
                                model.remesaService.setRm(null);
                                model.movplaService.setLista(null);
                                mensaje ="La planilla que selecciono para anular ya tiene reportes en trafico y no puede ser anulada.";
                                
                                next="/colpapel/anularDespacho.jsp?mensaje="+mensaje;
                                
                            }
                            if(remCumplida && request.getParameter("aplanilla")==null){
                                model.planillaService.setPlanilla(null);
                                model.remesaService.setRemesa(null);
                                model.remesaService.setRm(null);
                                model.movplaService.setLista(null);
                                mensaje =    "La remesa del despacho a anular ya esta cumplida.";
                                next="/colpapel/anularDespacho.jsp?mensaje="+mensaje;
                            }
                            if(request.getParameter("aplanilla")!=null){
                                if(sw){
                                    next="/colpapel/anularPlanilla.jsp?entro=ok";
                                }
                                else{
                                    next="/colpapel/anularPlanilla.jsp?mensaje="+mensaje;
                                }
                                if(tieneTrafico && model.tablaGenService.getTblgen()==null){
                                    model.planillaService.setPlanilla(null);
                                    model.remesaService.setRemesa(null);
                                    model.remesaService.setRm(null);
                                    model.movplaService.setLista(null);
                                    mensaje =    "La planilla que selecciono para anular ya tiene reportes en trafico y no puede ser anulada.";
                                    next="/colpapel/anularPlanilla.jsp?mensaje="+mensaje;
                                }
                                
                                
                            }
                        }
                        if(request.getParameter("nremesa")!=null){
                            if("".equals(base)){
                                model.planillaService.setPlanilla(null);
                                model.remesaService.setRemesa(null);
                                model.remesaService.setRm(null);
                                model.movplaService.setLista(null);
                                next="/colpapel/agregarRemesa.jsp?mensaje=La planilla seleccionada no fue elaborada en la web.";
                            }
                            else{
                                next="/colpapel/agregarRemesa.jsp";
                            }
                        }
                    }
                    //SI LA ACCION VIENE DE LA PAGINA DE MODIFICAR.
                    else{
                        
                        
                        model.planillaService.listaPlanillas(planilla);
                        
                        Vector plas = model.planillaService.getPlas();
                        logger.info("Cantidad encontrada... "+plas.size());
                        if(plas.size()==1 || request.getParameter("remesa")!=null){
                            
                            logger.info("Tengo una remesa... "+request.getParameter("remesa"));
                            
                            Planilla p = (Planilla) plas.elementAt(0);
                            model.anticiposService.searchAnticiposProveedor(p.getNumpla());
                            float total = 0;
                            for(int a = 0; a<model.anticiposService.getAnticiposProv().size();a++){
                                Anticipos ant = (Anticipos) model.anticiposService.getAnticiposProv().elementAt(a);
                                total= ant.getValor()+total;
                            }
                            
                            String numrem = request.getParameter("remesa")!=null?request.getParameter("remesa"):p.getNumrem();
                            
                            logger.info("La remesa es... "+numrem);
                            logger.info("Total de anticipos por proveedor "+total);
                            model.planillaService.bucarColpapel(p.getNumpla());
                            Planilla pla = model.planillaService.getPlanilla();
                            if(total ==0){
                                model.movplaService.buscarAnticipos(p.getNumpla());
                            }else{
                                model.movplaService.setLista(null);
                            }
                            model.remesaService.buscaRemesa(numrem);
                            Remesa r = model.remesaService.getRemesa();
                            logger.info("Ya busque la remesa... "+numrem);
                            String sj="";
                            if(r!=null){
                                logger.info("Encontre la remesa "+numrem);
                                sj=r.getStdJobNo();
                                //LLENAMOS LOS VALORES DE LA REMESA, QUE VIENEN DE MOVREM
                                Movrem mov = model.facturaService.buscarMovrem(usuario.getDstrct(),numrem, "TOTAL");
                                if (mov!=null){
                                    r.setVlrrem    ( (float)mov.getVlrrem()     );
                                    r.setVlrrem2   ( (float)mov.getVlrrem2()    );
                                    r.setQty_value( (float)mov.getQty_value()  );
                                    r.setPesoReal  ( (float)mov.getPesoreal()   );
                                    r.setTasa      ( mov.getTasa()       );
                                    System.out.println("Peso real remesa "+mov.getPesoreal());
                                }
                                
                                model.remesaService.setRemesa(r);
                                model.remesaService.setRm(r);
                                logger.info("Standard "+sj);
                            }
                            if(pla!=null){
                                pla.setInventario(model.discrepanciaService.obtenerProductos(planilla));
                                model.cliente_ubicacionService.listarTodasUbicaciones("000"+sj.substring(0,3));
                                
                            }
                            model.stdjobdetselService.searchStdJob(sj);
                            model.sjextrafleteService.setVlrtotalCR(0);
                            model.sjextrafleteService.setVlrtotalE(0);
                            model.sjextrafleteService.listaExtraFletesAplicados(p.getNumpla(),numrem,sj,usuario.getId_agencia());
                            model.sjextrafleteService.listaCostosAplicados(p.getNumpla(),numrem,sj,usuario.getId_agencia());
                            
                            model.planillaService.setPlanilla(pla);
                            model.tblgensvc.buscarListaCodigo("AUTEXFLETE", "000"+sj.substring(0,3));
                            
                            next="/colpapel/modificarDespacho.jsp?planilla="+p.getNumpla()+"&remesa="+p.getNumrem()+"&antprov="+total;
                            
                        }
                        else if(plas.size()>1){
                            next="/colpapel/InicioModificar.jsp?lista=ok";
                        }
                        else if(plas.size()==0){
                            next="/colpapel/InicioModificar.jsp?mensaje=No se encontro la planilla "+planilla;
                        }
                        
                    }
                }
                
                
            }else{
                next="/colpapel/InicioModificar.jsp?mensaje=La planilla "+planilla+" ya tiene generada OP, no puede modificarla.";
                if(request.getParameter("colpapel")!=null){
                    next="/colpapel/anularDespacho.jsp?mensaje=La planilla "+planilla+" ya tiene generada OP, no puede anularla.";
                }
                if(request.getParameter("aplanilla")!=null)
                    next="/colpapel/anularPlanilla.jsp?mensaje=La planilla "+planilla+" ya tiene generada OP, no puede anularla.";
                
                if(request.getParameter("nremesa")!=null)
                    next="/colpapel/agregarRemesa.jsp?mensaje=La planilla "+planilla+" ya tiene generada OP, no puede agregar remesas.";
                
            }
            logger.info("NEXT: "+next);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
}
