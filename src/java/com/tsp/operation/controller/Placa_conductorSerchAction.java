/*
 * Placa_conductorSerchAction.java
 *
 * Created on 30 de septiembre de 2005, 03:42 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

/**
 *
 * @author  Jose
 */
public class Placa_conductorSerchAction extends Action{
    
    /** Creates a new instance of Placa_conductorSerchAction */
    public Placa_conductorSerchAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        String exp = (request.getParameter("exportar")!=null)?request.getParameter("exportar"):"";
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario ) session.getAttribute ( "Usuario" );
        if (exp.equals("true")){
                 try{
                    HiloConsultaHistorial  hilo = new HiloConsultaHistorial();
                    hilo.start ( model.planillaService.ReporteVector(), usuario.getLogin ());
                    next="/placas/PlacaBuscar.jsp?msg=El Proceso de Exportacion inicio con exito...";
                 }catch (Exception e){
                    e.printStackTrace();
                 }
        }else{    
                String placa = request.getParameter("c_placa").toUpperCase();
                String fecha_fin = (request.getParameter("c_fecha_fin")!=null)?request.getParameter("c_fecha_fin").toUpperCase():"";
                String fecha_inicio = (request.getParameter("c_fecha_inicio")!=null)?request.getParameter("c_fecha_inicio").toUpperCase():"";
                String agencia_origen = (request.getParameter("c_agencia_origen").toUpperCase());
                String agencia_destino = (request.getParameter("c_agencia_destino").toUpperCase());
                String agencia_despacho = (request.getParameter("c_agencia_despacho").toUpperCase());
                String cedula = request.getParameter("c_cedula");
                String listar = (String) request.getParameter("listar");
                if((fecha_inicio.equals(""))&&(fecha_fin.equals(""))){
                    fecha_fin = Util.getFechaActual_String(4);
                    fecha_inicio = com.tsp.util.Util.fechaFinal(fecha_fin,-30);
                }
                try{                                
                    model.planillaService.obtenerPLacaConductor(placa, fecha_inicio, fecha_fin, agencia_origen, agencia_destino, agencia_despacho, cedula);
                    int total = model.planillaService.getTotal();
                    next="/placas/PlacaListar.jsp?total="+total;    
                    //next = Util.LLamarVentana(next, "Listar Viajes");
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
        }        
        this.dispatchRequest(next);
    }
    
}
