/****************************************************************************
* Nombre .................Concepto_equipoUpdateAction.java                  *
* Descripción.............Accion para modificar un concepto equipo a la bd. *
* Autor...................Ing. Jose de la rosa                              *
* Fecha Creación..........16 de diciembre de 2005, 11:19 PM                 *
* Modificado por..........LREALES                                           *
* Fecha Modificación......7 de junio de 2006, 12:02 PM                      *
* Versión.................1.0                                               *
* Coyright................Transportes Sanchez Polo S.A.                     *
*****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Concepto_equipoUpdateAction extends Action{
    
    /** Creates a new instance of Concepto_equipoUpdateAction */
    public Concepto_equipoUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/equipos/concepto_equipo/concepto_equipoModificar.jsp?&reload=ok";
        
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario ) session.getAttribute("Usuario");
        String dstrct = ( String ) session.getAttribute("Distrito");
        String descripcion = request.getParameter("c_descripcion");
        String codigo = request.getParameter("c_codigo").toUpperCase (); 
        
        try{
            
            Concepto_equipo concepto = new Concepto_equipo();
            
            concepto.setCodigo ( codigo );
            concepto.setDescripcion ( descripcion );
            concepto.setUsuario_modificacion( usuario.getLogin().toUpperCase() );
            
            model.concepto_equipoService.updateConcepto_equipo ( concepto );
            
            request.setAttribute( "mensaje", "La información ha sido modificada exitosamente!" );
            
            model.concepto_equipoService.searchConcepto_equipo( codigo );

        } catch ( SQLException e ){
            
            throw new ServletException( e.getMessage() );
            
        }
        
        this.dispatchRequest( next );   
        
    }
    
}