/*
 * Nombre        TramoAnularAction.java
 * Autor         Ing Jesus Cuestas
 * Fecha         26 de junio de 2005, 11:15 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class TramoAnularAction extends Action{
    
    /** Creates a new instance of TramoAnularAction */
    public TramoAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/tramo/"+request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");        

        try{
            String cia = request.getParameter("cia");
            String origen = request.getParameter("origen");
            String destino = request.getParameter("destino");
            
            if(request.getParameter("anultiket")==null){//anular tramo
                Tramo t = new Tramo();
                t.setDstrct(cia);
                t.setOrigen(origen);
                t.setDestino(destino);
                t.setUsuario(usuario.getLogin());
                
                model.tramoService.setTramo(t);
                model.tramoService.anularTramo();
                next+="?tipo=Tramo";
            }
            else{//anular tiquete
                String ticket_id = request.getParameter("c_tiket");
                Tramo_Ticket t = new Tramo_Ticket();
                t.setCia(cia);
                t.setOrigen(origen);
                t.setDestino(destino);
                t.setTicket_id(ticket_id);
                t.setUsuario(usuario.getLogin());
                
                model.tiketService.setTicket(t);
                model.tiketService.anularTramo_Ticket();
                next="/jsp/trafico/tramo/maTiket.jsp?tipo=Tiquete&reload=ok";
                
             
                model.tramoService.buscarTramo(cia, origen, destino);
                model.unidadTrfService.listarUnidadesxDistancia("D");
                model.unidadTrfService.listarUnidadesxTiempo("T");
                model.unidadTrfService.listarUnidadesxVolumen("V");
                model.tiketService.listarTramos_Ticket(cia,origen,destino);
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
