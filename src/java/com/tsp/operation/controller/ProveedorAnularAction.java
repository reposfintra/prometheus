/********************************************************************
 *      Nombre Clase.................   ProveedorAnularAction.java
 *      Descripci�n..................   Anula un registro en el archivo proveedor
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.09.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Andres
 */
public class ProveedorAnularAction extends Action{
        
        /** Creates a new instance of ProveedorAnularAction */
        public ProveedorAnularAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";

                String nit = request.getParameter("nit");
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        
                        Proveedor prov = new Proveedor();
                        prov.setC_agency_id(request.getParameter("c_agency_id"));
                        prov.setC_autoretenedor_iva(request.getParameter("c_autoretenedor_iva")) ;
                        prov.setC_agente_retenedor(request.getParameter("c_agente_retenedor"));
                        prov.setC_autoretenedor_ica( request.getParameter("c_autoretenedor_ica"));
                        prov.setC_autoretenedor_rfte(request.getParameter("c_autoretenedor_rfte"));
                        prov.setC_banco_transfer(request.getParameter("c_banco_transfer"));
                        prov.setC_branch_code(request.getParameter("c_branch_code"));
                        prov.setC_bank_account(request.getParameter("c_bank_account"));
                        prov.setC_clasificacion(request.getParameter("c_clasificacion"));
                        prov.setC_codciudad_cuenta(request.getParameter("c_codciudad_cuenta"));
                        prov.setC_gran_contribuyente(request.getParameter("c_gran_contribuyente"));
                        prov.setC_idMims(request.getParameter("c_idMims"));
                        prov.setC_nit(request.getParameter("c_nit"));
                        prov.setC_payment_name(request.getParameter("c_payment_name"));
                        prov.setC_numero_cuenta(request.getParameter("c_numero_cuenta"));
                        prov.setC_sucursal_transfer(request.getParameter("c_sucursal_transfer"));
                        prov.setC_tipo_cuenta(request.getParameter("c_tipo_cuenta"));
                        prov.setC_tipo_doc(request.getParameter("c_tipo_doc"));
                        
                        prov.setBase(usuario.getBase());
                        prov.setUsuario_creacion(usuario.getLogin());
                        prov.setUsuario_modificacion(usuario.getLogin());
                        prov.setDistrito(request.getParameter("distrito"));
 
                        model.proveedorService.anularProveedor(prov.getC_nit(), prov.getDistrito());                        

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }   
        
}
