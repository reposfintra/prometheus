/*
 * ReporteOcsConAnticiposSinReporteAction.java
 *
 * Created on 6 de marzo de 2007, 07:30 PM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HReporteOcsConAnticipoSinReporte;
/**
 *
 * @author  equipo
 */
public class ReporteOcsConAnticiposSinReporteAction extends Action {
    
    /** Creates a new instance of ReporteOcsConAnticiposSinReporteAction */
    public ReporteOcsConAnticiposSinReporteAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            
            String opcion = Util.coalesce( request.getParameter("opcion"), "");
            String fechaI = Util.coalesce( request.getParameter("fechaini"), "");
            String fechaF = Util.coalesce( request.getParameter("fechafin"), "");
            String next   = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/Consulta.jsp";
            
            //System.out.println("Opcion : " + opcion);
            if (opcion.equalsIgnoreCase("LISTAR")){
                model.reporteOcAnt.buscarOcsConAnticiposSinReporte(fechaI, fechaF, usuario.getDstrct());
                java.util.Vector d = model.reporteOcAnt.getVector();
                next = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/Listado.jsp";
                
                if ( d==null || d.isEmpty() )
                    request.setAttribute("msg", "No se encontraron datos para el periodo indicado.");
                
                
                //System.out.println(d);
            } else if (opcion.equalsIgnoreCase("EXCEL")){
                HReporteOcsConAnticipoSinReporte h = new HReporteOcsConAnticipoSinReporte();
                h.start(model, fechaI, fechaF, usuario);
                next = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/Listado.jsp";
                request.setAttribute("msg", "Proceso de exportacion iniciado con exito.");
            }
            
            
            this.dispatchRequest(next);
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new javax.servlet.ServletException(ex.getMessage());
        }
    }
    
}
