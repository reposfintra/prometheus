/*
 * MigracionRemesasAnuladasAction.java
 *
 * Created on 16 de julio de 2005, 02:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;


/**
 *
 * @author  Tito Andr�s Maturana
 */
public class MigracionRemesasAnuladasAction extends Action{
    
    /** Creates a new instance of MigracionRemesasAnuladasAction */
    public MigracionRemesasAnuladasAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/migracion/migracionRemesasAnuladas.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Vector reman = model.mraSvc.obtenerRemesasAnuladas(usuario.getLogin());        
        Vector reman = model.mraSvc.obtenerRemesasAnuladas();  
        String linea;
        Vector lineas = new Vector();
        for(int i=0; i<reman.size(); i++){
            RemesaAnulada remesa_anulada = (RemesaAnulada) reman.elementAt(i);
            linea = remesa_anulada.getNumero_remesa() + "," + remesa_anulada.getFecha_creacion() + "," +
                remesa_anulada.getUsuario_creacion() + ",AN";
            ////System.out.println(linea);
            lineas.add(linea);
        }
        
        //sandrameg 190905
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        
        String nombre_archivo = "";
        String ruta = path + "/exportar/migracion/" + usuario.getLogin() + "/";
        String fecha_actual = Util.getFechaActual_String(6);
          
        //ruta += fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
        nombre_archivo = path + "/exportar/migracion/" + usuario.getLogin()+ "/anula620" + fecha_actual.replace(":","").replace("/","").replace(' ','_') + ".csv";
        
        model.mraSvc.actualizarRegistros(usuario.getLogin());
        /*HMigracionRemesasAnuladas hilo = new HMigracionRemesasAnuladas();
        hilo.start(reman);*/        
        HGenerarCSV hilo = new HGenerarCSV();
        hilo.start(lineas, ruta, nombre_archivo);
                           
        request.setAttribute("msg","Migraci�n de Remesas Anuladas se realiz� de manera exitosa.");
                        
        this.dispatchRequest(next);
    }
    
}
