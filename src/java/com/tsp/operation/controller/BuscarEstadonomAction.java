/*
 * BuscarEstadonomAction.java
 *
 * Created on 4 de marzo de 2005, 09:22 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  DIBASMO
 */
public class BuscarEstadonomAction extends Action {
    
    /** Creates a new instance of BuscarEstadonomAction */
    public BuscarEstadonomAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String nompais = (request.getParameter("nompais").toUpperCase());
        String nomestado = (request.getParameter("nomestado").toUpperCase());
        String zona = (request.getParameter("zona").toUpperCase());
        HttpSession session = request.getSession();
        session.setAttribute("nompais", nompais);
        session.setAttribute("nomestado", nomestado);
        session.setAttribute("zona", zona);
       
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
