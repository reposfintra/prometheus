/********************************************************************
 *      Nombre Clase.................   PosBancariaObtenerAction.java
 *      Descripci�n..................   Obtiene un registro del archivo posicion_bancaria
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *      modificado...................   egonzalez2014
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Andres
 */
public class PosBancariaObtenerAction extends Action{
       
    /** Creates a new instance of PosBancariaRefreshAction */
    public PosBancariaObtenerAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/cxpagar/posbancaria/PosBancariaUpdate.jsp?mensaje=";
        
        String banco = request.getParameter("banco");
        String sucursal = request.getParameter("sucursal").replaceAll("_", " ");
        String agency_id = request.getParameter("agency_id");
        String fecha = request.getParameter("fecha");
        
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        String cia = (String) session.getAttribute("Distrito");
        
        //////System.out.println("................... fecha: " + fecha);
//        try{
//            model.posbancariaSvc.obtener(cia, agency_id, banco, sucursal, fecha);
//            
//            if( model.posbancariaSvc.getPosbanc()==null){
//                PosBancaria posb = new PosBancaria();
//                posb.setCreation_user(user.getLogin());
//                posb.setDstrct(cia);
//                posb.setBase(user.getBase());
//                posb.setAgency_id(agency_id);
//                posb.setBranch_code(banco);
//                posb.setBank_account_no(sucursal);
//                posb.setAnticipos(0);
//                posb.setProveedores(0);
//                posb.setCupo(0);
//                posb.setSaldo_anterior(0);
//                posb.setSaldo_inicial(0);
//                posb.setAgency_id(agency_id);
//                posb.setReg_status("");
//                posb.setFecha(fecha);
//                
//                model.posbancariaSvc.setPosbanc(posb);
//            } 
//            
//            String nomagencia = ""; 
//            Agencia agc = model.agenciaService.obtenerAgencia(agency_id);
//            if ( agc !=null )
//                nomagencia = agc.getNombre();
//            request.setAttribute("agencia", nomagencia);
//            request.setAttribute("agency_id", agency_id);
//            request.setAttribute("banco", banco);
//            request.setAttribute("sucursal", sucursal);
//            request.setAttribute("fecha", fecha); 
//        }catch (SQLException e){
//            throw new ServletException(e.getMessage());
//        }
        
        this.dispatchRequest(next);
    }
    
}
