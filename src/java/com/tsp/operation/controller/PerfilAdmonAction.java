/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.PerfilAdmonDAO;
import com.tsp.operation.model.DAOS.impl.PerfilAdmonImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;

/**
 *
 * @author egonzalez
 */
public class PerfilAdmonAction extends Action {

    private final int GUARDAR_PERFIL = 0;
    
    private static PerfilAdmonDAO dao = null;
    private JsonObject respuesta = null;
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            dao = new PerfilAdmonImpl();
            int opcion = (request.getParameter("opcion") != null)
                       ? Integer.parseInt(request.getParameter("opcion"))
                       : -1;
            switch(opcion) {
                case GUARDAR_PERFIL:
                    guardar();
                    break;
                default:
                    throw new Exception("La opcion de orden "+opcion+" no valida");
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                response.setContentType("application/json; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println((new Gson()).toJson(respuesta));
            } catch (Exception e) {
            }
        }
    }
    
    private void guardar() {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion")); 
        info.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        respuesta = dao.modificar(info);
    }
    
    public JsonObject buscarOpciones(String id) {
        if(dao == null) dao = new PerfilAdmonImpl();
        return dao.buscarOpciones(id);
    }
}
