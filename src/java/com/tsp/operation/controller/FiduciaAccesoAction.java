/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

/**
 *
 * @author Alvaro
 */




import java.util.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;

import com.tsp.operation.model.threads.*;

import com.tsp.util.LogWriter;
import java.util.List;
import com.tsp.util.Util;







public class FiduciaAccesoAction  extends Action {
    
    public FiduciaAccesoAction() {
    }    
    
    public void run() throws javax.servlet.ServletException {

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();

        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        String aceptarDisable = "";

        try {
            

            
            // ---------------------------------------------------------------------------------------------------------------------------------------------
            // DEFINICION DEL LOG DE ERROR
            LogWriter logWriter = new LogWriter("com/tsp/util/connectionpool/db", usuario.getLogin(), "/Fiducia NM ", usuario.getLogin() );
            // FIN DEFINICION DEL LOG DE ERROR
            // ---------------------------------------------------------------------------------------------------------------------------------------------

            logWriter.tituloInicial("PROCESOS ASOCIADOS A FIDUCIAS");

            
            
            //  EVENTO  :  CREAR_LISTA_NM  de iniciarNM.jsp
            //  Proceso para seleccionar las NM que se pasaran a PM


            if (evento.equalsIgnoreCase("CREAR_LISTA_NM")) {   


                logWriter.tituloInicial("SELECCION DE NM PARA TRASLADAR A PM");

                String fiducia =  request.getParameter("id_fiducia_inicial");
                String valor_fiducia = request.getParameter("valor_fiducia_inicial");
                
                
                model.fiduciaService.getFacturaNM();
                List listaFacturaNM = model.fiduciaService.getListaFacturaNM();

                if(listaFacturaNM.size()!=0) {

                    next  = "/jsp/consorcio/listaNM.jsp?fiducia="+fiducia+"&valor_fiducia="+valor_fiducia+"&msj=";
                    msj   = "";
                }
                else {
                    msj   = "No existen facturas NM a trasladar a PM";
                    next  = "/jsp/consorcio/iniciarNM.jsp?msj=";
                }                
                

                logWriter.tituloFinal();

                aceptarDisable = "S";


            } // Fin de CREAR_LISTA_NM     
            
            
            
            
            //  EVENTO  :  CREAR_LISTA_PM  de iniciarNM.jsp
            //  Proceso para seleccionar las PM que se pasaran a PM de Fintra


            if (evento.equalsIgnoreCase("CREAR_LISTA_PM") || evento.equalsIgnoreCase("CREAR_LISTA_RM")) {   


                logWriter.tituloInicial("SELECCION DE PM PARA TRASLADAR A PM Fintra");

                String fiducia =  request.getParameter("id_fiducia_inicial");
                String valor_fiducia = request.getParameter("valor_fiducia_inicial");
                String tipo = evento.substring(12);
                
                model.fiduciaService.getFacturaPM(valor_fiducia, tipo);
                List listaFacturaNM = model.fiduciaService.getListaFacturaNM();

                if(listaFacturaNM.size()!=0) {

                    next  = "/jsp/consorcio/listaNM.jsp?fiducia="+fiducia+"&valor_fiducia="+valor_fiducia+"&retorno="+tipo+"&msj=";
                    msj   = "";
                }
                else {
                    msj   = "No existen facturas NM a trasladar a PM";
                    next  = "/jsp/consorcio/iniciarNM.jsp?msj=";
                }                
                

                logWriter.tituloFinal();

                aceptarDisable = "S";


            } // Fin de CREAR_LISTA_PM     
            
            
            //  EVENTO  :  CREAR_LISTA_PMF  de iniciarNM.jsp
            //  Proceso para seleccionar las PM de fintra que se pasaran a PM de Fiducia


            if (evento.equalsIgnoreCase("CREAR_LISTA_PMF")) {   


                logWriter.tituloInicial("SELECCION DE PM Fintra PARA TRASLADAR A PM Fiducia");

                String fiducia =  request.getParameter("id_fiducia_inicial");
                String valor_fiducia = "FIDFIV";
                String tipo = evento.substring(12,14);
                
                model.fiduciaService.getFacturaPM(valor_fiducia, tipo);
                List listaFacturaNM = model.fiduciaService.getListaFacturaNM();

                if(listaFacturaNM.size()!=0) {

                    next  = "/jsp/consorcio/listaNM.jsp?fiducia="+fiducia+"&valor_fiducia="+valor_fiducia+"&retorno="+tipo+"F&msj=";
                    msj   = "";
                }
                else {
                    msj   = "No existen facturas NM a trasladar a PM";
                    next  = "/jsp/consorcio/iniciarNM.jsp?msj=";
                }                
                

                logWriter.tituloFinal();

                aceptarDisable = "S";


            } // Fin de CREAR_LISTA_PMF     
            


            // EVENTO : GENERAR_FACTURAR_NM en  listaNM.jsp
            // Procesa las facturas NM que se pasaran a PM
            
            if(evento.equals("GENERAR_FACTURAR_PM")) {
                
                String[] id_factura  = request.getParameterValues("ckEstado");
                String[] fiducia     = request.getParameterValues("id_fiducia");                
                String retorno      = request.getParameter("retorno");
                
                
                HFiducia hilo = new HFiducia();
                hilo.setRetorno(retorno);
                hilo.start(model, usuario, distrito, id_factura, fiducia, logWriter);
                
             
                aceptarDisable = "S";
                msj   = "La generacion de las facturas PM se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/consorcio/iniciarNM.jsp?aceptarDisable="+aceptarDisable+"&msj=";                
                
                   
                
            } // Fin de evento GENERAR_FACTURAR_PM
                


            
            next += msj ;
            this.dispatchRequest(next);

        }catch (Exception e) {
            Util.imprimirTrace(e);
        }

    }
    
    
    
}
