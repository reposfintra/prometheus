/*
 * ReporteViajesGenerarAction.java
 *
 * Created on 14 de junio de 2005, 08:34 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Karen
 */
public class ReporteViajesGenerarAction extends Action{
    
    /** Creates a new instance of ReporteViajesGenerarAction */
    public ReporteViajesGenerarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/informes/ListarReportesViajes.jsp";
        String fech1 = request.getParameter("fechai");
        String fech2 = request.getParameter("fechaf");
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            model.planillaService.reporteViajes(fech1,fech2);
            Vector reporte = model.planillaService.getReporte();
            
            HReportePlacaViajes Report = new HReportePlacaViajes();
            Report.start(reporte,fech1,fech2, usuario.getLogin());
            
            ////System.out.println("fecha inicial:"+fech1);
            ////System.out.println("fecha final:"+fech2);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
