/****************************************************************************
 * Nombre clase :      VerConceptosAction.java                              *
 * Descripcion :       Action del VerConceptosAction.java                   *
 * Autor :             LREALES                                              *
 * Fecha :             12 de julio de 2006, 05:42 PM                        *
 * Version :           1.0                                                  *
 * Copyright :         Fintravalores S.A.                              *
 ****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class VerConceptosAction extends Action {
    
    /** Creates a new instance of VerConceptosAction */
    public VerConceptosAction() {
    }    
    
    public void run() throws ServletException {
        
        String next = "/jsp/equipos/concepto_equipo/verConceptos.jsp";
        
        try{
            
            model.concepto_equipoService.searchDetallesTBLCON();

            int cont = model.concepto_equipoService.getOtro_vector().size();

            if ( cont > 0 )
                next = "/jsp/equipos/concepto_equipo/verConceptos.jsp";
        
        } catch ( SQLException e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
                        
    }
    
}