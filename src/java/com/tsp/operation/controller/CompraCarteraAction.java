/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.CompraCarteraDAO;
import com.tsp.operation.model.DAOS.impl.CompraCarteraImpl;
import com.tsp.operation.model.DAOS.impl.GarantiasCreditosImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.UtilFinanzas;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author egonzalez
 */
public class CompraCarteraAction extends Action{
    
    final int CARGAR_ARCHIVOS = 1;
    final int BUSCAR_INFO_CARTERA=2;
    final int CREAR_NEGOCIO_CARTERA=3;
    final int BUSCAR_NEGOCIO_CARTERA=4;
    final int DESCARTAR_NEGOCIO=5;
    final int LIMPIAR_CARGA_CARTERA=6;
    final int CREAR_DOCUMENTOS_CARTERA=7;
    
    Usuario usuario=null;
    CompraCarteraDAO ccdao; 
    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");          
            ccdao=new CompraCarteraImpl(usuario.getBd());
            int opcion = Integer.parseInt(request.getParameter("opcion"));
            switch(opcion){
                case CARGAR_ARCHIVOS:
                    cargarArchivo();
                    break;
                case BUSCAR_INFO_CARTERA:
                    cargarInfoCartera();
                    break;
                case CREAR_NEGOCIO_CARTERA:
                    crearNuevoNegocioCartera();
                    break;
                case BUSCAR_NEGOCIO_CARTERA:
                   buscarNegociosCartera();
                    break;    
                case DESCARTAR_NEGOCIO:
                    descartarNegocioCartera();
                    break;
                case LIMPIAR_CARGA_CARTERA:
                   eliminarCargaCartera();
                    break;
                case CREAR_DOCUMENTOS_CARTERA:
                   crearDocumentosCartera();
                    break;
                    
            }
        } catch (Exception ex) {
            Logger.getLogger(CompraCarteraAction.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
    
     private void cargarArchivo() throws Exception {

        String respuestaJson = "{}";
        try {

            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((usuario != null) ? usuario.getLogin() : "anonimo"));

            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)
            MultipartRequest mreq = new MultipartRequest(request, ruta, 4096*2048, new DefaultFileRenamePolicy());
            String filetypeCredito = mreq.getContentType("archivo_credito");
            String filetypePersona = mreq.getContentType("archivo_persona");
            String filetypeEstudiante = mreq.getContentType("archivo_estudiante")==null?"":mreq.getContentType("archivo_estudiante");
          
            //Validamos la extensi�n del archivo que va a cargarse
            if (validarExtArchivo(filetypeCredito, "archivo_credito") && 
                validarExtArchivo(filetypePersona, "archivo_persona") &&
                validarExtArchivo(filetypeEstudiante, "archivo_estudiante")) {

                String filenameCredito = mreq.getFilesystemName("archivo_credito");
                String filenamePersona = mreq.getFilesystemName("archivo_persona");
                String filenameEstudiante = mreq.getFilesystemName("archivo_estudiante");
                
                //validamos los nombres de archivos...
                if (validarNombreArchivo(filenameCredito, filetypeCredito, "archivo_credito")) {
                    if (validarNombreArchivo(filenamePersona, filetypePersona, "archivo_persona")) {
                        if (validarNombreArchivo(filenameEstudiante, filetypeEstudiante, "archivo_estudiante")) {
                            
                            
                            TransaccionService tservice = new TransaccionService(usuario.getBd());
                            tservice.crearStatement();
                            tservice = getAddBatch(tservice,ccdao.cargarCarteraNueva(ruta, filenameCredito, filenamePersona, filenameEstudiante, usuario));
                            tservice.execute();
                            tservice.closeAll();
                             
                            respuestaJson = "{\"respuesta\":\"OK\"}";

                        } else {
                            respuestaJson = "{\"error\":\"Nombre archivo no valido, SolicitudEstudiante.xls \","
                                    + "\"exception\":\"Nombre archivo no valido, SolicitudEstudiante.xls\"}";
                        }
                    } else {
                        respuestaJson = "{\"error\":\"Nombre archivo no valido, SolicitudPersona.xls \","
                                    + "\"exception\":\"Nombre archivo no valido, SolicitudPersona.xls\"}";
                    }
                } else {
                    respuestaJson = "{\"error\":\"Nombre archivo no valido, SolicitudCredito.xls \","
                                    + "\"exception\":\"Nombre archivo no valido, SolicitudCredito.xls\"}";
                }


            } else {
                mreq.getFile("archivo_credito").delete();
                mreq.getFile("archivo_persona").delete();
                mreq.getFile("archivo_estudiante").delete();
               
                respuestaJson = "{\"error\":\"La extensi�n de uno de los archivos no es inv�lida. Verifique.\"}";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            respuestaJson = "{\"error\":\"Lo sentimos no se pudo subir el archivo.\","
                           + "\"exception\":\""+ex.getMessage()+"\"}";
        } finally {

            this.printlnResponse(respuestaJson, "application/json;");
        }

    }
     
    private void cargarInfoCartera() throws Exception{
        String parametro = request.getParameter("parametro") != null ? request.getParameter("parametro") : "";
        printlnResponse(ccdao.getInfoCargaCartera(parametro.equals("") ? "INFO_CARGADA_CARTERA" : "INFO_CARGADA_CARTERA_PERSONA", parametro, usuario), "application/json;");        
    }
    
    private void crearNuevoNegocioCartera() throws Exception{       
        printlnResponse(ccdao.crearNuevoNegocioCartera(usuario), "application/json;");        
    }
    
    private void descartarNegocioCartera() throws Exception{       
        printlnResponse(ccdao.descartarNegocios(usuario), "application/json;");        
    }
    
    private void eliminarCargaCartera() throws Exception{       
        printlnResponse(ccdao.eliminarCargaCartera(usuario), "application/json;");        
    }
    
    private void buscarNegociosCartera() throws Exception{       
        printlnResponse(ccdao.buscarNegocioCartera(usuario,"NP"), "application/json;");        
    }
    
    private void crearDocumentosCartera() throws Exception{       
        printlnResponse(ccdao.crearDocumentosCartera(usuario), "application/json;");        
    }
     
    private boolean validarExtArchivo(String file, String opcion) {
        boolean retorno = false;
        //validacion.
        switch (opcion) {
            case "archivo_credito":
                retorno = extencionArchivo(file);
                break;
            case "archivo_persona":
                retorno = extencionArchivo(file);
                break;
            case "archivo_estudiante":                
                retorno = !file.equals("") ? extencionArchivo(file) : true;
                break;
        }
  
        return retorno;
    }
    
    private boolean validarNombreArchivo(String file, String filetype, String opcion) {
        boolean retorno = false;
        //validacion.
        switch (opcion) {
            case "archivo_credito":
                retorno = file.contains("SolicitudCredito");
                break;
            case "archivo_persona":
                retorno = file.contains("SolicitudPersona");
                break;
            case "archivo_estudiante":
                retorno = !filetype.equals("") ? file.contains("SolicitudEstudiante") : true;
                break;
        }

        return retorno;
    }

    
    private boolean extencionArchivo(String file) {
        return file.equals("application/excel") || file.equals("application/vnd.ms-excel")
                || file.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    }
     
     
    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
    
    
}
