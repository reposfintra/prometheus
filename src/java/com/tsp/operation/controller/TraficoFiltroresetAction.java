/********************************************************************
 *      Nombre Clase.................   TraficoFiltroAction.java
 *      Descripci�n..................   Action que se encarga de resetear los filtros creado por el usuario
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;import java.io.*;
/**
 *
 * @author  dlamadrid
 */
public class TraficoFiltroresetAction extends Action{
    
    /** Creates a new instance of TraficoFiltroresetAction */
    public TraficoFiltroresetAction () {
    }
    public void run () throws ServletException, InformationException {
        try{
            
            String campo       =""+ request.getParameter ("campo");
            String validacion  =""+ request.getParameter ("validacion");
            String zonasUsuario = request.getParameter("zonasUsuario");//23.06.2006
            
            HttpSession session= request.getSession ();
            String salida = (String)session.getAttribute ("salida");
            session.setAttribute ("salida", "");
            
            String linea= "";
            String next = "/jsp/trafico/controltrafico/filtro.jsp?linea="+linea+"&campo="+campo+"&validacion="+validacion+"reload="+"&zonasUsuario="+zonasUsuario;
            ////System.out.println ("next en Listas para Filtro"+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
}
