/********************************************************************************************* 
 *      Nombre Clase.................   ReporteAnticiposGenerarAction.java    
 *      Descripci�n..................   Action para la generaci�n del reporte de anticipos
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   04.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ********************************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;


public class ReporteAnticiposGenerarAction extends Action {

        public ReporteAnticiposGenerarAction() {

        }
    
        public void run() throws javax.servlet.ServletException, InformationException{

                String pag = "/informes/reporteAnticipos2.jsp";
                String next = "";
                
                SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
                Calendar FechaHoy = Calendar.getInstance();
                Date d = FechaHoy.getTime();
                String fecha = s.format(d);

                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                String user = usuario.getLogin();

                String fechai = request.getParameter("fechai");
                String fechaf = request.getParameter("fechaf");

                ReporteAnticiposTh hilo = new ReporteAnticiposTh();
                
                Vector informe = new Vector();
                Vector resumen = new Vector();
                
                try{
                        informe = model.planillaService.generarInformeAnticipos(fechai, fechaf);
                        resumen = model.planillaService.obtenerResumenInformeAnticipos(fechai, fechaf);
                }catch (SQLException e){
                    e.printStackTrace();
                       throw new ServletException(e.getMessage());
                }

                hilo.start(model, informe, resumen, usuario.getLogin(), fechai, fechaf);
                pag += "?mensaje=ReporteAnticipos_" + fecha + ".xsl";
                // Redireccionar a la p�gina indicada.
                next = com.tsp.util.Util.LLamarVentana(pag, "Informe Anticipos");
                this.dispatchRequest(next);

        }
    
}
