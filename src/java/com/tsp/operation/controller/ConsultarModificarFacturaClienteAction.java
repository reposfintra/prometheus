/*********************************************************************************
 * Nombre clase :                 ConsultarModificarFacturaClienteAction.java    *
 * Descripcion :                  Clase que maneja los eventos relacionados      *
 *                                con el programa que busca la informacion       *
 *                                de la factura de un cliente en la BD.          *
 * Autor :                        LREALES                                        *
 * Fecha :                        01 de diciembre de 2006, 08:00 AM              *
 * Version :                      1.0                                            *
 * Copyright :                    Fintravalores S.A.                        *
 ********************************************************************************/

package com.tsp.operation.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ConsultarModificarFacturaClienteAction extends Action {
    
    /** Creates a new instance of ConsultarModificarFacturaClienteAction */
    public ConsultarModificarFacturaClienteAction () { }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/cxcobrar/consultar_factura_cliente/FacturaCliente.jsp";
        
        HttpSession session = request.getSession ();
        Usuario usu = ( Usuario ) session.getAttribute ( "Usuario" );
        
        String usuario = usu.getLogin();
        
        String distrito = usu.getDstrct();
        
        String factura = ( request.getParameter ("documento") != null )?request.getParameter ("documento").toUpperCase():"";
        
        String hacer = ( request.getParameter ("hacer") != null )?request.getParameter ("hacer").toUpperCase():"";
        
        String nueva_fecha_probable_pago = ( request.getParameter ("fecha") != null )?request.getParameter ("fecha"):"0099-01-01";
        //System.out.println("*** distrito: "+distrito+" - factura: "+factura+" - hacer: "+hacer+" - nueva_fecha_probable_pago: "+nueva_fecha_probable_pago);
        try {
            
            Vector datos = null;
            
            if ( hacer.equals ("1") ) {

                model.consultarModificarFacturaClienteService.consultarFactura ( distrito, factura );
                datos = model.consultarModificarFacturaClienteService.getVectorReporte();
                
                if ( datos.size() <= 0 ) {
                    
                    next = "/jsp/cxcobrar/consultar_factura_cliente/FacturaCliente.jsp?msg=La Factura Digitada NO EXISTE!!";
                    
                }
                
            } else {

                // UPDATE
                model.consultarModificarFacturaClienteService.actualizarFactura ( distrito, factura, nueva_fecha_probable_pago, usuario );
                
                next = "/jsp/cxcobrar/consultar_factura_cliente/FacturaCliente.jsp?msg=Factura Modificada Exitosamente!";
            
            }          
            
        } catch ( Exception e ) {
            
            e.printStackTrace();
            throw new ServletException ( "Error en la Consulta de la Factura de un Cliente para Modificar - Action : " + e.getMessage () );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}