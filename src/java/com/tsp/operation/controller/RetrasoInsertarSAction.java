/*
 * RetrasoInsertarSAction.java
 *
 * Created on 15 de diciembre de 2005, 04:14 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class RetrasoInsertarSAction extends Action 
{
    
    /** Creates a new instance of RetrasoInsertarSAction */
    public RetrasoInsertarSAction ()
    {
    }
    
    public void run () throws ServletException, InformationException
    {
         String next="";
                try
                {
                        
                        HttpSession session = request.getSession ();
                        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
                        String userlogin=""+usuario.getLogin ();
                        String distrito=""+ usuario.getDstrct ();
                        
                        String c_actual=""+request.getParameter ("c_actual");
                        String c_nuevo=""+request.getParameter ("c_nuevo");
                        String planilla=""+request.getParameter ("planilla");
                        String causa=""+request.getParameter ("causa");
                        String motivo=""+request.getParameter ("motivo");
                        String tipo=""+request.getParameter ("tipo");
                       
                       
                        boolean existe=false;
                        
                        RetrasoSalida retraso = new RetrasoSalida();
                        retraso.setDstrct (distrito);
                        retraso.setCreation_user (userlogin);
                        retraso.setC_actual (c_actual);
                        retraso.setC_nuevo (c_nuevo);
                        retraso.setCausa (causa);
                        retraso.setDescripcion (motivo);
                        retraso.setTipo (tipo);
                        retraso.setPlanilla (planilla);
                        model.retrasoSService.setRetraso (retraso);
                        
                        model.retrasoSService.insertarRetrazo ();
                        String ms="";
                        ms="Registro Insertado";                          
                        next="/jsp/trafico/atraso/insertarSalida.jsp?accion=1&ms="+ms+"&fechaS="+c_nuevo+"&planilla="+planilla;  
                }
                catch (Exception e)
                {
                        throw new ServletException (e.getMessage ());
                        // e.printStackTrace();
                }
                this.dispatchRequest (next);
    }    
    
}
