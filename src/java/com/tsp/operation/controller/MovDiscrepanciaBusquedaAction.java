/***********************************************
 * Nombre clase: MovDiscrepanciaBusquedaAction
 * Descripci�n: Accion para buscar productos de discrepancia.
 * Autor: Diogenes Bastidas Morales
 * Fecha: 30 de diciembre de 2005, 12:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class MovDiscrepanciaBusquedaAction extends Action {
    
    /** Creates a new instance of MovDiscrepanciaBusquedaAction */
    public MovDiscrepanciaBusquedaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String cod = request.getParameter("cliente");
        String ubi = request.getParameter("ubicacion");
        String fecini = request.getParameter("fecini");
        String fecfin = request.getParameter("fecfin");
        String next = "/jsp/cumplidos/movimiento_discrepancia/listarMovDiscrepancia.jsp";
        try{
            model.discrepanciaService.listProductosMovTrafico(cod, ubi, fecini, fecfin,distrito);
            ////System.out.println(next);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
