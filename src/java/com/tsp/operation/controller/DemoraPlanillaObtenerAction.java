/********************************************************************
 *      Nombre Clase.................   DemoraPlanillaObtenertAction.java    
 *      Descripci�n..................   Obtiene un registro de la tabla demora
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   22.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DemoraPlanillaObtenerAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DemoraPlanillaObtenerAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String next = "/jsp/trafico/demora/consultarDemoraPlanillaParam.jsp?numpla=" + request.getParameter("numpla");
                
                String id = request.getParameter("id");
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Demora dem =  model.demorasSvc.obtenerDemoraID(id);
                        request.setAttribute("Demora", dem);                        

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
