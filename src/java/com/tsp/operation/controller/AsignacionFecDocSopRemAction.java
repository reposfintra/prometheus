/*************************************************************
 * Nombre      ............... AsignacionFecDocSopRemAction.java
 * Descripcion ............... Clase de asignacion de fechas de envio, recibido
 *                             a los soportes asignados a las remesas
 * Autor       ............... mfontalvo
 * Fecha       ............... Enero - 16 - 2006
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import java.util.*;

public class AsignacionFecDocSopRemAction extends Action{
    
    /**
     * Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            String      next    = "/jsp/cumplidos/soportes/consulta.jsp";
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            
            // obteniendo datos del objeto request

            String opcion = defaultString("opcion");
            String numrem = defaultString("numrem").toUpperCase();
            String agcfac = defaultString("agcfac");
            String codcli = defaultString("codcli");
            String clase  = defaultString("clase"); // fisico, logico, ambas
            String tipo   = defaultString("tipo");  // envio, recibido
            int    tipoB  = Integer.parseInt(defaultString("tipoB","0"));
            
            
            String fecIni  = defaultString("fecIni");
            String fecFin  = defaultString("fecFin");
            if (opcion.equalsIgnoreCase("Buscar")){
                
                switch (tipoB){
                    case 0: model.SoporteClienteSvc.obtenerSorportes_x_Remesa            (numrem, tipo, clase);                 break;
                    case 1: model.SoporteClienteSvc.obtenerSorportes_x_Cliente           (codcli, fecIni, fecFin, tipo, clase); break;
                    case 2: model.SoporteClienteSvc.obtenerSorportes_x_AgenciaFacturadora(agcfac, fecIni, fecFin, tipo, clase); break;
                }
                if (model.SoporteClienteSvc.getSoportes()==null || model.SoporteClienteSvc.getSoportes().isEmpty()){
                    if (tipoB == 0){
                        String msg = model.SoporteClienteSvc.obtenerCausa(numrem, tipo, clase);
                        request.setAttribute("msg", msg);
                    } else {
                        request.setAttribute("msg", "No se encontraron datos para los criterios especificados");
                    }
                    
                } else
                    next = "/jsp/cumplidos/soportes/listado.jsp";  
                
            } else if ( opcion.equalsIgnoreCase("Guardar")){
                
                
                String []soporte = request.getParameterValues("soporte");
                if (soporte!=null && soporte.length>0){
                    String []remesa  = request.getParameterValues("remesa");
                    if (tipo.equalsIgnoreCase("ENVIO")){
                        String [] agc = request.getParameterValues("agencia");
                        String [] fel = request.getParameterValues("fel");
                        String [] fef = request.getParameterValues("fef");
                        String []cagc = request.getParameterValues("cagencia");
                        String []cfel = request.getParameterValues("cfel");
                        String []cfef = request.getParameterValues("cfef");
                        for (int i = 0; i < soporte.length; i++){
                            boolean editAGE = ( agc.equals(cagc)? false: true );
                            boolean editFEL = ( fel.equals(cfel)? false: true );
                            boolean editFEF = ( fef.equals(cfef)? false: true );
                            if ( editAGE && editFEF){
                                model.SoporteClienteSvc.ActualizarFechas(remesa[i], soporte[i], editAGE, agc[i], editFEF, fef[i], false, "", editFEL, fel[i], false, "", usuario.getLogin());
                                request.setAttribute("msg","Soportes actualizados");
                            }
                        }
                    } else if (tipo.equalsIgnoreCase("RECIBIDO")){
                        String [] frl = request.getParameterValues("frl");
                        String [] frf = request.getParameterValues("frf");
                        String []cfrl = request.getParameterValues("cfrl");
                        String []cfrf = request.getParameterValues("cfrf");
                        for (int i = 0; i < soporte.length; i++){
                            boolean editFRL = ( frl.equals(cfrl)? false: true );
                            boolean editFRF = ( frf.equals(cfrf)? false: true );
                            if ( editFRF){
                                model.SoporteClienteSvc.ActualizarFechas(remesa[i], soporte[i], false, "", false, "", editFRF, frf[i], false, "", editFRL, frl[i], usuario.getLogin());
                                request.setAttribute("msg","Soportes actualizados");
                            }
                        }                        
                    }
                    
                    
                    // actualizamos los datos del listado
                    switch (tipoB){
                        case 0: model.SoporteClienteSvc.obtenerSorportes_x_Remesa            (numrem, tipo, clase);                 break;
                        case 1: model.SoporteClienteSvc.obtenerSorportes_x_Cliente           (codcli, fecIni, fecFin, tipo, clase); break;
                        case 2: model.SoporteClienteSvc.obtenerSorportes_x_AgenciaFacturadora(agcfac, fecIni, fecFin, tipo, clase); break;
                    }                    
                    next = "/jsp/cumplidos/soportes/listado.jsp";  
                }
            } else if (opcion.equalsIgnoreCase("consultaSoportes")) {     
                Vector v = model.SoporteClienteSvc.obtenerSoportesRemesa( numrem );
                request.setAttribute("datos",v);
                if (v==null || v.isEmpty()){
                    request.setAttribute("msg","No se han asignado fechas a los soportes");
                }
                next = "/jsp/cumplidos/soportes/consultaRemesa.jsp";  
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new ServletException("\nNo se pudo encontrar " + next);
            rd.forward(request, response);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ServletException("\nError en AsignacionFecDocSopRemAction ...\n"+ex.getMessage());
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            
            HttpSession session = request.getSession();
            Usuario user = (Usuario) session.getAttribute("Usuario");
            
            // obteniendo datos del objeto request
            String Mensaje = "";
            String Opcion  = defaultString("Opcion");
            String Remesa  = defaultString("Remesa").toUpperCase();
            String Soporte = defaultString("Soporte");
            String Estado  = defaultString("Estado");
            String Tipo    = defaultString("Tipo");
            String Agencia = defaultString("Agencia");
            String fecIni  = defaultString("fecIni");
            String fecFin  = defaultString("fecFin");
            
            String AgenciaEnvio        = defaultString("AgenciaEnvio");
            String FechaEnvioFisico    = defaultString("FechaEnvioFisico");
            String FechaRecibidoFisico = defaultString("FechaRecibidoFisico");
            String FechaEnvioLogico    = defaultString("FechaEnvioLogico");
            String FechaRecibidoLogico = defaultString("FechaRecibidoLogico");
            
            String editAGE = defaultString("editAGE");
            String editFEF = defaultString("editFEF");
            String editFRF = defaultString("editFRF");
            String editFEL = defaultString("editFEL");
            String editFRL = defaultString("editFRL");            


            if (Opcion.equals("Buscar")){
                model.cumplidoService.listarSoportesRemesa(Agencia, fecIni, fecFin, Remesa, Estado, Tipo);
                if (model.cumplidoService.getListaDocumentos().isEmpty())
                    Mensaje = "No se encontraron datos para su criterio";

                session.setAttribute("fltAgencia", Agencia);
                session.setAttribute("fltFecIni", fecIni);
                session.setAttribute("fltFecFin", fecFin);
                session.setAttribute("fltRemesa", Remesa);
                session.setAttribute("fltEstado", Estado);
                session.setAttribute("fltTipo"  , Tipo  );
            }
            else if (Opcion.equals("Edit")){

                model.cumplidoService.ActualizarFechas(
                        Remesa, 
                        Soporte, 
                        (new Boolean (editAGE)).booleanValue(), AgenciaEnvio, 
                        (new Boolean (editFEF)).booleanValue(), FechaEnvioFisico,
                        (new Boolean (editFRF)).booleanValue(), FechaRecibidoFisico,
                        (new Boolean (editFEL)).booleanValue(), FechaEnvioLogico,
                        (new Boolean (editFRL)).booleanValue(), FechaRecibidoLogico, 
                        user.getLogin()
                    );

                String fltRemesa = (String) session.getAttribute("fltRemesa");
                String fltEstado = (String) session.getAttribute("fltEstado");
                String fltTipo   = (String) session.getAttribute("fltTipo");
                String fltAgencia= (String) session.getAttribute("fltAgencia");
                String fltFecIni = (String) session.getAttribute("fltFecIni");
                String fltFecFin = (String) session.getAttribute("fltFecFin");
                model.cumplidoService.listarSoportesRemesa(fltAgencia, fltFecIni, fltFecFin, fltRemesa, fltEstado, fltTipo);
                
            }
            
            String next = "/jsp/masivo/soportes/SoportesRemesa.jsp?Mensaje="+Mensaje;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new ServletException("\nNo se pudo encontrar " + next);
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException("\nError en AsignacionFecDocSopRemAction ...\n"+ex.getMessage());
        }
        
    }*/
    
    
    /**
     * Procedimeto para obtener un valor del objeto request
     * y en caso de que este no exista se devuelve vacio en lugar
     * de null.
     * @autor mfontalvo
     * @params parameter .... Parametro a consultar
     * @return valor del objeto request.
     */
    
    private String defaultString(String parameter){
        return ( request.getParameter(parameter)!=null?request.getParameter(parameter):"" );
    }
    
    
    /**
     * Procedimeto para obtener un valor del objeto request
     * y en caso de que este no exista se devuelve vacio en lugar
     * de null.
     * @autor mfontalvo
     * @params parameter .... Parametro a consultar
     * @return valor del objeto request.
     */
    
    private String defaultString(String parameter, String def){
        return ( request.getParameter(parameter)!=null?request.getParameter(parameter):def );
    }    
}


