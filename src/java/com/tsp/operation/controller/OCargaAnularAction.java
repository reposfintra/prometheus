/*******************************************************************
 * Nombre clase: OCargaAnularAction.java
 * Descripci�n: Accion para anular  una orden de carga
 * Autor: Ing. Karen Reales
 * Fecha: 12 de mayo 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OCargaAnularAction extends Action{
    static Logger logger = Logger.getLogger(OCargaAnularAction.class);
    
    public OCargaAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        /**
         *
         * Usuario en Sesion
         */
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        /**
         *
         * Inicializacion Variable tipo String
         */
        String orden = request.getParameter("orden");
        String next="/jsp/masivo/ordencargue/InicioAnular.jsp?mensaje=La orden "+orden+" ha sido anulada con exito.";
        logger.info("orden "+orden);
        try{
            model.imprimirOrdenService.delete();
            logger.info("El next= "+next);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
