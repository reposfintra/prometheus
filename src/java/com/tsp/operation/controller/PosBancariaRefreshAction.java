/********************************************************************
 *      Nombre Clase.................   PosBancariaRefreshAction.java
 *      Descripci�n..................   Refresca la vista de acuerdo a los selects
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *      modificado .................    egonzalez2014
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Andres
 */
public class PosBancariaRefreshAction extends Action{
       
    /** Creates a new instance of PosBancariaRefreshAction */
    public PosBancariaRefreshAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/cxpagar/posbancaria/PosBancariaInsert.jsp";
        
        String listar = request.getParameter("listar");
        String traslado = request.getParameter("traslado");
        
        String agency = request.getParameter("agenciaSelec");
        String banco = (request.getParameter("bancoSelec")==null)? "" : request.getParameter("bancoSelec");
        String cargar = request.getParameter("cargar");
        
        String fecha = request.getParameter("fecha");
        
        request.setAttribute("agencia", agency);
        request.setAttribute("banco", banco);
        request.setAttribute("fecha", fecha);
        
        HttpSession session = request.getSession();
        String cia = (String) session.getAttribute("Distrito");
        
        /*////System.out.println(".................... agencia " + agency);
        ////System.out.println(".................... banco " + banco);
        ////System.out.println(".................... fecha " + fecha);*/
        
        try{
            model.servicioBanco.loadBancosAgencia(agency, cia);
            Vector bancos = null;
            if( banco.length()!=0 ){
                model.servicioBanco.obtenerSucursales(agency, banco, cia);
                bancos = model.servicioBanco.getBancos();
            } else if (agency!=null && cargar!=null){
                model.servicioBanco.obtenerBancosAgenciaCia(agency, cia);
                bancos = model.servicioBanco.getBancos(); 
            } else {
                bancos = null;
            }
            
            if(bancos!=null){
                Vector pbanc = new Vector();
                //////System.out.println("...................... de bancos: " + bancos.size());
                for( int i=0; i<bancos.size(); i++){
                    Banco b = (Banco) bancos.elementAt(i);
                    PosBancaria posb;
                    
                    String agenc = b.getCodigo_Agencia();
                    String bank = b.getBanco();
                    String suc = b.getBank_account_no();
                    
//                    model.posbancariaSvc.obtener(cia, agenc, bank, suc, fecha);
//                    posb = model.posbancariaSvc.getPosbanc();
//                    //////System.out.println(".............. se obtuvo la posici�n bancaria: (" + agenc + ", " + bank + ", " + suc + ") : " + (posb==null));
//                    if( posb == null ){
//                        posb = new PosBancaria();
//                        posb.setAgency_id(agenc);
//                        posb.setBranch_code(bank);
//                        posb.setBank_account_no(suc);
//                    }
//                    
//                    posb.setNom_agencia(b.getNombre_Agencia());                    
//                    //////System.out.println("........................ posici�n bancaria : " + posb.getNuevo_saldo());
//                    pbanc.add(posb);
                }
                
                //////System.out.println("...................... de posicion_bancaria: " + pbanc.size());
                bancos = pbanc;
            }
            request.removeAttribute("bancos");
            request.setAttribute("bancos", bancos);
        
            if( listar!=null ){
                next="/jsp/cxpagar/posbancaria/PosBancariaListar.jsp";
            }
            
            if( traslado!=null ){
                next += "?traslado=OK";
            }
            
            
            Calendar ante = Util.crearCalendar(fecha + " 00:00:00");
            boolean domingo = (ante.get(ante.DAY_OF_WEEK)==Calendar.SUNDAY);
            boolean sabado = (ante.get(ante.DAY_OF_WEEK)==Calendar.SATURDAY);
            boolean feriado = model.tablaGenService.isFeriado(fecha);
            if( domingo || sabado || feriado ){
                request.setAttribute("NoTraslado", "OK");
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
