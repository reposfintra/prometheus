/*
 * Campos_jspSerchAction.java
 *
 * Created on 17 de julio de 2005, 19:48
 */


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author  Rodrigo
 */
public class Campos_jspSerchAction extends Action{
    
    /** Creates a new instance of Tipo_ubicacionSerchAction */
    public Campos_jspSerchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession();        
        String listar = (String) request.getParameter("listar");
        String pagina = (String) request.getParameter("c_pagina");
        try{
            if (listar.equals("True")){                
                next="/jsp/trafico/permisos/campos_jsp/Campos_jspListar.jsp?sw=";          
                 
                Vector tipo_ubicaciones = model.camposjsp.searchDetallecampos_jsps(pagina); 
                request.setAttribute("tipo_ubicaciones",tipo_ubicaciones);
            }
            else{
                String cod = (String) request.getParameter("c_campo");
                model.camposjsp.serchcampos_jsp(cod);
                campos_jsp cj = model.camposjsp.getcampos_jsp();
                request.setAttribute("campos_jsp",cj);                
                next="/jsp/trafico/permisos/campos_jsp/Campos_jspModificar.jsp";
                
                
            }                        
        }
        catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
