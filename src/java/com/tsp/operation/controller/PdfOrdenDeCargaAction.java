/******************************************************************************
 * Nombre clase :                   PdfOrdenDeCargaAction.java                *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Impresion de Orden De Carga               *
 * Autor :                          LREALES                                   *
 * Fecha :                          11 de mayo de 2006, 01:50 PM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.pdf.*;
import java.io.*;

public class PdfOrdenDeCargaAction extends Action{
    
    /** Creates a new instance of PdfOrdenDeCargaAction */
    public PdfOrdenDeCargaAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
        try{
            
            HojaOrdenDeCarga hojaOrden = model.imprimirOrdenService.getHojaOrden();
            
            String ruta = application.getRealPath( "/" );
            File xslt = new File( ruta + "Templates/ordenDeCarga.xsl" );
            File pdf = new File( ruta + "pdf/oc.pdf" );
            
            Vector info_orden = model.imprimirOrdenService.getVec_lista();
            
            HojaOrdenDeCargaPDF Hoja_Orden_PDF = new HojaOrdenDeCargaPDF( model ); 
            Hoja_Orden_PDF.generarPDF ( xslt, pdf, info_orden );
            
            String next = "/pdf/oc.pdf";
            
            RequestDispatcher rd = application.getRequestDispatcher( next );
            
            if( rd == null )
                throw new ServletException( "No se pudo encontrar " + next );
            
            rd.forward( request, response );
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new ServletException( "Error en Pdf Orden De Carga Action.......\n" + e.getMessage() );
        
        }
        
    }    
    
}