/******************************************************************************
 *      Nombre Clase.................   RetencionPagoManagerAction.java
 *      Descripci�n..................   Retencion de pagos
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   19.20.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import com.tsp.finanzas.contab.model.threads.*;




import org.apache.log4j.*;

public class RetencionPagoManagerAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    private CXP_Doc facturas = new CXP_Doc();
    /** Creates a new instance of DocumentoInsertAction */
    public RetencionPagoManagerAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String opc = request.getParameter( "opc" );
        //Pr�xima vista
        
        String next  = "";
        
        String factura = request.getParameter("factura");
        String nit = request.getParameter("proveedor");
        String numpla = request.getParameter("planilla");
        String placa = request.getParameter("placa");
        String fechai = request.getParameter("FechaI");
        String fechaf = request.getParameter("FechaF");
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        
        try{
            
            switch( Integer.parseInt(opc) ){
                case 1:     
                    
                    /* Ir a la p�gina de retenci�n de pago a proveedor*/
                    next = "/jsp/cxpagar/retencion_pago/retencionNit.jsp";
                    break;
                
                case 2:     
                    
                    /* Ir a la p�gina de retenci�n de pago a factura de proveedor */
                    next = "/jsp/cxpagar/retencion_pago/retencionCxp.jsp";
                    break;
                case 3:
                    
                    /* Obtener la info del proveedor */
                    if( model.proveedorService.obtenerProveedorPorNit(nit)==null ){
                        next = "/jsp/cxpagar/retencion_pago/retencionNit.jsp?msg=El nit de proveedor ingresado no registra.";
                        request.setAttribute("nit",  nit);
                    } else {
                        /* Pagina de Info del Proveedor */
                        request.setAttribute("nit", model.proveedorService.getProveedor());
                        next = "/jsp/cxpagar/retencion_pago/retencionNitAplicar.jsp";
                    }
                    break;
                case 4:
                    
                    /* Actualizamos el campo ret_pago en proveedor*/
                    
                    String ret_pago = request.getParameter("ret_pago");
                    String cia = request.getParameter("dstrct");
                    
                    logger.info("? ret_pago: " + ret_pago);
                    logger.info("? dstrct: " + cia);
                    
                    model.proveedorService.aplicaRetencionPago(cia, nit, ret_pago);
                    model.proveedorService.obtenerProveedorPorNit(nit);
                    request.setAttribute("nit", model.proveedorService.getProveedor());
                    next = "/jsp/cxpagar/retencion_pago/retencionNitAplicar.jsp?msg=Modificaci�n exitosa.";
                    
                    break;
                case 5:
                    
                    logger.info("FACTURA: " + factura);
                    logger.info("PROVEEDOR: " + nit);
                    logger.info("PLANILLA: " + numpla);
                    logger.info("PLACA: " + placa);
                    logger.info("FECHAI: " + fechai);
                    logger.info("FECHAF: " + fechaf);
                    
                    model.cxpDocService.consultarDocumento(dstrct, factura, "010", numpla, "", "", "", nit, fechai, fechaf, "", placa, "");
                    model.cxpDocService.setFacturas_ret(model.cxpDocService.getVecCxp_doc());
                    next = "/jsp/cxpagar/retencion_pago/retencionCxpAplicar.jsp" + (model.cxpDocService.getFacturas_ret().size()<=0 ? "?msg=No se encontraron resultados" : "");
                    break;
                case 6:
                    
                    String docs[] = request.getParameterValues("ret_pago");
                    if( request.getParameterValues("ret_pago")==null && request.getParameter("ret_pago")!=null ){
                        docs = new String[1];
                        docs[0] = request.getParameter("ret_pago");
                    }
                    Vector aplica = new Vector();
                    Vector sql = new Vector();
                    
                    if( docs!=null ){
                        for( int i = 0; i < docs.length; i++){
                            logger.info("Aplica retecion al pago: " + docs[i]);
                            StringTokenizer token = new StringTokenizer(docs[i], "_");
                            String fra[] = {"","",""};
                            int k = 0;
                            while( token.hasMoreTokens() ){
                                fra[k] = token.nextToken();
                                k++;
                            }
                            CXP_Doc doc = new CXP_Doc();
                            doc.setDstrct(fra[0]);
                            doc.setProveedor(fra[1]);
                            doc.setDocumento(fra[2]);
                            logger.info("*** Aplica retecion al pago: " + doc.getDstrct() + "-" + doc.getProveedor() + "-" + doc.getDocumento());
                            aplica.add(doc);
                        }
                    }
                    
                    Vector fras = model.cxpDocService.getFacturas_ret();
                    Vector fras0 = new Vector();
                    logger.info("?fras aplican retenci�n al pago: " + aplica.size());
                    for( int i=0; i<fras.size(); i++){
                        CXP_Doc doc0 = (CXP_Doc) fras.elementAt(i);
                        String apl = "N";
                        for( int j=0; j<aplica.size(); j++){
                            CXP_Doc doc1 = (CXP_Doc) aplica.elementAt(j);
                            if( doc0.getDstrct().equals(doc1.getDstrct()) 
                                && doc0.getProveedor().equals(doc1.getProveedor()) 
                                && doc0.getDocumento().equals(doc1.getDocumento()) ){
                                    apl = "S";
                            }
                        }
                        logger.info("? Aplica retecion al pago: " + doc0.getDstrct() + "-" + doc0.getProveedor() + "-" + doc0.getDocumento() + ", ?Apl: " + apl);
                        sql.add(model.cxpDocService.aplicaRetencionPago(doc0.getDstrct(), doc0.getProveedor(), doc0.getDocumento(), apl));
                        doc0.setRetencion_pago(apl);
                        fras0.add(doc0);
                    }
                    
                    model.cxpDocService.setFacturas_ret(fras0);
                    model.despachoService.insertar(sql);
                    next = "/jsp/cxpagar/retencion_pago/retencionCxpAplicar.jsp?msg=Modificaci�n exitosa.";
                    break;
            }
            
            
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
