/*
 * ProveedoresDeleteAction.java
 *
 * Created on 21 de abril de 2005, 03:10 PM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class ProveedoresDeleteAction extends Action{
    
    /** Creates a new instance of ProveedoresDeleteAction */
    public ProveedoresDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next         ="/proveedores/proveedorDelete.jsp";
        String nit          = request.getParameter("nit");
        String sucursal     = request.getParameter("sucursal").toUpperCase();
        
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            model.proveedoresService.searchProveedor(nit, sucursal);
            if(model.proveedoresService.getProveedor()!=null){
                model.proveedoresService.setProveedor(model.proveedoresService.getProveedor());
                model.proveedoresService.anularProveedor();
                model.proveedoresService.vecProveedor(usuario.getBase());
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
    
}
