/*
 * ReporteInventarioDiscrepanciaClienteAction.java
 *
 * Created on 5 de junio de 2006, 08:41 AM
 */

/******************************************************************
 * Nombre ......................ReporteInventarioDiscrepanciaClienteAction.java
 * Descripci�n..................Clase Action para llamar al hilo que genera 
 *                              el reporte de Inventario 
 * Autor........................Osvaldo P�rez - Gil David Pi�a
 * Fecha........................05-06-2006
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.text.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import com.tsp.operation.model.*;


/**
 *
 * @author  Osvaldo P�rez
 */
public class ReporteInventarioDiscrepanciaClienteAction extends Action{
    
    /** Creates a new instance of ReporteInventarioDiscrepanciaClienteAction */
    public ReporteInventarioDiscrepanciaClienteAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/cumplidos/inventario/consultarInventario.jsp";            
        try{
            HttpSession session =  request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            Vector datos = model.productoService.getProductos();            
            String codubicacion = (String) request.getParameter("nomubicacion");
            
            String cliente =model.clienteService.getCliente().getNomcli();
            
            ReporteInventarioDiscrepanciaCliente RPT = new ReporteInventarioDiscrepanciaCliente();
            RPT.start(datos, usuario.getLogin(),cliente,codubicacion); 
            
            request.setAttribute("mensaje","Archivo generado exitosamente!"); 
        }catch (Exception ex){
            throw new ServletException("Error en RPTINVENTARIO : " + ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
