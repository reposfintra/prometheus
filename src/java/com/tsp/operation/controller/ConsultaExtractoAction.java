

/*
 * ConsultaExtractoPagosAction.java
 *
 * Created on 9 de enero de 2007, 01:30 PM
 */

package com.tsp.operation.controller;


import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import javax.servlet.http.HttpSession;

/**
 *
 * @author  equipo
 */
public class ConsultaExtractoAction extends Action {
    
    /** Creates a new instance of ConsultaExtractoPagosAction */
    public ConsultaExtractoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario user     = (Usuario) session.getAttribute("Usuario");
            String opcion    = Util.coalesce( request.getParameter("opcion")   , "" );
            String next      = "/jsp/cxpagar/consultaExtractos/consulta.jsp";

            String dstrct = Util.coalesce( request.getParameter("dstrct")      , "" );
            String nit    = Util.coalesce( request.getParameter("nit")         , "" );
            String fini   = Util.coalesce( request.getParameter("fini")        , "" );
            String ffin   = Util.coalesce( request.getParameter("ffin")        , "" );
            String verPag = Util.coalesce( request.getParameter("egPagados")   , "" );
            String verPen = Util.coalesce( request.getParameter("egPendientes"), "" );
            String show   = Util.coalesce( request.getParameter("show"), "" );
            String []dEgr = request.getParameterValues("dEgr");
            String []dFac = request.getParameterValues("dFac");
            String []dPla = request.getParameterValues("dPla");
            String []dPen = request.getParameterValues("dPen");


            if (opcion.equalsIgnoreCase("InitPropietario")){
                Proveedor p = model.proveedorService.obtenerProveedor(user.getCedula(), user.getDstrct());
                model.ConsultaExtractoSvc.setProveedor(p);
            } else if (opcion.equalsIgnoreCase("Init")){
                model.ConsultaExtractoSvc.setProveedor(null);
            }
            else if (opcion.equalsIgnoreCase("Consulta")){
                model.ConsultaExtractoSvc.setVectorEgresos(null);
                model.ConsultaExtractoSvc.setVectorFacturasPendientes (null);
                model.ConsultaExtractoSvc.setVectorPlanillasPendientes(null);
                model.ConsultaExtractoSvc.setVectorLiquidaciones(null);                

                
                Proveedor p = model.ConsultaExtractoSvc.getProveedor();
                boolean reset = false;
                if (show.equals("ok")){
                    p = model.proveedorService.obtenerProveedor(nit, user.getDstrct());
                    model.ConsultaExtractoSvc.setProveedor(p);                    
                    reset = true;
                }
                
                if (p!=null) {                    
                    // extraccion de las facturas pagadas
                    if (verPag.equalsIgnoreCase("ok")){
                        model.ConsultaExtractoSvc.obtenerEgresos(dstrct, nit, fini, ffin);
                    }
                    // extraccion de las facturas pendientes
                    if (verPen.equalsIgnoreCase("ok")){
                        //System.out.println("obtenerFacturasPendientes " + (new Date()) );
                        model.ConsultaExtractoSvc.obtenerFacturasPendientes (dstrct, nit);
                        //System.out.println("obtenerPlanillasPendientes" + (new Date()) );
                        model.ConsultaExtractoSvc.obtenerPlanillasPendientes(dstrct, nit);
                        //System.out.println("generarLiquidacionesPendientes" + (new Date()) );
                        model.ConsultaExtractoSvc.generarLiquidacionesPendientes();
                    }
                    if ((model.ConsultaExtractoSvc.getVectorEgresos() == null || model.ConsultaExtractoSvc.getVectorEgresos().isEmpty()) &&
                        (model.ConsultaExtractoSvc.getVectorFacturasPendientes () == null || model.ConsultaExtractoSvc.getVectorFacturasPendientes ().isEmpty()) &&
                        (model.ConsultaExtractoSvc.getVectorPlanillasPendientes() == null || model.ConsultaExtractoSvc.getVectorPlanillasPendientes().isEmpty()) &&
                        (model.ConsultaExtractoSvc.getVectorLiquidaciones() == null || model.ConsultaExtractoSvc.getVectorLiquidaciones().isEmpty() )
                    ){
                        request.setAttribute("msg","No se econtraron datos para el propietario " + nit);
                        if ( reset ) { model.ConsultaExtractoSvc.setProveedor(null); }
                    } else {
                        next      = "/jsp/cxpagar/consultaExtractos/listado.jsp";
                    }                                        
                } else {
                    request.setAttribute("msg","Propietario no registrado en la base de datos");                    
                }

                
                
            }
            
            // consulta del detalle de los egresos 
            else if (opcion.equalsIgnoreCase("ConsultaDetalleEgresos")){
                Vector v = model.ConsultaExtractoSvc.getVectorEgresos();
                for (int i = 0; i< v.size(); i++){
                    Egreso eg = (Egreso) v.get(i);
                    eg.setMostrarDetalleEgreso (false);
                    Vector itemsCheque = eg.getDetalle();
                    for (int j = 0; itemsCheque!=null && j < itemsCheque.size(); j++){
                        Egreso item = (Egreso) itemsCheque.get(j);
                        item.setMostrarDetalleEgreso(false);
                    }                    
                }
                
                // buscando detalle de los egresos seleccionados
                if (dEgr!=null){
                    for (int i = 0; i< dEgr.length; i++){
                        if (!dEgr[i].trim().equals("")){
                            int pos = Integer.parseInt(dEgr[i]);
                            Egreso eg = (Egreso) v.get(pos);
                            eg.setMostrarDetalleEgreso(true);
                            if (eg.getDetalle()==null || eg.getDetalle().isEmpty()){
                                model.ConsultaExtractoSvc.obtenerDetalleEgresos(eg.getDstrct(), eg.getBranch_code(), eg.getBank_account_no(), eg.getDocument_no());
                                eg.setDetalle( model.ConsultaExtractoSvc.getVectorDetalleEgresos() );
                            } else {
                                String []dEgrSel = request.getParameterValues("dFacE" + pos);                            
                                for (int j = 0; dEgrSel!=null && j < dEgrSel.length; j++ ){
                                    int posd = Integer.parseInt(dEgrSel[j]);
                                    Egreso detalle = (Egreso) eg.getDetalle().get(posd);
                                    detalle.setMostrarDetalleEgreso(true);
                                    
                                    if ((detalle.getConcept_code().equals("FAC") || detalle.getConcept_code().equals("TR")) && detalle.getFactura()==null){
                                        model.ConsultaExtractoSvc.obtenerFactura(detalle.getDstrct(), eg.getNit(), detalle.getTipo_documento(), detalle.getDocumento());
                                        detalle.setFactura( model.ConsultaExtractoSvc.getFactura() );
                                    } else if (!(detalle.getConcept_code().equals("FAC") || detalle.getConcept_code().equals("TR")) && detalle.getPlanilla()==null){
                                        model.ConsultaExtractoSvc.obtenerPlanilla( detalle.getOc() );
                                        detalle.setPlanilla( model.ConsultaExtractoSvc.getPlanilla() );
                                    }
                                }                                
                            }
                        }
                    }
                }
                next      = "/jsp/cxpagar/consultaExtractos/listado.jsp";
            }
                        

            
            
            // consulta de detalles de las liquidaciones
            else if (opcion.equalsIgnoreCase("ConsultaDetalleLiquidaciones")){
                Vector v = model.ConsultaExtractoSvc.getVectorPlanillasPendientes();
                for (int i = 0; i< v.size(); i++){
                    Planilla p = (Planilla) v.get(i);
                    p.setMostrarDetalles(false);
                }
                // buscado detalle de los egresos seleccionados
                if (dPla!=null){
                    for (int i = 0; i< dPla.length; i++){
                        if (!dPla[i].trim().equals("")){
                            int pos = Integer.parseInt(dPla[i]);
                            Planilla p = (Planilla) v.get( pos );
                            p.setMostrarDetalles(true);
                        }
                    }
                }    
                next      = "/jsp/cxpagar/consultaExtractos/listado.jsp";
            }
            
            
            
            // consulta de detalles de las facturas pendientes
            else if (opcion.equalsIgnoreCase("ConsultaDetalleFacturasPendientes")){
                Vector v = model.ConsultaExtractoSvc.getVectorFacturasPendientes();
                for (int i = 0; i< v.size(); i++){
                    CXP_Doc f = (CXP_Doc) v.get(i);
                    f.setMostrarDetalle(false);
                }
                // buscado detalle de los egresos seleccionados
                if (dPen!=null){
                    for (int i = 0; i< dPen.length; i++){
                        if (!dPen[i].trim().equals("")){
                            int pos = Integer.parseInt(dPen[i]);
                            CXP_Doc f = (CXP_Doc)  v.get(pos);
                            f.setMostrarDetalle(true);
                            if (f.getItems()==null || f.getItems().isEmpty()){
                                model.ConsultaExtractoSvc.obtenerDetalleFacturas(f);
                                model.ConsultaExtractoSvc.aplicarTasaItemsFactura(f);
                            }
                        }
                    }
                }    
                next      = "/jsp/cxpagar/consultaExtractos/listado.jsp";
            }
            
            
            
            
            this.dispatchRequest(next);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new javax.servlet.ServletException(ex.getMessage());
        }
        
    }
    
}