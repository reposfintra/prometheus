/*
 * MenuBuscarPlanillaImpresion.java
 *
 * Created on 18 de diciembre de 2004, 9:01
 */

package com.tsp.operation.controller;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Administrador
 */
public class MenuBuscarPlanillaImpresionAction extends Action {
    
    /** Creates a new instance of MenuBuscarPlanillaImpresion */
    public MenuBuscarPlanillaImpresionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        try
        {   model.PlanillaImpresionSvc.ReiniciarCIA();
            model.PlanillaImpresionSvc.ReiniciarPlanillas();
            final String next = "/impresion/buscarplanilla.jsp";            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e)
        {
            throw new ServletException(e.getMessage());
        }               
        
    }
    
}
