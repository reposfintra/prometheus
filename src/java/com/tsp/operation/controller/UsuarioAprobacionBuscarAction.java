/*
 * UsuarioAprobacionBuscarAction.java
 *
 * Created on 20 de enero de 2006, 01:08 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class UsuarioAprobacionBuscarAction extends Action{
    
    /** Creates a new instance of UsuarioAprobacionBuscarAction */
    public UsuarioAprobacionBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/general/usuarioAprobacion/usuarioAprobacion.jsp?sw=1";
        String agencia = request.getParameter("agencia");
        String tabla = request.getParameter("tabla").toUpperCase();
        String usu = request.getParameter("usuario");
        try {
            model.usuaprobacionService.buscarUsuarioAprobacion(agencia, tabla, usu);                        
            
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
