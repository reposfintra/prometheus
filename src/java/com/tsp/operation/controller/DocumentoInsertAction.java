/********************************************************************
 *      Nombre Clase.................   DocumentoInsertAction.java    
 *      Descripci�n..................   Inserta u nuevo registro en la tabla tbldoc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   13.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DocumentoInsertAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DocumentoInsertAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tbldoc/DocumentoInsert.jsp";
                String next = "";
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Documento doc = new Documento();
                        doc.setBase(usuario.getBase());//request.getParameter("
                        doc.setC_banco(request.getParameter("c_banco"));
                        doc.setC_document_name(request.getParameter("c_document_name"));
                        doc.setC_document_type(request.getParameter("c_document_type"));
                        doc.setC_ref_1((request.getParameter("c_ref_1")==null)? "" : request.getParameter("c_ref_1") );
                        doc.setC_sigla(request.getParameter("c_sigla"));
                        doc.setDistrito((String) session.getAttribute("Distrito"));
                        doc.setUsuario_creacion(usuario.getLogin());
                        
                        model.documentoSvc.obtenerDocumento(doc.getDistrito(), doc.getC_document_type());
                        Documento existe = model.documentoSvc.getDocumento();
                        
                        if ( existe!=null ){
                                if( existe.getEstado().matches("A") ){
                                        doc.setEstado("");
                                        doc.setC_ndocument_type(doc.getC_document_type());
                                        model.documentoSvc.actualizarDocumento(doc);
                                        pag += "?msg=Se ha ingresado el documento correctamente";
                                        model.documentoSvc.setDocumento(null);
                                }
                                else{
                                        pag += "?msg=No se puede procesar la informaci�n. Ya existe el documento.";
                                }                                
                        }
                        else{
                                model.documentoSvc.insertarDocumento(doc);
                                pag += "?msg=Se ha ingresado el documento correctamente";
                                model.documentoSvc.setDocumento(null);
                        }
                        
                        next = com.tsp.util.Util.LLamarVentana(pag, "Ingresar Documento");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
