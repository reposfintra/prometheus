/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AdministracionFintraDAO;
import com.tsp.operation.model.DAOS.impl.AdministracionFintraImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dvalencia
 */
public class AdministracionFintraAction extends Action {

    Usuario usuario = null;
    String reponseJson = "{}";

    private final int CARGAR_NEGOCIOS = 1;
    private final int EXPORTAR_PDF_DETALLE = 2;
    private final int CARGAR_PRESOLICITUDES_MICROCREDITO = 3;
    private final int ANULAR_PRESOLICITUD_MICROCREDITO = 4;
    private final int EXPORTAR_PDF_CONSOLIDADO = 5;
    private final int CARGAR_PAGADURIAS = 6;
    private final int CARGAR_NEGOCIO_LIBRANZA = 7;
    private final int ACTUALIZAR_PAGADURIA = 8;
    private final int CARGAR_TRAZABILIDAD_PAGADURIA = 9;
    private final int BUSCAR_SCORE_SOLICITUDES_CLIENTE = 10;
    private final int BUSCAR_SCORE_SOLICITUD = 11;
    private final int FILTROS_DUROS = 12;

    private AdministracionFintraDAO dao;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new AdministracionFintraImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_NEGOCIOS:
                    cargarNegocios();
                    break;
                case EXPORTAR_PDF_DETALLE:
                    exportaPdf(usuario.getLogin());
                    break;
                case CARGAR_PRESOLICITUDES_MICROCREDITO:
                    cargaPresolicitudesMicrocredito();
                    break;
                case ANULAR_PRESOLICITUD_MICROCREDITO:
                    this.rechazarnegociomicrocredito();
                    break;
                case EXPORTAR_PDF_CONSOLIDADO:
                    exportaPdfConsolidado(usuario.getLogin());
                    break;
                case CARGAR_PAGADURIAS:
                    cargarPagadurias();
                    break;
                case CARGAR_NEGOCIO_LIBRANZA:
                    consultarNegocioLibranza();
                    break;
                case ACTUALIZAR_PAGADURIA:
                    actualizarPagaduriaNegocio();
                    break;
                case CARGAR_TRAZABILIDAD_PAGADURIA:
                    cargarTrazabilidadPagaduria();
                    break;
                case BUSCAR_SCORE_SOLICITUDES_CLIENTE:
                    buscarScoreSolicitudesCliente();
                    break;
                case BUSCAR_SCORE_SOLICITUD:
                    buscarScoreSolicitud();
                    break;
                case FILTROS_DUROS:
                    FiltrosDuros();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarNegocios() {
        try {
            String cliente = request.getParameter("cliente") != null ? request.getParameter("cliente") : "";
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cargarNegocios(cliente, negocio), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void exportaPdf(String usuario) {
        //String json = "{\"respuesta\":\"GENERADO\"}";
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            dao.generarPdf(usuario, negocio);
            String respuestaJson = "{\"respuesta\":\"OK\"}";
            this.printlnResponseAjax(respuestaJson, "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargaPresolicitudesMicrocredito() {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String estado_solicitud = request.getParameter("estado_solicitud") != null ? request.getParameter("estado_solicitud") : "";
            String etapa = request.getParameter("etapa") != null ? request.getParameter("etapa") : "";
            String r_operaciones = request.getParameter("r_operaciones") != null ? request.getParameter("r_operaciones") : "";
            this.printlnResponseAjax(dao.cargaPresolicitudesMicrocredito(fechaInicio, fechaFin, identificacion, numero_solicitud, estado_solicitud, etapa, r_operaciones), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void rechazarnegociomicrocredito() {
        String json = "{\"respuesta\":\"Negocio Rechazado\"}";
        try {
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String causal = request.getParameter("causal") != null ? request.getParameter("causal") : "";
            String estado = request.getParameter("estado_pre") != null ? request.getParameter("estado_pre") : "";
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.rechazarnegociomicrocredito(usuario, numero_solicitud));
            tService.getSt().addBatch(this.dao.standbytrazabilidadpresolicitudes(usuario, numero_solicitud, causal, estado));
            tService.execute();
            //this.printlnResponseAjax(dao.rechazarnegociomicrocredito(usuario, numero_solicitud, causal), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void exportaPdfConsolidado(String usuario) {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            dao.generarPdfConsolidado(usuario, negocio);
            String respuestaJson = "{\"respuesta\":\"OK\"}";
            this.printlnResponseAjax(respuestaJson, "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarPagadurias() {
        try {
            this.printlnResponseAjax(dao.cargarPagadurias(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void consultarNegocioLibranza() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cargarNegocioLibranza(negocio), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void actualizarPagaduriaNegocio() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            String pagaduria = request.getParameter("pagaduria") != null ? request.getParameter("pagaduria") : "";
            this.printlnResponseAjax(dao.actualizarPagaduriaNegocio(negocio, pagaduria, usuario.getLogin()), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void cargarTrazabilidadPagaduria() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cargarTrazabilidadPagaduria(negocio), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void buscarScoreSolicitudesCliente() {
        try {
            String idParam = request.getParameter("identificador");
            int idCliente = idParam != null && !idParam.equals("") ? Integer.parseInt(idParam) : 0;
            String unidadNegocio = request.getParameter("unidadNegocio") != null ? request.getParameter("unidadNegocio") : "";
            String response = dao.buscarScoreSolicitudesCliente(idCliente, unidadNegocio);
            printlnResponseAjax(response, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void buscarScoreSolicitud() {
        try {
            String idParam = request.getParameter("identificador");
            int numeroSolicitud = idParam != null && !idParam.equals("") ? Integer.parseInt(idParam) : 0;
            String unidadNegocio = request.getParameter("unidadNegocio") != null ? request.getParameter("unidadNegocio") : "";
            String response = dao.buscarScoreSolicitud(numeroSolicitud, unidadNegocio);
            printlnResponseAjax(response, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void FiltrosDuros() {
        try {
            String opcionBuscar = request.getParameter("opcion_buscar");
            String idParam = request.getParameter("identificador");
            int numeroSolicitud = idParam != null && !idParam.equals("") ? Integer.parseInt(idParam) : 0;
            String response = dao.FiltrosDuros(opcionBuscar, numeroSolicitud);
            printlnResponseAjax(response, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
