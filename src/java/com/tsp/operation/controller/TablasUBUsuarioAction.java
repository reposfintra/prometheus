/******************************************************************
* Nombre ......................TablasUBUsuarioAction.java
* Descripci�n..................Clase Action para cargar las tablas de un usuario
* Autor........................David lamadrid
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class TablasUBUsuarioAction extends Action
{
    
    /** Creates a new instance of TablasUBUsuarioAction */
    public TablasUBUsuarioAction ()
    {
    }
    
    public void run () throws ServletException, InformationException
    {
       
        String next="";
        try {
            String sw=""+request.getParameter("sw");
            String nombre=""+request.getParameter("c_nombre");
            //////System.out.println("Nombre   "+nombre);
            model.usuarioService.obtenerUsuariosPorNombre (nombre);
            next="/jsp/general/consultas/usuarios.jsp?accion=1";
        }
        catch (Exception e) {
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
