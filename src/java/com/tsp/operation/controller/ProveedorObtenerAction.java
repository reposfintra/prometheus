
/********************************************************************
 *      Nombre Clase.................   ProveedorObtenerAction.java
 *      Descripci�n..................   Obtiene un registro del archivo proveedor.
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.09.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Andres
 */
public class ProveedorObtenerAction extends Action {
    
    /** Creates a new instance of ProveedorObtenerAction */
    public ProveedorObtenerAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next  = "/jsp/cxpagar/proveedor/ProveedorUpdate.jsp?mensaje=";
        
        String nit = request.getParameter("nit");
        String cia = request.getParameter("cia");
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute("Distrito");
        
        
        try{
            Proveedor prov = model.proveedorService.obtenerProveedor(nit, distrito);
            model.identidadService.buscarIdentidad( model.proveedorService.getProveedor().getNit_beneficiario(), "");
            model.proveedorService.getProveedor().setNom_beneficiario( model.identidadService.obtIdentidad().getNombre() );
            
            model.identidadService.buscarIdentidad(nit, "");
            Identidad id = model.identidadService.obtIdentidad();
            
            /*Ciudad, Pais, Estado de la identidad*/
            model.paisservice.buscarpais(id.getCodpais());
            String pais = (model.paisservice.obtenerpais()!=null)? model.paisservice.obtenerpais().getCountry_name() : "&nbsp;";
            model.estadoservice.buscarestado(id.getCodpais(), id.getCoddpto());
            String dpto = (model.estadoservice.obtenerestado()!=null)? model.estadoservice.obtenerestado().getdepartament_name() : "&nbsp;";
            model.ciudadService.buscarCiudad(id.getCodpais(), id.getCoddpto(), id.getCodciu());
            String ciudad = (model.ciudadService.obtenerCiudad()!=null)? model.ciudadService.obtenerCiudad().getNomCiu() : "&nbsp;";
            
            id.setCiudad_name(ciudad);
            id.setEstado_name(dpto);
            id.setPais_name(pais);
            
            model.identidadService.setIdentidad(id);
            
            String agencia = prov.getC_agency_id().equals("OP")?"":prov.getC_agency_id();
            
            model.servicioBanco.loadBancos(agencia, distrito);
            model.servicioBanco.loadSucursalesAgencia(agencia, prov.getC_branch_code(), distrito);
            model.agenciaService.loadAgencias();
            model.ciudadService.searchTreMapCiudades();
            
            model.tablaGenService.buscarCon_Proveedor();
            model.tablaGenService.buscarClasificacionPro();
            model.tablaGenService.buscarHandle_code();
            model.tablaGenService.buscarBanco();
            model.tblgensvc.buscarListaSinDato("TCUENTA", "PROVEEDOR");
            
            session.setAttribute("agency", prov.getC_agency_id());
            session.setAttribute("branch_code", prov.getC_branch_code());
            session.setAttribute("bank_account", prov.getC_bank_account());
                       
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
