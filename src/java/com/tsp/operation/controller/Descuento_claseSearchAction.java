    /*******************************************************************
 * Nombre clase: Descuento_claseSearchAction.java
 * Descripci�n: Accion para buscar un descuento por clase equipo a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 02:36 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class Descuento_claseSearchAction extends Action{
    
    /** Creates a new instance of Descuento_claseSearchAction */
    public Descuento_claseSearchAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        String clase = "";
        String codigo = "";
        String frecuencia = "";
        String mes = "";
        String ano = "";
        HttpSession session = request.getSession ();
        String distrito = (String) session.getAttribute ("Distrito");
        String listar = (String) request.getParameter ("listar");
        try{
            if (listar.equals ("True")){
                next="/jsp/equipos/descuento_clase/descuento_claseListar.jsp";
                String sw = (String) request.getParameter ("sw");
                if(sw.equals ("True")){
                    clase = (String) request.getParameter ("c_clase").toUpperCase ();
                    codigo = (String) request.getParameter ("c_codigo").toUpperCase ();
                    frecuencia = (String) request.getParameter ("c_frecuencia");
                    mes = (!request.getParameter("c_mes").equals (""))?request.getParameter("c_mes"):"%";
                    ano = (!request.getParameter("c_ano").equals (""))?request.getParameter("c_ano"):"%";
                    mes = ano+"-"+mes;
                    //System.out.println(mes);
                }
                else{
                    clase = "";
                    codigo = "";
                    frecuencia = "";
                    mes = "";
                }
                model.descuento_claseService.searchDetalleDescuento_clase(clase,codigo,frecuencia,mes,distrito);
            }
            else{
                clase = (String) request.getParameter ("c_clase").toUpperCase ();
                codigo = (String) request.getParameter ("c_codigo").toUpperCase ();
                model.descuento_claseService.searchDescuento_clase(clase, distrito, codigo);
                Descuento_clase d = model.descuento_claseService.getDescuento_clase(); 
                model.concepto_equipoService.searchConcepto_equipo (d.getCodigo_concepto());
                next="/jsp/equipos/descuento_clase/descuento_claseModificar.jsp";
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);         
    }
    
}
