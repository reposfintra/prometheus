/********************************************************************
 *      Nombre Clase.................   Campos_jspCargar.java    
 *      Descripci�n..................   Anula un campo en el archivo de campos_jsp.
 *      Autor........................   Ing. Rodrigo Salazar
 *      Fecha........................   18.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author  Rodrigo
 */
public class Campos_jspInsertAction extends Action{
    
    /** Creates a new instance of Campos_jspInsertAction */
    public Campos_jspInsertAction() {
    }
    
    public void run() throws ServletException, InformationException{
        
        String next = "/jsp/trafico/permisos/campos_jsp/Campos_jspInsertar.jsp?mensaje=";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");      
        String jsp = request.getParameter("c_pagina");
        String campo = request.getParameter("c_campo");
        try{
            if(!model.camposjsp.existcampos_jsp(jsp, campo)){
                campos_jsp campos = new campos_jsp();
                campos.setCampo(campo);
                campos.setPagina(jsp);
                campos.setCia(usuario.getCia().toUpperCase());
                campos.setUser_update(usuario.getLogin().toUpperCase());
                campos.setCreation_user(usuario.getLogin().toUpperCase());
                model.camposjsp.insertcampos_jsp(campos);
                request.setAttribute("mensaje","Proceso exitoso!!!");
                next = "/jsp/trafico/permisos/campos_jsp/Campos_jspInsertar.jsp?mensaje=Proceso Exitoso";
            }
            else{
                next = "/jsp/trafico/permisos/campos_jsp/Campos_jspInsertar.jsp?mensaje=Ya existe el Campo";
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
