/*
 * RemesasPlanillaAction.java
 *
 * Created on 29 de agosto de 2005, 11:29 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Jm
 */
public class RemesasPlanillaAction extends Action{
    
    /** Creates a new instance of RemesasPlanillaAction */
    public RemesasPlanillaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            String next = "/impresion/remesaPlanilla.jsp";
            String []Impresa  = request.getParameter("Planillas").split("~");
            String datos = "";
            String separador = "'";
            for(int i=0;i<Impresa.length;i++){
                //////System.out.println(" PLANILLA " + Impresa[i]);
                datos += separador+Impresa[i]+separador+",";
            }
            datos = datos.substring(0,datos.length()-1);
            
            model.RemesaSvc.buscaRemesa_no_impresaPlanillas(datos);
           
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
        
    }
    
}
