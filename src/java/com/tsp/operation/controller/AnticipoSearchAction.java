/*
 * AnticipoSearchAction.java
 *
 * Created on 3 de diciembre de 2004, 08:29 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AnticipoSearchAction extends Action {
    
    /** Creates a new instance of AnticipoSearchAction */
    public AnticipoSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/anticipo/anticipoUpdate.jsp";
        if(request.getParameter("num").equals("2"))
            next="/anticipo/anticipoDelete.jsp";
        try{
            String nit= request.getParameter("nit");
            String sucursal=request.getParameter("sucursal");
            model.proveedoranticipoService.buscaProveedor(nit,sucursal);
            if(model.proveedoranticipoService.getProveedor()!=null){
                Proveedor_Anticipo pa= model.proveedoranticipoService.getProveedor();
                
                request.setAttribute("panticipo",pa);
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
