/*
 * Nombre        InfoClienteSearchAction.java
 * Descripci�n   Clase para ejecutar las acciones del reporte de informaci�n al cliente.
 * Autor         (desconocido) por los comentarios en ingles se supone que es Ivan Erazo
 * Fecha         (desconocida)
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.beans.Usuario;
import org.apache.log4j.Logger;
import com.tsp.util.Utility;
import javax.servlet.jsp.*;
import javax.servlet.http.*;

/**
 * Searches the database for customers matching the
 * customer search argument
 */
public class InfoClienteSearchAction extends Action {
    
    int llamados = 0;
    
    static Logger logger = Logger.getLogger(InfoClienteSearchAction.class);
    
    /**
     * Executes the action
     */
    public void run() throws ServletException, InformationException {
        String next = "/jsp/sot/body/PantallaFiltroReporteInfoCliente.jsp?nombreCliente=";
        try {
            String cmd = request.getParameter("cmd");
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            Hashtable bean = new Hashtable();
            bean.put("fechaini", "hoy");
            bean.put("fechafin", "hoy");
            bean.put("listaTipoViaje", "ALL");
            bean.put("tipo_carga", "ALL");
            bean.put("criteria", "");
            bean.put("userDefValue", "");
            bean.put("cliente", "");
            model.tablaGenService.buscarTipodocumentos();
            
            
            if(cmd == null){
                model.reporteInfoClienteService.buscarTiposDeCarga(request.getParameter("cliente"),usuario.getLogin());
            }
            else if ( cmd.equals("update") ){
                bean.put("fechaini", noNull(request.getParameter("fechaini")));
                bean.put("fechafin", noNull(request.getParameter("fechafin")));
                bean.put("tipoViaje", noNull(request.getParameter("tipoViaje")));
                bean.put("tipo_carga", noNull(request.getParameter("tipo_carga")));
                bean.put("criteria", "");
                
                bean.put("userDefValue", noNull(request.getParameter("userDefValue")));
                bean.put("cliente",noNull(request.getParameter("cliente")));
                model.reporteInfoClienteService.buscarTiposDeCarga(request.getParameter("cliente"),usuario.getLogin());
                next += request.getParameter("nombreCliente")+"&clienteSelected="+request.getParameter("cliente");
            }
            else{
                next = "/jsp/sot/reports/ReporteInfoCliente.jsp";
                
                // Perform search
                String [] args = new String[11];
                args[0] = request.getParameter("cliente");
                //args[0] = request.getParameter("clientesConsultados");
                //logger.info("Parametro clientesConsultados: " + args[0]);
                args[1] = request.getParameter("fechaini");
                args[2] = request.getParameter("fechafin");
                args[3] = request.getParameter("listaTipoViaje");
               /* ��� OJO !!!:
                * Queda pendiente el par�metro: "Estado de viaje".
                * Se dejar� su espacio en el arreglo de par�metros hasta que se den
                * las especificaciones para su utilizaci�n.
                * Este comentario viene del sot.
                */
                
                args[4] = ""; //request.getParameter("viajes");
                args[5] = request.getParameter("userDefValue");
                args[6] = "";
                args[7] = request.getParameter("tipo_carga");
                args[8] = request.getParameter("mostrarPEC");
                args[9] = request.getParameter("todosLosClientes");
                args[10] = usuario.getLogin();
                for( int idx = 0; idx < 6; idx++ ) {
                    if (args[idx] != null)
                        args[idx] = args[idx].trim();
                    else
                        args[idx] = "";
                }
                
                
                String view = request.getParameter("displayInExcel");
                if(view!=null && !view.equals("File")){
                    model.reporteInfoClienteService.buscarDatosDeReporte( args );
                }
                else if(view!=null && view.equals("File")){
                    String notas                  =  (String) session.getAttribute("notas");
                    
                    // obtenemos los parametros
                    Hashtable headerFields = new Hashtable();
                    headerFields.clear();
                    String cliente       = args[9].equals("true")?"TODOS":request.getParameter("nombreCliente");
                    String userDefValue  = request.getParameter("userDefValue");
                    String searchCriteria = "";
                    headerFields.put( "nombreCliente", cliente );
                    String fechaini       = request.getParameter("fechaini");
                    String fechafin       = request.getParameter("fechafin");
                    String listaTipoViaje = request.getParameter("listaTipoViaje");
                    if( fechaini != null && !fechaini.equals("") ) {
                        headerFields.put( "fechaini", fechaini );
                        headerFields.put( "fechafin", fechafin );
                    }
                    headerFields.put( "listaTipoViaje", listaTipoViaje );
                    
                    /*if( searchCriteria.equals("ot") ) {
                        headerFields.put( "criteria", "OT" );
                    }
                    else if( searchCriteria.equals("facturacial") ){
                        headerFields.put( "criteria", "Factura Comercial" );
                    }
                    else if( searchCriteria.equals("docinterno") ){
                        headerFields.put( "criteria", "Documento Interno" );
                    }*/
                    
                    headerFields.put( "userDefValue", userDefValue );
                    String ruta = this.application.getRealPath("/WEB-INF/");
                    headerFields.put( "rutaPaginaLastUpdate", ruta );
                    
                    HExportarExcel_InfoCliente hilo  =  new HExportarExcel_InfoCliente();
                    hilo.start(model,usuario , headerFields, args, notas);
                    
                    next = "/jsp/sot/body/PantallaFiltroReporteInfoCliente.jsp";
                    next+="?comentario=Su proceso de Exportaci�n de Informaci�n al Cliente se ha iniciado...Puede consultar en [ Manejo de Archivos ]";
                    model.reporteInfoClienteService.buscarTiposDeCarga(request.getParameter("cliente"),usuario.getLogin());
                }
                if (usuario != null ){
                    logger.info(  usuario.getNombre() + " ejecuto infocliente con: Cliente " + args[0]
                    + " Fecha Inicial: " + args[1] + " Fecha Final: " + args[2] + " Tipo Viaje: " + args[3] );
                }
                
            }
            request.setAttribute("bean", bean);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
    private String noNull(String x){
        return x == null?"":x;
    }
}
