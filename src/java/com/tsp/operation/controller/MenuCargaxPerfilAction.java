/*
 * Nombre        MenuCargaxPerfilAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         30 de enero de 2006, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;


public class MenuCargaxPerfilAction extends Action {
    
    private Vector opciones;
    private String next = "/Marcos.jsp";
    
    /**
     * Crea una nueva instancia de  MenuCargaxPerfilAction.java
     */
    public MenuCargaxPerfilAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        try{
            cargarMenu();            
        }catch( Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        ////System.out.println("NEXT " + next);
        this.dispatchRequest(next);
    }
    
    public synchronized void cargarMenu() throws Exception{
        String estado = request.getParameter("cmd");
        HttpSession session = request.getSession();
        String direccion = (request.getParameter("direccion") != null)?request.getParameter("direccion"):"";
        int opcion = (request.getParameter("idop")!= null)?Integer.parseInt(request.getParameter("idop")):0;
        try{
            //model.publicacionService.getTreeMapTitulos();
            if (estado.equalsIgnoreCase("cgamenu")){//carga del menu
                String perfil = (String) session.getAttribute("Perfil");                
                
                //-------
                //Usuario u = (Usuario) session.getAttribute("Usuario");                                
                //System.out.println("ARMA POR USUARIO " + u.getLogin());
                //model.menuService.cargarMenuxUsuario(u.getLogin());
                //----------------
                
                //Vector vmenu = model.menuService.getVMenu();
                next = "/Marcos.jsp";
            }
            else if (estado.equalsIgnoreCase("cgaop")){//carga de opciones en insertar menu  -- exploracion
                model.menuService.obtenerPadreMenuxIDopcion(opcion);
                model.menuService.obtenerHijos(opcion);
                next = direccion;
            }
            else if (estado.equalsIgnoreCase("modop")){//carga de opciones en modificar menu  -- exploracion
                Menu m = model.menuService.buscarOpcionMenuxIdopcion(opcion);
                model.menuService.obtenerPadreMenuxIDopcion(opcion);
                model.menuService.obtenerHijos(opcion);
                next = direccion;
            }
            else if (estado.equalsIgnoreCase("cgaperfil")){//carga listado de opciones para nuevo perfil
                opciones = new Vector();
                next = "/jsp/trafico/perfil/Perfil.jsp?tipo=Insertar&msg=";
                //obtengo opciones del sistema en orden y armo un vector
                model.menuService.obtenerPadresRaizMenu();
                Vector raiz = model.menuService.getVMenu();
                
                for (int i = 0; i < raiz.size(); i++){
                    Menu m = (Menu) raiz.elementAt(i);
                    String mnom = m.getNombre();
                    int idpadre = m.getIdpadre();
                    
                    //inserto opcion
                    opciones.addElement(m);
                    ////System.out.println("INSERTO OPCION RAIZ " + m.getNombre());
                    if( m.getSubmenu() == 1){//carpeta
                        //busco hijos
                        hijos(m.getIdopcion());
                    }
                }
                model.menuService.setOpciones(opciones);               
                //si es modificacion/anulacion obtiene vector de opciones activas por perfil
                if (( request.getParameter("tipo")  != null ) &&
                        ( !request.getParameter("tipo").equals("Insertar"))){                    
                    model.perfilService.opcionesActivasxPerfil(request.getParameter("idp"));                    
                    next = "/controller?estado=Perfil&accion=Buscar&carpeta=/jsp/trafico/perfil&pagina=Perfil.jsp&idp="+
                            request.getParameter("idp")+"&tipo=objeto";
                }
            }
        }catch( Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
    
    public void hijos( int opcion ){
        try{
            model.menuService.obtenerHijos(opcion);
            Vector hijos = model.menuService.getVhijos();
            for ( int i = 0; i < hijos.size(); i++ ){
                Menu hijo = (Menu) hijos.elementAt(i);
                //inserto hijo
                opciones.addElement(hijo);
                ////System.out.println("INSERTO HIJO " + hijo.getNombre() + " NIVEL " + hijo.getNivel());
                
                if (hijo.getSubmenu()==1){
                    //busco hijos
                    hijos( hijo.getIdopcion() );
                }
            }
        }catch( SQLException ex){
            ex.printStackTrace();
        }
    }
}