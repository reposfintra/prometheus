/*
 * TiqueteSearchAction.java
 *
 * Created on 3 de diciembre de 2004, 10:11 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class TiqueteSearchAction extends Action{
    
    /** Creates a new instance of TiqueteSearchAction */
    public TiqueteSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/tiquetes/tiketUpdate.jsp";
        if(request.getParameter("num").equals("2"))
            next="/tiquetes/tiketDelete.jsp";
        try{
            String nit= request.getParameter("nit");
            String sucursal = request.getParameter("sucursal");
            model.proveedortiquetesService.buscaProveedor(nit,sucursal);
            if(model.proveedortiquetesService.getProveedor()!=null){
                Proveedor_Tiquetes pt= model.proveedortiquetesService.getProveedor();
                request.setAttribute("ptiquetes",pt);
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
