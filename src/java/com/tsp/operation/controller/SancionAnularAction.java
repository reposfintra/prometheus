/***********************************************
 * Nombre clase: SancionAnularAction.java
 * Descripci�n: Accion para anular una sanci�n a la bd.
 * Autor: Jose de la rosa
 * Fecha: 19 de octubre de 2005, 06:40 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Jose
 */
public class SancionAnularAction extends Action{
    /** Creates a new instance of SancionAnularAction */
    public SancionAnularAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession ();
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        int cod_sancion = Integer.parseInt (request.getParameter ("c_cod_sancion").toUpperCase ());
        String numpla = (request.getParameter ("c_numpla").toUpperCase ());
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        try{
            Sancion s = new Sancion ();
            s.setCod_sancion (cod_sancion);
            s.setNumpla (numpla);
            s.setUsuario_modificacion (usuario.getLogin ());
            model.sancionService.anularSancion (s);
            request.setAttribute ("msg","Sanci�n Anulada");
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
