package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Persona;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.WSHistCreditoService;
import com.tsp.operation.model.threads.HHistoriaDeCredito;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 * Action para consultar la historia de credito de Datacredito
 * 13/12/2011
 * @author darrieta
 */
public class HistoriaCreditoAction extends Action {

    Usuario usuario;
    String next = "";
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");            
            String opcion = request.getParameter("opcion");
            
            if(opcion.equals("GENERAR_PDF")){
                generarPDF();
            }

            this.dispatchRequest(next);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void generarPDF() throws InterruptedException, Exception{
        
        String tipoIdentificacion = request.getParameter("tipoIdentificacion");
        String identificacion = request.getParameter("identificacion");

        String vista = request.getParameter("vista");

        //Valida que la identificacion exista
        WSHistCreditoService srv = new WSHistCreditoService(usuario.getBd());
        if (srv.existePersona(new Persona(tipoIdentificacion, identificacion))) {
            HHistoriaDeCredito hilo = new HHistoriaDeCredito();
            hilo.start(usuario, tipoIdentificacion, identificacion, vista);
            hilo.join();

            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentType("application/pdf");

            next = "/exportar/migracion/" + usuario.getLogin() +"/HC_"+identificacion+".pdf";
        }else{
            next = "/jsp/historiaCredito/consultaHistoriaCredito.jsp?mensaje=La identificacion ingresada no existe "+identificacion;
        }
            
    }
    
}
