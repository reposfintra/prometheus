/*
 * Nombre        UnidadAnularAction.java
 * Autor         Ing Jose de la Rosa
 * Modificado    Ing Sandra Escalante
 * Fecha         26 de junio de 2005, 11:15 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UnidadAnularAction extends Action{
    
    /** Creates a new instance of UnidadAnularAction */
    public UnidadAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        String atipo = request.getParameter("c_atipo");
        
        try{
            
            Unidad unidad = new Unidad();
            unidad.setDescripcion(descripcion);
            unidad.setCodigo(codigo);
            unidad.setTipo(atipo);
            unidad.setUser_update(usuario.getLogin().toUpperCase());
            unidad.setCreation_user(usuario.getLogin().toUpperCase());
            model.unidadTrfService.anularUnidad(unidad);
            request.setAttribute("mensaje","MsgAnulado");            
        
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
