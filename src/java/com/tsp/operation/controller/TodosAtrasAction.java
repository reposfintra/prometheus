/*
 * TodosAtrasAction.java
 *
 * Created on 20 de marzo de 2005, 04:42 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class TodosAtrasAction extends Action {
    
    public TodosAtrasAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        HttpSession session = request.getSession();
        String lenguaje = (String) session.getAttribute("idioma");
        String next=request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        
        try{ 
            request.setAttribute("msg", "");
            /*request.setAttribute("estados", "");
            request.setAttribute("ciudades", "");*/
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
