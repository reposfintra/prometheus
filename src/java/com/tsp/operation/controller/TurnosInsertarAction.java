/********************************************************************
 *      Nombre Clase.................   TurnosIngresarAction.java
 *      Descripci�n..................   Action para Acceder al modulo de turnos en trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  dlamadrid
 */
public class TurnosInsertarAction extends Action{
    
    String usuario_turno="";
    int h_entrada;
    int h_salida;
    int m_entrada;
    int m_salida;
    String h_e;
    String h_s;
    String dia ="";
    
    /** Creates a new instance of TurnosInsertarAction */
    public TurnosInsertarAction() {
    }
    public void run() throws ServletException, InformationException {
        String next="";
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin=""+usuario.getLogin();
            
            String ms="";
            this.usuario_turno=""+ request.getParameter("usuario");
            String fecha_turno=""+request.getParameter("fecha");
            String fecha_turno2 = "";
            String[] zonas = request.getParameterValues("zonas");
            this.h_entrada=Integer.parseInt(request.getParameter("h_entrada"));
            this.h_salida= Integer.parseInt(request.getParameter("h_salida"));
            this.m_entrada=Integer.parseInt(request.getParameter("m_entrada"));
            this.m_salida=Integer.parseInt(request.getParameter("m_salida"));
            this.h_e=request.getParameter("h_e");
            this.h_s=request.getParameter("h_s");
            this.dia =request.getParameter("dia");
            
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            Calendar c3 = Calendar.getInstance();
            
            
            int dd1   = Integer.parseInt( fecha_turno.substring( ( fecha_turno.lastIndexOf('-') +1),fecha_turno.length()) );
            int mm1   = Integer.parseInt( fecha_turno.substring( ( fecha_turno.indexOf('-') +1) ,fecha_turno.lastIndexOf('-')) );
            int aaaa1   = Integer.parseInt( fecha_turno.substring(0, fecha_turno.indexOf('-')) );
            
            c1.set( aaaa1, mm1-1, dd1 );
            c3.set( aaaa1, mm1-1, dd1+1 );
            c2.set( aaaa1, mm1-1, dd1 );
            
            if( request.getParameter("semanal") != null ){
                fecha_turno2=""+request.getParameter("fecha2");
                
                int dd2   = Integer.parseInt( fecha_turno2.substring( ( fecha_turno2.lastIndexOf('-')+1 ),fecha_turno2.length()) );
                int mm2   = Integer.parseInt( fecha_turno2.substring( ( fecha_turno2.indexOf('-')+1 ),fecha_turno2.lastIndexOf('-')) );
                int aaaa2   = Integer.parseInt( fecha_turno2.substring(0, fecha_turno2.indexOf('-')) );
                
                c3.set( aaaa2, mm2-1, dd2+1 );
            }
            
            String zona="";
            for(int i=0;i<zonas.length;i++){
                if(i==0){
                    zona=zonas[i];
                }
                else{
                    zona=zona + "," + zonas[i];
                }
            }
            int cont = 0;
            
            if( this.validarRango(c2,c3) ){
                //////System.out.println(c1.get(c1.YEAR)+"-"+ ( (( c1.get(c1.MONTH) ) < 10)? "0"+c1.get(c1.MONTH) : ""+c1.get(c1.MONTH) ) +"-"+( (( c1.get(c1.DATE) ) < 10)? "0"+c1.get(c1.DATE) : ""+c1.get(c1.DATE) ));
                //////System.out.println(c3.get(c3.YEAR)+"-"+ ( (( c3.get(c3.MONTH) ) < 10)? "0"+c3.get(c3.MONTH) : ""+c3.get(c3.MONTH) ) +"-"+( (( c3.get(c3.DATE) ) < 10)? "0"+c3.get(c3.DATE) : ""+c3.get(c3.DATE) ));
                while( c1.before(c3) ){
                    cont++;
                    fecha_turno = c1.get(c1.YEAR)+"-"+ ( (( c1.get(c1.MONTH) +1 ) < 10)? "0"+( c1.get(c1.MONTH)+1 ): ""+ ((c1.get(c1.MONTH))+1 )) +"-"+( (( c1.get(c1.DATE) ) < 10)? "0"+c1.get(c1.DATE) : ""+c1.get(c1.DATE) );
                    Turnos turno= new Turnos();
                    turno.setUsuario_turno(usuario_turno);
                    turno.setFecha_turno(fecha_turno);
                    turno.setCreation_user(userlogin);
                    turno.setBase(usuario.getBase());
                    turno.setDstrct(usuario.getDstrct());
                    turno.setZona(zona);
                    
                    String HoraEntrada="";
                    String HoraSalida="";
                    if(dia.equals("a")){
                        HoraEntrada = model.turnosService.generarHora(h_e, h_entrada,m_entrada);
                        HoraSalida  = model.turnosService.generarHora(h_s, h_salida,m_salida);
                        turno.setH_entrada(HoraEntrada);
                        turno.setH_salida(HoraSalida);
                        model.turnosService.setTurno(turno);
                        if(model.turnosService.turnoValido(usuario_turno,fecha_turno,HoraEntrada,HoraSalida)==false){
                            model.turnosService.insertar();
                            ms=cont+" Turno(s) agregado(s) en el Sistema";
                        }
                        else{
                            ms="No se pudo insertar el turno ya esta dentro de un rango de otro turno";
                            cont--;
                        }
                    }
                    else{
                        
                        if((model.turnosService.turnoValido(usuario_turno,fecha_turno,model.turnosService.generarHora(h_e, h_entrada,m_entrada),"00:00:00")==false) || (model.turnosService.turnoValido(usuario_turno,Util.fechaFinal(fecha_turno+" 00:00:00",1),"00:00:00",model.turnosService.generarHora(h_s, h_salida,m_salida))==false)){
                            
                            HoraEntrada= model.turnosService.generarHora(h_e, h_entrada,m_entrada);
                            //                if( model.turnosService.turnoValido(usuario_turno,fecha_turno,HoraEntrada,"23:59:59")==false ){
                            
                            HoraSalida="23:59:59";
                            turno.setH_entrada(HoraEntrada);
                            turno.setH_salida(HoraSalida);
                            model.turnosService.setTurno(turno);
                            model.turnosService.insertar();
                            
                            HoraEntrada="00:00:00";
                            HoraSalida=model.turnosService.generarHora(h_s, h_salida,m_salida);
                            fecha_turno=fecha_turno+" 00:00:00";
                            //////System.out.println(fecha_turno+"FECHA FINAL"+Util.fechaFinal(fecha_turno,1));
                            turno.setFecha_turno(Util.fechaFinal(fecha_turno,1));
                            turno.setH_entrada(HoraEntrada);
                            turno.setH_salida(HoraSalida);
                            model.turnosService.setTurno(turno);
                            model.turnosService.insertar();
                            
                            //}else{
                            //       ms+=" No se pudo ingresar el turno "+fecha_turno+" , "+HoraEntrada+" a 23:59:59";
                            //                       cont--;
                            //                }
                        }
                        else{
                            ms="No se pudieron insertar los registros en Turnos";
                            cont--;
                        }
                        
                    }
                    c1.set( c1.get(c1.YEAR),c1.get(c1.MONTH),(c1.get(c1.DATE) )+1 );
                }
            }else{
                ms += "No se insertaron los turnos, verifique que no exista un turno asignado en el rango dado para el usuario "+usuario_turno;
            }
            if( cont >0 && dia.equals("S") ){ ms += "Se Insertaron "+( cont*2 )+" Turnos en el Sistema"; }
            /*model.usuarioService.getUsuariosPorDpto ("traf");
            model.zonaService.listarZonasxUsuario (userlogin);*/
            model.usuarioService.getUsuariosPorDpto("traf");
            TreeMap tm = model.usuarioService.usuariosPorDptoT();
            String login = (String) tm.get(tm.firstKey());
            model.zonaService.listarZonasxUsuario(login);
            //////System.out.println(".............................. LOGIN: " + login);
            next="/jsp/trafico/turnos/insertar.jsp?marco=no&ms="+ms;
        }
        catch (Exception e) {
            throw new ServletException(e.getMessage());
            
        }
        this.dispatchRequest(next);
    }
    
    public boolean validarRango(Calendar c1, Calendar c3){
        String fecha_turno = "";
        boolean valido = true;
        Calendar c12 = c1;
        
        String HoraEntrada="";
        String HoraSalida="";
        
        try{
            
            while( c12.before(c3) ){
                
                fecha_turno = c12.get(c12.YEAR)+"-"+ ( (( c12.get(c12.MONTH)+1 ) < 10)? "0"+ ( c12.get(c12.MONTH )+1 )  : ""+( c12.get(c12.MONTH)+1 ) ) +"-"+( (( c12.get(c12.DATE) ) < 10)? "0"+c12.get(c12.DATE) : ""+c12.get(c12.DATE) );
                ////System.out.println( fecha_turno );
                
                if(dia.equals("a")){
                    HoraEntrada = model.turnosService.generarHora(h_e, h_entrada,m_entrada);
                    HoraSalida  = model.turnosService.generarHora(h_s, h_salida,m_salida);
                    if( !model.turnosService.turnoValido(usuario_turno,fecha_turno,HoraEntrada,HoraSalida)==false ){
                        valido = false;
                        break;
                    }
                }
                else{
                    if( !( (model.turnosService.turnoValido(usuario_turno,fecha_turno,model.turnosService.generarHora(h_e, h_entrada,m_entrada),"00:00:00")==false) || (model.turnosService.turnoValido(usuario_turno,Util.fechaFinal(fecha_turno+" 00:00:00",1),"00:00:00",model.turnosService.generarHora(h_s, h_salida,m_salida))==false) ) ){
                        valido = false;
                        break;
                    }
                }
                c12.set( c12.get(c12.YEAR),c12.get(c12.MONTH),(c12.get(c12.DATE) )+1 );
            }
        }catch(Exception ex){
            ////System.out.println(" error en TurnosInsertarAction.validarRango --> "+ex.getMessage());
        }
       
        return valido;
    }
}