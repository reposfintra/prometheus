/*
 * InformacionTramo.java
 *
 * Created on 19 de agosto de 2005, 11:28 AM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  INTEL
 */
public class InformacionTramoAction extends Action {
    
    /** Creates a new instance of InformacionTramo */
    public InformacionTramoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/demora/asignarDemoraTramoParam.jsp";
        this.dispatchRequest(next);
    }
    
}
