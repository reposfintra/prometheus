/***************************************
* Nombre Clase ............. CrearPrestamoAction.java
* Descripci�n  .. . . . . .  Realiza los eventos de la creacion de prestamo
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  09/02/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/




package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.*;



public class CrearPrestamoAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {

        javax.servlet.http.HttpSession session           = request.getSession();
        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
        
        try{
            
            String    next      = "/jsp/cxpagar/prestamos/CrearPrestamo.jsp?msj=";
            String    msj       = "";
            Prestamo  prestamo  = new Prestamo();
            String    opcion    = request.getParameter("evento");
            String    user      = request.getParameter("usuario");
            
            
            if(opcion!=null  && !opcion.equals("CANCEL")){                
                
                   prestamo  = Prestamo.loadRequest(request);                                      
                   List listaAmortizacion  = FuncionesFinancieras.getAmortizacion(prestamo);
                   prestamo.setAmortizacion(listaAmortizacion);
                   
                   if(opcion.equals("COMPLETAR")){
                       msj = completarDatosPorPlaca( prestamo );
                       next += msj;
                   }                    
                   
                   
                   // GRABAR PRESTAMO
                   if(opcion.equals("SAVE")){
                       
                        model.proveedorService.obtenerProveedorPorNit( prestamo.getBeneficiario() );
                        Proveedor pro = model.proveedorService.getProveedor();
                        if ( pro!=null){
                            msj = model.PrestamoSvc.save(prestamo, user);
                            prestamo  = new Prestamo();
                        } else  {
                            msj = "El propietario no existe";
                        }
                        next += msj;
                   }

                   // VISTA TABLA AMORTIZACION
                   if(opcion.equals("VIEW")){
                       next  = "/jsp/cxpagar/prestamos/Amortizacion.jsp?noedit=true";
                       request.setAttribute("prestamo",prestamo);
                   }
               
            }
           
            model.PrestamoSvc.setPrestamo(prestamo);
            
            
             RequestDispatcher rd = application.getRequestDispatcher(next);
             if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
             rd.forward(request, response);
            
        }catch(Exception e){
            throw new ServletException( e.getMessage() );
        }
    }
    
    
    
    public String completarDatosPorPlaca(Prestamo p) throws Exception{
        try{
            
            if (p.getTipoPrestamo().equals("EQ") && p.getPlaca().equals("")){
                return "El tipo de prestamo requiere que se digite una placa";
            }
                
                
            if ( !p.getPlaca().equals("") ){
                model.placaService.buscaPlaca( p.getPlaca() );
                Placa placa =model.placaService.getPlaca();
                if (placa==null){
                    return "La placa digitada no existe o esta anulada";
                } else if ( placa.isVetado() ){
                    return "La placa digitada esta vetada";
                } else{
                    p.setBeneficiario( placa.getPropietario() );
                    model.proveedorService.obtenerProveedorPorNit( p.getBeneficiario() );
                    Proveedor pro = model.proveedorService.getProveedor();
                    if ( pro!=null){
                        p.setBeneficiarioName( pro.getC_payment_name() );
                    } else {
                        return "La proveedor de la placa no esta registrado o esta activo en el sistema";
                    }
                }
            }
                   
                   
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage());
        }
        return "";
        
    }
    
}
