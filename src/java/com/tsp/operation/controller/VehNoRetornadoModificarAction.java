/******************************************************************
* Nombre ......................VehNoRetornadoModificarAction.java
* Descripci�n..................Clase Action para tabla general
* Autor........................Armando Oviedo
* Fecha........................19/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class VehNoRetornadoModificarAction extends Action{
    
    /** Creates a new instance of VehNoRetornadoModificarAction */
    public VehNoRetornadoModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/VehNoRetornado/VehNoRetornadoModificar.jsp";
        try{
            String mensaje = request.getParameter("mensaje");
            HttpSession session = request.getSession();            
            String fecha = request.getParameter("fecha");
            String placa = request.getParameter("placa");
            String causa = request.getParameter("causa");            
            Usuario usuario = (Usuario) session.getAttribute("Usuario");                                            
            if(mensaje!=null){
                if(mensaje.equalsIgnoreCase("modificar")){                    
                    VehNoRetornado tmp = new VehNoRetornado();
                    tmp.setFecha(fecha);
                    tmp.setPlaca(placa);
                    tmp.setCausa(causa);                    
                    tmp.setUserUpdate(usuario.getLogin());
                    tmp.setLastUpdate("now()");
                    model.vehnrsvc.setVehiculoNoRetornado(tmp);
                    model.vehnrsvc.updateVehiculoNoRetornado();        
                    next+="?reload=ok&mensajemod=Vehiculo no retornado, modificado correctamente";
                }
                else if(mensaje.equalsIgnoreCase("eliminar")){                    
                    VehNoRetornado tmp = new VehNoRetornado();
                    tmp.setFecha(fecha);
                    tmp.setPlaca(placa);
                    tmp.setCausa(causa);
                    tmp.setUserUpdate(usuario.getLogin());
                    model.vehnrsvc.setVehiculoNoRetornado(tmp);
                    model.vehnrsvc.deleteVehiculoNoRetornado();
                    next+="?reload=ok&mensajemod=Vehiculo no retornado, eliminado correctamente";
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
