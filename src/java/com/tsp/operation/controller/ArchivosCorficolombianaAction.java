//que viva CORFICOLOMBINA
/*
 * ArchivosCorficolombianaAction.java
 * Created on 27 de octubre de 2008, 10:55
 */
package com.tsp.operation.controller;
/**
 * @author  navi 
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.exceptions.*;
import com.tsp.util.LogWriter;
import com.tsp.operation.controller.Action;
import com.tsp.operation.model.Model;

public class ArchivosCorficolombianaAction extends Action {
   
    private PrintWriter pw;
    private LogWriter logWriter;
   
    java.text.SimpleDateFormat fecha;
     
    String fechadoc;
   
    java.util.Date pdate;      
    
    private  FileWriter        fw265;
    private  BufferedWriter    bf265;
    private  PrintWriter       linea265;
    
    public ArchivosCorficolombianaAction() {        
    }
    
    public void run() throws ServletException,InformationException{
        HttpSession session = request.getSession();        
        Usuario u = (Usuario) session.getAttribute("Usuario");
        String user=u.getLogin();
        String msg = "Proceso iniciado!";
        String fecini2=request.getParameter("fecini2");
        String fecfin2=request.getParameter("fecfin2");
        String next = "/jsp/corficolombiana/generacionArchivos.jsp?msg=" + msg;
        String num=((String)(""+(Math.random()*1000))).substring(0,3);        
        String tipoarchivo=request.getParameter("tipoarchivo");
        //System.out.println("tipoarchivo"+tipoarchivo+"fecini2"+fecini2);
        try{
            Calendar hoy = Calendar.getInstance();
            hoy.add(hoy.DATE, -1);

            pdate = hoy.getTime();
            
            fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
                        
            fechadoc = fecha.format(pdate);
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
            File file = new File(ruta);
            file.mkdirs();

            pw = new PrintWriter(System.err, true);
            
             String ruta265      =  ruta + "/corficolombiana"+fechadoc+"_"+num+"_"+tipoarchivo+".txt";//model.DirectorioSvc.getUrl() + this.usuario.getLogin()  +"/MSF265_"+  hora   +".txt";
             this.fw265          = new FileWriter    ( ruta265     );
             this.bf265          = new BufferedWriter( this.fw265  );
             this.linea265       = new PrintWriter   ( this.bf265  );
             //linea265.println( "hola"   );
                        
            model.archivosCorficolombianaService.generarArchivo(fecini2,fecfin2, linea265,tipoarchivo);
            
            linea265.close();
        }catch(Exception e){
            System.out.println("error en ArchivosCorficolombianaAction:"+e.toString()+"__"+e.getMessage());
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }    
}
