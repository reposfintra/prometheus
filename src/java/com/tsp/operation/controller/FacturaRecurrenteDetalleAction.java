/********************************************************************
 *      Nombre Clase.................   FacturaDetalleAction.JAVA
 *      Descripci�n..................   Action que se encarga de la busqueda de facturas
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   Created on 14 de julio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  Ivan Gomez
 */
public class FacturaRecurrenteDetalleAction extends Action{
    
    /** Creates a new instance of FacturaLArchivoAction */
    public FacturaRecurrenteDetalleAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try {
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin=""+usuario.getLogin();
            String next = "/jsp/cxpagar/reportes/DetalleFacturaRecurrente.jsp";
            String agencia_usuario = "";
            CXP_Doc factura = new CXP_Doc();
            Vector vItems;
            String maxfila = "1";
            String fecha_Aprobacion = "";
            String fecha_Contabilizacion ="";
            String evento = request.getParameter("evento");
            
            String msj = "";
            
            model.ChequeFactXCorridasSvc.loadBancos( usuario.getDstrct(), usuario.getId_agencia() );
            model.ChequeFactXCorridasSvc.loadSucursales(usuario.getDstrct(), usuario.getId_agencia() );
           
            
            model.agenciaService.loadAgencias();
            model.tblgensvc.searchDatosTablaAUTCXP();
            
            
            String documento = request.getParameter("documento").replaceAll("-_-","#");
            String prov      = request.getParameter("prov");
            String tipo_doc  = request.getParameter("tipo_doc")!=null ?request.getParameter("tipo_doc"):"010";
            
            factura = model.factrecurrService.detalleFactura(usuario.getDstrct(),prov,documento,tipo_doc);
            
            Vector vTipoImp = model.TimpuestoSvc.vTiposImpuestos();
            vItems = model.factrecurrService.detalleItemFactura(factura, vTipoImp);
            
            request.setAttribute("Main", factura);
            request.setAttribute("Items", vItems);
            
            
            
            String frecuencia = factura.getFrecuencia();
            String ultima = factura.getFecha_ultima_transfer();
            
            int dias = 0;
            
            ////////////////////////////////////////
            if( frecuencia.equals("Diario") )
                dias = 1;
            else if( frecuencia.equals("Semanal") )
                dias = 7;
            else if( frecuencia.equals("Quincenal") )
                dias = 15;
            else if( frecuencia.equals("Mensual") )
                dias = 30;
            else if( frecuencia.equals("Semestral") )
                dias = 182;
            ////////////////////////////////////////
            
            String proxima  = "";
            String aplazada = "";
            
            try{
                
                proxima = Util.getFechaActual_String( 4 );
                if( factura.getFecha_inicio_transfer().compareTo(proxima) > 0 ){
                    proxima = factura.getFecha_inicio_transfer();
                }else if(!ultima.equals("0099-01-01")){
                    proxima = Util.fechaFinal( (ultima+" 00:00:00"), dias );
                    proxima = proxima.substring(0,10);
                }
                
            }catch ( Exception ex ){
                proxima = " -|- ";
            }
            
            if( evento!=null && evento.equals("plazo") ){
                
                try{
                    int periodos = Integer.parseInt( request.getParameter("periodos") );
                    aplazada = Util.fechaFinal( (proxima+" 00:00:00"), (dias*periodos) );
                    aplazada = aplazada.substring(0,10);
                }catch (Exception ex){
                    aplazada = " -|- ";
                }
                
            }else if( evento!=null && evento.equals("aplazar") ){
                
                
                String nueva = request.getParameter("aplazada");
                proxima = nueva;
                
                nueva = Util.fechaFinal( (nueva+" 00:00:00"), (dias*-1) );
                
                nueva = nueva.length()>10? nueva.substring(0,10):nueva;
                
                CXP_Doc f = new CXP_Doc();
                
                f.setDocumento            ( factura.getDocumento() );
                f.setProveedor            ( factura.getProveedor() );
                f.setTipo_documento       ( factura.getTipo_documento() );
                f.setDstrct               ( factura.getDstrct() );
                f.setUser_update          ( usuario.getLogin() );
                f.setFecha_ultima_transfer( nueva );
                
                model.factrecurrService.aplazarRecurrente( f );                
                
                msj = "?msj=Se aplaz� la fecha de la pr�xima cuota hasta "+proxima;
                next += msj;
            }
            
            request.setAttribute( "aplazada", aplazada );
            request.setAttribute( "ultima", ultima );
            request.setAttribute( "proxima", proxima );
            
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
    
    
    public static void main(String[]sd) throws Exception{
        
        String f1 = "2006-12-07 00:00:00";
        
        String f2 = "2007-03-07 00:00:00";
        
        //System.out.println( f1.compareTo(f2) );
        
        int dias = -10;
       
        ////System.out.println( Util.fechaFinal( hoy, dias ).substring(0,10) );        
        
    }
    
}

