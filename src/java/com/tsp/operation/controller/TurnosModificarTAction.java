/********************************************************************
 *      Nombre Clase.................   TurnosModificarTAction.java
 *      Descripci�n..................   Accion que se encarga de modificar los registros de turnos
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  dlamadrid
 */
public class TurnosModificarTAction extends Action{
    
    /** Creates a new instance of TurnosModificarTAction */
    public TurnosModificarTAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        try {
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin=""+usuario.getLogin();
            
            
            String ms="";
            String usuario_turno=""+ request.getParameter("usuario_turno");
            String fecha_turno=""+request.getParameter("fecha");
            String fecha_h=""+request.getParameter("fecha_h");
            String hora_e=""+request.getParameter("hora_e");
            String hora_s=""+request.getParameter("hora_s");
            String[] zonas = request.getParameterValues("zonas");
            int h_entrada = Integer.parseInt(request.getParameter("h_entrada"));
            int h_salida  = Integer.parseInt(request.getParameter("h_salida"));
            int m_entrada = Integer.parseInt(request.getParameter("m_entrada"));
            int m_salida  = Integer.parseInt(request.getParameter("m_salida"));
            String h_e=request.getParameter("h_e");
            String h_s=request.getParameter("h_s");
            String dia =request.getParameter("dia");
            Turnos temp = new Turnos();
            
            
            temp = model.turnosService.obtener_turno( usuario_turno, fecha_h, hora_e, hora_s );
            
            String zona="";
            Vector vZonas=new Vector();
            for(int i=0;i<zonas.length;i++){
                if(i==0){
                    zona=zonas[i];
                }
                else{
                    zona=zona + "," + zonas[i];
                }
                model.zonaService.buscarZona(zonas[i]);
                Zona zon = model.zonaService.obtenerZona();
                vZonas.add(zon.getNomZona());
            }
            
            
            Turnos turno= new Turnos();
            turno.setUsuario_turno(usuario_turno);
            model.usuarioService.buscaUsuario(usuario_turno);
            Usuario login = model.usuarioService.getUsuario();
            turno.setNombreUsuario(login.getNombre());
            turno.setFecha_turno(fecha_turno);
            turno.setUser_update(userlogin);
            turno.setBase(usuario.getBase());
            turno.setDstrct(usuario.getDstrct());
            turno.setZona(zona);
            turno.setNombresZonas(vZonas);
            
            String HoraEntrada="";
            String HoraSalida="";
            
            HoraEntrada = model.turnosService.generarHora(h_e, h_entrada,m_entrada);
            HoraSalida  = model.turnosService.generarHora(h_s, h_salida,m_salida);
            turno.setH_entrada(HoraEntrada);
            turno.setH_salida(HoraSalida);
            
            
            model.turnosService.eliminar( usuario_turno , fecha_h, hora_e, hora_s );
            //model.turnosService.setTurno (turno);
            
            
            if( model.turnosService.turnoValido(usuario_turno,fecha_turno,HoraEntrada,HoraSalida )==false ){
                              
                
                temp.setFecha_turno( fecha_turno );
                temp.setH_entrada( HoraEntrada );
                temp.setH_salida( HoraSalida );
                temp.setZona( turno.getZona() );
                try{
                model.turnosService.setTurno( temp );
                model.turnosService.update( usuario.getLogin() );
                }catch (Exception e){}
                //model.turnosService.setTurno(turno);
                //model.turnosService.actualizarTurno(fecha_h,hora_e,hora_s);
                ms="Se Modifico el Turno en el Sistema";
                               
            }
            else{                
                if( temp != null ){
                    model.turnosService.setTurno( temp );
                    model.turnosService.update( "" );
                }
                ms="No se pudo modificar el Turno en el Sistema";
                                
            }
            
            model.turnosService.turnosPorId(usuario_turno, fecha_turno, HoraEntrada,HoraSalida);
            
            model.usuarioService.getUsuariosPorDpto("traf");
            model.zonaService.listarZonasxUsuario(userlogin);
            model.turnosService.setTurno(turno);
            ////System.out.println(" pass 9");
            next="/jsp/trafico/turnos/modificar.jsp?marco=no&ms="+ms+"&update=";
        }
        catch (Exception e) {
            throw new ServletException(e.getMessage());
            
        }
        this.dispatchRequest(next);
    }
      
}
