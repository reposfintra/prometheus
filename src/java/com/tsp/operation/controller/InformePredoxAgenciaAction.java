/*
 * InformePredoxAgenciaAction.java
 *
 * Created on 1 de agosto de 2005, 10:40 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class InformePredoxAgenciaAction extends Action{
    
    /** Creates a new instance of InformePredoxAgenciaAction */
    public InformePredoxAgenciaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechai =request.getParameter("fechai");
        String fechaf =request.getParameter("fechaf");
        String msg = "El Proceso ha iniciado";
        String next = "/pags_predo/InfoxAgencia.jsp";
        try{
           model.ipredoSvc.ListarAgencias(fechai, fechaf);
           ////System.out.println("listo!!!");
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
