/*
 * HImportacionesCFAction.java
 *
 * Created on 12 de septiembre de 2006, 09:49 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import com.tsp.util.UtilFinanzas;
/**
/**
 *
 * @author  ALVARO
 */
public class HImportacionesCFAction extends Action{
    
    /** Creates a new instance of HImportacionesCFAction */
    public HImportacionesCFAction() {
    }
    public void run()throws ServletException, InformationException{
        String next="/jsp/general/reporte/migracionCF.jsp?Mensaje=Su proceso ha iniciado exitosamente!";
        try{
               
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
             
        Upload  upload = new Upload(ruta + "/" ,request);
            upload.load();
            List lista = upload.getFilesName();
            List archivo = upload.getFiles();                        

            String filename = lista.get(0).toString();
            File file = (File)archivo.get(0);
            
            int pos = file.getName().lastIndexOf(".");
            String ext = "";
            if (pos!=-1) {
                ext = file.getName().substring(pos+1, file.getName().length());
            }
            if( !ext.equalsIgnoreCase("xls") ) {
                file.delete();
                next="/jsp/general/reporte/migracionCF.jsp?Mensaje=Formato del archivo no es excel!";
                request.setAttribute( "mensaje", "No se puede procesar el archivo debido a que no es un excel." );
            }else{                 
                 HImportacionCF hicf = new HImportacionCF(model,usuario,file);
            }            
       
        this.dispatchRequest(next);    
        }catch (Exception e){
            e.printStackTrace();
        }
        
        
    }
}
