/********************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorXLSAction.java
 *      Descripci�n..................   Genera el reporte de precintos utilizados
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   14.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteFacturasProveedorXLSAction extends Action{
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteFacturasProveedorXLSAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String nit = request.getParameter("nit");     
                
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/cxpagar/reportes/consultaFacturasProveedorRespuesta.jsp?msg=" +
                "El Reporte se ha generado exitosamente en ReporteFacturasProveedor_" +
                FechaFormated1 + ".xls";
        
        try{
                        
            model.cxpDocService.listaDocPorProveedor(nit, "", "");
            Vector vec = model.cxpDocService.getVecCxp_doc();
            vec = (Vector) vec.elementAt(0);
            
            model.identidadService.buscarIdentidad(nit, "");
            Identidad prov = model.identidadService.obtIdentidad();
            
            request.setAttribute("Vec_cxp_Docs", vec);
            request.setAttribute("idprov", nit);
            request.setAttribute("proveedor", prov);
            
            //////System.out.println("�������������������> cxpDocs (xls): " + vec.size());         
            
            
            /**
             * Generamos el reporte
             */
            ReporteFacturasProveedorTh hilo = new ReporteFacturasProveedorTh();
            hilo.start(model, vec, usuario.getLogin(), nit, prov.getNombre());

        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
