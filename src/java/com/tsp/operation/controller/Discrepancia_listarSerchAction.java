/************************************************************************
 * Nombre clase: Discrepancia_listarSerchAction.java
 * Descripci�n: Accion para buscar las discrepancias con o sin fecha de cierre.
 * Autor: Jose de la rosa
 * Fecha: 18 de octubre de 2005, 04:17 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class Discrepancia_listarSerchAction extends Action{
    
    /** Creates a new instance of Discrepancia_listarSerchAction */
    public Discrepancia_listarSerchAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        String distrito = (String) session.getAttribute ("Distrito");
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");String listar = request.getParameter ("listar");
        String fecha_fin = (request.getParameter ("c_fecha_fin")!=null)?request.getParameter ("c_fecha_fin").toUpperCase ():"";
        String fecha_inicio = (request.getParameter ("c_fecha_inicio")!=null)?request.getParameter ("c_fecha_inicio").toUpperCase ():"";
        String pla = (request.getParameter ("numpla")!=null)?request.getParameter ("numpla").toUpperCase ():"";
        if((fecha_inicio.equals (""))&&(fecha_fin.equals (""))){
            //creaci�n de la fecha actual del sistema
            Date hoy = new Date ();
            SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            fecha_fin = s.format (hoy);
            //obtiene los ultimos 30 dias de una fecha dada
            fecha_inicio = Util.fechaFinal (fecha_fin,-30);
            fecha_inicio = fecha_inicio.substring (0,10);
            fecha_fin = fecha_fin.substring (0,10);
        }
        try{
            if(listar.equals ("True")){
                String tipo = request.getParameter ("c_tipo").toUpperCase ();
                if(tipo.equals ("S")){
                    model.discrepanciaService.listDiscrepanciaSinCierre ();
                }
                else if(tipo.equals ("P")){
                    model.discrepanciaService.listDiscrepanciaTodas(pla);
                }
                else{
                    model.discrepanciaService.listDiscrepanciaConCierre (fecha_inicio,fecha_fin);
                }
                next="/jsp/cumplidos/discrepancia/DiscrepanciaListar.jsp";
            }
            else{
                String sw = (String) request.getParameter ("sw");
                if (sw.equals ("false")){
                    String numrem = (String) request.getParameter ("c_numrem");
                    String numpla = (String) request.getParameter ("c_numpla");
                    String tipo_doc = (String) request.getParameter ("c_tipo_doc");
                    String documento = (String) request.getParameter ("c_documento");
                    String num_discre = (String) request.getParameter ("c_num_discre");
                    String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
                    String documento_rel = (String) request.getParameter ("c_documento_rel");
                    model.planillaService.obtenerInformacionPlanilla2 (numpla);
                    model.planillaService.obtenerNombrePropietario2 (numpla);
                    
                    model.remesaService.consultaRemesaDocumento(numrem);
                    model.planillaService.consultaPlanillaRemesa(numpla);
                    
                    model.planillaService.consultaPlanillaRemesaDiscrepancia (numpla, numrem, tipo_doc, documento, Integer.parseInt (num_discre) );
                    model.remesaService.consultaRemesaDiscrepancia(numpla, numrem, tipo_doc, documento, Integer.parseInt (num_discre) );
                    
                    model.discrepanciaService.searchDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito );
                    
                    Discrepancia d = model.discrepanciaService.getDiscrepancia ();
                    model.ubService.buscarUbicacion (d.getUbicacion (), usuario.getCia ());
                    model.discrepanciaService.listDiscrepanciaProducto (Integer.parseInt (num_discre) ,numpla,numrem, tipo_doc,documento,tipo_doc_rel,documento_rel,distrito);
                    next="/jsp/cumplidos/discrepancia/DiscrepanciaConsulta.jsp?c_num_discre="+num_discre+"&c_documento="+documento+"&c_tipo_doc="+tipo_doc+"&c_numrem="+numrem+"&sw=true&campos=false&c_numpla="+numpla+"&exis=false&campos=false&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                }
                else{
                    String numrem = (String) request.getParameter ("c_numrem");
                    String numpla = (String) request.getParameter ("c_numpla");
                    String tipo_doc = (String) request.getParameter ("c_tipo_doc");
                    String documento = (String) request.getParameter ("c_documento");
                    String num_discre = (String) request.getParameter ("c_num_discre");
                    String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
                    String documento_rel = (String) request.getParameter ("c_documento_rel");
                    if ( model.discrepanciaService.existeDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito ) ){
                        model.discrepanciaService.searchDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito );
                        next="/jsp/cumplidos/discrepancia/DiscrepanciaConsulta.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                    }
                    else{
                        next="/jsp/cumplidos/discrepancia/DiscrepanciaConsulta.jsp?c_numrem="+numrem+"&sw=true&exis=false&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                    }
                }
            }
            //next = Util.LLamarVentana (next, "Listar Discrepancias");
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
