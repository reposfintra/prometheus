/*
 * CargarVariostramoAction.java
 *
 * Created on 24 de junio de 2005, 02:26 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jcuesta
 */
public class CargarVariostramoAction extends Action{
    
    /** Creates a new instance of CargarVariostramoAction */
    public CargarVariostramoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=ok";                
        String pagina = request.getParameter("pagina");
       
        try{
            String pais = request.getParameter("c_paiso");
            model.ciudadservice.listarCiudadesxpais(pais);
            request.setAttribute("ciudadO", model.ciudadservice.obtenerCiudades());
            List co = model.ciudadService.obtenerCiudades();            
            String paisd = request.getParameter("c_paisd");
            model.ciudadservice.listarCiudadesxpais(paisd);
            request.setAttribute("ciudadD", model.ciudadservice.obtenerCiudades());
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
