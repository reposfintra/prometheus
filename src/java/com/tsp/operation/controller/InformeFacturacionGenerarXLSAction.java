/********************************************************************************************* 
 *      Nombre Clase.................   InformeFacturacionGenerarXLSAction.java    
 *      Descripci�n..................   Action para la generaci�n del reporte de facturacion en formato xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   04.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ********************************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;


public class InformeFacturacionGenerarXLSAction extends Action {

        public InformeFacturacionGenerarXLSAction() {

        }
    
        public void run() throws javax.servlet.ServletException, InformationException{

                String pag = "/informes/informeFacturacionXls.jsp";
                String next = "";
                
                SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
                Calendar FechaHoy = Calendar.getInstance();
                Date d = FechaHoy.getTime();
                String fecha = s.format(d);

                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                String user = usuario.getLogin();

                String fechai = (String) session.getAttribute("fechaiIFact");
                String fechaf = (String) session.getAttribute("fechafIFact");

                InformeFacturacionTh hilo = new InformeFacturacionTh();
                
                Vector informe = (Vector) session.getAttribute("InformeFact");

                hilo.start(model,informe, usuario.getLogin(), fechai, fechaf);
                pag += "?mensaje=InformeFacturacion_" + fecha + ".xsl";
                // Redireccionar a la p�gina indicada.
                next = com.tsp.util.Util.LLamarVentana(pag, "Informe Anticipos");
                this.dispatchRequest(next);

        }
    
}
