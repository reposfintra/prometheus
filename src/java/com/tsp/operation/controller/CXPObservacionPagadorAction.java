/***********************************************************************************
 * Nombre clase : ............... CXPObservacionPagadorAction.java                            
 * Descripcion :................. Clase que maneja Las Acciones Para la busqueda de las Observaciones
 * Autor :....................... Ing. Ivan Gomez Vanegas                     
 * Fecha :.......................  Created on 10 de octubre de 2005, 11:17 AM               
 * Version :..................... 1.0                                              
 * Copyright :................... Fintravalores S.A.                          
 ***********************************************************************************/



package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  hosorio
 */
public class CXPObservacionPagadorAction extends Action{
    
    /** Creates a new instance of CXPBuscarFacturas */
    public CXPObservacionPagadorAction() {
    }
     public void run() throws javax.servlet.ServletException, InformationException {
        String next="/jsp/cxpagar/facturas/";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String doc = (request.getParameter("doc")!=null)?request.getParameter("doc"):"";
          try{     
           
              if(doc.equals("ObservacionPagador")){
                  String Agencia      = request.getParameter("Agencia").trim();
                  String Fecini       = request.getParameter("fechai");
                  String Fecfin       = request.getParameter("fechaf");
                  model.cxpDocService.BuscarFactObsPagador(usuario.getLogin(),usuario.getDstrct(),Agencia,Fecini,Fecfin);  
                  next+="obsPagador.jsp";
//                  session.setAttribute("Agencia",Agencia); 
//                  session.setAttribute("Fecini", Fecini);
//                  session.setAttribute("Fecfin", Fecfin);
                  
              }else if(doc.equals("AUTORIZACION_FACTURA")){
                  String Agencia      = request.getParameter("Agencia").trim();
                  boolean Autorizadas = (request.getParameter("Tipo").trim().equals("Autorizadas"))?true:false;
                  String Fecini       = request.getParameter("fechai");
                  String Fecfin       = request.getParameter("fechaf");
                  model.cxpDocService.buscarFacturasxUsuario(usuario.getLogin(), usuario.getDstrct(), Agencia, Fecini, Fecfin, Autorizadas);
                  next +="autorizacionFacturas.jsp?Autorizadas="+Autorizadas; 
                  session.setAttribute("agencia",Agencia); 
                  session.setAttribute("Autorizadas", Boolean.toString(Autorizadas));
                  session.setAttribute("Fecini", Fecini);
                  session.setAttribute("Fecfin", Fecfin);
                  
              }else if(doc.equals("FiltroObservacionPagador")){
                  model.agenciaService.loadAgencias();
                  next+="FiltroObsPagador.jsp";
              }
              
              
              
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
