/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class MostrarSolicitudAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public MostrarSolicitudAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next ="/colpapel/autorizarCambioFlete.jsp";
        String planilla =request.getParameter("planilla");
        String clave= "NO SE PUDO GENERAR";
        try{
            if(request.getParameter("gclave")==null){
                model.afleteService.selecionar(planilla);
            }
            else{
                AutorizacionFlete a = model.afleteService.getAflete();
                String placa =a.getPlaca();
                String standard=a.getStandard();
                String cedcond = a.getCedcond();
                
                String viejoFlete = com.tsp.util.Util.customFormat(a.getViejoflete());
                String nuevoFlete = com.tsp.util.Util.customFormat(a.getNuevoflete());
                String fecha = a.getFecha_req();
                String emaildesp = a.getEmailDesp();
                String despachador = a.getDespachador();
                String justificacion = a.getJustificacion();
                String numsol = a.getNumsol();
                String texto1=a.getNuevoflete()+a.getPlaca()+a.getStandard();
                String texto2=a.getNumsol()+a.getAgency_id()+a.getDespachador();
                ////System.out.println("Texto 1: "+texto1);
                ////System.out.println("Texto 2: "+texto2);
                
                
                try{
                    clave= com.tsp.util.Util.getClave(texto1,texto2);
                    ////System.out.println("La clave es :"+clave);
                }catch(UnsupportedEncodingException e){
                    throw new ServletException(e.getMessage());
                }
                
                ////System.out.println("Ahora voy a seguir con el email a enviar");
                
                a = new AutorizacionFlete();
                a.setEmail_autorizador(emaildesp);
                //PARA EL EMAIL
                a.setMyEmail("notificacionFlete@mail.tsp.com");
                a.setSubject("AUMENTO A FLETE AUTORIZADO.");
                a.setBody("SE HA AUTORIZADO EL SIGUIENTE CAMBIO DE FLETE:\n\n\n " +
                "DATOS DEL DESPACHO A REALIZAR\n\n"+
                "PLACA     :    "+placa+"\n"+
                "CONDUCTOR : "+cedcond+"\n\n"+
                "DATOS DEL CLIENTE"+
                "\nSTANDARD   : "+standard+
                "AJUSTES EN EL FLETE"+
                "\nVALOR DEL FLETE DEL STANDARD : "+viejoFlete+"\n"+
                "VALOR DEL FLETE AJUSTADO     : "+nuevoFlete+"\n"+
                "JUSTIFICACION DEL AJUSTE     : "+justificacion+"\n"+
                "FECHA DEL CAMBIO             : "+fecha+"\n\n"+
                "DESPACHADOR                  : "+despachador+"\n\n"+
                "\n\n LA CLAVE DE AUTORIZACION ES: " +clave+
                "\n NUMERO DE LA SOLICITUD: "+numsol);
                a.setMyName("FINTRAVALORES");
                model.afleteService.setAflete(a);
                model.tService.crearStatement();
                model.tService.getSt().addBatch(model.afleteService.insertSendMail());
                model.tService.execute();
                
                model.afleteService.selecionar(planilla);
                next = "/colpapel/autorizarCambioFlete.jsp?clave="+clave;
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
        
    }
    
    
}
