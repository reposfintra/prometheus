/*
 * PlacaInsertAction.java
 *
 * Created on 4 de noviembre de 2004, 02:48 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  AMENDEZ
 */
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

public class PlacaUpdateAction extends Action{
    
    Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public PlacaUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = null;
        String next2 = "";
        String cmd = request.getParameter("cmd");
        Placa placa = new Placa();
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechacrea = format.format(fecha);
        String msg = "";
        String grupoEquipo = request.getParameter("grupo_equipo")!=null?request.getParameter("grupo_equipo"):"";
        
        if(grupoEquipo.equals("T")){
            next = "/placas/trailerUpdate.jsp?msg=";
        } else if(grupoEquipo.equals("C")){
            next = "/placas/placasUpdate.jsp?msg=";
        } else if(grupoEquipo.equals("O")){
            next = "/placas/placaUpdate.jsp?msg=";
        }
        
        next2 = next!=null? next:"";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String pag = (request.getParameter("pag")!=null) ? request.getParameter("pag") : "";
        if(cmd == null)
            next = "/placas/placaUpdate.jsp?msg=";
        else{
            
            //**************************    AMATURANA
            String capac_gal = request.getParameter("capac_gal")!=null ? request.getParameter("capac_gal") : "0";
            String capac_mts = request.getParameter("capac_mts")!=null ? request.getParameter("capac_mts") : "0";
            
            capac_gal = capac_gal.equals("")? "0" : capac_gal;
            capac_mts = capac_mts.equals("")? "0" : capac_mts;
            
            logger.info("capac_gal: " + capac_gal);
            logger.info("capac_mts: " + capac_mts);
            
            placa.setCapac_gal(capac_gal);
            placa.setCapac_mts(capac_mts);
            //*************************
            placa.setReg_status(request.getParameter("reg_status"));
            placa.setPlaca(request.getParameter("placa").toUpperCase());
            placa.setCondicion((request.getParameter("condicion")!=null)?request.getParameter("condicion"):"");
            placa.setTarjetaoper((request.getParameter("tarjetaoper")!=null)?request.getParameter("tarjetaoper"):"");
            placa.setVenctarjetaoper((request.getParameter("venctarjetaoper")!=null && !request.getParameter("venctarjetaoper").equals(""))?request.getParameter("venctarjetaoper"):"0099-01-01");
            placa.setMarca((request.getParameter("marca")!=null)?request.getParameter("marca"):"");
            placa.setClase((request.getParameter("clase")!=null)?request.getParameter("clase"):"");
            placa.setCapacidad((request.getParameter("capacidad")!=null)?request.getParameter("capacidad"):"");
            placa.setCarroceria((request.getParameter("carroceria")!=null)?request.getParameter("carroceria"):"");
            placa.setModelo((request.getParameter("modelo")!=null)?request.getParameter("modelo"):"");
            placa.setColor((request.getParameter("color")!=null)?request.getParameter("color"):"");
            placa.setNomotor((request.getParameter("nomotor")!=null)?request.getParameter("nomotor"):"");
            placa.setNochasis((request.getParameter("nochasis")!=null)?request.getParameter("nochasis"):"");
            placa.setNoejes((request.getParameter("noejes")!=null)?request.getParameter("noejes"):"");
            placa.setAgencia((request.getParameter("agencia")!=null)?request.getParameter("agencia"):"");
            placa.setDimcarroceria((request.getParameter("dimcarroceria")!=null)?request.getParameter("dimcarroceria"):"");
            placa.setVenseguroobliga((request.getParameter("venseguroobliga")!=null && !request.getParameter("venseguroobliga").equals(""))?request.getParameter("venseguroobliga"):"0099-01-01");
            placa.setHomologado((request.getParameter("homologado")!=null)?request.getParameter("homologado"):"");
            placa.setAtitulo((request.getParameter("atitulo")!=null)?request.getParameter("atitulo"):"");
            placa.setEmpresaafil((request.getParameter("empresaafil")!=null)?request.getParameter("empresaafil"):"");
            placa.setPropietario((request.getParameter("propietario")!=null)?request.getParameter("propietario"):"");
            placa.setConductor((request.getParameter("conductor")!=null)?request.getParameter("conductor"):"");
            placa.setTenedor((request.getParameter("tenedor")!=null)?request.getParameter("tenedor"):"");
            placa.setTipo((request.getParameter("tipo")!=null)?request.getParameter("tipo"):"");
            placa.setTara((request.getParameter("tara")!=null)?request.getParameter("tara"):"");
            placa.setLargo((request.getParameter("largo")!=null)?request.getParameter("largo"):"");
            placa.setAlto((request.getParameter("alto")!=null)?request.getParameter("alto"):"");
            placa.setLlantas((request.getParameter("llantas")!=null)?request.getParameter("llantas"):"");
            placa.setAncho((request.getParameter("ancho")!=null)?request.getParameter("ancho"):"");
            placa.setPiso((request.getParameter("piso")!=null)?request.getParameter("piso"):"");
            placa.setCargue((request.getParameter("cargue")!=null)?request.getParameter("cargue"):"");
            placa.setVolumen((request.getParameter("volumen")!=null)?request.getParameter("volumen"):"");
            placa.setRecurso((request.getParameter("recurso")!=null)?request.getParameter("recurso"):"");
            placa.setGrupoid((request.getParameter("grupoid")!=null)?request.getParameter("grupoid"):"");
            placa.setNombre((request.getParameter("nombre")!=null)?request.getParameter("nombre"):"");
            placa.setGrupo((request.getParameter("grupo")!=null)?request.getParameter("grupo"):"");
            placa.setEstadoequipo((request.getParameter("estadoequipo")!=null)?request.getParameter("estadoequipo"):"");
            placa.setLocalizacion((request.getParameter("localizacion")!=null)?request.getParameter("localizacion"):"");;
            placa.setFechaultact(fechacrea);
            //25 - 09 - 2005
            placa.setNumero_rin((request.getParameter("numero_rin")!=null)?request.getParameter("numero_rin"):"");
            placa.setPlaca_trailer((request.getParameter("placa_trailer")!=null)?request.getParameter("placa_trailer"):"");
            placa.setCapacidad_trailer((request.getParameter("capacidad_trailer")!=null)?request.getParameter("capacidad_trailer"):"");
            placa.setTarhabil((request.getParameter("tarhabil")!=null)?request.getParameter("tarhabil"):"");
            placa.setFecvenhabil((request.getParameter("fecvenhabil")!=null && !request.getParameter("fecvenhabil").equals(""))?request.getParameter("fecvenhabil"):"0099-01-01");
            placa.setTarprop((request.getParameter("tarprop")!=null)?request.getParameter("tarprop"):"");
            placa.setFecvenprop((request.getParameter("fecvenprop")!=null && !request.getParameter("fecvenprop").equals(""))?request.getParameter("fecvenprop"):"0099-01-01");
            placa.setPolizasoat((request.getParameter("polizasoat")!=null)?request.getParameter("polizasoat"):"");
            placa.setVenseguroobliga((request.getParameter("venseguroobliga")!=null && !request.getParameter("venseguroobliga").equals(""))?request.getParameter("venseguroobliga"):"0099-01-01");
            placa.setCertgases((request.getParameter("certgases")!=null)?request.getParameter("certgases"):"");
            placa.setFecvengases((request.getParameter("fecvengases")!=null && !request.getParameter("fecvengases").equals(""))?request.getParameter("fecvengases"):"0099-01-01");
            placa.setTarempresa((request.getParameter("tarempresa")!=null)?request.getParameter("tarempresa"):"");
            placa.setFecvenempresa((request.getParameter("fecvenempresa")!=null && !request.getParameter("fecvenempresa").equals(""))?request.getParameter("fecvenempresa"):"0099-01-01");
            placa.setReg_nal_carga((request.getParameter("reg_nal_carga")!=null)?request.getParameter("reg_nal_carga"):"");
            placa.setFecvenreg((request.getParameter("fecvenreg")!=null && !request.getParameter("fecvenreg").equals(""))?request.getParameter("fecvenreg"):"0099-01-01");
            placa.setPoliza_andina((request.getParameter("poliza_andina")!=null)?request.getParameter("poliza_andina"):"");
            placa.setFecvenandina((request.getParameter("fecvenandina")!=null && !request.getParameter("fecvenandina").equals(""))?request.getParameter("fecvenandina"):"0099-01-01");
            placa.setTenedor((request.getParameter("tenedor")!=null)?request.getParameter("tenedor"):"");
            placa.setGrado_riesgo((request.getParameter("grado_riesgo")!=null && !request.getParameter("grado_riesgo").equals(""))?request.getParameter("grado_riesgo"):"0");
            placa.setUsuariocrea(usuario.getLogin());
            //jose 2006-02-15
            placa.setGrupo_equipo( ((request.getParameter("grupo_equipo"))!=null)?(request.getParameter("grupo_equipo")).toUpperCase():"O");
            placa.setCiudad_tarjeta((request.getParameter("ciudad_tarjeta")!=null)?request.getParameter("ciudad_tarjeta"):"");
            ////System.out.println ("ciudad : "+request.getParameter ("c_ciudad"));
            placa.setResp_civil((request.getParameter("resp_civil")!=null)?request.getParameter("resp_civil"):"");
            String venrcivil = request.getParameter("fecvresp_civil");
            placa.setFecvresp_civil(  venrcivil == null || "".equals(venrcivil)?"0099-01-01": venrcivil );
            placa.setDescuento_equipo((request.getParameter("descuento_equipo")!=null)?request.getParameter("descuento_equipo"):"N");
            placa.setClasificacion_equipo((request.getParameter("clasificacion_equipo")!=null)?request.getParameter("clasificacion_equipo"):"");
            
            //Osvaldo
            boolean gps = false;
            Placa anterior = new Placa();
            
            //Enrique De Lavalle
            Placa anteriorParaVeto = new Placa();
            anteriorParaVeto = model.placaService.getPlaca_bean();
            
            
            //luigi.. 01 marzo 2007
            String control = session.getAttribute("USRIDEN")!=null?(String) session.getAttribute("USRIDEN"):"";
            String causa = (request.getParameter("causa")!=null)?request.getParameter("causa").toUpperCase():"";
            String causaant = (request.getParameter("causa_cambio")!=null)?request.getParameter("causa_cambio").toUpperCase():"";
            String ob = (request.getParameter("observacion")!=null)?request.getParameter("observacion").toUpperCase():"";
            String obant = (request.getParameter("observacion_cambio")!=null)?request.getParameter("observacion_cambio").toUpperCase():"";
            String vetoant = (request.getParameter("veto_cambio")!=null)?request.getParameter("veto_cambio").toUpperCase():"";
            String veto = (request.getParameter("veto")!=null)?request.getParameter("veto").toUpperCase():"";
            String fuenteant = (request.getParameter("fuente_cambio")!=null)?request.getParameter("fuente_cambio").toUpperCase():"";
            String fuente = (request.getParameter("fuente")!=null)?request.getParameter("fuente").toUpperCase():"";
            
            placa.setVeto(veto);
            
            if(grupoEquipo.equals("C")){
                gps = reset( request.getParameter("tiene_gps") ).equals("SI");
                anterior = model.placaService.getPlaca_bean();
            }
            
            
            boolean sw = false;
            if (usuario != null)
                placa.setUsuario(usuario.getLogin());
            
            try{
                if( !request.getParameter("grupo_equipo").equals( request.getParameter("grupos"))){
                    model.placaService.buscaPlaca( placa.getPlaca() );
                }
                else{
                    ////System.out.println( "gps = "+gps );
                    if (!pag.equals("ESC")) {
                        if (!model.identidadService.existePropietario(placa.getPropietario())){
                            msg = "EL NIT DEL PROPIETARIO NO EXISTE";
                            sw = true;
                        } else if (!grupoEquipo.equals("T") && !model.conductorService.existeConductorCGA( placa.getConductor())){//AMATURANA
                            msg = "EL NIT DEL CONDUCTOR NO EXISTE";
                            sw = true;
                        } else if(!placa.getTenedor().trim().equals("")){
                            if (!model.placaService.nitExist(placa.getTenedor())){
                                msg = "EL NIT DEL TENEDOR NO EXISTE";
                                sw = true;
                            }
                        }
                        
                        //validacion recurso modificaciones 2007-04-25 Enrique de Lavalle
                        if (!placa.getRecurso().equals("")){ //modificacion Enrique De Lavalle 2007-04-24
                            if (model.placaService.verificarRecurso(placa.getRecurso())){
                                if(!model.placaService.obtenerTipoPlacaRecurso(placa.getRecurso()).equals(placa.getGrupo_equipo())){
                                    msg = "EL RECURSO NO CORRESPONDE AL TIPO DE PLACA";
                                    sw=true;
                                }
                            }else{
                                msg = "EL RECURSO NO EXISTE";
                                sw=true;
                            }
                        }
                    }
                    
                    if(grupoEquipo.equals("C") && !pag.equals("ESC")){
                        if ( gps == true ){
                            placa.setOperador_gps( gps? reset(request.getParameter("operador_gps")) : "" );
                            boolean cambio = reset(request.getParameter("cambio_clave")).equals("S");
                            ////System.out.println("cambio = "+cambio);
                            if( cambio == true ){
                                String actual_passwd = reset(request.getParameter("actual_passwd"));
                                
                                ////System.out.println("clave actual "+anterior.getPasswd_seguimiento());
                                ////System.out.println("clave nueva "+request.getParameter("clave_seg") );
                                
                                if( anterior.getPasswd_seguimiento().equals(actual_passwd) || anterior.getPasswd_seguimiento().equals("") ){
                                    placa.setPasswd_seguimiento( gps? reset(request.getParameter("clave_seg")) : ""  );
                                }else{
                                    msg = "LA CLAVE ACTUAL ES INCORRECTA";
                                    sw = true;
                                }
                            }
                        }
                    }
                    if (sw==false) {
                        //luigi... Aqui modifico el veto si es nesesario.// modificado Enrique De Lavalle 2007-04-30
                        if(control.equals("S")) {
                            
                            if (!(anteriorParaVeto.getVeto().equals("N") && veto.equals("N"))){
                                if ((anteriorParaVeto.getVeto().equals("N") && veto.equals("S"))||(anteriorParaVeto.getVeto().equals("S") && veto.equals("S"))){
                                    if((!causa.equals(causaant)) || (!ob.equals(obant)) || (!fuente.equals(fuenteant))){
                                        model.vetoSvc.ModificarVeto(placa.getPlaca(), usuario.getLogin());
                                        model.vetoSvc.insertarVetoNuevo(usuario.getDstrct(), usuario.getLogin(), placa.getPlaca(), usuario.getBase(), causa, ob, veto, fuente, "P");
                                    }
                                }else{
                                    model.vetoSvc.ModificarVeto(placa.getPlaca(), usuario.getLogin());
                                    model.vetoSvc.insertarVetoNuevo(usuario.getDstrct(), usuario.getLogin(), placa.getPlaca(), usuario.getBase(), "", "", veto, "", "P");
                                }
                                
                            }
                            
                        }
                        //*******************autor Leonardo Parody************************************************************************************
                        model.placaService.archivoTxt("/rmi/Sincronizacion" , placa.getPlaca(), "placa", usuario, "placa");
                        //****************************************************************************************************************************
                        
                        //modificacion 2007/04/20  Enrique De Lavalle
                        String nitP = (String) session.getAttribute("nitP");
                        String nombreP = (String) session.getAttribute("nombreP");
                        if (placa.getPropietario().equals(nitP)){
                            model.placaService.actualizaPlaca(placa);
                        }
                        else{
                            System.out.println("Entro mod P");
                            model.placaService.actualizarPlacaRegPropietarioPlaca( placa, nitP, nombreP);
                        }
                        
                        Proveedor prov = model.proveedorService.obtenerProveedorPorNit(placa.getPropietario());
                        session.setAttribute("nitP", placa.getPropietario());
                        session.setAttribute("nombreP",prov.getC_payment_name());
                        
                        
                        //*******************autor Leonardo Parody************************************************************************************
                        model.placaService.archivoTxt("/rmi/Sincronizacion" , placa.getPlaca(), "placa", usuario, "placa");
                        //****************************************************************************************************************************
                        msg = "Datos modificados exitosamente";
                        //Luigi
                        model.placaService.buscaPlacaTipo(placa.getPlaca(), grupoEquipo);
                        model.vetoSvc.buscarCausaVeto(placa.getPlaca(), "P");
                    }
                }
                if (pag.equals("ESC")) {
                    if (!model.conductorService.existeConductorCGA(placa.getConductor()))
                        msg = "EL NIT DEL CONDUCTOR NO EXISTE";
                    else {
                        model.placaService.actualizaPlaca(placa);
                        msg = "Datos modificados exitosamente";
                    }
                }
                if(grupoEquipo.equals("C")){
                    if( placa != null ){
                        if( anterior!=null && anterior.getPlaca()!=null ){
                            model.placaService.buscaPlacaTipo(anterior.getPlaca(), "C");
                            placa = model.placaService.getPlaca();
                            if( placa!=null )
                                request.setAttribute("tenia_gps", placa.getTiene_gps());
                        }
                    }
                }
                
                if( cmd.equals("cargar_pagina") ){
                    
                    model.tablaGenService.buscarTipo( grupoEquipo );
                    model.tablaGenService.buscarClase( grupoEquipo );
                    
                    if(grupoEquipo.equals("C")){
                        if ( gps == true ){
                            placa.setOperador_gps( gps? reset(request.getParameter("operador_gps")) : "" );
                            boolean cambio = reset(request.getParameter("cambio_clave")).equals("S");
                            ////System.out.println("cambio = "+cambio);
                            if( cambio == true ){
                                String actual_passwd = reset(request.getParameter("actual_passwd"));
                                
                                ////System.out.println("clave actual "+anterior.getPasswd_seguimiento());
                                ////System.out.println("clave nueva "+request.getParameter("clave_seg") );
                                
                                if( anterior.getPasswd_seguimiento().equals(actual_passwd) || anterior.getPasswd_seguimiento().equals("") ){
                                    placa.setPasswd_seguimiento( gps? reset(request.getParameter("clave_seg")) : ""  );
                                }else{
                                    msg = "LA CLAVE ACTUAL ES INCORRECTA";
                                    sw = true;
                                }
                            }
                        }
                        model.placaService.buscarPlaca(anterior.getPlaca());
                        model.vetoSvc.buscarCausaVeto(anterior.getPlaca(), "P");
                    }else{
                        placa = model.placaService.getPlaca();
                        model.placaService.buscarPlaca(placa.getPlaca());
                        model.vetoSvc.buscarCausaVeto(placa.getPlaca(), "P");
                    }
                    
                    if( grupoEquipo.equals("T") ){
                        model.tablaGenService.buscarClasificacion();
                    }
                    next = next2;
                }
                if (pag.equals("ESC")){
                    next = "/jsp/trafico/caravana/escoltaUpdate.jsp?msg=";
                }
                
            }catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
            next+=msg;
        }
        
        this.dispatchRequest(next);
        ////System.out.println("salio");
    }
    
    public String reset(String value){
        return value!=null? value:"";
    }
}
