/*
 * ImporteTextoAction.java
 * Created on 24 de marzo de 2009, 16:47
 */
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import javax.servlet.*;
import com.tsp.operation.model.beans.Usuario;
import org.apache.commons.fileupload.servlet.ServletFileUpload; 
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import java.io.*;
import java.util.LinkedList;
import java.util.*;
import com.tsp.util.Util;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.HImportarTexto;

/** * @author  Fintra */
public class ImporteTextoAction extends Action{
    String next;    
    public ImporteTextoAction() {    }
    
    public void run() throws ServletException, InformationException {
        //System.out.println("cambiarEstado"+request.getParameter("opcion"));
        HttpSession session = request.getSession();
        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = session.getAttribute("Distrito").toString();

        String loginx=usuario.getLogin();            
            
        RequestDispatcher rd ;
        next = "";
        String estado_consultar=request.getParameter("estado_consultar");
            
        
        try{    
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("procesar")){
                if( ! model.importeTextoService.isProcess() ){//20100706
                    model.importeTextoService.setProcess( true );//20100706

                    HImportarTexto hilo =  new HImportarTexto();
                    hilo.start(model, usuario);

                    next="/jsp/fenalco/importe/procesarTextoAval.jsp?aceptarDisable=algo&msj=Proceso iniciado.";
                }else{//20100706
                    next="/jsp/fenalco/importe/procesarTextoAval.jsp?aceptarDisable=algo&msj=Proceso ya estaba iniciado.";//20100706
                }//20100706
            }  
            
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("borrar2")){                
                String carpeta="images/multiservicios";
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties        
        
                String directorioArchivos = rb.getString("ruta")+"/"+carpeta+"/"+loginx;//se establece la ruta de la imagen
        
                File carpetica =new File (directorioArchivos)    ;        
        
                deleteDirectory(carpetica);
                               
                next="/jsp/fenalco/importe/procesarTextoAval.jsp?cerrar=si";
            }
        }catch(Exception ee){
            System.out.println("eee"+ee.toString()+ee.getMessage());
        }
        try{                    
                rd = application.getRequestDispatcher(next);
                
                if(rd == null)
                    throw new ServletException("No se pudo encontrar "+ next);           
            
                rd.forward(request, response);               
        } catch ( Exception e){
            System.out.println("error en run de action..."+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new ServletException("Error en NegociosApplusAction en  run.....\n"+e.getMessage());
       }         
    }
    
    static public boolean deleteDirectory(File path) {
    if( path.exists() ) {
      File[] files = path.listFiles();
      for(int i=0; i<files.length; i++) {
         if(files[i].isDirectory()) {
           deleteDirectory(files[i]);
         }
         else {
           files[i].delete();
         }
      }
    }
    return( path.delete() );
  }
}
