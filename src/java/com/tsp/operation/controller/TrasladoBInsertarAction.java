/***********************************************
 * Nombre clase: PlacaInsertAction.java
 * Descripci�n: Accion para ingresar una placa a la bd.
 * Autor: AMENDEZ
 * Fecha: 4 de noviembre de 2004, 02:48 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.lang.*;
import com.tsp.operation.model.threads.*;
public class TrasladoBInsertarAction extends Action{
    
    //static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public TrasladoBInsertarAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/finanzas/contab/comprobante/traslados/traslado_adicionar.jsp";
       
        String cmd = request.getParameter ("cmd");
        BeanGeneral traslado = new BeanGeneral ();
        Date fecha = new Date ();
        SimpleDateFormat format = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String fechacrea = format.format (fecha);
        String msg = "";        
              HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            traslado.setValor_01 ((request.getParameter ("bancoOri")!=null)?request.getParameter ("bancoOri"):"");
            traslado.setValor_02 ((request.getParameter ("SucBancoOri")!=null)?request.getParameter ("SucBancoOri"):"");
            traslado.setValor_03 ((request.getParameter ("bancoDes")!=null)?request.getParameter ("bancoDes"):"");
            traslado.setValor_04((request.getParameter ("SucBancoDes")!=null)?request.getParameter ("SucBancoDes"):"");
            traslado.setValor_05((request.getParameter ("valor")!=null)?request.getParameter ("valor"):"");
            traslado.setValor_06 ((request.getParameter ("fecha")!=null)?request.getParameter ("fecha"):"");
            try{
                
                 /*//VALIDACION DE INGRESAR TRASLADO DEL CLIENTE*/
                
                      model.trasladoService.agregarTraslado_bank(traslado);
                      next = next + "?msg= fue agregado de forma exitosa";
            }
            
            catch (Exception e){
                e.printStackTrace ();
                throw new ServletException (e.getMessage ());
            }
        
        // Redireccionar a la p�gina indicada.
         this.dispatchRequest (next);
    }
}
