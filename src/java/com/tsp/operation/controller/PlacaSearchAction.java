/*************************************************************************
 * Nombre:        PlacaInsertAction.java
 * Descripci�n:   Clase Action para buscar placa
 * Autor:         Ing. Armando Mendez   
 * Fecha:         4 de noviembre de 2004, 02:48 PM
 * Versi�n        1.0                             
 * Coyright:      Transportes Sanchez Polo S.A.   
 *************************************************************************/
package com.tsp.operation.controller;


import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class PlacaSearchAction extends Action{
    
    
    /** Creates a new instance of PlacaInsertAction */
    public PlacaSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = null;
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String cmd = request.getParameter("cmd");
        String idplaca = request.getParameter("idplaca").toUpperCase();
        String pag = (request.getParameter("pag")!=null)?request.getParameter("pag"):"";
        next = "/placas/placaUpdate.jsp";
        String est = "";
        String agencrea ="";
        String agenmod = "";
        HojaVida hv = new HojaVida();
        if(cmd == null)
            next = "/placas/placaUpdate.jsp";
        else{
            Placa placa = (Placa)request.getAttribute("placa");            
            try{
                model.placaService.buscaPlaca2(idplaca);
                if (model.placaService.getPlaca() != null) {
                    placa = model.placaService.getPlaca();
                    if (pag.equals("hoja")) {
                        placa.setMarca(model.marcaService.getMarcaVehiculo(placa.getMarca()));
                        placa.setColor(model.colorService.getNombreColor(placa.getColor()));
                        placa.setCarroceria(model.carrService.getNombreCarroceria(placa.getCarroceria()));
                        placa.setAgencia(model.ciudadService.buscarNomCiudadxCodigo(placa.getAgencia()));
                    }                  
                    request.setAttribute("placa", placa);
                    hv.setPlaca(placa);
                    agencrea = model.usuarioService.obtenerAgenciaUsuario( placa.getUsuariocrea());
                    hv.setAgenUsuarioCrea( agencrea );
                    agenmod = model.usuarioService.obtenerAgenciaUsuario( placa.getUsuario() );
                    hv.setAgenUsuarioMod( agenmod );
                    
                    hv.setLeyenda1("");
                    hv.setLeyenda2("");
                    //lineas para cargar datos para la vista hoja de vida
                   // model.propService.buscarxCedulaNit(placa.getPropietario());
                    model.identidadService.buscarPropietarioxCedula(placa.getPropietario());
                    /***************************************************************************
                     ******************************** HENRY *************************************/
                    //Propietario propietario = model.propService.getPropietario();
                    Proveedor prop = model.identidadService.getPropietario();
                    request.setAttribute("propietario", prop);
                    
                    hv.setPropietario(prop);    //Seteando atributo de HV
                    
                    model.conductorService.buscar_ConductorCGA(placa.getConductor());
                    model.identidadService.buscarIdentidad(placa.getConductor(),"");
                    
                    Identidad iden = model.identidadService.obtIdentidad();
                    Ciudad ciu1 = model.ciudadService.obtenerCiudad(iden.getExpced());
                    Ciudad ciu2 = model.ciudadService.obtenerCiudad(iden.getLugarnac());
                    Ciudad ciu3 = model.ciudadService.obtenerCiudad(iden.getCodciu());
                    model.paisservice.buscarpais(iden.getCodpais());
                    Pais p = model.paisservice.obtenerpais();
                    
                    iden.setExpced(( ciu1 != null )?ciu1.getNombre():"NO REGISTRA");
                    iden.setLugarnac(( ciu2 != null)?ciu2.getNombre():"NO REGISTRA");
                    iden.setCodciu(( ciu3 != null)?ciu3.getNombre():"NO REGISTRA");
                    iden.setCodpais( (p!=null)? p.getCountry_name() : "NO REGISTRA");
                    
                    Conductor con = model.conductorService.obtConductor();
                    hv.setConductor(con); //seteando atributo de HV
                    hv.setIdentidad(iden); //seteando atributo de HV
                    
                    model.identidadService.buscarTenedorxCedula(placa.getTenedor(),placa.getAtitulo());
                    Tenedor tene = model.identidadService.getTenedor();
                    request.setAttribute("tenedor",tene);
                    hv.setTenedor(tene); //seteando atributo HV
                    
                    
                    //Cargando Imagenes Vehiculo
                    model.ImagenSvc.searchImagen("1","1","1",null,null,null,"005", "031",
                    placa.getPlaca(),null,null,null,null,usuario.getLogin());
                    List lista = model.ImagenSvc.getImagenes();
                    Imagen img = null;
                    String placaImg ="";
                    if (lista==null){
                        placaImg = "";
                    } else {
                        img = (Imagen) lista.get(0);
                        placaImg = usuario.getLogin()+"/"+img.getNameImagen();
                    }
                    hv.setFotoVehiculo(placaImg); //setando atributo HV
                    //Imagen Conductor
                    model.ImagenSvc.searchImagen("1","1","1",null,null,null,"003", "032",
                    placa.getConductor(),null,null,null,null,usuario.getLogin());
                    lista = null;
                    lista = model.ImagenSvc.getImagenes();
                    String condImg = "";
                    if(lista!=null){
                        img = (Imagen) lista.get(0);
                    }
                    if(img!=null)
                        condImg = usuario.getLogin()+"/"+img.getNameImagen();
                    if (lista==null){
                        condImg = "";
                    }
                    hv.setFotoConductor(condImg); // setando atributo hv
                    
                    //07-03-2006 Diogenes
                    String huella_der ="none.jpg";
                    String huella_izq ="none.jpg";
                    String firma ="none.jpg";
                    Conductor cond = model.conductorService.obtConductor();
                    if (cond != null){
                        cond.setDesHuella1("");
                        cond.setDesHuella2("");
                        
                        //Busca la imagen de la Huella 1
                        model.ImagenSvc.searchImagen("1","1","1",null,null,null,"015", cond.getHuella_der(),
                        cond.getCedula(),null,null,null,null,usuario.getLogin());
                        List listahd = model.ImagenSvc.getImagenes();
                        Imagen imghd = null;
                        
                        if (listahd!=null){
                            imghd = (Imagen) listahd.get(0);
                            huella_der = usuario.getLogin()+"/"+imghd.getNameImagen();
                        }
                        hv.setHuella_der(huella_der); //setando atributo HV
                        //buscar la decripcion de la Huella 1
                        model.documentoSvc.obtenerDocumento(distrito, cond.getHuella_der());
                        Documento doc = model.documentoSvc.getDocumento();
                        if ( doc != null ){
                            cond.setDesHuella1(doc.getC_document_name());
                            hv.setLeyenda1(doc.getC_document_name());//leyenda para la impresion
                        }
                        
                        //Busca la Huella 2
                        model.ImagenSvc.searchImagen("1","1","1",null,null,null,"016", cond.getHuella_izq(),
                        cond.getCedula(),null,null,null,null,usuario.getLogin());
                        List listahi = model.ImagenSvc.getImagenes();
                        Imagen imghi = null;
                        
                        if (listahi!=null){
                            imghi = (Imagen) listahi.get(0);
                            huella_izq = usuario.getLogin()+"/"+imghi.getNameImagen();
                        }
                        hv.setHuella_izq(huella_izq); //setando atributo HV
                        //buscar ls decripcion de la Huella 2
                        model.documentoSvc.obtenerDocumento(distrito, cond.getHuella_izq());
                        Documento doc1 = model.documentoSvc.getDocumento();
                        if (doc1 !=null){
                            cond.setDesHuella2(doc1.getC_document_name());
                            hv.setLeyenda2(doc1.getC_document_name());//leyenda para la impresion
                        }
                        //Busca la Imagen de la firma
                        model.ImagenSvc.searchImagen("1","1","1",null,null,null,"003", "039",
                        cond.getCedula(),null,null,null,null,usuario.getLogin());
                        List listafirma = model.ImagenSvc.getImagenes();
                        Imagen imgfirma = null;
                        
                        if (listafirma!=null){
                            imgfirma = (Imagen) listafirma.get(0);
                            firma = usuario.getLogin()+"/"+imgfirma.getNameImagen();
                        }
                        hv.setFirma(firma); //setando atributo HV
                    }
                    model.hojaVidaService.setHojaVida(hv);
                    /********************************  CIERRE MOD HENRY H.V  *****************************
                     **************************************************************************************/
                    
                    if(pag.equals("hoja"))
                        next = "/jsp/hvida/hojavida.jsp?marco=no&msg=OK&placa="+placaImg+"&cond="+condImg+"&huella="+huella_der+"&huella2="+huella_izq+"&firma="+firma+"&agencrea="+agencrea+"&agenmod="+agenmod;
                    if(pag.equals("ESC"))
                        next = "/jsp/trafico/caravana/escoltaUpdate.jsp";
                }else {
                    if(pag.equals("hoja")){
                        String msg = "No se encontro ningun resultado";
                        next = "/jsp/hvida/hojavida.jsp?marco=no&msg="+msg;
                    }
                    if(pag.equals("ESC")) {
                        String msg = "No se encontro ningun resultado";
                        next = "/jsp/trafico/caravana/escoltaUpdate.jsp";
                    }
                }
            }catch (Exception e){
                e.printStackTrace();                
                throw new ServletException(e.getMessage());
            }
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}
