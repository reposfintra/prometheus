/*
 * Nombre        InsertarCiudadAction.java
 * Autor         Ing. Diogenes Bastidas
 * Fecha         1 de abril de 2005, 02:58 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class InsertarCiudadAction extends Action {
    
    /** Creates a new instance of InsertarCiudadAction */
    public InsertarCiudadAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String pais = request.getParameter("c_combopais");
        String estado = request.getParameter("c_comboestado");
        String codigo = (request.getParameter("c_codigo").toUpperCase());
        String nombre = (request.getParameter("c_nombre").toUpperCase());
        String frontera = request.getParameter("c_frontera");
        String aplica   = request.getParameter("c_aplica");
        String codica = request.getParameter("c_codica").toUpperCase();
        String zona  = request.getParameter("c_combozona");
        String agasoc = request.getParameter("agencia");
        String tipoc = request.getParameter("c_tipoc");
        
        int indicativo = Integer.parseInt(request.getParameter("c_ind"));
        
        String next = "/jsp/trafico/ciudad/ciudad.jsp?mensaje=Agregado";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");  
        int sw=0;
        try{ 
            
            Ciudad ciudad = new Ciudad();
            ciudad.setcodciu(codigo);
            ciudad.setnomciu(nombre);
            ciudad.setpais_code(pais);
            ciudad.setdepartament_code(estado);
            ciudad.setCreation_user(usuario.getLogin());
            ciudad.setUser_update(usuario.getLogin());
            
            if (codica==null){codica = "";}
            
            ciudad.setCodIca(codica);
            ciudad.setAplIca(aplica);
            ciudad.setFrontera(frontera);
            ciudad.setAgAsoc(agasoc);
            ciudad.setTipociu(tipoc);
            ciudad.setZona(zona);
            ciudad.setBase(usuario.getBase());
            //nuevo
            ciudad.setIndicativo(indicativo);
            ciudad.setTiene_rep_urbano(request.getParameter("treporte"));
            ciudad.setZona_urb(request.getParameter("urbana"));
            ciudad.setFrontera_asoc(request.getParameter("fron_asoc"));
            
           
            try{
                model.ciudadservice.insertarCiudad(ciudad);
            }catch (SQLException e){
                //System.out.println("Error " + e.getMessage()+ " Codigo "+ e.getErrorCode() );
                model.TimpuestoSvc.buscarRicaAgencia( usuario.getDstrct(), codigo, Util.getFechaActual_String(1)+"-12-31" );
                sw=1;
            }
            if (sw==1){
                if ( model.ciudadservice.existeCiudadAnulado(pais,estado,codigo) ){
                    model.ciudadservice.activarCiudad(ciudad);
                }
                else{
                    model.estadoservice.buscarestado(pais,estado);
                    model.TimpuestoSvc.buscarRicaAgencia( usuario.getDstrct(), codigo, Util.getFechaActual_String(1)+"-12-31" );
                    next = "/jsp/trafico/ciudad/ciudad.jsp?mensaje=Error al Ingresar Ciudad&sw=0";                 
                }
            }
            model.estadoservice.listarEstado(pais);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
