/********************************************************************
 *      Nombre Clase.................   InformacionPlanillaAction.java
 *      Descripci�n..................   Valida el n�mero de la planilla para el ingreso de dmeoras
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   06.10.2005
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andr�s
 */
//logger
import org.apache.log4j.Logger;

public class InformacionPlanillaAction extends Action{
    
    Logger log = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of InformacionPlanillaAction */
    public InformacionPlanillaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String numpla = request.getParameter("numpla");
        String plamanual = request.getParameter("plamanual")!=null ? request.getParameter("plamanual") : "";
        
        numpla = numpla.toUpperCase();
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        String next = "/jsp/trafico/demora/asignarDemoraPlanillaParam.jsp";
        
        String fechahoy = com.tsp.util.Util.fechaActualTIMESTAMP().substring(0,17) + "00";
        
        try{
            //if( model.demorasSvc.existePlanilla(numpla) ){
            
            model.rmtService.BuscarReporteMovTraf(numpla, fechahoy);
            RepMovTrafico rmt = model.rmtService.getReporteMovTraf();
            log.info("EXISTE RMT: " + ( rmt!=null ));
            
            
            if( rmt!=null ){
                next = "/jsp/trafico/demora/asignarDemoraPlanilla.jsp?msg=" +
                "Ya existe un reporte de retraso en esa fecha.&marco=no";
            } else if( model.demorasSvc.planillaEnIngresoTrafico(numpla) ){
                model.demorasSvc.setEjecutoProceso(false);
                
                next = next + "?numpla=" + numpla + "&marco=no";
                model.tblgensvc.searchDatosTablaCausasDemora();
                model.traficoService.obtenerIngreso_Trafico(numpla.toUpperCase());
                model.codigo_demoraService.searchCDemoras();
                
                /* Cargar las planillas con la misma secuencia */
                boolean esdm = model.rmtService.esDespachoManual(numpla);
                if(esdm){
                    model.rmtService.BuscarPlanillaDM(numpla);
                }
                else{
                    model.rmtService.BuscarPlanilla(numpla);
                }
                DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
                
                if(dp!=null){
                    String origen = dp.getRutapla().substring(0,2);
                    String destino = dp.getRutapla().substring(2,4);
                    String secuencia = dp.getRutapla().substring(4,dp.getRutapla().length());
                    if(model.rmtService.existeVia(origen, destino, secuencia)){
                        model.rmtService.buscarClientes(numpla);
                        model.stdjobdetselService.buscaStandard(dp.getStdjob());
                        String front = "";
                        if(model.stdjobdetselService.getStandardDetSel()!=null){
                            Stdjobdetsel s = model.stdjobdetselService.getStandardDetSel();
                            String wo = s.getWoType();
                            if(wo.equals("RM")){
                                front = "PA" ;
                            }
                            else if(wo.equals("RC")){
                                front = "CU" ;
                            }
                            else if(wo.equals("RE")){
                                front = "IP";
                            }
                            else if(wo.equals("DC")){
                                front = "CU";
                            }
                            else if(wo.equals("DM")){
                                front = "PA";
                            }
                            else if(wo.equals("DE")){
                                front = "IP";
                            }
                        }
                        dp.setFrontera(front);
                        model.rmtService.BuscarPlanillasCaravana();//kreales
                        model.tblgensvc.buscarLista("REP", "REP_TRA");
                        model.rmtService.BuscarTiposubicacion();
                        model.rmtService.BuscarUbicaciones("PC");
                        model.rmtService.SearchLastCreatedReport();
                        if(model.rmtService.getUltRMT()!=null) {
                            model.rmtService.getNextPC();
                        }
                        model.rmtService.buscarPlanillas(dp.getNumpla(), dp.getPto_control_proxreporte());
                    }
                }    
                /*model.rmtService.BuscarPlanillasCaravana();//kreales
                 
                model.rmtService.buscarClientes(numpla);
                model.stdjobdetselService.buscaStandard(dp.getStdjob());
                String front = "";
                if(model.stdjobdetselService.getStandardDetSel()!=null){
                    Stdjobdetsel s = model.stdjobdetselService.getStandardDetSel();
                    String wo = s.getWoType();
                    if(wo.equals("RM")){
                        front = "PA" ;
                    }
                    else if(wo.equals("RC")){
                        front = "CU" ;
                    }
                    else if(wo.equals("RC")){
                        front = "IP";
                    }
                }
                dp.setFrontera(front);
                model.rmtService.BuscarPlanillasCaravana();//kreales
                model.tblgensvc.buscarLista("REP", "REP_TRA");
                model.rmtService.BuscarTiposubicacion();
                model.rmtService.BuscarUbicaciones("PC");
                model.rmtService.SearchLastCreatedReport();
                if(model.rmtService.getUltRMT()!=null) {
                    model.rmtService.getNextPC();
                }
                model.rmtService.buscarPlanillas(dp.getNumpla(), dp.getPto_control_proxreporte());*/
                
                model.demorasSvc.setPlanillasReporte(new Vector());
                model.demorasSvc.setPlanillasSelec(new Hashtable());
                
                //Vector caravana = model.rmtService.getPlanillas();
                //////System.out.println("................. **CARAVANA NULA? " + (caravana==null));
            } else {
                next = "/jsp/trafico/demora/asignarDemoraPlanilla.jsp?msg=" +
                "N�mero de planilla no est� registrada en Ingreso a Tr�fico.&marco=no";
            }
            
            /*} else {
                next = "/jsp/trafico/demora/asignarDemoraPlanilla.jsp?msg=" +
                        "N�mero de planilla no existe.&marco=no";
            }*/
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
