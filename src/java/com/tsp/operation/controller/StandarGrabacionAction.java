


/********************************************************************
 *      Nombre Clase.................   BuscarFacturaAction.JAVA
 *      Descripci�n..................   Action que se encarga de la busqueda de facturas
 *      Autor........................   Ivan Gomez Vanegas
 *      Fecha........................   Created on 15 de mayo de 2006, 03:48 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  Ivan Gomez
 */
public class StandarGrabacionAction extends Action{
    
    /** Creates a new instance of FacturaLArchivoAction */
    public StandarGrabacionAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try {
            
            HttpSession session = request.getSession();
            //ivan 21 julio 2006
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            //////////////////////
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin =  usuario.getLogin();
            String next = "";
            String opcion = request.getParameter("opcion");
            
            
            
            if(opcion.equals("BUSCAR_CLIENTE")){
                String tipo = request.getParameter("tipo");
                next="/jsp/standar/puente.jsp?opcion="+tipo;
                String cod_cli = (request.getParameter("cod_cli")!=null)?request.getParameter("cod_cli"):"";
                String cod ="";
                for(int i=0; i<6-cod_cli.length();i++ ){
                    cod +="0";
                }
                cod += cod_cli;
                
                model.StandarSvc.buscarCliente(cod);
                
                
            }else if(opcion.equals("BUSCAR_CARGA")){
                next="/jsp/standar/puente.jsp?opcion=CARGA";
                String cod_carga = (request.getParameter("cod_carga")!=null)?request.getParameter("cod_carga"):"";
                model.StandarSvc.buscarCarga(cod_carga);
                
            }else if(opcion.equals("BUSCAR_CIUDAD_LUPA")){
                
                String nomciu = (request.getParameter("nomciu")!=null)?request.getParameter("nomciu"):"";
                String op = (request.getParameter("op")!=null)?request.getParameter("op"):"";
                String id = (request.getParameter("id")!=null)?request.getParameter("id"):"";
                model.StandarSvc.buscarCiudad(nomciu);
                if(!op.equals("ITEM")){
                    next="/jsp/standar/buscarCiudad.jsp?accion=1&marco=no&op="+op;
                }else{
                    next="/jsp/standar/buscarCiudad.jsp?accion=1&marco=no&op="+op+"&id="+id;
                }
                
            }else if(opcion.equals("BUSCAR_CIUDAD")){
                String id = (request.getParameter("id")!=null)?request.getParameter("id"):"";
                String codciu = (request.getParameter("codciu")!=null)?request.getParameter("codciu"):"";
                String op = (request.getParameter("op")!=null)?request.getParameter("op"):"";
                model.StandarSvc.buscarCiudadPorCodigo(codciu);
                if(!op.equals("")){
                    next="/jsp/standar/puente.jsp?opcion=CIUDAD";
                }else{
                    next="/jsp/standar/puenteItem.jsp?opcion=CIUDAD&id="+id;
                }
                
            }else if(opcion.equals("BUSCAR_RECURSO")){
                String id = (request.getParameter("id")!=null)?request.getParameter("id"):"";
                String codrecurso = (request.getParameter("codrecurso")!=null)?request.getParameter("codrecurso"):"";
                String size = (request.getParameter("size")!=null)?request.getParameter("size"):"0";
                model.StandarSvc.buscarRecurso(codrecurso);
                next="/jsp/standar/puenteItem.jsp?opcion=RECURSO&id="+id+"&size"+size;
                
            }else if(opcion.equals("BUSCAR_RECURSO_LUPA")){
                String id = (request.getParameter("id")!=null)?request.getParameter("id"):"";
                String codrecurso = (request.getParameter("codrecurso")!=null)?request.getParameter("codrecurso"):"";
                String size = (request.getParameter("size")!=null)?request.getParameter("size"):"0";
                model.StandarSvc.buscarRecursoPorDescripcion(codrecurso);
                next="/jsp/standar/buscarRecurso.jsp?accion=1&marco=no&id="+id+"&size="+size;
                
            }else if(opcion.equals("BUSCAR_CLIENTE_LUPA")){
                String op = (request.getParameter("op")!=null)?request.getParameter("op"):"";
                String nomcli = (request.getParameter("nomcli")!=null)?request.getParameter("nomcli"):"";
                model.StandarSvc.buscarClientePorNombre(nomcli);
                next="/jsp/standar/buscarCliente.jsp?accion=1&marco=no&op="+op;
                
            }else if(opcion.equals("BUSCAR_CARGA_LUPA")){
                String descripcion = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):"";
                model.StandarSvc.buscarCargaPorDescripcion(descripcion);
                next="/jsp/standar/buscarCarga.jsp?accion=1&marco=no";
                
            }else if(opcion.equals("CARGAR")){
                next="/jsp/standar/GrabacionStandar.jsp";
                String op = (request.getParameter("op")!=null)?request.getParameter("op"):"";
                
                String cod_cli        = (request.getParameter("cod_cli")!=null)?request.getParameter("cod_cli"):"";
                String nom_cli        = (request.getParameter("nom_cli")!=null)?request.getParameter("nom_cli"):"";
                String ag_duenia      = (request.getParameter("ag_duenia")!=null)?request.getParameter("ag_duenia"):"";
                String pagador        = (request.getParameter("pagador")!=null)?request.getParameter("pagador"):"";
                String unidadNegocio  = (request.getParameter("unidadNegocio")!=null)?request.getParameter("unidadNegocio"):"";
                
                boolean planeable         = (request.getParameter("planeable")!=null)?true:false;
                boolean ind_vacio         = (request.getParameter("ind_vacio")!=null)?true:false;
                boolean remesa_facturable = (request.getParameter("remesa_facturable")!=null)?true:false;
                boolean remesapadre       = (request.getParameter("remesapadre")!=null)?true:false;
                boolean bloqdespacho      = (request.getParameter("bloqdespacho")!=null)?true:false;
                
                
                String codigo_sj       = (request.getParameter("codigo_sj")!=null)?request.getParameter("codigo_sj"):"";
                String descripcion     = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):"";
                String tipo_standar    = (request.getParameter("tipo_standar")!=null)?request.getParameter("tipo_standar"):"";
                String standar_general = (request.getParameter("standar_general")!=null)?request.getParameter("standar_general"):"";
                String ag_responsable  = (request.getParameter("ag_responsable")!=null)?request.getParameter("ag_responsable"):"";
                
                String origen     = (request.getParameter("origen")!=null)?request.getParameter("origen"):"";
                String nomorigen  = (request.getParameter("nomorigen")!=null)?request.getParameter("nomorigen"):"";
                String destino    = (request.getParameter("destino")!=null)?request.getParameter("destino"):"";
                String nomdestino = (request.getParameter("nomdestino")!=null)?request.getParameter("nomdestino"):"";
                String tipo_ruta  = (request.getParameter("tipo_ruta")!=null)?request.getParameter("tipo_ruta"):"";
                
                String dia_inicial      = (request.getParameter("dia_inicial")!=null)?request.getParameter("dia_inicial"):"0";
                String dia_final        = (request.getParameter("dia_final")!=null)?request.getParameter("dia_final"):"0";
                String tipo_facturacion = (request.getParameter("tipo_facturacion")!=null)?request.getParameter("tipo_facturacion"):"";
                String cod_carga        = (request.getParameter("cod_carga")!=null)?request.getParameter("cod_carga"):"";
                String vlr_tope_carga   = (request.getParameter("vlr_tope_carga")!=null)?request.getParameter("vlr_tope_carga"):"";
                String mail_autorizador = (request.getParameter("mail_autorizador")!=null)?request.getParameter("mail_autorizador"):"";
                String desc_carga       = (request.getParameter("desc_carga")!=null)?request.getParameter("desc_carga"):"";
                String desc_carga_corta = (request.getParameter("desc_carga_corta")!=null)?request.getParameter("desc_carga_corta"):"";
                String frontera_asoc    = (request.getParameter("frontera_asociada")!=null)?request.getParameter("frontera_asociada"):"";
                dia_inicial = (dia_inicial.equals(""))?"0":dia_inicial;
                dia_final   = (dia_final.equals(""))?"0":dia_final;
                
                Standar standar = new Standar();
                
                standar.setOrigen(origen);
                standar.setCod_cli(cod_cli);
                standar.setNomcli(nom_cli);
                standar.setAg_duenia(ag_duenia);
                standar.setPagador(pagador);
                standar.setUnidad_negocio(unidadNegocio);
                standar.setCodigo_sj(codigo_sj);
                standar.setDescripcion_sj(descripcion);
                standar.setTipo(tipo_standar);
                standar.setStandar_general(standar_general);
                standar.setBloq_despacho(bloqdespacho);
                standar.setE_mail(mail_autorizador);
                standar.setAg_responsable(ag_responsable);
                standar.setAprobado(false);
                standar.setPlaneable(planeable);
                standar.setNom_origen(nomorigen);
                standar.setDestino(destino);
                standar.setNom_destino(nomdestino);
                standar.setInd_vacio(ind_vacio);
                standar.setReq_remesa_padre(remesapadre);
                standar.setRemesaFacturable(remesa_facturable);
                standar.setTipo_ruta(tipo_ruta);
                standar.setCodigo_carga(cod_carga);
                standar.setTipo_facturacion(tipo_facturacion);
                standar.setVlr_tope_carga(Double.parseDouble(vlr_tope_carga.replaceAll(",","")));
                standar.setPeriodoFac_final(Integer.parseInt(dia_final));
                standar.setPeriodoFac_inicial(Integer.parseInt(dia_inicial));
                standar.setDesc_carga(desc_carga);
                standar.setDesc_carga_corta(desc_carga_corta);
                
                String dstrct_code    = usuario.getDstrct();
                
                String account_code_c ="";
                String account_code_i ="";
                int    seq_tarifa     =0;
                String creation_user  = userlogin;
                String base           = usuario.getBase();
                
                standar.setDstrct_code(dstrct_code);
                standar.setFrontera_asoc(frontera_asoc);
                standar.setAccount_code_c(account_code_c);
                standar.setAccount_code_i(account_code_i);
                standar.setSeq_tarifa(seq_tarifa);
                standar.setCreation_user(creation_user);
                standar.setBase(base);
                
                
                // Este bloque es para llenar los datos de las tarifas
                List listaTarifa = new LinkedList();
                for(int i =0; i< model.StandarSvc.getSj().getListaTarifa().size();i++ ){
                    String vlr_tarifa    = (request.getParameter("vlr_tarifa"+i)!=null)?request.getParameter("vlr_tarifa"+i):"0";
                    String moneda_tarifa = (request.getParameter("moneda_tarifa"+i)!=null)?request.getParameter("moneda_tarifa"+i):"";
                    String unidad_tarifa = (request.getParameter("unidad_tarifa"+i)!=null)?request.getParameter("unidad_tarifa"+i):"";
                    boolean vigente      = (request.getParameter("vigente"+i)!=null)?true:false;
                    
                    Standar tarifa = new Standar();
                    tarifa.setVlr_tarifa(Double.parseDouble(vlr_tarifa.replaceAll(",","")));
                    tarifa.setMoneda_tarifa(moneda_tarifa);
                    tarifa.setUnidad_tarifa(unidad_tarifa);
                    tarifa.setVigente_tarifa(vigente);
                    
                    listaTarifa.add(tarifa);
                }
                standar.setListaTarifa(listaTarifa);
                ////////////////////////////////////////////  Fin del bloque de tarifas
                
                // Bloque para llenar los datos de los tramos
                List listaTramos = new LinkedList();
                for(int t =0; t< model.StandarSvc.getSj().getListaTramos().size();t++ ){
                    String origen_tramo        = (request.getParameter("origen"+t)!=null)?request.getParameter("origen"+t):"";
                    String nomorigen_tramo     = (request.getParameter("nomorigen"+t)!=null)?request.getParameter("nomorigen"+t):"";
                    String destino_tramo       = (request.getParameter("destino"+t)!=null)?request.getParameter("destino"+t):"";
                    String nomdestino_tramo    = (request.getParameter("nomdestino"+t)!=null)?request.getParameter("nomdestino"+t):"";
                    String porcentaje_anticipo = (request.getParameter("porcentaje_anticipo"+t)!=null)?request.getParameter("porcentaje_anticipo"+t):"";
                    String peso_lleno_max      = (request.getParameter("peso_lleno_max"+t)!=null)?request.getParameter("peso_lleno_max"+t):"";
                    String numero_escolta      = (request.getParameter("numero_escolta"+t)!=null)?request.getParameter("numero_escolta"+t):"0";
                    numero_escolta = (numero_escolta.equals(""))?"0":numero_escolta;
                    
                    String tipo_trafico        = (request.getParameter("tipo_trafico"+t)!=null)?request.getParameter("tipo_trafico"+t):"";
                    
                    boolean viaje_vacio       = (request.getParameter("viaje_vacio"+t)!=null)?true:false;
                    boolean control_cargue    = (request.getParameter("control_cargue"+t)!=null)?true:false;
                    boolean control_descargue = (request.getParameter("control_descargue"+t)!=null)?true:false;
                    boolean caravana          = (request.getParameter("caravana"+t)!=null)?true:false;
                    
                    boolean require_planviaje    = (request.getParameter("require_planviaje"+t)!=null)?true:false;
                    boolean preferencia          = (request.getParameter("preferencia"+t)!=null)?true:false;
                    boolean requiere_hojareporte = (request.getParameter("requiere_hojareporte"+t)!=null)?true:false;
                    boolean contingente          = (request.getParameter("contingente"+t)!=null)?true:false;
                    
                    
                    Standar tramos = new Standar();
                    
                    tramos.setCod_origen_tramo(origen_tramo);
                    tramos.setCodigo_destino_tramo(destino_tramo);
                    tramos.setNombre_origen_tramo(nomorigen_tramo);
                    tramos.setNombre_destino_tramo(nomdestino_tramo);
                    tramos.setPorcentaje_tramo(porcentaje_anticipo);
                    tramos.setPeso_lleno_tramo(peso_lleno_max);
                    numero_escolta = (numero_escolta.equals(""))?"0":numero_escolta;
                    tramos.setNumero_escoltas(Integer.parseInt(numero_escolta));
                    tramos.setTipo_trafico(tipo_trafico);
                    tramos.setInd_vacio_tramo(viaje_vacio);
                    tramos.setControl_cargue(control_cargue);
                    tramos.setControl_descargue(control_descargue);
                    tramos.setCaravana(caravana);
                    tramos.setRequiere_planviaje(require_planviaje);
                    tramos.setPreferencia(preferencia);
                    tramos.setRequiere_hojareporte(requiere_hojareporte);
                    tramos.setContingente(contingente);
                    
                    //bloque para llenar los datos de los recursos
                    List listaRecursos = new LinkedList();
                    int size_recurso = ((Standar) model.StandarSvc.getSj().getListaTramos().get(t)).getListaRecursos().size(); //Saco el size de la lista de recursos para poder recorrer los que vienen de la jsp
                    for(int r=0;r< size_recurso ;r++){
                        String cod_recurso   = (request.getParameter("cod_recurso"+t+""+r)!=null)?request.getParameter("cod_recurso"+t+""+r):"";
                        String tipo_recurso  = (request.getParameter("tipo_recurso"+t+""+r)!=null)?request.getParameter("tipo_recurso"+t+""+r):"";
                        String desc_recurso  = (request.getParameter("desc_recurso"+t+""+r)!=null)?request.getParameter("desc_recurso"+t+""+r):"";
                        String prioridad  = (request.getParameter("prioridad"+t+""+r)!=null)?request.getParameter("prioridad"+t+""+r):"0";
                        prioridad = (prioridad.equals(""))?"0":prioridad;
                        Standar recursos = new Standar();
                        
                        recursos.setCodigo_recurso(cod_recurso);
                        recursos.setTipo_recurso(tipo_recurso);
                        recursos.setDescripcion_recurso(desc_recurso);
                        recursos.setPrioridad(Integer.parseInt(prioridad));
                        
                        //bloque para llenar los campos de los fletes
                        int size_flete =  ((Standar)((Standar) model.StandarSvc.getSj().getListaTramos().get(t)).getListaRecursos().get(r)).getListaFletes().size();
                        List listaFlete = new LinkedList();
                        for(int f=0;f< size_flete ;f++){
                            String vlr_flete   = (request.getParameter("vlr_flete"+t+""+r+""+f)!=null)?request.getParameter("vlr_flete"+t+""+r+""+f):"0";
                            String flete_moneda    = (request.getParameter("flete_moneda"+t+""+r+""+f)!=null)?request.getParameter("flete_moneda"+t+""+r+""+f):"";
                            String flete_unidad    = (request.getParameter("flete_unidad"+t+""+r+""+f)!=null)?request.getParameter("flete_unidad"+t+""+r+""+f):"";
                            boolean flete_vigente  = (request.getParameter("flete_vigente"+t+""+r+""+f)!=null)?true:false;
                            
                            Standar fletes = new Standar();
                            
                            fletes.setVlr_flete(Double.parseDouble(vlr_flete.replaceAll(",","")));
                            fletes.setMoneda_flete(flete_moneda);
                            fletes.setUnidad_flete(flete_unidad);
                            fletes.setVigente_flete(flete_vigente);
                            listaFlete.add(fletes);
                            
                        }
                        //Fin del bloque para llenar los datos de los fletes
                        recursos.setListaFletes(listaFlete);
                        listaRecursos.add(recursos);
                    }
                    ///// Fin del bloque para llenar los datos de los recursos
                    tramos.setListaRecursos(listaRecursos);
                    listaTramos.add(tramos);
                    
                    
                }
                ///////////// Fin del bloque para llenar los tramos
                standar.setListaTramos(listaTramos);
                
                
                model.StandarSvc.setSj(standar);
                
                
                
                
                if(op.equals("AGREGAR_FLETE")){
                    String[]id = request.getParameter("id").split("-");
                    Standar sj = model.StandarSvc.getSj();
                    Standar tramo   = (Standar) sj.getListaTramos().get(Integer.parseInt(id[0]));
                    Standar recurso = (Standar) tramo.getListaRecursos().get(Integer.parseInt(id[1]));
                    
                    
                    Standar fletes = new Standar();
                    
                    fletes.setVlr_flete(0);
                    fletes.setMoneda_flete("PES");
                    fletes.setUnidad_flete("");
                    fletes.setVigente_flete(false);
                    
                    recurso.getListaFletes().add(fletes);
                    
                    
                }else if(op.equals("AGREGAR_RECURSO")){
                    String[]id = request.getParameter("id").split("-");
                    Standar sj = model.StandarSvc.getSj();
                    Standar tramo   = (Standar) sj.getListaTramos().get(Integer.parseInt(id[0]));
                    
                    Standar recursos = new Standar();
                    
                    recursos.setCodigo_recurso("");
                    recursos.setTipo_recurso("");
                    recursos.setDescripcion_recurso("");
                    
                    //Informacion de los fletes
                    Standar fletes = new Standar();
                    
                    fletes.setVlr_flete(0);
                    fletes.setMoneda_flete("PES");
                    fletes.setUnidad_flete("");
                    fletes.setVigente_flete(false);
                    
                    
                    List listaFletes = new LinkedList();
                    listaFletes.add(fletes);
                    recursos.setListaFletes(listaFletes);
                    
                    
                    tramo.getListaRecursos().add(recursos);
                    
                }else if(op.equals("AGREGAR_TRAMO")){
                    String id  = request.getParameter("id");
                    Standar sj = model.StandarSvc.getSj();
                    Standar tr = (Standar)sj.getListaTramos().get(Integer.parseInt(id));
                    if(model.StandarSvc.existeTramo(tr.getCod_origen_tramo(),tr.getCodigo_destino_tramo())){
                       // esta es para saber si el tramo existe el los demas 
                      /*  String orig_dest = tr.getCod_origen_tramo()+""+tr.getCodigo_destino_tramo();
                        boolean existe = false;
                        
                        if(sj.getListaTramos().size()>1){
                            for(int t =0; t< sj.getListaTramos().size();t++ ){
                                Standar tra = (Standar)sj.getListaTramos().get(t);
                                String or_des_lista = tra.getCod_origen_tramo()+""+tra.getCodigo_destino_tramo();
                                if(orig_dest.equals(or_des_lista) && t != Integer.parseInt(id) ){
                                    existe = true;
                                }
                            }
                        }
                        
                      if(!existe){  */
                            Standar tramos = new Standar();
                            
                            tramos.setCod_origen_tramo("");
                            tramos.setCodigo_destino_tramo("");
                            tramos.setNombre_origen_tramo("");
                            tramos.setNombre_destino_tramo("");
                            tramos.setPorcentaje_tramo("");
                            tramos.setPeso_lleno_tramo("");
                            tramos.setNumero_escoltas(0);
                            tramos.setTipo_trafico("");
                            tramos.setInd_vacio_tramo(false);
                            tramos.setControl_cargue(false);
                            tramos.setControl_descargue(false);
                            tramos.setCaravana(false);
                            tramos.setRequiere_planviaje(false);
                            tramos.setPreferencia(false);
                            tramos.setRequiere_hojareporte(false);
                            tramos.setContingente(false);
                            
                            //Informacion de los recursos
                            Standar recursos = new Standar();
                            
                            recursos.setCodigo_recurso("");
                            recursos.setTipo_recurso("");
                            recursos.setDescripcion_recurso("");
                            
                            //Informacion de los fletes
                            Standar fletes = new Standar();
                            
                            fletes.setVlr_flete(0);
                            fletes.setMoneda_flete("PES");
                            fletes.setUnidad_flete("");
                            fletes.setVigente_flete(false);
                            
                            
                            List listaFletes = new LinkedList();
                            listaFletes.add(fletes);
                            recursos.setListaFletes(listaFletes);
                            /////////////////////////////////////
                            
                            
                            List listaRecursos = new LinkedList();
                            listaRecursos.add(recursos);
                            
                            tramos.setListaRecursos(listaRecursos);
                            
                            sj.getListaTramos().add(tramos);
                            
                     /*   }else{
                            next+="?ms=El tramo "+ (Integer.parseInt(id) + 1 ) + " con origen "+ tr.getNombre_origen_tramo()+ "  y destino "+ tr.getNombre_destino_tramo()+" ya existe en la lista de tramos";
                        }//fin if(!existe)*/
                        
                    }else if(tr.getCod_origen_tramo().equals("") || tr.getCodigo_destino_tramo().equals("")){
                        next+="?ms=El tramo "+ (Integer.parseInt(id) + 1 ) + " no puede tener el origen o el destino vacio";
                    }else{
                        next+="?ms=El tramo con origen "+ tr.getNombre_origen_tramo()+ "  y destino "+ tr.getNombre_destino_tramo()+" no existe..";
                    }
                    
                }else if(op.equals("AGREGAR_TARIFA")){
                    Standar sj = model.StandarSvc.getSj();
                    Standar tarifa = new Standar();
                    tarifa.setVlr_tarifa(0);
                    tarifa.setMoneda_tarifa("PES");
                    tarifa.setUnidad_tarifa("");
                    tarifa.setVigente_tarifa(false);
                    
                    sj.getListaTarifa().add(tarifa);
                }else if(op.equals("ELIMINAR_FLETE")){
                    String[]id = request.getParameter("id").split("-");
                    Standar sj = model.StandarSvc.getSj();
                    Standar tramo   = (Standar) sj.getListaTramos().get(Integer.parseInt(id[0]));
                    Standar recurso = (Standar) tramo.getListaRecursos().get(Integer.parseInt(id[1]));
                    if(recurso.getListaFletes().size() > 1){
                        recurso.getListaFletes().remove(Integer.parseInt(id[2]));
                    }
                    
                }else if(op.equals("ELIMINAR_RECURSO")){
                    String[]id = request.getParameter("id").split("-");
                    Standar sj = model.StandarSvc.getSj();
                    Standar tramo   = (Standar) sj.getListaTramos().get(Integer.parseInt(id[0]));
                    
                    if(tramo.getListaRecursos().size() > 1){
                        tramo.getListaRecursos().remove(Integer.parseInt(id[1]));
                    }
                    
                }else if(op.equals("ELIMINAR_TRAMO")){
                    String id = request.getParameter("id");
                    Standar sj = model.StandarSvc.getSj();
                    sj.getListaTramos().get(Integer.parseInt(id));
                    
                    if(sj.getListaTramos().size() > 1){
                        sj.getListaTramos().remove(Integer.parseInt(id));
                    }
                }else if(op.equals("ELIMINAR_TARIFA")){
                    String id = request.getParameter("id");
                    Standar sj = model.StandarSvc.getSj();
                    
                    if(sj.getListaTarifa().size() > 1){
                        sj.getListaTarifa().remove(Integer.parseInt(id));
                    }
                    
                }else if(op.equals("GUARDAR")){
                    //validamos si el origen y el destino del estandar existe en la tabla via
                    if(model.StandarSvc.existeTramo(model.StandarSvc.getSj().getOrigen(),model.StandarSvc.getSj().getDestino())){
                        boolean existeTramo= true;
                        Vector vectorTramos= new Vector();//guardamos los tramos marcados como preferentes
                        Vector TramosNoPref= new Vector();//guardamos los tramos no preferentes
                        int h = 0;
                        int j = 0;
                        
                        //////////////////////////////////////////////////////////////////////////
                        //se validan si los tramos existen en la tabla via
                        ////////////////////////////////////////////////////////////////////////
                        for(int t =0; t< model.StandarSvc.getSj().getListaTramos().size();t++ ){
                            Standar tramo = (Standar)model.StandarSvc.getSj().getListaTramos().get(t);
                            if(tramo.isPreferencia()){
                                vectorTramos.add(h,tramo.getCod_origen_tramo()+"-"+tramo.getCodigo_destino_tramo());
                                h++;
                            }else{
                                TramosNoPref.add(j,tramo.getCod_origen_tramo()+"-"+tramo.getCodigo_destino_tramo());
                                j++;
                            }
                            //validamos si el tramo existe ne la tabla via
                            if(!model.StandarSvc.existeTramo(tramo.getCod_origen_tramo(),tramo.getCodigo_destino_tramo())){
                                next+="?ms=El tramo "+tramo.getCod_origen_tramo()+" - "+ tramo.getCodigo_destino_tramo()+"  no exite";
                                existeTramo=false;
                                break;
                            }
                        }
                        //////////////////////////////////////////////////////////////////////////////////////////////////////
                        
                        
                        
                        
                        
                        ///////////////////////////////////////
                        //validaciones para la ruta preferente
                        ///////////////////////////////////////
                        if(existeTramo){
                            if(vectorTramos.size()>0){
                                String ori  ="";
                                String dest ="";
                                for(int i=0; i<vectorTramos.size();i++ ){
                                    String[] tra = ((String) vectorTramos.get(i)).split("-"); 
                                    if(i==0){
                                        ori = tra[0];
                                        dest= tra[1];
                                        if(!model.StandarSvc.existeTramo(model.StandarSvc.getSj().getOrigen(),ori)){
                                            next+="?ms=La via "+model.StandarSvc.getSj().getOrigen()+" - "+ ori+"  no exite";
                                            existeTramo =false;
                                            break;
                                            
                                        }else if(vectorTramos.size()==1 && !model.StandarSvc.existeTramo(dest,model.StandarSvc.getSj().getDestino())){
                                            next+="?ms=La via "+dest+" - "+ model.StandarSvc.getSj().getDestino()+"  no exite";
                                            existeTramo =false;
                                            break;
                                        }
                                        
                                        
                                    }else{
                                        String ori2 = tra[0];
                                        String dest2= tra[1];                                       
                                        if(!model.StandarSvc.existeTramo(dest,ori2)){
                                            next+="?ms=La via "+model.StandarSvc.getSj().getOrigen()+" - "+ ori+"  no exite";
                                            existeTramo =false;
                                            break;
                                            
                                        }else if(i == vectorTramos.size()-1 && !model.StandarSvc.existeTramo(dest2,model.StandarSvc.getSj().getDestino())){
                                            next+="?ms=La via "+dest+" - "+ model.StandarSvc.getSj().getDestino()+"  no exite";
                                            existeTramo =false;
                                            break;
                                        }
                                        ori = ori2;
                                        dest = dest2;
                                    }
                                    
                                    
                                }
                            }else{
                                next+="?ms=En los tramos debe existir una ruta preferente";
                                existeTramo=false;

                            }
                        }
                        ///////////////////////////////////////////////////////////////////////////////////////
                        //Fin validaciones para la ruta preferente
                        ////////////////////////////////////////////////////////////////////////////////////////
                        
                        
                        
                        ////////////////////////////////////////////////////////////////////////////////////////
                        //Validaciones para los tramos que no pertenecen a la ruta preferente 
                        //////////////////////////////////////////////////////////////////////////////////////////
                        if(existeTramo){
                            if(TramosNoPref.size()>0 && vectorTramos.size()>0 ){
                                String cadenaTramos=""; 
                                for(int a=0; a<vectorTramos.size();a++ ){
                                    cadenaTramos+=  (String) vectorTramos.get(a);    
                                } 
                                
                                 for(int i=0; i<TramosNoPref.size();i++ ){
                                      String[] tra = ((String) TramosNoPref.get(i)).split("-"); 
                                      String ori  = tra[0];
                                      String dest = tra[1];
                                      if(cadenaTramos.indexOf(ori)==-1 &&  cadenaTramos.indexOf(dest)==-1){
                                           next+="?ms=El tramo no preferencial con origen "+ori+" y destino "+dest+" debe tener como origen o destino una ciudad que este en alguno de los tramos preferenciales";
                                           existeTramo =false;
                                           break;
                                      } 
                                      
                                 }
                            }
                        }
                        /////////////////////////////////////////////////////////////////////////////////////////
                        
                        if(existeTramo){
                            if(!model.StandarSvc.existeSj(model.StandarSvc.getSj().getDstrct_code(),model.StandarSvc.getSj().getCodigo_sj())){
                                Vector VecAgencias = model.cxpDocService.getAgencias();
                                String agContable ="";
                                for(int i=0; i< VecAgencias.size();i++){
                                    Agencia ag = (Agencia) VecAgencias.get(i);
                                    if(usuario.getId_agencia().equals(ag.getId_agencia())){
                                        agContable =  ag.getAgenciaContable();
                                    }
                                }
                                if(!agContable.equals("")){
                                    account_code_c ="C"+agContable+""+model.StandarSvc.getSj().getUnidad_negocio()+""+model.StandarSvc.getSj().getCod_cli().substring(3,6)+"9005";
                                    account_code_i ="I"+agContable+""+model.StandarSvc.getSj().getUnidad_negocio()+""+model.StandarSvc.getSj().getCod_cli().substring(3,6)+"8005";
                                    model.StandarSvc.getSj().setAccount_code_c(account_code_c);
                                    model.StandarSvc.getSj().setAccount_code_i(account_code_i);
                                    model.StandarSvc.grabarEstandar(model.StandarSvc.getSj());
                                    next+="?ms=El estandar fue guardado exitosamente..";
                                }else{
                                    next+="?ms=La agencia "+usuario.getId_agencia()+" no tiene agencia contable para el codigo de cuenta";
                                }
                            }else{
                                next+="?ms=El estandar ya existe en la base de datos";
                            }
                        }//fin de if(existeTramo)
                    }else{
                        next+="?ms=El origen y el destino del estandar no existe en la tabla via";
                    }
                }
                
            }
            //System.out.println("NEXT--->"+ next);
            
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
        
    }
}


