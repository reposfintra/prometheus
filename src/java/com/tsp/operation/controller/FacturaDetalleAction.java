/********************************************************************
 *      Nombre Clase.................   FacturaDetalleAction.JAVA
 *      Descripci�n..................   Action que se encarga de la busqueda de facturas
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   Created on 14 de julio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  Ivan Gomez
 */
public class FacturaDetalleAction extends Action{
    
    /** Creates a new instance of FacturaLArchivoAction */
    public FacturaDetalleAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try {
            
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin=""+usuario.getLogin();
            String next = "/jsp/cxpagar/reportes/DetalleFactura.jsp";
            String agencia_usuario = "";
            CXP_Doc factura;
            Vector vItems;
            String maxfila = "1";
            String fecha_Aprobacion = "";
            String fecha_Contabilizacion ="";
            model.agenciaService.loadAgencias();
            model.tblgensvc.searchDatosTablaAUTCXP();
                        
            boolean isSaldoDif = request.getParameter("isSaldoDif")!=null ?true:false;
            next+= (isSaldoDif)?"?isSaldoDif=true":"";
            model.ChequeFactXCorridasSvc.loadBancos( usuario.getDstrct(), usuario.getId_agencia() );
            model.ChequeFactXCorridasSvc.loadSucursales(usuario.getDstrct(), usuario.getId_agencia() );
           
            String documento = request.getParameter("documento").replaceAll("-_-","#");
            String prov      = request.getParameter("prov");
            //System.out.println("AQUI---->>>>>>TIPO_DOC======>>>>"+request.getParameter("tipo_doc"));
            String tipo_doc  = request.getParameter("tipo_doc")!=null ?request.getParameter("tipo_doc"):"010";
            
            System.out.println("el documento = "+documento+usuario.getDstrct()+prov+tipo_doc);
            factura = model.cxpDocService.detalleFactura(usuario.getDstrct(),prov,documento,tipo_doc);
            Vector vTipoImp = model.TimpuestoSvc.vTiposImpuestos();
            vItems = model.cxpDocService.detalleItemFactura(factura, vTipoImp);
            
            
            request.setAttribute("Main", factura);            
            request.setAttribute("Items", vItems);
                
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}