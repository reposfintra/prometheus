/********************************************************************
 *      Nombre Clase.................   CamposJspBuscarPagIngresarAction.java
 *      Descripci�n..................   Selecciona una p�gina JSP para el ingreso campos.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   15.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;


public class CamposJspBuscarPagIngresarAction extends Action{
    
    /** Creates a new instance of ActividadSerchAction */
    public CamposJspBuscarPagIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String pag = "/jsp/trafico/permisos/campos_jsp/Campos_jspInsertar.jsp";
        String next;
        HttpSession session = request.getSession();
        String perfil = (String) request.getParameter("perfil");
        String pagina = (String) request.getParameter("pag");
        String campo = (String) request.getParameter("campo");
        //System.out.println(perfil);
        
        pag += "?perfil=" + perfil + "&pag=" + pagina + "&campo=" + campo;
        
        next = pag;
        
        this.dispatchRequest(next);
    }
}
