/*************************************************************
 * Nombre: ReporteDespachoXLS.java
 * Descripci�n: Accion para invocar el hilo del reporte indice de despacho
 * Autor: Ing. Jose de la rosa
 * Fecha: 15 de diciembre de 2005, 04:56 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;

public class ReporteDespachoXlsAction extends Action{
    
    /** Creates a new instance of ReporteDespachoXLS */
    public ReporteDespachoXlsAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String fechini = request.getParameter("c_fecha_inicio");        
        String fechfin = request.getParameter("c_fecha_fin");        
        try{
            ReporteExportarDespachoXLS hilo = new ReporteExportarDespachoXLS();
            hilo.start(usuario.getLogin(),fechini,fechfin);
            next = "/jsp/masivo/reportes/indicadorDespacho.jsp?";
            request.setAttribute("mensaje","Su reporte ha iniciado y se encuentra en el log de procesos");
        }catch (Exception ex){
            throw new ServletException("Error en OpcionesExportarAction .....\n"+ex.getMessage());
        }

        this.dispatchRequest(next);         
    }
    
}
