/**
 * Nombre        PlanillaCostosAction.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         10 de mayo de 2006, 09:08 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;


public class PlanillaCostosAction extends Action{
    
    /** Crea una nueva instancia de  PlanillaCostosAction */
    public PlanillaCostosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        try{
            String next   = "/jsp/masivo/utilidad/planillas.jsp";
            String opcion = defaultString("Opcion","");
            String numpla = defaultString("Planilla","");
            
            if (opcion.equals("Consultar")){
                model.PlanillaCostosSvc.generarCostos(numpla);
                if (model.PlanillaCostosSvc.getListaRemesas()==null || model.PlanillaCostosSvc.getListaRemesas().isEmpty()){
                    request.setAttribute("msg","No se econtraron datos para esta planilla");
                }
            }
            else if (opcion.equals("Resetear")){
                model.PlanillaCostosSvc.setListaRemesas(null);
            }
            
            
            this.dispatchRequest(next);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new javax.servlet.ServletException (ex.getMessage());
        }
    }
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }     
    
}
