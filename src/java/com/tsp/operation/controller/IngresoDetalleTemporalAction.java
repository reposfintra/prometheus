/*
 * ImgresoDetalleTemporalAction.java
 *
 * Created on 9 de agosto de 2006, 04:56 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;

/**
 *
 * @author  dbastidas
 */
public class IngresoDetalleTemporalAction extends Action{
    
    /** Creates a new instance of ImgresoDetalleTemporalAction */
    public IngresoDetalleTemporalAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next="/jsp/cxcobrar/ItemsIngreso/ItemsIngresoMiscelaneo.jsp";
        String auxiliar = "";
        String mon_local = (String) session.getAttribute("Moneda");
        String fecha_actual = Util.getFechaActual_String(4);
        String mon_ingreso = request.getParameter("mon_ingreso");
        String num_ingreso = request.getParameter("numingreso");
        int canItems = Integer.parseInt(request.getParameter("items"));
        
        Ingreso ing  = model.ingresoService.getIngreso();
        
        
        int item = 1;
        Vector vecitems = new Vector();
         
        try{
            for(int i=1; i<= canItems; i++ ){
                if ( request.getParameter("valor"+i) != null ){
                    Ingreso_detalle ingdetalle = new Ingreso_detalle();
                    ingdetalle.setDistrito(usuario.getDstrct());
                    ingdetalle.setNumero_ingreso(num_ingreso);
                    ingdetalle.setItem(item);
                    ingdetalle.setTipo_documento(request.getParameter("tipodoc"));
                    ingdetalle.setCuenta(request.getParameter( "cuenta"+i ).toUpperCase() );
                    ingdetalle.setTipo_aux( request.getParameter( "tipo"+i ) );
                    ingdetalle.setAuxiliar( request.getParameter( "auxiliar"+i ) );
                    ingdetalle.setDescripcion(request.getParameter( "c_descripcion"+i ) );
                    ingdetalle.setTipo_doc( (request.getParameter( "doc"+i ).equals("") )?"":request.getParameter( "tipodoc"+i ) );
                    ingdetalle.setDocumento( request.getParameter( "doc"+i ).toUpperCase() );
                    ingdetalle.setValor_ingreso_me( convertirdouble(request.getParameter("valor"+i) ) );
                    ingdetalle.setValor_ingreso( convertirdouble( request.getParameter("valor"+i)) );
                    ingdetalle.setCodigo_retefuente( "" );
                    ingdetalle.setValor_retefuente_me( 0.0 );
                    ingdetalle.setValor_retefuente( 0.0  );
                    ingdetalle.setCodigo_reteica( "" );
                    ingdetalle.setValor_reteica_me( 0.0 );
                    ingdetalle.setValor_reteica( 0.0  );
                    ingdetalle.setValor_total( 0.0 );
                    ingdetalle.setCreation_user(usuario.getLogin());
                    ingdetalle.setBase(usuario.getBase());
                    ingdetalle.setFecha_contabilizacion("0099-01-01 00:00:00");
                    LinkedList tbltipo = new LinkedList();
                    
                    //busco los tipos de subledger
                    if(modelcontab.planDeCuentasService.existCuenta(ingdetalle.getDistrito(),ingdetalle.getCuenta())){
                        modelcontab.subledgerService.buscarCuentasTipoSubledger(ingdetalle.getCuenta());
                        if(modelcontab.subledgerService.getCuentastsubledger()!=null){
                            //System.out.println(" != Null"+modelcontab.subledgerService.getCuentastsubledger().size() );
                            tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                        }
                    }
                    ingdetalle.setTipos(tbltipo);
                    vecitems.add(ingdetalle);
                    
                    item++;
                }
            }
            //System.out.println(vecitems.size());
            model.ingreso_detalleService.escribirArchivoTemporal(ing,vecitems,"Ing_miscelaneo"+num_ingreso+".txt",usuario.getLogin());
            model.ingreso_detalleService.setItemsMiscelaneo(vecitems);
            next+="?numero="+canItems;
            
            
            this.dispatchRequest(next);
            
        }catch(Exception e){
             e.printStackTrace();
        }
        
        
        
    }
    
    public double convertirdouble(String valor){
        String temp = (valor.equals(""))? "0": valor.replace(",","");
        return Double.parseDouble( temp );
    }
}


