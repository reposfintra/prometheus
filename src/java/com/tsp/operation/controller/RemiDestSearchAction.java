/***********************************************************************************
 * Nombre clase : ............... RemiDestSearchAction.java                        *
 * Descripcion :................. Clase que maneja los eventos relacionados con la *
 *                                busqueda de Remitentes y Destinatarios           * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 18 de noviembre de 2005, 08:30 AM                *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class RemiDestSearchAction extends Action{    
    
    public RemiDestSearchAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {                
        
        String next = "/jsp/masivo/remitente_destinatario/rem_destSearch.jsp?msg=";        
        String codcli      =  request.getParameter("codcli");        
        String tipo        =  request.getParameter("tipo");        
        String ciudad      =  request.getParameter("ciudad");        
        String codigo      = "";
        /* Construyendo el codigo a buscar*/        
        codigo = codcli.substring(3,6)+tipo+ciudad;                
        try{ 
            model.remidestService.listarRemitentesDestinatarios(codigo);
            Vector vec = model.remidestService.getVectorRemiDest();
            if (vec.size()==0) {
                next+="Su busqueda no arrojo ningun resultado";
            } else {                
                next = "/jsp/masivo/remitente_destinatario/listaRemDest.jsp";
            }
                
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }        
        this.dispatchRequest(next);
        
    }
    
}
