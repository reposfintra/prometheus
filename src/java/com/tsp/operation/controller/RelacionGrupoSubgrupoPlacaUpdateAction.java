/********************************************************************
 *      Nombre Clase.................   RelacionGrupoSubgrupoPlacaUpdateAction.java
 *      Descripci�n..................   Modifica las Relaciones entre las placas, grupos y subgrupos
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  EQUIPO12
 */
public class RelacionGrupoSubgrupoPlacaUpdateAction extends Action{
        
        /** Creates a new instance of RelacionGrupoSubgrupoPlacaUpdateAction */
        public RelacionGrupoSubgrupoPlacaUpdateAction() {
        }
        
         public void run() throws ServletException, InformationException {
                RelacionGrupoSubgrupoPlaca relacion = new RelacionGrupoSubgrupoPlaca();
                
                String  next="";
                //Info del usuario
                String sw="";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String placa = request.getParameter("placa");
                String grupo = request.getParameter("grupo");
                String subgrupo = request.getParameter("subgrupo");
                String user = usuario.getLogin();
                String base = usuario.getBase();
                String dstrct = usuario.getDstrct();
                sw = request.getParameter("sw");
                //////System.out.println("placa = "+placa+" grupo = "+grupo+" subgrupo = "+subgrupo);
                String grupo1 = request.getParameter("grupo1");
                String subgrupo1 = request.getParameter("subgrupo1");
                //////System.out.println("grupo1 = "+grupo1+"  subgrupo = "+subgrupo1);
                 
                relacion.setPlaca(placa);
                relacion.setGroup(grupo1);
                relacion.setSubgroup(subgrupo1);
                relacion.setBase(base);
                relacion.setUser_update(user);
                relacion.setDstrct(dstrct);
                
                try {
                        if (sw.equals("Mostrar")&&(sw!=null)){
                                next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlacaUpdate.jsp?placa="+placa+"&grupo="+grupo+"&subgrupo="+subgrupo+"&msg=";
                        }else if (sw.equals("anular")) {
                                
                                model.relaciongruposubgrupoplaca.AnularRelacion(placa, subgrupo1, grupo1);
                                next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlacaUpdate.jsp?msg=Relacion Anulada Satisfactoriamente&reload=ok";
                        
                        }else{
                                boolean existe = model.relaciongruposubgrupoplaca.ExisteRelacion(relacion.getPlaca(), relacion.getSubgroup(), relacion.getGroup());
                                if (existe == false){
                                        model.relaciongruposubgrupoplaca.UpdateRelacion(relacion, placa, subgrupo, grupo);
                                        next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlacaUpdate.jsp?msg=Relacion Modificada Satisfactoriamente&reload=ok";
                                }else{
                                        RelacionGrupoSubgrupoPlaca relacion1 = new RelacionGrupoSubgrupoPlaca();
                                        List relaciones;
                                        relaciones = model.relaciongruposubgrupoplaca.ConsultaInsertRelacion(placa, grupo1, subgrupo1);
                                        for (int i = 0; i < relaciones.size(); i++){
                                                relacion1 = (RelacionGrupoSubgrupoPlaca)relaciones.get(i);
                                        }
                                        String reg_status = relacion1.getReg_status();
                                        if (reg_status.equals("A")) {
                                                model.relaciongruposubgrupoplaca.EliminarRelacion(relacion);
                                                model.relaciongruposubgrupoplaca.UpdateRelacion(relacion, placa, subgrupo,  grupo);
                                                next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlacaUpdate.jsp?msg=Relacion Modificada Satisfactoriamente&reload=ok";
                                        }else{
                                                next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlacaUpdate.jsp?msg= La relacion a la que quiere Modificar Ya existe";
                                        }
                                }
                        }
                        
                }catch(SQLException e){
                        throw new ServletException(e.getMessage());
                }
                this.dispatchRequest(next);
        }
        
}
