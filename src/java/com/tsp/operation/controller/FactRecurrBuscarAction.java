/********************************************************************
 *      Nombre Clase.................   BuscarFacturaAction.JAVA
 *      Descripci�n..................   Action que se encarga de la busqueda de facturas
 *      Autor........................   Ivan Gomez Vanegas
 *      Fecha........................   Created on 15 de mayo de 2006, 03:48 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  Ivan Gomez
 */
//logger
import org.apache.log4j.Logger;

public class FactRecurrBuscarAction extends Action{
    
    Logger log = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of FacturaLArchivoAction */
    public FactRecurrBuscarAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try {
            
            HttpSession session = request.getSession();
            //ivan 21 julio 2006
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            //////////////////////
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin=""+usuario.getLogin();
            String next = "/jsp";
            String agencia_usuario = "";
            CXP_Doc factura;
            Vector vItems;
            String maxfila = "1";
            String fecha_Aprobacion = "";
            String fecha_Contabilizacion ="";
            
            String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
            model.servicioBanco.loadBancos(age, usuario.getCia());
            
            model.cxpDocService.obtenerAgencias();
            model.agenciaService.loadAgencias();
            model.tblgensvc.searchDatosTablaAUTCXP();
            model.cxpDocService.BuscarHC();
            model.factrecurrService.setTipos( model.tablaGenService.BuscarDatosTreemap("TIPO_RXP") );//Osvaldo
            
            String documento = request.getParameter("documento");
            String prov      = request.getParameter("prov");
            
            log.info("DOCUMENTO: " + documento);
            log.info("PROVEEDOR: " + prov);
            
            //System.out.println("el documento = "+documento);
            factura = model.factrecurrService.BuscarFactura(usuario.getDstrct(),prov,documento);
            if(factura!=null){
                fecha_Aprobacion = factura.getFecha_aprobacion();
                fecha_Contabilizacion = factura.getFecha_contabilizacion();
                model.factrecurrService.setFactura(factura);
                agencia_usuario = factura.getAgencia();
                session.setAttribute("id_agencia", agencia_usuario );
                Vector vTipImp = model.TimpuestoSvc.vTiposImpuestos();
                vItems = model.factrecurrService.BuscarItemFactura(factura,vTipImp);
                maxfila = String.valueOf(vItems.size());
                maxfila = (!maxfila.equals(null)?maxfila:"1");
                // Modificacion 21 julio 2006
                for(int i=0; i< vItems.size();i++){
                    CXPItemDoc item = (CXPItemDoc)vItems.get(i);
                    if(item != null){
                        
                        LinkedList tbltipo = null;
                        if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),item.getCodigo_cuenta())){
                            modelcontab.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),item.getCodigo_cuenta());
                            tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                        }
                        
                        item.setTipo(tbltipo);
                        item.setAgencia(usuario.getId_agencia());
                        
                    }
                }
                //////////////////////////////////////////////////////////////////////
                
                
                
                
                
                model.factrecurrService.setVecRxpItemsDoc(vItems);
                String VerifOP ="";
                if(factura.getDocumento().length()>= 2 ){
                    VerifOP = factura.getDocumento().substring(0,2);
                }
                
                if( (factura.getVlr_transfer_me() == 0) && (factura.getNum_cuotas_transfer() != factura.getNum_cuotas()) && (!factura.getReg_status().equals("A")) ){
                    if(!VerifOP.equals("OP") &&  factura.getCorrida().equals("")  ){
                        if(fecha_Aprobacion.equals("0099-01-01 00:00:00") && fecha_Contabilizacion.equals("0099-01-01 00:00:00")){
                            next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ag=false&maxfila="+maxfila+"&Modificar=si";
                            
                            agencia_usuario = usuario.getId_agencia();
                            if( !agencia_usuario.equals("OP")  ){
                                
                                model.tablaGenService.buscarReferencias("TAGECONT", agencia_usuario);
                                LinkedList l = model.tablaGenService.getReferencias();
                                String agenciaContable="--";
                                String unidadNegocio ="---";
                                if(l!=null && l.size() > 0  ){
                                    TablaGen tg = (TablaGen)l.getFirst();
                                    agenciaContable = tg.getTable_code();
                                    
                                    model.tablaGenService.buscarReferencias("TUNIDADES", tg.getTable_code());
                                    LinkedList l1 = model.tablaGenService.getReferencias();
                                    TablaGen tg1 = (TablaGen)l1.getFirst();
                                    unidadNegocio = tg1.getTable_code();
                                }
                                session.setAttribute("agContable", agenciaContable );
                                session.setAttribute("unidadC", unidadNegocio );
                                
                                
                                next = "/jsp/cxpagar/facturasrec/facturaP.jsp?op=cargarB&num_items=1&ag=false&id_agencia="+agencia_usuario+"&agContable="+agenciaContable+"&unidadC="+unidadNegocio+"&maxfila="+maxfila+"&Modificar=si";
                            }
                            else{
                                
                                agencia_usuario = (request.getParameter("agencia")!=null)?request.getParameter("agencia"):"";
                                model.tablaGenService.buscarReferencias("TAGECONT", agencia_usuario);
                                LinkedList l = model.tablaGenService.getReferencias();
                                
                                if( agencia_usuario.equals("") ){
                                    factura = model.factrecurrService.getFactura();
                                    if( factura != null )
                                        agencia_usuario = factura.getAgencia();
                                }
                                
                                session.setAttribute("id_agencia",agencia_usuario );
                                
                                String agenciaContable = "--";
                                String unidadNegocio = "---";
                                if( l!=null && l.size() > 0 ){
                                    TablaGen tg = (TablaGen)l.getFirst();
                                    agenciaContable = tg.getTable_code();
                                    //System.out.println("Agecia Contable " + tg.getTable_code());
                                    
                                    model.tablaGenService.buscarReferencias("TUNIDADES", tg.getTable_code());
                                    LinkedList l1 = model.tablaGenService.getReferencias();
                                    TablaGen tg1 = (TablaGen)l1.getFirst();
                                    unidadNegocio = tg1.getTable_code();
                                    //System.out.println("Unidad de Negocio " + tg1.getTable_code());
                                    
                                }
                                session.setAttribute("agContable", agenciaContable );
                                session.setAttribute("unidadC", unidadNegocio );
                                
                                
                                
                                if( request.getParameter("agencia")!=null  ){
                                    
                                    String tipo_documento =""+ request.getParameter("tipo_documento");
                                    documento = (request.getParameter("documento")!=null)?request.getParameter("documento"):"";
                                    String proveedor =""+ request.getParameter("proveedor");
                                    String tipo_documento_rel =""+ request.getParameter("tipo_documento_rel");
                                    String documento_relacionado =""+ request.getParameter("documento_relacionado");
                                    String fecha_documento=""+request.getParameter("fecha_documento");
                                    String banco =""+ request.getParameter("c_banco");
                                    double vlr_neto=model.factrecurrService.getNumero(""+ request.getParameter("vlr_neto"));
                                    double total = model.factrecurrService.getNumero(""+ request.getParameter("total"));
                                    String validar =""+ request.getParameter("validar");
                                    int plazo=0;
                                    int num_items=0;
                                    try{
                                        num_items  = Integer.parseInt(""+ request.getParameter("num_items"));
                                        //System.out.println("Numero de Items "+num_items);
                                        plazo = Integer.parseInt(""+ request.getParameter("plazo"));
                                    } catch(java.lang.NumberFormatException e){
                                        total = 0;
                                        vlr_neto=0;
                                        plazo=0;
                                        num_items=1;
                                    }
                                    
                                    vItems = new Vector();
                                    //System.out.println("Numero de Items " + num_items);
                                    for (int i=1;i <= num_items; i++){
                                        CXPItemDoc item = new CXPItemDoc();
                                        String descripcion_i=""+ request.getParameter("descripcion_i"+i);
                                        String codigo_cuenta=""+ request.getParameter("codigo_cuenta"+i);
                                        String codigo_abc=""+ request.getParameter("codigo_abc"+i);
                                        String planilla=""+ request.getParameter("planilla"+i);
                                        double valor =0;
                                        double vlr_total = 0;
                                        
                                        String tipcliarea=""+ request.getParameter("cod_oc"+i);
                                        String codcliarea= ""+ request.getParameter("oc"+i);
                                        String descliarea= ""+ request.getParameter("doc"+i);
                                        
                                        valor=model.factrecurrService.getNumero(""+ request.getParameter("valor1"+i));
                                        vlr_total = model.factrecurrService.getNumero(""+ request.getParameter("valorNeto"+i));
                                        
                                        
                                        
                                        //System.out.println("descripcion_i "+descripcion_i);
                                        //System.out.println("codigo_cuenta "+codigo_cuenta);
                                        //System.out.println("codigo_abc "+codigo_abc);
                                        //System.out.println("planilla"+planilla);
                                        //System.out.println("valor "+ valor);
                                        
                                        item.setDescripcion(descripcion_i);
                                        item.setCodigo_cuenta(codigo_cuenta);
                                        item.setCodigo_abc(codigo_abc);
                                        item.setPlanilla(planilla);
                                        item.setVlr(valor);
                                        item.setVlr_total(vlr_total);
                                        item.setTipcliarea(tipcliarea);
                                        item.setCodcliarea(codcliarea);
                                        item.setDescliarea(descliarea);
                                        
                                        
                                        
                                        Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos();
                                        Vector vImpuestosPorItem= new Vector();
                                        for(int x=0;x<vTipoImp.size();x++){
                                            CXPImpItem impuestoItem = new CXPImpItem();
                                            String cod_impuesto = ""+ request.getParameter("impuesto"+x+""+i);
                                            impuestoItem.setCod_impuesto(cod_impuesto);
                                            //System.out.println("impuesto"+cod_impuesto);
                                            vImpuestosPorItem.add(impuestoItem);
                                        }
                                        item.setVItems( vImpuestosPorItem);
                                        item.setVCopia(vImpuestosPorItem);
                                        vItems.add(item);
                                    }
                                    
                                    model.factrecurrService.setVecRxpItemsDoc(vItems);
                                    
                                    String sucursal =""+ request.getParameter("c_sucursal");
                                    String moneda=""+ request.getParameter("moneda");
                                    String descripcion =""+ request.getParameter("descripcion");
                                    String observacion =""+ request.getParameter("observacion");
                                    String usuario_aprobacion=""+request.getParameter("usuario_aprobacion");
                                    
                                    
                                    Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
                                    int b =0;
                                    if(o_proveedor != null) {
                                        //System.out.println(" Id Mins "+o_proveedor.getC_idMims());
                                        //System.out.println(" Beneficiario "+o_proveedor.getC_branch_code());
                                        //System.out.println(" Sucursal "+o_proveedor.getC_bank_account());
                                        //System.out.println(" Id Mins "+o_proveedor.getC_payment_name());
                                        b=1;
                                    }
                                    
                                    if (b!=1) {
                                        proveedor="";
                                    }
                                    
                                    factura = new CXP_Doc();
                                    factura.setTipo_documento(tipo_documento);
                                    factura.setDocumento(documento);
                                    factura.setProveedor(proveedor);
                                    factura.setTipo_documento_rel(tipo_documento_rel);
                                    factura.setDocumento_relacionado(documento_relacionado);
                                    factura.setFecha_documento(fecha_documento);
                                    factura.setBanco(banco);
                                    factura.setVlr_neto(total);
                                    factura.setVlr_total(vlr_neto);
                                    factura.setMoneda(moneda);
                                    factura.setSucursal(sucursal);
                                    factura.setDescripcion(descripcion);
                                    factura.setObservacion(observacion);
                                    factura.setUsuario_aprobacion(usuario_aprobacion);
                                    factura.setPlazo(plazo);
                                    model.factrecurrService.setFactura(factura);
                                }
                                
                                
                                next = "/jsp/cxpagar/facturasrec/facturaP.jsp?op=cargarB&num_items=1&ag=true&id_agencia="+agencia_usuario+"&agContable="+agenciaContable+"&unidadC="+unidadNegocio+"&maxfila="+maxfila+"&Modificar=si";
                            }
                            
                        }else{
                            next = "/jsp/cxpagar/facturasrec/VerfacturaP.jsp?ag=false&maxfila="+maxfila+"&Modificar=si";
                        }
                    }else{
                        next = "/jsp/cxpagar/facturasrec/VerfacturaP.jsp?ag=false&maxfila="+maxfila+"&Modificar=si";
                    }
                }else{
                    next = "/controller?estado=FacturaRecurrente&accion=Detalle&documento="+documento+"&prov="+prov+"&tipo_doc=010";
                }
            }else{
                next = "/jsp/cxpagar/facturasrec/BuscarFactura.jsp?mensaje=La factura no existe";
            }
            //System.out.println("NEXT-->"+ next);            
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}

