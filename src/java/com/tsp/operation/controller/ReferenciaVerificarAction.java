/*************************************************************************
 * Nombre:        ReferenciaVerificarAction.java
 * Descripci�n:   Clase Action para Buscar referencia
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         1 de marzo de 2006, 02:55 PM                        
 * Versi�n        1.0                                   
 * Coyright:      Transportes Sanchez Polo S.A.         
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class ReferenciaVerificarAction extends Action {
    
    /** Creates a new instance of ReferenciaVerificarAction */
    public ReferenciaVerificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String dstrct = (String) session.getAttribute ("Distrito");
        String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina")+"?men=ok";
        Referencia ref = new Referencia();
        ref.setReg_status( request.getParameter("esta") );
        ref.setUsrconfirma( request.getParameter("nombre").toUpperCase() );
        ref.setFecconfir(request.getParameter("fecha"));
        ref.setRef_confirmacion(request.getParameter("referencia"));
        ref.setDstrct_code(dstrct);
        ref.setTiporef(request.getParameter("tipo"));
        ref.setClase(request.getParameter("clase"));
        ref.setFecref(request.getParameter("fecref"));
        
        try{
           model.referenciaService.verificarReferencia(ref);          
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         ////System.out.println(next);
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
