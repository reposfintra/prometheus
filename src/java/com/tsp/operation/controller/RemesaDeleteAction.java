/*
 * RemesaDeleteAction.java
 *
 * Created on 15 de diciembre de 2004, 03:43 PM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class RemesaDeleteAction extends Action{
    
    /** Creates a new instance of RemesaDeleteAction */
    public RemesaDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String remesa = request.getParameter("remesa");
        String next="/colpapel/anularRemesa.jsp?mensaje=La Remesa "+remesa+" fue anulada con exito!";
        int sw=0;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            //SE VERIFICA PRIMERO SI ALGUNA REMESA TIENE SOLO UNA PLANILLA
            List planillas= model.remesaService.buscarPlanillas(remesa);
            List plaerror=new LinkedList();
            List plaimp=new LinkedList();
            Iterator it=planillas.iterator();
            
            model.remesaService.buscarPlanillaError(remesa);
            if(model.remesaService.getPlasError()!=null)
                plaerror = model.remesaService.getPlasError();
            
            request.setAttribute("plaerror",  plaerror);
            model.tService.crearStatement();
            if(plaerror.size()>=1){
                next="/colpapel/anularError.jsp?remesa="+remesa;
            }
            if(plaimp.size()>=1){
                next="/colpapel/anularErrorImp.jsp?remesa="+remesa;
            }
            if(plaerror.size()==0 && plaimp.size()==0){
                
                
                Remesa rem = new Remesa();
                rem.setNumRem(remesa);
                rem.setDistrito(usuario.getDstrct());
                rem.setUsuario(usuario.getLogin());
                rem.setBase(usuario.getBase());
                model.tService.getSt().addBatch(model.remesaService.anularRemesa(rem));
                
                planillas= model.remesaService.buscarPlanillas(remesa);
                it=planillas.iterator();
                while (it.hasNext()){
                    Planilla pla = (Planilla) it.next();
                    String planilla = pla.getNumpla();
                    
                    //AHORA BUSCO LA LISTA DE REMESAS Q VOY A ANULAR
                    //PARA REASIGNARLE LOS PORCENTAJES
                    double valortotalR = 0;
                    List list = model.planillaService.buscarRemesas(planilla,remesa);
                    Iterator itRem = list.iterator();
                    while(itRem.hasNext()){
                        rem = (Remesa) itRem.next();
                        if(!rem.getNumrem().equals(remesa)){
                            valortotalR  = valortotalR+ rem.getVlrRem();
                        }
                    }
                    System.out.println("Valor total de remesas "+valortotalR);
                    
                    //AHORA LE ASIGNO A CADA UNA EL PROCENTAJE
                   /* itRem = list.iterator();
                    
                    while(itRem.hasNext()){
                        rem = (Remesa) itRem.next();
                        model.remesaService.buscaRemesa(rem.getNumrem());
                        Remesa r = model.remesaService.getRemesa();
                    
                        //System.out.println("Valor de la remesa  "+rem.getNumrem()+" : "+r.getVlrRem());
                    
                        float porcentaje = (r.getVlrRem() /valortotalR) *100;
                    
                        //System.out.println("Porcentaje de la remesa "+rem.getNumrem()+" : "+porcentaje);
                    
                        model.remplaService.buscaRemPla(planilla,r.getNumrem());
                        if(model.remplaService.getRemPla()!=null){
                            RemPla rp = model.remplaService.getRemPla();
                            rp.setPorcent(porcentaje);
                            model.tService.getSt().addBatch(model.remplaService.updateRemPla(rp));
                    
                        }
                    }*/
                    
                    int totalPorcent =0;
                    System.out.println("Lista "+list.size());
                    for(int k=0; k<list.size()-1; k++){
                        rem = (Remesa) list.get(k);
                        model.remesaService.buscaRemesa(rem.getNumrem());
                        Remesa r = model.remesaService.getRemesa();
                        if(!rem.getNumrem().equals(remesa)){
                            
                            double vlrrem = r.getVlrRem();
                            double division = (vlrrem/valortotalR)*100;
                            int porcentaje = (int)division;
                            totalPorcent = totalPorcent + porcentaje;
                            
                            model.remplaService.buscaRemPla(planilla,r.getNumrem());
                            if(model.remplaService.getRemPla()!=null){
                                RemPla rp = model.remplaService.getRemPla();
                                rp.setPorcent(porcentaje);
                                model.tService.getSt().addBatch(model.remplaService.updateRemPla(rp));
                                
                            }
                        }
                    }
                    
                    if(list.size()>0){//ULTIMO ITEM
                        rem = (Remesa) list.get(list.size()-1);
                        model.remesaService.buscaRemesa(rem.getNumrem());
                        Remesa r = model.remesaService.getRemesa();
                        if(!rem.getNumrem().equals(remesa)){
                            int porcentaje = 100-totalPorcent;
                            model.remplaService.buscaRemPla(planilla,r.getNumrem());
                            if(model.remplaService.getRemPla()!=null){
                                RemPla rp = model.remplaService.getRemPla();
                                rp.setPorcent(porcentaje);
                                model.tService.getSt().addBatch(model.remplaService.updateRemPla(rp));
                                
                            }
                        }
                    }
                    
                    
                    //ANULO LAS RELACIONES DE REMPLA
                    model.remplaService.buscaRemPla(planilla,remesa);
                    if(model.remplaService.getRemPla()!=null){
                        
                        RemPla rempla= model.remplaService.getRemPla();
                        model.tService.getSt().addBatch(model.remplaService.anularPlanilla(rempla));
                    }
                    
                    model.tService.getSt().addBatch(model.remesaService.anularMovrem(remesa,usuario));
                }
                model.tService.execute();
                planillas= model.remesaService.buscarPlanillas(remesa);
                it=planillas.iterator();
                while (it.hasNext()){
                    Planilla pla = (Planilla) it.next();
                    String planilla = pla.getNumpla();
                    pla.setNumpla(planilla.toUpperCase());
                    pla.setClientes(model.planillaService.buscarClientes(pla.getNumpla()));
                    model.planillaService.setPlanilla(pla);
                    model.planillaService.actualizarTrafico();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
