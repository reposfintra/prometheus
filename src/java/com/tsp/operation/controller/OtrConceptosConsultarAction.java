/********************************************************************
 *      Nombre Clase.................   AplicacionObtenertAction.java    
 *      Descripci�n..................   Obtiene una lista de registros de la tabla otros_conceptos
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   27.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OtrConceptosConsultarAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public OtrConceptosConsultarAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblotros_conceptos/otrconceptConsultar.jsp";
                String next = "";
                
                try{   
                        Vector vec = model.otros_conceptosSvc.listarConceptos();
                        
                        HttpSession session = request.getSession(true);
                        session.setAttribute("conceptosConsulta", vec);
                        next = com.tsp.util.Util.LLamarVentana(pag, "Consultar Otros Conceptos");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
