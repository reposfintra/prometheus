/********************************************************************
 *      Nombre Clase.................   PagoAlmacenadoraAction.java
 *      Descripci�n..................   Genera cxp docs a partior del archivo de pago a almacenadora
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   29 Enero de 2007
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.UtilFinanzas;
import com.tsp.util.Util;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import java.util.*;
import java.io.*;
import org.apache.log4j.*;
import jxl.*;
import java.text.*;
import com.tsp.operation.model.beans.JXLRead;
import com.tsp.operation.model.beans.JXLWrite;
import com.tsp.operation.model.beans.CXP_Doc;
import java.sql.SQLException;


public class PagoAlmacenadoraAction extends Action{
    
    HttpSession session;
    Usuario usuario;
    String distrito;
    Logger logger = Logger.getLogger(this.getClass());
    
    private String Archivo;
    private String Hoja = "BASE";
    private boolean insertar;//Indica si se puede ingresar la factura o no.
    
    /** Log Migracion **/
    
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    private String ruta;
    private String archivo;
    private Vector lineas;
    /***/
    
    /** Creates a new instance of LoadExcelGeneradoresAction */
    public PagoAlmacenadoraAction() {
    }
    
    /**
     * Se crea el archivo para la escritura del log de migraci�n
     * y se configura los objetos para escribir en �l.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param ruta Ubicaci�n del log dentro del sitio
     * @param archivo Nombre del archivo del log
     **/
    protected void loadLogMigracion(String ruta, String archivo) {
        archivo = ruta + archivo;
        logger.info("?ruta del log de migracion: " + ruta + archivo);
        
        try{
            File archiv = new File(ruta.trim());
            archiv.mkdirs();
            FileOutputStream file = new FileOutputStream(archivo.trim());
            
            fw = new FileWriter(archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
            
            lineas = new Vector();
            
        } catch(java.io.IOException e){
            e.printStackTrace();
            //System.out.println(e.toString());
        }
    }
    
    /**
     * Escribe una cadena de caracteres en el log de migraci�n.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param info Cadena de caracteres a escribir en el log
     **/
    protected void info(String info){
        try{
            this.lineas.add(info);            
        } catch(Exception e){
            e.printStackTrace();
            //System.out.println(e.toString());
        }
    }
    
    /**
     * Escribe una cadena de caracteres en el log de migraci�n.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param info Cadena de caracteres a escribir en el log
     **/
    protected void escribir(){
        try{
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            
            for( int i=0; i<this.lineas.size(); i++ ){
                String info = (String) this.lineas.elementAt(i);
                pntw.println(s.format(d) + " INFO " + info);
            }
            
        } catch(Exception e){
            e.printStackTrace();
            //System.out.println(e.toString());
        }
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next    = "/jsp/cxpagar/pago_almacenadora/pagoAlmacenadora.jsp";
        
        try{
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            distrito = (String)(session.getAttribute("Distrito"));
            String opcion   = request.getParameter("opcion");
            String Mensaje  = "";
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/pago_almacenadora");
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_hhmm");
            
            logger.info("?ruta: " + ruta);
            
            this.loadLogMigracion(  UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin() + "/"), "Log_Pago_Almacenadora_" + s.format(d) + ".txt" );
            
            
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            
            com.tsp.finanzas.presupuesto.model.beans.Upload  upload = new com.tsp.finanzas.presupuesto.model.beans.Upload();
            upload.load(ruta + "/", request);
            
            List lista = upload.getFilesName();
            List archivo = upload.getFiles();
            
            logger.info("?lista nombres: " + lista.size());
            logger.info("?lista archivos: " + archivo.size());
            
            if( lista.size()!=0 ){
                
                String filename = lista.get( 0 ).toString();
                File file = (File)archivo.get( 0 );
                
                int pos = file.getName().lastIndexOf( "." );
                String ext = "";
                if ( pos != -1 ) {
                    ext = file.getName().substring( pos + 1, file.getName().length() );
                }
                
                logger.info("?archivo: " + file.getName());
                
                if( !ext.equalsIgnoreCase( "xls" ) ) {
                    file.delete();
                    next += "?msg=No se puede procesar el archivo debido a que no es un archivo en excel.";
                } else {
                    /* Inicio de la lectura del archivo*/
                    next += "?msg=El arhivo se subi� exitosamente. Favor consulte su directorio de archivos para revisar el log de migraci�n.";
                    this.Archivo = ruta + "/" + file.getName();
                    this.info("Se ha iniciado la migracion desde el arhivo " + file.getName() + "...\n");
                    logger.info("?Archivo: " + this.Archivo);
                    this.leerArchivo();
                }
                
                this.escribir();
                
            } else {
                next += "?msg=No ha subido el archivo seleccionado.";
            }            
            
        } catch( Exception ex ){
            ex.printStackTrace();
            throw new ServletException( "Error en PagoAlmacenadoraAction ...\n" + ex.getMessage() );
        } finally {
            if( pntw!=null )
                pntw.close();
        }
        
        this.dispatchRequest( next );
        
    }
    
    /**
     * Lee el arhivo .xls que ese subio al sitio con la informaci�n de la migraci�n.
     * @autor Ing. Andr�s Maturana De La Cruz
     **/
    public void leerArchivo () {
        
        try {
                        
            int i = 0, // contador
            Fila = 5, // Se empieza a contar desde aqui.. numero de la fila
            
            colRemesa = 0, // numero de la columna de la remesa
            colPlanilla = 18; // numero de la columna de la planilla
            
            JXLRead xls = new JXLRead ( Archivo );
            
            xls.obtenerHoja ( Hoja );
            
            int TotalFilas = xls.numeroFilas ();
            
            logger.info("?TotalFilas: " + TotalFilas);
            
            Vector items = new Vector();
            String almac = "";
            String fra = "";
            
            RepGral obj = new RepGral();
            
            Sheet page = xls.obtenerHoja();
                        
            String nomcol1 = page.getCell( 13, 4 ).getContents();
            logger.info("?nomcol1: " + nomcol1);
            if( nomcol1.trim().length()==0 ){
                this.info("El nombre de la columna 1 de costos est� vacio. No se puede procesar el archivo.");
            }
            String nomcol2 = page.getCell( 14, 4 ).getContents();
            logger.info("?nomcol2: " + nomcol2);
            if( nomcol1.trim().length()==0 ){
                this.info("El nombre de la columna 2 de costos est� vacio. No se puede procesar el archivo.");
            }
            String nomcol3 = page.getCell( 15, 4 ).getContents();
            logger.info("?nomcol3: " + nomcol3);
            if( nomcol3.trim().length()==0 ){
                this.info("El nombre de la columna 3 de costos est� vacio. No se puede procesar el archivo.");
            }
            String nomcol4 = page.getCell( 16, 4 ).getContents();
            logger.info("?nomcol4: " + nomcol4);
            if( nomcol1.trim().length()==0 ){
                this.info("El nombre de la columna 4 de costos est� vacio. No se puede procesar el archivo.");
            }
            
            this.setInsertar(true);
            
            for ( i = Fila; i < TotalFilas ; i++ ) {
                
                obj = null;
                obj = new RepGral();
                
                obj.setFila( i + 1 );
                obj.setAlmacenadora( page.getCell( 1, i ).getContents() );
                obj.setFactura( page.getCell( 2, i ).getContents() );
                obj.setOt( page.getCell( 5, i ).getContents() );
                obj.setOc( page.getCell( 6, i ).getContents() );
                obj.setPlaca( page.getCell( 8, i ).getContents() );
                obj.setNomcli_fto( page.getCell( 9, i ).getContents() );
                
                obj.setNom_costo_1( nomcol1 );
                obj.setNom_costo_2( nomcol2 );
                obj.setNom_costo_3( nomcol3 );
                obj.setNom_costo_4( nomcol4 );
                
                obj.setTipo_costo_1(  page.getCell( 23, i ).getContents() );
                obj.setTipo_costo_2(  page.getCell( 24, i ).getContents() );
                obj.setTipo_costo_3(  page.getCell( 25, i ).getContents() );
                obj.setTipo_costo_4(  page.getCell( 26, i ).getContents() );
                
                obj.setCodigo_agencia_1( page.getCell( 27, i ).getContents() );
                obj.setCodigo_agencia_2( page.getCell( 28, i ).getContents() );
                obj.setCodigo_agencia_3( page.getCell( 29, i ).getContents() );
                obj.setCodigo_agencia_4( page.getCell( 30, i ).getContents() );
                
                /*logger.info("?Item: " + page.getCell( 0, i ).getContents());
                logger.info("?cargue: " + obj.getVlr_cargue());
                logger.info("?descarque: " + obj.getVlr_descargue());
                logger.info("?bodegaje: " + obj.getVlr_bodegaje());
                logger.info("?inspeccion: " + obj.getVlr_inspeccion());
                logger.info("**********************************************");*/
                
                if( i == Fila ){
                    almac = obj.getAlmacenadora();
                    fra = obj.getFactura();
                }
                
                if( obj.getFactura().trim().length()!=0 ){
                    
                    if( obj.getAlmacenadora().equals(almac) && obj.getFactura().equals(fra) ){
                        
                        try {
                            obj.setVlr_cargue( Double.parseDouble( page.getCell( 13, i ).getContents().trim().length()==0 ? "0" : page.getCell( 13, i ).getContents()  ) );
                        } catch ( NumberFormatException nfe ){
                            this.info("El valor de la operaci�n " + obj.getNom_costo_1() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                            this.setInsertar(false);
                        }
                        
                        try {
                            obj.setVlr_descargue( Double.parseDouble( page.getCell( 14, i ).getContents().trim().length()==0 ? "0" : page.getCell( 14, i ).getContents() ) );
                        } catch ( NumberFormatException nfe ){
                            this.info("El valor de la operaci�n " + obj.getNom_costo_2() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                            this.setInsertar(false);
                        }
                        
                        try {
                            obj.setVlr_bodegaje( Double.parseDouble( page.getCell( 15, i ).getContents().trim().length()==0 ? "0" : page.getCell( 15, i ).getContents() ) );
                        } catch ( NumberFormatException nfe ){
                            this.info("El valor de la operaci�n " + obj.getNom_costo_3() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                            this.setInsertar(false);
                        }
                        
                        try {
                            obj.setVlr_inspeccion( Double.parseDouble( page.getCell( 16, i ).getContents().trim().length()==0 ? "0" : page.getCell( 16, i ).getContents() ) );
                        } catch ( NumberFormatException nfe ){
                            this.info("El valor de la operaci�n " + obj.getNom_costo_4() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                            this.setInsertar(false);
                        }
                        items.add(obj);
                    } else {
                        /* Ingresamos la fra */
                        logger.info("Se va a ingresar una factura para la almacenadora " + almac + " Nro. " + fra);
                        if( fra.trim().length()!=0 ){
                            try{
                                CXP_Doc doc = this.crearCxP_Doc(items, almac);
                                if( this.isInsertar() ){
                                    this.sqlInsertarCxP(doc);
                                } else {
                                    this.info("... No se pudo ingresar la factura " + doc.getDocumento() + " del proveedor " + doc.getProveedor());
                                }
                                this.setInsertar(true);
                            } catch ( NullPointerException exce ){
                                exce.printStackTrace();
                            }
                            items = null;
                            items = new Vector();
                            almac = obj.getAlmacenadora();
                            fra = obj.getFactura();
                            
                            try {
                                obj.setVlr_cargue( Double.parseDouble( page.getCell( 13, i ).getContents().trim().length()==0 ? "0" : page.getCell( 13, i ).getContents()  ) );
                            } catch ( NumberFormatException nfe ){
                                this.info("El valor de la operaci�n " + obj.getNom_costo_1() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                                this.setInsertar(false);
                            }
                            
                            try {
                                obj.setVlr_descargue( Double.parseDouble( page.getCell( 14, i ).getContents().trim().length()==0 ? "0" : page.getCell( 14, i ).getContents() ) );
                            } catch ( NumberFormatException nfe ){
                                this.info("El valor de la operaci�n " + obj.getNom_costo_2() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                                this.setInsertar(false);
                            }
                            
                            try {
                                obj.setVlr_bodegaje( Double.parseDouble( page.getCell( 15, i ).getContents().trim().length()==0 ? "0" : page.getCell( 15, i ).getContents() ) );
                            } catch ( NumberFormatException nfe ){
                                this.info("El valor de la operaci�n " + obj.getNom_costo_3() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                                this.setInsertar(false);
                            }
                            
                            try {
                                obj.setVlr_inspeccion( Double.parseDouble( page.getCell( 16, i ).getContents().trim().length()==0 ? "0" : page.getCell( 16, i ).getContents() ) );
                            } catch ( NumberFormatException nfe ){
                                this.info("El valor de la operaci�n " + obj.getNom_costo_4() + " en la l�nea " + (i + 1) + " no tiene el formato num�rico apropiado. Factura: " + obj.getFactura());
                                this.setInsertar(false);
                            }
                            items.add(obj);
                        } else {
                            logger.info("Se encontr� una celda vac�a.");
                        }
                    }
                }
                
                //logger.info("?almacenadora: " + obj.getAlmacenadora());
                //logger.info("?factura: " + obj.getFactura());
                
            }
            
            /* Ingresamos la fra */
            logger.info("Se va a ingresar una factura para la almacenadora " + almac + " Nro. " + fra);
            if( fra.trim().length()!=0 && obj.getFactura().trim().length()!=0 ){
                try{
                    CXP_Doc doc = this.crearCxP_Doc(items, almac);
                    if( this.isInsertar() ){
                        this.sqlInsertarCxP(doc);
                    } else {
                        this.info("... No se pudo ingresar la factura " + doc.getDocumento() + " del proveedor " + doc.getProveedor());
                    }
                } catch ( NullPointerException exce ){
                    exce.printStackTrace();
                }
            } else {
                logger.info("Se encontr� una celda vac�a.");
            }
            
            
        } catch ( SQLException exc ) {
            
            exc.printStackTrace();
            
        } catch ( ArrayIndexOutOfBoundsException ep ) {
            
            ep.printStackTrace();
            this.info("Verifique el n�mero de celdas a lo ancho de la hoja");
            
        } catch ( Exception ex ) {
            
            ex.printStackTrace();
            this.info("No se pudo leer el archivo. Verifique el nombre de la hoja y/o la estructura del archivo. ERROR: " + ex.getMessage());
            
        }
        
    }
    
    private CXP_Doc crearCxP_Doc( Vector items, String almac ) throws SQLException {
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        String cuenta;
        CXP_Doc doc = new CXP_Doc();
        
        try {
            RepGral obj = (RepGral) items.elementAt(0);
            
            String aprobador = model.tablaGenService.aprobadorCXPAuto(almac);
            if( aprobador == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se encontro el aprobador para la almacenadora: " + almac);
                this.setInsertar(false);
            }
        
            
            String nit = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "NIT");
            if( nit == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se encontro en nit de proveedor.");
                this.setInsertar(false);
            }
                        
            Proveedor prov = model.proveedorService.obtenerProveedor(nit, distrito);
            
            if( prov == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El proveedor no registra en el archivo de proveedores: " + nit);
                this.setInsertar(false);
            }
                        
            model.servicioBanco.obtenerBancoSucursal(distrito, prov.getC_branch_code(), prov.getC_bank_account());
            Banco ban = model.servicioBanco.getBannco();
            
            if( ban == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se encuentra la moneda del banco.");
                this.setInsertar(false);
            }
            
            String moneda_fto = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "MONEDA");
            if( moneda_fto == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se encontro la moneda del formato.");
                this.setInsertar(false);
            }
                        
            String agc = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "AGENCIA");
            /*if( agc == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se encontro la agencia. ");
                throw new SQLException();
            }*/
            
            String unidad = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "UNIDAD");
            /*if( unidad == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se encontro la unidad de negocios.");
                throw new SQLException();
            }*/
            
            String cod_abc = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "ABC");
            if( cod_abc == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo ABC no se ha definido para la almacenadora " + almac + ".");
                this.setInsertar(false);
            }            
            
            cuenta = "C" + agc + unidad;
            
            String mda_cia = (String) session.getAttribute("Moneda");
            Tasa tasa = model.tasaService.buscarValorTasa( mda_cia, moneda_fto, ban.getMoneda(), s.format(d));
            Tasa tasa_cia = model.tasaService.buscarValorTasa( mda_cia, ban.getMoneda(), mda_cia, s.format(d));
            
            if( tasa == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se ha definido la tasa de conversi�n de " + moneda_fto + " a " + ban.getMoneda() + ".");
                this.setInsertar(false);
            }
            
            if( tasa_cia == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se ha definido la tasa de conversi�n de " + ban.getMoneda() + " a " + mda_cia + ".");
                this.setInsertar(false);
            }
            
            //this.info("Tasa 1 " + moneda_fto + " - " + ban.getMoneda() + " = " + tasa.getValor_tasa());
            //this.info("Tasa 2 " + ban.getMoneda() + " - " + mda_cia + " = " + tasa_cia.getValor_tasa());
            
            String incr_str = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "PORINC");
            int incr = Integer.parseInt(incr_str.trim().length()==0 ? "0" : incr_str);
            
            String cod_iva = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "IVA");
            String cod_riva = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "RIVA");
            String cod_rfte = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "RFTE");
            String cod_rica = model.tablaGenService.obtenerLeyendaDato("PPTERCERO", almac, "RICA");
            
            logger.info("?IVA: " + cod_iva);
            logger.info("?RIVA: " + cod_riva);
            logger.info("?RFTE: " + cod_rfte);
            logger.info("?RICA: " + cod_rica);
            
            Tipo_impuesto iva = null;
            Tipo_impuesto riva = null;
            Tipo_impuesto rfte = null;
            Tipo_impuesto rica = null;
            
            if( !cod_iva.equals("") ){
                Tipo_impuesto ivax = model.TimpuestoSvc.buscarImpuestoxCodigo(distrito, cod_iva);
                if( ivax == null ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El codigo de impuesto " + cod_iva + " no registra.");
                    this.setInsertar(false);
                } else {
                    iva = ivax;
                }
            }
            
            if( !cod_riva.equals("") ){
                Tipo_impuesto rivax = model.TimpuestoSvc.buscarImpuestoxCodigo(distrito, cod_riva);
                if( rivax == null ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El codigo de impuesto " + cod_riva + " no registra.");
                    this.setInsertar(false);
                } else {
                    riva = rivax;
                }
            }
            
            if( !cod_iva.equals("") ){
                Tipo_impuesto rftex = model.TimpuestoSvc.buscarImpuestoxCodigo(distrito, cod_rfte);
                if( rftex == null ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El codigo de impuesto " + cod_rfte + " no registra.");
                    this.setInsertar(false);
                } else {
                    rfte = rftex;
                }
            }
            
            if( !cod_rica.equals("") ){
                Tipo_impuesto ricax = model.TimpuestoSvc.buscarImpuestoxCodigo(distrito, cod_rica);
                if( ricax == null ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El codigo de impuesto " + cod_rica + " no registra.");
                    this.setInsertar(false);
                } else {
                    rica = ricax;
                }
            }
            
            doc.setProveedor(nit);
            doc.setTipo_documento("010");
            doc.setDocumento(obj.getFactura());
            doc.setDescripcion( "Factura " + obj.getAlmacenadora() + " Nro. " + obj.getFactura());
            doc.setAprobador(aprobador);
            doc.setUsuario_aprobacion("");
            doc.setHandle_code(prov.getHandle_code());
            doc.setId_mims(prov.getC_idMims());
            doc.setBanco(prov.getC_branch_code());
            doc.setSucursal(prov.getC_bank_account());
            doc.setMoneda(ban.getMoneda());            
            doc.setAgencia(ban.getCodigo_Agencia());
            doc.setMoneda_banco(ban.getMoneda());
            doc.setClase_documento("4");
            doc.setCreation_user(usuario.getLogin());            
            doc.setDstrct(distrito);
            doc.setTasa(tasa_cia.getValor_tasa());
            doc.setPlazo(prov.getPlazo());
            
            CXP_Doc doc_aux = this.obtenerItems_CxP(doc, items, cuenta, tasa, tasa_cia, incr, cod_abc, iva, riva, rfte, rica);
            doc = new CXP_Doc();
            doc = doc_aux;
            
            
        } catch ( SQLException exc ) {
            exc.printStackTrace();
            throw new SQLException();
            
        } catch ( NullPointerException exce ) {
            exce.printStackTrace();
            throw new NullPointerException();
            
        } catch ( Exception e ) {
            e.printStackTrace();
            
        }
        
        return doc;
    }
    
    public CXP_Doc obtenerItems_CxP(
            CXP_Doc doc, 
            Vector items, 
            String codcuenta, 
            Tasa tasa, 
            Tasa tasa_cia, 
            int incr, 
            String abc, 
            Tipo_impuesto iva, 
            Tipo_impuesto riva, 
            Tipo_impuesto rfte, 
            Tipo_impuesto rica) throws NullPointerException, Exception{
                
        Vector cxp_items = new Vector();
        Vector cxp_impitems = new Vector();
        Vector cxp_imp = new Vector();
        double vlr_fra = 0;
        double vlr_fra_me = 0;
        double ttl_iva = 0;
        double ttl_riva = 0;
        double ttl_rfte = 0;
        double ttl_rica = 0;
        double ttl_iva_me = 0;
        double ttl_riva_me = 0;
        double ttl_rfte_me = 0;
        double ttl_rica_me = 0;
        String mda = (String) session.getAttribute("Moneda");
        logger.info("ME: " + tasa.getMoneda2());
        logger.info("ML: " + tasa.getMoneda1());
        
        logger.info("?items fra " + doc.getDocumento() + " : " + items.size());
        
        for(int i=0; i<items.size(); i++){
            RepGral obj = (RepGral) items.elementAt(i);
            
            
            if( obj.getOt().equals("") ) {
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El n�mero de la remesa no registra en la fila " + obj.getFila() + " del formato de migraci�n");
                this.setInsertar(false);
            }
            
            if( !model.cxpDocService.existeOT(obj.getOt()) ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La Remesa " + obj.getOt() + " no existe. Fila " + obj.getFila() + " del formato de migraci�n.");
                this.setInsertar(false);
            }
            
            String cliente_rem = model.RemesaSvc.obtenerClienteRemesa(obj.getOt());
            if( cliente_rem == null ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No se encontro el cliente de la remesa: " + obj.getOt() + "en la fila "+ obj.getFila() + " del formato de migraci�n");
                this.setInsertar(false);
            }
            
            if( obj.getOc().equals("") ) {
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El n�mero de la planilla no registra en la fila " + obj.getFila() + " del formato de migraci�n");
                this.setInsertar(false);
            }
            
            if( !model.cxpDocService.existeOC(obj.getOc()) ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La Planilla " + obj.getOc() + " no existe. Fila " + obj.getFila() + " del formato de migraci�n.");
                this.setInsertar(false);
            }
            
            if( !model.cxpDocService.relacionOtOc(obj.getOc(), obj.getOt()) ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La relacion entre la Planilla " + obj.getOc() + " y la Remesa " + obj.getOt() + " no existe. Fila " + obj.getFila() + " del formato de migraci�n.");
                this.setInsertar(false);
            }
            
            if( obj.getPlaca().equals("") ) {
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El n�mero de la placa no registra en la fila " + obj.getFila() + " del formato de migraci�n");
                this.setInsertar(false);
            }
            
            if( !model.cxpDocService.existePlaca(obj.getPlaca()) ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La Placa " + obj.getPlaca() + " no existe. Fila " + obj.getFila() + " del formato de migraci�n.");
                this.setInsertar(false);
            }
            
            if( !model.cxpDocService.relacionPlacaOc(obj.getOc(), obj.getPlaca()) ){
                this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La relacion entre la Planilla " + obj.getOc() + " y la Placa " + obj.getPlaca() + " no existe. Fila " + obj.getFila() + " del formato de migraci�n.");
                this.setInsertar(false);
            }
            
            String cuenta = null;
            cuenta = codcuenta + cliente_rem;
            
            if( obj.getVlr_cargue() != 0 ){
                CXPItemDoc item = new CXPItemDoc();
                
                item.setDstrct(doc.getDstrct());
                item.setProveedor(doc.getProveedor());
                item.setTipo_documento(doc.getTipo_documento());
                item.setDocumento(doc.getDocumento());
                //item.setCodigo_cuenta(cuenta);
                item.setPlanilla(obj.getOc());
                item.setPlanilla(obj.getOc());
                item.setCreation_user(usuario.getLogin());
                //item.setAuxiliar(doc.getProveedor());
                item.setCodigo_abc(abc);
                item.setItem(this.formatearItem(String.valueOf(cxp_items.size() + 1)));
                logger.info("? Factura: " + doc.getDocumento() + ": " + item.getItem());
                double valor = obj.getVlr_cargue() + (obj.getVlr_cargue()*(incr/100.0));
                logger.info("? Vlr: " + obj.getVlr_cargue() + " - Incremento: " + incr + " - Ttl Incr: " + (obj.getVlr_cargue()*(incr/100.0)) + " - Ttl Vlr: " + valor );
                item.setVlr_me(this.redondear(valor*tasa.getValor_tasa(), doc.getMoneda()));
                item.setVlr(this.redondear(item.getVlr_me()*tasa_cia.getValor_tasa(), mda));
                item.setDescripcion(obj.getNom_costo_1().toUpperCase() + " " + obj.getNomcli_fto() + " " + obj.getPlaca() + " " + obj.getOc()+ " " + obj.getOt());
                vlr_fra += item.getVlr();
                vlr_fra_me += item.getVlr_me();
                //item.setCodigo_cuenta(cuenta + "9202");
                
                TablaGen treemb1 = model.tablaGenService.obtenerTablaGenByDesc("REEMBOL1", obj.getNom_costo_1());
                logger.info("?treemb1: " + treemb1);
                if( treemb1==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL1 para " + obj.getNom_costo_1().toUpperCase());
                    this.setInsertar(false);
                }
                TablaGen treemb2 = model.tablaGenService.obtenerTablaGen("REEMBOL2", treemb1.getTable_code() + obj.getTipo_costo_1());
                if( treemb2==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL2 para " + obj.getNom_costo_1().toUpperCase());
                    this.setInsertar(false);
                }
                
                logger.info("?Fra." + obj.getFactura() + " - Tipo costo 1: " + obj.getTipo_costo_1());
                
                if( obj.getTipo_costo_1().equalsIgnoreCase("P") ){
                    
                    String nitpro = model.cxpDocService.propietarioPlacaPlanilla( obj.getOc() );
                    
                    if( nitpro == null  ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El propietario de la placa asociada a la planilla " + obj.getOc() + "" +
                            " no registra en el Sistema. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitpro);
                    
                } else if ( obj.getTipo_costo_1().equalsIgnoreCase("C") ){
                    
                    model.remesaService.buscaRemesa( obj.getOt() );
                    String nitcli = model.cxpDocService.getNitClienteRemesa( obj.getOt() );
                    
                    if( nitcli == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El nit del cliente de la remesa " + obj.getOt() + "" +
                            " No registra. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitcli);
                    
                } else {
                    
                    String pcuenta = model.cxpDocService.getParteCuentaRemesa( obj.getOt() );
                    
                    if( pcuenta == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La unidad de negocios no registra para la remesa " + obj.getOt() + "" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_1().length()== 0 ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no registra" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_1());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_1().length() != 0 && obj.getCodigo_agencia_1().length() != 2){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no contiene la longitud correcta" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_1());
                        this.setInsertar(false);                        
                    }
                    
                    if( !model.cxpDocService.existeAgenciaContable(obj.getCodigo_agencia_1()) ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no existe" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_1());
                        this.setInsertar(false);                        
                    }
                    
                    item.setCodigo_cuenta("C" + obj.getCodigo_agencia_1() + pcuenta + treemb2.getDescripcion());
                    item.setAuxiliar("");
                    
                }
                
                if( !model.cxpDocService.existeCuentaContable(item.getDstrct(), item.getCodigo_cuenta()) ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La cuenta contable " + item.getCodigo_cuenta() + "" +
                    " No registra o est� anulada. Fila : " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                    this.setInsertar(false);
                }
                
                cxp_items.add(item);
                
                /* Impuestos del item */
                
                if( iva!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(iva.getCodigo_impuesto());
                    imp.setPorcent_impuesto(iva.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_iva += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_iva_me += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    if( riva!=null ){
                        CXPImpItem imp2 = new CXPImpItem();
                        
                        imp2.setDstrct(doc.getDstrct());
                        imp2.setProveedor(doc.getProveedor());
                        imp2.setTipo_documento(doc.getTipo_documento());
                        imp2.setDocumento(doc.getDocumento());
                        imp2.setItem(item.getItem());
                        imp2.setCod_impuesto(riva.getCodigo_impuesto());
                        imp2.setPorcent_impuesto(riva.getPorcentaje1());
                        imp2.setVlr_total_impuesto(this.redondear(imp.getVlr_total_impuesto()*(imp2.getPorcent_impuesto()/100.0), mda));
                        imp2.setVlr_total_impuesto_me(this.redondear(imp.getVlr_total_impuesto_me()*(imp2.getPorcent_impuesto()/100.0), doc.getMoneda()));
                        imp2.setCreation_user(usuario.getLogin());
                        imp2.setPorcent_impuesto(this.abs(imp2.getPorcent_impuesto()));
                        
                        cxp_impitems.add(imp2);
                        
                        vlr_fra += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        vlr_fra_me +=  this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                        
                        ttl_riva += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        ttl_riva_me += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                    }
                }
                
                if( rfte!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rfte.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rfte.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rfte += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rfte_me += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
                
                if( rica!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rica.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rica.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rica += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rica_me += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
            }
            
            if( obj.getVlr_descargue() != 0 ){
                CXPItemDoc item = new CXPItemDoc();
                
                item.setDstrct(doc.getDstrct());
                item.setProveedor(doc.getProveedor());
                item.setTipo_documento(doc.getTipo_documento());
                item.setDocumento(doc.getDocumento());
                //item.setCodigo_cuenta(cuenta);
                item.setPlanilla(obj.getOc());
                item.setPlanilla(obj.getOc());
                item.setCreation_user(usuario.getLogin());
                //item.setAuxiliar(doc.getProveedor());
                item.setCodigo_abc(abc);
                item.setItem(this.formatearItem(String.valueOf(cxp_items.size() + 1)));
                //logger.info("? Factura: " + doc.getDocumento() + ": " + item.getItem());
                double valor = obj.getVlr_descargue() + (obj.getVlr_descargue()*(incr/100.0));
                item.setVlr_me(this.redondear(valor*tasa.getValor_tasa(), doc.getMoneda()));
                item.setVlr(this.redondear(item.getVlr_me()*tasa_cia.getValor_tasa(), mda));
                item.setDescripcion(obj.getNom_costo_2().toUpperCase() + " " + obj.getNomcli_fto() + " " + obj.getPlaca() + " " + obj.getOc()+ " " + obj.getOt());
                vlr_fra += item.getVlr();
                vlr_fra_me += item.getVlr_me();
                //item.setCodigo_cuenta(cuenta + "9202");
                
                TablaGen treemb1 = model.tablaGenService.obtenerTablaGenByDesc("REEMBOL1", obj.getNom_costo_2());
                if( treemb1==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL1 para " + obj.getNom_costo_2().toUpperCase());
                    this.setInsertar(false);
                }
                TablaGen treemb2 = model.tablaGenService.obtenerTablaGen("REEMBOL2", treemb1.getTable_code() + obj.getTipo_costo_2());
                if( treemb2==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL2 para " + obj.getNom_costo_2().toUpperCase());
                    this.setInsertar(false);
                }
                
                if( obj.getTipo_costo_2().equalsIgnoreCase("P") ){
                    
                    String nitpro = model.cxpDocService.propietarioPlacaPlanilla( obj.getOc() );
                    
                    if( nitpro == null  ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El propietario de la placa asociada a la planilla " + obj.getOc() + "" +
                            " no registra en el Sistema. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitpro);
                    
                } else if ( obj.getTipo_costo_2().equalsIgnoreCase("C") ){
                    
                    model.remesaService.buscaRemesa( obj.getOt() );
                    String nitcli = model.cxpDocService.getNitClienteRemesa( obj.getOt() );
                    
                    if( nitcli == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El nit del cliente de la remesa " + obj.getOt() + "" +
                            " No registra. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitcli);
                    
                } else {
                    
                    String pcuenta = model.cxpDocService.getParteCuentaRemesa( obj.getOt() );
                    
                    if( pcuenta == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La unidad de negocios no registra para la remesa " + obj.getOt() + "" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_2().length()== 0 ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no registra" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_2().length() != 0 && obj.getCodigo_agencia_2().length() != 2){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no contiene la longitud correcta" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_2());
                        this.setInsertar(false);                        
                    }
                    
                    if( !model.cxpDocService.existeAgenciaContable(obj.getCodigo_agencia_2()) ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no existe" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_2());
                        this.setInsertar(false);                        
                    }
                    
                    item.setCodigo_cuenta("C" + obj.getCodigo_agencia_2() + pcuenta + treemb2.getDescripcion());
                    item.setAuxiliar("");
                    
                }
                
                if( !model.cxpDocService.existeCuentaContable(item.getDstrct(), item.getCodigo_cuenta()) ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La cuenta contable " + item.getCodigo_cuenta() + "" +
                    " No registra o est� anulada. Fila : " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                    this.setInsertar(false);
                }
                
                cxp_items.add(item);
                
                /* Impuestos del item */
                
                if( iva!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(iva.getCodigo_impuesto());
                    imp.setPorcent_impuesto(iva.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_iva += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_iva_me += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    if( riva!=null ){
                        CXPImpItem imp2 = new CXPImpItem();
                        
                        imp2.setDstrct(doc.getDstrct());
                        imp2.setProveedor(doc.getProveedor());
                        imp2.setTipo_documento(doc.getTipo_documento());
                        imp2.setDocumento(doc.getDocumento());
                        imp2.setItem(item.getItem());
                        imp2.setCod_impuesto(riva.getCodigo_impuesto());
                        imp2.setPorcent_impuesto(riva.getPorcentaje1());
                        imp2.setVlr_total_impuesto(this.redondear(imp.getVlr_total_impuesto()*(imp2.getPorcent_impuesto()/100.0), mda));
                        imp2.setVlr_total_impuesto_me(this.redondear(imp.getVlr_total_impuesto_me()*(imp2.getPorcent_impuesto()/100.0), doc.getMoneda()));
                        imp2.setCreation_user(usuario.getLogin());
                        imp2.setPorcent_impuesto(this.abs(imp2.getPorcent_impuesto()));
                        
                        cxp_impitems.add(imp2);
                        
                        vlr_fra += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        vlr_fra_me +=  this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                        
                        ttl_riva += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        ttl_riva_me += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                    }
                }
                
                if( rfte!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rfte.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rfte.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rfte += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rfte_me += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
                
                if( rica!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rica.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rica.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rica += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rica_me += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
            }
            
            if( obj.getVlr_bodegaje() != 0 ){
                CXPItemDoc item = new CXPItemDoc();
                
                item.setDstrct(doc.getDstrct());
                item.setProveedor(doc.getProveedor());
                item.setTipo_documento(doc.getTipo_documento());
                item.setDocumento(doc.getDocumento());
                //item.setCodigo_cuenta(cuenta);
                item.setPlanilla(obj.getOc());
                item.setPlanilla(obj.getOc());
                item.setCreation_user(usuario.getLogin());
                //item.setAuxiliar(doc.getProveedor());
                item.setCodigo_abc(abc);
                item.setItem(this.formatearItem(String.valueOf(cxp_items.size() + 1)));
                //logger.info("? Factura: " + doc.getDocumento() + ": " + item.getItem());
                double valor = obj.getVlr_bodegaje() + (obj.getVlr_bodegaje()*(incr/100.0));
                item.setVlr_me(this.redondear(valor*tasa.getValor_tasa(), doc.getMoneda()));
                item.setVlr(this.redondear(item.getVlr_me()*tasa_cia.getValor_tasa(), mda));
                item.setDescripcion(obj.getNom_costo_3().toUpperCase() + " " + obj.getNomcli_fto() + " " + obj.getPlaca() + " " + obj.getOc()+ " " + obj.getOt());
                vlr_fra += item.getVlr();
                vlr_fra_me += item.getVlr_me();
                //item.setCodigo_cuenta(cuenta + "9222");
                
                TablaGen treemb1 = model.tablaGenService.obtenerTablaGenByDesc("REEMBOL1", obj.getNom_costo_3());
                if( treemb1==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL1 para " + obj.getNom_costo_3().toUpperCase());
                    this.setInsertar(false);
                }
                TablaGen treemb2 = model.tablaGenService.obtenerTablaGen("REEMBOL2", treemb1.getTable_code() + obj.getTipo_costo_3());
                if( treemb2==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL2 para " + obj.getNom_costo_3().toUpperCase());
                    this.setInsertar(false);
                }
                
                if( obj.getTipo_costo_3().equalsIgnoreCase("P") ){
                    
                    String nitpro = model.cxpDocService.propietarioPlacaPlanilla( obj.getOc() );
                    
                    if( nitpro == null  ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El propietario de la placa asociada a la planilla " + obj.getOc() + "" +
                            " no registra en el Sistema. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitpro);
                    
                } else if ( obj.getTipo_costo_3().equalsIgnoreCase("C") ){
                    
                    model.remesaService.buscaRemesa( obj.getOt() );
                    String nitcli = model.cxpDocService.getNitClienteRemesa( obj.getOt() );
                    
                    if( nitcli == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El nit del cliente de la remesa " + obj.getOt() + "" +
                            " No registra. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitcli);
                    
                } else {
                    
                    String pcuenta = model.cxpDocService.getParteCuentaRemesa( obj.getOt() );
                    
                    if( pcuenta == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La unidad de negocios no registra para la remesa " + obj.getOt() + "" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_3().length()== 0 ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no registra" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_3().length() != 0 && obj.getCodigo_agencia_3().length() != 2){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no contiene la longitud correcta" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_3());
                        this.setInsertar(false);                        
                    }
                    
                    if( !model.cxpDocService.existeAgenciaContable(obj.getCodigo_agencia_3()) ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no existe" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_3());
                        this.setInsertar(false);                        
                    }
                    
                    item.setCodigo_cuenta("C" + obj.getCodigo_agencia_3() + pcuenta + treemb2.getDescripcion());
                    item.setAuxiliar("");
                    
                }
                
                if( !model.cxpDocService.existeCuentaContable(item.getDstrct(), item.getCodigo_cuenta()) ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La cuenta contable " + item.getCodigo_cuenta() + "" +
                    " No registra o est� anulada. Fila : " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                    this.setInsertar(false);
                }
                
                cxp_items.add(item);
                
                /* Impuestos del item */
                
                if( iva!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(iva.getCodigo_impuesto());
                    imp.setPorcent_impuesto(iva.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_iva += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_iva_me += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    if( riva!=null ){
                        CXPImpItem imp2 = new CXPImpItem();
                        
                        imp2.setDstrct(doc.getDstrct());
                        imp2.setProveedor(doc.getProveedor());
                        imp2.setTipo_documento(doc.getTipo_documento());
                        imp2.setDocumento(doc.getDocumento());
                        imp2.setItem(item.getItem());
                        imp2.setCod_impuesto(riva.getCodigo_impuesto());
                        imp2.setPorcent_impuesto(riva.getPorcentaje1());
                        imp2.setVlr_total_impuesto(this.redondear(imp.getVlr_total_impuesto()*(imp2.getPorcent_impuesto()/100.0), mda));
                        imp2.setVlr_total_impuesto_me(this.redondear(imp.getVlr_total_impuesto_me()*(imp2.getPorcent_impuesto()/100.0), doc.getMoneda()));
                        imp2.setCreation_user(usuario.getLogin());
                        imp2.setPorcent_impuesto(this.abs(imp2.getPorcent_impuesto()));
                        
                        cxp_impitems.add(imp2);
                        
                        vlr_fra += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        vlr_fra_me +=  this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                        
                        ttl_riva += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        ttl_riva_me += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                    }
                }
                
                if( rfte!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rfte.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rfte.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rfte += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rfte_me += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
                
                if( rica!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rica.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rica.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rica += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rica_me += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
            }
            
            if( obj.getVlr_inspeccion() != 0 ){
                CXPItemDoc item = new CXPItemDoc();
                
                item.setDstrct(doc.getDstrct());
                item.setProveedor(doc.getProveedor());
                item.setTipo_documento(doc.getTipo_documento());
                item.setDocumento(doc.getDocumento());
                //item.setCodigo_cuenta(cuenta);
                item.setPlanilla(obj.getOc());
                item.setPlanilla(obj.getOc());
                item.setCreation_user(usuario.getLogin());
                //item.setAuxiliar(doc.getProveedor());
                item.setCodigo_abc(abc);
                item.setItem(this.formatearItem(String.valueOf(cxp_items.size() + 1)));
                //logger.info("? Factura: " + doc.getDocumento() + ": " + item.getItem());
                double valor = obj.getVlr_inspeccion() + (obj.getVlr_inspeccion()*(incr/100.0));
                item.setVlr_me(this.redondear(valor*tasa.getValor_tasa(), doc.getMoneda()));
                item.setVlr(this.redondear(item.getVlr_me()*tasa_cia.getValor_tasa(), mda));
                item.setDescripcion(obj.getNom_costo_4().toUpperCase() + " " + obj.getNomcli_fto() + " " + obj.getPlaca() + " " + obj.getOc()+ " " + obj.getOt());
                vlr_fra += item.getVlr();
                vlr_fra_me += item.getVlr_me();
                //item.setCodigo_cuenta(cuenta + "9218");
                
                TablaGen treemb1 = model.tablaGenService.obtenerTablaGenByDesc("REEMBOL1", obj.getNom_costo_4());
                if( treemb1==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL1 para " + obj.getNom_costo_4().toUpperCase());
                    this.setInsertar(false);
                }
                TablaGen treemb2 = model.tablaGenService.obtenerTablaGen("REEMBOL2", treemb1.getTable_code() + obj.getTipo_costo_4());
                if( treemb2==null ) {
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": No est� definida la Tabla General REEMBOL2 para " + obj.getNom_costo_4().toUpperCase());
                    this.setInsertar(false);
                }
                
                if( obj.getTipo_costo_4().equalsIgnoreCase("P") ){
                    
                    String nitpro = model.cxpDocService.propietarioPlacaPlanilla( obj.getOc() );
                    
                    if( nitpro == null  ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El propietario de la placa asociada a la planilla " + obj.getOc() + "" +
                            " no registra en el Sistema. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitpro);
                    
                } else if ( obj.getTipo_costo_4().equalsIgnoreCase("C") ){
                    
                    model.remesaService.buscaRemesa( obj.getOt() );
                    String nitcli = model.cxpDocService.getNitClienteRemesa( obj.getOt() );
                    
                    if( nitcli == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El nit del cliente de la remesa " + obj.getOt() + "" +
                            " No registra. No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    item.setCodigo_cuenta(treemb2.getDescripcion().trim());
                    item.setAuxiliar(treemb2.getReferencia() + "-" + nitcli);
                    
                } else {
                    
                    String pcuenta = model.cxpDocService.getParteCuentaRemesa( obj.getOt() );
                    
                    if( pcuenta == null ) {
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La unidad de negocios no registra para la remesa " + obj.getOt() + "" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_4().length()== 0 ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no registra" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                        this.setInsertar(false);
                    }
                    
                    if( obj.getCodigo_agencia_4().length() != 0 && obj.getCodigo_agencia_4().length() != 2){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no contiene la longitud correcta" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_4());
                        this.setInsertar(false);                        
                    }
                    
                    if( !model.cxpDocService.existeAgenciaContable(obj.getCodigo_agencia_4()) ){
                        this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": El c�digo de la agencia contable no existe" +
                            ". No se puede generar la cuenta para el item. Fila " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion() + ". Operaci�n: " + obj.getNom_costo_4());
                        this.setInsertar(false);                        
                    }
                    
                    item.setCodigo_cuenta("C" + obj.getCodigo_agencia_4() + pcuenta + treemb2.getDescripcion());
                    item.setAuxiliar("");
                    
                }
                
                if( !model.cxpDocService.existeCuentaContable(item.getDstrct(), item.getCodigo_cuenta()) ){
                    this.info("No se pudo crear la Factura Nro. " + obj.getFactura() + ": La cuenta contable " + item.getCodigo_cuenta() + "" +
                    " No registra o est� anulada. Fila : " + obj.getFila() + " del formato de migraci�n." + "  Descripci�n: " + item.getDescripcion());
                    this.setInsertar(false);
                }
                
                cxp_items.add(item);
                
                /* Impuestos del item */
                
                if( iva!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(iva.getCodigo_impuesto());
                    imp.setPorcent_impuesto(iva.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_iva += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_iva_me += this.redondear((iva.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    if( riva!=null ){
                        CXPImpItem imp2 = new CXPImpItem();
                        
                        imp2.setDstrct(doc.getDstrct());
                        imp2.setProveedor(doc.getProveedor());
                        imp2.setTipo_documento(doc.getTipo_documento());
                        imp2.setDocumento(doc.getDocumento());
                        imp2.setItem(item.getItem());
                        imp2.setCod_impuesto(riva.getCodigo_impuesto());
                        imp2.setPorcent_impuesto(riva.getPorcentaje1());
                        imp2.setVlr_total_impuesto(this.redondear(imp.getVlr_total_impuesto()*(imp2.getPorcent_impuesto()/100.0), mda));
                        imp2.setVlr_total_impuesto_me(this.redondear(imp.getVlr_total_impuesto_me()*(imp2.getPorcent_impuesto()/100.0), doc.getMoneda()));
                        imp2.setCreation_user(usuario.getLogin());
                        imp2.setPorcent_impuesto(this.abs(imp2.getPorcent_impuesto()));
                        
                        cxp_impitems.add(imp2);
                        
                        vlr_fra += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        vlr_fra_me +=  this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                        
                        ttl_riva += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto(), mda);
                        ttl_riva_me += this.redondear((riva.getPorcentaje1()/100.0)*imp.getVlr_total_impuesto_me(), mda);
                    }
                }
                
                if( rfte!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rfte.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rfte.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rfte += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rfte_me += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
                
                if( rica!=null ){
                    CXPImpItem imp = new CXPImpItem();
                    
                    imp.setDstrct(doc.getDstrct());
                    imp.setProveedor(doc.getProveedor());
                    imp.setTipo_documento(doc.getTipo_documento());
                    imp.setDocumento(doc.getDocumento());
                    imp.setItem(item.getItem());
                    imp.setCod_impuesto(rica.getCodigo_impuesto());
                    imp.setPorcent_impuesto(rica.getPorcentaje1());
                    imp.setVlr_total_impuesto(this.redondear(item.getVlr()*(imp.getPorcent_impuesto()/100.0), mda));
                    imp.setVlr_total_impuesto_me(this.redondear(item.getVlr_me()*(imp.getPorcent_impuesto()/100.0), doc.getMoneda()));
                    imp.setCreation_user(usuario.getLogin());
                    imp.setPorcent_impuesto(this.abs(imp.getPorcent_impuesto()));
                    
                    cxp_impitems.add(imp);
                    
                    vlr_fra += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    vlr_fra_me +=  this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                    
                    ttl_rica += this.redondear((rfte.getPorcentaje1()/100.0)*item.getVlr(), mda);
                    ttl_rica_me += this.redondear((rica.getPorcentaje1()/100.0)*item.getVlr_me(), mda);
                }
            }
            
            logger.info("?vlr_fra = " + vlr_fra);
        }
        
        doc.setVlr_neto( this.redondear(vlr_fra, mda) );
        doc.setVlr_neto_me( this.redondear(vlr_fra_me, doc.getMoneda()) );
        doc.setVlr_saldo(doc.getVlr_neto());
        doc.setVlr_saldo_me(doc.getVlr_neto_me());
        doc.setItems(cxp_items);
        doc.setImp_items(cxp_impitems); 
        
        /* Impuestos de la cabecera */
        
        if( iva!=null ) {
            CXPImpDoc impdoc = new CXPImpDoc();
            
            impdoc.setDstrct(doc.getDstrct());
            impdoc.setProveedor(doc.getProveedor());
            impdoc.setTipo_documento(doc.getTipo_documento());
            impdoc.setDocumento(doc.getDocumento());
            impdoc.setCod_impuesto(iva.getCodigo_impuesto());
            impdoc.setPorcent_impuesto(this.abs(iva.getPorcentaje1()));
            impdoc.setVlr_total_impuesto(this.redondear(ttl_iva, mda));
            impdoc.setVlr_total_impuesto_me(this.redondear(ttl_iva_me, doc.getMoneda()));
            impdoc.setCreation_user(usuario.getLogin());
            
            cxp_imp.add(impdoc);
        }
        
        if( riva!=null ) {
            CXPImpDoc impdoc = new CXPImpDoc();
            
            impdoc.setDstrct(doc.getDstrct());
            impdoc.setProveedor(doc.getProveedor());
            impdoc.setTipo_documento(doc.getTipo_documento());
            impdoc.setDocumento(doc.getDocumento());
            impdoc.setCod_impuesto(riva.getCodigo_impuesto());
            impdoc.setPorcent_impuesto(this.abs(riva.getPorcentaje1()));
            impdoc.setVlr_total_impuesto(this.redondear(ttl_riva, mda));
            impdoc.setVlr_total_impuesto_me(this.redondear(ttl_riva_me, doc.getMoneda()));
            impdoc.setCreation_user(usuario.getLogin());
            
            cxp_imp.add(impdoc);
        }
        
        if( rfte!=null ) {
            CXPImpDoc impdoc = new CXPImpDoc();
            
            impdoc.setDstrct(doc.getDstrct());
            impdoc.setProveedor(doc.getProveedor());
            impdoc.setTipo_documento(doc.getTipo_documento());
            impdoc.setDocumento(doc.getDocumento());
            impdoc.setCod_impuesto(rfte.getCodigo_impuesto());
            impdoc.setPorcent_impuesto(this.abs(rfte.getPorcentaje1()));
            impdoc.setVlr_total_impuesto(this.redondear(ttl_rfte, mda));
            impdoc.setVlr_total_impuesto_me(this.redondear(ttl_rfte_me, doc.getMoneda()));
            impdoc.setCreation_user(usuario.getLogin());
            
            cxp_imp.add(impdoc);
        }
        
        if( rica!=null ) {
            CXPImpDoc impdoc = new CXPImpDoc();
            
            impdoc.setDstrct(doc.getDstrct());
            impdoc.setProveedor(doc.getProveedor());
            impdoc.setTipo_documento(doc.getTipo_documento());
            impdoc.setDocumento(doc.getDocumento());
            impdoc.setCod_impuesto(rica.getCodigo_impuesto());
            impdoc.setPorcent_impuesto(this.abs(rica.getPorcentaje1()));
            impdoc.setVlr_total_impuesto(this.redondear(ttl_rica, mda));
            impdoc.setVlr_total_impuesto_me(this.redondear(ttl_rica_me, doc.getMoneda()));
            impdoc.setCreation_user(usuario.getLogin());
            
            cxp_imp.add(impdoc);
        }
        
        doc.setImp_doc(cxp_imp);
        
        return doc;
    }
    
    private void sqlInsertarCxP( CXP_Doc doc ){
        Vector sql = new Vector();
        try{
            String cab = "";
            cab = model.cxpDocService.insertarCXPDocMigracion(doc);
            logger.info("?sql cabecera: " + cab);
            sql.add(cab);
            
            Vector items = doc.getItems();
            
            for( int i=0; i<items.size(); i++){
                String it = "";
                CXPItemDoc item = (CXPItemDoc) items.elementAt(i);
                it = model.cxpDocService.insertarItemCXPDocMigracion(item);
                logger.info("?sql item: " + it);
                sql.add(it);
            }
            
            Vector imp_items = doc.getImp_items();
            
            for( int i=0; i<imp_items.size(); i++){
                CXPImpItem imp = (CXPImpItem) imp_items.elementAt(i);
                String impit = "";
                impit = model.cxpDocService.insertarImpuestoItemCXPDocMigracion(imp);
                logger.info("?sql imp item: " + impit);
                sql.add(impit);
            }
            
            Vector imp_doc = doc.getImp_doc();
            
            for( int i=0; i<imp_doc.size(); i++){
                CXPImpDoc impdoc = (CXPImpDoc) imp_doc.elementAt(i);
                String imp = "";
                imp = model.cxpDocService.insertarImpuestoCXPDocMigracion(impdoc);
                logger.info("?sql imp doc: " + imp);
                sql.add(imp);
            }
            
            if( !model.cxpDocService.existeCxPDoc(doc)){
                model.despachoService.insertar(sql);
                this.info("Se ha ingresado satisfactoriamente la factura: " + doc.getDocumento() + " del proveedor " + doc.getProveedor());
            } else {
                this.info("La Factura " + doc.getDocumento() + " del proveedor " + doc.getProveedor() + " ya existe.");
            }
            
        } catch ( Exception e){
            this.info("No se grab� la siguiente factura " + doc.getDocumento()  + ". Se ha presentado la siguiente excepcion: " + e.getMessage());
        }
        
    }
    
    private double redondear( double valor, String moneda){
        return ( moneda.equals("DOL") ? Util.roundByDecimal(valor, 2) : (int)Math.round(valor)) ;
    }
    
    private String formatearItem( String nitem ){
        String format = nitem;
        for( int i = nitem.length(); i<3; i++ ){
            format = "0" + format;
        }
        return format;
    }
    
    private double abs( double vlr){
        double abs = vlr;
        if( abs < 0 )
            return abs*-1;
        else
            return abs;
    }
    
    /**
     * Getter for property insertar.
     * @return Value of property insertar.
     */
    public boolean isInsertar() {
        return insertar;
    }
    
    /**
     * Setter for property insertar.
     * @param insertar New value of property insertar.
     */
    public void setInsertar(boolean insertar) {
        this.insertar = insertar;
    }
    
}
 /*********************************************
 Entregado a Tito 23 Marzo de 2007
 **********************************************/



