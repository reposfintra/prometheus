/*
 * PvuUpdateAction.java
 *
 * Created on 19 de julio de 2005, 16:09
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Rodrigo
 */
public class PvuUpdateAction extends Action{
    
    /** Creates a new instance of PvuUpdateAction */
    public PvuUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/permisos/perfil_vista_usuario/PvuModificar.jsp?lista=ok&reload=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String usu = request.getParameter("c_usuario");
        String codigo = request.getParameter("c_codigo");
        String perfil = request.getParameter("c_perfil");
        try{
            PerfilVistaUsuario js = new PerfilVistaUsuario();
            js.setCodigo(codigo);
            js.setUsuario(usu);
            js.setPerfil(perfil);
            js.setUser_update(usuario.getLogin().toUpperCase());
            js.setCia(usuario.getCia().toUpperCase());
            model.perfilvistausService.modificarPvu(js);
            request.setAttribute("mensaje","MsgModificado");            
            model.perfilvistausService.searchDetallePerfilVistaUsuarios(codigo);
            PerfilVistaUsuario pvu2 = model.perfilvistausService.getPerfilVistaUsuario();
            request.setAttribute("pvu",pvu2);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }    
}
