/******************************************************************************
 * Nombre clase :      ActualizarDocumentoRemesaAction.java                   *
 * Descripcion :       Action del ActualizarDocumentoRemesaAction.java        *
 * Autor :             LREALES                                                *
 * Fecha :             22 de julio de 2006, 10:01 AM                          *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 *****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.operation.model.threads.*;
import javax.servlet.http.*;
import com.tsp.util.Contabilidad.*;

public class ActualizarDocumentoRemesaAction  extends Action {
    
    /** Creates a new instance of ActualizarDocumentoRemesaAction */
    public ActualizarDocumentoRemesaAction() {
    }    
    
    public void run() throws ServletException {
        
        String next = "/jsp/general/exportar/ActualizarDocumentoRemesa.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            HiloActualizarDocumentoRemesa hadr = new HiloActualizarDocumentoRemesa();
            
            hadr.start();

            next = next + "?msg=El Proceso de Actualizacion esta ejecutandose!";
                     
        } catch ( Exception e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
                        
    }
    
}
