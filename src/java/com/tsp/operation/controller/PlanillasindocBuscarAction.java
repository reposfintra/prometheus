/*************************************************************************
 * Nombre ......................PlanillasindocBuscarAction.java          *
 * Descripci�n..................Clase Action para buscar planillas       *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................11 de febrero de 2006, 11:01 AM           *
 * Versi�n......................1.0                                      *
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class PlanillasindocBuscarAction extends Action {
    
    /** Creates a new instance of PlanillasindocBuscarAction */
    public PlanillasindocBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/masivo/reportes/PlanillaSinDoc.jsp";
        
        try{
            model.planillaService.buscarPlanillasinDocumentos(usuario.getLogin(),request.getParameter("fechai"), request.getParameter("fechaf"));
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
