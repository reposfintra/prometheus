/*
 * MigracionRemesasAnuladasAction.java
 *
 * Created on 16 de julio de 2005, 02:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
//import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  Hager
 */
public class MigracionRemesasModificacionOTAction extends Action{
    static Logger logger = Logger.getLogger(MigracionRemesasModificacionOTAction.class);
    /** Creates a new instance of MigracionRemesasAnuladasAction */
    public MigracionRemesasModificacionOTAction() {
    }
    
    public void run() throws ServletException, InformationException{        
        String next="/migracion/migracionRemesasModificacionOT.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        logger.info("Entre a la accion");
        try {
            String fechaActual = Util.fechaActualTIMESTAMP();
            
            model.migracionOTsModifService.cargarArchivoPropiedades();
            String fechaIni = model.migracionOTsModifService.getUltimaFechaProceso();
            
            logger.info("Fecha ultimo proceso"+fechaIni);
            
            model.migracionOTsModifService.setUltimaFechaProceso(fechaActual);
            
            HMigracionRemesasModificacionOT hilo = new HMigracionRemesasModificacionOT();
            hilo.start(fechaIni, fechaActual, usuario.getLogin());            
            
            next = "/migracion/mostrarOT.jsp";
            
            
            
        }catch (Exception ex) {
            ex.printStackTrace();
        }    
                        
        this.dispatchRequest(next);
    }
    
}
