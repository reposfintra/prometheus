/********************************************************************
 *      Nombre Clase.................   FacturaImpuestoAction.java
 *      Descripci�n..................   Action que se encarga de generar un vector con los resgitros de proveedores por numero de nit
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FactRecurrProveedoresnitAction extends Action
{
        
        /** Creates a new instance of FacturaProveedoresnitAction */
        public FactRecurrProveedoresnitAction()
        {
        }
        
        public void run () throws ServletException, InformationException
        {
                try
                {
                        String maxfila = request.getParameter("maxfila");
                        String tipo_documento =""+ request.getParameter ("tipo_documento");
                        String documento =""+ request.getParameter ("documento");
                        String proveedor =""+ request.getParameter ("proveedor");
                        String subtotal =(request.getParameter ("subtotal")==null?"":request.getParameter ("subtotal"));
                        String validar  =(request.getParameter ("validar")==null?"":request.getParameter ("validar"));
                        model.proveedorService.obtenerProveedoresPorNit (proveedor);
                   
                        String next = "/jsp/cxpagar/facturasrec/buscarnit.jsp?accion=1&num_items=&subtotal"+subtotal+"&marco=no&validar="+validar+"&maxfila="+maxfila;
                        this.dispatchRequest (next);
                        
                }
                catch(Exception e)
                {
                        throw new ServletException ("Accion:"+ e.getMessage ());
                }
        }
        
}
