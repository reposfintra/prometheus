/*
 * DespachomSalirAction.java
 *
 * Created on 22 de noviembre de 2005, 03:40 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachomSalirAction extends Action {
    
    /** Creates a new instance of DespachomSalirAction */
    public DespachomSalirAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="";
        try {
            model.despachoManualService.listaDespacho();
            next="/jsp/trafico/despacho_manual/despacho.jsp?reload=1";
        }
        catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
