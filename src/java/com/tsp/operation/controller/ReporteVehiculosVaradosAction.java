/********************************************************************
 *      Nombre Clase....   ReporteVehiculosVaradosRefreshAction.java
 *      Descripci�n.....   Genera el reporte de oportunidad
 *      Autor...........   Ing. Tito Andr�s Maturana
 *      Fecha...........   07.01.2005
 *      Versi�n.........   1.0
 *      Copyright.......   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteVehiculosVaradosAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteVehiculosVaradosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String plaveh = request.getParameter("plaveh");
        String cliente = request.getParameter("clientes");  
        String orig = request.getParameter("origen");
        String dest = request.getParameter("destino");
        String agencia = request.getParameter("agencias");
        String fechai = (request.getParameter("FechaI")!=null)? request.getParameter("FechaI") : "";
        String fechaf = (request.getParameter("FechaF")!=null)? request.getParameter("FechaF") : "";
        String tiprep = (request.getParameter("treporte")!=null)? request.getParameter("treporte") : "";
        String zona = (request.getParameter("zona")!=null)? request.getParameter("zona") : "";
        String rstatus = (request.getParameter("rstatus")!=null)? request.getParameter("rstatus") : "";
        
        logger.info(" plaveh: " + plaveh);
        logger.info(" cliente: " + cliente);
        logger.info(" origen: " + orig);
        logger.info(" destino: " + dest);
        logger.info(" agencia: " + agencia);
        logger.info(" fechai: " + fechai);
        logger.info(" fechaf: " + fechaf);
        logger.info(" tiprep: " + tiprep);
        logger.info(" zona: " + zona);
                
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/trafico/reportes/ReporteVehiculosDemoradosRespuesta.jsp";
        
        try{
            
            if( rstatus.length() == 0)
                model.rmtService.reporteVehiculosDemoradosPlaca(plaveh, cliente, orig, dest, agencia, fechai, fechaf, tiprep, zona);
            else
                model.rmtService.reporteVehiculosDemoradosPlacaMod(plaveh, cliente, orig, dest, agencia, fechai, fechaf, tiprep, zona);
                //model.rmtService.reporteVehiculosDemoradosPlaca();
                
            Vector rep = model.rmtService.getReportesPlanilla();
            
            if( rep.size() == 0){
                next = "/jsp/trafico/reportes/ReporteVehiculosDemorados.jsp?msg=" +
                        "La consulta no gener� ning�n resultado.";
            }
            
            model.rmtService.setPlanillasReportes(rep);
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
