/*******************************************************************
 * Nombre clase: PlacaBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  jdelarosa
 */
public class ControlBusquedaModificarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ControlBusquedaModificarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/planviaje/control_excepcion/ModificarControl_execpcion.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String codigo = request.getParameter ("codigo")!=null?request.getParameter ("codigo"):"";
        String origen = request.getParameter ("origen")!=null?request.getParameter ("origen"):"";
        String destino = request.getParameter ("destino")!=null?request.getParameter ("destino"):"";
        String agencia = request.getParameter ("agencia")!=null?request.getParameter ("agencia"):"";
        Vector vector = new Vector();
        try{
             model.control_excepcionService.buscarControl(codigo,origen,destino,agencia);
            vector = model.clienteService.getVector();
               
            if ( vector.size()> 0){
                next+="?opcion=1";
            }else {
                next+="?msg=No se encontro ningun resultado";
            }
           
        }catch (Exception e){
            e.printStackTrace ();
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
}
