/*
 * RemesaSearchAction.java
 *
 * Created on 14 de diciembre de 2004, 01:02 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class RemesaPadreSearchAction extends Action{
    
    /** Creates a new instance of RemesaSearchAction */
    public RemesaPadreSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next = "/colpapel/agregarOTpadre.jsp";
        String no_rem= request.getParameter("numrem").toUpperCase();
        String[] num_remesas = null;
        String mensaje = "La(s) siguente(s) remesa(s) ya tiene(n) asociada una remesa Padre: ";
        String padre = ( request.getParameter("numpadre")!=null )? request.getParameter("numpadre").toUpperCase() : "" ;
        String opcion = request.getParameter("opcion");
        
        boolean tienepadre = false;
        try{
            
            Vector remesas = new Vector();
            
            if( opcion.equals("1") ){
                if( no_rem.length()>0 ){
                    num_remesas = no_rem.split(",");
                }
                if( num_remesas != null ){
                    Remesa rem = new Remesa();
                    for( int i=0; i<num_remesas.length; i++){
                        model.remesaService.buscaRemesa( num_remesas[i] );
                        rem = model.remesaService.getRemesa();
                        if( rem != null ){
                            if( rem.getPadre().equals("") ){
                                remesas.add( rem );
                            }else{
                                tienepadre = true;
                                mensaje +=  rem.getNumrem()+"  ";
                            }
                        }
                    }
                }
            }
            else{
                if( padre != null && padre.length()>0 ){
                    Vector v = new Vector();
                    v = model.remesaService.obtenerRemesasHijas( padre );
                    
                    for( int i=0; i<v.size(); i++ ){                        
                        model.remesaService.buscaRemesa( (String)v.get(i) );                        
                        remesas.add( model.remesaService.getRemesa() );
                    }
                }
            }

            mensaje = (tienepadre)? mensaje : null;
            next = "/colpapel/agregarOTpadre.jsp?mensaje="+mensaje+"&numrem="+no_rem;
            //request.setAttribute( "hijas" , (tienepadre)? null : remesas );
            request.getSession().setAttribute( "hijas", (tienepadre)? null : remesas );
            if( !opcion.equals("1") ){
                request.setAttribute( "remesa_padre", model.remesaService.getPadre() );
            }
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
