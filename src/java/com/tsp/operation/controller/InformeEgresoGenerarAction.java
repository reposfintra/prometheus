/*
 * InformeGenerarAction.java
 *
 * Created on 1 de abril de 2005, 03:20 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class InformeEgresoGenerarAction extends Action {
    
    /** Creates a new instance of InformeGenerarAction */
    public InformeEgresoGenerarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/informes/InformeEgreso.jsp";
        String fechai = request.getParameter("fechai");
        String fechaf = request.getParameter("fechaf")+" 07:00";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        if(request.getParameter("excel")!=null){
            ////System.out.println("Ok vamos pa excel");
            next = "/informes/BombasprevExcel.jsp";
        }
        try{
            
            model.planillaService.llenarInformeEgeso(fechai, fechaf,usuario.getBase());
            model.planillaService.llenarInformeProveedores(fechai, fechaf,usuario.getBase());
            Informe inf = new Informe();
            inf.setFechai(fechai);
            inf.setFechaf(fechaf);
            model.planillaService.setInforme(inf);
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
