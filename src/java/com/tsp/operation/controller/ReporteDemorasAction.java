/*********************************************************************************
 * Nombre clase :                 ReporteDemorasAction.java                      *
 * Descripcion :                  Clase que maneja los eventos relacionados      *
 *                                con el programa que busca para el              *
 *                                reporte de las demoras en la BD.               *
 * Autor :                        LREALES                                        *
 * Fecha :                        26 de septiembre de 2006, 04:00 PM             *
 * Version :                      1.0                                            *
 * Copyright :                    Fintravalores S.A.                        *
 ********************************************************************************/

package com.tsp.operation.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ReporteDemorasAction extends Action {
    
    /** Creates a new instance of ReporteDemorasAction */
    public ReporteDemorasAction () { }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/general/exportar/demoras/ReporteDemoras.jsp";
        
        HttpSession session = request.getSession ();
        Usuario usuario = ( Usuario ) session.getAttribute ( "Usuario" );
        
        String hacer = ( request.getParameter ("hacer") != null )?request.getParameter ("hacer").toUpperCase():"";
        
        String fecini = ( request.getParameter ("fecini") != null )?request.getParameter ("fecini").toUpperCase():"";
        String fecfin = ( request.getParameter ("fecfin") != null )?request.getParameter ("fecfin").toUpperCase():"";
        
        String cod_cli = ( request.getParameter ("cod_cli") != null )?request.getParameter ("cod_cli").toUpperCase():"";
        
        String opcion = ( request.getParameter ("opcion") != null )?request.getParameter ("opcion"):"";

        try {
            
            Vector datos = model.reporteDemorasService.getVectorReporte ();
            
            if ( opcion.equals ("1") ) {

                model.reporteDemorasService.reporteTodos ( fecini, fecfin );
                datos = model.reporteDemorasService.getVectorReporte ();  

            } else if ( opcion.equals ("2")  ) {

                model.reporteDemorasService.reporteEspecifico ( cod_cli, fecini, fecfin );
                datos = model.reporteDemorasService.getVectorReporte (); 
            
            } else {
                
                next = "/jsp/general/exportar/demoras/ReporteDemoras.jsp?msg=Defina un parametro de busqueda!";
                
            }
            
            if ( datos.size () > 0 ) {
                
                if ( hacer.equals ( "2" ) ) {
                    
                    HiloReporteDemoras HRD = new HiloReporteDemoras ();
                    HRD.start ( datos, usuario.getLogin (), fecini, fecfin ); 
                    
                    next = "/jsp/general/exportar/demoras/ReporteDemoras.jsp?msg=Archivo exportado a excel!!";
                    
                }
            
            } else { 
                
                next = "/jsp/general/exportar/demoras/ReporteDemoras.jsp?msg=Su busqueda no arrojo resultados!";
                
            }            
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de Demoras Action : " + e.getMessage () );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}