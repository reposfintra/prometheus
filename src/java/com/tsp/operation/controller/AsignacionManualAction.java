package com.tsp.operation.controller;

import javax.servlet.http.*;
import javax.servlet.*;
import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.lang.reflect.*;
import java.util.Vector;
import org.apache.log4j.Logger;

public class AsignacionManualAction extends Action implements  java.io.Serializable {
    static Logger logger = Logger.getLogger(AsignacionManualAction.class);
    public AsignacionManualAction() {
    }
    
    /**
     * run
     */
    public void run() throws ServletException {
        try {
            String fechaInicio = request.getParameter( "fechai" );
            String fechaFin = request.getParameter( "fechaf" );
            HttpSession session = request.getSession();
            Usuario usuario = ( Usuario ) session.getAttribute( "Usuario" );
            String user =usuario.getLogin();
            
            
            
            if(!request.getParameter("cmd").equalsIgnoreCase("Buscar")){
                logger.info("ASIGNAR MANUALMENTE");
                String asignado="";
                String[] asignados;
                java.util.Enumeration enum1;
                String parametro;
                enum1 = request.getParameterNames();
                
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    if(parametro.indexOf("asignado")>=0){
                        //logger.info("Asignado..."+request.getParameter(parametro));
                        asignado=request.getParameter(parametro)+";"+asignado;
                    }
                }
                logger.info("Total asignados "+asignado);
                asignados= asignado.split(";");
                logger.info("Cantidad de asignados "+asignados.length);
                model.rdSvc.setAsignados(new Vector());
                if ( asignados != null ){
                    logger.info("Asignados no es nulo");
                    for ( int i = 0; i < asignados.length; i++ ) {
                        String asignacion = asignados[i].toUpperCase();
                        logger.info("Asignacion numero "+i+" :"+asignacion);
                        if (asignacion.length() > 0){
                            int n = Integer.parseInt(asignacion.substring(1));
                            String filaC = request.getParameter("filaC"+n );
                            String filaR = request.getParameter("filaR"+n );
                            if (  filaC != null && filaC.equals(filaR) ) {
                                // si el recurso se mantubo asignado con su mismo requerimiento, no se hace nada, ya estaban asignados
                                // y seguiran asignados.
                                continue;
                            }
                        }
                        if ( asignacion.length() > 0 ) { //C"+(++c)+"dstrct_code, num_sec, std_job_no, fecha_dispo, numpla
                            if(request.getParameter(asignacion)!=null && !request.getParameter(asignacion).equals("")){
                                int numrec = Integer.parseInt(request.getParameter(asignacion));
                                logger.info("reasignado por caso 1: "+request.getParameter("R"+numrec+"placa")+" con "+asignacion);
                                // CREANDO EL REQUERIMIENTO
                                String dstrct_code = request.getParameter( asignacion + "dstrct_code" );
                                logger.info(dstrct_code);
                                String num_sec = request.getParameter( asignacion + "num_sec" );
                                logger.info(num_sec);
                                String std_job_no = request.getParameter( asignacion + "std_job_no" );
                                logger.info(std_job_no);
                                String fecha_dispo = request.getParameter( asignacion + "fecha_dispo" );
                                logger.info(fecha_dispo);
                                String numpla = request.getParameter( asignacion + "numpla" );
                                logger.info(numpla);
                                ReqCliente requerimiento = model.regAsigService.obtenerRequerimientoDeRegistro( dstrct_code,num_sec,std_job_no,fecha_dispo,numpla );
                                logger.info("Se creo req cliente.........");
                                //CREANDO EL RECURSO dstrct, placa, fecha_disp
                                String dstrct = request.getParameter("R"+numrec+"dstrct");
                                String placa = request.getParameter("R"+numrec+"placa");
                                String fecha_disp = request.getParameter("R"+numrec+"fecha_disp");
                                Recursosdisp recurso = model.regAsigService.obtenerRecursoDeRegistro(dstrct, placa, fecha_disp);
                                logger.info("Se creo recurso.........");
                                //  model.regAsigService.limpiarAsignacion(recurso, requerimiento);
                                model.regAsigService.desasignarRecursosYRequerimientos(recurso,requerimiento, false);
                                model.rdSvc.realizarAsignacion(recurso, null, requerimiento, "M");
                                logger.info("Se realizo asignacion.........");
                                if (requerimiento == null ){
                                    continue;
                                }
                                
                            }
                        }
                    
                        
                    }
                    
                }
                Vector asignadosV = model.rdSvc.getAsignados();
                logger.info("Asignando "+asignadosV.size()+" registros...");
                for(int i=0; i<asignadosV.size(); i++){
                    model.regAsigService.guardarRegistroAsignacion((RegistroAsignacion)asignadosV.elementAt(i));
                }
                model.regAsigService.insertarRegistrosNoAsignados();
                model.ipredoSvc.LlenarAgasoc();
            }
            logger.info("Voy a Buscar");
            String tipo = "SINFECHA";
            if(request.getParameter("fecha").equalsIgnoreCase("Si")){
                tipo = "FECHAS";
            }
            logger.info("Tipo :"+tipo);
            logger.info("Agencia :"+request.getParameter("agencia"));
            logger.info("Fecha1 :"+request.getParameter("fechai"));
            logger.info("Fecha2 :"+request.getParameter("fechaf"));
            logger.info("Cliente :"+request.getParameter("cliente"));
            logger.info("Zona :"+request.getParameter("zona"));
            model.ipredoSvc.llenarInforme(request.getParameter("fechai"),request.getParameter("fechaf"),request.getParameter("agencia"),tipo,request.getParameter("cliente"),request.getParameter("zona"));
            
            this.dispatchRequest("/reportePlaneacion/mostrarEditorDeReporte.jsp?mostrar=ok");
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
            throw new ServletException("Error en AsignacionManualAction:\n"+ex.getMessage());
        }
    }
}