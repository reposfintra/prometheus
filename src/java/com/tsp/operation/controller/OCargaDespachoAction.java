/*******************************************************************
 * Nombre clase: OCargaSearchAction.java
 * Descripci�n: Accion para realizar la buscqueda de una orden de carga
 * Autor: Ing. Karen Reales
 * Fecha: 12 de mayo 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OCargaDespachoAction extends Action{
    static Logger logger = Logger.getLogger(OCargaValidarAction.class);
    
    public OCargaDespachoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        /**
         *
         * Usuario en Sesion
         */
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        /**
         *
         * Inicializacion Variable tipo String
         */
        String next = "/colpapel/InicioDespacho.jsp";
        String orden = request.getParameter("orden");
        logger.info("orden "+orden);
        try{
            model.imprimirOrdenService.buscarOrdenDeCarga(usuario.getDstrct(), orden);
            if(model.imprimirOrdenService.getHojaOrden()==null){
                next="/colpapel/preliminar.jsp?mensaje=La orden de carga no existe o esta anulada";
            }
            else  {
                if(model.imprimirOrdenService.getHojaOrden().getNumpla().equals("")){
                    try{
                        model.RemDocSvc.listarDocOCargue(orden,"");
                        model.RemDocSvc.setDocumentos(model.RemDocSvc.getVec());
                        String stdjob=model.imprimirOrdenService.getHojaOrden().getStd_job_no();
                        model.stdjobcostoService.searchRutas(stdjob);
                        //System.out.println("Termine la ruta");
                        //System.out.println("Busco Standard");
                        model.stdjobdetselService.searchStdJob(stdjob);
                        model.cliente_ubicacionService.listarUbicaciones("000"+stdjob.substring(0,3));
                        model.tblgensvc.buscarListaCodigo("AUTEXFLETE", "000"+stdjob.substring(0,3));
                        model.ImagenSvc.resetImagesDespacho(usuario.getLogin());
                        model.productoService.setProductos(null);
                        model.sjextrafleteService.listaCostos(stdjob,usuario.getId_agencia());
                        model.sjextrafleteService.listaFletes(stdjob,usuario.getId_agencia());
                        model.anticiposService.vecAnticipos("",stdjob);
                        model.anticiposService.searchAnticiposProveedor(usuario);
                        model.remidestService.searchCiudades(stdjob.substring(0,3));
                        //System.out.println("Termine Standard");
                        next = "/colpapel/InicioDespacho.jsp?standard="+stdjob;
                    }catch(Exception ex){
                        throw new ServletException(ex.getMessage());
                    }
                }
                else{
                    next="/colpapel/preliminar.jsp?mensaje=La orden de carga ya fue utilizada";
                }
            }
            logger.info("El next= "+next);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
