/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AnticiposAplicarAction extends Action{
    static Logger logger = Logger.getLogger(ExtrafleteAplicarAction.class);
    /** Creates a new instance of AcpmDeleteAction */
    public AnticiposAplicarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        logger.info("*********************INSERTAR anticipo de proveedor*************************");
        
        String next="/colpapel/anticipos.jsp?cerrar=0K";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        logger.info("Entre a la accion");
        
        
        Vector descuentos = model.anticiposService.getAnticiposProv();
        Vector nuevo = new Vector();
        if(descuentos!=null){
            for(int k = 0 ; k<descuentos.size();k++){
                Anticipos ant = (Anticipos) descuentos.elementAt(k);
                String codigo = ant.getAnticipo_code();
                if(request.getParameter(codigo)!=null){
                    String value = request.getParameter(codigo).equals("")?"0":request.getParameter(codigo);
                    if(Float.parseFloat(value)>0){
                        ant.setValor(Float.parseFloat(value));
                       // ant.setChequeado(true);
                        ant.setProveedor(request.getParameter("provee"+codigo));
                    }
                }
                nuevo.add(ant);
            }
            model.anticiposService.setAnticiposProv(nuevo);
        }
        this.dispatchRequest(next);
    }
}
