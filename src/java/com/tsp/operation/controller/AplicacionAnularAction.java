/********************************************************************
 *      Nombre Clase.................   AplicacionAnularAction.java    
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   13.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class AplicacionAnularAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public AplicacionAnularAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblapl/AppUpdate.jsp?mensaje=MsgModificado&estado=Anulado";
                String next = "";
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        String cia = request.getParameter("c_cia");
                        String codigo = request.getParameter("c_codigo");
                                                
                        model.appSvc.anularAplicacion(cia, codigo);
                        
                        pag += "&msg=Aplicacion anulada exitosamente.";
                        next = com.tsp.util.Util.LLamarVentana(pag, "Aplicacion Anulada");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
