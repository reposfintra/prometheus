/********************************************************************
 *      Nombre Clase.................   FacturaServiciosAction .java
 *      Descripci�n..................   Action que se encarga de generar el arbol de unservicio en particular
 *      Autor........................   David Lamadrid
 *      Fecha........................   1 de noviembre de 2005, 05:00 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaServiciosAction extends Action{
    
    /** Creates a new instance of FacturaServiciosAction */
    public FacturaServiciosAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String distrito =  usuario.getDstrct();
            String opcion = (request.getParameter("OP")!= null)?request.getParameter("OP"):"";
            String next ="";
            System.out.println("FacturaServiciosAction");
            System.out.println("OPCION------------------->" + opcion);
            if(opcion.equals("BUSCAR_IMPUESTO")){
                String Tipo   = request.getParameter("tipo");
                String Codigo = request.getParameter("codigo");
                String Id     = request.getParameter("Id");
                Tipo_impuesto datos = new  Tipo_impuesto();
                datos = model.TimpuestoSvc.buscarImpuestoPorCodigos(Tipo,Codigo,distrito,""); 
                next = "/jsp/cxpagar/facturasxpagar/BuscarCodigoImpuesto.jsp?Id="+Id;
                
            }else if(opcion.equals("ENTER")){
                String concepto = request.getParameter("concepto");
                String Id = request.getParameter("Id");
                model.ConceptoPagosvc.BuscarConcepto(concepto);
                List listaConcepto = model.ConceptoPagosvc.getListaConcepto();
                for(int j=0;j<listaConcepto.size();j++){
                    ConceptoPago tmp = (ConceptoPago) listaConcepto.get(j);
                    ////System.out.println("DECRIPCION--"+tmp.getDescripcion());
                }
                next = "/jsp/cxpagar/facturasxpagar/BuscarConcepto.jsp?Id="+Id;
            }else if(opcion.equals("BUSCAR_REFERENCIA")){
                String concepto = request.getParameter("concepto");
                String Id = request.getParameter("Id");
                String cre = request.getParameter("cre");
                
                model.ConceptoPagosvc.BuscarReferencia3(concepto,cre);
                next = "/jsp/cxpagar/facturasxpagar/AgregarCuenta.jsp?Id="+Id+"&cre="+cre;
                
            }else if(opcion.equals("UPDATE_BANCO")){
               
                String banco    = request.getParameter("banco");
                //System.out.println("BANCO------------->======>"+banco);
                String sucursal = request.getParameter("sucursal");
                //System.out.println("SUCURSAL------------->======>"+sucursal);
                String dstrct    = request.getParameter("distrito");
                //System.out.println("DISTRITO------------->======>"+dstrct);
                String proveedor   = request.getParameter("proveedor");
                //System.out.println("PROVEEDOR------------->======>"+proveedor);
                String tipo        = request.getParameter("tipo");
                //System.out.println("TIPO------------->======>"+tipo);
                String documento   = request.getParameter("documento");
                //System.out.println("DOCUMENTO------------->======>"+documento);
                String fecha_documento    = request.getParameter("fechaDoc");
                //System.out.println("FECHA DOCUMENTO------------->======>"+fecha_documento);
                int plazo = Integer.parseInt(request.getParameter("plazo")); 
                //System.out.println("PLAZO------------->======>"+plazo);
                
                String fecha_vencimiento  =  Util.fechaFinalYYYYMMDD(fecha_documento,plazo);
                //System.out.println("FECHA VENCIMIENTO------------->======>"+fecha_vencimiento);
                
                model.cxpDocService.updateBanco(dstrct, proveedor, tipo, documento, banco, sucursal,fecha_vencimiento); 
                next = "/controller?estado=Factura&accion=Detalle&documento="+ documento+"&prov="+proveedor+"&tipo_doc="+tipo+"&isSaldoDif=true";
                request.setAttribute("ms", "La Factura fue modificada con exito");
                
            }else if(opcion.equals("UPDATE_BANCO_RECURRENTE")){
                String banco    = request.getParameter("banco");
                String sucursal = request.getParameter("sucursal");
                String dstrct    = request.getParameter("distrito");
                String proveedor   = request.getParameter("proveedor");
                String tipo        = request.getParameter("tipo");
                String documento   = request.getParameter("documento");
                model.factrecurrService.updateBanco(dstrct, proveedor, tipo, documento, banco, sucursal);  
                next = "/controller?estado=FacturaRecurrente&accion=Detalle&documento="+documento+"&prov="+proveedor+"&tipo_doc=010";
                request.setAttribute("ms", "Banco y sucursal modificado con exito");
                
            }else{
                String nombreServicio =""+ request.getParameter("servicio");
                String controlador =""+ request.getParameter("controlador");
                String codigo =""+ request.getParameter("codigo");
                controlador= controlador.replaceAll("~", "&");
                String ag=usuario.getId_agencia();
                ////System.out.println("ag /////////////////////////"+ag);
                model.ConceptoPagosvc.load(nombreServicio,ag);
                
                next = "/jsp/cxpagar/facturasxpagar/contenedorMenu.jsp?codigo="+codigo;
                ////System.out.println("next en Filtro"+next);
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
    }
    
}
