/*
 * ModificarZonaAction.java
 *
 * Created on 13 de junio de 2005, 11:10 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class ModificarZonaUsuarioAction extends Action {
    
    /** Creates a new instance of ModificarZonaAction */
    public ModificarZonaUsuarioAction() {
    }
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/zona/VerZonasUsuario.jsp?reload=";
        String codigo = (request.getParameter("codigo").toUpperCase());
        String nombre = (request.getParameter("nombre"));        
        String viejo = (request.getParameter("viejo"));   
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{   
            if(!nombre.equals(viejo)){
                if (model.zonaUsuarioService.existeZonaUsuario(codigo,nombre))
                    next = "/jsp/trafico/zona/zonaUsuario.jsp?mensaje=ErrorA&sw=1";
                else if(!model.zonaUsuarioService.existeUsuario(nombre))
                    next = "/jsp/trafico/zona/zonaUsuario.jsp?mensaje=ErrorC&sw=1";
                else  {
                    ZonaUsuario zona = new ZonaUsuario();
                    zona.setCodZona(codigo);
                    zona.setNomUsuario(nombre);
                    zona.setUser_update(usuario.getLogin());
                    model.zonaUsuarioService.modificarZonaUsuario(zona,viejo); 
                }
            }
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
}
 