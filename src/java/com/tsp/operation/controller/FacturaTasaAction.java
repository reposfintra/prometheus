/*
 * FacturaInsertAction.java
 *
 * Created on 6 de enero de 2005, 09:00 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  AMARTINEZ
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.pdf.*;
import com.tsp.util.*;
import java.text.*;

public class FacturaTasaAction extends Action{
    
    /** Creates a new instance of ConsultarConductor */
    public FacturaTasaAction() {
        
    }
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute("Distrito");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct   = usuario.getDstrct();
        String base = usuario.getBase();
        
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String actual = format.format(fecha);
        
        String Agregar= (request.getParameter("Agregar")!=null)?request.getParameter("Agregar"):"";
        
        String next="/jsp/cxcobrar/facturacion/Iniciofacturacion.jsp";
        try{
            
            
            
            if(Agregar.equals("ok")){
                String causa = (request.getParameter("causa")!=null)?request.getParameter("causa"):"";
                
                //System.out.println("agregar");
                /**************************************
                 * Obtiene los datos enviados desde el formulario
                 ***************************************/
                
                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"0";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                
                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";
                
                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";
                
                String guardar =(request.getParameter("guardar")!=null)?request.getParameter("guardar"):"";
                String actualizar =(request.getParameter("actualizar")!=null)?request.getParameter("actualizar"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield").replaceAll(",",""):"1";
                double valtasa = Double.parseDouble(tasa);
                String mensaje="";
                
                model.facturaService.getFactu().setAgencia_facturacion(usuario.getId_agencia());
                
                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/
                
                
                
                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setValor_tasa(new Double(valtasa));
                if(mensaje.equals(""))
                    model.facturaService.getFactu().setMoneda(moneda);
                //System.out.println(" moneda factu---------->"+ model.facturaService.getFactu().getMoneda());
                
                String maxfila= (request.getParameter("numerofilas")!=null)?request.getParameter("numerofilas"):"";
                String facturacion= (request.getParameter("facturacion")!=null)?request.getParameter("facturacion"):"";
                String facturacionmodif= (request.getParameter("facturacionmodif")!=null)?request.getParameter("facturacionmodif"):"";
                
                ////System.out.println("maxfila:"+maxfila);
                Vector a = new Vector();
                
                /**************************************
                 * for para recorre todos los items del formulario
                 * y obtenerlos
                 ***************************************/
                
                
                for(int i =0; i<= Integer.parseInt(maxfila); i++){
                    
                    String textfield11= (request.getParameter("textfield11"+i)!=null)?request.getParameter("textfield11"+i):"";
                    String textfield12= (request.getParameter("textfield12"+i)!=null)?request.getParameter("textfield12"+i):"";
                    String textfield13= (request.getParameter("textfield13"+i)!=null)?request.getParameter("textfield13"+i):"";
                    String textfield14= (request.getParameter("textfield14"+i)!=null)?request.getParameter("textfield14"+i):"";
                    String textfield15= (request.getParameter("textfield15"+i)!=null)?request.getParameter("textfield15"+i):"";
                    String textfield16= (request.getParameter("textfield16"+i)!=null)?request.getParameter("textfield16"+i):"";
                    String textfield17= (request.getParameter("textfield17"+i)!=null)?request.getParameter("textfield17"+i):"";
                    String textfield18= (request.getParameter("textfield18"+i)!=null)?request.getParameter("textfield18"+i):"";
                    String textfield19= (request.getParameter("textfield19"+i)!=null)?request.getParameter("textfield19"+i):"";
                    
                    String hiddenField= (request.getParameter("hiddenField"+i)!=null)?request.getParameter("hiddenField"+i):"0";
                    String hiddenField2= (request.getParameter("hiddenField2"+i)!=null)?request.getParameter("hiddenField2"+i):"0";
                    String hiddenField3= (request.getParameter("hiddenField3"+i)!=null)?request.getParameter("hiddenField3"+i):"0";
                    
                    //Nuevos Campos para auxiliar
                    String tipo=(request.getParameter("tipo"+i)!=null)?request.getParameter("tipo"+i):"";
                    String auxiliar=(request.getParameter("auxiliar"+i)!=null)?request.getParameter("auxiliar"+i):"";
                    String aux=(request.getParameter("aux"+i)!=null)?request.getParameter("aux"+i):"";
                    
                    
                    /**************************************
                     * condicional para saber si los campos q se mandaron del formulario
                     * estan vacios, de caso contrario es por que hay un item !.
                     ***************************************/
                    
                    if(textfield11.equals("")&&textfield12.equals("")&&textfield13.equals("")&&textfield14.equals("")&&textfield15.equals("")&&textfield16.equals("")&&textfield17.equals("")&&textfield18.equals("")){
                        
                    }
                    else{
                        ////System.out.println(" "+textfield11+" "+textfield12+" "+textfield13+" "+textfield14+" "+textfield15+" "+textfield16+" "+textfield17+" "+textfield18+" "+textfield19);
                        /**************************************
                         * A continuacion se setea el item en el objeto con los
                         * campos obtenidos del formulario y luego se
                         * almacena en el vector
                         ***************************************/
                        
                        ////System.out.println("hay!!! item!!!");
                        factura_detalle factu_deta = new factura_detalle();
                        
                        Vector remesas = model.facturaService.getRemesas();
                        if(remesas != null && remesas.size() > 0){
                            for(int h=0; h< remesas.size();h++){
                                factura_detalle fd= (factura_detalle) ((factura_detalle)remesas.get(h)).clonee();
                                if(fd.getNumero_remesa().equals(textfield11)){
                                    factu_deta = fd;
                                    break;
                                }
                                
                            }
                        }
                        
                        
                        
                     
                        
                        String[] c={"",""};
                        c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());
                        
                        if(c[1].equals("S")){
                            try{
                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                
                            }catch(Exception o){
                                o.printStackTrace();
                            }
                        }
                        factu_deta.setAux(c[1]);
                        factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                        
                        
                        //System.out.println("MONEDA FACTURA  - >" + model.facturaService.getFactu().getMoneda());
                        
                        
                        if(!factu_deta.getMoneda().equals("PES")){//esta es la moneda de la remesa
                            
                            try{
                             
                                
                                
                                double valuni = factu_deta.getValor_unitario();
                                double raro = 0;
                               
                                raro/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                                //System.out.println("Raro "+raro);

                                factu_deta.setValor_unitario(valuni*valtasa);
                                factu_deta.setValor_unitariome(valuni);
                                
                                factu_deta.setValor_item(valuni*factu_deta.getCantidad().doubleValue()*valtasa);
                                factu_deta.setValor_itemme(factu_deta.getValor_item());
                                
                                
                                
                                
                                factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                factu_deta.setValor_tasa(new Double(valtasa));
                                
                                //System.out.println("Tasa:"+valtasa);
                                //System.out.println("Valor Uni "+valuni*valtasa);
                                //System.out.println("Sub Total "+valuni*factu_deta.getCantidad().doubleValue()*valtasa);
                                
                                
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        
                        a.add(factu_deta);
                        
                        
                    }
                }
                
                /**************************************
                 * el vector de items seteados (a), el a su
                 * vez copiado en los vectores de
                 * remesasfacturadas y modificadas
                 ***************************************/
                
                model.facturaService.setRemesasModificadas(a);
                model.facturaService.setRemesasFacturar(a);
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?agregarItems=ok&Mensaje="+mensaje;
                
                /**************************************
                 * condicional para refrescar la pantalla y poder
                 *cargar modificaciones etc.
                 ***************************************/
                
                if(actualizar.equals("ok")){
                    ////System.out.println("actualizar");
                    next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?Mensaje="+mensaje;
                    
                }
                
                /**************************************
                 * condicionalcuando se undio el boton de aceptar
                 * y porder ingresar la factura
                 ***************************************/
                
                if(facturacion.equals("ok")){
                    ////System.out.println("facturacion");
                    
                    
                    /*************************************************
                     * Antes de insertar la factura se cerciora que la
                     * factura este en la moneda del distrito,
                     * sino se hace la respectiva conversion.
                     *************************************************/
                    
                    if(model.facturaService.getMonedaLocal().equals(model.facturaService.getFactu().getMoneda())){
                        ////System.out.println("son iguales las monedas:");
                        ////System.out.println("getValor_factura:"+model.facturaService.getFactu().getValor_factura());
                        model.facturaService.getFactu().setValor_facturame(model.facturaService.getFactu().getValor_factura());
                        ////System.out.println("getValor_facturame:"+model.facturaService.getFactu().getValor_facturame());
                        model.facturaService.getFactu().setValor_tasa(new Double("1"));
                        ////System.out.println("getValor_tasa:"+ model.facturaService.getFactu().getValor_tasa());
                        
                    }
                    else{
                        ////System.out.println("son diferentes las monedas:");
                        model.facturaService.getFactu().setValor_facturame(model.facturaService.getFactu().getValor_factura());
                        ////System.out.println("getValor_facturame:"+model.facturaService.getFactu().getValor_facturame());
                        
                        try{
                            Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),model.facturaService.getMonedaLocal(), actual);
                            double valorF = model.facturaService.getFactu().getValor_facturame()*tasas.getValor_tasa();
                            model.facturaService.getFactu().setValor_factura(valorF);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    model.facturaService.getFactu().setAgencia_cobro( model.facturaService.getFactu().getAgencia_facturacion());
                    
                    //Para redondear cabecera antes de insertar
                    
                    if(!model.facturaService.getFactu().getMoneda().equals("PES")){
                        
                        model.facturaService.getFactu().setValor_abonome(Util.roundByDecimal(model.facturaService.getFactu().getValor_abonome(),2));
                        model.facturaService.getFactu().setValor_facturame(Util.roundByDecimal(model.facturaService.getFactu().getValor_facturame(),2));
                        model.facturaService.getFactu().setValor_saldome(Util.roundByDecimal(model.facturaService.getFactu().getValor_saldome(),2));
                        
                    }
                    else{
                        
                        model.facturaService.getFactu().setValor_abonome(Math.round(model.facturaService.getFactu().getValor_abonome()));
                        model.facturaService.getFactu().setValor_facturame(Math.round(model.facturaService.getFactu().getValor_facturame()));
                        model.facturaService.getFactu().setValor_saldome(Math.round(model.facturaService.getFactu().getValor_saldome()));
                        
                    }
                    if(!model.facturaService.getMonedaLocal().equals("PES")){
                        
                        model.facturaService.getFactu().setValor_abono(Util.roundByDecimal(model.facturaService.getFactu().getValor_abono(),2));
                        model.facturaService.getFactu().setValor_factura(Util.roundByDecimal(model.facturaService.getFactu().getValor_factura(),2));
                        model.facturaService.getFactu().setValor_saldo(Util.roundByDecimal(model.facturaService.getFactu().getValor_saldo(),2));
                        
                    }
                    else{
                        
                        model.facturaService.getFactu().setValor_abono(Math.round(model.facturaService.getFactu().getValor_abono()));
                        model.facturaService.getFactu().setValor_factura(Math.round(model.facturaService.getFactu().getValor_factura()));
                        model.facturaService.getFactu().setValor_saldo(Math.round(model.facturaService.getFactu().getValor_saldo()));
                    }
                    
                    String f = model.facturaService.ingresarFactura(model.facturaService.getFactu(), distrito,usuario.getLogin(), base);
                    
                    
                    /*************************************************
                     * la consulta arroja un mesaje f, el cual
                     * es concatenado con el next para mostrar la alerta
                     * en la pagina jsp.
                     *************************************************/
                    
                    
                    if(f.equals(""))
                        next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=La factura no pudo ser creada !!! favor intente de nuevo";
                    else{
                        next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=La factura creada exitosamente!!! con el numero:"+f;
                        Vector vfd= new Vector();
                        for(int i=0;i<model.facturaService.getRemesasFacturar().size();i++){
                            
                            factura_detalle fd = (factura_detalle)model.facturaService.getRemesasFacturar().get(i);
                            ////System.out.println("moneda fact:"+fd.getMoneda());
                            if(fd.getMoneda().equals(model.facturaService.getMonedaLocal())){
                                ////System.out.println("la moneda es igual la remesa!!!");
                            }
                            else{
                                ////System.out.println("la moneda es dif!!!");
                                
                                try{
                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),fd.getMoneda(),model.facturaService.getMonedaLocal(), actual);
                                    ////System.out.println("val:"+fd.getValor_unitario());
                                    ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();
                                    
                                    
                                    double valoruni=0;
                                    
                                    valoruni = fd.getValor_unitario()*val2*val2;
                                    ////System.out.println("else:"+valoruni );
                                    ////System.out.println("tasas!= "+valoruni);
                                    fd.setValor_unitario(valoruni);
                                    fd.setValor_itemme(fd.getValor_item());
                                    fd.setValor_item(valoruni*fd.getCantidad().doubleValue());
                                    fd.setValor_unitariome(fd.getValor_unitario());
                                    fd.setMoneda(model.facturaService.getMonedaLocal());
                                    fd.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                    
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                            }
                            
                            //Redondea factura_detalle antes de insertar
                            
                            if(model.facturaService.getMonedaLocal().equals(model.facturaService.getFactu().getMoneda())){
                                
                                fd.setValor_itemme(fd.getValor_item());
                                fd.setValor_unitariome(fd.getValor_unitario());
                                fd.setValor_tasa(new Double("1"));
                            }
                            
                            if(!model.facturaService.getFactu().getMoneda().equals("PES")){
                                fd.setValor_itemme(Util.roundByDecimal(fd.getValor_itemme(),2));
                                fd.setValor_unitariome(Util.roundByDecimal(fd.getValor_unitariome(),2));
                            }
                            else{
                                fd.setValor_itemme(Math.round(fd.getValor_itemme()));
                                fd.setValor_unitariome(Math.round(fd.getValor_unitariome()));
                            }
                            if(!model.facturaService.getMonedaLocal().equals("PES")){
                                fd.setValor_item(Util.roundByDecimal(fd.getValor_item(),2));
                                fd.setValor_unitario(Util.roundByDecimal(fd.getValor_unitario(),2));
                            }
                            else{
                                fd.setValor_item(Math.round(fd.getValor_item()));
                                fd.setValor_unitario(Math.round(fd.getValor_unitario()));
                            }
                            
                            vfd.add(fd);
                        }
                        model.facturaService.setRemesasFacturar(vfd);
                        if(model.facturaService.ingresarFacturaDetalle(model.facturaService.getFactu(), distrito,usuario.getLogin(), base,f).equals(""))
                            next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=La factura no pudo ser creada !!! favor intente de nuevo";
                        else{
                            
                            //eliminar el archivo de texto
                            
                            try{
                                model.facturaService.borrarArchivo( "factura"+usuario.getLogin()+".txt", usuario.getLogin() );
                            }catch(Exception ex){
                                request.setAttribute("mensaje","Error no se pudo borrar el archivo");
                            }
                        }
                    }
                }
                
                /**************************************
                 * condicionalcuando para guardar los items en un
                 * archivo .txt, el cual se encuentra en c:slt/migraciones/
                 ***************************************/
                
                if(guardar.equals("ok")){
                    ////System.out.println("guardar");
                    model.facturaService.setRemesasFacturar(a);
                    try{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp";
                        model.facturaService.escribirArchivo(model.facturaService.getFactu(), model.facturaService.getRemesasFacturar(), "factura"+usuario.getLogin()+".txt", usuario.getLogin());
                        
                    }catch(Exception ex){
                        request.setAttribute("mensaje","Error no se pudo almacenar el archivo");
                    }
                }
                
                
                
            }
            
            
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        ////System.out.println("next:"+next);
        this.dispatchRequest(next);
    }
    
}

