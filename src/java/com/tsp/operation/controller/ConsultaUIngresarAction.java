/******************************************************************
 * Nombre ......................ConsultaUConsultaAction.java
 * Descripci�n..................Clase Action para insertar una consulta
 * Autor........................David lamadrid
 * Fecha........................21/12/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUIngresarAction extends Action{
    
    /** Creates a new instance of ConsultaIngresarAction */
    public ConsultaUIngresarAction () {
    }
     public void run () throws ServletException, InformationException {
        
        String next="";
        try {
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String user=""+usuario.getLogin ();
            //System.out.println("Usuario:"+user);
            model.tablasUsuarioService.tTablasPorUsuario (user);
            Vector inicio =new Vector();
            model.consultaUsuarioService.setVFrom (inicio);
            model.consultaUsuarioService.setVSelect (inicio);
            model.consultaUsuarioService.setFrom ("");
            model.consultaUsuarioService.tConfiguracionesPorUsuario(user);
            next="/jsp/general/consultas/insertarconsultas.jsp?from=&otros=&select=&where=";
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
