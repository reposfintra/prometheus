 /***************************************
    * Nombre Clase ............. RevisionSoftwareAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para la revision de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  12/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;



public class RevisionSoftwareAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {
        
        try{
            HttpSession session  = request.getSession();
            Usuario     usuario  = (Usuario)session.getAttribute("Usuario");
            String      user     = usuario.getLogin();
                  
            String next          = "/jsp/general/inventario/RevisionSoftware.jsp?msj=";
            String msj           = "";
            String opcion        = request.getParameter("evento");
            
            if(opcion!=null){
                opcion = opcion.toUpperCase(); 
                
                if(opcion.equals("LOAD")){  //carga programas                    
                   model.RevisionSoft.loadRequest(request);
                   model.RevisionSoft.searchPrograms(); 
                }
                
                if(opcion.equals("VIEW")){  //Buscar un objeto de la lista
                   String idProgram  = request.getParameter("programa");
                   Programa programa = model.RevisionSoft.getPrograma(idProgram);
                   List listRev      = model.RevisionSoft.searchRevisionesPrograma(programa);
                   List listFile      = model.RevisionSoft.searchFilesPrograma(programa);
                   programa.setListRevisiones( listRev );
                   programa.setListFile(listFile);
                   model.RevisionSoft.setPrograma(programa);
                   request.setAttribute("programa",programa);
                   next          = "/jsp/general/inventario/MostarPrograma.jsp";
                }
                
                if(opcion.equals("REVISIONPROGRAM")){
                     
                    Programa programa  = model.RevisionSoft.getPrograma();
                    
                    String tipoRev     = request.getParameter("tipoRevision");
                    String observacion = request.getParameter("observacion");
                           observacion = this.truncar(observacion, 80);
                    String aprobado    = request.getParameter("aprobacion");
                   
                    Revision revision  = new Revision();
                      revision.setArchivo     (programa.getPrograma() );
                      revision.setEquipo      (programa.getEquipo()   );
                      revision.setSitio       (programa.getSitio()    );
                      revision.setTipoRegistro("P");
                      revision.setUser        (user);
                      revision.setTipoRevision(tipoRev);
                      revision.setObservacion (observacion);
                      revision.setAprovado    (aprobado);  
                      revision.setFecha       ( Utility.getDate(6) );                    
                    model.RevisionSoft.saveRevision(revision);
                    
                    List listRev      = model.RevisionSoft.searchRevisionesPrograma(programa);
                    programa.setListRevisiones( listRev );
                   
                    model.RevisionSoft.setPrograma(programa);
                    request.setAttribute("programa",programa);
                    next          = "/jsp/general/inventario/MostarPrograma.jsp";
                }
                
                
                if(opcion.equals("SEARCHFILE")){
                    Programa programa      = model.RevisionSoft.getPrograma();
                    int id = Integer.parseInt( request.getParameter("archivo") );
                    List listFile          = programa.getListFile();
                    InventarioSoftware inv = (InventarioSoftware) listFile.get(id);
                    
                    model.RevisionSoft.archivo = inv;
                    request.setAttribute("archivo",inv);                    
                    next          = "/jsp/general/inventario/MostarArchivo.jsp";
                    
                }
                
                if(opcion.equals("REVISIONFILE")){
                    InventarioSoftware inv = model.RevisionSoft.archivo;  
                    
                    String tipoRev     = request.getParameter("tipoRevision");
                    String observacion = request.getParameter("observacion");
                           observacion = this.truncar(observacion, 80);
                    String aprobado    = request.getParameter("aprobacion");                  
                    
                     Revision revision  = new Revision();
                       revision.setTipoRegistro("A");
                       revision.setEquipo      ( inv.getEquipo() );
                       revision.setSitio       ( inv.getSitio()  );
                       revision.setArchivo     ( inv.getNombre() );
                       revision.setRuta        ( inv.getRuta()   );
                       revision.setExtension   (inv.getExtension());
                       revision.setFecha       ( Utility.getDate(6) );
                       revision.setUser        (user);
                       revision.setTipoRevision(tipoRev);
                       revision.setObservacion (observacion);
                       revision.setAprovado    (aprobado); 
                       
                    model.RevisionSoft.saveRevision(revision);
                    
                    List listRevFile = model.RevisionSoft.searchRevisionesFile(inv);
                    inv.setRevisiones(listRevFile);                    
                    model.RevisionSoft.archivo = inv;
                    request.setAttribute("archivo",inv);                    
                    next          = "/jsp/general/inventario/MostarArchivo.jsp";                    
                }
                
                
                if(opcion.equals("REFRESCAR")){ 
                    Programa programa  = model.RevisionSoft.getPrograma();                    
                    List listRev       = model.RevisionSoft.searchRevisionesPrograma(programa);
                    List listFile      = model.RevisionSoft.searchFilesPrograma(programa);
                    programa.setListRevisiones( listRev );
                    programa.setListFile(listFile);               
                    model.RevisionSoft.setPrograma(programa);
                    request.setAttribute("programa",programa);
                    next          = "/jsp/general/inventario/MostarPrograma.jsp?swFile=yes";
                }
                
                
                if(opcion.equals("RESET")){  
                   model.RevisionSoft.reset(); 
                }
                
                
            }else
                model.RevisionSoft.reset();
                    
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response); 
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
    }
    
    
    
    
    
     /**
     * M�todos que trunca la observacion para que no se sobrepase en la vista jsp
     * @autor.......fvillacob
     * @version.....1.0.     
     **/ 
    public String truncar(String cadena, int tope)throws Exception{
        StringBuffer sb = new StringBuffer();   
        try{
            int k=0;
            for(int i=0;i<cadena.length();i++){
                String caracter  = cadena.substring(i,i+1);
                if(k==tope){
                    sb.append("<BR>");
                    k=0;
                }
                sb.append( caracter );
                k++;
            }
        }catch (Exception e){
             throw new Exception("truncar: "+e.getMessage());
        }
        return sb.toString();        
    }
    
    
    
    
}
