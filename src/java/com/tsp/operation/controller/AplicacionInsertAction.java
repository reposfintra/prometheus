/********************************************************************
 *      Nombre Clase.................   AplicacionInsertAction.java    
 *      Descripci�n..................   Inserta u nuevo registro en la tabla tblapp    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class AplicacionInsertAction extends Action{
        
        public AplicacionInsertAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblapl/AppInsert.jsp";
                String next = "";
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Aplicacion app = new Aplicacion();
                        app.setBase(usuario.getBase());//request.getParameter("
                        app.setC_codigo(request.getParameter("c_codigo"));
                        app.setC_descripcion(request.getParameter("c_descripcion"));                        
                        app.setDistrito((String) session.getAttribute("Distrito"));
                        app.setUsuario_creacion(usuario.getLogin());
                        app.setUsuario_modificacion(usuario.getLogin());
                        
                        model.appSvc.obtenerAplicacion(app.getDistrito(), app.getC_codigo());
                        Aplicacion existe = model.appSvc.getAplicacion();
                        
                        if ( existe!=null ){
                                if( existe.getEstado().matches("A") ){                                        
                                        app.setEstado("");
                                        model.appSvc.actualizarAplicacion(app);
                                        pag += "?msg=Se ha ingresado la plicaci�n correctamente";
                                        model.appSvc.setAplicacion(null);
                                }
                                else{
                                        model.appSvc.setAplicacion(app);
                                        pag += "?msg=No se puede procesar la informaci�n. Ya existe la aplicaci�n.";
                                }                                
                        }
                        else{
                                model.appSvc.insertarAplicacion(app);
                                pag += "?msg=Se ha ingresado la aplicaci�n correctamente";
                                model.appSvc.setAplicacion(null);
                        }
                        
                        next = com.tsp.util.Util.LLamarVentana(pag, "Ingresar Aplicaci�n");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
