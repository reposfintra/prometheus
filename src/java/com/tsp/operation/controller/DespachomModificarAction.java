/*
 * DespachomModificarAction.java
 *
 * Created on 22 de noviembre de 2005, 10:34 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachomModificarAction extends Action {
    
    /** Creates a new instance of DespachomModificarAction */
    public DespachomModificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        try {
            String d=""+request.getParameter("d");
            String p=""+request.getParameter("p");
            String f=""+request.getParameter("f");
            
            model.despachoManualService.despachoPorCodigo(d, p, f);
            next="/jsp/trafico/despacho_manual/modificar.jsp?sw=2";
        }
        catch (Exception e) {
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
