/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class RemesaDoctoAplicarAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public RemesaDoctoAplicarAction() {
    }
    static Logger logger = Logger.getLogger(RemesaDoctoAplicarAction.class);
    
    public void run() throws ServletException, InformationException {
        
        String next="/docremesas/AgregarDocRel.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        logger.info( "*************************************************************");
        logger.info( "-----------------USUARIO : "+usuario.getLogin());
        logger.info( "ACCION PARA DOCUMENTOS INTERNOS... ");
        if(request.getParameter("generar")!=null){
            
            logger.info( "SE VA A MOSTRAR EL VECTOR...");
            Vector vec = model.RemDocSvc.getDocumentos();
            //    logger.info(" DESTINATARIO    " + request.getParameter("destinatario") );
            if(vec==null){
                logger.info("El vector es nulo");
                vec = new Vector();
                for(int i=0; i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setDestinatario(request.getParameter("destinatario"));
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                    Vector hijos = new Vector();
                    remesa_docto remH = new remesa_docto();
                    remH.setTipo_doc("");
                    remH.setDocumento("");
                    rem.setHijos(hijos);
                    vec.add(rem);
                }
                logger.info("Tama�o del nuevo vector con elementos vacios "+vec.size());
                model.RemDocSvc.setDocumentos(vec);
                
                // logger.info("Tama�o del vector "+vec.size());
                
                if(request.getParameter("ocargue")!=null){
                    logger.info("ENTRE PORQ ME MANDARON ORDEN DE CARGA");
                    if(request.getParameter("numrem")!=null){
                        logger.info("DOCUMENTOS DEL DOCUMENTO "+request.getParameter("numrem").toUpperCase());
                        try{
                            if(model.remesaService.getRemesa()!=null){
                                logger.info("la remesa es distinta de null");
                                if(model.remesaService.getRemesa().getTipo_documento().equals("OCARGUE")){
                                    
                                    logger.info("Listar docs de REMESA");
                                    model.RemDocSvc.LISTVC(request.getParameter("numrem").toUpperCase(), request.getParameter("destinatario"));
                                    
                                    
                                }else{
                                    logger.info("Listar docs de ORDEN DE CARGA");
                                    model.RemDocSvc.listarDocOCargue(request.getParameter("numrem").toUpperCase(), request.getParameter("destinatario"));
                                    
                                }
                                vec = model.RemDocSvc.getVec();
                                logger.info("TAMANO DEL VECTOR DESPUES DE BUSCAR "+vec.size());
                            }
                        }catch (Exception ex){
                            throw new ServletException(ex.getMessage());
                        }
                    }
                }
                
                
                model.RemDocSvc.setDocumentos(vec);
            }else{
                logger.info("El vector no es nulo");
            }
            if(vec.size()<5){
                Vector vec1 = new Vector();
                for(int i=vec.size(); i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setDestinatario(request.getParameter("destinatario"));
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                    Vector hijos = new Vector();
                    remesa_docto remH = new remesa_docto();
                    remH.setTipo_doc("");
                    remH.setDocumento("");
                    rem.setHijos(hijos);
                    vec.add(rem);
                }
            }
        }else{
            logger.info( "SE VA A ACTUALIZAR EL VECTOR...");
            
            Vector vec = model.RemDocSvc.getDocumentos();
            logger.info("Cantidad de Documentos encontrados: " + vec.size() );
            int cant = 0;
            String destinatario = request.getParameter("destinatario");
            logger.info("DETINATARIO " + destinatario );
            Vector nuevo = new Vector();
            for(int i = 0; i<vec.size();i++){
                remesa_docto rem = (remesa_docto) vec.elementAt(i);
                String destrem = rem.getDestinatario()!=null?rem.getDestinatario():"";
                if(!destrem.equals(destinatario)){    
                    nuevo.add(rem);
                    logger.info("SE AGREGO: "+rem.getDocumento()+"/"+rem.getDocumento_rel());
                }
            }
            logger.info("Despues de remover el vector quedo con: " + vec.size() );
            
            logger.info( "Parametros recibidos");
            
            vec = nuevo;
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames(); // Leemos todos los atributos del request
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                //logger.info( "Usuario: "+usuario.getLogin()+" Parametro: "+parametro+" Valor: "+request.getParameter(parametro));
                if(parametro.indexOf("_")<0){
                    if(parametro.substring(0,2).equals("dp")){
                        if(!request.getParameter(parametro).equals("")){
                            
                            int numero =Integer.parseInt( parametro.substring(2));
                            
                            //SE ENCONTRARON LOS PADRES, SE CREAUN OBJETO DE TIPO REMESA DOC QUE OBTIENE LOS DATOS DEL PADRE
                            logger.info("Nombre del padre = "+parametro);
                            logger.info("Numero "+numero);
                            logger.info("Documento padre = "+request.getParameter(parametro));
                            
                            remesa_docto rem = new remesa_docto();
                            rem.setDestinatario(destinatario);
                            rem.setTipo_doc(request.getParameter("tp"+numero));
                            rem.setDocumento(request.getParameter(parametro));
                            
                            Vector hijos = new Vector();
                            // RECORRO NUEVAMENTE LOS PARAMETROS PARA OBTENER LOS HIJOS DE ESTE OBJETO
                            java.util.Enumeration enum2;
                            String parametro2;
                            enum2 = request.getParameterNames(); // Leemos todos los atributos del request
                            while (enum2.hasMoreElements()) {
                                parametro2 = (String) enum2.nextElement();
                                //logger.info("NOMBRE PARAM = "+parametro2);
                                if(parametro2.indexOf("tp"+numero+"_TipodocRel")>=0){
                                    logger.info("ES UN HIJO!! "+parametro2);
                                    String vecS [] = parametro2.split("_");
                                    String numeroH = vecS[1].substring(10);
                                    String hijo2 = "tp"+numero+"_DocumentoRel"+numeroH;
                                    logger.info("Este es un hijo valor: "+request.getParameter(hijo2));
                                    remesa_docto remH = new remesa_docto();
                                    remH.setTipo_doc(request.getParameter(parametro2));
                                    remH.setDocumento(request.getParameter(hijo2));
                                    hijos.add(remH);
                                    
                                }
                            }
                            rem.setHijos(hijos);
                            vec.add(rem);
                            cant++;
                        }
                    }
                    
                }
                
                
            }
            for(int i = 0; i<vec.size();i++){
                remesa_docto rem = (remesa_docto) vec.elementAt(i);
                if(rem.getDocumento().equals("")){
                    logger.info("Se removio la posicion "+i);
                    vec.remove(i);
                }
            }
            
            logger.info("El tamano del vector es "+vec.size());
            /*if(vec.size()<5){
                for(int i=vec.size(); i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setDestinatario(request.getParameter("destinatario"));
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                    Vector hijos = new Vector();
                    remesa_docto remH = new remesa_docto();
                    remH.setTipo_doc("");
                    remH.setDocumento("");
                    rem.setHijos(hijos);
                    vec.add(rem);
                }
             
               // this.dispatchRequest(next);
            }*/
            model.RemDocSvc.setDocumentos(vec);
            next="/docremesas/AgregarDocRel.jsp?cerrar=ok";
        }
        
        this.dispatchRequest(next);
    }
    
}
