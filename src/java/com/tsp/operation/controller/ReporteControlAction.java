/******************************************************************************
 * Nombre clase :      ReporteControlAction.java                              *
 * Descripcion :       Action del ReporteControlAction.java                   *
 * Autor :             LREALES                                                *
 * Fecha :             16 de septiembre de 2006, 09:48 AM                     *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 *****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.operation.model.threads.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.finanzas.contab.model.services.*;
import javax.servlet.http.*;
import com.tsp.util.Contabilidad.HiloReporteControl;

public class ReporteControlAction  extends Action {
    
    /** Creates a new instance of ReporteControlAction */
    public ReporteControlAction() { }    
    
    public void run() throws ServletException {
        
        String next = "/jsp/finanzas/contab/reporte_control/ReporteControl.jsp?msg=Proceso Ejecutandose Exitosamente!";
        
        HttpSession session = request.getSession();
        Usuario usu = (Usuario) session.getAttribute("Usuario");        
        String usuario = "" + usu.getLogin();
        
        String archivo = (request.getParameter("archivo") != null)?request.getParameter("archivo"):"";
               
        String fecha_inicial = archivo != ""?archivo.substring(16,24):"";
        String fecha_final = archivo != ""?archivo.substring(25,33):"";                
              
        fecha_inicial = fecha_inicial.substring(0,4) + "-" + fecha_inicial.substring(4,6) + "-" + fecha_inicial.substring(6,8);
        fecha_final = fecha_final.substring(0,4) + "-" + fecha_final.substring(4,6) + "-" + fecha_final.substring(6,8);        
        
        try{
            
            // llamo a Reporte Control
            HiloReporteControl hrc = new HiloReporteControl();
            hrc.leer( usuario, archivo, fecha_inicial, fecha_final );
            
        } catch ( Exception e ){
            
            e.printStackTrace();
            
        }
        
        this.dispatchRequest( next );
                        
    }
    
}