/*
 * CruceAYSProcesoAction.java
 *
 * Created on 26 de junio de 2005, 11:56 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Sandrameg
 */
public class CruceAYSProcesoAction extends Action {
    
    /** Creates a new instance of CruceAYSProcesoAction */
    public CruceAYSProcesoAction() {
    }
    
    public void run() throws ServletException,InformationException{
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        
        CruceAYSThread cays = new CruceAYSThread();
        cays.star( u.getLogin());
        
        String msg = "Proceso iniciado!";
        String next = "/pags_predo/cruce.jsp?msg=" + msg;        
        this.dispatchRequest(next);
    }
}
