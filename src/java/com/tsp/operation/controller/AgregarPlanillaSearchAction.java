/*
 * RemesaSearchAction.java
 *
 * Created on 14 de diciembre de 2004, 01:02 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class AgregarPlanillaSearchAction extends Action{
    
    /** Creates a new instance of RemesaSearchAction */
    public AgregarPlanillaSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String usuario_genero = "";
        
        String next="/colpapel/agregarPlanilla.jsp";
        String no_rem= request.getParameter("remesa").toUpperCase();
        String base="";
        if(request.getParameter("existe")!=null){
            next="/colpapel/RemesaExistente.jsp";
        }
        if(request.getParameter("lista")!=null){
            next="/colpapel/agregarListaPlanilla.jsp";
        }
        try{
            model.remesaService.buscaRemesa(no_rem);
            if(request.getParameter("placol")!=null){
                model.remesaService.buscaRemesa(no_rem);
            }
            if(model.remesaService.getRemesa()!=null){
                
                Remesa rem=model.remesaService.getRemesa();
                
                
                base = rem.getBase();
                usuario_genero = rem.getUsuario();
                
                model.sjextrafleteService.listaCostos(rem.getStdJobNo(),usuario.getId_agencia());
                model.sjextrafleteService.listaFletes(rem.getStdJobNo(),usuario.getId_agencia());
                
                model.movplaService.setLista(null);
                model.stdjobcostoService.searchRutas(rem.getStdJobNo());
                model.anticiposService.vecAnticipos("",rem.getStdJobNo());
                model.anticiposService.searchAnticiposProveedor(usuario);
                model.cliente_ubicacionService.listarUbicaciones("000"+rem.getStdJobNo().substring(0,3));
                String nombreR="";
                String remitentes[]= rem.getRemitente().split(",");
                for(int i=0; i<remitentes.length; i++){
                    nombreR=model.remidestService.getNombre(remitentes[i])+",";
                }
                String nombreD="";
                String destinatarios[]= rem.getDestinatario().split(",");
                for(int i=0; i<destinatarios.length; i++){
                    nombreD=model.remidestService.getNombre(destinatarios[i])+",";
                }
                
                rem.setRemitente(nombreR);
                rem.setDestinatario(nombreD);
                model.remesaService.setRemesa(rem);
                request.setAttribute("remesa",rem);
                
                model.remplaService.buscaRemPlaR(no_rem);
                if(model.remplaService.getRemPla()!=null){
                    RemPla rempla = model.remplaService.getRemPla();
                    String planilla= rempla.getPlanilla();
                    
                    model.planillaService.bucarColpapel(planilla);
                    if(model.planillaService.getPlanilla()!=null){
                        
                        Planilla pla = model.planillaService.getPlanilla();
                        String nomruta= model.planillaService.getRutas(pla.getNumpla());
                        pla.setCodruta(pla.getRuta_pla());
                        pla.setRuta_pla(nomruta);
                        request.setAttribute("planilla",pla);
                        
                        model.movplaService.buscaMovpla(planilla,"01");
                        if(model.movplaService.getMovPla()!=null){
                            
                            Movpla movpla= model.movplaService.getMovPla();
                            request.setAttribute("movpla",movpla);
                        }
                        List remesas = model.planillaService.buscarRemesas(planilla);
                        request.setAttribute("remesas" , remesas);
                    }
                }
                
                List planillas= model.remesaService.buscarPlanillas(no_rem);
                List lplanillas= new LinkedList();
                Iterator it=planillas.iterator();
                while (it.hasNext()){
                    Planilla pla = (Planilla) it.next();
                    String nomruta= model.planillaService.getRutas(pla.getNumpla());
                    pla.setCodruta(pla.getRuta_pla());
                    pla.setRuta_pla(nomruta);
                    lplanillas.add(pla);
                }
                request.setAttribute("planillas",lplanillas);
                String mensaje ="";
                
                if("".equalsIgnoreCase(base)){
                    mensaje ="La remesa que selecciono no fue elaborada por la web.";
                    model.planillaService.setPlanilla(null);
                    model.remesaService.setRemesa(null);
                    model.remesaService.setRm(null);
                    request.setAttribute("planillas",null);
                    request.setAttribute("remesas" , null);
                    request.setAttribute("movpla",null);
                    request.setAttribute("planilla",null);
                    request.setAttribute("remesa",null);
                    next= next + "?mensaje="+mensaje;
                }
                
                
                
            }
            
            
            else{
                String mensaje = "La remesa "+no_rem+" no existe o esta anulada";
                
                next="/colpapel/agregarPlanilla.jsp?mensaje="+mensaje;
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
