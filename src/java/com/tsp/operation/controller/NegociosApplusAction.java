/*
 * NegociosApplusAction.java
 * Created on 2 de febrero de 2009, 17:32
 */
package com.tsp.operation.controller;
import com.google.gson.Gson;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.NegocioApplus;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HGenerarFacturaFormulaPro;
import com.tsp.operation.model.threads.HSendMail2;
import com.tsp.util.Util;
import java.io.*;
import java.util.*;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.*;

import javax.servlet.http.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
/** * @author  Fintra */
public class NegociosApplusAction  extends Action  {
    /** Creates a new instance of NegociosApplusAction */
    String loginx;
    String next;
    //numero de solicitud
    String numSolicitud; 
    public NegociosApplusAction() {

    }

    public void run() throws ServletException, InformationException {
        //ystem.out.println("cambiarEstado"+request.getParameter("opcion"));
        HttpSession session = request.getSession();
        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = session.getAttribute("Distrito").toString();
        String                      msjRecepcionObra = "Recepcion pendiente.";
        boolean isJsonResponse = false;
        if(request.getParameter("opcion").equals("clselect"))
        {   String filtro=request.getParameter("filtro");
            String nomselect=request.getParameter("nomselect");

            try{
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(model.negociosApplusService.getClientesEca(filtro,nomselect,((request.getParameter("idclie")!=null)?request.getParameter("idclie"):"")));
            }
            catch(Exception e)
            {   e.printStackTrace();
                System.out.println(e.getMessage());

            }
        }
        else
        {
            loginx=usuario.getLogin();

            RequestDispatcher rd ;
            next = "";
            String estado_consultar=request.getParameter("estado_consultar");
            String estado_asignar=request.getParameter("estado_asignar");
            String observacion=request.getParameter("observacion");
            String esquema_comision=request.getParameter("esquema_comision");
            String svx=request.getParameter("svx");
            String contratista_consultar=request.getParameter("contratista_consultar");
            String fact_conformed=request.getParameter("fact_conformed");
            String num_osx=request.getParameter("num_osx");
            String cuoticas=request.getParameter("cuoticas");
            String fecfaccli=request.getParameter("fecfaccli");
            String fact_conformada_consultar=request.getParameter("fact_conformada_consultar");
            String esquema_financiacion=request.getParameter("esquema_financiacion");
            //numero de solicitud
            numSolicitud = (String) session.getAttribute("numSolicitud");
          
            //ystem.out.println("en actionnnnnnnnnnnnnn"+contratista_consultar);
            try{
                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("cambiarEstado")){

                    HSendMail2 hSendMail3=new HSendMail2();//090919
                    ArrayList archivillos=new ArrayList();//090919
                    String[] archi1={"C:\\test.gif","archix1"};//090919
                    String[] archi2={"C:\\test2.gif","archix2"};//090919
                    archivillos.add(archi1);//090919
                    archivillos.add(archi2);//090919
    /*                hSendMail3.start("imorales@fintravalores.com",
                        "imorales@fintravalores.com", "", "",
                        "asuntillo",
                        "textillo \n remix",loginx,archivillos);//090919*/

                    String[] negocios  =  request.getParameterValues("idNegocio");

                    //inicio de 090715
                    ArrayList accords=model.negociosApplusService.obtainAccords(negocios);

                    for (int k=0;k<accords.size();k++){
                        NegocioApplus na=(NegocioApplus)accords.get(k);
                        if (!(na.getIdEstado().equals(estado_asignar))){

                            if (estado_asignar.equals("96")){
                                String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());
                                HSendMail2 hSendMail2=new HSendMail2();
                                hSendMail2.start("imorales@fintravalores.com",
                                    model.electricaribeVerSvc.getEmailContratistaApp(na.getIdContratista()), "", "",
                                    "entrega al contratista",
                                        //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                    ""+model.electricaribeVerSvc.getNombreContratistaApp(na.getIdContratista())+": Se debe realizar visita al cliente "+na.getNombreCliente()+" "+na.getIdCliente()+" ("+dat_cli[7]+" "+dat_cli[5]+") del " +
                                    "multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx);
                            }

                            if (estado_asignar.equals("4") && k==0){
                                //String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());
                                HSendMail2 hSendMail2=new HSendMail2();
                                hSendMail2.start("imorales@fintravalores.com",
                                    model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "aprobado financiado",
                                        //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                    "APPLUS: Ha sido aprobada la financiaci�n de "+na.getNumOs()+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
                                        //+"Se debe realizar visita al cliente "+na.getNombreClient()+" "+na.getIdCliente()+" ("+dat_cli[7]+") del " +"multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx);
                            }

                            if (estado_asignar.equals("6")){
                                //String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());

                                HSendMail2 hSendMail2=new HSendMail2();
                                hSendMail2.start("imorales@fintravalores.com",
                                    model.electricaribeVerSvc.getEmailContratistaApp(na.getIdContratista()), "", "",
                                    "orden de trabajo",
                                        //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                    ""+model.electricaribeVerSvc.getNombreContratistaApp(na.getIdContratista())+": Se ha pasado a estado 'orden de trabajo' de "+na.getNumOs()+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
                                        //+"Se debe realizar visita al cliente "+na.getNombreClient()+" "+na.getIdCliente()+" ("+dat_cli[7]+") del " +"multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx);
                            }

                            if (estado_asignar.equals("7") && k==0){
                                //String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());
                                HSendMail2 hSendMail2=new HSendMail2();
                                hSendMail2.start("imorales@fintravalores.com",
                                    model.electricaribeVerSvc.getEmailEntidad("ECA"), "", "",
                                    "recepcion de obra",
                                        //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                    "ECA: Se ha cambiado el "+na.getNumOs()+" al estado Recepci�n de Obra. Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
                                        //+"Se debe realizar visita al cliente "+na.getNombreClient()+" "+na.getIdCliente()+" ("+dat_cli[7]+") del " +"multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx);
                            }

                            if ((estado_asignar.equals("1") || estado_asignar.equals("2")) && k==0){
                                HSendMail2 hSendMail2=new HSendMail2();
                                hSendMail2.start("imorales@fintravalores.com",
                                    model.electricaribeVerSvc.getEmailEntidad("FINTRA"), "", "",
                                    "en estudio",
                                    "FINTRA: Se ha cambiado el "+na.getNumOs()+" al estado En Estudio. Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);

                            }

                        }
                    }
                    //fin de 090715

                    model.negociosApplusService.CambiarEstado(negocios,estado_asignar,observacion,esquema_comision,svx,contratista_consultar,fact_conformed,cuoticas,fecfaccli,fact_conformada_consultar,esquema_financiacion,loginx);
                    //ystem.out.println("contratista_consultar"+contratista_consultar);
                    next="/jsp/applus/cambiar_estado_negocios.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar;
                }
                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarEstado")){
                    if (estado_consultar==null || estado_consultar.equals("")){
                        estado_consultar="2";
                    }

                    next="/jsp/applus/cambiar_estado_negocios.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar;
                    if (request.getParameter("caso").equals("applus")){//090428
                        next="/jsp/applus/partedcambiar_estado_negocios.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar;
                    }
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("asignarEstado")){ //
                    if (estado_consultar==null ){
                        estado_consultar="0";
                    }
                    String nic_consultable=request.getParameter("nicc");
                    String nomcli_consultable=request.getParameter("nomclie");
                    String solici_consultable=request.getParameter("id_solici");
                    String estado_asignable=request.getParameter("estado_asignable");

                    String[] acciones  =  request.getParameterValues("idAcc");

                    String asignacionEstado=model.negociosApplusService.asignarEstado(loginx,estado_asignable,acciones);

                    next="/jsp/applus/cambiar_estado_negocios2.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar+
                        "&id_solici="+solici_consultable+"&nicc="+nic_consultable+"&nomclie="+nomcli_consultable;
                }



                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("subirArchivo")){
                    //ystem.out.println("numerito"+request.getParameter("num_osx"));
                    next=guardarArchivoEnDirectorio( );
                    //next="/jsp/applus/importar.jsp";
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("nombres")){
                    String numerit_os=request.getParameter("numerit_os");
                    String tipito=request.getParameter("tipito");
                    //ystem.out.println("numeritooo"+numerit_os);

                    next="/jsp/applus/mostrar_archivos.jsp?num_osx="+numerit_os+"&tipito="+tipito;
                }                
                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarCodigo")){
                    ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                    List fileItemsList = servletFileUpload.parseRequest(request);

                    //// Itero para obtener todos los FileItem
                    Iterator it = fileItemsList.iterator();
                    String nombre_archivo;
                    String fieldtem="",valortem="",idxx="na";
                    String secuenciax="na";
                    String tipitox="na";
                    while (it.hasNext()){
                        FileItem fileItem = (FileItem)it.next();
                        if (fileItem.isFormField()){

                            fieldtem = fileItem.getFieldName();
                            //ystem.out.println("fieldtem"+fieldtem);
                            valortem = fileItem.getString();
                            if (fieldtem.equals("numerit_osx")){
                                idxx=valortem;//request.getParameter("");
                            }
                            if (fieldtem.equals("tipito")){
                                tipitox=valortem;
                            }
                            if (fieldtem.equals("secuenciax")){
                                secuenciax=valortem;//request.getParameter("");
                                //ystem.out.println("secuenciax"+secuenciax);
                            }


                        }
                    }
                    String[] archivosx=model.negociosApplusService.getArchivo(secuenciax,loginx,tipitox);
                    //ystem.out.println("numerit_osxremix"+request.getParameter("numerit_os")+"idxx"+idxx);

                    if (archivosx!=null && archivosx.length>0){
                        next="/jsp/applus/mostrar_archivos.jsp?archivolisto="+archivosx[0]+"&secuenciax="+secuenciax;
                    }else{
                        next="/jsp/applus/mostrar_archivos.jsp?num_osx="+idxx+"&tipito="+tipitox;
                    }


                }
                
                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarArchivo")){
                    ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                    List fileItemsList = servletFileUpload.parseRequest(request);
                    
                    //// Itero para obtener todos los FileItem
                    Iterator it = fileItemsList.iterator();                    
                    String fieldtem="",valortem="",idxx="";
                    String nomarchivo="";                   
                    while (it.hasNext()){
                        FileItem fileItem = (FileItem)it.next();
                        if (fileItem.isFormField()){

                            fieldtem = fileItem.getFieldName();
                            //ystem.out.println("fieldtem"+fieldtem);
                            valortem = fileItem.getString();
                            if (fieldtem.equals("numerit_osx")){
                                idxx=valortem;//request.getParameter("");
                            }
                         
                            if (fieldtem.equals("nomarchivo")){
                                nomarchivo=valortem;
                            }


                        }
                    }
   
                    boolean archivoCargado = model.negociosApplusService.almacenarArchivoEnCarpetaUsuario(idxx,loginx,nomarchivo);
                 
                    if (archivoCargado){
                        next="/jsp/applus/mostrar_archivos.jsp?archivolisto="+nomarchivo;
                    }else{
                        next="/jsp/applus/mostrar_archivos.jsp?num_osx="+idxx;
                    }

                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("borrar")){
                    String carpeta="images/multiservicios";
                    ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

                    String directorioArchivos = rb.getString("ruta")+"/"+carpeta+"/"+loginx;//se establece la ruta de la imagen

                    File carpetica =new File (directorioArchivos)    ;

                    deleteDirectory(carpetica);

                    next="/jsp/applus/mostrar_archivos.jsp?cerrar=si";
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("borrar2")){
                    String carpeta="images/multiservicios";
                    ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

                    String directorioArchivos = rb.getString("ruta")+"/"+carpeta+"/"+loginx;//se establece la ruta de la imagen

                    File carpetica =new File (directorioArchivos)    ;

                    deleteDirectory(carpetica);

                    next="/jsp/applus/importar.jsp?cerrar=si";
                }


                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("cxp_formula_prov")){

                    HGenerarFacturaFormulaPro hilo = new HGenerarFacturaFormulaPro();
                    hilo.start(model, usuario, dstrct);

                    next="/jsp/applus/facturaFormulaCrearPro.jsp?msj=Proceso Iniciado.";
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarEstado2")){ //090922
                    if (estado_consultar==null ){
                        estado_consultar="0";
                    }
                    String nic_consultable=request.getParameter("nicc");
                    String nomcli_consultable=request.getParameter("nomclie");
                    String idclie=request.getParameter("idclie");
                    String solici_consultable=request.getParameter("id_solici");

                    next="/jsp/applus/cambiar_estado_negocios2.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar+
                        "&id_solici="+solici_consultable+"&nicc="+nic_consultable+"&nomclie="+nomcli_consultable+"&id_cliente="+idclie;
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarAccion")){ //
                    String solicitud_consultable=request.getParameter("id_solici");
                    String id_accion=request.getParameter("id_accion");
                    //model.negociosApplusService.obtainAccione(solicitud_consultable,id_accion,loginx);

                    //next="/jsp/applus/acciones2.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx;
                    next="/jsp/applus/alcance_acciones.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx;
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("marcarTipoTrabajo")){ //
                    String solicitud_consultable=request.getParameter("id_solici");
                    String id_accion=request.getParameter("id_accion");
                    String tipotraba=request.getParameter("tipotraba");
                    String observaci=request.getParameter("observaci");

                    String extipotraba=request.getParameter("extipotraba");

                    String fecvisitaplan=request.getParameter("fecvisitaplan");//091030
                    String fecvisitareal=request.getParameter("fecvisitareal");//091030

                    String marcaTipoTrabajo=model.negociosApplusService.marcarTipoTrabajo(solicitud_consultable,id_accion,loginx,tipotraba,observaci,extipotraba,fecvisitaplan,fecvisitareal);//091030

                    next="/jsp/applus/alcance_acciones.jsp?id_solici=" + solicitud_consultable+
                        "&id_acci="+id_accion+"&usuari="+loginx+"";

                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("asignar")){ //
                    if (estado_consultar==null ){
                        estado_consultar="0";
                    }
                    String nic_consultable=request.getParameter("nicc");
                    String nomcli_consultable=request.getParameter("nomclie");
                    String solici_consultable=request.getParameter("id_solici");
                    String fecof=request.getParameter("fecof");

                    String[] solicitudes  =  request.getParameterValues("idNegocio");

                    String asignacion=model.negociosApplusService.asignar(loginx,fecof,solicitudes);

                    next="/jsp/applus/cambiar_estado_negocios2.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar+
                        "&id_solici="+solici_consultable+"&nicc="+nic_consultable+"&nomclie="+nomcli_consultable;
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarAlcances")){ //
                    String solicitud_consultable=request.getParameter("id_solici");
                    String id_accion=request.getParameter("id_accion");

                    next="/jsp/applus/alcance_acciones.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx;
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("marcarAlcances")){ //
                    String solicitud_consultable=request.getParameter("id_solici");
                    String id_accion=request.getParameter("id_accion");

                    String alcanci=request.getParameter("alcanci");
                    String adicioni=request.getParameter("adicioni");
                    String trabaji=request.getParameter("trabaji");

                    String marcaAvance=model.negociosApplusService.marcarAvance(solicitud_consultable,id_accion,loginx,alcanci,adicioni,trabaji);

                    next="/jsp/applus/alcance_acciones.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx;
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarprod")){ //
                    String idmat=request.getParameter("idmat");
                    next="/jsp/delectricaribe/cotizacion/modificar_producto.jsp?materiale="+idmat;
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("recibirObra")){ //
                    String solicitud=request.getParameter("solicitud");
                    BeanGeneral dat_recep_obra=model.negociosApplusService.obtainDatRecepObra(solicitud);
                    next="/jsp/delectricaribe/recepcion_obra.jsp?nomcli="+dat_recep_obra.getValor_01()+"&consecutivo="+dat_recep_obra.getValor_02()+
                            "&num_os="+dat_recep_obra.getValor_03()+"&solicitud="+solicitud;
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarrecibirObra")){ //

                    try{

                        String solicitud=request.getParameter("solicitud");

                        String[] observaciones =request.getParameterValues("observ");
                        String[] acciones =request.getParameterValues("accione");

                        String[] fecFin =request.getParameterValues("fecFin");
                        String[] fecInterventor =request.getParameterValues("fecInterventor");

                        for (int i=0;i<observaciones.length;i++){
                            System.out.println("observaciones:"+observaciones[i]+"_acciones:"+acciones[i]+"_fecFin:"+fecFin[i]+"_fecInterventor:"+fecInterventor[i]);
                        }

                        String respuesta=model.negociosApplusService.aceptarRecepObra(solicitud,observaciones,acciones,fecFin,fecInterventor,loginx);
                        msjRecepcionObra=respuesta;

                        BeanGeneral dat_recep_obra=model.negociosApplusService.obtainDatRecepObra(solicitud);
                        next="/jsp/delectricaribe/recepcion_obra.jsp?nomcli="+dat_recep_obra.getValor_01()+"&consecutivo="+dat_recep_obra.getValor_02()+
                                "&num_os="+dat_recep_obra.getValor_03()+"&solicitud="+solicitud;
                    }catch(Exception e){
                        msjRecepcionObra="Error recibiendo.";
                        System.out.println("error en acion recibiendo obra:"+e.toString()+"__"+e.getMessage());
                        e.printStackTrace();
                    }
                }
                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarArchivos")){
                    String numerit_os =request.getParameter("numerit_os");
                    String tipito =request.getParameter("tipito");
                    String idCategoria =request.getParameter("categoria");
                    String tipoPersona =request.getParameter("tipoPersona");
                    
                    boolean cargados = true;                  
                    List<Map<String, String>> urlArchivos = model.negociosApplusService.getArchivosCredito(idCategoria, numerit_os.split("-")[0], numerit_os.split("-")[1], tipito, tipoPersona);
                    
                    if (cargados) {
                        next = "";
                        response.getOutputStream().print(new Gson().toJson(urlArchivos));
                        isJsonResponse = true;
                    }                    
                }

                //inicio de 20100222
                try {
                    if (isJsonResponse) {
                        response.setContentType("application/json;charset=utf-8");
                    } else {
                        response.setContentType("text/plain; charset=utf-8");
                    }
                    response.setHeader("Cache-Control", "no-cache");
                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarrecibirObra")){
                        response.getWriter().println(msjRecepcionObra);
                    }
                }
                catch (Exception ex) {
                    System.out.println("error en negapplusaction:"+ex.toString()+"__"+ex.getMessage());
                    ex.printStackTrace();
                }
                //fin de 20100222



            }catch(Exception ee){
                System.out.println("eee"+ee.toString()+ee.getMessage());
                next = ee.toString()+ee.getMessage();
            }
            try{
                if (!isJsonResponse){
                System.out.println("nexttty"+next);
                    rd = application.getRequestDispatcher(next);

                    if(rd == null){
                        System.out.println("no se encontro"+next);
                        throw new ServletException("No se pudo encontrar "+ next);
                    }
                    System.out.println("request"+request+"response"+response);
                    if (!(request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarrecibirObra"))){ //20100222
                        rd.forward(request, response);
                    }//20100222
                }
            } catch ( Exception e){
                System.out.println("error en run de action..."+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
                throw new ServletException("Error en NegociosApplusAction en  run.....\n"+e.getMessage());
           }
        }
    }





  public String guardarArchivo(){
    String num_osx="";
    String respuesta="";
    try{

        String carpeta="images/multiservicios";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

        String directorioArchivos = rb.getString("ruta")+"/"+carpeta+"/"+loginx;//se establece la ruta de la imagen

        File carpetica =new File (directorioArchivos)    ;

        //boolean borrar =carpetica.delete();
        deleteDirectory(carpetica);
        //ystem.out.println("borrar::"+borrar);

        this.createDir(directorioArchivos );
        if (ServletFileUpload.isMultipartContent(request)){

            ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
            List fileItemsList = servletFileUpload.parseRequest(request);

            //// Itero para obtener todos los FileItem
            Iterator it = fileItemsList.iterator();
            String nombre_archivo;
            String fieldtem="",valortem="",idxx="";
            String tipitoxx="";

            String multixx="",contratixx="",finarchivo="";//090715

            while (it.hasNext()){
                FileItem fileItem = (FileItem)it.next();
                if (fileItem.isFormField()){

                    fieldtem = fileItem.getFieldName();
                    //ystem.out.println("fieldtem"+fieldtem);
                    valortem = fileItem.getString();
                            if (fieldtem.equals("numerit_osx")){
                                num_osx=valortem;//request.getParameter("conductor_foto");
                                next="/jsp/applus/importar.jsp?num_osx="+num_osx+"&Mensaje=Archivo Cargado.";
                                //next=next+"?num_osx="+num_osx;

                                //ystem.out.println("nexttyyy"+next);
                            }
                            if (fieldtem.equals("idxx")){
                                idxx=valortem;//request.getParameter("");
                            }

                            if (fieldtem.equals("tipito")){
                                tipitoxx=valortem;//request.getParameter("");
                                next=next+"&tipito="+tipitoxx+"&form="+numSolicitud+"&tipoconv=Consumo&vista=6";
                            }


                            if (fieldtem.equals("multixx")){//090715
                                multixx=valortem;
                            }

                            if (fieldtem.equals("contratixx")){//090715
                                contratixx=valortem;
                            }

                            if (fieldtem.equals("finarchivo")){//090715
                                finarchivo=valortem;
                            }

                            if (fieldtem.equals("swmsgx")){//090715
                                //System.out.println("valortem22"+valortem+"contratixx"+contratixx);
                                if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("OFERTA")){

                                    HSendMail2 hSendMail20=new HSendMail2();
                                    hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "oferta",
                                    "APPLUS:  Se ha montado oferta de "+multixx +" , contratista "+contratixx+
                                                        " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);

                                }


                                if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("FINAL")){

                                  /*  HSendMail2 hSendMail20=new HSendMail2();
                                    hSendMail2.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "acta final",
                                    "APPLUS:  Se ha montado acta de finalizaci�n de "+multixx +
                                                        " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
*/
                                }

                                if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("CREDITO")){

                                    HSendMail2 hSendMail20=new HSendMail2();
                                    hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "credito",
                                    "APPLUS:  Se ha montado el contrato de credito de "+multixx +
                                                        " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);

                                }

                            }

                }
                if (!(fileItem.isFormField())){
                    String nombreCampo = fileItem.getFieldName();
                    //// Tama�o de archivo en bytes
                    long tamanioArchivo = fileItem.getSize();

                    //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                    //// incluyen el path completo, lo que puede implicar separar path
                    //// de nombre.
                    String nombreArchivo = fileItem.getName();

                    //// Content type (tipo MIME) del archivo.
                    //// Esta informaci�n la proporciona el navegador del cliente.
                    //// Algunos ejemplos: .jpg = image/jpeg, .txt = text/plain
                    String contentType = fileItem.getContentType();

                    //// Obtengo caracteristicas de campo y archivo
                    //ystem.out.println( "<p>--> Name:" + nombreCampo + "</p>");
                    //ystem.out.println( "<p>--> Tama�o archivo:" + tamanioArchivo + "</p>");
                    //ystem.out.println( "<p>--> Nombre archivo del cliente:" + nombreArchivo + "</p>");
                    //ystem.out.println( "<p>--> contentType:" + contentType + "</p>");

                    //// Obtengo extensi�n del archivo de cliente
                    String extension = nombreArchivo.substring(nombreArchivo.indexOf("."));
                    //ystem.out.println( "<p>--> Extensi�n del archivo:" + extension + "</p>");

                    //// Guardo archivo del cliente en servidor, con un nombre 'fijo' y la
                    //// extensi�n que me manda el cliente
                    //directorioArchivos=rutica;
                    Date ahora=new Date();
                    String ahorax= Util.dateFormat(ahora);
                    //String soloNombreArchivo="cc"+ num_osx +"fec"+ahorax+"usu"+loginx+"x"+""+ extension;
                    //model.AnticiposGasolinaService.setNombreNewFotoBd(soloNombreArchivo);
                    //ystem.out.println("nombreArchivo::::::hjb::::"+nombreArchivo);
                    String[] nombrearreglo=nombreArchivo.split("\\\\");
                    //ystem.out.println("nombrearreglo[(nombrearreglo.length-1)]"+nombrearreglo[0]);
                    String nombreArchivoremix=nombrearreglo[(nombrearreglo.length-1)];
                    nombre_archivo=directorioArchivos + "/"+ nombreArchivoremix;

                    File archivo = new File(nombre_archivo);

                    //ystem.out.println("nombre_archivo"+nombre_archivo);
                    fileItem.write(archivo);

                    if ( archivo.exists() ){

                            //ystem.out.println( "<p>--> GUARDADO " + archivo.getAbsolutePath() + "</p>");
                            respuesta="&respuesta=Archivo Guardado";
                            //model.AnticiposGasolinaService.setEstado_foto(true);
                            //model.AnticiposGasolinaService.setNombre_archivo(nombre_archivo);
                            //ystem.out.println("idxx"+idxx);
                            guardarArchivoEnBd(num_osx,directorioArchivos,idxx,tipitoxx);
                    }else{
                            //ystem.out.println( "<p>--> FALLO AL GUARDAR. NO EXISTE " + archivo.getAbsolutePath() + "</p>");
                            respuesta="&respuesta=Archivo Rechazado";
                            //model.AnticiposGasolinaService.setEstado_foto(false);
                    }


                }

            }
         }


    }catch(Exception ee){
        System.out.println("eroror:"+ee.toString()+"__"+ee.getMessage());
    }
    return next;
  }

  public void guardarArchivoEnBd(String num_osx,String namecarpeta,String idxx2,String tipito){
        try{
            //ystem.out.println("num_osx"+num_osx+"namecarpeta"+namecarpeta+"loginx"+loginx);

            List archivos   = new LinkedList();

            //ystem.out.println("model.AnticiposGasolinaService.getNombre_archivo()"+model.AnticiposGasolinaService.getNombre_archivo());

            Dictionary fields   = new Hashtable();
            fields.put("actividad",     "023" );
            fields.put("tipoDocumento", "031"   );
            fields.put("documento",     num_osx);
            fields.put("agencia",       "OP"   );
            fields.put("usuario",       loginx);

            File  file                = new File( namecarpeta );

            String comentario="";
            //ystem.out.println("file"+file+"namearchivo "+namecarpeta );
            if(file.exists()){
               //ystem.out.println("existe");
               File []arc =  file.listFiles();
               //ystem.out.println("arc"+arc);
               for (int i=0;i< arc.length;i++){
                   //ystem.out.println("in for"+arc[i].getAbsolutePath()+"__"+arc[i].getName()+"__"+arc[i].getPath());
                     if( ! arc[i].isDirectory()){
                           //ystem.out.println("no dir");
                           String name  = arc[i].getName();
                           //ystem.out.println("name"+name);
                           //if(  model.ImagenSvc.isLoad( model.ImagenSvc.getImagenes(), name)   && model.ImagenSvc.ext(name) ){
                                  //ystem.out.println("isload_"+file+"_"+name);
                                  FileInputStream       in  = new FileInputStream(file  +"/"+  name);
                                  //ystem.out.println("in");
                                  ByteArrayOutputStream out = new ByteArrayOutputStream();
                                  ByteArrayInputStream  bfin;
                                  //ystem.out.println("antes de read");
                                  int input                 = in.read();
                                  //ystem.out.println("despues de read");
                                  byte[] savedFile          = null;
                                  while(input!=-1){
                                      //ystem.out.println("while");
                                      out.write(input);
                                      input  = in.read();
                                      //ystem.out.println("fin de while");
                                  }
                                  in.close();
                                  //ystem.out.println("antes de close");
                                  out.close();
                                  //ystem.out.println("despues de close");
                                  savedFile  = out.toByteArray();
                                  //ystem.out.println("saved");
                                  bfin       = new ByteArrayInputStream( savedFile );
                                  //ystem.out.println("bfin ya");
                                  fields.put("archivo", name );
                                  //ystem.out.println("antes de comentario");
                              // Insertar imagen
                                 comentario += this.insertImagen(bfin, savedFile.length ,  fields,idxx2,tipito);
                                 //ystem.out.println("insertada");
                           //}
                     }
                }
            }

            //model.ImagenSvc.reset();

        }catch(Exception ee){
            System.out.println("eeeche"+ee.toString()+"_"+ee.getMessage());
        }
    }


    public String insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields,String idxx3,String tipito) throws ServletException{
        String estado   = "";
        String fileName = (String)fields.get("archivo");
        //ystem.out.println("longitud"+longitud+"bfin"+bfin.toString());

        try{

        //model.ImagenSvc.setEstado(false);
        model.negociosApplusService.insertImagen(bfin,longitud,fields,idxx3,tipito);
        estado = "Archivo "+ fileName +" ha sido guardado <br>";

        }catch(Exception e){
            System.out.println("errorrcito"+e.toString()+"__"+e.getMessage());
         estado =  "No se pudo guardar la imagen "+  fileName  +"<br>" + e.getMessage() ;
        }
        return estado;
  }
    public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir);
        if(! f.exists() ) f.mkdir();
     /*   else{
            File []arc =  f.listFiles();
            while( arc.length >0  ){
               File  imagen = arc[0];
               imagen.delete();
               arc =  f.listFiles();
            }
        }*/
        }catch(Exception e){ throw new Exception ( e.getMessage());}
   }

   static public boolean deleteDirectory(File path) {
    if( path.exists() ) {
      File[] files = path.listFiles();
      for(int i=0; i<files.length; i++) {
         if(files[i].isDirectory()) {
           deleteDirectory(files[i]);
         }
         else {
           files[i].delete();
         }
      }
    }
    return( path.delete() );
  }
   
    public String guardarArchivoEnDirectorio() {
        String num_osx = "";
        String vista = (String) request.getSession().getAttribute("myvista");

        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes");//se establece la ruta de la imagen
            String folderName = "fintracredit/";
            /* File carpetica =new File (directorioArchivos)    ;

        //boolean borrar =carpetica.delete();
        deleteDirectory(carpetica);
      
        this.createDir(directorioArchivos );*/
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);
                //servletFileUpload.setHeaderEncoding("utf-8"); 
                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String idNombreArchivo = null;
                String fieldtem = "", valortem = "", idxx = "";
                String tipitoxx = "";

                String multixx = "", contratixx = "", finarchivo = "";//090715

                while (it.hasNext()) {
                    FileItem fileItem = (FileItem) it.next();
                    if (fileItem.isFormField()) {

                        fieldtem = fileItem.getFieldName();
                        //ystem.out.println("fieldtem"+fieldtem);
                        valortem = fileItem.getString();
                        if (fieldtem.equals("numerit_osx")) {
                            num_osx = valortem;//request.getParameter("conductor_foto");
                            next = "/jsp/applus/importar.jsp?num_osx=" + num_osx;
                            //next=next+"?num_osx="+num_osx;

                            //ystem.out.println("nexttyyy"+next);
                        }
                        if (fieldtem.equals("idxx")) {
                            idxx = valortem;//request.getParameter("");
                        }

                        if (fieldtem.equals("tipito")) {
                            tipitoxx = valortem;//request.getParameter("");
                            next = next + "&tipito=" + tipitoxx + "&form=" + numSolicitud + "&tipoconv=Consumo&vista=" + vista;
                        }

                        if (fieldtem.equals("multixx")) {//090715
                            multixx = valortem;
                        }

                        if (fieldtem.equals("contratixx")) {//090715
                            contratixx = valortem;
                        }

                        if (fieldtem.equals("finarchivo")) {//090715
                            finarchivo = valortem;
                        }

                        if (fieldtem.equals("swmsgx")) {//090715
                            //System.out.println("valortem22"+valortem+"contratixx"+contratixx);
                            if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("OFERTA")) {

                                HSendMail2 hSendMail20 = new HSendMail2();
                                hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                        "oferta",
                                        "APPLUS:  Se ha montado oferta de " + multixx + " , contratista " + contratixx
                                        + " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .", loginx);
                            }

                            if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("FINAL")) {

                                /*  HSendMail2 hSendMail20=new HSendMail2();
                                    hSendMail2.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "acta final",
                                    "APPLUS:  Se ha montado acta de finalizaci�n de "+multixx +
                                                        " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx);
                                 */
                            }

                            if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("CREDITO")) {

                                HSendMail2 hSendMail20 = new HSendMail2();
                                hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                        "credito",
                                        "APPLUS:  Se ha montado el contrato de credito de " + multixx
                                        + " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .", loginx);
                            }
                        }
                        if (fieldtem.equals("categoria_archivo")) {
                            idNombreArchivo = valortem;
                        }
                    }
                    if (!(fileItem.isFormField())) {

                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                        //  String nombreArchivo = new String(fileItem.getName().getBytes(),"iso-8859-1");                   
                        String nombreArchivoOriginal = cleanAcentos(fileItem.getName());

                        //// Content type (tipo MIME) del archivo.
                        //// Esta informaci�n la proporciona el navegador del cliente.
                        //// Algunos ejemplos: .jpg = image/jpeg, .txt = text/plain
//                    String contentType = fileItem.getContentType();
                        //// Obtengo extensi�n del archivo de cliente
                        String extension = nombreArchivoOriginal.substring(nombreArchivoOriginal.indexOf("."));

//                    String[] nombrearreglo=nombreArchivo.split("\\\\");                 
//                    String nombreArchivoremix=nombrearreglo[(nombrearreglo.length-1)];
                        String rutaArchivo = directorioArchivos + folderName + num_osx;
                        File f = new File(rutaArchivo);
                        if (!f.exists()) {
                            folderName = "fintracredit1/";
                            this.createDir(directorioArchivos + folderName);
                        }
                        rutaArchivo = directorioArchivos + folderName + num_osx;
                        this.createDir(rutaArchivo);
                        
                        String nombreArchivo = null;
                        int idCategoria = Integer.parseInt(idNombreArchivo);
                        // Si el par�metro idNombreArchivo es vac�o o nulo, deja el nombre original del archivo                        
                        if (idNombreArchivo != null && !idNombreArchivo.equals("")) {
                            nombreArchivo = model.negociosApplusService.obtenerNombreArchivo(idCategoria);
                        } else {
                            nombreArchivo = nombreArchivoOriginal;
                        }

                        File archivo = new File(rutaArchivo + File.separator + nombreArchivo + extension);

                        if (archivo.exists()) {
                            // Extrae el �ltimo consecutivo del archivo
                            Pattern p = Pattern.compile("(?<=_)[0-9]+(?=\\.+)");
                            Matcher m = p.matcher(nombreArchivo);
                            
                            int numeroArchivo = 0;
                                    
                            if (m.find()) {
                                numeroArchivo = Integer.parseInt(m.group(1));
                            }
                            // Se incrementa el consecutivo del archivo
                            String nuevoNombreArchivo = nombreArchivo + "_" + (numeroArchivo + 1) + extension;
                            archivo = new File(rutaArchivo + File.separator + nuevoNombreArchivo);
                            guardarRegistroEnBd(num_osx, directorioArchivos + folderName, nuevoNombreArchivo, idxx, tipitoxx, idCategoria);
                        } else {
                            guardarRegistroEnBd(num_osx, directorioArchivos + folderName, nombreArchivo  + extension, idxx, tipitoxx, idCategoria);
                        }
                        
                        fileItem.write(archivo);
                        next = next + "&Mensaje=Archivo Cargado.";
                    }
                }
            }
        } catch (Exception ee) {
            System.out.println("eroror:" + ee.toString() + "__" + ee.getMessage());
        }
        return next;
    }

  public void guardarRegistroEnBd(String num_osx,String ruta,String nomArchivo,String idxx2,String tipito, int categoria){
        try{
          
            Dictionary fields   = new Hashtable();
            fields.put("actividad",     "023" );
            fields.put("tipoDocumento", "031"   );
            fields.put("documento",     num_osx);
            fields.put("agencia",       "OP"   );
            fields.put("usuario",       loginx);
            fields.put("archivo", nomArchivo );
            fields.put("categoria", categoria);

           this.insertRegistroArchivo(ruta,  fields,idxx2,tipito);


        }catch(Exception ee){
            System.out.println("eeeche"+ee.toString()+"_"+ee.getMessage());
        }
    }


    public String insertRegistroArchivo(String ruta,  Dictionary fields,String idxx3,String tipito) throws ServletException{
        String estado   = "";
        String fileName = (String)fields.get("archivo");
       
        try{

        model.negociosApplusService.insertRegistroArchivo(ruta,fields,idxx3,tipito,"");
        estado = "Archivo "+ fileName +" ha sido guardado <br>";

        }catch(Exception e){
            System.out.println("errorrcito"+e.toString()+"__"+e.getMessage());
         estado =  "Error al insertar registro en BD "+  fileName  +"<br>" + e.getMessage() ;
        }
        return estado;
  }
    
    public static String cleanAcentos(String texto) {       
          String original = "��������������u���������������������':";
          String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcCu   ";

	        if (texto != null) {
	            //Recorro la cadena y remplazo los caracteres originales por aquellos sin acentos

	            for (int i = 0; i < original.length(); i++ ) {

	                texto = texto.replace(original.charAt(i), ascii.charAt(i));

	            }

	          //Establezco todos los caracteres a min�scula.
	          texto = texto.toLowerCase();

	        }    
      
	        return texto;
    }


}
