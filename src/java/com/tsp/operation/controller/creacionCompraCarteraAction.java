/*
 * creacionCompraCarteraAction.java
 *
 * Created on 1 de enero de 2008, 06:00 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author NAVI
 */
public class creacionCompraCarteraAction  extends Action{
    
    String tipodocx;
    
    String modalidadcustodia;
    String modalidadremesa;
    
    /** Creates a new instance of creacionCompraCarteraAction */
    public creacionCompraCarteraAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try{
            
            String respuesta ="";

            String cantidadcheques = request.getParameter("cantidadcheques");
            int numcantidadcheques =0;
            if (cantidadcheques!=null && !(cantidadcheques.equals(""))){
                numcantidadcheques =Integer.parseInt(cantidadcheques );
            }

            String nit = request.getParameter("nit");
            String proveedor= request.getParameter("proveedor");
            String fechainicio= request.getParameter("fechainicio");
            //System.out.println("nit y proveedor"+nit+","+proveedor);
            String itemcillo = request.getParameter("itemcillo");
            String fechacheque = request.getParameter("fechacheque");
            String fechaconsignacion = request.getParameter("fechaconsignacion");

            String porte= "";
                //porte=request.getParameter("porte");

            String valor= request.getParameter("valorcillo");
            //System.out.println("itemfechas"+itemcillo+fechacheque+fechaconsignacion);
            
            if (model.creacionCompraCarteraSvc.getCheques().size()<=0 || request.getParameter("modalidadcustodia")!=null){
                modalidadcustodia=request.getParameter("modalidadcustodia");
                //System.out.println("modalidadcustodiaaction1"+modalidadcustodia);
                modalidadremesa=request.getParameter("modalidadremesa");
                //System.out.println("modalidadremesaaction1"+modalidadremesa);
            }else{
                modalidadcustodia=request.getParameter("modalidadcustodia2");
                //System.out.println("modalidadcustodiaaction2"+modalidadcustodia);
                modalidadremesa=request.getParameter("modalidadremesa2");
                //System.out.println("modalidadremesaaction2"+modalidadremesa);
            }
            if (modalidadremesa!=null && modalidadremesa.equals("2")){
                porte="0";
            }else{
                try{
                    porte=model.creacionCompraCarteraSvc.obtenerPorte();
                }catch(Exception e){
                    System.out.println("error al conseguir porte"+e.toString()+"___"+e.getMessage());
                    porte="0";
                }

            }
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String base =usuario.getBase();
            String opcion=request.getParameter("opcion");
            String next="/jsp/fenalco/comprascartera/creacion_compra_cartera.jsp";

            String agregacionCheque="";
            if (opcion.equals("consultar")){        
                next="/jsp/fenalco/comprascartera/mostrar_detalles_compra_cartera.jsp?codigo_negocio="+request.getParameter("consultablex")+"&nit="+nit+"&proveedor="+proveedor+"";  
            }
            
            if (opcion.equals("modificar")){
                String codigo_negocio=request.getParameter("consultablex");
                if (codigo_negocio!=null){
                    ArrayList detalle=model.creacionCompraCarteraSvc.obtenerCheques(codigo_negocio);
                    model.creacionCompraCarteraSvc.setCheques(detalle);
                    respuesta="?respuesta=negociomodificable";
                    next+=respuesta;
                    String fechainiciomodif=((chequeCartera)detalle.get(0)).getFechaDesembolso().substring(0,10);
                    next+="&fechainicio="+fechainiciomodif;
                    String modremesmodif=  ((chequeCartera)detalle.get(0)).getModRemesa();
                    String modcustodimodif=  ((chequeCartera)detalle.get(0)).getModCustodia();
                    next+="&modalidadremesa="+modremesmodif+"&modalidadcustodia="+modcustodimodif;  
                }       
            }
            if (opcion.equals("AgregarCheque")){
                
                if (request.getParameter("tipodocx")!=null){
                    tipodocx=request.getParameter("tipodocx");
                }
                //System.out.println("tipodocx"+tipodocx);
                try{
                    if (itemcillo==null || itemcillo.equals("")){
                        itemcillo=model.creacionCompraCarteraSvc.getNextItem(); 
                    }
                    if (valor!=null && porte!=null && fechacheque!=null && fechaconsignacion!=null && !(porte.equals("")) && !(fechacheque.equals("")) && !(fechaconsignacion.equals("")) && !(valor.equals(""))) {
                        agregacionCheque=model.creacionCompraCarteraSvc.agregarOcambiarCheque(itemcillo,fechacheque,fechaconsignacion,porte,valor,nit,proveedor,fechainicio,modalidadremesa);
                        if (agregacionCheque.equals("ok")){
                            respuesta="?respuesta=chequeagregado";
                        }else{
                            respuesta="?respuesta=chequenoagregado";
                        } 
                        next+=respuesta;
                    }else{
                        respuesta="?respuesta=chequenoagregado";
                        next+=respuesta;                    
                    } 
                    next+="&fechainicio="+request.getParameter("fechainicio");
                    next+="&tipodocx="+tipodocx;

                    if (model.creacionCompraCarteraSvc.getCheques().size()==1){
                        chequeCartera primercheque=model.creacionCompraCarteraSvc.getPrimerCheque();
                        for (int i=1;i<numcantidadcheques;i++){
                            model.creacionCompraCarteraSvc.copiarCheque(i,primercheque);
                        }

                    }

                }catch (Exception e){  
                    respuesta="?respuesta=chequenoagregado";
                    next+=respuesta;                    
                    System.out.println("errorcillo"+e.toString()+"..."+e.getMessage());
                    throw new ServletException(e.getMessage());
                }finally{
                    next+="&modalidadremesa="+modalidadremesa+"&modalidadcustodia="+modalidadcustodia+"&valorcillo="+valor;  
                }

            }

            if (opcion.equals("AceptarConjuntoDcheques")){
                try{
                    String tpr=request.getParameter("totrp");
                    String tdes=request.getParameter("totdesc");
                    String fechadesembolso=request.getParameter("fechainicio");
                    String total_valorgirable=request.getParameter("total_valorgirable");
                    String total_valor_cheque=request.getParameter("total_valor_cheque");
                    
                    //System.out.println("tipodocx"+tipodocx);
                    
                    ArrayList cheques=model.creacionCompraCarteraSvc.getCheques();
                    //System.out.println("AceptarConjuntoDcheques en action");
                    String aceptacionDeConjuntoDeCheques=model.creacionCompraCarteraSvc.aceptarConjuntoDeCheques(cheques,fechadesembolso,usuario,total_valorgirable,modalidadcustodia,modalidadremesa,total_valor_cheque,tpr,tdes,tipodocx);
                    tipodocx=null;
                    respuesta="?respuesta=chequesaceptados";
                    next+=respuesta;   
                    model.creacionCompraCarteraSvc.cancelarCompraCartera();
                }catch (Exception e){
                    System.out.println("errorcillo2"+e.toString()+"..."+e.getMessage());
                    respuesta="?respuesta=chequesnoaceptados";
                    next+=respuesta;  
                    throw new ServletException(e.getMessage());
                }finally{
                    next+="&modalidadremesa="+modalidadremesa+"&modalidadcustodia="+modalidadcustodia;  
                }
            }
            //com.tsp.util.Util.customFormat(value   
            if (opcion.equals("obtenercheque")){
                String itemobtenible=request.getParameter("itemobtenible");
                int itemobtenible2=Integer.parseInt(itemobtenible)-1;
                String fechachequeobtenible=((chequeCartera)model.creacionCompraCarteraSvc.getCheques().get(itemobtenible2)).getFechaCheque();
                String feccheqconsigobtenible=((chequeCartera)model.creacionCompraCarteraSvc.getCheques().get(itemobtenible2)).getFechaConsignacion();
                String valorobtenible=((chequeCartera)model.creacionCompraCarteraSvc.getCheques().get(itemobtenible2)).getValor();
                next+="?respuesta=chequeobtenido";
                next+=respuesta;   
                respuesta="&itemobtenible="+itemobtenible+"&feccheqconsigobtenible="+feccheqconsigobtenible+"&fechachequeobtenible="+fechachequeobtenible+"&valorobtenible="+valorobtenible;
                next+=respuesta;   
                next+="&fechainicio="+request.getParameter("fechainicio");
                next+="&modalidadremesa="+modalidadremesa+"&modalidadcustodia="+modalidadcustodia;  
            }
            
            if (opcion.equals("cancelarCompraCartera")){
                //System.out.println("se cancela");
                tipodocx=null;
                next+="?respuesta=negociocancelado";
                model.creacionCompraCarteraSvc.cancelarCompraCartera();
            }

            if (opcion.equals("obtenerfechaconsig")){
                String fechachequeobtenible=request.getParameter("fechacheque");               
                String itemobtenible=request.getParameter("itemcillo");               
                String valorobtenible=request.getParameter("valorcillo");               
                String feccheqconsigx=model.creacionCompraCarteraSvc.obtenerFechaConsignacion(fechachequeobtenible);
                
                next+="?respuesta=fecconsigobtenida";
                next+=respuesta;   
                respuesta="&feccheqconsigobtenible="+feccheqconsigx+"&fechachequeobtenible="+fechachequeobtenible+"&valorobtenible="+valorobtenible+"&itemobtenible="+itemobtenible;
                next+=respuesta;   
                next+="&fechainicio="+request.getParameter("fechainicio");
                next+="&modalidadremesa="+modalidadremesa+"&modalidadcustodia="+modalidadcustodia;  
            }
            
            if (opcion.equals("imprimirDetalle")){
                //System.out.println("imprimirDetalle");
                String codigo_negocioy=request.getParameter("codigo_negocio");
                ArrayList detalle=null;
                detalle=model.creacionCompraCarteraSvc.obtenerCheques(codigo_negocioy);
                model.creacionCompraCarteraSvc.imprimirDetalleCompraCartera(model,detalle);
                //System.out.println("despues de imprimir detalle");
                next="/jsp/fenalco/comprascartera/mostrar_detalles_compra_cartera.jsp?codigo_negocio="+codigo_negocioy+"&imprimir=si";  
            }
            
            //System.out.println("next"+next);

            this.dispatchRequest(next); 
        }catch(Exception e){
            System.out.println("errorcillo en run de action"+e.toString()+"____"+e.getMessage());
            throw new InformationException("ERROR DURANTE action " + e.getMessage());            
        }
    }
}
