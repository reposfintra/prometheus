/*
 * CamposValidarAction.java
 *
 * Created on 18 de noviembre de 2004, 10:34 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;


/**
 *
 * @author  KREALES
 */
public class CamposValidarAction extends Action {
    
    /** Creates a new instance of CamposValidarAction */
    public CamposValidarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        
        String placa = request.getParameter("placa").toUpperCase();
        String cedula = request.getParameter("conductor");
        String remision = "CC"+request.getParameter("remision");
        String nombre="";
        String standar = request.getParameter("standard");
        String fecpla ="";
        String hpla ="";
        
        if(request.getParameter("IVIA")!=null){
            fecpla =request.getParameter("IVIA");
            
        }
        if(request.getParameter("hIVIA")!=null){
            hpla =request.getParameter("hIVIA");
            
        }
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        int sw=0;
        Calendar fecha=null;
        String nombreProp="No se encontro";
        
        String next ="/despacho/MasivoValidado.jsp?nombre="+nombre+"&IVIA="+fecpla+"&hIVIA="+hpla+"&nombreProp="+nombreProp;
        
        
        //INICIALIZO VARIABLES DEL REQUEST.
        request.setAttribute("placa","#99cc99");
        request.setAttribute("remision","#99cc99");
        request.setAttribute("conductor","#99cc99");
        request.setAttribute("peso","#99cc99");
        
        try{
            if(request.getParameter("inicio")!=null){
                
                //VALIDO SI LA REMISION YA EXISTE
                if(model.remesaService.estaRemision(remision)){
                    request.setAttribute("remision","#cc0000");
                    sw=1;
                }
                
                if(!model.placaService.placaExist(placa)){
                    sw=1;
                    request.setAttribute("placa","#cc0000");
                }
                else{
                    model.placaService.buscaPlaca(placa);
                    if(model.placaService.getPlaca()!=null){
                        Placa p = model.placaService.getPlaca();
                        String cedProp = p.getPropietario();
                        nombreProp = model.placaService.nombreNit(cedProp);
                        
                    }
                }
                if(model.conductorService.conductorExist(cedula)){
                    //System.out.println("Existe el conductor");
                    model.conductorService.buscaConductor(cedula);
                    if(model.conductorService.getConductor()!=null){
                        //System.out.println("El conductor no es nulo");
                        Conductor conductor = model.conductorService.getConductor();
                        nombre=conductor.getNombre();
                        cedula=conductor.getCedula();
                        //System.out.println("El nombre es " +nombre);
                    }
                    
                }
                else{
                    sw=1;
                    request.setAttribute("conductor","#cc0000");
                }
                
                //VALIDO LAS FECHAS PERO SOLO LAS DE INICIO DE VIAJE...
                model.tbltiempoService.searchTblTiemposEnt(standar, usuario.getBase());
                Vector tiempos = model.tbltiempoService.getTiempos();
                
                
                for(int i =0; i<tiempos.size();i++){
                    Tbltiempo tbl = (Tbltiempo) tiempos.elementAt(i);
                    request.setAttribute("error"+tbl.getTimeCode(),"#99cc99");
                    
                }
                //Recupero las fechas
                
                for(int i =0; i<tiempos.size();i++){
                    
                    fecha = Calendar.getInstance();
                    
                    Tbltiempo tbl = (Tbltiempo) tiempos.elementAt(i);
                    
                    //System.out.println("Voy a verificar la fecha con codigo : "+tbl.getTimeCode());
                    
                    
                    if(request.getParameter("IVIA")!=null)
                        fecpla= request.getParameter("IVIA");
                    
                    if(tbl.getTimeCode().equals("IVIA") && request.getParameter("IVIA")==null){
                        
                        //System.out.println("COMO IVIA ES NULO VOY A BUSCAR EL ULTIMO VIAJE  ");
                        
                        model.planillaService.buscaFecha(placa);
                        if(model.planillaService.getPlanilla()!=null){
                            
                            fecpla =""+ model.planillaService.getPlanilla().getFecdsp().substring(0,10);
                            hpla = ""+ model.planillaService.getPlanilla().getFecdsp().substring(11,16);
                            int year=Integer.parseInt(fecpla.substring(0,4));
                            int month=Integer.parseInt(fecpla.substring(5,7))-1;
                            int date= Integer.parseInt(fecpla.substring(8,10));
                            int hora=Integer.parseInt(hpla.substring(0,2));
                            int minuto=Integer.parseInt(hpla.substring(3,5));
                            
                            Calendar fecha_ant = Calendar.getInstance();
                            fecha_ant.set(year,month,date,hora,minuto,0);
                            
                            
                            Date fecha_pla_date = fecha_ant.getTime();
                            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                            
                            fecpla = s.format(fecha_pla_date);
                            
                            //fecha.setTime(model.planillaService.getPlanilla().getFecdspDate());
                            fecha.set(year,month,date,hora,minuto,0);
                            fecha.set(fecha.MILLISECOND,0);
                            
                            
                            
                        }
                        else{
                            Tbltiempo tbl2 = (Tbltiempo) tiempos.elementAt(i+1);
                            if(request.getParameter(tbl2.getTimeCode())!=null){
                                if(request.getParameter("h"+tbl2.getTimeCode()).length()>4){
                                    
                                    int year   =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(0,4));
                                    int month  =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(5,7))-1;
                                    int date   =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(8,10));
                                    int hora   =    Integer.parseInt(request.getParameter("h"+tbl2.getTimeCode()).substring(0,2));
                                    //System.out.println(""+hora);
                                    int minuto=Integer.parseInt(request.getParameter("h"+tbl2.getTimeCode()).substring(3,5));
                                    //System.out.println(""+minuto);
                                    Calendar fecha_ant = Calendar.getInstance();
                                    fecha_ant.set(year,month,date-1,hora,minuto,0);
                                    
                                    Date fecha_pla_date = fecha_ant.getTime();
                                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                    
                                    
                               /* if(request.getParameter(tbl2.getTimeCode()).substring(16,18).equalsIgnoreCase("pm")){
                                    if(hora<12)
                                        fecha.set(year,month,date,hora+12,minuto);
                                }
                                else*/
                                    fecha.set(year,month,date-1,hora,minuto);
                                    
                                    fecha.set(fecha.MILLISECOND,0);
                                    s = new SimpleDateFormat("yyyy-MM-dd");
                                    fecpla = s.format(fecha_pla_date);
                                }else{
                                    request.setAttribute("error"+tbl2.getTimeCode(),"#cc0000");
                                    sw=1;
                                }
                            }
                        }
                    }
                    
                    else{
                        //System.out.println("VOY A BUSCAR EL VALOR DE LA FECHA CON CODIGO...."+tbl.getTimeCode());
                        if(request.getParameter(tbl.getTimeCode())!=null){
                            if(request.getParameter("h"+tbl.getTimeCode()).length()>4){
                                int year=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(0,4));
                                int month=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(5,7))-1;
                                int date= Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(8,10));
                                int hora=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(0,2));
                                //System.out.println(""+hora);
                                int minuto=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(3,5));
                                //System.out.println(""+minuto);
                                fecha.set(year,month,date,hora,minuto,0);
                                fecha.set(fecha.MILLISECOND,0);
                            }
                            else{
                                request.setAttribute("error"+tbl.getTimeCode(),"#cc0000");
                                sw=1;
                            }
                        }
                    }
                    Calendar hoy=Calendar.getInstance();
                    
                    if(fecha.after(hoy)){
                        
                        //System.out.println("LA FECHA ESTA DESPUES DE HOY....");
                        request.setAttribute("error"+tbl.getTimeCode(),"#cc0000");
                        sw=1;
                        //System.out.println("La primera fecha es : "+fecha);
                        //System.out.println("La segunda fecha es : "+hoy);
                    }
                    
                    else{
                        
                        int j= i+1;
                        //System.out.println("EL VALOR DE J ES  ..."+j);
                        
                        if(j < tiempos.size()){
                            
                            Tbltiempo tbl2 = (Tbltiempo) tiempos.elementAt(j);
                            
                            //System.out.println("VOY A BUSCAR  EL VALOR DE LA FECHA "+tbl2.getTimeCode()+" CON LA Q VOY A COMPARAR ...");
                            
                            
                            Calendar fecha2 = Calendar.getInstance();
                            
                            if(request.getParameter(tbl2.getTimeCode())!=null){
                                if(request.getParameter("h"+tbl2.getTimeCode()).length()>4){
                                    int year2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(0,4));
                                    int month2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(5,7))-1;
                                    int date2= Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(8,10));
                                    int hora2=Integer.parseInt(request.getParameter("h"+tbl2.getTimeCode()).substring(0,2));
                                    //System.out.println(""+hora2);
                                    int minuto2=Integer.parseInt(request.getParameter("h"+tbl2.getTimeCode()).substring(3,5));
                                    //System.out.println(""+minuto2);
                                    fecha2.set(year2,month2,date2,hora2,minuto2,0);
                                    fecha2.set(fecha2.MILLISECOND,0);
                                }else{
                                    sw=1;
                                    request.setAttribute("error"+tbl2.getTimeCode(),"#cc0000");
                                }
                                if(fecha.after(fecha2)){
                                    //System.out.println("LA PRIMERA FECHA ES MAYOR Q LA SEGUNDA ...");
                                    sw=1;
                                    request.setAttribute("error"+tbl2.getTimeCode(),"#cc0000");
                                    
                                }
                                else if(fecha.equals(fecha2)){
                                    sw=1;
                                    //System.out.println("LAS FECHAS SON IGUALES....");
                                    request.setAttribute("error"+tbl2.getTimeCode(),"#cc0000");
                                    
                                }
                                
                            }
                            
                            //System.out.println("La primera fecha es : "+fecha);
                            //System.out.println("La segunda fecha es : "+fecha2);
                            
                        }
                        
                    }
                    
                    
                }
                
                if(fecpla.length()>9)
                    fecpla = fecpla.substring(0,10);
                if(sw==0)
                    next = "/despacho/MasivoValidado.jsp?nombre="+nombre+"&IVIA="+fecpla+"&hIVIA="+hpla+"&nombreProp="+nombreProp;
                else
                    next = "/despacho/MasivoErrores.jsp?nombre="+nombre+"&nombreProp="+nombreProp+"&IVIA="+fecpla+"&hIVIA="+hpla;
            }
            
                /*
                String traffic="";
                if(request.getParameter("trafico")==null){
                    traffic  = model.planillaService.buscarTrafficCode(placa);
                    if(traffic.equals("")){
                        sw=1;
                        errorTrafico="#cc0000";
                    }
                }
                else{
                    //System.out.println("Se recibio un codigo de trafico");
                    traffic  = request.getParameter("trafico");
                    if(!model.planillaService.estaTagTrafficPlaca(request.getParameter("trafico"),placa)){
                        //System.out.println("No esta la placa  y el codigo");
                        if(model.planillaService.estaTagTraffic(request.getParameter("trafico"))){
                            //System.out.println("Esta el codigo del trafico");
                            sw=1;
                            errorTrafico="#cc0000";
                        }
                    }
                 
                }*/
            
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
