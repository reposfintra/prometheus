/*
 * Nombre        BuscarContactoAction.java
 * Autor         Ing. Rodrigo Salazar
 * Fecha         23 de junio de 2005, 10:45 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class BuscarContactoAction extends Action {
    
    /** Creates a new instance of BuscarPaisAction */
    public BuscarContactoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession();

        try{ 
            if (request.getParameter("cmd")!= null ){
                next="/jsp/trafico/contacto/VerContactosXBusq.jsp";
                String c_cto = (request.getParameter("c_codigo").toUpperCase())+"%";
                String c_cia = (request.getParameter("c_cia").toUpperCase())+"%";
                String c_tipo = (request.getParameter("c_tipo").toUpperCase())+"%";               
             
                model.contactoService.buscarContactoNombre(c_cto, c_cia, c_tipo);
                if ( model.contactoService.obtcontactos().size() == 0){
                    next = "/jsp/trafico/contacto/BuscarContacto.jsp?mensaje=Su b�squeda no arroj� resultados!";
                }         
            }else{
                model.contactoService.buscarContacto(request.getParameter("codigo"), request.getParameter("compania") ); 
                Vector tcont = model.tipo_contactoService.listarTContactos();
                session.setAttribute("tipocont", tcont);
                next="/jsp/trafico/contacto/contacto.jsp?mensaje=Modificar";
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);

    }
    
}
