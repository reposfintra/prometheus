/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AdminFintraDAO;
import com.tsp.operation.model.DAOS.impl.AdminFintraImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.ParmetrosDinamicaTSP;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.ExcelApiUtil;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author egonzalez
 */
public class AdminFintraAction extends Action {

    Usuario usuario = null;
    String reponseJson = "{}";

    private final int CARGAR_AUDITORIA_CREDI100 = 1;
    private final int EXPORTAR_EXCEL_REPORTE_CREDI100 = 2;
    private final int CARGAR_LINEA_NEGOCIO = 3;
    private final int CARGAR_SOLICITUDES_DOCUMENTOS = 4;
    private final int GUARDAR_SOLICITUDES_SELECCIONADAS = 5;
    private final int CARGAR_EMPRESA_ENVIO = 6;
    private final int EXPORTAR_EXCEL_SOLICITUDES_SELECCIONADAS = 7;
    private final int CARGAR_LINEA_NEGOCIO_ = 8;
    private final int EXPORTAR_EXCEL_REPORTE_COEMRCIAL_CREDI100 = 9;
    private final int CARGAR_AUTORIZADOR = 10;
    private final int CARGAR_HC = 11;
    private final int BUSCAR_PROVEEDOR = 12;
    private final int BUSCAR_IMPUESTO = 13;
    private final int GUARDAR_DOCUMENTO = 14;
    private final int BUSCAR_CUENTAS = 15;
    private final int CARGAR_MOTIVOS = 16;
    private final int BUSCAR_MULTISERVICIO = 17;
    private final int CARGAR_CXP_CABECERA = 18;
    private final int CARGAR_CXP_DETALLE = 19;
    private final int CARGAR_NEGOCIOS_CUOTA_MANEJO = 20;
    private final int ANULAR_CUOTA_MANEJO = 21;
    private final int CARGAR_NEGOCIO_RECHAZADO = 22;
    private final int REACTIVAR_NEGOCIO_RECHAZADO = 23;
    private final int GUARDAR_ANTICIPO_CAJA_MENOR = 24;
    private final int CARGAR_ANTICIPO_CAJA_MENOR = 25;
    private final int LEGALIZAR_ANTICIPO_CAJA_MENOR = 26;
    private final int CARGAR_CXP_GASTOS = 27;
    private final int CARGAR_DETALLE_ANTICIPO_CAJA_MENOR = 28;
    private final int CARGAR_DOCUMENTOS_AFECTO_CXC = 29;
    private final int CARGAR_DOCUMENTOS_AFECTO_GCM = 30;
    private final int ANULAR_ANTICIPO = 31;
    private final int CARGAR_CARGAR_DINAMICA_CONTABLE_PASO = 32;
    private final int CARGAR_CARGAR_REPORTE_PRODUCCION = 33;
    private final int CARGAR_DETALLE_MOVIMIENTO_TSP = 34;
    private final int CARGAR_ANALISIS_REPORTE_PRODUCCION = 35;
    private final int EXPORTAR_EXCEL_ANALISIS_REPORTE_PRODUCCION = 36;
    private final int CARGAR_REPORTE_INVERSIONISTA = 37;
    private final int CARGAR_TERCERO_INVERSIONISTA = 38;
    private final int EXPORTAR_EXCEL_REPORTE_INVERSIONISTA = 39;
    private final int EXPORTAR_EXCEL_REPORTE_INVERSIONISTA_TOTAL = 40;
    private final int CARGAR_HELP_DINAMICAS_CONTABLES = 41;
    private final int PERMISO_HELP_DINAMICAS_CONTABLES = 42;
    private final int CARGAR_HELP_DINAMICAS_CONTABLES_DATOS = 43;
    private final int ACTUALIZAR_HELP_DINAMICAS_CONTABLES_DATOS = 44;
    private final int EXPORTAR_EXCEL_TRANS_PRONT_COMB = 45;
    private final int CARGAR_REVISION_PASO2 = 46;
    private final int CARGAR_CUENTAS = 47;
    private final int CARGAR_SEGUIMIENTO_CAJA = 50;
    private final int PROCEDIMIENTO_SEGUIMIENTO_CAJA = 51;
    private final int EXPORTAR_EXCEL_SEGUIMIENTO_CAJA = 52;
    private final int CARGAR_AUTOCOMPLETAR_CUENTAS = 53;
    private final int CARGAR_CONFIGURAR_CUENTAS_DC = 54;
    private final int CAMBIAR_ESTADO_CONFIGURAR_CUENTAS_DC = 55;
    private final int CARGAR_INFORMACION_CUENTA = 56;
    private final int GUARDAR_INFORMACION_CUENTA = 57;
    private final int CARGAR_CONCEPTOS = 59;
    private final int CARGAR_FACTURAS = 60;
    private final int EXPORTAR_MOVIMIENTO_LIBRO_AUXILIAR = 58;
    private final int BUSCAR_CLIENTE = 61;
    private final int CARGAR_TIPO_INGRESO = 62;
    private final int CARGAR_HC_NOMBRES = 63;
    private final int CARGAR_MONEDA = 64;
    private final int ACTUALIZAR_CUENTA_DC = 65;
    private final int GET_DOCUMENTOS_REQUERIDOS = 66;
    private final int GUARDAR_DATOS_INGRESOS = 67;
    private final int CARGAR_PAGOS_SIN_REFERENCIA = 68;
    private final int BUSCAR_NEGOCIO = 69;
    private final int CARGAR_DC_FINTRA_SOLUCIONES = 70;
    private final int EXPORTAR_EXCEL_FINTRA_SOLUCIONES = 71;
    private final int CARGAR_MODULOS_CUENTAS_DC = 72;
    private final int CARGAR_PAZ_Y_SALVO = 73;
    private final int CARGAR_LIQUIDACION_DETALLE = 74;
    private final int EXPORTAR_PDF_PAZ_Y_SALVO = 75;
    private final int GENERAR_ESTADO_CUENTA = 76;
    private final int BUSCAR_ASESOR = 77;
    private final int CARGAR_NEGOCIOS = 78;
    private final int CARGAR_ASESORES = 79;
    private final int ACTUALIZAR_NEGOCIOS = 80;
    private final int CARGAR_OBSERVACIONES_RECOLECCION_FIRMAS = 81;
    private final int GUARDAR_OBSERVACIONES_RECOLECCION_FIRMAS = 82;
    private final int CARGAR_ANALISIS_RECAUDO = 83;
    private final int CARGAR_BARRIOS = 84;
    private final int CARGAR_DETALLE_NEGOCIO_LIQUIDACION = 85;
    private final int CARGAR_FACTURAS_CAUSACION_INTERESES = 86;
    private final int CAUSAR_INTERESES_MULTISERVICIO = 87;
    private final int CARGAR_CMC_CAUSACION_INTERESES_MS = 88;
    private final int CARGAR_NEGOCIO_RECHAZO = 89;
    private final int RECHAZAR_NEGOCIO = 90;
    private final int CARGAR_CONVENIOS = 91;
    private final int EXPORTAR_EXCEL_REPORTE = 92;
    private final int EXTRACTO_ESTADO_CUENTA = 93;
    private final int CARGAR_PRESOLICITUDES_MICROCREDITO = 94;
    private final int VER_ARCHIVOS = 95;
    private final int MOSTRAR_ARCHIVOS = 96;
    private final int ANULAR_PRESOLICITUD_MICROCREDITO = 97;
    private final int CARGAR_NEGOCIOS_A_DESISTIR=98;
    private final int STANDBY_PRESOLICITUD_MICROCREDITO = 99;
    private final int DEVOLVER_PRESOLICITUD_MICROCREDITO = 100;
    private final int CONSULTAR_CAUSAL_STANDBY = 101;
    private final int CARGAR_ENTIDADES_PRESOLICITUD = 102;
    private final int CARGAR_PRESOLICITUDES_MICRO_R = 103;

    private AdminFintraDAO dao;

    @Override
    public void run() throws ServletException, InformationException {
        try {
             HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new AdminFintraImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_AUDITORIA_CREDI100:
                    cargarReporteCredi100();
                    break;
                case EXPORTAR_EXCEL_REPORTE_CREDI100:
                    exportarExcelAuditoria();
                    break;
                case CARGAR_LINEA_NEGOCIO:
                    cargarLineaNegocio();
                    break;
                case CARGAR_SOLICITUDES_DOCUMENTOS:
                    cargarSolicitudesDocumentos();
                    break;
                case GUARDAR_SOLICITUDES_SELECCIONADAS:
                    guardarSolicitudesSeleccionadas();
                    break;
                case CARGAR_EMPRESA_ENVIO:
                    cargarEmpresaEnvio();
                    break;
                case EXPORTAR_EXCEL_SOLICITUDES_SELECCIONADAS:
                    exportarExcelSolicitudesSeleccionadas();
                    break;
                case CARGAR_LINEA_NEGOCIO_:
                    cargarLineaNegocio_();
                    break;
                case EXPORTAR_EXCEL_REPORTE_COEMRCIAL_CREDI100:
                    exportarExcelAuditoriaComercial();
                    break;
                case CARGAR_AUTORIZADOR:
                    cargarAutorizador();
                    break;
                case CARGAR_HC:
                    cargarHC();
                    break;
                case BUSCAR_PROVEEDOR:
                    buscarProveedor();
                    break;
                case BUSCAR_IMPUESTO:
                    buscarImpuesto();
                    break;
                case GUARDAR_DOCUMENTO:
                    guardarDocumentosCXP();
                    break;
                case BUSCAR_CUENTAS:
                    buscarCuentas();
                    break;
                case CARGAR_MOTIVOS:
                    cargarMotivo();
                    break;
                case BUSCAR_MULTISERVICIO:
                    buscarMultiservicio();
                    break;
                case CARGAR_CXP_CABECERA:
                    cargarCxpCabecera();
                    break;
                case CARGAR_CXP_DETALLE:
                    cargarCxpDetalle();
                    break;
                case CARGAR_NEGOCIOS_CUOTA_MANEJO:
                    cargarNegociosCuotaManejo();
                    break;
                case ANULAR_CUOTA_MANEJO:
                    anularCuotaManejo();
                    break;
                case CARGAR_NEGOCIO_RECHAZADO:
                    cargarNegocioRechazado();
                    break;
                case REACTIVAR_NEGOCIO_RECHAZADO:
                    reactivarNegocio();
                    break;
                case GUARDAR_ANTICIPO_CAJA_MENOR:
                    guardarDocumentosAnticipoCajaMenor();
                    break;
                case CARGAR_ANTICIPO_CAJA_MENOR:
                    cargarAnticipoCajaMenor();
                    break;
                case LEGALIZAR_ANTICIPO_CAJA_MENOR:
                    legalizarAnticipoCajaMenor();
                    break;
                case CARGAR_CXP_GASTOS:
                    buscarCXPGastos();
                    break;
                case CARGAR_DETALLE_ANTICIPO_CAJA_MENOR:
                    cargarDetalleAnticipoCajaMenor();
                    break;
                case CARGAR_DOCUMENTOS_AFECTO_CXC:
                    cargarDocumentosAfectoCXC();
                    break;
                case CARGAR_DOCUMENTOS_AFECTO_GCM:
                    cargarDocumentosAfectoGCM();
                    break;
                case ANULAR_ANTICIPO:
                    anularAnticipo();
                    break;
                case CARGAR_CARGAR_DINAMICA_CONTABLE_PASO:
                    cargarAnticipoTransferenciaPaso();
                    break;
                case CARGAR_CARGAR_REPORTE_PRODUCCION:
                    cargarReporteProduccion();
                    break;
                case CARGAR_DETALLE_MOVIMIENTO_TSP:
                    cargarConsolidadoMovimientoTSP();
                    break;
                case CARGAR_ANALISIS_REPORTE_PRODUCCION:
                    cargarAnalisisReporteProduccion();
                    break;
                case EXPORTAR_EXCEL_ANALISIS_REPORTE_PRODUCCION:
                    exportarExcelAnalisisReporteProduccion();
                    break;
                case CARGAR_REPORTE_INVERSIONISTA:
                    cagarReporteInversionista();
                    break;
                case CARGAR_TERCERO_INVERSIONISTA:
                    cargarTercerInversionistas();
                    break;
                case EXPORTAR_EXCEL_REPORTE_INVERSIONISTA:
                    exportarExcelReporteInversionista();
                    break;
                case EXPORTAR_EXCEL_REPORTE_INVERSIONISTA_TOTAL:
                    cagarReporteInversionistaSinFiltros();
                    break;
                case CARGAR_HELP_DINAMICAS_CONTABLES:
                    cagarHelpDinamicasContables();
                    break;
                case PERMISO_HELP_DINAMICAS_CONTABLES:
                    permisoHelpDinamicasContables();
                    break;
                case CARGAR_HELP_DINAMICAS_CONTABLES_DATOS:
                    cagarHelpDinamicasContablesDatos();
                    break;
                case ACTUALIZAR_HELP_DINAMICAS_CONTABLES_DATOS:
                    actualizarHelpDinamicasContablesDatos();
                    break;
                case EXPORTAR_EXCEL_TRANS_PRONT_COMB:
                    exportarExcelTransferenciaProntoPagoCombustible();
                    break;
                case CARGAR_REVISION_PASO2:
                    cargarRevisionPaso2();
                    break;
                case CARGAR_CUENTAS:
                    cargarCuentas();
                    break;
                case CARGAR_SEGUIMIENTO_CAJA:
                    cagarSeguimientoCaja();
                    break;
                case PROCEDIMIENTO_SEGUIMIENTO_CAJA:
                    procedimientoSeguimientoCaja();
                    break;
                case EXPORTAR_EXCEL_SEGUIMIENTO_CAJA:
                    exportarExcelSeguimientoCaja();
                    break;
                case CARGAR_AUTOCOMPLETAR_CUENTAS:
                    buscarAutoCompletarCuentas();
                    break;
                case CARGAR_CONFIGURAR_CUENTAS_DC:
                    cagarConfiguracionCuentasDC();
                    break;
                case CAMBIAR_ESTADO_CONFIGURAR_CUENTAS_DC:
                    cambiarEstadoConfiguracionCuentasDC();
                    break;
                case CARGAR_INFORMACION_CUENTA:
                    cagarInformacionCuenta();
                    break;
                case GUARDAR_INFORMACION_CUENTA:
                    guardarInformacionCuenta();
                    break;
                case CARGAR_CONCEPTOS:
                    cargarConcepto();
                    break;
                case CARGAR_FACTURAS:
                    cagarFacturas();
                    break;
                case EXPORTAR_MOVIMIENTO_LIBRO_AUXILIAR:
                    exportarMovimientoAuxiliar();
                    break;
                case BUSCAR_CLIENTE:
                    buscarCliente();
                    break;
                case CARGAR_TIPO_INGRESO:
                    cargarTipoIngreso();
                    break;
                case CARGAR_HC_NOMBRES:
                    cargarHcNombres();
                    break;
                case CARGAR_MONEDA:
                    cagarMoneda();
                    break;
                case ACTUALIZAR_CUENTA_DC:
                    actualizarInformacionCuenta();
                    break;
                case GET_DOCUMENTOS_REQUERIDOS:
                    getDocumentosRequeridos();
                    break;
                case BUSCAR_NEGOCIO:
                    buscarNegocio();
                    break;
                case GUARDAR_DATOS_INGRESOS:
                    guardarDatosIngresos();
                    break;
                case CARGAR_PAGOS_SIN_REFERENCIA:
                    cargarPagosSinReferencia();
                    break;
                case CARGAR_DC_FINTRA_SOLUCIONES:
                    cagarFintraSoluciones();
                    break;
                case EXPORTAR_EXCEL_FINTRA_SOLUCIONES:
                    exportarFintraSoluciones();
                    break;
                case CARGAR_MODULOS_CUENTAS_DC:
                    cargarModulos();
                    break;
                case CARGAR_PAZ_Y_SALVO:
                    cargarPazYSalvo();
                    break;
                case CARGAR_LIQUIDACION_DETALLE:
                    cargarLiquidacionDetalle();
                    break;
                case EXPORTAR_PDF_PAZ_Y_SALVO:
                    exportarPdfPazYSalvo();
                    break;
                case GENERAR_ESTADO_CUENTA:
                    generarEstadoCuenta();
                    break;
                case BUSCAR_ASESOR:
                    buscarAsesor();
                    break;
                case CARGAR_NEGOCIOS:
                    buscarNegocios();
                    break;
                case CARGAR_ASESORES:
                    cargarAsesores();
                    break;
                case ACTUALIZAR_NEGOCIOS:
                    actualizarNegocios();
                    break;
                case CARGAR_OBSERVACIONES_RECOLECCION_FIRMAS:
                    cargarObservaciones();
                    break;
                case GUARDAR_OBSERVACIONES_RECOLECCION_FIRMAS:
                    guardarObservaciones();
                    break;
                case CARGAR_ANALISIS_RECAUDO:
                    cagarAnalisisRecaudo();
                    break;
                case CARGAR_BARRIOS:
                    cagarBarrios();
                    break;
                case CARGAR_DETALLE_NEGOCIO_LIQUIDACION:
                    cargarDetalleNegocioLiquidacion();
                    break;
                case CARGAR_FACTURAS_CAUSACION_INTERESES:
                    cargarFacturasCausacionIntereses();
                    break;
                case CAUSAR_INTERESES_MULTISERVICIO:
                    causarInteresesMultiservicio();
                    break;
                case CARGAR_CMC_CAUSACION_INTERESES_MS:
                    cargarCmcCausacionInteresesMS();
                    break;
                case CARGAR_NEGOCIO_RECHAZO:
                    cargar_negocio_rechazo();
                    break;
                case RECHAZAR_NEGOCIO:
                    rechazarNegocio();
                    break;
                case CARGAR_CONVENIOS:
                    cargar_convenios();
                    break;
                case EXPORTAR_EXCEL_REPORTE:
                    exportarexcelreporte();
                    break;
                case EXTRACTO_ESTADO_CUENTA:
                    extracto_estado_cuenta();
                    break;
                case CARGAR_PRESOLICITUDES_MICROCREDITO:
                    cargaPresolicitudesMicrocredito();
                    break;
                case VER_ARCHIVOS:
                    this.getNombresArchivos();
                    break;
                case MOSTRAR_ARCHIVOS:
                    this.almacenarArchivoEnCarpetaUsuario();
                    break;    
                case ANULAR_PRESOLICITUD_MICROCREDITO:
                    this.rechazarnegociomicrocredito();
                    break;
                case CARGAR_NEGOCIOS_A_DESISTIR:
                    this.cargarnegociosdesistir();
                    break;
                case STANDBY_PRESOLICITUD_MICROCREDITO:
                    this.standbypresolicitudes();
                    break;
                case DEVOLVER_PRESOLICITUD_MICROCREDITO:
                    this.devolverSolicitud();
                    break;
                case CONSULTAR_CAUSAL_STANDBY:
                    this.consultarCausalStandBy();
                    break;
                case CARGAR_ENTIDADES_PRESOLICITUD:
                    this.cargarEntidadesPresolicitud();
                    break;
                case CARGAR_PRESOLICITUDES_MICRO_R:
                    cargarPresolicitudesMicroRechazadas();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void cargarReporteCredi100() {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String linea_negocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
            String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String estado_solicitud = request.getParameter("estado_solicitud") != null ? request.getParameter("estado_solicitud") : "";
            String negocio = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            this.printlnResponseAjax(dao.cargarAuditoriaCredi100(fechaInicio, fechaFin, identificacion, linea_negocio, numero_solicitud, negocio, estado_solicitud), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportarExcelAuditoria() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Autitoria_credi100_", titulo = "AUDITORIA CREDI 100";

        cabecera = new String[]{"Entidad", "Numero solicitud", "Negocio", "Identificacion", "Nombre cliente", "Afiliado", "Estado inicial solicitud", "Estado solicitud", "Estado negocio",
            "Causal", "Asesor", "id_convenio", "Monto credito", "Valor cuota", "Numero cuotas", "Score", "Score lisim", "Score total", "Total obligaciones financieras",
            "Total gastos familiares", "Total ingresos", "Endeudamiento", "Comentario", "Cuotas pendientes", "Etapa", "Medio", "Fecha presolicitud",
            "Tipo Cliente","Qb","Qm","Qa","Porc Endeudamiento","Rechazo Operaciones","Causal Rechazo"};

        dimensiones = new short[]{
            7000, 5000, 3000, 5000, 5000, 9000, 7000, 7000, 7000, 9000, 3000, 5000, 3000, 5000, 3000,
            3000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000, 7000, 9000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cargarLineaNegocio() {
        try {
            this.printlnResponseAjax(dao.cargarLineaNegocio(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarEmpresaEnvio() {
        try {
            this.printlnResponseAjax(dao.cargarEmpresaEnvio(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarSolicitudesDocumentos() {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String linea_negocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
            String empresa = request.getParameter("empresa") != null ? request.getParameter("empresa") : "";
            String consulta = request.getParameter("consulta") != null ? request.getParameter("consulta") : "";
            this.printlnResponseAjax(dao.cargarSolicitudesDocumentos(fechaInicio, fechaFin, linea_negocio, empresa, consulta), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void guardarSolicitudesSeleccionadas() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            String empresa_envio = request.getParameter("empresa_envio") != null ? request.getParameter("empresa_envio") : "";
            String exportar = request.getParameter("exportar") != null ? request.getParameter("exportar") : "";
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            // JsonArray jsonSelect = new JsonParser().parse(json).getAsJsonArray();
            this.printlnResponseAjax(dao.guardarSolicitudesSeleccionadas(obj, usuario, empresa_envio, exportar), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void exportarExcelSolicitudesSeleccionadas() throws Exception {
        String json = request.getParameter("info") != null ? request.getParameter("info") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "solicitudes_seleccionadas_", titulo = "SOLCITUDES SELECCIONADAS";

        cabecera = new String[]{"Linea Negocio", "Numero solicitud", "Estado negocio", "Estado solicitud", "tipo", "Nombre", "Identificacion", "Direccion",
            "Ciudad", "Municipio", "Telefono", "Celular", "tipo", "Nombre", "Identificacion", "Direccion", "Ciudad", "Municipio",
            "Telefono", "Celular", "Negocio", "Fecha solcitud", "Fecha entrega documento", "Fecha recibido"};

        dimensiones = new short[]{
            5000, 5000, 5000, 5000, 3000, 9000, 6000, 6000,
            5000, 6000, 4000, 5000, 3000, 9000, 6000, 6000, 5000, 6000,
            4000, 5000, 5000, 5000, 6000, 6000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cargarLineaNegocio_() {
        try {
            this.printlnResponseAjax(dao.cargarLineaNegocio_(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void exportarExcelAuditoriaComercial() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Autitoria_credi100_", titulo = "AUDITORIA CREDI 100";

        cabecera = new String[]{"Entidad", "Numero solicitud", "Negocio", "Identificacion", "Nombre cliente", "Celular", "Email", "Afiliado", "Estado inicial solicitud",
            "Estado solicitud", "Estado negocio", "Causal", "Asesor", "id_convenio", "Monto credito", "Valor cuota", "Numero cuotas", "Comentario", "Etapa", "Medio", "Fecha presolicitud"};

        dimensiones = new short[]{
            7000, 5000, 3000, 5000, 5000, 9000, 7000, 7000, 7000, 9000, 3000, 5000, 3000, 5000, 3000,
            3000, 5000, 5000, 3000, 5000, 3000,};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cargarAutorizador() {
        try {
            this.printlnResponseAjax(dao.cargarAutorizador(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdminFintraAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarHC() {
        try {
            this.printlnResponseAjax(dao.cargarHC(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdminFintraAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void buscarProveedor() {
        try {
            String parametro_busqueda = request.getParameter("busqueda");
            String informacion_ = request.getParameter("informacion_");
            this.printlnResponseAjax(dao.buscarProveedor(informacion_, parametro_busqueda), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buscarImpuesto() {
        try {
            String impuesto = request.getParameter("impuesto");
            this.printlnResponseAjax(dao.buscarImpuesto(impuesto), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void guardarDocumentosCXP() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            // JsonArray jsonSelect = new JsonParser().parse(json).getAsJsonArray();
            this.printlnResponseAjax(dao.guardarCabeceraCXP(obj, usuario), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buscarCuentas() {
        try {
            String parametro_busqueda = request.getParameter("busqueda");
            String informacion = request.getParameter("info");
            this.printlnResponseAjax(dao.buscarCuentas(parametro_busqueda, informacion), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarMotivo() {
        try {
            this.printlnResponseAjax(dao.cargarMotivo(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buscarMultiservicio() {
        try {
            String multiservicio = request.getParameter("multiservicio");
            this.printlnResponseAjax(dao.buscarMultiservicio(multiservicio, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarCxpCabecera() {
        try {
            String documento = request.getParameter("documento") != null ? request.getParameter("documento") : "";
            this.printlnResponseAjax(dao.cargarCxpCabecera(documento), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarCxpDetalle() {
        try {
            String documento = request.getParameter("documento") != null ? request.getParameter("documento") : "";
            this.printlnResponseAjax(dao.cargarCxpDetalle(documento), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarNegociosCuotaManejo() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cargarSolicitudesDocumentos(negocio), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void anularCuotaManejo() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.anularCuotaManejo(obj, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarNegocioRechazado() {

        try {
            String numero_solicitud = (request.getParameter("numero_solicitud") != null) ? request.getParameter("numero_solicitud") : "";
            String identificacion = (request.getParameter("identificacion") != null) ? request.getParameter("identificacion") : "";
            String fechaInicio = request.getParameter("fecha_ini") != null ? request.getParameter("fecha_ini") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            this.printlnResponseAjax(dao.cargarNegocioRechazado(numero_solicitud, identificacion, fechaInicio, fechaFin), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void reactivarNegocio() {
        try {
            String numero_solicitud = (request.getParameter("numero_solicitud") != null) ? request.getParameter("numero_solicitud") : "";
            String comentario = (request.getParameter("comentario") != null) ? request.getParameter("comentario") : "";
            String fechaInicio = request.getParameter("fecha_ini") != null ? request.getParameter("fecha_ini") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            this.printlnResponseAjax(dao.reactivarNegocio(numero_solicitud, comentario, fechaInicio, fechaFin, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void guardarDocumentosAnticipoCajaMenor() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.guardarDocumentosAnticipoCajaMenor(obj, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarAnticipoCajaMenor() {
        try {
            String fechaInicio = request.getParameter("fechaini") != null ? request.getParameter("fechaini") : "";
            String fechaFin = request.getParameter("fechafin") != null ? request.getParameter("fechafin") : "";
            String identificacion = (request.getParameter("empleado") != null) ? request.getParameter("empleado") : "";
            String anticipo = (request.getParameter("anticipo") != null) ? request.getParameter("anticipo") : "";
            this.printlnResponseAjax(dao.cargarAnticipoCajaMenor(fechaInicio, fechaFin, identificacion, anticipo), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void legalizarAnticipoCajaMenor() {
        try {
            String valorCXP = request.getParameter("valorCXP") != null ? request.getParameter("valorCXP") : "";
            String valorLegalizar = request.getParameter("valorLegalizar") != null ? request.getParameter("valorLegalizar") : "";
            String anticipo = (request.getParameter("anticipo") != null) ? request.getParameter("anticipo") : "";
            this.printlnResponseAjax(dao.legalizarAnticipoCajaMenor(valorCXP, valorLegalizar, anticipo, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void buscarCXPGastos() {
        try {
            String anticipo = (request.getParameter("cod_anticipo") != null) ? request.getParameter("cod_anticipo") : "";
            this.printlnResponseAjax(dao.buscarCXPGastos(anticipo), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarDetalleAnticipoCajaMenor() {
        try {
            String anticipo = (request.getParameter("idanticipo") != null) ? request.getParameter("idanticipo") : "";
            this.printlnResponseAjax(dao.cargarDetalleAnticipoCajaMenor(anticipo), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarDocumentosAfectoCXC() {
        try {
            String factura = (request.getParameter("factura") != null) ? request.getParameter("factura") : "";
            this.printlnResponseAjax(dao.cargarDocumentosAfectoCXC(factura), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarDocumentosAfectoGCM() {
        try {
            String cxp = (request.getParameter("cxp") != null) ? request.getParameter("cxp") : "";
            this.printlnResponseAjax(dao.cargarDocumentosAfectoGCM(cxp), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void anularAnticipo() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.anularAnticipo(id, usuario)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    private void cargarAnticipoTransferenciaPaso() {
        try {
            ParmetrosDinamicaTSP pdtsp = new ParmetrosDinamicaTSP();
            pdtsp.setPaso((request.getParameter("paso") != null) ? request.getParameter("paso") : "");
            pdtsp.setFiltro_general((request.getParameter("filtro_") != null) ? request.getParameter("filtro_") : "");
            pdtsp.setPeriodo_inicio((request.getParameter("periodo_inicio") != null) ? request.getParameter("periodo_inicio") : "");
            pdtsp.setPeriodo_fin((request.getParameter("periodo_fin") != null) ? request.getParameter("periodo_fin") : "");
            pdtsp.setTipo_anticipo((request.getParameter("tipo") != null) ? request.getParameter("tipo") : "");

            this.printlnResponseAjax(dao.cargarAnticipoTransferenciaPaso(pdtsp, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarReporteProduccion() {
        try {
            String paso = (request.getParameter("paso") != null) ? request.getParameter("paso") : "";
            String periodo = (request.getParameter("periodo") != null) ? request.getParameter("periodo") : "";
            String filtro = (request.getParameter("filtro_") != null) ? request.getParameter("filtro_") : "";
            String periodo_inicio = (request.getParameter("periodo_inicio") != null) ? request.getParameter("periodo_inicio") : "";
            String periodo_fin = (request.getParameter("periodo_fin") != null) ? request.getParameter("periodo_fin") : "";
            String concept_code = (request.getParameter("concept_code") != null) ? request.getParameter("concept_code") : "";
            String anulado = (request.getParameter("anulado") != null) ? request.getParameter("anulado") : "";
            this.printlnResponseAjax(dao.cargarReporteProduccion(paso, periodo, filtro, periodo_inicio, periodo_fin, concept_code, anulado), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarConsolidadoMovimientoTSP() {
        try {
            this.printlnResponseAjax(dao.cargarConsolidadoMovimientoTSP(), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarAnalisisReporteProduccion() {
        try {
            ParmetrosDinamicaTSP pdtsp = new ParmetrosDinamicaTSP();
            pdtsp.setPaso((request.getParameter("paso") != null) ? request.getParameter("paso") : "");
            pdtsp.setFiltro_general((request.getParameter("filtro_") != null) ? request.getParameter("filtro_") : "");
            pdtsp.setPeriodo_inicio((request.getParameter("periodo_inicio") != null) ? request.getParameter("periodo_inicio") : "");
            pdtsp.setPeriodo_fin((request.getParameter("periodo_fin") != null) ? request.getParameter("periodo_fin") : "");
            pdtsp.setEstadoReporte((request.getParameter("anulado") != null) ? request.getParameter("anulado") : "");
            this.printlnResponseAjax(dao.cargarAnalisisReporteProduccion(pdtsp), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportarExcelAnalisisReporteProduccion() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Autitoria_credi100_", titulo = "AUDITORIA CREDI 100";

        cabecera = new String[]{"Periodo Anticipo", "Descripcion", "Valor", "Valor Descuento", "Valor Neto", "Valor Combancaria", "Valor Consignacion"};

        dimensiones = new short[]{
            7000, 7000, 7000, 7000, 7000, 7000, 7000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cagarReporteInversionista() {
        try {
            ParmetrosDinamicaTSP pdt = new ParmetrosDinamicaTSP();
            pdt.setPaso((request.getParameter("paso") != null) ? request.getParameter("paso") : "");
            pdt.setFiltro_general((request.getParameter("filtro_") != null) ? request.getParameter("filtro_") : "");
            pdt.setPeriodo_inicio((request.getParameter("periodo_inicio") != null) ? request.getParameter("periodo_inicio") : "");
            pdt.setPeriodo_fin((request.getParameter("periodo_fin") != null) ? request.getParameter("periodo_fin") : "");
            pdt.setTercero((request.getParameter("tercero") != null) ? request.getParameter("tercero") : "");
            this.printlnResponseAjax(dao.cagarReporteInversionista(pdt), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarTercerInversionistas() {
        try {
            this.printlnResponseAjax(dao.cagarTercerInversionistas(), "application/json;");
        } catch (Exception e) {
        }
    }

    private void exportarExcelReporteInversionista() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        String paso = request.getParameter("paso") != null ? request.getParameter("paso") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "ReporteInversionista" + paso, titulo = "REPORTE INVERDIONISTA" + paso.toUpperCase();

        cabecera = new String[]{"Periodo", "Tipo Comprobante", "Cuenta", "Valor Debito", "Valor Credito"};

        dimensiones = new short[]{
            7000, 7000, 7000, 7000, 7000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void cagarReporteInversionistaSinFiltros() throws Exception {
//        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";

        ParmetrosDinamicaTSP pdt = new ParmetrosDinamicaTSP();
        pdt.setPaso((request.getParameter("paso") != null) ? request.getParameter("paso") : "");
        pdt.setFiltro_general((request.getParameter("filtro_") != null) ? request.getParameter("filtro_") : "");
        pdt.setPeriodo_inicio((request.getParameter("periodo_inicio") != null) ? request.getParameter("periodo_inicio") : "");
        pdt.setPeriodo_fin((request.getParameter("periodo_fin") != null) ? request.getParameter("periodo_fin") : "");
        pdt.setTercero((request.getParameter("tercero") != null) ? request.getParameter("tercero") : "");
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.cagarReporteInversionistaSinFiltros(pdt));
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "ReporteInversionista" + pdt.getPaso(), titulo = "REPORTE INVERDIONISTA" + pdt.getPaso().toUpperCase();

        cabecera = new String[]{"Dstrct", "Cuenta", "Auxiliar", "Periodo", "Fecha Documento", "Tipo Documento", "Tipo Doc Desc", "Numero documento", "Detalle", "Detalle Comprobante",
            "ABC", "Valor Debito", "Valor Credito", "Tercero", "Nombre Tercero", "Tipo documento relacionado", "Documento Relacionado", " Valor for", "Modena Foranea", "Tipo Referencia 1",
            "Referencia 1", "Tipo Referencia 2", "Referencia 2", "Tipo Referencia 3", "Referencia 3", "Documento rel2", "referencia 4"};

        dimensiones = new short[]{
            7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000,
            7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000, 7000,
            7000, 7000, 7000, 7000, 7000, 7000, 7000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cagarHelpDinamicasContables() {
        try {
            ParmetrosDinamicaTSP pdt = new ParmetrosDinamicaTSP();
            pdt.setPaso((request.getParameter("paso") != null) ? request.getParameter("paso") : "");
            pdt.setModulo((request.getParameter("modulo") != null) ? request.getParameter("modulo") : "");
            this.printlnResponseAjax(dao.cagarHelpDinamicasContables(pdt), "application/text;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void permisoHelpDinamicasContables() {
        try {
            this.printlnResponseAjax(dao.permisoHelpDinamicasContables(usuario), "application/text;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cagarHelpDinamicasContablesDatos() {
        try {
            ParmetrosDinamicaTSP pdt = new ParmetrosDinamicaTSP();
            pdt.setPaso((request.getParameter("paso") != null) ? request.getParameter("paso") : "");
            pdt.setModulo((request.getParameter("modulo") != null) ? request.getParameter("modulo") : "");
            this.printlnResponseAjax(dao.cagarHelpDinamicasContablesDatos(pdt), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actualizarHelpDinamicasContablesDatos() {
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "";
            String textoAyuda = (request.getParameter("textoAyuda") != null) ? request.getParameter("textoAyuda") : "";
            this.printlnResponseAjax(dao.actualizarHelpDinamicasContablesDatos(id, textoAyuda, usuario), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportarExcelTransferenciaProntoPagoCombustible() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        String paso = request.getParameter("paso") != null ? request.getParameter("paso") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte" + paso, titulo = "REPORTE " + paso.toUpperCase();

        cabecera = new String[]{"Periodo", "Cuenta", "Valor Debito", "Valor Credito"};

        dimensiones = new short[]{
            7000, 7000, 7000, 7000, 7000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cargarRevisionPaso2() {
        try {
            String modo_busqueda = request.getParameter("modo_busqueda") != null ? request.getParameter("modo_busqueda") : "";
            String nulo = request.getParameter("nulo") != null ? request.getParameter("nulo") : "";
            this.printlnResponseAjax(dao.cargarRevisionPaso2(modo_busqueda, nulo), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cagarSeguimientoCaja() {
        try {
            String anio = request.getParameter("anio") != null ? request.getParameter("anio") : "";
            String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";
            String cuenta = request.getParameter("cuenta") != null ? request.getParameter("cuenta") : "";
            this.printlnResponseAjax(dao.cagarSeguimientoCaja(anio, fecha, cuenta, usuario), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void procedimientoSeguimientoCaja() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.procedimientoSeguimientoCaja(obj, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarCuentas() {
        try {
            this.printlnResponseAjax(dao.cargarCuentas(), "application/json;");
        } catch (Exception e) {
        }
    }

    private void exportarExcelSeguimientoCaja() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Posicion_Caja", titulo = "Posicion de Caja";

        cabecera = new String[]{"Id_", "Cuenta", "Nombre Cuenta", "Fecha Movimiento", "Saldo Anterior", "Valor Debito", "Valor Credito", "Saldo Actual Libro", "Saldo Extracto", "Diferencia"};

        dimensiones = new short[]{
            3000, 7000, 9000, 7000, 7000, 7000, 7000, 7000, 7000, 7000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void buscarAutoCompletarCuentas() {
        try {
            String informacion_ = request.getParameter("informacion_");
            this.printlnResponseAjax(dao.buscarAutoCompletarCuentas(informacion_), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cagarConfiguracionCuentasDC() {
        try {
            String modulo = request.getParameter("modulo") != null ? request.getParameter("modulo") : "";
            String cuenta = request.getParameter("cuenta") != null ? request.getParameter("cuenta") : "";
            this.printlnResponseAjax(dao.cagarConfiguracionCuentasDC(modulo, cuenta, usuario), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cambiarEstadoConfiguracionCuentasDC() {
        try {
            String accion_ = request.getParameter("accion_") != null ? request.getParameter("accion_") : "";
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String json = request.getParameter("info") != null ? request.getParameter("info") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            switch (accion_) {
                case "uno_uno":
                    this.printlnResponseAjax(dao.cambiarEstadoConfiguracionCuentasDC(id, usuario), "application/json;");
                    break;
                case "multiple":
                    this.printlnResponseAjax(dao.eliminarMultipleConfiguracionCuentasDC(obj), "application/json;");
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cagarInformacionCuenta() {
        try {
            String cuenta = request.getParameter("cuenta") != null ? request.getParameter("cuenta") : "";
            this.printlnResponseAjax(dao.cagarConfiguracionCuentasDC(cuenta), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarInformacionCuenta() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.guardarInformacionCuenta(obj, usuario), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void actualizarInformacionCuenta() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.actualizarInformacionCuenta(obj, usuario), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarConcepto() {
        try {
            this.printlnResponseAjax(dao.cargarConcepto(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarTipoIngreso() {
        try {
            this.printlnResponseAjax(dao.cargarTipoIngreso(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarHcNombres() {
        try {
            this.printlnResponseAjax(dao.cargarHcNombres(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cagarFacturas() {
        try {
            String nit = request.getParameter("nit") != null ? request.getParameter("nit") : "";
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cagarFacturas(nit, negocio), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cagarMoneda() {
        try {
            this.printlnResponseAjax(dao.cagarMoneda(), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportarMovimientoAuxiliar() throws Exception {
        String periodoInicio = request.getParameter("periodo_inicio") == null ? "" : request.getParameter("periodo_inicio");
        String periodoFin = request.getParameter("periodo_fin") == null ? "" : request.getParameter("periodo_fin");
        String arrayCuentas[] = request.getParameter("listadocuentas").split(",");

        String lineacuenta = "";
        int longitud = arrayCuentas.length;
        for (int i = 0; i < longitud - 1; i++) {
            lineacuenta = lineacuenta + "'" + arrayCuentas[i] + "',";
        }
        lineacuenta = lineacuenta + "'" + arrayCuentas[longitud - 1] + "'";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.cargarInfoMovimientoAuxiliar(lineacuenta, periodoInicio, periodoFin));

        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_movimiento_auxiliar", titulo = "MOVIMIENTO AUXILIAR";

        cabecera = new String[]{"Cuenta", "Periodo", "Fecha", "Tipo Comprobante", "No Comprobante", "Descripcion", "D�bito", "Cr�dito", "Nit Tercero", "Tercero",
            "Tipo Doc Rel", "Documento Relacionado", "Tipo Referencia 1", "Referencia 1", "Tipo Referencia 2", "Referencia 2", "Tipo Referencia 3", "Referencia 3"};

        dimensiones = new short[]{
            4000, 3000, 5000, 4000, 5000, 8000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void buscarCliente() {
        try {
            String parametro_busqueda = request.getParameter("busqueda");
            String informacion_ = request.getParameter("informacion_");
            this.printlnResponseAjax(dao.buscarCliente(informacion_, parametro_busqueda), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buscarNegocio() {
        try {
            String negocio = request.getParameter("negocio");
            this.printlnResponseAjax(dao.buscarNegocio(negocio), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void guardarDatosIngresos() {
        try {
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.guardarDatosIngresos(obj, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarPagosSinReferencia() {
        try {
            String fechaInicio = request.getParameter("fechaInicio") != null ? request.getParameter("fechaInicio") : "";
            String fechaFin = request.getParameter("fechaFin") != null ? request.getParameter("fechaFin") : "";
            this.printlnResponseAjax(dao.cargarPagosSinReferencia(fechaInicio, fechaFin), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDocumentosRequeridos() {
        try {
            String id_unidad_negocio = request.getParameter("id_unidad_negocio") == null ? "" : request.getParameter("id_unidad_negocio");
            String id_solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            this.printlnResponseAjax(new Gson().toJson(dao.getDocumentosRequeridos(id_unidad_negocio, id_solicitud)), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cagarFintraSoluciones() {
        try {
            ParmetrosDinamicaTSP pdt = new ParmetrosDinamicaTSP();
            pdt.setPaso((request.getParameter("paso") != null) ? request.getParameter("paso") : "");
            pdt.setFiltro_general((request.getParameter("filtro_") != null) ? request.getParameter("filtro_") : "");
            pdt.setPeriodo_inicio((request.getParameter("periodo_inicio") != null) ? request.getParameter("periodo_inicio") : "");
            pdt.setPeriodo_fin((request.getParameter("periodo_fin") != null) ? request.getParameter("periodo_fin") : "");
            this.printlnResponseAjax(dao.cargarFintraSoluciones(pdt), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportarFintraSoluciones() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Fintra_Soluciones", titulo = "Fintra Soluciones";

        cabecera = new String[]{"Periodo", "Tipo Comprobante", "Cuenta", "Valor Debito", "Valor Credito"};

        dimensiones = new short[]{
            3000, 7000, 9000, 7000, 7000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cargarModulos() {
        try {
            this.printlnResponseAjax(dao.cargarModulos(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarPazYSalvo() {
        try {
            String cliente = request.getParameter("cliente") != null ? request.getParameter("cliente") : "";
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cargarPazYSalvo(cliente, negocio), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarLiquidacionDetalle() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            String fecha_max = request.getParameter("fecha_max") != null ? request.getParameter("fecha_max") : "";
            String general = request.getParameter("general") != null ? request.getParameter("general") : "";
            this.printlnResponseAjax(dao.cargarLiquidacionDetalle(negocio, fecha_max, general), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void exportarPdfPazYSalvo() {
        try {
            String json = request.getParameter("info") != null ? request.getParameter("info") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.printlnResponseAjax(dao.datosPdfPazYSalvo(obj, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void generarEstadoCuenta() {
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}");
            this.printlnResponseAjax(dao.exportarPdfEstadoCuenta(usuario, informacion), "application/json;");
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buscarAsesor() {
        try {
            String asesor = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
            this.printlnResponseAjax(dao.buscarAsesor(asesor), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void buscarNegocios() {
        try {
            String asesor = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
            String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String estadoCartera = request.getParameter("estadoCartera") != null ? request.getParameter("estadoCartera") : "";
            this.printlnResponseAjax(dao.buscarNegocios(asesor, fecha_inicio, fecha_fin ,estadoCartera), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarAsesores() {
        try {
            String id_combo = request.getParameter("id_combo") != null ? request.getParameter("id_combo") : "";
            String producto = request.getParameter("producto") != null ? request.getParameter("producto") : "";
            this.printlnResponseAjax(dao.cargarAsesores(id_combo, producto), "application/json;");
        } catch (Exception ex) {
             Logger.getLogger(AdminFintraAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarNegocios() {
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}");
            this.printlnResponseAjax(dao.actualizarNegocios(informacion, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
 
    public void cargarObservaciones() {
        try {
            String numero_solicitud = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "";
            this.printlnResponseAjax(dao.cargarObservaciones(numero_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void guardarObservaciones() {
        try {
            String numero_solicitud = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "";
            String observaciones = request.getParameter("observaciones") != null ? request.getParameter("observaciones") : "";
            this.printlnResponseAjax(dao.guardarObservaciones(numero_solicitud, observaciones, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void cagarAnalisisRecaudo() {
        try {
            String banco = request.getParameter("banco") != null ? request.getParameter("banco") : "";
            String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            this.printlnResponseAjax(dao.cagarAnalisisRecaudo(banco, fecha_inicio, fecha_fin), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cagarBarrios() {
        try {
            String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";
            this.printlnResponseAjax(dao.cagarBarrios(ciudad), "application/json;");
        } catch (Exception ex) {
             Logger.getLogger(AdminFintraAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarDetalleNegocioLiquidacion() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cargarDetalleNegocioLiquidacion(negocio), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarFacturasCausacionIntereses() {
         try {
            String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
            String cmc = request.getParameter("cmc") != null ? request.getParameter("cmc") : "";
            this.printlnResponseAjax(dao.cargarFacturasCausacionIntereses(periodo, cmc), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void causarInteresesMultiservicio() {
         try {
             String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
             String cmc = request.getParameter("cmc") != null ? request.getParameter("cmc") : "";
             String cta_ingreso = request.getParameter("cta_ingreso") != null ? request.getParameter("cta_ingreso") : "";
             String comprobanteGenerado = dao.causarInteresesMultiservicio(periodo, cmc, cta_ingreso, usuario);
             if (comprobanteGenerado != null && !comprobanteGenerado.equals("")) {
                 exportarExcelCausacionIntereses(comprobanteGenerado);
             }
             //this.printlnResponseAjax(dao.causarInteresesMultiservicio(periodo, cmc, cta_ingreso, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarCmcCausacionInteresesMS() {
        try {
            this.printlnResponseAjax(dao.cargarCmcCausacionInteresesMS(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_negocio_rechazo() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.cargar_negocio_rechazo(negocio), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void rechazarNegocio() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.rechazarNegocio(negocio, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_convenios() {
        try {
            this.printlnResponseAjax(dao.cargar_convenios(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void exportarexcelreporte() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        String nameColum = ((String) request.getParameter("namecolum") != null ? request.getParameter("namecolum") : "");
        String namereporte = ((String) request.getParameter("namereporte") != null ? request.getParameter("namereporte") : "");
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_" + namereporte, titulo = "Reporte " + namereporte;
        cabecera = nameColum.split(",");
        dimensiones = new short[]{
            5000, 5000, 5000, 7000, 7000, 7000, 7000, 7000, 7000, 7000,
            7000, 7000, 7000, 7000, 7000, 7000, 7000
        };
        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void extracto_estado_cuenta() {
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}");
            this.printlnResponseAjax(dao.extracto_estado_cuenta(usuario, informacion), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void exportarExcelCausacionIntereses(String comprobanteGenerado) throws Exception {
        JsonArray json = (JsonArray) new JsonParser().parse(dao.cargarCompobanteCausacion(comprobanteGenerado));        ;
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Causacion", titulo = "Reporte Causacion Intereses";
        cabecera = new String[]{"Tipodoc", "Numdoc", "Periodo", "Cuenta", "Detalle", "Valor Debito", "Valor Credito", "Tercero", "Tipodoc Rel", 
            "Documento Rel", "Tipo Referencia 1", "Referencia 1"};
        dimensiones = new short[]{
            5000, 5000, 5000, 7000, 7000, 7000, 7000, 7000, 7000, 7000,
            7000, 7000
        };
        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, json, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void cargaPresolicitudesMicrocredito() {
         try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String estado_solicitud = request.getParameter("estado_solicitud") != null ? request.getParameter("estado_solicitud") : "";
            String etapa = request.getParameter("etapa") != null ? request.getParameter("etapa") : "";
            String r_operaciones = request.getParameter("r_operaciones") != null ? request.getParameter("r_operaciones") : "";
            String unidadNegocio = request.getParameter("unidad_negocio") != null ? request.getParameter("unidad_negocio") : "";
            this.printlnResponseAjax(dao.cargaPresolicitudesMicrocredito(fechaInicio, fechaFin, identificacion, numero_solicitud, estado_solicitud, etapa,r_operaciones, unidadNegocio), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getNombresArchivos() {
        java.util.List lista = null;
        Gson gson = new Gson();
        try {
            String num_solicitud = (request.getParameter("num_solicitud1") != null) ? request.getParameter("num_solicitud1") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "fintracredit1/";
            this.printlnResponseAjax(gson.toJson(dao.searchNombresArchivos(rutaOrigen, num_solicitud)), "application/json");
        } catch (Exception e) {
            System.out.println("error::" + e.toString() + "__" + e.getMessage());
        }
    }

    public void almacenarArchivoEnCarpetaUsuario() throws Exception {
        try {
            String num_solicitud = (request.getParameter("num_solicitud1") != null) ? request.getParameter("num_solicitud1") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? request.getParameter("nomarchivo") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "fintracredit1/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";
            if (dao.almacenarArchivoEnCarpetaUsuario(num_solicitud, ("" + rutaOrigen + num_solicitud), ("" + rutaDestino), nomarchivo)) {
                this.printlnResponseAjax("{\"respuesta\":\"SI\",\"login\":\"" + usuario.getLogin() + "\"}", "application/json");
            } else {
                this.printlnResponseAjax("{\"respuesta\":\"NO\"}", "application/json");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    public void rechazarnegociomicrocredito() {
        String json = "{\"respuesta\":\"Negocio Rechazado\"}";
        try {
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String causal = request.getParameter("causal") != null ? request.getParameter("causal") : "";
            String estado = request.getParameter("estado_pre") != null ? request.getParameter("estado_pre") : "";
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.rechazarnegociomicrocredito(usuario, numero_solicitud));
            tService.getSt().addBatch(this.dao.standbytrazabilidadpresolicitudes(usuario, numero_solicitud, causal, estado));
            tService.execute();
            //this.printlnResponseAjax(dao.rechazarnegociomicrocredito(usuario, numero_solicitud, causal), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    private void cargarnegociosdesistir() {
         try {            
            String cod_neg = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
            this.printlnResponseAjax(dao.cargarnegociosdesistir(cod_neg,identificacion), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void standbypresolicitudes() {
        String json = "{\"respuesta\":\"Negocio puesto en Stand By\"}";
        try {
            JsonObject objeto = new JsonObject();
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String causal = request.getParameter("causal") != null ? request.getParameter("causal") : "";
            String estado = request.getParameter("estado_pre") != null ? request.getParameter("estado_pre") : "";
            //this.printlnResponseAjax(dao.standbypresolicitudes(usuario, numero_solicitud), "application/json;");
            
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.standbypresolicitudes(usuario, numero_solicitud));
            tService.getSt().addBatch(this.dao.standbytrazabilidadpresolicitudes(usuario, numero_solicitud, causal, estado));
            tService.execute();
            String resp = "{\"respuesta\":\"Guardado\"}";
           if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                 String id_und_negocio = dao.getUnidadConvenio(numero_solicitud);
                String cliente = dao.getNombreCliente(numero_solicitud);
                String receiversType = (id_und_negocio.equals("1")) ? "Comerciales_Micro" : (id_und_negocio.equals("22")) ? "Comerciales_Libranza" : "Comerciales_Fenalco"; 
                String MyMessage = "Solicitud No " +numero_solicitud+ " a nombre del cliente " + cliente + " fue colocado en STANDBY  por el usuario " +usuario.getLogin()+ " debido a: "+causal;
                String mailsToSend = dao.getCorreosToSendStandBy(receiversType);                      
                String asunto = "Cambio estado Solicitud " +numero_solicitud+  " a StandBy";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "STANDBY", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
                System.out.println("Info email:" +MyMessage);
            }

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void devolverSolicitud() {
         String json = "{\"respuesta\":\"Negocio Devuelto\"}";
        try {
            
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            //this.printlnResponseAjax(dao.devolverSolicitud(usuario, numero_solicitud), "application/json;");
             TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.devolverSolicitud(usuario, numero_solicitud));
            tService.getSt().addBatch(this.dao.devolverSolicitudtrazabilidad(usuario, numero_solicitud));
            tService.execute();
            String resp = "{\"respuesta\":\"Guardado\"}";
           if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                 String id_und_negocio = dao.getUnidadConvenio(numero_solicitud);
                String cliente = dao.getNombreCliente(numero_solicitud);
                String receiversType = (id_und_negocio.equals("1")) ? "Coordinadores_Micro" : (id_und_negocio.equals("22")) ? "Coordinadores_Libranza" : "Coordinadores_Fenalco"; 
                String MyMessage = "Solicitud No " + numero_solicitud + " a nombre del cliente " + cliente + " fue devuelto de StandBy por el usuario " + usuario.getLogin() + ". Observaciones: Favor seguir con el debido proceso.";
                String mailsToSend = dao.getCorreosToSendStandBy(receiversType);                      
                String asunto = "Devoluci�n de solicitud " + numero_solicitud + " desde estado de StandBy";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "STANDBY", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
                System.out.println("Info email:" +MyMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    
    private void consultarCausalStandBy() {
        try {
            
            String numero_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            this.printlnResponseAjax(dao.consultarCausalStandBy(numero_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void cargarEntidadesPresolicitud () {
        try {            
            this.printlnResponseAjax(dao.cargarEntidadesPresolicitud(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarPresolicitudesMicroRechazadas() {
        try {
            printlnResponseAjax(dao.cargarPresolicitudesMicroRechazadas(), "application/json");
        } catch (Exception e) {
            Logger.getLogger("").log(Level.SEVERE, null, e);
            System.err.println("Error cargando las presolicitudes micro rechazadas: " + e.getMessage());
        }
    }
}

