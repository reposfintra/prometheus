/*
 * Codigo_discrepanciaInsertAction.java
 *
 * Created on 28 de junio de 2005, 01:41 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Codigo_discrepanciaInsertAction extends Action{
    
    /** Creates a new instance of Codigo_discrepanciaInsertAction */
    public Codigo_discrepanciaInsertAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "/jsp/trafico/codigo_discrepancia/Codigo_discrepanciaInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");   
        String dstrct = (String) session.getAttribute("Distrito");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        int sw=0;
        try{
            Codigo_discrepancia codigo_discrepancia = new Codigo_discrepancia();
            codigo_discrepancia.setDescripcion(descripcion);
            codigo_discrepancia.setCodigo(codigo);
            codigo_discrepancia.setCia(dstrct.toUpperCase());
            codigo_discrepancia.setUser_update(usuario.getLogin().toUpperCase());
            codigo_discrepancia.setCreation_user(usuario.getLogin().toUpperCase());
            codigo_discrepancia.setBase(usuario.getBase().toUpperCase());
            try{
                model.codigo_discrepanciaService.insertCodigo_discrepancia(codigo_discrepancia);
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.codigo_discrepanciaService.existCodigo_discrepancia(codigo)){
                    model.codigo_discrepanciaService.updateCodigo_discrepancia(codigo, descripcion, usuario.getLogin().toUpperCase());                    
                    request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                }
                else{
                    request.setAttribute("mensaje","Error el Codigo de Discrepancia ya existe!");
                }
            }
            else{
                request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
