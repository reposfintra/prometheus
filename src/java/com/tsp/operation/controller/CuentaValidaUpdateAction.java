/********************************************************************
 *      Nombre Clase.................   CuentaValidaUpdateAction.java
 *      Descripci�n..................   Modifica y Anula Cuentas Validas
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  EQUIPO12
 */
public class CuentaValidaUpdateAction extends Action{
    
    /** Creates a new instance of CuentaValidaUpdateAction */
    public CuentaValidaUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String user = usuario.getLogin ();
        String base = usuario.getBase ();
        String dstrct = usuario.getDstrct ();
        String clase = request.getParameter ("clase1");
        String agencia = request.getParameter ("agencia1");
        String unidad = request.getParameter ("unidad1");
        String cliente_area = request.getParameter ("cliente_area2");
        String elemento = request.getParameter ("elemento1");
        String clase1 = request.getParameter ("clase");
        String agencia1 = request.getParameter ("agencia");
        String unidad1 = request.getParameter ("unidad");
        String cliente_area1 = request.getParameter ("cliente_area");
        String elemento1 = request.getParameter ("elemento");
        String sw = request.getParameter ("sw");
        CuentaValida cuenta = new CuentaValida ();
        cuenta.setClase (clase);
        cuenta.setAgencia (agencia);
        cuenta.setUnidad (unidad);
        cuenta.setCliente_area (cliente_area);
        cuenta.setElemento (elemento);
        cuenta.setDstrct (dstrct);
        cuenta.setCreation_user (user);
        cuenta.setBase (base);
        cuenta.setUser_update (user);
        try {
            if (sw.equals ("Mostrar")){
                next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasUpdate.jsp?clase="+clase1+"&agencia="+agencia1+"&unidad="+unidad1+"&elemento="+elemento1+"&cliente_area="+cliente_area1+"&msg=";
            }else if (sw.equals ("Modificar")){
                boolean existe=false;
                CuentaValida CV = new CuentaValida ();
                existe = model.cuentavalidaservice.ExisteCuentaValida (clase, agencia, unidad, cliente_area, elemento);
                if(agencia.equals (agencia1)&&clase.equals (clase1)&&unidad.equals (unidad1)&&cliente_area.equals (cliente_area1)&&elemento.equals (elemento1)){
                    next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasUpdate.jsp?reload=ok&msg=Cuenta Valida Modificada Satisfactoriamente";
                }else if (existe==true){
                    List cuentas = model.cuentavalidaservice.ListCuentaValida (clase, agencia, cliente_area,  elemento, unidad);
                    for (int i = 0; i < cuentas.size (); i++){
                        CV = (CuentaValida)cuentas.get (i);
                    }
                    if (CV.getReg_status ().equals ("")){
                        next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasUpdate.jsp?msg=Esa cuenta Valida ya Existe";
                    }
                }else{
                    model.cuentavalidaservice.UpdateCuentaValida (cuenta, clase1, agencia1, unidad1, cliente_area1, elemento1);
                    next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasUpdate.jsp?reload=ok&msg=Cuenta Valida Modificada Satisfactoriamente";
                }
            }else{
                
                model.cuentavalidaservice.EliminarCuentaValida (clase, agencia, unidad, cliente_area1, elemento);
                next = "/jsp/trafico/mensaje/MsgEliminado.jsp";
            }
            
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
