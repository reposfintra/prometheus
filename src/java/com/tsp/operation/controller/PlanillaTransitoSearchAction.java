/*
 * PlanillaAnticipoSearchAction.java
 *
 * Created on 28 de diciembre de 2004, 09:04 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class PlanillaTransitoSearchAction extends Action{
    
    /** Creates a new instance of PlanillaAnticipoSearchAction */
    public PlanillaTransitoSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/despacho/InicioMasivo.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            model.planillaService.setPlaaux(null);
            if(request.getParameter("remision")!=null){
                String remision = "CC"+request.getParameter("remision").toUpperCase();
                model.planillaService.buscarTransitoRemision(remision, usuario.getBase());
                model.planillaService.bucaPlanilla(remision);
            }
            else if(request.getParameter("placa")!=null){
                String placa = request.getParameter("placa").toUpperCase();
                model.planillaService.buscarTransitoPlaca(placa, usuario.getBase());
            }
            if(model.planillaService.getPlaaux()!=null){
                next="/despacho/SalidaViaje.jsp?remision="+model.planillaService.getPlaaux().getRemision();
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
