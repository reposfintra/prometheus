/******
 * Nombre:        Jerarquia_tiempoUpdateAction.java            
 * Descripci�n:   Clase Action para actualizar jerarquia tiempo   
 * Autor:         Ing. Jose de la Rosa 
 * Fecha:         26 de junio de 2005, 04:13 PM                     
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 *******/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class Jerarquia_tiempoUpdateAction extends Action{
    
    /** Creates a new instance of Jerarquia_tiempoUpdateAction */
    public Jerarquia_tiempoUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/jerarquia/Jerarquia_tiempoModificar.jsp?lista=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String actividad = request.getParameter("c_actividad");
        String responsable = request.getParameter("c_responsable");
        String demora = request.getParameter("c_demora");
        String nactividad = request.getParameter("c_nactividad");
        String nresponsable = request.getParameter("c_nresponsable");
        String ndemora = request.getParameter("c_ndemora");
        try{
            Jerarquia_tiempo jerarquia_tiempo = new Jerarquia_tiempo();
            jerarquia_tiempo.setActividad(actividad);
            jerarquia_tiempo.setResponsable(responsable);
            jerarquia_tiempo.setDemora(demora);
            jerarquia_tiempo.setUser_update(usuario.getLogin().toUpperCase());
            jerarquia_tiempo.setCia((String)session.getAttribute("Distrito"));
            
            try {
                model.jerarquia_tiempoService.updateJerarquia_tiempo(actividad,responsable,demora, usuario.getLogin().toUpperCase(),nactividad,nresponsable,ndemora, usuario.getBase());
                next+="&reload=ok";
                request.setAttribute("mensaje","MsgModificado");
                ////System.out.println("Modificacion Existosa!");
            }
            catch (SQLException ex) {
                if (!model.jerarquia_tiempoService.existJerarquia_tiempo(actividad, responsable, demora)){
                    model.jerarquia_tiempoService.updateJerarquia_tiempo(actividad,responsable,demora, usuario.getLogin().toUpperCase(),actividad,responsable,demora, usuario.getBase());
                    next+="&reload=ok";
                    request.setAttribute("mensaje","MsgModificado");
                    ////System.out.println("Modificacion Existosa!");
                }else {
                    request.setAttribute("mensaje","Error durante la modificacion, probablemente ya existe la Jerarquia Tiempo que se encuentra modificando!");
                    ////System.out.println("Error....ya existe!!");
                }
            }
            
            model.jerarquia_tiempoService.serchJerarquia_tiempo(actividad,responsable,demora);
            Jerarquia_tiempo jerarquia_tiempo2 = model.jerarquia_tiempoService.getJerarquia_tiempo();
            request.setAttribute("jerarquia_tiempo",jerarquia_tiempo2);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
