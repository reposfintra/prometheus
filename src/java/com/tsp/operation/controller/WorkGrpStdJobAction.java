/********************************************************************
 *      Nombre Clase.................   WorkGrpStdJobAction.java
 *      Descripci�n..................   Actualiza el el archivo wgroup_stadart
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class WorkGrpStdJobAction extends Action{
    
    public WorkGrpStdJobAction() {
    }
    
    public void run() throws ServletException, InformationException {        
               
        String[] stds = request.getParameterValues("c_stdSelec");
        String[] wgs = request.getParameterValues("c_wgSelec");
        
        String cliente = request.getParameter("clienteSelec");
        String origen = request.getParameter("origenSelec");
        String destino = request.getParameter("destinoSelec");
        
        String next="/jsp/masivo/workgroup_std/WorkGrpStdJInsert.jsp?mensaje=MsgUpdate";

        /*if( stds!=null)
            for( int i=0; i<stds.length; i++){
                ////System.out.println("............. se recibio: " + stds[i]);
            }

        if( wgs!=null)
            for( int i=0; i<wgs.length; i++){
                ////System.out.println("............. se recibio: " + wgs[i]);
            }
         */

        try{
            model.wgroup_stdjobSvc.obtenerCampoJSWorkGroups(stds, wgs);

            request.setAttribute("cliente", cliente);
            request.setAttribute("origen", origen);
            request.setAttribute("destino", destino);

            model.stdjobService.origenesStdJobsCliente(cliente);
            model.stdjobService.destinosStdJobsCliente(cliente, origen);
            model.stdjobService.listarStdJobsCliente(cliente, origen, destino);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }

        this.dispatchRequest(next);
    }

}
