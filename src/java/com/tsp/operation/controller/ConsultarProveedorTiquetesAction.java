/*
 * ConsultarProveedorTiquetesAction.java
 *
 * Created on 12 de enero de 2005, 10:10 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ConsultarProveedorTiquetesAction extends Action{
    
    /** Creates a new instance of ConsultarProveedorTiquetesAction */
    public ConsultarProveedorTiquetesAction() {
    }
    
    public void run() throws ServletException,InformationException {
        String nit = request.getParameter("nit");
        String ciudad = request.getParameter("ciudad").toUpperCase();
        String nombre = request.getParameter("nombre").toUpperCase();
        String next="/consultas/consultasRedirect.jsp?tiket=ok";
        try{
            if(request.getParameter("solo")==null){
                model.proveedortiquetesService.consultaProveedor(ciudad, nombre, nit);
                
            }
            else{
                model.proveedortiquetesService.buscaProveedor(nit,"");
                next="/consultas/datosProveedorTiquetes.jsp";
                
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
