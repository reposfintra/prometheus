/*
 * ClientactInsertarAction.java
 *
 * Created on 29 de agosto de 2005, 12:54 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes
 */
public class PruebaEnviarAction extends Action  {
    
    /** Creates a new instance of ClientactInsertarAction */
    public PruebaEnviarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/pruebaSenMail.jsp";
        //LLAMO EL HILO QUE ENVIA EMAILS
        Email e = new Email();
        e.setEmailbody("Esta es una pruebita");
        e.setEmailfrom("autorizaciones@sanchezpolo.com");
        e.setEmailsubject("PRUEBA");
        e.setSenderName("AUTORIZACIONES");
        e.setEmailto("kreales@sanchezpolo.com");
        e.setEmailcopyto("");
        ////System.out.println("Voy a correr el hilo de sendmail");
        // e.send("mail.tsp.com", e);
        com.tsp.operation.model.threads.SendMail s = new com.tsp.operation.model.threads.SendMail();
        s.start(e);
        
        this.dispatchRequest(next);
    }
    
}
