package com.tsp.operation.controller;


 
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.Usuario;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class LogProcesoAction extends Action {
    
     public void run() throws ServletException, InformationException{
       try{         
           
           HttpSession session = request.getSession();
           Usuario     usuario = (Usuario) session.getAttribute("Usuario");    
               
           
           String comentario="";
   
   //---  Obtenemos los parametros
           String estado = (request.getParameter("Estado")==null)?request.getParameter("estado"):request.getParameter("Estado");
           String fecha1 = (request.getParameter("FechaInicial")==null)?request.getParameter("FechaInicial2"):request.getParameter("FechaInicial");
           String fecha2 = (request.getParameter("FechaFinal")==null)?request.getParameter("FechaFinal2"):request.getParameter("FechaFinal");
           estado=estado.toUpperCase();
           String next;
           next = "/LogProceso/LogProcesos.jsp?";
           
    //--- Intentamos anular los procesos        
           String evento = request.getParameter("evento");
           if(evento!=null){
               String[] idProc = request.getParameterValues("proceso");
               if( idProc != null  )
                   for(int i=0;i<idProc.length;i++){
                       int id = Integer.parseInt( idProc[i] );
                       model.LogProcesosSvc.AnularProceso(id); 
                   }
               
           }
             
             
             
     //--- Realizamos la busqueda de los procesos  
           if(fecha1==null || fecha2==null  || fecha1.equals("null") || fecha2.equals("null")){
                fecha1 = com.tsp.util.Utility.getHoy("-");
                fecha2 = com.tsp.util.Utility.getHoy("-");
                next = "/LogProceso/LogProcesos.jsp?FechaInicial="+fecha1+"&FechaFinal="+fecha2;
           }               
           String p = (String) request.getSession().getAttribute("Perfil");
           
           if((p.equals("ADMIN"))||(p.equals("ADMIN2"))){
               
                comentario=model.LogProcesosSvc.searchLogProcesos(estado,fecha1,fecha2, "ADMINISTRADOR" );                
               
           }else{
                
                 comentario=model.LogProcesosSvc.searchLogProcesos(estado,fecha1,fecha2, usuario.getLogin() );   
           }
           
             
           
    //---  Guardamos los parametros       
           request.setAttribute("comentario"  , comentario);
           request.setAttribute("Estado"      , estado);
           request.setAttribute("FechaInicial", fecha1);
           request.setAttribute("FechaFinal"  , fecha2);
           
           
    //---  LLamamos la nueva pagina    
           
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response);             
       }
       catch(Exception e){
          throw new ServletException(e.getMessage());
       }

     }
}