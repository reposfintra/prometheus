/********************************************************************
 *      Nombre Clase.................   ReporteMovTraCerromatosoAction.java
 *      Descripci�n..................   Genera el reporte de los movimientos de 
 *                                      trafico del cliente Cerromatoso
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   06.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  EQUIPO12
 */
public class ReporteMovTraCerromatosoAction extends Action {
        
        /** Creates a new instance of ReporteMovTraCerromatosoAction */
        public ReporteMovTraCerromatosoAction() {
        }
        
        public void run() throws ServletException, InformationException {
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        //Pr�xima vista
        //////System.out.println("Estoy en el Action");
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/trafico/reportes/ReporteMovTraficoCerromatoso.jsp?msg=" +
                "El Reporte se ha generado exitosamente en ReporteMovTraficoCerromatoso_" +
                FechaFormated1 + ".xls";
        
        try{
            //ystem.out.println("Voy a extraer la lista de reportes");
            List repmovtrafico = model.reporteMovTraficoCerromatosoService.ReporteMovTrafCerromatoso();
            ReporteMovTraCerromatosoXLS hilo = new ReporteMovTraCerromatosoXLS();
            hilo.start(usuario.getLogin(), repmovtrafico);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        }
        
}
