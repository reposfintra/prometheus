/******************************************************************************
 * Nombre clase :                   PublicacionInsertAction.java              *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Insertar una Publicacion en la BD.        *
 * Autor :                          LREALES                                   *
 * Fecha :                          12 de abril de 2006, 04:25 PM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

public class PublicacionInsertAction extends Action {
    
    /** Creates a new instance of PublicacionInsertAction */
    public PublicacionInsertAction () {
    }

    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/publicacion/ingresarPublicacion/IngresarPublicacion2.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String creation_user = usuario.getLogin().toUpperCase();
        String base = usuario.getBase().toUpperCase();
        
        String msg = "";
        
        String id_opcion = (request.getParameter("enviar_opcion")!= null)?request.getParameter("enviar_opcion"):"";
        String texto = (request.getParameter("texto") != null)?request.getParameter("texto"):"";
        String tipo = (request.getParameter("enviar_tipo").toUpperCase()!= null)?request.getParameter("enviar_tipo"):"";
        String codigo_titulo = (request.getParameter("titulos").toUpperCase()!= null)?request.getParameter("titulos"):"";
        
        try{
            
            next = "/jsp/publicacion/ingresarPublicacion/IngresarPublicacion2.jsp";

            if ( ( distrito != "" ) && ( id_opcion != "" ) && ( texto != "" ) && ( tipo != "" ) && ( codigo_titulo != "" ) && ( creation_user != "" ) && ( base != "" ) ){
                
                model.publicacionService.insertPublicacion( distrito, id_opcion, texto, tipo, codigo_titulo, creation_user, base );
                next = next + "?msg=Se ha ingresado su publicacion exitosamente!!";                
                
            } else {
                
                next = next + "?msg=Por favor ingrese todos los datos obligatorios!!";

            }            
                            
        } catch ( Exception e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}