/********************************************************************
 *      Nombre Clase.................   TraficoAConfAction.java
 *      Descripci�n..................   Action para Actualizar las configuracion de las filas de control trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
import org.apache.log4j.*;

public class TraficoAConfAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of TraficoAConfAction */
    public TraficoAConfAction () {
    }
    
    public void run () throws ServletException, InformationException {
        ////System.out.println ("entro en run ");
        try{
            String filtroP = "" + request.getParameter("filtroP");
            String  []camposS   = request.getParameterValues ("camposS");
            String campoS = ""+request.getParameter ("cConfiguraciones");
            String user        = request.getParameter ("usuario");
            String clas        =""+ request.getParameter ("clas");
            String filtro        = ""+request.getParameter ("filtro");
            String var3     = ""+request.getParameter ("var3");
            String conf     = ""+request.getParameter ("conf");
            ////System.out.println ("configuracion: "+ conf);
            String nombre     = ""+request.getParameter ("nombre");
            ////System.out.println ("nombre: "+ nombre);
            ////System.out.println ("Filtro: "+ filtro);
            logger.info("FILTRO: " + filtro);
            String configuracion="";
            ////System.out.println ("id"+campoS);
            filtro = model.traficoService.decodificar (filtro);
            ConfiguracionUsuario confi = model.traficoService.listarConfiguracionPorId (campoS);
            
            String var1 = confi.getVar1 ();
            String var2= confi.getVar2 ();
            String linea=confi.getLinea ();
            configuracion=confi.getConfiguracion (); 
            
            configuracion = configuracion+",ult_observacion,color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias ";
            logger.info ("configuracion: "+configuracion);
            
            model.traficoService.generarVZonas (user);
            ////System.out.println ("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas ();
            String validacion=" zona='-1'";
            
            if ((zonas == null)||zonas.size ()<=0){}else{
                validacion = model.traficoService.validacion (zonas);
            }
            ////System.out.println ("\nValidacion final: "+ validacion);
            
            if(var3.equals ("null") ){
                var3=model.traficoService.obtVar3 ();
            }
            if (clas.equals ("null")||clas.equals ("")){
                clas="";
            }
            if (filtro.equals ("null")||filtro.equals ("")){
                filtro = "";
                filtro= "where "+validacion+filtro;
            }
            else{
                filtro=filtro;
            }
            model.traficoService.listarColTrafico (var3);
            Vector col = model.traficoService.obtColTrafico ();
            model.traficoService.listarColtrafico2 (col);
            
            String consulta      = model.traficoService.getConsulta (configuracion,filtro,clas);
            ////System.out.println ("consulta: "+ consulta);
            
            Vector colum= new Vector ();
            colum = model.traficoService.obtenerCampos (linea);
            
            ////System.out.println ("linea="+linea+" y obtiene colmunas"+colum.size ());
            
            model.traficoService.listarCamposTrafico (linea);
            
            model.traficoService.generarTabla (consulta, colum);
            ////System.out.println ("genera tabla ");
            filtro=model.traficoService.codificar (filtro);
            String next ="";
            
            next= "/jsp/trafico/controltrafico/vertrafico2.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion+"&clas="+clas+"&reload="+"&filtroP="+filtroP;
            System.out.println("NEXT------>"+ next);
            ////System.out.println ("next "+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
    
}
