/***************************************************************************
 * Nombre clase : ............... PlanillaSinCumplidoAction.java           *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el Reporte de           *
 *                                Planillas Sin Cumplido                   *
 * Autor :....................... Ing. David Velasquez Gonzalez   
 * Modified:......................Ing. Enrique De Lavalle
 * Fecha :........................23 de noviembre de 2006, 10:00 AM        *
 * Version :......................1.0                                      *
 * Copyright :....................Fintravalores S.A.                  *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

public class PlanillasinCumplidoAction extends Action{
    
    /** Creates a new instance of AnulacionPlanillaAction */
    public PlanillasinCumplidoAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String next = "";
            
            String opcion                 = (request.getParameter("Opcion")!=null) ? request.getParameter("Opcion") : "";
            String fechai                 = (request.getParameter("finicial")!=null) ? request.getParameter("finicial") : "";
            String fechaf                 = (request.getParameter("ffinal")!=null) ? request.getParameter("ffinal") : "";
            String cliente                = (request.getParameter("codcli")!=null) ? request.getParameter("codcli") : "";
                       
            // Manejo de Session
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario = user.getLogin();
            
            String Mensaje = "";
            
            if(cliente.equals("")){
                cliente="%";
            }
            
            
            if(opcion.equals("Generar")){
                model.planillaSinCumplidoService.listPlanillaSinCumplido(fechai, fechaf, cliente);
                next = "/jsp/masivo/reportes/PlanillasSinCumplido.jsp?fechai="+fechai+"&fechaf="+fechaf+"&cliente="+cliente;
            }
            
            if(opcion.equals("Excel")){
                ReportePlanillaSinCumplido hilo = new ReportePlanillaSinCumplido();
                //ReporteAnulacionPlanilla hilo = new ReporteAnulacionPlanilla();
                hilo.start(model.planillaSinCumplidoService.getListPA(),fechai,fechaf,cliente,user.getLogin(),usuario);                   
                Mensaje = "El proceso se realiz� exitosamente";
                next = "/jsp/masivo/reportes/ParametrosPlanillasSinCumplido.jsp?Mensaje="+Mensaje;
                
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en PlanillasinCumplidoAction .....\n"+e.getMessage());
        }
    }
    
}

