/*
 * ProcesoActualizarPlanillaFronteraAction.java
 *
 * Created on 21 de julio de 2006, 12:08 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
/**
 *
 * @author  dbastidas
 */
public class ProcesoActualizarPlanillaFronteraAction extends Action{

    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/pags_predo/actualizarPlanillas.jsp?msg=Proceso Iniciado";
        
        try{
             ActualizarPlanillasPlaneacion proc = new ActualizarPlanillasPlaneacion();
             proc.start(usuario.getDstrct(), usuario.getLogin());            
        }catch (Exception e){
            e.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
