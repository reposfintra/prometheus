/********************************************************************
 *      Nombre Clase.................   ReporteRetroactivosAction.java
 *      Descripci�n..................   Genera el reporte de retroactivos de carb�n
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   14.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteRetroactivosAction extends Action{
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteRetroactivosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter("FechaI");
        String fechaF = request.getParameter("FechaF");
        
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/masivo/reportes/ReporteRetroactivos.jsp?msg=" +
                "El Reporte se ha generado exitosamente en ReporteRetroactivos_" +
                FechaFormated1 + ".xls";
        
        try{
            model.repRetroactivosSvc.reporteRetroactivoStart(fechaI, fechaF);
            model.repRetroactivosSvc.reporteRetroactivoUnicosRepetidos(); 
            
            Vector h1 = new Vector();
            Vector h2 = new Vector();
            
            h1 = model.repRetroactivosSvc.Unicos();
            h2 = model.repRetroactivosSvc.Repetidos();
            
            Vector intermedios = model.repRetroactivosSvc.clasificadosIntermediarios();
            Vector directos = model.repRetroactivosSvc.clasificadosDirectos();
            ReporteRetroactivosTh hilo = new ReporteRetroactivosTh();
            
            hilo.start(fechaI, fechaF, usuario.getLogin(), h1, h2, intermedios, directos);
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
