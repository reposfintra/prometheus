/*
 * BuscarImgInfoClienteAction.java
 *
 * Created on 12 de junio de 2006, 03:32 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;



public class BuscarImgInfoClienteAction extends Action{
    
    /** Creates a new instance of BuscarImgInfoClienteAction */
    public BuscarImgInfoClienteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String planilla = request.getParameter("planilla");
        String remesa = request.getParameter("remesa");
        
        String next = "/jsp/sot/reports/verDocumentosDig.jsp";
        try {
             model.ImagenSvc.buscarDocumentosDigitalizados(planilla, remesa, usuario.getLogin());
             
        }catch (Exception e){
               e.printStackTrace();
               throw new ServletException(e.getMessage());
         } 
         this.dispatchRequest(next);
    }
    
}
