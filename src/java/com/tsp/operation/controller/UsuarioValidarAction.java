/*
 * UsuarioValidar.java
 *
 * Created on 22 de noviembre de 2004, 08:30 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.UsuarioService;
import com.tsp.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author  KREALES
 */
public class UsuarioValidarAction extends Action {
    
    public UsuarioService usuarioService;
    public CiudadService ciudadService;
    public CompaniaService ciaService;
    /** Creates a new instance of UsuarioValidar */
    public UsuarioValidarAction() {
    }
    //Alejandro 16-ene -2006
    public void run() throws javax.servlet.ServletException, InformationException {
        usuarioService = new UsuarioService();
        ciudadService = new CiudadService("fintra");
        ciaService = new CompaniaService();
        String login=  (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?request.getParameter("usuario"):request.getParameter("usuario1"); 
        String clave = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?request.getParameter("clave"):request.getParameter("clave1");
        String dstrct = (request.getParameter("dstrct") == null || request.getParameter("dstrct").equals("")) ? ((request.getParameter("dstrct1") == null || !request.getParameter("dstrct1").equals("")) ? request.getParameter("dstrct1") : "") : request.getParameter("dstrct");
        String proyecto = (request.getParameter("proy") == null || request.getParameter("proy").equals("")) ? (request.getParameter("proy1") == null || !request.getParameter("proy1").equals("") ? request.getParameter("proy1") : "") : request.getParameter("proy");
        String perfil = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?request.getParameter("perfil"):request.getParameter("perfil1");
        String cia = (request.getParameter("cia") != null) ? request.getParameter("cia") : "";
        String latitud= (request.getParameter("lat_x") != null) ? request.getParameter("lat_x") : "0";
        String longitud= (request.getParameter("lon_y") != null) ? request.getParameter("lon_y") : "0";
   
       
        String esconsorcio = request.getParameter("consorcio");//090710
        String login_into = request.getParameter("login_into");//090710
        
        boolean cambiandoClave = request.getParameter("cambiandoClave") != null;
      
        String loginNuevo= request.getParameter("loginNuevo") != null? request.getParameter("loginNuevo"):"NO";
        
        String paginaindex="index.jsp";//090710
            
        if (esconsorcio!=null && esconsorcio.equals("S")){//090710
            paginaindex="indexconsorcio.jsp";
        }


       if (login_into!=null && login_into.equals("Fenalco")){//090710
            paginaindex="index_fen.jsp";
            login= request.getParameter("usuario");
        }
       
       if (login_into!=null && login_into.equals("Fenalco_bol")){//090710
            paginaindex="index_fen_bol.jsp";
            login= request.getParameter("usuario");
        }
        
        String next = "/"+paginaindex+"?msg=<br>Ud. no se encuentra registrado en el Proyecto seleccionado!";//090710
        
        boolean usuarioOK = true;
        boolean debeRenovarClave = false;
        boolean debeActualizarFechas = false;
        boolean sesionSimultanea = false;
        try{
            try{ 
                if ( verificarClave(clave, login) == true ){
                    usuarioService.buscaUsuario(login, dstrct);
                    if(usuarioService.getUsuario()!=null){
                        Usuario usuario = usuarioService.getUsuario();
                        if ( usuario.getEstado().equals("I") ) {
                            next = "/"+paginaindex+"?tipo=login&msg=Usted ha sido inactivado, dirijase al administrador de usuarios para poder ingresar al sistema!";
                            usuarioOK = false;
                        }
                        else if ( request.getParameter("tipo") == null || request.getParameter("tipo").equals("login") ){
                            if (!usuarioService.verificarDistrito( login, dstrct ) ){
                                next = "/"+paginaindex+"?tipo=login&msg=COMPA�IA incorrecta... Verifique!";
                                usuarioOK = false;
                            }else{
                                if (!usuarioService.verificarPerfil( login, perfil) ){
                                    next = "/"+paginaindex+"?tipo=login&msg=PERFIL incorrecto... Verifique!";
                                    usuarioOK = false;
                                }else{
                                    if (!usuarioService.verificarProyecto(login, dstrct, proyecto)){
                                        next = "/"+paginaindex+"?tipo=login&msg=PROYECTO incorrecto... Verifique que Compa�ia y login correspondan al proyecto seleccionado!";
                                        usuarioOK = false;
                                    }
                                }
                            }
                        }
                        if ( usuarioOK ){
                            usuarioOK = false;
                            //usuario correcto - verificacion de fechas
                            String fechaDefault="0099-01-01 00:00:00";
                            if ( !cambiandoClave && usuario.isCambiarClaveLogin() ) {
                                debeRenovarClave = true;
                            }
                            else if ( request.getParameter("tipo") == null || request.getParameter("tipo").equals("login")){
                                String fechaActual = Util.getFechaActual_String(4);
                                String fchr = Util.getFechaFormatoyyyyMMdd(usuario.getUlt_fecha_renov_clave());
                                String fchi = Util.getFechaFormatoyyyyMMdd(usuario.getFecha_ini_act());
                                int diaVig = usuario.getNum_dias_vigencia();
                                int diaRen = usuario.getNum_dias_para_renovar_clave();
                                //verificacion de valores nulos en BD
                                ///RENOVACION CLAVE
                                if(  usuario.getNum_dias_para_renovar_clave() == 0 ||
                                (usuario.getUlt_fecha_renov_clave().equals(fechaDefault) || usuario.getUlt_fecha_renov_clave().equals("")) ||
                                (usuario.getFecha_ini_act().equals(fechaDefault) || usuario.getUlt_fecha_renov_clave().equals("")) ){
                                    debeActualizarFechas = true;
                                    debeRenovarClave = true;
                                }
                                ///Verificacion de vencimiento de fechas
                                //RENOVACION CLAVE
                                else if( (Integer.parseInt(Util.getFecha(fchr,diaRen)) <= Integer.parseInt(Util.getFechaFormatoyyyyMMdd(fechaActual)) )){
                                    debeRenovarClave = true;
                                    //actualizo fecha renovacion
                                }
                                //VIGENCIA
                                else if( Integer.parseInt(fchi) > Integer.parseInt(Util.getFechaFormatoyyyyMMdd(fechaActual)) ){
                                    next = "/"+paginaindex+"?msg=<br>No se ha habilitado su cuenta de usuario, su Fecha de Inicio de Actividad es<br>  " + usuario.getFecha_ini_act().substring(0,16);
                                }
                                //Verificacion de sesion activa
                                else if (request.getParameter("opcion") != null) {
                                    //request.getSession().invalidate(); 
                                    Usuario use = Util.getUsuarioSession(login.toUpperCase());
                                    if(use != null && !use.getEmpresa().equalsIgnoreCase(dstrct)) {
                                        sesionSimultanea = true;
                                    }
                                }
                                else {
                                    usuarioOK = true;
                                }
                            }else {//tipo = renovarClave
                                //actualizar clave y fecha de ultima renovacion clave...ENTRA
                                String nclave = Util.encript(application.getInitParameter("deskey"), request.getParameter("nclave"));
                                usuarioService.actualizarClave(login, nclave);
                                usuarioOK = true;
                            }
                            if (usuarioOK){
                                if (loginNuevo.equals("NO")) {
                                    usuarioService.actualizarFechaUltimoIngreso(usuario.getLogin(), usuario.getBd());
                                    /*Distrito del usuario de la session*/
                                    usuario.setDstrct("FINV");
                                    usuario.setCia("FINV");
                                    usuario.setEmpresa(dstrct);
                                    usuario.setPassword(clave);
                                    usuario.setPerfil(perfil);
                                    HttpSession session = request.getSession();                                
                                    session.setAttribute("Usuario", usuario);
                                    session.setAttribute("Distrito", "FINV");
                                    session.setAttribute("Perfil", perfil);
                                    session.setAttribute("Agencia", ciudadService.obtenerNombreCiudad(usuario.getId_agencia()) );
                                    session.setAttribute("Login", usuario.getLogin());
                                    ciaService.buscarCia(dstrct);
                                    session.setAttribute("Moneda",  ciaService.getCompania().getMoneda()); 
                                    Util.setUsuarioSession(usuario.getLogin(), usuario);
                                    session.setAttribute("login_into",login_into);       
                                    
                                    //Guardamos la ip del cliente por seguridad 
                                    String ipAddress = request.getRemoteAddr();
                                    usuarioService.saveRemoteAddress(ipAddress, login,latitud,longitud);

                                    next = "/controller?estado=Menu&accion=CargaxPerfil&cmd=cgamenu";   //sescalante 01.02.06
                                }
                            }else {
                                if (debeActualizarFechas){
                                    //actualizar valores inconsistentes
                                    next = "/"+paginaindex+"?tipo=renovarClave&msg=<br>Debe realizar el proceso de Renovacion de Clave, la ultima fecha de renovaci�n es " + usuario.getUlt_fecha_renov_clave().substring(0,16);
                                    usuarioService.actualizarFechasInc(login);
                                }else if (debeRenovarClave){
                                    next = "/"+paginaindex+"?tipo=renovarClave&msg=<br>Debe realizar el proceso de Renovacion de Clave, la ultima fecha de renovaci�n es " + usuario.getUlt_fecha_renov_clave().substring(0,16);
                                }
                            }
                        }
                    }
                }
                else {
                    next = "/"+paginaindex+"?msg=<br>LOGIN o PASSWORD incorrecto... Verifique!";
                }
            }catch ( EncriptionException eex){
                eex.printStackTrace();
            }
            //System.outil.println("NEXT " + next);
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
       
        //validamos si hay que renovar clave para el inicio nuevo. 
        if (loginNuevo.equals("SI")) {
            String respuestaJson = "{\"respuesta\":\"NO\"}";
            if (sesionSimultanea) {
                respuestaJson = "{\"respuesta\":\"DUO\"}";
            } else if (debeRenovarClave) {
                respuestaJson = "{\"respuesta\":\"SI\",\"dstrct\":\""+dstrct+"\",\"proy\":\""+proyecto+"\",\"cia\":\""+cia+"\"}";
            }
            try {
                this.printlnResponse(respuestaJson, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                Logger.getLogger(UsuarioValidarAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            this.dispatchRequest(next);
        }
    }
   
    boolean verificarClave( String clave, String login ) throws SQLException, EncriptionException{
        String claveEncr = null;
        try {
            Util u = new Util();
            claveEncr = Util.encript(application.getInitParameter("deskey"), clave);
            ////System.out.println("RESULTADO " + model.usuarioService.validarClave(login,claveEncr));
            return usuarioService.validarClave(login,claveEncr);
        }catch (Exception E){
            throw new EncriptionException("Model.java: Error Metodo validarClave(). " +
            "(Codigo: Encriptacion de clave). Mensaje: " +
            E.getMessage());
        }
    }
    
     /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    public void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
   
}