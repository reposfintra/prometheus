 /********************************************************************
 *      Nombre Clase.................   ConsultarDemoraPlanilla.java    
 *      Descripci�n..................   Verifica que el numero de planilla tenga asignado demora(s),
 *                                      y redirecciona a la p�gina del listado de demoras de planilla
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;


public class ConsultarDemoraPlanillaAction extends Action{
    
    /** Creates a new instance of ConsultarDemoraPlanillaAction */
    public ConsultarDemoraPlanillaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String numpla = request.getParameter("numpla");
        numpla = numpla.toUpperCase();
        HttpSession session = request.getSession();
        String next = "/jsp/trafico/demora/consultarDemoraPlanillaListar.jsp";
        
        try{
            Demora dem = model.demorasSvc.obtenerDemora(numpla);
            
            if( dem!=null ){    
                session.removeAttribute("numplaDemoraListar");
                session.setAttribute("numplaDemoraListar", numpla);
            } else {
                next = "/jsp/trafico/demora/consultarDemoraPlanilla.jsp?msg=" +
                    "N�mero de planilla no se encuentra en el archivo de demoras.";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
