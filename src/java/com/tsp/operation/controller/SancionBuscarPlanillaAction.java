/*******************************************************************
 * Nombre clase: SancionBuscarPlanillaAction.java
 * Descripci�n: Accion para buscar unas sanciones dada una planilla.
 * Autor: Jose de la rosa
 * Fecha: 24 de septiembre de 2005, 09:16 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class SancionBuscarPlanillaAction extends Action{
    
    /** Creates a new instance of SancionBuscarPlanillaAction */
    public SancionBuscarPlanillaAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String numpla = request.getParameter ("numpla").toUpperCase ();
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String next = "";
        String tipo_sancion = "";
        try{
            if( model.demorasSvc.existePlanilla (numpla) ){
                session.setAttribute ("tipo_sancion", tipo_sancion);
                session.setAttribute ("numpla", numpla);
                next = "/jsp/cumplidos/sancion/SancionAprobadosLista.jsp";
            }
            else
                next = "/jsp/cumplidos/sancion/buscarSancionPlanilla.jsp?msg=N�mero de planilla no existe.";
            next = Util.LLamarVentana (next, "Listar Sanci�nes");
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
