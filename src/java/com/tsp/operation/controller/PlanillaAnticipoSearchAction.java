/*
 * PlanillaAnticipoSearchAction.java
 *
 * Created on 28 de diciembre de 2004, 09:04 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class PlanillaAnticipoSearchAction extends Action{
    
    /** Creates a new instance of PlanillaAnticipoSearchAction */
    public PlanillaAnticipoSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        Usuario     usuario = (Usuario)session.getAttribute("Usuario");
        
        
        String next="/reanticipo/incioReAnticipo.jsp";
        String planilla= request.getParameter("planilla").toUpperCase();
        model.buService.setListBUsuariosOtros(new TreeMap());
        
        try{
            String proveedorAnt="";
            model.ciaService.buscarCia(usuario.getDstrct());
            if(model.ciaService.getCompania()!=null){
                proveedorAnt = model.ciaService.getCompania().getnit();
            }
            
            request.setAttribute("planilla",null);
            model.movplaService.buscarAnticipoSinCheque(planilla,proveedorAnt);
            Vector anticipos = model.movplaService.getLista();
            if(anticipos.size()>0){
                Movpla movpla = (Movpla) anticipos.get(0);
                String mensaje ="Existe un anticipos por valor de "+com.tsp.util.Util.customFormat(movpla.getVlr())+" "+movpla.getCurrency()+" asociado al proveedor "+
                movpla.getProveedor_anticipo()+" y debe ser impreso o en caso contrario anulado.";
                next="/reanticipo/incioReAnticipo.jsp?mensaje="+mensaje;
            }else{
                if(!model.planillaService.tieneFactura(planilla)){
                    model.planillaService.buscarPlanillaAnticipo(planilla);
                    model.buService.buscarBancos(usuario.getLogin());
                    
                    
                    if(model.planillaService.getPlanilla()!=null){
                        
                        Planilla pla = model.planillaService.getPlanilla();
                        
                        model.anticiposService.searchAnticiposProveedor(usuario);
                        //model.planillaService.searchSaldo(planilla);
                        request.setAttribute("planilla",pla);
                        
                        if(request.getParameter("agency")!=null){
                            model.usuarioService.buscarUsuariosAgencia(request.getParameter("agency"));
                        }
                        if(request.getParameter("usuario")!=null && request.getParameter("sw")!=null){
                            model.buService.buscarBancosOtros(request.getParameter("usuario").toUpperCase());
                        }
                        
                        //LIQUIDACION DE LA PLANILLA
                        try{
                            
                            LiquidarPlanilla  x = new LiquidarPlanilla();
                            x.Liquidar("FINV", planilla, "MPL");
                            
                            Vector valores = new Vector();
                            List lista  =  x.getMovimientos();
                            for(int i=0;i<lista.size();i++){
                                ItemLiquidacion item = (ItemLiquidacion)lista.get(i);
                                valores.add(item);
                                
                            }
                            model.planillaService.setInformes(valores);
                            //model.planillaService.getInformes();
                            pla.setSaldo(Float.parseFloat(""+ x.getSaldo()));
                            pla.setMaxReanticipo(x.getVlrMaximoReanticipos());
                            pla.setVlrpla((float) x.getValorTotalPlanilla());
                            pla.setVlrpla2((float) x.getValorMaxAnticipo() );
                            
                        }catch(Exception e){
                            throw new ServletException(e.getMessage());
                        }
                        
                        
                    }
                    else{
                        next = next+"?mensaje=La planilla no existe o esta anulada";
                    }
                }else{
                    next="/reanticipo/incioReAnticipo.jsp?mensaje=La planilla "+planilla+" ya tiene generada OP, no puede generar reanticipos.";
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
