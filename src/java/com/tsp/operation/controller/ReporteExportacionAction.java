/******************************************************************************
 *      Nombre Clase.................   ReporteExportacionAction.java
 *      Descripci�n..................   Genera el reporte de Importaci�n-Exportaci�n
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   26.08.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;

public class ReporteExportacionAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of DocumentoInsertAction */
    public ReporteExportacionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String pag  = "/jsp/masivo/reportes/ReporteExportacionInfo.jsp";
        String next = pag;
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        
        String cliente = request.getParameter("cliente");
        String doc = request.getParameter("doc")!=null ? request.getParameter("doc") : "" ;
        
        logger.info("CLIENTE: " + cliente);
        
        try{
            
            if( doc.length()==0 ){
                model.impoExpoService.obtenerRegistroCliente(cliente, dstrct);
                logger.info(" REPORTE EXPORTACION: " + model.impoExpoService.getVector().size());     
                request.setAttribute("info", model.impoExpoService.getVector());
            } else {
                if( model.impoExpoService.existeDocExpo(dstrct, doc) ){
                    String[] docs = new String[1];
                    docs[0] = dstrct + "|EXP|" +  doc;
                    /**
                     * Generamos el reporte
                     */
                    ReporteExportacionMabeTh hilo = new ReporteExportacionMabeTh();
                    hilo.start(usuario.getLogin(), docs);
                    next = "/jsp/masivo/reportes/ReporteExportacion.jsp?msg=" +
                            "Se ha iniciado la generaci�n del documento de exportaci�n digitado.<br> Puede consultar el log de proceso para ver su estado.";
                } else {
                    next = "/jsp/masivo/reportes/ReporteExportacion.jsp?msg=" +
                            "El documento de exportaci�n digitado no existe.";
                    
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
