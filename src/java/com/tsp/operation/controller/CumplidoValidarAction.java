/*
 * CumplidoValidarAction.java
 *
 * Created on 20 de diciembre de 2004, 03:47 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class CumplidoValidarAction extends Action{
    
    /** Creates a new instance of CumplidoValidarAction */
    public CumplidoValidarAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next     ="/jsp/cumplidos/cumplidos/CumplidoValido.jsp";
        String no_rem   = request.getParameter("remesa").toUpperCase();
        //System.out.println("la remesa es " + no_rem);
        String no_pla   = request.getParameter("planilla").toUpperCase();
        //System.out.println("la planilla es" +no_pla);
        String feccum   = request.getParameter("feccum");
        //System.out.println("el feccum es" + feccum);
        String fecdes ="";
        Calendar fecdesp = Calendar.getInstance();
        int sw=0;
        float pesoreal=0;
        float cantidad =0;
        //if(request.getParameter("cantidad")!=null){
           // if(!request.getParameter("cantidad").equals("")){
             //System.out.println("La cantidad no es vacia");
                cantidad=Float.parseFloat(request.getParameter("cantidad"));
            //}
        //}
        Planilla pla = new Planilla();
        Remesa rem = new Remesa();
        request.setAttribute("fecha","#99cc99");
        request.setAttribute("cantidad","#99cc99");
        
        try{
            
            model.remesaService.buscaRemesa(no_rem);
            if(model.remesaService.getRemesa()!=null){
                rem=model.remesaService.getRemesa();
                String nombreR="";
                String remitentes[]= rem.getRemitente().split(",");
                for(int i=0; i<remitentes.length; i++){
                    nombreR=model.remidestService.getNombre(remitentes[i])+",";
                }
                String nombreD="";
                String destinatarios[]= rem.getDestinatario().split(",");
                for(int i=0; i<destinatarios.length; i++){
                    nombreD=model.remidestService.getNombre(destinatarios[i])+",";
                }
                
                rem.setRemitente(nombreR);
                rem.setDestinatario(nombreD);
                request.setAttribute("remesa",rem);
                
                List planillas= model.remesaService.buscarPlanillas(no_rem);
                List lplanillas= new LinkedList();
                Iterator it=planillas.iterator();
                while (it.hasNext()){
                    pla = (Planilla) it.next();
                    String nomruta= model.planillaService.getRutas(pla.getNumpla());
                    pla.setCodruta(pla.getRuta_pla());
                    pla.setRuta_pla(nomruta);
                    lplanillas.add(pla);
                }
                request.setAttribute("planillas",lplanillas);
            }
            
            //SE BUSCA LA PLANILLA
            model.planillaService.bucarColpapel(no_pla);
            pla.setNumpla(no_pla);
            if(model.planillaService.getPlanilla()!=null){
                pla = model.planillaService.getPlanilla();
                pesoreal = pla.getPesoreal();
                fecdes = pla.getFecdsp();
                
                //CREO UN CALENDARIO CON LA FECHA DEL DESPACHO
                int yeard=Integer.parseInt(fecdes.substring(0,4));
                int monthd=Integer.parseInt(fecdes.substring(5,7))-1;
                int dated= Integer.parseInt(fecdes.substring(8,10));
                fecdesp.set(yeard,monthd,dated);
                
                String nomruta= model.planillaService.getRutas(pla.getNumpla());
                pla.setCodruta(pla.getRuta_pla());
                pla.setRuta_pla(nomruta);
                request.setAttribute("planilla",pla);
                
                model.movplaService.buscaMovpla(no_pla,"01");
                if(model.movplaService.getMovPla()!=null){
                    Movpla movpla= model.movplaService.getMovPla();
                    request.setAttribute("movpla",movpla);
                }
                List remesas = model.planillaService.buscarRemesas(no_pla);
                request.setAttribute("remesas" , remesas);
            }
            
            //SE VALIDA QUE LA FECHA SEA CORRECTA
            Calendar fecha = Calendar.getInstance();
            int year=Integer.parseInt(request.getParameter("feccum").substring(0,4));
            int month=Integer.parseInt(request.getParameter("feccum").substring(5,7))-1;
            int date= Integer.parseInt(request.getParameter("feccum").substring(8,10));
            fecha.set(year,month,date);
            
            Calendar fecha2= Calendar.getInstance();
            if(fecha.after(fecha2)){
                request.setAttribute("fecha","#cc0000");
                sw=1;
            }
            else if (fecha.before(fecdesp)) {
                
                request.setAttribute("fecha","#cc0000");
                sw=1;
              
            }
            else{
                
                Calendar c= Calendar.getInstance();
                Calendar d= Calendar.getInstance();
                c.add(c.DATE,-30);
                
                if(fecha.before(c)){
                    request.setAttribute("fecha","#cc0000");
                    sw=1;
                }
                
            }
            if(!model.planillaService.validarCantidad(pesoreal,cantidad)){
                request.setAttribute("cantidad","#cc0000");
                sw=1;
            }
            if(sw==1)
                next = "/cumplidos/CumplidoError.jsp";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
