/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AdministracionEtesDAO;
import com.tsp.operation.model.DAOS.fintralogisticaAdministrativoDAO;
import com.tsp.operation.model.DAOS.impl.AdministracionEtesImpl;
import com.tsp.operation.model.DAOS.impl.fintralogisticaAdministrativoImpl;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.hslf.model.Sheet;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author mariana
 */
public class fintralogisticaAdministrativoAction extends Action {

    private final int MOSTRAR_REPORTE = 1;
    private final int CARGAR_TRANPORTADORAS = 2;
    private final int EXPORTAR_EXCEL_CONTROLDOC = 3;
    private final int LISTAR_IA_PENDIENTES = 4;

    private fintralogisticaAdministrativoDAO dao;
    private AdministracionEtesDAO daoTrans;

    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle letras, header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, letra2, letra3, letra4, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;
    Usuario usuario = null;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new fintralogisticaAdministrativoImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case MOSTRAR_REPORTE:
                    cargarReporte();
                    break;
                case CARGAR_TRANPORTADORAS:
                    cargarTransportadoras();
                    break;
                case EXPORTAR_EXCEL_CONTROLDOC:
                    exportarInfo();
                    break;
                case LISTAR_IA_PENDIENTES:
                    generarIAPendientes();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarReporte() {
        try {
            Gson gson = new Gson();
            String ventas = request.getParameter("ventas");
            String planilla = request.getParameter("planilla");
            String corrida = request.getParameter("corrida");
            String fecha_corrida = request.getParameter("fecha");
            String empresa = request.getParameter("empresa");
            String ingreso = request.getParameter("ingreso");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarReporte(ventas, planilla, corrida, fecha_corrida, empresa, ingreso)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(fintralogisticaAdministrativoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarTransportadoras() {
        try {
            daoTrans = new AdministracionEtesImpl(this.usuario.getBd());
            this.printlnResponse(daoTrans.cargarTransportadoras(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(fintralogisticaAdministrativoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void generarIAPendientes() {
        try {
            Gson gson = new Gson();
            String empresa = request.getParameter("empresa");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.generarIAPendientes(empresa, usuario)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(fintralogisticaAdministrativoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void exportarInfo() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("ControlDocumentos_", "Reporte");
            String[] cabecera = {"Empresa", "Planilla", "Venta","Fecha Venta", "Reanticipo", "Fecha Corrida", "Corrida", "Documento", "Fecha Referencia", "Tipo documento", "# Ingreso"};

            short[] dimensiones = new short[]{
                7000, 3000, 3000, 5000, 5000, 7000, 5000, 5000, 3000, 5000,5000
            };
            this.generaTitulos(cabecera, dimensiones);
            
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                if (objects.get("venta").getAsString().equals("No")) {
                    letras = letra2;
                } else {
                    if ((objects.get("numingreso_ultimoingreso").getAsString().equals("")) && (!objects.get("documento").getAsString().equals(""))) {
                        letras = letra3;
                    } else {
                        if ((objects.get("numingreso_ultimoingreso").getAsString().equals("")) && (objects.get("documento").getAsString().equals(""))) {
                            letras = letra4;
                        } else {
                            letras = letra;
                        }

                    }
                }
                xls.adicionarCelda(fila, col++, objects.get("empresa").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("venta").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("fecha_venta").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("reanticipo").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("fecha_corrida").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("cxc_corrida").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("documento").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("referencia1").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("tipdoc_ultimoingreso").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("numingreso_ultimoingreso").getAsString(), letras);

            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ControlDocumentos_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {
        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            // xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE, 1);
            letra2 = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, HSSFColor.CORAL.index, xls.NONE, 1);
            letra3 = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, HSSFColor.YELLOW.index, xls.NONE, 1);
            letra4 = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, HSSFColor.LIGHT_ORANGE.index, xls.NONE, 1);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }

}
