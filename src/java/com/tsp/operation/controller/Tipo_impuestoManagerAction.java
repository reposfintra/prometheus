/********************************************************************
 *      Nombre Clase.................   Tipo_impuestoManagerAction.java
 *      Descripci�n..................   Administrador de los eventos del programa Tipo de Impuesto
 *      Autor........................   Ing. Juan Manuel Escand�n
 *      Fecha........................   30 de septiembre de 2005, 09:35 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import org.apache.log4j.*;
/**
 *
 * @author  JuanM
 */
public class Tipo_impuestoManagerAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private static String tipo;
    private static String fecvig;
    private static String cod;
    private static String age;
    private static String conc;
    /** Creates a new instance of Tipo_impuestoManagerAction */
    public Tipo_impuestoManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            model.ciudadService.ListaAgencias();
            //model.TimpuestoSvc.ReiniciarLista();
            String agcs = model.agenciaService.Agencias();
            request.setAttribute("agenciasLst", agcs);
            boolean listar = false; /* Indica si debe ir a la vista de listar */
            boolean update = false; /* Indica si debe ir a la vista de actualizar */
            boolean anular = false; /* Indica si se debe import a la vista de anular */
            boolean updated = false; /* Indica si se actualizo un registro */
            
            String signo                  = request.getParameter("signo") != null ? request.getParameter("signo") : "1";
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String Tipo                   = (request.getParameter("Tipo")!=null)?request.getParameter("Tipo"):"";
            String tipo_impuesto          = (request.getParameter("impuesto")!=null)?request.getParameter("impuesto"):"";
            String codigo_impuesto        = (request.getParameter("codimpuesto")!=null)?request.getParameter("codimpuesto"):"";
            String concepto               = (request.getParameter("concepto")!=null)?request.getParameter("concepto"):"";
            String descripcion            = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):"";
            String fecha_vigencia         = (request.getParameter("fechavigencia")!=null)?request.getParameter("fechavigencia"):"";
            String porcentaje1            = (request.getParameter("porcentaje1")!=null)?request.getParameter("porcentaje1"):"";
            String porcentaje2            = (request.getParameter("porcentaje2")!=null)?request.getParameter("porcentaje2"):"";
            String cod_cuenta_contable    = (request.getParameter("cod_cuenta_contable")!=null)?request.getParameter("cod_cuenta_contable"):"";
            String agencia                = (request.getParameter("agencia")!=null && (tipo_impuesto.equals("RICA") || tipo_impuesto.equals("RICACL")) )?request.getParameter("agencia"):"";           
            String agenciah               = (request.getParameter("agenciah")!=null)?request.getParameter("agenciah"):"";                        
            String conceptoh              = (request.getParameter("conceptoh")!=null)?request.getParameter("conceptoh"):"";
            
            String fconcepto              = (request.getParameter("fconcepto")!=null)?request.getParameter("fconcepto"):"";
            fconcepto                     = (fconcepto.equals(""))?"%":fconcepto;
            
            String filtroagencias         = (request.getParameter("filtroagencias")!=null)?request.getParameter("filtroagencias"):"";
            
            String cadena                 = (request.getParameter("cadena")!=null)?request.getParameter("cadena"):"";
            
            String original               = (request.getParameter("original2")!=null)?request.getParameter("original2"):"";
            
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String dstrct                 = (String)Session.getAttribute("Distrito");
            
            
            String TipoI		  = (request.getParameter("TipoI")!=null)?request.getParameter("TipoI"):"";
            
            String []LOV                  = request.getParameterValues("LOV");
            String Mensaje                = "";
            
            logger.info("? Opcion: " + Opcion);
            ////System.out.println(".................. OPCION: " + Opcion + " - TIPO IMPUESTO: " + TipoI);             
            ////System.out.println(".................. FECHA_VIGENCIA: " + fecha_vigencia);
            if (Opcion.equals("Guardar") && Tipo.equals("IVA")){
                if( !model.TimpuestoSvc.EXISTEIVA(dstrct, codigo_impuesto, fecha_vigencia, concepto) ){
                    model.TimpuestoSvc.INSERTIVA(dstrct, codigo_impuesto, tipo_impuesto, concepto, fecha_vigencia, Double.parseDouble(porcentaje1), Double.parseDouble(porcentaje2), cod_cuenta_contable, user.getLogin(), descripcion, signo);
                    Mensaje = "El Registro ha sido agregado";
                } else {
                    Mensaje = "El Registro ya existe";
                }
            } else if (Opcion.equals("Guardar") && ( Tipo.equals("RICA") || Tipo.equals("RICACL") ) ){
                if( !model.TimpuestoSvc.EXISTERICA(dstrct, codigo_impuesto, fecha_vigencia, agencia, concepto, Tipo) ){
                    model.TimpuestoSvc.INSERTRICA(dstrct, codigo_impuesto, tipo_impuesto, concepto, descripcion, fecha_vigencia, Double.parseDouble(porcentaje1), cod_cuenta_contable, agencia, user.getLogin(), signo);
                    Mensaje = "El Registro ha sido agregado";
                } else {
                    Mensaje = "El Registro ya existe";
                }                
            } else if (Opcion.equals("Guardar") && ( Tipo.equals("RFTE") || Tipo.equals("RFTECL") ) ){
                if( !model.TimpuestoSvc.EXISTERFTE(dstrct, codigo_impuesto, fecha_vigencia, concepto, Tipo) ){
                    model.TimpuestoSvc.INSERTRFTE(dstrct, codigo_impuesto, tipo_impuesto, concepto, descripcion, fecha_vigencia, Double.parseDouble(porcentaje1), cod_cuenta_contable, user.getLogin(), signo);
                    Mensaje = "El Registro ha sido agregado";
                } else {
                    Mensaje = "El Registro ya existe";
                }                
            } else if (Opcion.equals("Guardar") && Tipo.equals("RIVA")){//Tito Andr�s
                if( !model.TimpuestoSvc.EXISTERIVA(dstrct, codigo_impuesto, fecha_vigencia) ){
                    model.TimpuestoSvc.INSERTRIVA(dstrct, codigo_impuesto, tipo_impuesto, concepto, fecha_vigencia, Double.parseDouble(porcentaje1), cod_cuenta_contable, user.getLogin(), signo);
                    Mensaje = "El Registro ha sido agregado";
                } else {
                    Mensaje = "El Registro ya existe";
                }                
            }  else if (Opcion.equals("Listado")){
                logger.info("... ingrese al listado");
                listar = true;
                model.TimpuestoSvc.LIST(TipoI,filtroagencias,fconcepto);
                List list = model.TimpuestoSvc.getList();
                if( list.size() == 0 || list == null )
                    Mensaje = "No hay registros de ese tipo de impuesto";
                
                //AMATURANA 13.03.2007
                model.TimpuestoSvc.setTipoI(TipoI);
                model.TimpuestoSvc.setFagencias(filtroagencias);
                model.TimpuestoSvc.setFconcepto(fconcepto);
                logger.info("? TipoI: " + model.TimpuestoSvc.getTipoI());
                logger.info("? fagencias: " + model.TimpuestoSvc.getFagencias());
                logger.info("? fconcepto: " + model.TimpuestoSvc.getFconcepto());
                //Mensaje = "El Registro ha sido agregado";
            } else if (Opcion.equals("Nuevo")){
                model.TimpuestoSvc.ReiniciarDato();
            } else if (Opcion.equals("Ocultar Lista")){
                model.TimpuestoSvc.ReiniciarLista();
            } else if (Opcion.equals("Anular")){
                anular = true;
                
                Conversion2(original);
                                
                if( tipo.equals("RICA") || tipo.equals("RICACL") )
                    model.TimpuestoSvc.UPDATEESTADORICA("A", dstrct, cod, fecvig, age, conc, user.getLogin());
                else if( tipo.equals("IVA") )
                    model.TimpuestoSvc.UPDATEESTADOIVA("A", dstrct, cod, fecvig, user.getLogin(), concepto);
                else if( tipo.equals("RFTE") || tipo.equals("RFTECL") )
                    model.TimpuestoSvc.UPDATEESTADORFTE("A", dstrct, cod, fecvig, conc, user.getLogin());
                else if( tipo.equals("RIVA") )//Modific. Tito Andr�s Maturana
                    model.TimpuestoSvc.UPDATEESTADORIVA("A", dstrct, cod, fecvig, user.getLogin());
                                
                Mensaje = "El Registro ha sido anulado";
                //Tito Andr�s Maturana 8.03.2006
                model.TimpuestoSvc.ReiniciarLista();
                model.TimpuestoSvc.LIST(TipoI,filtroagencias,fconcepto);
                List list = model.TimpuestoSvc.getList();
            } else if (Opcion.equals("Activar")){
                for(int i=0;i<LOV.length;i++){                    
                    this.Conversion(LOV[i]);
                    if( tipo.equals("RICA") )
                        model.TimpuestoSvc.UPDATEESTADORICA("", dstrct, cod, fecvig, age, conc, user.getLogin());
                    if( tipo.equals("IVA") )
                        model.TimpuestoSvc.UPDATEESTADOIVA("", dstrct, cod, fecvig, user.getLogin(), concepto);
                    if( tipo.equals("RFTE") )
                        model.TimpuestoSvc.UPDATEESTADORFTE("", dstrct, cod, fecvig, conc, user.getLogin());
                    if( tipo.equals("RIVA") )//Modific. Tito Andr�s Maturana
                        model.TimpuestoSvc.UPDATEESTADORIVA("", dstrct, cod, fecvig, user.getLogin());
                }
                Mensaje = "El Registro ha sido activado";
                //Tito Andr�s Maturana 8.03.2006
                model.TimpuestoSvc.ReiniciarLista();
                model.TimpuestoSvc.LIST(TipoI,filtroagencias,fconcepto);
                List list = model.TimpuestoSvc.getList();
            } else if (Opcion.equals("Eliminar")){
                for(int i=0;i<LOV.length;i++){                    
                    this.Conversion(LOV[i]);
                    if( tipo.equals("RICA") )
                        model.TimpuestoSvc.DELETERICA(dstrct, cod, fecvig, age, conc);
                    if( tipo.equals("IVA") )
                         model.TimpuestoSvc.DELETEIVA(dstrct, cod, fecvig );
                    if( tipo.equals("RFTE") )
                        model.TimpuestoSvc.DELETERFTE(dstrct, cod, fecvig, conc);
                    if( tipo.equals("RIVA") )//Modific. Tito Andr�s Maturana
                         model.TimpuestoSvc.DELETERIVA(dstrct, cod, fecvig );
                }
                Mensaje = "El Registro ha sido eliminado";
                //Tito Andr�s Maturana 8.03.2006
                model.TimpuestoSvc.ReiniciarLista();
                model.TimpuestoSvc.LIST(TipoI,filtroagencias,fconcepto);
                List list = model.TimpuestoSvc.getList();
            } else if(Opcion.equals("Seleccionar")){
                update = true;
                this.Conversion2(original);
                model.TimpuestoSvc.SEARCH_TI(dstrct,cod,fecvig,age, conc);
                //logger.info("... obtener impuesto. " + model.TimpuestoSvc.getDato().getInd_signo());
            } else if(Opcion.equals("Seleccionar2")){                
                model.TimpuestoSvc.SEARCH(dstrct,tipo_impuesto,codigo_impuesto);
                if(model.TimpuestoSvc.getDato()== null)
                    Mensaje = "El impuesto " + codigo_impuesto + " no existe";
                
            } else if (Opcion.equals("Modificar") && tipo_impuesto.equals("IVA")){
                update = true;
                Conversion2(original);
                /*//System.out.println("....... cod : " + cod);
                //System.out.println("....... codigo_impuesto: " + codigo_impuesto);
                //System.out.println("....... fecvig: " + fecvig);
                //System.out.println("....... fecha_vigencia: " + fecha_vigencia);
                //System.out.println("....... condicion: " + ( cod.compareTo(codigo_impuesto)!=0 || fecvig.compareTo(fecha_vigencia)!=0 ));
                */
                if ( cod.compareTo(codigo_impuesto)!=0 || fecvig.compareTo(fecha_vigencia)!=0 || conc.compareTo(concepto)!=0 ) {
                    ////System.out.println(".......... ENTRAR A EXISTE: " + ( !model.TimpuestoSvc.EXISTEIVA(dstrct, codigo_impuesto, fecha_vigencia)));
                    if( !model.TimpuestoSvc.EXISTEIVA(dstrct, codigo_impuesto, fecha_vigencia, concepto) ){
                        logger.info("... actualizar IVA 1");
                        model.TimpuestoSvc.UPDATEIVA(tipo_impuesto, codigo_impuesto, concepto, fecha_vigencia, porcentaje1, porcentaje2, cod_cuenta_contable, descripcion, cod, fecvig, conc, user.getLogin(), signo);
                        Mensaje = "El Registro ha sido modificado";
                        updated = true;
                        model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia, concepto, agencia); 
                    } else {                        
                        Mensaje = "El Registro ya existe";
                    }
                } else {
                    logger.info("... actualizar IVA 2");
                    model.TimpuestoSvc.UPDATEIVA(tipo_impuesto, codigo_impuesto, concepto, fecha_vigencia, porcentaje1, porcentaje2, cod_cuenta_contable, descripcion, cod, fecvig, conc, user.getLogin(), signo);
                    Mensaje = "El Registro ha sido modificado";
                    updated = true;
                    model.TimpuestoSvc.SEARCH_TI(dstrct,cod,fecvig, conc, age);
                }
                //model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia,"", concepto);                
            } else if (Opcion.equals("Modificar") && ( tipo_impuesto.equals("RICA") || tipo_impuesto.equals("RICACL") ) ){
                update = true;
                Conversion2(original);
                if ( cod.compareTo(codigo_impuesto)!=0 || fecvig.compareTo(fecha_vigencia)!=0 || conc.compareTo(concepto)!=0 || age.compareTo(agencia)!=0 ){
                    if ( !model.TimpuestoSvc.EXISTERICA(dstrct, codigo_impuesto, fecha_vigencia, agencia, concepto, tipo_impuesto) ) {
                        model.TimpuestoSvc.UPDATERICA(tipo_impuesto, codigo_impuesto, concepto, descripcion, fecha_vigencia, porcentaje1, cod_cuenta_contable,  agencia, cod, fecvig, conc, age, user.getLogin(), signo  );
                        Mensaje = "El Registro ha sido modificado";
                        updated = true;
                        model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia, concepto, agencia);
                    } else {
                        Mensaje = "El Registro ya existe";
                    }
                } else {
                    model.TimpuestoSvc.UPDATERICA(tipo_impuesto, codigo_impuesto, concepto, descripcion, fecha_vigencia, porcentaje1, cod_cuenta_contable,  agencia, cod, fecvig, conc, age, user.getLogin(), signo  );
                    Mensaje = "El Registro ha sido modificado";
                    updated = true;
                    model.TimpuestoSvc.SEARCH_TI(dstrct,cod,fecvig, conc, age);
                }
                
                //model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia,agencia, concepto);
                
            } else if ( Opcion.equals("Modificar") && ( tipo_impuesto.equals("RFTE") || tipo_impuesto.equals("RFTECL") ) ){
                update = true;
                Conversion2(original);
                if ( cod.compareTo(codigo_impuesto)!=0 || fecvig.compareTo(fecha_vigencia)!=0 || conc.compareTo(concepto)!=0 ){
                    if ( !model.TimpuestoSvc.EXISTERFTE(dstrct, codigo_impuesto, fecha_vigencia, concepto, tipo_impuesto) ){
                        model.TimpuestoSvc.UPDATERFTE(tipo_impuesto, codigo_impuesto, concepto, descripcion, fecha_vigencia, porcentaje1, cod_cuenta_contable,  cod, fecvig, conc, user.getLogin(), signo  );
                        Mensaje = "El Registro ha sido modificado";
                        updated = true;
                        model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia, concepto, agencia); 
                    } else {
                        model.TimpuestoSvc.SEARCH_TI(dstrct,cod,fecvig, conc, age);
                        Mensaje = "El Registro ya existe";
                    }
                } else {
                    model.TimpuestoSvc.UPDATERFTE(tipo_impuesto, codigo_impuesto, concepto, descripcion, fecha_vigencia, porcentaje1, cod_cuenta_contable,  cod, fecvig, conc, user.getLogin(), signo  );
                    Mensaje = "El Registro ha sido modificado";
                    updated = true;
                    model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia, concepto, agencia);
                }
                //model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia,agencia, concepto);                
            } else if (Opcion.equals("Modificar") && tipo_impuesto.equals("RIVA")){//Modific. Tito Andr�s Maturana
                update = true;
                Conversion2(original);
                if ( cod.compareTo(codigo_impuesto)!=0 || fecvig.compareTo(fecha_vigencia)!=0 ) {
                    if ( !model.TimpuestoSvc.EXISTERIVA(dstrct, codigo_impuesto, fecha_vigencia) ){
                        model.TimpuestoSvc.UPDATERIVA(tipo_impuesto, codigo_impuesto, concepto, fecha_vigencia, porcentaje1, cod_cuenta_contable, cod, fecvig, conc, user.getLogin(), signo);
                        Mensaje = "El Registro ha sido modificado";
                        updated = true;
                        model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia, concepto, agencia);
                    } else {
                        model.TimpuestoSvc.SEARCH_TI(dstrct,cod,fecvig, conc, age);
                        Mensaje = "El Registro ya existe";
                    }
                } else {
                    model.TimpuestoSvc.UPDATERIVA(tipo_impuesto, codigo_impuesto, concepto, fecha_vigencia, porcentaje1, cod_cuenta_contable, cod, fecvig, conc, user.getLogin(), signo);
                    Mensaje = "El Registro ha sido modificado";
                    updated = true;
                    model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia, concepto, agencia);
                }
                //model.TimpuestoSvc.SEARCH_TI(dstrct,codigo_impuesto,fecha_vigencia,agencia, concepto);
            } 
            
            if ("Modificar|Guardar|Eliminar|Anular|Activar".indexOf(Opcion) != -1) {
                //model.TimpuestoSvc.LIST("", "","");
                model.TimpuestoSvc.LIST(model.TimpuestoSvc.getTipoI(), model.TimpuestoSvc.getFagencias(), model.TimpuestoSvc.getFconcepto());
            }
            
            String next ="/jsp/cxpagar/Tipo_impuestos/Tipo_impuesto.jsp?Mensaje="+Mensaje+"&impuesto="+tipo;
            
            if ( listar ){
                next ="/jsp/cxpagar/Tipo_impuestos/Tipo_impuestoListar.jsp?Mensaje="+Mensaje+"&impuesto="+TipoI;
            } else if ( update  ){
                next ="/jsp/cxpagar/Tipo_impuestos/Tipo_impuestoUpdate.jsp?Mensaje="+Mensaje+"&impuesto="+TipoI+"&updated=";
                
                if( updated ){
                    next ="/jsp/cxpagar/Tipo_impuestos/Tipo_impuestoUpdate.jsp?Mensaje="+Mensaje+"&impuesto="+TipoI+"&updated=MsgModificado";
                }
            } else if ( anular ){
                next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
            }
            
            logger.info("? next: " + next );
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
                                    
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en Tipo_impuestoManagerAction .....\n"+e.getMessage());
        }
    }
    
    public static void Conversion(String f){
        String vF [] = f.split(",");
        tipo = vF[0];
        cod = vF[1];
        fecvig = vF[2];
        age = vF[3];
        conc = vF[4];
    }
    
    public static void Conversion2(String f){
        String vF [] = f.split(",");
        cod = vF[0];
        conc = vF[1];
        fecvig = vF[2];
        age = vF[3];
        tipo = vF[4];
    }
    
    
}
