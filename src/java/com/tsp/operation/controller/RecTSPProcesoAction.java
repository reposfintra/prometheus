/*
 * RecTSPProcesoAction.java
 *
 * Created on 25 de junio de 2005, 10:41 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Sandrameg
 */
public class RecTSPProcesoAction extends Action{
    
    /** Creates a new instance of RecTSPProcesoAction */
    public RecTSPProcesoAction() {
    }
    
    public void run() throws ServletException,InformationException {
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        
        String fi = "", ds = "", tp = "", nu = "";
        String next;
        
        fi = request.getParameter("fechai");        
        nu = u.getLogin();
        ds = u.getDstrct();
        tp = request.getParameter("tipo");
        
        Util ut= new Util();
        Calendar fchi = ut.crearCalendarDate(fi);
        Calendar fchf = ut.crearCalendarDate(ut.getFechaActual_String(4));
        
        if (fchf.compareTo(fchi) >= 0){
            RecTSPThread rtsp = new RecTSPThread();
            rtsp.star(nu, fi, ds, tp);       
            String msg = "El Proceso ha iniciado, consulte su estado en el log de procesos";
            next = "/pags_predo/recTSP.jsp?msg=" + msg;        
        }
        else {
            next = "/pags_predo/recTSP.jsp?msg=La fecha inicial no puede ser mayor que la fecha actual";
        }
        this.dispatchRequest(next);
    }
}
