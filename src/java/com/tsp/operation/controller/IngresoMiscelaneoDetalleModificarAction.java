/*
 * IngresoMiscelaneoDetalleModificarAction.java
 *
 * Created on 31 de julio de 2006, 04:44 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.pdf.*;


public class IngresoMiscelaneoDetalleModificarAction extends Action {
    
    /** Creates a new instance of IngresoMiscelaneoDetalleModificarAction */
    public IngresoMiscelaneoDetalleModificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next="/jsp/cxcobrar/ItemsIngreso/ItemsIngresoMiscelaneo.jsp?sw=ok";
        String auxiliar = "";
        String mon_local = (String) session.getAttribute("Moneda");
        String fecha_actual = Util.getFechaActual_String(4);
        String mon_ingreso = request.getParameter("mon_ingreso");
        String num_ingreso = request.getParameter("numingreso");
        int canItems = Integer.parseInt(request.getParameter("items"));
        double valor_tasa = 0;
        int item = 1;
        int nummax = 1;
        Vector vecitems = new Vector();
        
        String opcion = ( request.getParameter("opcion") != null )?request.getParameter("opcion"):"";
        
        try{
            
            if( opcion.equals("") ){
                Tasa t = model.tasaService.buscarValorTasa(mon_local, mon_ingreso, mon_local, fecha_actual );
                if(t!=null){
                    valor_tasa = t.getValor_tasa();
                    vecitems = new Vector();
                    for(int i=1; i<= canItems; i++ ){
                        ////System.out.println(i);
                        if ( request.getParameter("valor"+i) != null ){
                            Ingreso_detalle ingdetalle = new Ingreso_detalle();
                            ingdetalle.setDistrito(usuario.getDstrct());
                            ingdetalle.setNumero_ingreso(num_ingreso);
                            ingdetalle.setItem(Integer.parseInt(request.getParameter("nroitem"+i)) );
                            ingdetalle.setTipo_documento(request.getParameter("tipodoc"));
                            ingdetalle.setCuenta(request.getParameter( "cuenta"+i ).toUpperCase() );
                            auxiliar = ( request.getParameter( "auxiliar"+i ).equals("") && request.getParameter( "tipo"+i ).equals("") )?"" :request.getParameter( "tipo"+i )+"-"+request.getParameter( "auxiliar"+i ) ;
                            ingdetalle.setAuxiliar( auxiliar );
                            ingdetalle.setDescripcion(request.getParameter( "c_descripcion"+i ) );
                            ingdetalle.setTipo_doc( (request.getParameter( "doc"+i ).equals("") )?"":request.getParameter( "tipodoc"+i ) );
                            ingdetalle.setDocumento( request.getParameter( "doc"+i ).toUpperCase() );
                            ingdetalle.setValor_ingreso_me( convertirdouble(request.getParameter("valor"+i) ) );
                            ingdetalle.setValor_ingreso( convertirdouble( request.getParameter("valor"+i)  ) * valor_tasa );
                            ingdetalle.setCodigo_retefuente( "" );
                            ingdetalle.setValor_retefuente( 0.0 );
                            ingdetalle.setValor_retefuente_me( 0.0 );
                            ingdetalle.setCodigo_reteica( "" );
                            ingdetalle.setValor_reteica( 0.0 );
                            ingdetalle.setValor_reteica_me( 0.0 );
                            ingdetalle.setCreation_user(usuario.getLogin());
                            ingdetalle.setBase(usuario.getBase());
                            vecitems.add(ingdetalle);
                        }
                    }
                    model.ingreso_detalleService.actualizarIngresoMiscelaneo(usuario.getDstrct(),num_ingreso,request.getParameter("tipodoc"),vecitems,usuario.getLogin());
                    
                    //busco si tiene items
                    model.ingreso_detalleService.itemsMiscelaneo(num_ingreso, request.getParameter("tipodoc"), usuario.getDstrct());
                    Vector vecitem = model.ingreso_detalleService.getItemsMiscelaneo();
                    if(vecitem != null){
                        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
                        nummax = model.ingreso_detalleService.nroMayorItems(usuario.getDstrct(), request.getParameter("tipodoc"), num_ingreso);
                        for(int i = 0; i < vecitem.size(); i++){
                            Ingreso_detalle ingdetalle = (Ingreso_detalle) vecitem.get(i);
                            if(modelcontab.planDeCuentasService.existCuenta(ingdetalle.getDistrito(),ingdetalle.getCuenta())){
                                modelcontab.subledgerService.busquedaCuentasTipoSubledger(ingdetalle.getDistrito(),ingdetalle.getCuenta());
                                ingdetalle.setTipos(modelcontab.subledgerService.getCuentastsubledger());
                            }else{
                                ingdetalle.setTipos(null);
                            }
                        }
                    }
                    next+="&numero="+nummax+"&mensaje=Modificacion exitosa";
                }else{
                    next+="&mensaje=No existen Registro en la Tasas para la conversión";
                }
            }
            
            
            if( opcion.equals("imp_all") ){
                
                IngresoMPDF remision = new IngresoMPDF();
                
                remision.RemisionPlantilla();
                
                remision.crearCabecera();
                
                String tipo     = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                
                String numing   = ( request.getParameter("numingreso") != null )?request.getParameter("numingreso"):"";
                
                remision.crearRemision( model, usuario.getDstrct() , tipo , numing , "ALL", usuario.getLogin());
                
                remision.generarPDF();
                
                next = "/pdf/IngresoMPDF.pdf";

            }
            ////System.out.println(next);
            this.dispatchRequest(next);
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public double convertirdouble(String valor){
        String temp = (valor.equals(""))? "0": valor.replace(",","");
        return Double.parseDouble( temp );
    }
}
