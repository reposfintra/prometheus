/*
 * Codigo_demoraAnularAction.java
 *
 * Created on 26 de junio de 2005, 03:52 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class Codigo_demoraAnularAction extends Action{
    
    /** Creates a new instance of Codigo_demoraAnularAction */
    public Codigo_demoraAnularAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        try{
            Codigo_demora codigo_demora = new Codigo_demora();
            codigo_demora.setDescripcion(descripcion);
            codigo_demora.setCodigo(codigo);
            codigo_demora.setCia(usuario.getCia().toUpperCase());
            codigo_demora.setUser_update(usuario.getLogin().toUpperCase());
            codigo_demora.setCreation_user(usuario.getLogin().toUpperCase());
            model.codigo_demoraService.anularCodigo_demora(codigo_demora);
            request.setAttribute("mensaje","Codigo Demora Anulado");
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
