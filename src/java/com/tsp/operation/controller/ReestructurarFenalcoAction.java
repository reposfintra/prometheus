/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ReestructurarNegociosDAO;
import com.tsp.operation.model.DAOS.impl.ReestructurarNegociosImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.ConfiguracionDescuentosObligaciones;
import com.tsp.operation.model.beans.ConfiguracionTablaInicial;
import com.tsp.operation.model.beans.DocumentosNegAceptado;
import com.tsp.operation.model.beans.LiquidacionFenalcoBeans;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.formularioSaldo;
import com.tsp.operation.model.beans.resumenSaldosReestructuracion;
import com.tsp.operation.model.beans.tablaPagoInicial;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author egonzalez
 */
public class ReestructurarFenalcoAction extends Action {

    Usuario usuario = null;
    private final int BUSCAR_NEGOCIOS = 1;
    private final int BUSCAR_UNIDAD_NEG = 2;
    private final int SALDOS_NEGOCIOS_SEG = 3;
    private final int LIQUIDAR_NEGOCIO_FENALCO = 4;
    private final int GENERAR_EXTRACTO = 5;
    private final int CARGAR_DESCUENTOS = 6;
    private final int VALIDAR_DESCUENTOS = 7;
    private final int VALIDAR_PAGO_INICIAL = 8;
    private final int BUSCAR_NEGOCIOS_POR_APROBAR = 9;
    private final int BUSCAR_SALDOS_RESUMEN = 10;
    private final int BUSCAR_TABLA_INICIAL = 11;
    private final int VALIDAR_PERMISOS = 12;
    private final int APROBAR_REESTRUCTURACION = 13;
    private final int IMPRIMIR_EXTRACTO = 14;
    private final int BUSCAR_DATOS_LIQUIDACION = 15;
    private final int CARGAR_LIQUIDACION = 16;
    private final int RECHAZAR_NEGOCIO = 17;
    private final int GENERAR_IA = 18;
    private final int BUSCAR_CONFIG_ACTUAL = 19;
    private final int UPDATE_CONFIG_ACTUAL = 20;
    private final int BUSCAR_CONFIG_PAGO_INI = 21;
    private final int IMPRIMIR_EXTRACTO_PAGO = 22;

    HttpSession session;

    @Override
    public void run() throws ServletException, InformationException {

        session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        int opcion = Integer.parseInt(request.getParameter("opcion"));

        switch (opcion) {
            case BUSCAR_NEGOCIOS:
                buscarNegocios();
                break;
            case BUSCAR_UNIDAD_NEG:
                unidadNegocioLoad();
                break;
            case SALDOS_NEGOCIOS_SEG:
                saldosNegocioSegFenalco();
                break;
            case LIQUIDAR_NEGOCIO_FENALCO:
                liquidarNegocioFenalco();
                break;
            case GENERAR_EXTRACTO:
                generarExtracto();
                break;
            case CARGAR_DESCUENTOS:
                cargarDescuentos();
                break;
            case VALIDAR_DESCUENTOS:
                validarDescuentos();
                break;
            case VALIDAR_PAGO_INICIAL:
                validarPagoInicial();
                break;
            case BUSCAR_NEGOCIOS_POR_APROBAR:
                buscarNegociosPorAprobar();
                break;
            case BUSCAR_SALDOS_RESUMEN:
                buscarResumenReestructuracion(); 
                break;
            case BUSCAR_TABLA_INICIAL:
                buscarTablaInicial();
                break;
            case VALIDAR_PERMISOS:
                validarPermisos();
                break;
            case APROBAR_REESTRUCTURACION: 
                aprobarReestructuracion();
                break;                
            case IMPRIMIR_EXTRACTO:   
                imprimirExtracto();                        
                break;
            case BUSCAR_DATOS_LIQUIDACION:
                buscarCabeceraLiquidacion();
                break;
            case CARGAR_LIQUIDACION:
                buscarliquidarNegocioFenalco();
                break;
            case RECHAZAR_NEGOCIO:
                rechazarReestructuracion();
                break;
            case GENERAR_IA:
                generarNotaAjuste();
                break;
            case BUSCAR_CONFIG_ACTUAL:
                buscarConfigActual();
                break;
            case UPDATE_CONFIG_ACTUAL:
                actualizarConfigActual();
                break;
            case BUSCAR_CONFIG_PAGO_INI:
                buscarConfigPagoIni();
                break;
            case IMPRIMIR_EXTRACTO_PAGO :   
                imprimirExtractoPagos();                        
                break;

        }

    }

    private void buscarNegocios() {
        try {
            String cedula = request.getParameter("cedula") != null ? request.getParameter("cedula") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listGeneral = dao.buscarNegocioFenalco(cedula,usuario);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listGeneral) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private  void unidadNegocioLoad() {
        try {
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.buscarUnidadNegocio();
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private  void saldosNegocioSegFenalco() {
        try {

            String codNegocio = (request.getParameter("codneg") != null ? request.getParameter("codneg") : "");
            ArrayList<String> listNegocio;
            listNegocio = new ArrayList<>(Arrays.asList(codNegocio.split(";")));
            Collections.sort(listNegocio);
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listFacturasNegocios = dao.saldosNegocioSegFenalco(listNegocio,usuario);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listFacturasNegocios) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private synchronized void liquidarNegocioFenalco() {

        try {

            String idconvenio = request.getParameter("idconvenio") != null ? request.getParameter("idconvenio") : "";
            int nrCuotas = Integer.parseInt(request.getParameter("cuota") != null ? request.getParameter("cuota") : "0");
            double valorNegocio = Integer.parseInt(request.getParameter("valor_negocio") != null ? request.getParameter("valor_negocio").replaceAll(",", "") : "0");
            String primeraCuota = request.getParameter("primeracuota") != null ? request.getParameter("primeracuota") : "";
            String titulo_valor = request.getParameter("titulo_valor") != null ? request.getParameter("titulo_valor") : "";
            String afiliado = request.getParameter("afiliado") != null ? request.getParameter("afiliado") : "";

            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listLiquidacion = dao.liquidadorCreditoFenalco(nrCuotas, valorNegocio, primeraCuota, titulo_valor, idconvenio, afiliado);
            //int cuota, double valor_negocio, String primeracuota, String titulo_valor,String idconvenio,String afiliado
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listLiquidacion) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private synchronized void cargarDescuentos() {
        String respuestaJson = "{\"respuesta\":\"OK\"}";
        try {
            //Cargamos los descuentos tabla resumen
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList<ConfiguracionDescuentosObligaciones> cargarDescuentos = dao.cargarDescuentos(usuario);
            session.setAttribute("listaDescuentos", cargarDescuentos);

            //Cargamos configuracion inicial.
            ArrayList<ConfiguracionTablaInicial> cargarConfInicial = dao.cargarConfInicial(usuario);
            session.setAttribute("listaConfInicial", cargarConfInicial);

            if (cargarConfInicial.isEmpty() && cargarDescuentos.isEmpty()) {
                respuestaJson = "{\"respuesta\":\"NO\"}";
            }
        } catch (SQLException ex) {
            respuestaJson = "{\"error\":\"Lo sentimos algo salio mal\"}";
            Logger.getLogger(ReestructurarFenalcoAction.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                this.printlnResponse(respuestaJson, "application/json;");
            } catch (Exception ex) {
                Logger.getLogger(ReestructurarFenalcoAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private synchronized void validarDescuentos() {
        try {
            String respuestaJson = "{}";

            String tipo_negocio = request.getParameter("tipo_negocio") != null ? request.getParameter("tipo_negocio") : "";
            String tipo_descuento = request.getParameter("tipo_descuento") != null ? request.getParameter("tipo_descuento") : "";
            String inicial = request.getParameter("inicial") != null ? request.getParameter("inicial") : "N";
            int porcentaje = request.getParameter("porcentaje") != null ? Integer.parseInt(request.getParameter("porcentaje")) : 0;

            ArrayList<ConfiguracionDescuentosObligaciones> descuentos = (ArrayList<ConfiguracionDescuentosObligaciones>) session.getAttribute("listaDescuentos");

            for (ConfiguracionDescuentosObligaciones cdo : descuentos) {
                if (cdo.getTipo_negocio().equals(tipo_negocio) && cdo.getConcepto().equals(tipo_descuento)) {
                    if (inicial.equals("N")) {
                        if (cdo.getDescuento() >= porcentaje) {
                            respuestaJson = "{\"respuesta\":\"OK\"}";
                        } else {
                            respuestaJson = "{\"respuesta\":\"NO\"}";
                        }

                    } else if (cdo.getAplica_inicial().equals("S")) {
                        if (cdo.getPorcentaje_cta_inicial() <= porcentaje) {
                            respuestaJson = "{\"respuesta\":\"OK\"}";
                        } else {
                            respuestaJson = "{\"respuesta\":\"PI\"}";
                        }
                    } else {
                        respuestaJson = "{\"respuesta\":\"NOT\"}";
                    }
                    break;
                }
            }

            this.printlnResponse(respuestaJson, "application/json;");

        } catch (Exception ex) {
            Logger.getLogger(ReestructurarFenalcoAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void validarPagoInicial() {
        try {
            String respuestaJson = "{}";
            String tipo_negocio = request.getParameter("tipo_negocio") != null ? request.getParameter("tipo_negocio") : "";
            ArrayList<ConfiguracionTablaInicial> list = (ArrayList<ConfiguracionTablaInicial>) session.getAttribute("listaConfInicial");

            for (ConfiguracionTablaInicial cti : list) {
                if (cti.getTipo_negocio().equals(tipo_negocio) && cti.getPagoTotal().equals("S")) {
                    respuestaJson = "{\"respuesta\":\"OK\"}";
                    break;
                } else if (cti.getTipo_negocio().equals(tipo_negocio)) {
                    respuestaJson = "{\"respuesta\":\"" + cti.getIncluyeCapital() + ";" + cti.getIncluyeInteres() + ";" + cti.getIncluyeIntxmora() + ";" + cti.getIncluyeGac() + "\"}";

                }
            }

            this.printlnResponse(respuestaJson, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructurarFenalcoAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private synchronized void generarExtracto() {
        String tablaInicial = request.getParameter("tablaInicial") != null ? request.getParameter("tablaInicial") : "";
        String tablaSaldos = request.getParameter("tablaSaldos") != null ? request.getParameter("tablaSaldos") : "";
        String formulario = request.getParameter("formulario") != null ? request.getParameter("formulario") : "";
        String liquidador = request.getParameter("liquidador") != null ? request.getParameter("liquidador") : "";
        int id_rop = 0;
        ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());

        if (!tablaInicial.equals("") && !tablaSaldos.equals("") && !liquidador.equals("")) {
            try {

                String respuestaJson = "{}";
                Gson gson = new Gson();
                //Convertimos el json a un array de pagos iniciales
                Type tipoListaInicial = new TypeToken<ArrayList<tablaPagoInicial>>() {
                }.getType();
                ArrayList<tablaPagoInicial> listPagoInicial = gson.fromJson(tablaInicial, tipoListaInicial);

                //Convertimos el json a un array de resumen de pago
                Type tipoListaSaldos = new TypeToken<ArrayList<resumenSaldosReestructuracion>>() {
                }.getType();
                ArrayList<resumenSaldosReestructuracion> listResumenSaldos = gson.fromJson(tablaSaldos, tipoListaSaldos);

                //Convertimos el json a un array con los datos del formulario
                Type tipoFormularioSaldo = new TypeToken<ArrayList<formularioSaldo>>() {
                }.getType();
                ArrayList<formularioSaldo> listForm = gson.fromJson(formulario, tipoFormularioSaldo);

                Map<String, String> resultsMap = new HashMap<String, String>();
                for (formularioSaldo formularioBeasn : listForm) {
                    resultsMap.put(formularioBeasn.getName(), formularioBeasn.getValue());
                }

                //Convertimos el json del la liquidacion en array list
                Type tipoLiquidacion = new TypeToken<ArrayList<LiquidacionFenalcoBeans>>() {
                }.getType();
                ArrayList<LiquidacionFenalcoBeans> listLiquidacion = gson.fromJson(liquidador, tipoLiquidacion);

                String idRop = dao.guardarExtracto(listPagoInicial, usuario.getLogin());
                if (!idRop.equals("FAIL")) {
                    TransaccionService tservice = new TransaccionService(usuario.getBd());
                    tservice.crearStatement();
                    id_rop = Integer.parseInt(idRop);
                    //guardamos la tabla del extracto
                    for (int i = 0; i < listPagoInicial.size() - 1; i++) {
                        tablaPagoInicial pagoInicial = listPagoInicial.get(i);

                        tservice.getSt().addBatch(dao.guardarTablaPagoInicial(pagoInicial, id_rop, usuario));
                    }

                    tservice.execute();
                    tservice.closeAll();

                    //Insertamos la lista de saldos
                    tservice.crearStatement();
                    for (int i = 0; i < listResumenSaldos.size() - 1; i++) {
                        resumenSaldosReestructuracion beansResumenSaldos = listResumenSaldos.get(i);

                        if (beansResumenSaldos.getEstado().equals("VENCIDO")) {
                            beansResumenSaldos.setDcto_capital(resultsMap.get("dtoCapital_" + beansResumenSaldos.getNegocio() + "_1") != null ? resultsMap.get("dtoCapital_" + beansResumenSaldos.getNegocio() + "_1") : "0");
                            beansResumenSaldos.setDcto_interes(resultsMap.get("dtoInteres_" + beansResumenSaldos.getNegocio() + "_1") != null ? resultsMap.get("dtoInteres_" + beansResumenSaldos.getNegocio() + "_1") : "0");
                            beansResumenSaldos.setDcto_intxmora(resultsMap.get("dtointxMora_" + beansResumenSaldos.getNegocio() + "_1") != null ? resultsMap.get("dtointxMora_" + beansResumenSaldos.getNegocio() + "_1") : "0");
                            beansResumenSaldos.setDcto_gac(resultsMap.get("dtogac_" + beansResumenSaldos.getNegocio() + "_1") != null ? resultsMap.get("dtogac_" + beansResumenSaldos.getNegocio() + "_1") : "0");
                            beansResumenSaldos.setTotal_Dcto(resultsMap.get("dtoGeneral_" + beansResumenSaldos.getNegocio() + "_1") != null ? resultsMap.get("dtoGeneral_" + beansResumenSaldos.getNegocio() + "_1") : "0");

                        } else {
                            beansResumenSaldos.setDcto_capital("0");
                            beansResumenSaldos.setDcto_interes("0");
                            beansResumenSaldos.setDcto_intxmora("0");
                            beansResumenSaldos.setDcto_gac("0");
                            beansResumenSaldos.setTotal_Dcto("0");
                        }
                        beansResumenSaldos.setId_rop(id_rop);
                        tservice.getSt().addBatch(dao.guardarSaldosResumen(beansResumenSaldos, usuario));
                    }
                    tservice.execute();
                    tservice.closeAll();

                    //Insertamos la lista de liquidacion
                    tservice.crearStatement();
                    for (LiquidacionFenalcoBeans liquidacionBeans : listLiquidacion) {
                        liquidacionBeans.setId_rop(id_rop);
                        liquidacionBeans.setCreation_user(usuario.getLogin());
                        tservice.getSt().addBatch(dao.guardarLiquidacion(liquidacionBeans));
                    }

                    tservice.execute();
                    tservice.closeAll();

                    //  dao.generarPdf(usuario.getLogin(), id_rop);
                    respuestaJson = "{\"respuesta\":\"OK\"}";

                } else {
                    respuestaJson = "{\"error\":\"Lo sentimos algo salio mal\"}";
                }

                this.printlnResponse(respuestaJson, "application/json;");
            } catch (SQLException ex) {
                if (id_rop > 0) {
                    dao.borrarExtracto(id_rop);
                }
                Logger.getLogger(ReestructurarFenalcoAction.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ReestructurarFenalcoAction.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    private synchronized void buscarNegociosPorAprobar() {
        try {
            String aprobar = request.getParameter("apro") != null ? request.getParameter("apro") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listGeneral = dao.buscarNegociosPorAprobar(aprobar);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listGeneral) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void buscarResumenReestructuracion() {
        try {
            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) :0; 
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listGeneral = dao.buscarResumenSaldos(idRop);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listGeneral) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void buscarTablaInicial() {
        try {
            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) :0; 
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listGeneral = dao.buscarTablaPagoInicial(idRop);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listGeneral) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void validarPermisos() {
        try {
            String respuestaJson="{}";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String validar  = dao.validarPermisos(usuario.getLogin());
            switch(validar){
                case "T":
                    respuestaJson="{\"respuesta\":\"T\"}";
                    break;
                case "A":
                    respuestaJson="{\"respuesta\":\"A\"}";
                    break;
                case "I":
                    respuestaJson="{\"respuesta\":\"I\"}";
                    break;  
                 default: 
                    respuestaJson="{\"respuesta\":\"F\"}";
            }
          
            this.printlnResponse(respuestaJson, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private synchronized void aprobarReestructuracion() {
        try {
            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) :0; 
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String aprobarReestructuracion = dao.aprobarReestructuracion(idRop, usuario.getLogin());
            this.printlnResponse(aprobarReestructuracion, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private synchronized void  imprimirExtracto() {
        try {
            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) :0; 
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            dao.generarPdf(usuario.getLogin(), idRop);
            String actualizarImpresion = dao.actualizarImpresion(idRop,usuario.getLogin());
            
            this.printlnResponse(actualizarImpresion, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private synchronized void  imprimirExtractoPagos() {
        try {
            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) :0; 
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            dao.generarPdf(usuario.getLogin(), idRop);
            String respuestaJson = "{\"respuesta\":\"OK\"}";            
            this.printlnResponse(respuestaJson, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private synchronized void buscarCabeceraLiquidacion() {
        try {
            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) :0; 
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String buscarCabeceralIquidacion = dao.buscarCabeceralIquidacion(idRop);
            this.printlnResponse(buscarCabeceralIquidacion, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void buscarliquidarNegocioFenalco() {

        try {

            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) : 0;

            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList<DocumentosNegAceptado> buscarliquidacionFenalco = dao.buscarliquidacionFenalco(idRop);
            //int cuota, double valor_negocio, String primeracuota, String titulo_valor,String idconvenio,String afiliado
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(buscarliquidacionFenalco) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void rechazarReestructuracion() {

        try {

            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) : 0;
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String rString = dao.rechazarReestructuracionFenalco(idRop);
            this.printlnResponse(rString, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void generarNotaAjuste() {
        try {
            String respuestaJson = "{}";
            int idRop = request.getParameter("idRop") != null ? Integer.parseInt(request.getParameter("idRop")) : 0;
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String rString = dao.crearNotaAjusteFenalco(idRop, usuario);
            switch (rString) {
                case "OK":
                    respuestaJson = "{\"respuesta\":\"OK\"}";
                    break;
                case "FAIL":
                    respuestaJson = "{\"respuesta\":\"FAIL\"}";
                    break;

            }
            this.printlnResponse(rString, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void buscarConfigActual() {
        try {
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList <ConfiguracionDescuentosObligaciones> listConfigActual = dao.buscarConfigActual();
            if(listConfigActual.isEmpty()){  
                //aqui llamo metodo de insertar para agregar la informaci�n correspondiente al nuevo mes en base al anterior
                int result=dao.insertarConfigActual(usuario.getLogin());
                dao.insertarConfigPagoIni(usuario.getLogin());
                if (result>0){
                     //llamo nuevamente el m�todo de b�squeda.
                     listConfigActual = dao.buscarConfigActual();
                }
              
            }
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listConfigActual) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void buscarConfigPagoIni() {
        try {
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList <ConfiguracionTablaInicial> listConfigPagoIni = dao.buscarConfigPagoIni();
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listConfigPagoIni) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void  actualizarConfigActual() {
        try {
            int id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")):0;
            String tiponeg = request.getParameter("tiponeg") != null ? request.getParameter("tiponeg"):"";
            String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo"):"";
            int descuento = request.getParameter("descuento") != null ? Integer.parseInt(request.getParameter("descuento")):0;
            int porc_cta_ini = request.getParameter("cta_ini") != null ? Integer.parseInt(request.getParameter("cta_ini")):0;
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String actualizarConfigActual = dao.actualizarConfigActual(id,tiponeg,descuento,porc_cta_ini,tipo,usuario.getLogin());
            
            this.printlnResponse(actualizarConfigActual, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

}
