/**
 *
 */
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class CaravanaUpdateAction extends Action {
  static Logger logger = Logger.getLogger(CaravanaUpdateAction.class);
  
  /**
   * Executes the action
   */
  public void run() throws ServletException, InformationException {
    String cmd = request.getParameter("cmd");
    PlanViaje planVj = null;
    String next = null;
    Caravana caravana = null;
    HttpSession session = request.getSession();
    Usuario usuario = (Usuario)session.getAttribute("Usuario");
    if(cmd == null){
         try{
               model.caravanaService.listarPvjCaravana("0");
         }
         catch(SQLException e){
                throw new ServletException(e.getMessage());
         }
         next = "/caravana/PantallaModCaravana.jsp";
    }
    else{
        //BUSCAR UNA CARAVANA
        if(cmd.equals("find")){
            String carNo = request.getParameter("caravana");
            try{
               model.caravanaService.listarPvjCaravana(carNo);
               if (model.caravanaService.getPlanilla().size() == 0){  
                  request.setAttribute("mensaje", "*** � LA CARAVANA NO EXISTE ! ***");
               }
               
            }
            catch(SQLException e){
                throw new ServletException(e.getMessage());
            }
            next = "/caravana/PantallaModCaravana.jsp";
        }
        
        //ADICIONAR UNA PLANILLA A UNA CARAVANA EXISTENTE
        else if(cmd.equals("add")){
                if (usuario.getAccesoplanviaje().equals("1")){
                    String cia =  request.getParameter("cia");
                    String planilla = request.getParameter("planilla").toUpperCase();
                    String carNo = request.getParameter("caravana");
                    try{
                       //SE VERIFICA QUE LA PLANILLA EXISTA EN MIMS
                       if (model.planViajeService.planillaExist(cia, planilla)){
                           //SE VERIFICA QUE NO SE LE HALLA CREADO PLAN DE VIAJE
                           if (!model.planViajeService.planViajeExist(cia, planilla)){
                               model.planViajeService.getPlanVjInfoNew(cia, planilla);
                               planVj = model.planViajeService.getPlanViaje();
                               model.viaService.getViasRelPg(planVj.getRuta());
                               //SE PONEN LOS DATOS DEL PLAN DE VIAJE Y EL NUMERO DE CRAVANA EN SESION
                               session.setAttribute("planVj", planVj);
                               session.setAttribute("caravana", carNo);
                               next = "/caravana/PantallaCaravanaExistInfo.jsp";
                           }
                           else{
                               //SI EL PLAN DE VIAJE YA FUE CREADO SE VERIFICA QUE NO PERTENESCA A OTRA CARAVANA
                               if ( !model.caravanaService.planViajeExist(cia, planilla) ){
                                  caravana = new Caravana();
                                  caravana.setCodigo(carNo);
                                  caravana.setPlanilla(planilla);
                                  caravana.setCia(cia);
                                  caravana.setUsuario(usuario.getLogin());
                                  model.caravanaService.crearCaravana(caravana);
                                  model.caravanaService.listarPvjCaravana(carNo);
                               } 
                               else{
                                    request.setAttribute("mensaje", "*** � LA PLANILLA " + planilla + " YA PERTENECE A UNA CARAVANA ! ***");
                               }
                               next = "/caravana/PantallaModCaravana.jsp";
                           }
                       }
                       else{
                            request.setAttribute("mensaje", "*** � LA PLANILLA " + planilla + " NO HA SIDO CREADA ! ***");
                            next = "/caravana/PantallaModCaravana.jsp";
                       }                       
                    }
                    catch(SQLException e){
                        throw new ServletException(e.getMessage());
                    }
                }
                else{
                    request.setAttribute("mensaje", "*** � NO TIENE LOS PERMISOS SUFICIENTES PARA ADICIONAR UNA PLANILLA A LA CARAVANA ! ***");
                    next = "/caravana/PantallaModCaravana.jsp";
                }
        }
        
        //ELIMINRA UNA PLANILLA DE UNA CARAVANA
        else if (cmd.equals("del")){
                    if (usuario.getAccesoplanviaje().equals("1")){
                        String cia =  request.getParameter("cia");
                        String planilla = request.getParameter("planilla");
                        String carNo = request.getParameter("caravana");
                        String motivo = request.getParameter("motivo");
                        String creation_date = request.getParameter("creation_date");
                        try{
                           model.caravanaService.elimnarPlaCaravana(cia, planilla, carNo, usuario.getLogin(), motivo, creation_date);
                           model.caravanaService.listarPvjCaravana(carNo);
                        }
                        catch(SQLException e){
                            throw new ServletException(e.getMessage());
                        }
                        next = "/caravana/PantallaModCaravana.jsp";
                    }
                    else{
                        request.setAttribute("mensaje", "*** � NO TIENE LOS PERMISOS NESCESARIOS PARA ELIMINAR UNA PLANILLA DE UNA CARAVANA ! ***");
                        next = "/caravana/PantallaModCaravana.jsp";
                    }
        }    
    }
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }
}
