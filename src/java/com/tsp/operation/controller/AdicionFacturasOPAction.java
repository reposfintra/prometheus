  /***************************************
    * Nombre Clase ............. AdicionFacturasOPAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para la generacion de facturas adicionales a las OPs
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  29/11/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/





package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.HFacturasAdicionalOP;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;




public class AdicionFacturasOPAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {
        
        
         try{
             
         
                HttpSession session    = request.getSession();
                Usuario     usuario    = (Usuario)session.getAttribute("Usuario");
                String      user       = usuario.getLogin();
                String      distrito   = usuario.getDstrct();


                String      next       = "/jsp/cxpagar/generarOP/AdicionFacturas.jsp";
                String      msj        = "";


               String      evento     = request.getParameter("evento");  

               if( evento!= null ){
               
                   if(evento.equals("GENERAR")){
                       HFacturasAdicionalOP  hilo =  new HFacturasAdicionalOP();
                       hilo.start(model, usuario, distrito);                       
                       msj = "El proceso de generaci�n ha comenzado...";
                   }
                    
               }               
               
               
              next += "?msj=" + msj ; 
               
              RequestDispatcher rd = application.getRequestDispatcher( next );
              if(rd == null)
                 throw new Exception("No se pudo encontrar "+ next);
              rd.forward(request, response); 
              
                            
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }    
               
        
       
    }
    
}
