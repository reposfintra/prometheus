/*************************************************************************
 * Nombre:        RetroactivoBuscarAction.java                          *
 * Descripci�n:   Clase Action para modificar retroactivo               *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales                *
 * Fecha:         7 de febrero de 2006, 04:19 PM                        * 
 * Versi�n        1.0                                                   * 
 * Coyright:      Transportes Sanchez Polo S.A.                         *
 *************************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

 
public class RetroactivoBuscarAction extends Action {
    
    /** Creates a new instance of RetroactivoBuscarAction */
    public RetroactivoBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/masivo/retroactivo/retroactivoMod.jsp";
        String stdjob = request.getParameter("stdjob");
        String ruta = request.getParameter("ruta");
        String fecha1 = request.getParameter("fecha1");
        String fecha2 = request.getParameter("fecha2");
        try{
            model.retroactivoService.buscarRetroactivo(stdjob,ruta,fecha1,fecha2); 
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
