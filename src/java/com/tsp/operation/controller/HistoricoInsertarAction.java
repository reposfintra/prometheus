/*
 * HistoricoInsertarAction.java
 *
 * Created on 29 de septiembre de 2005, 11:01 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.sql.SQLException.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import org.apache.log4j.Logger;
/**
 *
 * @author  dlamadrid
 */
public class HistoricoInsertarAction extends Action{
    
    /** Creates a new instance of HistoricoInsertarAction */
    public HistoricoInsertarAction() {
    }
    
    public void run() throws ServletException,InformationException{
      //  String 
       // HttpSession session = request.getSession();
       // Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
         String usuario = ""+request.getParameter("usuario");
        String tablaOrigen = ""+request.getParameter("tablaO");
        //System.out.println("tablaO"+tablaOrigen);
        String tablaDestino = "h_"+tablaOrigen;
        //System.out.println("tabla D " +tablaDestino);
        String fechaInicio =""+request.getParameter("fechaInicio")+":00";
        //System.out.println("fechaI"+fechaInicio);
        int duracion = Integer.parseInt(""+request.getParameter("duracion"));
        //System.out.println("duracion"+duracion);
        String fechaFinal =model.adminH.fechaFinal(fechaInicio,duracion);
        //System.out.println("fechafinal"+fechaFinal);
        String rutina=""+request.getParameter("rutina");
        //System.out.println("rutina"+rutina);
        String estado=""+request.getParameter("estadoo");
        //System.out.println("estado"+estado);
       // String us=usuario.getIdusuario();
        //System.out.println("usuario"+usuario);
        
        if(!rutina.equals("")){
            tablaOrigen="";
            tablaDestino="";
        }
        AdminHistorico adminH;
        adminH = new AdminHistorico("",fechaInicio,fechaFinal,tablaOrigen,tablaDestino,rutina,duracion,estado,usuario,"");
         //System.out.println("paso por los request");
         
        try {
            String error="";
            //System.out.println("tabla origen"+tablaOrigen);
            if (rutina.equals("")){ 
                if ((model.adminH.existeTabla(tablaOrigen)==true)){
                    //System.out.println("entra em insertar"+model.adminH.existeTabla(tablaOrigen));
                    //System.out.println("entra em insertar"+model.adminH.existeHistorico(tablaOrigen)+tablaOrigen);
                    if (model.adminH.existeHistorico(tablaOrigen)==false ){
                        model.adminH.setAdmin(adminH);
                        //System.out.println("seteo el objeto");
                        model.adminH.insertar();
                        model.adminH.listarHistorico();
                        String next = "/adminHistorico/ahistorico.jsp?reload=";
                        this.dispatchRequest(Util.LLamarVentana(next, "Cronograma de Historicos"));
                       // this.dispatchRequest(next);
                    }
                    else{
                        error="Ya existe un Cronograma para la tabla"+tablaOrigen;
                        String next = "/adminHistorico/errorpage.jsp?error="+error;
                        this.dispatchRequest(next);
                    }
                 }   
                 else{
                     error="No existe una Tabla con el nombre "+tablaOrigen+ " en la Base de Datos";
                     String next = "/adminHistorico/errorpage.jsp?error="+error;
                     this.dispatchRequest(next);
                 }
            }
            else{
                model.adminH.setAdmin(adminH);
                //System.out.println("seteo el objeto");
                model.adminH.insertar();
                model.adminH.listarHistorico();
                String next = "/adminHistorico/ahistorico.jsp?reload=";
                this.dispatchRequest(Util.LLamarVentana(next, "Cronograma de Historicos"));
               // this.dispatchRequest(next);
            }
        }     
       catch(SQLException e){
           //System.out.println(e.getMessage());
          //throw new ServletException("Accion:"+ e.getMessage());
        }    
        //model.adminH.setAdmin(admin
        
    }    
}
