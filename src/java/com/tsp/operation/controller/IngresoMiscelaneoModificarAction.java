
/***************
 * Nombre:        IngresoMiscelaneoModificarAction.java
 * Descripci�n:   Clase Action para controlar la modificacion
 *                de los ingresos miscelaneos
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         Created on 28 de junio de 2006, 03:21 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 ****************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.pdf.*;

public class IngresoMiscelaneoModificarAction extends Action {
    
    /** Creates a new instance of IngresoMiscelaneoModificarAction */
    public IngresoMiscelaneoModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        String mon_local = "";
        String mon_ingreso = request.getParameter("moneda");
        double valor_con = Double.parseDouble(request.getParameter("valor"));
        String fecha_actual = Util.getFechaActual_String(4);
        boolean sw = true;
        //System.out.println("Entra al Action [" +request.getParameter("tipodoc")+"]");
        
        String opcion = ( request.getParameter("opcion") != null )?request.getParameter("opcion"):"" ;
        
        
        try{
            
            if(opcion.equals("")){
                Ingreso ing = new Ingreso();
                ing.setDstrct(usuario.getDstrct());
                ing.setReg_status("");
                ing.setCodcli(request.getParameter("cliente"));
                ing.setNum_ingreso( request.getParameter("num") );
                ing.setConcepto(request.getParameter("concepto"));
                ing.setFecha_consignacion(request.getParameter("fecha"));
                ing.setBranch_code(request.getParameter("banco"));
                String sucursal = request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"";
                String vec[] = sucursal.split("/");
                if( vec != null && vec.length > 1 )
                    sucursal = vec[0];
                ing.setBank_account_no(sucursal);
                ing.setCodmoneda(mon_ingreso);
                ing.setLast_update(Util.fechaActualTIMESTAMP());
                ing.setUser_update(usuario.getLogin());
                ing.setDescripcion_ingreso(request.getParameter("descripcion"));
                ing.setNro_consignacion(request.getParameter("nro_consignacion"));
                ing.setTipo_documento(request.getParameter("tipodoc"));
                ing.setCuenta("");
                ing.setAuxiliar("");
                ing.setAbc("");
                mon_local =  (String) session.getAttribute("Moneda");
                Tasa tasa = model.tasaService.buscarValorTasa( mon_local, mon_ingreso, mon_local, request.getParameter("fecha"));
                
                if(tasa!=null){
                    ing.setVlr_ingreso(tasa.getValor_tasa() * valor_con );
                    ing.setVlr_ingreso_me(valor_con);
                    ing.setVlr_tasa(tasa.getValor_tasa());
                    ing.setFecha_tasa(tasa.getFecha());
                }
                else{
                    sw = false;
                }
                
                if(sw){
                    Banco ban = model.servicioBanco.obtenerBanco( ing.getDstrct(), ing.getBranch_code(), ing.getBank_account_no());
                    //valido cuando es ingreso y la moneda del ingreso sea igual a la moneda del banco
                    if( ban.getMoneda().equals( ing.getCodmoneda() ) ){
                        model.ingresoService.modificarIngreso(ing);
                        model.ingresoService.buscarIngresoMiscelaneo( usuario.getDstrct(), request.getParameter("tipodoc"), request.getParameter("num") );
                        next+="?msg=ok&sw=ok";
                        if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), ing.getTipo_documento(), ing.getNum_ingreso() )  )
                            next+="&modificar=true";
                        else
                            next+="&modificar="+null;
                    }
                    else{
                        request.setAttribute("mensaje","La moneda "+ ing.getCodmoneda()+" no pertenece al banco "+ing.getBranch_code()+" "+ing.getBank_account_no());
                        if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), ing.getTipo_documento(), ing.getNum_ingreso() )  )
                            next+="?modificar=true";
                        else
                            next+="?modificar="+null;
                    }
                }
                else{
                    next+="?msg=error";
                }
            }
            if( opcion.equals("imp") ){
                
                IngresoMPDF remision = new IngresoMPDF();
                
                remision.RemisionPlantilla();
                
                remision.crearCabecera();
                
                String tipo     = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                
                String numing   = ( request.getParameter("num") != null )?request.getParameter("num"):"";
                
                remision.crearRemision( model, usuario.getDstrct(), tipo ,  numing, "ONE",usuario.getLogin());
                
                remision.generarPDF();
                
                next = "/pdf/IngresoMPDF.pdf";
                
            } else if( opcion.equals("imp_all") ){
                
                IngresoMPDF remision = new IngresoMPDF();
                
                remision.RemisionPlantilla();
                
                remision.crearCabecera();
                
                String tipo     = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                
                String numing   = ( request.getParameter("numingreso") != null )?request.getParameter("numingreso"):"";
                
                remision.crearRemision( model, usuario.getDstrct() , tipo , numing , "ALL", usuario.getLogin());
                
                remision.generarPDF();
                
                next = "/pdf/IngresoMPDF.pdf";
                
            }
            
            
            this.dispatchRequest(next);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
