/***********************************************
 * Nombre clase: PlacaInsertAction.java
 * Descripci�n: Accion para ingresar una placa a la bd.
 * Autor: ffernandez
 * Fecha: 4 de noviembre de 2004, 02:48 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class Control_ExcepcionAdicionarAction extends Action{
    
    //static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public Control_ExcepcionAdicionarAction() {
    }
    
    public void run () throws ServletException, InformationException {
   
        String next = "/jsp/trafico/planviaje/control_excepcion/Control_execpcion.jsp";
        BeanGeneral Control = new BeanGeneral ();
        Date fecha = new Date ();
        SimpleDateFormat format = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String fechacrea = format.format (fecha);
        String msg = "";        
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            Control.setValor_01 ((request.getParameter ("cod_cli")!=null)?request.getParameter ("cod_cli"):"");
            Control.setValor_02 ((request.getParameter ("ciudadOri")!=null)?request.getParameter ("ciudadOri"):"");
            Control.setValor_03 ((request.getParameter ("ciudadDest")!=null)?request.getParameter ("ciudadDest"):"");
            
            String a = (request.getParameter ("agencia")!=null)?request.getParameter ("agencia"):"";
            a = a.equals("NADA")?"":a;
            Control.setValor_04 (a); 
            Control.setValor_05((request.getParameter ("P_Viaje")!=null)?request.getParameter ("P_Viaje"):"N");
            Control.setValor_06((request.getParameter ("H_Reporte")!=null)?request.getParameter ("H_Reporte"):"N");
            Control.setValor_07((request.getParameter ("Perno")!=null)?request.getParameter ("Perno"):"N");
            String Codigo = Control.getValor_01(); 
            String Origen = Control.getValor_02();
            String Destino = Control.getValor_03();
            String Agencia = Control.getValor_04();
            TreeMap ciudades = model.stdjobdetselService.getCiudadesOri();
            
            try{
                System.out.println("Codigo "+Codigo + "Origen "+ Origen + "Destino "+ Destino + "Agencia "+Agencia);
               if (!model.control_excepcionService.existeControl(Codigo,Origen,Destino,Agencia)){
                    if(model.clienteService.existeCliente(Codigo)){
                          model.control_excepcionService.agregarControlE(Control);
                          next += "?msg=" + "Control Excepci�n fue agregado exitosamente";

                    }else{
                        next += "?msg=" + "El codigo del Clinte no existe"  ;

                    }
               } else{
                  next += "?msg=" + "Control Excepci�n ya existe"; 
               }
            }
            catch (Exception e){
                e.printStackTrace ();
                throw new ServletException (e.getMessage ());
            }
        
        // Redireccionar a la p�gina indicada.
         this.dispatchRequest (next);
    }
}
