/*
 * ReportesSetupAction.java
 *
 * Created on 5 de abril de 2004, 09:49 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;

/**
 * Acci�n que permite gestionar la configuraci�n de reportes.
 * @author  iherazo
 */
public class GestionReportesSetupAction extends Action {
    /**
     * Ejecuta la acci�n. Esta en particular, es la encargada de gestionar
     * la configuraci�n de los campos que aparecen en los reportes.
     * @throws ServletException Si ocurre un error durante el procesamiento
     *         de la accion.
     * @throws IOException Si no se puede abrir la vista solicitada por esta
     *         accion, la cual corresponde a una p�gina jsp.
     */
    public void run() throws ServletException, InformationException {
        try {
            String cmd = request.getParameter("cmd");
            Usuario loggedUser = (Usuario) request.getSession().getAttribute("Usuario");
            if ( cmd == null ){
                model.confReportService.generarDatosReporte();
                this.dispatchRequest("/jsp/sot/admin/ConfigReportes.jsp");
            }
            else if ( cmd.equals("loadUsers") ) {
                String tipo = request.getParameter("tipo_usuario");
                model.confReportService.buscarUsuarios(tipo, loggedUser);
                model.confReportService.buscarReportes();
                this.dispatchRequest("/jsp/sot/body/PantallaAgregarConfigReporte.jsp?tipo_usuario="+tipo+"&listaCampos=");
            }
            else if ( cmd.equals("actualizar") ){
                String [] reportSetup = new String[4];
                reportSetup[0] = request.getParameter("cliente");
                reportSetup[1] = request.getParameter("reporte");
                String campos [] = request.getParameterValues("fields");
                campos = organizarCampos(campos);
                StringBuffer sb = new StringBuffer();
                for(int i=0; i<campos.length; i++ ){
                    sb.append(campos[i]);
                    if ( i < campos.length-1 ){
                        sb.append(',');
                    }
                }
                reportSetup[2] = sb.toString();
                reportSetup[3] = request.getParameter("tipoUsuario");
                model.confReportService.salvarConfigReporte(reportSetup);
                request.setAttribute("tsp.sot.bodyMessage","�� Los campos fueron editados correctamente !!");
                this.dispatchRequest("/jsp/sot/admin/popupMensaje.jsp");
            }
            else if( cmd.equals("eliminar") ) {
                model.confReportService.eliminarConfigReporte(request.getParameterValues("chk"));
                model.confReportService.generarDatosReporte();
                this.dispatchRequest("/jsp/sot/admin/ConfigReportes.jsp");
            }
            else if( cmd.equals("save") ) {
                String [] reportSetup = new String[4];
                reportSetup[0] = request.getParameter("usuario");
                reportSetup[1] = request.getParameter("reporte");
                reportSetup[2] = request.getParameter("listaCampos").replaceAll(" ","");
                // La primera letra puede ser C = Cliente | D = Destinatario | U = Usuario
                reportSetup[3] = ""+request.getParameter("tipoUsuario").charAt(0);
                model.confReportService.salvarConfigReporte(reportSetup);
                request.setAttribute("tsp.sot.bodyMessage", "�� Configuraci�n agregada exitosamente !!");
                model.confReportService.generarDatosReporte();
                this.dispatchRequest("/jsp/sot/admin/ConfigReportes.jsp");
            }
            else if ( cmd.equals("adicionar") ){
                model.confReportService.buscarReportes();
                this.dispatchRequest("/jsp/sot/body/PantallaAgregarConfigReporte.jsp?listaCampos=");
            }
            else if ( cmd.equals("agregar") ){
                String campos [] = request.getParameterValues("fields");
                StringBuffer sb = new StringBuffer();
                for(int i=0; i<campos.length; i++ ){
                    sb.append(campos[i]);
                    if ( i < campos.length-1 ){
                        sb.append(',');
                    }
                }
                this.dispatchRequest("/jsp/sot/body/agregarConfReporte.jsp?listaCampos="+sb);
            }
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
    
    private String [] organizarCampos(String [] campos){
        String [] nuevo = new String[campos.length];
        for(int i=0; i<nuevo.length; i++ ){
            int pos = Integer.parseInt(request.getParameter("orden"+campos[i]));
            nuevo[pos-1] = campos[i];
        }
        return nuevo;
    }
}
