/********************************************************************
 * Nombre clase: Esquema_formatoInsertAction.java
 * Descripci�n: Accion para ingresar un registro de esquema forma
 * Autor: Jose de la rosa
 * Fecha: 19 de octubre de 2006, 10:02 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  EQUIPO13
 */
public class Esquema_formatoInsertAction extends Action{
    
    /** Creates a new instance of Esquema_formatoInsertAction */
    public Esquema_formatoInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/masivo/formato/Esquema_formatoInsertar.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String dstrct = (String) session.getAttribute ("Distrito");
        String codigo_programa = request.getParameter ("c_codigo");
        String nombre_campo = request.getParameter ("c_nombre");
        String titulo = request.getParameter ("c_titulo");
        String tipo = request.getParameter ("c_tipo");
        int inicio = Integer.parseInt ( request.getParameter ("c_pos_inicial").equals ("")?"0":request.getParameter ("c_pos_inicial") );
        int fin = Integer.parseInt ( request.getParameter ("c_pos_final").equals ("")?"0":request.getParameter ("c_pos_final") );
        int orden = Integer.parseInt ( request.getParameter ("c_orden").equals ("")?"0":request.getParameter ("c_orden") );
        int sw=0;
        try{
            
            Esquema_formato esquema = new Esquema_formato ();
            esquema.setCodigo_programa (codigo_programa);
            esquema.setNombre_campo (nombre_campo);
            esquema.setPosicion_final (fin);
            esquema.setPosicion_inicial (inicio);
            esquema.setOrden (orden);
            esquema.setTipo (tipo);
            esquema.setTitulo (titulo);
            esquema.setDistrito (dstrct.toUpperCase ());
            esquema.setUsuario (usuario.getLogin ().toUpperCase ());
            esquema.setBase (usuario.getBase ().toUpperCase ());
            if( !model.esquema_formatoService.ordenEsquema_formato ( dstrct.toUpperCase (), codigo_programa, nombre_campo, orden ) ){
                if( !model.esquema_formatoService.rangoEsquema_formato ( dstrct.toUpperCase (), codigo_programa, nombre_campo, inicio, fin ) ){
                    try{
                        model.esquema_formatoService.insertEsquema_formato (esquema);
                    }catch(SQLException e){
                        sw=1;
                    }
                    if(sw==1){
                        if( !model.esquema_formatoService.existEsquema_formato (codigo_programa, dstrct.toUpperCase (), nombre_campo) ){
                            model.esquema_formatoService.updateEsquema_formato ( esquema );
                            request.setAttribute ("mensaje","La informaci�n ha sido ingresada exitosamente!");
                        }
                        else{
                            request.setAttribute ("mensaje","Error el registro ya existe!");
                        }
                    }
                    else{
                        request.setAttribute ("mensaje","La informaci�n ha sido ingresada exitosamente!");
                    }
                }else
                    request.setAttribute ("mensaje","Error el la posici�n inicial o final, esta dentro de un rango ya grabado!");
            }else
                request.setAttribute ("mensaje","Error el orden ya existe!");
            model.tablaGenService.buscarRegistros ("TFORDESP");
            request.setAttribute ( "set_formato", model.tablaGenService.obtenerTablas ());
            model.tablaGenService.buscarRegistros ("TFORTIP");
            request.setAttribute ( "set_tipo", model.tablaGenService.obtenerTablas ());
            model.tablaGenService.buscarRegistros ("TBLFORMA");
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
    
}
