/*
 * InformacionPlanilla_sancionAction.java
 *
 * Created on 20 de octubre de 2005, 08:26 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class InformacionPlanilla_sancionAction extends Action{
    
    /** Creates a new instance of InformacionPlanilla_sancionAction */
    public InformacionPlanilla_sancionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String numpla = request.getParameter("numpla").toUpperCase();
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "";
        try{
            if( model.demorasSvc.existePlanilla(numpla) )            
                next = "/jsp/cumplidos/sancion/SancionInsertar.jsp?c_numpla=" + numpla+"&sw=false&msg=";
            else
                next = "/jsp/cumplidos/sancion/asignarSancionPlanilla.jsp?msg=N�mero de planilla no existe.";
            next = Util.LLamarVentana(next, "Ingresar Sanci�n");
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);        
    }
    
}
