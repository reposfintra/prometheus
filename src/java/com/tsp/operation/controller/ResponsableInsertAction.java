
/*
 * ResponsableInsertAction.java
 *
 * Created on 28 de octubre de 2005, 04:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class ResponsableInsertAction extends Action{
    
    /** Creates a new instance of ResponsableInsertAction */
    public ResponsableInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/responsable/responsableInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");   
        String dstrct = (String) session.getAttribute("Distrito");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        int sw=0;
        try{
            Responsable resp = new Responsable();
            resp.setDescripcion(descripcion);
            resp.setCodigo(codigo);
            resp.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            resp.setUsuario_creacion(usuario.getLogin().toUpperCase());
            resp.setBase(usuario.getBase().toUpperCase());
            try{
                model.responsableService.insertResponsable(resp);
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.responsableService.existResponsable(codigo)){
                    model.responsableService.updateResponsable(resp);                    
                    request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                }
                else{
                    request.setAttribute("mensaje","Error el Responsable ya existe!");
                }
            }
            else{
                request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);        
    }
    
}
