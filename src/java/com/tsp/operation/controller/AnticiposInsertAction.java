/*
 * AnticiposInsertAction.java
 *
 * Created on 20 de abril de 2005, 11:22 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class AnticiposInsertAction extends Action{
    
    /** Creates a new instance of AnticiposInsertAction */
    public AnticiposInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next ="/anticipos/anticipoInsert.jsp";
        String codant       = request.getParameter("codant").toUpperCase();
        String desant       = request.getParameter("descant").toUpperCase();
        String dist         = request.getParameter("distrito");
        String sj           = request.getParameter("sj");
        String tipo_des     = request.getParameter("tipo_des");
        float valor         = Float.parseFloat(request.getParameter("valor"));
        String moneda       = request.getParameter("moneda");
        String indicador    = request.getParameter("indicador");
        String migracion    = request.getParameter("migracion");
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        
        Anticipos ant = new Anticipos();
        ant.setAnticipo_code(codant);
        ant.setAnticipo_desc(desant);
        ant.setDstrct(dist);
        ant.setSj(sj);
        ant.setTipo_s(tipo_des);
        ant.setCreation_user(usuario.getLogin());
        ant.setCodmigra(migracion);
        ant.setValor(valor);
        ant.setMoneda(moneda);
        ant.setIndicador(indicador);
        
        request.setAttribute("error","ECE0D8" );
        request.setAttribute("error1","ECE0D8" );
        try{
            model.anticiposService.searchAnticipos(dist, codant,sj);
            if(model.anticiposService.getAnticipo()!=null){
                request.setAttribute("error","#cc0000" );
                next= "/anticipos/anticipoInsertError.jsp";
            }
            else{
                if(model.stdjobdetselService.estaStandardJob(sj)){
                    model.anticiposService.setAnticipos(ant);
                    model.anticiposService.insertAnticipo(usuario.getBase());
                }
                else{
                    if(sj.equals("")){
                        model.anticiposService.setAnticipos(ant);
                        model.anticiposService.insertAnticipo(usuario.getBase());
                    }
                    else{
                        request.setAttribute("error1","#cc0000" );
                        next= "/anticipos/anticipoInsertError.jsp";
                    }
                }
                
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
    
}
