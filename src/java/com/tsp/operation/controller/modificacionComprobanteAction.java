/*
 * modificacionComprobanteAction.java
 *
 * Created on abril de 2008
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author NAVI
 */
public class modificacionComprobanteAction  extends Action{
    
    /*String tipodocx;
    
    String modalidadcustodia;
    String modalidadremesa;*/
    
    /** Creates a new instance of creacionCompraCarteraAction */
    public modificacionComprobanteAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try{
            
            String respuesta ="";

            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String base =usuario.getBase();
            String opcion=request.getParameter("opcion");
            
            String doc=request.getParameter("documento");
            String tipodoc=request.getParameter("tipodoc");
            String proveedor=request.getParameter("proveedor");
            String fecha=request.getParameter("fecha");
            String periodo=request.getParameter("periodo");
            
            String tipond=request.getParameter("tipond");
            //String proveedor=request.getParameter("proveedor");
            
            String loginx=usuario.getLogin();
                    
            String grupo_transaccion=   request.getParameter("transaccion");
            
            String next="/jsp/finanzas/contab/cambiar_periodo_comprobante.jsp?doc="+doc+"&tipodoc="+tipodoc+"&proveedor="+proveedor+"&fecha="+fecha+"&periodo="+periodo+"&transaccion="+grupo_transaccion;

            //String agregacionCheque="";
            if (opcion.equals("modificarperiodo")){       
                respuesta=model.modificacionComprobantService.actualizarComprobante(doc, tipodoc, proveedor, fecha, periodo, loginx,grupo_transaccion,tipond);
                next=next+"&respuesta="+respuesta;
                //next="/jsp/fenalco/comprascartera/mostrar_detalles_compra_cartera.jsp?codigo_negocio="+request.getParameter("consultablex")+"";  
            }

            this.dispatchRequest(next); 
        }catch(Exception e){
            System.out.println("errorcillo en run de action"+e.toString()+"____"+e.getMessage());
            throw new InformationException("ERROR DURANTE action " + e.getMessage());            
        }
    }
}
