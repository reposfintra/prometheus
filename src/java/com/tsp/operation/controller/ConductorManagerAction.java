
package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;


public class ConductorManagerAction extends Action{
    
    public void run() throws ServletException, InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String login=usuario.getLogin();
            //String login = "fernel";
            String next="";
            if( !login.equals("") ){
                //--- variables de bloqueos
                String bloqueoInsert="";
                String bloqueoSearch="";
                String bloqueoUpdate="disabled='disabled'";
                String bloqueoDelete="disabled='disabled'";
                String botones="";
                String comentario="";
                
                //--- obtenemos variables
                String evento  = request.getParameter("evento");
                String id      = request.getParameter("identificacion");
                if(request.getParameter("consulta")==null){
                    //--- creamos los objetos a partir de las variables   pantalla
                    Conductor objConductor = Conductor.getObject(request, usuario );
                    Nit  objNit   = Nit.getObject(request);
                    
                    //--- eventos seleccionado
                    
                    if(evento.equals("INSERT") ){
                        model.ConductorSvc.searchConductor(objConductor.getCedula());
                        Conductor auxiliar = model.ConductorSvc.getConductor();
                        model.ConductorSvc.searchNit(objNit.getNit());
                        Nit auxiliar2=model.ConductorSvc.getNit();
                        int sw=0;
                        if(auxiliar==null || auxiliar2==null){
                            if(auxiliar2==null){
                                model.ConductorSvc.insertNit(objNit,login);
                                sw=1;
                            }
                            if(auxiliar==null){
                                model.ConductorSvc.searchNit(objConductor.getCedula());
                                Nit auxiliar3=model.ConductorSvc.getNit();
                                if(auxiliar3!=null){
                                    model.ConductorSvc.insertConductor(objConductor);
                                    sw=(sw==1)?2:3;
                                }
                                else
                                    sw=4;
                            }
                            switch(sw){
                                case 1: comentario="El registro se ha insertado unicamente en Nit, ya que existia en Conductores";     break;
                                case 2: comentario="El registro se ha insertado satisfactoriamente...";                                break;
                                case 3: comentario="El registro se ha insertado unicamente en Conductores, ya que existia en Nit...";  break;
                                case 4: comentario="El registro de conductor no se pudo insertar, debido a que no existe un nit asociado a dicha cedula";  break;
                            }
                            model.ConductorSvc.reset();
                        }
                        else{
                            model.ConductorSvc.setNit(objNit);
                            model.ConductorSvc.setConductor(objConductor);
                            comentario="El registro con esa identificaci�n y/o nit ya se encontraba registrado, por lo tanto su inserci�n NO fu� realizada, verifique sus datos...";
                        }
                    }
                    
                    
                    if(evento.equals("SEARCH") ){
                        if((id!=null && !id.equals(""))  ) {
                            model.ConductorSvc.searchConductor(id.trim());
                            model.ConductorSvc.searchNit(id.trim());
                        }
                        if(model.ConductorSvc.getConductor()!=null || model.ConductorSvc.getNit()!=null) {
                            bloqueoSearch="disabled='disabled'";
                            bloqueoInsert="disabled='disabled'";
                            botones="disabled";
                            bloqueoUpdate="";
                            bloqueoDelete="";
                        }
                        else
                            comentario="No se encontr� registro con dicha identificaci�n  establecida...";
                    }
                    
                    
                    if(evento.equals("UPDATE") ){
                        
                        model.ConductorSvc.searchConductor(objConductor.getCedula());
                        Conductor auxiliar = model.ConductorSvc.getConductor();
                        model.ConductorSvc.searchNit(objNit.getNit());
                        Nit auxiliar2=model.ConductorSvc.getNit();
                        int sw=0;
                        
                        if(auxiliar!=null){
                            model.ConductorSvc.updateConductor(objConductor);
                            sw=1;  //update conductor
                        }
                        
                        if(auxiliar2!=null){
                            model.ConductorSvc.updateNit(objNit,login);
                            switch(sw){
                                case 0:  sw=2;  break;  // update nit
                                case 1:  sw=3;  break;  // update conductor update nit
                            }
                        }
                        
                        if(auxiliar==null && !id.equals("") ){
                            model.ConductorSvc.searchNit(objConductor.getCedula());
                            Nit auxiliar3=model.ConductorSvc.getNit();
                            if(auxiliar3!=null){
                                model.ConductorSvc.insertConductor(objConductor);
                                if(sw==2) sw=4;
                            }
                            else
                                if(sw==2) sw=5;
                        }
                        
                        model.ConductorSvc.searchConductor(objConductor.getCedula());
                        model.ConductorSvc.searchNit(objNit.getNit());
                        
                        switch(sw){
                            case 1: comentario="El registro en Conductores ha sido actualizado correctamente";   break;
                            case 2: comentario="El registro en Nit ha sido actualizado correctamente";           break;
                            case 3: comentario="El registro ha sido actualizado correctamente";                  break;
                            case 4: comentario="El registro en Nit se ha Actualizado, mientras en conductores se ha insertado, debido a que no estaba registrado";   break;
                            case 5: comentario="El registro en Nit se ha Actualizado, mientras que el conductor no tiene registro en nit asociado, por tal motivo no se le realiz� ninguna operaci�n";   break;
                        }
                        
                        bloqueoInsert="disabled='disabled'";
                        bloqueoSearch="disabled='disabled'";
                        bloqueoUpdate="";
                        bloqueoDelete="";
                        botones="disabled";
                    }
                    
                    
                    
                    if(evento.equals("DELETE") ){
                        model.ConductorSvc.delete(id);
                        model.ConductorSvc.reset();
                        bloqueoInsert="";
                        bloqueoSearch="";
                        bloqueoUpdate="disabled='disabled'";
                        bloqueoDelete="disabled='disabled'";
                        comentario="El registro ha sido eliminado satisfactoriamente";
                    }
                     next = "/conductor/Conductor.jsp?estadoInsert="+ bloqueoInsert +"&estadoSearch="+bloqueoSearch+"&estadoDelete="+ bloqueoDelete+"&estadoUpdate="+bloqueoUpdate +"&botones="+botones+"&comentario="+comentario;
                }
                else{
                    
                    next = "/conductor/ConductorConsulta.jsp?estadoInsert="+ bloqueoInsert +"&estadoSearch="+bloqueoSearch+"&estadoDelete="+ bloqueoDelete+"&estadoUpdate="+bloqueoUpdate +"&botones="+botones+"&comentario="+comentario;
                    if(evento.equals("SEARCH") ){
                        if((id!=null && !id.equals(""))  ) {
                            model.ConductorSvc.searchConductor(id.trim());
                            model.ConductorSvc.searchNit(id.trim());
                        }
                        if(model.ConductorSvc.getConductor()!=null || model.ConductorSvc.getNit()!=null) {
                            bloqueoSearch="disabled='disabled'";
                            bloqueoInsert="disabled='disabled'";
                            botones="disabled";
                            bloqueoUpdate="disabled='disabled'";
                            bloqueoDelete="disabled='disabled'";
                        }
                        else
                            comentario="No se encontr� registro con dicha identificaci�n  establecida...";
                        
                    }
                }
                if(evento.equals("NEW") ){
                    model.ConductorSvc.reset();
                }
                
               
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            //throw new ServletException(e.getMessage());
            e.printStackTrace();
        }
    }
    
    
}