/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.impl.FacturaElectronicaImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;
import com.tsp.operation.model.DAOS.FacturaElectronicaDAO;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author HaroldC
 */
public class FacturaElectronicaAction extends Action {

    private final int LIGIN_TOKEN = 1;
    private final int GUARDAR_FILTRO = 2;

    private JsonObject respta = null;
    private JsonObject respuesta = null;
    private Usuario usuario;
    private FacturaElectronicaDAO fcDAO;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            
            usuario = (Usuario) request.getSession().getAttribute("Usuario");
            fcDAO = new FacturaElectronicaImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion")) : -1;
            switch (opcion) {
                
                case LIGIN_TOKEN:

                    respuesta = new JsonObject();
                    JsonObject joS = request.getParameter("form") != null ? (JsonObject) (new JsonParser()).parse(request.getParameter("form")) : null;
                    /*
                    joS.addProperty("usuario", usuario.getLogin());
                    String[] myJsonData2 = request.getParameterValues("listadodocs[]");
                    
                    respuesta.add("Prb", fcDAO.LoginToken(joS));
                    */
                    fcDAO.LoginToken(joS);
                    //respuesta.add("Prb", fcDAO.LoginToken(joS));
                    break;
                
                default:
                    throw new Exception("La opcion de orden " + opcion + " no valida");
            }
            
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
            exc.printStackTrace();
        } finally {
            try {
                response.setContentType("application/json; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println((new Gson()).toJson(respuesta));
            } catch (Exception e) {
            }
        }
    }
 
    
    //public JsonObject LoginToken(String _CadURL, String _XWho, String _User, String _Passwd, String _TendantId) {
    public String ObtenerToken(String _CadURL, String _XWho, String _User, String _Passwd, String _TendantId) {
        
        String inputLine;
        URL apiUrl = null;
        HttpURLConnection connection = null;
        
        JsonObject response = new JsonObject();
        String respuesta = "";
        
        try {

            apiUrl = new URL(_CadURL);
            connection = (HttpURLConnection) apiUrl.openConnection();
            connection.setRequestMethod("POST");                    
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Who", _XWho);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);

            JsonObject paramsObjJson = new JsonObject();
            paramsObjJson.addProperty("u", _User);
            paramsObjJson.addProperty("p", _Passwd);
            paramsObjJson.addProperty("t", _TendantId);

            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.writeBytes(new Gson().toJson(paramsObjJson));
            out.flush();
            out.close();

            int status = connection.getResponseCode();
            
            if (status == 200) {
                
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuffer content = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                response = (JsonObject) new JsonParser().parse(content.toString());
                respuesta = response.get("accessToken").getAsString();

                in.close();
                
                
            } else {
                throw new IOException("Response code " + status);
            }            

        } catch (Exception e) {
            //respuesta = new JsonObject();
            //respuesta.addProperty("error", e.getMessage());
            respuesta = "error: " + e.getMessage();
        } finally {
            return respuesta;
        }
    }
    
}
