/********************************************************************
 *      Nombre Clase.................   EscribirJSPAction.java    
 *      Descripci�n..................   Reescribe un archivp jsp, a�adiendole los metodos perfil-vista
 *                                      a los campos.
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   5.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class EscribirJSPAction extends Action{
        
        /** Creates a new instance of EscribirJSPAction */
        public EscribirJSPAction() {
        }
        
        public void run() throws javax.servlet.ServletException, InformationException {
                String next="/jsp/trafico/permisos/campos_jsp/Campos_jspListed.jsp?msg=ok";
                try{
                        
                        String cod = request.getParameter("codigo");
                        String pagina = request.getParameter("pagina");
                        model.jspService.serchJsp(cod);
                        Jsp jsp = model.jspService.getJsp();
                        String ruta = application.getRealPath("/") + jsp.getRuta();
                        //System.out.println("ruta---"+ruta+"----codigo--"+cod+"---pagina---"+pagina);
                        Vector campos = model.camposjsp.searchDetallecampos_jsps(cod);
                        int indice = campos.size();
                        for (int i = 0; i < indice; i++){
                                campos_jsp campo = (campos_jsp)campos.get(i);
                                model.camposjsp.archivojsp(ruta, jsp.getNombre(), campo.getCampo(), campo.getTipo_campo());
                        }
                                            
                }catch (SQLException e){
                        throw new ServletException("error"+e.getMessage());
                        
                }
                this.dispatchRequest(next);
        }
        
}
