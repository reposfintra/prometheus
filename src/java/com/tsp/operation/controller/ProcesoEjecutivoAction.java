/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ProcesoEjecutivoDAO;
import com.tsp.operation.model.DAOS.impl.ProcesoEjecutivoImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;
import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author mcastillo
 */
public class ProcesoEjecutivoAction extends Action {

    Usuario usuario = null;
    private final int CARGAR_GRILLA_ESTADOS_CARTERA = 0;
    private final int GUARDAR_ESTADO_CARTERA = 1;
    private final int ACTUALIZAR_ESTADO_CARTERA = 2;
    private final int ACTIVAR_INACTIVAR_ESTADO_CARTERA = 3;
    private final int CARGAR_UNIDADES_NEGOCIOS = 4;
    private final int CARGAR_ESTADOS_CARTERA_POR_UNIDAD = 5;
    private final int LISTAR_ESTADOS_CARTERA = 6;
    private final int ACTUALIZAR_CONF_ESTADOS_CARTERA_POR_UNIDAD = 7;
    private final int CARGAR_GRILLA_ETAPAS = 8;
    private final int GUARDAR_ETAPA = 9;
    private final int ACTUALIZAR_ETAPA = 10;
    private final int ACTIVAR_INACTIVAR_ETAPA = 11;
    private final int CARGAR_GRILLA_RESPUESTAS_ETAPA = 12;
    private final int GUARDAR_RESPUESTA_ETAPA = 13;
    private final int ACTUALIZAR_RESPUESTA_ETAPA = 14;
    private final int ACTIVAR_INACTIVAR_RESPUESTA_ETAPA = 15;
    private final int CARGAR_GRILLA_COSTOS_ETAPA = 16;
    private final int GUARDAR_COSTO_ETAPA = 17;
    private final int ACTUALIZAR_COSTO_ETAPA = 18;
    private final int ACTIVAR_INACTIVAR_COSTO_ETAPA = 19;
    private final int CARGAR_CBO_UNIDADES_NEGOCIOS = 20;
    private final int CARGAR_REPORTE_JURIDICA = 21;
    private final int CAMBIAR_ESTADO_PROCESO_JURIDICO = 22;
    private final int CARGAR_COSTOS_NEG_POR_ETAPA = 23;
    private final int CARGAR_COSTOS_POR_ETAPA = 24;
    private final int INSERTAR_COSTOS_NEG_ETAPA = 25;
    private final int ACTUALIZAR_COSTO_NEG_POR_ETAPA = 26;
    private final int ELIMINAR_COSTO_NEG_POR_ETAPA = 27;
    private final int CARGAR_CBO_RESPUESTAS_ETAPA = 28;
    private final int AGREGAR_RESPUESTA_PROC_TRAZ = 29;
    private final int CARGAR_CONFIG_DOCS_DEMANDA = 30;
    private final int GUARDAR_CONFIG_DOCS_DEMANDA = 31;
    private final int ASIGNAR_UNIDAD_PROCESO = 32;
    private final int DESASIGNAR_UNIDAD_PROCESO = 33;
    private final int EXISTE_DEMANDA_PARA_UND_NEGOCIO = 34;
    private final int CARGAR_GRILLA_TIPO_ACTORES = 35;
    private final int GUARDAR_TIPO_ACTOR = 36;
    private final int ACTUALIZAR_TIPO_ACTOR = 37;
    private final int ACTIVAR_INACTIVAR_TIPO_ACTOR = 38;
    private final int CARGAR_GRILLA_ACTORES = 39;
    private final int CARGAR_CBO_TIPO_ACTORES = 40;
    private final int CARGAR_CBO_TIPOS_IDENTIFICACION = 41;
    private final int GUARDAR_ACTOR = 42;
    private final int ACTUALIZAR_ACTOR = 43;
    private final int CARGAR_GRILLA_EQUIVALENCIAS = 44;
    private final int CARGAR_GRILLA_COSTOS_AUTORIZACION = 45;
    private final int CARGAR_CBO_ESTADOS_AUTORIZACION = 46;
    private final int AUTORIZAR_COSTOS_ETAPA = 47;
    private final int GENERAR_DOCUMENTOS_DEMANDA = 48;
    private final int EXISTE_DEMANDA_GENERADA = 49;
    private final int CARGAR_DOCUMENTOS_DEMANDA = 50;
    private final int GUARDAR_DOCUMENTOS_DEMANDA = 51;
    private final int GENERAR_PDF_DEMANDA = 52;
    private final int VER_PDF_DEMANDA_GENERADO = 53;
    private final int ACTUALIZAR_RESPUESTA_PROCESO = 54;
    private final int VER_TRAZA_CONFIG_ETAPA = 55;
    private final int VER_TRAZABILIDAD_PROCESO = 56;
    private final int EXPORTAR_EXCEL = 57;
    private final int EXPORTAR_EXCEL_CARTERA_VENCIDA = 58;
    private final int CARGAR_GRILLA_JUZGADOS = 59;
    private final int GUARDAR_JUZGADO = 60;
    private final int ACTUALIZAR_JUZGADO = 61;
    private final int ACTIVAR_INACTIVAR_JUZGADO = 62;
    private final int CARGAR_CBO_JUZGADOS = 63;
    private final int ACTUALIZAR_RADICADO_JUZGADO = 64;
    private final int GENERAR_ACTA_ENTREGA_PAGARES = 65;
    private final int CARGAR_CONDICIONES_ESPECIALES = 66;
    private final int CAMBIAR_CONFIG_CONDICIONES_ESPECIALES = 67;
    private final int ELIMINAR_CONFIG_CONDICIONES_ESPECIALES_POR_ITEM = 68;    

    private ProcesoEjecutivoDAO dao;
    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, letra1, letra2,letras, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ProcesoEjecutivoImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_GRILLA_ESTADOS_CARTERA:
                    cargarEstadosCartera();
                    break;
                case GUARDAR_ESTADO_CARTERA:
                    guardarEstadosCartera();
                    break;
                case ACTUALIZAR_ESTADO_CARTERA:
                    actualizarEstadosCartera();
                    break;
                case ACTIVAR_INACTIVAR_ESTADO_CARTERA:
                    activaInactivaEstadosCartera();
                    break;
                case CARGAR_UNIDADES_NEGOCIOS:
                    cargarUndNegocios();
                    break;
                case CARGAR_ESTADOS_CARTERA_POR_UNIDAD:
                    cargarEstadosCarteraxUndNegocio();
                    break;
                case LISTAR_ESTADOS_CARTERA:
                    listarEstadosCartera();
                    break;
                case ACTUALIZAR_CONF_ESTADOS_CARTERA_POR_UNIDAD:
                    actualizarConfigEstadosCartera();
                    break;
                case CARGAR_GRILLA_ETAPAS:
                    cargarEtapas();
                    break;
                case GUARDAR_ETAPA:
                    guardarEtapas();
                    break;
                case ACTUALIZAR_ETAPA:
                    actualizarEtapas();
                    break;
                case ACTIVAR_INACTIVAR_ETAPA:
                    activaInactivaEtapas();
                    break;
                case CARGAR_GRILLA_RESPUESTAS_ETAPA:
                    cargarRespuestasEtapa();
                    break;
                case GUARDAR_RESPUESTA_ETAPA:
                    guardarRespuestasEtapa();
                    break;
                case ACTUALIZAR_RESPUESTA_ETAPA:
                    actualizarRespuestasEtapa();
                    break;
                case ACTIVAR_INACTIVAR_RESPUESTA_ETAPA:
                    activaInactivaRespuestasEtapa();
                    break;
                case CARGAR_GRILLA_COSTOS_ETAPA:
                    cargarCostosEtapa();
                    break;
                case GUARDAR_COSTO_ETAPA:
                    guardarCostosEtapa();
                    break;
                case ACTUALIZAR_COSTO_ETAPA:
                    actualizarCostosEtapa();
                    break;
                case ACTIVAR_INACTIVAR_COSTO_ETAPA:
                    activaInactivaCostosEtapa();
                    break;
                case CARGAR_CBO_UNIDADES_NEGOCIOS:
                    cargarCboUndNegocio();
                    break;
                case CARGAR_REPORTE_JURIDICA:
                    listarReporteJuridica();
                    break;
                case CAMBIAR_ESTADO_PROCESO_JURIDICO:
                    cambiarEstadoProcesoJuridico();
                    break;
                case CARGAR_COSTOS_NEG_POR_ETAPA:
                    listarCostosNegocioXEtapa();
                    break;
                case CARGAR_COSTOS_POR_ETAPA:
                    listarCostosXEtapa();
                    break;
                case INSERTAR_COSTOS_NEG_ETAPA:
                    insertarCostosNegXEtapa();
                    break;
                case ACTUALIZAR_COSTO_NEG_POR_ETAPA:
                    actualizarCostosNegXEtapa();
                    break;
                case ELIMINAR_COSTO_NEG_POR_ETAPA:
                    eliminarCostosNegXEtapa();
                    break;
                case CARGAR_CBO_RESPUESTAS_ETAPA:
                    cargarCboRespuestasEtapa();
                    break;
                case AGREGAR_RESPUESTA_PROC_TRAZ:
                    break;
                case CARGAR_CONFIG_DOCS_DEMANDA:
                    cargarConfigDocsDemanda();
                    break;
                case GUARDAR_CONFIG_DOCS_DEMANDA:
                    guardarConfigDocsDemanda();
                    break;
                case ASIGNAR_UNIDAD_PROCESO:
                    asignarUndProcesoJuridico();
                    break;
                case DESASIGNAR_UNIDAD_PROCESO:
                    desasignarUndProcesoJuridico();
                    break;
                case EXISTE_DEMANDA_PARA_UND_NEGOCIO:
                    existenDemandasRelUnidad();
                    break;
                case CARGAR_GRILLA_TIPO_ACTORES:
                    cargarTiposActores();
                    break;
                case GUARDAR_TIPO_ACTOR:
                    this.guardarTipoActor();
                    break;
                case ACTUALIZAR_TIPO_ACTOR:
                    actualizarTipoActor();
                    break;
                case ACTIVAR_INACTIVAR_TIPO_ACTOR:
                    activaInactivaTipoActor();
                    break;
                case CARGAR_GRILLA_ACTORES:
                    cargarActores();
                    break;
                case CARGAR_CBO_TIPO_ACTORES:
                    cargarCboTipoActores();
                    break;
                case CARGAR_CBO_TIPOS_IDENTIFICACION:
                    cargarTiposIdentificacion();
                    break;
                case GUARDAR_ACTOR:
                    this.guardarActor();
                    break;
                case ACTUALIZAR_ACTOR:
                    this.actualizarActor();
                    break;
                case CARGAR_GRILLA_EQUIVALENCIAS:
                    this.cargarEquivalencias();
                    break;
                case CARGAR_GRILLA_COSTOS_AUTORIZACION:
                    this.cargarCostosAutorizacion();
                    break;
                case CARGAR_CBO_ESTADOS_AUTORIZACION:
                    cargarCboEstadosAutorizacion();
                    break;
                case AUTORIZAR_COSTOS_ETAPA:
                    cambiarEstadoAprobCosto();
                    break;
                case EXISTE_DEMANDA_GENERADA:
                    existeDemanda();
                    break;
                case GENERAR_DOCUMENTOS_DEMANDA:
                    crearDemanda();
                    break;
                case CARGAR_DOCUMENTOS_DEMANDA:
                    cargarDocumentosDemanda();
                    break;
                case GUARDAR_DOCUMENTOS_DEMANDA:
                    guardarDocsDemanda();
                    break;
                case GENERAR_PDF_DEMANDA:
                    generarPdfDocsDemanda();
                    break;
                case VER_PDF_DEMANDA_GENERADO:
                    almacenarArchivoEnCarpetaUsuario();
                    break;
                case ACTUALIZAR_RESPUESTA_PROCESO:
                    actualizaRespuestaProceso();
                    break;
                case VER_TRAZA_CONFIG_ETAPA:
                    listarTrazabilidadEtapa();
                    break;
                case VER_TRAZABILIDAD_PROCESO:
                    listarTrazabilidadProceso();
                    break;
                case EXPORTAR_EXCEL:
                    exportarExcel();
                    break;
                case EXPORTAR_EXCEL_CARTERA_VENCIDA:
                    exportarExcelCarteraVenc();
                    break;
                case CARGAR_GRILLA_JUZGADOS:
                    cargarJuzgados();
                    break;
                case GUARDAR_JUZGADO:
                    guardarJuzgado();
                    break;
                case ACTUALIZAR_JUZGADO:
                    actualizarJuzgado();
                    break;
                case ACTIVAR_INACTIVAR_JUZGADO:
                    activaInactivaJuzgados();
                    break;
                case CARGAR_CBO_JUZGADOS:
                    cargarCboJuzgados();
                    break;
                case ACTUALIZAR_RADICADO_JUZGADO:
                    actualizaRadicadoJuzgado();
                    break;
                case GENERAR_ACTA_ENTREGA_PAGARES:
                    generarActaEntrega();
                    break;
                case CARGAR_CONDICIONES_ESPECIALES:
                    cargarCondicionesEspeciales();
                    break;
                case CAMBIAR_CONFIG_CONDICIONES_ESPECIALES:
                    actualizarConfigCondicionesEspeciales();
                    break;
                case ELIMINAR_CONFIG_CONDICIONES_ESPECIALES_POR_ITEM:
                    eliminarConfigCondicionesEspeciales();
                    break;   
            }
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }

    }

    private void cargarEstadosCartera() {
        try {
            String json = dao.cargarEstadosCartera();
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarEstadosCartera() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String resp = "{}";
            if (!dao.existeEstadoCartera(nombre)) {
                resp = dao.guardarEstadoCartera(nombre, descripcion, usuario.getLogin(), usuario.getDstrct());
            } else {
                resp = "{\"error\":\" No se cre� el estado, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarEstadosCartera() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String idEstado = request.getParameter("idEstado");
            String resp = "{}";
            if (!dao.existeEstadoCartera(nombre, Integer.parseInt(idEstado))) {
                resp = dao.actualizarEstadoCartera(nombre, descripcion, idEstado, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se actualiz� el estado, puede que este ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaEstadosCartera() {
        try {
            String idEstado = request.getParameter("idEstado");
            String estado = request.getParameter("estadoCartera");
            String resp = dao.activaInactivaEstadoCartera(idEstado, estado, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarUndNegocios() {
        try {
            Usuario us = (Usuario) request.getSession().getAttribute("Usuario");
            String usuario = (us.getLogin() != null) ? us.getLogin() : "";
            String ref = request.getParameter("ref_1");
            ArrayList<UnidadesNegocio> listUndsNegocio = dao.cargarUndNegocios(ref);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listUndsNegocio) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void asignarUndProcesoJuridico() {
        try {
            int undNegocio = request.getParameter("und_negocio") != null ? Integer.parseInt(request.getParameter("und_negocio")) : 0;
            String ref = request.getParameter("ref_1");

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.asignarDesasignarUndProceso(undNegocio, ref));
            tService.getSt().addBatch(dao.insertarConfigIniEstadosCartera(undNegocio, 1, usuario.getLogin()));

            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void desasignarUndProcesoJuridico() {
        try {
            int undNegocio = request.getParameter("und_negocio") != null ? Integer.parseInt(request.getParameter("und_negocio")) : 0;
            String ref = request.getParameter("ref_1");

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.asignarDesasignarUndProceso(undNegocio, ref));
            tService.getSt().addBatch(dao.eliminarRelUndEstadoCartera(undNegocio));

            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void existenDemandasRelUnidad() {
        try {
            int undNegocio = request.getParameter("und_negocio") != null ? Integer.parseInt(request.getParameter("und_negocio")) : 0;
            String resp = "{}";
            if (dao.existenDemandasRelUndNegocio(undNegocio)) {
                resp = "{\"respuesta\":\"SI\"}";
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarEstadosCarteraxUndNegocio() {
        try {
            int undNegocio = request.getParameter("und_negocio") != null ? Integer.parseInt(request.getParameter("und_negocio")) : 0;
            ArrayList<BeanGeneral> listEstadosCartera = dao.cargarEstadosCarteraxUnd(undNegocio);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listEstadosCartera) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarEstadosCartera() {
        try {

            Gson gson = new Gson();
            String json;
            json = gson.toJson(dao.listarEstadosCartera());
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void actualizarConfigEstadosCartera() {
        try {
            int id_und_negocio = request.getParameter("unidad_negocio") != null ? Integer.parseInt(request.getParameter("unidad_negocio")) : 0;
            int id_intervalo_mora = request.getParameter("intervalo_mora") != null ? Integer.parseInt(request.getParameter("intervalo_mora")) : 0;
            int id_estado_cartera = request.getParameter("estado_cartera") != null ? Integer.parseInt(request.getParameter("estado_cartera")) : 0;

            String resp = dao.actualizarConfigEstadosCartera(id_und_negocio, id_intervalo_mora, id_estado_cartera, usuario.getLogin());

            this.printlnResponse(resp, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cargarEtapas() {
        try {
            boolean swEtapa = (request.getParameter("soloActivas").equals("N")) ? true : false;
            String json = dao.cargarEtapas(swEtapa);
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarEtapas() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            int max_dias = (request.getParameter("estimado_dias")) != null ? Integer.parseInt(request.getParameter("estimado_dias")) : 0;
            String resp = "{}";
            if (!dao.existeEtapa(nombre, 0)) {
                resp = dao.guardarEtapa(nombre, descripcion, max_dias, usuario.getLogin(), usuario.getDstrct());
            } else {
                resp = "{\"error\":\" No se pudo crear la etapa, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarEtapas() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            int max_dias = (request.getParameter("estimado_dias")) != null ? Integer.parseInt(request.getParameter("estimado_dias")) : 0;
            String idEtapa = request.getParameter("idEtapa");
            String resp = "{}";
            if (!dao.existeEtapa(nombre, Integer.parseInt(idEtapa))) {
                resp = dao.actualizarEtapa(nombre, descripcion, max_dias, idEtapa, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se actualiz� la etapa, puede que esta ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaEtapas() {
        try {
            String idEtapa = request.getParameter("idEtapa");
            String estado = request.getParameter("estadoEtapa");
            String resp = dao.activaInactivaEtapa(idEtapa, estado, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarRespuestasEtapa() {
        try {
            String idEtapa = request.getParameter("idEtapa");
            String json = dao.cargarRespuestasEtapa(Integer.parseInt(idEtapa));
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarRespuestasEtapa() {
        try {
            String idEtapa = request.getParameter("idEtapa");
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            int max_dias = (request.getParameter("estimado_dias")) != null ? Integer.parseInt(request.getParameter("estimado_dias")) : 0;
            int secuencia = Integer.parseInt(request.getParameter("secuencia"));
            String finaliza_proceso = request.getParameter("finaliza_proceso");
            String resp = "{}";
            resp = dao.guardarRespuestaEtapa(Integer.parseInt(idEtapa), nombre, descripcion, max_dias, secuencia, finaliza_proceso, usuario.getLogin(), usuario.getDstrct());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarRespuestasEtapa() {
        try {
            int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
            int idEtapa = (request.getParameter("idEtapa") != null) ? Integer.parseInt(request.getParameter("idEtapa")) : 0;
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            int max_dias = (request.getParameter("estimado_dias") != null) ? Integer.parseInt(request.getParameter("estimado_dias")) : 0;
            int secuencia = Integer.parseInt(request.getParameter("secuencia"));
            String finaliza_proceso = request.getParameter("finaliza_proceso");
            String resp = "{}";
            resp = dao.actualizarRespuestaEtapa(id, idEtapa, nombre, descripcion, max_dias, secuencia, finaliza_proceso, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaRespuestasEtapa() {
        try {
            String id = request.getParameter("id");
            String estado = request.getParameter("estadoRespuesta");
            String resp = dao.activaInactivaRespuestaEtapa(id, estado, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCostosEtapa() {
        try {
            String idEtapa = request.getParameter("idEtapa");
            String json = dao.cargarCostosEtapa(Integer.parseInt(idEtapa));
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarCostosEtapa() {
        try {
            String idEtapa = request.getParameter("idEtapa");
            String concepto = request.getParameter("concepto");
            String tipo = request.getParameter("tipo");
            String valor = request.getParameter("valor");
            String solo_automotor = request.getParameter("solo_automotor");
            String resp = "{}";
            resp = dao.guardarCostoEtapa(Integer.parseInt(idEtapa), concepto, tipo, Double.parseDouble(valor), solo_automotor, usuario.getLogin(), usuario.getDstrct());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarCostosEtapa() {
        try {
            int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
            int idEtapa = (request.getParameter("idEtapa") != null) ? Integer.parseInt(request.getParameter("idEtapa")) : 0;
            String concepto = request.getParameter("concepto");
            String tipo = request.getParameter("tipo");
            String valor = request.getParameter("valor");
            String solo_automotor = request.getParameter("solo_automotor");
            String resp = "{}";
            resp = dao.actualizarCostoEtapa(id, idEtapa, concepto, tipo, Double.parseDouble(valor), solo_automotor, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaCostosEtapa() {
        try {
            String id = request.getParameter("id");
            String estado = request.getParameter("estadoCosto");
            String resp = dao.activaInactivaCostoEtapa(id, estado, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCboUndNegocio() {
        try {
            String ref = request.getParameter("ref_1");
            ArrayList<UnidadesNegocio> listUndsNegocio = dao.cargarUndNegocios(ref);
            Gson gson = new Gson();
            String json;
            json = gson.toJson(listUndsNegocio);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarReporteJuridica() {
        try {
            int undNegocio = request.getParameter("und_negocio") != null ? Integer.parseInt(request.getParameter("und_negocio")) : 0;
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            String cedula = request.getParameter("cedula") != null ? request.getParameter("cedula") : "";
            String etapa = request.getParameter("idetapa") != null ? request.getParameter("idetapa") : "0";
            String json = dao.cargarReporteJuridica(etapa, undNegocio, negocio.toUpperCase(), cedula);
            this.printlnResponse(json, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cambiarEstadoProcesoJuridico() {
        String idEtapa = request.getParameter("idetapa");
        String idEtapaNew = request.getParameter("idetapaNew");
        String actualiza_fecha = (request.getParameter("actualizar_fecha") != null) ? request.getParameter("actualizar_fecha") : "N";
        String comentario = (request.getParameter("comentario") != null) ? request.getParameter("comentario") : "";
        String listado[] = request.getParameter("listado").split(",");

        int idrespuesta = (Integer.parseInt(idEtapaNew) < Integer.parseInt(idEtapa)) ? 0 : 99;
        String respuesta = (Integer.parseInt(idEtapaNew) < Integer.parseInt(idEtapa)) ? "DEVUELTO A ETAPA " + idEtapaNew : "PASA A ETAPA " + idEtapaNew;

        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < listado.length; i++) {
                tService.getSt().addBatch(dao.actualizarEtapaNegocio(listado[i], idEtapaNew, actualiza_fecha, usuario.getLogin()));
                tService.getSt().addBatch(dao.insertarRespuestaTrazabilidad(Integer.parseInt(idEtapa), listado[i], idrespuesta, respuesta, comentario, usuario.getLogin(), usuario.getDstrct()));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarCostosNegocioXEtapa() {
        try {
            String etapa = request.getParameter("idetapa") != null ? request.getParameter("idetapa") : "";
            String negocio = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            String json = dao.cargarCostosNegocioXEtapa(etapa, negocio);
            this.printlnResponse(json, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarCostosXEtapa() {
        try {
            String etapa = request.getParameter("idEtapa") != null ? request.getParameter("idEtapa") : "";
            String IsAutomotor = request.getParameter("is_automotor") != null ? request.getParameter("is_automotor") : "N";
            String json = dao.cargarCostosXEtapa(etapa, IsAutomotor);
            this.printlnResponse(json, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void insertarCostosNegXEtapa() {
        String listadoNegocios = request.getParameter("listadoNegocios");
        String listadoCostos = request.getParameter("listadoCostos");

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jneg = (JsonObject) jsonParser.parse(listadoNegocios);
            JsonArray jsonArrNeg = jneg.getAsJsonArray("negocios");

            JsonObject jo = (JsonObject) jsonParser.parse(listadoCostos);
            JsonArray jsonArr = jo.getAsJsonArray("costos");

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < jsonArrNeg.size(); i++) {
                String negocio = jsonArrNeg.get(i).getAsJsonObject().get("cod_neg").getAsJsonPrimitive().getAsString();
                double valor_saldo = jsonArrNeg.get(i).getAsJsonObject().get("valor_saldo").getAsJsonPrimitive().getAsDouble();
                for (int j = 0; j < jsonArr.size(); j++) {
                    int idEtapa = jsonArr.get(j).getAsJsonObject().get("id_etapa").getAsJsonPrimitive().getAsInt();
                    int idCosto = jsonArr.get(j).getAsJsonObject().get("id_costo").getAsJsonPrimitive().getAsInt();
                    String tipo = jsonArr.get(j).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                    double valor = jsonArr.get(j).getAsJsonObject().get("valor").getAsJsonPrimitive().getAsDouble();
                    tService.getSt().addBatch(dao.insertarRelCostoNegXEtapa(idEtapa, negocio, valor_saldo, idCosto, tipo, valor, usuario.getLogin(), usuario.getDstrct()));
                }
            }

            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarCostosNegXEtapa() {
        try {
            int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
            String valor = request.getParameter("valor");
            String resp = "{}";
            resp = dao.actualizarRelCostoNegXEtapa(id, Double.parseDouble(valor), usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void eliminarCostosNegXEtapa() {
        try {
            int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
            String resp = dao.eliminarRelCostoNegXEtapa(id);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCboRespuestasEtapa() {
        try {
            String idEtapa = request.getParameter("idEtapa");
            String json = dao.cargarCboRespuestasEtapa(Integer.parseInt(idEtapa));
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cargarConfigDocsDemanda() {
        try {
            //int tipo_doc = Integer.parseInt(request.getParameter("tipo_doc"));
            String json = dao.cargarConfigDocs();
            this.printlnResponse(json, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cargarTiposActores() {
        try {
            String json = dao.cargarTipoActores();
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarTipoActor() {
        try {
            String tipo = request.getParameter("tipo");
            String resp = "{}";
            if (!dao.existeTipoActor(tipo)) {
                resp = dao.guardarTipoActor(tipo, usuario.getLogin(), usuario.getDstrct());
            } else {
                resp = "{\"error\":\" No se cre� el tipo, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarTipoActor() {
        try {
            String tipo = request.getParameter("tipo");
            String idTipo = request.getParameter("idTipo");
            String resp = "{}";
            if (!dao.existeTipoActor(tipo, Integer.parseInt(idTipo))) {
                resp = dao.actualizarTipoActor(Integer.parseInt(idTipo), tipo, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se actualiz� el tipo, puede que este ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaTipoActor() {
        try {
            String idTipo = request.getParameter("idTipo");
            String estado = request.getParameter("estadoTipoActor");
            String resp = dao.activaInactivaTipoActor(idTipo, estado, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarActores() {
        try {
            String json = dao.cargarActores();
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCboTipoActores() {
        try {
            String json = dao.cargarCboTipoActores();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cargarTiposIdentificacion() {
        try {
            String json = dao.cargarTiposIdentificacion();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void guardarActor() {
        try {
            String tipoActor = request.getParameter("tipo_actor");
            String tipoDoc = request.getParameter("tipo_doc");
            String documento = request.getParameter("documento");
            String nombre = request.getParameter("nombre");
            String direccion = request.getParameter("direccion");
            String email = request.getParameter("email");
            String celular = request.getParameter("celular");
            String telefono = request.getParameter("telefono");
            String extension = request.getParameter("ext");
            String pais = request.getParameter("pais");
            String departamento = request.getParameter("departamento");
            String ciudad = request.getParameter("ciudad");
            String tarj_prof = request.getParameter("tarjeta_prof");
            String lugarExpCed = request.getParameter("lugar_expedicion");
            String reg_status = request.getParameter("reg_status");

            String resp = "{}";

            resp = dao.guardarActor(Integer.parseInt(tipoActor), tipoDoc, documento, nombre, ciudad, departamento, pais, direccion, telefono, extension, celular, email, tarj_prof, lugarExpCed, reg_status, usuario.getLogin(), usuario.getDstrct());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarActor() {
        try {
            String tipoActor = request.getParameter("tipo_actor");
            String tipoDoc = request.getParameter("tipo_doc");
            String documento = request.getParameter("documento");
            String nombre = request.getParameter("nombre");
            String direccion = request.getParameter("direccion");
            String email = request.getParameter("email");
            String celular = request.getParameter("celular");
            String telefono = request.getParameter("telefono");
            String extension = request.getParameter("ext");
            String pais = request.getParameter("pais");
            String departamento = request.getParameter("departamento");
            String ciudad = request.getParameter("ciudad");
            String tarj_prof = request.getParameter("tarjeta_prof");
            String lugarExpCed = request.getParameter("lugar_expedicion");
            String reg_status = request.getParameter("reg_status");
            int id = Integer.parseInt(request.getParameter("id"));
            String resp = "{}";

            resp = dao.actualizarActor(id, Integer.parseInt(tipoActor), tipoDoc, documento, nombre, ciudad, departamento, pais, direccion, telefono, extension, celular, email, tarj_prof, lugarExpCed, reg_status, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarEquivalencias() {
        try {
            String json = dao.cargarEquivalencias();
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarConfigDocsDemanda() {
        try {

            String listadoDocs = request.getParameter("listadoDocs");

            JsonParser jsonParser = new JsonParser();
            JsonObject jdocs = (JsonObject) jsonParser.parse(listadoDocs);
            JsonArray jsonArrDocs = jdocs.getAsJsonArray("docs");

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            for (int i = 0; i < jsonArrDocs.size(); i++) {
                int tipo_doc = jsonArrDocs.get(i).getAsJsonObject().get("tipo_doc").getAsJsonPrimitive().getAsInt();
                String header_info = jsonArrDocs.get(i).getAsJsonObject().get("header_info").getAsJsonPrimitive().getAsString();
                String initial_info = jsonArrDocs.get(i).getAsJsonObject().get("initial_info").getAsJsonPrimitive().getAsString();
                String footer_info = jsonArrDocs.get(i).getAsJsonObject().get("footer_info").getAsJsonPrimitive().getAsString();
                String signing_info = jsonArrDocs.get(i).getAsJsonObject().get("signing_info").getAsJsonPrimitive().getAsString();
                String footer_page = jsonArrDocs.get(i).getAsJsonObject().get("footer_page").getAsJsonPrimitive().getAsString();
                String aux_1 = jsonArrDocs.get(i).getAsJsonObject().get("aux_1").getAsJsonPrimitive().getAsString();
                String aux_2 = jsonArrDocs.get(i).getAsJsonObject().get("aux_2").getAsJsonPrimitive().getAsString();
                String aux_3 = jsonArrDocs.get(i).getAsJsonObject().get("aux_3").getAsJsonPrimitive().getAsString();
                String aux_4 = jsonArrDocs.get(i).getAsJsonObject().get("aux_4").getAsJsonPrimitive().getAsString();
                String aux_5 = jsonArrDocs.get(i).getAsJsonObject().get("aux_5").getAsJsonPrimitive().getAsString();
                tService.getSt().addBatch(dao.guardarConfigDocs(tipo_doc, header_info, initial_info, footer_info, signing_info, footer_page, aux_1, aux_2, aux_3, aux_4, aux_5, usuario.getLogin(), usuario.getDstrct()));
                //Eliminamos detalle configuraci�n documentos demanda
                tService.getSt().addBatch(dao.eliminarConfigDocsDet(tipo_doc));
                if (tipo_doc == 1) {
                    JsonArray jsonArrHechos = jsonArrDocs.get(i).getAsJsonObject().get("hechos").getAsJsonArray();
                    for (int j = 0; j < jsonArrHechos.size(); j++) {
                        String id_det_doc = jsonArrHechos.get(j).getAsJsonObject().get("id_det_doc").getAsJsonPrimitive().getAsString();
                        String tipo = jsonArrHechos.get(j).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                        String titulo = jsonArrHechos.get(j).getAsJsonObject().get("titulo").getAsJsonPrimitive().getAsString();
                        String nombre_ = jsonArrHechos.get(j).getAsJsonObject().get("nombre").getAsJsonPrimitive().getAsString();
                        String descripcion = jsonArrHechos.get(j).getAsJsonObject().get("descripcion").getAsJsonPrimitive().getAsString();
                        tService.getSt().addBatch(dao.guardarConfigDocsDet(tipo_doc, tipo, titulo, nombre_, descripcion, usuario.getLogin(), usuario.getDstrct()));
                    }
                    JsonArray jsonArrPret = jsonArrDocs.get(i).getAsJsonObject().get("pretensiones").getAsJsonArray();
                    for (int k = 0; k < jsonArrPret.size(); k++) {
                        String id_det_doc = jsonArrPret.get(k).getAsJsonObject().get("id_det_doc").getAsJsonPrimitive().getAsString();
                        String tipo = jsonArrPret.get(k).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                        String titulo = jsonArrPret.get(k).getAsJsonObject().get("titulo").getAsJsonPrimitive().getAsString();
                        String nombre_ = jsonArrPret.get(k).getAsJsonObject().get("nombre").getAsJsonPrimitive().getAsString();
                        String descripcion = jsonArrPret.get(k).getAsJsonObject().get("descripcion").getAsJsonPrimitive().getAsString();
                        tService.getSt().addBatch(dao.guardarConfigDocsDet(tipo_doc, tipo, titulo,  nombre_, descripcion, usuario.getLogin(), usuario.getDstrct()));
                    }
                } else if (tipo_doc == 2) {
                    JsonArray jsonArrMed = jsonArrDocs.get(i).getAsJsonObject().get("medidas").getAsJsonArray();
                    for (int k = 0; k < jsonArrMed.size(); k++) {
                        String id_det_doc = jsonArrMed.get(k).getAsJsonObject().get("id_det_doc").getAsJsonPrimitive().getAsString();
                        String tipo = jsonArrMed.get(k).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                        String titulo = jsonArrMed.get(k).getAsJsonObject().get("titulo").getAsJsonPrimitive().getAsString();
                        String nombre_ = jsonArrMed.get(k).getAsJsonObject().get("nombre").getAsJsonPrimitive().getAsString();
                        String descripcion = jsonArrMed.get(k).getAsJsonObject().get("descripcion").getAsJsonPrimitive().getAsString();
                        tService.getSt().addBatch(dao.guardarConfigDocsDet(tipo_doc, tipo, titulo,  nombre_, descripcion, usuario.getLogin(), usuario.getDstrct()));
                    }
                }
            }

            tService.execute();
            tService.closeAll();

            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void existeDemanda() {
        try {
            String negocio = request.getParameter("cod_neg");
            boolean valida_pdf = (request.getParameter("validar_pdf").equals("SI")) ? true : false;
            String resp = "{}";
            if (dao.existeDemandaDocs(negocio, valida_pdf)) {
                resp = "{\"respuesta\":\"SI\"}";
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void crearDemanda() throws Exception {

        String listadoNegocios = request.getParameter("listadoNegocios");
        String resp = "{}";
        JsonParser jsonParser = new JsonParser();
        JsonObject jneg = (JsonObject) jsonParser.parse(listadoNegocios);
        JsonArray jsonArrNeg = jneg.getAsJsonArray("negocios");
        resp = dao.crearDemanda(jsonArrNeg, usuario);
        this.printlnResponse(resp, "application/json;");
        

    }

    private void cargarDocumentosDemanda() {
        try {
            String negocio = request.getParameter("cod_neg");
            String json = dao.cargarDemandaDocs(negocio);
            this.printlnResponse(json, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void guardarDocsDemanda() {
        try {
            String demanda = request.getParameter("id_demanda");
            String listadoDocs = request.getParameter("listadoDocs");

            JsonParser jsonParser = new JsonParser();
            JsonObject jdocs = (JsonObject) jsonParser.parse(listadoDocs);
            JsonArray jsonArrDocs = jdocs.getAsJsonArray("docs");

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < jsonArrDocs.size(); i++) {
                int id_doc = jsonArrDocs.get(i).getAsJsonObject().get("id_doc").getAsJsonPrimitive().getAsInt();
                int tipo_doc = jsonArrDocs.get(i).getAsJsonObject().get("tipo_doc").getAsJsonPrimitive().getAsInt();
                String header_info = jsonArrDocs.get(i).getAsJsonObject().get("header_info").getAsJsonPrimitive().getAsString();
                String initial_info = jsonArrDocs.get(i).getAsJsonObject().get("initial_info").getAsJsonPrimitive().getAsString();
                String footer_info = jsonArrDocs.get(i).getAsJsonObject().get("footer_info").getAsJsonPrimitive().getAsString();
                String signing_info = jsonArrDocs.get(i).getAsJsonObject().get("signing_info").getAsJsonPrimitive().getAsString();
                String footer_page = jsonArrDocs.get(i).getAsJsonObject().get("footer_page").getAsJsonPrimitive().getAsString();
                String aux_1 = jsonArrDocs.get(i).getAsJsonObject().get("aux_1").getAsJsonPrimitive().getAsString();
                String aux_2 = jsonArrDocs.get(i).getAsJsonObject().get("aux_2").getAsJsonPrimitive().getAsString();
                String aux_3 = jsonArrDocs.get(i).getAsJsonObject().get("aux_3").getAsJsonPrimitive().getAsString();
                String aux_4 = jsonArrDocs.get(i).getAsJsonObject().get("aux_4").getAsJsonPrimitive().getAsString();
                String aux_5 = jsonArrDocs.get(i).getAsJsonObject().get("aux_5").getAsJsonPrimitive().getAsString();
                tService.getSt().addBatch(dao.guardarDemandaDocs(id_doc, Integer.parseInt(demanda), tipo_doc, header_info, initial_info, footer_info, signing_info, footer_page, aux_1, aux_2, aux_3, aux_4, aux_5, usuario.getLogin(), usuario.getDstrct()));
                if (tipo_doc == 1) {
                    JsonArray jsonArrHechos = jsonArrDocs.get(i).getAsJsonObject().get("hechos").getAsJsonArray();
                    //Eliminamos hechos asociados a la demanda
                    tService.getSt().addBatch(dao.eliminarDemandaDocsDet(id_doc, "H"));
                    for (int j = 0; j < jsonArrHechos.size(); j++) {
                        String id_det_doc = jsonArrHechos.get(j).getAsJsonObject().get("id_det_doc").getAsJsonPrimitive().getAsString();
                        String tipo = jsonArrHechos.get(j).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                        String titulo = jsonArrHechos.get(j).getAsJsonObject().get("titulo").getAsJsonPrimitive().getAsString();
                        String descripcion = jsonArrHechos.get(j).getAsJsonObject().get("descripcion").getAsJsonPrimitive().getAsString();
                        tService.getSt().addBatch(dao.guardarDemandaDocsDet(id_det_doc, id_doc, tipo, titulo, descripcion, usuario.getLogin(), usuario.getDstrct()));
                    }
                    JsonArray jsonArrPret = jsonArrDocs.get(i).getAsJsonObject().get("pretensiones").getAsJsonArray();
                    //Eliminamos pretensiones asociadas a la demanda
                    tService.getSt().addBatch(dao.eliminarDemandaDocsDet(id_doc, "P"));
                    for (int k = 0; k < jsonArrPret.size(); k++) {
                        String id_det_doc = jsonArrPret.get(k).getAsJsonObject().get("id_det_doc").getAsJsonPrimitive().getAsString();
                        String tipo = jsonArrPret.get(k).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                        String titulo = jsonArrPret.get(k).getAsJsonObject().get("titulo").getAsJsonPrimitive().getAsString();
                        String descripcion = jsonArrPret.get(k).getAsJsonObject().get("descripcion").getAsJsonPrimitive().getAsString();
                        tService.getSt().addBatch(dao.guardarDemandaDocsDet(id_det_doc, id_doc, tipo, titulo, descripcion, usuario.getLogin(), usuario.getDstrct()));
                    }
                } else if (tipo_doc == 2) {
                    JsonArray jsonArrMed = jsonArrDocs.get(i).getAsJsonObject().get("medidas").getAsJsonArray();
                    //Eliminamos medidas asociadas a la demanda
                    tService.getSt().addBatch(dao.eliminarDemandaDocsDet(id_doc, "M"));
                    for (int m = 0; m < jsonArrMed.size(); m++) {
                        String id_det_doc = jsonArrMed.get(m).getAsJsonObject().get("id_det_doc").getAsJsonPrimitive().getAsString();
                        String tipo = jsonArrMed.get(m).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                        String titulo = jsonArrMed.get(m).getAsJsonObject().get("titulo").getAsJsonPrimitive().getAsString();
                        String descripcion = jsonArrMed.get(m).getAsJsonObject().get("descripcion").getAsJsonPrimitive().getAsString();
                        tService.getSt().addBatch(dao.guardarDemandaDocsDet(id_det_doc, id_doc, tipo, titulo, descripcion, usuario.getLogin(), usuario.getDstrct()));
                    }
                }
            }

            tService.execute();
            tService.closeAll();
            
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generarPdfDocsDemanda() {
        try {
            String demanda = request.getParameter("id_demanda");
            String negocio = request.getParameter("cod_neg");
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioArchivos = rb.getString("rutaImagenes") + "juridico/";//se establece la ruta de la imagen
            this.createDir(directorioArchivos);
            String ruta = directorioArchivos + negocio;
            this.createDir(ruta);
            String resp = "";
            if (dao.generarDemandaDocs(Integer.parseInt(demanda), ruta, usuario.getLogin(), usuario.getDstrct())){
                resp = dao.actualizaCampoPdfGenerado(demanda, "S", usuario.getLogin());
            }else{
                resp = "{\"respuesta\":\"Ocurri� un error al generar pdf\"}"; 
            } 
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizaRespuestaProceso() {
        try {
            String idEtapa = request.getParameter("id_etapa");
            String negocio = request.getParameter("cod_neg");
            String demanda = request.getParameter("id_demanda");
            String idrespuesta = request.getParameter("idrespuesta");
            String respuesta = request.getParameter("respuesta");
            String comentario = request.getParameter("comentario");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.actualizaRespuestaDemanda(demanda, Integer.parseInt(idrespuesta), usuario.getLogin()));
            tService.getSt().addBatch(dao.insertarRespuestaTrazabilidad(Integer.parseInt(idEtapa), negocio, Integer.parseInt(idrespuesta), respuesta, comentario, usuario.getLogin(), usuario.getDstrct()));
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCostosAutorizacion() {
        try {
            String json = dao.cargarCostosAutorizacion();
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCboEstadosAutorizacion() {
        try {
            String json = dao.cargarCboAutorizacionCostos();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cambiarEstadoAprobCosto() {
        String listado[] = request.getParameter("listado").split(",");
        String estado = request.getParameter("estadoAprob");

        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < listado.length; i++) {
                tService.getSt().addBatch(dao.actualizaAprobCostoEtapa(Integer.parseInt(listado[i]), estado, usuario.getLogin(), usuario.getDstrct()));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarTrazabilidadEtapa() {
        try {
            String idEtapa = request.getParameter("id_etapa") != null ? request.getParameter("id_etapa") : "0";
            String json = dao.listarEtapaConfigTraz(idEtapa);
            this.printlnResponse(json, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarTrazabilidadProceso() {
        try {
            String negocio = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            String json = dao.listarProcesoTrazabilidad(negocio);
            this.printlnResponse(json, "application/json;");
        } catch (SQLException ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void almacenarArchivoEnCarpetaUsuario() throws Exception {
        String respuesta = "";
        try {
            String negocio = request.getParameter("cod_neg");
            String nomarchivo = "demanda_" + ((request.getParameter("id_demanda") != null) ? request.getParameter("id_demanda") : "") + ".pdf";

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "juridico/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";

            boolean archivoCargado = dao.copiarArchivo(negocio, ("" + rutaOrigen), ("" + rutaDestino), nomarchivo);
            if (archivoCargado) {
                respuesta = "{\"respuesta\":\"SI\",\"Ruta\":\"" + "/images/multiservicios/" + usuario.getLogin() + "/" + nomarchivo + "\"}";
                this.printlnResponse(respuesta, "application/json;");
            } else {
                respuesta = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(respuesta, "application/json;");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void exportarExcel() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("ProcesoDemanda_", "Reporte");
            String[] cabecera = {"Und Negocio", "Negocio", "Altura mora", "F. inicio", "Dias transc.", "Cedula", "Nombre Cliente", "Ciudad", "Direcci�n", "Barrio", "Tel�fono",
                "Celular", "Email", "Pagar�", "Afiliado comercial", "Valor negocio", "Valor saldo"};

            short[] dimensiones = new short[]{
                5000, 5000, 5000, 5000, 5000, 7000, 8000, 5000, 7000, 5000, 3000, 5000, 5000, 5000, 7000, 5000, 5000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                letras = letra;
                if ((objects.get("id_demanda").getAsString().equals("0")) || (objects.get("docs_generados").getAsString().equals("N"))) {
                    letras = letra2;
                }else if (objects.get("id_juzgado").getAsString().equals("0") || objects.get("radicado").getAsString().equals("")) {
                    letras = letra1;
                }          

                xls.adicionarCelda(fila, col++, objects.get("und_negocio").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("negocio").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("mora").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("fecha_inicio").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("dias_transcurridos").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("cedula").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("nombre").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("ciudad").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("direccion").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("barrio").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("telefono").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("celular").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("email").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("num_pagare").getAsString(), letras);
                xls.adicionarCelda(fila, col++, objects.get("niter").getAsString(), letras);
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("vr_negocio").getAsString()), letras);
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_saldo").getAsString()), letras);
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ProcesoDemanda_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void exportarExcelCarteraVenc() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("ProcesoDemanda_", "Reporte");
            String[] cabecera = {"Negocio", "Altura mora", "F. marcaci�n", "Cedula", "Nombre Cliente", "Und Negocio", "Convenio", "Pagar�",
                "Afiliado comercial", "Valor negocio", "Valor saldo", "Estado cartera"};

            short[] dimensiones = new short[]{
                5000, 5000, 5000, 5000, 5000, 7000, 8000, 5000, 7000, 5000, 3000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("mora").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_marcacion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cedula").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("und_negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("convenio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("num_pagare").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("niter").getAsString(), letra);
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("vr_negocio").getAsString()), letra);
                xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_saldo").getAsString()), letra);
                xls.adicionarCelda(fila, col++, objects.get("estado_cartera").getAsString(), letra);
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ProcesoDemanda_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {
        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            // xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE, 1);
            letra1 = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, HSSFColor.LIGHT_YELLOW.index, xls.NONE, 1);
            letra2 = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, HSSFColor.CORAL.index, xls.NONE, 1);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    private void cargarJuzgados() {
        try {
            String json = dao.cargarJuzgados();
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarJuzgado() {
        try {
            String nombre = request.getParameter("nombre");
            String resp = "{}";
            if (!dao.existeJuzgado(nombre)) {
                resp = dao.guardarJuzgado(nombre, usuario.getLogin(), usuario.getDstrct());
            } else {
                resp = "{\"error\":\" No se cre� el juzgado, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarJuzgado() {
        try {
            String nombre = request.getParameter("nombre");
            String idJuzgado = request.getParameter("idJuzgado");
            String resp = "{}";
            if (!dao.existeJuzgado(nombre, Integer.parseInt(idJuzgado))) {
                resp = dao.actualizarJuzgado(nombre, idJuzgado, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se actualiz� el juzgado, puede que este ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaJuzgados() {
        try {
            String idJuzgado = request.getParameter("idJuzgado");
            String estado = request.getParameter("estadoJuzgado");
            String resp = dao.activaInactivaJuzgado(idJuzgado, estado, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCboJuzgados() {
        try {
            String json = dao.cargarCboJuzgados();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void actualizaRadicadoJuzgado() {
        try {
            String idEtapa = request.getParameter("id_etapa");
            String negocio = request.getParameter("cod_neg");
            String demanda = request.getParameter("id_demanda");
            String idjuzgado = request.getParameter("idjuzgado");
            String juzgado = request.getParameter("juzgado");
            String radicado = request.getParameter("radicado");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.actualizaRadicadoJuzgado(demanda, Integer.parseInt(idjuzgado), radicado, usuario.getLogin()));
            tService.getSt().addBatch(dao.insertarRespuestaTrazabilidad(Integer.parseInt(idEtapa), negocio, Integer.parseInt(idjuzgado), juzgado, "SE AGREGA RADICADO NO: " + radicado, usuario.getLogin(), usuario.getDstrct()));
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generarActaEntrega() {

        String listadoNegocios = request.getParameter("listadoNegocios");

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jneg = (JsonObject) jsonParser.parse(listadoNegocios);
            JsonArray jsonArrNeg = jneg.getAsJsonArray("negocios");
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioArchivos = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen            
            /*String ruta = directorioArchivos + "actas/";   */
            this.createDir(directorioArchivos);
            String respuesta = dao.generarActaPagares(jsonArrNeg, directorioArchivos, usuario.getLogin());
            this.printlnResponse(respuesta, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    private void cargarCondicionesEspeciales() {
        try {
            String idItem = request.getParameter("id_item");
            String json = dao.listarCondicionesEspeciales(idItem);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void actualizarConfigCondicionesEspeciales() {
        try {
            String idItem = request.getParameter("id_item");          
            String listado[] = request.getParameter("listado").split(",");
            try{
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();       
                tService.getSt().addBatch(dao.eliminarCondicionesEspeciales(idItem));
                for(int i = 0; i < listado.length; i++){
                    tService.getSt().addBatch(dao.guardarCondicionEspecial(listado[i], idItem, usuario.getLogin(), usuario.getDstrct()));
                }
                tService.execute();
                tService.closeAll();
                String resp = "{\"respuesta\":\"OK\"}";
                this.printlnResponse(resp , "application/json;");
            }catch(Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void eliminarConfigCondicionesEspeciales() {
        try {
            String idItem = (request.getParameter("id_item") != null) ? request.getParameter("id_item") : "";          
            String resp = dao.eliminarConfigCondicionEspecial(idItem);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }      

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
}
