/**
 * Nombre        TransferenciasProntoPagoAction.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         13 de agosto de 2006, 01:02 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;


import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.beans.Compania;
import com.tsp.operation.model.beans.Movpla;
import com.tsp.operation.model.beans.Liquidacion;
import com.tsp.operation.model.beans.Planilla;
import com.tsp.operation.model.beans.Extracto;
import com.tsp.operation.model.beans.ExtractoDetalle;
import com.tsp.operation.model.beans.OPItems;
import com.tsp.operation.model.beans.DescuentoMIMS;
import com.tsp.operation.model.beans.OP;
import java.util.List;
import java.util.Vector;
import java.util.TreeMap;


import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.SeriesDAO;
import com.tsp.operation.model.Model;

public class TransferenciasProntoPagoAction extends Action{
    //2009-09-02
    Usuario usuario = null;
    int temporal;
    
    private  SeriesDAO   seriesDAO;    
    double   Valor_otras_Facturas_Pro=0;
    //double   SaldoR = 0 ;
    //private  int seriE = 0;
    //private  int ConSe = 0;
    private  String prFj ="";
    //private  float valorpagarFactura=0;
    private  boolean estadoProceso = true;
    private  String planillasConReanticipos = "";
    
    
    /** Crea una nueva instancia de  TransferenciasProntoPagoAction */
    public TransferenciasProntoPagoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String Opcion = defaultString("Opcion", "");
            String next   = "/jsp/cxpagar/liquidacion/transferencia.jsp";
            
            
            if (Opcion.equals("ConsultarProveedor")){
                String nit    = defaultString("nit"   , "");
                String datosProveedor = this.buscarDatosProveedores(nit);
                request.setAttribute("variableRetorno", datosProveedor);
                next = "/jsp/cxpagar/liquidacion/transferenciaAjax.jsp";
            }
            if (Opcion.equals("ConsultarProveedorPorUsuario")){
                String nit = model.AnticiposPagosTercerosSvc.getProveedorUser( usuario.getLogin() );
                String datosProveedor = this.buscarDatosProveedores(nit);
                request.setAttribute("variableRetorno", datosProveedor);
                next = "/jsp/cxpagar/liquidacion/transferencia.jsp";
            }
            else if (Opcion.equals("Transferir")){
                String estadoInsercion = this.grabarReanticipo();
                request.setAttribute("variableRetorno", estadoInsercion);
                next = "/jsp/cxpagar/liquidacion/transferenciaAjax.jsp";
                
            }
            this.dispatchRequest(next);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ServletException("ERROR TransferenciasProntoPagoAction " + ex.getMessage() );
        }
    }
    
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }
    
    
    
    private String buscarDatosProveedores(String nit) throws Exception{
        String retorno = "";
        try {
            model.proveedorService.obtenerProveedorPorNit(nit);
            Proveedor p = model.proveedorService.getProveedor();
            if (p!=null){
                retorno = "found|"+
                p.getC_nit()          + "|" +
                p.getC_payment_name() + "|" +
                p.getC_idMims()       + "|" +
                p.getC_branch_code()  + "|" +
                p.getC_bank_account() + "|" +
                p.getC_agency_id()    + "|" ;
            } else{
                retorno = "not found|";
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return retorno;
    }
    
    
    
    
    
    
    
    
    
    private String CrearExtractos(int opcion, OP op, float valor,String fecha ) throws Exception{
        HttpSession session = request.getSession();
        String sql="";
        try {
            Extracto extracto =new Extracto();
            extracto.setReg_status          (""                     );
            extracto.setDstrct              (usuario.getDstrct()    );//distrito
            extracto.setFecha               (fecha );//fecha actual
            extracto.setNit                 (op.getProveedor());//nit proovedor
            extracto.setVlr_pp              (valor); // valor pronto pago
            extracto.setVlr_ppa             (0);// valor pp anticipo
            extracto.setCurrency            ("PES");// moneda
            extracto.setBanco               (op.getBanco()           );
            extracto.setSucursal            (op.getSucursal()        );
            extracto.setCreation_user       (usuario.getLogin()      );
            extracto.setCreation_date       ("now");
            extracto.setUser_update         (usuario.getLogin()      );
            extracto.setLast_update         ("now");
            extracto.setBase               ( usuario.getBase() );
            extracto.setSecuencia          ( temporal );
//            sql =  model.movplaService.insertExtracto(extracto);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Extracto "+ex.getMessage());
        }
        return sql;
    }
    
    /**
     * Metodo para cargar los datos del beneficiario del pronto pago
     */
    private Proveedor loadBeneficiarioPago(){
        Proveedor p = new Proveedor();
        // datos de la persona beneficiaria del pronto pago
        p.setC_nit          ( this.defaultString("t_nit"       ,""));
        p.setC_payment_name( this.defaultString("t_nombre"    ,""));
        p.setC_tipo_cuenta  ( this.defaultString("t_tipo"      ,""));
        p.setC_branch_code  ( this.defaultString("t_banco"     ,""));
        p.setC_bank_account( this.defaultString("t_sucursal"  ,""));
        p.setC_numero_cuenta( this.defaultString("t_cuenta"    ,""));
        return p;
    }
    
    /**
     * Metodo para cargar los datos de la cuenta de transaccion
     */
    private String loadCuentatransaccion(){
        String p = "";
        // datos de la persona beneficiaria del pronto pago
        p= ( this.defaultString("B_trans"       ,""));
        return p;
    }
    
    
    
   /* Julio 12 Marzo 2007*/
    private String grabarReanticipo() throws Exception{
        String retorno = "error|No se pudo grabar en la base de datos|";
        float valorAT = 0 ;
        double valorpagarFactura=0;
        double valorReanticipo =0;
        String cedula="";
        try{
            estadoProceso = true;
                        
            // datos tercero
            String nit      = this.defaultString("nit"           ,"");
            String tercero  = this.defaultString("tercero"       ,"");
            String banco    = this.defaultString("banco"         ,"");
            String sucursal = this.defaultString("sucursal"      ,"");
            String valor    = this.defaultString("vlrReanticipo" ,"");
            //String AcumuOF  = this.request.getParameter("voajp");
            //Valor ajustes propietario  aca esa el valor de las facturas y de las corridas
            String AcumuOF  = this.defaultString("voajp","");
            
            Proveedor beneficiarioPP = loadBeneficiarioPago();
            String Complemento       = loadCuentatransaccion();
            
            
            model.ciaService.buscarCia(usuario.getDstrct());
            Compania cia = model.ciaService.getCompania();
            Vector liquidaciones = model.LiqOCSvc.getLiquidaciones();
            Vector  ajustesPropietario = model.LiqOCSvc.getDescuentosPropietario(); 
            
            liquidaciones = OrdenarLiquidacion(liquidaciones);
            
            TreeMap t = new TreeMap();
            
            
            String beneficiario  = model.LiqOCSvc.getBeneficiario(nit);
            if (beneficiario == null)
                return "error|Este proveedor no existe.|";
            
            try{
                Valor_otras_Facturas_Pro = Double.parseDouble(AcumuOF);
                model.tService.crearStatement();
                double saldoReanticipo = Double.parseDouble(valor);
                
                /*if( Valor_otras_Facturas_Pro < 0){
                    saldoReanticipo +=(Math.abs(Valor_otras_Facturas_Pro));
                }*/
                
                //SaldoR=saldoReanticipo;
                //para la secuencia
                model.ExtractoPPSvc.bucar_Secuencia();
                temporal = model.ExtractoPPSvc.get_Secuencia();
                ////////////////////////////////////////////////////////
                //Primero se Liquidan las facturas negativas 
                if ( ajustesPropietario!=null && !ajustesPropietario.isEmpty() ){
                    Liquidacion liq = (Liquidacion) liquidaciones.get(0);
                    OP op = (OP) liq.getFacturas().get(0);
                    cedula = op.getProveedor();
                    for (int i=0; i< ajustesPropietario.size();i++){
                        DescuentoMIMS descuento = (DescuentoMIMS) ajustesPropietario.get(i);
                        valorReanticipo =  Math.round(descuento.getValor());
                        if(valorReanticipo < 0 ){
                            valorpagarFactura=0;
                            //-------------------- Para grabacion de cxp_doc --------------------
                            ////////////////////////////////////////////////////////////
                            // para no asignar mas reanticipos a las planillas en caso
                            // de no haber asignado el valor total del saldo de la
                            // liquidacion
                            if (saldoReanticipo<=0){
                                break;
                            }
                            String TipoDoc="";
                            //asigno el valor del anticipo
                            if (saldoReanticipo < valorReanticipo){
                                valorReanticipo = saldoReanticipo;
                            }else{
                                saldoReanticipo -= valorReanticipo;
                            }
                            valorpagarFactura = valorReanticipo;
                            if ( valorReanticipo < 0 ){
                                TipoDoc="036"; // ("ND");
                                valorpagarFactura = valorReanticipo;
                                valorReanticipo = Math.abs(valorReanticipo);
                            }else{
                                TipoDoc="035"; // ("NC");
                            }
                            String sqlCxp = Crear_Cxp_Doc_AP(descuento,cedula,usuario,valorpagarFactura,valorReanticipo,TipoDoc);
                            System.out.println(sqlCxp);
                            model.tService.getSt().addBatch(sqlCxp);
                            valorAT += valorpagarFactura;
                            t.put(descuento.getFactura(),""+(float) java.lang.Math.round(valorpagarFactura) );
                        }
                    }
                }//Facturas a propietario
                //ahora liquidamos Oc's y Op's
                for (int i=0; i<liquidaciones.size();i++){
                    ////////////////////////////////////////////////////////////
                    // para no asignar mas reanticipos a las planillas en caso
                    // de no haber asignado el valor total del saldo de la
                    // liquidacion
                    if (saldoReanticipo<=0)
                        break;
                    ////////////////////////////////////////////////////////////
                    // datos de la liquidacion
                    Liquidacion liq = (Liquidacion) liquidaciones.get(i);
                    OP op = (OP) liq.getFacturas().get(0);
                    System.out.println("valor neto "+op.getVlrNeto() );
                    if (op.getVlrNeto() != 0){
                        ////////////////////////////////////////////////////////////
                        // asignacion del valor del reanticipo
                        valorReanticipo = op.getVlrNeto();
                        //asigno el valor del anticipo en caso de que sean solo oc's
                        if (saldoReanticipo < valorReanticipo){
                            valorReanticipo = saldoReanticipo;
                        }else{
                            saldoReanticipo -= valorReanticipo;
                        }
                        ////////////////////////////////////////////////////////////
                        // verificacion de reanticipos pendiente de la planilla
                        boolean tieneAnticiposNoImpresos = model.LiqOCSvc.verificarAnticipos(op.getOc(), op.getProveedor());
                        if (tieneAnticiposNoImpresos){
                            estadoProceso = false;
                            planillasConReanticipos += (planillasConReanticipos.equals("")?"":",") + op.getOc();
                        }
                        // datos del nuevo reanticipo
                        Movpla movpla = new Movpla();
                        movpla.setDstrct        (usuario.getDstrct()     );
                        movpla.setAgency_id     (usuario.getId_agencia() );
                        movpla.setPla_owner     (op.getProveedor()       );
                        movpla.setPlanilla      (op.getOc()              );
                        movpla.setSupplier      (op.getPlaca()           );
                        movpla.setVlr           ((float) java.lang.Math.round(valorReanticipo) );
                        movpla.setCurrency      (cia.getMoneda()         );
                        movpla.setVlr_for       ((float) java.lang.Math.round(valorReanticipo) );
                        movpla.setDocument_type("001");
                        movpla.setConcept_code  ("50");
                        movpla.setReanticipo    ("Y");
                        movpla.setAp_ind        ("S");
                        movpla.setProveedor     (tercero);
                        movpla.setBanco         (banco);
                        movpla.setCuenta        (sucursal);
                        movpla.setSucursal      ("");
                        movpla.setBeneficiario  (beneficiario);
                        movpla.setCreation_user(usuario.getLogin());
                        movpla.setCreation_date("now()");//+ new java.sql.Timestamp(System.currentTimeMillis()+11));
                        movpla.setBase          (usuario.getBase());
                        movpla.setTercero       ("T");
                        
                        
                        System.out.println("tiene op  ?? : "+op.isTieneOp() );
                        if(op.isTieneOp()==false){
                            System.out.println(" insert in  movpla "+op.getOc());
                            String sqlMovpla = "";// model.movplaService.insertMovPla(movpla, usuario.getBase());
                            String sqlAPT    =  model.AnticiposPagosTercerosSvc.InsertAnticipo(movpla , beneficiarioPP,temporal,Complemento);
                            System.out.println(sqlMovpla);
                            System.out.println(sqlAPT);
                            model.tService.getSt().addBatch(sqlMovpla);
                            model.tService.getSt().addBatch(sqlAPT);
                        }else{
                            System.out.println("insert in cxp_doc "+op.getOc());
                            String TipoDoc="";
                            valorpagarFactura = valorReanticipo;
                            if ( valorReanticipo < 0 ){
                                TipoDoc="036"; // ("ND");
                                valorpagarFactura = valorReanticipo;
                                valorReanticipo = Math.abs(valorReanticipo);
                            }else{
                                TipoDoc="035"; // ("NC");
                            }
                            String sqlCxp    = Crear_Cxp_Doc_OCOP(liq,usuario,valorpagarFactura,valorReanticipo,TipoDoc);
                            System.out.println(sqlCxp);
                            model.tService.getSt().addBatch(sqlCxp);
                            valorAT += valorpagarFactura;
                            }
                        //Guardo el numero de la OC y el valor por el cual se va a liquidar una planilla 
                        t.put(op.getOc(),""+(float) java.lang.Math.round(valorpagarFactura) );
                    }
                }//Liquidaciones
                ////////////////////////////////////////////////////////
                //Primero se Liquidan las facturas negativas 
                if ( ajustesPropietario!=null && !ajustesPropietario.isEmpty() ){
                    Liquidacion liq = (Liquidacion) liquidaciones.get(0);
                    OP op = (OP) liq.getFacturas().get(0);
                    cedula = op.getProveedor();
                    for (int i=0; i< ajustesPropietario.size();i++){
                        DescuentoMIMS descuento = (DescuentoMIMS) ajustesPropietario.get(i);
                        valorReanticipo =  Math.round(descuento.getValor());
                        if(valorReanticipo > 0 ){
                            valorpagarFactura=0;
                            //-------------------- Para grabacion de cxp_doc --------------------
                            ////////////////////////////////////////////////////////////
                            // para no asignar mas reanticipos a las planillas en caso
                            // de no haber asignado el valor total del saldo de la
                            // liquidacion
                            if (saldoReanticipo<=0){
                                break;
                            }
                            String TipoDoc="";
                            //asigno el valor del anticipo
                            if (saldoReanticipo < valorReanticipo){
                                valorReanticipo = saldoReanticipo;
                            }else{
                                saldoReanticipo -= valorReanticipo;
                            }
                            valorpagarFactura = valorReanticipo;
                            if ( valorReanticipo < 0 ){
                                TipoDoc="036"; // ("ND");
                                valorpagarFactura = valorReanticipo;
                                valorReanticipo = Math.abs(valorReanticipo);
                            }else{
                                TipoDoc="035"; // ("NC");
                            }
                            String sqlCxp = Crear_Cxp_Doc_AP(descuento,cedula,usuario,valorpagarFactura,valorReanticipo,TipoDoc);
                            System.out.println(sqlCxp);
                            model.tService.getSt().addBatch(sqlCxp);
                            valorAT += valorpagarFactura;
                            t.put(descuento.getFactura(),""+(float) java.lang.Math.round(valorpagarFactura) );
                        }
                    }
                }//Facturas a propietario
                System.out.println("ejecutando extracto");
                model.tService.getSt().addBatch( Extracto(t,valorAT));
                if (estadoProceso)
                    model.tService.execute();
            } catch (Exception e) {
                e.printStackTrace();
                estadoProceso = false;
                throw new Exception("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO EN MOVPLA Y CXP_DOC" + e.getMessage());
            } finally{
                model.tService.closeAll();
            }
            if( estadoProceso ){
                retorno = "save|Reanticipo grabado en la base de datos|";
                Valor_otras_Facturas_Pro=0;//SaldoR = 0 ;
                prFj ="";
                valorpagarFactura=0;estadoProceso = true;planillasConReanticipos = "";
            }else if (!estadoProceso && !planillasConReanticipos.equals("")){
                retorno = "error|no puede realizar el proceso, la planilla "+ planillasConReanticipos +" tiene anticipos o reanticipos no impresos|";
            }
        } catch (Exception ex){
            ex.printStackTrace();
            retorno  = "ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DEL PP " + ex.getMessage();
            throw new Exception("ERROR DURANTE LA INSERCCION DE LOS MOVIMIENTO DEL PP " + ex.getMessage());
        }
        return retorno;
    }
    
    private Vector OrdenarLiquidacion (Vector liquidaciones){ 
        for (int i = (liquidaciones.size() - 1); i >= 0; i--){
          for (int j = 1; j <= i; j++){
              Liquidacion liq = (Liquidacion) liquidaciones.get(j);
              OP op = (OP) liq.getFacturas().get(0);
              double t1 = op.getVlrNeto();
              Liquidacion liq2 = (Liquidacion) liquidaciones.get(j-1);
              OP op2 = (OP) liq2.getFacturas().get(0);
              double t2 = op2.getVlrNeto();
              if (t2 > t1){
                Liquidacion temp = liq2;
                liquidaciones.setElementAt(liq, (j-1));
                liquidaciones.setElementAt(temp, j);
              }
          }
        }
        return liquidaciones;
    }
    
    private String Extracto( TreeMap t,float valorAT) throws Exception{
        String sql="",cedula="",placa="";
        try {
            Vector  liquidaciones      = model.LiqOCSvc.getLiquidaciones();//  pl
            Vector  ajustesPropietario = model.LiqOCSvc.getDescuentosPropietario(); //
            Vector  corridasNegativas  = model.LiqOCSvc.getCorridasNegativas();
            OP op = null;
            String fecha    = ""+new java.sql.Timestamp(System.currentTimeMillis()+11);
            System.out.println("Extracto Liquidaciones");
            for (int i=0; i< liquidaciones.size();i++){
                float prontopago = 0 ;
                float Liquidacion=0;
                Liquidacion liquidacion = (Liquidacion) liquidaciones.get(i);
                List        facturas    =  liquidacion.getFacturas();
                op = (OP) facturas.get(0);
                cedula = op.getProveedor();
                System.out.println("en el tremap ("+t.get(op.getOc())+")");
                if ( t.get(op.getOc())!= null && !t.get(op.getOc()).equals("")  )
                    prontopago = Float.parseFloat(""+t.get(op.getOc()));
                
                //}
                if(i==0){
                    sql +=  CrearExtractos(0 , op , Liquidacion ,fecha )+";";
                }
                List listItem = op.getItem();
                if ( listItem!=null ) {
                    for(int j=0;j < listItem.size();j++){
                        OPItems  item = (OPItems)listItem.get(j);
                        if (item.isVisible()==true || op.isTieneOp() ){
                            if(j==0){
                                if ( t.get(op.getOc())== null  ){
                                    sql +=  CrearExtractosDetalle(0 , op.getOc() , 0 ,fecha,item, null, cedula, op.getPlaca() )+";";//sin cargo
                                }else{
                                    sql +=  CrearExtractosDetalle(0 ,op.getOc() , prontopago ,fecha,item, null, cedula, op.getPlaca() )+";";
                                }
                            }
                            else{
                                sql +=  CrearExtractosDetalle(0 , op.getOc() , 0 ,fecha,item, null, cedula, op.getPlaca() )+";";//sin cargo
                            }
                        
                        }
                        
                    }
                }
            }
            System.out.println("Extracto ajuste Propietarios");
            if ( ajustesPropietario!=null && !ajustesPropietario.isEmpty() ){
                for (int i=0; i< ajustesPropietario.size();i++){
                    float prontopago = 0 ;
                    DescuentoMIMS descuento = (DescuentoMIMS) ajustesPropietario.get(i);
                    System.out.println("en el tremap 2 parte ("+t.get(descuento.getFactura())+")");
                    if ( t.get(descuento.getFactura())!= null && !t.get(descuento.getFactura()).equals("")  )
                        prontopago = Float.parseFloat(""+t.get(descuento.getFactura() ));
                    sql += CrearExtractosDetalle(1 , descuento.getFactura() , prontopago ,fecha, null ,  descuento, cedula, op.getPlaca() )+";";
                }
            }
            System.out.println("Trnasaccion trasnferir");
            if (valorAT != 0){
                model.ciaService.buscarCia(usuario.getDstrct());
                Compania cia = model.ciaService.getCompania();
                String nit      = this.defaultString("nit"           ,"");
                String tercero  = this.defaultString("tercero"       ,"");
                String banco    = this.defaultString("banco"         ,"");
                String sucursal = this.defaultString("sucursal"      ,"");
                String beneficiario  = model.LiqOCSvc.getBeneficiario(nit);
                Proveedor beneficiarioPP = loadBeneficiarioPago();
                String Complemento       = loadCuentatransaccion();
                Movpla movpla = new Movpla();
                movpla.setDstrct        ( usuario.getDstrct()     );
                movpla.setAgency_id     ( usuario.getId_agencia() );
                movpla.setPla_owner     ( op.getProveedor()       );
                movpla.setPlanilla      ( "E"+temporal            );
                movpla.setSupplier      ( ""              );
                movpla.setVlr           ( (float) java.lang.Math.round( valorAT ) );
                movpla.setCurrency      ( cia.getMoneda()         );
                movpla.setVlr_for       ( (float) java.lang.Math.round( valorAT ) );
                movpla.setDocument_type ( "001" );
                movpla.setConcept_code  ( "50" );
                movpla.setReanticipo    ( "Y" );
                movpla.setAp_ind        ( "S" );
                movpla.setProveedor     ( tercero );
                movpla.setBanco         ( banco );
                movpla.setCuenta        ( sucursal );
                movpla.setSucursal      ( "" );
                movpla.setBeneficiario  (beneficiario);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setCreation_date("now()");//+ new java.sql.Timestamp(System.currentTimeMillis()+11));
                movpla.setBase          (usuario.getBase());
                movpla.setTercero       ("T");
                sql +=  model.AnticiposPagosTercerosSvc.InsertAnticipo(movpla , beneficiarioPP,temporal,Complemento);
            }
            
            
            if (corridasNegativas!=null && !corridasNegativas.isEmpty()){
                for (int i=0; i< corridasNegativas.size();i++){
                    DescuentoMIMS descuento = (DescuentoMIMS) corridasNegativas.get(i);
                    sql += CrearExtractosDetalle(2 , descuento.getFactura() , 0,fecha, null ,  descuento, cedula, op.getPlaca() )+";";
                }
            }
        } catch (Exception ex){
            estadoProceso=false;
            ex.printStackTrace();
            throw new Exception("ERROR DURANTE LA CREACION DE EXTRACTOS " + ex.getMessage());
        }
        System.out.println("el SQL Extractos ---> "+sql);
        return sql;
    }
    
    
    
    
    private String CrearExtractosDetalle( int opcion , String op , float valor , String fecha , OPItems  item, DescuentoMIMS descuento, String cedula, String placaOP ) throws Exception{
        HttpSession session = request.getSession();
        String sql="";
            try {
                ExtractoDetalle exdetalle =new ExtractoDetalle();
                exdetalle.setReg_status     ("");
                exdetalle.setNit            (cedula);
                exdetalle.setFecha          (fecha                              );//fecha documento
                if(opcion == 1 || opcion == 2 ){
                    exdetalle.setTipo_documento("PR" );// si es Cedula
                    exdetalle.setDocumento      ( cedula );
                    if(opcion == 2){
                        exdetalle.setTipo_documento("CN" );// si es placa
                        exdetalle.setDocumento      ( descuento.getFactura_ext()  );//numero de la corrida
                    }
                    exdetalle.setDstrct         ( descuento.getDistrito()            );
                    exdetalle.setConcepto       ( descuento.getItem() );
                    exdetalle.setDescripcion    ( descuento.getDescripcion()        );
                    exdetalle.setFactura        ( descuento.getFactura()        ); // para la planilla que da en blanco, solo aplica propietario y placa
                    exdetalle.setVlr            ( Float.parseFloat( "" + descuento.getValor_item()    )    ); //el del item
                    exdetalle.setRetefuente     ( Float.parseFloat( "" +descuento.getRetefuente())    );// el del item
                    exdetalle.setReteica        ( Float.parseFloat( "" +descuento.getRica()      )    ); //el del item
                    exdetalle.setImpuestos      ( Float.parseFloat( "" + (descuento.getIva()  -descuento.getRiva() -descuento.getRica() - descuento.getRetefuente()) ));  //solo palica propietario y placa
                    exdetalle.setVlr_pp_item    ( Float.parseFloat( "" +descuento.getValor() ) );//
                    exdetalle.setVlr_ppa_item   ( valor );  //  cero
                    exdetalle.setPlaca          ( "" );

                }
                else{
                    exdetalle.setDstrct         (item.getDstrct()                   );
                    exdetalle.setTipo_documento( "PL" );
                    exdetalle.setDocumento      ( op );
                    exdetalle.setConcepto       (item.getItem()); // item de la planilla
                    exdetalle.setDescripcion    (item.getDescconcepto() );
                    exdetalle.setFactura        ("");
                    exdetalle.setVlr            ( Float.parseFloat( "" + item.getVlr())  ); //el del item
                    exdetalle.setRetefuente     ( Float.parseFloat( "" + item.getVlrReteFuente()) );// el del item
                    exdetalle.setReteica        ( Float.parseFloat( "" + item.getVlrReteIca()   ) ); //el del item
                    exdetalle.setImpuestos      (0);
                    exdetalle.setVlr_pp_item    ( 0 );
                    exdetalle.setVlr_ppa_item   ( valor );
                    if (placaOP != null )
                        exdetalle.setPlaca          ( placaOP );
                    else
                        exdetalle.setPlaca          ("");
                }
                exdetalle.setCreation_user  (usuario.getLogin());
                exdetalle.setCreation_date  ("now()"); // actual
                exdetalle.setUser_update    (usuario.getLogin());  // el msismo de creation user
                exdetalle.setLast_update    ("now()"); //*/ //actual
                exdetalle.setBase           ( usuario.getBase() );
                exdetalle.setSecuencia      ( temporal );
                sql =  model.movplaService.insertExtractoDetalle(exdetalle);
            }catch (Exception ex){
                ex.printStackTrace();
                throw new Exception("Extracto detalle "+ex.getMessage());
            }
        return sql;
    }
    
    
    private String Crear_Cxp_Doc_OCOP(Liquidacion doc,Usuario usuario,double valorReantFin,double valorReanticipo,String TipoDoc) throws Exception{
        String sql = "";
        String ABC = "";
        String moneda_local = model.ingreso_detalleService.monedaLocal (usuario.getDstrct());
        double vlr_tasa = 1;
        OP op = null;
        List        facturas    =  doc.getFacturas();
        op = (OP) facturas.get(0);
        String NitPro = op.getProveedor();
        try{
            if ( valorReanticipo != 0){
            ////////////////////////////////////////////////////////////    
            // asignacion del valor del reanticipo
                String Fecha01 =  Util.getFechaActual_String(4);
                String Fecha02 =  Util.getFechaActual_String(1) + Util.getFechaActual_String(3);
                Proveedor prov = model.proveedorService.obtenerProveedorPorNit(NitPro);                
                OpDAO op2 = new OpDAO(usuario.getBd());
                ABC = op2.getABC( usuario.getId_agencia() );
                //carga la factura desde el programa de ivan
                System.out.println("CuaL ES LA PLANILLA   ?? "+doc.getOC());
                System.out.println("docuemnto op ?? "+op.getDocumento());
                CXP_Doc factura = model.cxpDocService.BuscarFactura(usuario.getDstrct(),NitPro,op.getDocumento(),"010");
                //modifico los datos deacuerdo a las nesecidades
                /***********************************************************************************************/
                factura.setClase_documento_rel                          (factura.getClase_documento());
                factura.setDocumento_relacionado                        (op.getDocumento());//"OP"+doc.getOC()
                factura.setTipo_documento_rel                           (factura.getTipo_documento());
                /**********************************************************************************************/
                factura.setReg_status                                   ("");
                factura.setDstrct                                       ( usuario.getDstrct() );
                factura.setProveedor                                    (NitPro);
                factura.setTipo_documento                               (TipoDoc); // Tipo_documento
                factura.setDocumento                                    ("PP "+op.getDocumento()+" "+temporal);
                factura.setObservacion                                  ("Cruce por PP "+op.getDocumento());
                factura.setDescripcion                                  ("Pronto Pago FINTRA "+op.getDocumento()+" L"+temporal  );
                factura.setId_mims                                      ("");
                factura.setFecha_documento                              (Fecha01);                
                factura.setFecha_aprobacion                             (Fecha01+" 00:00:00");
                factura.setUsuario_aprobacion                           ("ADMIN");
                factura.setAprobador                                    ("ADMIN");
                factura.setFecha_vencimiento                            (Fecha01);
                factura.setUltima_fecha_pago                            ("0099-01-01 00:00:00");
                factura.setAgencia                                      ( prov.getC_agency_id() );
                factura.setBanco                                        ( prov.getC_branch_code() );
                factura.setSucursal                                     ( prov.getC_bank_account());
                factura.setVlr_neto                                     (valorReanticipo );// aca va el valor neto calculado en esta infaz
                factura.setVlr_total_abonos                             (0);
                factura.setVlr_saldo                                    (valorReanticipo);// el mismo valor neto
                factura.setVlr_neto_me                                  (valorReanticipo);// el mismo valor neto
                factura.setVlr_total_abonos_me                          (0);
                factura.setVlr_saldo_me                                 (valorReanticipo);
                factura.setTasa                                         (vlr_tasa);
                factura.setUsuario_contabilizo                          ("");
                factura.setFecha_contabilizacion                        ("0099-01-01 00:00:00");
                factura.setUsuario_anulo                                ("");
                factura.setFecha_anulacion                              ("0099-01-01 00:00:00");
                factura.setFecha_contabilizacion_anulacion              ("0099-01-01 00:00:00");
                factura.setNum_obs_autorizador                          (0);
                factura.setNum_obs_pagador                              (0);
                factura.setNum_obs_registra                             (0);
                factura.setCreation_user                                (usuario.getLogin());
                factura.setUser_update                                  (usuario.getLogin());
                factura.setBase                                         (usuario.getBase());  
                factura.setPeriodo                                      (Fecha02);
                factura.setFecha_vencimiento                            (Fecha01);
                factura.setClase_documento                              ("4");
                //se crean vacias la lista de impuestos,tipo impuestos e items
                Vector vTipImp  = new Vector();
                CXPItemDoc item = new CXPItemDoc();
                Vector vTipoImp = model.TimpuestoSvc.vTiposImpuestos();
                Vector vItems   = new Vector();
                // Se crea el Item unico
                item.setReg_status                                      ("");
                item.setDstrct                                          (usuario.getDstrct() );
                item.setProveedor                                       (NitPro);
                item.setTipo_documento                                  (TipoDoc); // Tipo_documento       036 ("ND");
                item.setDocumento                                       ("PP "+op.getDocumento()+" "+temporal);
                item.setDescripcion                                     ("Cruce por PP "+op.getDocumento());
                item.setItem                                            ("001");  //com.tsp.util.Utility.rellenar( String.valueOf(cod_item) ,3)
                //item.setConcepto                                        ( doc.getCodConcepto());
                item.setVlr                                             ( valorReanticipo );//valor moneda local  el mismo valor neto
                item.setVlr_me                                          ( valorReanticipo );//valor me del item   el mismo valor neto
                item.setCodigo_cuenta                                   ("23050101037");
                item.setCodigo_abc                                      (ABC);
                if ( doc.getPlanilla() != null  && !doc.getPlanilla().equals("") )
                    item.setPlanilla                                    (doc.getOC());                //Planilla
                else
                    item.setPlanilla                                    ("");
                item.setCodcliarea                                      ("");
                item.setTipcliarea                                      ("");
                item.setCreation_user                                   (usuario.getLogin());
                item.setUser_update                                     (usuario.getLogin());
                item.setBase                                            (usuario.getBase());
                item.setAuxiliar                                        ("");
                item.setTipoSubledger                                   ("");
                item.setConcepto                                        ("50");//preguntar
                vItems.add(item);
                //retorna la consulat que crea el nuevo registro a cxp con todas las asociaciones.
                sql += model.cxpDocService.insertarCXPDoc_SQL(factura,vItems,vTipImp,usuario.getId_agencia(),"" ); 
                ///////////////////////////////////////////////////////////////////////////////////////////////
                // Para que Fintra le cobre a TSP                                                            //
                ///////////////////////////////////////////////////////////////////////////////////////////////
                vItems.removeAllElements();
                NitPro = "802022016";
                prov = model.proveedorService.obtenerProveedorPorNit(NitPro);
                factura.setProveedor                                    (NitPro);
                factura.setTipo_documento                               ("010"); // Tipo_documento
                factura.setVlr_neto                                     (valorReantFin );// aca va el valor neto calculado en esta infaz
                factura.setVlr_saldo                                    (valorReantFin);// el mismo valor neto
                factura.setVlr_neto_me                                  (valorReantFin);// el mismo valor neto
                factura.setVlr_saldo_me                                 (valorReantFin);
                factura.setHandle_code                                  (prov.getHandle_code());
                factura.setAgencia                                      ( prov.getC_agency_id() );
                factura.setBanco                                        ( prov.getC_branch_code() );
                factura.setSucursal                                     ( prov.getC_bank_account());
                item.setProveedor                                       (NitPro);
                item.setTipo_documento                                  ("010");
                item.setVlr                                             ( valorReantFin );//valor moneda local  el mismo valor neto
                item.setVlr_me                                          ( valorReantFin );//valor me del item   el mismo valor neto
                item.setAuxiliar                                        ("");
                /**********************************************************************************************/
                factura.setClase_documento_rel                          ("4");
                factura.setDocumento_relacionado                        ("");//"OP"+doc.getOC()
                factura.setTipo_documento_rel                           ("");
                /**********************************************************************************************/
                vItems.add(item);
                sql += model.cxpDocService.insertarCXPDoc_SQL(factura,vItems,vTipImp,usuario.getId_agencia(),"" ); 
            }
            
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("ERROR DURANTE INSERCION DE CXP_DOC OP'S " + e.getMessage());
        }
        return sql;
    }
    
    private String Crear_Cxp_Doc_AP(DescuentoMIMS doc,String NitPro,Usuario usuario,double valorReantFin,double valorReanticipo,String TipoDoc) throws Exception{
        String sql = "";
        String ABC = "";
        String moneda_local = model.ingreso_detalleService.monedaLocal (usuario.getDstrct());
        double vlr_tasa = 1;
        try{
            if ( valorReanticipo != 0){
            ////////////////////////////////////////////////////////////    
            // asignacion del valor del reanticipo
                String Fecha01 =  Util.getFechaActual_String(4);
                String Fecha02 =  Util.getFechaActual_String(1) + Util.getFechaActual_String(3);
                Proveedor prov = model.proveedorService.obtenerProveedorPorNit(NitPro);
                OpDAO op = new OpDAO(usuario.getBd());
                ABC = op.getABC( usuario.getId_agencia() );
                //carga la factura desde el programa de ivan
                CXP_Doc factura = model.cxpDocService.BuscarFactura(usuario.getDstrct(),NitPro,doc.getFactura(),"010");
                //modifico los datos deacuerdo a las nesecidades
                factura.setReg_status                                   ("");
                /**********************************************************************************************/
                factura.setClase_documento_rel                          (factura.getClase_documento());
                factura.setDocumento_relacionado                        (doc.getFactura());//"OP"+doc.getOC()
                factura.setTipo_documento_rel                           (factura.getTipo_documento());
                /**********************************************************************************************/
                factura.setDstrct                                       ( usuario.getDstrct() );
                factura.setProveedor                                    (NitPro);
                factura.setTipo_documento                               (TipoDoc); // Tipo_documento       036 ("ND");      
                factura.setDocumento                                    ("PP "+doc.getFactura()+" "+temporal);
                factura.setObservacion                                  ("Cruce por PP "+doc.getFactura());
                factura.setDescripcion                                  ("Pronto Pago FINTRA "+doc.getFactura()+" L"+temporal);
                factura.setId_mims                                      ("");
                factura.setFecha_documento                              (Fecha01);
                factura.setFecha_aprobacion                             (Fecha01+" 00:00:00");
                factura.setUsuario_aprobacion                           ("ADMIN");
                factura.setAprobador                                    ("ADMIN");
                factura.setFecha_vencimiento                            (Fecha01);
                factura.setUltima_fecha_pago                            ("0099-01-01 00:00:00");
                factura.setAgencia                                      ( prov.getC_agency_id() );
                factura.setBanco                                        ( prov.getC_branch_code() );
                factura.setSucursal                                     ( prov.getC_bank_account());
                factura.setMoneda                                       (doc.getMoneda());
                factura.setMoneda_banco                                 (doc.getMoneda());
                factura.setVlr_neto                                     (valorReanticipo );// aca va el valor neto calculado en esta infaz
                factura.setVlr_total_abonos                             (0);
                factura.setVlr_saldo                                    (valorReanticipo);// el mismo valor neto
                factura.setVlr_neto_me                                  (valorReanticipo);// el mismo valor neto
                factura.setVlr_total_abonos_me                          (0);
                factura.setVlr_saldo_me                                 (valorReanticipo);
                factura.setTasa                                         (vlr_tasa);
                factura.setUsuario_contabilizo                          ("");
                factura.setFecha_contabilizacion                        ("0099-01-01 00:00:00");
                factura.setUsuario_anulo                                ("");
                factura.setFecha_anulacion                              ("0099-01-01 00:00:00");
                factura.setFecha_contabilizacion_anulacion              ("0099-01-01 00:00:00");
                factura.setNum_obs_autorizador                          (0);
                factura.setNum_obs_pagador                              (0);
                factura.setNum_obs_registra                             (0);
                factura.setCreation_user                                (usuario.getLogin());
                factura.setUser_update                                  (usuario.getLogin());
                factura.setBase                                         (usuario.getBase());
                factura.setPeriodo                                      (Fecha02);
                factura.setFecha_vencimiento                            (Fecha01);
                factura.setClase_documento                              ("4");
                //se crean vacias la lista de impuestos,tipo impuestos e items
                Vector vTipImp  = new Vector();
                CXPItemDoc item = new CXPItemDoc();
                Vector vTipoImp = model.TimpuestoSvc.vTiposImpuestos();
                Vector vItems = new Vector();
                // Se crea el Item unico
                item.setReg_status                                      ("");
                item.setDstrct                                          (usuario.getDstrct() );
                item.setProveedor                                       (NitPro);
                item.setTipo_documento                                  (TipoDoc); // Tipo_documento       036 ("ND");
                item.setDocumento                                       ("PP "+doc.getFactura()+" "+temporal);
                item.setDescripcion                                     ("Cruce por PP "+doc.getFactura());
                item.setItem                                            ("001");  //com.tsp.util.Utility.rellenar( String.valueOf(cod_item) ,3)
                item.setConcepto                                        (doc.getCodConcepto());
                item.setVlr                                             ( valorReanticipo );//valor moneda local  el mismo valor neto
                item.setVlr_me                                          ( valorReanticipo );//valor me del item   el mismo valor neto
                item.setCodigo_cuenta                                   ("23050101037");
                item.setCodigo_abc                                      (ABC);
                if ( doc.getPlanilla() != null  && !doc.getPlanilla().equals("") )
                    item.setPlanilla                                    (doc.getPlanilla());                //Planilla
                else
                    item.setPlanilla                                    ("");
                item.setCodcliarea                                      ("");
                item.setTipcliarea                                      ("");
                item.setCreation_user                                   (usuario.getLogin());
                item.setUser_update                                     (usuario.getLogin());
                item.setBase                                            (usuario.getBase());
                item.setAuxiliar                                        ("");
                item.setTipoSubledger                                   ("");
                item.setConcepto                                        ("50");//preguntar
                vItems.add(item);
                //retorna la consulat que crea el nuevo registro a cxp con todas las asociaciones.
                sql += model.cxpDocService.insertarCXPDoc_SQL(factura,vItems,vTipImp,usuario.getId_agencia(),"" );
                ///////////////////////////////////////////////////////////////////////////////////////////////
                // Para que Fintra le cobre a TSP                                                            //
                ///////////////////////////////////////////////////////////////////////////////////////////////
                vItems.removeAllElements();
                NitPro = "802022016";
                prov = model.proveedorService.obtenerProveedorPorNit(NitPro);
                factura.setProveedor                                    (NitPro);
                factura.setTipo_documento                               ("010"); // Tipo_documento
                factura.setVlr_neto                                     (valorReantFin );// aca va el valor neto calculado en esta infaz
                factura.setVlr_saldo                                    (valorReantFin);// el mismo valor neto
                factura.setVlr_neto_me                                  (valorReantFin);// el mismo valor neto
                factura.setVlr_saldo_me                                 (valorReantFin);
                factura.setHandle_code                                  (prov.getHandle_code());
                factura.setAgencia                                      ( prov.getC_agency_id() );
                factura.setBanco                                        ( prov.getC_branch_code() );
                factura.setSucursal                                     ( prov.getC_bank_account());
                item.setProveedor                                       (NitPro);
                item.setTipo_documento                                  ("010");
                item.setVlr                                             ( valorReantFin );//valor moneda local  el mismo valor neto
                item.setVlr_me                                          ( valorReantFin );//valor me del item   el mismo valor neto
                item.setAuxiliar                                        ("");
                /**********************************************************************************************/
                factura.setClase_documento_rel                          ("4");
                factura.setDocumento_relacionado                        ("");//"OP"+doc.getOC()
                factura.setTipo_documento_rel                           ("");
                /**********************************************************************************************/
                vItems.add(item);
                sql += model.cxpDocService.insertarCXPDoc_SQL(factura,vItems,vTipImp,usuario.getId_agencia(),"" );
            }
            
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("ERROR DURANTE Crear_Cxp_Doc_AP " + e.getMessage());
        }
        return sql;
    }
    
    /*FP Julio 12 Marzo*/
    
    
    
}
