/*
* Nombre        UbicacionEquipoRutaSearchAction.java
* Descripci�n   Acci�n ejecutada por el controlador para elaborar el reporte de ubicaci�n
* Autor         iherazo - nparejo
* Fecha         24 de marzo de 2004, 04:42 PM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/


package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.util.Utility;
import java.util.*;
import com.tsp.operation.model.threads.*;


/**
 * Acci�n ejecutada por el controlador para elaborar el reporte de ubicaci�n
 * y llegada de equipos en ruta.
 * @author  iherazo - nparejo
 */
public class UbicacionEquipoRutaSearchAction extends Action {
    
    static Logger logger = Logger.getLogger(UbicacionEquipoRutaSearchAction.class);
    
    
    
    public void run() throws ServletException, InformationException {
        String next = null;
        // Perform search
        String [] args = new String[14];
        args[0] = request.getParameter("fechaini");
        if( args[0] == null ) {
            try {
                model.ciudadService.getCiudadSearch();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            next = "/jsp/sot/reports/FiltroUbicaEquipoRuta.jsp";
        } else {
            //      args[1] = request.getParameter("fechaini");
            //      args[1] = (args[1] == null ? "" : args[1]);
            args[1] = request.getParameter("fechafin");
            args[1] = (args[1] == null ? "" : args[1]);
            args[2] = request.getParameter("listaTipoViaje");
            args[3] = request.getParameter("estadoViajes");
            args[4] = request.getParameter("grupoequipo");
            args[4] = (args[4] == null ? "" : args[4].trim().replaceAll("-_-", "&" ));
            args[5] = request.getParameter("placasTrailers");
            args[5] = (args[5] == null ? "" : args[5] );
            args[6] = request.getParameter("planillas");
            args[6] = (args[6] == null ? "" : args[6].trim() );
            
            args[7] = request.getParameter("origen");
            args[7] = (args[7] == null ? "" : args[7].trim() );
            args[8] = request.getParameter("destino");
            args[8] = (args[8] == null ? "" : args[8].trim() );
            args[9] = request.getParameter("tipoReporte");//*******APAYARES 20060203
            args[10] = request.getParameter("asociadasorigen");
            args[10] = (args[10] == null ? "N" : args[10].trim() );
            args[11] = request.getParameter("asociadasdestino");
            args[11] = (args[11] == null ? "N" : args[11].trim() );
            args[12] = request.getParameter("tipoBusqueda");
            args[13] = request.getParameter("FITDEF");
            
            
            try {
                
                HttpSession session = request.getSession();
                Usuario loggedUser  = (Usuario)session.getAttribute("Usuario");
                next                = "/jsp/sot/reports/ReporteUbicaEquipoRuta.jsp";
                
                String view = request.getParameter("displayInExcel");
                if(view!=null && !view.equals("File")){
                    model.reporteUbicacionVehService.buscarDatosReporte(args);
                }
                else{
                    next = "/jsp/sot/reports/FiltroUbicaEquipoRuta.jsp?comentario=Su proceso de Exportaci�n de Ubicaci�n y LLegada de Equipos se ha iniciado... Puede consultar en Manejo de Archivo";
                    
                    
                    //--- tomamos los parametros
                    Hashtable headerValues = new Hashtable();
                    headerValues.clear();
                    String nombreCliente = (request.getParameter("nombreCliente") == null)?"":request.getParameter("nombreCliente");
                    String fechaini      = (request.getParameter("fechaini")      == null)?"":request.getParameter("fechaini");
                    String estadoViajes  = (request.getParameter("estadoViajes")  == null)?"":request.getParameter("estadoViajes");
                    estadoViajes  = (estadoViajes.equals("RUTA") ? "En Ruta" :
                        (estadoViajes.equals("PORCONF") ? "Por Confirmar Salida" :
                            (estadoViajes.equals("CONENTREGA") ? "Con Entrega" : "TODOS")));
                            String placasTrailers = (request.getParameter("placasTrailers") == null )?"":request.getParameter("placasTrailers");
                            String tipoBusqueda   = (request.getParameter("tipoBusqueda")   == null )?"":request.getParameter("tipoBusqueda");
                            String planillas      = (request.getParameter("planillas")      == null )?"":request.getParameter("planillas");
                            String listaTipoViaje = (request.getParameter("listaTipoViaje") == null )?"":request.getParameter("listaTipoViaje");
                            String fechafin       = (request.getParameter("fechafin")       == null )?"":request.getParameter("fechafin");
                            if( !placasTrailers.equals("") )
                                headerValues.put( "placasTrailers", placasTrailers );
                            headerValues.put( "tipoBusqueda", tipoBusqueda );
                            if(fechaini != null && !fechaini.equals("")){
                                headerValues.put( "fechaInicial", fechaini );
                                headerValues.put( "fechaFinal",   fechafin);
                            }
                            if(planillas!=null &&  !planillas.equals("") )
                                headerValues.put( "planillas", planillas );
                            headerValues.put( "estadoViajes",   estadoViajes );
                            headerValues.put( "listaTipoViaje", listaTipoViaje );
                            String ruta = this.application.getRealPath("/WEB-INF/");
                            headerValues.put( "rutaPaginaLastUpdate", ruta );
                            
                            HExportarExcel_UbicacionLLegada  hilo  =  new HExportarExcel_UbicacionLLegada();
                            hilo.start(model, loggedUser , headerValues, args);
                            
                }
                
                //logger.info(  model.getSQL() );
                logger.info(  loggedUser.getNombre() + " # ejecuto Ubicacion de Equipos en Ruta con: Cliente " + args[0]
                + " Fecha Inicial: " + args[1] + " Fecha Final: " + args[2] + " Tipo Viaje: " + args[3] );
            }catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
            
        }
        ////System.out.println("redirecionando hacia: "+next);
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}
