/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AplicarDocumentosAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public AplicarDocumentosAction() {
    }
    static Logger logger = Logger.getLogger(RemesaDoctoAplicarAction.class);
    
    public void run() throws ServletException, InformationException {
        
        String next="/docremesas/AgregarDoc.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        logger.info( "*************************************************************");
        logger.info( "-----------------USUARIO : "+usuario.getLogin());
        logger.info( "ACCION PARA DOCUMENTOS INTERNOS... ");
        
        
        if(request.getParameter("generar")!=null){
            
            logger.info( "SE VA A MOSTRAR EL VECTOR...");
            if(request.getParameter("numrem")!=null){
                try{
                    next="/docremesas/ModificarDoc.jsp";
                    logger.info( "se buscan los docs de la remesa ..."+request.getParameter("numrem"));
                    model.RemDocSvc.setDocs(model.RemDocSvc.listaDocs(request.getParameter("numrem")));
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            Vector vec = model.RemDocSvc.getDocs();
            //    logger.info(" DESTINATARIO    " + request.getParameter("destinatario") );
            if(vec==null){
                logger.info("El vector es nulo");
                vec = new Vector();
                for(int i=0; i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                }
                logger.info("Tama�o del nuevo vector con elementos vacios "+vec.size());
                model.RemDocSvc.setDocs(vec);
                
                
            }else{
                logger.info("El vector no es nulo");
            }
            if(vec.size()<5){
                Vector vec1 = new Vector();
                for(int i=vec.size(); i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                    vec.add(rem);
                }
            }
            model.RemDocSvc.setDocs(vec);
        }
        else{
            
            logger.info( "SE VA A ACTUALIZAR EL VECTOR...");
            Vector vec = new Vector();
            logger.info("Cantidad de Documentos encontrados: " + vec.size() );
            int cant = 0;
            boolean sw= false;
            java.util.Enumeration enum1;
            String parametro;
            String mensaje ="Ingrese los datos necesarios para la importacion";
            enum1 = request.getParameterNames(); // Leemos todos los atributos del request
            int cantImpo=0;
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                //logger.info( "Usuario: "+usuario.getLogin()+" Parametro: "+parametro+" Valor: "+request.getParameter(parametro));
                if(parametro.indexOf("documento")>=0){
                    if(!request.getParameter(parametro).equals("")){
                        int numero =Integer.parseInt( parametro.substring(9));
                        String numeroDocOri = request.getParameter("docimp"+numero);
                        remesa_docto rem = new remesa_docto();
                        String tipodoc[]= request.getParameter("tipodoc"+numero).split("/");
                        
                        String importacion=tipodoc.length>1?tipodoc[1]:"";
                        String exportacion = tipodoc.length>2?tipodoc[2]:"";
                        
                        rem.setTipo_doc(tipodoc.length>0?tipodoc[0]:"");
                        rem.setDocumento(request.getParameter(parametro));
                        
                        rem.setImportacion(importacion);
                        rem.setExportacion(exportacion);
                        
                        if(importacion.equals("S")){
                            cantImpo++;
                        }
                        
                        
                        if(importacion.equals("S")&& cantImpo==1){
                            cantImpo++;
                            remesa_docto remImp= null;
                            try{
                                remImp=model.RemDocSvc.buscarImpoExpo(usuario.getDstrct(),rem.getTipo_doc(),rem.getDocumento());
                            }catch (SQLException e){
                                throw new ServletException(e.getMessage());
                            }
                            if(remImp!=null){
                                rem.setFecha_eta(remImp.getFecha_eta());
                                rem.setFecha_sia(remImp.getFecha_sia());
                                rem.setDescripcion_cga(remImp.getDescripcion_cga());
                                sw=true;
                                mensaje="Esta importacion ya existe, los datos no seran modificados en el despacho.";
                            }
                            if(request.getParameter("sia"+numero)!=null){
                                sw=false;
                                if(!request.getParameter("sia"+numero).equals("")){
                                    if(remImp==null|| (request.getParameter("numrem")!=null && numeroDocOri.equals(request.getParameter(parametro)))){
                                        rem.setFecha_eta(request.getParameter("eta"+numero));
                                        rem.setFecha_sia(request.getParameter("sia"+numero));
                                        rem.setDescripcion_cga(request.getParameter("descripcion"+numero));
                                        mensaje="Datos de la importacion modificados con exito!";
                                    }
                                }
                                else{
                                    mensaje="Todos los datos de la importacion deben ser digitados, de lo contrario "+
                                    "el documento no sera relacionado en el despacho.";
                                    sw=true;
                                }
                            }
                            else{
                                sw=true;
                            }
                            
                        }
                        if(cant++>1 && importacion.equals("S") ){
                            sw=true;
                            mensaje="Solo puede agregar un documento de importacion, Todos los datos de la nueva importacion digitada deben ser digitados, de lo contrario "+
                            "el documento no sera relacionado en el despacho.";
                            
                        }
                        else{
                            logger.info("Se agrego el documento "+rem.getDocumento());
                            vec.add(rem);
                        }
                        cant++;
                    }
                }
                
            }
            logger.info("El tamano del vector es "+vec.size());
            if(vec.size()<5){
                Vector vec1 = new Vector();
                for(int i=vec.size(); i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                    vec.add(rem);
                }
            }
            try{
                model.RemDocSvc.setDocs(vec);
                if(request.getParameter("numrem")!=null){
                    Vector consultas = new Vector();
                    if(!sw){
                        
                        String numre=request.getParameter("numrem");
                        consultas.add( model.RemDocSvc.borrarDocs(usuario.getDstrct(), request.getParameter("numrem")));
                        model.despachoService.insertar(consultas);
                        
                        Vector vecR = model.RemDocSvc.getDocs();
                        consultas = new Vector();
                        
                        if(vecR!=null){
                            consultas.add(model.RemDocSvc.borrarRelacion(usuario.getDstrct(),numre));
                            for(int i=0; i<vecR.size();i++){
                                remesa_docto remdoc = (remesa_docto) vecR.elementAt(i);
                                logger.info("Documento "+remdoc.getDocumento());
                                if(!remdoc.getDocumento().equals("")){
                                    
                                    remdoc.setCreation_user( usuario.getLogin());
                                    remdoc.setNumrem(numre);
                                    remdoc.setDstrct(usuario.getDstrct());
                                    remdoc.setCodcli("000"+request.getParameter("standard").substring(0,3));
                                    
                                    consultas.add(model.RemDocSvc.INSERTDOC(remdoc));
                                    
                                    
                                    boolean importacion = remdoc.getImportacion()!=null?remdoc.getImportacion().equals("S")?true:false:false;
                                    
                                    if(importacion){
                                        logger.info("ES IMPORTACION");
                                        remesa_docto remImp=model.RemDocSvc.buscarImpoExpo(usuario.getDstrct(),remdoc.getTipo_doc(),remdoc.getDocumento());
                                        if(remImp==null){
                                            consultas.add(model.RemDocSvc.insertImpoExpo(remdoc.getDstrct(), remdoc.getTipo_doc(),remdoc.getDocumento(),  remdoc.getCreation_user(),remdoc.getFecha_sia(),remdoc.getFecha_eta(),remdoc.getDescripcion_cga() ,remdoc.getCodcli()));
                                        }
                                        else{
                                            consultas.add(model.RemDocSvc.updateImpoExpo(remdoc.getDstrct(), remdoc.getTipo_doc(),remdoc.getDocumento(),  remdoc.getCreation_user(),remdoc.getFecha_sia(),remdoc.getFecha_eta(),remdoc.getDescripcion_cga()));
                                        }
                                        consultas.add(model.RemDocSvc.relacionImpoExpo(remdoc.getDstrct(),numre,remdoc.getTipo_doc(),remdoc.getDocumento(),remdoc.getCreation_user()));
                                    }
                                    
                                    boolean exportacion = remdoc.getExportacion()!=null?remdoc.getExportacion().equals("S")?true:false:false;
                                    if(exportacion){
                                        logger.info("ES EXPORTACION");
                                        remesa_docto remImp=model.RemDocSvc.buscarImpoExpo(usuario.getDstrct(),remdoc.getTipo_doc(),remdoc.getDocumento());
                                        if(remImp==null){
                                            consultas.add(model.RemDocSvc.insertImpoExpo(remdoc.getDstrct(), remdoc.getTipo_doc(),remdoc.getDocumento(),  remdoc.getCreation_user(),"0099-01-01 00:00:00","0099-01-01 00:00:00","" ,remdoc.getCodcli()));
                                        }
                                        consultas.add(model.RemDocSvc.relacionImpoExpo(remdoc.getDstrct(),numre,remdoc.getTipo_doc(),remdoc.getDocumento(),remdoc.getCreation_user()));
                                    }
                                }
                                
                            }
                            for(int i =0; i<consultas.size();i++){
                                String sql = (String)consultas.elementAt(i);
                                logger.info(sql);
                            }
                            model.despachoService.insertar(consultas);
                        }
                        next="/docremesas/ModificarDoc.jsp?cerrar=ok";
                    }
                    else{
                        next="/docremesas/ModificarDoc.jsp?mensaje=" +mensaje;
                        
                    }
                }
                else{
                    if(!sw){
                        next="/docremesas/AgregarDoc.jsp?cerrar=ok";
                    }
                    else{
                        
                        next="/docremesas/AgregarDoc.jsp?mensaje=" +mensaje;
                        
                    }
                    
                }
            }catch (SQLException e){
                throw new ServletException(e.getMessage());
            }
            //next="/docremesas/AgregarDocRel.jsp?cerrar=ok";
        }
        
        this.dispatchRequest(next);
    }
    
}
