/*
 * MigracionCambioChk.java
 *
 * Created on 21 de octubre de 2005, 11:10 AM
 */

package com.tsp.operation.controller;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
/**
 *
 * @author  JuanM
 */
public class MigracionCambioChkAction extends Action {
        
        /** Creates a new instance of MigracionCambioChk */
        public MigracionCambioChkAction() {
        }
        public void run() throws ServletException, com.tsp.exceptions.InformationException {
                try{
                        HttpSession Session           = request.getSession();
                        Usuario user                  = new Usuario();
                        user                          = (Usuario)Session.getAttribute("Usuario");
                        MigracionCambioCheque hilo = new MigracionCambioCheque();
                        
                        hilo.start(user.getLogin());
                        String Mensaje = "El proceso de migracion se ha iniciado ";
                        String next = "/migracion/migracionCambioChk.jsp?Mensaje="+Mensaje;
                        
                        RequestDispatcher rd = application.getRequestDispatcher(next);
                        if(rd == null)
                                throw new ServletException("No se pudo encontrar "+ next);
                        rd.forward(request, response);
                        
                        
                }catch(Exception e){
                        e.printStackTrace();
                        throw new ServletException("Error en MigracionCambioChkAction .....\n"+e.getMessage());
                }
        }
}
