/*
 * ReporteGeneracionCorridasAction.java
 *
 * Created on 29 de septiembre de 2006, 02:37 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  JDELAROSA
 */
public class ReporteGeneracionCorridasAction  extends Action{
    
    /** Creates a new instance of ReporteGeneracionCorridasAction */
    public ReporteGeneracionCorridasAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/cxpagar/corridas/PantallaFiltroExtracto.jsp";
        HttpSession session = request.getSession();
        Usuario usuario =  (Usuario) session.getAttribute("Usuario");
        String distrito =  (String) request.getParameter("distrito")!=null?request.getParameter("distrito"):"";
        String corrida  =  (String) request.getParameter("corrida")!=null?request.getParameter("corrida"):"";
        String sucursal =  (String) request.getParameter("cuenta")!=null?request.getParameter("cuenta"):"";
        String banco    =  (String) request.getParameter("banco")!=null?request.getParameter("banco"):"";
        String pagadas = request.getParameter("pagadas")!=null?request.getParameter("pagadas").toUpperCase():"";
        String aprobadas  = (request.getParameter ("aprobadas")!=null)?request.getParameter ("aprobadas"):"";
        String opcion  = request.getParameter ("opcion");
        
        try{
            if (model.cxpDocService.getEnproceso()){
                request.setAttribute ("mensaje","Hay un proceso en ejecucion. Una vez terminado podra realizar la generacion de un nuevo reporte...<br>Para realizar el seguimiento del actual proceso haga clic en el vinculo");
                next += "?ruta=PROCESOS/Seguimiento de Procesos";
            }
            else {
                model.cxpDocService.setEnproceso();
                ReporteGeneracionCorridasThreads hilo = new ReporteGeneracionCorridasThreads(model);
                hilo.start( usuario.getLogin(),distrito, corrida, banco, sucursal, aprobadas, pagadas );
                request.setAttribute ("mensaje","La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo");
                next += "?ruta=PROCESOS/Seguimiento de Procesos";
            }
        }catch (Exception ex){
            throw new ServletException("Error en Reporte_CorridasXLSAction......\n"+ex.getMessage());
        }
        this.dispatchRequest(next);  
    }
    
}
