/*
 JPACOSTA. Febrero 2014
 */
package com.tsp.operation.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AsignacionCarteraDAO;
import com.tsp.operation.model.DAOS.impl.AsignacionCarteraImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;

/**
 * @author jpacosta
 */
public class AsignacionCarteraAction extends Action {

    private final int CARGAR_DATOS_FILTROS = 0;
    private final int NEGOCIOS = 1;
    private final int ASIGNAR = 2;
    private final int FICHAS = 3;
    private final int RESUMEN = 4;
    private final int PREVISUALIZAR_FICHAS = 5;
    private AsignacionCarteraDAO dao;
    private String jSon = "{}";
    private String tipo = "application/json";
    private Usuario usuario;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            jSon = "{}";
            usuario = (Usuario) request.getSession().getAttribute ("Usuario");
            dao = new AsignacionCarteraImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_DATOS_FILTROS:
                    this.datosFiltros();
                    break;
                case NEGOCIOS:
                    this.negocios();
                    break;
                case ASIGNAR:
                    this.asignar();
                    break;
                case FICHAS:
                    this.getFichas();
                    break;
                case RESUMEN:
                    this.resumen();
                    break;
                case PREVISUALIZAR_FICHAS:
                    this.previsualizarFichas();
                    break;
            }

        } catch (Exception exc) {
        } finally {
            this.responser(tipo);
        }
    }

    /**
     * @return <PRE>
     * {
     *  "uNegocios": { id:valor, ... , id:valor },
     *  "periodos": { id:valor, ... , id:valor },
     *  "vencMayor": { id:valor, ... , id:valor },
     *  "asesores": { id:valor, ... , id:valor }
     * }
     * {
     *  "mensaje":valor
     * } </pre>
     */
    private void datosFiltros() {
        boolean unidades = ("true".equals(request.getParameter("unidades")));
        boolean agencias = ("true".equals(request.getParameter("agencias")));
        boolean periodos = ("true".equals(request.getParameter("periodos")));
        boolean vencimientos = ("true".equals(request.getParameter("vencimiento")));
        boolean asesores = ("true".equals(request.getParameter("asesores")));
        boolean agentes = ("true".equals(request.getParameter("agentes")));
        jSon = dao.getFiltros(unidades, agencias, periodos, vencimientos, asesores, agentes);
    }

    /**
     * @return <PRE>
     * {
     *  "asignados":{cantidad:valor, negocios:valor, valor:valor},
     *  "rows":{ ... }
     * } </PRE>
     */
    private void negocios() {
        String tipo = ("undefined".equals(request.getParameter("tipo")))
                ? "vacio" : request.getParameter("tipo");
        String unidad_negocio = ("undefined".equals(request.getParameter("unegocio")))
                ? "vacio" : request.getParameter("unegocio");
        String rangoMayor = ("undefined".equals(request.getParameter("rangoMayor")))
                ? "vacio" : request.getParameter("rangoMayor");
        String periodo = ("undefined".equals(request.getParameter("periodo")))
                ? "vacio" : request.getParameter("periodo");
        String reasig = ("undefined".equals(request.getParameter("reasigna")))
                ? "vacio" : request.getParameter("reasigna");
        String agencia = ("undefined".equals(request.getParameter("agencia")))
                ? "vacio" : request.getParameter("agencia");
        jSon = (tipo.equals("vacio") || unidad_negocio.equals("vacio") || periodo.equals("vacio")) 
               ? "{mensaje:'error en los parametros'}"
               : dao.getNegocios(tipo, unidad_negocio, rangoMayor, periodo, reasig, agencia);
    }

    /**
     * @return <PRE>
     * {
     *  cod_neg:{cuotas:valor, valor_asignado:valor , valor}
     * }
     * {
     *  "mensaje":valor
     * } </PRE>
     */
    private void asignar() {
        String tipo = ("undefined".equals(request.getParameter("tipo")))
                ? "vacio" : request.getParameter("tipo");
        String periodo = request.getParameter("periodo");
        String asesor = ("vacio".equals(request.getParameter("asesor")))
                ? "" : request.getParameter("asesor");
        String negocios = request.getParameter("negocios");
        String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
        jSon = (tipo.equals("vacio")) 
               ? "{mensaje:'error en los parametros'}"
               : dao.asignar(tipo, periodo, asesor, usuario, negocios);
    }

    private void getFichas() {
        String periodo = request.getParameter("periodo");
        String asesor = ("vacio".equals(request.getParameter("asesor")))
                ? "" : request.getParameter("asesor");
        String vencimiento = ("vacio".equals(request.getParameter("venci")))
                ? "" : request.getParameter("venci");
        String unegocio = ("vacio".equals(request.getParameter("negocio")))
                ? "" : request.getParameter("negocio");
        String campoOrden = ("vacio".equals(request.getParameter("campoOrden")))
                ? "" : request.getParameter("campoOrden");
        String criterio = ("vacio".equals(request.getParameter("criterio")))
                ? "" : request.getParameter("criterio"); 
        String agencia = ("undefined".equals(request.getParameter("agencia")))
                ? "vacio" : request.getParameter("agencia");
        
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        
        String filtro  = "";
        if (campoOrden.isEmpty()) {
            filtro = "JOIN (VALUES";
            
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);

                if (asJsonArray.size() - 1 == i) {
                    filtro += "('" + objects.get(campoOrden).getAsString() + "'," + i + ")";
                } else {
                    filtro += "('" + objects.get(campoOrden).getAsString() + "'," + i + "),";
                }
            }
            
            filtro += ") AS V (cod, ordering) ON nfi." + campoOrden +" = V.cod";
        } else {
            filtro = "AND nfi.cod_neg in (";
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);

                if (asJsonArray.size() - 1 == i) {
                    filtro += "'" + objects.get("cod_neg").getAsString() + "')";
                } else {
                    filtro += "'" + objects.get("cod_neg").getAsString() + "',";
                }
            }
        }                
        jSon = dao.getFichas(usuario, filtro, asesor, campoOrden +" "+ criterio, agencia);
    }
    
    private void resumen() {
        String tipo = ("undefined".equals(request.getParameter("tipo")))
                ? "vacio" : request.getParameter("tipo");
        String periodo = request.getParameter("periodo");
        String unegocio = ("vacio".equals(request.getParameter("unegocio")))
                ? "" : request.getParameter("unegocio");
        jSon = (tipo.equals("vacio") || unegocio.equals("")) 
               ? "{mensaje:'error en los parametros'}"
               :dao.resumen(tipo, periodo, unegocio);        
    }
    /**
     * @param tipo <PRE> "application/json" | "text/plain" </PRE>
     */
    private void responser(String tipo) {
        try {
            response.setContentType(tipo + "; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(jSon);
        } catch (Exception e) {
        }
    }
    
    private void previsualizarFichas(){
        String periodo = request.getParameter("periodo");
        String asesor = ("vacio".equals(request.getParameter("asesor")))
                ? "" : request.getParameter("asesor");
        String vencimiento = ("vacio".equals(request.getParameter("venci")))
                ? "" : request.getParameter("venci");
        String unegocio = ("vacio".equals(request.getParameter("negocio")))
                ? "" : request.getParameter("negocio");
        String agencia = ("undefined".equals(request.getParameter("agencia")))
                ? "vacio" : request.getParameter("agencia");
        
        String juridica = (request.getParameter("juridica") != null ? request.getParameter("juridica") : "");
        String ciclo=(request.getParameter("ciclo") != null ? request.getParameter("ciclo") : "");
        
        jSon=dao.previsualizarFichas(periodo, asesor, vencimiento, unegocio, juridica, ciclo,usuario, agencia);    
    
    }

}