/****************************************************************************
* Nombre .................Concepto_equipoAnularAction.java                  *
* Descripci�n.............Accion para modificar un concepto equipo a la bd. *
* Autor...................Ing. Jose de la rosa                              *
* Fecha Creaci�n..........16 de diciembre de 2005, 10:00 PM                 *
* Modificado por..........LREALES                                           *
* Fecha Modificaci�n......7 de junio de 2006, 02:31 PM                      *
* Versi�n.................1.0                                               *
* Coyright................Transportes Sanchez Polo S.A.                     *
*****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Concepto_equipoAnularAction extends Action{
    
    /** Creates a new instance of Concepto_equipoAnularAction */
    public Concepto_equipoAnularAction () {
    }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        
        HttpSession session = request.getSession();        
        Usuario usuario = ( Usuario ) session.getAttribute("Usuario");
        String dstrct = ( String ) session.getAttribute("Distrito");
        String codigo = request.getParameter("c_codigo").toUpperCase ();
        
        try{
            
            Concepto_equipo concepto = new Concepto_equipo();
            
            concepto.setCodigo ( codigo );
            concepto.setUsuario_modificacion( usuario.getLogin().toUpperCase() );
            
            model.concepto_equipoService.anularConcepto_equipo ( concepto );
            
        } catch ( SQLException e ){
            
            throw new ServletException( e.getMessage() );
            
        }
        
        this.dispatchRequest( next );  
        
    }    
    
}