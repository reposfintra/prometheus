/********************************************************************
 *      Nombre Clase.................   BuscarDestinatariosAction.java
 *      Descripci�n..................   Busca un vector de destinatarios en el despacho
 *      Autor........................   Karen Reales
 *      Fecha........................   04.04.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class BuscarDestinatariosAction extends Action{
    static Logger logger = Logger.getLogger(BuscarDestinatariosAction.class);
    /** Creates a new instance of DocumentoInsertAction */
    public BuscarDestinatariosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next  = "/colpapel/destinatarios.jsp";
        
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            if(request.getParameter("CMDdocumentos")==null){
                if(model.stdjobdetselService.getStandardDetSel()!=null){
                    
                    Stdjobdetsel sj = model.stdjobdetselService.getStandardDetSel();
                    String sj1 = sj.getSj();
                    model.remidestService.searchCiudades(sj1.substring(0,3));
                    String cliente1=sj1.substring(0,3);
                    String nombre = request.getParameter("nombre");
                    
                    if(request.getParameter("ciudad")!=null){
                        Remesa r=model.remesaService.getRemesa();
                        if(r!=null){
                            model.remidestService.searchListaD(r.getStd_job_no().substring(0,3), request.getParameter("ciudad"),nombre);
                        }
                    }
                    else{
                        model.remidestService.setVectorRemiDest(null);
                        Remesa r = new Remesa();
                        r.setStdJobNo(sj1);
                        if(request.getParameter("ocargue")==null){
                            r.setTipo_documento("002");
                            r.setNumrem(request.getParameter("numrem"));
                            model.remidestService.listaDestinatariosPla(request.getParameter("numrem"));
                            model.remesaService.setRemesa(r);
                            
                        }
                        else{
                            r.setNumrem(request.getParameter("ocargue"));
                            r.setTipo_documento("OCARGUE");
                            if(request.getParameter("modif")!=null){
                                r.setEstado("M");
                            }
                            model.remesaService.setRemesa(r);
                            if(request.getParameter("numrem")==null){
                                String destinatarios =request.getParameter("destinatario");
                                if(destinatarios!=null){
                                    String vecdes[] = destinatarios.split(",");
                                    Vector dest = new Vector();
                                    for(int i=0; i<vecdes.length;i++){
                                        String dato = vecdes[i];
                                        model.remidestService.searchRemiDest(dato);
                                        dest.add(model.remidestService.getRd());
                                    }
                                    model.remidestService.setVectorRemiDest(dest);
                                }
                                if(request.getParameter("despacho")!=null){
                                    r.setTipo_documento("REM");
                                }
                            }
                            else{
                                model.remidestService.listaDestinatariosPla(request.getParameter("numrem"));
                            }
                        }
                    }
                    
                }
            }
            else{
                next = "/docremesas/ModificarDocRel.jsp";
                String destinatario=request.getParameter("destinatario");
                String nombre=request.getParameter("nombre");
                
                try{
                    
                    if(model.remesaService.getRemesa()!=null){
                        String numrem=model.remesaService.getRemesa().getNumrem();
                        logger.info("La remesa existe y es de tipo "+model.remesaService.getRemesa().getTipo_documento());
                        if(model.remesaService.getRemesa().getTipo_documento().equals("002")){
                            model.RemDocSvc.LISTVC(numrem, destinatario);
                        }
                        else{
                            model.RemDocSvc.listarDocOCargue(numrem,destinatario);
                            logger.info("Tama�o del vector encontrado "+model.RemDocSvc.getVec());
                        }
                        
                    }
                    
                }catch (Exception ex){
                    throw new ServletException(ex.getMessage());
                }
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
