/*
 * ReporteDiferenciaTiempoAction.java
 *
 * Created on 16 de junio de 2006, 08:47 AM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;

public class ReporteDiferenciaTiempoAction extends Action{
    
    /** Creates a new instance of ReporteDiferenciaTiempoAction */
    public ReporteDiferenciaTiempoAction () {
    }
    
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String next = "";
        String fecha_ini = request.getParameter ("c_fecha_ini");
        String fecha_fin = request.getParameter ("c_fecha_fin");
        String cliente = request.getParameter ("c_cliente");
        try{
            if(model.clienteService.existeCliente (cliente)){
                next = "/jsp/masivo/reportes/reporteDiferenciaTiempo.jsp?msg=Su reporte ha iniciado y se encuentra en el log de procesos";
                ReporteDieferenciaTiempoThreads hilo = new ReporteDieferenciaTiempoThreads ();
                hilo.start (fecha_ini, fecha_fin, cliente, usuario.getLogin ());
            }
            else
                next = "/jsp/masivo/reportes/reporteDiferenciaTiempo.jsp?msg=El cliente con codigo "+cliente+" no existe en la BD";
        }catch (Exception ex){
            throw new ServletException ("Error en ReporteDiferenciaTiempos .....\n"+ex.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
