/******
 * Nombre:        Jerarquia_tiempoSerchAction.java            
 * Descripci�n:   Clase Action para buscar jerarquia tiempo   
 * Autor:         Ing. Jose de la Rosa 
 * Fecha:         26 de junio de 2005, 04:06 PM                         
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 *******/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class Jerarquia_tiempoSerchAction extends Action{
    
    /** Creates a new instance of Jerarquia_tiempoSerchAction */
    public Jerarquia_tiempoSerchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession();        
        String listar = (String) request.getParameter("listar");
        String actividad = request.getParameter("c_actividad");
        String responsable = request.getParameter("c_responsable");
        String demora = request.getParameter("c_demora");
        try{                
            if (listar.equals("True")){                
                next="/jsp/trafico/jerarquia/Jerarquia_tiempoListar.jsp";                    
                Vector jerarquia_tiempos = model.jerarquia_tiempoService.searchDetalleJerarquia_tiempos(actividad,responsable,demora);               
                request.setAttribute("jerarquia_tiempos",jerarquia_tiempos);
            }
            else{
                model.jerarquia_tiempoService.serchJerarquia_tiempo(actividad,responsable,demora);
                Jerarquia_tiempo jerarquia_tiempo = model.jerarquia_tiempoService.getJerarquia_tiempo();
                request.setAttribute("jerarquia_tiempo",jerarquia_tiempo);
                model.tablaGenService.buscarResponsableActividad();
                model.tablaGenService.buscarCausaDemora();
                model.actividadSvc.listarActividad();
                if(model.jerarquia_tiempoService.existJerarquia_tiempo(actividad,responsable,demora)){
                    next="/jsp/trafico/jerarquia/Jerarquia_tiempoModificar.jsp?sw=1"+1;
                }
                else{
                    next="/jsp/trafico/jerarquia/Jerarquia_tiempoModificar.jsp?sw="+0;
                }
                
            }                                    
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    
    }
    
}
