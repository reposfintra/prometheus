/*
 * ProveedoresInsertAction.java
 *
 * Created on 21 de abril de 2005, 09:34 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class ProveedoresInsertAction  extends Action{
    
    /** Creates a new instance of ProveedoresInsertAction */
    public ProveedoresInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next ="/proveedores/proveedorInsert.jsp";
        String nit         = request.getParameter("nit");
        String sucursal    = request.getParameter("sucursal").toUpperCase();
        String dist        = request.getParameter("distrito");
        String ciudad      = request.getParameter("ciudad");
        String codmigracion    = request.getParameter("codigo_m");
        String moneda   = request.getParameter("moneda");
        float valor    = Float.parseFloat(request.getParameter("valor"));
        float porcentaje = Float.parseFloat(request.getParameter("porcentaje"));
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        
        Proveedores prov = new Proveedores();
        prov.setDstrct(dist);
        prov.setCity_code(ciudad);
        prov.setTotale(valor);
        prov.setNit(nit);
        prov.setSucursal(sucursal);
        prov.setMoneda(moneda);
        prov.setPorcentaje(porcentaje);
        prov.setmigracion(codmigracion);
        prov.setCreation_user(usuario.getLogin());
        request.setAttribute("error","ECE0D8" );
        request.setAttribute("error1","ECE0D8" );
        try{
            
            model.proveedoresService.searchProveedor(nit, sucursal);
            if(model.proveedoresService.getProveedor()!=null){
                request.setAttribute("error","#cc0000" );
                request.setAttribute("error1","#cc0000" );
                next= "/proveedores/proveedorInsertError.jsp";
            }
            else{
                if(!model.proveedoresService.existNit(nit)){
                    request.setAttribute("error","#cc0000" );
                    next= "/proveedores/proveedorInsertError.jsp";
                }
                else{
                    model.proveedoresService.setProveedor(prov);
                    model.proveedoresService.insertProveedor(usuario.getBase());
                }
                 
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
}
