/*
 * PlacaInsertAction.java
 *
 * Created on 4 de noviembre de 2004, 02:48 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  FFERNANDEZ
 */
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import org.apache.log4j.Logger;

public class TrasladoAnularAction extends Action{
    
    //  static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public TrasladoAnularAction () {
    }
    
    public void run () throws ServletException, InformationException {
   
        String next = "/jsp/finanzas/contab/comprobante/traslados/traslado_modificar.jsp?reload=ok";
        BeanGeneral traslado = new BeanGeneral ();
        String msg = "";        
        
        
        
        String bori = request.getParameter ("bori")!=null?request.getParameter ("bori"):"";
        String bdes = (request.getParameter("bdes")!=null)?request.getParameter ("bdes"):"";
        String sucori = (request.getParameter("sucori")!=null)?request.getParameter ("sucori"):"";
        String sucdes = (request.getParameter("sucdes")!=null)?request.getParameter ("sucdes"):"";
        String val = (request.getParameter("val")!=null)?request.getParameter ("val"):"";
        String fec = (request.getParameter("fec")!=null)?request.getParameter ("fec"):"0099-01-01";
        String comprobante = (request.getParameter("comprobante")!=null)?request.getParameter ("comprobante"):"";
            traslado.setValor_01 (bori);
            traslado.setValor_02 (sucori);
            traslado.setValor_03 (bdes);
            traslado.setValor_04(sucdes);
            traslado.setValor_05(val);
            traslado.setValor_06(fec);

            
            try{
               
                model.trasladoService.AnularTraslado(traslado); 
                model.trasladoService.AnularComprobanteYcomprodec(comprobante);
                next += "&msg=" + "El traslado bancario fue eliminado exitosamente";
                    
            }
            catch (Exception e){
                e.printStackTrace ();
                throw new ServletException (e.getMessage ());
            }
        
        // Redireccionar a la p�gina indicada.
         this.dispatchRequest (next);
    }
}
