/**************************************************************************
 * Nombre clase: DescuentoTercmAction.java                                 *
 * Descripci�n: Clase Recoge los parametros de la jsp y se los envia a el  *
 * Service para procesar los datos                                         *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 22 de Diciembre de 2005, 08:20 AM                     *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author  Ivan Dario Gomez
 */
public class DescuentoTercmAction extends Action {
    
    /** Creates a new instance of DescuentoTercmAction */
    public DescuentoTercmAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String next="";
        String OC     = request.getParameter("OC");
        String Opcion = request.getParameter("Opcion");
        String[] boletas = new String[11];
        String CC   = "";
        String foto = "";
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");
        String NumPrefactura = request.getParameter("NumPrefactura");
        
        try{
            if(Opcion.equals("buscar")){
                
                model.DescuentoTercmSvc.Verificar(OC);
                List Existe = model.DescuentoTercmSvc.getExiste();
                Iterator Exi = Existe.iterator();
                if(!Exi.hasNext()){
                    model.DescuentoTercmSvc.BuscarValor(OC);
                    model.DescuentoTercmSvc.Valor();
                    next = "/jsp/sot/body/PantallaCreacionBoleta.jsp";
                }else{
                    next = "/jsp/sot/body/PantallaDescuentoTERCM.jsp?IMP=Existe";
                }
            }
            if(Opcion.equals("BuscarParaImprimir")){
                String OC1 = request.getParameter("OCIMPRIMIR");
                model.DescuentoTercmSvc.VerificarImpresa(OC1);
                List ExisteOC = model.DescuentoTercmSvc.getExisteImpresa();
                Iterator iterator = ExisteOC.iterator();
                if(iterator.hasNext()){
                    model.DescuentoTercmSvc.BuscarValor(OC1);
                    List val = model.DescuentoTercmSvc.getListado();
                    Iterator iter = val.iterator();
                    if(iter.hasNext()){
                        DescuentoTercm  te   = (DescuentoTercm)iter.next();
                        int Valor = te.getValor();
                        model.DescuentoTercmSvc.BuscarBoleta(OC1);
                        next = "/jsp/sot/body/PantallaCreacionBoleta.jsp?IMP=OK&Total="+Valor;
                    }
                    
                }else{
                    next = "/jsp/sot/body/PantallaDescuentoTERCM.jsp?IMP=ExisteImpresa";
                }
            }
            
            if(Opcion.equals("guardar")){
                boletas[0]  = (request.getParameter("Boleta1")!="")? request.getParameter("Boleta1"):"";
                boletas[1]  = (request.getParameter("Boleta2")!="")? request.getParameter("Boleta2"):"";
                boletas[2]  = (request.getParameter("Boleta3")!="")? request.getParameter("Boleta3"):"";
                boletas[3]  = (request.getParameter("Boleta4")!="")? request.getParameter("Boleta4"):"";
                boletas[4]  = (request.getParameter("Boleta5")!="")? request.getParameter("Boleta5"):"";
                boletas[5]  = (request.getParameter("Boleta6")!="")? request.getParameter("Boleta6"):"";
                boletas[6]  = (request.getParameter("Boleta7")!="")? request.getParameter("Boleta7"):"";
                boletas[7]  = (request.getParameter("Boleta8")!="")? request.getParameter("Boleta8"):"";
                boletas[8]  = (request.getParameter("Boleta9")!="")? request.getParameter("Boleta9"):"";
                boletas[9]  = (request.getParameter("Boleta10")!="")?request.getParameter("Boleta10"):"";
                
                int Total    = Integer.parseInt(request.getParameter("total"));
                
                model.DescuentoTercmSvc.Verificar(OC);
                List Exis = model.DescuentoTercmSvc.getExiste();
                Iterator Itera = Exis.iterator();
                if(!Itera.hasNext()){
                    model.DescuentoTercmSvc.BuscarValor(OC);
                    List listado = model.DescuentoTercmSvc.getListado();
                    Iterator It = listado.iterator();
                    if(It.hasNext()){
                        DescuentoTercm  tercm   = (DescuentoTercm)It.next();
                        if(Total == tercm.getValor()){
                            model.DescuentoTercmSvc.GuardarBoletas(OC, tercm.getPlaca(), tercm.getCedula(), boletas, tercm.getValor(),usuario.getLogin());
                            model.DescuentoTercmSvc.BuscarBoleta(OC);
                            next = "/jsp/sot/body/PantallaCreacionBoleta.jsp?IMP=OK&Total="+Total;
                        }
                    }
                }else{
                    next = "/jsp/sot/body/PantallaCreacionBoleta.jsp?IMP=Existe";
                }
            }
            
            if(Opcion.equals("imprimir")){
                model.DescuentoTercmSvc.BuscarValor(OC);
                model.DescuentoTercmSvc.BuscarBoleta(OC);
                
                List listaImp = model.DescuentoTercmSvc.getListaImpresion();
                Iterator Ite = listaImp.iterator();
                if(Ite.hasNext()){
                    DescuentoTercm desc = (DescuentoTercm) Ite.next();
                    int Ced = desc.getCedula();
                    CC = String.valueOf(Ced);
                }
                model.ImagenSvc.searchImagen(null,null,"1",null,null,null,null, null,
                CC,null,null,null,null,usuario.getLogin());
                
                List lista = model.ImagenSvc.getImagenes();
                if (lista!=null){
                    Imagen img  = (Imagen) lista.get(0);
                    foto = request.getContextPath()+"/documentos/imagenes/"+ usuario.getLogin() +"/"+img.getNameImagen();
                }
                
                next = "/jsp/sot/body/PantallaImprimirBoleta.jsp?foto="+foto;
                
            }
            
            if(Opcion.equals("Cambiar")){
                model.DescuentoTercmSvc.UpdateFechaImpresion(OC);//Actualiza la fecha de impresion
                next = "/jsp/sot/body/PantallaDescuentoTERCM.jsp";
            }
            /****************************************************/
            /**************Creacion de Prefactura ***************/
            if(Opcion.equals("listar")){
                model.DescuentoTercmSvc.PreFactura(fechaini,fechafin);
                next = "/jsp/sot/body/PantallaPreFacturaTERCM.jsp?OP=Listar";
            }
            
            if(Opcion.equals("xls")){
                model.DescuentoTercmSvc.Prefacturar(fechaini,fechafin);
                NumPrefactura = Util.getFechaActual_String(Util.FORMATO_YYYYMMDD);
                HListarPrefacturaTercm hilo = new HListarPrefacturaTercm();
                hilo.init(model, usuario, NumPrefactura);
                
                next = "/jsp/sot/body/PantallaPreFacturaTERCM.jsp?OP=mensage";
            }
            /******************************************************/
            /****************************************************/
            /**************Creacion de FACTURA ***************/
            if(Opcion.equals("BuscarPrefactura")){
                String NunPrefactura = request.getParameter("NumPrefactura");
                model.DescuentoTercmSvc.BuscarPreFactura(NunPrefactura);
                next = "/jsp/sot/body/PantallaFacturaTERCM.jsp?OP=Listar";
            }
            if(Opcion.equals("marcar")){
                String exportar = request.getParameter("exportar");
                if(exportar.equals("NO")){
                    String[] NumBoletas = request.getParameterValues("selec");
                    model.DescuentoTercmSvc.UpdatePrefactura(NumBoletas);
                    model.DescuentoTercmSvc.BuscarPreFactura(NumPrefactura);
                    next = "/jsp/sot/body/PantallaFacturaTERCM.jsp?OP=Listar";
                    
                }else{
                    HListarPrefacturaTercm hilo = new HListarPrefacturaTercm();
                    hilo.init(model, usuario, NumPrefactura);
                    next = "/jsp/sot/body/PantallaFacturaTERCM.jsp?OP=mensage&msg=El archivo fue enviado al log de procesos... ";
                }
            }
            
            /******************************************************/
            
           /*
            *   Anulacion De Boletas
            */
            
            if(Opcion.equals("ListarAnular")){
                model.DescuentoTercmSvc.BuscarBoletaAnular(OC);//busca las boletas de la oc para anularlas
                next = "/jsp/sot/body/PantallaAnulacionBoletas.jsp?OP=Listar";
            }
            
            if(Opcion.equals("Anular")){
                model.DescuentoTercmSvc.AnularBoletas(OC, usuario.getLogin());//busca las boletas de la oc para anularlas
                next = "/jsp/sot/body/PantallaAnulacionBoletas.jsp?OP=mensage";
            }
        }catch (Exception ex){
            throw new ServletException("Error en DescuentoTercmAction[CONTROLLER] .....\n"+ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
