/*
 * Nombre        BuscarTramosAction.java
 * Autor         Ing Jesus Cuestas
 * Fecha         25 de junio de 2005, 10:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class BuscarTramosAction extends Action{
    
    /** Creates a new instance of BuscarTramosAction */
    public BuscarTramosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        
        HttpSession session = request.getSession();
        String dstrct = (String)session.getAttribute("Distrito"); 
        
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=ok";
        
        try{
            int sw = Integer.parseInt(request.getParameter("sw"));
            int sw1 = 0;
            Vector tramos = null;
            
            if(sw==1){//ver todos los tramos
                model.tramoService.listarTramos();
                if (model.tramoService.getTramos().size() == 0){
                    sw1 = 1;
                }
            }
            else if(sw==2){//tramos x busqueda
                String cia = request.getParameter("c_cia").toUpperCase();
                String origen = request.getParameter("c_origen").toUpperCase();
                String destino = request.getParameter("c_destino").toUpperCase();
                String combustible =request.getParameter("c_combustible");
                String distancia =request.getParameter("c_distancia");
                String duracion =request.getParameter("c_duracion");
                String paiso =request.getParameter("paiso").toUpperCase();
                String paisd =request.getParameter("paisd").toUpperCase();
                
                Tramo t = new Tramo();
                t.setDstrct(cia);
                t.setOrigen(origen);
                t.setDestino(destino);
                t.setScombustible(combustible);
                t.setSdistancia(distancia);
                t.setSduracion(duracion);
                t.setPaiso(paiso);
                t.setPaisd(paisd);
                
                model.tramoService.setTramo(t);
                model.tramoService.consultarTramo();
                
                if (model.tramoService.getTramos().size() == 0){
                    sw1 = 1;
                }
                
            }
            else if(sw==3){//obtener tramo
                String cia = request.getParameter("cia");
                String origen = request.getParameter("origen");
                String destino = request.getParameter("destino");
                model.tramoService.buscarTramo(cia, origen, destino);
                model.unidadTrfService.listarUnidadesxDistancia("D");
                model.unidadTrfService.listarUnidadesxTiempo("T");
                model.unidadTrfService.listarUnidadesxVolumen("V");
                model.tiketService.listarTramos_Ticket(cia,origen,destino);               
            }
            else if(sw==4){//buscar tiket
                String cia = request.getParameter("c_cia");
                String origen = request.getParameter("c_origen");
                String destino = request.getParameter("c_destino");
                String tiket_id = request.getParameter("tiket");
                model.tiketService.buscarTramo_Ticket(cia, origen, destino, tiket_id);
            }
            else if (sw == 5){//obtener tiquete
                next = "/jsp/trafico/tramo/ingresarTiket.jsp?c_origen="+request.getParameter("origen")+
                        "&c_destino=" +request.getParameter("destino") + "&c_cia=" + request.getParameter("ccia") + 
                        "&c_dcia=" + request.getParameter("ncia") + "&c_norigen="+request.getParameter("norigen") +
                        "&c_ndestino=" + request.getParameter("ndestino"); 
                model.tiketService.listarPeajes(dstrct);
            }
            
            if (sw1 == 1){
                next = "/jsp/trafico/tramo/buscarTramo.jsp?mensaje=Su b�squeda no arroj� resultados!";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
