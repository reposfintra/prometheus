/*
 * FacturaModificarAction.java
 *
 * Created on 22 de mayo de 2006, 23:02
 */
package com.tsp.operation.controller;

/**
 *
 * @author  AMARTINEZ
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.*;

public class FacturaModificarAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    private String  TIPO_FAC_CL = "FAC";
    
    /** Creates a new instance of FacturaModificarAction */
    public FacturaModificarAction() {
    }
    public void run() throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute("Distrito");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct   = usuario.getDstrct();
        String base = usuario.getBase();
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        model.facturaService.setIncluirCumplidas("a.reg_status = 'C'");
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String actual = format.format(fecha);
        
        
        String busqueda = (request.getParameter("busqueda")!=null)?request.getParameter("busqueda"):"";
        String aceptar = (request.getParameter("aceptar")!=null)?request.getParameter("aceptar"):"";
        String regresar = (request.getParameter("regresar")!=null)?request.getParameter("regresar"):"";
        String cargar = (request.getParameter("cargar")!=null)?request.getParameter("cargar"):"";
        String recargar = (request.getParameter("recargar")!=null)?request.getParameter("recargar"):"";
        String eliminar = (request.getParameter("eliminar")!=null)?request.getParameter("eliminar"):"";
        String modificar = (request.getParameter("modificar")!=null)?request.getParameter("modificar"):"";
        String cmc= (request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"";
        
        String cargarcodigo = (request.getParameter("cargarcodigo")!=null)?request.getParameter("cargarcodigo"):"";
        
        /*************************************
         *
         ************************************/
        String cliente  = (request.getParameter("codcli")!=null)?request.getParameter("codcli"):"";
        String Agregar= (request.getParameter("Agregar")!=null)?request.getParameter("Agregar"):"";
        String impresion= (request.getParameter("impresion")!=null)?request.getParameter("impresion"):"";
        
        String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
        
        String next="/jsp/cxcobrar/facturacion/IniciofacturacionModif.jsp";
        try{
            if(opcion.equals("ELIMINAR_ITEM")){
               
                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp";
                String numrem = (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
                Vector remesas_eliminadas = new Vector(); 
                Vector remesas_facturar = model.facturaService.getRemesasFacturar();
                for(int i=0 ;i< remesas_facturar.size();i++){
                    factura_detalle factu_deta = (factura_detalle)remesas_facturar.get(i);
                    if(factu_deta.getNumero_remesa().equals(numrem)){
                        remesas_eliminadas.add(factu_deta);
                        remesas_facturar.remove(i);
                    }
                }
                model.facturaService.setRemesasEliminadas(remesas_eliminadas);
                System.out.println("REMESA SIZE ACTIONNNNNNNNNN------>" + model.facturaService.getRemesasEliminadas().size());
                
            }
            if(busqueda.equals("modif")){
                String che1 =(request.getParameter("checkbox1")!=null)?request.getParameter("checkbox1"):"no";
                String che2 =(request.getParameter("checkbox2")!=null)?request.getParameter("checkbox2"):"no";
                String che3 =(request.getParameter("checkbox3")!=null)?request.getParameter("checkbox3"):"no";
                
                logger.info("? che1: " + che1);
                logger.info("? che2 (Impresas): " + che2);
                logger.info("? che3 (No Impresas): " + che3);
                
                next="/jsp/cxcobrar/facturacion/IniciofacturacionModif.jsp?";
                String query="";
                if(che1.equals("")){
                    String rango1 =(request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                    String rango2 =(request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                    query+=" AND (a.fecha_factura between '"+rango1+"' and '"+rango2+"') ";
                    next+="r1="+rango1+"&r2="+rango2;
                }
                if(che2.equals("")){
                    query+=" AND a.fecha_impresion != '0099-01-01 00:00:00'  ";
                    if(che1.equals(""))
                        next+="&che2=ok";
                    else
                        next+="che2=ok";
                }
                if(che3.equals("")){
                    query+=" AND a.fecha_impresion = '0099-01-01 00:00:00'  ";
                    if(che2.equals("") || che1.equals(""))
                        next+="&che3=ok";
                    else
                        next+="che3=ok";
                }
                
                logger.info("?query: " + query);
                
                
                /******* jescandon  - Modificacione 15-02-07********************************/
                model.tablaGenService.buscarRegistros("LIMFACCXC");
                List limiteFac      =   model.tablaGenService.obtenerTablas();
                TablaGen  obj       =   (TablaGen)limiteFac.get(0);
                if( obj != null  ){
                    String limite   =   obj.getTable_code();
                    next            +=  "&limite="+limite;
                }
                /**************************************************************************/
                
                
                
                String cod  = (request.getParameter("cod")!=null)?request.getParameter("cod"):"";
                
                if( !cod.equals("") ){
                    model.facturaService.buscarFacturas(dstrct, cod, query, usuario.getId_agencia());
                }
                else{
                    model.facturaService.buscarFacturasRangoFechas(dstrct, query, usuario.getId_agencia());
                }
                if(model.facturaService.getVFacturas()==null || model.facturaService.getVFacturas().size()==0)
                    next="/jsp/cxcobrar/facturacion/IniciofacturacionModif.jsp?Mensaje=No se encontro Facturas Relacionadas";
            }
            if(impresion.equals("modif")){
                next="/jsp/cxcobrar/facturacion/InicioImpresion.jsp";
                String cod  = (request.getParameter("codcli")!=null)?request.getParameter("codcli"):"";
                String fecha1  = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fecha2  = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                if(cod.equals("")){
                    model.facturaService.buscarFacturasImpresion2(dstrct, fecha1, fecha2);
                }else{
                    model.facturaService.buscarFacturasImpresion1(dstrct, cod, fecha1, fecha2);
                    
                }
                
                if(model.facturaService.getVFacturas()==null || model.facturaService.getVFacturas().size()==0)
                    next="/jsp/cxcobrar/facturacion/InicioImpresion.jsp?Mensaje=No se encontro Facturas Relacionadas";
            }
            if(eliminar.equals("ok")){
                //System.out.println("Aqui22");
                String factura  = (request.getParameter("factura")!=null)?request.getParameter("factura"):"";
                String codcli  = (request.getParameter("codcli")!=null)?request.getParameter("codcli"):"";
                try {
                    model.ingresoService.consultaFacturasIngresos(distrito, factura);
                }catch(Exception e){
                    e.printStackTrace();
                }
                if( model.ingresoService.getListadoingreso() != null && model.ingresoService.getListadoingreso().size() > 0 ){
                    next="/jsp/cxcobrar/facturacion/IniciofacturacionModif.jsp?Mensaje=No se pudo anular la factura:"+factura+" por que se encuentra registrada en los ingresos ";
                
                }
                
                else{
                    //System.out.println("RRR55 ");
                    next="/jsp/cxcobrar/facturacion/IniciofacturacionModif.jsp?Mensaje=Se elimino la factura:"+factura;
                    for(int i=0;i<model.facturaService.getVFacturas().size();i++){
                        factura fa = (factura) ((factura)model.facturaService.getVFacturas().get(i)).clonee();
                        if(fa.getFactura().equals(factura)){
                            
                            if(!fa.getFecha_impresion().equals("0099-01-01 00:00:00")){
                                Vector consultas = new Vector();
                                consultas.add( model.facturaService.anularFactura(distrito,fa.getTipo_documento(),factura, usuario.getLogin()) );//AMATURANA 28.03.2007
                                //jose 2007-01-22 descontabilizacion de factura
                                if(!fa.getFecha_contabilizacion().equals("0099-01-01 00:00:00")){
                                    String fecha_actual = Util.getFechaActual_String(6);
                                    int grupTrans = 0;
                                    com.tsp.finanzas.contab.model.Model modFin = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
                                    modFin.comprobanteService.getComprobante(fa.getTipo_documento(), fa.getFactura().toString(), fa.getTransaccion().intValue() );
                                    com.tsp.finanzas.contab.model.beans.Comprobantes comp = modFin.comprobanteService.getComprobante();
                                    double cred = 0, deb = 0;
                                    int grup = comp.getGrupo_transaccion();
                                    cred = comp.getTotal_credito();
                                    deb = comp.getTotal_debito();
                                    comp.setTotal_debito( cred );
                                    comp.setTotal_credito( deb );
                                    comp.setDstrct( distrito );
                                    comp.setSucursal( "OP" );
                                    comp.setFechadoc( Util.getFechaActual_String(4) );
                                    comp.setDetalle( "DESCONTABILIZACION CLIENTE FAC "+fa.getFactura() );
                                    comp.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
                                    comp.setGrupo_transaccion( modFin.comprobanteService.getGrupoTransaccion() );
                                    grupTrans = comp.getGrupo_transaccion();
                                    comp.setUsuario( usuario.getLogin() );
                                    comp.setBase( usuario.getBase() );
                                    modFin.comprobanteService.setComprobante( comp );
                                    consultas.add( modFin.comprobanteService.getInsert() );
                                    comp.setGrupo_transaccion( grup );
                                    consultas.add( model.facturaService.anularTransaccion( distrito, fa.getTipo_documento(), factura, grupTrans ) );
                                    modFin.comprobanteService.getDetallesComprobante(comp);
                                    for( int k=0; k < modFin.comprobanteService.getVector().size(); k++ ){
                                        com.tsp.finanzas.contab.model.beans.Comprobantes compVec = ( com.tsp.finanzas.contab.model.beans.Comprobantes )modFin.comprobanteService.getVector().get( k );
                                        compVec.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
                                        cred = 0; deb = 0;
                                        cred = compVec.getTotal_credito();
                                        deb = compVec.getTotal_debito();
                                        compVec.setTotal_debito( cred );
                                        compVec.setTotal_credito( deb );
                                        compVec.setGrupo_transaccion( grupTrans );
                                        compVec.setDetalle( "DESCONTABILIZACION CLIENTE FAC "+fa.getFactura() );
                                        compVec.setDocumento_interno( fa.getTipo_documento() );
                                        compVec.setUsuario( usuario.getLogin() );
                                        compVec.setBase( usuario.getBase() );
                                        modFin.comprobanteService.setComprobante( compVec );
                                        consultas.add( modFin.comprobanteService.getInsertItem() );
                                    }
                                }
                                boolean sw=false;
                                try {
                                    model.despachoService.insertar( consultas );
                                }catch(SQLException e){
                                    sw=true;
                                    next="/jsp/cxcobrar/facturacion/IniciofacturacionModif.jsp?Mensaje=No se pudo anular la factura:"+factura+" por el siguiente error "+e.getMessage();
                                }
                                if(!sw)
                                    model.facturaService.getVFacturas().remove(i);
                            }
                            else{
                                next="/jsp/cxcobrar/facturacion/IniciofacturacionModif.jsp?Mensaje=No se pudo anular la factura:"+factura+" por que no esta impresa";
                            }
                        }
                    }
                }
            }
            if(modificar.equals("ok")){
                
                String factura  = (request.getParameter("factura")!=null)?request.getParameter("factura"):"";
                String codcli  = (request.getParameter("codcli")!=null)?request.getParameter("codcli"):"";
                
                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp";
                //next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?";
                ////System.out.println(next);
                
                /****************************
                 * seteo todo a 0
                 ****************************/
                Vector a= new Vector();
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.setRemesasModificadas(a);
                model.facturaService.setRemesas(a);
                model.facturaService.setRemesasEliminadas(a);
                factura b = new factura();
                model.facturaService.setFactu(b);
                
                /**************************************
                 *buscarMoneda:Busca la moneda del distrito
                 *buscarCliente:Carga datos del cliente en el objeto factura del dao
                 *buscarRemesas:Cargar en el vector remesas todas las remesas cumplidas del cliente
                 ***************************************/
                
                model.facturaService.buscarMoneda(distrito);
                
                for(int i=0;i<model.facturaService.getVFacturas().size();i++){
                    factura fa = (factura) ((factura)model.facturaService.getVFacturas().get(i)).clonee();
                    
                    //factura fa = (factura)model.facturaService.getVFacturas().get(i);
                    if(fa.getCodcli().equals(codcli)&&fa.getFactura().equals(factura)){
                        ////System.out.println("moneda vfacturas::"+fa.getMoneda());
                        model.facturaService.buscarCliente(fa.getCodcli(),usuario.getId_agencia());
                        //String pla= model.facturaService.getFactu().getPlazo();
                        model.facturaService.setFactu(fa);
                        model.facturaService.getFactu().setZona(model.facturaService.getFactu().getHc());
                        //model.facturaService.getFactu().setPlazo(pla);
                        ////System.out.println("moneda model factu:"+model.facturaService.getFactu().getMoneda());
                        if(model.facturaService.getFactu().getMoneda().equals("")){
                            model.facturaService.getFactu().setMoneda(model.facturaService.getMonedaLocal());
                        }
                    }
                }
                model.facturaService.buscarRemesas(codcli,model.facturaService.getIncluirCumplidas(),usuario.getDstrct());
                //System.out.println("Agencia Fact:"+model.facturaService.getFactu().getAgencia_facturacion());
                
                /****************************
                 * se buscan las remesas facturadas
                 ****************************/
                model.facturaService.buscarFacturaDetalles(dstrct, factura);
                
                for(int i=0;i<model.facturaService.getRemesasFacturar().size();i++){
                    
                    factura_detalle fd = (factura_detalle)model.facturaService.getRemesasFacturar().get(i);
                    if(!fd.getAuxliliar().equals("")){
                        try{
                            modelcontab.subledgerService.buscarCuentasTipoSubledger(fd.getCodigo_cuenta_contable());
                            fd.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                        }catch(Exception e){
                            
                        }
                    }
                }
                //System.out.println("alert4:");
                try{
                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),model.facturaService.getMonedaLocal(), actual);
                    if(tasas==null){
                        //System.out.println("es nulo!!!");
                        model.facturaService.getFactu().setValor_tasa(new Double("0"));
                        
                    }
                    else
                        model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa()));
                }catch(Exception e){
                    e.printStackTrace();
                }
                
            }
            
            else if(cargarcodigo.equals("ok")){
                String c[] ={"",""};
                String x = (request.getParameter("x")!=null)?request.getParameter("x"):"";
                String campo = (request.getParameter("campo")!=null)?request.getParameter("campo"):"";
                
                String causa = (request.getParameter("causa")!=null)?request.getParameter("causa"):"";
                
                //System.out.println("cargarcodigo");
                /**************************************
                 * Obtiene los datos enviados desde el formulario
                 ***************************************/
                
                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String zona = (request.getParameter("zona")!=null)?request.getParameter("zona"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"0";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                
                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";
                
                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";
                
                String guardar =(request.getParameter("guardar")!=null)?request.getParameter("guardar"):"";
                String actualizar =(request.getParameter("actualizar")!=null)?request.getParameter("actualizar"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield"):"";
                
                String cs="";
                
                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?cargarCodigo=no&x="+x+"&campo="+campo;
                if(x.length()>=15 ){
                    c= model.facturaService.buscarplancuentas(x.substring(0,11));
                    if(!c[0].equals("")){
                        String s="";
                        s=model.facturaService.buscarsubledger(x.substring(0,11),x.substring(15,x.length()));
                        if(!s.equals("")){
                            cs="ok";
                            next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?cargarCodigo=ok&x="+x+"&campo="+campo;
                            if(c[1].equals("S")){
                                try{
                                    modelcontab.subledgerService.buscarCuentasTipoSubledger(x.substring(0,11));
                                    LinkedList tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                                    //System.out.println("------------------>"+tbltipo.size());
                                }catch(Exception o){
                                }
                            }
                            
                        }
                        else{
                            cs="no";
                            next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?cargarCodigo=no&x="+x+"&campo="+campo;
                        }
                    }else{
                        cs="no";
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?cargarCodigo=no&x="+x+"&campo="+campo;
                    }
                }
                else {
                    ////System.out.println("x:"+x);
                    
                    c= model.facturaService.buscarplancuentas(x);
                    if(!c[0].equals("")){
                        cs="ok";
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?cargarCodigo=ok&x="+x+"&campo="+campo;
                        if(c[1].equals("S")){
                            try{
                                modelcontab.subledgerService.buscarCuentasTipoSubledger(x);
                                
                            }catch(Exception o){
                                o.printStackTrace();
                            }
                        }
                    }else{
                        cs="no";
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?cargarCodigo=no&x="+x+"&campo="+campo;
                    }
                }
                
                if(causa.equals("pormoneda")){
                    try{
                        ////System.out.println("Moneda1:"+model.facturaService.getFactu().getMoneda()+" MONEDA2:"+moneda);
                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),moneda,model.facturaService.getMonedaLocal(), actual);
                        
                        tasa=""+tasas.getValor_tasa();
                        ////System.out.println("por moneda!!!:"+tasa);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
                
                model.facturaService.getFactu().setAgencia_facturacion(usuario.getId_agencia());
                
                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/
                
                if(tasa.equals("")){
                    tasa+="0";
                }
                
                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setZona(zona);
                
                
                model.facturaService.getFactu().setMoneda(moneda);
                ////System.out.println(" moneda factu---------->"+ model.facturaService.getFactu().getMoneda());
                
                String maxfila= (request.getParameter("numerofilas")!=null)?request.getParameter("numerofilas"):"";
                String facturacion= (request.getParameter("facturacion")!=null)?request.getParameter("facturacion"):"";
                String facturacionmodif= (request.getParameter("facturacionmodif")!=null)?request.getParameter("facturacionmodif"):"";
                
                ////System.out.println("maxfila:"+maxfila);
                Vector a = new Vector();
                
                /**************************************
                 * for para recorre todos los items del formulario
                 * y obtenerlos
                 ***************************************/
                
                
                for(int i =0; i<= Integer.parseInt(maxfila); i++){
                    
                    String textfield11= (request.getParameter("textfield11"+i)!=null)?request.getParameter("textfield11"+i):"";
                    String textfield12= (request.getParameter("textfield12"+i)!=null)?request.getParameter("textfield12"+i):"";
                    String textfield13= (request.getParameter("textfield13"+i)!=null)?request.getParameter("textfield13"+i):"";
                    String textfield14= (request.getParameter("textfield14"+i)!=null)?request.getParameter("textfield14"+i):"";
                    String textfield15= (request.getParameter("textfield15"+i)!=null)?request.getParameter("textfield15"+i):"";
                    String textfield16= (request.getParameter("textfield16"+i)!=null)?request.getParameter("textfield16"+i):"";
                    String textfield17= (request.getParameter("textfield17"+i)!=null)?request.getParameter("textfield17"+i):"";
                    String textfield18= (request.getParameter("textfield18"+i)!=null)?request.getParameter("textfield18"+i):"";
                    String textfield19= (request.getParameter("textfield19"+i)!=null)?request.getParameter("textfield19"+i):"";
                    
                    String hiddenField= (request.getParameter("hiddenField"+i)!=null)?request.getParameter("hiddenField"+i):"0";
                    String hiddenField2= (request.getParameter("hiddenField2"+i)!=null)?request.getParameter("hiddenField2"+i):"0";
                    String hiddenField3= (request.getParameter("hiddenField3"+i)!=null)?request.getParameter("hiddenField3"+i):"0";
                    
                    //Nuevos Campos para auxiliar
                    String tipo=(request.getParameter("tipo"+i)!=null)?request.getParameter("tipo"+i):"";
                    String auxiliar=(request.getParameter("auxiliar"+i)!=null)?request.getParameter("auxiliar"+i):"";
                    String aux=(request.getParameter("aux"+i)!=null)?request.getParameter("aux"+i):"";
                    
                    if(x.equals(textfield17)){
                        if(cs.equals("no")){
                            textfield17="";
                        }
                    }
                    /**************************************
                     * condicional para saber si los campos q se mandaron del formulario
                     * estan vacios, de caso contrario es por que hay un item !.
                     ***************************************/
                    
                    if(textfield11.equals("")&&textfield12.equals("")&&textfield13.equals("")&&textfield14.equals("")&&textfield15.equals("")&&textfield16.equals("")&&textfield17.equals("")&&textfield18.equals("")){
                        
                    }
                    else{
                        ////System.out.println(" "+textfield11+" "+textfield12+" "+textfield13+" "+textfield14+" "+textfield15+" "+textfield16+" "+textfield17+" "+textfield18+" "+textfield19);
                        /**************************************
                         * A continuacion se setea el item en el objeto con los
                         * campos obtenidos del formulario y luego se
                         * almacena en el vector
                         ***************************************/
                        
                        ////System.out.println("hay!!! item!!!");
                        factura_detalle factu_deta = new factura_detalle();
                        
                        factu_deta.setNumero_remesa(textfield11);
                        factu_deta.setFecrem(textfield12);
                        factu_deta.setDescripcion(textfield13);
                        factu_deta.setTiposubledger(tipo);
                        factu_deta.setAuxliliar(auxiliar);
                        factu_deta.setAux(aux);
                        
                        if(textfield14.equals(""))
                            textfield14+="0";
                        if(textfield15.equals(""))
                            textfield15+="0";
                        if(textfield16.equals(""))
                            textfield16+="0";
                        if(hiddenField.equals(""))
                            hiddenField+="0";
                        if(hiddenField2.equals(""))
                            hiddenField2+="0";
                        if(hiddenField3.equals(""))
                            hiddenField3+="0";
                        
                        factu_deta.setCantidad(new Double(textfield14.replaceAll(",","")));
                        factu_deta.setUnidad(textfield18);
                        factu_deta.setValor_unitario(Double.parseDouble((textfield15.replaceAll(",",""))));
                        factu_deta.setValor_unitariome(Double.parseDouble(hiddenField2));
                        factu_deta.setCodigo_cuenta_contable(textfield17);
                        
                        if(c[1]==null)
                            factu_deta.setAux("N");
                        
                        if(x.equals(textfield17)){
                            if(cs.equals("no")){
                                textfield17="";
                            }else{
                                factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                factu_deta.setAux(c[1]);
                            }
                        }
                        
                        factu_deta.setMoneda(moneda);
                        
                        factu_deta.setValor_tasa(new Double(hiddenField));
                        factu_deta.setValor_item(Double.parseDouble((textfield16.replaceAll(",",""))));
                        factu_deta.setValor_itemme(Double.parseDouble(hiddenField3));
                        
                        if(moneda.equals(textfield19)){
                            ////System.out.println("la moneda es igual la remesa!!!");
                            
                            double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                            if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                model.facturaService.getFactu().setValor_tasa(new Double("1"));
                            }
                            val/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                            double val2 = new Double(tasa.replaceAll(",","")).doubleValue();
                            val*=val2;
                            factu_deta.setValor_unitario(val);
                            factu_deta.setValor_item(this.getValor(val*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                            ////System.out.println("factu_deta.getValor_unitario():"+factu_deta.getValor_unitario());
                            ////System.out.println("factu_deta.getValor_item():"+factu_deta.getValor_item());
                        }
                        
                        else{
                            
                            ////System.out.println("la moneda es dif!!!");
                            ////System.out.println("moneda remesa:"+textfield19);
                            
                            try{
                                Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),textfield19,moneda, actual);
                                
                                double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                                ////System.out.println("val:"+val);
                                ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                double val2 = new Double(tasa.replaceAll(",","")).doubleValue();
                                
                                if(causa.equals("pormoneda")){
                                    if(tasas==null){
                                        ////System.out.println("es nulo!!!");
                                        tasa="0";
                                    }
                                    else{
                                        
                                        val2=tasas.getValor_tasa1();
                                    }////System.out.println("por monesa val2:"+val2);
                                    
                                }
                                else{
                                    ////System.out.println("val:"+val);
                                    if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                        model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                    }
                                    val/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                                    
                                    
                                    ////System.out.println("val2:"+val2);
                                    val*=val2;
                                    ////System.out.println("val:"+val);
                                }
                                
                                factu_deta.setValor_unitario(val);
                                ////System.out.println("factu_deta.getValor_unitario:"+factu_deta.getValor_unitario());
                                
                                if(tasas!=null){
                                    
                                    double valoruni=0;
                                    if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                        
                                        valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                        ////System.out.println("if:"+valoruni );
                                        
                                    }
                                    else{
                                        valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();
                                        ////System.out.println( );
                                        ////System.out.println(factu_deta.getValor_unitario()+" * "+val2+" * "+tasas.getValor_tasa2()+" = "+valoruni);
                                        
                                    }
                                    ////System.out.println("tasas!= "+valoruni);
                                    factu_deta.setValor_unitario(valoruni);
                                    factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                    factu_deta.setValor_itemme(factu_deta.getValor_item());
                                    factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                    
                                    factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                    factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                    
                                }
                                else{
                                    ////System.out.println("tasas==null ");
                                    double valoruni=0;
                                    factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                    factu_deta.setValor_itemme(factu_deta.getValor_item());
                                    factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                    factu_deta.setValor_unitario(valoruni);
                                    factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                    factu_deta.setValor_tasa(new Double("0"));
                                }
                                
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        
                        a.add(factu_deta);
                        
                        
                    }
                }
                
                /**************************************
                 * el vector de items seteados (a), el a su
                 * vez copiado en los vectores de
                 * remesasfacturadas y modificadas
                 ***************************************/
                
                model.facturaService.setRemesasModificadas(a);
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.calcularTotal();
                
            }
            /**************************************
             * Condicional para cargar remesas digitadas
             ***************************************/
            else if(cargar.equals("ok")){
                ////System.out.println("cargar");
                /**************************************
                 * Obtiene los datos enviados desde el formulario
                 ***************************************/
                
                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String zona = (request.getParameter("zona")!=null)?request.getParameter("zona"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";
                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";
                String x = (request.getParameter("x")!=null)?request.getParameter("x"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield"):"";
                String gasto = (request.getParameter("gasto")!=null)?request.getParameter("gasto"):"";
                
                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/
                
                if(tasa.equals("")){
                    tasa+="0";
                }
                
                if(vtotal.equals("")){
                    vtotal+="0";
                }
                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setZona(zona);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setMoneda(moneda);
                model.facturaService.getFactu().setValor_tasa(new Double(tasa.replaceAll(",","")));
                
                /**************************************
                 * Busca si la remesa ya esta en la lista de remesas
                 * escojidas por el cliente para facturar
                 ***************************************/
                
                Vector a = model.facturaService.getRemesasFacturar();
                Vector b = model.facturaService.getRemesas();
                
                /**************************************
                 * condicional para no recorrer el vector
                 * si la remesa esta escojida
                 ***************************************/
                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp";
                if(gasto.equals("no")){
                    
                    int cont=0;
                    for(int i =0; i< a.size(); i++){
                        factura_detalle factu_de =(factura_detalle)a.get(i);
                        if(factu_de.getNumero_remesa().equals(x)){
                            cont++;
                        }
                    }
                    
                    if(cont==0){
                        ////System.out.println("gasto(no)");
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?gas= No se encuentra la remesa "+x;
                        for(int i =0; i< model.facturaService.getRemesas().size(); i++){
                            factura_detalle factu_deta =(factura_detalle)model.facturaService.getRemesas().get(i);
                            
                            if(factu_deta.getNumero_remesa().equals(x)){
                                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?";
                                
                                // modif para cargar remesas con el tipo
                                String c[] ={"",""};
                                c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());
                                if(!c[0].equals("")){
                                    if(c[1].equals("S")){
                                        try{
                                            modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                            factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                            factu_deta.setAux("S");
                                            factu_deta.setAuxliliar("");
                                        }catch(Exception e){
                                            
                                        }
                                    }
                                }
                                else{
                                    factu_deta.setAux("N");
                                    factu_deta.setAuxliliar("");
                                }
                                /**************************************
                                 * condicional para ver si la remesa que se digito tiene la misma moneda
                                 * local, de lo contrario se hace la conversion
                                 ***************************************/
                                if(!factu_deta.getAuxliliar().equals("")){
                                    try{
                                        modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                        factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                    }catch(Exception e){
                                        
                                    }
                                }
                                if(moneda.equals(factu_deta.getMoneda())){
                                    
                                    ////System.out.println("iguales");
                                    factu_deta.setValor_tasa(new Double("1"));
                                    factu_deta.setValor_unitariome(factu_deta.getValor_unitariome());
                                    factu_deta.setValor_itemme( factu_deta.getValor_itemme());
                                }
                                else{
                                    try{
                                        
                                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_deta.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);
                                        
                                        
                                        ////System.out.println("val:"+factu_deta.getValor_unitario());
                                        ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                        double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();
                                        
                                        
                                        if(tasas!=null){
                                            
                                            double valoruni=0;
                                            if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                                valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                                ////System.out.println("if:"+valoruni );
                                            }
                                            else{
                                                valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();
                                                ////System.out.println("else:"+valoruni );
                                            }
                                            ////System.out.println("tasas!= "+valoruni);
                                            factu_deta.setValor_unitario(valoruni);
                                            factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),moneda));
                                            factu_deta.setValor_itemme(factu_deta.getValor_item());
                                            factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                            factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                            factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                            
                                        }
                                        else{
                                            ////System.out.println("tasas==null ");
                                            double valoruni=0;
                                            factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),moneda));
                                            factu_deta.setValor_itemme(factu_deta.getValor_item());
                                            factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                            factu_deta.setValor_unitario(valoruni);
                                            factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                            factu_deta.setValor_tasa(new Double("0"));
                                        }
                                        
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
                                    
                                    
                                    
                                }
                                a.add(factu_deta);
                                
                                
                                Vector cr= model.facturaService.costoReembolsable(factu_deta.getNumero_remesa());
                                
                                for(int f=0;f<cr.size();f++){
                                    factura_detalle factu_d = (factura_detalle)cr.get(f);
                                    
                                    factu_d.setTiposubledger("");
                                    factu_d.setAuxliliar("");
                                    factu_d.setAux("N");
                                    
                                    String cc[] ={"",""};
                                    cc=model.facturaService.buscarplancuentas(factu_d.getCodigo_cuenta_contable());
                                    if(!cc[0].equals("")){
                                        if(cc[1].equals("S")){
                                            try{
                                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                                factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                                factu_deta.setAux("S");
                                                factu_deta.setAuxliliar("");
                                            }catch(Exception e){
                                                
                                            }
                                        }
                                    }
                                    else{
                                        factu_deta.setAux("N");
                                        factu_deta.setAuxliliar("");
                                    }
                                    
                                    /////////////////
                                    if(model.facturaService.getFactu().getMoneda().equals(factu_d.getMoneda())){
                                        ////System.out.println("iguales");
                                        factu_d.setValor_tasa(new Double("1"));
                                        factu_d.setValor_unitariome(factu_d.getValor_unitariome());
                                        factu_d.setValor_itemme( factu_d.getValor_itemme());
                                        
                                    }
                                    else{
                                        
                                        ////System.out.println("diferentes");
                                        try{
                                            Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_d.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);
                                            
                                            
                                            ////System.out.println("val:"+factu_deta.getValor_unitario());
                                            ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                            double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();
                                            
                                            if(tasas!=null){
                                                double valoruni=0;
                                                if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                                    valoruni = factu_d.getValor_unitario()*tasas.getValor_tasa1();
                                                    ////System.out.println("if:"+valoruni );
                                                }
                                                else{
                                                    valoruni = factu_d.getValor_unitario()*val2*tasas.getValor_tasa2();
                                                    ////System.out.println("else:"+valoruni );
                                                }
                                                ////System.out.println("tasas!= "+valoruni);
                                                factu_d.setValor_unitario(valoruni);
                                                factu_d.setValor_item(this.getValor(valoruni*factu_d.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                                factu_d.setValor_itemme(factu_d.getValor_item());
                                                factu_d.setValor_unitariome(factu_d.getValor_unitario());
                                                factu_d.setMoneda(model.facturaService.getFactu().getMoneda());
                                                factu_d.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                                
                                            }
                                            else{
                                                ////System.out.println("tasas==null ");
                                                double valoruni=0;
                                                factu_d.setValor_item(this.getValor(valoruni*factu_d.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                                factu_d.setValor_itemme(factu_d.getValor_item());
                                                factu_d.setValor_unitariome(factu_d.getValor_unitario());
                                                factu_d.setValor_unitario(valoruni);
                                                factu_d.setMoneda(model.facturaService.getFactu().getMoneda());
                                                factu_d.setValor_tasa(new Double("0"));
                                            }
                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }
                                        
                                    }
                                    /////////////////
                                    
                                    a.add(factu_d);
                                }
                                /*******************/
                                
                            }
                        }
                        model.facturaService.setRemesasFacturar(a);
                        model.facturaService.calcularTotal();
                    }
                    else{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?gas=La remesa "+x+" ya fue ingresada!";
                    }
                    
                }
                if(gasto.equals("ok")){
                    
                    if(model.facturaService.existeRemesa(model.facturaService.getFactu().getCodcli(),x)){
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?";
                        ////System.out.println("gasto(ok)");
                        factura_detalle factu_deta = new factura_detalle();
                        factu_deta.setNumero_remesa("");
                        factu_deta.setFecrem("");
                        factu_deta.setDescripcion("GASTO DE LA REMESA "+x);
                        factu_deta.setCantidad(new Double("0"));
                        factu_deta.setUnidad("");
                        factu_deta.setValor_unitario(Double.parseDouble("0"));
                        factu_deta.setValor_unitariome(Double.parseDouble("0"));
                        factu_deta.setCodigo_cuenta_contable("");
                        factu_deta.setAux("N");
                        factu_deta.setAuxliliar("");
                        factu_deta.setTiposubledger("");
                        
                        factu_deta.setMoneda(moneda);
                        
                        factu_deta.setValor_tasa(new Double("0"));
                        factu_deta.setValor_item(Double.parseDouble("0"));
                        factu_deta.setValor_itemme(Double.parseDouble("0"));
                        a.add(factu_deta);
                        model.facturaService.setRemesasFacturar(a);
                    }
                    else{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?gas=No se encuentra la remesa "+x;
                    }
                }
                
            }
            if(recargar.equals("ok")){
                ////System.out.println("recargar");
                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp";
            }
            /**************************************
             * condicional para cargar remesas que estan
             * actualmente escojidas y digitadas
             ***************************************/
            else if(Agregar.equals("ok")){
                String causa = (request.getParameter("causa")!=null)?request.getParameter("causa"):"";
                
                ////System.out.println("agregar");
                /**************************************
                 * Obtiene los datos enviados desde el formulario
                 ***************************************/
                
                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String zona = (request.getParameter("zona")!=null)?request.getParameter("zona"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"0";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                
                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";
                
                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";
                
                String guardar =(request.getParameter("guardar")!=null)?request.getParameter("guardar"):"";
                String actualizar =(request.getParameter("actualizar")!=null)?request.getParameter("actualizar"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield"):"";
                String tasa2=tasa;
                String mensaje="";
                if(causa.equals("pormoneda")){
                    try{
                        ////System.out.println("Moneda1:"+model.facturaService.getFactu().getMoneda()+" MONEDA2:"+moneda);
                        //Tasa tasass = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),moneda,model.facturaService.getMonedaLocal(), actual);
                        Tasa tasass = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),moneda, actual);
                        
                         /*if(tasass==null){
                             ////System.out.println("es nulo!!!");
                             tasa="0";
                         }
                         else
                             tasa=""+tasass.getValor_tasa();*/
                        if(tasass!=null){
                            if(tasass.getValor_tasa()==0){
                                tasass=null;
                                mensaje="No se pudo realizar conversión a "+moneda+", No se encontró valor de la tasa!";
                            }else
                                tasa=""+tasass.getValor_tasa();
                        }else
                            mensaje="No se pudo realizar conversión a "+moneda+", No se encontró valor de la tasa!";
                        
                        ////System.out.println("por moneda!!!:"+tasa);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
                
                model.facturaService.getFactu().setAgencia_facturacion(usuario.getId_agencia());
                
                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/
                
                if(tasa.equals("")){
                    tasa+="0";
                }
                
                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setZona(zona);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setHc(cmc);
                
                if(mensaje.equals(""))
                    model.facturaService.getFactu().setMoneda(moneda);
                ////System.out.println(" moneda factu---------->"+ model.facturaService.getFactu().getMoneda());
                
                String maxfila= (request.getParameter("numerofilas")!=null)?request.getParameter("numerofilas"):"";
                String facturacion= (request.getParameter("facturacion")!=null)?request.getParameter("facturacion"):"";
                String facturacionmodif= (request.getParameter("facturacionmodif")!=null)?request.getParameter("facturacionmodif"):"";
                
                ////System.out.println("maxfila:"+maxfila);
                Vector a = new Vector();
                
                /**************************************
                 * for para recorre todos los items del formulario
                 * y obtenerlos
                 ***************************************/
                
                
                for(int i =0; i<= Integer.parseInt(maxfila); i++){
                    
                    String textfield11= (request.getParameter("textfield11"+i)!=null)?request.getParameter("textfield11"+i):"";
                    String textfield12= (request.getParameter("textfield12"+i)!=null)?request.getParameter("textfield12"+i):"";
                    String textfield13= (request.getParameter("textfield13"+i)!=null)?request.getParameter("textfield13"+i):"";
                    String textfield14= (request.getParameter("textfield14"+i)!=null)?request.getParameter("textfield14"+i):"";
                    String textfield15= (request.getParameter("textfield15"+i)!=null)?request.getParameter("textfield15"+i):"";
                    String textfield16= (request.getParameter("textfield16"+i)!=null)?request.getParameter("textfield16"+i):"";
                    String textfield17= (request.getParameter("textfield17"+i)!=null)?request.getParameter("textfield17"+i):"";
                    String textfield18= (request.getParameter("textfield18"+i)!=null)?request.getParameter("textfield18"+i):"";
                    String textfield19= (request.getParameter("textfield19"+i)!=null)?request.getParameter("textfield19"+i):"";
                    
                    String hiddenField= (request.getParameter("hiddenField"+i)!=null)?request.getParameter("hiddenField"+i):"0";
                    String hiddenField2= (request.getParameter("hiddenField2"+i)!=null)?request.getParameter("hiddenField2"+i):"0";
                    String hiddenField3= (request.getParameter("hiddenField3"+i)!=null)?request.getParameter("hiddenField3"+i):"0";
                    
                    //Nuevos Campos para auxiliar
                    String tipo=(request.getParameter("tipo"+i)!=null)?request.getParameter("tipo"+i):"";
                    String auxiliar=(request.getParameter("auxiliar"+i)!=null)?request.getParameter("auxiliar"+i):"";
                    String aux=(request.getParameter("aux"+i)!=null)?request.getParameter("aux"+i):"";
                    
                    /**************************************
                     * condicional para saber si los campos q se mandaron del formulario
                     * estan vacios, de caso contrario es por que hay un item !.
                     ***************************************/
                    
                    if(textfield11.equals("")&&textfield12.equals("")&&textfield13.equals("")&&textfield14.equals("")&&textfield15.equals("")&&textfield16.equals("")&&textfield17.equals("")&&textfield18.equals("")){
                        
                    }
                    else{
                        /**************************************
                         * A continuacion se setea el item en el objeto con los
                         * campos obtenidos del formulario y luego se
                         * almacena en el vector
                         ***************************************/
                        
                        ////System.out.println("hay!!! item!!!");
                        factura_detalle factu_deta = new factura_detalle();
                        
                        factu_deta.setNumero_remesa(textfield11);
                        factu_deta.setFecrem(textfield12);
                        factu_deta.setDescripcion(textfield13);
                        factu_deta.setTiposubledger(tipo);
                        factu_deta.setAuxliliar(auxiliar);
                        
                        factu_deta.setAux(aux);
                        if(textfield14.equals(""))
                            textfield14+="0";
                        if(textfield15.equals(""))
                            textfield15+="0";
                        if(textfield16.equals(""))
                            textfield16+="0";
                        if(hiddenField.equals(""))
                            hiddenField+="0";
                        if(hiddenField2.equals(""))
                            hiddenField2+="0";
                        if(hiddenField3.equals(""))
                            hiddenField3+="0";
                        
                        factu_deta.setCantidad(new Double(textfield14.replaceAll(",","")));
                        factu_deta.setUnidad(textfield18);
                        factu_deta.setValor_unitario(Double.parseDouble((textfield15.replaceAll(",",""))));
                        factu_deta.setValor_unitariome(Double.parseDouble(hiddenField2));
                        factu_deta.setCodigo_cuenta_contable(textfield17);
                        String[] c={"",""};
                        c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());
                        
                        if(c[1].equals("S")){
                            try{
                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                
                            }catch(Exception o){
                                o.printStackTrace();
                            }
                        }
                        factu_deta.setAux(c[1]);
                        factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                        
                        if(mensaje.equals(""))
                            factu_deta.setMoneda(moneda);
                        else
                            factu_deta.setMoneda( model.facturaService.getFactu().getMoneda());
                        
                        factu_deta.setValor_tasa(new Double(hiddenField));
                        factu_deta.setValor_item(Double.parseDouble((textfield16.replaceAll(",",""))));
                        factu_deta.setValor_itemme(Double.parseDouble(hiddenField3));
                        
                        if(model.facturaService.getFactu().getMoneda().equals(textfield19)){
                            ////System.out.println("la moneda es igual la remesa!!!");
                            
                            double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                            if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                model.facturaService.getFactu().setValor_tasa(new Double("1"));
                            }
                            val/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                            double val2 = new Double(tasa.replaceAll(",","")).doubleValue();
                            val*=val2;
                            factu_deta.setValor_unitario(val);
                            factu_deta.setValor_item(this.getValor(val*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                            
                        }
                        
                        else{
                            
                            ////System.out.println("la moneda es dif!!!");
                            ////System.out.println("moneda remesa:"+textfield19);
                            
                            try{
                                Tasa tasas2 = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),textfield19,model.facturaService.getFactu().getMoneda(), actual); 
                                
                                
                                double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                                ////System.out.println("val:"+val);
                                ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                double val2 = new Double(tasa.replaceAll(",","")).doubleValue();
                                
                                
                                if(causa.equals("pormoneda")){
                                    try{
                                        
                                        if(tasas2==null){
                                            ////System.out.println("es nulo!!!");
                                            tasa="0";
                                        }
                                        else{
                                            
                                            val2=tasas2.getValor_tasa1();
                                        }////System.out.println("por monesa val2:"+val2);
                                    }catch(Exception t){
                                        t.printStackTrace();
                                    }
                                }
                                else{
                                    ////System.out.println("val:"+val);
                                    if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                        model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                    }
                                    val/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                                    
                                    
                                    ////System.out.println("val2:"+val2);
                                    val*=val2;
                                    ////System.out.println("val:"+val);
                                }
                                
                                factu_deta.setValor_unitario(val);
                                ////System.out.println("factu_deta.getValor_unitario:"+factu_deta.getValor_unitario());
                                
                                if(tasas2!=null){
                                    
                                    double valoruni=0;
                                    if(tasas2.getValor_tasa1()==tasas2.getValor_tasa2()){
                                        ////System.out.println("tasa1:"+tasas2.getValor_tasa1());
                                        valoruni = factu_deta.getValor_unitario()*tasas2.getValor_tasa1();
                                        ////System.out.println("if:"+valoruni );
                                        
                                    }
                                    else{
                                        ////System.out.println("tasa1:"+tasas2.getValor_tasa1());
                                        ////System.out.println("tasa2:"+tasas2.getValor_tasa2());
                                        valoruni = factu_deta.getValor_unitario()*val2*tasas2.getValor_tasa2();
                                        ////System.out.println( );
                                        ////System.out.println(factu_deta.getValor_unitario()+" * "+val2+" * "+tasas2.getValor_tasa2()+" = "+valoruni);
                                        
                                    }
                                    ////System.out.println("tasas!= "+valoruni);
                                    factu_deta.setValor_unitario(valoruni);
                                    factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                    factu_deta.setValor_itemme(factu_deta.getValor_item());
                                    factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                    
                                    factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                    factu_deta.setValor_tasa(new Double(""+tasas2.getValor_tasa2()));
                                    
                                    
                                }
                                else{
                                    ////System.out.println("tasas==null ");
                                    double valoruni=0;
                                    factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                    factu_deta.setValor_itemme(factu_deta.getValor_item());
                                    factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                    factu_deta.setValor_unitario(valoruni);
                                    factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                    factu_deta.setValor_tasa(new Double("0"));
                                }
                                
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        
                        a.add(factu_deta);
                        
                        
                    }
                }
                
                /**************************************
                 * el vector de items seteados (a), el a su
                 * vez copiado en los vectores de
                 * remesasfacturadas y modificadas
                 ***************************************/
                
                model.facturaService.setRemesasModificadas(a);
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.calcularTotal();
                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?agregarItems=ok&Modif=ok&Mensaje="+mensaje;
                
                /**************************************
                 * condicional para refrescar la pantalla y poder
                 *cargar modificaciones etc.
                 ***************************************/
                
                if(actualizar.equals("ok")){
                    ////System.out.println("actualizar");
                    next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?Mensaje="+mensaje;
                    
                }
                
                
                /**************************************
                 * condicionalcuando se undio el boton de modificar
                 * y porder modificar la factura
                 ***************************************/
                
                if(facturacionmodif.equals("ok")){
                    ////System.out.println("facturacion");
                    
                    
                    try{
                        next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=Modificacion exitosa de la factura:"+model.facturaService.getFactu().getFactura()+"&Modif=ok";
                        
                        Vector vfd= new Vector();
                        for(int i=0;i<model.facturaService.getRemesasFacturar().size();i++){
                            
                            factura_detalle fd = (factura_detalle)model.facturaService.getRemesasFacturar().get(i);
                            ////System.out.println("moneda fact:"+fd.getMoneda());
                           /* if(model.facturaService.getFactu().getMoneda().equals(model.facturaService.getMonedaLocal())){
                                fd.setValor_itemme(this.getValor(fd.getValor_itemme(),model.facturaService.getFactu().getMoneda()));
                                fd.setValor_tasa(new Double(1));
                                ////System.out.println("la moneda es igual la remesa!!!");
                            }
                            else{*/
                                ////System.out.println("la moneda es dif!!!");
                                
                                try{
                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),model.facturaService.getMonedaLocal(), actual);
                                    ////System.out.println("val:"+fd.getValor_unitario());
                                    ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();
                                    
                                    if(tasas!=null){
                                        
                                        double valoruni=0;
                                        if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                            valoruni = fd.getValor_unitario()*tasas.getValor_tasa1();
                                            ////System.out.println("if:"+valoruni );
                                        }
                                        else{
                                            valoruni = fd.getValor_unitario()*val2*tasas.getValor_tasa2();
                                            ////System.out.println("else:"+valoruni );
                                        }
                                        ////System.out.println("tasas!= "+valoruni);
                                        fd.setValor_unitariome(fd.getValor_unitario());
                                        fd.setValor_unitario(this.getValor(valoruni,model.facturaService.getMonedaLocal()));
                                        fd.setValor_itemme(this.getValor(fd.getValor_item(),model.facturaService.getFactu().getMoneda()));
                                        fd.setValor_item(this.getValor(valoruni*fd.getCantidad().doubleValue(),model.facturaService.getMonedaLocal()));
                                        fd.setMoneda(model.facturaService.getMonedaLocal());
                                        fd.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                    }
                                    else{
                                        double valoruni=0;
                                        fd.setValor_itemme(fd.getValor_item());
                                        fd.setValor_item(valoruni*fd.getCantidad().doubleValue());
                                        fd.setValor_unitariome(fd.getValor_unitario());
                                        fd.setValor_unitario(valoruni);
                                        fd.setMoneda(model.facturaService.getMonedaLocal());
                                        fd.setValor_tasa(new Double("0"));
                                    }
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                           // }
                            
                            vfd.add(fd);
                        }
                        model.facturaService.setRemesasFacturar(vfd);
                        model.facturaService.calcularTotalme(); 
                        
                        
                        if(model.facturaService.getMonedaLocal().equals(model.facturaService.getFactu().getMoneda())){
                            System.out.println("#########son iguales las monedas:");
                            ////System.out.println("getValor_factura:"+model.facturaService.getFactu().getValor_factura());
                            //model.facturaService.getFactu().setValor_facturame(model.facturaService.getFactu().getValor_factura());
                            ////System.out.println("getValor_facturame:"+model.facturaService.getFactu().getValor_facturame());
                            model.facturaService.getFactu().setValor_tasa(new Double("1"));
                            ////System.out.println("getValor_tasa:"+ model.facturaService.getFactu().getValor_tasa());
                            
                        }
                        else{
                            System.out.println("##############son diferentes las monedas:");
                            //model.facturaService.getFactu().setValor_facturame(model.facturaService.getFactu().getValor_factura());
                            ////System.out.println("getValor_facturame:"+model.facturaService.getFactu().getValor_facturame());
                            
                            try{
                                Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),model.facturaService.getMonedaLocal(), actual);
//                                double valorF = this.getValor(model.facturaService.getFactu().getValor_facturame()*tasas.getValor_tasa(),model.facturaService.getMonedaLocal());
//                                model.facturaService.getFactu().setValor_factura(valorF);
                                model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa()));
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        model.facturaService.getFactu().setAgencia_cobro(model.facturaService.buscarAgenciaCobro(model.facturaService.getFactu().getCodcli()));
                        
                        
                        ArrayList<String> ListSQL = new  ArrayList<String>();
                        ListSQL.add(model.facturaService.modificarFactura(model.facturaService.getFactu(), distrito,usuario.getLogin(),model.facturaService.getFactu().getFactura()));
                        
                        ListSQL.add(model.facturaService.borrarFacturaDetalles(distrito, "FAC", model.facturaService.getFactu().getFactura()));
                        ListSQL.add(model.facturaService.ingresarFacturaDetalle(model.facturaService.getFactu(), distrito,usuario.getLogin(), base,model.facturaService.getFactu().getFactura()));
                        
                        TransaccionService tService = new TransaccionService(usuario.getBd());
                        tService.crearStatement();
                        for (String sql : ListSQL) {
                            tService.getSt().addBatch(sql); // Cabecera, Detalle, Impuesto detalle
                        }
                        
                        tService.execute();
                        
                        
                    }catch(Exception ex){
                        next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=Modificacion fallida de la factura:"+model.facturaService.getFactu().getFactura()+"&Modif=ok";
                    }
                    model.facturaService.borrarArchivo( "factura"+usuario.getLogin()+".txt", usuario.getLogin() );
                    
                }
                /**************************************
                 * condicionalcuando para guardar los items en un
                 * archivo .txt, el cual se encuentra en c:slt/migraciones/
                 ***************************************/
                
                if(guardar.equals("ok")){
                    ////System.out.println("guardar");
                    model.facturaService.setRemesasFacturar(a);
                    try{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp";
                        model.facturaService.escribirArchivo(model.facturaService.getFactu(), model.facturaService.getRemesasFacturar(), "factura"+usuario.getLogin()+".txt", usuario.getLogin());
                        
                    }catch(Exception ex){
                        request.setAttribute("mensaje","Error no se pudo almacenar el archivo");
                    }
                }
                if(causa.equals("pormoneda")){
                    try{
                        if(mensaje.equals("")){
                            Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),moneda,model.facturaService.getMonedaLocal(), actual);
                            
                            model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa()));
                        }
                        else
                            model.facturaService.getFactu().setValor_tasa(new Double(tasa2));
                    }catch(Exception r){
                        
                    }
                }else
                    model.facturaService.getFactu().setValor_tasa(new Double(tasa.replaceAll(",","")));
                
            }
            
            
            else if(aceptar.equals("ok")){
                
                
                
                ////System.out.println("aceptar");
                next="/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp?close=ok";
                Vector a = new Vector();
                Vector remesasModificadas =model.facturaService.getRemesasModificadas();
                ////System.out.println("remesasModif.size:"+model.facturaService.getRemesasModificadas().size());
                
                String s = "";
                String[] c={"",""};
                for(int i=0; i<model.facturaService.getRemesas().size();i++){
                    factura_detalle factu_deta = (factura_detalle) ((factura_detalle)model.facturaService.getRemesas().get(i)).clonee();
                    
                    factu_deta.setTiposubledger("");
                    factu_deta.setAuxliliar("");
                    factu_deta.setAux("N");
                    if(!s.equals(factu_deta.getCodigo_cuenta_contable())){
                        c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());
                        
                        if(c[1].equals("S")){
                            try{
                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                
                            }catch(Exception o){
                                o.printStackTrace();
                            }
                        }
                        
                        
                        
                    }
                    factu_deta.setAux(c[1]);
                    factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                    
                    int modif=0;
                    for(int j=0;j<remesasModificadas.size();j++){
                        factura_detalle factu_det =(factura_detalle)remesasModificadas.get(j);
                        if(factu_det.getNumero_remesa().equals(factu_deta.getNumero_remesa())){
                            modif++;
                            factu_deta= factu_det;
                            
                            remesasModificadas.remove(j);
                        }
                    }
                    if(request.getParameter("checkbox"+i)!=null){
                        
                        Vector cr= model.facturaService.costoReembolsable(factu_deta.getNumero_remesa());
                        
                        if(modif>0){
                            a.add(factu_deta);
                        }
                        else{
                            if(model.facturaService.getFactu().getMoneda().equals(factu_deta.getMoneda())){
                                ////System.out.println("iguales");
                                factu_deta.setValor_tasa(new Double("1"));
                                factu_deta.setValor_unitariome(factu_deta.getValor_unitariome());
                                factu_deta.setValor_itemme( factu_deta.getValor_itemme());
                                
                            }
                            else{
                                
                                ////System.out.println("diferentes");
                                try{
                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_deta.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);
                                    
                                    
                                    ////System.out.println("val:"+factu_deta.getValor_unitario());
                                    ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();
                                    
                                    if(tasas!=null){
                                        double valoruni=0;
                                        if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                            valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                            ////System.out.println("if:"+valoruni );
                                        }
                                        else{
                                            valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();
                                            ////System.out.println("else:"+valoruni );
                                        }
                                        ////System.out.println("tasas!= "+valoruni);
                                        factu_deta.setValor_unitario(valoruni);
                                        factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                        factu_deta.setValor_itemme(factu_deta.getValor_item());
                                        factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                        factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                        factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                        
                                    }
                                    else{
                                        ////System.out.println("tasas==null ");
                                        double valoruni=0;
                                        factu_deta.setValor_itemme(factu_deta.getValor_item());
                                        factu_deta.setValor_item(valoruni*factu_deta.getCantidad().doubleValue());
                                        factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                        factu_deta.setValor_unitario(valoruni);
                                        factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                        factu_deta.setValor_tasa(new Double("0"));
                                    }
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                                
                            }
                            a.add(factu_deta);
                            
                            /**Para calcular el valor en moneda si es diferente para los costos reembolsables*/
                            for(int f=0;f<cr.size();f++){
                                factura_detalle factu_d = (factura_detalle)cr.get(f);
                                
                                factu_d.setTiposubledger("");
                                factu_d.setAuxliliar("");
                                factu_d.setAux("N");
                                
                                if(!s.equals(factu_d.getCodigo_cuenta_contable())){
                                    c=model.facturaService.buscarplancuentas(factu_d.getCodigo_cuenta_contable());
                                    
                                    if(c[1].equals("S")){
                                        try{
                                            modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_d.getCodigo_cuenta_contable());
                                            
                                        }catch(Exception o){
                                            o.printStackTrace();
                                        }
                                    }
                                    
                                }
                                factu_d.setAux(c[1]);
                                factu_d.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                
                                /////////////////
                                if(model.facturaService.getFactu().getMoneda().equals(factu_d.getMoneda())){
                                    ////System.out.println("iguales");
                                    factu_d.setValor_tasa(new Double("1"));
                                    factu_d.setValor_unitariome(factu_d.getValor_unitariome());
                                    factu_d.setValor_itemme( factu_d.getValor_itemme());
                                    
                                }
                                else{
                                    
                                    ////System.out.println("diferentes");
                                    try{
                                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_d.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);
                                        
                                        
                                        ////System.out.println("val:"+factu_deta.getValor_unitario());
                                        ////System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                        double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();
                                        
                                        if(tasas!=null){
                                            double valoruni=0;
                                            if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                                valoruni = factu_d.getValor_unitario()*tasas.getValor_tasa1();
                                                ////System.out.println("if:"+valoruni );
                                            }
                                            else{
                                                valoruni = factu_d.getValor_unitario()*val2*tasas.getValor_tasa2();
                                                ////System.out.println("else:"+valoruni );
                                            }
                                            ////System.out.println("tasas!= "+valoruni);
                                            factu_d.setValor_unitario(valoruni);
                                            factu_d.setValor_item(this.getValor(valoruni*factu_d.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                            factu_d.setValor_itemme(factu_d.getValor_item());
                                            factu_d.setValor_unitariome(factu_d.getValor_unitario());
                                            factu_d.setMoneda(model.facturaService.getFactu().getMoneda());
                                            factu_d.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                            
                                        }
                                        else{
                                            ////System.out.println("tasas==null ");
                                            double valoruni=0;
                                            factu_d.setValor_itemme(factu_d.getValor_item());
                                            factu_d.setValor_item(valoruni*factu_d.getCantidad().doubleValue());
                                            factu_d.setValor_unitariome(factu_d.getValor_unitario());
                                            factu_d.setValor_unitario(valoruni);
                                            factu_d.setMoneda(model.facturaService.getFactu().getMoneda());
                                            factu_d.setValor_tasa(new Double("0"));
                                        }
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
                                    
                                }
                                /////////////////
                                
                                a.add(factu_d);
                            }
                        }
                    }
                }
                
                
                /************************************
                 *por ultimo se agregan los items modificados o manuales
                 ***********************************/
                for(int j=0;j<remesasModificadas.size();j++){
                    a.add((factura_detalle)remesasModificadas.get(j));
                }
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.calcularTotal();
                
            }
            
            /************************************
             * Condicional para regresar a inicioFacturacion,
             * de esta manera se setean los vectores en cero.
             ***********************************/
            
            if(regresar.equals("ok")){
                ////System.out.println("regresar");
                Vector a= new Vector();
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.setRemesasModificadas(a);
                model.facturaService.setRemesas(a);
                factura b = new factura();
                model.facturaService.setFactu(b);
                ////System.out.println("regresar!!!");
            }
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    public double getValor(double valor, String moneda){
        int decimales = (moneda.equals("DOL")?2:0);
        return com.tsp.util.Util.roundByDecimal(valor, decimales);
    }
}
//28 Marzo tito
