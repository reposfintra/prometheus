/*
 * Nombre        TiketIngresarAction.java
 * Autor         Ing Jesus Cuestas
 * Modificar     Ing Sandra Escalante   
 * Fecha         25 de junio de 2005, 10:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class TiketIngresarAction extends Action{
    
    /** Creates a new instance of TramoIngresarAction */
    public TiketIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/tramo/ingresarTiket.jsp?lista=ok";        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");        
        
        String cia = request.getParameter("c_cia");
        String origen = request.getParameter("c_origen");
        String destino = request.getParameter("c_destino");
        float cantmax = Float.parseFloat(request.getParameter("c_cant"));
        String tiket = request.getParameter("c_tiket");
        
        Tramo_Ticket t = new Tramo_Ticket();
        t.setCia(cia);
        t.setOrigen(origen);
        t.setDestino(destino);
        t.setTicket_id(tiket);
        t.setCantidad(cantmax);
        t.setUsuario(usuario.getLogin());
        t.setBase(usuario.getBase());
        model.tiketService.setTicket(t);
        
        int sw = 0; 
        
        try{
            if (!model.tiketService.existeTicketID(t.getCia(), t.getTicket_id())){
                request.setAttribute("mensaje","El Tiquete " + t.getTicket_id() + " no se encuentra registrado en peajes...");
                sw = 1;
            }
            
            if (sw == 0 ){
                model.tiketService.agregarTramo_Ticket();
                request.setAttribute("mensaje","El Tiquete fue ingresado con exito!");
                next += "&reload=ok";
            }
            
            model.tramoService.buscarTramo(cia, origen, destino);
            model.tiketService.listarTramos_Ticket(cia,origen,destino);
        }catch (SQLException e){
            try{
                if(model.tiketService.existeTramo_Ticket(cia,origen,destino,tiket)){
                    model.tiketService.modificarTramo_Ticket();
                    next += "&reload=ok";
                    request.setAttribute("mensaje","El Tiquete fue ingresado con exito!");
                }
                else{
                    request.setAttribute("mensaje","El tiquete ya existe!");
                }
            }catch (SQLException a){
                throw new ServletException(a.getMessage());
            }
        }
        this.dispatchRequest(next);
    }
}
