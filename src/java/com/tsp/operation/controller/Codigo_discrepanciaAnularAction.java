/*
 * Codigo_discrepanciaAnularAction.java
 *
 * Created on 28 de junio de 2005, 09:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Codigo_discrepanciaAnularAction extends Action{
    
    /** Creates a new instance of Codigo_discrepanciaAnularAction */
    public Codigo_discrepanciaAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        try{
            Codigo_discrepancia codigo_discrepancia = new Codigo_discrepancia();
            codigo_discrepancia.setDescripcion(descripcion);
            codigo_discrepancia.setCodigo(codigo);
            codigo_discrepancia.setCia(usuario.getCia().toUpperCase());
            codigo_discrepancia.setUser_update(usuario.getLogin().toUpperCase());
            codigo_discrepancia.setCreation_user(usuario.getLogin().toUpperCase());
            model.codigo_discrepanciaService.anularCodigo_discrepancia(codigo_discrepancia);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }
    
}
