
package com.tsp.operation.controller;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.Smservice;
import com.tsp.operation.model.services.TrayectoriaClienteService;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jpinedo
 */
public class TrayectoriaClienteAction extends Action {

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  Usuario usuario;

/**
 *
 * @author jpinedo
 */

public void run(){
        String next = "/jsp/fenalco/negocios/TrayectoriaCliente.jsp";
        String opcion = "";
        String   msj="";
        String cadenatabla = "";
        String img="<img src='" + request.getContextPath() +"/images/images.jpg' width='15' height='15' align='left' title='Ver Historial ' />";
        TrayectoriaClienteService tcs = null;
        boolean redirect = false;
        List   lista_trayectoria   = new LinkedList();
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");  
            opcion = request.getParameter("opcion")!= null ? request.getParameter("opcion"):"0";
           
             String   id = (request.getParameter("id")==null)?"": request.getParameter("id") ;
          //   String   tipo = (request.getParameter("tipo")==null)?"": request.getParameter("tipo") ;
            


            tcs = new TrayectoriaClienteService(usuario.getBd());
            HttpSession sesion = request.getSession();
            Usuario user = (Usuario)sesion.getAttribute("Usuario");

             int op=Integer.parseInt(opcion);
            switch (op)
            {
                 case 1:/*listar trayectoria*/
                    //sesion.removeAttribute("lista_sms");
                    redirect=true;
                    lista_trayectoria =tcs.ListarTrayectoriaCliente(id);
                    if((lista_trayectoria!=null)&& (lista_trayectoria.size()>0))
                      {
                        sesion.setAttribute("lista_trayectoria",lista_trayectoria);

                      }
                        else
                    {
                         msj="No Existen Registros Para Este Documento: "+id;
                    }
                    next=next+"?msj="+msj;
                break;


            }

                if(redirect==true) this.dispatchRequest(next);
        }
        catch (Exception e) {
            try {
                throw new Exception("Error al generar el archivo xls: " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(SmsAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //System.out.println("fin proceso xls");
    }

}

