/*
 * Nombre        MenuSCrearOpcionAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         26 de abril de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class MenuCrearOpcionAction extends Action {
        
    public MenuCrearOpcionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        String pagina = request.getParameter("pagina");
        String next=request.getParameter("carpeta") + "/" + request.getParameter("pagina") + "?";
        
        String nom = request.getParameter("nom");
	int subm = Integer.parseInt( request.getParameter("op") );
	int idpadre = Integer.parseInt(request.getParameter("id_padre"));
	String desc = request.getParameter("desc");
        int orden = Integer.parseInt(request.getParameter("orden"));
        
        String url = "";
        String carp="", pag ="", ot="";
        
        if (!request.getParameter("tipo").equals("raiz")){
            carp = request.getParameter("carp");
            pag = request.getParameter("pag");
            ot = request.getParameter("otro");
        }
        
	int nivel = Integer.parseInt( request.getParameter("nivel") );	
        
        try{ 
             //////System.out.println(" nivel " + nivel + " padre " + idpadre + " desc " + desc + " sub " + subm + " url " + url + " nombre " + nom);
            
            if ( subm == 1 ){//carp
                url = "";
            }
            else if ( subm == 2 ) {//opcion
                url = "../controller?estado=Menu&accion=Cargar&carpeta=" + carp + "&pagina=" + pag + ot;
                //////System.out.println(" url " + url);
            }
            
            Menu menu= new Menu();
            menu.setIdopcion(0);
            menu.setNivel(nivel);
            menu.setIdpadre(idpadre);
            menu.setDescripcion(desc);        
            menu.setSubmenu(subm);
            menu.setUrl(url);            
            menu.setNombre(nom);            
            menu.setCreado_por(usuario.getLogin().toUpperCase());
            menu.setUser_update(usuario.getLogin().toUpperCase());
            menu.setOrden(orden);
               
            int idopcion = model.menuService.nuevaOpcionMenu(menu);            
                       
            if (pagina.equals("InsertarOpcionMenu2.jsp")){
                next = next+ "idop=" + idpadre + "&";
            }
            
            if (idpadre != 0 ){
                model.menuService.obtenerPadreMenuxIDopcion(idpadre);                   
                model.menuService.obtenerHijos(idpadre);
            }else{
                model.menuService.obtenerPadresRaizMenu ();
            }
            
          
            next = next + "msg=Opcion agregada exitosamente!";            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
