/*
 * TiquetesDeleteAction.java
 *
 * Created on 3 de diciembre de 2004, 02:11 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class TiquetesDeleteAction extends Action{
    
    /** Creates a new instance of TiquetesDeleteAction */
    public TiquetesDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/tiquetes/tiketDelete.jsp?mensaje=ok";
        
        try{
            String nit= request.getParameter("nit");
            String sucursal = request.getParameter("sucursal");
            model.proveedortiquetesService.buscaProveedor(nit,sucursal);
            if(model.proveedortiquetesService.getProveedor()!=null){
                Proveedor_Tiquetes pt= model.proveedortiquetesService.getProveedor();
                model.proveedortiquetesService.anularProveedor(pt);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
