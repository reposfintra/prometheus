/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import java.util.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;





/**
 *
 * @author Alvaro
 */
public class FinanzasComprobanteAction extends Action {


    public FinanzasComprobanteAction() {
    }


    public void run() throws javax.servlet.ServletException {

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();

        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        String aceptarDisable = "";

        try {

            //  EVENTO  :  FIN_DE_MES de comprobanteFinMes.jsp
            //  Genera un comprobante de reclasificacion de las cuentas I,C,G
            if (evento.equalsIgnoreCase("FIN_DE_MES")) {


                String mes =  request.getParameter("mes");
                String anio=  request.getParameter("anio");
                String opcion = request.getParameter("tipo");

                System.out.println(opcion);
                HComprobanteFinMes hilo =  new HComprobanteFinMes();
                hilo.start(model, usuario,distrito,anio,mes,opcion) ;

                aceptarDisable = "S";
                msj   = "La creacion del comprobante contable se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/finanzas/contab/adminComprobantes/comprobanteFinMes.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de FIN_DE_MES



            //  EVENTO  :  FIN_DE_ANO de comprobanteFinAno.jsp
            //  Genera un comprobante de reclasificacion de las cuentas 4,5,6....
            if (evento.equalsIgnoreCase("FIN_DE_ANO")) {


                String anio=  request.getParameter("anio");
                String opcion = request.getParameter("tipo");

                HComprobanteFinAno hilo =  new HComprobanteFinAno();
                hilo.start(model, usuario,distrito,anio,opcion) ;

                aceptarDisable = "S";
                msj   = "La creacion del comprobante contable se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/finanzas/contab/adminComprobantes/comprobanteFinAno.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de FIN_DE_ANO












            next += msj ;
            this.dispatchRequest(next);

        }catch (Exception e) {
            e.printStackTrace();
        }

    }
}





