/******************************************************************
* Nombre ......................VehNoRetornadoInsertAction.java
* Descripci�n..................Clase Action para veh�culos no retornados
* Autor........................Armando Oviedo
* Fecha........................18/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class VehNoRetornadoInsertAction extends Action{
    
    /** Creates a new instance of VehNoRetornadoInsertAction */
    public VehNoRetornadoInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/trafico/VehNoRetornado/VehNoRetornadoIngresar.jsp" ;
        try{
            HttpSession session = request.getSession();            
            String fecha = request.getParameter("fechar");
            String placa = request.getParameter("placa");
            String causa = request.getParameter("causa");            
            
            if(fecha!=null && placa!=null){
                Usuario usuario = (Usuario) session.getAttribute("Usuario");                        
                String base = usuario.getBase();                
                String distrito = (String)(session.getAttribute("Distrito"));
                VehNoRetornado tmp = new VehNoRetornado();
                tmp.setBase(base);
                tmp.setFecha(fecha);
                tmp.setPlaca(placa.toUpperCase());
                tmp.setCausa(causa);                
                tmp.setCreationDate("now()");
                tmp.setCreationUser(usuario.getLogin());                
                tmp.setDstrct(distrito);
                tmp.setLastUpdate("01-01-1999");
                tmp.setRegStatus("");
                tmp.setUserUpdate("");
                model.vehnrsvc.setVehiculoNoRetornado(tmp);                
                boolean existe = model.vehnrsvc.existeVehiculoNoRetornado();
                boolean existeplaca = model.placaService.placaExist(tmp.getPlaca());
                if(existeplaca){
                    if(!existe){                        
                        model.vehnrsvc.addVehiculoNoRetornado();
                        next += "?mensaje=Elemento ingresado correctamente";
                    }
                    else{
                        next += "?mensaje=Ya existe este elemento de Veh�culo no retornado";
                    }
                }
                else{
                    next += "?mensaje=La placa no existe";
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
