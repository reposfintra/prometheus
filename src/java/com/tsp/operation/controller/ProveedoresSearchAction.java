/*
 * ProveedoresSearchAction.java
 *
 * Created on 21 de abril de 2005, 11:30 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class ProveedoresSearchAction extends Action{
    
    /** Creates a new instance of ProveedoresSearchAction */
    public ProveedoresSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next         ="/proveedores/proveedorUpdate.jsp";
        String nit          = request.getParameter("nit");
        String sucursal     = request.getParameter("sucursal").toUpperCase();
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        
        if(request.getParameter("anular")!=null){
            next = "/proveedores/proveedorDelete.jsp";
        }
        try{
            
            model.proveedoresService.searchProveedor(nit, sucursal);
            if(model.proveedoresService.getProveedor()!=null){
                request.setAttribute("provee", model.proveedoresService.getProveedor());
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
    
}
