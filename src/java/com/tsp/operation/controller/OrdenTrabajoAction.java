/*
 * OrdenTrabajoAction.java
 * Created on 21 de abril de 2009, 17:32
 */

package com.tsp.operation.controller;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HOrdenTrabajo;
import com.tsp.operation.model.Model;
import com.tsp.util.*;

import com.tsp.util.mailing.SendMail;
//imorales
public class OrdenTrabajoAction extends Action{
      
    public OrdenTrabajoAction() {
    }   
  
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
                 
         try{
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario     usuario   = (Usuario) session.getAttribute("Usuario");       
            String  base    = "/jsp/applus/";
            String  next    = "orden_de_trabajo.jsp?msg=";
            String  msj     = "";
                
            HOrdenTrabajo hilo =  new HOrdenTrabajo();
            hilo.start();

            msj   = "Proceso de doc ha iniciado. "  ;
           
            dispatchRequest( base + next + msj );
            
        } catch (Exception e){
            System.out.println("error aaaaa");
             throw new ServletException(e.getMessage());
        }
    }
    
}

