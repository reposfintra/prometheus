/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.InterfazLogisticaDAO;
import com.tsp.operation.model.DAOS.impl.LogisticaAdminImpl;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeansAgencia;
import com.tsp.operation.model.beans.BeansAnticipo;
import com.tsp.operation.model.beans.BeansConductor;
import com.tsp.operation.model.beans.BeansPropietario;
import com.tsp.operation.model.beans.BeansVehiculo;
import com.tsp.operation.model.beans.EDSPropietarioBeans;
import com.tsp.operation.model.beans.JXLRead;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.RMCantidadEnLetras;
import com.tsp.operation.model.beans.TramaJsonAnticipo;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import com.tsp.util.UtilFinanzas;
import com.tsp.util.Utility;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author egonzalez
 */
public class AdministracionLogisticaAction extends Action {

    Usuario usuario = null;
    private BeansAgencia agencia = null;
    private final int BUSCAR_TRANSPORTADORAS = 1;
    private final int BUSCAR_TITULOS_GRID = 2;
    private final int GET_REPORT_PRODUCTION = 3;
    private final int EXPORTAR_EXCEL = 4;
    private final int EXTRACTO_EDS = 5;
    private final int LOAD_EDS = 6;
    private final int EXTRACTO_PROPIETARIO = 7;
    private final int SEARCH_OWNER = 8;
    private final int SEARCH_PLACA = 9;
    private final int EXPORTAR_EXCEL_MANUAL = 10;
    private final int APROBAR_ANTICIPOS_TRANSFERENCIA = 11;
    private final int UPDATE_APROBAR_TRANSFERENCIA = 12;
    private final int ANTICIPOS_POR_TRANSFERIR = 13;
    private final int DESAPROBAR_ANTICIPOS = 14;
    private final int ANULAR_ANTICIPOS = 15;
    private final int CUENTAS_TERCERO = 16;
    private final int TRANSFERIR = 17;
    private final int REPORTE_PRODUCCION_TRANSPORTADORA = 18;
    private final int EXPORTAR_EXCEL_REPORTE_PRODUCCION = 19;
    private final int CUENTA_COBRO_TRANSPORTADORAS = 20;
    private final int G_CUENTA_COBRO_TRANSPORTADORAS = 21;
    private final int DETALLE_PLANILLAS_CXC = 22;
    private final int FACTURAS_PENDIENTES_EDS = 23;
    private final int FACTURAR_EDS=24;
    private final int EXPORTAR_EXCEL_EXT_EDS=25;
    private final int SUBIR_ANTICIPOS = 26;
    private final int CARGAR_OPCIONES_TRANSPORTADORAS = 27;
    private final int CARGAR_GRILLA_ANTICIPOS = 28;
    private final int ENVIAR_REANTICIPOS = 29;
    private final int LISTAR_REANTICIPOS_TRANSPORTADORA = 30;
    private final int ANULAR_ANTICIPOS_TRANSPORTADORA = 31;
    private final int ANULAR_REANTICIPOS_TRANSPORTADORA = 32;
    private final int LISTAR_PRODUCTOS_TRANSPORTADORA = 33;
    private final int BUSCAR_TRANSPORTADORAS_AGENCIA = 34;
    private final int REVERSAR_LOTE = 35;
    private final int BUSCAR_LOTE = 36;
    private final int BUSCAR_TRANSPORTADORAS_X_USUARIO = 37;

    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;


    @Override
    public void run() throws ServletException, InformationException {

        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            int opcion = Integer.parseInt(request.getParameter("opcion"));  
            //agregamos la agencia a nivel global
            this.agenciaLogin();            

                switch (opcion) {                
                    case BUSCAR_TRANSPORTADORAS:
                        this.getListaTansportadoras();
                        break;
                    case BUSCAR_TITULOS_GRID:
                        this.getTitulosGrid();
                        break;
                    case GET_REPORT_PRODUCTION:
                        this.getReportProduction();
                        break;
                    case EXPORTAR_EXCEL:
                        this.exportarExcelGrid();
                        break;
                    case EXTRACTO_EDS:
                        getReportextractEds();
                        break;
                    case LOAD_EDS:
                        getLoadEds();
                        break;
                    case EXTRACTO_PROPIETARIO:
                        getReportExtractOwner();
                        break;
                    case SEARCH_OWNER:
                        getOwner();
                        break;
                    case SEARCH_PLACA:
                        getPlaca();
                        break;
                    case EXPORTAR_EXCEL_MANUAL:
                        exportExcelOwner();
                        break;
                    case APROBAR_ANTICIPOS_TRANSFERENCIA:
                        aprobarAnticiposTransferencia();
                        break;
                    case UPDATE_APROBAR_TRANSFERENCIA:
                        updateAprobarTranferencia();
                        break;
                    case ANTICIPOS_POR_TRANSFERIR:
                        getAnticiposTransferencia();
                        break;
                    case DESAPROBAR_ANTICIPOS:
                        desAprobarAnticipos();
                        break;
                    case ANULAR_ANTICIPOS:
                        anularAnticipos();
                        break;
                    case CUENTAS_TERCERO:
                        cuentasTercero();
                        break;
                    case TRANSFERIR:
                        transferir();
                        break;
                    case REPORTE_PRODUCCION_TRANSPORTADORA:
                        reporteProduccionTrans();
                        break;
                    case EXPORTAR_EXCEL_REPORTE_PRODUCCION:
                        exportarExcelReporteProduccion();
                        break;
                    case CUENTA_COBRO_TRANSPORTADORAS:
                        cuentaCobroTransportadoras();
                        break;
                    case G_CUENTA_COBRO_TRANSPORTADORAS:
                        generarCuentaCobroTransportador();
                        break;
                    case DETALLE_PLANILLAS_CXC:
                        detallePlanillaCXC();
                        break;
                    case FACTURAS_PENDIENTES_EDS:
                        facturasPendientesEDS();
                        break;
                    case FACTURAR_EDS:
                        facturarEDS();
                        break;
                    case EXPORTAR_EXCEL_EXT_EDS:
                        exportarExcelExtractoEds();
                        break;
                    case SUBIR_ANTICIPOS:
                        enviarTramaJsonTransportadoras();
                        break;
                    case CARGAR_OPCIONES_TRANSPORTADORAS:
                        cargarMenuTransportadora();
                        break;
                    case CARGAR_GRILLA_ANTICIPOS:
                        cargarAnticiposTransportadora();
                        break;
                    case ENVIAR_REANTICIPOS:
                        enviarReanticiposTransportadora();
                        break;
                    case LISTAR_REANTICIPOS_TRANSPORTADORA:
                        cargarReanticiposTransportadora();
                        break;
                    case ANULAR_ANTICIPOS_TRANSPORTADORA:
                        anularAnticiposTransportadora();
                        break;
                    case ANULAR_REANTICIPOS_TRANSPORTADORA:
                        anularReanticiposTransportadora();
                        break;
                    case LISTAR_PRODUCTOS_TRANSPORTADORA:
                        cargarProductosTransportadora();
                        break;
                    case BUSCAR_TRANSPORTADORAS_AGENCIA :
                        getListaTansportadorasAgencias();
                        break;
                    case REVERSAR_LOTE :
                        reversarLote();
                        break;
                    case BUSCAR_LOTE :
                        buscarLote();
                        break;
                    case BUSCAR_TRANSPORTADORAS_X_USUARIO:
                        this.getListaTansportadorasXusuario();
                        break;
            }

        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        }

    }

    private void getListaTansportadoras() throws Exception {
        InterfazLogisticaDAO dao = new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.getTransportadorasLista(usuario), "application/json;");
    }

    private void getTitulosGrid() throws Exception {
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.getTitulosGrid(), "application/json;");
    }

    private void getReportProduction() throws Exception {
        String transportadora = request.getParameter("transportadora") != null ? request.getParameter("transportadora") : "";
        String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
        String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String c_conductor = request.getParameter("c_conductor") != null ? request.getParameter("c_conductor") : "";
        String c_propietario = request.getParameter("c_propietario") != null ? request.getParameter("c_propietario") : "";
        String planilla = request.getParameter("planilla") != null ? request.getParameter("planilla") : "";
        String placa = request.getParameter("placa") != null ? request.getParameter("placa") : "";
        String factura = request.getParameter("factura") != null ? request.getParameter("factura") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.getReportProduction(transportadora, fecha_inicio, fecha_fin, c_conductor, c_propietario, planilla, placa, factura), "application/json;");
    }

    private void getReportextractEds() throws Exception {

        String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
        String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String nombre = request.getParameter("nombre") != null ? request.getParameter("nombre") : "";
        String nit = request.getParameter("nit") != null ? request.getParameter("nit") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.getReportExtractEds(usuario, fecha_inicio, fecha_fin, nombre, nit), "application/json;");
    }

    private void getLoadEds() throws Exception {

        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.loadEds(usuario), "application/json;");
    }

    private void getReportExtractOwner() throws Exception {
        String propietario = request.getParameter("propietario") != null ? request.getParameter("propietario") : "";
        String placa = request.getParameter("placa") != null ? request.getParameter("placa") : "";
        String planilla = request.getParameter("planilla") != null ? request.getParameter("planilla") : "";
        String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
        String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";

        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.getReportExtactOwner(usuario, propietario, placa, planilla, fecha_inicio, fecha_fin), "application/json;");
    }

    private void getOwner() throws Exception {

        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.loadOwner(usuario), "application/json;");
    }

    private void getPlaca() throws Exception {
        int id_propietario = (request.getParameter("id_propietario") != null && !request.getParameter("id_propietario").equals("")) ? Integer.parseInt(request.getParameter("id_propietario")) : 0;
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.loadPlaca(id_propietario), "application/json;");
    }

    private void exportExcelOwner() throws Exception {

        String propietario = request.getParameter("propietario") != null ? request.getParameter("propietario") : "";
        String placa = request.getParameter("placa") != null ? request.getParameter("placa") : "";
        String planilla = request.getParameter("planilla") != null ? request.getParameter("planilla") : "";
        String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
        String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String resp1 = "";
        String url = "";

        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        String reportExtactOwner = dao.getReportExtactOwner(usuario, propietario, placa, planilla, fecha_inicio, fecha_fin);
        JsonObject object = (JsonObject) new JsonParser().parse(reportExtactOwner);
        Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("ExtractoPropietario_", "EXTRACTO PROPIETARIO");
        String[] cabecera = {"Planilla", "Placa", "Nombre Conductor", "Cedula", "Valor Anticipo", "Descuento", "Valor Neto", "Consecutivo Venta", "Fecha Venta", "Hora", "Bandera",
            "Nombre EDS", "Kilometraje", "Producto", "Precio Unitario", "Unidades", "Valor Venta", "Disponible"};

        short[] dimensiones = new short[]{
            3000, 3000, 7000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 5000, 3000, 7000, 3000, 3000, 3000, 3000
        };
        this.generaTitulos(cabecera, dimensiones);

        for (Map.Entry<String, JsonElement> entrySet1 : entrySet) {
            Object key = entrySet1.getKey();
            // Object value = entrySet1.getValue();         
            JsonArray asJsonArray = object.get((String) key).getAsJsonObject().get("detalle").getAsJsonArray();
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("placa").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_conductor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cedula_conductor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_anticipo").getAsDouble(), letra);
                xls.adicionarCelda(fila, col++, objects.get("descuento").getAsDouble(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_planilla").getAsDouble(), letra);
                xls.adicionarCelda(fila, col++, objects.get("num_venta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_venta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("hora").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("bandera").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_eds").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("kilometraje").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_producto").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("precio_xunidad").getAsDouble(), letra);
                xls.adicionarCelda(fila, col++, objects.get("unidades").getAsDouble(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_venta").getAsDouble(), letra);
                xls.adicionarCelda(fila, col++, objects.get("disponible").getAsDouble(), letra);

            }
        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ExtractoPropietario_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";

        this.printlnResponse(resp1, "text/plain");

    }

    private void exportarExcelGrid() throws IOException, DocumentException {
        //exporta a excel los datos de la grilla
        String pdfBuffer = request.getParameter("pdfBuffer");
        String fileName = request.getParameter("fileName");
        String fileType = request.getParameter("fileType");
        boolean isPDF = fileType.equals("pdf");
        if (isPDF) {
            /*este codigo es para exporta a pdf para el futuro
             * 
             *  *
             *
             */
            ServletOutputStream outputStream = response.getOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(pdfBuffer);
            renderer.layout();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "." + fileType + "\"");
            renderer.createPDF(outputStream);
            outputStream.flush();
            outputStream.close();

        } else {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "." + fileType + "\"");
            PrintWriter out = response.getWriter();
            out.print(pdfBuffer);
            out.close();
        }
    }

    private void aprobarAnticiposTransferencia() throws Exception {
        int id_transportadora = (request.getParameter("id_transportadora") != null && !request.getParameter("id_transportadora").equals("")) ? Integer.parseInt(request.getParameter("id_transportadora")) : 0;
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.aprobateAavances(id_transportadora), "application/json;");

    }

    private void updateAprobarTranferencia() throws Exception {

        String data = request.getParameter("datajson") != null ? request.getParameter("datajson") : "";
        JsonArray readJsonArray = readJsonArrayParameter(data);
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.aprobarTransferencias(readJsonArray, usuario), "application/json;");
    }

    private void getAnticiposTransferencia() throws Exception {
        int id_transportadora = (request.getParameter("id_transportadora") != null && !request.getParameter("id_transportadora").equals("")) ? Integer.parseInt(request.getParameter("id_transportadora")) : 0;
        String banco = request.getParameter("banco") != null ? request.getParameter("banco") : "";
        String[] splitBanco = banco.split(",");
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        //esto se hace porque fintra tiene culo de empanada en los bancos.
        banco = splitBanco[0].replace("7B", "07");
        this.printlnResponse(dao.buscarAnticiporTransferencia(id_transportadora,splitBanco[1], banco,splitBanco[2], splitBanco[3],usuario), "application/json;");

    }

    private void desAprobarAnticipos() throws Exception {

        String data = request.getParameter("datajson") != null ? request.getParameter("datajson") : "";
        JsonArray readJsonArray = readJsonArrayParameter(data);
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.desAprobarAnticipos(readJsonArray, usuario), "application/json;");
    }

    private void anularAnticipos() throws Exception {

        String data = request.getParameter("datajson") != null ? request.getParameter("datajson") : "";
        JsonArray readJsonArray = readJsonArrayParameter(data);
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.anularAnticipos(readJsonArray, usuario), "application/json;");
    }

    private void cuentasTercero() throws Exception {
        String proveedor = model.AnticiposPagosTercerosSvc.getProveedorUser(usuario.getLogin());
        model.AnticiposPagosTercerosSvc.searchListaCuentasTercero(proveedor);
        List listCTATercero = model.AnticiposPagosTercerosSvc.getListCTATercero();
        this.printlnResponse(new Gson().toJson(listCTATercero), "application/json;");
    }

    private void transferir() throws Exception {
        String data = request.getParameter("datajson") != null ? request.getParameter("datajson") : "";
        String banco = request.getParameter("banco") != null ? request.getParameter("banco") : "";
        JsonArray readJsonArray = readJsonArrayParameter(data);
        String[] splitBanco = banco.split(",");
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.transferir(readJsonArray, usuario, splitBanco), "application/json;");
    }

    private void reporteProduccionTrans() throws Exception {
        String transportadora = request.getParameter("transportadora") != null ? request.getParameter("transportadora") : "";
        String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
        String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.reporteProduccionTrans(transportadora, fecha_inicio, fecha_fin), "application/json;");
    }
    
    private void cuentaCobroTransportadoras() throws Exception{
        int transportadora = request.getParameter("transportadora") != null ? Integer.parseInt(request.getParameter("transportadora")) : 0;
        String fecha_corrida = request.getParameter("fecha_corrida") != null ? request.getParameter("fecha_corrida") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.cuentaCobroTransportadora(transportadora, fecha_corrida, usuario), "application/json;");
        
    }
    
    private void generarCuentaCobroTransportador() throws Exception{
        int transportadora = request.getParameter("transportadora") != null ? Integer.parseInt(request.getParameter("transportadora")) : 0;
        String fecha_corrida = request.getParameter("fecha_corrida") != null ? request.getParameter("fecha_corrida") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());          
        this.printlnResponse( dao.generarCuentaCobroTransportadora(transportadora, fecha_corrida, usuario), "application/json;");
    }
    
    private void detallePlanillaCXC() throws Exception{
        int transportadora = request.getParameter("transportadora") != null ? Integer.parseInt(request.getParameter("transportadora")) : 0;
        String fecha_corrida = request.getParameter("fecha_corrida") != null ? request.getParameter("fecha_corrida") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.detalleCorridaCXCTransportadora(transportadora, fecha_corrida, usuario),"application/json;");
    }

    private JsonArray readJsonArrayParameter(String json) {
        JsonArray jsonArray = null;
        try {
            JsonParser parser = new JsonParser();
            jsonArray = (JsonArray) parser.parse(json);

        } catch (Exception e) {
            jsonArray = null;
        } finally {
            return jsonArray;
        }
    }
    
    private void facturasPendientesEDS() throws Exception{        
        int id_eds = request.getParameter("eds") != null ? Integer.parseInt(request.getParameter("eds")) : 0;
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.facturasPendientesEDS(id_eds, fecha, usuario),"application/json;");      
    }
    
    private void facturarEDS() throws Exception {
        String respuesta = "";
        int id_eds = request.getParameter("eds") != null ? Integer.parseInt(request.getParameter("eds")) : 0;
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        String[] split = dao.facturarEds(id_eds, fecha, usuario).split(";");

        if (split[0].equals("OK")) {
            EDSPropietarioBeans buscarPropietario = dao.buscarPropietario(id_eds, usuario);
            String generarPdf = "";
            if (buscarPropietario != null) {
                generarPdf = this.generarPdf(Double.parseDouble(split[2]), usuario.getLogin(), split[1], buscarPropietario);
            }
            
            switch (generarPdf) {
                case "OK":
                    respuesta = "{\"respuesta\":\"OK\"}";
                    break;
                case "ERROR":
                    respuesta = "{\"respuesta\":\"PDFERROR\"}";
                    break;
                default:
                    respuesta = "{\"respuesta\":\"BPERROR\"}";
            }
       
            this.printlnResponse(respuesta, "application/json;");
        } else {
            respuesta = "{\"respuesta\":\"" + split[0] + "\"}";
            this.printlnResponse(respuesta, "application/json;");
        }
    }
    
    
     private void exportarExcelExtractoEds() throws Exception {
      
        String fecha_inicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
        String fecha_fin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String nombre = request.getParameter("nombre") != null ? request.getParameter("nombre") : "";
        String nit = request.getParameter("nit") != null ? request.getParameter("nit") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());       
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.getReportExtractEds(usuario, fecha_inicio, fecha_fin, nombre, nit)); 
        
         
        String resp1 = "";
        String url = "";
      
        this.generarRUTA();
        this.crearLibro("Extracto_eds_", "EXTRACTO DE VENTAS EDS");
        String[] cabecera = {"Cosecutivo Venta", "Nombre Eds", "Fecha Venta", "Hora", "Nombre Transportadora", "Planilla", "Placa", "Nombre Conductor", "Cedula", "Producto",
            "Precio Unitario", "Unidades", "Valor Venta", "Descuento", "Valor a Cobrar"};

        short[] dimensiones = new short[]{
            3000, 5000, 3000, 3000, 3000, 7000, 3000, 3000, 7000, 3000, 3000, 3000, 3000, 3000, 5000
        };
        this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("num_venta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_eds").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_venta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("hora").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_transportadora").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("placa").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_conductor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cedula_conductor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_producto").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("precio_xunidad").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("unidades").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_venta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("descuento").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_a_cobrar").getAsString(), letra);
        

            }

        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/Extracto_eds_" + fmt.format(new Date()) + ".xls";
        resp1 = "Reporte generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";

        this.printlnResponse(resp1, "text/plain");
    }


    private void exportarExcelReporteProduccion() throws Exception {
        String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("ReporteProduccion_", "TRANSPORTADORA REPORTE PRODUCCIO");
        String[] cabecera = {"Id", "Estado", "Observaci�n Anulaci�n", "Producto", "Nombre Agencia", "Nit Condutor", "Nombre Conductor", "Veto", "Veto Casual", "Nit Propietario",
            "Nombre Propietario", "Veto Propietario", "Veto Casual Propietario", "Placa", "Planilla", "Fecha Anticipo", "Fecha Env�o", "Fecha Creaci�n de Fintra", "Reanticipo", "Aprobado",
            "Usuario Creaci�n", "Transferido", "Banco Transferencia", "Cuenta Transferencia", "Tipo Cuenta Transferencia", "Banco", "Cuenta", "Tipo Cuenta", "Nombre Cuenta", "Nit Cuenta",
            "Valor Manifiesto", "Valor Anticipo", "Total Descuento", "Valor Anticipo con Descuento", "Valor Comisi�n", "Valor Consignado", "Fecha Transferencia",
            "Numero Egreso", "Valor Egreso", "Transportadora", "Origen", "Destino", "Fecha Pago Fintra","N�mero de Corrida"};

        short[] dimensiones = new short[]{
            3000, 3000, 7000, 5000, 5000, 5000, 8000, 3000, 5000, 5000, 5000, 5000, 3000, 3000, 5000, 5000, 5000, 3000, 3000, 5000,
            3000, 3000, 7000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 5000, 7000, 7000, 3000, 5000, 5000, 5000, 3000, 5000,
            5000, 5000, 7000,5000
        };
        this.generaTitulos(cabecera, dimensiones);

//        for (Map.Entry<String, JsonElement> entrySet1 : entrySet) {
//            Object key = entrySet1.getKey();
//            // Object value = entrySet1.getValue();         
//            JsonArray asJsonArray = object.get((String) key).getAsJsonObject().get("detalle").getAsJsonArray();
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("id").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("estado").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("obs_anulacion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("producto").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_agencia").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nit_conductor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_conductor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("veto").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("veto_causal").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nit_propietario").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_propietario").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("veto_propietario").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("veto_causal_propietario").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("placa").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_anticipo").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_envio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_creacion_fintra").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("reanticipo").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("aprobado").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("usuario_creacion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("transferido").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("banco_transferencia").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cuenta_transferencia").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("tipo_cuenta_transferencia").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("banco").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cuenta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("tipo_cuenta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_cuenta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nit_cuenta").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_manifiesto").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_anticipo").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("total_dsct").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_anticipo_con_descuento").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_comision").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_consignado").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_transferencia").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("numero_egreso").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_egreso").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("transportadora").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("origen").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("destino").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_pago_fintra").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nro_corrida").getAsString(), letra);

            }
//        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ReporteProduccion_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";

        this.printlnResponse(resp1, "text/plain");
    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            //xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }
    
    
     /**
     * Genera una cuenta de cobro de la estacion en formato .pdf. Guarda el archivo en el directorio de archivos del usuario
     * @param total Total de la cuenta
     * @param login_usuario Usuario que genera
     * @param consec Codigo de la cxp
     * @param edspb
     * @return Mensaje de generacion correcta o incorrecta del archivo
     * @throws Exception Cuando hay error
     */
    public String generarPdf(double total,String login_usuario,String consec,EDSPropietarioBeans edspb) throws Exception{
        String ruta = "";
        String msg = "OK";
        try {
            ruta = this.directorioArchivo(login_usuario, consec, "pdf");
          
            Font fuente = new Font(Font.HELVETICA,10,Font.NORMAL,new java.awt.Color(0,0,0));
            Font fuenteG = new Font(Font.HELVETICA,10,Font.BOLD,new java.awt.Color(0,0,0));
            PdfPCell celda = null;
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream( ruta ));
            documento.open();
            documento.newPage();
            documento.add(new Paragraph(new Phrase(edspb.getNombreprop(),fuenteG)));
            documento.add(new Paragraph(new Phrase(edspb.getEdsnombre(),fuenteG)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(edspb.getDireccion()+" , "+edspb.getCiudad()+". Tel: "+edspb.getTelefono(),fuente)));
            documento.add(new Paragraph(new Phrase("_____________________________________________"
                    + "_____________________________________________________",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            PdfPTable thead = new PdfPTable(1);
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("Cuenta de cobro No.: "+consec,fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //row2
            thead = new PdfPTable(1);
            Calendar calendario = Calendar.getInstance();
            String mes = "";
            mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            celda.setPhrase(new Paragraph(new Phrase("Ciudad y fecha : "+edspb.getCiudad()+", "+mes+" "+calendario.get(Calendar.DAY_OF_MONTH)+" de "+calendario.get(Calendar.YEAR),fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("FINTRA S.A.",fuenteG)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Nit: 802022016-1",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Debe a",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase(edspb.getNombreprop(),fuenteG)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Nit: "+edspb.getNit(),fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            RMCantidadEnLetras c = new RMCantidadEnLetras();
            String[] a;
            a = c.getTexto(total);
            String res = "";
            for(int i=0;i<a.length;i++){
                res = res + ((String)a[i]).replace("-"," ");
            }
            res=res.trim();
            documento.add(new Paragraph(new Phrase("La suma de "+res+" pesos ($ "+Util.customFormat(total)+") por concepto"
                    + " de reembolso por entrega de anticipos de viaje a conductores.",fuente)));
            //
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase("Atentamente,",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase("__________________________________",fuente)));
            documento.add(new Paragraph(new Phrase(edspb.getRepresentante(),fuente)));
            documento.add(new Paragraph(new Phrase("Representante legal",fuente)));
            documento.add(new Paragraph(new Phrase("Nit: "+edspb.getNit(),fuente)));
            documento.close();
        }
        catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }
    
      /**
     * Crea un objeto de tipo Document
     * @return Objeto creado
     */
    private Document createDoc() {
        Document doc = new Document(PageSize.A4,25,25,35,30);
        return doc;
    }

    
     /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons Numero de la cxp generada
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user,String cons,String extension) throws Exception{
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            ruta = ruta + "/" + "cuenta_cobro_" + cons +"_" + fmt.format( new Date() ) +"."+extension;
        }
        catch (Exception e) {
            throw new Exception("Error al generar el directorio: "+e.toString());
        }
        return ruta;
    }
    
    /**
     * Devuelve el nombre del mes del a�o que se le pasa
     * @param mes Numero del mes que se quiere transformar
     * @return Cadena con el nombre del numero de mes
     */
    private String mesToString(int mes){
        String texto = "";
        switch (mes) {
            case 1:
                texto = "Enero";
            break;
            case 2:
                texto = "Febrero";
            break;
            case 3:
                texto = "Marzo";
            break;
            case 4:
                texto = "Abril";
            break;
            case 5:
                texto = "Mayo";
            break;
            case 6:
                texto = "Junio";
            break;
            case 7:
                texto = "Julio";
            break;
            case 8:
                texto = "Agosto";
            break;
            case 9:
                texto = "Septiembre";
            break;
            case 10:
                texto = "Octubre";
            break;
            case 11:
                texto = "Noviembre";
            break;
            case 12:
                texto = "Diciembre";
            break;
            default:
                texto = "Enero";
            break;
        }
        return texto;
    }
    
    private void enviarTramaJsonTransportadoras() throws Exception{
        String respuesta = "{}";
        try {
            
           com.fintra.ws.transportadoras.WsTransportadoras_Service service = new com.fintra.ws.transportadoras.WsTransportadoras_Service();
           com.fintra.ws.transportadoras.WsTransportadoras port = service.getWsTransportadorasPort();
            
            String testConnectionParameter = port.testConnectionParameter(usuario.getLogin(), usuario.getPassword());
            System.out.println(testConnectionParameter);
            String formato = request.getParameter("formato") != null ? request.getParameter("formato") : ""; 
            String json = (formato.equals("s")) ? this.getJsonAnticiposSimplified() : this.getJsonAnticipos(); 
            JsonParser parser = new JsonParser();            
            JsonObject jobj =  parser.parse(json).getAsJsonObject();
         
            String errorCargue = (jobj.get("error")!= null) ? jobj.get("error").getAsString():"";
            if (errorCargue.equals("")){
                String enviarAnticiposLogin = port.enviarAnticiposLogin(json, usuario.getLogin(), usuario.getPassword());
                System.out.println(enviarAnticiposLogin);

                respuesta = "{\"success\":true,\"respuesta\":\"" + enviarAnticiposLogin + "\"}";
            }else{
                respuesta = "{\"success\":false,\"error\":\"" + errorCargue + "\"}";
            }
        
        } catch (Exception ex) {
            respuesta = "{\"success\":false,\"error\":" + ex.getMessage() + "}";
            Logger.getLogger(AdministracionLogisticaAction.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.printlnResponse(respuesta, "application/json;");
        }
    
    }
    
     private String getJsonAnticipos() throws Exception {
        MultipartRequest mreq = null;
        String json = "{}";
        JsonObject obj = new JsonObject();    
        Gson gson = new Gson();
        Boolean isValid = true;
        String errorMessage = "";
        try {
            
            InterfazLogisticaDAO dao = new LogisticaAdminImpl(usuario.getBd());                       
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String usuario_login =  ((usuario != null) ? usuario.getLogin() : "anonimo");
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((usuario != null) ? usuario.getLogin() : "anonimo"));
            String directorioImagenes  =  rb.getString("rutaImagenes");          
            
            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)        
            mreq = new MultipartRequest(request, ruta, 4096 * 2048, new DefaultFileRenamePolicy());
            String filetype = mreq.getContentType("examinar");
            String filename = mreq.getFilesystemName("examinar");
            ruta += "/" + filename;
        
            File archivo = new File(ruta); 
            //Creamos archivo en directorio de imagenes
            String rutaImagenes = directorioImagenes +"transportadora/" + usuario_login + "/" ;
            dao.copiarArchivoEnCarpetaDestino(usuario_login, UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"), rutaImagenes, filename);
            
            JXLRead xls = new JXLRead(archivo.getAbsolutePath());
            xls.obtenerHoja(0);
            String[] datos = null;
            int numfil = xls.numeroFilas();    
            int numcol =  xls.numeroColumnas();
            if (numcol !=  57){               
                errorMessage = "Archivo inv�lido para el formato seleccionado. Verifique n�mero de columnas.";
                json = "{\"success\":false,\"error\":\"" + errorMessage + "\"}";
                return json;
            }
            JsonObject fila_prop, fila_cond, fila_veh, fila_ant;
            JsonArray arr_prop = new JsonArray();
            JsonArray arr_cond = new JsonArray();
            JsonArray arr_int = new JsonArray();
            JsonArray arr_veh = new JsonArray();
            JsonArray arr_ant = new JsonArray();             
            for (int i = 1; i < numfil; i++) {
                if(xls.obtenerDato(i, 0).equals("") || xls.obtenerDato(i, 1).equals("") || xls.obtenerDato(i, 2).equals("") || xls.obtenerDato(i, 3).equals("") || xls.obtenerDato(i, 13).equals("") || xls.obtenerDato(i, 14).equals("") ||
                   xls.obtenerDato(i, 15).equals("") || xls.obtenerDato(i, 17).equals("") || xls.obtenerDato(i, 32).equals("") || xls.obtenerDato(i, 33).equals("") || xls.obtenerDato(i, 34).equals("") || xls.obtenerDato(i, 35).equals("") ||
                   xls.obtenerDato(i, 36).equals("") || xls.obtenerDato(i, 37).equals("") || xls.obtenerDato(i, 40).equals("") || xls.obtenerDato(i, 41).equals("") || xls.obtenerDato(i, 42).equals("") || xls.obtenerDato(i, 47).equals("") ||
                   xls.obtenerDato(i, 48).equals("") || xls.obtenerDato(i, 49).equals("") || xls.obtenerDato(i, 50).equals("") || xls.obtenerDato(i, 51).equals("") || xls.obtenerDato(i, 52).equals("")) {
                        isValid = false;
                        errorMessage = "Informaci�n faltante en fila "+ i;
                        break;
                }else if(!isNumeric(xls.obtenerDato(i, 51))){
                    isValid = false;
                    errorMessage = "Informaci�n inv�lida en fila "+ i +" columna "+ xls.obtenerDato(0, 51)+ ". Debe ser numero";
                    break;
                }else if(!isNumeric(xls.obtenerDato(i, 52))){
                    isValid = false;
                    errorMessage = "Informaci�n inv�lida en fila "+ i +" columna "+ xls.obtenerDato(0, 52)+ ". Debe ser numero";
                    break;
                }
                // Llenamos array de propietarios
                fila_prop = new JsonObject();
                fila_prop.addProperty(xls.obtenerDato(0, 0), xls.obtenerDato(i, 0));
                fila_prop.addProperty(xls.obtenerDato(0, 1), xls.obtenerDato(i, 1));
                fila_prop.addProperty(xls.obtenerDato(0, 2), xls.obtenerDato(i, 2));
                fila_prop.addProperty(xls.obtenerDato(0, 3), xls.obtenerDato(i, 3));
                fila_prop.addProperty(xls.obtenerDato(0, 4), xls.obtenerDato(i, 4));
                fila_prop.addProperty(xls.obtenerDato(0, 5), xls.obtenerDato(i, 5));
                fila_prop.addProperty(xls.obtenerDato(0, 6), xls.obtenerDato(i, 6));
                fila_prop.addProperty(xls.obtenerDato(0, 7), xls.obtenerDato(i, 7));
                fila_prop.addProperty(xls.obtenerDato(0, 8), xls.obtenerDato(i, 8));
                fila_prop.addProperty(xls.obtenerDato(0, 9), xls.obtenerDato(i, 9));
                fila_prop.addProperty(xls.obtenerDato(0, 10), xls.obtenerDato(i, 10));
                fila_prop.addProperty(xls.obtenerDato(0, 11), xls.obtenerDato(i, 11));
                fila_prop.addProperty(xls.obtenerDato(0, 12), xls.obtenerDato(i, 12));
                arr_prop.add(fila_prop);
                
                // Llenamos array de conductores
                fila_cond = new JsonObject();
                fila_cond.addProperty(xls.obtenerDato(0, 13), xls.obtenerDato(i, 13));
                fila_cond.addProperty(xls.obtenerDato(0, 14), xls.obtenerDato(i, 14));
                fila_cond.addProperty(xls.obtenerDato(0, 15), xls.obtenerDato(i, 15));
                fila_cond.addProperty(xls.obtenerDato(0, 16), xls.obtenerDato(i, 16));
                fila_cond.addProperty(xls.obtenerDato(0, 17), xls.obtenerDato(i, 17));
                fila_cond.addProperty(xls.obtenerDato(0, 18), xls.obtenerDato(i, 18));
                fila_cond.addProperty(xls.obtenerDato(0, 19), xls.obtenerDato(i, 19));
                fila_cond.addProperty(xls.obtenerDato(0, 20), xls.obtenerDato(i, 20));
                fila_cond.addProperty(xls.obtenerDato(0, 21), xls.obtenerDato(i, 21));
                fila_cond.addProperty(xls.obtenerDato(0, 22), xls.obtenerDato(i, 22));
                fila_cond.addProperty(xls.obtenerDato(0, 23), xls.obtenerDato(i, 23));
                fila_cond.addProperty(xls.obtenerDato(0, 24), xls.obtenerDato(i, 24));
                fila_cond.addProperty(xls.obtenerDato(0, 25), xls.obtenerDato(i, 25));
                fila_cond.addProperty(xls.obtenerDato(0, 26), xls.obtenerDato(i, 26));
                fila_cond.addProperty(xls.obtenerDato(0, 27), xls.obtenerDato(i, 27));
                fila_cond.addProperty(xls.obtenerDato(0, 28), xls.obtenerDato(i, 28));
                fila_cond.addProperty(xls.obtenerDato(0, 29), xls.obtenerDato(i, 29));
                fila_cond.addProperty(xls.obtenerDato(0, 30), xls.obtenerDato(i, 30));
                fila_cond.addProperty(xls.obtenerDato(0, 31), xls.obtenerDato(i, 31));
                arr_cond.add(fila_cond);
                
                  // Llenamos array de vehiculos
                fila_veh = new JsonObject();
                fila_veh.addProperty(xls.obtenerDato(0, 32), xls.obtenerDato(i, 32));
                fila_veh.addProperty(xls.obtenerDato(0, 33), xls.obtenerDato(i, 33));
                fila_veh.addProperty(xls.obtenerDato(0, 34), xls.obtenerDato(i, 34));
                fila_veh.addProperty(xls.obtenerDato(0, 35), xls.obtenerDato(i, 35));
                fila_veh.addProperty(xls.obtenerDato(0, 36), xls.obtenerDato(i, 36));
                fila_veh.addProperty(xls.obtenerDato(0, 37), xls.obtenerDato(i, 37));
                fila_veh.addProperty(xls.obtenerDato(0, 38), xls.obtenerDato(i, 38));
                fila_veh.addProperty(xls.obtenerDato(0, 39), xls.obtenerDato(i, 39));
                arr_veh.add(fila_veh);
                
                // Llenamos array de anticipos
                fila_ant = new JsonObject();
                fila_ant.addProperty(xls.obtenerDato(0, 40), xls.obtenerDato(i, 40));
                fila_ant.addProperty(xls.obtenerDato(0, 41), xls.obtenerDato(i, 41));
                fila_ant.addProperty(xls.obtenerDato(0, 42), xls.obtenerDato(i, 42));
                fila_ant.addProperty(xls.obtenerDato(0, 43), xls.obtenerDato(i, 43));
                fila_ant.addProperty(xls.obtenerDato(0, 44), xls.obtenerDato(i, 44));
                fila_ant.addProperty(xls.obtenerDato(0, 45), xls.obtenerDato(i, 45));
                fila_ant.addProperty(xls.obtenerDato(0, 46), xls.obtenerDato(i, 46));
                fila_ant.addProperty(xls.obtenerDato(0, 47), xls.obtenerDato(i, 47));
                fila_ant.addProperty(xls.obtenerDato(0, 48), xls.obtenerDato(i, 48));
                fila_ant.addProperty(xls.obtenerDato(0, 49), xls.obtenerDato(i, 49));
                fila_ant.addProperty(xls.obtenerDato(0, 50), xls.obtenerDato(i, 50));
                fila_ant.addProperty(xls.obtenerDato(0, 51), Double.parseDouble(xls.obtenerDato(i, 51)));
                fila_ant.addProperty(xls.obtenerDato(0, 52), Double.parseDouble(xls.obtenerDato(i, 52)));
                fila_ant.addProperty(xls.obtenerDato(0, 53), Double.parseDouble(xls.obtenerDato(i, 53)));
                fila_ant.addProperty(xls.obtenerDato(0, 54), Double.parseDouble(xls.obtenerDato(i, 54)));
                fila_ant.addProperty(xls.obtenerDato(0, 55), xls.obtenerDato(i, 55));
                fila_ant.addProperty(xls.obtenerDato(0, 56), xls.obtenerDato(i, 56));
                arr_ant.add(fila_ant);
                                                 
            }
          if(isValid){  
            //A�adimos los arreglos al objeto
            obj.add("propietarios", arr_prop);
            obj.add("conductores", arr_cond);
            obj.add("intermediarios", arr_int);
            obj.add("vehiculos", arr_veh);
            obj.add("anticipos", arr_ant);
       
            json=gson.toJson(obj);          
          }else{
               json = "{\"success\":false,\"error\":\"" + errorMessage + "\"}";
           }
            System.out.println(json);
            xls.cerrarLibro();
        } catch (Exception e) {
            json = "{\"success\":false,\"error\":" + e.getMessage() + "}";
            e.printStackTrace();
            System.out.println(e);
        } finally {
           // mreq.getFile("examinar").delete();
            return json;
        }
    }
     
    private void cargarMenuTransportadora() {
        try {
            Usuario us = (Usuario) request.getSession().getAttribute("Usuario");            
            String usuario = (us.getLogin() != null) ? us.getLogin() : "";       
            InterfazLogisticaDAO dao =new LogisticaAdminImpl(us.getBd());
            ArrayList<MenuOpcionesModulos> listOpcionesTransportadora = dao.cargarMenuTransportadora(usuario,us.getDstrct());
            Gson gson = new Gson();
            String json;
            json =  gson.toJson(listOpcionesTransportadora);
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     private void cargarAnticiposTransportadora() {
                 
        try{
            InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
            String transportadora = request.getParameter("id_transportadora") != null ? request.getParameter("id_transportadora") : "";
            String fechaini = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechafin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String planilla = request.getParameter("planilla") != null ? request.getParameter("planilla") : "";       
            String placa = request.getParameter("placa") != null ? request.getParameter("placa") : ""; 
            String producto = request.getParameter("producto") != null ? request.getParameter("producto") : ""; 
            String json = dao.cargarAnticposTransportadora(transportadora, fechaini, fechafin, planilla, placa, producto);          
            this.printlnResponse(json, "application/json;"); 
           
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
       
     private void enviarReanticiposTransportadora() throws Exception {
        String respuesta = "{}";         
        try{
            
            com.fintra.ws.transportadoras.WsTransportadoras_Service service = new com.fintra.ws.transportadoras.WsTransportadoras_Service();
            com.fintra.ws.transportadoras.WsTransportadoras port = service.getWsTransportadorasPort();
          
            String listreanticipo = request.getParameter("listadoReanticipos");
             
            String enviarReanticipoLogin = port.enviarReanticipoLogin(listreanticipo, usuario.getLogin(), usuario.getPassword());          
            System.out.println(enviarReanticipoLogin);
            respuesta = "{\"success\":true,\"respuesta\":\"" + enviarReanticipoLogin + "\"}";             
           
        }catch(Exception e){
            respuesta = "{\"success\":false,\"error\":" + e.getMessage() + "}";
            e.printStackTrace();
        }finally{
            this.printlnResponse(respuesta, "application/json;"); 
        }
    }
   
    private void cargarReanticiposTransportadora() {

        try {
            
            com.fintra.ws.transportadoras.WsTransportadoras_Service service = new com.fintra.ws.transportadoras.WsTransportadoras_Service();
            com.fintra.ws.transportadoras.WsTransportadoras port = service.getWsTransportadorasPort();
          
            InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
            String transportadora = request.getParameter("id_transportadora") != null ? request.getParameter("id_transportadora") : "";          
            String planilla = request.getParameter("planilla") != null ? request.getParameter("planilla") : ""; 
            String jsonListarReanticipo = dao.generarJsonListarReanticipo(transportadora, planilla);
            System.out.println(jsonListarReanticipo);
            String json = port.listarReanticiposLogin(jsonListarReanticipo, usuario.getLogin(), usuario.getPassword());
            this.printlnResponse(json, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
          
    private void anularAnticiposTransportadora() throws Exception {
        String respuesta = "{}";
        try {
            
            com.fintra.ws.transportadoras.WsTransportadoras_Service service = new com.fintra.ws.transportadoras.WsTransportadoras_Service();
            com.fintra.ws.transportadoras.WsTransportadoras port = service.getWsTransportadorasPort();

            String listanovedad = request.getParameter("listadoNovedades");
            System.out.println(listanovedad);
            String agregarNovedadLogin = port.agregarNovedadLogin(listanovedad, usuario.getLogin(), usuario.getPassword());
            System.out.println(agregarNovedadLogin);
            respuesta = "{\"success\":true,\"respuesta\":\"" + agregarNovedadLogin + "\"}";

        } catch (Exception e) {
            respuesta = "{\"success\":false,\"error\":" + e.getMessage() + "}";
            e.printStackTrace();
        } finally {
            this.printlnResponse(respuesta, "application/json;");
        }
    }
    
    private void anularReanticiposTransportadora() throws Exception {
        String respuesta = "{}";
        try {
            
            com.fintra.ws.transportadoras.WsTransportadoras_Service service = new com.fintra.ws.transportadoras.WsTransportadoras_Service();
            com.fintra.ws.transportadoras.WsTransportadoras port = service.getWsTransportadorasPort();
            
            JsonParser jsonParser = new JsonParser();
            String listadoReanticipo = request.getParameter("listado");
            System.out.println(listadoReanticipo);
            JsonObject jo = (JsonObject) jsonParser.parse(listadoReanticipo);
           
            JsonObject jreant = (JsonObject) jsonParser.parse(listadoReanticipo);
            JsonArray jsonArrReant = jreant.getAsJsonArray("reanticipo");          
            jsonArrReant.get(0).getAsJsonObject().addProperty("usuario_anulacion", usuario.getLogin());  
            jsonArrReant.get(0).getAsJsonObject().addProperty("descripcion_anulacion", "ANULACION REANTICIPO"); 
         
            String anularReanticipoLogin = port.anularReanticipoLogin(new Gson().toJson(jsonArrReant), usuario.getLogin(), usuario.getPassword());
            System.out.println(anularReanticipoLogin);
            respuesta = "{\"success\":true,\"respuesta\":\"" + anularReanticipoLogin + "\"}";

        } catch (Exception e) {
            respuesta = "{\"success\":false,\"error\":" + e.getMessage() + "}";
            e.printStackTrace();
        } finally {
            this.printlnResponse(respuesta, "application/json;");
        }
    }
    
    private void cargarProductosTransportadora() {
        String json = "{}";
        InterfazLogisticaDAO dao = null;         
        try {
            if (this.getAgencia() != null) {
                dao = new LogisticaAdminImpl(usuario.getBd());
                Usuario u =new Usuario();
                u.setBd(usuario.getBd());
                u.setNombre(usuario.getNombre());
                u.setLogin(getAgencia().getUser_trans());
                json = dao.cargarProductosTransportadora(u);              
            } else {
                json = "{\"success\":false,\"error\":\"No existe informacion de agencia asociada al usuario\"}";
            }        
           
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesoEjecutivoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String getJsonAnticiposSimplified() throws Exception {

        MultipartRequest mreq = null;
        String json = "{}";
        Gson gson = new Gson();
        Boolean isValid = true;
        String errorMessage = "";

        try {
            //validamos la agencia
            if (this.getAgencia() != null) {
                
                InterfazLogisticaDAO dao = new LogisticaAdminImpl(usuario.getBd());
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String usuario_login = ((usuario != null) ? usuario.getLogin() : "anonimo");
                String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                        + ((usuario != null) ? usuario.getLogin() : "anonimo"));
                String directorioImagenes = rb.getString("rutaImagenes");
                String producto = request.getParameter("producto") != null ? request.getParameter("producto") : "";

                //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)        
                mreq = new MultipartRequest(request, ruta, 4096 * 2048, new DefaultFileRenamePolicy());
                String filetype = mreq.getContentType("examinar");
                String filename = mreq.getFilesystemName("examinar");
                ruta += "/" + filename;

                File archivo = new File(ruta);
                //Creamos archivo en directorio de imagenes
                String rutaImagenes = directorioImagenes + "transportadora/" + usuario_login +"/";
                dao.copiarArchivoEnCarpetaDestino(usuario_login, UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"), rutaImagenes, filename);

                JXLRead xls = new JXLRead(archivo.getAbsolutePath());
                xls.obtenerHoja(0);
                String[] datos = null;
                
                if (xls.numeroColumnas() != 21 ) {
                    errorMessage = "Archivo inv�lido para el formato seleccionado. Verifique n�mero de columnas.";
                    json = "{\"success\":false,\"error\":\"" + errorMessage + "\"}";
                    return json;
                }

                ArrayList<BeansPropietario> listaPropietario = new ArrayList();
                ArrayList<BeansConductor> listaConductor = new ArrayList();
                ArrayList<String> listaIntermediario = new ArrayList();
                ArrayList<BeansVehiculo> listaVehiculo = new ArrayList();
                ArrayList<BeansAnticipo> listaAnticipo = new ArrayList();
                TramaJsonAnticipo tramaJsonAnticipo = new TramaJsonAnticipo();

                for (int i = 1; i < xls.numeroFilas(); i++) {
                    // Llenamos beans de propietarios
                    BeansPropietario fila_prop = new BeansPropietario("", "", "", "", "", "", "", "", "", "", "", "N", "");
                    
                    if (xls.obtenerDato(i, 0).equals("") || xls.obtenerDato(i, 1).equals("") || xls.obtenerDato(i, 2).equals("") || xls.obtenerDato(i, 3).equals("") || xls.obtenerDato(i, 4).equals("") || xls.obtenerDato(i, 5).equals("")
                            || xls.obtenerDato(i, 6).equals("") || xls.obtenerDato(i, 7).equals("") || xls.obtenerDato(i, 8).equals("") || xls.obtenerDato(i, 9).equals("") || xls.obtenerDato(i, 10).equals("") || xls.obtenerDato(i, 11).equals("")
                            || xls.obtenerDato(i, 12).equals("") || xls.obtenerDato(i, 13).equals("") || xls.obtenerDato(i, 14).equals("")) {
                        isValid = false;
                        errorMessage = "Faltan campos por llenar";
                        break;
                    } else if (!isNumeric(xls.obtenerDato(i, 14))) {
                        isValid = false;
                        errorMessage = "Informaci�n inv�lida en fila " + i + " columna " + xls.obtenerDato(0, 15) + ". Debe ser numero";
                        break;
                    }else if(producto.equals("ANT00002") && (xls.obtenerDato(i, 15).equals("") || xls.obtenerDato(i, 16).equals("") ||xls.obtenerDato(i, 17).equals("") ||
                                                            xls.obtenerDato(i, 18).equals("") ||xls.obtenerDato(i, 19).equals("")||xls.obtenerDato(i, 20).equals("") )) {
                        isValid = false;
                        errorMessage = "Faltan por llenar los campos de la informacion bancaria";
                    
                    }                    

                    fila_prop.setClasificacion(xls.obtenerDato(i, 0));
                    fila_prop.setNit(xls.obtenerDato(i, 1));
                    fila_prop.setTipo_doc(xls.obtenerDato(i, 2));
                    fila_prop.setNombre(xls.obtenerDato(i, 3));
                    dao.setearBeansPropietario(fila_prop);
                    listaPropietario.add(fila_prop);

                    // Llenamos beans de conductores
                    BeansConductor fila_cond = new BeansConductor("","","", "", "", "", "", "", "", "", "", "N", "", "", "", "", "", "", "");
                    fila_cond.setClasificacion("PN");
                    fila_cond.setNit(xls.obtenerDato(i, 4));
                    fila_cond.setTipo_doc(xls.obtenerDato(i, 5));
                    fila_cond.setNombre(xls.obtenerDato(i, 6));
                    dao.setearBeansConductor(fila_cond);
                    listaConductor.add(fila_cond);

                    // Llenamos beans de vehiculos
                    BeansVehiculo fila_veh = new BeansVehiculo();
                    fila_veh.setCedula_propietario(fila_prop.getNit());
                    fila_veh.setPlaca(xls.obtenerDato(i, 7));
                    fila_veh.setMarca(xls.obtenerDato(i, 8));
                    fila_veh.setModelo(xls.obtenerDato(i, 9));
                    fila_veh.setServicio(xls.obtenerDato(i, 10));
                    fila_veh.setTipo_vehiculo("");
                    fila_veh.setVeto("N");
                    fila_veh.setVeto_causal("");
                    listaVehiculo.add(fila_veh);

                    // Llenamos beans de anticipos
                    BeansAnticipo fila_ant = new BeansAnticipo();
                    fila_ant.setCodigo_empresa(getAgencia().getCod_transportadora());
                    fila_ant.setCodigo_agencia(getAgencia().getCod_agencia());
                    fila_ant.setPlaca(xls.obtenerDato(i, 7));
                    fila_ant.setTipo_doc_conductor(xls.obtenerDato(i, 5));
                    fila_ant.setCedula_conductor(xls.obtenerDato(i, 4));
                    fila_ant.setCodigo_producto(producto);
                    fila_ant.setOrigen(xls.obtenerDato(i, 11));
                    fila_ant.setDestino(xls.obtenerDato(i, 12));
                    fila_ant.setPlanilla(xls.obtenerDato(i, 13));
                    fila_ant.setValor_planilla(Float.parseFloat(xls.obtenerDato(i, 14)));
                    fila_ant.setValor_neto_anticipo(Float.parseFloat(xls.obtenerDato(i, 14)));
                    fila_ant.setBanco(xls.obtenerDato(i, 15));
                    fila_ant.setSucursal(xls.obtenerDato(i, 16));
                    fila_ant.setCedula_titular_cuenta(xls.obtenerDato(i, 17));
                    fila_ant.setNombre_titular_cuenta(xls.obtenerDato(i, 18));
                    fila_ant.setTipo_cuenta(xls.obtenerDato(i, 19));
                    fila_ant.setNo_cuenta(xls.obtenerDato(i, 20));
                    dao.setearBeansAnticipo(fila_ant);
                    listaAnticipo.add(fila_ant);

                }

                if (isValid) {
                    //A�adimos los beans al beans principal
                    tramaJsonAnticipo.setPropietario(listaPropietario);
                    tramaJsonAnticipo.setConductor(listaConductor);
                    tramaJsonAnticipo.setIntermediarios(listaIntermediario);
                    tramaJsonAnticipo.setVehiculo(listaVehiculo);
                    tramaJsonAnticipo.setAnticipo(listaAnticipo);

                    json = gson.toJson(tramaJsonAnticipo);
                } else {
                    json = "{\"success\":false,\"error\":\"" + errorMessage + "\"}";
                }

                System.out.println(json);
                xls.cerrarLibro();

            } else {
                json="{\"success\":false,\"error\":\"No existe informacion de agencia asociada al usuario\"}";
            }
        } catch (Exception e) {
            json = "{\"success\":false,\"error\":" + e.getMessage() + "}";
            e.printStackTrace();
            System.out.println(e);
        } finally {
            // mreq.getFile("examinar").delete();
            return json;
        }
    }
    
    private void getListaTansportadorasAgencias() throws Exception {
        String json = "{}";
        InterfazLogisticaDAO dao = null;
         if (this.getAgencia() != null) {
            dao = new LogisticaAdminImpl(usuario.getBd());
            Usuario u =new Usuario();
            u.setBd(usuario.getBd());
            u.setNombre(usuario.getNombre());
            u.setLogin(getAgencia().getUser_trans());
            
            json = dao.getTransportadorasLista(u);
        } else {
            json = "{\"success\":false,\"error\":\"No existe informacion de agencia asociada al usuario\"}";
        }

        this.printlnResponse(json, "application/json;");
    }
     
    private boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
    
    private void agenciaLogin() throws Exception {
        InterfazLogisticaDAO dao = new LogisticaAdminImpl(usuario.getBd());
        agencia = dao.getInfoAgenciaUsuario(usuario.getLogin());
        setAgencia(agencia);

    }

    public BeansAgencia getAgencia() {
        return agencia;
    }

    public void setAgencia(BeansAgencia agencia) {
        this.agencia = agencia;
    }

    private void reversarLote() {
        try {
            
            String data = request.getParameter("datajson") != null ? request.getParameter("datajson") : "";
            JsonArray readJsonArray = readJsonArrayParameter(data);
            InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
            this.printlnResponse(dao.reversarLote(readJsonArray, usuario), "application/json;");
            
        } catch (Exception ex) {
            Logger.getLogger(AdministracionLogisticaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buscarLote() {
        String id_transportadora = request.getParameter("id_transportadora") != null ? request.getParameter("id_transportadora") : "";
        InterfazLogisticaDAO dao =new LogisticaAdminImpl(usuario.getBd());
        try {
            this.printlnResponse(dao.buscarLote(id_transportadora), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionLogisticaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void getListaTansportadorasXusuario() throws Exception {
        InterfazLogisticaDAO dao = new LogisticaAdminImpl(usuario.getBd());
        this.printlnResponse(dao.getTransportadorasListaXusuario(usuario), "application/json;");
    }
    
    
    
}
