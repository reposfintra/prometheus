/*
 * AutoAnticipoNullifyAction.java
 *
 * Created on 18 de junio de 2005, 04:15 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class AutoAnticipoNullifyAction extends Action {
    
    /** Creates a new instance of AutoAnticipoNullifyAction */
    public AutoAnticipoNullifyAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/anticipo_placas/anticipopNullify.jsp";
                
        String placa = request.getParameter("pl").toUpperCase();
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        
        try{    
            
            model.autoAnticipoService.buscar(placa);
            
            if(model.autoAnticipoService.getAuto_Anticipo()!= null){
                Auto_Anticipo aa = new Auto_Anticipo();
                aa.setPlaca(placa);                
                aa.setUser_update(usuario.getLogin());
                
                model.autoAnticipoService.nullify(aa);
                
                next = next + "?msg=exitoA&pl=" + placa;            
            }            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}

