/********************************************************************
 *      Nombre Clase.................   FacturaCargarPAction.java
 *      Descripci�n..................   Action que se encarga de abrir la busqueda de proveedores
 *      Autor........................   David Lamadrid
 *      Fecha........................   12 de octubre de 2005, 06:36 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaCargarPAction extends Action{
    
    /** Creates a new instance of FacturaCargarP */
    public FacturaCargarPAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            
            //ivan 21 julio 2006
            HttpSession session = request.getSession();
            Usuario usuario     = (Usuario) session.getAttribute("Usuario");
            
            String maxfila = request.getParameter("maxfila");
            String x=(request.getParameter("x")==null?"":request.getParameter("x"));
            String proveedor=(request.getParameter("proveedor")==null?"":request.getParameter("proveedor"));
            String banco=(request.getParameter("banco")==null?"":request.getParameter("banco"));
            String sucursal=(request.getParameter("sucursal")==null?"":request.getParameter("sucursal"));
            String subtotal=(request.getParameter("subtotal")==null?"":request.getParameter("subtotal"));
            int plazo = Integer.parseInt((request.getParameter("plazo")==null?"0":request.getParameter("plazo")));
            String moneda_banco = (request.getParameter("moneda_banco")!=null)?request.getParameter("moneda_banco"):"";
            String agenciaBanco = (request.getParameter("agenciaBanco")!=null)?request.getParameter("agenciaBanco"):"";
            String beneficiario = (request.getParameter("beneficiario")!=null)?request.getParameter("beneficiario"):"";
            String hc = (request.getParameter("hc")!=null)?request.getParameter("hc"):"";
           
            String agenteRet = "";
            String rica      = "";
            String rfte      = "";
            String riva      = "";
            
            model.tblgensvc.searchDatosTablaArea();
            model.cxpDocService.obtenerNumeroFXP(proveedor);
            model.servicioBanco.loadSusursales(banco);
            
            CXP_Doc factura = new CXP_Doc();
            factura = model.cxpDocService.getFactura();
            factura.setProveedor(proveedor);
            factura.setBanco(banco);
            factura.setSucursal(sucursal);
            factura.setPlazo(plazo);
            factura.setMoneda(moneda_banco);
            factura.setMoneda_banco(moneda_banco);
            System.out.println("AGENCI BANCO--" + agenciaBanco);
            factura.setAgenciaBanco(agenciaBanco);
            factura.setBeneficiario(beneficiario);
            factura.setHandle_code(hc);
            String prov ="";
            
            Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
            if( o_proveedor!=null ){
                prov      = o_proveedor.getC_nit();
                agenteRet = o_proveedor.getC_agente_retenedor();
                rica      = o_proveedor.getC_autoretenedor_ica();
                rfte      = o_proveedor.getC_autoretenedor_rfte();
                riva      = o_proveedor.getC_autoretenedor_iva();
                model.cxpDocService.BuscarDocumentos(usuario.getDstrct(), prov, factura.getTipo_documento_rel()); 
            }
            if(!agenteRet.equals("S")){
                
                if(rica.equals("N")){
                    factura.setRica("");
                }
                if( rfte.equals("N")){
                    factura.setRfte("");
                }
                if( riva.equals("N")){
                    factura.setRiva("");
                }
            }else{
                factura.setRica("");
                factura.setRfte("");
                factura.setRiva("");
            }
            
            model.cxpDocService.setFactura(factura);
            Vector vItems = model.cxpItemDocService.getVecCxpItemsDoc();
            Vector vitemsModif = new Vector();
            for(int i=0;i<vItems.size();i++){
                CXPItemDoc item = (CXPItemDoc)vItems.elementAt(i);
                if(item.getDescripcion()!=null){
                    
                    System.out.println(item.getVlr()+"---"+item.getDescripcion());
                    Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos();
                    Vector vImpuestosPorItem = item.getVItems();
                    Vector vImpPorItem= new Vector();
                    //Ivan 24 julio 2006.
                    String codIva ="";
                    for(int a=0;a<vTipoImp.size();a++){
                        String Impuesto = (String)vTipoImp.elementAt(a);
                        CXPImpItem impuestoItem = (CXPImpItem)vImpuestosPorItem.elementAt(a);
                        impuestoItem.setCod_impuesto("");//Agrege esto
                        item.setIva("");
                        item.setPorc_riva("");
                        item.setPorc_rica("");
                        item.setPorc_rfte("");
                        
                       /* if(Impuesto.equals("IVA"))
                            codIva = (impuestoItem.getCod_impuesto()!=null)?impuestoItem.getCod_impuesto():"";
                            
                            if(!agenteRet.equals("S")){
                                
                                if(Impuesto.equals("RICA") && rica.equals("N")){
                                    impuestoItem.setCod_impuesto("");
                                }else if(Impuesto.equals("RFTE") && rfte.equals("N")){
                                    impuestoItem.setCod_impuesto("");
                                }else if(Impuesto.equals("RIVA") && riva.equals("N")){
                                    impuestoItem.setCod_impuesto("");
                                }else if(Impuesto.equals("RIVA") && riva.equals("S") && !codIva.equals("")){
                                    Tipo_impuesto datos =  model.TimpuestoSvc.buscarImpuestoPorCodigos( "IVA", codIva, usuario.getDstrct(),"");
                                    if(datos!= null){
                                       impuestoItem.setCod_impuesto(datos.getCod_cuenta_contable());
                                       item.setIva(String.valueOf(datos.getPorcentaje1()));
                                    }
                                }
                                
                                
                                
                            }else if(!Impuesto.equals("IVA")){
                                impuestoItem.setCod_impuesto("");
                            }*/
                            
                            vImpPorItem.add(impuestoItem);
                            ////////////////////////////////////
                    }
                    item.setVItems( vImpPorItem);
                    item.setVCopia(vImpPorItem);
                    vitemsModif.add(item);
                }else{
                    vitemsModif.add(item);
                }
            }
            model.cxpItemDocService.setVecCxpItemsDoc(vitemsModif);
            int num_items=0;
            
            try {
                num_items  = Integer.parseInt(""+ request.getParameter("num_items"));
            }
            catch(java.lang.NumberFormatException e) {
                plazo=0;
                num_items=1;
            }
            
            String foco = "fecha_documento";
            if (!factura.getTipo_documento().equals("010")){
                foco = "documento_relacionado";
            }
            String next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp?op=cargarB&num_items="+num_items+"&x="+x+"&subtotal="+subtotal+"&maxfila"+maxfila+"&foco="+foco;
            //System.out.println ("next en Filtro"+next);
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
    }
    
}
