
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;


/**
 * Action para comunicar la interfaz JSP
 * con las consultas de base de Datos
 *
 * @Clase ........ FechaChequesAction.java
 * @Autor ........ Mario Fontalvo
 * @Fecha ........ 2005-10-20 11:27 AM
 * @version ...... 1.0
 * @Copyrigth .... Transporte sanchez Polo S.A.
 */

public class FechasChequesAction extends Action {
    
    /**
     * Constructor de la Clase
     */
    public FechasChequesAction() {
    }
    
    /**
     * Metodo Principal de la Clase
     * @see com.tsp.operation.model.egresoService
     * @throws ServletException .
     * @throws InformationException .
     */
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            
            HttpSession session = (HttpSession) request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            String Opcion      = getParameter("Opcion"     , null);
            String fltBanco    = getParameter("fltBanco"   , "%");
            String fltSucursal = getParameter("fltSucursal", "%");
            int    fltTipo     = Integer.parseInt(getParameter("fltTipo","0"));
            String fltAgencia  = (!usuario.getId_agencia().equals("")? usuario.getId_agencia(): "ALL");
            
            if (usuario.getLogin().equalsIgnoreCase("IBRITO")){
                fltAgencia = "BQ";
            }
            
            
            String [] codigos  = request.getParameterValues("codigos");
            String [] fechas   = request.getParameterValues("fechas");
            
            
            if (Opcion!=null){
                
                if (Opcion.equals("Filtrar")){
                    model.egresoService.BuscarCheques(fltTipo, fltAgencia , fltBanco, fltSucursal);
                    session.setAttribute("fltBanco"   , fltBanco);
                    session.setAttribute("fltSucursal", fltSucursal);
                    session.setAttribute("fltTipo"    , String.valueOf(fltTipo));
                }
                
                else if (Opcion.equals("Actualizar")){
                    for (int i=0; fechas!=null && i<fechas.length; i++){
                        String [] dt = codigos[i].split("~");
                        
                        if (!fechas[i].equals("")){
                            //System.out.println("Modificar : "+ codigos[i] + " " + fechas[i]);
                            model.egresoService.ActualizarFechasCheques(fltTipo, dt[0], dt[1], dt[2], dt[3], usuario.getLogin(), fechas[i]);
                        }
                        
                    }
                    model.egresoService.BuscarCheques(fltTipo, fltAgencia, fltBanco, fltSucursal);
                }
            }
            
            
            final String next = "/cheques/regchqent.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }catch (Exception e){
            throw new ServletException("Error en  : FechasChequesAction  ....\n" + e.getMessage());
        }
    }
    
    /**
     * Metodo que toma una variable del objeto request
     * en caso de ser nula este en lugar de devolver el
     * valor de la variable devolvera otro valor por defecto
     * @autor ...... Mario Fontalvo
     * @param name . Indica el nombre de la Variable
     * @param otro . Este Indica el valor por defecto que retornara la funcion en caso de ser nulas
     * @return ..... devulve el valor de una variable en caso de ser diferente de null si no devuelve el valor por defecto
     */
    
    public String getParameter(String name, String otro){
        return (request.getParameter(name)!=null?request.getParameter(name):otro);
    }
    
    
}