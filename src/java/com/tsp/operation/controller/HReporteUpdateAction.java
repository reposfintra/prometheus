/**
 *
 */
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class HReporteUpdateAction extends Action {
  static Logger logger = Logger.getLogger(HReporteUpdateAction.class);
  
  /**
   * Executes the action
   */
  public void run() throws ServletException, InformationException {
      String next = null;
    
      HttpSession session = request.getSession();
      Usuario usuario = (Usuario)session.getAttribute("Usuario");
      ReporteConductor rConductor = new ReporteConductor();
      rConductor.setPlanilla(request.getParameter("planilla"));
      rConductor.setPlaca_vehiculo(request.getParameter("placa_vehiculo"));
      rConductor.setPlaca_unidad_carga(request.getParameter("placa_unidad_carga"));
      rConductor.setContenedores(request.getParameter("contenedores"));
      rConductor.setPrecinto(request.getParameter("precinto"));
      rConductor.setRuta(request.getParameter("ruta"));
      rConductor.setUsuario(usuario.getLogin());
      rConductor.setConductor(request.getParameter("conductor"));
      rConductor.setComentario(request.getParameter("comentarios"));
      rConductor.setCia(request.getParameter("cia"));
      rConductor.setAgcUsuario(usuario.getId_agencia());
      
      try{
        model.hReporteService.updateHjReporte(rConductor);
        rConductor.setUsuario(usuario.getNombre());
        model.hReporteService.generarReporteHojaConductor(rConductor);
        logger.info(usuario.getNombre() + " Modific� Hoja de Reporte con planilla = " + rConductor.getPlanilla());
        next = "/hojareporte/PantallaReporteHR.jsp";
      }
      catch (SQLException e){
        throw new ServletException(e.getMessage());
      }
      // Redireccionar a la p�gina indicada.
      this.dispatchRequest(next);
  }
}
