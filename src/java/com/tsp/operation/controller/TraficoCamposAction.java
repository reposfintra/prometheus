/********************************************************************
 *      Nombre Clase.................   TraficoCamposAction.java
 *      Descripci�n..................   Action para cargar la pagina de Filtros de Control de trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  David A
 */
public class TraficoCamposAction extends Action{
    
    /** Creates a new instance of TraficoCamposAction */
    public TraficoCamposAction () {
    }
    public void run () throws ServletException, InformationException { 
        try{      
            String user  = request.getParameter ("usuario");
            String clas  = request.getParameter ("clas");
            String campo = request.getParameter ("campo");
            
            if (clas.equals ("null")){
                clas="";
            }
            
            ////System.out.println ("genera Accion Campos  ");
            String next = "/jsp/trafico/controltrafico/filtro.jsp?";
            
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
}
