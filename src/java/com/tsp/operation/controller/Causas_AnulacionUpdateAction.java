/*
 * Codigo_demoraUpdateAction.java
 *
 * Created on 26 de junio de 2005, 03:47 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Causas_AnulacionUpdateAction extends Action{
    
    /** Creates a new instance of Codigo_demoraUpdateAction */
    public Causas_AnulacionUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="/jsp/masivo/causa_anulacion/causa_anulacionModificar.jsp?lista=ok";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
    
        try{
            Causas_Anulacion ca = new Causas_Anulacion();
            ca.setCodigo(codigo);
            ca.setDescripcion(descripcion);
            ca.setUsuario(usuario.getLogin());
            ca.setDistrito(usuario.getDstrct());
            model.causas_anulacionService.setCausa_anulacion(ca);
            if(request.getParameter("modif")!=null){
                model.causas_anulacionService.update();
                request.setAttribute("mensaje","MsgModificado");
                next = next + "&reload=ok";
            }else{
                model.causas_anulacionService.anular();
                request.setAttribute("mensaje","MsgAnulado");
                next="/jsp/trafico/mensaje/MsgAnulado.jsp";
            }
            model.causas_anulacionService.consultar("", "");
            model.causas_anulacionService.search(codigo);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }
    
}
