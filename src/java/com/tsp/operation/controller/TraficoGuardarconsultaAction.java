/********************************************************************
 *      Nombre Clase.................   TraficoGuardarconsultaAction.java
 *      Descripci�n..................   Action que se encarga de guardar una consulta realizada por el usuraio
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  dlamadrid
 */
public class TraficoGuardarconsultaAction extends Action{
    
    /** Creates a new instance of TraficoGuardarconsultaAction */
    public TraficoGuardarconsultaAction () {
    }
    static Logger logger = Logger.getLogger(TraficoGuardarconsultaAction.class);
    public void run () throws ServletException, InformationException {
        try{
            
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String user=""+usuario.getLogin ();
            logger.info ("Usuario:"+user);
            String dstrc=""+usuario.getDstrct ();
            
            //String user        =""+ request.getParameter ("usuario");
            logger.info ("Usuario:"+user);
            String linea        =""+ request.getParameter ("linea");
            String conf        = ""+request.getParameter ("conf");
            String clas        = ""+request.getParameter ("clas");
            String nombre        = ""+request.getParameter ("nombre");
            logger.info ("nombre"+nombre);
            String campo        =""+ request.getParameter ("campo");
            String zonasUsuario = request.getParameter("zonasUsuario");//23.06.2006
            
            String []filtros   = request.getParameterValues ("Cfiltro");
            
            
            String cadena="";
            String filtroCadena="";
            for(int i=0; i<filtros.length; i++){
                cadena = cadena+filtros[i];
                filtroCadena = filtros[i].replaceAll("\\|","=")+","+filtroCadena;
            }
            logger.info("Filtro :"+filtroCadena);
            
            Vector vectorFiltro = new Vector ();
            vectorFiltro=model.traficoService.obtenerVectorFiltros (filtros);
            cadena="";
            for(int i=0;i<vectorFiltro.size ();i++){
                cadena=cadena+vectorFiltro.elementAt (i);
            }
            
            cadena="";
            Vector vectorFiltroO = new Vector ();
            vectorFiltroO=model.traficoService.ordenarVectorFiltro (vectorFiltro);
            cadena="";
            for(int i=0;i<vectorFiltroO.size ();i++){
                cadena=cadena+vectorFiltroO.elementAt (i);
            }
            
            
            String filtro= model.traficoService.filtro (vectorFiltroO);
            
            model.traficoService.generarVZonas (user);
            Vector zonas = model.traficoService.obdtVectorZonas ();
            String validacion="where codzona='-1'";
            if ((zonas == null)||zonas.size ()<=0){}else{
                validacion = model.traficoService.validacion (zonas);
            }
            
            if(filtro.length ()>1){
                filtro="where "+validacion +" and "+filtro;
            }
            if(filtro.equals ("")){
                filtro=validacion+filtro;
            }
            if(linea.equals ("null") ){
                linea=model.traficoService.obtlinea ();
            }
            if(conf.equals ("null")||conf.equals ("")){
                conf = model.traficoService.obtConf ();
            }
            if(clas.equals ("null")||clas.equals ("")){
                clas="";
            }
            Vector colum= new Vector ();
            colum = model.traficoService.obtenerCampos (linea);
            
            String consulta = model.traficoService.getConsulta (conf, filtro, clas);
            logger.info ("Consulta final en guardar Filtro :"+consulta);
            
            FiltrosUsuarios u = new FiltrosUsuarios ();
            logger.info ("bandera");
            u.setIdUsuario (user);
            u.setConsulta (consulta);
            u.setLinea (linea);
            u.setNombre (nombre);
            u.setDstrct (dstrc);
            u.setFiltro(filtroCadena);
            model.traficoService.insertarFiltro (u);
            
            
            String next = "/jsp/trafico/controltrafico/filtro.jsp?linea="+linea+"&conf="+conf+"&clas="+clas+"&zonasUsuario="+zonasUsuario;
            RequestDispatcher rd = application.getRequestDispatcher (next);
            
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
        
    }
    
}
