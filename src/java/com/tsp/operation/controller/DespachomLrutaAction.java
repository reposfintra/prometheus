/*
 * DespachomLrutaAction.java
 *
 * Created on 21 de noviembre de 2005, 05:38 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachomLrutaAction extends Action {
    
    /** Creates a new instance of DespachomLrutaAction */
    public DespachomLrutaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        try {
            String sw=""+request.getParameter("sw");
            String ciudad=""+request.getParameter("c_ciudad");
            model.rutaService.vClientes(ciudad);
            if(sw.equals("1")){
                next="/jsp/trafico/despacho_manual/ruta.jsp?accion=1";
            }
            else{
                next="/jsp/trafico/despacho_manual/dxruta.jsp?accion=1";
            }
        }
        catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
