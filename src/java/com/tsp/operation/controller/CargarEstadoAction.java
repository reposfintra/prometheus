/*
 * CargarEstadoAction.java
 *
 * Created on 8 de marzo de 2005, 09:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIBASMO
 */
public class CargarEstadoAction extends Action{
    
    /** Creates a new instance of CargarEstadoAction */
    public CargarEstadoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next = "/jsp/trafico/ciudad/ciudad.jsp?mensaje=cargado";
        HttpSession session = request.getSession();
        String cpais = request.getParameter("pais");
        //String lenguaje = (String) session.getAttribute("idioma");
         try{
               model.paisservice.buscarpais(cpais); 
               
               model.estadoservice.listarEstado(cpais); 
             }catch (SQLException m){
                    throw new ServletException(m.getMessage());
             }
         
        
        
        this.dispatchRequest(next);
    }
    
}
