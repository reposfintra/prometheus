/************************************************************************
 * Nombre clase: InformacionPlanilla_discrepanciaAction.java
 * Descripci�n: Accion para buscar una discrepancia dada una planilla.
 * Autor: Jose de la rosa
 * Fecha: 26 de septiembre de 2005, 09:19 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;

public class InformacionPlanilla_discrepanciaAction extends Action{
    
    /** Creates a new instance of InformacionPlanilla_discrepanciaAction */
    public InformacionPlanilla_discrepanciaAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String numpla = request.getParameter ("numpla").toUpperCase ();
        String tipo = request.getParameter ("tipo").toUpperCase ();
        String num = (request.getParameter ("c_num_discre")!=null)?request.getParameter ("c_num_discre"):"0";
        int num_discre = Integer.parseInt (num);
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String next = "";
        try{
            if( model.demorasSvc.existePlanilla (numpla) ){
                if(tipo.equals ("D")){
                    next = "/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numpla=" + numpla+"&sw=false&msg=";
                    model.planillaService.obtenerInformacionPlanilla2 (numpla);
                    model.planillaService.obtenerNombrePropietario2 (numpla);
                    model.planillaService.consultaPlanillaRemesa (numpla);
                }
                else if (tipo.equals ("G")){
                    next = "/jsp/cumplidos/discrepancia/DiscrepanciaInsertarGeneral.jsp?c_numpla=" + numpla;
                    model.discrepanciaService.listDiscrepanciaGeneral (numpla, distrito);
                    model.planillaService.obtenerInformacionPlanilla2 (numpla);
                    model.planillaService.obtenerNombrePropietario2 (numpla);
                }
                else if (tipo.equals ("S")){
                    next = "/jsp/cumplidos/discrepancia/DiscrepanciaInsertarSoporte.jsp?c_numpla=" + numpla;
                    model.discrepanciaService.listDiscrepanciaGeneral (numpla, distrito);
                    model.planillaService.obtenerInformacionPlanilla2 (numpla);
                    model.planillaService.obtenerNombrePropietario2 (numpla);
                    model.planillaService.consultaPlanillaRemesa(numpla);
                }
                else if (tipo.equals ("M")){
                    next = "/jsp/cumplidos/discrepancia/DiscrepanciaModificarGeneral.jsp?c_numpla=" + numpla;
                    model.discrepanciaService.searchDiscrepanciaGeneral (numpla, num_discre, distrito);
                    model.planillaService.obtenerInformacionPlanilla2 (numpla);
                    model.planillaService.obtenerNombrePropietario2 (numpla);
                }
                else if (tipo.equals ("I")){
                    next = "/jsp/cumplidos/discrepancia/asignarDiscrepanciaPlanilla.jsp?c_numpla=" + numpla;
                }
                else{
                    next = "/jsp/cumplidos/discrepancia/DiscrepanciaCierreGeneral.jsp?c_numpla=" + numpla+"&marco=no";
                    model.discrepanciaService.searchDiscrepanciaGeneral (numpla, num_discre, distrito);
                    model.planillaService.obtenerInformacionPlanilla2 (numpla);
                    model.planillaService.obtenerNombrePropietario2 (numpla);
                }
            }
            else
                next = "/jsp/cumplidos/discrepancia/asignarDiscrepanciaPlanilla.jsp?msg=N�mero de planilla no existe.";
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
