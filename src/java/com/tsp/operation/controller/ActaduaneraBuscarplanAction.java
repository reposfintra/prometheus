/***********************************************************************************
 * Nombre clase : ............... ActaduaneraBuscarplanAction.java                 *
 * Descripcion :.................                                                  * 
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 1 de diciembre de 2005, 10:21 AM                *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class ActaduaneraBuscarplanAction extends Action  {
    
    /** Creates a new instance of ActaduaneraBuscarplanAction */
    public ActaduaneraBuscarplanAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String numrem = request.getParameter("numrem");
        String next; 
        try{
            model.controlactAduaneraService.buscarPlanillaRemesa( numrem );
            List planillas = model.controlactAduaneraService.getPlanillasRem ();
            if( planillas.size()==1 ){
                Planilla pla= (Planilla) planillas.get(0);
                next = "/controller?estado=Infoact&accion=Buscarclin&numpla="+pla.getNumpla(); 
            }
            else{
                next="/jsp/trafico/actividad_Aduanera/planillas.jsp";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
