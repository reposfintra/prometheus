

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class CargueLGManagerAction extends Action{
    
    static Logger logger = Logger.getLogger(CargueLGManagerAction.class);
    /** Creates a new instance of PlanillaSearchAction */
    public CargueLGManagerAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/jsp/masivo/despacho/instrucciones_despacho/InicioCargue.jsp";
        String planilla= request.getParameter("planilla")!=null?request.getParameter("planilla").toUpperCase():"";
        
        model.remesaService.setRemesa(null);
        model.remesaService.setRm(null);
        model.planillaService.setPlanilla(null);
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String cmd  = request.getParameter("cmd")!=null? request.getParameter("cmd"):"";
        
        try{
            
            //BUSCAR LAS REMESAS DE UNA PLANILLA
            if(cmd.equals("Buscar")){
                if(!planilla.equals("")){
                    String opcion = request.getParameter("opcion");
                    if(opcion.equals("1")){
                        model.instrucciones_despachoService.ListarRemesas(planilla);
                    }
                    else if(opcion.equals("2")){
                        model.instrucciones_despachoService.buscarRemesa(planilla);
                    }
                    else if (opcion.equals("3")){
                        model.instrucciones_despachoService.buscarOcompra(planilla,usuario.getDstrct());
                        model.instrucciones_despachoService.ListarCausas();
                        next="/jsp/masivo/despacho/instrucciones_despacho/relacionarOcompraCargue.jsp";
                    }
                    
                    
                    Vector plas = model.instrucciones_despachoService.getRemesas();
                    logger.info("Cantidad encontrada... "+plas.size());
                    
                    boolean sw= true;
                    for(int i = 0; i<plas.size(); i++){
                        Remesa remesa= (Remesa) plas.elementAt(i);
                        if(remesa.getDocumento().equals("")){
                            sw= false;
                        }
                    }
                    
                    if(sw){
                        next = next +"?mostrar=ok";
                    }
                    if(plas.size()==0){
                        String documento =opcion.equals("1")?"Planilla":opcion.equals("2")?"Remesa":"Orden de compra";
                        
                        next="/jsp/masivo/despacho/instrucciones_despacho/InicioCargue.jsp?mensaje=No se encontro la "+documento+" "+planilla;
                    }
                    
                    
                }
            }
            else if(cmd.equals("Consultar")){
                next="/jsp/masivo/despacho/instrucciones_despacho/ConsultaCargue.jsp";
                String opcion = request.getParameter("opcion");
                if(opcion.equals("1")){
                    model.instrucciones_despachoService.ListarRemesas(planilla);
                }
                else{
                    model.instrucciones_despachoService.buscarRemesa(planilla);
                }
                Vector plas = model.instrucciones_despachoService.getRemesas();
                if(plas.size()==0){
                    String documento =opcion.equals("1")?"Planilla":"Remesa";
                    next="/jsp/masivo/despacho/instrucciones_despacho/ConsultaCargue.jsp?mensaje=No se encontro la "+documento+" "+planilla;
                }
            }
            else if(cmd.equals("Cantidades")){
                next="/jsp/masivo/despacho/instrucciones_despacho/segundaCargue.jsp?mostrar=ok";
                model.instrucciones_despachoService.ListarCausas();
                
                
                
            }
            else if(cmd.equals("Listar")){
                model.instrucciones_despachoService.ListarOrdenCompra(usuario.getDstrct());
                next="/jsp/masivo/despacho/instrucciones_despacho/ListaOrdenCompra.jsp";
                
            }
            else if(cmd.equals("Relacionar")){
                
                Vector remesas = model.instrucciones_despachoService.getRemesas();
                Vector nuevo = new Vector();
                for(int i = 0; i<remesas.size(); i++){
                    Remesa remesa= (Remesa) remesas.elementAt(i);
                    remesa.setDocumento(request.getParameter("ocompra"+remesa.getNumrem()));
                    remesa.setDocumento_rel(request.getParameter("factura"+remesa.getNumrem()));
                    remesa.setFecha_factura(request.getParameter("fecha"+remesa.getNumrem()).equals("")?"0099-01-01 00:00:00":request.getParameter("fecha"+remesa.getNumrem()));
                    remesa.setReferencias(model.instrucciones_despachoService.buscarReferencias(remesa));
                    nuevo.add(remesa);
                    
                }
                model.instrucciones_despachoService.setRemesas(nuevo);
                
                //VALIDACIONES
                boolean sw= true;
                String mensaje= "";
                //1. VALIDA QUE LAS ORDEN DE COMPRA ESCRITAS NINGUNA ESTE VACIA.
                java.util.Enumeration enum1;
                String parametro;
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    if(parametro.indexOf("ocompra")>=0){
                        if("".equals(request.getParameter("parametro"))){
                            sw= false;
                            break;
                        }
                        
                    }
                }
                if(sw==false){
                    next="/jsp/masivo/despacho/instrucciones_despacho/InicioCargue.jsp?mensaje=Una o mas de las ordenes de cargue esta vacia.";
                }
                
                //2. VALIDA QUE LAS ORDEN DE COMPRA ESCRITAS EXISTAN EN LA TABLA DE INSTRUCCIONES DESPACHO.
                if(sw==true){
                    enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                    String ocompra="";
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("ocompra")>=0){
                            if(!model.instrucciones_despachoService.existeOrdenCompra(usuario.getDstrct(),request.getParameter(parametro))){
                                sw=false;
                                ocompra = ocompra + request.getParameter(parametro)+"<br>";
                            }
                            
                        }
                    }
                    if(sw==false){
                        next="/jsp/masivo/despacho/instrucciones_despacho/InicioCargue.jsp?mensaje=Las ordenes de compra: "+ocompra+" no exisrten en la tabla " +
                        " de instrucciones de despacho.";
                    }
                }
                
                
                //3. VALIDA QUE LAS ORDEN DE COMPRA ESCRITAS NO SEAN IGUALES EN LAS REMESAS.
                if(sw==true){
                    String ocompra="";
                    Hashtable h = new Hashtable();
                    enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("ocompra")>=0){
                            if(h.get(request.getParameter(parametro))!=null){
                                sw=false;
                                ocompra = ocompra + request.getParameter(parametro)+"<br>";
                            }
                            h.put(request.getParameter(parametro),"");
                        }
                    }
                    if(sw==false){
                        next="/jsp/masivo/despacho/instrucciones_despacho/InicioCargue.jsp?mensaje=Las ordenes de compra: "+ocompra+" estan repetidas.";
                    }
                }
                
                //SI CUMPLE CON TODAS LAS CONDICIONES SE RELACIONA LA ORDEN DE CARGUE
                if(sw==true){
                    Vector consultas = new Vector();
                    
                    remesas = model.instrucciones_despachoService.getRemesas();
                    for(int i = 0; i<remesas.size(); i++){
                        Remesa remesa= (Remesa) remesas.elementAt(i);
                        //PRIMERO SE ACTUALIZA LA TABLA instrucciones_despacho CON EL NUMERO DE LA REMESA, FACTURA Y FECHA DE LA FACTURA.
                        consultas.add(model.instrucciones_despachoService.actualizarOCompra(remesa));
                        consultas.add(model.instrucciones_despachoService.agregarDocumento(remesa));
                        if(!remesa.getDocumento().equals(request.getParameter("Vieja"+remesa.getNumrem()))){
                            remesa.setDocuinterno(request.getParameter("Vieja"+remesa.getNumrem()));
                            ////consultas.add(model.instrucciones_despachoService.borrarDocumento(remesa));
                            //consultas.add(model.instrucciones_despachoService.desmarcarRemesa(remesa));
                        }
                    }
                    //  model.despachoService.insertar(consultas);
                    model.instrucciones_despachoService.ListarCausas();
                    next="/jsp/masivo/despacho/instrucciones_despacho/segundaCargue.jsp?mensaje=Ingrese las cantidades de cargue.&mostrar=ok";
                }
            }
            else if(cmd.equals("CantidadCargue")){
                
                Vector consultas = new Vector();
                Vector remesas = model.instrucciones_despachoService.getRemesas();
                Vector remesasNew = new Vector();
                for(int i = 0; i<remesas.size(); i++){
                    
                    Remesa remesa= (Remesa) remesas.elementAt(i);
                    Vector modelos = remesa.getReferencias();
                    Vector modeloNew = new Vector();
                    for(int j = 0; j<modelos.size(); j++){
                        
                        Instrucciones_Despacho inst = (Instrucciones_Despacho) modelos.elementAt(j);
                        inst.setCant_despachada(Integer.parseInt(request.getParameter("cantRef"+remesa.getNumrem()+inst.getModelo())!=null?request.getParameter("cantRef"+remesa.getNumrem()+inst.getModelo()):"0"));
                        inst.setCausa(request.getParameter("causa"+remesa.getNumrem()+inst.getModelo())!=null?request.getParameter("causa"+remesa.getNumrem()+inst.getModelo()):"");
                        inst.setDstrct(remesa.getDistrito());
                        consultas.add(model.instrucciones_despachoService.actualizarCantCargue(inst));
                        consultas.add(model.instrucciones_despachoService.desmarcarRemesa(remesa));
                        modeloNew.add(inst);
                    }
                    consultas.add(model.instrucciones_despachoService.actualizarOCompra(remesa));
                    consultas.add(model.instrucciones_despachoService.borrarDocumento(remesa));
                    consultas.add(model.instrucciones_despachoService.agregarDocumento(remesa));
                    remesa.setReferencias(modeloNew);
                    remesasNew.add(remesa);
                }
                System.out.println("CONSULTAS...");
                
                for (int i=0; i<consultas.size(); i++){
                    System.out.println(consultas.elementAt(i));
                }
                model.despachoService.insertar(consultas);
                model.instrucciones_despachoService.setRemesas(remesasNew);
                next="/jsp/masivo/despacho/instrucciones_despacho/segundaCargue.jsp?mostrar=ok&mensaje=Datos modificados con exito!";
                
            }
            if(cmd.equals("BuscarFCargue")){
                if(!planilla.equals("")){
                    
                    model.instrucciones_despachoService.ListarRemesas(planilla);
                    
                    Vector plas = model.instrucciones_despachoService.getRemesas();
                    logger.info("Cantidad encontrada... "+plas.size());
                    
                    boolean sw= true;
                    for(int i = 0; i<plas.size(); i++){
                        Remesa remesa= (Remesa) plas.elementAt(i);
                        if(remesa.getDocumento().equals("")){
                            sw= false;
                        }
                    }
                    
                    if(!sw){
                        model.instrucciones_despachoService.setRemesas(null);
                        next="/jsp/masivo/despacho/instrucciones_despacho/FechaRealCargue.jsp?mensaje=Las remesas relacionadas a las planillas" +
                        " no tienen relacionada una orden de compra.";
                    }
                    else{
                        next="/jsp/masivo/despacho/instrucciones_despacho/FechaRealCargue.jsp";
                    }
                    if(plas.size()==0){
                        next="/jsp/masivo/despacho/instrucciones_despacho/FechaRealCargue.jsp?mensaje=No se encontro la planilla "+planilla;
                    }
                    
                    
                    
                }
            }
            if(cmd.equals("IngresarFCargue")){
                Vector consultas = new Vector();
                Vector remesas = model.instrucciones_despachoService.getRemesas();
                Vector nuevo= new Vector();
                for(int i = 0; i<remesas.size(); i++){
                    Remesa remesa= (Remesa) remesas.elementAt(i);
                    remesa.setFecha_realcargue(request.getParameter("fecha"+remesa.getDestinatario()+remesa.getNumrem()));
                    consultas.add(model.instrucciones_despachoService.actualizarFechaCargue(remesa));
                    nuevo.add(remesa);
                }
                model.instrucciones_despachoService.setRemesas(nuevo);
                model.despachoService.insertar(consultas);
                next="/jsp/masivo/despacho/instrucciones_despacho/FechaRealCargue.jsp?mensaje=Datos actualizados con exito!";
            }
            if(cmd.equals("Reporte")){
                
                Date fecha = new Date();
                java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd");
                
                com.tsp.operation.model.threads.HReporteInformacionCargue hilo = new com.tsp.operation.model.threads.HReporteInformacionCargue();
                String fechai = request.getParameter("fecini").equals("")?"2000-01-01 ":request.getParameter("fecini");
                String fechaF = request.getParameter("fecfin").equals("")?s.format(fecha):request.getParameter("fecfin");
                String user = usuario.getLogin();
                String ocompra = request.getParameter("ocompra");
                String factura = request.getParameter("factura");
                String destinatario = request.getParameter("destinatario");
                String origen = request.getParameter("origen")!=null?request.getParameter("origen"):"";
                String destino = request.getParameter("destino")!=null?request.getParameter("destino"):"";
                //String fec1,String fec2,String user,String ocompra, String factura, String destinatario, String origen, String destino
                model.instrucciones_despachoService.reporteTrakin(ocompra, factura, destinatario, origen, destino, fechai, fechaF);
                
                hilo.start( fechai+" 00:00:00",fechaF+" 23:59:59",user,ocompra,factura,destinatario,origen,destino);
                next="/jsp/masivo/despacho/instrucciones_despacho/ResultadoTraking.jsp?mensaje=Se ha iniciado la creacion del archivo ReporteTrakinLG, " +
                "puede verificar el estado en Procesos/Seguimiento de Procesos, una vez terminado puede consultar los archivos creados durante el proceso en el Directorio de Archivos";
            }
            if(cmd.equals("destinatarios")){
                model.remidestService.searchListaD("584", request.getParameter("ciudad"),request.getParameter("nombre")!=null?request.getParameter("nombre"):"");
                next="/jsp/masivo/despacho/instrucciones_despacho/destinatarios.jsp";
            }
            if(cmd.equals("ListarNovedad")){
                String condicion = "";
                if(request.getParameter("solucion")!=null){
                    condicion = " AND id.cod_sol!=''";
                }
                if(request.getParameter("nosolucion")!=null){
                    condicion = " AND id.cod_sol=''";
                }
                if(request.getParameter("nosolucion")!=null && request.getParameter("solucion")!=null){
                    condicion = "";
                }
                String fecha = request.getParameter("ano")+request.getParameter("mes");
                
                model.instrucciones_despachoService.reporteNovedad(condicion,fecha,request.getParameter("mes"));
                model.instrucciones_despachoService.ListarSolucion();
                next="/jsp/masivo/despacho/instrucciones_despacho/IngresoNovedades.jsp";
                if(model.instrucciones_despachoService.getLista().size()==0){
                    next="/jsp/masivo/despacho/instrucciones_despacho/IngresoNovedades.jsp?mensaje=No se encontraron datos para el filtro seleccionado";
                }
            }
            if(cmd.equals("GuardarNovedad")){
                Vector consultas = new Vector();
                Vector remesas = model.instrucciones_despachoService.getLista();
                Vector nuevo= new Vector();
                Vector remesasMail= new Vector();
                Date f = new Date();
                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                for(int i = 0; i<remesas.size(); i++){
                    Instrucciones_Despacho remesa= (Instrucciones_Despacho) remesas.elementAt(i);
                    if(request.getParameter("causa"+remesa.getOrder_no()+remesa.getModelo())!=null){
                        
                        if(!request.getParameter("causa"+remesa.getOrder_no()+remesa.getModelo()).equals("")){
                            
                            remesa.setSolucion_novedad(request.getParameter("causa"+remesa.getOrder_no()+remesa.getModelo()));
                            remesa.setUsuario(usuario.getLogin());
                            remesa.setFecha_solucion(s.format(f));
                            remesasMail.add(remesa);
                        }
                    }
                    consultas.add(model.instrucciones_despachoService.actualizarNovedad(remesa));
                    nuevo.add(remesa);
                }
                model.instrucciones_despachoService.setLista(nuevo);
                model.despachoService.insertar(consultas);
                next="/jsp/masivo/despacho/instrucciones_despacho/IngresoNovedades.jsp?mensaje=Datos actualizados con exito!";
                
                //ENVIO DE EMAIL
                String texto="SOLUCION A NOVEDADES LG.";
                for(int i=0; i<remesasMail.size();i++){
                    Instrucciones_Despacho remesa= (Instrucciones_Despacho) remesas.elementAt(i);
                    String solnovedad = "";
                    model.tablaGenService.obtenerRegistro("SOLNOVEDAD",remesa.getSolucion_novedad(), "");
                    if(model.tablaGenService.getTblgen()!=null){
                        solnovedad = model.tablaGenService.getTblgen().getDescripcion();
                    }
                    texto = texto +"\n\nRemesa: "+remesa.getNumrem()+"\nOrden de compra: "+remesa.getOrder_no()+"" +
                    "\nProducto: " +remesa.getModelo()+
                    "\nCodigo novedad: "+remesa.getCodigo_novedad()+"\nMotivo Novedad: "+remesa.getMotivo_novedad()+
                    "\nFecha novedad: "+remesa.getFecha_novedad()+"\nSolucion novedad: "+solnovedad+"" +
                    "\nFecha solucion novedad: "+remesa.getFecha_solucion()+"\nUsuario ingreso novedad: "+remesa.getUsuario();
                    
                }
                
                //ENVIAMOS EMAIL DE LA CONTABILIZACION
                String mailInt="";
                String mailExt="";
                boolean swInt=false;
                boolean swExt=false;
                LinkedList lista = model.tablaGenService.obtenerInfoTablaGen("EMAIL", "NOVEDAD");
                if(lista!=null){
                    Iterator it = lista.iterator();
                    while(it.hasNext()){
                        TablaGen t = (TablaGen)it.next();
                        System.out.println("-"+t.getDescripcion());
                        
                        if(t.getReferencia().equals("I")){
                            //verifico si exsite una arroba en la descripcion
                            if( t.getDescripcion().indexOf("@") != -1){
                                mailInt += t.getDescripcion()+";";
                            }
                            //se obtiene el login del usuario y se busca el email
                            else{
                                model.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                                Usuario u = model.usuarioService.getUsuario();
                                if(!u.getEmail().equals(""))
                                    mailInt += u.getEmail()+";";
                            }
                            swInt = true;
                        }
                        //si el email del cliente es externo
                        else{
                            //verifico si exsite una arroba en la descripcion
                            if( t.getDescripcion().indexOf("@") != -1){
                                mailExt += t.getDescripcion()+";";
                            }
                            else{
                                //se obtiene el login del usuario y se busca el email
                                model.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                                Usuario u = model.usuarioService.getUsuario();
                                if(!u.getEmail().equals(""))
                                    mailExt += u.getEmail()+";";
                            }
                            swExt = true;
                        }
                    }
                    
                    System.out.println("interno "+mailInt);
                    System.out.println("externo "+mailExt);
                    
                    com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                    
                    email.setSendername( "SOLUCION NOVEDADES LG" );
                    email.setRecstatus("A");
                    email.setEmailcode("");
                    email.setEmailcopyto("");
                    email.setEmailsubject("SOLUCION NOVEDADES LG");
                    email.setEmailbody(texto);
                    
                    if(swInt==true){
                        mailInt = mailInt.substring(0, mailInt.length() - 1 );
                        email.setEmailfrom("procesos@mail.tsp.com");
                        email.setEmailto(mailInt);
                        email.setTipo("I");
                        model.sendMailService.sendMail(email);
                    }
                    if(swExt==true){
                        mailExt = mailExt.substring(0, mailExt.length() - 1 );
                        email.setEmailfrom("procesos@sachezpolo.com");
                        email.setEmailto(mailExt);
                        email.setTipo("E");
                        model.sendMailService.sendMail(email);
                    }
                    
                    
                }
            }
            if(cmd.equals("ReporteNovedad")){
                com.tsp.operation.model.threads.HReporteNovedades hilo = new com.tsp.operation.model.threads.HReporteNovedades();
                String fecha =  request.getParameter("ano")+request.getParameter("mes");
                hilo.start(model.instrucciones_despachoService.getLista(),usuario.getLogin(),request.getParameter("mes"));
                next="/jsp/masivo/despacho/instrucciones_despacho/IngresoNovedades.jsp?mensaje=Se ha iniciado la creacion del archivo ReporteNovedadesLg, " +
                "puede verificar el estado en Procesos/Seguimiento de Procesos, una vez terminado puede consultar los archivos creados durante el proceso en el Directorio de Archivos";
                
            }
            if(cmd.equals("ReporteFactura")){
                String opcion= request.getParameter("opcion")!=null?request.getParameter("opcion"):"";
                if(opcion.equals("1")){//POR NUMERO DE FACTURA
                    model.instrucciones_despachoService.buscarFactura(usuario.getDstrct(), "FAC", request.getParameter("fac_tsp"));
                }
                if(opcion.equals("2")){// POR ORDEN DE COMPRA
                    String condicion = " WHERE id.order_no= '"+request.getParameter("ocompra")+"'";
                    String factura = model.instrucciones_despachoService.BuscarNumeroFactura(condicion);
                    model.instrucciones_despachoService.buscarFactura(usuario.getDstrct(), "FAC", factura);
                    
                }
                if(opcion.equals("3")){// POR ORDEN DE COMPRA
                    String condicion = " WHERE id.factura= '"+request.getParameter("fac_lg")+"'";
                    String factura = model.instrucciones_despachoService.BuscarNumeroFactura(condicion);
                    model.instrucciones_despachoService.buscarFactura(usuario.getDstrct(), "FAC", factura);
                    
                }
                if(model.instrucciones_despachoService.getLista()!=null){
                    if(model.instrucciones_despachoService.getLista().size()>0){
                        next="/jsp/masivo/despacho/instrucciones_despacho/FacturaDetalle.jsp";
                    }
                    else{
                        next="/jsp/masivo/despacho/instrucciones_despacho/ConsultaFactura.jsp?mensaje=No se encontraron datos para los filtros realizados.";
                    }
                }
                
                if(opcion.equals("0")){//POR NUMERO DE FACTURA
                    model.instrucciones_despachoService.buscarListaFactura(request.getParameter("fecini"),request.getParameter("fecfin"));
                    
                    if(model.instrucciones_despachoService.getRemesas().size()>0){
                        next="/jsp/masivo/despacho/instrucciones_despacho/ConsultaFactura.jsp";
                    }
                    else{
                        next="/jsp/masivo/despacho/instrucciones_despacho/ConsultaFactura.jsp?mensaje=No se encontraron datos para los filtros realizados.";
                    }
                }
                
                
            }
            if(cmd.equals("FacturaExcel")){
                com.tsp.operation.model.threads.HReporteFacturacionLG hilo = new com.tsp.operation.model.threads.HReporteFacturacionLG();
                hilo.start(model.instrucciones_despachoService.getLista() ,usuario.getLogin());
                next="/jsp/masivo/despacho/instrucciones_despacho/FacturaDetalle.jsp?mensaje=Se ha iniciado la creacion del archivo ReporteFacturacionLg, " +
                "puede verificar el estado en Procesos/Seguimiento de Procesos, una vez terminado puede consultar los archivos creados durante el proceso en el Directorio de Archivos";;
            }
            if(cmd.equals("Inventario")){
                String ocompra= request.getParameter("ocompra");
                String factura= request.getParameter("factura");
                
                model.instrucciones_despachoService.inventario(ocompra, factura, usuario.getDstrct());
                if(model.instrucciones_despachoService.getLista().size()>0){
                    next="/jsp/masivo/despacho/instrucciones_despacho/ReporteInventario.jsp";
                }
                else{
                    next="/jsp/masivo/despacho/instrucciones_despacho/ConsultaInventario.jsp?mensaje=No se encontraron datos para el filtro realizado";
                }
            }
            if(cmd.equals("InventarioDetalle")){
                String ocompra= request.getParameter("ocompra");
                String factura= request.getParameter("factura");
                String condicion = "";
                if(!ocompra.equals("")){
                    condicion = "and im.ocompra = '"+ocompra+"'";
                    
                }
                if(!factura.equals("")){
                    condicion = "and im.nro_fact_comercial = '"+factura+"'";
                    
                }
                String ubicacion = request.getParameter("ubicacion");
                String material = request.getParameter("material");
                
                model.instrucciones_despachoService.detalleInventario(condicion,usuario.getDstrct(), ubicacion, material);
                if(model.instrucciones_despachoService.getLista().size()>0){
                    next="/jsp/masivo/despacho/instrucciones_despacho/ReporteInventarioDetalle.jsp";
                }
                else{
                    next="/jsp/masivo/despacho/instrucciones_despacho/ConsultaInventario.jsp?mensaje=No se encontraron datos para el filtro realizado";
                }
            }
            else if(cmd.equals("CantidadCargueOcompra")){
                
                Vector consultas = new Vector();
                Vector remesas = model.instrucciones_despachoService.getRemesas();
                Vector remesasNew = new Vector();
                for(int i = 0; i<remesas.size(); i++){
                    
                    Remesa remesa= (Remesa) remesas.elementAt(i);
                    Vector modelos = remesa.getReferencias();
                    Vector modeloNew = new Vector();
                    for(int j = 0; j<modelos.size(); j++){
                        
                        Instrucciones_Despacho inst = (Instrucciones_Despacho) modelos.elementAt(j);
                        inst.setCant_despachada(Integer.parseInt(request.getParameter("cantRef"+remesa.getNumrem()+inst.getModelo())!=null?request.getParameter("cantRef"+remesa.getNumrem()+inst.getModelo()):"0"));
                        inst.setCausa(request.getParameter("causa"+remesa.getNumrem()+inst.getModelo())!=null?request.getParameter("causa"+remesa.getNumrem()+inst.getModelo()):"");
                        inst.setDstrct(remesa.getDistrito());
                        consultas.add(model.instrucciones_despachoService.actualizarCantCargue(inst));
                        modeloNew.add(inst);
                    }
                    remesa.setReferencias(modeloNew);
                    remesasNew.add(remesa);
                }
                System.out.println("CONSULTAS...");
                
                for (int i=0; i<consultas.size(); i++){
                    System.out.println(consultas.elementAt(i));
                }
                model.despachoService.insertar(consultas);
                model.instrucciones_despachoService.setRemesas(remesasNew);
                next="/jsp/masivo/despacho/instrucciones_despacho/relacionarOcompraCargue.jsp?mensaje=Datos modificados con exito!";
                
            }
            logger.info("NEXT: "+next);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
}
