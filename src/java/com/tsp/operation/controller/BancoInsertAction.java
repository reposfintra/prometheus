/*******************************************************************
 * Nombre clase: BancoInsertAction.java
 * Descripci�n: Accion para ingresar un banco a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 28 de septiembre de 2005, 11:17 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class BancoInsertAction extends Action {
    
    /** Creates a new instance of BancoInsertAction */
    public BancoInsertAction () {
    }
    public void run () throws ServletException, InformationException {
        String next = "/jsp/cxpagar/banco/BancoInsertar.jsp";
        HttpSession session = request.getSession ();
        String distrito = request.getParameter ("c_distrito").toUpperCase ();
        String banco = (request.getParameter ("c_banco").toUpperCase ());
        String sucursal = (request.getParameter ("c_sucursal").toUpperCase ());
        String agencia = (request.getParameter ("c_agencia").toUpperCase ());
        String cuenta = (request.getParameter ("c_cuenta").toUpperCase ());
        String moneda = (request.getParameter ("c_moneda").toUpperCase ());
        String codigo_cuenta = (request.getParameter ("c_codigo_cuenta").toUpperCase ());
        String posbancaria = (request.getParameter ("c_posbancaria").toUpperCase ());
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        try {
            Banco b = new Banco ();
            b.setBanco (banco);
            b.setDstrct (distrito);
            b.setCuenta (cuenta);
            b.setBank_account_no (sucursal);
            b.setCodigo_Agencia (agencia);
            b.setMoneda (moneda);
            b.setCodigo_cuenta (codigo_cuenta);
            b.setUsuario_modificacion (usuario.getLogin ());
            b.setUsuario_creacion (usuario.getLogin ());
            b.setBase (usuario.getBase ());
            b.setPosbancaria(posbancaria);
            model.servicioBanco.setBannco (b);
            try {
                if(model.servicioBanco.existeCuenta(codigo_cuenta)){
                    if (!model.servicioBanco.bancoExiste (distrito, banco, sucursal)) {
                        model.servicioBanco.insertarBanco (b);
                        next += "?mensaje=Banco Agregado"; 
                    }else{
                        String entrar = model.servicioBanco.bancoAnulado(distrito, banco, sucursal);
                        if(entrar.equals("A")){
                            model.servicioBanco.bancoAnuladoCambiar(distrito, banco, sucursal);
                            model.servicioBanco.updateBanco(b);
                            next += "?mensaje=Banco Agregado";
                        }else{
                            next += "?mensaje=Error Ya Existe El Banco";
                        }     
                    }
                }else{
                    next += "?mensaje=Error, la cuenta contable ingresada no existe.";
                }
                
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }catch (Exception e) {
            e.printStackTrace();
            throw new ServletException (e.getMessage ());
            
        }
        this.dispatchRequest (next);
        
    }
}
