/********************************************************************
 *      Nombre Clase.................   ReporteUtilidadAction.java
 *      Descripci�n..................   Accion para la generaci�n del archivo de utilidad
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   15.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.controller;



import java.io.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;



public class ReporteUtilidadAction extends Action{
    
    
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        try{
            HttpSession session = request.getSession();
            Usuario     user    = (Usuario) session.getAttribute("Usuario");
            String      cia     = (String) session.getAttribute("Distrito");
            
            String next   = "";
            String ano    = defaultString("ano","");
            String mes    = defaultString("mes","");
            String tipo   = defaultString("viaje","");
            String opcion = defaultString("opcion","indefinido");
            
            
            if (opcion.equals("indefinido")) {
                
               next = "/jsp/finanzas/reportes/ReporteUtilidad.jsp?msg=Se ha iniciado el proceso de la generacion" +
                      " del Reporte de Utilidad en el per�odo: " + UtilFinanzas.NombreMes(Integer.parseInt(mes)) + " " + ano + " " +
                      "para el Proyecto " + ( tipo.compareTo("1")==0 ? "Carga General" : "Carb�n" ) + ".";
                ReporteUtilidadTh hilo = new ReporteUtilidadTh();
                hilo.start(model, ano, mes, user, cia, tipo);
                
            } else if (opcion.equalsIgnoreCase("Generacion")){
                
                next = "/jsp/masivo/utilidad/reporteUtilidad.jsp";
                HReporteUtilidadPlanillas h = new HReporteUtilidadPlanillas();
                h.start(model, user, ano , mes );                
                request.setAttribute("msg","Generacion de reporte inicializado. para el periodo " +ano+mes);
                
            }
                
            
            
            // Redireccionar a la p�gina indicada.
            this.dispatchRequest(next);
        } catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
    }
    
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }
        
    
}
