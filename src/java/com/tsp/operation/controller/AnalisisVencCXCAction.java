/**********************************************************************
 *      Nombre Clase.................   AnalisisVencCXCAction.java
 *      Descripci�n..................   Analisis de Vencimiento de Facturas
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   17.11.2006
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *********************************************************************/

package com.tsp.operation.controller;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.operation.model.threads.*;

/**
 *
 * @author  Andr�s
 */
public class AnalisisVencCXCAction extends Action{
    Vector detall;
    Vector resum;
    Vector general;
    Logger logger;

    HttpSession session;

    String tipoRespuesta = "";// JPACOSTA
    /** Creates a new instance of InformacionPlanillaAction */
    public AnalisisVencCXCAction() {
        logger = Logger.getLogger(this.getClass());
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {

        session = request.getSession();
        //Info del usuario
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        //Pr�xima vista
        String next = "";
        String opcion = request.getParameter("opcion")!=null? request.getParameter("opcion") :"";
        boolean redirect = false;
        String dato="";
        tipoRespuesta = "text/plain";
        try{
           if(opcion.equals("") == false){
               if (opcion.equals("unidades")||opcion.equals("convenios")) {
                   String filtro = request.getParameter("filtro")!=null
                                 ? request.getParameter("filtro") : "";
                   dato = model.facturaService.cargarListas(opcion, filtro);
                   tipoRespuesta = "application/json";
               } else {
                dato = "<option value=''>      Todos</option>";
                String buscar = request.getParameter("dato")!=null? request.getParameter("dato") : "";
                String filtro = request.getParameter("filtro")!=null? request.getParameter("filtro") : "cliente";
                buscar = buscar.toUpperCase();
                if(buscar.equals("")==false) buscar = buscar.replaceAll(" ", "%");
                if(filtro.equals("cliente")){
                    Vector clientes = null;
                    boolean pasa = true;
                    try {
                        model.clienteService.consultarCliente("%"+buscar+"%");
                        clientes = model.clienteService.getClientesVec();
                    }
                    catch (Exception e) {
                        pasa = false;
                        System.out.println("Error: "+e.toString());
                        e.printStackTrace();
                    }
                    if(pasa==true && clientes.size()>0){
                        Cliente cl = null;
                        for (int i = 0; i < clientes.size(); i++) {
                            cl = (Cliente)clientes.get(i);
                            dato += "<option value='"+cl.getCodcli()+"'>"+cl.getNomcli()+"</option>";
                        }
                    }
                }
                else{
                    boolean pasa = true;
                    Vector provs = null;
                    try {
                        provs = model.proveedorService.searchProveedores("", buscar);
                    }
                    catch (Exception e) {
                        pasa = false;
                        System.out.println("Error: "+e.toString());
                        e.printStackTrace();
                    }
                    if(pasa==true && provs.size()>0){
                        Proveedor cl = null;
                        for (int i = 0; i < provs.size(); i++) {
                            cl = (Proveedor)provs.get(i);
                            dato += "<option value='"+cl.getC_nit()+"'>"+cl.getNombre()+"_"+cl.getC_nit()+"</option>";
                        }
                    }
                }
           }
           }
           else{
                redirect = true;
                detall = new Vector();
                resum = new Vector();
                String codcli = request.getParameter("clientes")==null ? "" : request.getParameter("clientes");
                String prov = request.getParameter("proveedor")==null ? "" : request.getParameter("proveedor");
                String cccli = request.getParameter("cccli")==null ? "" : request.getParameter("cccli");
                String agc = request.getParameter("agencia")==null ? "" : request.getParameter("agencia");
                String agcd = request.getParameter("agencia_d")==null ? "" : request.getParameter("agencia_d");
                String agcobro = request.getParameter("agencia_c")==null ? "" : request.getParameter("agencia_c");
                String fecha = request.getParameter("fecha");
                //String degen = request.getParameter("general");//20101028
                String vto = request.getParameter("vence");
                String cofac = request.getParameter("tfac");
                String hc = request.getParameter("hc");
                String lim1 = request.getParameter("lim1");
                String lim2 = request.getParameter("lim2");

                String exp1 = request.getParameter("exp1");
                String exp2 = request.getParameter("exp2");
                //JPACOSTA, 2014-04
                String convenio = request.getParameterValues("convenios")!=null
                            ? Arrays.toString(request.getParameterValues("convenios")).replace("[", "").replace("]","")
                            : ""; //convenio.

                String codigocliente = request.getParameter("codigocliente")==null ? "" : request.getParameter("codigocliente");//20100630
                //String codigocliente=request.getParameter("codigocliente");//20100629//20100630

                logger.info("......... CLIENTE: " + codcli);
                logger.info("......... CC Cli:  "+  cccli);
                logger.info("......... codigocliente:  "+  codigocliente);//20100630
                logger.info("......... AGENCIA FACTUR.: " + agc);
                logger.info("......... AGENCIA DUE�A: " + agcd);
                logger.info("......... AGENCIA COBRO: " + agcobro);
                logger.info("......... FECHA: " + fecha);
                logger.info("......... EXPORTAR1: " + exp1);
                logger.info("......... EXPORTAR2: " + exp2);
                //model.facturaService.facturasNoPagadas(codcli, agc, fecha, (String) session.getAttribute("Distrito"), agcd, agcobro,prov,cccli,vto,cofac,hc,lim1,lim2);
                model.facturaService.facturasNoPagadas(codcli, agc, fecha, (String) session.getAttribute("Distrito"), agcd, agcobro,prov,cccli,vto,cofac,hc,lim1,lim2,codigocliente,convenio);//20100629
                Vector info = model.facturaService.getVDocumentos();

                this.organizarInfo2(info, fecha, agcobro);

                if( request.getParameter("exp")!= null ){
                    //ReporteCarteraTh hilo = new ReporteCarteraTh();
                    //hilo.start(model, this.general, usuario.getLogin(), fecha);
                    ListadoCarteraTh hilo = new ListadoCarteraTh();
                    hilo.start(model, this.general, usuario, fecha);
                    next = "/jsp/cxcobrar/reportes/analisisCarteraMsg.jsp";
                } else {
                    logger.info("? detalles: " + this.general.size());
                    next = "/jsp/cxcobrar/reportes/analisisCarteraDet.jsp";
                    request.setAttribute("det", this.general);
                }

                /*
                if( info.size()>0 )
                    this.organizarInfo(info, fecha, agc);

                logger.info("CLIENTES: " + this.resum.size());

                if( this.resum.size()<= 1 ){
                    next = "/jsp/cxcobrar/reportes/analisisCarteraRpta.jsp";
                    request.setAttribute("resum", this.resum);
                } else {
                    next = "/jsp/cxcobrar/reportes/analisisCarteraGralRpta.jsp";
                    request.setAttribute("gral", this.general);
                }

                if( exp1!=null ){
                    ReporteCarteraGralTh hilo = new ReporteCarteraGralTh();
                    hilo.start(model, this.general, usuario.getLogin(), fecha);
                    next = "/jsp/cxcobrar/reportes/analisisCarteraMsg.jsp";
                }

                if( exp2!=null ){
                    ReporteCarteraTh hilo = new ReporteCarteraTh();
                    hilo.start(model, this.resum, usuario.getLogin(), fecha);
                    next = "/jsp/cxcobrar/reportes/analisisCarteraMsg.jsp";
                }*/

                request.setAttribute("fecha", fecha);
                request.setAttribute("afcfact", agc);
                request.setAttribute("codcli", codcli);
                request.setAttribute("afcdu", agcd);
                request.setAttribute("afcob", agcobro);
                request.setAttribute("prov", prov);
                request.setAttribute("cccli", cccli);
                request.setAttribute("codigocliente", codigocliente);//20100630
                request.setAttribute("vencem", vto);
                request.setAttribute("cfact", cofac);
                request.setAttribute("hc", hc);
                request.setAttribute("lim1",lim1 );
                request.setAttribute("lim2",lim2 );
                //logger.info("RESUM: " + this.resum);
           }
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        if(redirect==true){
            this.dispatchRequest(next);
        }
        else{
            try {this.escribirResponse(dato);}
            catch (Exception e) {System.out.println("error: "+e.toString()); e.printStackTrace();}
        }
    }

    public void organizarInfo2(Vector info, String fecha, String agc) throws Exception{
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        this.general = new Vector();

        for( int i=0; i< info.size(); i++){
            RepGral obj = null;
            obj = new RepGral();
            obj = (RepGral) info.elementAt(i);

            /*if( agc.equals("VA") ){
                obj.setVlr_saldo(obj.getVlr_saldo_me());
                if( obj.getMoneda().equals("BOL") ){
                    obj.setVlr_saldo(obj.getVlr_saldo_me());
                    obj.setMoneda("BOL");
                } else {
                    Tasa tasa = model.tasaService.buscarValorTasa( (String) session.getAttribute("Moneda"), obj.getMoneda(),
                    "BOL", s.format(d));
                    if( tasa!=null ){
                        logger.info("? tasa de conversi�n... " + tasa.getValor_tasa());
                        obj.setVlr_saldo(obj.getVlr_saldo_me()*tasa.getValor_tasa());
                        obj.setMoneda("BOL");
                    }
                }
            }*/

            int dias = obj.getNdias();

            obj.setVencida_6(0);
            obj.setVencida_5(0);
            obj.setVencida_4(0);
            obj.setVencida_3(0);
            obj.setVencida_2(0);
            obj.setVencida_1(0);
            obj.setNo_vencida(0);

            if( dias>90 ){
                obj.setVencimiento(">90");
            } else if ( dias > 60 ){
                obj.setVencimiento("90");
            } else if ( dias > 30 ){
                obj.setVencimiento("60");
            } else if ( dias > 14 ){
                obj.setVencimiento("30");
            } else if ( dias > 7 ){
                obj.setVencimiento("14");
            } else if ( dias > 0 ){
                obj.setVencimiento("7");
            } else if ( dias > -8 ){
                obj.setVencimiento("-7");
            } else if ( dias > -15 ){
                obj.setVencimiento("-14");
            } else if ( dias > -22 ){
                obj.setVencimiento("-21");
            } else {
                obj.setVencimiento("-28");
            }

            this.general.add(obj);
        }
    }

    public void organizarInfo(Vector info, String fecha, String agc) throws Exception{

        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        this.detall = new Vector();
        this.resum = new Vector();
        this.general = new Vector();

        RepGral obj0 = null;
        RepGral ttl = new RepGral();
        RepGral gen = new RepGral();

        double col0 = 0;
        double col1 = 0;
        double col2 = 0;
        double col3 = 0;
        double col4 = 0;
        double col5 = 0;
        double col6 = 0;

        RepGral obj = null;
        for( int i=0; i< info.size(); i++){

            obj = new RepGral();
            obj = (RepGral) info.elementAt(i);

            //logger.info("? AGC: " + agc + " == VA : " + (agc.equals("VA")));

            if( agc.equals("VA") ){
                if( obj.getMoneda().equals("BOL") ){
                    obj.setVlr_saldo(obj.getVlr_saldo_me());
                    obj.setMoneda("BOL");
                } else {
                    Tasa tasa = model.tasaService.buscarValorTasa( (String) session.getAttribute("Moneda"), obj.getMoneda(),
                            "BOL", s.format(d));
                    if( tasa!=null ){
                        logger.info("? tasa de conversi�n... " + tasa.getValor_tasa());
                        obj.setVlr_saldo(obj.getVlr_saldo_me()*tasa.getValor_tasa());
                        obj.setMoneda("BOL");
                    }
                }

            }

            //logger.info("?Fac: " + obj.getFactura() + ". Valor: " + obj.getVlr_saldo() + ". Mda: " + obj.getMoneda());

            if( i==0 ) obj0 = obj;

            logger.info("obj    | obj0");
            logger.info(obj.getCodcli() + " | " + obj0.getCodcli());
            logger.info(obj.getCodagcfacturacion() + "    | " + obj0.getCodagcfacturacion());
            logger.info("?cambiar: " + ( obj.getCodcli().compareTo(obj0.getCodcli())!=0 || obj.getCodagcfacturacion().compareTo(obj0.getCodagcfacturacion())!=0 ));
            if( obj.getCodcli().compareTo(obj0.getCodcli())!=0 || obj.getCodagcfacturacion().compareTo(obj0.getCodagcfacturacion())!=0 ){
                logger.info("... se debe generar una nueva entrada.");
                ttl.setVencida_6(col6);
                ttl.setVencida_5(col5);
                ttl.setVencida_4(col4);
                ttl.setVencida_3(col3);
                ttl.setVencida_2(col2);
                ttl.setVencida_1(col1);
                ttl.setNo_vencida(col0);

                gen = null;
                gen = new RepGral();
                gen.setCodagcfacturacion(obj0.getCodagcfacturacion());
                gen.setAgcfacturacion(obj0.getAgcfacturacion());
                gen.setCodcli(obj0.getCodcli());
                gen.setNomcli(obj0.getNomcli());
                gen.setVlr_saldo(obj0.getVlr_saldo());
                gen.setFecha_fra(obj0.getFecha_fra());
                gen.setFecha_venc_fra(obj0.getFecha_venc_fra());
                gen.setFactura(obj0.getFactura());
                gen.setMoneda(obj0.getMoneda());
                gen.setNdias(obj0.getNdias());
                gen.setNegocio(obj0.getNegocio());
                gen.setVencida_6(col6);
                gen.setVencida_5(col5);
                gen.setVencida_4(col4);
                gen.setVencida_3(col3);
                gen.setVencida_2(col2);
                gen.setVencida_1(col1);
                gen.setNo_vencida(col0);

                this.general.add(gen);
                this.detall.add(ttl);
                this.resum.add(detall);
                col0 = 0;
                col1 = 0;
                col2 = 0;
                col3 = 0;
                col4 = 0;
                col5 = 0;
                col6 = 0;
                this.resum.add(ttl);
                this.detall = new Vector();
                ttl = new RepGral();
            }

            int dias = obj.getNdias();
            //dias = dias < 0 ? dias*-1 : dias;

            /*
            logger.info("*****************************************");
            logger.info("CLIENTE: " + obj.getCodcli());
            logger.info("FACTURA: " + obj.getFactura());
            logger.info("VALOR SALDO: " + obj.getVlr_saldo() );
            logger.info("VENCE: " + obj.getFecha_venc_fra() );
            logger.info("DIAS: " + dias );
            */

            obj.setVencida_6(0);
            obj.setVencida_5(0);
            obj.setVencida_4(0);
            obj.setVencida_3(0);
            obj.setVencida_2(0);
            obj.setVencida_1(0);
            obj.setNo_vencida(0);

            if( dias>90 ){
                obj.setVencida_6(obj.getVlr_saldo());
                col6 += obj.getVlr_saldo();
            } else if ( dias > 60 ){
                obj.setVencida_5(obj.getVlr_saldo());
                col5 += obj.getVlr_saldo();
            } else if ( dias > 45 ){
                obj.setVencida_4(obj.getVlr_saldo());
                col4 += obj.getVlr_saldo();
            } else if ( dias > 30 ){
                obj.setVencida_3(obj.getVlr_saldo());
                col3 += obj.getVlr_saldo();
            } else if ( dias > 15 ){
                obj.setVencida_2(obj.getVlr_saldo());
                col2 += obj.getVlr_saldo();
            } else if ( dias > 0 ){
                obj.setVencida_1(obj.getVlr_saldo());
                col1 += obj.getVlr_saldo();
            } else {
                obj.setNo_vencida(obj.getVlr_saldo());
                col0 += obj.getVlr_saldo();
            }

            this.detall.add(obj);

            obj0 = obj;
        }

        ttl.setVencida_6(col6);
        ttl.setVencida_5(col5);
        ttl.setVencida_4(col4);
        ttl.setVencida_3(col3);
        ttl.setVencida_2(col2);
        ttl.setVencida_1(col1);
        ttl.setNo_vencida(col0);

        gen = null;
        gen = new RepGral();
        gen.setAgcfacturacion(obj.getAgcfacturacion());
        gen.setCodagcfacturacion(obj0.getCodagcfacturacion());
        gen.setCodcli(obj.getCodcli());
        gen.setNomcli(obj.getNomcli());
        gen.setVlr_saldo(obj.getVlr_saldo());
        gen.setFecha_fra(obj.getFecha_fra());
        gen.setFecha_venc_fra(obj.getFecha_venc_fra());
        gen.setFactura(obj.getFactura());
        gen.setMoneda(obj.getMoneda());
        gen.setNdias(obj.getNdias());
        gen.setVencida_6(col6);
        gen.setVencida_5(col5);
        gen.setVencida_4(col4);
        gen.setVencida_3(col3);
        gen.setVencida_2(col2);
        gen.setVencida_1(col1);
        gen.setNo_vencida(col0);

        this.general.add(gen);
        this.detall.add(ttl);
        this.resum.add(this.detall);
    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType(tipoRespuesta+"; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }


}
// Entregado a ivan 23 febrero

