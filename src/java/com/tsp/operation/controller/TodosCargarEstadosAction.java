/*
 * TodosCargarEstadosAction.java
 *
 * Created on 13 de marzo de 2005, 10:53 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class TodosCargarEstadosAction extends Action {
    
    /** Creates a new instance of CompaniaCargarEstadosAction */
    public TodosCargarEstadosAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        String pagina = request.getParameter("pagina"); 
        String pais = request.getParameter("pais");
        HttpSession session = request.getSession();
        try{ 
            model.paisservice.buscarpais(request.getParameter("pais"));      
            Pais spais = model.paisservice.obtenerpais();
            
            next = next + "?cp=" + spais.getCountry_code() + "&np=" +spais.getCountry_name();
            
            request.setAttribute("estados", "cargados");
            request.setAttribute("msg", "");
            request.setAttribute("ciudades", "");                       
        }catch (SQLException e){
               throw new ServletException(e.getMessage());
         }
        this.dispatchRequest(next);
    }
}
