/*
 * SeriesOpcionesAction.java
 * Modificado: Ing. Iv�n Devia Acosta
 * Created on 2 de diciembre de 2004, 09:00 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.*;
/**
 *
 * @author  mcelin
 */
public class SeriesOpcionesAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of SeriesOpcionesAction */
    public SeriesOpcionesAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            String Opcion = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            Series serie = null;
            String cancelar = (request.getParameter("cancelar")!=null)?request.getParameter("cancelar"):"";
            String Pagina = "";
            String Mensaje= "";
            Pagina = "/series/series.jsp";
            Mensaje="�El registro ya existe en la Base de Batos!";
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            if( Opcion.equals("anular_serie_cheque") ){
                Series s = model.SeriesSvc.getserie();
                //logger.info("? id : " + s.getId() );
                //model.SeriesSvc.Anular_Series( s.getDistrito(),  s.getCodigo_Ciudad(), s.getCodigo_Documento(), s.getBanco(), s.getAgencia_Banco(), s.getCuenta(), s.getN_Inicial() );
                model.SeriesSvc.anularSerieCheque(s.getId());
                Mensaje = "";
                
                model.SeriesSvc.setRegistros( new LinkedList() );
                
                Pagina = "/series/buscarseries.jsp";
                
            }else{
                if(Opcion.equals("Anular")) {
                    
                    ////System.out.println("anular");
                    List lista = model.SeriesSvc.getRegistros();
                    List Registros = new LinkedList();
                    Iterator It = lista.iterator();
                    int i=0;
                    while(It.hasNext()) {
                        Series datos = (Series) It.next();
                        String CHK = request.getParameter("Serie_"+i);
                        if(CHK!=null) {
                            model.SeriesSvc.Anular_Series(datos.getDistrito(), datos.getCodigo_Ciudad(), datos.getCodigo_Documento(), datos.getBanco(), datos.getAgencia_Banco(), datos.getCuenta(), datos.getN_Inicial());
                            datos.setEstado("checked='checked'");
                        }
                        else {
                            model.SeriesSvc.No_Anular_Series(datos.getDistrito(), datos.getCodigo_Ciudad(), datos.getCodigo_Documento(), datos.getBanco(), datos.getAgencia_Banco(), datos.getCuenta(), datos.getN_Inicial());
                            datos.setEstado(" ");
                        }
                        Registros.add(datos);
                        i++;
                    }
                    model.SeriesSvc.setRegistros(Registros);
                    Pagina = "/series/buscarseries.jsp";
                    Mensaje= "";
                    
                }else{
                    if(!Opcion.equals("Buscar")){
                        if(!Opcion.equals("Seleccionar")){
                            
                            if(!Opcion.equals("Guardar")){
                                Opcion="Modificar";
                            }
                        }
                    }
                    else{
                        Mensaje="";
                    }
                    if(Opcion.equals("Modificar")) {
                        ////System.out.println("modificar");
                        boolean sw = false;
                        serie = model.SeriesSvc.getserie();
                        if( serie.getPrefijo().equals( request.getParameter("Prefijo") ) )
                            sw = true;
                        serie.setUsuario(request.getParameter("Usuario"));
                        //serie.setPrefijo(request.getParameter("Prefijo"));
                        //AMATURANA 17.04.2007
                        serie.setPrefijo(request.getParameter("Prefijo")!=null ? request.getParameter("Prefijo") : "");
                        serie.setN_Final(request.getParameter("Serie_Final"));
                        serie.setL_Numero(request.getParameter("Ultimo_Numero"));
                        //AMATURANA 17.04.2007
                        serie.setConcepto(request.getParameter("concept_code"));
                        String[] logins = {};
                        if( request.getParameterValues("c_docSelec")!=null )
                            logins = request.getParameterValues("c_docSelec");
                        serie.setUsuarios(logins);
                        /**/
                        model.SeriesSvc.setSerie(serie);
                        
                        if( !sw ) {
                            if( !model.SeriesSvc.Existe_Series() ){
                                //if( serie!=null && serie.getCodigo_Documento().equals("004") ){//AMATURANA 17.04.2007
                                    model.SeriesSvc.modificarSerieCheque(usuario.getBase());
                                /*} else {
                                    model.SeriesSvc.Modificar();
                                }*/
                                model.SeriesSvc.Reinicial();
                                Mensaje= "Se modifico la serie";
                            }
                            else{
                                Mensaje="�El registro ya existe en la Base de Batos!";
                                //model.SeriesSvc.Reinicial();
                            }
                        } else {
                            //if( serie!=null && serie.getCodigo_Documento().equals("004") ){//AMATURANA 17.04.2007
                                model.SeriesSvc.modificarSerieCheque(usuario.getBase());
                            /*} else {
                                model.SeriesSvc.Modificar();
                            }*/
                            model.SeriesSvc.Reinicial();
                            Mensaje= "Se modifico la serie";
                        }
                        Pagina = "/series/buscarseries.jsp";
                    }else{
                        serie = Series.load(request);
                        logger.info("? concepto: " + serie.getConcepto());
                        logger.info("? usuarios: " + serie.getUsuarios());
                        model.SeriesSvc.setSerie(serie);
                    }
                }
                if(Opcion.equals("Guardar")) {
                    if(cancelar.equals("ok")) {
                        ////System.out.println("reiniciar");
                        model.SeriesSvc.Reinicial();
                        Pagina = "/series/series.jsp";
                        Mensaje="";
                    }
                    else{
                        ////System.out.println("guardar");
                        if(!model.SeriesSvc.Existe_Series()) {
                            model.SeriesSvc.insertarSerieCheque(usuario.getBase());
                            model.SeriesSvc.Reinicial();
                            Mensaje="�El registro fue ingresado con exito!";
                        }
                        else{
                            Mensaje="�El registro ya existe en la Base de Batos!";
                            //Series nueva = model.SeriesSvc.getserie();
                            model.SeriesSvc.Reinicial();
                            //request.setAttribute("nueva",  nueva);
                        }
                        Pagina = "/series/series.jsp";
                    }
                }
                
                if(Opcion.equals("Seleccionar")) {
                    ////System.out.println("seleccionar");
                    model.SeriesSvc.Buscar_Ciudades_Js();
                    model.SeriesSvc.Buscar_Documento_Js();
                    model.SeriesSvc.Buscar_Banco_Js();
                    model.SeriesSvc.Buscar_Agencia_Js();
                    model.SeriesSvc.Buscar_Cuenta_Js();
                    Pagina = "/series/series.jsp";
                    Mensaje="";
                    //AMATURANA 17.04.2007
                    TreeMap conceptos = model.tablaGenService.loadTableType("CONCHEQUES");
                    model.SeriesSvc.setConceptos(conceptos);
                    model.SeriesSvc.loadUsuariosJs();
                    model.SeriesSvc.loadUsuariosSerie_Js(String.valueOf(serie.getId()));
                }
                
                if(Opcion.equals("Buscar")) {
                    ////System.out.println("buscar");
                    model.SeriesSvc.Buscar_Series_all();
                    if(model.SeriesSvc.getRegistros().size()>0){
                        Mensaje="";
                    }
                    else{
                        Mensaje="�No se encontraron registros!";
                    }
                    Pagina = "/series/buscarseries.jsp";
                }
                
                /*if (Opcion.equals("Cancelar")){
                    model.SeriesSvc.Reinicial();
                    model.SeriesSvc.Buscar_Ciudades_Js();
                    model.SeriesSvc.Buscar_Documento_Js();
                    model.SeriesSvc.Buscar_Banco_Js();
                    model.SeriesSvc.Buscar_Agencia_Js();
                    model.SeriesSvc.Buscar_Cuenta_Js();
                }*/
                
                
            }
            final String next = Pagina+"?Mensaje="+Mensaje;
            ////System.out.println("next:"+next);
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Erro en [SeriesOpcionesAction]...\n"+e.getMessage());
        }
    }
}
