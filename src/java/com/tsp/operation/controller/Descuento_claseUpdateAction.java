    /*******************************************************************
 * Nombre clase: Descuento_especialUpdateAction.java
 * Descripci�n: Accion para actualizar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 02:42 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Descuento_claseUpdateAction extends Action{
    
    /** Creates a new instance of Descuento_especialUpdateAction */
    public Descuento_claseUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="/jsp/equipos/descuento_clase/descuento_claseModificar.jsp?&reload=ok";
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute ("Distrito");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo").toUpperCase ();
        String mes = request.getParameter("c_mes");
        String ano = request.getParameter("c_ano");
        mes = ano+"-"+mes;        
        String frecuencia = request.getParameter("c_frecuencia");
        double valor = Double.parseDouble ( !request.getParameter("c_valor").equals ("")? request.getParameter("c_valor").replaceAll (",","") : "0"  );
        String clase = request.getParameter("c_clase").toUpperCase ();
        float porcentaje_descuento = Float.parseFloat ( !request.getParameter("c_porcentaje").equals ("") ? request.getParameter("c_porcentaje").replaceAll (",","") : "0" );    
        try{
            Descuento_clase descuento = new Descuento_clase();
            descuento.setClase_equipo (clase);
            descuento.setValor_mes (valor);
            descuento.setMes_proceso (mes);
            descuento.setFrecuencia (frecuencia);
            descuento.setCodigo_concepto (codigo);
            descuento.setPorcentaje_descuento (porcentaje_descuento);
            descuento.setDistrito (distrito);
            descuento.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            descuento.setUsuario_creacion(usuario.getLogin().toUpperCase());
            descuento.setBase(usuario.getBase().toUpperCase());
            model.descuento_claseService.updateDescuento_clase(descuento);
            request.setAttribute("mensaje","La informaci�n ha sido modificada exitosamente!");
            model.descuento_claseService.searchDescuento_clase(clase, distrito, codigo);

        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);         
    }
    
}
