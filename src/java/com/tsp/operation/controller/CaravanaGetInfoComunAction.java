/**
 *
 */
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class CaravanaGetInfoComunAction extends Action {
  static Logger logger = Logger.getLogger(CaravanaGetInfoComunAction.class);
  
  /**
   * Executes the action
   */
  public void run() throws ServletException, InformationException {
    String cmd = request.getParameter("cmd");
    PlanViaje planVj = null;
    String next = null;
    Caravana caravana = null;
    HttpSession session = request.getSession();
    Usuario usuario = (Usuario)session.getAttribute("Usuario");
    if(cmd == null){
      
      if (usuario.getAccesoplanviaje().equals("1")){
         next = "/caravana/PantallaCaravanaInfoComun.jsp";
      }
      else{
          request.setAttribute("mensaje", "NO TIENE AUTORIZACION PARA ACCEDER A ESTA OPCION");
          next = "/planviaje/PantallaFiltroPlanViaje.jsp";
      }
    }
    else{
      planVj = new PlanViaje();
      
      planVj.setAl1(request.getParameter("al1"));
      planVj.setAt1(request.getParameter("at1"));
      planVj.setAl2(request.getParameter("al2"));
      planVj.setAt2(request.getParameter("at2"));
      planVj.setAl3(request.getParameter("al3"));
      planVj.setAt3(request.getParameter("at3"));
      planVj.setPl1(request.getParameter("pl1"));
      planVj.setPt1(request.getParameter("pt1"));
      planVj.setPl2(request.getParameter("pl2"));
      planVj.setPt2(request.getParameter("pt2"));
      planVj.setPl3(request.getParameter("pl3"));
      planVj.setPt3(request.getParameter("pt3"));
      planVj.setQlo(request.getParameter("qlo"));
      planVj.setQto(request.getParameter("qto"));
      planVj.setQld(request.getParameter("qld"));
      planVj.setQtd(request.getParameter("qtd"));
      planVj.setQl1(request.getParameter("ql1"));
      planVj.setQt1(request.getParameter("qt1"));
      planVj.setQl2(request.getParameter("ql2"));
      planVj.setQt2(request.getParameter("qt2"));
      planVj.setQl3(request.getParameter("ql3"));
      planVj.setQt3(request.getParameter("qt3"));
      planVj.setTl1(request.getParameter("tl1"));
      planVj.setTt1(request.getParameter("tt1"));
      planVj.setTl2(request.getParameter("tl2"));
      planVj.setTt2(request.getParameter("tt2"));
      planVj.setTl3(request.getParameter("tl3"));
      planVj.setTt3(request.getParameter("tt3"));
      planVj.setUsuario(usuario.getLogin());
      
      request.getSession().setAttribute("planVj", planVj);
      caravana = new Caravana();
      
      try{
        model.caravanaService.obtenerNumCaravana();
        caravana.setCodigo(model.caravanaService.getNumCaravana());
        caravana.setUsuario(usuario.getLogin());
      }
      catch(SQLException e){
        throw new ServletException(e.getMessage());
      }
      request.getSession().setAttribute("caravana", caravana);
      logger.info(usuario.getNombre() + " ejecuto CARAVANA ");
      next = "/caravana/PantallaCaravanaPlanilla.jsp";
    }
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }
}
