/*
 * ConsultasManageAction.java
 *
 * Created on 15 de abril de 2005, 04:37 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.QueryToExcelThread;
import org.apache.log4j.Logger;

/**
 * Acci�n invocada por el controlador para utilizar el m�dulo de ejecuci�n
 * de comandos SQL.
 * @author  iherazo
 */
public class ConsultasManageAction extends Action {
  private static Logger logger = Logger.getLogger(ConsultasManageAction.class);

  public void run() throws ServletException, InformationException {
    String page = "/admin/Consultas.jsp";
    String cmd = request.getParameter("cmd");
    String content = request.getParameter("contentType");
    Hashtable<String, String[]> parameters;
    cmd = (cmd == null ? "" : cmd);
    //System.out.println("cmd"+cmd);
    try {
      if( !cmd.equals("") ) {
        HttpSession session = request.getSession();
        Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
        if (cmd.equals("sql_list")){
          page = "/body/PantallaConsultas.jsp?cmd=sql_list";
        }else if( cmd.equals("create") ) {
          page = "/body/PantallaConsultas.jsp?cmd=";
        }else if( cmd.equals("sql_create") ) {
          parameters = new Hashtable<String, String[]>(request.getParameterMap());
          String baseDeDatos[] = parameters.get("baseDeDatos");
          //System.out.println("parameters:"+baseDeDatos[0]);//baseDeDatos
          model.consultaService.consultasManageSqlCreate(parameters);
          logger.info(
          (loggedUser != null ? loggedUser.getNombre() : "Usuario desconocido") +
          " cre� la consulta " + request.getParameter("query")
          );
          request.setAttribute("mensaje", "� CONSULTA CREADA EXITOSAMENTE !");
        }else if( cmd.equals("sql_execute") || cmd.equals("sql_listexec") ){
          if( content.equals("file") )
          {
            String dir = loggedUser.getLogin();
            String descripcion = loggedUser.getBd()+"_"+request.getParameter("descripcion");
            QueryToExcelThread hilo = null;
            try{
              model.DirectorioSvc.create(dir);
            }
            catch(Exception e){
                System.out.println("error en action de consultas"+e.toString());
              throw new SQLException("ERROR CREANDO DIRECTORIO " + dir + " " + e.getMessage());
            }
            String fechaActual = Utility.getDate(6).replaceAll("/","");
            fechaActual=fechaActual.replaceAll(":", "");
            if (descripcion.length() > 20)
              descripcion = descripcion.substring(0, 20).toUpperCase().trim();
            else
              descripcion = descripcion.toUpperCase().trim();
            String fileName =  descripcion + fechaActual + ".xls";
            String carpeta = model.DirectorioSvc.getUrl() + dir;
            String path = carpeta + "/" + fileName;
            hilo = new QueryToExcelThread();
            hilo.start(model, path, request.getParameterMap());
            page = "/body/PantallaConsultas.jsp?cmd=file";
          }else{
            parameters = new Hashtable<String, String[]>(request.getParameterMap());
            model.consultaService.consultasManageSqlExecute(parameters);
            if ( cmd.equals("sql_execute") || cmd.equals("sql_listexec") )
              page = "/body/PantallaConsultas.jsp?cmd=sql_execute";

            if (content.equals("nitobix")){//20101117
                page = page + "&nitobix="+content;//20101117
            }//20101117
            
            String query = request.getParameter("query");
            String recordId = request.getParameter("recordId");
            logger.info(
            (loggedUser != null ? loggedUser.getNombre() : "Usuario desconocido") +
            " ejecut� la consulta almacenada " + (recordId != null ? "con id = " +
            recordId : query)
            );
          }
        }else if( cmd.equals("sql_search") ){
          String usuarios[];
          String id = request.getParameter("id");
          TreeMap userAdd = new TreeMap();
          ConsultaSQL consultaSQL = model.consultaService.getQry(id);
          Usuario usuario;
          if (consultaSQL != null){
            DatosConsulta datosConsulta = new DatosConsulta();
            usuario = model.consultaService.UsuarioSearch(consultaSQL.getCreador());
            datosConsulta.setId(consultaSQL.getRecordId());
            datosConsulta.setCreador(usuario.getNombre());
            datosConsulta.setDescripcion(consultaSQL.getDescripcion());
            datosConsulta.setQuery(consultaSQL.getQuery().replace("?", "<param>"));
            datosConsulta.setBaseDeDatos(consultaSQL.getBaseDeDatos());
            usuarios = consultaSQL.getUsuarioAsociado().split(",");
            for (int i = 0; i < usuarios.length; i++){
              usuario = model.consultaService.UsuarioSearch(usuarios[i].trim());
              userAdd.put(usuario.getNombre(), usuario.getLogin());
            }
            datosConsulta.setUserAdd(userAdd);
            datosConsulta.setParamNamesList(consultaSQL.getNombresParametros());
            request.setAttribute("consulta", datosConsulta);
            page = "/body/PantallaConsultas.jsp?cmd=sql_update";
          }
        }else if( cmd.equals("sql_update") ){
          parameters = new Hashtable<String, String[]>(request.getParameterMap());
          model.consultaService.consultasManageSqlUpdate(parameters);
          logger.info(
          (loggedUser != null ? loggedUser.getNombre() : "Usuario desconocido") +
          " actualizo la consulta " + request.getParameter("query")
          );
          request.setAttribute("mensaje", "� CONSULTA MODIFICADA EXITOSAMENTE !");
          page = "/body/PantallaGestionConsultas.jsp";
        }else if (cmd.equals("sql_delete")){
          String ids[] = request.getParameterValues("id");
          for (int i = 0; i < ids.length; i++)
            model.consultaService.consultasManageSqlDelete(ids[i]);
          request.setAttribute("mensaje", "� CONSULTA ELIMINADA EXITOSAMENTE !");
        }
      }
    }catch(Exception SQLE){//20101118
      throw new ServletException(SQLE.getMessage());
    }
    //System.out.println("page"+page);
    this.dispatchRequest(page);
  }
}