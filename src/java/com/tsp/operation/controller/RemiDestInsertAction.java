/***********************************************************************************
 * Nombre clase : ............... RemiDestInsertAction.java                        *
 * Descripcion :................. Clase que maneja los eventos relacionados con el *
 *                                ingreso de Remitentes y Destinatarios            * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 17 de noviembre de 2005, 06:30 PM                *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class RemiDestInsertAction extends Action{    
    
    public RemiDestInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {                
        
        String next = "/jsp/masivo/remitente_destinatario/rem_destInsert.jsp?msg=";
        HttpSession session = request.getSession();
        String msg = "";
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        RemiDest remdest = new RemiDest();
        String codcli     =  request.getParameter("codcli");
        String tipo       =  request.getParameter("tipo");
        String secuencia  =  request.getParameter("secuencia");
        String ciudad     =  request.getParameter("ciudad"); 
        String tipo_mensaje = "";
        //Osvaldo
        String referencia     =  request.getParameter("referencia");  
        
        //Ivan Dario 30 Oct 2006
        String nombre_contacto = request.getParameter("nombre_contacto");  
        String telefono = request.getParameter("telefono"); 
        
        /* Creando la llave primaria codigo */
        String codigo = codcli.substring(3,6)+tipo+ciudad+secuencia;        
        /* Seteando el objeto remidest */
        remdest.setCodigo(codigo);
        remdest.setTipo(tipo);
        remdest.setCodcli(codcli);
        remdest.setNombre(request.getParameter("nombre"));
        remdest.setDireccion(request.getParameter("direccion"));
        remdest.setCiudad(ciudad);
        remdest.setCration_user(usuario.getLogin());
        remdest.setBase(usuario.getBase());
        remdest.setDstrct(session.getAttribute("Distrito").toString());
        remdest.setEstado("A"); //Estado del registro A=Activo     
        //Osvaldo
        remdest.setReferencia(referencia);
        tipo_mensaje = tipo.equals("R")? "Remitente":"Destinatario";
        
        //Ivan Dario 30 Oct 2006
        remdest.setNombre_contacto(nombre_contacto);
        remdest.setTelefono(telefono);
        
        try{ 
            if (!model.remidestService.existeRemitenteDestinatario(codigo)) {
                if (!model.ciudadService.existeCiudad(ciudad)) {
                    msg = "El codigo de la ciudad no existe";
                } else if (!model.clienteService.existeCliente(codcli)) {
                    msg = "El codigo del cliente no existe";
                }
                else {                
                    model.remidestService.insertarRemitenteDestinatario(remdest);
                    msg = tipo_mensaje+" agregado Exitosamente";
                }
            } else {
                msg = "Ya existe un registro con este codigo, cambie el numero de secuencia";
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        next+=msg;
        this.dispatchRequest(next);
        
    }
    
}
