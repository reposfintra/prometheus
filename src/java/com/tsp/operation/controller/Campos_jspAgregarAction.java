/********************************************************************
 *      Nombre Clase.................   Campos_jspAgregarAction.java    
 *      Descripci�n..................   Almacena un arreglo de campos y los ingresa en el 
 *                                      respectivo archivo.
 *      Autor........................   Ing. Rodrigo Salazar
 *      Fecha........................   20.06.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  rodrigo
 */
public class Campos_jspAgregarAction extends Action{
    
     public Campos_jspAgregarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        //get data
        String pag = request.getParameter("c_pagina");
        String carp = request.getParameter("c_carpeta");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        String pagina = request.getParameter("pagina");
        //tamatu 26.10.2005
        String next="/jsp/trafico/permisos/campos_jsp/cargarcamposListar.jsp";
        
        try{ 
            model.jspService.serchJsp(pag);
            Jsp jsp = model.jspService.getJsp();
            
            //tamatu 25.10.2005
            String ruta = application.getRealPath("/") + jsp.getRuta();
            
            model.camposjsp.leeArchivos(pag, ruta, jsp.getNombre(), usuario.getLogin().toUpperCase());
            //System.out.println(jsp.getNombre());
            Vector campos = model.camposjsp.searchDetallecampos_jsps(pag);
            //System.out.println("------------> Pagina: " + pag );
            //System.out.println("------------> Campos: " + campos.size() );
            
            session.setAttribute("tipo_ubicaciones", campos);
            session.setAttribute("pag", jsp.getRuta() + jsp.getNombre());
            session.setAttribute("cod_jsp", pag);
        }catch (SQLException e){
            throw new ServletException("hola"+e.getMessage());
        
        }
        this.dispatchRequest(next);
    }
}//end class
