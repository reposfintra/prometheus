/********************************************************************
 *      Nombre Clase.................   AplicacionInsertAction.java    
 *      Descripci�n..................   Inserta u nuevo registro en la tabla otros_conceptos
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OtrConceptosInsertAction extends Action{
        
        public OtrConceptosInsertAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblotros_conceptos/otrconceptInsert.jsp";
                String next = "";
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Concepto concepto = new Concepto();
                        concepto.setBase(usuario.getBase());
                        concepto.setC_tabla(request.getParameter("c_tabla"));
                        concepto.setC_descripcion(request.getParameter("c_descripcion"));                        
                        concepto.setDistrito((String) session.getAttribute("Distrito"));
                        concepto.setUsuario_creacion(usuario.getLogin());
                        concepto.setUsuario_modificacion(usuario.getLogin());
                        
                        Concepto existe = model.otros_conceptosSvc.obtenerConcepto(concepto.getC_tabla(), concepto.getC_descripcion());
                                                
                        if ( existe!=null ){
                                if( existe.getEstado().matches("A") ){                                        
                                        concepto.setEstado("");
                                        concepto.setNdescripcion(concepto.getC_descripcion());
                                        concepto.setNtabla(concepto.getC_tabla());
                                        model.otros_conceptosSvc.actualizarConcepto(concepto);
                                        pag += "?msg=Se ha definido el concepto correctamente.";                                        
                                }else{
                                        session.setAttribute("conceptoInsert", concepto);
                                        pag += "?msg=No se puede procesar la informaci�n. El concepto ya se encuentra definido.";
                                }
                        }else{
                                model.otros_conceptosSvc.ingresarConcepto(concepto);
                                pag += "?msg=Se ha definido el concepto correctamente.";
                        }                        
                        
                        next = com.tsp.util.Util.LLamarVentana(pag, "Definir otros conceptos");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
