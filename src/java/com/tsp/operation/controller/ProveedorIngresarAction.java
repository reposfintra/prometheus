/********************************************************************
 *      Nombre Clase.................   ProveedorIngresarAction.java
 *      Descripci�n..................   Ingresa un registro en el archivo proveedor.
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.09.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Tito Andres
 */
public class ProveedorIngresarAction extends Action {

    /** Creates a new instance of ProveedorIngresarAction */
    public ProveedorIngresarAction() {
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next = "/jsp/cxpagar/proveedor/Proveedor.jsp";

        String nit = request.getParameter("nit");
        String pag = request.getParameter("pag");

        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String opcion = "";
        boolean redirect = false;
        String resp = "";
        try {
            opcion = request.getParameter("opcion") != null ? request.getParameter("opcion") : "";
            if (opcion != null && !opcion.equals("")) {
                if (opcion.equals("buscarsub")) {
                    String idsect = request.getParameter("idsect") != null ? request.getParameter("idsect") : "0";
                    try {
                        resp = model.proveedorService.buscarSubSects(idsect);
                    } catch (Exception e) {
                        System.out.println("error: " + e.toString());
                        e.printStackTrace();
                    }
                    try {
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(resp);
                    } catch (Exception e) {
                        throw new Exception("Error al escribir el response: " + e.toString());
                    }
                }
                if (opcion.equals("viewdats")) {
                    String idconv = request.getParameter("idconv") != null ? request.getParameter("idconv") : "0";
                    try {
                        resp = model.proveedorService.buscarDats(idconv);
                    } catch (Exception e) {
                        System.out.println("error: " + e.toString());
                        e.printStackTrace();
                    }
                    try {
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(resp);
                    } catch (Exception e) {
                        throw new Exception("Error al escribir el response: " + e.toString());
                    }
                }
                if (opcion.equals("buscardat")) {
                    try {
                        resp = model.proveedorService.datosAfilS(nit);
                    } catch (Exception e) {
                        System.out.println("error: " + e.toString());
                        e.printStackTrace();
                    }
                    try {
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(resp);
                    } catch (Exception e) {
                        throw new Exception("Error al escribir el response: " + e.toString());
                    }
                }
                if (opcion.equals("cargarest")) {//<!-- 20101111 -->
                    String cod = request.getParameter("cod") != null ? request.getParameter("cod") : "";
                    ArrayList<BeanGeneral> lista = null;
                    BeanGeneral rk = null;
                    try {
                        lista = model.proveedorService.buscarDeps(cod);
                    } catch (Exception e) {
                        System.out.println("error(action): " + e.toString());
                        e.printStackTrace();
                    }
                    resp = "<select name='est' id='est' onchange='cargarCiudades(this.selectedIndex);' class='textbox'>";
                    if (lista != null && lista.size() > 0) {
                        resp += "<option value=''>...</option>";
                        for (int i = 0; i < lista.size(); i++) {
                            rk = lista.get(i);
                            resp += "<option value='" + rk.getValor_01() + "'>" + rk.getValor_02() + "</option>";
                        }
                    } else {
                        resp += "<option value=''>No se encontraron registros</option>";
                    }
                    resp += "</select>";
                    try {
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(resp);
                    } catch (Exception e) {
                        throw new Exception("Error al escribir el response: " + e.toString());
                    }
                }
                if (opcion.equals("cargarciu")) {
                    String codpt = request.getParameter("cod") != null ? request.getParameter("cod") : "";
                    String pais = request.getParameter("pais") != null ? request.getParameter("pais") : "";
                    ArrayList<BeanGeneral> lista = null;
                    BeanGeneral rk = null;
                    try {
                        lista = model.proveedorService.buscaCius(pais, codpt);
                    } catch (Exception e) {
                        System.out.println("error(action): " + e.toString());
                        e.printStackTrace();
                    }
                    resp = "<select name='ciudad' id='ciudad' class='textbox'>";
                    if (lista != null && lista.size() > 0) {
                        resp += "<option value=''>...</option>";
                        for (int i = 0; i < lista.size(); i++) {
                            rk = lista.get(i);
                            resp += "<option value='" + rk.getValor_01() + "'>" + rk.getValor_02() + "</option>";
                        }
                    } else {
                        resp += "<option value=''>No se encontraron registros</option>";
                    }
                    resp += "</select>";
                    try {
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(resp);
                    } catch (Exception e) {
                        throw new Exception("Error al escribir el response: " + e.toString());
                    }
                }
            } else {
                redirect = true;
                Proveedor prov = new Proveedor();
                prov.setC_agency_id(request.getParameter("c_agency_id"));
                //Modificacion Diogenes 2006-02-16
                prov.setC_agente_retenedor(request.getParameter("c_agente_retenedor"));
                prov.setC_autoretenedor_iva((request.getParameter("c_autoretenedor_iva") == null) ? "N" : request.getParameter("c_autoretenedor_iva"));
                prov.setC_agente_retenedor((request.getParameter("c_agente_retenedor") == null) ? "N" : request.getParameter("c_agente_retenedor"));
                prov.setC_autoretenedor_ica((request.getParameter("c_autoretenedor_ica") == null) ? "N" : request.getParameter("c_autoretenedor_ica"));
                prov.setC_autoretenedor_rfte((request.getParameter("c_autoretenedor_rfte") == null) ? "N" : request.getParameter("c_autoretenedor_rfte"));
                prov.setC_banco_transfer(request.getParameter("c_banco_transfer") != null ? request.getParameter("c_banco_transfer") : "");
                prov.setC_branch_code(request.getParameter("c_branch_code") != null ? request.getParameter("c_branch_code") : "");
                prov.setC_bank_account(request.getParameter("c_bank_account") != null ? request.getParameter("c_bank_account") : "");
                prov.setC_clasificacion(request.getParameter("c_clasificacion") != null ? request.getParameter("c_clasificacion") : "");
                prov.setC_codciudad_cuenta(request.getParameter("c_codciudad_cuenta") != null ? request.getParameter("c_codciudad_cuenta") : "");
                prov.setC_gran_contribuyente(request.getParameter("c_gran_contribuyente") != null ? request.getParameter("c_gran_contribuyente") : "");
                prov.setC_idMims(request.getParameter("c_idMims") != null ? request.getParameter("c_idMims") : "");
                prov.setC_nit(request.getParameter("c_nit") != null ? request.getParameter("c_nit") : "");
                prov.setC_payment_name(request.getParameter("c_payment_name") != null ? request.getParameter("c_payment_name").toUpperCase() : "");
                prov.setC_numero_cuenta(request.getParameter("c_numero_cuenta") != null ? request.getParameter("c_numero_cuenta") : "");
                prov.setC_sucursal_transfer(request.getParameter("c_sucursal_transfer") != null ? request.getParameter("c_sucursal_transfer") : "");
                prov.setC_tipo_cuenta(request.getParameter("c_tipo_cuenta") != null ? request.getParameter("c_tipo_cuenta") : "");
                prov.setC_tipo_doc(request.getParameter("c_tipo_doc") != null ? request.getParameter("c_tipo_doc") : "");
                
                prov.setBase(usuario.getBase());
                prov.setUsuario_creacion(usuario.getLogin());
                prov.setUsuario_modificacion(usuario.getLogin());
                prov.setDistrito((String) session.getAttribute("Distrito"));
                prov.setHandle_code(request.getParameter("handle_code") != null ? request.getParameter("handle_code") : "");
                prov.setPlazo(request.getParameter("plazo") != null ? Integer.parseInt(request.getParameter("plazo")) : 0);
                //nuevo 18-03-2006
                prov.setCedula_cuenta(request.getParameter("cedula_cuenta") != null ? request.getParameter("cedula_cuenta") : "");
                prov.setNombre_cuenta(request.getParameter("nombre_cuenta") != null ? request.getParameter("nombre_cuenta").toUpperCase() : "");

                //nuevo 01-06-2006
                prov.setTipo_pago(request.getParameter("tipo_pago"));
                //jose 2006-09-28
                prov.setNit_beneficiario((request.getParameter("nit_ben") == null) ? "" : request.getParameter("nit_ben"));
                prov.setNom_beneficiario((request.getParameter("nom_ben") == null) ? "" : request.getParameter("nom_ben"));

                //01-02-2007
                prov.setConcept_code(request.getParameter("concept_code"));
                prov.setCmc(request.getParameter("handle_code"));
                prov.setAprobado(request.getParameter("aprobado") == null ? "N" : request.getParameter("aprobado"));
                session.removeAttribute("proveedor");
                Proveedor prov0 = model.proveedorService.obtenerProveedor(prov.getC_nit(), prov.getDistrito());
                //-----------20100817-------------//
                prov.setAfil(request.getParameter("afil") != null ? request.getParameter("afil") : "N");
                prov.setSede(request.getParameter("sede") != null ? request.getParameter("sede") : "N");
                prov.setRegimen(request.getParameter("regimen") != null ? request.getParameter("regimen") : "");//20100827
                prov.setCodfen(request.getParameter("codigo")!=null&&request.getParameter("codigo1")!=null&&request.getParameter("codigo2")!=null ? request.getParameter("codigo")+request.getParameter("codigo1")+request.getParameter("codigo2") : "");
                prov.setTipoProveedor(request.getParameter("tipo_proveedor"));
                String[] nit_afil = prov.getC_nit().split("_");
                prov.setNit_afiliado(nit_afil[0]);

                //-----------/20100817-------------//

                if (prov0 != null && prov0.getEstado().compareTo("A") == 0) {
                    model.proveedorService.actualizarProveedor(prov);
                } else {
                    model.proveedorService.agregarProveedor(prov);

                }

                next += "?msg=Se ha ingresado el proveedor correctamente";

            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        if (redirect == true) {
            if (pag.equals("S")) {
                    next += "&pag=S";
                }
            this.dispatchRequest(next);
        }
    }
}
