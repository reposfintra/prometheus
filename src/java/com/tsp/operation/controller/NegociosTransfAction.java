/**
 * Autor : Ing. Roberto Rocha P.. Date : 10 de Julio de 2007 Copyrigth Notice :
 * Fintravalores S.A. S.A Version 1.0 --> <%-- -@(#) --Descripcion : Action que
 * maneja los avales de Fenalco
*
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.TransaccionService;
import javax.servlet.http.*;
import java.sql.SQLException;

import com.tsp.operation.model.threads.HReporte_pago_fenalco;
import com.tsp.util.Utility;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

public class NegociosTransfAction extends Action {

    //protected HttpServletRequest request;
    boolean redirect = true;
    com.tsp.operation.model.beans.POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;

    public NegociosTransfAction() {

    }
    Usuario usuario = null;

    public void run() throws ServletException, InformationException {
        redirect = true;
        String next = "/jsp/fenalco/negocios/inicio_tranferencias.jsp";
        HttpSession session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        String op = ((String) request.getParameter("evento"));
        String option = request.getParameter("option") != null ? request.getParameter("option") : "";
        //com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();

        //System.out.println("Valor de bancos"+bco);
        if (op.equals("1")) {

            int tam = Integer.parseInt((request.getParameter("tam") != null ? request.getParameter("tam") : "0"));
            String[] nits = new String[tam];
            String bco = ((String) request.getParameter("val"));
            String fechaInicio = (String) request.getParameter("fechaini") != null ? request.getParameter("fechaini") : "";
            String fechaFin = (String) request.getParameter("fechafin") != null ? request.getParameter("fechafin") : "";
            String[] bn;
            bn = bco.split(",");
            session.setAttribute("bcosel", bn[0]);

            for (int i = 0; i < tam; i++) {

                nits[i] = request.getParameter("nit" + i);

            }

            try {

                if (fechaInicio.equals("") && fechaInicio.equals("")) {
                    model.Negociossvc.listt(nits);
                } else {
                    model.Negociossvc.listt(nits, fechaInicio, fechaFin);
                }
            } catch (Exception e) {
                throw new ServletException(e.getMessage());
            }
            next = "/jsp/fenalco/negocios/listaNegocios.jsp?";
        }

        //Mod Junio 19-2009
        if (op.equals("3")) {//Transferencia
            GestionSolicitudAvalService solcService = new GestionSolicitudAvalService(usuario.getBd());
            String msj = "";
            Vector negs = new Vector();
            try {
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                String listt[] = null;
                String[] comisiones = null;
                //esta opcion es para el modulo nuevo
                if (option.equals("0205")) {
                    listt = request.getParameter("listado").split(";_;");
                    comisiones = request.getParameter("selcom").split(";_;");
                } else {
                    listt = request.getParameterValues("listado");
                    comisiones = request.getParameterValues("selcom");
                }

                String[] vcodn = new String[listt.length];//vector con los codigos de los negocios
                String[] vcxp = new String[listt.length];//vector con las cxp del negocio
                String vno = "";//vector con los valores de la comision
                String[] bn2 = new String[3];//iniciada
                String bco = ((String) request.getParameter("bco"));
                String[] bn;
                bn = bco.split(",");
                for (int j = 0; j < listt.length; j++) {
                    bn2 = listt[j].split(",");
                    vcodn[j] = bn2[0];
                    vcxp[j] = bn2[2];
                    vno = bn2[1];
                    negs.add(bn2[0]);
                    try {
                        /*/--ACTUALIZANDO LA COMISION DE LOS NEGOCIOS TRANSFERIDOS--/*/
                        String nbanco = bn[0];
                        if (bn[0].equals("BANCOLOMBIAPAB")) {
                            nbanco = "BANCOLOMBIA";
                        }
                        tService.getSt().addBatch(model.Negociossvc.actcomString(vcxp[j], vcodn[j], comisiones[Integer.parseInt(vno)], nbanco, bn[1]));
                      //model.Negociossvc.actcom(vcodn[j],comisiones[Integer.parseInt(vno)],bn[0],bn[1]);

                        /*/--INSERTA LA TRAZA DE LA ACTIVIDAD DE DESEMBOLSO--/*/
                        if (model.Negociossvc.isCxpAfiliado(vcodn[j], vcxp[j])) {
                            Date fecha_hoy = new Date();
                            Timestamp t = new Timestamp(fecha_hoy.getTime());
                            String fecha = t.toString();
                            NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService(usuario.getBd());
                            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                            negtraza.setActividad("DES");
                            negtraza.setNumeroSolicitud(solcService.buscarNumSolicitud(vcodn[j]));
                            negtraza.setUsuario(usuario.getLogin());
                            negtraza.setCausal("");
                            negtraza.setComentarios("");
                            negtraza.setDstrct(usuario.getDstrct());
                            negtraza.setConcepto("");
                            negtraza.setCodNeg(vcodn[j]);
                            tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza).replace("now()", "'" + fecha + "'"));
                        }

                        if (model.Negociossvc.isCxpAfiliado(vcodn[j], vcxp[j])) {
                            /*/--ACTUALIZANDO EL NEGOCIO = T (transferido)--/*/
                            tService.getSt().addBatch(model.Negociossvc.uptranfString(vcodn[j]));
                        }

                        /*/--CREANDO EL EGRESO Y ACTUALIZANDO CXP--  /*/
                        ArrayList listaQuerys = model.Negociossvc.saveEGDAOString(vcxp[j], vcodn[j], usuario, nbanco, bn[1], comisiones[Integer.parseInt(vno)]);
                        // tService.getSt().addBatch(model.Negociossvc.saveEGDAOString(vcxp[j], vcodn[j], usuario, nbanco, bn[1], comisiones[Integer.parseInt(vno)]));
                        Iterator i = listaQuerys.iterator();
                        while (i.hasNext()) {

                            String sql = (String) i.next();
                            System.out.println(sql);
                            tService.getSt().addBatch(sql);
                        }

                    } catch (Exception e) {
                        throw new ServletException(e.getMessage());
                    }
                    vno = "";
                    System.out.println("Factura procesadas: " + j + " de " + listt.length);
                }
                bn2 = null;

                tService.execute();

                for (int i = 0; i < vcodn.length; i++) {
                    Negocios neg = new Negocios();
                    neg = model.Negociossvc.buscarNegocio(vcodn[i]);
                    Convenio convenio = model.gestionConveniosSvc.buscar_convenio("fintra", neg.getId_convenio() + "");
                    if (convenio.getTipo().equals("Microcredito")) {
                        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                        negtraza.setActividad("");
                        if (!neg.getFecha_liquidacion().equals(neg.getFechatran().substring(0, 10))) {
                            Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
                            if (impuesto == null) {
                                throw new Exception("Error: el convenio " + convenio.getId_convenio() + " no tiene impuesto vigente");
                            }
                            String fechaInicial = Util.fechaMasDias(neg.getFecha_liquidacion(), Integer.parseInt(neg.getFpago()));
                            neg.setFecha_liquidacion(neg.getFechatran());
                            neg.setFpago(Util.fechasDiferenciaEnDias(Util.convertiraDate(neg.getFecha_liquidacion()), Util.convertiraDate(fechaInicial)) + "");
                            //buscar numero informacion del aval...
                            SolicitudAval solicitud = solcService.buscarSolicitud(Integer.parseInt(solcService.buscarNumSolicitud(vcodn[i])));
                            model.Negociossvc.calcularLiquidacionMicrocredito(neg, usuario, impuesto, solicitud.getRenovacion(),solicitud.getNumeroSolicitud());
                            model.Negociossvc.updateNegocioMicrocredito(neg, solcService.buscarNumSolicitud(vcodn[i]), negtraza);
                            model.Negociossvc.generarCXCMicrocredito(neg, usuario, convenio);
                        }
                    }
                }

                HArchivosTransferencias hilo = new HArchivosTransferencias();
                String codb = "";
                int sw = 0;
                if (bn[0].equals("BANCOLOMBIA")) {
                    codb = "07";
                } else if (bn[0].toUpperCase().contains("BCO OCCIDENTE") || bn[0].toUpperCase().contains("CAJA SUPEREFECT")) {
                    codb = "23";
                } else if (bn[0].equals("BANCOLOMBIAPAB")) {
                    codb = "7B";
                } else {
                    msj = "No existen formatos para este banco";
                    sw = 1;
                }
                if (sw == 0) {
                    msj = "El archivo para transferir al banco " + bn[0] + " est� siendo generado....";
                    hilo.start(model, usuario, vcodn, "802022016", "FINTRA", codb, bn[0], bn[2], bn[1], vcxp);
                }
                hilo.join();

                bn = null;
                bco = null;

                //generacion de soporte de pago
                HReporte_pago_fenalco hreporte = new HReporte_pago_fenalco(negs,usuario);
                hreporte.start();
                negs = null;//libera objeto
            } catch (Exception e) {
                msj = "No se pudo realizar el archivo de transferencias intente mas tarde";
                throw new ServletException(e.getMessage());
            }
            next = "/jsp/fenalco/negocios/resultado_tranferencias.jsp?msg=" + msj;
            if (option.equals("0205")) {
                try {
                    responseTrans();
                } catch (Exception ex) {
                    throw new ServletException(ex.getMessage());
                }
            }

        }//End Opcion = 3

        if (op.equals("4")) {
            //System.out.println("Entro en la opcion de negocios");
            String con = ((String) request.getParameter("com"));
            String val = ((String) request.getParameter("val"));
            int i = Integer.parseInt(val) - Integer.parseInt(con);
            next = "/jsp/fenalco/negocios/bcotext.jsp?ms=" + i;

        }

        if (op.equals("5")) {
            String negocio = request.getParameter("negocio");
            String bco = (request.getParameter("banco") != null) ? request.getParameter("banco"): "";
            String[] bn;
            bn = bco.split(",");
            try {
                this.filtroProveedoresTransferencias(negocio, bn[0]);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (op.equals("6")) {// Buscar los negocios por unidad de negocio
            String negocio = request.getParameter("negocio");
            String bco = (request.getParameter("banco") != null) ? request.getParameter("banco"): "";
            String[] bn;
            bn = bco.split(",");
            try {
                this.cargarNegociosTrasferencia(negocio, bn[0]);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (op.equals("7")) {// Buscar los negocios por unidad de negocio y proveedor
            int tam = Integer.parseInt((request.getParameter("tam") != null ? request.getParameter("tam") : "0"));
            String[] nits = new String[tam];
            String fechaInicio = (String) request.getParameter("fechaini") != null ? request.getParameter("fechaini") : "";
            String fechaFin = (String) request.getParameter("fechafin") != null ? request.getParameter("fechafin") : "";
            String un_neg = request.getParameter("un_neg");
            String bco = (request.getParameter("val") != null) ? request.getParameter("val"): "";
            String[] bn;
            bn = bco.split(",");

            for (int i = 0; i < tam; i++) {

                nits[i] = request.getParameter("nit" + i);

            }
            try {
                this.cargarNegociosTrasferenciaProveedor(nits, fechaInicio, fechaFin, un_neg, bn[0]);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (op.equals("8")) {//Cambia la comision de acuerdo al tipo de cliente

            try {

                cambiarComision();
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (op.equals("9")) {//carga el combo de comision

            try {

                cargarComision();
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        if (op.equals("10")) {//Transferencia
            GestionSolicitudAvalService solcService = new GestionSolicitudAvalService(usuario.getBd());
            String msj = "";
            Vector negs = new Vector();
            boolean swAval = false;

            try {
                String listt[] = null;
                String[] comisiones = null;

                //esta opcion es para el modulo nuevo
                if (option.equals("0205")) {
                    listt = request.getParameter("listado").split(";_;");
                    comisiones = request.getParameter("selcom").split(";_;");
                } else {
                    listt = request.getParameterValues("listado");
                    comisiones = request.getParameterValues("selcom");
                }
                // fin

                String[] vcodn = new String[listt.length];//vector con los codigos de los negocios
                String[] vcxp = new String[listt.length];//vector con las cxp del negocio
                String vno = "";//vector con los valores de la comision
                String[] bn2 = new String[3];//iniciada
                String bco = ((String) request.getParameter("bco"));
                String[] bn;
                bn = bco.split(",");

                for (int j = 0; j < listt.length; j++) {
                    bn2 = listt[j].split(",");
                    vcodn[j] = bn2[0];
                    vcxp[j] = bn2[2];
                    vno = bn2[1];
                    negs.add(bn2[0]);
                    String nbanco = bn[0];
                    if (bn[0].equals("BANCOLOMBIAPAB")) {
                        nbanco = "BANCOLOMBIA";
                    }
                    if (comisiones[Integer.parseInt(vno)].equals("sin comision")) {
                        comisiones[Integer.parseInt(vno)] = "0";
                    }
                    try {
                        if (model.Negociossvc.isNegocioAval(vcodn[j]) || (model.Negociossvc.isCxPAval(vcxp[j]))) {
                            swAval = true;
                            generarEgresos(vcodn[j], usuario, vcxp[j], comisiones[Integer.parseInt(vno)], nbanco, bn[1], solcService);
                        } else {
                            transferirNegocio(vcodn[j], usuario, vcxp[j], comisiones[Integer.parseInt(vno)], nbanco, bn[1], solcService);
                            String negAval = "";
                            String codCxPAval = "";
                            negAval = model.Negociossvc.obtenerNegocioAval(vcodn[j]);
                            if (!negAval.equals("")) {
                                codCxPAval = model.Negociossvc.obtenerCxPAval(negAval);
                                generarCxCAval(negAval, usuario, codCxPAval, comisiones[Integer.parseInt(vno)], nbanco, bn[1], solcService);
                            }
                        }

                    } catch (Exception e) {
                        throw new ServletException(e.getMessage());
                    }
                    vno = "";
                    System.out.println("Factura procesadas: " + j + " de " + listt.length);
                }
                bn2 = null;//ya la use

                for (int i = 0; i < vcodn.length; i++) {
                    Negocios neg = new Negocios();
                    neg = model.Negociossvc.buscarNegocio(vcodn[i]);
                    Convenio convenio = model.gestionConveniosSvc.buscar_convenio("fintra", neg.getId_convenio() + "");
                     SolicitudAval solicitud = solcService.buscarSolicitud(Integer.parseInt(solcService.buscarNumSolicitud(vcodn[i])));
                    if ( convenio.getTipo().equals("Microcredito") && !model.Negociossvc.validarcompracartera(solicitud.getNumeroSolicitud()) ) {
                        ArrayList detNegocio = model.Negociossvc.buscarDetallesNegocio(neg.getCod_negocio());
                        model.Negociossvc.setLiquidacion(detNegocio);
                        model.Negociossvc.generarCXCMicrocredito(neg, usuario, convenio);
                        
                        if (neg.getPolitica().equals("R")){                        
                        model.Negociossvc.anular_documentos_micro_retanqueo(solicitud, usuario.getLogin());
                        }
                    }
                    
                    if (!convenio.getTipo().equals("Libranza")) {
                        int ciclo = model.Negociossvc.obtenerCicloFacturacion(vcodn[i]);
                        model.Negociossvc.actualizarCicloNegocio(vcodn[i], ciclo);
                        model.Negociossvc.actualizarCicloAval(vcodn[i], ciclo);
                        model.Negociossvc.actualizarCicloSeguros(vcodn[i], ciclo);
                        model.Negociossvc.actualizarCicloGps(vcodn[i], ciclo);
                    }    
                    
                    if (neg.getPolitica().equals("R")&& neg.getId_convenio()==58){
                        
                        model.Negociossvc.anular_documentos_educativo_renovado(solicitud, usuario.getLogin());                  
                    
                    }
                }
                
                if (!swAval) {
                    HArchivosTransferencias hilo = new HArchivosTransferencias();
                    String codb = "";
                    int sw = 0;
                    if (bn[0].equals("BANCOLOMBIA")) {
                        codb = "07";
                    } else if (bn[0].toUpperCase().contains("BCO OCCIDENTE") || bn[0].toUpperCase().contains("CAJA SUPEREFECT")) {
                        codb = "23";
                    } else if (bn[0].equals("BANCOLOMBIAPAB")) {
                        codb = "7B";
                    }else if (bn[0].equals("EFECTY")) {
                        codb = "10000003";
                    }else {
                        msj = "No existen formatos para este banco";
                        sw = 1;
                    }
                    if (sw == 0) {
                        msj = "El archivo para transferir al banco " + bn[0] + " est� siendo generado....";
                        hilo.start(model, usuario, vcodn, "802022016", "FINTRA", codb, bn[0], bn[2], bn[1], vcxp);
                    }
                    hilo.join();

                    bn = null;
                    bco = null;

                    //generacion de soporte de pago
                    HReporte_pago_fenalco hreporte = new HReporte_pago_fenalco(negs, usuario);
                    hreporte.start();
                }
                negs = null;//libera objeto
            } catch (Exception e) {
                msj = "No se pudo realizar el archivo de transferencias intente mas tarde";
                throw new ServletException(e.getMessage());
            }
            next = "/jsp/fenalco/negocios/resultado_tranferencias.jsp?msg=" + msj;
            if (option.equals("0205")) {
                try {
                    responseTrans();
                } catch (Exception ex) {
                    throw new ServletException(ex.getMessage());
                }
            }

        }//End Opcion 10

        if (op.equals("11")) {//Devolucion de negocio
            String neg = request.getParameter("negocio");
            String accion = request.getParameter("ac");

            String estado = "";
            String actividad = "";
            String negAval = "";
            ArrayList list = new ArrayList();
            ArrayList listAval = new ArrayList();

            try {
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                GestionSolicitudAvalService solcService = new GestionSolicitudAvalService(usuario.getBd());
                Date fecha_hoy = new Date();
                Timestamp t = new Timestamp(fecha_hoy.getTime());
                String fecha = t.toString();
                NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService(usuario.getBd());
                NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                negtraza.setActividad("DES");
                negtraza.setNumeroSolicitud(solcService.buscarNumSolicitud(neg));
                negtraza.setUsuario(usuario.getLogin());
                negtraza.setCausal("");
                negtraza.setDstrct(usuario.getDstrct());
                negtraza.setCodNeg(neg);

                negAval = model.Negociossvc.obtenerNegocioAval(neg);

                if (accion.equals("rechazar")) {
                    estado = "R";
                    actividad = "FOR";
                    negtraza.setConcepto("RECHAZAD");
                    negtraza.setComentarios("Negocio se rechaza en proceso de transferencia");
                    list = model.Negociossvc.anularCXPNegocio(neg, usuario.getLogin());
                } else if (accion.equals("devolver")) {
                    estado = "V";
                    actividad = "DEC";
                    negtraza.setConcepto("DEVOLVER");
                    negtraza.setComentarios("Negocio se devuelve a formalizacion en proceso de transferencia");
                } else if (accion.equals("desistir")) {
                    estado = "D";
                    actividad = "FOR";
                    negtraza.setConcepto("DESISTIDO");
                    negtraza.setComentarios("Negocio desistido en proceso de transferencia");
                    list = model.Negociossvc.anularCXPNegocio(neg, usuario.getLogin());
                }

                Iterator i = list.iterator();
                while (i.hasNext()) {
                    String sql = (String) i.next();
                    System.out.println(sql);
                    tService.getSt().addBatch(sql);
                }

                if ((estado.equals("R") || estado.equals("D")) && (!negAval.equals(""))) {
                    listAval = model.Negociossvc.anularCXPNegocio(negAval, usuario.getLogin());

                    Iterator j = listAval.iterator();
                    while (j.hasNext()) {
                        String sql = (String) j.next();
                        System.out.println(sql);
                        tService.getSt().addBatch(sql);
                    }
                }

                model.Negociossvc.updateEstadoNegocio(neg, usuario.getLogin(), estado, actividad);
                tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza).replace("now()", "'" + fecha + "'"));
                tService.execute();

            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (op.equals("13")) {
            boolean generado = false;
            String url = "";
            String resp1 = "";
            try {
                String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
                if (!json.equals("")) {
                    Gson gson = new Gson();
                    Type tipoListaFacturas;
                    tipoListaFacturas = new TypeToken<ArrayList<BeanGeneral>>() {
                    }.getType();
                    ArrayList<BeanGeneral> lista = gson.fromJson(json, tipoListaFacturas);
                    this.generarRUTA();
                    this.crearLibro("TransferenciaNegocios_", "TRANSFERENCIA DE NEGOCIOS");

                    String[] cabecera = {
                        "CXP", "Cod. Negocio", "Id Cliente", "Nombre", "Vr. Desembolso", "Comision", "Fecha Negocio", "Banco", "Tipo CTA", "Titular CTA", "CC o Nit", "No CTA", "Observaciones"
                    };
                    short[] dimensiones = new short[]{
                        3000, 3000, 3000, 9000, 4000, 4000, 4000, 5000, 3000, 9000, 3000, 5000, 5000
                    };
                    this.generaTitulos(cabecera, dimensiones);
                    generado = generaArchivoExcel(lista);

                    System.out.println("exportar " + generado);
                    this.cerrarArchivo();
                    if (generado) {
                        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
                        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/TransferenciaNegocios_" + fmt.format(new Date()) + ".xls";
                        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
                    }
                    this.printlnResponse(resp1, "text/plain");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                try {
                    throw new SQLException("ERROR DURANTE EXPORTACION DE LA TABLA. \n " + e.getMessage());
                } catch (SQLException ex) {
                    Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (op.equals("14")) {
            String negocio = request.getParameter("negocio");
            String bco = (request.getParameter("banco") != null) ? request.getParameter("banco") : "";
            String[] bn;
            bn = bco.split(",");
            try {
                this.cargarNegociosTrasferenciaLibranza(negocio, bn[0]);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else if (op.equals("15")) {
            String doc_rel = request.getParameter("doc_rel");
            try {
                this.cargarNegociosCHequeLibranza(doc_rel);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else if (op.equals("16")) {
            String negocio = request.getParameter("negocio");
            String cxp = request.getParameter("cxp");
            try {
                 Negocios neg = new Negocios();
                neg = model.Negociossvc.buscarNegocio(negocio);
                String idconvenio= Integer.toString(neg.getId_convenio());
                GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
                Convenio convenio = convdao.buscar_convenio(usuario.getBd(), idconvenio);
                if(convenio.getTipo().equals("Microcredito")){
                this.perfeccionarNegocioMicro(negocio,cxp, usuario); 
               
                }else{
                this.perfeccionarNegocioLibranza(negocio,cxp, usuario);
                }
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }
            next = "/jsp/fenalco/negocios/resultado_tranferencias.jsp?msg=NegocioPerfeccionado";
            
        }
        else if (op.equals("17")) {
            String negocio = request.getParameter("negocio");
            String cxp = request.getParameter("cxp");
            String banco_transferencia = request.getParameter("banco_transferencia");
            String[] bn;
            bn = banco_transferencia.split(",");
            try {
                Negocios neg = new Negocios();
                neg = model.Negociossvc.buscarNegocio(negocio);
               String idconvenio= Integer.toString(neg.getId_convenio());
                GestionConveniosDAO convdao = new GestionConveniosDAO(usuario.getBd());
                Convenio convenio = convdao.buscar_convenio(usuario.getBd(), idconvenio);
                String conv=convenio.getTipo();
                this.exportarPdf(negocio, cxp, banco_transferencia, usuario,conv);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (op.equals("18")) {//carga el combo de comision
            try {
                String unidad_negocio = request.getParameter("unidad_negocio");
                esCompraCartera(unidad_negocio, usuario);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        if (op.equals("19")) {// Buscar los negocios por unidad de negocio
            String negocio = request.getParameter("negocio");
            String bco = (request.getParameter("banco") != null) ? request.getParameter("banco"): "";
            String[] bn;
            bn = bco.split(",");
            try {
                this.cargarNegociosArchivoPlano(negocio,bn[0]);
            } catch (Exception ex) {
                Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
            }
            
           

        }
         
         if (op.equals("20")) {//Transferencia
            GestionSolicitudAvalService solcService = new GestionSolicitudAvalService(usuario.getBd());
            String msj = "";
            Vector negs = new Vector();
            boolean swAval = false;

            try {
                String listt[] = null;
                String[] comisiones = null;

                //esta opcion es para el modulo nuevo
                if (option.equals("0205")) {
                    listt = request.getParameter("listado").split(";_;");
                    comisiones = request.getParameter("selcom").split(";_;");
                } else {
                    listt = request.getParameterValues("listado");
                    comisiones = request.getParameterValues("selcom");
                }
                // fin

                String[] vcodn = new String[listt.length];//vector con los codigos de los negocios
                String[] vcxp = new String[listt.length];//vector con las cxp del negocio
                String vno = "";//vector con los valores de la comision
                String[] bn2 = new String[3];//iniciada
                String bco = ((String) request.getParameter("bco"));
                String[] bn;
                bn = bco.split(",");

                for (int j = 0; j < listt.length; j++) {
                    bn2 = listt[j].split(",");
                    vcodn[j] = bn2[0];
                    vcxp[j] = bn2[2];
                    vno = bn2[1];
                    negs.add(bn2[0]);
                    String nbanco = bn[0];
                    if (bn[0].equals("BANCOLOMBIAPAB")) {
                        nbanco = "BANCOLOMBIA";
                    }
                    if (comisiones[Integer.parseInt(vno)].equals("sin comision")) {
                        comisiones[Integer.parseInt(vno)] = "0";
                    }
                   
                    vno = "";
                    System.out.println("Factura procesadas: " + j + " de " + listt.length);
                }
                bn2 = null;//ya la use

                
                if (!swAval) {
                    HArchivosTransferencias hilo = new HArchivosTransferencias();
                    String codb = "";
                    int sw = 0;
                    if (bn[0].equals("BANCOLOMBIA")) {
                        codb = "07";
                    } else if (bn[0].toUpperCase().contains("BCO OCCIDENTE") || bn[0].toUpperCase().contains("CAJA SUPEREFECT")) {
                        codb = "23";
                    } else if (bn[0].equals("BANCOLOMBIAPAB")) {
                        codb = "7B";
                    }else if (bn[0].equals("EFECTY")) {
                        codb = "10000003";
                    }else {
                        msj = "No existen formatos para este banco";
                        sw = 1;
                    }
                    if (sw == 0) {
                        msj = "El archivo para transferir al banco " + bn[0] + " est� siendo generado....";
                        hilo.start(model, usuario, vcodn, "802022016", "FINTRA", codb, bn[0], bn[2], bn[1], vcxp);
                    }
                    hilo.join();

                    bn = null;
                    bco = null;

                    //generacion de soporte de pago
                    HReporte_pago_fenalco hreporte = new HReporte_pago_fenalco(negs, usuario);
                    hreporte.start();
                }
                negs = null;//libera objeto
            } catch (Exception e) {
                msj = "No se pudo realizar el archivo de transferencias intente mas tarde";
                throw new ServletException(e.getMessage());
            }
            next = "/jsp/fenalco/negocios/resultado_tranferencias.jsp?msg=" + msj;
            if (option.equals("0205")) {
                try {
                    responseTrans();
                } catch (Exception ex) {
                    throw new ServletException(ex.getMessage());
                }
            }

        }
        
        if (this.redirect) {
            this.dispatchRequest(next);
        }
    }

    public void filtroProveedoresTransferencias(String negocio, String banco) throws Exception {
        ArrayList<Proveedor> lista = new ArrayList<Proveedor>();
        lista = model.Negociossvc.filtroProveedoresTransferencias(negocio, banco);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;

    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void cargarNegociosTrasferencia(String negocio, String banco) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        lista = model.Negociossvc.cargarNegocios(negocio, banco);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }

     private void cargarNegociosTrasferenciaLibranza(String negocio, String banco) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        lista = model.Negociossvc.cargarNegociosLibranza(negocio, banco);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }

    private void cargarNegociosCHequeLibranza(String doc_rel) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        lista = model.Negociossvc.cargarNegociosCHequeLibranza(doc_rel);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }
    
    private void perfeccionarNegocioLibranza(String negocio, String cxp, Usuario usuario) throws Exception {
        TransaccionService tService = new TransaccionService(this.usuario.getBd());
        tService.crearStatement();
        Gson gson = new Gson();
        String respuesta = "";
        tService.getSt().addBatch(model.Negociossvc.perfeccionarNegocioLibranza(negocio, usuario));
        tService.getSt().addBatch(model.Negociossvc.perfeccionarNegocioLI(negocio, usuario));
        tService.getSt().addBatch(model.Negociossvc.marcarCXPGeneradoCheque(cxp, negocio,usuario));
        //respuesta = "Guardado";
        tService.execute();
        respuesta = model.Negociossvc.CxCLibranza( negocio,usuario);
        String json = "{\"respuesta\":" + gson.toJson(respuesta) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }

    public void exportarPdf(String negocio, String cxp,String banco_transferencia, Usuario usuario,String conv) {
        String json = "";
        try {
            json = model.Negociossvc.exportarPdf(cxp, negocio, banco_transferencia, usuario,conv);
            this.printlnResponse(json, "application/json;");
            redirect = false;
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }
    
    private void cargarNegociosTrasferenciaProveedor(String[] nits, String fechaInicio, String fechaFin, String un_neg, String banco) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        lista = model.Negociossvc.cargarNegociosProveedor(nits, fechaInicio, fechaFin, un_neg, banco);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }

    private void cambiarComision() throws Exception {

        BeanGeneral bg = new BeanGeneral();
        bg.setValor_15("ok");

        Gson gson = new Gson();
        String json = gson.toJson(bg);
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }

    private void cargarComision() throws Exception {
        String comision = model.Negociossvc.cargarComision();

        this.printlnResponse(comision, "application/text;");
        redirect = false;
    }

    private void responseTrans() throws Exception {
        this.printlnResponse("OK", "application/text;");
        redirect = false;
    }

    public ArrayList generarDocumentosFenalco(Negocios negocioF, Convenio convenioF, TransaccionService tService, Usuario usuario) throws Exception {
        String tn = "";
        ArrayList querys = null;
        if (negocioF.getTneg().equals("01")) {
            tn = "CH";
        } else {
            if (negocioF.getTneg().equals("02")) {
                tn = "LT";
            } else {
                tn = "PG";
            }
        }
        NegociosGenService Negociossvc=new NegociosGenService(usuario.getBd());

        BeanGeneral bg = new BeanGeneral();
        bg.setValor_01(negocioF.getCod_cli()); //codcli - nit del cliente
        bg.setValor_02(negocioF.getNodocs() + ""); // numero de cuotas
        bg.setValor_03((negocioF.getTotpagado() / negocioF.getNodocs()) + ""); //Valor de las cuotas
        bg.setValor_04(usuario.getLogin()); //usuario en sesion
        bg.setValor_05(tn); //tipo de negocio: cheque, letra, etc
        bg.setValor_06(negocioF.getCod_negocio()); //codigo del negocio
        bg.setValor_07(usuario.getBase()); //base
        bg.setValor_08("RD-" + negocioF.getCod_cli());
        bg.setValor_09(negocioF.getNit_tercero()); //nit tercero (nit del afiliado)
        bg.setValor_10(negocioF.getTotpagado() + ""); //total pagado(valor del campo tot_pagado en la tabla negocios)
        bg.setValor_11(negocioF.getVr_desem() + ""); //valor del desembolso
        bg.setValor_12(negocioF.getFecha_neg()); //fecha del negocio
        bg.setValor_13(negocioF.getNumaval()); //numero de aval
        bg.setValor_15(negocioF.getvalor_aval()); //valor del aval
        bg.setValor_16(negocioF.getTneg()); //Codigo del titulo valor
        bg.setValor_17(negocioF.getId_convenio() + ""); //codigo del convenio
        bg.setValor_18(usuario.getDstrct()); //distrito del usuario en sesion
        bg.setValor_27(negocioF.getValor_capacitacion() + "");//valor capacitacion
        bg.setValor_28(negocioF.getValor_seguro() + "");//valor seguro
        bg.setValor_29(negocioF.getVr_negocio() + "");// valor negocio

        ArrayList detNegocio;
        if (negocioF.getTipo_liq().equals("REFINANCIACION")){
           detNegocio = Negociossvc.buscarDetallesNegocioRefi(bg.getValor_06(),negocioF.getTipo_liq()); 
        }else{
           detNegocio = Negociossvc.buscarDetallesNegocio(bg.getValor_06());
        }
        
        Negociossvc.setLiquidacion(detNegocio);
        querys = Negociossvc.generarDocumentosCXC(bg, usuario, negocioF, Negociossvc.getLiquidacion());

        return querys;
    }

    private void transferirNegocio(String codNeg, Usuario usuario, String codCxP, String comision, String nbanco, String sucursal, GestionSolicitudAvalService solcService) throws Exception {
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            Negocios negocioF = new Negocios();
            Convenio convenioF = new Convenio();

            // Metodo para crear la CXC Fenalco
            negocioF = model.Negociossvc.buscarNegocio(codNeg);
            convenioF = model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), String.valueOf(negocioF.getId_convenio()));

            if (convenioF.getTipo().equals("Consumo") && convenioF.isRedescuento() && convenioF.isAval_anombre()) {
                ArrayList listQuery = null;
                listQuery = generarDocumentosFenalco(negocioF, convenioF, tService, usuario);

                Iterator i = listQuery.iterator();
                while (i.hasNext()) {
                    String sql = (String) i.next();
                    System.out.println("Query CXC Fenalco: " + sql);
                    tService.getSt().addBatch(sql);
                }
            }
            //FIN

            tService.getSt().addBatch(model.Negociossvc.actcomString(codCxP, codNeg, comision, nbanco, sucursal));
                        //model.Negociossvc.actcom(vcodn[j],comisiones[Integer.parseInt(vno)],bn[0],bn[1]);

            /*/--INSERTA LA TRAZA DE LA ACTIVIDAD DE DESEMBOLSO--/*/
            //  if (model.Negociossvc.isCxpAfiliado(vcodn[j], vcxp[j])) {
            Date fecha_hoy = new Date();
            Timestamp t = new Timestamp(fecha_hoy.getTime());
            String fecha = t.toString();
            NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService(usuario.getBd());
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
            negtraza.setActividad("DES");
            negtraza.setNumeroSolicitud(solcService.buscarNumSolicitud(codNeg));
            negtraza.setUsuario(usuario.getLogin());
            negtraza.setCausal("");
            negtraza.setComentarios("");
            negtraza.setDstrct(usuario.getDstrct());
            negtraza.setConcepto("");
            negtraza.setCodNeg(codNeg);
            tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza).replace("now()", "'" + fecha + "'"));
                       // }

            // if (model.Negociossvc.isCxpAfiliado(vcodn[j], vcxp[j])) {
                            /*/--ACTUALIZANDO EL NEGOCIO = T (transferido)--/*/
            tService.getSt().addBatch(model.Negociossvc.uptranfString(codNeg));
            // }

            /*/--CREANDO EL EGRESO Y ACTUALIZANDO CXP--  /*/
            ArrayList listaQuerys = model.Negociossvc.saveEGDAOString(codCxP, codNeg, usuario, nbanco, sucursal, comision);
            // tService.getSt().addBatch(model.Negociossvc.saveEGDAOString(vcxp[j], vcodn[j], usuario, nbanco, bn[1], comisiones[Integer.parseInt(vno)]));
            Iterator i = listaQuerys.iterator();
            while (i.hasNext()) {
                String sql = (String) i.next();
                System.out.println(sql);
                tService.getSt().addBatch(sql);
            }

            tService.execute();
            
             //INICIO INSERTAR CXP
                switch (nbanco) {
                    case "CAJA SUPEREFECT":
                        model.Negociossvc.insert_cxp_banco_transf(codCxP,nbanco,(model.Negociossvc.getValor_cxp()),usuario);    
                        break;
                    case "EFECTY":
                        model.Negociossvc.insert_cxp_banco_transf(codCxP,nbanco,(model.Negociossvc.getValor_cxp()),usuario);    
                        break;    
                    default:
                        break;
                }
                //FIN INSERTAR CXP
                   
            
            
        } catch (SQLException ex) {
            Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void generarCxCAval(String negAval, Usuario usuario, String codCxPAval, String comision, String nbanco, String sucursal, GestionSolicitudAvalService solcService) throws Exception {
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            Negocios negocioF = new Negocios();
            Convenio convenioF = new Convenio();

            // Metodo para crear la CXC Fenalco
            negocioF = model.Negociossvc.buscarNegocio(negAval);
            convenioF = model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), String.valueOf(negocioF.getId_convenio()));

            ArrayList listQuery = null;
            listQuery = generarDocumentosFenalco(negocioF, convenioF, tService, usuario);

            Iterator i = listQuery.iterator();
            while (i.hasNext()) {
                String sql = (String) i.next();
                System.out.println("Query CXC Fenalco: " + sql);
                tService.getSt().addBatch(sql);
            }

            tService.execute();
        } catch (SQLException ex) {
            Logger.getLogger(NegociosTransfAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void generarEgresos(String codNeg, Usuario usuario, String codCxP, String comision, String nbanco, String sucursal, GestionSolicitudAvalService solcService) {
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            /*/--CREANDO EL EGRESO Y ACTUALIZANDO CXP--  /*/
            ArrayList listaQuerys = model.Negociossvc.saveEGDAOString(codCxP, codNeg, usuario, nbanco, sucursal, comision);

            Iterator i = listaQuerys.iterator();
            while (i.hasNext()) {
                String sql = (String) i.next();
                System.out.println(sql);
                tService.getSt().addBatch(sql);
            }
            if (model.Negociossvc.isNegocioAval(codNeg)) {
                tService.getSt().addBatch(model.Negociossvc.uptranfString(codNeg)); /*/--ACTUALIZANDO EL NEGOCIO = T (transferido)--/*/

            }
            tService.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            //xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    private boolean generaArchivoExcel(ArrayList<BeanGeneral> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            BeanGeneral neg = new BeanGeneral();
            Iterator it = lista.iterator();
            while (it.hasNext()) {
                neg = (BeanGeneral) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, neg.getValor_13(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_10(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_01(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_02(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_03(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_15(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_11(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_09(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_07(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_05(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_06(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_08(), letra);
                xls.adicionarCelda(fila, col++, neg.getValor_04(), letra);

            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generaArchivoExcel .... \n" + ex.getMessage());
        }
        return generado;
    }
    
    private void esCompraCartera(String negocio, Usuario usuario) throws Exception {
        String comision = model.Negociossvc.esCompraCartera(negocio, usuario);

        this.printlnResponse(comision, "application/text;");
        redirect = false;
    }
    
     private void perfeccionarNegocioMicro(String negocio, String cxp, Usuario usuario) throws Exception { 
         GestionSolicitudAvalService solcService = new GestionSolicitudAvalService(usuario.getBd());
         String respuesta="OK";
         Gson gson = new Gson();
        model.Negociossvc.uptdatefehadesem(negocio);
        Negocios neg = new Negocios();
        neg = model.Negociossvc.buscarNegocio(negocio);
        Convenio convenio = model.gestionConveniosSvc.buscar_convenio("fintra", neg.getId_convenio() + "");
       
       ArrayList detNegocio = model.Negociossvc.buscarDetallesNegocio(neg.getCod_negocio());
       model.Negociossvc.setLiquidacion(detNegocio);
       model.Negociossvc.generarCXCMicrocredito(neg, usuario, convenio);
     
        TransaccionService tService = new TransaccionService(this.usuario.getBd());
        tService.crearStatement();        
        tService.getSt().addBatch(model.Negociossvc.perfeccionarMicrocredito(negocio, usuario));
        tService.getSt().addBatch(model.Negociossvc.perfeccionarNegocioMicrocredito(negocio, usuario));
        tService.getSt().addBatch(model.Negociossvc.marcarCXPGeneradoChequeMicro(cxp, negocio,usuario));    
        tService.execute(); 
        
         if (neg.getPolitica().equals("R")){   
           SolicitudAval solicitud = solcService.buscarSolicitud(Integer.parseInt(solcService.buscarNumSolicitud(negocio)));
           model.Negociossvc.anular_documentos_micro_retanqueo(solicitud, usuario.getLogin());
        }
        
        String json = "{\"respuesta\":" + gson.toJson(respuesta) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }
     
     private void cargarNegociosArchivoPlano(String negocio, String banco) throws Exception {
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        lista = model.Negociossvc.cargarNegociosArchivoPlano(negocio,banco);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
        redirect = false;
    }

}
