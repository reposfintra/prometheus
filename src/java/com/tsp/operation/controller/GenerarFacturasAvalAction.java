package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.DAOS.Tipo_impuestoDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.ChequeXFacturaService;
import com.tsp.operation.model.services.GenerarFacturasAvalService;
import com.tsp.operation.model.services.GestionConveniosService;
import com.tsp.util.Util;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 * Controlador para el programa de generacion de facturas de aval
 * @author ivargas - geotech
 */
public class GenerarFacturasAvalAction extends Action {

    HttpSession session;
    Usuario usuario;
    String next = "";
    GenerarFacturasAvalService gfacserv;
    ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
    ArrayList<String> afiliados = new ArrayList<String>();
    int items;//los items del comprobante;
    List listitems = new LinkedList();

    @Override
    public void run() throws ServletException, InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            gfacserv = new GenerarFacturasAvalService(usuario.getBd());
            String opcion = request.getParameter("opcion");
            boolean redirect = true;
            String strRespuesta = "";

            if (opcion.equals("CARGAR_CONVENIOS_ANOMBRE")) {
                strRespuesta = cargarConveniosAnombre();
                redirect = false;
            } else if (opcion.equals("CARGAR_AFILIADOS")) {
                strRespuesta = cargarAfiliados();
                redirect = false;
            } else if (opcion.equals("CARGAR_NEGOCIOS")) {
                strRespuesta = cargarNegocios();
                redirect = false;
            } else if (opcion.equals("GENERAR_CXC")) {
                strRespuesta = generarCXC();
                redirect = false;
            } else if (opcion.equals("CARGAR_CONVENIOS_AVALISTA")) {
                strRespuesta = cargarConveniosAvalista();
                redirect = false;
            }else if (opcion.equals("GENERAR_CXP")) {
                strRespuesta = generarCXP();
                redirect = false;
            }

            if (redirect) {
                this.dispatchRequest(next);
            } else {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-store");
                response.setDateHeader("Expires", 0);
                response.getWriter().print(strRespuesta);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error en GenerarFacaturasAvalAction: " + e.getMessage());
        }

    }

    /**
     * visualiza el listado de afiliados segun convenio en un select
     */
    public String cargarAfiliados() {
        String cadenaWrite = "";
        int id_convenio = request.getParameter("id_convenio") != null ? Integer.parseInt(request.getParameter("id_convenio")) : 0;

        ArrayList lista = null;
        try {
            lista = gfacserv.buscarAfiliadosConvenio(id_convenio);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<select id='afiliado' name='afiliado' style='width:20em;' onchange='limpiartabla();'>";

        cadenaWrite += "<option value=''>...</option>";

        String[] dato1 = null;
        for (int i = 0; i < lista.size(); i++) {
            dato1 = ((String) lista.get(i)).split(";_;");
            cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
        }

        cadenaWrite += "</select>";
        return cadenaWrite;

    }

    /**
     * visualiza el listado de convenios segun anombre en un select
     */
    public String cargarConveniosAvalista() {
        String cadenaWrite = "";
        String avalista = request.getParameter("avalista") != null ? request.getParameter("avalista") : "";

        ArrayList lista = null;
        try {
            lista = gfacserv.buscarConveniosAvalista(avalista);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<select id='convenio' name='convenio' style='width:20em;' >";
        cadenaWrite += "<option value=''>...</option>";

        String[] dato1 = null;
        for (int i = 0; i < lista.size(); i++) {
            dato1 = ((String) lista.get(i)).split(";_;");
            cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
        }

        cadenaWrite += "</select>";
        return cadenaWrite;

    }

    /**
     * visualiza el listado de convenios segun anombre en un select
     */
    public String cargarConveniosAnombre() {
        String cadenaWrite = "";
        boolean anombre = request.getParameter("anombre") != null ? Boolean.parseBoolean(request.getParameter("anombre")) : false;

        ArrayList lista = null;
        try {
            lista = gfacserv.buscarConveniosAnombre(anombre);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<select id='convenio' name='convenio' style='width:20em;' onchange='cargarAfiliados(this.selectedIndex);'>";
        cadenaWrite += "<option value=''>...</option>";

        String[] dato1 = null;
        for (int i = 0; i < lista.size(); i++) {
            dato1 = ((String) lista.get(i)).split(";_;");
            cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
        }

        cadenaWrite += "</select>";
        return cadenaWrite;

    }

    /* visualiza el listado de negocios en una tabla */
    public String cargarNegocios() {
        String cadenaWrite = "";
        int id_convenio = request.getParameter("id_convenio") != null && !request.getParameter("id_convenio").equals("") ? Integer.parseInt(request.getParameter("id_convenio")) : 0;
        String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
        String fechaini = request.getParameter("fechaini") != null ? request.getParameter("fechaini") : "";
        String fechafin = request.getParameter("fechafin") != null ? request.getParameter("fechafin") : "";

        if (vista.equals("1")) {
            boolean anombre = request.getParameter("anombre") != null ? Boolean.parseBoolean(request.getParameter("anombre")) : false;
            String afiliado = request.getParameter("afiliado") != null ? request.getParameter("afiliado") : "";
            afiliados = new ArrayList<String>();
            try {
                lista = gfacserv.buscarNegocios(fechaini, fechafin, id_convenio, anombre, afiliado);
                if (afiliado.equals("")) {
                    afiliados = gfacserv.buscarAfiliados(fechaini, fechafin, id_convenio);
                } else {
                    afiliados.add(afiliado);
                }

            } catch (Exception e) {
                System.out.println("error(action): " + e.toString());
                e.printStackTrace();
            }

            cadenaWrite = " <table width='100%' border='1' align='center' >";
            cadenaWrite += "            <tr>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Convenio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Nit afiliado</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Afiliado</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Negocio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Fecha Negocio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Valor Negocio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Valor Aval</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Cedula Cliente</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Cliente</th>";
            cadenaWrite += "              </tr>";

            cadenaWrite += "    <tbody>";

            if (lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    cadenaWrite += "                  <tr class='" + (i % 2 == 0 ? "filagris" : "filaazul") + "'>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_01() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_02() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_03() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_04() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_05() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_06() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_07() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_08() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_09() + "</td>";
                    cadenaWrite += "              </tr>";
                }

            } else {
                cadenaWrite += "              <tr class='filagris'>";
                cadenaWrite += "                  <td class='bordereporte' colspan='9'  align='center' >No se encontraron Registros</td>";
                cadenaWrite += "              </tr>";

            }
            cadenaWrite += "         </tbody>";
            cadenaWrite += "   </table>";
            if (lista.size() > 0) {
                cadenaWrite += "  <br/>";
                cadenaWrite += " <img style='cursor:pointer' src='" + request.getContextPath() + "/images/botones/generar.gif' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' onClick='generar()'/>";
                if (anombre) {
                    cadenaWrite += " <img style='cursor:pointer' src='" + request.getContextPath() + "/images/botones/exportarExcel.gif' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' onClick='exportar()'/>";

                }
            }
        } else if(vista.equals("2")) {
            try {
                lista = gfacserv.buscarNegocios(fechaini, fechafin, id_convenio);

            } catch (Exception e) {
                System.out.println("error(action): " + e.toString());
                e.printStackTrace();
            }

            cadenaWrite = " <table width='100%' border='1' align='center' >";
            cadenaWrite += "            <tr>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Convenio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Nit avalista</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Avalista</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Negocio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Fecha Negocio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Valor Negocio</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Valor Aval</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Cedula Cliente</th>";
            cadenaWrite += "               <th class='subtitulo1' align='center' >Cliente</th>";
            cadenaWrite += "              </tr>";

            cadenaWrite += "    <tbody>";

            if (lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    cadenaWrite += "                  <tr class='" + (i % 2 == 0 ? "filagris" : "filaazul") + "'>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_01() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_02() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_03() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_04() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_05() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_06() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_07() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_08() + "</td>";
                    cadenaWrite += "                  <td class='bordereporte' align='center' >" + lista.get(i).getValor_09() + "</td>";
                    cadenaWrite += "              </tr>";
                }

            } else {
                cadenaWrite += "              <tr class='filagris'>";
                cadenaWrite += "                  <td class='bordereporte' colspan='9'  align='center' >No se encontraron Registros</td>";
                cadenaWrite += "              </tr>";

            }
            cadenaWrite += "         </tbody>";
            cadenaWrite += "   </table>";
            if (lista.size() > 0) {
                cadenaWrite += "  <br/>";
                cadenaWrite += " <img style='cursor:pointer' src='" + request.getContextPath() + "/images/botones/generar.gif' onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' onClick='generar()'/>";

            }
        }
        return cadenaWrite;

    }

    public String generarCXC() throws Exception {
        String cadenaWrite = "";
        int id_convenio = request.getParameter("id_convenio") != null && !request.getParameter("id_convenio").equals("") ? Integer.parseInt(request.getParameter("id_convenio")) : 0;        
        String fecha_actual = Util.getFechaActual_String(4);
        ChequeXFacturaService md = new ChequeXFacturaService(usuario.getBd());
        String monedaLocal = md.getMonedaLocal(usuario.getDstrct());
        TransaccionService tService = new TransaccionService(usuario.getBd());
        tService.crearStatement();
        GestionConveniosService convserv = new GestionConveniosService(usuario.getBd());
        Convenio convenio = convserv.buscar_convenio(usuario.getBd(), String.valueOf(id_convenio));
        BeanGeneral datos = new BeanGeneral();
        datos.setValor_01(convenio.getPrefijo_cxc_aval());
        Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(usuario.getBd());
        Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(usuario.getDstrct(), convenio.getImpuesto(), fecha_actual);
        String error = "";
        for (int i = 0; i < lista.size(); i++) {
            listitems = new LinkedList();
            items = 0;
            String periodo = fecha_actual.substring(0, 7).replaceAll("-", "");
            Comprobantes comprobante = (Comprobantes) gfacserv.buscarNegocio(lista.get(i).getValor_04(), usuario.getDstrct());
            comprobante.setAbc("N/A");
            comprobante.setBase(usuario.getBase());
            comprobante.setDocrelacionado(comprobante.getNumdoc());
            comprobante.setTdoc_rel("NEG");
            comprobante.setTipodoc("NEG");
            comprobante.setMoneda(monedaLocal);
            comprobante.setSucursal("BQ");
            comprobante.setFechadoc(fecha_actual);//fecha del comprobante
            comprobante.setPeriodo(periodo);//periodo del comprobante preguntar que fecha debe quedar el comprobante
            comprobante.setTipo_operacion("001");
            comprobante.setUsuario(usuario.getLogin());
            comprobante.setAprobador(usuario.getLogin());
            comprobante.setTipo("C");
            comprobante.setTercero(comprobante.getRef_1());
            //DEBITO
            double iva = (Double.parseDouble(lista.get(i).getValor_07()) * impuesto.getPorcentaje1()) / (100 + impuesto.getPorcentaje1());
            double ingreso = Double.parseDouble(lista.get(i).getValor_07()) - iva;
            itemscopy(comprobante, convenio.getCctrl_cr_cxc_aval(), ingreso, "D");
            itemscopy(comprobante, convenio.getCctrl_iva_cxc_aval(), iva, "D");
            //CREDITO
            itemscopy(comprobante, convenio.getCctrl_db_cxc_aval(), Double.parseDouble(lista.get(i).getValor_07()), "C");
            comprobante.setTotal_credito(Double.parseDouble(lista.get(i).getValor_07()));
            comprobante.setTotal_debito(Double.parseDouble(lista.get(i).getValor_07()));
            comprobante.setTotal_items(items);
            comprobante.setItems(listitems);
            error = gfacserv.insertar(comprobante, usuario.getLogin());
            if (!error.substring(0, 5).equals("Error")) {
                tService.getSt().addBatch(error);
                error = "";
            } else {
                i = lista.size();
            }
        }

        if (error.equals("")) {
            String documento = "";
            datos.setValor_03(fecha_actual);
            datos.setValor_04(convenio.getHc_cxc_aval());
            datos.setValor_05(usuario.getDstrct());
            datos.setValor_08(usuario.getLogin());
            String des_cuenta = gfacserv.getAccountDES(convenio.getCuenta_cxc_aval());
            String des_cuenta_imp = gfacserv.getAccountDES(impuesto.getCod_cuenta_contable());
            if (convenio.isAval_anombre()) {
                double total_comision = 0;
                for (int i = 0; i < lista.size(); i++) {
                    total_comision += Double.parseDouble(lista.get(i).getValor_07());
                }
                documento = model.Negociossvc.UpCP(datos.getValor_01());
                datos.setValor_02(convenio.getNit_anombre());
                datos.setValor_06(total_comision + "");
                datos.setValor_07(documento);
                tService.getSt().addBatch(gfacserv.ingresarCXCComisionAval(datos));

                for (int i = 0; i < lista.size(); i++) {
                    double iva = (Double.parseDouble(lista.get(i).getValor_07()) * impuesto.getPorcentaje1()) / (100 + impuesto.getPorcentaje1());
                    double ingreso = Double.parseDouble(lista.get(i).getValor_07()) - iva;
                    datos.setValor_11(lista.get(i).getValor_04());
                    datos.setValor_06(ingreso + "");
                    datos.setValor_09(convenio.getCuenta_cxc_aval());
                    datos.setValor_10(des_cuenta);
                    tService.getSt().addBatch(gfacserv.ingresarDetalleCXCComisionAval(datos, "1"));
                    datos.setValor_06(iva + "");
                    datos.setValor_09(impuesto.getCod_cuenta_contable());
                    datos.setValor_10(des_cuenta_imp);
                    tService.getSt().addBatch(gfacserv.ingresarDetalleCXCComisionAval(datos, "2"));
                }
                tService.execute();
                cadenaWrite = "Generacion de CXC de  Aval Exitosa con " + lista.size() + " negocios";

            } else {
                for (int i = 0; i < afiliados.size(); i++) {
                    double comision = 0;
                    for (int j = 0; j < lista.size(); j++) {
                        if (afiliados.get(i).equals(lista.get(j).getValor_02())) {
                            comision += Double.parseDouble(lista.get(j).getValor_07());
                        }
                    }
                    documento = model.Negociossvc.UpCP(datos.getValor_01());
                    datos.setValor_02(afiliados.get(i));
                    datos.setValor_06(comision + "");
                    datos.setValor_07(documento);
                    tService.getSt().addBatch(gfacserv.ingresarCXCComisionAval(datos));
                    for (int j = 0; j < lista.size(); j++) {
                        if (afiliados.get(i).equals(lista.get(j).getValor_02())) {
                            double iva = (Double.parseDouble(lista.get(j).getValor_07()) * impuesto.getPorcentaje1()) / (100 + impuesto.getPorcentaje1());
                            double ingreso = Double.parseDouble(lista.get(j).getValor_07()) - iva;
                            datos.setValor_11(lista.get(j).getValor_04());
                            datos.setValor_06(ingreso + "");
                            datos.setValor_09(convenio.getCuenta_cxc_aval());
                            datos.setValor_10(des_cuenta);
                            tService.getSt().addBatch(gfacserv.ingresarDetalleCXCComisionAval(datos, "1"));
                            datos.setValor_06(iva + "");
                            datos.setValor_09(impuesto.getCod_cuenta_contable());
                            datos.setValor_10(des_cuenta_imp);
                            tService.getSt().addBatch(gfacserv.ingresarDetalleCXCComisionAval(datos, "2"));
                        }
                    }
                }
                tService.execute();
                cadenaWrite = "Generacion de " + afiliados.size() + " CXC de  Aval Exitosa(s)";
            }


        } else {
            cadenaWrite = error;
        }

        return cadenaWrite;

    }

     public String generarCXP() throws Exception {
        String cadenaWrite = "";
        int id_convenio = request.getParameter("id_convenio") != null && !request.getParameter("id_convenio").equals("") ? Integer.parseInt(request.getParameter("id_convenio")) : 0;
   
        TransaccionService tService = new TransaccionService(usuario.getBd());
        tService.crearStatement();
        GestionConveniosService convserv = new GestionConveniosService(usuario.getBd());
        Convenio convenio = convserv.buscar_convenio(usuario.getBd(), String.valueOf(id_convenio));
        BeanGeneral datos = new BeanGeneral();
        String documento = "";
        double total_comision = 0;
                for (int i = 0; i < lista.size(); i++) {
                    total_comision += Double.parseDouble(lista.get(i).getValor_07());
                }
        datos.setValor_01(convenio.getPrefijo_cxp_avalista());
        documento = model.Negociossvc.UpCP(datos.getValor_01());
        datos.setValor_02(convenio.getNit_convenio());
        datos.setValor_03(usuario.getLogin());
        datos.setValor_04(documento);
        datos.setValor_05(usuario.getBase());
        datos.setValor_06(convenio.getHc_cxp_avalista());
        datos.setValor_07(convenio.getCuenta_cxp_avalista());
        datos.setValor_08(usuario.getDstrct());
        datos.setValor_09(total_comision+"");

        tService.getSt().addBatch(gfacserv.ingresarCXPAvalista(datos));
   
        for (int i = 0; i < lista.size(); i++) {
                datos.setValor_09(lista.get(i).getValor_07());
                datos.setValor_10(lista.get(i).getValor_04());
                tService.getSt().addBatch(gfacserv.ingresarDetalleCXPAvalista(datos));
        }

         tService.execute();
         cadenaWrite = "Generacion de 1 CXP al avalista exitosa con "+lista.size()+" negocios.";

        return cadenaWrite;

    }

    public void itemscopy(Comprobantes neg, String parm, double parm2, String tipo) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getTercero());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getDocrelacionado());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setTdoc_rel(neg.getTdoc_rel());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(gfacserv.getAccountDES(parm));
            if (tipo.equals("D")) {
                item.setTotal_credito(0.0);
                item.setTotal_debito(parm2);
            } else {
                item.setTotal_credito(parm2);
                item.setTotal_debito(0.0);
            }
            item.setAuxiliar("");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }
}
