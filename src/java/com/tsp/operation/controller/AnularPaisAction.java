/*
 * AnularPaisAction.java
 *
 * Created on 1 de marzo de 2005, 10:01 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class AnularPaisAction extends Action{
    
    /** Creates a new instance of AnularPaisAction */
    public AnularPaisAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        String codigo = (request.getParameter("c_codigo"));
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
       // String lenguaje = (String) session.getAttribute("idioma");
     
        try{
        //    model.idiomaService.cargarIdioma(lenguaje, "MsgAnulado.jsp");
            
            Pais pais = new Pais();
            pais.setCountry_code(codigo);
            pais.setUser_update(usuario.getLogin());
            model.paisservice.anularpais(pais); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
    
}
