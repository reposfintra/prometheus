/******************************************************************
* Nombre ......................SeriesVDetalleAction.java
* Descripci�n..................Clase Action para vigilante series
* Autor........................Armando Oviedo
* Fecha........................10/10/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class SeriesVDetalleAction extends Action{
    
    /** Creates a new instance of SeriesVDetalleAction */
    public SeriesVDetalleAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/masivo/series/series_centinela/DetalleSeriesGrupo.jsp";
        String posicion = request.getParameter("posicion");
        next +="?posicion="+posicion;
        this.dispatchRequest(next);
    }
    
}
