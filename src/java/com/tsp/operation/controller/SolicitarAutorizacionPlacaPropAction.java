/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class SolicitarAutorizacionPlacaPropAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public SolicitarAutorizacionPlacaPropAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next ="/jsp/hvida/identidad/identidad.jsp?mensajeAut=Su solicitud ha sido enviada exitosamente.";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String base =usuario.getBase();
        String numsol = "000";
        String email = usuario.getEmail();
        try{
            numsol = model.afleteService.getSolicitud();
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        Email e = new Email();
        e.setEmailfrom("autorizaciones@sanchezpolo.com");
        e.setEmailsubject("SOLICITUD DE AUTORIZACION");
        e.setSenderName("SISTEMA MASIVO");
        e.setEmailto("apolifroni@sanchezpolo.com;kreales@sanchezpolo.com");
        e.setEmailcopyto("");
        
        if(request.getParameter("cmd").equals("PROP")){
            
            ////System.out.println("Autorizacion para crear propietario");
            String nit = request.getParameter("nit");
            String nombre = request.getParameter("nombre");
            
            
            //LLAMO EL HILO QUE ENVIA EMAILS
            
            e.setEmailbody("SOLICITUD DE AUTORIZACION PARA LA CREACION DE PROPIETARIOS\n" +
            "\nNIT    : "+nit+"" +
            "\nNOMBRE : "+nombre+"" +
            "\nDESPACHADOR : "+usuario.getLogin()+"" +
            "\nSOLICITUD NUMERO : " +numsol+
            "\n\n\nHAGA CLICK AQUI PARA AUTORIZAR EL CAMBIO http://tspweb.tsp.com:8080/slt2/jsp/hvida/identidad/identidadAutorizacion.jsp?nit="+nit+"&nombre="+nombre.replaceAll(" ","%20")+"&despachador="+usuario.getLogin()+"&numsol="+numsol+"&email="+email);
            
            
            
        }
        if(request.getParameter("cmd").equals("PLACA")){
            String placa = request.getParameter("placa");
            String nuevoProp = request.getParameter("propietario");
            String vprop = request.getParameter("vpropietario");
            String nombreNProp ="No se encontro el nombre";
            String nombreVProp ="No se encontro el nombre";
            try{
                nombreNProp= model.nitService.obtenerNombre(nuevoProp);
                nombreVProp= model.nitService.obtenerNombre(vprop);
            }catch (SQLException ex){
                throw new ServletException(ex.getMessage());
            }
            e.setEmailbody("SOLICITUD DE AUTORIZACION PARA LA MODIFICACION DE PLACA - PROPIETARIO\n" +
            "\nPLACA    : "+placa+"" +
            "\nNUEVO PROPIETARIO : "+nuevoProp+" -  " + nombreNProp+
            "\nPROPIETARIO ACTUAL : "+vprop+" -  " + nombreVProp+
            "\nDESPACHADOR : "+usuario.getLogin()+"" +
            "\nSOLICITUD NUMERO : " +numsol+
            "\n\n\nHAGA CLICK AQUI PARA AUTORIZAR EL CAMBIO http://tspweb.tsp.com:8080/slt2/placas/placaAutorizacion.jsp?placa="+placa+"&nombreNProp="+nombreNProp.replaceAll(" ","%20")+"&NProp="+nuevoProp+"&VProp="+vprop+"&nombreVProp="+nombreVProp.replaceAll(" ","%20")+"&despachador="+usuario.getLogin()+"&numsol="+numsol+"&email="+email);
            
            next ="/placas/placaUpdate.jsp?mensajeAut=Su solicitud ha sido enviada exitosamente.";
            
        }
        
        if(request.getParameter("cmd").equals("PLACAINSERT")){
            String placa = request.getParameter("placa");
            String nuevoProp = request.getParameter("propietario");
            String nombreNProp ="No se encontro el nombre";
            try{
                nombreNProp= model.nitService.obtenerNombre(nuevoProp);
            }catch (SQLException ex){
                throw new ServletException(ex.getMessage());
            }
            e.setEmailbody("SOLICITUD DE AUTORIZACION PARA LA CREACION DE PLACA\n" +
            "\nPLACA    : "+placa+"" +
            "\nPROPIETARIO : "+nuevoProp+" -  " + nombreNProp+
            "\nDESPACHADOR : "+usuario.getLogin()+"" +
            "\nSOLICITUD NUMERO : " +numsol+
            "\n\n\nHAGA CLICK AQUI PARA AUTORIZAR EL CAMBIO http://tspweb.tsp.com:8080/slt2/placas/placaAutorizacionInsert.jsp?placa="+placa+"&nombreNProp="+nombreNProp.replaceAll(" ","%20")+"&NProp="+nuevoProp+"&despachador="+usuario.getLogin()+"&numsol="+numsol+"&email="+email);
            
            next ="/placas/placaInsert.jsp?mensajeAut=Su solicitud ha sido enviada exitosamente.";
            
        }
        ////System.out.println("Voy a correr el hilo de sendmail");
        com.tsp.operation.model.threads.SendMail s = new com.tsp.operation.model.threads.SendMail();
        s.start(e);
        
        this.dispatchRequest(next);
        
        
    }
    
    
}
