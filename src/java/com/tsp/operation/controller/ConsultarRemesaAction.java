/*
 * ConsultarRemesaAction.java
 *
 * Created on 5 de enero de 2005, 02:10 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ConsultarRemesaAction extends Action{
    
    /** Creates a new instance of ConsultarRemesaAction */
    public ConsultarRemesaAction() {
    }
    public synchronized void run() throws ServletException,InformationException {
        String next="/consultas/consultasRedirect.jsp?remesa=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
           
            String fechaini = request.getParameter("fechaini");
            String fechafin = request.getParameter("fechafin");
            if(fechaini.equals("")|| fechafin.equals("")){
                fechaini = "0099-01-01";
                fechafin = "now()";
            }
            String remision = request.getParameter("remesa").toUpperCase();
            String anulada  = "";
            String feccum ="";
            String sj = request.getParameter("sj");
            String nomcli = request.getParameter("cliente").toUpperCase();
            String faccial = request.getParameter("docint");
            String doci = request.getParameter("faccial");
            //Henry 19/01/06
            String tipo_doc = request.getParameter("tipo_doc")!=null?request.getParameter("tipo_doc"):"";
            String document =request.getParameter("documento")!=null?request.getParameter("documento").toUpperCase():"";
            if(request.getParameter("Anulada")!=null)
                anulada = "A";
            if (request.getParameter("cumplida")!=null)
                feccum = "20";
            
            model.remesaService.consultaRemesa(fechafin, fechaini, sj, anulada, feccum, nomcli,usuario.getBase(),remision,tipo_doc,document);
            
            
            if(request.getParameter("remision")!=null){
                 next="/consultas/consultasRedirect.jsp?remision=ok";
             }
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
