/*******************************************************************
 * Nombre clase: ConsultaRemesaPagadorAction.java
 * Descripci�n: Accion para buscar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 26 de enero de 2007, 11:42 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class ConsultaRemesaPagadorAction extends Action{
    
    /** Creates a new instance of ConsultaRemesaPagadorAction */
    public ConsultaRemesaPagadorAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next = "/jsp/general/consultas/BuscarDatosRemesa.jsp";
        
        String cliente = request.getParameter ("cliente")!=null?request.getParameter ("cliente"):"";
        String inicio = request.getParameter ("fechaInicio")!=null?request.getParameter ("fechaInicio"):"";
        String fin = request.getParameter ("fechaFinal")!=null?request.getParameter ("fechaFinal"):"";
        String cumplido = request.getParameter ("cumplida")!=null?request.getParameter ("cumplida"):"";
        
        HttpSession session = request.getSession ();
        String distrito = (String)(session.getAttribute ("Distrito"));
        String listar = (String) request.getParameter ("listar");
        
        try{
            if (listar.equals ("True")){
                if( model.clienteService.existeCliente(cliente) ){
                    model.remesaService.consultaRemesasPagador(inicio, fin, cliente, cumplido, distrito);
                    if( model.remesaService.getRemesas()!=null && model.remesaService.getRemesas().size() == 0 ){
                        next += "?msg=Su b�queda no arroj� resultados!";
                        model.remesaService.setRemesas( null );
                    }
                    else{
                        session.setAttribute ("inicio", inicio);
                        session.setAttribute ("fin", fin);
                        session.setAttribute ("cliente", cliente);
                        session.setAttribute ("cumplido", cumplido);
                    }
                }
                else
                    next += "?msg=no existe el cliente";
            }
            else if (listar.equals ("Back")){
                model.remesaService.setRemesas( null );
                session.setAttribute ("inicio", null);
                session.setAttribute ("fin", null);
                session.setAttribute ("cliente", null);
                session.setAttribute ("cliente", null);
                next += "?msg=";
            }
            else{
                String []textbox = request.getParameterValues ("checkbox");
                String pagador = request.getParameter ("pagador")!=null?request.getParameter ("pagador"):"";
                if( model.clienteService.existeCliente(pagador) ){
                    Vector consulta = new Vector();
                    if( textbox!=null ){
                        for(int j=0; j<textbox.length; j++){
                            Remesa rem = (Remesa) model.remesaService.getRemesas().get ( Integer.parseInt ( textbox[j] ) );
                            consulta.add( model.remesaService.updateCod_pagador( rem.getNumrem(), pagador ) );
                        }
                        next = "/jsp/general/consultas/BuscarDatosRemesa.jsp?msg=Se grabaron los datos exitosamnete";
                    }
                    else{
                        next = "/jsp/general/consultas/BuscarDatosRemesa.jsp?msg=Debe seleccionar por lo menos una remesa";
                    }
                    model.despachoService.insertar ( consulta );
                    inicio = (String)session.getAttribute ("inicio");
                    fin = (String)session.getAttribute ("fin");
                    cliente = (String)session.getAttribute ("cliente");
                    cumplido = (String)session.getAttribute ("cumplido");
                    model.remesaService.consultaRemesasPagador(inicio, fin, cliente, cumplido, distrito);
                }
                else
                    next += "?msg=no existe el pagador";
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);  
        
    }
    
}
