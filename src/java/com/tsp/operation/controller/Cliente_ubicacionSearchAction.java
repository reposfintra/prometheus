/********************************************************************
 * Nombre clase: Cliente_ubicacionSearchAction.java
 * Descripci�n: Accion para buscar un cliente_ubicacion a la bd.
 * Autor: Jose de la rosa
 * Fecha: 20 de enero de 2006, 09:45 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class Cliente_ubicacionSearchAction extends Action{
    
    /** Creates a new instance of Cliente_ubicacionSearchAction */
    public Cliente_ubicacionSearchAction () {
    }
    public void run() throws ServletException, InformationException{
        String next="";
        String cedula = "";
        String ubicacion = "";
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute ("Distrito");
        String listar = (String) request.getParameter("listar");
        try{
            if (listar.equals("True")){
                next="/jsp/trafico/cliente_ubicacion/Cliente_ubicacionListar.jsp";
                String sw = (String) request.getParameter("sw");
                if(sw.equals("True")){
                    cedula = (String) request.getParameter("c_cliente").toUpperCase();
                    ubicacion = (String) request.getParameter("c_ubicacion");
                }
                else{
                    cedula = "";
                    ubicacion = "";
                }
                session.setAttribute("cliente", cedula);
                session.setAttribute("ubicacion", ubicacion);
            }
            else{
                cedula = (String) request.getParameter("c_cliente").toUpperCase();
                ubicacion = (String) request.getParameter("c_ubicacion").toUpperCase();
                model.cliente_ubicacionService.searchCliente_ubicacion(cedula,ubicacion);
                model.clienteService.datosCliente (cedula);
                model.ubService.buscarUbicacion (ubicacion,distrito);
                next="/jsp/trafico/cliente_ubicacion/Cliente_ubicacionModificar.jsp";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }    
    
}
