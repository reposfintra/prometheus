package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;//20100722
public class MensajeSistemaAction extends Action {
    public MensajeSistemaAction() {    }
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String op=request.getParameter("opcion");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        //System.out.println("MensajeSistemaAction__opcion:"+op);
        if (op!=null && op.equals("horaserver")){//si piden la hora se da
            try {
                String msjHoraServer = Util.getFechaActual_String(10);
                response.setContentType("text/plain; charset=utf-8");//
                response.setHeader("Cache-Control", "no-cache");//
                //System.out.println("msjHoraServer");
                response.getWriter().println(msjHoraServer);
                //System.out.println("msjHoraServer2");
            }
            catch (Exception ex) {
                System.out.println("error en MensajeSistemaAction1:"+ex.toString()+"__"+ex.getMessage());
                ex.printStackTrace();
            }
        }else{
            if (op!=null && op.equals("variablesbd")){//si piden datos de bd se dan
                try {
                    String msjVariablesBd =model.mensajeSistemaService.getVariablesBd(usuario.getLogin());
                    response.setContentType("text/plain; charset=utf-8");//
                    response.setHeader("Cache-Control", "no-cache");//
                    response.getWriter().println(msjVariablesBd);
                }
                catch (Exception ex) {
                    System.out.println("error en MensajeSistemaAction2:"+ex.toString()+"__"+ex.getMessage());
                    ex.printStackTrace();
                }
            }else{
                String next = "";
                String msg="";
                String swActualizar=request.getParameter("swActualizar");
                if (swActualizar==null){swActualizar="";}
                try{
                    if (swActualizar.equals("si")){
                        msg=model.mensajeSistemaService.actualizarMensajeSistema(  );
                    }else{
                        msg=model.mensajeSistemaService.consultarMensajeSistema(  );
                    }
                    next = "/resultadoAjax.jsp?msg="+msg;
                }catch( Exception e ){
                    e.printStackTrace();
                    throw new ServletException(e.getMessage());
                }
                this.dispatchRequest(next);
            }
      }
    }
}