/*
 * Nombre        BuscarCiudadnomAction.java
 * Autor         Ing. Diogenes Bastidas
 * Fecha         1 de abril de 2005, 02:58 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  DIBASMO
 */
public class BuscarCiudadnomAction extends Action{
    
    /** Creates a new instance of BuscarCiudadnomAction */
    public BuscarCiudadnomAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String nompais = request.getParameter("nompais").toUpperCase();
        String nomestado = request.getParameter("nomestado").toUpperCase();
        String nomciudad = request.getParameter("nomciudad").toUpperCase(); 
        try{
            HttpSession session = request.getSession();
            model.ciudadservice.buscarCiudadesXnom(nompais+"%" , nomestado+"%" ,nomciudad+"%" );
        }catch (Exception e ){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }         
        this.dispatchRequest(next);
    }
    
}
