/*******************************************************************
 * Nombre clase: PlacaBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  jdelarosa
 */
public class ClienteAnularAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ClienteAnularAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/sot/body/BusquedaCliente.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String Codigo = request.getParameter("Codigo")!=null?request.getParameter("Codigo"):"";
        //System.out.println("Codigo=  "+Codigo);
        Vector vector = new Vector();
        try{
            /* jose 2007-05-04 */
            Identidad ident = new Identidad(); 
            ident.setCedula     ( ( request.getParameter("CedCliente")!=null)?request.getParameter("CedCliente"):"" );            
            ident.setUsuario    ( usuario.getLogin() );
            if ( model.identidadService.existeIdentidad( ident.getCedula() ) )
                model.identidadService.anularIdentidad(ident);
            /* jose 2007-05-04 */
            model.clienteService.eliminarCliente(Codigo);
            next+="?msg=El Cliente a sido Eliminado Satisfactoriamente";
            
        }catch (Exception e){
            e.printStackTrace();
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}
/********************************************************
 * Entregado a Fily 12 Febrero 2007
 *********************************************************/
