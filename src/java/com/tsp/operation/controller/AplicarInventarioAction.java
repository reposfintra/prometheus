/************************************************************************
 * Nombre clase: AplicarInventarioAction.java                           *
 * Descripci�n: Clase para la aplicacion de inventarios en el           *
 *              despacho.                                               *
 * Autor: Ing. Karen Reales                                             *
 * Fecha: Created on 01 de marzo de 2006, 08:40 AM                      *
 * Versi�n: Java 1.0                                                    *
 * Copyright: Fintravalores S.A. S.A.                              *
 ***********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;
/**
 *
 * @author Karen Reales
 */
public class AplicarInventarioAction extends Action{
    
    static Logger logger = Logger.getLogger(AplicarCaravanaAction.class);
    
    /** Creates a new instance of InsertRepMovTraficoAction */
    public AplicarInventarioAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/colpapel/inventarios.jsp" ;
        String cmd = request.getParameter("cmd");
        
        try{
            if(cmd.equals("buscar")){
                model.productoService.listarProductos(request.getParameter("cliente"), request.getParameter("ubicacion"));
                if("ok".equals(request.getParameter("modif"))){
                    model.productoService.listarTodosProductos(request.getParameter("cliente"), request.getParameter("ubicacion"),request.getParameter("numpla"));
                }
            }
            
            
        }catch(SQLException ex){
            throw new ServletException(ex.getMessage());
            
        }
        this.dispatchRequest(next);
    }
}
