/*
 * AutoAnticipoUpdateAction.java
 *
 * Created on 18 de junio de 2005, 04:01 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class AutoAnticipoUpdateAction extends Action {
    
    /** Creates a new instance of AutoAnticipoUpdateAction */
    public AutoAnticipoUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/anticipo_placas/anticipopUpdate.jsp";
        
        String placa = request.getParameter("pl").toUpperCase();
        
        double efectivo = 0;
        double acpm = 0;
        int ticket_a = 0;
        int ticket_b = 0;
        int ticket_c = 0;               
        
        if (!request.getParameter("ef").equals("")) {
            efectivo = Double.parseDouble(request.getParameter("ef"));
        }
        if (!request.getParameter("acpm").equals("")){ 
             acpm = Double.parseDouble(request.getParameter("acpm"));
        }
        if (!request.getParameter("ta").equals("")) {
            ticket_a = Integer.parseInt(request.getParameter("ta"));
        }
        if  (!request.getParameter("tb").equals("")){
            ticket_b = Integer.parseInt(request.getParameter("tb"));
        }
        if (!request.getParameter("tc").equals("")){
            ticket_c = Integer.parseInt(request.getParameter("tc"));               
        }
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        
        try{    
            
            model.autoAnticipoService.buscar(placa);
            
            if(model.autoAnticipoService.getAuto_Anticipo() != null){
                Auto_Anticipo aa = new Auto_Anticipo();
                aa.setPlaca(placa);
                aa.setEfectivo(efectivo);
                aa.setacpm(acpm);
                aa.setTicket_a(ticket_a);
                aa.setTicket_b(ticket_b);
                aa.setTicket_c(ticket_c);
                aa.setUser_update(usuario.getLogin());
                
                model.autoAnticipoService.update(aa);

                request.setAttribute("autoant", aa);
                next = next + "?msg=exitoM";            
            }            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}

