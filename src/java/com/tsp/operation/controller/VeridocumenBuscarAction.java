/*
 * VeridocumenBuscarAction.java
 *
 * Created on 4 de octubre de 2005, 05:14 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;


/**
 *
 * @author  dbastidas
 */
public class VeridocumenBuscarAction  extends Action{
    
    /** Creates a new instance of VeridocumenBuscarAction */
    public VeridocumenBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String pag = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String next="";
        String ced = (request.getParameter("identificacion")!=null)?request.getParameter("identificacion"):"";
        String pla = (request.getParameter("placa")!=null)?request.getParameter("placa"):"";
        String even = (request.getParameter("evento")!=null)?request.getParameter("evento"):"";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        try{
            if (even.equals("Placa")){
                ////System.out.println("Placa "+pla);
                model.veridocService.cargarDocPlaca(pla.toUpperCase());
                if (model.veridocService.ObtDocumentos()!=null){
                        session.setAttribute("men", "Placa");
                        next= Util.LLamarVentana(pag, "Verificación de Documentos");        
                }
                else{
                    pag="/jsp/hvida/veridocumen/buscardocPlaca.jsp?men=no";                    
                    next= Util.LLamarVentana(pag, "Buscar Documentos");
                }
            }
            else{
                model.veridocService.cargarDocConductor(ced);
                if (model.veridocService.ObtDocumentos()!=null){
                        session.setAttribute("men", "Conductor");
                        next= Util.LLamarVentana(pag, "Verificación de Documentos");        
                }
                else{
                    pag="/jsp/hvida/veridocumen/buscardocumentos.jsp?men=no";
                    next= Util.LLamarVentana(pag, "Buscar Documentos");
                }
            }
           
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         ////System.out.println(next);
         // Redireccionar a la página indicada.
        this.dispatchRequest(next);
    }
    
}
