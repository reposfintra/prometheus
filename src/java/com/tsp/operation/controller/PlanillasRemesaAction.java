/*
 * PlanillasRemesaAction.java
 *
 * Created on 29 de agosto de 2005, 09:57 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Jm
 */
public class PlanillasRemesaAction extends Action {
    
    /** Creates a new instance of PlanillasRemesaAction */
    public PlanillasRemesaAction() {
    }
    
    public void run() throws ServletException, InformationException {
         try {
            String next = "/impresion/planillaRemesa.jsp";
            String []Impresa  = request.getParameter("Impresa").split("~");
            String datos = "";
            String separador = "'";
            for(int i=0;i<Impresa.length;i++){
                //////System.out.println(" PLANILLA " + Impresa[i]);
                datos += separador+Impresa[i]+separador+",";
            }
            datos = datos.substring(0,datos.length()-1);
            
            model.PlanillaImpresionSvc.BuscarPlaNoImpRemesa(datos);
           
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
        
    }
    
}
