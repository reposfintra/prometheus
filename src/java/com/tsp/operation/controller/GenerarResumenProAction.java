/*
 * GenerarResumenProAccion.java
 *
 * Created on 11 de septiembre de 2006, 09:46 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  jbarrios
 */
public class GenerarResumenProAction extends Action {
  
  /** Creates a new instance of GenerarResumenProAccion */
  public GenerarResumenProAction() {
  }
  
   public void run() throws ServletException, InformationException {
        String fechai = request.getParameter("fechai");
        String fechaf = request.getParameter("fechaf");
        String base   = request.getParameter("base");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next="/informes/SalidaResumenPro.jsp";
        
        try{
            
            model.planillaService.generarResumenPro(fechai, fechaf, base);
            
            Informe inf = new Informe ();
            inf.setFechai(fechai);
            inf.setFechaf(fechaf);
            model.planillaService.setInforme(inf);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
  
}
