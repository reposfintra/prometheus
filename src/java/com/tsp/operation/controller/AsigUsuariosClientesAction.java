/*
 * AsigUsuariosClientesAction.java
 *
 * Created on 19 de septiembre de 2005, 10:55 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  mfontalvo
 */
public class AsigUsuariosClientesAction extends Action {
    
    /** Creates a new instance of AsigUsuariosClientesAction */
    public AsigUsuariosClientesAction() {
    }
    
    public void run() throws ServletException, InformationException {
       try{           
           String Opcion = request.getParameter("Opcion");
           
           HttpSession session = request.getSession();
           Usuario user = (Usuario) session.getAttribute("Usuario");
           
           String usuario    = user.getLogin();
           String Perfil     = request.getParameter("Perfil");
           String Tipo       = request.getParameter("Tipo");
           String []Usuarios = request.getParameterValues("UsuariosA");
           String []Clientes = request.getParameterValues("ClientesA");
           
           if (Opcion!=null){
               if (Opcion.equals("Grabar")){
                   model.AsigUsuCliSvc.upUsuariosClientes(Usuarios, Clientes, Tipo, Perfil, usuario);
                   model.AsigUsuCliSvc.buscarPerfilClientes(Perfil);
                   model.AsigUsuCliSvc.buscarPerfilUsuarios(Perfil);  
                   model.AsigUsuCliSvc.buscarRelacionPerfil(Perfil);
               }
               if (Opcion.equals("SeleccionarPerfil")){
                   model.AsigUsuCliSvc.buscarPerfilClientes(Perfil);
                   model.AsigUsuCliSvc.buscarPerfilUsuarios(Perfil);
                   model.AsigUsuCliSvc.buscarRelacionPerfil(Perfil);
               }               
           }
           
           
           if (Perfil!=null) model.AsigUsuCliSvc.setPerfil(Perfil);
           
           final String next = "/jsp/trafico/AsignacionUsuariosClientes/AsignacionUsuariosClientes.jsp";
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
       }
       catch(Exception e){
          throw new ServletException(e.getMessage());
       } 
                
    }
    
}
