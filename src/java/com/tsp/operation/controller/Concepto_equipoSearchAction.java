/****************************************************************************
* Nombre .................Concepto_equipoSearchAction.java                  *
* Descripci�n.............Accion para buscar un concepto equipo a la bd.    *
* Autor...................Ing. Jose de la rosa                              *
* Fecha Creaci�n..........16 de diciembre de 2005, 10:30 PM                 *
* Modificado por..........LREALES                                           *
* Fecha Modificaci�n......7 de junio de 2006, 11:10 AM                      *
* Versi�n.................1.0                                               *
* Coyright................Transportes Sanchez Polo S.A.                     *
*****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class Concepto_equipoSearchAction extends Action{
    
    /** Creates a new instance of Concepto_equipoSearchAcion */
    public Concepto_equipoSearchAction () {
    }
    
    public void run () throws ServletException, InformationException {
        
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = ( Usuario ) session.getAttribute("Usuario");   
        String dstrct = ( String ) session.getAttribute("Distrito");
        String listar = ( String ) request.getParameter ("listar");
        
        String descripcion = (request.getParameter("c_descripcion")!= null)?request.getParameter("c_descripcion"):"";
        String codigo = (request.getParameter("c_codigo")!= null)?request.getParameter("c_codigo").toUpperCase():"";
                    
        try{
            
            if ( listar.equals ("True") ){
                
                next="/jsp/equipos/concepto_equipo/concepto_equipoListar.jsp";
                
                String sw = ( String ) request.getParameter ("sw");
                
                if( sw.equals ("True") ){
                    
                    if ( model.concepto_equipoService.existConcepto_equipo( codigo ) ){
                        
                        model.concepto_equipoService.searchConcepto_equipo( codigo );
                        model.concepto_equipoService.getConcepto_equipos();

                        next = "/jsp/equipos/concepto_equipo/concepto_equipoModificar.jsp";
                    
                    } else if ( !model.concepto_equipoService.existConcepto_equipo( codigo ) ){
                        
                        next = "/jsp/equipos/concepto_equipo/concepto_equipoBuscar.jsp?msg=El Concepto Digitado NO EXISTE!!";
                        
                    }
                    
                } else{
                    
                    descripcion = "";
                    codigo = "";
                    
                    model.concepto_equipoService.searchDetalleConcepto_equipo( codigo, descripcion );
                    model.concepto_equipoService.getConcepto_equipos();
                    
                    next = "/jsp/equipos/concepto_equipo/concepto_equipoListar.jsp";
                    
                }
                
                session.setAttribute ( "descripcion", descripcion );
                session.setAttribute ( "codigo", codigo );
                
            } else{
                
                codigo = ( String ) request.getParameter ("c_codigo").toUpperCase ();
                
                model.concepto_equipoService.searchConcepto_equipo( codigo );
                model.concepto_equipoService.getConcepto_equipo();
                
                next = "/jsp/equipos/concepto_equipo/concepto_equipoModificar.jsp";
                
            }
            
        } catch ( SQLException e ){
            
            throw new ServletException ( e.getMessage () );
            
        }
        
        this.dispatchRequest ( next ); 
        
    }    
    
}