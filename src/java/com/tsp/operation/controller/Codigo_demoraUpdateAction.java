/*
 * Codigo_demoraUpdateAction.java
 *
 * Created on 26 de junio de 2005, 03:47 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Codigo_demoraUpdateAction extends Action{
    
    /** Creates a new instance of Codigo_demoraUpdateAction */
    public Codigo_demoraUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="/jsp/trafico/demora/Codigo_demoraModificar.jsp?lista=ok&reload=ok";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
    
        try{
            Codigo_demora codigo_demora = new Codigo_demora();
            codigo_demora.setCodigo(codigo);
            codigo_demora.setDescripcion(descripcion);
            codigo_demora.setUser_update(usuario.getLogin().toUpperCase());
            codigo_demora.setCia(usuario.getCia().toUpperCase());
            model.codigo_demoraService.updateCodigo_demora(codigo, descripcion, usuario.getLogin().toUpperCase(),usuario.getBase().toUpperCase());
            request.setAttribute("mensaje","Codigo Demora Modificado");
            model.codigo_demoraService.serchCodigo_demora(codigo);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }
    
}
