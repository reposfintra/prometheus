/*
 * Nombre        UbicacionIngresarAction.java
 * Autor         Ing. Jesus Cuesta
 * Modificado    Ing. Sandra Escalante
 * Fecha         22 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UbicacionModificarAction extends Action{
    
    /** Creates a new instance of UbicacionModificarAction */
    public UbicacionModificarAction() {
    }
    
    public void run() throws ServletException,InformationException {
        String next = "/jsp/trafico/ubicacion/maUbicacion.jsp?lista=ok&reload=ok";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        try{
            String codigo = request.getParameter("c_codigo");
            String distrito = request.getParameter("c_distrito");
            String desc = request.getParameter("c_descripcion");
            String tipo = request.getParameter("c_tipou");
            String cia = request.getParameter("c_cia");
            String contacto = request.getParameter("c_contacto").equals("")?"":request.getParameter("c_contacto");                        
            String tel1 = request.getParameter("c_tel1");
            String tel2 = request.getParameter("c_tel2");
            String tel3 = request.getParameter("c_tel3");
            String fax = request.getParameter("c_fax");
            
            String pais=request.getParameter("codpais");    
            String est = request.getParameter("codest");
            String ciudad=request.getParameter("c_ciudad");
            String dir = request.getParameter("c_direccion").toUpperCase();
            
            //sescalante 02.02.06
            String mod = request.getParameter("modalidad");
            String tramo = (mod.equalsIgnoreCase("C"))?"":request.getParameter("c_tramo");
            String relacion = (mod.equalsIgnoreCase("C"))?"O":request.getParameter("c_relacion");
            double tiempo = (mod.equalsIgnoreCase("C"))?0:Double.parseDouble(request.getParameter("c_tiempo"));
            double distancia = (mod.equalsIgnoreCase("C"))?0:Double.parseDouble(request.getParameter("c_distancia")); 
            
            int sw = 0;
            String o = "";
            String d = "";
        
            //obtengo origen y destino
            if (tramo.length() == 4){
                o = tramo.substring(0,2);
                d = tramo.substring(tramo.length()-2,tramo.length());
            }
 
            try{
                //verifico existencia de compania y tramo
                if (!model.identidadService.existeIdentidad(cia)){
                    sw=1;
                    request.setAttribute("mensaje","El Nit ingresado no existe!");
                    next = "/jsp/trafico/ubicacion/maUbicacion.jsp?sw=e";
                }
                
                if ( mod.equals("T") ){    
                    if (!model.tramoService.existeTramo(distrito, o, d) ){
                        sw=1;
                        request.setAttribute("mensaje","El Tramo ingresado no existe!");
                        next = "/jsp/trafico/ubicacion/maUbicacion.jsp?sw=e";
                    }
                }
                
            }catch(SQLException ex){
                
            }
            
            if(sw==0){
               Ubicacion u = new Ubicacion();
                u.setCod_ubicacion(codigo);
                u.setDescripcion(desc);
                u.setCia(cia);
                u.setTipo(tipo);
                u.setContacto(contacto);
                u.setPais(pais);        
                u.setEstado(est);
                u.setCiudad(ciudad);
                u.setDireccion(dir);
                u.setTorigen(o);
                u.setTdestino(d);
                u.setRelacion(relacion);
                u.setTiempo(tiempo);
                u.setDistancia(distancia);
                u.setTel1(tel1);
                u.setTel2(tel2);
                u.setTel3(tel3);
                u.setFax(fax);
                u.setDstrct(distrito);
                u.setBase(usuario.getBase());
                u.setUser_update(usuario.getLogin());
                u.setModalidad(mod);   
                
                u.setNompais(request.getParameter("c_pais"));
                u.setNomestado(request.getParameter("c_comboestado"));
                u.setNomciudad(request.getParameter("c_ciudad"));
            
                model.ubService.setUb(u);
                model.ubService.modificarUbicacion();
            
                model.ubService.buscarUbicacion(codigo, cia);
                request.setAttribute("mensaje","La ubicacion fue modificada con exito!");
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
