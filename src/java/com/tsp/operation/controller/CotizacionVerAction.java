/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;
import java.util.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author Rhonalf
 */
public class CotizacionVerAction extends Action {

    public CotizacionVerAction(){
        
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next  = "/jsp/delectricaribe/cotizacion/ver_cotizacion.jsp";
        CotizacionService csr = new CotizacionService();
        request.setAttribute("msg", "");
        ArrayList list = new ArrayList();
        try {
           String con = (String)request.getParameter("consecutivo");
           list = csr.verDetalles(con);
           if(list.size()>0) {
               request.setAttribute("lista", list);
               request.setAttribute("msg", "");
           }
           else request.setAttribute("msg", "No hay datos que mostrar");
        }
        catch(Exception e){
            System.out.println("Error en el run del action ver datos: "+e.toString());
            request.setAttribute("msg", "Error en la visualizacion");
        }
        this.dispatchRequest(next);
    }

}
