/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




import java.util.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;


import com.tsp.util.LogWriter;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;



/**
 *
 * @author Alvaro
 */
public class ConsorcioAccesoAction  extends Action {


String     rutaInformes;

    public ConsorcioAccesoAction() {
    }


    public void run() throws javax.servlet.ServletException {

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();

        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        String aceptarDisable = "";

        try {


            // 2012-03-14
            // ---------------------------------------------------------------------------------------------------------------------------------------------
            // DEFINICION DEL LOG DE ERROR
            LogWriter logWriter = new LogWriter("com/tsp/util/connectionpool/db", usuario.getLogin(), "/Consorcio Causacion ", usuario.getLogin() );
            // FIN DEFINICION DEL LOG DE ERROR
            // ---------------------------------------------------------------------------------------------------------------------------------------------


            //  EVENTO  :  CAUSACION_INGRESOS  de causacionIngresoAnticipado.jsp
            //  Proceso para generar causacion de ingresos


            if (evento.equalsIgnoreCase("CAUSACION_INGRESOS")) {   // 20110306




                logWriter.tituloInicial("PROCESO DE CAUSACION DE INGRESOS");


                String ano   =  request.getParameter("ano");
                String mes   =  request.getParameter("mes");

                model.consorcioService.generaCausacionIngresos(model, distrito, usuario.getLogin(), ano, mes, logWriter);

                logWriter.tituloFinal();

                aceptarDisable = "S";
                msj   = "La creacion de las INM e IPM  se ha iniciado...Ver log de errores para ver su finalizacion";
                next  = "/jsp/consorcio/causacionIngresoAnticipado.jsp?aceptarDisable="+aceptarDisable+"?msj=";



            } // Fin de CAUSACION_INGRESOS











            //  EVENTO  :  SOLICITUD_POR_FACTURAR  de solicitudLiquidacion.jsp
            //  Arma una lista de solicitudes pendientes de facturar a los clientes
            if (evento.equalsIgnoreCase("SOLICITUD_POR_FACTURAR")) {

                model.consorcioService.buscaSolicitudPorFacturar();
                List listaSolicitudPorFacturar = model.consorcioService.getSolicitudPorFacturar();

                if(!listaSolicitudPorFacturar.isEmpty()) {

                    next  = "/jsp/consorcio/solicitudListaPorFacturar.jsp?msj=";
                    msj   = "";
                }
                else {
                    msj   = "No existen ofertas a listar";
                    next  = "/jsp/consorcio/ofertaAcceder.jsp?msj=";
                }
            } // Fin de OFERTAS_POR_FACTURAR



            //  EVENTO  :  DETALLAR_SOLICITUD  de solicitudListaPorFacturar.jsp o de solicitudLiquidacionEspecifica.jsp
            //  Extrae la informacion de la solicitud seleccionada
            //  Extrae la informacion de los diferentes subclientes de esa solicitud
            //  Extrae la informacion de las acciones de esa solicitud
            if (evento.equalsIgnoreCase("DETALLAR_SOLICITUD")) {

                try {

                    String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";
                    String id_solicitud = ( request.getParameter("id_solicitud")!= null) ? request.getParameter("id_solicitud") : "";
                    int parcial = Integer.parseInt( (request.getParameter("parcial")!= null) ? request.getParameter("parcial") : "1"  );

                    // Crea un objeto SolicitudPorFacturar correspondiente a la solicitud seleccionada,
                    // la extrae de la lista de ofertas por facturar
                    // El objeto sera utilizado en la pagina solicitudDetallar.jsp
                    model.consorcioService.buscarSolicitudPorFacturar(id_solicitud, parcial);

                    // Crea una lista de objetos SubclientePorSolicitud correspondiente a los subclientes asociados a la solicitud seleccionada,
                    // El objeto sera utilizado en la pagina solicitudDetallar.jsp
                    model.consorcioService.buscaSubclientePorSolicitud(id_solicitud, parcial);


                    // Crea una lista de objetos AccionPorFacturar correspondiente a la solicitud seleccionada,
                    // El objeto sera utilizado en la pagina solicitudDetallar.jsp
                    model.consorcioService.buscaAccionPorSolicitud(id_solicitud, parcial);


                    // Actualiza la lista de SubclientePorFacturar incluyendoles los valores relacionados a la financiacion
                    model.consorcioService.CalculaFinanciacion();

                    msj   = "";
                    next  = "/jsp/consorcio/solicitudDetallar.jsp?opcion="+opcion+"&msj=";
                    
                }
                catch (Exception e) {
                    Util.imprimirTrace(e);
                }
            } // Fin de DETALLAR_SOLICITUD


            //  EVENTO  :  LEER_RECAUDO_EXCEL  de recaudoElectrificadora.jsp
            //  Lee un archivo excell donde estan los recaudos de Electrificadora
            if (evento.equalsIgnoreCase("LEER_RECAUDO_EXCEL")) {


                logWriter.tituloInicial("PROCESO DE RECAUDOS");



                List listaArchivos = upLoadArchivo(request, usuario, logWriter);

                HRecaudoElectrificadora hilo = new HRecaudoElectrificadora();
                hilo.start(model, usuario, distrito, listaArchivos, logWriter);


                logWriter.tituloFinal();

                aceptarDisable = "S";

                next  = "/jsp/consorcio/recaudoElectrificadora.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de leer los Recaudos. Ver log de proceso para ver su finalizacion";


            } // Fin de LEER_RECAUDO_EXCEL




            //  EVENTO  :  APLICAR_LIQUIDACION  de solicitudDetallar.jsp
            //  Actualiza simbolo variable , observacion , cambia id_estado del negocion para una orden y registra la liquidacion de cada accion
            if (evento.equalsIgnoreCase("APLICAR_LIQUIDACION")) {


               String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";


               System.out.println("Inicio lectura parametro") ;


               String lista_simboloVariable  = request.getParameter("lista_simboloVariable");
               String lista_observacion      = request.getParameter("lista_observacion");
               String lista_fechaFactura     = request.getParameter("lista_fechaFactura");

               lista_simboloVariable = lista_simboloVariable.substring(0,lista_simboloVariable.length() - 1 );
               lista_observacion     = lista_observacion.substring(0,lista_observacion.length() - 1 );
               lista_fechaFactura    = lista_fechaFactura.substring(0,lista_fechaFactura.length() - 1 );

               System.out.println("Parametro lista_simboloVariable : " + lista_simboloVariable);
               System.out.println("Parametro lista_observacion : " + lista_observacion);
               System.out.println("Parametro lista_fechaFactura : " + lista_fechaFactura);




               String[] simboloVariable  = lista_simboloVariable.split("\\|");
               String[] observacion      = lista_observacion.split("\\|");
               String[] fechaFactura     = lista_fechaFactura.split("\\|");



               System.out.println("Final lectura parametro") ;
               
               System.out.println("Impresion de parametros Longitud = "+simboloVariable.length);
               for (int x=0;x<simboloVariable.length;x++) {
                   System.out.println("Simbolo variable[" + x + "] = " + simboloVariable[x]);
                   System.out.println("Observacion = [" + x + "] = " + observacion[x]);
                   System.out.println("fechaFactura = [" + x + "] = " + fechaFactura[x]);
               }
               

               String error = "NO";


               
                Vector comandos_sql =new Vector();
                String comandoSQL = "";

                java.util.Date fechaActual = new Date();
                String last_update = fechaActual.toString();



                // Actualiza las acciones relacionadas a la solicitud
                try {
                    List listaAccionPorSolicitud = model.consorcioService.getAccionPorSolicitud();
                    AccionPorSolicitud accion = null;


                    Iterator it = listaAccionPorSolicitud.iterator();

                    while (it.hasNext()) {
                       accion = (AccionPorSolicitud)it.next();
                       comandoSQL = model.consorcioService.setAcciones(accion, last_update, usuario.getLogin());

                       // System.out.println(comandoSQL+ "\n");

                       comandos_sql.add(comandoSQL);
                    }
                }
                catch (Exception e) {
                    error = "SI";
                }

              
               // Actualiza los valores de los subclientes relacionados a la solicitud
                try {
                    int i = 0;
                    String simbolo_variable = "";
                    String fecha_factura = "";
                    String observacion_subcliente = "";

                    SubclientePorSolicitud subcliente = null;
                    List listaSubclientePorSolicitud = model.consorcioService.getSubclientePorSolicitud();
                    ListIterator lit = listaSubclientePorSolicitud.listIterator();

                    while (lit.hasNext()) {
                       subcliente  = (SubclientePorSolicitud) lit.next();

                       simbolo_variable = simboloVariable[i];
                       observacion_subcliente = observacion[i];
                       fecha_factura = fechaFactura[i];


                       System.out.println(simbolo_variable + "-" + observacion_subcliente + "-" + fecha_factura);
                       System.out.println(simboloVariable[i] + "-" + observacion[i] + "-" + fechaFactura[i]);


                       subcliente.setSimbolo_variable(simbolo_variable);
                       subcliente.setObservacion(observacion_subcliente);
                       subcliente.setFecha_factura(fecha_factura);
                       
                       i++;

                       comandoSQL = model.consorcioService.setSubclientesOfertas(subcliente, simbolo_variable,observacion_subcliente, fecha_factura, usuario.getLogin()) ;

                       System.out.println(comandoSQL+ "\n");
                       
                       comandos_sql.add(comandoSQL);
                    }
                }
                catch (Exception e) {
                    error = "SI";
                }



                if(error.equalsIgnoreCase("NO")){

                    // Grabando todo a la base de datos.
                    String estado = model.consorcioService.ejecutarBatchSQL(comandos_sql);

                    if (estado.equalsIgnoreCase("ERROR")){
                        msj  = "Los datos no fueron registrados. Se genero un rollback sobre la base de datos";
                        next = "/jsp/consorcio/solicitudDetallar.jsp?opcion="+opcion+"&msj=";
                    }
                    else{

                        model.consorcioService.buscaSolicitudPorFacturar();
                        
                        msj="";
                        next = "/jsp/consorcio/solicitudListaPorFacturar.jsp?opcion="+opcion+"&msj=";



                    }
                }
                else {
                    if ( opcion.equalsIgnoreCase("LISTADO") ){
                        msj  = "Los datos no fueron registrados. Hubo un error al generar el SQL de actualizacion al subcliente"  ;
                        next = "/jsp/consorcio/solicitudListaPorFacturar.jsp?opcion="+opcion+"&msj=";

                    }
                    else
                    {
                        msj  = ""  ;
                        next = "/jsp/consorcio/solicitudLiquidacionEspecifica.jsp?opcion="+opcion+"&msj=";
                    }
                }

            } // Fin de APLICAR_LIQUIDACION


            //  EVENTO  :  GENERAR_FACTURAR_ECA de facturaEcaCreacion.jsp
            //  Envia una prefactura a exportar a excell
            if (evento.equalsIgnoreCase("GENERAR_FACTURAR_ECA")) {

                System.out.println ("Inicio antes del hilo");


                HGenerarFacturaEcaOpav hilo =  new HGenerarFacturaEcaOpav();
                hilo.start(model, usuario);

                System.out.println ("Final despues del hilo");

                aceptarDisable = "S";
                msj   = "La creacion de las facturas CxC a Eca se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/consorcio/facturaEcaCreacion.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de GENERAR_FACTURAR_ECA


            next += msj ;
            this.dispatchRequest(next);

        }catch (Exception e) {
            e.printStackTrace();
        }

    }






    /**
     *
     * Procedimiento para subir un archivo xls al sistema
     * @param request
     * @param usuario Login del usuario en sesion
     * @param logWriter Objeto para controlar los log del programa
     * @return Un list de los archivos que se subiran al sistema
     * @throws ServletException
     * @throws IOException
     */
     private List upLoadArchivo(HttpServletRequest request, Usuario usuario, LogWriter logWriter) throws ServletException, IOException {


        List listaArchivos = new LinkedList();


        try {

            logWriter.log("Inicia proceso de subir archivos al servidor \n",LogWriter.INFO);
            // Crea el directorio donde quedara el archivo que se sube al servidor
            this.generarRUTA(usuario) ;


            // DIRECTORIOS TEMPORALES

            // Define el directorio donde temporalmente se sube el archivo en caso de ser mayor a 500K
            int indice = rutaInformes.lastIndexOf("/") ;
            String RUTA_DIRECTORIO_TEMPORAL = rutaInformes.substring(0, indice);
            File directorioTemporal;

            // Verifica que el directorio temporal exista
            directorioTemporal = new File(RUTA_DIRECTORIO_TEMPORAL);
            if(!directorioTemporal.isDirectory()) {
                    logWriter.log("La ruta : " + RUTA_DIRECTORIO_TEMPORAL + " no es un directorio \n",LogWriter.INFO);
                    throw new ServletException(RUTA_DIRECTORIO_TEMPORAL + " no es un directorio");
            }


            // DIRECTORIOS FINALES

            // Define el directorio donde finalmente quedara el archivo
            String RUTA_DIRECTORIO_DESTINO ="/" + usuario.getLogin();
            File directorioDestino;
            String rutaReal = rutaInformes ;

            // Verifica que el directorio final exista
            directorioDestino = new File(rutaReal);
            if(!directorioDestino.isDirectory()) {
                logWriter.log("La ruta : " + RUTA_DIRECTORIO_DESTINO + " no es un directorio \n",LogWriter.INFO);
                    throw new ServletException(RUTA_DIRECTORIO_DESTINO+" no es un directorio");
            }


            // PROCESO DEL ARCHIVO O ARCHIVOS A SUBIR

            DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();

             // Define el limite del tama�o del archivo por sobre el cual el archivo se subira temporalmente al directorio temporal

            fileItemFactory.setSizeThreshold(1*128*1024);   // 128 KB

             // Define el directorio temporal donde almacenar termporalmente el archivo cuando sobrepasa los 0.5 MB

            fileItemFactory.setRepository(directorioTemporal);

            ServletFileUpload manejaUpload = new ServletFileUpload(fileItemFactory);
            try {

                    // Analiza el requerimiento

                    List items = manejaUpload.parseRequest(request);
                    Iterator itr = items.iterator();
                    while(itr.hasNext()) {
                            FileItem item = (FileItem) itr.next();

                             // Manejo de los campos

                            if(item.isFormField()) {
                                logWriter.log("Nombre del archivo = " + item.getFieldName()+", Valor = "+item.getString() + " \n",LogWriter.INFO);

                            } else {

                                    logWriter.log("Nombre del campo   = " + item.getFieldName(),LogWriter.INFO );
                                    logWriter.log("Nombre del archivo = " + item.getName(),LogWriter.INFO );
                                    logWriter.log("Tipo de contenido  = " + item.getContentType(),LogWriter.INFO );
                                    logWriter.log("Tama�o del archivo = " + item.getSize(),LogWriter.INFO );

                                    // Escribe el archivo en el destino final
                                    File archivo = new File(directorioDestino,item.getName());
                                    item.write(archivo);

                                    listaArchivos.add(directorioDestino + "%" + item.getName() );


                            }

                    }
            }catch(FileUploadException ex) {
                logWriter.log("ERROR : Error encontrado mientras analizaba el request"  ,LogWriter.INFO);
                logWriter.log("        " + ex.getMessage() + "\n",LogWriter.INFO);

            } catch(Exception ex) {
                logWriter.log("ERROR : Error encontrado mientras mientras se subia el archivo al servidor"  ,LogWriter.INFO);
                logWriter.log("        " + ex.getMessage() + "\n",LogWriter.INFO);
            }


        } catch(Exception ex) {
                logWriter.log("ERROR : Error encontrado en el procedimiento"  ,LogWriter.INFO);
                logWriter.log("        " + ex.getMessage() + "\n",LogWriter.INFO);
        }

        logWriter.log("Finaliza proceso de subir archivos al servidor \n"  ,LogWriter.INFO);


        return listaArchivos;

    }


        /**
         * Ubica la informacion de la ruta donde quedara el informe
         * Si no existe crea el directorio
         *
         * @author  Alvaro Pabon Martinez
         * @version %I%, %G%
         * @since   1.0
         *
         */

        public void generarRUTA( Usuario usuario) throws Exception{
            try{

                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
                File archivo = new File( rutaInformes );
                if (!archivo.exists()) archivo.mkdirs();

            }catch (Exception ex){
                 Util.imprimirTrace(ex);
                throw new Exception(ex.getMessage());
            }

        }


        public double get_anualidad(double vp , double i , double n ) {

            // Sirve para pasar conseguir el valor de una anualidad especificando un interes en  Mensual Vencida

            // r = anual trimestre anticipado.

            double r = Math.pow((1+(i/100)),n);
            double factor =  (r-1)/(i/100*r);
            double a = vp / factor;

            return a;
        }     

            


}












