/*
 * Nombre        ReporteVaradosAction.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         8 de noviembre de 2006, 07:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.HReporteVarados;

public class ReporteVaradosAction extends Action {
    
    /**
     * Crea una nueva instancia de  ReporteVaradosAction
     */
    public ReporteVaradosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next="/jsp/trafico/reportes/reporteVarados.jsp";
        String opc = (request.getParameter("opcion") != null)? request.getParameter("opcion") : "";
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");        
        
        try{
            
            if( opc.equals("report") ){
                
                String cliente = request.getParameter("cod_cli");
                String inicio = request.getParameter("inicial");
                String fin = request.getParameter("final");
                
                Vector v = model.rmtService.reporteVaradosCliente(cliente, inicio, fin, u.getDstrct() );
                model.rmtService.setClientes( v );
                
                request.setAttribute( "varados",  v );
                next="/jsp/trafico/reportes/reporteVarados.jsp?ini="+inicio+"&fin="+fin;
                
            }else if( opc.equals("excel") ){
                
                
		String inicio = request.getParameter("inicial");
                String fin = request.getParameter("final");
                Vector v = model.rmtService.getClientes();
                if( v.size() > 0 ){
                    HReporteVarados h = new HReporteVarados();
                    h.start( u, v, inicio, fin);
                }
                request.setAttribute( "varados",  v );
                
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ServletException(ex.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
