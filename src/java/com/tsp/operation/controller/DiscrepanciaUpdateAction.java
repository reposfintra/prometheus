/************************************************************************
 * Nombre clase: DiscrepanciaAnularAction.java
 * Descripci�n: Accion para actualizar las discrepancias.
 * Autor: Jose de la rosa
 * Fecha: 27 de septiembre de 2005, 11:44 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Jose
 */
public class DiscrepanciaUpdateAction extends Action{
    
    /** Creates a new instance of DiscrepanciaAnularAction */
    public DiscrepanciaUpdateAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/cumplidos/discrepancia/DiscrepanciaModificar.jsp?reload=ok";
        HttpSession session = request.getSession ();
        int num_discre = Integer.parseInt (request.getParameter ("c_num_discre"));
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String numrem = request.getParameter ("c_numrem").toUpperCase ();
        String tipo_doc = request.getParameter ("c_tipo_doc").toUpperCase ();
        String documento = request.getParameter ("c_documento").toUpperCase ();
        String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter ("c_documento_rel");
        String fecha_devolucion = request.getParameter ("c_fecha_devolucion");
        String nro_rechazo = (request.getParameter ("c_nro_rechazo")!=null)?request.getParameter ("c_nro_rechazo"):"";
        String reportado = (request.getParameter ("c_reportado")!=null)?request.getParameter ("c_reportado"):"";
        String contacto = (request.getParameter ("c_contacto")!=null)?request.getParameter ("c_contacto"):"";
        String ubicacion = (request.getParameter ("c_ubicacion")!=null)?request.getParameter ("c_ubicacion"):"";
        String retencion = request.getParameter ("c_retencion");
        String observacion = request.getParameter ("c_observacion");
        String nom_doc = request.getParameter ("nom_doc");
        String clientes = request.getParameter ("client");
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        try{
            Discrepancia dis = new Discrepancia ();
            dis.setNro_discrepancia (num_discre);
            dis.setNro_planilla (numpla);
            dis.setNro_remesa (numrem);
            dis.setTipo_doc_rel (tipo_doc_rel);
            dis.setDocumento_rel (documento_rel);
            dis.setTipo_doc (tipo_doc);
            dis.setRetencion (retencion);
            dis.setDocumento (documento);
            dis.setFecha_devolucion (fecha_devolucion);
            dis.setNumero_rechazo (nro_rechazo);
            dis.setReportado (reportado);
            dis.setContacto (contacto);
            dis.setUbicacion (ubicacion);
            dis.setObservacion (observacion);
            dis.setUsuario_modificacion (usuario.getLogin ());
            dis.setUsuario_creacion (usuario.getLogin ());
            dis.setBase (usuario.getBase ());
            dis.setDistrito (distrito);
            model.discrepanciaService.searchDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel,distrito );
            Discrepancia discre = model.discrepanciaService.getDiscrepancia ();
            if ( discre != null ) {
                Vector consultas = new Vector ();
                dis.setFecha_cierre ( discre.getFecha_cierre () );
                consultas.add ( model.discrepanciaService.updateDiscrepancia ( dis ) );
                model.despachoService.insertar (consultas);
            }
            else{
                dis.setFecha_cierre ("0099-01-01 00:00:00");
            }
            request.setAttribute ("msg", "Discrepancia Modificada");
            model.discrepanciaService.setCabDiscrepancia ( dis );
            next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&nom_doc="+nom_doc+"&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client="+clientes;
        }catch (Exception e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
