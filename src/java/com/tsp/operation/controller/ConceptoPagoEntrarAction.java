/******************************************************************
 * Nombre ......................ConceptoPagoAction.java
 * Descripci�n..................Conceptos de pago
 * Autor........................Armando Oviedo
 * Fecha........................Created on 19 de octubre de 2005, 11:36 AM
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class ConceptoPagoEntrarAction extends Action{        
    public void run() throws javax.servlet.ServletException, InformationException{        
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");            
            String opcion = request.getParameter("Opcion");
            String codigos[] = request.getParameterValues("LOV");
            String idFolder = request.getParameter("idFolder");
            String ref1 = request.getParameter("ref1");
            String ref2 = request.getParameter("ref2");
            String ref3 = request.getParameter("ref3");//MODIFICADO POR DAVID PI�A LOPEZ
            String ref4 = request.getParameter("ref4");//MODIFICADO POR DAVID PI�A LOPEZ
            String nivel = request.getParameter("nivel");
            String codigo = request.getParameter("codigo"); 
            String index = request.getParameter("index");  
            if(opcion.equalsIgnoreCase("Ver")){
                String iter = request.getParameter("Index");
                for(int i=0;i<model.ConceptoPagosvc.getListaFolder().size();i++){
                    ConceptoPago cpt = (ConceptoPago)(model.ConceptoPagosvc.getListaFolder().get(i));
                    //System.out.println("codigo: "+cpt.getCodigo()+" Nombre: "+cpt.getDescripcion());                    
                }
                ConceptoPago cpt = (ConceptoPago)(model.ConceptoPagosvc.getListaFolder().get(Integer.parseInt(iter)));                
                model.ConceptoPagosvc.addTraza(cpt);
                model.ConceptoPagosvc.actualizarContenido();                
                model.ConceptoPagosvc.mostrarContenido(cpt.getCodigo());
                model.ConceptoPagosvc.buscarHijos("0", cpt.getCodigo());
            }
            else if(opcion.equalsIgnoreCase("Reg")){
                ConceptoPago cpt = model.ConceptoPagosvc.getUTraza();
                model.ConceptoPagosvc.removeTraza();
                if(model.ConceptoPagosvc.getListaTraza()!=null && model.ConceptoPagosvc.getListaTraza().size()>0){
                    model.ConceptoPagosvc.mostrarContenido(cpt.getCodigo());
                    model.ConceptoPagosvc.actualizarContenido();
                }
                else{
                    model.ConceptoPagosvc.mostrarContenido(model.ConceptoPagosvc.getRaiz());
                }
            }
            else if(opcion.equalsIgnoreCase("modificar")){
                String mover = request.getParameter("mover");
                String moveradestino = request.getParameter("moveradestino");
                ConceptoPago tmp = model.ConceptoPagosvc.getUTraza();
                ConceptoPago cpt = new ConceptoPago();
                if(tmp!=null)cpt.setCodpadre(tmp.getCodigo());
                else cpt.setCodpadre("0");
                cpt.setCodigo(request.getParameter("Id"));
                cpt.setBase(usuario.getBase());                                                
                cpt.setDescripcion(request.getParameter("Nombre"));
                cpt.setDstrct(usuario.getDstrct());                            
                cpt.setId_folder((idFolder!=null? idFolder : "Y"));
                cpt.setUser_update(usuario.getLogin());
                cpt.setLast_update("now()");  
                cpt.setReferencia1(ref1);
                cpt.setReferencia2(ref2);
                cpt.setReferencia3( ref3 );//MODIFICADO POR DAVID PI�A LOPEZ
                cpt.setReferencia4( ref4 );//MODIFICADO POR DAVID PI�A LOPEZ                
                cpt.setNivel(Integer.parseInt(nivel));
                model.ConceptoPagosvc.update(cpt);                
                model.ConceptoPagosvc.actualizarContenido();
                //System.out.println("mover: "+mover);
                if(mover!=null && mover.equalsIgnoreCase("true")){
                    model.ConceptoPagosvc.updateDestFolfer(cpt, moveradestino);
                }
                model.ConceptoPagosvc.actualizarContenido();
                next+="?reload=ok&index=";               
            }
            else if(opcion.equalsIgnoreCase("agregar")){
                ConceptoPago tmp = model.ConceptoPagosvc.getUTraza();
                ConceptoPago cpt = new ConceptoPago();
                if(tmp!=null)cpt.setCodpadre(tmp.getCodigo());                
                else cpt.setCodpadre("0");        
                int codtemp = model.ConceptoPagosvc.getCodigoSerial();
                cpt.setCodigo(String.valueOf(codtemp));
                cpt.setBase(usuario.getBase());   
                if(idFolder.equalsIgnoreCase("N")){
                    cpt.setControlparam("../../../controller?estado=ConceptoPago&accion=CargarObjeto&carpeta=/jsp/cxpagar/Concepto_Pagos&pagina=conceptoPagoCargado.jsp&codigo="+cpt.getCodigo());
                }      
                else{
                    cpt.setControlparam("");
                }
                cpt.setDescripcion(request.getParameter("Nombre"));
                cpt.setDstrct(usuario.getDstrct());
                cpt.setId_folder(idFolder);                
                cpt.setCreation_date("now()");
                cpt.setCreation_user(usuario.getLogin());
                cpt.setReferencia1(ref1);
                cpt.setReferencia2(ref2);
                if(model.ConceptoPagosvc.getListaTraza()!=null){
                    cpt.setNivel(model.ConceptoPagosvc.getListaTraza().size()+1);
                }
                else{
                    cpt.setNivel(1);
                }                                
                cpt.setReferencia3( ref3 );//MODIFICADO POR DAVID PI�A LOPEZ
                cpt.setReferencia4( ref4 );//MODIFICADO POR DAVID PI�A LOPEZ
                cpt.setOrden(0);                                
                model.ConceptoPagosvc.insert(cpt);
                model.ConceptoPagosvc.actualizarContenido();                
                next+="?reload=ok";
            }
            else if(opcion.equalsIgnoreCase("Eliminar")){
                if (codigos!=null){
                    for (int i=0; i<codigos.length;i++){
                        model.ConceptoPagosvc.delete(codigos[i]);                        
                    }
                    model.ConceptoPagosvc.actualizarContenido();                    
                }      
                next+="?reload=ok";
            }                 
            
            else if(opcion.equalsIgnoreCase("cargarvistafolder")){                
                model.ConceptoPagosvc.buscarHijos("0", codigo);
                next += "?index=" + index;                
            }                             
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new ServletException(ex.getMessage());            
        }
        this.dispatchRequest(next);
    }    
}
