/*
 * Nombre        CrearUsuarioAction.java
 * Descripci�n   Clase para la administraci�n de usuarios
 * Autor         desconociido
 * Fecha         17 de marzo de 2004, 11:10 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import java.util.*;
import com.tsp.operation.model.beans.Usuario;

/**
 * Acci�n tomada cuando se crea un usuario nuevo.
 * @author  iherazo
 */
public class GestionUsuariosCreateUserAction extends Action {
    /**
     * Ejecuta la accion: Crear un usuario nuevo, exceptuando los destinatarios,
     * los cuales son creados por los clientes admin. de transporte.
     * @throws ServletException Si ocurre un error durante el procesamiento
     *         de la accion.
     * @throws IOException Si no se puede abrir la vista solicitada por esta
     *         accion, la cual corresponde a una p�gina jsp.
     */
    public void run() throws ServletException, InformationException {
        String next = "/jsp/sot/admin/CrearUsuario.jsp";
            
        try {
            String crearUsuario = request.getParameter("salvarDatos");
            if( crearUsuario == null ){
                next += "?salvarDatos=N";
                model.menuService.buscarPerfiles();
            }
            else if( crearUsuario.equals("S") ){
                String [] createArgs = new String[22];
                String cia;
                createArgs[0]  = this.application.getInitParameter("deskey");
                createArgs[1]  = request.getParameter("nombre");
                createArgs[2]  = request.getParameter("direccion");
                createArgs[3]  = request.getParameter("pais");
                createArgs[4]  = request.getParameter("ciudad");
                createArgs[5]  = request.getParameter("email");
                createArgs[6]  = request.getParameter("telefono");
                createArgs[7]  = request.getParameter("tipoUsuario");
                createArgs[8]  = request.getParameter("nit");
                String id = "";
                if(createArgs[7].equals("CLIENTETSP") || createArgs[7].equals("CLIENTEADM") ){
                    String[] clien = request.getParameterValues("clienteDestinat");
                    id             =  ArrayToString(clien);
                } else
                    id = request.getParameter("clienteDestinat");
                
                createArgs[9]  = id;
                createArgs[9]  = (createArgs[9] == null ? "" : createArgs[9]);
                createArgs[10] = request.getParameter("estadoUsuario");
                createArgs[11] = request.getParameter("idUser").toUpperCase().trim();
                createArgs[12] = request.getParameter("pass1");
                //AMENDEZ 20050628
                createArgs[13]  = request.getParameter("agencia");
                createArgs[13]  = (createArgs[13] == null ? "" : createArgs[13]);
                createArgs[14] = request.getParameter("savePlanVj");
                createArgs[14]  = (createArgs[14] == null ? "0" : createArgs[14]);
                cia = Arrays.deepToString(request.getParameterValues("cia"));
                cia = cia.replace("[", ""); cia = cia.replace("]", ""); cia = cia.replace(" ", "");
                createArgs[15] = cia;
                createArgs[16] = request.getParameter("updatePass");
                createArgs[16] = (createArgs[16] == null ? "false" : createArgs[16]);
                createArgs[17] = request.getParameter("dpto");
                createArgs[17]  = (createArgs[17] == null ? "" : createArgs[17]);
                createArgs[18]  = request.getParameter("base");
                createArgs[19]  = ((Usuario)request.getSession().getAttribute("Usuario")).getLogin();                
                
                //JEscandon 
                createArgs[20]  = ( request.getParameter("reanticipos") != null )?request.getParameter("reanticipos"):"N";
                createArgs[21]  = request.getParameter("nits_prop");
                
                model.usuarioService.agregarUsuario( createArgs,request.getParameterValues("perfil") );
                
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
    public String ArrayToString(String [] arreglo){
        String lista = "";
        for (int i=0;arreglo!=null && i<arreglo.length;lista+=arreglo[i]+"#",i++);
        return (lista.equals("")?"":(lista.substring(0, lista.length()-1)));
    }
    
    
}