/*
 * ModificarPaisAction.java
 *
 * Created on 28 de febrero de 2005, 03:20 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class ModificarPaisAction extends Action {
    
    /** Creates a new instance of ModificarPaisAction */ 
    public ModificarPaisAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next = "/jsp/trafico/pais/pais.jsp?mensaje=Modificado&reload=ok";
        String codigo = (request.getParameter("c_codigo").toUpperCase());
        String nombre = (request.getParameter("c_nombre").toUpperCase());
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            
            Pais pais = new Pais();
            pais.setCountry_code(codigo);
            pais.setCountry_name(nombre);
            pais.setUser_update(usuario.getLogin());
            next=next+"&sw=1";  
            model.paisservice.modificarpais(pais); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
}
 