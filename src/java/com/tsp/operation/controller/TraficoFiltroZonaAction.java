/********************************************************************
 *      Nombre Clase.................   TraficoFiltroAction.java
 *      Descripci�n..................   Action que se encarga de eliminar un filtro creado por el usuario
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 *
 * @author  dlamadrid
 */
public class TraficoFiltroZonaAction extends Action {
    
    /** Creates a new instance of TraficoFiltroZonaAction */
    public TraficoFiltroZonaAction () {
    }
    
    
    public void run () throws ServletException, InformationException {
        try{
            String linea  =""+ request.getParameter ("linea");
            String conf   =""+request.getParameter ("conf");
            String clas   =""+request.getParameter ("clas");
            String var1   =""+request.getParameter ("var1");
            String var2   =""+request.getParameter ("var2");
            String var3   =""+request.getParameter ("var3");
            String zona   =""+request.getParameter ("zonasUsuario");
            
            String cadena="";
            
            ////System.out.println ("Cadena de Salida"+cadena);
            
            Vector vectorFiltro = new Vector ();
            cadena="";
            for(int i=0;i<vectorFiltro.size ();i++){
                cadena=cadena+vectorFiltro.elementAt (i);
            }
            ////System.out.println ("Vector de filtros: "+cadena);
            
            cadena="";
            Vector vectorFiltroO = new Vector ();
            vectorFiltroO=model.traficoService.ordenarVectorFiltro (vectorFiltro);
            cadena="";
            for(int i=0;i<vectorFiltroO.size ();i++){
                cadena=cadena+vectorFiltroO.elementAt (i);
            }
            ////System.out.println ("Vector de filtros Ordenado: "+cadena);
            
            String filtro= model.traficoService.filtro (vectorFiltroO);
            ////System.out.println ("Filtro final: "+ filtro);
            
            String validacion =" zona='"+zona+"'";
            if(filtro.length ()>1){
                filtro="where "+validacion +" and "+filtro;
            }
            if(filtro.equals ("")){
                filtro="where "+validacion+filtro;
            }
            if(var3.equals ("null") ){
                var3=model.traficoService.obtVar3 ();
            }
            if(linea.equals ("null")||linea.equals ("")){
                linea=model.traficoService.obtlinea ();
            }
            if(conf.equals ("null")||conf.equals ("")){
                conf = model.traficoService.obtConf ();
            }
            if(clas.equals ("null")||clas.equals ("")){
                clas="";
            }
            
            ////System.out.println ("conf antes de consulta final"+conf);
            Vector colum= new Vector ();
            
            model.traficoService.listarColTrafico (var3);
            Vector col = model.traficoService.obtColTrafico ();
            model.traficoService.listarColtrafico2 (col);
            
            colum = model.traficoService.obtenerCampos (linea);
            
            String consulta = model.traficoService.getConsulta (conf, filtro, clas);
            ////System.out.println ("Consulta final:"+consulta);
            
            for (int i=0;i< colum.size ();i++){
                ////System.out.println ("col2: "+ colum.elementAt (i));
            }
            model.traficoService.generarTabla (consulta, colum);
            filtro=model.traficoService.codificar (filtro);
            String next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&clas="+clas+"";
            ////System.out.println ("next en Filtro"+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
        
        
    }
    
}
