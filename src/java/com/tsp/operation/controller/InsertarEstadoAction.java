/*
 * InsertarEstadoAction.java
 *
 * Created on 1 de marzo de 2005, 01:21 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIBASMO
 */
public class InsertarEstadoAction extends Action{
    
    /** Creates a new instance of InsertarEstadoAction */
    public InsertarEstadoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String codigo = (request.getParameter("c_codigo").toUpperCase());
        String nombre = (request.getParameter("c_nombre").toUpperCase());
        String pais = (request.getParameter("c_pais"));
        String zona = (request.getParameter("c_zona").toUpperCase());
        String next = "/jsp/trafico/estado/estado.jsp?mensaje=Agregado";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        int sw=0;
        
        try{ 
            Estado estado = new Estado();
            estado.setdepartament_code(codigo);
            estado.setdepartament_name(nombre);
            estado.setpais_code(pais);
            estado.setzona(zona);
            estado.setUser_update(usuario.getLogin());
            estado.setCreation_user(usuario.getLogin());
            try{
                model.estadoservice.insertarPais(estado);
            }
            catch (SQLException e){
                ////System.out.println("Error " + e.getMessage()+ " Codigo "+ e.getErrorCode() );
                sw=1;
            }
            if (sw==1){
                if ( model.estadoservice.existeEstadoAnulado(pais,codigo) ){
                    model.estadoservice.Activarestado(estado);
                }
                else{
                    model.paisservice.buscarpais(pais);
                    next = "/jsp/trafico/estado/estado.jsp?mensaje=Codigo " + codigo + " ya existe..&sw=0";
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
