/*********************************************************************************
 * Nombre clase :                 ReporteFacturasClientesAction.java             *
 * Descripcion :                  Clase que maneja los eventos relacionados      *
 *                                con el programa que busca para el reporte      *
 *                                de las facturas de un cliente en la BD.        *
 * Autor :                        LREALES                                        *
 * Fecha :                        29 de noviembre de 2006, 05:00 PM             *
 * Version :                      1.0                                            *
 * Copyright :                    Fintravalores S.A.                        *
 ********************************************************************************/

package com.tsp.operation.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ReporteFacturasClientesAction extends Action {
    
    /** Creates a new instance of ReporteFacturasClientesAction */
    public ReporteFacturasClientesAction () { }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/general/exportar/facturas_clientes/ReporteFacturasClientes.jsp";
        
        HttpSession session = request.getSession ();
        Usuario usuario = ( Usuario ) session.getAttribute ( "Usuario" );
        
        String hacer = ( request.getParameter ("hacer") != null )?request.getParameter ("hacer").toUpperCase():"";
        
        String opcion = ( request.getParameter ("opcion") != null )?request.getParameter ("opcion").toUpperCase():"";
        
        String numfac = ( request.getParameter ("documento") != null )?request.getParameter ("documento").toUpperCase():"";
        
        String codcli = ( request.getParameter ("cod_cli") != null )?request.getParameter ("cod_cli").toUpperCase():"";
        
        String numrem = ( request.getParameter ("remesa") != null )?request.getParameter ("remesa").toUpperCase():"";
        
        String fecini = ( request.getParameter ("fecini") != null )?request.getParameter ("fecini"):"";
        String fecfin = ( request.getParameter ("fecfin") != null )?request.getParameter ("fecfin"):"";
        
        String dist = ( request.getParameter ("dist") != null )?request.getParameter ("dist").toUpperCase():"";
        String docu = ( request.getParameter ("docu") != null )?request.getParameter ("docu").toUpperCase():"";
        String tdoc = ( request.getParameter ("tdoc") != null )?request.getParameter ("tdoc").toUpperCase():"";
        
        String fec_creacion = ( request.getParameter ("fec_creacion") != null )?request.getParameter ("fec_creacion"):"";
        String usu_creacion = ( request.getParameter ("usu_creacion") != null )?request.getParameter ("usu_creacion").toUpperCase():"";
        
        //nuevos filtros
        
        String prov = ( request.getParameter ("clientes") != null )?request.getParameter ("clientes"):"";
        String est = ( request.getParameter ("estate") != null )?request.getParameter ("estate"):"";
        
        String fecven = ( request.getParameter ("fec_venc") != null )?request.getParameter ("fec_venc"):"";
        String est1=( request.getParameter ("estate1") != null )?request.getParameter ("estate1"):"";
        
        session.setAttribute("prov", prov );
        session.setAttribute("est", est);
        boolean sw = false;
        
        Vector datos = null;

        Vector otros_datos = null;
        
        boolean error = false;
        
        String multi = ( request.getParameter ("multi") != null )?request.getParameter ("multi").toUpperCase():"";
        
        try {
                        
            if ( hacer.equals ( "3" ) ) {
                
                model.reporteFacturasClientesService.buscarFactura ( dist, tdoc, docu );
                otros_datos = model.reporteFacturasClientesService.getVectorFactura (); 

                if ( otros_datos.size () > 0 ) {
                    
                    next = "/jsp/general/exportar/facturas_clientes/FacturaDetalle.jsp";
                    
                } else {
                    
                    next = "/jsp/general/exportar/facturas_clientes/FacturaDetalle.jsp?msg=Su busqueda no arrojo resultados!";
                    
                }
                
                error = true;
                
            }
            
            
            if ( opcion.equals("1") ) {
                
                sw = model.clienteService.existeCliente( codcli );
                
                if ( !sw ) {                    
                    
                    next = "/jsp/general/exportar/facturas_clientes/ReporteFacturasClientes.jsp?msg=El Codigo del Cliente que fue digitado NO EXISTE!";
                    
                    error = true;
                    
                }
                
            }
            
            if ( !error ) {
                
                model.reporteFacturasClientesService.reporte ( codcli, fecini, fecfin, numfac, numrem, usu_creacion, fec_creacion, opcion ,prov,est, fecven,est1,multi);
                //model.reporteFacturasClientesService.reporte ( codcli, fecini, fecfin, numfac, numrem, usu_creacion, fec_creacion, opcion ,prov,est, fecven,est1);
                
                datos = model.reporteFacturasClientesService.getVectorReporte (); 

                if ( datos.size () > 0 ) {

                    if ( hacer.equals ( "2" ) ) {

                        HiloReporteFacturasClientes HRFC = new HiloReporteFacturasClientes (model);
                        HRFC.start ( datos, usuario.getLogin () ); 

                        next = "/jsp/general/exportar/facturas_clientes/ReporteFacturasClientes.jsp?msg=Archivo exportado a excel!!";

                    }

                } else { 

                    next = "/jsp/general/exportar/facturas_clientes/ReporteFacturasClientes.jsp?msg=Su busqueda no arrojo resultados!";

                }  
                
            }
            
        } catch ( Exception e ) {
            
            e.printStackTrace();
            throw new ServletException ( "Error en el Reporte de Facturas de un Cliente Action : " + e.getMessage () );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}