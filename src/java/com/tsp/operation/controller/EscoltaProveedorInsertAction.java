/************************************************************************************
 * Nombre clase : ............... EscoltaProveedorInsertAction.java                 *
 * Descripcion :................. Clase que maneja los eventos relacionados con el  *
 *                                ingreso de Proveedores de escolta                 * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 26 de Noviembre de 2005, 11:30 AM                 *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ***********************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class EscoltaProveedorInsertAction extends Action{    
    
    public EscoltaProveedorInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String next = "/jsp/masivo/proveedor_escolta/proveedorInsert.jsp?msg=";
        String nit = request.getParameter("nit");        
        String origen = request.getParameter("origen");        
        String destino = request.getParameter("destino");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        Proveedor_Escolta prov =  new Proveedor_Escolta();
        prov.setNit(nit);
        prov.setOrigen(origen);
        prov.setDestino(destino);
        try{
            if (model.identidadService.existeIdentidad(nit)) {
                if(!model.proveedorEscoltaService.existeProveedorEscolta(prov)) {                
                    prov.setTarifa(Double.parseDouble(request.getParameter("tarifa")));
                    prov.setCod_contable(request.getParameter("codigo"));
                    prov.setCreation_user(usuario.getLogin());
                    prov.setDstrct((String)session.getAttribute("Distrito"));
                    prov.setBase(usuario.getBase());
                    model.proveedorEscoltaService.insertarProveedorEscolta(prov);
                    next+="Proveedor Agregado exitosamente!";
                } else {
                    next+="Ya existe una tarifa de este proveedor con este origen y destino";
                }
            } else {
                next+="El nit no esta registrado en la tabla de identidad";
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
