/*
 * RTOpcionesAction.java
 *
 * Created on 30 de noviembre de 2004, 01:55 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Mario Fontalvo
 */
public class RegistroTiempoOpcionesAction extends Action {

    /** Creates a new instance of RTOpcionesAction */
    public RegistroTiempoOpcionesAction() {
    }

    public void run() throws ServletException, InformationException {
        try{
            // datos de Buscar
            String Mensaje = "";
            String CodigoPlanilla  = request.getParameter("CodigoPlanilla");
            
            // datos para Grabar o Actualizar
            String Distrito    = request.getParameter("Distrito");
            String Planilla    = request.getParameter("Planilla");
            String Sj          = request.getParameter("Sj"); 
            String CF_Code     = request.getParameter("CF_Code"); 
            String T_Code      = request.getParameter("T_Code");
            String Secuence    = request.getParameter("Secuence");
            String DTT         = request.getParameter("DTT"); 
            String D_Code      = request.getParameter("D_Code");  
            String Observacion = request.getParameter("Observacion"); 
            String Demora      = request.getParameter("Demora"); 
            
                       
            //String  login     = request.getParameter("Usuario"); 
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            String      login   = usuario.getLogin();
                        
            // opcion elegida
            String Opcion          = request.getParameter("Opcion");


            if (Opcion.equals("Buscar")){
                model.RegistroTiempoSvc.BuscarDatosPlanilla(CodigoPlanilla.toUpperCase());
                if (model.RegistroTiempoSvc.getDatosPlanilla()!=null){
                    CodigoPlanilla = model.RegistroTiempoSvc.getDatosPlanilla().getNumeroPlanilla();
                
                    model.RegistroTiempoSvc.BuscarPlanillaTiempo(CodigoPlanilla.toUpperCase());
                    model.RegistroTiempoSvc.BuscarListaDelay(CodigoPlanilla.toUpperCase());
                }
                else
                    Mensaje="No se encontro la Planilla"; 
                //if (model.RegistroTiempoSvc.getDatosPlanilla()==null)
                    //Mensaje="No se encontro la Planilla"; 
                    
            }
            else if (Opcion.equals("Nuevo")) {
                model.RegistroTiempoSvc.ReiniciarPlanilla();
                model.RegistroTiempoSvc.ReiniciarListaTBLTiempo();
                model.RegistroTiempoSvc.ReiniciarListaPlanillaTiempos();
                model.RegistroTiempoSvc.ReiniciarListaDelay();
            }
            else if( Opcion.equals("Grabar")){
                model.RegistroTiempoSvc.Insertar(Distrito, Planilla, Sj, CF_Code, T_Code, Secuence, DTT, D_Code, Observacion,Demora, login);
                Mensaje="Registro Grabado";
                model.RegistroTiempoSvc.ReiniciarListaTBLTiempo();
                model.RegistroTiempoSvc.ReiniciarListaPlanillaTiempos();
                model.RegistroTiempoSvc.BuscarPlanillaTiempo(Planilla);                
            }
            else if (Opcion.equals("Modificar")){
                model.RegistroTiempoSvc.Modificar(Distrito, Planilla, Sj, CF_Code, T_Code, Secuence, DTT, D_Code, Observacion, Demora, login);
                Mensaje="Registro Actualizado";
                model.RegistroTiempoSvc.ReiniciarListaTBLTiempo();
                model.RegistroTiempoSvc.ReiniciarListaPlanillaTiempos();
                model.RegistroTiempoSvc.BuscarPlanillaTiempo(Planilla);
            }

            final String next = "/registrotiempo/registrotiempo.jsp?Mensaje="+Mensaje;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
		throw new Exception("No se pudo encontrar "+ next);
	    rd.forward(request,response);

        }catch(Exception ex){
            throw new ServletException("Error en RegistroTiempoOpcionesAction: \n"+ex.getMessage());
        }

    }

}
