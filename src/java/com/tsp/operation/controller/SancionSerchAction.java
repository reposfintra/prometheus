/***********************************************
 * Nombre clase: SancionSerchAction.java
 * Descripci�n: Accion para buscar una sanci�n a la bd.
 * Autor: Jose de la rosa
 * Fecha: 19 de octubre de 2005, 06:29 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class SancionSerchAction extends Action{
    
    /** Creates a new instance of SancionSerchAction */
    public SancionSerchAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        int cod_sancion = 0;
        String tipo_sancion = "";
        String numpla = "";
        String listar = (String) request.getParameter ("listar");
        try{
            if (listar.equals ("True")){
                next="/jsp/cumplidos/sancion/SancionListar.jsp";
                next = Util.LLamarVentana (next, "Listar Sanci�n");
                String sw = (String) request.getParameter ("sw");
                if(sw.equals ("True")){
                    tipo_sancion = request.getParameter ("c_tipo_sancion");
                    numpla = (request.getParameter ("c_numpla").toUpperCase ());
                }
                else{
                    tipo_sancion = "";
                    numpla = "";
                }                
                session.setAttribute ("tipo_sancion", tipo_sancion);
                session.setAttribute ("numpla", numpla);
            }
            else{
                cod_sancion = Integer.parseInt (request.getParameter ("c_cod_sancion").toUpperCase ());
                numpla = (request.getParameter ("c_numpla").toUpperCase ());
                model.sancionService.searchSancion (numpla, cod_sancion);
                Sancion sanc = model.sancionService.getSancion ();
                next="/jsp/cumplidos/sancion/SancionModificar.jsp?msg=";
                next = Util.LLamarVentana (next, "Modificar Sanci�n");
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
