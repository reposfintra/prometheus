/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
**/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;


public class NegociosIngresoAction  extends Action{
    
   //protected HttpServletRequest request;
    public NegociosIngresoAction () {
    }
    
    
    
    public void run() throws ServletException, InformationException
    { 
       HttpSession session  = request.getSession();
       Usuario usuario = (Usuario)session.getAttribute("Usuario");
       String next="";
       
       String op=request.getParameter ("op");
       if(op.equals("1"))
       {
            Vector fp1=(Vector)session.getAttribute("head");
            String[][] bod=(String[][])session.getAttribute("body");
            ListadoIFenalcoTh hilo = new ListadoIFenalcoTh();
            hilo.start(model,fp1,bod, usuario);   
            next = "/jsp/fenalco/negocios/analisisCarteraMsg.jsp";
       }
       else
       {
           String ckest=null,est="",ckdate=null,fi="",ff="",cknita=null,nita="",cknoma=null,noma="",cknitc=null,nitc="",cknomc=null,nomc="";
           String ckfap=null,fi1="",ff1="",ckfde=null,fi2="",ff2="";
           next="/jsp/fenalco/contabilizacion/ConsultarRep.jsp";
           Vector fp=new Vector(); 
           ckdate=request.getParameter ("ckdate"); 
           fi=(request.getParameter ("fecini") != null )?request.getParameter ("fecini").toUpperCase():"";
           ff=(request.getParameter ("fecfin") != null )?request.getParameter ("fecfin").toUpperCase():"";
           cknitc=request.getParameter ("cknitc");
           nitc=(request.getParameter ("nitc") != null )?request.getParameter ("nitc").toUpperCase():"";
           cknomc=request.getParameter ("cknomc");
           nomc=(request.getParameter ("nomc") != null )?request.getParameter ("nomc").toUpperCase():"";
            try{
                model.Negociossvc.LDim(fi,ff); 
                model.Negociossvc.LCod(fi,ff); 
            }
            catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
       }
       this.dispatchRequest(next);
    }
    
}
