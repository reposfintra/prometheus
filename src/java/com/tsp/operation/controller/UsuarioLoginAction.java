/**
 * ******************************************************************
 * Nombre Clase................. UsuarioLoginAction.java
 * Descripci�n.................. Accion que permite obtener los perfiles de un
 * usuario Autor........................ David Pi�a L�pez
 * Fecha........................ 12 de julio de 2006, 10:13 AM
 * Versi�n...................... 1.0 Copyright.................... Transportes
 * Sanchez Polo S.A.
 ******************************************************************
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.operation.model.services.PerfilService;
import java.sql.SQLException;
import java.util.Vector;
import javax.servlet.*;

/**
 *
 * @author David
 */
public class UsuarioLoginAction extends Action {

    /**
     * Creates a new instance of UsuarioLoginAction
     */
    public UsuarioLoginAction() {
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //System.out.println("Usuario login");
        String next = "";
        try {
            int opcion = Integer.parseInt((request.getParameter("opcion")!=null?request.getParameter("opcion"):"0"));
            String usuario = request.getParameter("login");

            switch (opcion) {
                case 1:
                    getPerfilesUsuario(usuario);

                    break;
                case 2:
                    getCiaUsuario(usuario);
                    break;
                default:
                    //System.out.println("en UsuarioLoginAction"+usuario);
                    PerfilService perfilService = new PerfilService();
                    perfilService.getPerfilesUsuario(usuario);
                    request.setAttribute("v_perfiles", perfilService.getVPerfil());
                    next = "/listaPerfiles.jsp";
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        if (!next.equals("")) {
            this.dispatchRequest(next);
        }
    }

    private void getPerfilesUsuario(String login) throws Exception {
        try {
            String distrito = request.getParameter("distrito");
            String proyecto = request.getParameter("proyecto");
            PerfilService perfilService = new PerfilService();
            perfilService.getPerfilesUsuario(login, distrito, proyecto);
            Vector vPerfil = perfilService.getVPerfil();

            Gson gson = new Gson();
            String json = gson.toJson(vPerfil);
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

    }

    private void getCiaUsuario(String login) throws Exception {
        try {
            PerfilService perfilService = new PerfilService();
            perfilService.getCompania(login);
            Vector vPerfil = perfilService.getVPerfil();

            Gson gson = new Gson();
            String json = gson.toJson(vPerfil);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    public void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

}
