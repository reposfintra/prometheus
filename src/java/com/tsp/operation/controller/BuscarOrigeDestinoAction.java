/*
 * BuscarStandardColpapelAction.java
 *
 * Created on 23 de abril de 2005, 02:29 PM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class BuscarOrigeDestinoAction extends Action{
    
    static Logger logger = Logger.getLogger(BuscarStandardColpapelAction.class);
    /** Creates a new instance of BuscarStandardColpapelAction */
    public BuscarOrigeDestinoAction() {
        
        
    }
    public void run() throws ServletException, InformationException {
        
        String next = "/jsp/trafico/planviaje/control_excepcion/Control_execpcion.jsp";
        String op = request.getParameter("op")==null?"":request.getParameter("op");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String base =usuario.getBase();
        model.RemDocSvc.setDocumentos(null);//Juan 12.11.05
        String stdjob = "";
        try{
            
            String cliente = request.getParameter("cliente").toUpperCase();
            logger.info("Cliente "+cliente);
            model.stdjobdetselService.searchCliente(cliente);
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                logger.info("Encontro el cliente");
                Stdjobdetsel sj = model.stdjobdetselService.getStandardDetSel();
                request.setAttribute("cliente",sj.getCodCliente()+"-"+sj.getCliente());
                request.setAttribute("agency",sj.getAg_cliente());
            }
            else{
                logger.info("Nada NO Encontro el cliente");
            }
            if(op.equals("1")){
                
                if(cliente.substring(0,3).equals("000")){
                    String std = cliente;
                    TreeMap t = new TreeMap();
                    model.stdjobdetselService.setCiudadesDest(t);
                    model.stdjobdetselService.setCiudadesOri(t);
                    model.stdjobdetselService.setStdjobTree(t);
                    model.stdjobdetselService.searchRutasCliente(std);
                    //System.out.println(""+std);
                    
                }
                else{
                    model.stdjobdetselService.searchStdJob(cliente);
                    if(model.stdjobdetselService.getStandardDetSel()!=null){
                        if(model.stdjobdetselService.getStandardDetSel()!=null){
                            Stdjobdetsel sj = model.stdjobdetselService.getStandardDetSel();
                            request.setAttribute("std", sj.getSj()+"-"+sj.getSj_desc());
                            request.setAttribute("sj", sj.getSj());
                            stdjob =sj.getSj();
                            
                        }
                    }
                }
            }
            else if(op.equals("2")){
                String std = cliente;
                String origen = request.getParameter("origen");
                model.stdjobdetselService.searchDestinosOrigen(std, origen);
            }
            else if(op.equals("3")){
                
                String std = cliente;
                String origen = request.getParameter("origen");
                String destino =request.getParameter("destino");
                model.stdjobdetselService.searchStandaresOrDest(std, origen, destino);
                
                
                request.setAttribute("ok", "ok");
            }
            
            else{
                try{
                    model.imprimirOrdenService.setHoc(null);
                    stdjob = request.getParameter("standard");
                     model.stdjobcostoService.searchRutas(stdjob);
                     model.stdjobdetselService.searchStdJob(stdjob);
                    model.cliente_ubicacionService.listarUbicaciones("000"+stdjob.substring(0,3));
                    model.tblgensvc.buscarListaCodigo("AUTEXFLETE", "000"+stdjob.substring(0,3));
                    model.ImagenSvc.resetImagesDespacho(usuario.getLogin());
                    model.anticiposService.vecAnticipos("",stdjob);
                    model.anticiposService.searchAnticiposProveedor(usuario);
                    model.productoService.setProductos(null);
                    model.remidestService.searchCiudades(stdjob.substring(0,3));
                    model.RemDocSvc.setDocumentos(new Vector(5));
                    model.RemDocSvc.setDocs(null);
                    
                    if(request.getParameter("remesa")!=null){
                        
                        String planilla=request.getParameter("planilla");
                        List remesas= model.planillaService.buscarRemesas(planilla);
                        int i =remesas.size();
                        if(i>0){
                            Remesa remesa = (Remesa)remesas.get(i-1);
                            String clienteRemesa = remesa.getCodcli();
                            cliente = "000"+stdjob.substring(0,3);
                             
                            if(cliente.equals(clienteRemesa)){
                                
                                if(!model.stdjobdetselService.getStandardDetSel().getOrigin_code().equals(remesa.getOriRem())){
                                    remesa.setRemitente(null);
                                }
                                
                                model.remesaService.setRemesa(remesa);
                                
                              }
                            else{
                                  model.remesaService.setRemesa(null);
                            }
                            
                        }
                        
                       
                    }
                }catch(Exception ex){
                    throw new ServletException(ex.getMessage());
                }
            }
            
            
            logger.info("Buscar costos de :"+stdjob);
            model.sjextrafleteService.listaCostos(stdjob,usuario.getId_agencia());
            model.sjextrafleteService.listaFletes(stdjob,usuario.getId_agencia());
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
