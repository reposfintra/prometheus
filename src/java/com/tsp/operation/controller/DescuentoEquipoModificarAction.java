/********************************************************************
* Nombre ......................DescuentoEquipoModificarAction.java  *
* Descripci�n..................Clase Action de descuento de equipos *
* Autor........................Armando Oviedo                       *
* Fecha Creaci�n...............06/12/2005                           *
* Modificado por...............LREALES                              *
* Fecha Modificaci�n...........22/05/2006                           *
* Versi�n......................1.0                                  *
* Coyright.....................Transportes Sanchez Polo S.A.        *
********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class DescuentoEquipoModificarAction extends Action{
    
    /** Creates a new instance of DescuentoEquipoModificarAction */
    public DescuentoEquipoModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next = "/jsp/equipos/descuentos_equipo/ModificarDescuentoEquipo.jsp";
        
        try{
            
            String mensaje = request.getParameter("mensaje");
            HttpSession session = request.getSession();  
            Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
            String user = usuario.getLogin().toUpperCase();
            
            String codigo = ( request.getParameter("codigo") != null )?request.getParameter("codigo").toUpperCase():"";
            String descripcion = ( request.getParameter("descripcion") != null )?request.getParameter("descripcion"):"";
            String conc_contable = ( request.getParameter("conc_contable") != null )?request.getParameter("conc_contable"):"";
            String conc_especial = ( request.getParameter("conc_especial") != null )?request.getParameter("conc_especial"):"";
            
            if( mensaje != null ){
                
                if( mensaje.equalsIgnoreCase("modificar") ){
                                        
                    DescuentoEquipo tmp = new DescuentoEquipo();
                    
                    tmp.setCodigo( codigo );
                    tmp.setDescripcion( descripcion );
                    tmp.setConcContable( conc_contable );
                    tmp.setConcEspecial( conc_especial );
                    tmp.setUserUpdate( user );
                    tmp.setLastUpdate( "now()" );
                    
                    model.descuentoequiposvc.setDE( tmp );
                    model.descuentoequiposvc.updateDE();  
                    
                    next += "?reload=ok&mensajemod=Descuento de equipo modificado correctamente";
                    
                } else if( mensaje.equalsIgnoreCase("eliminar") ){
                    
                    DescuentoEquipo tmp = new DescuentoEquipo();
                    tmp.setCodigo( codigo );
                    
                    model.descuentoequiposvc.setDE( tmp );
                    model.descuentoequiposvc.deleteDE();
                    
                    next += "?reload=ok&mensajemod=Descuento de equipo eliminado correctamente";
                    
                }
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}