/*
 * GenerarReporteInterfaceContable.java
 *
 * Created on 5 de septiembre de 2005, 03:48 PM
 */

package com.tsp.operation.controller;

import com.tsp.operation.model.HExport_Migracion;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Alejandro
 */
public class GenerarReporteInterfaceContableAction extends Action {
    
    /** Creates a new instance of GenerarReporteInterfaceContable */
    public GenerarReporteInterfaceContableAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            Serie serie = model.servicioSerie.obtenerSerie( "006" );
            
            if (serie!=null){
                HExport_Migracion hilo = new HExport_Migracion(usuario.getBd());
                hilo.start(usuario.getLogin());
                request.setAttribute("mensaje", "El proceso fue inciado correctamente!");
            }
            else {
                request.setAttribute("mensaje", "No se pudo iniciar el proceso no se encontro SERIE.");
            }
            this.dispatchRequest("/migracion/EjecutarReporteExtrafleteyAjustes.jsp");
        } catch (Exception ex){
            throw new ServletException( ex.getMessage() );
        }
        
        
    }
    
}
