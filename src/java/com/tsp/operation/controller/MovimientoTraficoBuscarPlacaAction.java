/******************************************************************
 * Nombre ...................... MovimientoTraficoBuscarPlacaAction.java
 * Descripci�n.................. Busca el movimiento en tr�fico correspondientes a una planilla.
 * Autor........................ Ing. Andr�s Maturana De La Cruz
 * Fecha........................ 24 de septiembre de 2005
 * Versi�n...................... 1.0
 * Coyright..................... Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

import org.apache.log4j.*;


/**
 *
 * @author  Tito Andr�s
 */
public class MovimientoTraficoBuscarPlacaAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of SancionBuscarPlanillaAction */
        public MovimientoTraficoBuscarPlacaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next  = "";
        
        String placa = request.getParameter("placa");
        String planilla = request.getParameter("numpla");
        
        logger.info("PLACA PARAM: " + placa);
        logger.info("PLANILLA PARAM: " + planilla);
        
        try{             
            
            if( planilla == null ){

                placa = placa.toUpperCase();
                model.traficoService.obtenerIngreso_TraficoPlaca(placa);
                Vector reg = model.traficoService.getRegistros();
                logger.info("REGISTROS EN INGRESO TRAFICO: " + reg.size());
                
                if( reg.size() == 0 ){
                    // MENSAJE
                    next  = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoConsultaPlaca.jsp?marco=no&msg=La placa no registra en Ingreso Tr�fico";
                } else if ( reg.size() > 1 ){
                    request.setAttribute("datos", reg);
                    next  = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoConsultaPlaca.jsp?marco=no";
                } else {
                    Ingreso_Trafico ingreso = (Ingreso_Trafico) reg.elementAt(0);
                    String numpla = ingreso.getPlanilla();
                    model.rmtService.BuscarReportesPlanilla(numpla);
                    model.traficoService.setIngreso(ingreso);
                    logger.info(".................... MOVIMIENTO TRAFICO x PLACA: " + model.rmtService.getReportesPlanilla().size());
                    
                    String escoltas = model.traficoService.obtenerEscoltasVehiculo(numpla) + ", " + model.traficoService.obtenerEscoltasCaravana(numpla);
                    escoltas = escoltas.trim();
                    if( escoltas.startsWith(",") ) escoltas = escoltas.substring(1);
                    if( escoltas.endsWith(",") ) escoltas = escoltas.substring(0, escoltas.length() - 1);
                    String caravana = model.traficoService.obtenerNumeroCaravana(numpla);
                    ingreso.setEscolta(escoltas);
                    ingreso.setCaravana(caravana);
                    model.traficoService.setIngreso(ingreso);
                    
                    next  = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoConsulta.jsp?marco=no";
                }
            } else {
                planilla = planilla.toUpperCase();
                model.traficoService.obtenerIngreso_Trafico(planilla);
                String numpla = planilla;
                model.rmtService.BuscarReportesPlanilla(numpla);
                Ingreso_Trafico ingreso = model.traficoService.getIngreso();
                logger.info(".................... MOVIMIENTO TRAFICO x PLANILLA: " + model.rmtService.getReportesPlanilla().size());
                
                String escoltas = model.traficoService.obtenerEscoltasVehiculo(numpla) + ", " + model.traficoService.obtenerEscoltasCaravana(numpla);
                escoltas = escoltas.trim();
                if( escoltas.startsWith(",") ) escoltas = escoltas.substring(1);
                if( escoltas.endsWith(",") ) escoltas = escoltas.substring(0, escoltas.length() - 1);
                String caravana = model.traficoService.obtenerNumeroCaravana(numpla);
                ingreso.setEscolta(escoltas);
                ingreso.setCaravana(caravana);
                model.traficoService.setIngreso(ingreso);
                
                next  = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoConsulta.jsp?marco=no";
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
