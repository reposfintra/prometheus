/*
 * MonedaBuscarAction.java
 *
 * Created on 25 de octubre de 2005, 02:38 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class MonedaBuscarAction extends Action{
        
        /** Creates a new instance of MonedaBuscarAction */
        public MonedaBuscarAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
                String cod = request.getParameter("codigo");
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");  
                
                try{
                        model.monedaSvc.buscarMoneda(cod);
                        next=next+"?sw=1";
                }catch (SQLException e){
                        throw new ServletException(e.getMessage());
                }
                this.dispatchRequest(Util.LLamarVentana(next,"Modificar Moneda"));
        }
        
}
