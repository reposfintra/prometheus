/***********************************************
 * Nombre clase :  CorridaSearchAction.java    *
 * Autor : Ing. Henry A.Osorio Gonz�lez        *
 * Modificado Ing Sandra Escalante             *
 * Fecha : 12 de diciembre de 2005, 16:30 AM   *
 * Copyright : Fintravalores S.A.         *
 ***********************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.ReporteExportarFlotaUtilizadaXLS;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import java.util.Hashtable;

public class CorridaSearchAction extends Action{
    
    public CorridaSearchAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String next = "/jsp/cxpagar/corridas/corridasSearch.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        String agencia = (String) session.getAttribute("Agencia");
        
        String cmd = getParameter("cmd","");
        
        try{
            
            if (cmd.equalsIgnoreCase("busqueda")){
                String nro = this.getParameter("nrocorrida","");
                model.corridaService.buscarCorridaBancos(dstrct, usuario.getId_agencia(), nro,false);
                session.setAttribute("nrocorrida", nro);
                
            }else if (cmd.equalsIgnoreCase("DELETE_CORRIDA")){
                String nro = (String) session.getAttribute("nrocorrida");
                String valor = (request.getParameter("valor")!=null)?request.getParameter("valor").replaceAll(",",""):"0"; 
                double vlr = Double.parseDouble(valor);
                model.corridaService.excluirValoresMenores(dstrct,  nro, vlr);
                model.corridaService.buscarCorridaBancos(dstrct, usuario.getId_agencia(), nro,false);
                
            }else if (cmd.equalsIgnoreCase("busquedaProveedor")){
                next = "/jsp/cxpagar/corridas/BancoProveedor.jsp";
                String nro = this.getParameter("nrocorrida","");
                String banco        = request.getParameter("banco");
                String sucursal     = request.getParameter("sucursal");
                String AgenciaBanco = request.getParameter("agencia");
                model.corridaService.buscarBancoProveedor(dstrct, AgenciaBanco, nro,banco,sucursal,false);
                session.setAttribute("nrocorrida", nro);
                session.setAttribute("bancoSucursal", banco+" - "+sucursal);
                session.setAttribute("banco", banco);
                session.setAttribute("sucursal", sucursal);
                session.setAttribute("agenciaBanco", AgenciaBanco);
                
            }else if (cmd.equalsIgnoreCase("busquedaFactura")){
                next = "/jsp/cxpagar/corridas/ProveedorFactura.jsp";
                String nro = this.getParameter("nrocorrida","");
                String banco    = request.getParameter("banco")!=null?request.getParameter("banco"):"";
                String sucursal = request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"";
                String NitPro   = request.getParameter("nitPro")!=null?request.getParameter("nitPro"):"";
                String Nombre   = request.getParameter("nombre")!=null?request.getParameter("nombre"):"";
                String HC       = request.getParameter("hc")!=null?request.getParameter("hc"):"";
                String AgenciaBanco = (String) session.getAttribute("agenciaBanco");
                model.corridaService.buscarProveedorFactura(dstrct, AgenciaBanco, nro,banco,sucursal,NitPro,false);
                session.setAttribute("nrocorrida", nro);
                session.setAttribute("bancoSucursal", banco+" - "+sucursal);
                session.setAttribute("codigo", NitPro);
                session.setAttribute("nombre", Nombre);
                session.setAttribute("hc", HC);
                
                
            }else if (cmd.equalsIgnoreCase("AutorizarCorridas")){
                String msg ="Las Corridas fueron modificadas con exito..";
                next = "/jsp/cxpagar/corridas/mostrarCorridas.jsp?msg="+msg;
                String[]CorridasSelec = request.getParameterValues("corridasSelec");
                List listado = model.corridaService.getListado();
                List CorridasNoSelec = new LinkedList();
                Iterator it = listado.iterator();
                while(it.hasNext()){
                    Corrida corrida = (Corrida) it.next();
                    boolean esta = false;
                    if(CorridasSelec != null){
                        for(int i=0;i<CorridasSelec.length;i++){
                            if(corrida.getCorrida().equals(CorridasSelec[i])){
                                esta = true;
                            }
                        }
                        if(!esta){
                            CorridasNoSelec.add(corrida);
                        }
                    }else{
                        CorridasNoSelec.add(corrida);
                    }
                }
                model.corridaService.autorizarCorrida(usuario.getLogin(), CorridasSelec, CorridasNoSelec);
                model.corridaService.buscarCorridasUsuarios(usuario.getLogin(), usuario.getDstrct(), usuario.getId_agencia());
                
            }else if (cmd.equalsIgnoreCase("AutorizarBancos")){
                String nro = (String) session.getAttribute("nrocorrida");
                String msg ="Los bancos de la corrida fueron modificadas con exito..";
                next = "/jsp/cxpagar/corridas/corridasSearch.jsp?msg="+msg;
                String[]Bancos = request.getParameterValues("bancosSelec");
                
                List listado = model.corridaService.getListado();
                List BancosNoSelec = new LinkedList();
                List BancosSelec   = new LinkedList();
                Iterator it = listado.iterator();
                while(it.hasNext()){
                    Corrida corrida = (Corrida) it.next();
                    boolean esta = false;
                    if(Bancos != null){
                        for(int i=0;i<Bancos.length;i++){
                            String[]vec = Bancos[i].split(",");
                            if(corrida.getBanco().trim().equals(vec[0].trim()) && corrida.getSucursal().trim().equals(vec[1].trim())&& corrida.getAgencia().trim().equals(vec[2].trim())){
                                esta = true;
                            }
                        }
                        if(!esta){
                            BancosNoSelec.add(corrida);
                        }else{
                            BancosSelec.add(corrida);
                        }
                    }else{
                        BancosNoSelec.add(corrida);
                    }
                }
                model.corridaService.autorizarBanco(usuario.getLogin(), BancosSelec, BancosNoSelec);
                model.corridaService.buscarCorridaBancos(dstrct, usuario.getId_agencia(), nro,false);
                
            }else if (cmd.equalsIgnoreCase("AutorizarProveedor")){
                String nro      = (String) session.getAttribute("nrocorrida");
                String banco    = (String) session.getAttribute("banco");
                String sucursal = (String) session.getAttribute("sucursal");
                String AgenciaBanco = (String) session.getAttribute("agenciaBanco");
                String msg ="Los Provedores del Banco asociados a la corrida fueron modificados con exito..";
                next = "/jsp/cxpagar/corridas/BancoProveedor.jsp?msg="+msg;
                String[]Proveedor = request.getParameterValues("ProveedorSelec");
                
                List listado = model.corridaService.getListado();
                List ProveedorNoSelec = new LinkedList();
                List ProveedorSelec   = new LinkedList();
                Iterator it = listado.iterator();
                while(it.hasNext()){
                    Corrida corrida = (Corrida) it.next();
                    boolean esta = false;
                    if(Proveedor != null){
                        for(int i=0;i<Proveedor.length;i++){
                            if(corrida.getNitProveedor().equals(Proveedor[i])){
                                esta = true;
                            }
                        }
                        if(!esta){
                            ProveedorNoSelec.add(corrida);
                        }else{
                            ProveedorSelec.add(corrida);
                        }
                    }else{
                        ProveedorNoSelec.add(corrida);
                    }
                }
                model.corridaService.autorizarProveedor(dstrct , usuario.getLogin(), ProveedorSelec, ProveedorNoSelec);
                model.corridaService.buscarBancoProveedor(dstrct, AgenciaBanco, nro,banco,sucursal,false);
                
            }else if (cmd.equalsIgnoreCase("AutorizarFactura")){
                String nro          = (String) session.getAttribute("nrocorrida");
                String banco        = (String) session.getAttribute("banco");
                String sucursal     = (String) session.getAttribute("sucursal");
                String AgenciaBanco = (String) session.getAttribute("agenciaBanco");
                String NitPro       = (String) session.getAttribute("codigo");
                String msg ="Las facturas fueron modificadas con exito..";
                next = "/jsp/cxpagar/corridas/ProveedorFactura.jsp?msg="+msg;
                String[]Factura = request.getParameterValues("FacturaSelec");
                
                List listado = model.corridaService.getListado();
                List FacturaNoSelec = new LinkedList();
                List FacturaSelec   = new LinkedList();
                Iterator it = listado.iterator();
                while(it.hasNext()){
                    Corrida corrida = (Corrida) it.next();
                    boolean esta = false;
                    if(Factura != null){
                        for(int i=0;i<Factura.length;i++){
                            if(corrida.getDocumento().equals(Factura[i].replaceAll("-_-","#"))){
                                esta = true;
                            }
                        }
                        if(!esta){
                            FacturaNoSelec.add(corrida);
                        }else{
                            FacturaSelec.add(corrida);
                        }
                    }else{
                        FacturaNoSelec.add(corrida);
                    }
                }
                model.corridaService.autorizarFactura(dstrct , usuario.getLogin(), FacturaSelec, FacturaNoSelec);
                model.corridaService.buscarProveedorFactura(dstrct, AgenciaBanco, nro,banco,sucursal,NitPro,false);
            }
            
            //System.out.println("NEXT ------- " + next);
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    /**
     *  Metodo getParameter, permite dar formato a un parametro nulo
     *  @autor Mario Fontalvo
     *  @param nombre del parametro, valor en caso que valor sea nulo
     *  @return String
     *  @version 1.0
     */
    public String getParameter(String str1, String str2){
        return (request.getParameter(str1)==null?str2:request.getParameter(str1));
    }
    
}
