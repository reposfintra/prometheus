/******************************************************************
* Nombre ......................InventarioSoftwareGenerarAction.java
* Descripci�n..................Clase Action para Inventario De Software
* Autor........................Armando Oviedo
* Fecha........................27/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class InventarioSoftwareGenerarAction extends Action{
    
    /** Creates a new instance of InventarioSoftwareGenerarAction */
    public InventarioSoftwareGenerarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/general/inventario/reporteInventarioSoftware.jsp?mensaje=Reporte generado exitosamente";
        try{            
            
            HttpSession session    =  request.getSession();            
            Usuario usuario        =  (Usuario) session.getAttribute("Usuario");
            ResourceBundle  rb     =  ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String  ruta           =  rb.getString("ruta"); 
            ReporteInventarioSoftwareXLS ris      = new ReporteInventarioSoftwareXLS();
            
            String  checkdb        =  request.getParameter("grabarBD");                        
            String equipo          =  request.getParameter("equipo");
            String sitio           =  request.getParameter("sitio");
            
                        
            
         // Seteamos en el service el equipo y el sitio
            model.repinvsvc.setEquipo(equipo);
            model.repinvsvc.setSitio(sitio);
            
            
            if(checkdb!=null){
                model.repinvsvc.deleteTablaInventarioSoftware();
            }
            
                        
            boolean existe = model.repinvsvc.existeReporte();      // Verifica que halla registros en la base de datos 
            
                                    
            if(!existe){           
                model.repinvsvc.setListaProgramas(new Vector());                
                model.repinvsvc.buscarListaProgramas( ruta ,usuario);
                model.repinvsvc.grabarInventario();                
                ris.start(usuario, model.repinvsvc.getListaProgramas());
            }
            else{                
                model.repinvsvc.obtenerReporteBaseDatos();                
                ris.start(usuario, model.repinvsvc.getVectorBaseDatos());
            }     
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
