/**
 * Autor : Ing. Roberto Rocha P.. Date : 10 de Julio de 2007 Copyrigth Notice :
 * Fintravalores S.A. S.A Version 1.0 --> <%-- -@(#) --Descripcion : Action que
 * maneja los avales de Fenalco
 *
 */
package com.tsp.operation.controller;

import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.opav.model.DAOS.ApplusDAO;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import javax.servlet.http.*;

public class NegociosInsertAction extends Action {

    GestionSolicitudAvalService gsaserv;
    Usuario usuario = null;

    public NegociosInsertAction() {
    }

    public void run() throws ServletException, InformationException {
        //OPCIONES PARA RELIQUIDAIONES
        // 1--> reactivacion de un negocio en convenio difernte
        // 2--> reactivacion de un negocio rechazado por datacredito max limite endeudamiento
        HttpSession session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        //verificamos por la session por donde entramos 
        String login_into=(String)session.getAttribute("login_into");
        
        gsaserv = new GestionSolicitudAvalService(usuario.getBd());
        Negocios neg = new Negocios();
        String next = "/jsp/fenalco/liquidadores/prueba.jsp";
        Vector batch = new Vector();
        ApplusDAO apdao = new ApplusDAO(usuario.getBd());
        String opcion = (request.getParameter("opcion") != null ? request.getParameter("opcion") : "");
        String act_liq = (request.getParameter("act_liq") != null ? request.getParameter("act_liq") : "");
        String radicacion= request.getParameter("radicacion")!=null?request.getParameter("radicacion"):"S";
        
        neg.setCod_cli(request.getParameter("nit") != null ? request.getParameter("nit") : "");
        neg.setMod_aval((String) request.getParameter("aval1"));
        neg.setMod_cust((String) request.getParameter("cust1"));
        neg.setMod_rem((String) request.getParameter("rem"));
        neg.setVr_negocio(Double.parseDouble((String) request.getParameter("vrnegocio")));
        neg.setVr_desem(Double.parseDouble((String) request.getParameter("vrdesem")));
        neg.setVr_aval(Double.parseDouble((String) request.getParameter("vraval") != null ? request.getParameter("vraval") : "0"));
        neg.setVr_cust(Double.parseDouble((String) request.getParameter("vrcust") != null ? request.getParameter("vrcust") : "0"));
        neg.setPor_rem(Double.parseDouble((String) request.getParameter("porcrem") != null ? request.getParameter("porcrem") : "0"));
        neg.setNodocs(Integer.parseInt((String) request.getParameter("nodocs")));
        neg.setCreation_user(usuario.getLogin());
        neg.setDist(usuario.getDstrct());
        neg.setTneg((String) request.getParameter("tneg"));
        neg.setFpago((String) request.getParameter("formpago"));
        neg.setCod_tabla((String) request.getParameter("codtab"));
        neg.setEsta((String) request.getParameter("remesas"));
        neg.setFecha_neg((String) request.getParameter("fechaneg"));
        neg.setFecha_liquidacion((String) request.getParameter("fechaliq") != null ? request.getParameter("fechaliq") : neg.getFecha_neg());
        neg.setNitp((String) request.getParameter("nitp"));
        neg.setTotpagado(Double.parseDouble((String) request.getParameter("tpag")));
        neg.setCodigoBanco((String) request.getParameter("banco") != null ? request.getParameter("banco") : "0");
        // neg.setFrecuencia_cuota(Integer.parseInt(request.getParameter("frecuenciapago") != null ? request.getParameter("frecuenciapago") : "0"));
        neg.setvalor_aval((String) request.getParameter("valor_aval"));
        neg.setvalor_remesa((String) request.getParameter("valor_remesa"));

        String numero_solicitud = request.getParameter("numero_solicitud");
        String numero_form = request.getParameter("numsolc");
        String ciclo = request.getParameter("ciclo");
        String prop = request.getParameter("prop");
        neg.setCnd_aval((prop != null) ? prop : "");
        neg.setId_convenio(Integer.parseInt(request.getParameter("convenio")));
        neg.setId_remesa(Integer.parseInt(request.getParameter("idRemesa") != null ? request.getParameter("idRemesa") : "0"));
        neg.setId_sector(request.getParameter("idsect"));
        neg.setId_subsector(request.getParameter("idsubsect"));
        neg.setEstado("P");
        neg.setTasa(request.getParameter("tasa"));
        neg.setPorcentaje_cat(Double.parseDouble((String) request.getParameter("porc_cat") != null ? request.getParameter("porc_cat") : "0"));
        neg.setValor_capacitacion(Double.parseDouble((String) request.getParameter("capacitacion") != null ? request.getParameter("capacitacion") : "0"));
        neg.setValor_central(Double.parseDouble((String) request.getParameter("central") != null ? request.getParameter("central") : "0"));
        neg.setValor_seguro(Double.parseDouble((String) request.getParameter("seguro") != null ? request.getParameter("seguro") : "0"));
        neg.setFechatran(request.getParameter("fechatrans"));
        String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "Consumo";
        String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
        neg.setTipo_cuota(request.getParameter("tipo_cuota") != null ? request.getParameter("tipo_cuota") : "");
        neg.setTipoConv(tipoconv);
        neg.setValor_fianza(Double.parseDouble((String)  request.getParameter("vrfianza") != null ? request.getParameter("vrfianza"):"0"));
        neg.setValor_total_poliza(Double.parseDouble((String) request.getParameter("vrpoliza") != null ? request.getParameter("vrpoliza"):"0"));
        try {

            Convenio convenio = model.gestionConveniosSvc.buscar_convenio("fintra", request.getParameter("convenio"));//Obtener la informacion del convenio
            SolicitudAval bean_sol = gsaserv.buscarSolicitud(Integer.parseInt(numero_form));
            NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService(usuario.getBd());
            NegocioTrazabilidad sw = trazaService.buscarTraza(Integer.parseInt(numero_form), "FOR");
            AvalFianzaBeans avalFianza =gsaserv.getConfiguracionFianza(Integer.parseInt(convenio.getId_convenio()), neg.getNodocs(),bean_sol.getProducto_fondo(),bean_sol.getCobertura_fondo(),bean_sol.getNit_fondo());
            neg.setPorc_dto_aval(avalFianza.getPorc_dto_fianza());
            neg.setPorc_fin_aval(avalFianza.getPorc_fin_fianza());

            if (convenio.isRedescuento()) {
                neg.setTipoProceso("PCR");
            } else {
                if (neg.getNodocs() == 1 && neg.getFpago().equals("1")) {
                    neg.setTipoProceso("DSR");
                } else {
                    neg.setTipoProceso("PSR");
                }
            }
            if (tipoconv.equals("Microcredito")) {
                NegocioTrazabilidad negtraza = new NegocioTrazabilidad();                            
                negtraza.setActividad("LIQ");
                negtraza.setNumeroSolicitud(numero_form);
                negtraza.setUsuario(usuario.getLogin());
                negtraza.setCausal("");
                negtraza.setComentarios("");
                negtraza.setDstrct(usuario.getDstrct());
                negtraza.setConcepto("");

                if (bean_sol.getCodNegocio() == null) {
                    if (!convenio.isCat()) {
                        SolicitudPersona bean_pers = gsaserv.buscarPersona(Integer.parseInt(numero_form), "S");
                        if (bean_pers.getTipoPersona().equals("N")) {
                            WSHistCreditoService wsdc = new WSHistCreditoService(usuario.getBd());
                            RespuestaSuperfil respuesta = new RespuestaSuperfil();
                            respuesta = wsdc.consultarHistoriaCredito("1", bean_pers.getIdentificacion(), bean_pers.getPrimerApellido(), usuario);
                            negtraza.setComentarios(wsdc.buscarValor("H", "cod_consultas", respuesta.getCodigoRespuesta()));

                        }
                    }
//                    neg.setCod_negocio(model.Negociossvc.UpCP(convenio.getId_convenio()));//Generar el codigo del negocio
                    neg.setCod_negocio(model.Negociossvc.serialNegocios(convenio.getUnidad_negocio().getPrefijo_negocio()));//Generar el codigo del negocio
                    negtraza.setCodNeg(neg.getCod_negocio());
                    neg.setPolitica(bean_sol.getPolitica());
                    neg.setValor_renovacion(bean_sol.getValor_renovacion());
                    model.Negociossvc.insertNegocioMicrocredito(neg, numero_form, negtraza);
                    /**********************************agilidad de formulario********************************************/
                    if (neg.getTneg().equals("01") && (!neg.getEstado().equals("R")) && act_liq.equals("")) {
                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), ""));
                        next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&vista=" + "2&opcion=1";
                    }
                    if (!neg.getTneg().equals("01") && (!neg.getEstado().equals("R") && act_liq.equals(""))) {
                        if (radicacion.equals("S")) {
                            batch.add(trazaService.updateActividad(neg.getCod_negocio(), "LIQ"));
                            apdao.ejecutarSQL(batch);
                            next = "/jsp/applus/importar.jsp?num_osx=" + neg.getCod_negocio() + "&tipito=negocio&form=" + bean_sol.getNumeroSolicitud() + "&tipoconv=" + convenio.getTipo() + "&vista=6";
                            // se redirecciona
                        } else {
                            // se registra la zatraza de radicacion con concepto no aplica
                            batch.add(trazaService.updateActividad(neg.getCod_negocio(), "RAD"));
                            batch.add(trazaService.insertNegocioTrazabilidad(trazaService.trazabilidad_radicacion(neg.getCod_negocio(), String.valueOf(bean_sol.getNumeroSolicitud()), usuario)));
                        }
                    }
                    /*****************************************fin aagilidad*************************************************/
                } else {
                    
                    /*validar si el negocio de microcredito ha sido devuelto
                     * desde la actividad de formalizacion para ello preguntamos
                     * si sw es diferente de null sw se inicializa en la linea 92
                     * y trae la traza del negocio cuando la activida es formalizacion*/
                    
                    if(sw!=null){
                        
                     neg.setEstado("V");
                     neg.setActividad("");
                    }else{
                        
                     /*si es null preguntamos si en la traza el negocio se esta reliquidando desde 
                        la actividad de desicion */
                      sw = trazaService.buscarTraza(Integer.parseInt(numero_form), "DEC") ;
                        if (sw != null && sw.getActividad().equals("DEC")) {
                            neg.setActividad("ANA");
                        } else {
                            NegocioTrazabilidad buscarTrazaNegocio = trazaService.buscarTrazaNegocio(Integer.parseInt(numero_form));
                            neg.setActividad(buscarTrazaNegocio.getActividad());
                        }
                    
                    }
                    neg.setCod_negocio(bean_sol.getCodNegocio());
                    model.Negociossvc.updateNegocioMicrocredito(neg, numero_form, negtraza);
                }
                
            } else {
                NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                String mensaje = "";
                //aqui vamos hacer una mara�a para saltar data para los seguros.
                boolean consultaDataCredito = false;
                if(login_into.equals("Fenalco_bol")){//si es fenalco bolivar 
             
                    System.out.println("Fenalco Bolivar");

                    consultaDataCredito = (neg.getId_subsector().equals("01") && neg.getId_sector().equals("J66")) ? true
                            : gsaserv.aptoCreditoFen_bol(convenio, numero_form, usuario, neg.getNodocs(), neg.getTotpagado(), neg.getTipoProceso());
                     
                     mensaje = gsaserv.getMensaje();

                }else {//si es fenalco atlantico - fintra
                
                    System.out.println("Fenalco atlantico o fintra ");
                    
                    consultaDataCredito = (neg.getId_subsector().equals("01") && neg.getId_sector().equals("J66")) ? true
                            : gsaserv.aptoCredito(convenio, numero_form, usuario, neg.getNodocs(), neg.getTotpagado(), neg.getTipoProceso());
                    
                     mensaje = gsaserv.getMensaje();
                }
             
                if (consultaDataCredito) {
           //  if (true) {
                    mensaje = gsaserv.getMensaje();
                    String comentario = gsaserv.getComentario();
                    negtraza.setCausal(gsaserv.getCausal());
                    
                    if (!gsaserv.getEstado_neg().equals("")) {
                       
                        /*aqui validados si el ya paso por datacredito para ignorar
                         * el concepto emitido por esta entidad. Para ello consultamos la trazabilidad
                         * cuando la actividad es LIQ, si el resultado es diferente null,  es porque ya se consulto 
                         * datacredito al menos una vez y por lo tanto dejamos pasar el negocio.
                         */
                        NegocioTrazabilidad validaData = trazaService.buscarTraza(Integer.parseInt(numero_form), "LIQ");
                        if(validaData==null){
                          neg.setEstado(gsaserv.getEstado_neg());
                        }else{
                         //Reiniciamos el causal y los comentarios arrojados por data.
                            negtraza.setCausal("");
                            comentario="";
                        }
                    }
                    if (!neg.getEstado().equals("R")) {
                        model.sectorsubsectorSvc.buscar_sector(neg.getId_sector(), usuario.getBd());
                        String reliquida_sector = model.sectorsubsectorSvc.getReliquida();
                        if ((reliquida_sector.equals("S") && bean_sol.getCodNegocio() == null)) {
                            neg.setEstado("Q");
                        } else {
                            neg.setEstado("P");
                        }
                        mensaje += "Negocio: Continua Proceso";
                    } else {
                        mensaje += "Negocio: Rechazado";
                    }

                    negtraza.setActividad("LIQ");
                    negtraza.setNumeroSolicitud(numero_form);
                    negtraza.setUsuario(usuario.getLogin());
                    if (comentario.equals("")) {
                        comentario = mensaje;
                    }
                    negtraza.setComentarios(comentario);
                    negtraza.setDstrct(usuario.getDstrct());
                    negtraza.setConcepto(gsaserv.getResdeudor());
                    if (bean_sol.getCodNegocio() == null || opcion.equals("1")) {

                        String cod_neg = model.Negociossvc.UpCP(convenio.getPrefijo_negocio());//Generar el codigo del negocio
                        if (convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre()) {
                            if (opcion.equals(""))//acetar negocio
                            {
                                neg.setCod_negocio(cod_neg);
                                neg.setFinanciaAval(model.Negociossvc.getNegocio().isFinanciaAval());
                                  batch.add(model.Negociossvc.DeleteDocumentosForms(neg, bean_sol.getNumeroSolicitud()));
                                batch.add(model.Negociossvc.GuardarNegocio(neg, numero_form, convenio, negtraza));
                                if (model.Negociossvc.getNegocio().isFinanciaAval()) {
                                    Negocios negAval = model.Negociossvc.getNegocioAval();
                                    negAval.setNegocio_rel(cod_neg);
                                    cod_neg = model.Negociossvc.UpCP(convenio.getPrefijo_negocio());//Generar el codigo del negocio
                                    negAval.setCod_negocio(cod_neg);
                                    negAval.setvalor_aval("0");
                                    negAval.setTipoProceso(neg.getTipoProceso());
                                    negAval.setEstado(neg.getEstado());
                                    negAval.setCreation_user(neg.getCreation_user());
                                    negAval.setPor_rem(neg.getPor_rem());
                                    negAval.setNit_tercero(convenio.getNit_anombre()); //setamos el tercero el afiliado

                                    String nuevo_numero_form = gsaserv.numeroSolc();
                                    batch.add(model.Negociossvc.GuardarNegocioAval(negAval, nuevo_numero_form, convenio, negtraza));
                                    batch.add(model.Negociossvc.MarcarNegocio(bean_sol.getCodNegocio(), cod_neg, "AF"));
                                    //clonar solicitud
                                    batch.add(gsaserv.ClonarSolicitud(numero_form, cod_neg, negAval.getVr_negocio(), nuevo_numero_form));
                                    batch.add(gsaserv.ClonarDetalleSolicitud(numero_form, nuevo_numero_form));
                                    batch.add(model.Negociossvc.InsertaDocumentosForms(negAval, nuevo_numero_form));

                                }
                                /**********************************agilidad de formulario********************************************/
                                if (neg.getTneg().equals("01") && (!neg.getEstado().equals("R")) && act_liq.equals("")) {
                                    batch.add(trazaService.updateActividad(neg.getCod_negocio(), ""));
                                    next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&vista=" +"2&opcion=1";
                                }
                                if (!neg.getTneg().equals("01") && (!neg.getEstado().equals("R")&& act_liq.equals(""))) {
                                    if (radicacion.equals("S")) {
                                        mensaje="";
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "LIQ"));                                       
                                         next = "/jsp/applus/importar.jsp?num_osx=" + neg.getCod_negocio() + "&tipito=negocio&form=" + bean_sol.getNumeroSolicitud() + "&tipoconv=" + convenio.getTipo() + "&vista=6";
                                        // se redirecciona
                                    } else {
                                        // se registra la zatraza de radicacion con concepto no aplica
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "RAD"));
                                        batch.add(trazaService.insertNegocioTrazabilidad(trazaService.trazabilidad_radicacion(neg.getCod_negocio(),String.valueOf(bean_sol.getNumeroSolicitud()), usuario)));

                                    }
                                }
                                /*****************************************fin agilidad*************************************************/
                                 apdao.ejecutarSQL(batch);

                                //validar si el negocio es rechazado por datacredito con unica causal limite maximo de endeudamiento y ademas no es convenio de tipo educativo
                                if (neg.getEstado().equals("R") && !bean_sol.getProducto().equals("01")) {
                                    if (gsaserv.validarCausalMLE(neg.getCod_cli(), gsaserv.getNitEmpresa()))//validar la si la causal es LMD
                                    {
                                        batch = new Vector();
                                        double monto_sujerido = gsaserv.getMontoSugerido(bean_sol, cod_neg); //calcular monto sujerido

                                        if (monto_sujerido >= neg.getVr_negocio()) {
                                            // aceptar negocio
                                            NegocioTrazabilidadService nt_service = new NegocioTrazabilidadService(this.usuario.getBd());
                                            mensaje = "Negocio: Continua Proceso";
					    negtraza.setActividad("LIQ");
                                            negtraza.setNumeroSolicitud(numero_form);
                                            negtraza.setUsuario(usuario.getLogin());
                                            negtraza.setCausal("");
                                            negtraza.setDstrct(usuario.getDstrct());
                                            negtraza.setConcepto("");
                                            negtraza.setComentarios("Deudor:PRE APROBADO <br/>" + mensaje);                                            
                                            batch.add(nt_service.insertNegocioTrazabilidad(negtraza));
                                            batch.add(nt_service.updateEstadoNeg(cod_neg, "P"));
                                            batch.add(nt_service.updateEstadoForm(bean_sol.getNumeroSolicitud(), "P"));

                                            if (model.Negociossvc.getNegocioAval() != null) {
                                                SolicitudAval bean_sol_aval = gsaserv.buscarSolicitud(model.Negociossvc.getNegocioAval().getCod_negocio());
                                                batch.add(nt_service.updateEstadoNeg(bean_sol_aval.getCodNegocio(), "P"));
                                                batch.add(nt_service.updateEstadoForm(bean_sol_aval.getNumeroSolicitud(), "P"));
                                            }
                                            apdao.ejecutarSQL(batch);

                                        } else {
                                            if (monto_sujerido > (neg.getVr_negocio() * 40 / 100)) {
                                                next = "/jsp/fenalco/liquidadores/mensaje.jsp";
                                                mensaje = "Negocio Rechazado por limite maximo de endeudamiento&monto_sujerido=" + monto_sujerido + "&plazo=" + neg.getNodocs() + "&cod_neg=" + neg.getCod_negocio();
                                            }
                                        }

                                    }

                                }
                                model.Negociossvc.setNegocio(null);
                                model.Negociossvc.setNegocioAval(null);
                                model.Negociossvc.setLiquidacion(null);
                                model.Negociossvc.setLiquidacionAval(null);
                            }



                            //crear nuevo negocio con convenio diferente
                            if (opcion.equals("1")) {
                                neg.setCod_negocio(cod_neg);//Generar el codigo del negocio
                                String nuevo_numero_form = gsaserv.numeroSolc();
                                batch.add(model.Negociossvc.NegociosGuardar(neg, numero_solicitud, ciclo, numero_form, convenio, negtraza));//guardar nuevo negocio

                                //clonar solicitud  de negocio anterior con nuevo nuemero de solicitud
                                batch.add(gsaserv.ClonarSolicitud(numero_form, neg.getCod_negocio(), neg.getVr_negocio(), nuevo_numero_form));
                                batch.add(gsaserv.ClonarDetalleSolicitud(numero_form, nuevo_numero_form));
                                batch.add(model.Negociossvc.MarcarNegocio(bean_sol.getCodNegocio(), cod_neg, "CC"));
                                batch.add(gsaserv.MarcarSolicitud(opcion, cod_neg, "" + neg.getId_convenio()));
                                batch.add(model.Negociossvc.InsertaDocumentosFormsReliquidacionConvenio(neg, numero_form));
                                apdao.ejecutarSQL(batch);

                            }

                        } else {
                            neg.setCod_negocio(model.Negociossvc.UpCP(convenio.getPrefijo_negocio()));//Generar el codigo del negocio
                            model.Negociossvc.listado(neg, numero_solicitud, ciclo, numero_form, convenio, negtraza);

                                /**********************************hagilidad de formulario********************************************/
                                if (neg.getTneg().equals("01") && (!neg.getEstado().equals("R")) && act_liq.equals("")) {
                                    batch.add(trazaService.updateActividad(neg.getCod_negocio(), ""));
                                    next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&vista=" +"2&opcion=1";
                                }
                                if (!neg.getTneg().equals("01") && (!neg.getEstado().equals("R")&& act_liq.equals(""))) {
                                    if (radicacion.equals("S")) {
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "LIQ"));
                                        apdao.ejecutarSQL(batch);
                                         next = "/jsp/applus/importar.jsp?num_osx=" + neg.getCod_negocio() + "&tipito=negocio&form=" + bean_sol.getNumeroSolicitud() + "&tipoconv=" + convenio.getTipo() + "&vista=6";
                                        // se redirecciona
                                    } else {
                                        // se registra la zatraza de radicacion con concepto no aplica
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "RAD"));
                                        batch.add(trazaService.insertNegocioTrazabilidad(trazaService.trazabilidad_radicacion(neg.getCod_negocio(),String.valueOf(bean_sol.getNumeroSolicitud()), usuario)));
                                        apdao.ejecutarSQL(batch);
                                    }
                                }
                                /*****************************************fin hagilidad*************************************************/
                        }
                    } else {
                        ///inicio  reliquidacion fa educativo
                        // reliquidacion redescuento
                        if (convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre()) {
                            if (opcion.equals("")) {
                                neg.setCod_negocio(bean_sol.getCodNegocio());
                                neg.setFinanciaAval(model.Negociossvc.getNegocio().isFinanciaAval());
                                //validar si el negocio vine d ela actividad de formalizacion.
                                if (sw != null && sw.getActividad().equals("FOR") && sw.getConcepto().equals("ZONA GRIS LIQ")) {
                                    neg.setEstado("V");
                                }
                                batch.add(model.Negociossvc.ActualizarNegocioRLQ(neg, numero_form, convenio, negtraza));
                                //validar si reliquida por decicion o perfeccionamiento
                                if (gsaserv.ValidarSolicitudDevuelta(String.valueOf(bean_sol.getNumeroSolicitud()), "SOL", act_liq)) {
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(),(act_liq.equals("DEC")?"ANA":act_liq.equals("PERF")?"DEC":"" )));
                                        batch.add(trazaService.updateEstadoNeg(neg.getCod_negocio(),act_liq.equals("PERF")?"V":neg.getEstado() ));
                                }
                                
                                /*validamos si sw vienen en null si es asi e sporque el negocio no ha estado 
                                 * en formalizacion por lo tanto el negocio viene en un curso normal.
                                 */
                                NegocioTrazabilidad sw2= null;
                                if (sw == null) {
                                    sw2= null;
                                    sw =new NegocioTrazabilidad();
                                    sw.setActividad("");
                                    sw.setConcepto("");
                                }else{
                                    sw2=sw;
                                }
                               /*preguntar si el negocio viene de formalizacion y el concepto es ZONAGRILI*/
                              
                                if ((sw.getActividad().equals("FOR") || sw2 == null) && !sw.getConcepto().equals("ZONA GRIS LIQ")){
                                   
                                 /**********************************hagilidad de formulario********************************************/
                                if (neg.getTneg().equals("01") && (!neg.getEstado().equals("R")) && act_liq.equals("")) {
                                    batch.add(trazaService.updateActividad(neg.getCod_negocio(), ""));
                                    next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&vista=" +"2&opcion=1";
                                }
                                if (!neg.getTneg().equals("01") && (!neg.getEstado().equals("R")&& act_liq.equals(""))) {
                                    if (radicacion.equals("S")) {
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "LIQ"));
                                         next = "/jsp/applus/importar.jsp?num_osx=" + neg.getCod_negocio() + "&tipito=negocio&form=" + bean_sol.getNumeroSolicitud() + "&tipoconv=" + convenio.getTipo() + "&vista=6";
                                        // se redirecciona
                                    } else {
                                        // se registra la zatraza de radicacion con concepto no aplica
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "RAD"));
                                        batch.add(trazaService.insertNegocioTrazabilidad(trazaService.trazabilidad_radicacion(neg.getCod_negocio(),String.valueOf(bean_sol.getNumeroSolicitud()), usuario)));
                                    }
                                }
                                /*****************************************fin hagilidad*************************************************/
                               
                               }//fin llave validar actividad formalizacion.
                               
                                if (model.Negociossvc.getNegocio().isFinanciaAval()) {

                                    Negocios negAval = model.Negociossvc.getNegocioAval();
                                    negAval.setNegocio_rel(neg.getCod_negocio());
                                    SolicitudAval bean_sol_aval = gsaserv.buscarSolicitud(model.Negociossvc.buscarNegocioAval(neg.getCod_negocio()).getCod_negocio());
                                    negAval.setCod_negocio(bean_sol_aval.getCodNegocio());
                                    negAval.setvalor_aval("0");
                                    negAval.setTipoProceso(neg.getTipoProceso());
                                    negAval.setEstado(neg.getEstado());
                                    negAval.setCreation_user(neg.getCreation_user());
                                    negAval.setPor_rem(neg.getPor_rem());
                                    negAval.setNit_tercero(convenio.getNit_anombre());
                                    batch.add(model.Negociossvc.DeleteDocumentosForms(negAval, bean_sol_aval.getNumeroSolicitud()));
                                    batch.add(model.Negociossvc.ActualizarNegocioAvalRLQ(negAval, bean_sol_aval.getNumeroSolicitud(), convenio, negtraza));
                                    //validar si reliquida por decicion o perfeccionamiento
                                    if (gsaserv.ValidarSolicitudDevuelta(String.valueOf(bean_sol_aval.getNumeroSolicitud()), "SOL", act_liq)) {
                                        batch.add(trazaService.updateActividad(negAval.getCod_negocio(), (act_liq.equals("DEC") ? "ANA" : act_liq.equals("PERF") ? "DEC" : "")));
                                        batch.add(trazaService.updateEstadoNeg(neg.getCod_negocio(), act_liq.equals("PERF") ? "V" : ""));
                                    }
                                }
                                
                                //Validar si el negocio ha sido devuelto a solicitud para cambiar la activida del negocio
                                //educativos y otros para que quede en radicacion.
                                NegocioTrazabilidad ultimaTraza = trazaService.buscarUltimaActivida(Integer.parseInt(bean_sol.getNumeroSolicitud()));
                                if(ultimaTraza.getActividad().equals("SOL")){
                                  batch.add(trazaService.updateActividad(neg.getCod_negocio(), "LIQ"));
                                }
  
                                apdao.ejecutarSQL(batch);
                                model.Negociossvc.setNegocio(null);
                                model.Negociossvc.setNegocioAval(null);
                                model.Negociossvc.setLiquidacion(null);
                                model.Negociossvc.setLiquidacionAval(null);
                            }

                            if (opcion.equals("2")) {//reliquidacion negocio rechazado por datacredito

                                 neg.setCod_negocio(bean_sol.getCodNegocio());
                                neg.setFinanciaAval(model.Negociossvc.getNegocio().isFinanciaAval());                               
                                neg.setEstado("P");
                                mensaje = "Negocio: Continua Proceso";
                                negtraza.setConcepto("RELIQUIDADO");
                                negtraza.setComentarios("Deudor:PRE APROBADO <br/>"+mensaje);
                                negtraza.setCausal("22");
                                batch.add(model.Negociossvc.ActualizarNegocioRLQ(neg, numero_form, convenio, negtraza));
                                bean_sol.setValorSolicitado(String.valueOf(neg.getVr_negocio()));
                                bean_sol.setEstadoSol(neg.getEstado());
                                batch.add(gsaserv.updateSolicitud(bean_sol));//actualizar valor solicitud con nuevo valor solicitado


                                /**********************************hagilidad de formulario********************************************/
                                if (neg.getTneg().equals("01") && (!neg.getEstado().equals("R")) && act_liq.equals("")) {
                                    batch.add(trazaService.updateActividad(neg.getCod_negocio(), ""));
                                    next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&vista=" +"2&opcion=1";
                                }
                                if (!neg.getTneg().equals("01") && (!neg.getEstado().equals("R")&& act_liq.equals(""))) {
                                    if (radicacion.equals("S")) {
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "LIQ"));                                        
                                         next = "/jsp/applus/importar.jsp?num_osx=" +neg.getCod_negocio() + "&tipito=negocio&form=" + bean_sol.getNumeroSolicitud() + "&tipoconv=" + convenio.getTipo() + "&vista=6";
                                        // se redirecciona
                                    } else {
                                        // se registra la zatraza de radicacion con concepto no aplica
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "RAD"));
                                        batch.add(trazaService.insertNegocioTrazabilidad(trazaService.trazabilidad_radicacion(neg.getCod_negocio(),String.valueOf(bean_sol.getNumeroSolicitud()), usuario)));
                                    }
                                }
                                /*****************************************fin hagilidad*************************************************/
  
                                if (model.Negociossvc.getNegocio().isFinanciaAval()) {

                                    Negocios negAval = model.Negociossvc.getNegocioAval();
                                    negAval.setNegocio_rel(neg.getCod_negocio());
                                    SolicitudAval bean_sol_aval = gsaserv.buscarSolicitud(model.Negociossvc.buscarNegocioAval(neg.getCod_negocio()).getCod_negocio());
                                    negAval.setCod_negocio(bean_sol_aval.getCodNegocio());
                                    negAval.setvalor_aval("0");
                                    negAval.setTipoProceso(neg.getTipoProceso());
                                    negAval.setEstado(neg.getEstado());
                                    negAval.setCreation_user(neg.getCreation_user());
                                    negAval.setPor_rem(neg.getPor_rem());
                                    negAval.setNit_tercero(convenio.getNit_anombre());
                                    batch.add(model.Negociossvc.DeleteDocumentosForms(negAval, bean_sol_aval.getNumeroSolicitud()));
                                    batch.add(model.Negociossvc.ActualizarNegocioAvalRLQ(negAval, bean_sol_aval.getNumeroSolicitud(), convenio, negtraza));

                                }
                                

                                
                                apdao.ejecutarSQL(batch);
                                model.Negociossvc.setNegocio(null);
                                model.Negociossvc.setNegocioAval(null);
                                model.Negociossvc.setLiquidacion(null);
                                model.Negociossvc.setLiquidacionAval(null);
                            }




                        } else {
                            //si el negocio viene de formalizacion poner el estado en Avalado
                            if(sw!=null&&sw.getActividad().equals("FOR")){
                             neg.setEstado("V");
                            }
                            neg.setCod_negocio(bean_sol.getCodNegocio());
                            model.Negociossvc.negocioUpdate(neg, numero_solicitud, ciclo, numero_form, convenio, negtraza);
                            //validar si reliquida por decicion o perfeccionamiento
                            if (gsaserv.ValidarSolicitudDevuelta(String.valueOf(bean_sol.getNumeroSolicitud()), "SOL", act_liq)) {
                                batch.add(trazaService.updateActividad(neg.getCod_negocio(),(act_liq.equals("DEC")?"ANA":act_liq.equals("PERF")?"DEC":"" )));
                                batch.add(trazaService.updateEstadoNeg(neg.getCod_negocio(),act_liq.equals("PERF")?"V":neg.getEstado() ));
                              
                            }
                            //preguntar si el negocio viene de formalizacion.  
                            if(!sw.getActividad().equals("FOR") ){
                              
                                                       
                              /**********************************hagilidad de formulario********************************************/
                                if (neg.getTneg().equals("01") && (!neg.getEstado().equals("R")) && act_liq.equals("")) {
                                    batch.add(trazaService.updateActividad(neg.getCod_negocio(), ""));
                                    next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&vista=" +"2&opcion=1";
                                }
                                if (!neg.getTneg().equals("01") && (!neg.getEstado().equals("R")&& act_liq.equals(""))) {
                                    if (radicacion.equals("S")) {
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "LIQ"));
                                         mensaje="";
                                         next = "/jsp/applus/importar.jsp?num_osx=" + bean_sol.getCodNegocio() + "&tipito=negocio&form=" + bean_sol.getNumeroSolicitud() + "&tipoconv=" + convenio.getTipo() + "&vista=6";
                                        // se redirecciona
                                    } else {
                                        // se registra la zatraza de radicacion con concepto no aplica
                                        batch.add(trazaService.updateActividad(neg.getCod_negocio(), "RAD"));
                                        batch.add(trazaService.insertNegocioTrazabilidad(trazaService.trazabilidad_radicacion(neg.getCod_negocio(),String.valueOf(bean_sol.getNumeroSolicitud()), usuario)));
                                    }
                                }
                                /*****************************************fin hagilidad*************************************************/
                            }
                            //preguntamos si el batch no esta vacio.
                            if(!batch.isEmpty()){
                             apdao.ejecutarSQL(batch);
                            }  

                        }

                        ///fin reliquidacion

                    }


                }
                if (!mensaje.equals("")) {
                    next += "?mensaje=" + mensaje;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }

        this.dispatchRequest(next);
        neg = null;
    }
}
