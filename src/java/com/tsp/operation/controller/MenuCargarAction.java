/*
 * CompaniaMenuAction.java
 *
 * Created on 26 de febrero de 2005, 11:20 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class MenuCargarAction extends Action {
    
    public MenuCargarAction() {
    }
    
    public void run() throws javax.servlet.ServletException {
        request.setAttribute("msg","");
        request.setAttribute("estados","");
        request.setAttribute("ciudades","");        
        //24 enero 2006 LGCARGUE
        model.instrucciones_despachoService.setRemesas(null);
        model.instrucciones_despachoService.setLista(null);
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();
        //kreales 23-03-2006
        model.planillaService.setPlanillasVec(null);
        model.codextrafleteService.setCodextra(null);
        //reseteo de objetos
        model.identidadService.reset();
        model.conductorService.reset();
        //model.caravanaSVC.setVectorRutas(new Vector());
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        //Juan 10.11.05
        TreeMap t = new TreeMap();
        model.stdjobdetselService.setCiudadesDest(t);
        model.stdjobdetselService.setCiudadesOri(t);
        model.stdjobdetselService.setStdjobTree(t);
        
        //orientacion de pagina
        String marco = ( request.getParameter("marco")!= null) ? request.getParameter("marco") : "";
        String next = request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        //System.out.println("-- NEXT-------------:"+session.getAttribute("modelcontab"));
        try {
            //if(model.tablaGenService.obtenerInformacionDato( "USRVETONIT",usuario.getLogin()) !=null)
            //    session.setAttribute("USRIDEN","S");        
            //opciones
            if (request.getParameter("opcion") != null ) {
                /*Henry*/
                if (request.getParameter("opcion").equalsIgnoreCase("1")) {
                    model.agenciaService.loadAgencias();
                } else if (request.getParameter("opcion").equalsIgnoreCase("2")){
                    File f = new File("/exportar/masivo/Sincronizacion/servidor/");
                    Vector lista = new Vector();
                    File[] vec=f.listFiles();
                    for(int i=0; i<vec.length; i++){
                        if(vec[i].isFile()){
                            lista.add(vec[i].getName());
                        }
                    }
                    request.setAttribute("lista", lista);
                    f = new File("/exportar/masivo/Sincronizacion/servidor/recibido/");
                    lista = new Vector();
                    vec=f.listFiles();
                    for(int i=0; i<vec.length; i++){
                        if(vec[i].isFile())
                            lista.add(vec[i].getName());
                    }
                    request.setAttribute("journal", lista);
                    next="/Sincronizacion/Consultas.jsp";
                } else if (request.getParameter("opcion").equalsIgnoreCase("3")) {
                    model.ReporteDrummondSvc.searchClientes();
                } else if (request.getParameter("opcion").equalsIgnoreCase("4")) {
                    if ( next.equals("gestion/usuarios.jsp") ){
                        next = "/controller?estado=GestionUsuarios&accion=ManageUsers";
                    }
                    else if ( next.equals("reporte/infocliente.jsp") ){
                        next = "/controller?estado=InfoCliente&accion=Search";
                    }
                    else if ( next.equals("reporte/ubicaequiporuta.jsp")){
                        next = "/controller?estado=UbicacionEquipoRuta&accion=Search";
                    }
                    else if( next.equals("admin/nuevaTablagen.jsp") ){
                        next = "/jsp/sot/body/PantallaTablaGenNueva.jsp?titulo=Agregar Tabla";
                    }
                    else if( next.equals("admin/listarTablagen.jsp") ){
                        next = "/controller?estado=TablaGen&accion=Manager";
                    }
                    else if(next.equals("reporte/UbicacionVehicularCliente.jsp")){
                        next = "/controller?estado=UbicacionVehicular&accion=Search";
                    }
                    else if(next.equals("/jsp/sot/body/PantallaFiltroReporteTiempoConductores.jsp")){
                        next = "/controller?estado=ReporteTiemposDeViaje&accion=Conductores";
                    }else if ( next.equals("reporte/UbicacionVehicularPropietario.jsp")){
                        next = "/controller?estado=UbicacionVehicular&accion=Propietario";
                    }else if ( next.equals("body/FiltroPromedioEntrePC.jsp") ) {
                        next = "/controller?estado=ReporteTiempos&accion=PuestosDeControl&cmd=loadPCs";
                    } else if ( next.equals("reporte/nitProp.jsp")){
                        next = "/controller?estado=AsignacionNits&accion=Propietario";
                    } else if ( next.equals("/config/reportes.jsp") ) {
                        next = "/controller?estado=GestionReportes&accion=Setup";
                    } else if (next.equals("/reporte/general.jsp") ) {
                        next = "/controller?estado=Grupos&accion=Reporte&cmd=list";
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("5")){
                    // JuanM 7-12-05
                   // model.DPtoSvc.Init_II(usuario.getId_agencia());
                } else if (request.getParameter("opcion").equalsIgnoreCase("6")) {
                    //Henry 23-12-2005
                    if(usuario.getBase().equalsIgnoreCase("pco") || usuario.getBase().equals("spo")) {
                        next = "/controller?estado=Menu&accion=Conductor";
                    } else {
                        next = "/jsp/trafico/conductor/Conductor.jsp?titulo=Conductor";
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("7")) {
                    next = "/controller?estado=Factura&accion=LArchivo";
                } else if (request.getParameter("opcion").equalsIgnoreCase("8")) {
                    //Henry 23-12-2005
                    if(request.getParameter("grupo_equipo")!=null){
                        /*Reseteamos los datos de la busqueda anterior*/
                        model.placaService.setPlaca(new Placa());
                        if(request.getParameter("tipo").equals("I")){
                            if(request.getParameter("grupo_equipo").equals("T")){
                                next = "/placas/trailerInsert.jsp";
                                model.tablaGenService.buscarClasificacion();
                            }
                            else if(request.getParameter("grupo_equipo").equals("C")){
                                next = "/placas/placasInsert.jsp";
                            }
                            else
                                next = "/placas/placaInsert.jsp";
                        } else{
                            model.tablaGenService.buscarGrupos();
                            if(request.getParameter("grupo_equipo").equals("T")){
                                next = "/placas/trailerUpdate.jsp";
                                model.tablaGenService.buscarClasificacion();
                            }
                            else if(request.getParameter("grupo_equipo").equals("C"))
                                next = "/placas/placasUpdate.jsp";
                            else
                                next = "/placas/placaUpdate.jsp";
                        }
                        model.tablaGenService.buscarRegistrosOrdenadosByDesc("GPSOPER");
                        model.tablaGenService.setDatos2( model.tablaGenService.obtenerTablas() );
                        model.tablaGenService.buscarTipo( request.getParameter("grupo_equipo") );
                        model.tablaGenService.buscarClase( request.getParameter("grupo_equipo") );
                        model.tablaGenService.buscarRines();
                        model.tablaGenService.buscarModelo();
                        model.tablaGenService.buscarPiso();
                        model.tablaGenService.buscarTitulo();
                        model.tablaGenService.buscarGruposEq();
                    }
                    
                }  else if (request.getParameter("opcion").equalsIgnoreCase("9")) {
                    
                    model.tblgensvc.searchDatosTablaCambioCelCond();
                    
                }  else if (request.getParameter("opcion").equalsIgnoreCase("10")){
                    model.SoporteClienteSvc.buscarClientes();
                    model.SoporteClienteSvc.buscarSoportes();
                    model.SoporteClienteSvc.buscarRelaciones();
                    String codcli = request.getParameter("codcli")!=null ? request.getParameter("codcli"):"";
                    if(request.getParameter("op")!=null && request.getParameter("op").equals("1")){
                        next = "/jsp/masivo/asignaciones/SoportesClientes.jsp?codcli="+codcli;
                        
                    }
                    // //System.out.println(""+ next);
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("11")){
                    // Diogenes
                    
                    model.discrepanciaService.listClienteDiscrepancia(distrito);
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("12")){
                    if ( next.equals("/jsp/trafico/turnos/insertar.jsp") ){
                        next = "/controller?estado=Turnos&accion=Ingresar";
                    }
                    else if ( next.equals("/jsp/trafico/turnos/reporte0.jsp") ){
                        next = "/controller?estado=Turnos&accion=IngresarR";
                    } else if (next.equals("/VehExternos/ListadoVE.jsp")){
                        model.VExternosSvc.ReiniciarLista();
                    }
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("13")) {
                    next="/controller?estado=Log&accion=Proceso&Estado=ACTIVO";
                } else if ( request.getParameter("opcion").equalsIgnoreCase("14")){
                    /* mfontalvo, redirecionamiento a las pagianas de presupuesto*/
                    next = "/controllerpto?ACCION=/Me/nu"+
                    "&JSP="    + request.getParameter("pagina") +
                    "&FOLDER=" + request.getParameter("carpeta");
                } else if (request.getParameter("opcion").equalsIgnoreCase("15")) {
                    
                    model.tblgensvc.searchDatosTablaWorkGroup();
                    model.wgroup_placaSvc.ReiniciarDato();
                    if (request.getParameter("item").equalsIgnoreCase("1")){
                        model.ReporteEgresoSvc.ReiniciarListaRE();
                    }
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("16")) {
                    /****Ing. Armando Oviedo******/
                    
                    model.ConceptoPagosvc.insertDefault();
                    model.ConceptoPagosvc.reiniciar();
                    model.ConceptoPagosvc.buscarHijos( "0", "x" );
                    model.ConceptoPagosvc.load();
                    model.ConceptoPagosvc.mostrarContenido( "0" );
                    model.tablaGenService.buscarRegistros( "TELEMENTOS" );
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("17")) {
                    if ( next.equals("none/consulta") ){
                        next = "/controller?estado=ConsultaU&accion=Ingresar";
                    }
                    else if ( next.equals("none/tabla") ){
                        next = "/controller?estado=TablasU&accion=Ingresar";
                    } else if ( next.equals("/jsp/cxpagar/corridas/PantallaFiltroExtracto.jsp") ){
                        model.servicioBanco.obtenerNombresBancos();
                    } else if (next.equals("/cxpagar/autorizar.jsp")) {
                        next = "/controller?estado=CXPObservacion&accion=Pagador&doc=FiltroObservacionPagador";
                    }  else if (next.equals("/cxpagar/generarOp.jsp")) {
                        next = "/controller?estado=Adicion&accion=FacturasOP&evento=LOAD";
                    }  else if (next.equals("jsp/cxpagar/factura_migracion/filtro_formato")) {
                        next = "/controller?estado=Factura&accion=Migracion&evento=LOAD";
                    } else if (next.equals("/jsp/consultaAdmin.jsp")) {
                        next = "/controller?estado=Consulta&accion=Extracto&opcion=Init&show=ok";
                    } else if (next.equals("/jsp/consultaPropietario.jsp")) {
                        next = "/controller?estado=Consulta&accion=Extracto&opcion=InitPropietario";
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("18")) {
                    
                    model.seriesService.BuscarTodosGrupos();
                    
                } else if ( request.getParameter("opcion").equalsIgnoreCase("19")){
                    /* dbastida,cargar las tablas que autoriza el usuario*/
                    try{
                        model.usuaprobacionService.listarTablaUsuAprobacion(usuario.getLogin());
                    }catch(Exception e){}
                    if ( next.equals("/jsp/general/usuarioAprobacion/usuarioAprobacion.jsp") || next.equals("/jsp/general/usuarioAprobacion/buscarUsuarios.jsp") ){
                        try{
                            model.agenciaService.loadAgencias();
                            model.tablaGenService.buscarRegistros("TPROGRAMA");
                        }catch(Exception e){}
                    }
                    
                } else if ( request.getParameter("opcion").equalsIgnoreCase("20")){
                    /* Tito Andr�s, 13.01.2006 */
                    model.clienteService.setTreeMapClientes();
                    model.tblgensvc.searchDatosTablaWorkGroup();
                    
                    model.stdjobService.setOrigenesStdJobsCliente(new TreeMap());
                    model.stdjobService.setDestinosStdJobsCliente(new TreeMap());
                    model.stdjobService.setStdJobsCliente(new TreeMap());
                    
                    model.wgroup_stdjobSvc.setVarCamposJS("var CamposJS = [];");
                    model.wgroup_stdjobSvc.setVarCamposJStd("var CamposStd = [];");
                    
                } else if ( request.getParameter("opcion").equalsIgnoreCase("21")){
                    
                    model.RegistroTiempoSvc.ReiniciarListaPlanillas();
                    
                }  else if (request.getParameter("opcion").equalsIgnoreCase("22")) {
                    
                    String AgenciaUsuario =usuario.getId_agencia();
                    String Numrem = request.getParameter("Numrem");
                    next = "/impresiones_pdf/remesanoimpPDF.jsp";
                    
                    if(Numrem == null)
                        model.RemesaSvc.buscaRemesa_no_impresa(Numrem,usuario.getLogin(),2);
                    else{
                        model.RemesaSvc.buscaRemesa_no_impresa(Numrem.toUpperCase(),usuario.getLogin(),1);
                        if(model.RemesaSvc.getRegistros().size()==0)
                            next = "/impresiones_pdf/buscarremesaPDF.jsp?Mensaje='La remesa con codigo ["+Numrem+"] no existe en la base de datos...'";
                    }
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("23")) {
                    
                    String      Login   = usuario.getLogin();
                    String      Agencia = usuario.getId_agencia();
                    
                    //String Agencia = "A01";
                    model.PlanillaImpresionSvc.ReiniciarCIA();
                    model.PlanillaImpresionSvc.ReiniciarPlanillas();
                    model.PlanillaImpresionSvc.BuscarDatosCIA();
                    model.PlanillaImpresionSvc.BuscarPlaNoImp(Agencia,usuario.getBase(),usuario.getLogin());
                    
                }  else if ( request.getParameter("opcion").equalsIgnoreCase("24")){
                    /* dbastida*/
                    if ( next.equals("/consultas/consultaPlanillasGeneral.jsp") ){
                        model.agenciaService.cargarAgencias();
                        model.ciudadService.getCiudadSearch();
                        
                    } else if ( next.equals("/jsp/hvida/identidad/identidad.jsp")|| next.equals("/jsp/hvida/identidad/identidad.jsp?clas=0E000") ){
                        //01052007
                        if(model.tablaGenService.obtenerInformacionDato( "USRVETONIT",usuario.getLogin()) !=null){
                            session.setAttribute("USRIDEN","S");
                        }
                        model.tdocumentoService.ListarTipoDocumentos();
                        model.paisservice.listarpaises();
                        model.ciudadService.searchTreMapCiudades();
                        
                        if(usuario.getBase().equalsIgnoreCase("pco") || usuario.getBase().equals("spo")) {
                            next = next+"?base=carbon&clas=0000A";
                        }
                    }else if ( next.equals("/jsp/hvida/buscarHVida.jsp") ){
                        model.placaService.setVehiculos(null);
                    }else  if ( next.equals("/jsp/trafico/clienteactividad/clienteactividad.jsp") || next.equals("/jsp/trafico/clienteactividad/BuscarActAsig.jsp")){
                        
                        model.clienteService.clienteSearch();
                        model.actividadSvc.listarActividad();
                        model.tablaGenService.buscarRegistros("TBLFRON");
                    } else if ( next.equals("/jsp/trafico/actividad/actividad.jsp") ){
                        model.tablaGenService.buscarRegistros("EMAIL_GRUP");
                    }
                    if(usuario.getBase().equalsIgnoreCase("pco") || usuario.getBase().equals("spo")) {
                        next = next+"?base=carbon&clas=0000A";
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("25")) {
                    model.menuService.obtenerPadresRaizMenu();
                    if ( next.equals("/jsp/trafico/jerarquia/Jerarquia_tiempoInsertar.jsp")||  next.equals("/jsp/trafico/jerarquia/Jerarquia_tiempoBuscar.jsp")){
                        model.tablaGenService.buscarResponsableActividad();
                        model.tablaGenService.buscarCausaDemora();
                        model.actividadSvc.listarActividad();
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("26")) {//sescalante
                    if ( next.equals("/jsp/trafico/contacto/VerContactos.jsp") ){
                        //a buscar listar contactos
                        model.contactoService.contactos();
                        
                    } else if ( next.equals("/jsp/trafico/contacto/contacto.jsp") ){
                        //nuevo contacto
                        Vector tcont = model.tipo_contactoService.listarTContactos();
                        next += "?mensaje=";
                        session.setAttribute("tipocont", tcont);
                        
                    } else if ( next.equals("/jsp/trafico/ubicacion/ingresarUbicacion.jsp") ){
                        //nueva ubicacion
                        model.tipo_ubicacionService.listTipo_ubicacion();
                        model.paisservice.listarpaises();
                        
                    } else if ( next.equals("/jsp/trafico/tramo/ingresarTramo.jsp") ){
                        //nuevo tramo
                        model.paisservice.listarpaises();
                        model.unidadTrfService.listarUnidadesxDistancia("D");
                        model.unidadTrfService.listarUnidadesxTiempo("T");
                        model.unidadTrfService.listarUnidadesxVolumen("V");
                        
                    }  else if ( next.equals("/jsp/trafico/ciudad/ciudad.jsp") ){
                        //nueva ciudad
                        model.ciudadService.searchTreMapCiudades();
                        model.paisservice.listarpaises();
                        model.zonaService.listarZonasFronterisas();
                        model.zonaService.listarZonasNoFronterisas();
                        model.TimpuestoSvc.setImpuestos(new Vector());
                        //model.zonaService.listarZonas();
                        TreeMap agencia = model.agenciaService.listar();
                        
                    } else if ( next.equals("/jsp/cxpagar/corridas/mostrarCorridas.jsp") ){
                        //consultar corridas
                        model.corridaService.buscarCorridasUsuarios(usuario.getLogin(),usuario.getDstrct(), usuario.getId_agencia());
                        
                    }	else if ( (next.equals("/jsp/trafico/unidad/UnidadInsertar.jsp")) || (next.equals("/jsp/trafico/unidad/UnidadBuscar.jsp")) ){
                        //nuevo unidad
                        model.unidadTrfService.cargarTipoUnidad();
                    }  else if ( next.equals("/jsp/trafico/perfil/Perfil.jsp?tipo=Insertar") ) {
                        next = "/controller?estado=Menu&accion=CargaxPerfil&cmd=cgaperfil";
                        model.menuService.obtenerPadres();
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("27")) {
                    //banco
                    model.agenciaService.cargarAgencias();
                    //model.monedaSvc.cargarMonedas();
                } else if (request.getParameter("opcion").equalsIgnoreCase("28")) {
                    //Rep Vehiculos varados y demorados
                    model.agenciaService.loadAgencias();
                    model.clienteService.setTreeMapClientes();
                    model.tramoService.loadOrigenes();
                    //model.tramoService.setDestinos(new TreeMap());
                    model.viaService.setCbxVias(new TreeMap());
                    model.zonaService.loadZonas();
                    
                }    else if (request.getParameter("opcion").equalsIgnoreCase("29")) {
                    /* Programa de Posici�n Bancaria */
                    model.agenciaService.loadAgencias();
                    model.servicioBanco.setBanco(new TreeMap());
                    if ( next.equals("/jsp/publicacion/modificarPublicacion/ModificarPublicacion1.jsp") ){
                        String creation_user = usuario.getLogin().toUpperCase();
                        model.publicacionService.listaPublicaciones(creation_user);
                        int cont = model.publicacionService.getVectorPublicaciones().size();
                        if ( cont > 0 )
                            next = "/jsp/publicacion/modificarPublicacion/ModificarPublicacion1.jsp";
                        
                    }
                }else if (request.getParameter("opcion").equalsIgnoreCase("30")) {
                    model.agenciaService.cargarAgencias();
                    model.monedaSvc.cargarMonedas();
                    TreeMap agencia = model.agenciaService.listar();
                    if ( next.equals("/jsp/publicacion/eliminarPublicacion/EliminarPublicacion1.jsp") ){
                        
                        String creation_user = usuario.getLogin().toUpperCase();
                        model.publicacionService.listaPublicaciones(creation_user);
                        int cont = model.publicacionService.getVectorPublicaciones().size();
                        if ( cont > 0 )
                            next = "/jsp/publicacion/eliminarPublicacion/EliminarPublicacion1.jsp";
                        
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("31")) {
                    
                    String item = request.getParameter("item");
                    if(item.toUpperCase().equals("ADMINSOFT")){
                        model.AdminSoft.loadDatos();
                    } else if(item.toUpperCase().equals("CONSULTASOFT")){
                        model.RevisionSoft.loadDatos();
                    } else if(item.toUpperCase().equals("REVISIONSOFT")){
                        model.RevisionSoft.loadDatos();
                    } else if(item.toUpperCase().equals("CHEQUEFACTURA")){
                        
                        boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                        if( hayTasa ){
                            model.ChequeXFacturaSvc.resetDatos();
                            model.ChequeXFacturaSvc.setUsuario(usuario);
                            model.ChequeXFacturaSvc.searchProveedores();
                            model.ChequeXFacturaSvc.searchDocumentos();
                        }else
                            next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();
                        
                        
                    }else  if(item.toUpperCase().equals("CHEQUEFACTURASCORRIDA")){
                        boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                        if( hayTasa ){
                            next = "/controller?estado=ChequeFacturas&accion=Corridas&evento=FILTER";
                        }else
                            next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();
                        
                    } else  if(item.toUpperCase().equals("LIBERARFACTURAS")) {
                        model.LiberarFacturasSvc.setTreemap(new TreeMap());
                        model.LiberarFacturasSvc.getCorridasParaLiberar((String) session.getAttribute("Distrito"), usuario.getLogin());
                    } else if (item.toUpperCase().equals("GENERAROP")){
                        boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                        if( hayTasa ){
                            model.OpSvc.searchHC();
                        }else
                            next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();
                        
                    } 	else if( item.equals("LIST_PRECHEQUES") ){
                        boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                        if( hayTasa ){
                            model.ChequeXFacturaSvc.setUsuario( usuario );
                            model.ChequeXFacturaSvc.resetDatos();
                            model.ChequeXFacturaSvc.resetCheque();
                            model.ChequeXFacturaSvc.loadPrecheques();
                        }else
                            next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();
                    } else if( item.equals("REIMPRESION") ){
                        next = "/controller?estado=Cheque&accion=Reimpresion&evento=BANCOS";
                    }
                }  else if (request.getParameter("opcion").equalsIgnoreCase("32")){
                    // Leonardo Parody 31-01-06
                    if (next.equalsIgnoreCase("/jsp/finanzas/Cuentas_Validas/CuentasValidasInsert.jsp")) {
                        
                        model.tblgensvc.searchDatosTablaAgencia();
                        model.tblgensvc.searchDatosTablaArea();
                        model.tblgensvc.searchDatosTablaClaseCuenta();
                        model.tblgensvc.searchDatosTablaElementoGasto();
                        model.tblgensvc.searchDatosTablaUnidaProyecto();
                        modelcontab.cuentaService.searchDatosCuentas();
                        TreeMap cuentas = new TreeMap();
                        cuentas = modelcontab.cuentaService.getCuentas();
                        request.setAttribute("cuentas",cuentas);
                        
                        
                    } else if (next.equalsIgnoreCase("/jsp/finanzas/Cuentas_Validas/CuentasValidasUpdate.jsp")){
                        
                        model.tblgensvc.searchDatosTablaAgencia();
                        model.tblgensvc.searchDatosTablaArea();
                        model.tblgensvc.searchDatosTablaClaseCuenta();
                        model.tblgensvc.searchDatosTablaElementoGasto();
                        model.tblgensvc.searchDatosTablaUnidaProyecto();
                        modelcontab.cuentaService.searchDatosCuentas();
                        TreeMap cuentas = new TreeMap();
                        cuentas = modelcontab.cuentaService.getCuentas();
                        request.setAttribute("cuentas",cuentas);
                        
                        
                    } else if (next.equalsIgnoreCase("/jsp/finanzas/Cuentas_Validas/CuentasValidasSearch.jsp")){
                        
                        model.tblgensvc.searchDatosTablaAgencia();
                        model.tblgensvc.searchDatosTablaArea();
                        model.tblgensvc.searchDatosTablaClaseCuenta();
                        model.tblgensvc.searchDatosTablaElementoGasto();
                        model.tblgensvc.searchDatosTablaUnidaProyecto();
                        modelcontab.cuentaService.searchDatosCuentas();
                        TreeMap cuentas = new TreeMap();
                        
                        cuentas = modelcontab.cuentaService.getCuentas();
                        model.cuentavalidaservice.setCuentasTree(cuentas);
                        ////System.out.println("Ya cargue el arbol de cuentas");
                        
                    }
                } else if (request.getParameter("opcion").equalsIgnoreCase("33")){
                    /* Ing. Tito Andr�s Maturana D.*/
                    if (request.getParameter("item").equalsIgnoreCase("1")) {
                        /* Rep Extrafletes y Costos Reembolsables */
                        model.clienteService.setTreeMapClientes();
                        model.RemDocSvc.loadDocumentos();
                        /* Rep Extrafletes y Costos Reembolsables */
                    } else if (request.getParameter("item").equalsIgnoreCase("2")) {
                        /* Presupuesto gastos administrativos */
                        model.tblgensvc.searchDatosTablaAgencia();
                        model.tblgensvc.searchDatosTablaArea();
                        model.tblgensvc.searchDatosTablaElementoGasto();
                        model.tblgensvc.searchDatosTablaUnidaProyecto();
                        model.tblgensvc.searchDatosTablaClaseCuenta();
                        
                    } else if (request.getParameter("item").equalsIgnoreCase("3")) {
                        /* Informe de Facturaci�n */
                        model.stdjobdetselService.buscarStdjobcarbon();
                    } else if (request.getParameter("item").equalsIgnoreCase("4")) {
                        /* Indicadores */
                        String cia = (String) session.getAttribute("Distrito");
                        model.indicadoresSvc.load(cia);
                        model.indicadoresSvc.reiniciar();
                        model.indicadoresSvc.buscarHijos(cia, model.indicadoresSvc.getRaiz());
                        model.indicadoresSvc.soloPadres(cia);
                        model.tblgensvc.searchDatosTablaMetodo();
                    } else if (request.getParameter("item").equalsIgnoreCase("5")) {
                        /* Precargar Agencias */
                        model.agenciaService.loadAgencias();
                    } else if (request.getParameter("item").equalsIgnoreCase("6")) {
                        /* Documentos escaneados de cumplidos */
                        String usu_dest = model.cumplido_docService.clientesAsignados(usuario.getLogin());
                        StringTokenizer st = new StringTokenizer(usu_dest,"#");
                        TreeMap cli_dest = new TreeMap();
                        
                        while( st.hasMoreTokens()){
                            String codcli = st.nextToken();
                            model.clienteService.datosCliente(codcli);
                            Cliente cli = model.clienteService.getCliente();
                            cli_dest.put(cli.getNomcli(), codcli);
                        }
                        model.clienteService.setTreemap(cli_dest);
                    } else if (request.getParameter("item").equalsIgnoreCase("7")) {
                        /* Depuracion Masiva de Tramos */
                        model.paisservice.loadPaises();
                        model.ciudadService.setCiudadTM(new TreeMap());
                    } else if (request.getParameter("item").equalsIgnoreCase("8")) {
                        /* Reporte de An�lisis de Vencimiento de CxP */
                        TreeMap tm = new TreeMap();
                        model.PrestamoSvc.loadBeneficiarios(usuario.getId_agencia());
                        List lst = model.PrestamoSvc.getBeneficiarios();
                        for(int i=0; i< lst.size(); i++){
                            Hashtable tabla = (Hashtable) lst.get(i);
                            String nit  = (String) tabla.get("nit");
                            String name = (String) tabla.get("nombre");
                            tm.put(name, nit);
                        }
                        request.setAttribute("trm", tm);
                    } else if (request.getParameter("item").equalsIgnoreCase("10")) {
                        /* Definici�n de Tipos de Comprobantes */
                        modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
                        modelcontab.tipo_doctoSvc.codigosInternos();
                        TreeMap tm = modelcontab.tipo_doctoSvc.getTreemap();
                        com.tsp.finanzas.contab.model.beans.Tipo_Docto tdoc;
                        tdoc =  new com.tsp.finanzas.contab.model.beans.Tipo_Docto(tm.get((String) tm.firstKey()).toString());
                        request.setAttribute("TDocto", tdoc);
                        
                    } else if (request.getParameter("item").equalsIgnoreCase("11")) {
                        /* Reporte de Facturas de Clientes */
                        model.tablaGenService.loadTUsuariosFactura();
                        TreeMap tmap = model.tablaGenService.getTreemap();
                        request.setAttribute("TUser", tmap);
                    }  else if (request.getParameter("item").equalsIgnoreCase("12")) {
                        /* Ingresar Plan de Cuentas */
                        modelcontab.planDeCuentasService.setElementos(new Vector());
                        modelcontab.planDeCuentasService.setHashtable(new Hashtable());
                        session.setAttribute( "cuenta", "" );
                        session.setAttribute( "nom_largo", "" );
                        session.setAttribute( "nom_corto", "" );
                        session.setAttribute( "observacion", "" );
                        session.setAttribute( "auxiliar", "N" );
                        session.setAttribute( "nivel", "1" );
                        session.setAttribute( "subledger", "N" );
                        session.setAttribute( "tercero", "N" );
                        session.setAttribute( "cta_dependiente", "" );
                        session.setAttribute( "cta_cierre", "" );
                        session.setAttribute( "modulo1", "S" );
                        session.setAttribute( "modulo2", "S" );
                        session.setAttribute( "modulo3", "S" );
                        session.setAttribute( "modulo4", "S" );
                        session.setAttribute( "modulo5", "S" );
                        session.setAttribute( "modulo6", "S" );
                        session.setAttribute( "modulo7", "S" );
                        session.setAttribute( "modulo8", "S" );
                        session.setAttribute( "modulo9", "S" );
                        session.setAttribute( "modulo10", "S" );
                        
                        session.setAttribute( "detalle", "N" );
                        
                    } else if (request.getParameter("item").equalsIgnoreCase("13")) {
                        /* Reporte de Egresos */
                        List ListaAgencias = model.ciudadService.ListarAgencias();
                        TreeMap tm = new TreeMap();
                        if(ListaAgencias.size()>0) {
                            Iterator It3 = ListaAgencias.iterator();
                            while(It3.hasNext()) {
                                Ciudad  datos2 =  (Ciudad) It3.next();
                                tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                                //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                            }
                        }
                        request.setAttribute("agencias", ListaAgencias);
                        request.setAttribute("agencias_tm", tm);
                        model.servicioBanco.setBanco(new TreeMap());
                        model.servicioBanco.setSucursal(new TreeMap());
                    } else if (request.getParameter("item").equalsIgnoreCase("14")) {
                        /* Consultar Facturas */
                        model.clienteService.BusquedaSoporteAD();
                        model.clienteService.getVectorsSoporte();
                        
                        //seteo el vector para cargar soporte
                        model.clienteService.setVectorsSoporte(new Vector()); 
                        
                        
                        model.clienteService.setCedula("");
                        
                        model.clienteService.setTreeMapClientes();
                        List ListaAgencias = model.clienteService.listarAgecniaFacturacion();
                        TreeMap tm = new TreeMap();
                        if(ListaAgencias.size()>0) {
                            Iterator It3 = ListaAgencias.iterator();
                            while(It3.hasNext()) {
                                Ciudad  datos2 =  (Ciudad) It3.next();
                                tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                                //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                            }
                        }
                        String cli = "";
                        model.clienteService.CodCliente();
                        
                        request.setAttribute("agencias", ListaAgencias);
                        request.setAttribute("agencias_tm", tm);
                        model.tablaGenService.listarHc();
                        model.tablaGenService.listarFpago();
                        model.servicioBanco.setBanco(new TreeMap());
                        model.servicioBanco.setSucursal(new TreeMap());
                        
                        /******************************************************************
                         * creacion de codigo del cliente  de la tabla serie
                         * /******************************************************************/
                        
                    } else if (request.getParameter("item").equalsIgnoreCase("15")) {
                        /* Reporte de Cartera */
                        model.clienteService.setTreeMapClientes();
                        model.ciudadService.loadListadoAgencias();
                    } else if (request.getParameter("item").equalsIgnoreCase("16")) {
                        /* Consultar Cheques Pendientes */
                        List ListaAgencias = model.ciudadService.ListarAgencias();
                        TreeMap tm = new TreeMap();
                        if(ListaAgencias.size()>0) {
                            Iterator It3 = ListaAgencias.iterator();
                            while(It3.hasNext()) {
                                Ciudad  datos2 =  (Ciudad) It3.next();
                                tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                                //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                            }
                        }
                        request.setAttribute("agencias", ListaAgencias);
                        request.setAttribute("agencias_tm", tm);
                    } else if (request.getParameter("item").equalsIgnoreCase("17")) {
                        /* Anular Cheques CxP */
                        //model.servicioBanco.loadBancos((String)session.getAttribute("Agencia"), (String) session.getAttribute("Distrito"));
                        model.servicioBanco.obtenerNombresBancos();
                        model.servicioBanco.setSucursal(new TreeMap());
                    } else if (request.getParameter("item").equalsIgnoreCase("18")) {
                        /* Consultar Cheques Pendientes */
                        List ListaAgencias = model.ciudadService.ListarAgencias();
                        TreeMap tm = new TreeMap();
                        if(ListaAgencias.size()>0) {
                            Iterator It3 = ListaAgencias.iterator();
                            while(It3.hasNext()) {
                                Ciudad  datos2 =  (Ciudad) It3.next();
                                tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                                //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                            }
                        }
                        request.setAttribute("agencias", ListaAgencias);
                        request.setAttribute("agencias_tm", tm);
                        //model.servicioBanco.setBanco(new TreeMap());
                        model.servicioBanco.obtenerNombresBancos();
                        model.servicioBanco.setSucursal(new TreeMap());
                    } else if (request.getParameter("item").equalsIgnoreCase("cargando") || request.getParameter("item").equalsIgnoreCase("cargandomod")) {
                        try {
                            HttpSession seciones  = request.getSession();
                            Usuario usuarioBanco  = (Usuario) seciones.getAttribute("Usuario");
                            String agencia= "%";
                            
                            String banco = request.getParameter("BanConsignacion");
                            String sucursal = request.getParameter("SucBanco");
                            String op = request.getParameter("item");
                            request.setAttribute("agencia", agencia );
                            request.setAttribute("banco", banco );
                            request.setAttribute("sucursal", sucursal );
                            //System.out.println("op = "+op);
                            if (op.equals("cargando")) {
                                next = "/jsp/finanzas/contab/comprobante/traslados/traslado_adicionar.jsp";
                            }
                            if(op.equals("cargandomod")){
                                //System.out.println("entro al la opcion");
                                next = "/jsp/finanzas/contab/comprobante/traslados/tasladoBuscar.jsp";
                            }
                            
                            String target =  "bancos";
                            List ListaAgencias = model.ciudadService.ListarAgencias();
                            TreeMap tm = new TreeMap();
                            if(ListaAgencias.size()>0) {
                                Iterator It3 = ListaAgencias.iterator();
                                while(It3.hasNext()) {
                                    Ciudad  datos2 =  (Ciudad) It3.next();
                                    tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                                    //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                                }
                            }
                            request.setAttribute("agencias", ListaAgencias);
                            request.setAttribute("agencias_tm", tm);
                            
                            if( target.length()!=0 && target.compareTo("bancos")==0 ){
                                model.servicioBanco.loadBancos(agencia, (String) session.getAttribute("Distrito"));
                                model.servicioBanco.setSucursal(new TreeMap());
                            } else if( target.length()!=0 && target.compareTo("sucursales")==0 ){
                                model.servicioBanco.loadSucursalesAgencia(agencia, banco, (String) session.getAttribute("Distrito"));
                                TreeMap SucBanco = model.servicioBanco.getSucursal();
                            }
                            //System.out.println("treepmat"+model.servicioBanco.getBanco());
                            
                        }catch(Exception e){
                            e.printStackTrace();
                            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
                        }
                        
                    } else if (request.getParameter("item").equalsIgnoreCase("19")) {
                        /* Consultar Facturas */
                        List ListaAgencias = model.ciudadService.ListarAgencias();//jdelarosa
                        TreeMap tm = new TreeMap();
                        if(ListaAgencias.size()>0) {
                            Iterator It3 = ListaAgencias.iterator();
                            while(It3.hasNext()) {
                                Ciudad  datos2 =  (Ciudad) It3.next();
                                tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());                                
                            }
                        }
                        String cli = "";
                        model.clienteService.CodCliente();
                        
                        request.setAttribute("agencias", ListaAgencias);
                        request.setAttribute("agencias_tm", tm);
                        model.tablaGenService.listarHc();
                        model.tablaGenService.listarFpago();
                        model.servicioBanco.setBanco(new TreeMap());
                        model.servicioBanco.setSucursal(new TreeMap());
                    } else if (request.getParameter("item").equalsIgnoreCase("20")) {
                        /* Serie de documentos anulados */
                        request.setAttribute("tdoc", model.tablaGenService.loadTableType("TDOC"));
                        request.setAttribute("tcanul", model.tablaGenService.loadTableType("TCAUSANUL"));
                    } else if (request.getParameter("item").equalsIgnoreCase("21")) {
                        /* Consulta de RxP x concepto */
                        model.factrecurrService.cargarConceptos(usuario.getDstrct());
                        request.setAttribute("conceptos", model.factrecurrService.getTreemap());
                    }
                    else if (request.getParameter("item").equalsIgnoreCase("propietario") ) {
                       try {
                            model.tablaGenService.listarTODOHc();
                            HttpSession seciones  = request.getSession();
                            Usuario usuarioBanco  = (Usuario) seciones.getAttribute("Usuario");
                            String agencia= "%";

                            String banco = request.getParameter("BanConsignacion");
                            String sucursal = request.getParameter("SucBanco");
                            String op = request.getParameter("item");
                            request.setAttribute("agencia", agencia );
                            request.setAttribute("banco", banco );
                            request.setAttribute("sucursal", sucursal );
                            //System.out.println("op = "+op);
                            String target =  "bancos";
                            List ListaAgencias = model.ciudadService.ListarAgencias();
                            TreeMap tm = new TreeMap();
                            if(ListaAgencias.size()>0) {
                                Iterator It3 = ListaAgencias.iterator();
                                while(It3.hasNext()) {
                                    Ciudad  datos2 =  (Ciudad) It3.next();
                                    tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                                    //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                                }
                            }
                            request.setAttribute("agencias", ListaAgencias);
                            request.setAttribute("agencias_tm", tm);
                            
                            if( target.length()!=0 && target.compareTo("bancos")==0 ){
                                model.servicioBanco.loadBancos(agencia, (String) session.getAttribute("Distrito"));
                                model.servicioBanco.setSucursal(new TreeMap());
                            } else if( target.length()!=0 && target.compareTo("sucursales")==0 ){
                                model.servicioBanco.loadSucursalesAgencia(agencia, banco, (String) session.getAttribute("Distrito"));
                                TreeMap SucBanco = model.servicioBanco.getSucursal();
                            }
                            //System.out.println("treepmat"+model.servicioBanco.getBanco());
                            
                        }catch(Exception e){
                            e.printStackTrace();
                            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
                        }
                    }
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("34")){
                    /* Juan Escandon */
                    
                    if ( next.equals("/jsp/cxpagar/Tipo_impuestos/Tipo_impuesto.jsp") ){
                        model.ciudadService.ListaAgencias();
                        model.TimpuestoSvc.ReiniciarLista();
                        String agcs = model.agenciaService.Agencias();
                        request.setAttribute("agenciasLst", agcs);
                        model.TimpuestoSvc.ReiniciarDato();
                    } else if ( next.equals("/jsp/cxpagar/Tipo_impuestos/Tipo_impuestoListar.jsp") ){
                        model.TimpuestoSvc.ReiniciarDato();
                        model.ciudadService.ListaAgencias();
                        model.TimpuestoSvc.ReiniciarLista();
                        model.TimpuestoSvc.LIST("","","");
                        String agcs = model.agenciaService.Agencias();
                        request.setAttribute("agenciasLst", agcs);
                    } else if ( next.equals("/jsp/cxpagar/banco/BancoInsertar.jsp") || next.equals("/jsp/cxpagar/banco/BancoBuscar.jsp") ){
                        model.agenciaService.cargarAgencias();
                        model.monedaSvc.cargarMonedas();
                    } else  if (request.getParameter("item").equalsIgnoreCase("1")) {
                        /* FACTURAS RECURRENTES */
                        next = "/controller?estado=FactRecurr&accion=LArchivo";
                    } else if (request.getParameter("item").equalsIgnoreCase("2")) {
                        //Listado de Clientes
                        TreeMap cli = model.clienteService.listarClientes();
                        request.setAttribute("TClientes", cli);
                        
                    } else if ( (request.getParameter("item").equalsIgnoreCase("3") )) {
                        model.remesaImpresionSvc.ReiniciarLista();
                    } else if ( (request.getParameter("item").equalsIgnoreCase("4") )) {
                        model.egresoService.reiniciarListaChk();
                        session.setAttribute("fltBanco"   , "");
                        session.setAttribute("fltSucursal", "");
                        session.setAttribute("fltRangoini", "");
                        session.setAttribute("fltRangofin", "");
                    }//Jescandon 23-02-07
                    /*Jose 03-04-2006*/
                } else if(request.getParameter("opcion").equalsIgnoreCase("CCPlaca")){
                    ResourceBundle  dbProps = ResourceBundle.getBundle("com/tsp/operation/model/DAOS/StoreInf");
                    String fecha = dbProps.getString("fechaCargue");
                    next = next +"?fechai="+fecha;
                } else if (request.getParameter("opcion").equalsIgnoreCase("35")) {
                    String JSP    = request.getParameter("pagina");
                    // creacion de prestamos
                    if( JSP.equalsIgnoreCase("CrearPrestamo.jsp") ){
                        model.PrestamoSvc.loadList();
                    }
                    
                    // consulta de coutas a migrar
                    else if( JSP.equalsIgnoreCase("Cuotas.jsp") ){
                        model.PrestamoSvc.loadTerceros();
                    }
                    
                    // Resumen genaral:
                    else if( JSP.equalsIgnoreCase("ResumenPrestamos.jsp") ){
                        model.PrestamoSvc.loadTerceros();
                        model.PrestamoSvc.loadClasificacionesPrestamos();
                    }
                    
                    // Ingresos
                    else if( JSP.equalsIgnoreCase("Ingresos.jsp") ){
                        model.PrestamoSvc.loadTerceros();
                    }
                    
                    // Migrar Cuotas por proveedor
                    else if( JSP.equalsIgnoreCase("MigracionCuotasProveedor.jsp") ){
                        model.PrestamoSvc.loadTerceros();
                        model.PrestamoSvc.loadBeneficiarios( usuario.getId_agencia() );
                    }
                    
                    else if (JSP.equals("Esquema_formatoInsertar.jsp") || JSP.equals("Esquema_formatoBuscar.jsp")) {//jose de la rosa
                        model.tablaGenService.buscarRegistros("TFORDESP");
                        request.setAttribute( "set_formato", model.tablaGenService.obtenerTablas());
                        model.tablaGenService.buscarRegistros("TFORTIP");
                        request.setAttribute( "set_tipo", model.tablaGenService.obtenerTablas());
                        model.tablaGenService.buscarRegistros("TBLFORMA");
                    }
                    
                    // Consulta de prestamos
                    else if (JSP.equalsIgnoreCase("ConsultaPrestamos.jsp")){
                        model.PrestamoSvc.loadTerceros();
                        model.PrestamoSvc.loadPeriodos();
                        model.PrestamoSvc.loadTipoPrestamos();
                        model.PrestamoSvc.loadClasificacionesPrestamos();
                    }
                    
                    // amortizaciones transferidas
                    else if ( JSP.equalsIgnoreCase("ConsultaTransferencias.jsp") ){
                        model.ciaService.listarDistritos();
                        model.PrestamoSvc.loadTerceros();
                    }
                    
                    else if (JSP.equals("ReporteTiemposViaje.jsp")) {//jose de la rosa
                        model.tablaGenService.buscarRegistros("TIPORUTA");
                    }
                    else if ( JSP.equals("FormatoTablaIngreso.jsp") || JSP.equals("FormatoTablaModificar.jsp") ) {//jose de la rosa 2006-11-27
                        model.formato_tablaService.setVecftabla(null);
                        if( !JSP.equals("FormatoTablaIngreso.jsp") )
                            model.formato_tablaService.formatoTablaModificar("TNOMFOR");
                    }  else if( JSP.equalsIgnoreCase("BuscarDatosRemesa.jsp") ){
                        model.remesaService.setRemesas( null );
                        session.setAttribute("inicio", null);
                        session.setAttribute("fin", null);
                        session.setAttribute("cliente", null);
                        session.setAttribute("cliente", null);
                    }  else if( JSP.equalsIgnoreCase("DatosEscoltaVehiculo.jsp") ){
                        session.setAttribute("tablaProv",(String)request.getParameter("tablaProv"));
                        session.setAttribute("proveedor", (String)request.getParameter("proveedor"));
                        model.escoltaVehiculoSVC.listadoEscoltaSinGeneracion( model.tablaGenService.buscarCampoTablagenDato( (String)request.getParameter("tablaProv"), (String)request.getParameter("proveedor"), "1").toString().trim(), distrito );
                    }
                    
                    else if( JSP.equalsIgnoreCase("GeneracionEscoltaVehiculo.jsp") ){
                        session.setAttribute("tablaProv",(String)request.getParameter("tablaProv"));
                        session.setAttribute("proveedor", (String)request.getParameter("proveedor"));
                        model.escoltaVehiculoSVC.listadoEscoltaConFactura( model.tablaGenService.buscarCampoTablagenDato( (String)request.getParameter("tablaProv"), (String)request.getParameter("proveedor"), "1").toString().trim(), distrito );
                        model.tablaGenService.buscarRegistros(request.getParameter("tabla"));
                        for(int i = 0 ; i < model.tablaGenService.obtenerTablas().size() ; i++){
                            TablaGen bean = (TablaGen) model.tablaGenService.obtenerTablas().get(i);
                            if( bean.getTable_code().equals(usuario.getLogin()) )
                                request.setAttribute("aprobacion","ok" );
                        }
                    } else if (JSP.equals("EquivalenciaCargaInsertar.jsp") || JSP.equals("EquivalenciaCargaBuscar.jsp")) {//jose de la rosa
                        if( !JSP.equals("EquivalenciaCargaBuscar.jsp") ){
                            model.tablaGenService.buscarRegistros("TUNIDAD");
                            request.setAttribute( "set_unidad", model.tablaGenService.obtenerTablas());
                        }
                        model.tablaGenService.buscarRegistros("TIPCARGA");
                        request.setAttribute( "set_tipo", model.tablaGenService.obtenerTablas());
                    }
                    
                } else if ( request.getParameter("opcion").equalsIgnoreCase("36") ){
                    // opciones de conceptos
                    if (next.equals("/jsp/masivo/conceptos/edicion.jsp") ){
                        request.setAttribute("Accion", "Insert");
                        model.ciaService.listarDistritos();
                        model.tablaGenService.buscarRegistros( "TCONCCLASS" );
                        model.ConceptosSvc.setCmbClases(
                        model.tablaGenService.obtenerTreeMapLista(
                        model.tablaGenService.obtenerTablas()
                        )
                        );
                    }
                    else if (next.equals("/jsp/masivo/conceptos/listado.jsp") ){
                        model.ConceptosSvc.obtenerTodosLosConceptos();
                    }
                    else if (next.equals("/jsp/trafico/reportes/Rep.jsp") ){
                        next = "/controller?estado=Reporte&accion=PlanillasVia&evento=INIT";
                    }
                } else if ( request.getParameter("opcion").equalsIgnoreCase("37") ){
                    if (next.equals("/jsp/finanzas/contab/interfaceContable/ingresarInterfaceContable.jsp") ){
                        
                        com.tsp.finanzas.contab.model.beans.Tipo_Docto tipodocto = new com.tsp.finanzas.contab.model.beans.Tipo_Docto();
                        tipodocto.setDistrito( distrito );
                        modelcontab.tipo_doctoSvc.setDocto( tipodocto );
                        modelcontab.tipo_doctoSvc.ObtenerTipo_docto();
                        modelcontab.cmcService.obtenerCmc();
                    }else if (next.equals("/jsp/finanzas/contab/interfaceContable/buscarInterfaceContable.jsp") ){
                        com.tsp.finanzas.contab.model.beans.Tipo_Docto tipodocto = new com.tsp.finanzas.contab.model.beans.Tipo_Docto();
                        tipodocto.setDistrito( distrito );
                        modelcontab.tipo_doctoSvc.setDocto( tipodocto );
                        modelcontab.tipo_doctoSvc.ObtenerTipo_docto();
                        modelcontab.cmcService.obtenerCmc();
                    }else if (next.equals("/jsp/finanzas/contab/adminComprobantes/buscarComprobantes.jsp") ){
                        com.tsp.finanzas.contab.model.beans.Tipo_Docto tipodocto = new com.tsp.finanzas.contab.model.beans.Tipo_Docto();
                        tipodocto.setDistrito( distrito );
                        modelcontab.tipo_doctoSvc.setDocto( tipodocto );
                        modelcontab.tipo_doctoSvc.ObtenerTipo_docto();
                        session.setAttribute( "resultado", null );
                        session.setAttribute( "tipodoc", null );
                        session.setAttribute( "anio", null );
                        session.setAttribute( "mes", null );
                        session.setAttribute( "numdoc", null );
                        session.setAttribute( "criterios", null );
                        session.setAttribute( "orden", null );
                        session.setAttribute( "fechaInicial", null );
                        session.setAttribute( "fechaFinal", null );
                    } if (next.equals("/jsp/cxpagar/consultaEgresos/consultarEgresos.jsp") ){
                        
                        model.servicioBanco.obtenerBancos( usuario.getDstrct() );
                        
                        model.tblgensvc.buscarLista( "TDOC", "EGRESO" );
                        model.egresoService.setListaTipoEgresos( model.tblgensvc.getLista_des());
                        model.tblgensvc.buscarLista( "TCHEQUES", "EGRESO" );
                        model.egresoService.setListaTipoConceptos( model.tblgensvc.getLista_des());
                        
                        session.setAttribute( "branch_code", null );
                        session.setAttribute( "bank_account_no", null );
                        session.setAttribute( "document_no", null );
                        session.setAttribute( "tipo_documento", null );
                        session.setAttribute( "fechaInicial", null );
                        session.setAttribute( "fechaFinal", null );
                        session.setAttribute( "opc_sucursales", null );
                        session.setAttribute( "resultadoE", null );
                        session.setAttribute( "conceptos", null );
                        session.setAttribute( "usuario_creacion", null );
                    }
                }else if(request.getParameter("opcion").equalsIgnoreCase("LPaisAduana")) {
                    
                    model.paisservice.listarPaises();
                    model.ciudadService.listarFronteras("__");
                    model.tblgensvc.buscarLista("REP", "REP_TRA");
                    model.remesaService.setRemesas(null);
                    
                } else if(request.getParameter("opcion").equalsIgnoreCase("IMAGEN")) {
                    if (next.equalsIgnoreCase("imagen/Manejo.jsp") )
                        next = "/controller?estado=Load&accion=Imagenes&pagina=/imagen/Manejo.jsp&evento=RESET";
                    else if (next.equalsIgnoreCase("imagen/Busqueda.jsp") )
                        next = "/controller?estado=Load&accion=Imagenes&pagina=/imagen/Busqueda.jsp&evento=RESET";
                } else if (request.getParameter("opcion").equalsIgnoreCase("38")) {
                    //Oswaldo
                    if (next.equals("/jsp/trafico/planviaje/planillasSinPlanViaje.jsp") ){
                        next = "/controller?estado=Planillas&accion=SinPlanViaje";
                    }
                    /*Opciones de Contabiliad*/
                } else if (request.getParameter("opcion").equalsIgnoreCase("Contabilidad")) {
                    if (next.equals("/jsp/finanzas/contab.jsp") ) {
                        next = "/controllercontab?estado=Contabilizacion&accion=Facturas";
                    } else if (next.equals("/jsp/finanzas/movaux.jsp") ) {
                        next="/controllercontab?estado=Mov&accion=Auxiliar&evento=INIT";
                    }else if(next.equals("/jsp/finanzas/contab/cuenta_tipo_subledger/cuenta_tipo_subledger.jsp")){
                        model.tablaGenService.buscarRegistros("TSUBLEDGER");
                    }
                    else if(next.equals("/jsp/finanzas/contab/subledger/subledger.jsp")){
                        modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
                        model.tablaGenService.buscarRegistros("TSUBLEDGER");
                    }else if(next.equals("/jsp/finanzas/contab/subledger/buscarsubledger.jsp")){
                        model.tablaGenService.buscarRegistros("TSUBLEDGER");
                    }else if (next.equals("/jsp/finanzas/contab.jsp") ) {
                        next = "/controllercontab?estado=Contabilizacion&accion=Facturas";
                    } else if (next.equals("/jsp/finanzas/movaux.jsp") ) {
                        next="/controllercontab?estado=Mov&accion=Auxiliar&evento=INIT";
                    }else if(next.equals("/jsp/finanzas/contab/cuenta_tipo_subledger/cuenta_tipo_subledger.jsp")){
                        model.tablaGenService.buscarRegistros("TSUBLEDGER");
                    }else if(next.equals("/jsp/finanzas/contab/PeriodoContable/periodoContableListar.jsp")){
                        next = "/controllercontab?estado=Periodo&accion=Contable&opcion=list";
                        //Ivan grabar comprobante
                    }else if(next.equals("/jsp/finanzas/contab/comprobante/grabacionComprobante.jsp")){
                        next = "/controllercontab?estado=Grabar&accion=Comprobante&OP=INICIO";
                    } else if(next.equals("/jsp/finanzas/contab/comprobante/ModificarComprobante.jsp")){
                        next = "/controllercontab?estado=Grabar&accion=Comprobante&OP=MODIFICAR";
                    } else if (next.equals("/jsp/finanzas/contab/impresion/FormularioImpresion.jsp")){
                        com.tsp.finanzas.contab.model.beans.Tipo_Docto td = new com.tsp.finanzas.contab.model.beans.Tipo_Docto();
                        td.setDistrito(distrito);
                        modelcontab.tipo_doctoSvc.setDocto(td);
                        modelcontab.tipo_doctoSvc.ObtenerTipo_docto();
                    }
                    
                } else if(request.getParameter("opcion").equalsIgnoreCase("equipos_propios")) {
                    model.tablaGenService.buscarRegistrosNoAnulados("CLAEQUI");
                } else if(request.getParameter("opcion").equalsIgnoreCase("recursos_disponibles")){
                    next = "/controller?estado=Recursos&accion=Disponibles&opcion=filter";
                }else if(request.getParameter("opcion").equalsIgnoreCase("cxcobrar")) {
                    String age = "";
                    String moneda = (String) session.getAttribute("Moneda");
                    if(next.equals("/jsp/cxcobrar/ingreso/ingreso.jsp")){
                        session.setAttribute("msg","");
                        session.setAttribute("msg2","");
                        model.tablaGenService.buscarCodigosABC();
                        model.tablaGenService.buscarTipoIngreso("INGRESO");
                        model.servicioBanco.loadBancos(age, usuario.getCia());
                        model.servicioBanco.setSucursal(new TreeMap());
                        model.monedaService.cargarMonedas();
                        model.tablaGenService.buscarRegistros("CONINGRESO");
                        model.clienteService.setCliente(null);
                        model.ingresoService.setTemporal(false);
                        model.ingresoService.setNro_ing("");
                        modelcontab.subledgerService.buscarCuentasTipoSubledger( "" );
                        
                        next+="?moneda="+moneda+"&concepto=TR&tipodoc=ING";//jose 2007-03-06
                    }
                    else if(next.equals("/jsp/cxcobrar/ingresoMiscelaneo/ingresoMiscelaneo.jsp")){
                        model.tablaGenService.buscarTipoIngreso("MISCELANEO");
                        model.servicioBanco.loadBancos(age, usuario.getCia());
                        model.servicioBanco.setSucursal(new TreeMap());
                        model.monedaService.cargarMonedas();
                        model.tablaGenService.buscarRegistros("CONINGRESO");
                        model.ingresoService.setTempmis(false);
                        model.ingresoService.setNro_ingmis("");
                        next+="?moneda="+moneda;
                        
                    } else if(next.equals("/jsp/cxcobrar/ingreso/BuscarDatosIngreso.jsp")){
                        model.tablaGenService.buscarCodigosABC();
                        model.tblgensvc.buscarLista( "TDOC", "INGRESO" );
                    } else if( next.equals("/jsp/cxcobrar/ingreso/BuscarIngreso.jsp") ){
                        model.tablaGenService.buscarCodigosABC();
                        model.tablaGenService.buscarTipoIngreso("INGRESO");
                    } else if( next.equals("/jsp/cxcobrar/ingresoMiscelaneo/BuscarIngreso.jsp") ){
                        model.tablaGenService.buscarTipoIngreso("MISCELANEO");
                    }
                }else if(request.getParameter("opcion").equalsIgnoreCase("prestamo")) {
                    //System.out.println("jujujujujujuj");
                    if (next.equals("/jsp/fintra/1.jsp")){
                        next = "/controller?estado=Anticipos&accion=PagosTerceros&evento=INITCONSULTA";      //Aprobacion Anticipos
                    } else if (next.equals("/jsp/fintra/2.jsp")){
                        next = "/controller?estado=Anticipos&accion=PagosTerceros&evento=BUSCARAPROBADAS";   //Transferir Anticipos
                    } else if (next.equals("/jsp/fintra/3.jsp")){
                        next = "/controller?estado=Cuentas&accion=Propietarios";                            // Administrar Cuenta propietarios
                    } else if (next.equals("/jsp/fintra/4.jsp")) {
                        next="/controller?estado=Consulta&accion=AnticiposTerceros"; //Consultas
                    } else if (next.equals("/jsp/fintra/5.jsp")) {
                        next = "/controller?estado=ReporteContable&accion=Anticipos";    //Reporte Producci�n";
                    } else if  (next.equals("/jsp/fintra/6.jsp")) {
                        next = "/controller?estado=Status&accion=Barra"; //Monitoreo
                    }else if (next.equals("/jsp/fintra/7.jsp")){
                        //System.out.println("por lo menos llego");
                        next = "/controller?estado=Captaciones&accion=Fintra&opcion=BUSCARAPROBADAS";   //Transferir Anticipos
                    }
                    
                } else if(request.getParameter("opcion").equalsIgnoreCase("facturacionc")){
                    boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                    if( hayTasa ){
                        model.facturaService.setVFacturas(new Vector());
                    }else
                        next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();
                    
                } else if (request.getParameter("opcion").equalsIgnoreCase("formato")) {
                    if (next.equals("/jsp/formatonacional.jsp")) {
                        next ="/controller?estado=FormatoNacional&accion=Datos&evento=INIT";
                    } else if (next.equals("/jsp/pendiente.jsp")){
                        next = "/controller?estado=ReporteBanco&accion=Transferencia";
                    }
                }  else if(request.getParameter("opcion").equalsIgnoreCase("cargarDocto")){
                    com.tsp.finanzas.contab.model.beans.Tipo_Docto tipodocto = new com.tsp.finanzas.contab.model.beans.Tipo_Docto();
                    tipodocto.setDistrito( distrito );
                    modelcontab.tipo_doctoSvc.setDocto( tipodocto );
                    modelcontab.tipo_doctoSvc.ObtenerTipo_docto();
                }  else if(request.getParameter("opcion").equalsIgnoreCase("formulario")) {
                    if (next.equals("/jsp/general/form_tabla/Formulario.jsp")||next.equals("/jsp/general/form_tabla/BuscarDatos.jsp")) {
                        model.formato_tablaService.formatoDefinidos("TNOMFOR");
                        model.formato_tablaService.setVecCampos(new Vector());
                        model.formato_tablaService.setVecbusqueda(new Vector());
                    }
                }  else if(request.getParameter("opcion").equalsIgnoreCase("Iniciofacturacion")){
                    boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                    if( hayTasa ){
                        next  = "/jsp/cxcobrar/facturacion/Iniciofacturacion.jsp";
                    }else
                        next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();
                    
                }   else if(request.getParameter("opcion").equalsIgnoreCase("PerfilesGen")){
                    
                    model.PerfilesGenService.buscarPerfiles();
                    model.PerfilesGenService.buscarTablas();
                    model.PerfilesGenService.buscarTP();
                    next  = "/jsp/masivo/PerfilesGen/PerfilesGen.jsp";
                }   else if(request.getParameter("opcion").equals("InfoTrakin")){
                    model.ciudadService.searchTreMapCiudades();
                }   else if(request.getParameter("opcion").equalsIgnoreCase("GRABACION_STANDAR")){
                    model.StandarSvc.buscarTableCode("TUNIDADES");
                    model.StandarSvc.buscarAgentesResponsable("AGENTERESP");
                    model.StandarSvc.buscarTiporuta("TIPORUTA");
                    model.StandarSvc.buscarUnidades("TUNIDAD");
                    model.StandarSvc.buscarTipoFacturacion("TIPOFACT");
                    model.StandarSvc.buscarFronteraAsociada("FRONTASOC");
                    model.cxpDocService.obtenerAgencias();
                    model.StandarSvc.buscarMonedas();
                    model.StandarSvc.setStandarVacio();
                } else if(request.getParameter("opcion").equalsIgnoreCase("REIMPRESION_CHEQUE")){
                    String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
                    model.servicioBanco.loadBancos(age, usuario.getCia());
                }  else if(request.getParameter("opcion").equalsIgnoreCase("CORRIDASTRANSFERENCIA")){
                    String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
                    model.servicioBanco.loadBancos(age, usuario.getCia());
                    next  = "/jsp/cxpagar/transferencias/FiltroCorridaTransferencia.jsp";
                }else if (request.getParameter("opcion").equals("UPDATE_BANCOS_X_PORVEEDOR")){
                    model.ChequeFactXCorridasSvc.loadBancos( usuario.getDstrct(), usuario.getId_agencia() );
                    model.ChequeFactXCorridasSvc.loadSucursales(usuario.getDstrct(), usuario.getId_agencia() );
                }else if(request.getParameter("opcion").equals("ESTADO_PLACA")){
                    model.placaService.setPlaca(null);
                } else if (request.getParameter("opcion").equals("CAJAS_EFECTIVO")) {
                    model.tablaGenService.setTblgen(null);
                    model.tablaGenService.buscarDatos("CE_AGENCIA", usuario.getId_agencia());
                    model.tablaGenService.getTblgen();
                    if (model.tablaGenService.getTblgen() != null) {
                        next = "/jsp/cajasEfectivo/cajasEfectivo.jsp";
                    }
                }
                
                if( !marco.equals("no") ){
                    next = Util.LLamarVentana(next,request.getParameter("titulo"));
                }
            }
            
             if (next.equals("/jsp/fenalco/negocios/ver_negocios.jsp")) {
                String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "0";               
                next = "/controller?estado=Negocios&accion=Ver&op=5&vista="+ vista;                																		
            }
            if (next.equals("jsp/perfilesRiesgos/ListarPerfilesRiesgos.jsp")) {              
                next = "/controller?estado=Perfiles&accion=Riesgos";                
            }
            
            if (next.equals("jsp/perfilesRiesgos_Asignacion/ListarAnalistas.jsp")) {              
                next = "/controller?estado=AsigPerfiles&accion=Riesgos";                
            } 
            
            if (next.equals("jsp/roles/ListarRoles.jsp")) {              
                next = "/controller?estado=Roles&accion=Administrar";                
            }
             if (next.equals("jsp/reasignacion_negocio/ReasignarNegocios.jsp")) {              
                next = "/controller?estado=Negocios&accion=Reasignar";                
            }
            this.dispatchRequest(next);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
}
