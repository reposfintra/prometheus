/********************************************************************
 *      Nombre Clase.................   TraficoGConfAction.java
 *      Descripci�n..................   Action que gurada una configuracion creada  por los  usuarios
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class TraficoGConfAction extends Action {
    
    /** Creates a new instance of TraficoGConfAction */
    public TraficoGConfAction () {
    }
    
    public void run () throws ServletException, InformationException {
        ////System.out.println ("entro en run ");
        try{
            
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            
            ////System.out.println ("entro en try");
            String  []camposS   = request.getParameterValues ("camposS");
            String user        = request.getParameter ("usuario");
            String clas        =""+ request.getParameter ("clas");
            String filtro        = ""+request.getParameter ("filtro");
            String var1        = ""+request.getParameter ("var1");
            String var2     = ""+request.getParameter ("var2");
            String var3     = ""+request.getParameter ("var3");
            String conf     = ""+request.getParameter ("conf");
            ////System.out.println ("configuracion: "+ conf);
            String nombre     = ""+request.getParameter ("nombre");
            ////System.out.println ("nombre: "+ nombre);
            ////System.out.println ("Filtro: "+ filtro);
            String linea=   ""+request.getParameter ("linea");
            String nombreC=""+request.getParameter ("nombreC");
            String filtroP = "" + request.getParameter("filtroP");
            
            filtro=model.traficoService.decodificar (filtro);
            
            String configuracion ="";
            
            if(camposS != null){
                for(int i=0;i<=camposS.length-1;i++){
                    String campo   = camposS[i];
                    ////System.out.println ("Campo "+i+": "+campo);
                }
                configuracion = model.traficoService.getConfiguracion (camposS); 
                configuracion =configuracion+" ,color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias ";
                linea="";
                for(int i=0;i<=camposS.length-1;i++){
                    linea  = linea+"-"+camposS[i];
                }
                
                ////System.out.println ("linea: "+ linea);
                linea="-tipo_despacho-planilla-placa"+linea;
            }
            ////System.out.println ("LINEA   "+linea);
          
            String confG = configuracion+",demora,cedcon,tipo_despacho,planilla,placa,fecha_ult_reporte,fecha_prox_reporte,cedcon,fecha_salida,cel_cond,oid,nomcond,neintermedias ";
            ConfiguracionUsuario con= new ConfiguracionUsuario ();
            con.setNombre (nombreC);
            con.setConfiguracion (confG.replaceAll("-_-","'"));
            con.setLinea (linea);
            con.setVar1 (var1);
            con.setVar2 (var2);
            con.setCreation_user (user);
            con.setDstrct (usuario.getDstrct ());
            model.traficoService.insertarConfiguracion (con);
  
            filtro=model.traficoService.codificar (filtro);
            String next ="";
            
            next= "/jsp/trafico/controltrafico/configuracion3.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion.replaceAll("'","-_-")+"&clas="+clas+"&filtroP="+filtroP;
            
            ////System.out.println ("next "+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
    
}
