/*******************************************************************
 * Nombre clase: Ingreso_especialInsertAction.java
 * Descripción: Accion para ingresar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 08:08 AM
 * Versión: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Ingreso_especialInsertAction extends Action{
    
    /** Creates a new instance of Ingreso_especialInsertAction */
    public Ingreso_especialInsertAction () {
    }
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String dstrct = (String) session.getAttribute ("Distrito");
        String tipo = request.getParameter ("c_tipo_acuerdo").toUpperCase ();
        String codigo = request.getParameter ("c_codigo_concepto").toUpperCase ();
        String clase = request.getParameter ("c_clase_equipo").toUpperCase ();
        float porcentaje = Float.parseFloat (request.getParameter ("c_porcentaje"));
        String next = "/jsp/equipos/ingreso_especial/ingreso_especialInsertar.jsp";
        int sw=0;
        try{
            Ingreso_especial ingreso = new Ingreso_especial ();
            ingreso.setTipo_acuerdo (tipo);
            ingreso.setCodigo_concepto (codigo);
            ingreso.setClase_equipo (clase);
            ingreso.setPorcentaje_ingreso (porcentaje);
            ingreso.setUsuario_modificacion (usuario.getLogin ());
            ingreso.setUsuario_creacion (usuario.getLogin ());
            ingreso.setDistrito (dstrct);
            ingreso.setBase (usuario.getBase ().toUpperCase ());
            try{
                model.ingreso_especialService.insertIngreso_especial (ingreso);
                request.setAttribute ("mensaje","La información ha sido ingresada exitosamente!");
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.ingreso_especialService.existIngreso_especial (tipo, codigo, clase, dstrct)){
                    model.ingreso_especialService.updateIngreso_especial (ingreso);
                    request.setAttribute ("mensaje","La información ha sido ingresada exitosamente!");
                }
                else{
                    request.setAttribute ("mensaje","Error la información ya existe!");
                }
            }
            else{
                request.setAttribute ("mensaje","La información ha sido ingresada exitosamente!");
            }
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
