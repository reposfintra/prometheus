 /*
 * CaravanaBisquedaAction.java
 *
 * Created on 04 de Agosto de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class EscoltaBusquedaAction extends Action{
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/caravana/caravana.jsp?reload=&escolta=visible&agregar=visible";
        String next2 = "/jsp/trafico/caravana/asignarEscolta.jsp?reload=&escolta=visible&agregar=visible";
        String checkbox = "";        
        String pag = (request.getParameter("pag")!=null)?request.getParameter("pag"):"";
        try{ 
            Vector datos = model.escoltaVehiculoSVC.getVectorEscoltas();
            for (int i=0; i<datos.size(); i++){
                checkbox = (request.getParameter("fila"+i)!=null)?request.getParameter("fila"+i):"";
                //System.out.println("CHECKBOX "+i+":"+checkbox);
                if (!checkbox.equals("")){
                    Escolta es = (Escolta)datos.elementAt(Integer.parseInt(checkbox));                                            
                    if (!model.escoltaVehiculoSVC.existeEscoltaCaravanaActual(es)) {
                        if (pag.equals("mod"))
                            es.setSeleccionado(true);
                        model.escoltaVehiculoSVC.agregarVectorEscoltaCaravanaActual(es);
                    }
                }
            }
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        if (pag.equals("escolta"))        
            this.dispatchRequest(next2);
        else
            this.dispatchRequest(next);
    }
    
}
