/*
 * pvuIngresarAction.java
 *
 * Created on 19 de julio de 2005, 10:07
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DRIGO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;


/**
 *
 * @author  Jose
 */
public class pvuIngresarAction extends Action{
    
    /** Creates a new instance of pvuIngresarAction */
    public pvuIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String pag = "/jsp/trafico/permisos/perfil_vista_usuario/PvuInsertar.jsp";
        String next;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        String user = request.getParameter("c_usuario");
        String perfil = request.getParameter("c_perfil");
        ////System.out.println("perfil action: "+perfil);
        try{
            PerfilVistaUsuario pvu = new PerfilVistaUsuario();
            pvu.setUsuario(user);
            pvu.setPerfil(perfil);
            pvu.setCia(usuario.getCia().toUpperCase());
            pvu.setUser_update(usuario.getLogin().toUpperCase());
            pvu.setCreation_user(usuario.getLogin().toUpperCase());
            ////System.out.println("perfil action objeto pvu: "+pvu.getPerfil());
        
            model.perfilvistausService.insertPerfilVistaUsuario(pvu);
            
            pag += "?mensaje=Proceso exitoso!!!!";
            //request.setAttribute("mensaje","Proceso exitoso!!!!");            
            next = pag; 
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
