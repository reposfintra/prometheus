/***************************************
    * Nombre Clase ............. LiquidarOCAction.java
    * Descripci�n  .. . . . . .  Permite Liq. OC
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  20/12/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/

package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.http.*;

import com.tsp.util.*;

public class LiquidarOCAction extends Action{
    
    Usuario usuario = null;
    
    public void run() throws ServletException, InformationException {
        
        try{
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
             // indica el tipo de parametro recibido
            String tipo        = defaultString("tipo","");
            // planilla, placa o propietario
            String parametro   = defaultString("parametro","0");            
            String opcion      = defaultString("Opcion","");
            String base        = defaultString("base","");
            String next      = "/jsp/cxpagar/liquidacion/verLiquidacion.jsp"; 
            
            if (opcion.equals("Liquidar")){
                model.LiqOCSvc.buscarDatosALiquidar(parametro.toUpperCase(), Integer.parseInt(tipo),base);
                Vector datos = model.LiqOCSvc.getOcsALiquidar();
                if (datos!=null && !datos.isEmpty()){
                    model.LiqOCSvc.LiquidacionPorBloque(usuario);
                }else{
                    next = "/jsp/cxpagar/liquidacion/liquidacion.jsp?msg=no se encontraron datos para las restricciones definidas...";
                }
                datos = null;
            } else if (opcion.equals("VistaPrevia")){
                String  eventoProduccion  = request.getParameter("produccion");
                //System.out.println("PRODUCCION -- >>" +eventoProduccion );
                if( eventoProduccion==null  )
                    model.LiqOCSvc.buscarDatosALiquidar(parametro, Integer.parseInt(tipo), base);
                else{
                    String[]  planillas =  request.getParameterValues("planillaNO");
                    String    CADENAOC  = "";
                    if( planillas!= null  ){
                        for(int i=0;i<planillas.length;i++){
                            String noOC = planillas[i];
                            if(!CADENAOC.equals("")  )
                                CADENAOC +=",";
                            CADENAOC += "'"+ noOC +"'";
                        }
                        model.LiqOCSvc.buscarDatosALiquidarRep(CADENAOC, parametro); 
                    }
                }
                
                Vector datos = model.LiqOCSvc.getOcsALiquidar();
                
                
                if (datos!=null && !datos.isEmpty()){
                    next = "/jsp/cxpagar/liquidacion/vistaPreviasOcs.jsp";
                }else{
                    next = "/jsp/cxpagar/liquidacion/liquidacion.jsp?msg=no se encontraron datos para las restricciones definidas...";
                }  
                datos = null;
            }else if (opcion.equals("ConfirmacionOcs")){
                filtrar();
                Vector datos = model.LiqOCSvc.getOcsALiquidar();
                if (datos!=null && !datos.isEmpty()){
                    model.LiqOCSvc.LiquidacionPorBloque(usuario);
                }else{
                    next = "/jsp/cxpagar/liquidacion/liquidacion.jsp?msg=no se encontraron datos para las restricciones definidas...";
                }
                datos = null;
            }
            else if (opcion.equals("RetirarOc")){
                Vector liquidaciones = model.LiqOCSvc.getLiquidaciones();
                for (int i=0; i<liquidaciones.size();i++){
                    Liquidacion liq = (Liquidacion) liquidaciones.get(i);
                    if (liq.getOC().equals(parametro)){
                        liquidaciones.remove(i);
                        break;
                    }
                }
            }else if (opcion.equals("RetirarOcAll")){
                Vector liquidaciones = model.LiqOCSvc.getLiquidaciones();
                int i = 0;
                while( i<liquidaciones.size()){
                    Liquidacion liq = (Liquidacion) liquidaciones.get(i);
                    OP op  = (OP) liq.getFacturas().get(0); 
                    if (!(op.getComentario().trim().equals(""))){
                        liquidaciones.remove(i);
                    }else{
                        i++;
                    }
                }
            }       
            
            
            /*if ( model.LiqOCSvc.buscarOC(oc) ){
                model.LiqOCSvc.setOC(oc);
                model.LiqOCSvc.liquidarOC(oc);            
            }
            else {
                next = "/jsp/cxpagar/liquidacion/liquidacion.jsp?msg=Planilla No. " + oc + " no existe o se encuentra anulada...";
            }*/
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);         
            
        } catch (Exception e){
             e.printStackTrace();
             throw new ServletException(e.getMessage());
        }
    }
    
    
    
    public void filtrar() throws Exception{
        try{
            String [] idListOc = request.getParameterValues("idListOc");
            Vector ocsFiltradas = new Vector();
            Vector ocs          = model.LiqOCSvc.getOcsALiquidar();
            for (int i = 0; i< ocs.size() ; i++){
                Planilla p = (Planilla) ocs.get(i);
                for (int j = 0; j<idListOc.length; j++){
                    if (p.getNumpla().equals(idListOc[j])){
                       ocsFiltradas.add( p );
                       break;
                    }
                }
                    
            }
            model.LiqOCSvc.setOcsALiquidar(ocsFiltradas);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
       /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }
    
    
}
