/*************************************************************************
 * Nombre:        ReferenciaInsertAction.java         
 * Descripci�n:   Clase Action para modificar actividad    
 * Autor:         Ing. Henry Osorio 
 * Fecha:         26 de septiembre de 2005, 03:51 PM                           
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 *************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;

public class ReferenciaSearchAction extends Action{
    
    /** Creates a new instance of ReferenciaInsertAction */
    public ReferenciaSearchAction() {
    }
    public void run() throws ServletException {
        String next = "/jsp/hvida/referencias/referenciaUpdate.jsp?tipo=";       
        try{
            String doc = (request.getParameter("documento")!=null)?request.getParameter("documento"):"";
            String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
            model.referenciaService.buscarReferencias(doc);
            Vector vecRef = model.referenciaService.getVectorReferencias();
            ////System.out.println("Cantidad "+vecRef.size());
            model.tablaGenService.buscarRegistros("RELACION");
            model.ciudadService.searchTreMapCiudades();
            next = Util.LLamarVentana(next+tipo+"&documento="+doc, "Modificar Referencias");
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());                       
        }        
        this.dispatchRequest(next);
    }   
}