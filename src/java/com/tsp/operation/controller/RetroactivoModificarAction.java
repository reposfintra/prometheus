/*************************************************************************
 * Nombre:        RetroactivoModificarAction.java                        *
 * Descripci�n:   Clase Action para modificar retroactivo                *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales                 *
 * Fecha:         6 de febrero de 2006, 01:23 PM                         * 
 * Versi�n        1.0                                                    * 
 * Coyright:      Transportes Sanchez Polo S.A.                          * 
 *************************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class RetroactivoModificarAction extends Action {
    
    /** Creates a new instance of RetroactivoModificarAction */
    public RetroactivoModificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/masivo/retroactivo/retroactivoMod.jsp?mensaje=MsgModificado&reload=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        int sw=0;
        
        Retroactivo retact = new Retroactivo();
        retact.setFecha1(request.getParameter("fechai"));
        retact.setFecha2(request.getParameter("fechaf"));
        retact.setStd_job_no(request.getParameter("stdjob"));
        retact.setRuta(request.getParameter("ruta"));
        retact.setValor_retro(Double.parseDouble(request.getParameter("vlr_ret")));
        retact.setValor_stdjob(Double.parseDouble(request.getParameter("vlr_stbjob")));
        retact.setCreation_user(usuario.getLogin());
        retact.setUser_update(usuario.getLogin());
        retact.setBase(usuario.getBase());
        retact.setDstrct(usuario.getDstrct());
        retact.setEstado("");
        try{
            model.retroactivoService.modificarRetroactivo(retact);
            model.retroactivoService.buscarRetroactivo(request.getParameter("stdjob"), request.getParameter("ruta"), request.getParameter("fechai"), request.getParameter("fechaf"));
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
