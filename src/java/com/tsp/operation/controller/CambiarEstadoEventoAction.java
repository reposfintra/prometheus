/*******************************************************************
 * Nombre:        CambiarEstadoEventoAction.java
 * Descripci�n:   Clase Action para cambiar los estados de los registros de placa o nit
 * Autor:         Ing. Diogenito Antonio Bastidas Morales
 * Fecha:         16 de marzo de 2006, 10:03 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class CambiarEstadoEventoAction extends Action{
    
    /** Creates a new instance of CambiarEstadoEventoAction */
    public CambiarEstadoEventoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/hvida/cambiar_estado/";
        String tipo = request.getParameter("tipo");
        ////System.out.println("Placa "+request.getParameter("placa") );
        try{
            if (tipo.equals("Conductor")){
                model.conductorService.cambiarEstadoConductor(request.getParameter("identificacion"), request.getParameter("est"));
                model.conductorService.buscarConductorCGA(request.getParameter("identificacion"));
                next += "Conductor.jsp?men=ok";
            }
            else if (tipo.equals("Placa")){
                model.placaService.cambiarEstadoPlaca(request.getParameter("placa"), request.getParameter("est"));
                model.placaService.buscarPlacaCGA(request.getParameter("placa"));
                next += "Placa.jsp?men=ok";
                
            }else if (tipo.equals("Nit")){
                model.conductorService.cambiarEstadoNit(request.getParameter("identificacion"), request.getParameter("est"));
                model.conductorService.buscarNit(request.getParameter("identificacion"));
                next += "Nit.jsp?men=ok";
            }
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
