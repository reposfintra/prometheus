/*
 * Nombre        BuscarCiudadAction.java
 * Autor         Ing. Diogenes Bastidas
 * Fecha         1 de abril de 2005, 02:58 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*; 

/**
 *
 * @author  DIBASMO
 */
public class BuscarCiudadAction extends Action {
    
    /** Creates a new instance of BuscarCiudadAction */
    public BuscarCiudadAction() {
        
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina")+"?sw=1";
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute("Distrito");
       
        try{ 
            model.estadoservice.buscarestado(request.getParameter("codpais"), request.getParameter("codestado") );
            model.ciudadservice.buscarCiudad( request.getParameter("codpais"), request.getParameter("codestado"), request.getParameter("codigo") );             
            model.ciudadService.searchTreMapCiudades();
            model.paisservice.listarpaises();
            model.zonaService.listarZonasFronterisas();
            model.zonaService.listarZonasNoFronterisas(); 
            model.TimpuestoSvc.buscarRicaAgencia( distrito, request.getParameter("codigo"), Util.getFechaActual_String(1)+"-12-31" );
            
            TreeMap agencia = model.agenciaService.listar();
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
