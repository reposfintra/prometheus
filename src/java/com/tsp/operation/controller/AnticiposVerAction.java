/*
 * AnticiposVerAction.java
 *
 * Created on 28 de mayo de 2008, 19:01
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

/**
  * @author  Fintra
 */
public class AnticiposVerAction extends Action{
    
    /** Creates a new instance of AnticiposVerAction */
    public AnticiposVerAction() {
    }
  
       
    public void run() throws ServletException, InformationException
    { 
       //System.out.println("run xls anticips");
       HttpSession session  = request.getSession();
       
       Usuario usuario = (Usuario)session.getAttribute("Usuario");
       String op=request.getParameter ("op"); 
       String ckest=null,est="",ckdate=null,fi="",ff="",cknita=null,nita="",cknoma=null,noma="",cknitc=null,nitc="",cknomc=null,nomc="";
       String ckfap=null,fi1="",ff1="",ckfde=null,fi2="",ff2="";
       String next="/jsp/fenalco/negocios/ver_negocios.jsp";
       ArrayList fp=new ArrayList();
       if(op.equals("3"))
       {
                //System.out.println("op3");
                fp=(ArrayList)session.getAttribute("vant");
                ListadoAnticiposTh hilo = new ListadoAnticiposTh();  
                hilo.start(model,fp, usuario);  
                next = "/jsp/fenalco/negocios/analisisCarteraMsg.jsp";
       }
       
       this.dispatchRequest(next);
    }
    
}
