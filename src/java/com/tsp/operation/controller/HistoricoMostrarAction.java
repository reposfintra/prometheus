/*
 * HistoricoMostrarAction.java
 *
 * Created on 5 de octubre de 2005, 10:21 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class HistoricoMostrarAction extends Action{
    
    /** Creates a new instance of HistoricoMostrarAction */
    public HistoricoMostrarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/adminHistorico/ahistorico.jsp?";
        this.dispatchRequest(Util.LLamarVentana(next, "Cronograma de Historicos"));
    }
    
}
