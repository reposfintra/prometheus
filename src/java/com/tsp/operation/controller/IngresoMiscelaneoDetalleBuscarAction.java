/*
 * IngresoMiscelaneoDetalleBuscarAction.java
 *
 * Created on 2 de diciembre de 2004, 09:42 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;



public class IngresoMiscelaneoDetalleBuscarAction  extends Action{
    
    
    /** Creates a new instance of AcpmSearchAction */
    public IngresoMiscelaneoDetalleBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/jsp/cxcobrar/ingresoMiscelaneo/";
        String evento = request.getParameter("evento");
        String campo = request.getParameter("campo");
        int modulo = Integer.parseInt(request.getParameter("campo"));
        String cuenta =  (request.getParameter("cuenta") != null) ?request.getParameter("cuenta").toUpperCase():"";
        String mon_local = "";
        boolean sw = false;
        try{
            ////System.out.println("Evento "+evento);
            if(evento.equals("Cuenta")){
                
                if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),cuenta)){
                    modelcontab.subledgerService.buscarCuentasTipoSubledger(cuenta);
                    next+="auxiliar.jsp?opcion=Cuenta&campo="+campo+"&tipo="+request.getParameter("tipo");
                }
                else{
                    modelcontab.subledgerService.setCuentastsubledger(null);
                    next+="auxiliar.jsp?opcion=Cuenta&campo="+campo+"&mensaje=No existe la Cuenta";
                }
                
            }else if(evento.equals("Documento")){
                String Documento = request.getParameter("documento").toUpperCase();
                String tipo = request.getParameter("tipo");
                boolean resultado = false;
                ////System.out.println("Tipo "+tipo +" Doc "+Documento);
                if(tipo.equals("001")){
                    resultado = model.planillaService.existPlanilla(Documento);
                }
                else{
                    model.remesaService.buscaRemesa(Documento);
                    if(model.remesaService.getRemesa() != null){
                        resultado = true;
                    }
                }
                if(!resultado){
                    next+="auxiliar.jsp?opcion=Documento&campo="+campo+"&mensaje=El nro del documento no existe";
                }
                
            }
            else if(evento.equals("Auxiliar")){
                String tip = request.getParameter("tipo");
                String idsubledger = request.getParameter("auxiliar");
                modelcontab.subledgerService.setCuentastsubledger(null);
                if(!modelcontab.subledgerService.existeSubledger(usuario.getDstrct(), cuenta, tip, idsubledger)){
                    next+="auxiliar.jsp?opcion=Auxiliar&mensaje=El Auxiliar no existe";
                }
            }
            
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
