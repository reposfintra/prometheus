/********************************************************************
 *      Nombre Clase.................   FacturaImpuestoAction.java
 *      Descripci�n..................   Action que se encarga de buscar un tipo de impuesto
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  David A
 */
public class FacturaImpuestoAction extends Action
{
        
        /** Creates a new instance of FacturaImpuestoAction */
        public FacturaImpuestoAction ()
        {
        }
        
        public void run () throws ServletException, InformationException
        {
                try
                {
                        
                        String codigo=""+request.getParameter ("codigo");
                        String tipo=""+request.getParameter ("tipo");
                        String fecha=""+request.getParameter ("fecha");
                        String idImpuesto= ""+ request.getParameter ("idImpuesto");
                        //System.out.println("codigo "+codigo);
                        //System.out.println("tipo "+tipo);
                        //System.out.println("fecha "+fecha);
                        //System.out.println("Id Impuesto "+idImpuesto);
                        int pos1 = idImpuesto.indexOf ("_");
                        //System.out.println("pos inicial "+pos1);
                        String codigo2 = idImpuesto.substring (pos1+1,idImpuesto.length ());
                        //System.out.println("codigo2 "+codigo2);
                        pos1 = codigo2.indexOf ("_");
                        String codigo3 = codigo2.substring (0,pos1);
                        int n1=Integer.parseInt (codigo3);
                        //System.out.println("numero1 "+codigo3);
                        codigo3=codigo2.substring (pos1+1,codigo2.length ());
                        //System.out.println("numero2 "+codigo3);
                        int n2=Integer.parseInt (codigo3);
                        String tipo_documento =""+ request.getParameter ("tipo_documento");
                        String documento =""+ request.getParameter ("documento");
                        String proveedor =""+ request.getParameter ("proveedor1");
                        //System.out.println("PROVEEDOR "+proveedor);
                        String tipo_documento_rel =""+ request.getParameter ("tipo_documento_rel");
                        String documento_relacionado =""+ request.getParameter ("documento_relacionado");
                        String fecha_documento=""+request.getParameter ("fecha_documento");
                        String banco =""+ request.getParameter ("c_banco");
                        double vlr_neto=0;
                        int plazo=0;
                        int num_items=0;
                        try
                        {
                                num_items  = Integer.parseInt (""+ request.getParameter ("num_items"));
                                //System.out.println("plazo"+request.getParameter ("plazo"));
                                vlr_neto  = Double.parseDouble (""+ request.getParameter ("vlr_neto"));
                                plazo = Integer.parseInt (""+ request.getParameter ("plazo"));
                        }
                        catch(java.lang.NumberFormatException e)
                        {
                                vlr_neto=0;
                                plazo=0;
                                num_items=1;
                        }
                        
                        Vector vItems = new Vector ();
                        //System.out.println("Numero de Items " + num_items);
                        for (int i=1;i <= num_items; i++)
                        {
                                CXPItemDoc item = new CXPItemDoc ();
                                String descripcion_i=""+ request.getParameter ("descripcion_i"+i);
                                String codigo_cuenta=""+ request.getParameter ("codigo_cuenta"+i);
                                String codigo_abc=""+ request.getParameter ("codigo_abc"+i);
                                String planilla=""+ request.getParameter ("planilla"+i);
                                double  valor =0;
                                try
                                {
                                        //System.out.println("VALORRRRRRRRRRRRRR"+request.getParameter ("valor1"+i));
                                        valor=Double.parseDouble (""+ request.getParameter ("valor1"+i));
                                }
                                catch(java.lang.NumberFormatException e)
                                {
                                        valor=0;
                                }
                                //System.out.println("descripcion_i "+descripcion_i);
                                //System.out.println("codigo_cuenta "+codigo_cuenta);
                                //System.out.println("codigo_abc "+codigo_abc);
                                //System.out.println("planilla"+planilla);
                                //System.out.println("valor "+ valor);
                                
                                item.setDescripcion (descripcion_i);
                                item.setCodigo_cuenta (codigo_cuenta);
                                item.setCodigo_abc (codigo_abc);
                                item.setPlanilla (planilla);
                                item.setVlr_me (valor);
                                
                                
                                Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos ();
                                Vector vImpuestosPorItem= new Vector ();
                                for(int x=0;x<vTipoImp.size ();x++)
                                {
                                        CXPImpItem impuestoItem = new CXPImpItem ();
                                        String cod_impuesto = ""+ request.getParameter ("impuesto"+x+""+i);
                                        if (i== n2 && x==n1)
                                        {
                                                impuestoItem.setCod_impuesto (codigo);
                                        }
                                        else
                                        {
                                                impuestoItem.setCod_impuesto (cod_impuesto);
                                        }
                                        //System.out.println("impuesto"+cod_impuesto);
                                        vImpuestosPorItem.add (impuestoItem);
                                }
                                item.setVItems ( vImpuestosPorItem);
                                vItems.add (item);
                        }
                        
                        model.cxpItemDocService.setVecCxpItemsDoc (vItems);
                        
                        String sucursal =""+ request.getParameter ("c_sucursal");
                        String moneda=""+ request.getParameter ("moneda");
                        String descripcion =""+ request.getParameter ("descripcion");
                        String observacion =""+ request.getParameter ("observacion");
                        String usuario_aprobacion=""+request.getParameter ("usuario_aprobacion");
                        
                        
                        Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit (proveedor);
                        int b =0;
                        if(o_proveedor != null)
                        {
                                //System.out.println(" Id Mins "+o_proveedor.getC_idMims ());
                                //System.out.println(" Beneficiario "+o_proveedor.getC_branch_code ());
                                //System.out.println(" Sucursal "+o_proveedor.getC_bank_account ());
                                //System.out.println(" Id Mins "+o_proveedor.getC_payment_name ());
                                b=1;
                        }
                        
                        if (b!=1)
                        {
                                proveedor="";
                        }
                        
                        CXP_Doc factura = new CXP_Doc ();
                        factura.setTipo_documento (tipo_documento);
                        factura.setDocumento (documento);
                        factura.setProveedor (proveedor);
                        factura.setTipo_documento_rel (tipo_documento_rel);
                        factura.setDocumento_relacionado (documento_relacionado);
                        factura.setFecha_documento (fecha_documento);
                        factura.setBanco (banco);
                        factura.setVlr_neto (vlr_neto);
                        factura.setMoneda (moneda);
                        factura.setSucursal (sucursal);
                        factura.setDescripcion (descripcion);
                        factura.setObservacion (observacion);
                        factura.setUsuario_aprobacion (usuario_aprobacion);
                        factura.setPlazo (plazo);
                        model.cxpDocService.setFactura (factura);
                        
                        //System.out.println("seteo la factura");
                        String next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp";
                        //System.out.println("next en Filtro"+next);
                        
                        //this.dispatchRequest(Util.LLamarVentana(next, "Documentos por pagar 2"));
                        
                        RequestDispatcher rd = application.getRequestDispatcher (next);
                        if(rd == null)
                                throw new Exception ("No se pudo encontrar "+ next);
                        rd.forward (request, response);
                        
                        
                }
                catch(Exception e)
                {
                        throw new ServletException ("Accion:"+ e.getMessage ());
                }
                
        }
        
}
