/*
 * CXPTransferirRecurrentes.java
 *
 * Created on 7 de julio de 2006, 10:19 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Osvaldo
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HTransferirRecurrentes;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.DAOS.RXPDocDAO;



public class CXPTransferirRecurrentesAction extends Action{
    
    /** Creates a new instance of CXPTransferirRecurrentes */
    public CXPTransferirRecurrentesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/cxpagar/transferencias/recurrente_a_factura.jsp";
        HttpSession session = request.getSession();
        Usuario u = (Usuario)session.getAttribute("Usuario");
        String mensaje = "";
        try{
            
            RXPDocDAO r = new RXPDocDAO();
            HTransferirRecurrentes t = new HTransferirRecurrentes();
            
            String filtro = request.getParameter("filtro");
            
            if( filtro.equals("ALL") ){
                
                t.start(u.getLogin(), u.getBase(), "");
                
            }else{
                
                String nit = request.getParameter("nit");
                t.start(u.getLogin(), u.getBase(), nit);
                
            }
            mensaje = "El proceso de Transferencia ha iniciado con �xito!";
            
            request.setAttribute("mensaje", mensaje );
        }catch(Exception ex){
            ex.printStackTrace();
            throw new ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
