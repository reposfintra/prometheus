/*
 * Codigo_discrepanciaSerchAction.java
 *
 * Created on 28 de junio de 2005, 01:54 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Tipo_RecuperacionASerchAction extends Action{
    
    /** Creates a new instance of Codigo_discrepanciaSerchAction */
    public Tipo_RecuperacionASerchAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="";              
        String listar = (String) request.getParameter("listar");
        String codigo = (String) request.getParameter("c_codigo");
        String descripcion = (String) request.getParameter("c_descripcion"); 
        try{                
            if (listar.equals("True")){
                next="/jsp/masivo/tipo_recuperacion/tipo_recuperacionListar.jsp";                    
                model.trecuperacionaService.consultar(codigo,descripcion);
                ////System.out.println("Tamano vector "+model.trecuperacionaService.getTipo_Recuperacion_Anticipo());
                if ( model.trecuperacionaService.getTipo_Recuperacion_Anticipo().size() == 0){
                    next="/jsp/trafico/mensaje/ErrorBusq.jsp?ruta=/jsp/masivo/tipo_recuperacion/tipo_recuperacionBuscar.jsp";              
                }
                
            }
            else{
                model.trecuperacionaService.search(codigo);
                next="/jsp/masivo/tipo_recuperacion/tipo_recuperacionModificar.jsp";
         //       model.trecuperacionaService.getTipo_Recuperacion()
            }            
         }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
