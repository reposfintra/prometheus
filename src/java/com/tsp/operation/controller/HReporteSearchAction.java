/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class HReporteSearchAction extends Action {
  static Logger logger = Logger.getLogger(HReporteSearchAction.class);
  
  public void run() throws ServletException {
    String cmd = request.getParameter("cmd");
    String next = null;
    Usuario user = null;
    String creation_user = null;
    String print_user = null;
 if(cmd == null){
       next = "/hojareporte/PantallaHojaRutaGetInfo.jsp";
    }
    else{
      HttpSession session = request.getSession();
      
      String [] args = new String[2];
      args[0] = request.getParameter("cia");
      args[1] = request.getParameter("planilla");
      
      if (args[1] != null)
        args[1] = args[1].trim().toUpperCase();
      else
        args[1] = "";
      
      try {
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        ReporteConductor rConductor = (ReporteConductor)request.getAttribute("hReporte");
        if (model.hReporteService.hReporteExist(args[0], args[1])){
              model.hReporteService.getHrInfoOld(args[0], args[1]);
              rConductor =  model.hReporteService.getRConductor();
              model.usuarioService.buscaUsuario(rConductor.getCreation_user());
              user = model.usuarioService.getUsuario();
              if (user != null)
                creation_user = user.getNombre();
              else
                creation_user = "";
              rConductor.setCreation_user(creation_user);
              model.usuarioService.buscaUsuario(rConductor.getPrint_user());
              user = model.usuarioService.getUsuario();
              if (user != null)
                print_user = user.getNombre();
              else
                print_user = "";
              rConductor.setPrint_user(print_user);
              model.viaService.getViasRelPg(rConductor.getRuta());
              if (model.viaService.getCbxVias().size() == 0){
                    logger.info("[ER0001] LA VIA '"+rConductor.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA EN POSGRES");
                    throw new InformationException("[ER0001] LA VIA '"+rConductor.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA EN POSGRES");
              }
              next = "/hojareporte/PantallaHojaRutaUpdate.jsp";
        }
        else{
          if (usuario.getAccesoplanviaje().equals("1")){
                if (model.planViajeService.planillaExist(args[0], args[1])){
                   model.hReporteService.getHrInfoNew(args[0], args[1]);
                   rConductor =  model.hReporteService.getRConductor();
                   model.viaService.getViasRelPg(rConductor.getRuta());
                   //VERIICA QUE LA VIA EXISTA EN CASO CONTRARIO DISPARA UN EXCEPCION
                   if (model.viaService.getCbxVias().size() == 0){
                      logger.info("[ER0000] LA VIA '"+rConductor.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA");
                      throw new InformationException("[ER0000] LA VIA '"+rConductor.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA");
                   }
                   next = "/hojareporte/PantallaHojaRutaCreate.jsp";
                }
                else{
                     request.setAttribute("mensaje", "� LA PLANILLA DIGITADA NO HA SIDO CREADA !");
                     next = "/hojareporte/PantallaHojaRutaGetInfo.jsp";
                }
          }
          else{
               request.setAttribute("mensaje", "� LA HOJA DE RUTA DIGITADA NO HA SIDO CREADA Y UD NO TIENE PERMISO PARA CREARLA !");
               next = "/hojareporte/PantallaHojaRutaGetInfo.jsp";
          }
          
        }
        request.setAttribute("hReporte", rConductor);
        logger.info( usuario.getNombre() + " Consult� Hoja de Reporte con: Planilla = " + args[1]);
      }
      catch (SQLException e){
        throw new ServletException(e.getMessage());
      }
      catch (InformationException e){
        next = "/hojareporte/PantallaHojaRutaGetInfo.jsp?mensaje="+e.getMessage();
      }
      
    }
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }
}
