/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import java.util.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import jxl.format.UnderlineStyle;






/**
 *
 * @author Alvaro
 */
public class FinanzasReportesAction extends Action {


    public FinanzasReportesAction() {
    }


    public void run() throws javax.servlet.ServletException {

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();

        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        String aceptarDisable = "";

        try {

            //  EVENTO  :  ESTADO_FINANCIERO_1 de estadosFinancieros1.jsp
            //  Genera un reporte en excell de estados financieros
            if (evento.equalsIgnoreCase("ESTADO_FINANCIERO_1")) {


                String mes =  request.getParameter("mes");
                String anio=  request.getParameter("anio");
                String formato = request.getParameter("formato");
                double secuencia1=Double.parseDouble((request.getParameter("secuencia_inicio")!=null?request.getParameter("secuencia_inicio"):"0"));
                double secuencia2=Double.parseDouble((request.getParameter("secuencia_final")!=null?request.getParameter("secuencia_final"):"0"));


                HEstadosFinancieros1 hilo =  new HEstadosFinancieros1();
                hilo.start(model, usuario,distrito,anio,mes,formato,secuencia1,secuencia2) ;

                aceptarDisable = "S";
                msj   = "La creacion del reporte de estados financieros 1 se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/finanzas/reportes/estadosFinancieros1.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de ESTADO_FINANCIERO_1


            //  EVENTO  :  ESTADO_FINANCIERO_2 de estadosFinancieros2.jsp
            //  Genera un reporte en excell de estados financieros
            if (evento.equalsIgnoreCase("ESTADO_FINANCIERO_2")) {


                String mes =  request.getParameter("mes");
                String anio=  request.getParameter("anio");


                HEstadosFinancieros2 hilo =  new HEstadosFinancieros2();
                hilo.start(model, usuario,distrito,anio,mes) ;

                aceptarDisable = "S";
                msj   = "La creacion del reporte de estados financieros 2 se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/finanzas/reportes/estadosFinancieros1.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de ESTADO_FINANCIERO_1






            next += msj ;
            this.dispatchRequest(next);

        }catch (Exception e) {
            e.printStackTrace();
        }

    }
}





