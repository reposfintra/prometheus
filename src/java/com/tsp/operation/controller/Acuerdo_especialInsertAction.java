/*******************************************************************
 * Nombre clase: Acuerdo_especialInsertAction.java
 * Descripción: Accion para ingresar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 6 de diciembre de 2005, 02:30 PM
 * Versión: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Acuerdo_especialInsertAction extends Action{
    
    /** Creates a new instance of Acuerdo_especialInsertAction */
    public Acuerdo_especialInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/equipos/acuerdo_especial/";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        String standar = request.getParameter("c_standar").toUpperCase();
        String tipo = request.getParameter("c_tipo").toUpperCase();//tipo de acuerdo
        String tip = request.getParameter("tipo");
        String codigo = request.getParameter("c_codigo").toUpperCase();
        next+= tip.equals("S")?"acuerdo_especialInsertar.jsp":"acuerdo_especialInsertar2.jsp";
        float porcentaje_descuento = Float.parseFloat(request.getParameter("c_porcentaje"));
        int sw=0;
        boolean estado = true;
        try{
            Acuerdo_especial acuerdo = new Acuerdo_especial();
            acuerdo.setTipo(tip);
            acuerdo.setStandar(standar);
            acuerdo.setTipo_acuerdo(tipo);
            acuerdo.setCodigo_concepto(codigo);
            acuerdo.setPorcentaje_descuento(porcentaje_descuento);
            acuerdo.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            acuerdo.setUsuario_creacion(usuario.getLogin().toUpperCase());
            acuerdo.setDistrito(dstrct);
            acuerdo.setBase(usuario.getBase().toUpperCase());
            if(tip.equals("P")){
                if(!model.placaService.existePlaca(standar)){
                    estado =  false;
                    request.setAttribute("mensaje","Error la placa no existe!");
                }
            }
            if(estado){
                try{
                    model.acuerdo_especialService.insertAcuerdo_especial(acuerdo);
                }catch(SQLException e){
                    sw=1;
                }
                if(sw==1){
                    if(!model.acuerdo_especialService.existAcuerdo_especial(standar, dstrct, codigo,tip)){
                        model.acuerdo_especialService.updateAcuerdo_especial(acuerdo);
                        request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                    }
                    else{
                        request.setAttribute("mensaje","Error la información ya existe!");
                    }
                }
                else{
                    request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                }
            }
            request.setAttribute("tipo",tipo);
            request.setAttribute("cod",codigo);
            TreeMap t = new TreeMap();
            model.stdjobdetselService.setCiudadesDest(t);
            model.stdjobdetselService.setCiudadesOri(t);
            model.stdjobdetselService.setStdjobTree(t);
        }catch(SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
