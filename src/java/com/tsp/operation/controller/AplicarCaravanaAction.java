/*
 * InsertRepMovTraficoAction.java
 *
 * Created on 14 de septiembre de 2005, 08:31 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;
/**
 *
 * @author Karen Reales
 */
public class AplicarCaravanaAction extends Action{
    
    static Logger logger = Logger.getLogger(AplicarCaravanaAction.class);
    
    /** Creates a new instance of InsertRepMovTraficoAction */
    public AplicarCaravanaAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/jsp/trafico/Movimiento_trafico/movtrafing.jsp" ;
        String numpla = request.getParameter("numplanilla");
        String cmd = request.getParameter("cmd");
        try{
            if(cmd.equals("caravana")){
                Vector caravana = new Vector();
                java.util.Enumeration enum1;
                String parametro;
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    //System.out.println(parametro);
                    
                    if(model.rmtService.estaPlanilla(parametro)){
                        //System.out.println("Planilla agregada "+parametro);
                        caravana.add(parametro);
                    }
                    
                }
                model.rmtService.setCaravana(caravana);
            }
            else if(cmd.equals("destinos")){
                DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
                if(dp!=null){
                    int sw=0;
                    dp.setClientes("");
                    java.util.Enumeration enum1;
                    String parametro;
                    enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                    String texto = "Se realizo entrega parcial para: ";
                    Vector cliente = model.rmtService.getClientes();
                    Vector destinos = new Vector();
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("check")>=0){
                            
                            logger.info( "Valor parametro "+parametro+" : "+request.getParameter(parametro));
                            logger.info( "Nombre : "+"nombre"+parametro.substring(5));
                            logger.info( "Cargo : "+request.getParameter("cargo"+parametro.substring(5)));
                            
                            Remesa r =(Remesa)cliente.elementAt(Integer.parseInt(parametro.substring(5)));
                            logger.info( "Cliente : "+r.getCodcli());
                            r.setNdest(request.getParameter("nombre"+parametro.substring(5)));//NOMBRE PERSONA
                            r.setDescripcion(request.getParameter("cargo"+parametro.substring(5)));//CARGO PERSONA
                            destinos.add(r);
                            logger.info( "Se agrego el destinatario");
                        }
                    }
                    dp.setDestinatarios(destinos);
                    logger.info("Cantidad destinatarios "+dp.getDestinatarios().size());
                    
                }
                model.rmtService.setDatosPlanillaRMT(dp);
                next="/jsp/trafico/Movimiento_trafico/RepEntregaParcial.jsp?cerrar=ok" ;
            }
            else if(cmd.equals("planillas")){
                next="/jsp/trafico/Movimiento_trafico/RepPlanillas.jsp?cerrar=ok" ;
                java.util.Enumeration enum1;
                String parametro;
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                String causa="";
                Vector plas = new Vector();
                
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    DatosPlanillaRMT dp = model.rmtService.obtenerPlanilla(parametro);
                    if(dp!=null){
                        logger.info( "Planilla "+parametro);
                        causa =causa+"&causa"+parametro+"=ok";
                        String fechareporte=request.getParameter("fechar"+parametro);
                        String causas = request.getParameter("causas"+parametro)!=null?request.getParameter("causas"+parametro):"";
                        String texto = parametro+","+causas+","+fechareporte;
                        logger.info( "Fecha reporte "+fechareporte);
                        logger.info("SE AGREGO LA SIGUIENTE..."+texto);
                        plas.add(texto);
                        
                        
                        if(request.getParameter("causas"+parametro)==null){
                            
                            logger.info( "NO SE SELECCIONO CAUSA...");
                            //VALIDAMOS SI LA FECHA DEL REPORTE A INGRESAR ES MAYOR O MENOR UNA HORA AL
                            //REPORTE QUE DEBERIA GENERARSE.
                            String fecha_antRep = dp.getFecha_prox_rep();
                            
                            logger.info( "FECHA_ANTERIOR REPORTE: "+fecha_antRep);
                            
                            int year=Integer.parseInt(fecha_antRep.substring(0,4));
                            int month=Integer.parseInt(fecha_antRep.substring(5,7))-1;
                            int date= Integer.parseInt(fecha_antRep.substring(8,10));
                            int hora=Integer.parseInt(fecha_antRep.substring(11,13));
                            int minuto=Integer.parseInt(fecha_antRep.substring(14,16));
                            
                            Calendar fecha_antRepC = Calendar.getInstance();
                            fecha_antRepC.set(year,month,date,hora,minuto,0);
                            fecha_antRepC.add(fecha_antRepC.HOUR, -1);
                            
                            Date mostrar = fecha_antRepC.getTime();
                            SimpleDateFormat timestam = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String  mostrarF=timestam.format(mostrar);
                            logger.info( "FECHA_ANTERIOR REPORTE MENOS UNA HORA: "+mostrarF);
                            
                            year=Integer.parseInt(fechareporte.substring(0,4));
                            month=Integer.parseInt(fechareporte.substring(5,7))-1;
                            date= Integer.parseInt(fechareporte.substring(8,10));
                            hora=Integer.parseInt(fechareporte.substring(11,13));
                            minuto=Integer.parseInt(fechareporte.substring(14,16));
                            
                            Calendar fecha_rep= Calendar.getInstance();
                            fecha_rep.set(year,month,date,hora,minuto,0);
                            logger.info( "FECHA REPORTE INGRESADO: "+fechareporte);
                            
                            if(fecha_rep.before(fecha_antRepC)){
                                next="/jsp/trafico/Movimiento_trafico/RepPlanillas.jsp?ok=ok"+causa;
                                model.rmtService.searchListaCausas("REP_TRA");
                                logger.info("LA FECHA ES MENOR UNA HORA");
                            }
                            else{
                                fecha_antRepC.add(fecha_antRepC.HOUR, +2);
                                mostrar = fecha_antRepC.getTime();
                                mostrarF=timestam.format(mostrar);
                                logger.info( "FECHA_ANTERIOR REPORTE MAS UNA HORA: "+mostrarF);
                                
                                if(fecha_rep.after(fecha_antRepC)){
                                    logger.info("LA FECHA ES MAYOR UNA HORA");
                                    next="/jsp/trafico/Movimiento_trafico/RepPlanillas.jsp?ok=ok"+causa;
                                    model.rmtService.searchListaCausas("REP_TRA");
                                }
                            }
                        }
                    }
                }
                model.rmtService.setPlanillasReportes(plas);
                
            }
            
            
        }catch(SQLException ex){
            throw new ServletException(ex.getMessage());
            
        }
        this.dispatchRequest(next);
    }
}
