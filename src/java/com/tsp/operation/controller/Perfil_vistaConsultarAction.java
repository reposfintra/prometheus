/********************************************************************
 *      Nombre Clase.................   PerfilVistaConsultarAction.java
 *      Descripci�n..................   Lista las relaciones entre los perfiles y las vistas.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   25.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;


public class Perfil_vistaConsultarAction extends Action{
    
    /** Creates a new instance of ActividadSerchAction */
    public Perfil_vistaConsultarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String pag = "/jsp/trafico/permisos/perfil_vista/perfil_vistaListar.jsp";
        String next;
        HttpSession session = request.getSession();
        String perfil = (String) request.getParameter("c_perfil");
        String pagina = (String) request.getParameter("c_pagina");
        String descripcion = (String) request.getParameter("c_descripcion");
        ////System.out.println(perfil);
        
        session.setAttribute("vista", pagina);
        session.setAttribute("perfil", perfil);
        
        next = pag;
        
        this.dispatchRequest(next);
    }
}
