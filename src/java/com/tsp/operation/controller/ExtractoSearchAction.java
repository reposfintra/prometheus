/***********************************************************************************
 * Nombre clase : ............... ExtractoSearchAction.java                        *
 * Descripcion :................. Clase invocada por el controlador para elaborar  *
 *                                extractos.                                       *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 01 de enero de 2006, 06:30 PM                    *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import java.util.*;

/**
 * Acci�n invocada por el controlador para elaborar extractos.
 * @author  Henry
 */
public class ExtractoSearchAction extends Action {
    static Logger logger = Logger.getLogger(ExtractoSearchAction.class);
    
    public void run() throws ServletException, InformationException {
        String next       = null;
        String opcion     = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
        String aprobadas  = (request.getParameter("aprobadas")!=null)?request.getParameter("aprobadas"):"";
        String banco      = (request.getParameter("banco")!=null)?request.getParameter("banco"):"";
        String tipo       = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
        String cuenta     = (request.getParameter("cuenta")!=null)?request.getParameter("cuenta"):"";
        String dstrct     = request.getParameter("distrito")!=null ?request.getParameter("distrito").toUpperCase():"";
        String corrida    = (request.getParameter("corrida")!=null)?request.getParameter("corrida"):"";
        String pagadas    = request.getParameter("pagadas")!=null ?request.getParameter("pagadas").toUpperCase():"";
        model.extractoService.setMensaje(null);        
        try{
            if(opcion.equals("BuscarS")) {
                model.servicioBanco.obtenerSucursalesBanco(banco);
                next = "/jsp/cxpagar/corridas/PantallaFiltroExtracto.jsp";
            } else if(opcion.equals("propietario")) {
                int totalBene = model.extractoService.getBeneficiarios().size();
                //System.out.println("TAMANO VECTOR :"+totalBene);
                String nit = "";
                for (int i =0; i<totalBene; i++) {
                    if (request.getParameter("nit"+i)!=null)
                        nit+= request.getParameter("nit"+i)+"-";
                    System.out.println("NIT NIT:"+nit);
                }
                model.extractoService.setPropietarios(nit);
                next = "/jsp/cxpagar/corridas/beneficiarios.jsp?cerrar=ok";
            } else if(opcion.equals("srhProveedor")) {
                model.extractoService.searchBeneficiariosCorrida(dstrct, banco, cuenta, corrida, aprobadas, pagadas);                
                next = "/jsp/cxpagar/corridas/beneficiarios.jsp";
            } else{
            
                next = "/jsp/cxpagar/corridas/PantallaFiltroExtracto.jsp";
                //request.setAttribute ("bodyPage", "jsp/cxpagar/corridas/PantallaFiltroExtracto.jsp");
                //request.setAttribute ("subTitle", "EXTRACTOS");
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                
                if(!corrida.equals("")) {                    
                    model.extractoService.extracto(dstrct, banco, cuenta, corrida, aprobadas, pagadas, tipo);                   
                    if (model.extractoService.getMensaje()!=null)
                        next = "/jsp/cxpagar/corridas/PantallaFiltroExtracto.jsp?msg="+model.extractoService.getMensaje();
                    else
                        next = "/jsp/cxpagar/corridas/ReporteExtracto.jsp?reload=yes";                    
                }else{
                    model.extractoService.listaCorridaBanco(dstrct, banco, cuenta, aprobadas, pagadas);
                    List lista = model.extractoService.getCorridaBanco();
                    
                    if(lista == null || lista.size()==0){
                        next = "/jsp/cxpagar/corridas/PantallaFiltroExtracto.jsp?msg=No se encontraron datos para los filtros especificados";        
                    } else{
                        model.extractoService.setTipo(tipo);
                        model.extractoService.setAprobadas(aprobadas);
                        model.extractoService.setPagadas(pagadas);
                        model.extractoService.setDistrito(dstrct);                        
                        next = "/jsp/cxpagar/corridas/ReporteBancoCorrida.jsp";
                    }
                   
                }
                
                
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
