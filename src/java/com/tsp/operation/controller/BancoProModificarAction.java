/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class BancoProModificarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public BancoProModificarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String Distrito = request.getParameter ("Distrito")!=null?request.getParameter ("Distrito"):"";
        String Propietario = (request.getParameter("Propietario")!=null)?request.getParameter ("Propietario"):"";
        String HC = (request.getParameter("HC")!=null)?request.getParameter ("HC"):"";
        String Banco = (request.getParameter("Banco")!=null)?request.getParameter ("Banco"):"";
        String Sucursal = (request.getParameter("Sucursal")!=null)?request.getParameter ("Sucursal"):"";
        Vector datos = new Vector();
         
        String next = "/jsp/sot/body/propietario/Modificar_B_Propietario.jsp";
        //System.out.println("comprobante trasmodificar  "+comprobante);
        HttpSession session = request.getSession ();
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        System.out.println("entro a la opciones modificar");
           /********************************************************************/
         try{
              if ( opcion.equals("1") ){
                    model.banco_PropietarioService.BusquedaDatosBancoPro(Propietario, HC, Distrito);
                    datos = model.banco_PropietarioService.getVector();
                    Banco_Propietario BancoPRO= (Banco_Propietario) datos.elementAt(0);    
                    String agencia = "%";
                    model.servicioBanco.loadSucursalesAgencia(agencia, BancoPRO.getBanco(), Distrito);
                          
              }
               if ( opcion.equals("2") ){
                  Banco_Propietario BancoP = new Banco_Propietario();
                   
                  BancoP.setDistrito(Distrito);
                  BancoP.setPropietario(Propietario);
                  BancoP.setHc(HC);
                  BancoP.setBanco(Banco);
                  BancoP.setSucursal(Sucursal);
                  model.banco_PropietarioService.ModificarBancoPropietario(BancoP);
                  next = next + "?reload=ok&msg=Se Modificaron  los datos  con exito!";
                   
               }
               if ( opcion.equals("3") ){
                   System.out.println("entro op 3");
                  Banco_Propietario BancoP = new Banco_Propietario();
                  model.banco_PropietarioService.AnularBancoPro(Distrito, Propietario, HC);
                  next = next + "?reload=ok&msg=El banco Propietario fue anulado con exito!";
                   
               }
              
               RequestDispatcher rd = application.getRequestDispatcher(next);
               if(rd == null)
                    throw new ServletException("No se pudo encontrar "+ next);
                rd.forward(request, response);
             
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en la busqueda  de TrasladoBuscarModificarAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
