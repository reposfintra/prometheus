/*
 * Nombre        PerfilModificarAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class PerfilModificarAction extends Action {
    
    /** Creates a new instance of PerfilModificarAction */
    public PerfilModificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next= request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        String idp = request.getParameter("idp");        
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String []opciones = request.getParameterValues("opciones");
        
        try{ 
            model.perfilService.obtenerPerfilxPropietario(idp);
            if(model.perfilService.getPerfil()!= null){
                Perfil perfil = model.perfilService.getPerfil();
                perfil.setId(idp);
                perfil.setNombre(request.getParameter("nom").toUpperCase());                
                perfil.setUser_update(usuario.getLogin());
                model.perfilService.updatePerfil(perfil);
                
                //obtener lista perfil_opcion con perfil dado
                model.perfilService.listarPerfilOpcion(idp);
                Vector vpo = model.perfilService.getVPerfil();                
                //Agrego opciones que se encuentran en []opciones y no en PO
                int j = 0;        
                for ( j = 0; j < opciones.length; j++){                    
                    int esta = 0;
                    for ( int i = 0; i<vpo.size(); i++ ){
                        PerfilOpcion po = (PerfilOpcion) vpo.elementAt(i);                                                             
                        if (Integer.parseInt(opciones[j]) == po.getId_opcion()){
                            esta = 1;
                        }
                    }
                   
                    if ( esta == 0 ){
                        try{
                            //AGREGO PO
                            model.perfilService.agregarPerfilOpcion(idp, Integer.parseInt(opciones[j]), usuario.getLogin());                            
                            //obtengo opcion
                            model.menuService.obtenerOpcion(Integer.parseInt(opciones[j]));
                            Menu op = model.menuService.getMenuOpcion();                            
                            //verifico si padre ya esta agregado en po
                            int padre = op.getIdpadre();
                            while ( padre != 0 ){
                                try {
                                    model.perfilService.agregarPerfilOpcion(idp, padre, usuario.getLogin()); 
                                }
                                catch(SQLException exe){
                                    model.perfilService.activarPerfilOpcion(usuario.getLogin(), idp, padre);
                                }                                
                                model.menuService.obtenerOpcion(padre);
                                padre = model.menuService.getMenuOpcion().getIdpadre();
                            }
                        }catch ( SQLException ex ){//ya se encuentra opcion pero anulada
                            //ACTIVO  PO 
                            model.perfilService.activarPerfilOpcion(usuario.getLogin(), idp, Integer.parseInt(opciones[j]));
                            //obtengo opcion
                            model.menuService.obtenerOpcion(Integer.parseInt(opciones[j]));
                            Menu op = model.menuService.getMenuOpcion();
                            int padre = op.getIdpadre();
                            //padre ya esta en po y esta anulado
                             while ( padre != 0 ){
                                try {
                                    model.perfilService.agregarPerfilOpcion(idp, padre, usuario.getLogin()); 
                                }
                                catch(SQLException exe){
                                   //Activo al padre                                    
                                    model.perfilService.activarPerfilOpcion(usuario.getLogin(), idp, padre);
                                }
                                //obtengo nueva opcion (padre)
                                model.menuService.obtenerOpcion(padre);
                                padre = model.menuService.getMenuOpcion().getIdpadre();
                            }
                        }
                    }                   
                }
               
                //Anulo opciones que estan en PO y No en opciones[]
                for ( int i = 0; i<vpo.size(); i++ ){
                    PerfilOpcion po = (PerfilOpcion) vpo.elementAt(i); 
                    //obtengo la opcion
                    Menu m = model.menuService.buscarOpcionMenuxIdopcion(po.getId_opcion());
                    //////System.out.println(" OPCION " + m.getNombre());
                    if (m.getSubmenu() != 1 ){
                        int esta = 0;
                        for ( int z = 0; z < opciones.length; z++){ 
                           if (po.getId_opcion() == Integer.parseInt(opciones[z])){                        
                                esta = 1;
                            }
                        }
                   
                        if ( esta == 0 ){
                             //ANULO PO
                            model.perfilService.anularPerfilOpcion(usuario.getLogin(), idp, po.getId_opcion());                               
                            //verifico si el padre queda sin hijos para anularlo tambien
                            int opc = po.getId_opcion();//opcion
                            int pop = m.getIdpadre();//padre opc
                            //mientras q la opcion no sea 0
                            while ( opc != 0 ){
                                if (!model.menuService.tieneHijosActivosxPerfil(idp, pop)){
                                    //ANULO PO
                                    model.perfilService.anularPerfilOpcion(usuario.getLogin(), idp, pop);                               
                                }
                                Menu o = model.menuService.buscarOpcionMenuxIdopcion(pop);
                                opc = pop;
                                pop = o.getIdpadre();
                            }
                        }
                    }
                }
                model.perfilService.opcionesActivasxPerfil(idp);                    
                request.setAttribute("perfil",model.perfilService.getPerfil());                
                next = next + "?tipo=Modificar&msg=Modificacion exitosa!";
            }
            else{
                request.setAttribute("msg", "error");
            }            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage() );
        }
        this.dispatchRequest(next);     
    }    
}//end class    