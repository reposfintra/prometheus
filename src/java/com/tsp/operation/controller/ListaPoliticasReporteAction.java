/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.ListaPoliticasReporteService;
import com.tsp.operation.model.beans.Politicas;
import com.tsp.operation.model.beans.PreReporteCentralRiesgo;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.*;
import javax.servlet.http.HttpSession;
import com.tsp.util.*;
import java.io.PrintWriter;

/**
 *
 * @author aariza
 */
public class ListaPoliticasReporteAction extends Action{
   
    ListaPoliticasReporteService lprs;
    
    public  ListaPoliticasReporteAction(){}
    
    @Override
    public void run() throws ServletException, InformationException {
        String next = "";
        String opcion = "";
        String conv = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        lprs = new ListaPoliticasReporteService(usuario.getBd());
        boolean redirect = false;
        String cadenaWrite = "";
        String respuesta = "";
        String base = (request.getParameter("base") != null) ? request.getParameter("base") : "";
        String control = (request.getParameter("control") != null) ? request.getParameter("control") : "";
        
        //pilitica datacredito.
        String reporte = (request.getParameter("reporte") != null) ? request.getParameter("reporte") : "";
        String politica=(request.getParameter("politica") != null) ? request.getParameter("politica") : "";
       
        try{
        opcion = request.getParameter("opcion");
       
        if(opcion.equals("1")){
                String entidad = (request.getParameter("entidad") != null) ? request.getParameter("entidad") : "";
                conv = (request.getParameter("idconv") != null) ? request.getParameter("idconv") : "";
                ArrayList lista = null;
                 try {
                    lista = lprs.getNomCovenios(entidad);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
              try{   
             cadenaWrite = "<select id='seleconv' name='seleconv' style='width:20em;'>";
                cadenaWrite += "<option value=''>...</option>";
                 String[] dato1 = null;
                for (int i = 0; i < lista.size(); i++) {
                    dato1 = ((String) lista.get(i)).split("-");
                    cadenaWrite += " <option value='" + dato1[0] + "'";
                    if(conv.equals(dato1[0])){
                    cadenaWrite += "selected='selected'";
                    }
                   cadenaWrite += ">" + dato1[1] + "</option>"; 
                }

                cadenaWrite += "</select>";    
            
                
        }catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                } finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(cadenaWrite);
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
        }
                
        }else if(opcion.equals("2")){
        try{
            Politicas poli = null;
            poli = cargarPoliticas();
           respuesta =  lprs.insertarPolitica(poli);
         
            }catch(Exception e){
           
            System.out.println("Error en action ListaPoliticasReporte: " + e.toString());            
            e.printStackTrace();
        }finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(respuesta);
                    
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
        }
            
}else{
        if(opcion.equals("3")){
        try{
            Politicas poli = null;
            poli = cargarPoliticas();
            int idpol = !request.getParameter("idpolitica").equals("") ? Integer.parseInt(request.getParameter("idpolitica")) : 0;
            lprs.updatePolitica(poli, idpol);
            respuesta = "OK";
            }catch(Exception e){
            respuesta = "ER";
            System.out.println("Error en action ListaPoliticasReporte: " + e.toString());            
            e.printStackTrace();
        }finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(respuesta);
                    
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
        }
        
        }else if(opcion.equals("4")){
        
            try{
            
            int idpolitica = !request.getParameter("idpolitica").equals("") ? Integer.parseInt(request.getParameter("idpolitica")) : 0;
           respuesta = lprs.deletePolitica(idpolitica);
            
            }catch(Exception e){
            
            System.out.println("Error en action ListaPoliticasReporte: " + e.toString());            
            e.printStackTrace();
                }finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(respuesta);
                    
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
                }
        
        
            }else {
        
                if(opcion.equals("5")){
                     try{  
                            //ListaPoliticasReporteService lpservice= new ListaPoliticasReporteService();
                              ArrayList<PreReporteCentralRiesgo> lista = null; //lpservice.getPreReporte();
                           
                     cadenaWrite ="<table class='tablaInferior' border='0' width='100%'>"
                                    + " <tr >"
                                    + "   <td width='50%' align='left' class='subtitulo1'>&nbsp;Reporte a DATACREDITO</td>"
                                    + "   <td width='50%' align='left' class='barratitulo'></td>"
                                    + " </tr>"
                                    + "</table>"
                             + "<table class='tablaInferior' border='0'>"
                             + "    <tr class='fila'>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;No.&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Negocio&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Nombre&nbsp;&nbsp;&nbsp;&nbsp;</td> "
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Cedula&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Total Obligacion&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Total Cuotas&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Inicio Obligacion&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Vencimiento Obligacion&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Cuotas Vencidas&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "      <td>&nbsp;&nbsp;&nbsp;&nbsp;Altura de Mora&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "        <td>&nbsp;&nbsp;&nbsp;&nbsp;Obligacion Vencida&nbsp;&nbsp;&nbsp;&nbsp;</td>"
                             + "    </tr>";
                             
                                            Iterator<PreReporteCentralRiesgo> iterator = lista.iterator();
int contadore = 1;
                         while (iterator.hasNext()) {
                        
                             cadenaWrite += "  <tr>";
                             
                             PreReporteCentralRiesgo listasreporte = iterator.next();
                     
                             cadenaWrite += "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += Integer.toString(contadore);
                             cadenaWrite += "</td>";
                        
                             cadenaWrite += "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += listasreporte.getNegocio();
                             cadenaWrite += "</td>";
                            
                             cadenaWrite += "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += listasreporte.getNomCliente();
                             cadenaWrite += "</td>"
                                    + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += listasreporte.getIdentificacion();
                             cadenaWrite += "</td>"
                                + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                                 cadenaWrite += Utility.customFormat(listasreporte.getObligacionTotal()); 
                             cadenaWrite += "</td>"
                                + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += listasreporte.getNumCuotas();
                             cadenaWrite += "</td>"
                                + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += listasreporte.getFechaApertura();
                             cadenaWrite += "</td>"
                                + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += listasreporte.getFechaVenci();
                             cadenaWrite += "</td>"
                                            + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                                  
                             cadenaWrite += listasreporte.getCuotasEnMora();
                                            
                             cadenaWrite += "</td>"
                                         + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             if (listasreporte.getEstado() <= 30) {
                                     cadenaWrite += "Al Dia";
                             } else if (30 < listasreporte.getEstado() && listasreporte.getEstado() < 61) {
                                 
                                  cadenaWrite += "Mora 30";
                             } else {
                                 if (60 < listasreporte.getEstado() && listasreporte.getEstado() < 91) {
                                       cadenaWrite += "Mora 60";
                                 } else if (90 < listasreporte.getEstado() && listasreporte.getEstado() < 121) {
                                        cadenaWrite += "Mora 90";
                                 } else {
                                     if (120 < listasreporte.getEstado()) {
                                          cadenaWrite += "Mora 120 o mas";
                                          }
                                      
                                      }
                                 }
                                              
                                               
                             cadenaWrite += "</td>"
                                + "<td class='bordereporte' align='center' nowrap style='font-size:11'>";
                             cadenaWrite += Utility.customFormat(listasreporte.getObligacionPendiente());
                             cadenaWrite += "</td>"
                                + "</tr>";
                             contadore = contadore + 1;
                         }

                        cadenaWrite +="</table>";
                          
                        
                            }catch (Exception e) {
                            System.out.println("error(action): " + e.toString());
                            e.printStackTrace();
                            } finally {
                            try {
                            if (redirect == false) {
                            this.escribirResponse(cadenaWrite);
                            } else {
                            this.dispatchRequest(next);
                            }
                            } catch (Exception e) {
                            System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                            e.printStackTrace();
                            }
                            }
                    
                    
                    }else if(opcion.equals("7")){
                    
                        try{
                     int convenio = Integer.parseInt((request.getParameter("convenio") != null) ? request.getParameter("convenio") : "");
                     int entidad = Integer.parseInt((request.getParameter("entidad") != null) ? request.getParameter("entidad") : "");   
                        respuesta =  lprs.insertEntidadConvenio(entidad,convenio);

                        }catch(Exception e){

                        System.out.println("Error en action ListaPoliticasReporte: " + e.toString());            
                        e.printStackTrace();
                        }finally {
                        try {
                        if (redirect == false) {
                        this.escribirResponse(respuesta);

                        } else {
                        this.dispatchRequest(next);
                        }
                        } catch (Exception e) {
                        System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                        e.printStackTrace();
                        }
                        }
                
                    
                    
                    
                    }else {
                    
                   
                    if(opcion.equals("8")){
                    
                        try{
                        String entidad = (request.getParameter("entidad") != null) ? request.getParameter("entidad") : "";   
                        respuesta =  lprs.insertEntidad(entidad);

                        }catch(Exception e){

                        System.out.println("Error en action ListaPoliticasReporte: " + e.toString());            
                        e.printStackTrace();
                        }finally {
                        try {
                        if (redirect == false) {
                        this.escribirResponse(respuesta);

                        } else {
                        this.dispatchRequest(next);
                        }
                        } catch (Exception e) {
                        System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                        e.printStackTrace();
                        }
                        }
                
                    
                    
                    
                    }else if(opcion.equals("9")){
                    
                            try{
                        int id = Integer.parseInt((request.getParameter("id") != null) ? request.getParameter("id") : "");   
                        respuesta =  lprs.deleteEntidadConvenio(id);

                        }catch(Exception e){

                        System.out.println("Error en action ListaPoliticasReporte: " + e.toString());            
                        e.printStackTrace();
                        }finally {
                        try {
                        if (redirect == false) {
                        this.escribirResponse(respuesta);

                        } else {
                        this.dispatchRequest(next);
                        }
                        } catch (Exception e) {
                        System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                        e.printStackTrace();
                        }
                        }
                    
                    
                        } else if (opcion.equals("10")) {

                            /*obtenemos los rangos de vencimientos dependiendo de la 
                             la politica selecionada */
                            //ListaPoliticasReporteService repor = new ListaPoliticasReporteService();
                            String[] rangoVencimiento;
                            String result = lprs.getVencimientosPoliticas(politica, reporte);

                            if (!result.equals("")) { //inicio de if interno

                                rangoVencimiento = result.split(",");
                                
                                String convenio=rangoVencimiento[3].equals("Microcredito")?"10,11,12,13":rangoVencimiento[2]; 

                                //ListaPoliticasReporteService lpservice = new ListaPoliticasReporteService();
                                ArrayList<PreReporteCentralRiesgo> lista = lprs.getPreReporte(rangoVencimiento[0], rangoVencimiento[1],convenio);
                                Gson gson = new Gson();
                                String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
                                this.printlnResponse(json, "application/json;");

                            } else {
                                //respuesta a jquery
                                this.escribirResponse("N");


                            }//fin if interno

                    }
                        if (opcion.equals("11")) {
                    
                            //exporta a excel los datos de la grilla
                            String pdfBuffer = request.getParameter("pdfBuffer");
                            String fileName = request.getParameter("fileName");
                            String fileType = request.getParameter("fileType");
                            boolean isPDF = fileType.equals("pdf");
                            if (isPDF) {
                                /*este codigo es para exporta a pdf para el futuro
                                 * 
                                 * 
                                 ServletOutputStream outputStream = response.getOutputStream();
                                 ITextRenderer renderer = new ITextRenderer();
                                 renderer.setDocumentFromString(pdfBuffer);
                                 renderer.layout();
                                 response.setContentType("application/octet-stream");
                                 response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "." + fileType + "\"");
                                 renderer.createPDF(outputStream);
                                 outputStream.flush();
                                 outputStream.close();
                                 *
                                 *
                                 */
                            } else {
                                response.setContentType("application/vnd.ms-excel");
                                response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "." + fileType + "\"");
                                PrintWriter out = response.getWriter();
                                out.print(pdfBuffer);
                                out.close();
                    }
            

                }   
        


        }
        
                }
        
            }

        
        }catch(Exception e){
        
        System.out.println("Error en action ListaPoliticasReporte: " + e.toString());            
            e.printStackTrace();
        
        
        }
       
    }
    
     protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }
     
 private Politicas cargarPoliticas (){
     
                Politicas politica = new Politicas();
                String centralries = (request.getParameter("centralries") != null) ? request.getParameter("centralries") : "";
                String nompoli = (request.getParameter("nompoli") != null) ? request.getParameter("nompoli") : "";
                String uneg = (request.getParameter("uneg") != null) ? request.getParameter("uneg") : "";
                int conve = !request.getParameter("conv").equals("") ? Integer.parseInt(request.getParameter("conv")) : 0;
                String agenc = (request.getParameter("agenc") != null) ? request.getParameter("agenc") : "";
                int einic = !request.getParameter("einic").equals("") ? Integer.parseInt(request.getParameter("einic")) : 0;
                int efin = !request.getParameter("efin").equals("") ? Integer.parseInt(request.getParameter("efin")) : 0;
                double minic = !request.getParameter("minic").equals("") ? Double.parseDouble(request.getParameter("minic")) : 0;
                double mfin =!request.getParameter("mfin").equals("") ? Double.parseDouble(request.getParameter("mfin")) : 0;
                
                String monto = (request.getParameter("monto") != null) ? request.getParameter("monto") : "";
                String edad = (request.getParameter("edad") != null) ? request.getParameter("edad") : "";
                String agencia = (request.getParameter("agencia") != null) ? request.getParameter("agencia") : "";

           
         
             politica.setAgenciaCobro(agenc);
             politica.setCentralRiesgo(centralries);
             politica.setEdadFin(efin);
             politica.setEdadIni(einic);
             politica.setIdConvenio(conve);
             politica.setMontoFin(mfin);
             politica.setMontoIni(minic);
             politica.setPolitica(nompoli);
             politica.setUnidadNegocio(uneg);
             if(monto.equals("true")){politica.setMonto(true);
             }else if(monto.equals("false")){politica.setMonto(false);}
             if(edad.equals("true")){politica.setEdad(true);
             }else if(edad.equals("false")){politica.setEdad(false);}
             if(agencia.equals("true")){politica.setAgencia(true);
             }else if(agencia.equals("false")){politica.setAgencia(false);}
             return politica;
     }
    
  public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

}
    
}
