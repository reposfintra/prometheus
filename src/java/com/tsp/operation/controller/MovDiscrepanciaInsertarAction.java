/*************************************************************************
 * Nombre ......................MovDiscrepanciaInsertarAction.java       *
 * Descripci�n..................Clase Action para modificar actividad    *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................29 de diciembre de 2005, 11:11 AM        * 
 * Versi�n......................1.0                                      * 
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/
 

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class MovDiscrepanciaInsertarAction extends Action {
    
    /** Creates a new instance of MovDiscrepanciaInsertarAction */
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/cumplidos/movimiento_discrepancia/productosclienteubicacion.jsp?mensaje=Ok";
        String cod = request.getParameter("codcli");
        String ubi = request.getParameter("ubi");
        String candes = "candes";
        try{
           Vector vec = model.discrepanciaService.getDiscrepancias();
           for(int i=0; i< vec.size(); i++ ){
               int cant = Integer.parseInt( ((request.getParameter(candes+i)!=null) && (!request.getParameter(candes+i).equals("")))?request.getParameter(candes+i):"0" );
               if (cant > 0){
                   Discrepancia dis = (Discrepancia) vec.elementAt(i);
                   dis.setCantidad_nueva(cant);
                   dis.setUsuario_creacion(usuario.getLogin());
                   dis.setUsuario_modificacion(usuario.getLogin());
                   dis.setBase(usuario.getBase());
                   dis.setDistrito(usuario.getDstrct());
                   dis.setNuevo_numpla(request.getParameter("numpla"));
                   model.discrepanciaService.insertarMovDiscrepancia(dis);
                   model.discrepanciaService.actualizarInventario(dis);   
                   
               }
               
           }
           model.discrepanciaService.listProductosCliente(cod, ubi,distrito); 
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
