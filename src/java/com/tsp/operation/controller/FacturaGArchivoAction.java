/********************************************************************
 *      Nombre Clase.................   FacturaBuscarproveedoresAction.java
 *      Descripci�n..................   Action que se encarga de cargar los bancos en la pagina siguiente
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaGArchivoAction extends Action{
    
    /** Creates a new instance of FacturaGArchivoAction */
    public FacturaGArchivoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            
            HttpSession session = request.getSession();
            //Ivan 21 julio 2006
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin=""+usuario.getLogin();
            
            String opcion = (request.getParameter("opcion")!= null )?request.getParameter("opcion"):"";
            String next = "";
            
            if(opcion.equals("") ){
                String maxfila               =     request.getParameter("maxfila");
                String tipo_documento        = ""+ request.getParameter("tipo_documento");
                String documento             = ""+ request.getParameter("documento");
                String proveedor             = ""+ request.getParameter("proveedor");
                String tipo_documento_rel    = ""+ request.getParameter("tipo_documento_rel");
                String documento_relacionado = ""+ request.getParameter("documento_relacionado");
                String fecha_documento       = ""+ request.getParameter("fecha_documento");
                String banco                 = ""+ request.getParameter("c_banco");
                String agencia               =     request.getParameter("agencia");
                double vlr_neto              = model.cxpDocService.getNumero(""+ request.getParameter("vlr_neto"));
                double total    =   model.cxpDocService.getNumero(""+ request.getParameter("total"));
                double total_neto       =    model.cxpDocService.getNumero (""+ request.getParameter ("total_neto"));
                
                String CabIva           =   (request.getParameter("CabIva")!=null?request.getParameter("CabIva").toUpperCase():"");
                String CabRiva          =   (request.getParameter("CabRiva")!=null?request.getParameter("CabRiva").toUpperCase():"");
                String CabRica          =   (request.getParameter("CabRica")!=null?request.getParameter("CabRica").toUpperCase():"");
                String CabRfte          =   (request.getParameter("CabRfte")!=null?request.getParameter("CabRfte").toUpperCase():"");
                String moneda_banco     = (request.getParameter("moneda_banco")!=null)?request.getParameter("moneda_banco"):"";
                //Ivan Dario 28 Octubre 2006
                String agenciaBanco     = (request.getParameter("agenciaBanco")!=null)?request.getParameter("agenciaBanco"):"";
                /////////////////////////////////
                String fecha_aprobacion  =   (request.getParameter("fecha_aprobacion")!=null?request.getParameter("fecha_aprobacion"):"");
                String usu_ap            =   (request.getParameter("usu_ap")!=null?request.getParameter("usu_ap"):"");
                String beneficiario = (request.getParameter("beneficiario")!=null)?request.getParameter("beneficiario"):"";
                String hc = (request.getParameter("hc")!=null)?request.getParameter("hc"):"";
                int plazo=0;
                int num_items=0;
                
                // //System.out.println("Numero de Items sin castin "+ request.getParameter ("num_items"));
                try {
                    num_items  = Integer.parseInt(""+ request.getParameter("num_items"));
                    plazo = Integer.parseInt(""+ request.getParameter("plazo"));
                }
                
                
                catch(java.lang.NumberFormatException e) {
                    vlr_neto=0;
                    plazo=0;
                    num_items=1;
                    total = 0;
                }
                
                Vector vItems = new Vector();
                //System.out.println("Numero de Items " + num_items);
                int MaxFi = Integer.parseInt(maxfila);
                for (int i=1;i <= MaxFi; i++){
                    CXPItemDoc item = new CXPItemDoc();
                    String filaTabla = request.getParameter("valor1"+i);
                    if ( filaTabla!=null  ){
                        
                        String descripcion = ""+request.getParameter("desc"+i);
                        String concepto=""+ request.getParameter("descripcion_i"+i);
                        String codigo_cuenta=""+ request.getParameter("codigo_cuenta"+i);
                        String codigo_abc=""+ request.getParameter("codigo_abc"+i).toUpperCase();
                        String planilla=""+ request.getParameter("planilla"+i);
                        double valor =0;
                        double vlr_total = 0;
                        
                        String tipcliarea=""+ request.getParameter("cod_oc"+i);
                        String codcliarea= ""+ request.getParameter("oc"+i);
                        String descliarea= ""+ request.getParameter("doc"+i);
                        String iva       = ""+ request.getParameter("iva"+i);
                        
                        String vlr_riva  = ""+ request.getParameter("vlr_riva"+i);
                        String vlr_rica  = ""+ request.getParameter("vlr_rica"+i);
                        String vlr_rfte  = ""+ request.getParameter("vlr_rfte"+i);
                        
                        //Ivan 26 julio 2006
                        String ree = ""+ request.getParameter("REE"+i);
                        String Ref3 = ""+ request.getParameter("Ref3"+i);
                        String Ref4 = ""+ request.getParameter("Ref4"+i);
                        String agenciaItem = ""+ request.getParameter("agencia"+i);
                        
                        String cod1 = request.getParameter("cod1"+i);
                        String cod2 = request.getParameter("cod2"+i);
                        String cod3 = request.getParameter("cod3"+i);
                        String cod4 = request.getParameter("cod4"+i);
                        String cod5 = request.getParameter("cod5"+i);
                        String [] codigos = {cod1,cod2,cod3,cod4,cod5};
                        
                        // Modificacion 21 julio 2006
                        LinkedList tbltipo = null;
                        String auxiliar      = request.getParameter("auxiliar"+i);
                        String tipoSubledger = request.getParameter("tipo"+i);
                        
                        valor=model.cxpDocService.getNumero(""+ request.getParameter("valor1"+i));
                        vlr_total = model.cxpDocService.getNumero(""+ request.getParameter("valorNeto"+i));
                        //Ivan 26 julio 2006
                        item.setRee(ree);
                        item.setRef3(Ref3);
                        item.setRef4(Ref4);
                        item.setAgencia(agenciaItem);
                        
                        item.setCodigos(codigos);
                        item.setConcepto(concepto);
                        item.setDescripcion(descripcion);
                        item.setCodigo_cuenta(codigo_cuenta);
                        item.setCodigo_abc(codigo_abc);
                        item.setPlanilla(planilla);
                        item.setVlr_me(valor);
                        item.setVlr_total(vlr_total);
                        item.setTipcliarea(tipcliarea);
                        item.setCodcliarea(codcliarea);
                        item.setDescliarea(descliarea);
                        item.setIva(iva);
                        item.setPorc_riva(vlr_riva);
                        item.setPorc_rica(vlr_rica);
                        item.setPorc_rfte(vlr_rfte);
                        
                        
                        // ivan 21 julio 2006
                        item.setAuxiliar(auxiliar);
                        item.setTipo(tbltipo);
                        item.setTipoSubledger(tipoSubledger);
                        /////////////////////////////////
                        
                        
                        Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos();
                        Vector vImpuestosPorItem= new Vector();
                        for(int x=0;x<vTipoImp.size();x++){
                            CXPImpItem impuestoItem = new CXPImpItem();
                            String cod_impuesto = ""+ request.getParameter("impuesto"+x+""+i).toUpperCase();
                            impuestoItem.setCod_impuesto(cod_impuesto);
                            //System.out.println("impuesto"+cod_impuesto);
                            vImpuestosPorItem.add(impuestoItem);
                        }
                        
                        item.setVItems( vImpuestosPorItem);
                        item.setVCopia(vImpuestosPorItem);
                        vItems.add(item);
                    }else{
                        vItems.add(item);
                    }
                }
                
                model.cxpItemDocService.setVecCxpItemsDoc(vItems);
                
                
                //System.out.println("numero de items"+num_items);
                String sucursal =""+ request.getParameter("c_sucursal");
                String moneda=""+ request.getParameter("moneda");
                String descripcion =""+ request.getParameter("descripcion");
                String observacion =""+ request.getParameter("observacion");
                String usuario_aprobacion=""+request.getParameter("usuario_aprobacion");
                
                CXP_Doc factura = new CXP_Doc();
                factura.setTipo_documento(tipo_documento);
                factura.setDocumento(documento);
                factura.setProveedor(proveedor);
                factura.setTipo_documento_rel(tipo_documento_rel);
                factura.setDocumento_relacionado(documento_relacionado);
                factura.setFecha_documento(fecha_documento);
                factura.setBanco(banco);
                factura.setVlr_neto(total);
                factura.setVlr_total(vlr_neto);
                factura.setTotal_neto(total_neto);
                factura.setMoneda(moneda);
                factura.setSucursal(sucursal);
                factura.setDescripcion(descripcion);
                factura.setObservacion(observacion);
                factura.setUsuario_aprobacion(usuario_aprobacion);
                factura.setPlazo(plazo);
                factura.setAgencia(agencia);
                factura.setMaxfila(maxfila);
                factura.setIva(CabIva);
                factura.setRiva(CabRiva);
                factura.setRica(CabRica);
                factura.setRfte(CabRfte);
                factura.setMoneda_banco(moneda_banco);
                
                //Ivan DArio 28 Octubre 2006
                factura.setAgenciaBanco(agenciaBanco);
                ////////////////////////////////////
                factura.setFecha_aprobacion(fecha_aprobacion);
                factura.setAprobador(usu_ap);
                factura.setBeneficiario(beneficiario);
                factura.setHandle_code(hc);
                
                model.cxpDocService.setFactura(factura);
                
                model.cxpDocService.escribirArchivo(factura,vItems,maxfila,"factura"+userlogin+".txt", userlogin);
                
                for(int i=0; i< vItems.size();i++){
                    CXPItemDoc item = (CXPItemDoc)vItems.get(i);
                    if(item != null){
                        
                        LinkedList tbltipo = null;
                        if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),item.getCodigo_cuenta())){
                            modelcontab.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),item.getCodigo_cuenta());
                            tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                        }
                        
                        item.setTipo(tbltipo);
                    }
                }
                model.cxpItemDocService.setVecCxpItemsDoc(vItems);
                String ag = usuario.getId_agencia() ;
                if( !ag.equals("OP") )
                    next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp?op=cargarB&num_items="+num_items+"&ag=false&maxfila="+maxfila;
                else
                    next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp?op=cargarB&num_items="+num_items+"&maxfila="+maxfila;
            }
            else{
                model.cxpDocService.setFactura(null);
                model.cxpItemDocService.setVecCxpItemsDoc(null);
                boolean flag = model.cxpDocService.BorrarArchivo("factura"+userlogin+".txt", userlogin);
                
                String ag = usuario.getId_agencia() ;
                if( !ag.equals("OP") ){
                    session.setAttribute("id_agencia", ag );
                    next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp?op=cargarB&ag=false";
                }
                else
                    next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp";
                
            }
            
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}
