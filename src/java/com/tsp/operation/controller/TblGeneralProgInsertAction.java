/******************************************************************
 * Nombre ......................TblGeneralProgInsertAction.java
 * Descripci�n..................Clase Action para descuento de equipos
 * Autor........................Ricardo Rosero
 * Fecha........................23/12/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class TblGeneralProgInsertAction extends Action{
    
    /** Creates a new instance of TblGeneralProgInsertAction */
    public TblGeneralProgInsertAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String Mensaje =  "";
        String next = "";
        int sw=0;
        HttpSession session = request.getSession ();
        String table_code = request.getParameter ("table_code")!=null?request.getParameter ("table_code"):"";
        String table_type = request.getParameter ("table_type")!=null?request.getParameter ("table_type"):"";
        String program = request.getParameter ("program")!=null?request.getParameter ("program"):"";
        try{
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String base = usuario.getBase ();
            String distrito = (String)(session.getAttribute ("Distrito"));
            TblGeneralProg tmp = new TblGeneralProg ();
            Calendar fechaXI = Calendar.getInstance ();
            fechaXI.add (fechaXI.DATE, -0);
            java.util.Date fechXI = fechaXI.getTime ();
            SimpleDateFormat s1 = new SimpleDateFormat ("yyyy-MM-dd");
            String fechaX1 = s1.format (fechXI);
            tmp.setBase (base);
            tmp.settable_code (table_code);
            tmp.settable_type (table_type);
            tmp.setFC (fechaX1);
            tmp.setUc (usuario.getLogin ());
            tmp.setprogram (program);
            tmp.setDistrito (distrito);
            tmp.setFm (fechaX1);
            tmp.setUm (usuario.getLogin ());
            if(!model.tbl_GeneralProgService.existeTblGeneralProg (table_type, table_code, program)){
                if(model.tbl_GeneralProgService.estaAnuladoTblGeneralProg (table_type, table_code, program)){
                    model.tbl_GeneralProgService.setTblGeneralProg (tmp);
                    model.tbl_GeneralProgService.updateTblGeneralProg ();
                }
                else{
                    model.tbl_GeneralProgService.setTblGeneralProg (tmp);
                    model.tbl_GeneralProgService.addTblGeneralProg ();
                }
                Mensaje = "El registro ha sido insertado satisfactoriamente";  
                
            }
            else{
                Mensaje = "Error la informaci�n ya existe!";
            }
            next = "/jsp/general/tablagen_prog/TblGeneralProgIngresar.jsp?Mensaje="+Mensaje;
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
