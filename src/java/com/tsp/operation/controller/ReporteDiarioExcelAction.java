/*
 * ReporteDiarioExcelAction.java
 *
 * Created on 5 de septiembre de 2005, 04:44 PM
 */

package com.tsp.operation.controller;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
//import com.tsp.operation.model.GenerarReporteDiario;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 *
 * @author  Administrador
 */
public class ReporteDiarioExcelAction extends Action{
    
    /** Creates a new instance of ReporteDiarioExcelAction */
    public ReporteDiarioExcelAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String Opcion = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
        HttpSession Session           = request.getSession();
        
        if(Opcion.equals("Guardar")){
            ReporteDiarioDespacho hilo = new ReporteDiarioDespacho();
            hilo.start(model,(Vector)Session.getAttribute("Informes"),(Vector)Session.getAttribute("Agencias"),(String)Session.getAttribute("fechaInicio"),(String)Session.getAttribute("fechaFin"), (String)Session.getAttribute("id") );
        }
        
        String next = "/agencia/ReporteDD.jsp";
        this.dispatchRequest(next);
    }
    
}
