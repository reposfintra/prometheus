/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.SectorSubsectorService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author maltamiranda
 */
public class SectorSubsectorAction extends Action {

    //protected HttpServletRequest request;
    public SectorSubsectorAction() {
    }

    @Override
    public void run() throws ServletException, InformationException {
        String id, tipo, nombre, detalle, idsub,codigo, activo, codigo_alterno, reliquida, nombre_alterno;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        id = (request.getParameter("id") != null) ? request.getParameter("id") : "";
        idsub = (request.getParameter("idsub") != null) ? request.getParameter("idsub") : "";
        tipo = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";
        nombre = (request.getParameter("nombre") != null) ? request.getParameter("nombre") : "";
        codigo = (request.getParameter("codigo") != null) ? request.getParameter("codigo") : "";
        detalle = (request.getParameter("detalle") != null) ? request.getParameter("detalle") : "";
        activo=(request.getParameter("activo") != null) ? request.getParameter("activo") : "";
        codigo_alterno = (request.getParameter("codigo_alterno") != null) ? request.getParameter("codigo_alterno") : "";
        nombre_alterno = (request.getParameter("nombre_alterno") != null) ? request.getParameter("nombre_alterno") : "";
        reliquida=(request.getParameter("reliquida") != null) ? request.getParameter("reliquida") : "N";
        
     
        String next = "/jsp/fenalco/convenio/detalleSectores.jsp";
        try {
            if (tipo.equals("MODIFICAR")) {
                session.removeAttribute("subsectores");
                session.setAttribute("subsectores", model.sectorsubsectorSvc.getSubSectores(id, usuario.getBd()));
                model.sectorsubsectorSvc.buscar_sector(id, usuario.getBd());
                next+="?nombre="+model.sectorsubsectorSvc.getNombre_sector()+"&detalle="+model.sectorsubsectorSvc.getDetalle_sector()+"&activo="+model.sectorsubsectorSvc.getActivo();
            } else {
                if (tipo.equals("NUEVO_SECTOR") || tipo.equals("NUEVO_SUBSECTOR")) {
                    next = "/jsp/fenalco/convenio/NuevoSectorSubsector.jsp";
                } else {
                    if (tipo.equals("AGREGAR_SECTOR")) {
                        response.setContentType("text/plain");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().print(model.sectorsubsectorSvc.agregar_sector(nombre, detalle, codigo,activo, usuario.getLogin(), usuario.getBd(), reliquida, codigo_alterno, nombre_alterno));
                    } else {
                        if (tipo.equals("AGREGAR_SUBSECTOR")) {
                            response.setContentType("text/plain");
                            response.setHeader("Cache-Control", "no-cache");
                            response.getWriter().print(model.sectorsubsectorSvc.agregar_subsector(nombre, detalle, codigo,activo, id, usuario.getLogin(), usuario.getBd(), nombre_alterno));
                        }
                        else {
                            if (tipo.equals("VENTANA_EDITAR")) {
                                model.sectorsubsectorSvc.buscar_sector(id, usuario.getBd());
                                next = "/jsp/fenalco/convenio/NuevoSectorSubsector.jsp";
                            } else {
                                if (tipo.equals("VENTANA_EDITAR_SUBSECTOR")) {
                                    model.sectorsubsectorSvc.buscar_subsector(id, idsub, usuario.getBd());
                                    next = "/jsp/fenalco/convenio/NuevoSectorSubsector.jsp";
                                }
                                else {
                                    if (tipo.equals("EDITAR")) {
                                        model.sectorsubsectorSvc.editar_sector(codigo, nombre, detalle, activo,usuario.getLogin(), usuario.getBd(), reliquida, codigo_alterno, nombre_alterno);
                                        response.setContentType("text/plain");
                                        response.setHeader("Cache-Control", "no-cache");
                                        response.getWriter().print("La actualizacion fue realizada exitosamente...");
                                    }
                                    else{
                                        if (tipo.equals("EDITAR_SUBSECTOR")) {
                                            model.sectorsubsectorSvc.editar_subsector(id, codigo, nombre, detalle,activo, usuario.getLogin(), usuario.getBd(), nombre_alterno);
                                            response.setContentType("text/plain");
                                            response.setHeader("Cache-Control", "no-cache");
                                            response.getWriter().print("La actualizacion fue realizada exitosamente...");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!tipo.equals("AGREGAR_SECTOR") && !tipo.equals("AGREGAR_SUBSECTOR") && !tipo.equals("EDITAR") && !tipo.equals("EDITAR_SUBSECTOR")) {
                this.dispatchRequest(next);
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.toString();
        }
    }
}
