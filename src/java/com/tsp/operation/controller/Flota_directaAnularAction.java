/*******************************************************************
 * Nombre clase: Flota_directaAnularAction.java
 * Descripci�n: Accion para anular una flota directa a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 2 de diciembre de 2005, 03:02 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Flota_directaAnularAction extends Action{
    
    /** Creates a new instance of Flota_directaAnularAction */
    public Flota_directaAnularAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String nit = request.getParameter("c_nit");
        String placa = request.getParameter("c_placa");
        try{
            Flota_directa flota = new Flota_directa();
            flota.setNit (nit);
            flota.setPlaca (placa);
            flota.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            model.flota_directaService.anularFlota_directa(flota);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
