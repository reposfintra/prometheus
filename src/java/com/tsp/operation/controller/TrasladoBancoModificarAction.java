/*
 * PlacaInsertAction.java
 *
 * Created on 4 de noviembre de 2004, 02:48 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  FFERNANDEZ
 */
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import org.apache.log4j.Logger;

public class TrasladoBancoModificarAction extends Action{
    
    //  static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public TrasladoBancoModificarAction () {
    }
    
    public void run () throws ServletException, InformationException {
   
        String next = "/jsp/finanzas/contab/comprobante/traslados/traslado_modificar.jsp?reload=ok";
        BeanGeneral traslado = new BeanGeneral ();
        String msg = "";        
        
        
        String banco_Destino = request.getParameter ("bancoDes")!=null?request.getParameter ("bancoDes"):"";
        String banco_Origen = (request.getParameter("bancoOri")!=null)?request.getParameter ("bancoOri"):"";
        String SucBancoOri = (request.getParameter("SucBancoOri")!=null)?request.getParameter ("SucBancoOri"):"";
        String SucBancoDes = (request.getParameter("SucBancoDes")!=null)?request.getParameter ("SucBancoDes"):"";
        String valor = (request.getParameter("valor")!=null)?request.getParameter ("valor"):"";
        String fecha = (request.getParameter("fecha")!=null)?request.getParameter ("fecha"):"0099-01-01";
        
        String bori = request.getParameter ("bori")!=null?request.getParameter ("bori"):"";
        String bdes = (request.getParameter("bdes")!=null)?request.getParameter ("bdes"):"";
        String sucori = (request.getParameter("sucori")!=null)?request.getParameter ("sucori"):"";
        String sucdes = (request.getParameter("sucdes")!=null)?request.getParameter ("sucdes"):"";
        String val = (request.getParameter("val")!=null)?request.getParameter ("val"):"";
        String fec = (request.getParameter("fec")!=null)?request.getParameter ("fec"):"0099-01-01";
        String llave = (request.getParameter("llave")!=null)?request.getParameter ("llave"):"";
        
     
            traslado.setValor_01 (banco_Origen);
            traslado.setValor_02 (SucBancoOri);
            traslado.setValor_03 (banco_Destino);
            traslado.setValor_04(SucBancoDes);
            traslado.setValor_05(valor);
            traslado.setValor_06 (fecha);
            traslado.setValor_07 (bori);
            traslado.setValor_08 (sucori);
            traslado.setValor_09 (bdes);
            traslado.setValor_10(sucdes);
            traslado.setValor_11(val);
            traslado.setValor_12(fec);

            
            try{
               
                model.trasladoService.modificarTrasladoBancario(traslado);
                //System.out.println("llave es    =====  "+llave);
                if(!llave.equals("")){
                    double temp = 0;
                    double valores = Double.parseDouble(valor);
                    String detallesOri = ""+bori + "-"+ bdes;
                    String detallesDes = ""+sucori + "-"+ sucdes;
                    //System.out.println("detalles ori     "+detallesOri.trim());
                    //System.out.println("detalles Des     "+detallesDes.trim());
                    model.trasladoService.modificarComrpobante(llave,banco_Origen,SucBancoOri,banco_Destino,SucBancoDes,valores);
                    //agrego en banco de origen
                    model.trasladoService.modificarComprodec(llave,banco_Origen,SucBancoOri,temp,valores,detallesOri.trim());
                    //agrego en banco destino
                    model.trasladoService.modificarComprodec(llave,banco_Destino,SucBancoDes,valores,temp,detallesDes.trim());
                }
                next += "&msg=" + "El traslado bancario fue modificado exitosamente";
                    
            }
            catch (Exception e){
                e.printStackTrace ();
                throw new ServletException (e.getMessage ());
            }
        
        // Redireccionar a la p�gina indicada.
         this.dispatchRequest (next);
    }
}
