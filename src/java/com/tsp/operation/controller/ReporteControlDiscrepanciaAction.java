/***************************************************************************
 * Nombre clase : ............... ReporteControlDiscrepanciaAction.java    *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de control de discrepancias      *
 * Autor :....................... Ing. Jose De la rosa                     *
 * Fecha :........................ 29 de agosto de 2006, 11:16 AM          *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.controller;

import org.apache.log4j.Logger;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;

public class ReporteControlDiscrepanciaAction extends Action{
    
    /** Creates a new instance of ReporteControlDiscrepanciaAction */
    public ReporteControlDiscrepanciaAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "";
        String cmd = request.getParameter("cmd");
        String fecini = request.getParameter("fechaInicio");
        String fecfin = request.getParameter("fechaFinal");
        String contentType = request.getParameter("tipoPage");
        String tipo = request.getParameter("reporte");
        if(cmd == null)
            next = "/jsp/general/reporte/ReporteBuscarDiscrepancia.jsp";
        else{
             HttpSession session = request.getSession();
             Usuario usuario = (Usuario)session.getAttribute("Usuario");
             if (contentType.equals("HTML") || contentType.equals("EXCEL")){
                 try{
                     model.discrepanciaService.planillasMigradasMIMS (fecini, fecfin, tipo);
                 }
                 catch(SQLException e){
                     throw new ServletException(e.getMessage());
                 }
                 next = "/jsp/general/reporte/ReporteControlDiscrepancia.jsp?tipoPage="+contentType;
             }
             else if(contentType.equals("file")){
                  next = "/jsp/general/reporte/ReporteControlDiscrepancia.jsp?op=file";
             }
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
