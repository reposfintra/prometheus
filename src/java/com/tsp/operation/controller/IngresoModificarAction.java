/*************************************************************************
 * Nombre:        IngresoModificarAction.java
 * Descripci�n:   Clase Action para modificar registros del ingreso de pagos por clientes
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         16 de mayo de 2006, 03:41 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.NotaCreditoPDF;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

import com.tsp.pdf.*;


public class IngresoModificarAction extends Action{
    
    /** Creates a new instance of IngresoModificarAction */
    public IngresoModificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        String mon_local = "";
        String mon_ingreso = request.getParameter("moneda");
        double valor_con = Double.parseDouble(request.getParameter("valor"));
        String fecha_actual = Util.getFechaActual_String(4);
        boolean sw = true;
        String auxiliar = "";
        String opcion = ( request.getParameter("opcion") != null )?request.getParameter("opcion"):"";
        String abc = ( request.getParameter("abc") != null )?request.getParameter("abc").toUpperCase():"";
        double tasaDB = Double.parseDouble( ( request.getParameter("tasa") != null )? !request.getParameter("tasa").equals("")? request.getParameter("tasa").replaceAll(",",""):"0":"0" );
        
        try{
            if (opcion.equals("imp")){
                try{
                    
                    String numero_ingreso   = ( request.getParameter("num") != null )?request.getParameter("num"):"";
                    String tipo_documento    = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                    
                    
                    if( tipo_documento.equals("ING") ){
                        IngresoPDF ingreso = new IngresoPDF();
                        ingreso.RemisionPlantilla();
                        ingreso.crearCabecera();
                        ingreso.crearRemision(model, usuario.getDstrct(), numero_ingreso, tipo_documento, "", usuario.getLogin() );
                        ingreso.generarPDF();
                        next = "/pdf/IngresoPDF.pdf";
                    }
                    else{
                        NotaCreditoPDF nota = new NotaCreditoPDF();
                        nota.RemisionPlantilla();
                        nota.crearCabecera();
                        nota.crearRemision(model, usuario.getDstrct(), numero_ingreso, tipo_documento, "", usuario.getLogin() );
                        nota.generarPDF();
                        next = "/pdf/NotaCreditoPDF.pdf";
                    }
                    
                }catch(Exception ex){
                    ex.getMessage();
                    ex.printStackTrace();
                }
                
            }else{
                String sucursal = request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"";
                String vec[] = sucursal.split("/");
                String cban = "";
                if( vec != null && vec.length > 1 ){
                    sucursal = vec[0];
                    cban = vec[1];
                }
                Ingreso ingres  = model.ingresoService.getIngreso();
                Ingreso ing = new Ingreso();
                ing.setDstrct(usuario.getDstrct());
                ing.setReg_status("");
                ing.setCodcli("");
                ing.setNum_ingreso( request.getParameter("num") );
                ing.setNitcli(request.getParameter("identificacion"));
                ing.setConcepto(request.getParameter("concepto"));
                ing.setFecha_consignacion(request.getParameter("fecha")!=null?request.getParameter("fecha"):ingres.getFecha_consignacion()==null?fecha_actual:ingres.getFecha_consignacion());
                ing.setBranch_code(request.getParameter("banco"));
                ing.setBank_account_no(sucursal);
                ing.setCodmoneda(mon_ingreso);
                ing.setLast_update(Util.fechaActualTIMESTAMP());
                ing.setUser_update(usuario.getLogin());
                ing.setDescripcion_ingreso(request.getParameter("descripcion"));
                ing.setNro_consignacion("");
                ing.setTipo_documento(request.getParameter("tipodoc"));
                ing.setCuenta(request.getParameter( "cuenta1" ).toUpperCase() );
                ing.setTasaDolBol(tasaDB);
                ing.setAbc(abc);
                LinkedList t_aux = new LinkedList();
                try{
                    modelcontab.subledgerService.buscarCuentasTipoSubledger( ing.getCuenta() );
                    t_aux = modelcontab.subledgerService.getCuentastsubledger();
                }catch(Exception e){
                    t_aux = null;
                }
                ing.setTipos( t_aux );
                String aux = request.getParameter( "auxiliar1" )!=null?request.getParameter( "auxiliar1" ):"";
                String tipo = request.getParameter( "tipo1" ) != null?request.getParameter( "tipo1" ):"";
                auxiliar = ( aux.equals("") && tipo.equals("") )?"" :request.getParameter( "tipo1" )+"-"+request.getParameter( "auxiliar1" ) ;
                ing.setAuxiliar( auxiliar );
                mon_local =  (String) session.getAttribute("Moneda");
                Tasa tasa = model.tasaService.buscarValorTasa( mon_local, mon_ingreso, mon_local, request.getParameter("fecha")!=null?request.getParameter("fecha"):ingres.getFecha_consignacion()==null?fecha_actual:ingres.getFecha_consignacion());
                
                if(tasa!=null){
                    ing.setVlr_ingreso( (!mon_local.equals("DOL"))? (int)  Math.round( tasa.getValor_tasa() * valor_con ) : tasa.getValor_tasa() * valor_con  );
                    ing.setVlr_ingreso_me(valor_con);
                    ing.setVlr_saldo_ing( valor_con ); 
                    ing.setVlr_tasa(tasa.getValor_tasa());
                    ing.setFecha_tasa(tasa.getFecha());
                }
                else{
                    sw = false;
                }
                if(sw){
                    System.out.println(" cuentas "+cban);
                    if( ( request.getParameter("tipodoc").equals("ICR") && abc.equals("") ) ||
                    ( request.getParameter("tipodoc").equals("ICR") && model.tblgensvc.existeRegistro("ABC", abc) && !abc.equals("") ) ||
                    !request.getParameter("tipodoc").equals("ICR") ){
                        if( !request.getParameter("tipodoc").equals("ICR") && !request.getParameter("tipodoc").equals("ICA") ){
                            Banco ban = model.servicioBanco.obtenerBanco( ing.getDstrct(), ing.getBranch_code(), ing.getBank_account_no());
                            if( ban.getMoneda().equals( ing.getCodmoneda() ) ){
                                model.ingresoService.modificarIngreso(ing);
                                model.ingresoService.buscarIngreso( usuario.getDstrct(), request.getParameter("tipodoc"), request.getParameter("num") );
                                next+="?msg=ok&sw=ok";
                                if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), ing.getTipo_documento(), ing.getNum_ingreso() )  )
                                    next+="&modificar=true";
                                else
                                    next+="&modificar="+null;
                            }
                             else{
                                request.setAttribute("mensaje","La moneda "+ ing.getCodmoneda()+" no pertenece al banco "+ing.getBranch_code()+" "+ing.getBank_account_no());
                                if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), ing.getTipo_documento(), ing.getNum_ingreso() )  )
                                    next+="?modificar=true&cuentas="+cban;
                                else
                                    next+="?modificar="+null+"&cuentas="+cban;
                            }
                        }
                        else{
                            model.ingresoService.modificarIngreso(ing);
                            model.ingresoService.buscarIngreso( usuario.getDstrct(), request.getParameter("tipodoc"), request.getParameter("num") );
                            next+="?msg=ok&sw=ok";
                            if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), ing.getTipo_documento(), ing.getNum_ingreso() )  )
                                next+="&modificar=true&cuentas="+cban;
                            else
                                next+="&modificar="+null+"&cuentas="+cban;
                        }
                    }
                    else{
                        request.setAttribute("mensaje","No existe el codigo ABC "+abc+" en la tabla");
                        if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), ing.getTipo_documento(), ing.getNum_ingreso() )  )
                            next+="?modificar=true&cuentas="+cban;
                        else
                            next+="?modificar="+null+"&cuentas="+cban;
                    }
                    
                }
                else{
                    next+="?msg=error&modificar=true&sw=";
                }
            }
            this.dispatchRequest(next);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
