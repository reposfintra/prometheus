
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;

/**
 *
 * @author kreales
 */
public class PlanillaExtrafleteAction extends Action{
    /** Creates a new instance of BuscarNumPlanillaAction */
    static Logger logger = Logger.getLogger(PlanillaExtrafleteAction.class);
    
    public PlanillaExtrafleteAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String planilla= request.getParameter("planilla").toUpperCase();
        String cmd = request.getParameter("cmd")!=null?request.getParameter("cmd"):"";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String moneda_cia = (String) session.getAttribute("Moneda");

        try{
            if(cmd.equals("Buscar")){
                
                model.planillaService.listaPlanillas(planilla);
                
                Vector plas = model.planillaService.getPlas();
                logger.info("Cantidad encontrada... "+plas.size());
                
                if(plas.size()==1 || request.getParameter("remesa")!=null){
                    
                    logger.info("Tengo una remesa... "+request.getParameter("remesa"));
                    
                    Planilla p = (Planilla) plas.elementAt(0);
                    
                    String numrem = request.getParameter("remesa")!=null?request.getParameter("remesa"):p.getNumrem();
                    
                    logger.info("La remesa es... "+numrem);
                    
                    try{
                        
                        model.PlanillasSvc.searchOC(usuario.getDstrct(), planilla, usuario.getBase(), numrem);
                        
                    } catch(Exception ex){
                        ex.printStackTrace();
                        throw new ServletException(ex.getMessage());
                    }
                    Planillas  pla = model.PlanillasSvc.getPlanilla() ;
                    
                    if(pla!=null){
                        
                        String sj = pla.getSJ();
                        
                        model.stdjobdetselService.searchStdJob(sj);
                        model.sjextrafleteService.setVlrtotalCR(0);
                        model.sjextrafleteService.setVlrtotalE(0);
                        model.sjextrafleteService.listaExtraFletesAplicados(p.getNumpla(),numrem,sj,usuario.getId_agencia());
                        model.sjextrafleteService.listaCostosAplicados(p.getNumpla(),numrem,sj,usuario.getId_agencia());
                        
                        model.tblgensvc.buscarListaCodigo("AUTEXFLETE", "000"+sj.substring(0,3));
                        next="/colpapel/agregarExtrafletes.jsp?planilla="+p.getNumpla()+"&remesa="+p.getNumrem();
                    }
                    else{
                        next="/colpapel/InicioExtraflete.jsp?mensaje=No se encontro la planilla "+planilla;
                    }
                    
                    
                }
                else if(plas.size()>1){
                    next="/colpapel/InicioExtraflete.jsp?lista=ok";
                }
                else if(plas.size()==0){
                    next="/colpapel/InicioExtraflete.jsp?mensaje=No se encontro la planilla "+planilla;
                }
            }
            else if(cmd.equals("Aplicar")){
                
                Planillas  pla = model.PlanillasSvc.getPlanilla() ;
                String numpla=request.getParameter("planilla");
                String numre = request.getParameter("remesa");
                
                next="/colpapel/agregarExtrafletes.jsp?mensaje=Se ha aplicado con exito los extrafletes a la planilla "+planilla;
                Vector efletes= model.sjextrafleteService.getFletes();
                Vector efletesN = new Vector();
                for(int i=0; i<efletes.size();i++){
                    SJExtraflete sj = (SJExtraflete)efletes.elementAt(i);
                    if(request.getParameter("chek"+sj.getCod_extraflete())!=null){
                        logger.info("extraflete: "+sj.getCod_extraflete());
                        sj.setSelec(true);
                        String cod = sj.getCod_extraflete();
                        sj.setAprobador(request.getParameter("aprobador"+cod));
                        if(request.getParameter("cant"+cod+"o")!=null){
                            logger.info("CANTIDAD: "+request.getParameter("cant"+cod+"o"));
                            sj.setCantidad(Float.parseFloat(request.getParameter("cant"+cod+"o")));
                        }
                        if(request.getParameter("liq"+cod+"o")!=null){
                            logger.info("VALOR LIQUIDADO: "+request.getParameter("liq"+cod+"o"));
                            sj.setVlrtotal(Float.parseFloat(request.getParameter("liq"+cod+"o")));
                        }
                        
                        if(request.getParameter("ingreso"+cod+"o")!=null){
                            logger.info("VALOR INGRESO: "+request.getParameter("ingreso"+cod+"o"));
                            sj.setValor_ingreso(Float.parseFloat(request.getParameter("ingreso"+cod+"o")));

                        }
                        if(request.getParameter("liqIng"+cod+"o")!=null){
                            logger.info("VALOR TOTAL INGRESO: "+request.getParameter("liqIng"+cod+"o"));
                            sj.setVlrtotalIng(Float.parseFloat(request.getParameter("liqIng"+cod+"o")));
                            
                        }
                        if(request.getParameter("cost"+cod+"o")!=null){
                            logger.info("VALOR COSTO: "+request.getParameter("cost"+cod+"o"));
                            sj.setValor_costo(Float.parseFloat(request.getParameter("cost"+cod+"o")));
                            
                        }
                    }
                    else{
                        sj.setSelec(false);
                    }
                    efletesN.add(sj);
                }
                model.sjextrafleteService.setFletes(efletesN);
                Date d =new Date();
                java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
                String fecha = s.format(d);
                Vector consultas = new Vector();
                efletes= model.sjextrafleteService.getFletes();
                String nit =pla.getCedulaPropietario();
                String placa =pla.getPlaca();
                if(efletes!=null){
                    
                    for(int i=0; i<efletes.size();i++){
                        SJExtraflete sjE = (SJExtraflete)efletes.elementAt(i);
                        model.movplaService.buscarValorConcepto(numpla,sjE.getCod_extraflete());
                        logger.info("EXTRAFLETE ENCONTRADO "+sjE.getCod_extraflete());
                        sjE.setNumpla(numpla);
                        sjE.setNumrem(numre);
                        sjE.setFecdsp("now()");
                        sjE.setDistrito(usuario.getDstrct());
                        sjE.setProveedor("");
                        model.sjextrafleteService.setSj(sjE);
                        
                        Movpla movpla= model.movplaService.getMovPla();
                        if(model.movplaService.getMovPla()!=null){
                            if(movpla.getVlr()>0){
                                movpla.setAgency_id(usuario.getId_agencia());
                                movpla.setCurrency(sjE.getMoneda_costo());
                                movpla.setDstrct(usuario.getDstrct());
                                movpla.setPla_owner(nit);
                                movpla.setPlanilla(numpla);
                                movpla.setSupplier(placa);
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(sjE.getCod_extraflete());
                                movpla.setAp_ind("S");
                                movpla.setProveedor("");
                                movpla.setSucursal("");
                                movpla.setCreation_user(usuario.getLogin());
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
                                
                                logger.info("SE ENCONTRO UN MOVPLA CON ESTE EXTRAFLETE CON VALOR "+movpla.getVlr());
                                logger.info("EL VALOR DEL EXTRAFLETE ES DE  "+sjE.getVlrtotal());
                                
                                
                                if(sjE.isSelec()){
                                    if(movpla.getVlr()!=sjE.getVlrtotal()){
                                        //movpla.setVlr((movpla.getVlr()-sjE.getVlrtotal())*-1);
                                        movpla.setVlr_ml((movpla.getVlr()-sjE.getVlrtotal())*-1);
                                        movpla.setCantidad(sjE.getCantidad());
                                        
                                        //*************** AMATURANA 15.12.2006
                                        
                                        //movpla.setVlr_for(movpla.getVlr());
                                        movpla.setVlr_me(movpla.getVlr_ml());
                                        logger.info("HOY: " + com.tsp.util.UtilFinanzas.getHoy("-"));
                                        
                                        if( !movpla.getCurrency().equals(moneda_cia) ){
                                            Tasa obj =  model.tasaService.buscarValorTasa(moneda_cia, movpla.getCurrency(), moneda_cia, com.tsp.util.UtilFinanzas.getHoy("-"));
                                            
                                            if (obj != null){
                                                double valor_tasa = obj.getValor_tasa();
                                                //movpla.setVlr( movpla.getVlr()*valor_tasa );
                                                movpla.setVlr_ml(movpla.getVlr_ml()*valor_tasa);
                                            } else {
                                                 next="/colpapel/agregarExtrafletes.jsp?mensaje=No se ha aplicado con exito los extrafletes a la planilla " +
                                                    ""+planilla+". No se ha definido la tasa de conversion de " + movpla.getCurrency() + " a " + moneda_cia;
                                                 break;
                                            }
                                        }
                                        
                                        //************************************
                                        
                                        
                                        //consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                        consultas.add(model.movplaService.insertMovPlaDouble(movpla,usuario.getBase()));
                                        consultas.add(model.sjextrafleteService.ActualizarAplicados());
                                    }
                                }
                                else{
                                    //movpla.setVlr(sjE.getVlrtotal()*-1);
                                    movpla.setVlr_ml(sjE.getVlrtotal()*-1);
                                    movpla.setCantidad(sjE.getCantidad());
                                    
                                    //*************** AMATURANA 15.12.2006
                                    
                                    //movpla.setVlr_for(movpla.getVlr());
                                    movpla.setVlr_me(movpla.getVlr_me());
                                    logger.info("HOY: " + com.tsp.util.UtilFinanzas.getHoy("-"));
                                    
                                    
                                    
                                    if( !movpla.getCurrency().equals(moneda_cia) ){
                                        Tasa obj =  model.tasaService.buscarValorTasa(moneda_cia, movpla.getCurrency(), moneda_cia, com.tsp.util.UtilFinanzas.getHoy("-"));
                                        
                                        if (obj != null){
                                            double valor_tasa = obj.getValor_tasa();
                                            //movpla.setVlr( movpla.getVlr()*valor_tasa );
                                            movpla.setVlr_ml(movpla.getVlr_ml()*valor_tasa);
                                        } else {
                                            next="/colpapel/agregarExtrafletes.jsp?mensaje=No se ha aplicado con exito los extrafletes a la planilla " +
                                            ""+planilla+". No se ha definido la tasa de conversion de " + movpla.getCurrency() + " a " + moneda_cia;
                                            break;
                                        }
                                    }
                                    
                                    //************************************
                                    //consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                    consultas.add(model.movplaService.insertMovPlaDouble(movpla,usuario.getBase()));
                                    consultas.add(model.sjextrafleteService.anularCosto());
                                }
                            }
                            else if(sjE.isSelec()){
                                
                                movpla= new Movpla();
                                movpla.setAgency_id(usuario.getId_agencia());
                                movpla.setCurrency(sjE.getMoneda_costo());
                                movpla.setDstrct(usuario.getDstrct());
                                movpla.setPla_owner(nit);
                                movpla.setPlanilla(numpla);
                                movpla.setSupplier(placa);
                                //movpla.setVlr(sjE.getVlrtotal());
                                movpla.setVlr_ml(sjE.getVlrtotal());
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(sjE.getCod_extraflete());
                                movpla.setAp_ind("N");
                                movpla.setProveedor("");
                                movpla.setSucursal("");
                                movpla.setCreation_user(usuario.getLogin());
                                movpla.setCantidad(sjE.getCantidad());
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
                                
                                //*************** AMATURANA 15.12.2006
                                
                                //movpla.setVlr_for(movpla.getVlr());
                                movpla.setVlr_me(movpla.getVlr_ml());
                                logger.info("HOY: " + com.tsp.util.UtilFinanzas.getHoy("-"));
                                
                                
                                
                                if( !movpla.getCurrency().equals(moneda_cia) ){
                                    Tasa obj =  model.tasaService.buscarValorTasa(moneda_cia, movpla.getCurrency(), moneda_cia, com.tsp.util.UtilFinanzas.getHoy("-"));
                                    
                                    if (obj != null){
                                        double valor_tasa = obj.getValor_tasa();
                                        //movpla.setVlr( movpla.getVlr()*valor_tasa );
                                        movpla.setVlr_ml(movpla.getVlr_ml()*valor_tasa);
                                    } else {
                                        next="/colpapel/agregarExtrafletes.jsp?mensaje=No se ha aplicado con exito los extrafletes a la planilla " +
                                        ""+planilla+". No se ha definido la tasa de conversion de " + movpla.getCurrency() + " a " + moneda_cia;
                                        break;
                                    }
                                }
                                
                                //************************************
                                //consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                consultas.add(model.movplaService.insertMovPlaDouble(movpla,usuario.getBase()));
                                consultas.add(model.sjextrafleteService.insertarCostoReembolsable());
                            }
                            
                        }
                        else{
                            if(sjE.isSelec()){
                                
                                movpla= new Movpla();
                                movpla.setAgency_id(usuario.getId_agencia());
                                movpla.setCurrency(sjE.getMoneda_costo());
                                movpla.setDstrct(usuario.getDstrct());
                                movpla.setPla_owner(nit);
                                movpla.setPlanilla(numpla);
                                movpla.setSupplier(placa);
                                //movpla.setVlr(sjE.getVlrtotal());
                                movpla.setVlr_ml(sjE.getVlrtotal());
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(sjE.getCod_extraflete());
                                movpla.setAp_ind("N");
                                movpla.setProveedor("");
                                movpla.setSucursal("");
                                movpla.setCreation_user(usuario.getLogin());
                                movpla.setCantidad(sjE.getCantidad());
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
                                
                                //*************** AMATURANA 15.12.2006
                                
                                //movpla.setVlr_for(movpla.getVlr());
                                movpla.setVlr_me(movpla.getVlr_ml());
                                logger.info("HOY: " + com.tsp.util.UtilFinanzas.getHoy("-"));
                                
                                
                                if( !movpla.getCurrency().equals(moneda_cia) ){
                                    Tasa obj =  model.tasaService.buscarValorTasa(moneda_cia, movpla.getCurrency(), moneda_cia, com.tsp.util.UtilFinanzas.getHoy("-"));
                                    
                                    if (obj != null){
                                        double valor_tasa = obj.getValor_tasa();
                                        //movpla.setVlr( movpla.getVlr()*valor_tasa );
                                        movpla.setVlr_ml(movpla.getVlr_ml()*valor_tasa);
                                    } else {
                                        next="/colpapel/agregarExtrafletes.jsp?mensaje=No se ha aplicado con exito los extrafletes a la planilla " +
                                        ""+planilla+". No se ha definido la tasa de conversion de " + movpla.getCurrency() + " a " + moneda_cia;
                                        break;
                                    }
                                }
                                
                                //************************************
                                //consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                consultas.add(model.movplaService.insertMovPlaDouble(movpla,usuario.getBase()));
                                consultas.add(model.sjextrafleteService.insertarCostoReembolsable());
                            }
                        }
                        
                    }
                }
                model.despachoService.insertar(consultas);
            }
            if(request.getParameter("pag")!=null){
                next="/colpapel/InicioExtraflete.jsp?lista=ok";
            }
            
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
    }
}
