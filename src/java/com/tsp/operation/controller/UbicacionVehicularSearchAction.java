/*******************************************************************
 * Nombre clase: UbicacionVehicularSearchAction.java
 * Descripci�n: Accion para anular un acuerdo especial a la bd.
 * Autor: Ing. Ivan Gomez
 * Fecha: 24 de marzo de 2004, 04:42 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;

import org.apache.log4j.Logger;

/**
 *
 * @author  iherazo
 */
public class UbicacionVehicularSearchAction extends Action
{
  static Logger logger = Logger.getLogger(UbicacionVehicularSearchAction.class);
   /**
   * Ejecuta la accion.
   * @throws ServletException Si ocurre un error durante el procesamiento
   *         de la accion.
   * @throws IOException Si no se puede abrir la vista solicitada por esta
   *         accion, la cual corresponde a una p�gina jsp.
   */
  public void run() throws ServletException
  {
    String next = null;
    // Perform search
    String [] args = new String[5];
    args[0] = request.getParameter("cliente");
    if( args[0] == null )
      next = "/jsp/sot/reports/FiltroUbicacionVehicular.jsp";
    else{
      args[1] = request.getParameter("fechaini");
      args[1] = (args[1] == null ? "" : args[1]);
      args[2] = request.getParameter("fechafin");
      args[2] = (args[2] == null ? "" : args[2]);
      args[3] = request.getParameter("listaTipoViaje");
      args[4] = request.getParameter("estadoViajes");      
      try
      {
        HttpSession session = request.getSession();
        Usuario loggedUser = (Usuario)session.getAttribute("Usuario");
        model.UbicacionVehicularClientesSvc.UbicacionVehicularSearch( args, loggedUser ); 

        logger.info(  loggedUser.getNombre() + " ejecuto Ubicacion Vehicular con: Cliente " + args[0] 
                       + " Fecha Inicial: " + args[1] + " Fecha Final: " + args[2] + " Tipo Viaje: " + args[3] );  
        
      }catch (SQLException e){
          e.printStackTrace();
       // throw new ServletException(e.getMessage());
      }
      next = "/jsp/sot/reports/ReporteUbicacionVehicular.jsp?cliente="+args[0];
    }
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }  
}
