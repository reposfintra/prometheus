/*
 * CuentaContableVerificaAction.java
 *
 * Created on 9 de febrero de 2007, 09:34 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;



public class CuentaContableVerificarAction extends Action {
    
    /** Creates a new instance of CuentaContableVerificaAction */
    public CuentaContableVerificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/jsp/cxcobrar/ingresoMiscelaneo/auxiliar.jsp";
        String evento = request.getParameter("evento");
        String campo = request.getParameter("campo");
        int modulo = Integer.parseInt(request.getParameter("modulo"));
        String cuenta =  (request.getParameter("cuenta") != null) ?request.getParameter("cuenta").toUpperCase():"";
        String mensaje = "";
        String mon_local = "";
        boolean sw = false;
        try{
            if(evento.equals("Cuenta")){
                com.tsp.finanzas.contab.model.beans.PlanDeCuentas plan_cuenta = modelcontab.planDeCuentasService.consultaCuentaModulo(usuario.getDstrct(),cuenta,modulo);
                if(plan_cuenta!=null){
                    if(plan_cuenta.getDetalle().equalsIgnoreCase("S") &&  plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S") ){
                        if(plan_cuenta.getSubledger().equalsIgnoreCase("S"))
                            modelcontab.subledgerService.buscarCuentasTipoSubledger(cuenta);
                        else
                            modelcontab.subledgerService.setCuentastsubledger(null);
                        next+="?opcion=Cuenta&campo="+campo+"&tipo="+request.getParameter("tipo");
                    }
                    else{
                         modelcontab.subledgerService.setCuentastsubledger(null);
                        if(!plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S")){
                            mensaje+=" no pertence a este modulo";
                        }
                        if(!plan_cuenta.getDetalle().equalsIgnoreCase("S")){
                            mensaje+=" no es de detalle";
                        }
                        if(!plan_cuenta.getSubledger().equalsIgnoreCase("S")){
                            mensaje+=" no requiere subledger";
                        }
                        next+="?opcion=Cuenta&mensaje=La cuenta"+mensaje;
                    }
                }
                else{
                    modelcontab.subledgerService.setCuentastsubledger(null);
                    next+="?opcion=Cuenta&campo="+campo+"&mensaje=No existe la Cuenta";
                }
            }
            else if(evento.equals("Auxiliar")){
                String tip = request.getParameter("tipo");
                String idsubledger = request.getParameter("auxiliar");
                modelcontab.subledgerService.setCuentastsubledger(null);
                if(!modelcontab.subledgerService.existeSubledger(usuario.getDstrct(), cuenta, tip, idsubledger)){
                    next+="?opcion=Auxiliar&mensaje=El Auxiliar no existe";
                }
            }
           System.out.println(next); 
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
