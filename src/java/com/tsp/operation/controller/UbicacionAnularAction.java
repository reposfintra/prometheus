/*
 * Nombre        MenuCargaxPerfilAction.java
 * Autor         Ing Jesus Cuestas
 * Fecha         30 de enero de 2006, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UbicacionAnularAction extends Action{
    
    /** Creates a new instance of UbicacionAnularAction */
    public UbicacionAnularAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");        
        try{
            String codigo = request.getParameter("c_codigo");            
            String cia = request.getParameter("c_distrito");
            
            Ubicacion u = new Ubicacion();
            u.setCod_ubicacion(codigo);           
            u.setDstrct(cia);
            u.setUser_update(usuario.getLogin());
            
            model.ubService.setUb(u);
            model.ubService.anularUbicacion();
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);        
    }    
}