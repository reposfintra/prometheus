/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ContabilidadGeneralDAO;
import com.tsp.operation.model.DAOS.impl.ContablilidadGeneralImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.ParametrosBeans;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.ExcelApiUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author egonzalez
 */
public class ContabilidadGeneralAction extends Action {

    final int BUSCAR_FACTURAS_INDEMNIZACION = 1;
    final int BUSCAR_FACTURAS_DESISTIMIENTO = 2;
    final int INDEMNIZAR_FACTURAS = 3;
    final int CARGAR_CARTERA_EN = 4;
    final int DESISTIR_FACTURAS = 5;
    final int EXPORTAR_EXCEL = 6;
    final int EXPORTAR_FACTURAS_INDEMNIZADAS = 7;
    final int CARTERA_ENCUSTODIA_DE = 8;
    final int BUSCAR_FACTURAS_ENDOSAR = 9;
    final int UNIDAD_NEGOCIO = 10;
    final int ENDOSAR_AFIDUCIA = 11;
    final int ENDOSAR_FACTURAS = 12;
    final int EXPORTAR_EXCEL_ENDOSO = 13;
    final int CARGAR_TIPOS_DIFERIDOS = 14;
    final int BUSCAR_DIFERIDOS = 15;
    final int ADELANTAR_DIFERIDOS = 16;
    final int ANULAR_DIFERIDOS = 17;
    final int BUSCAR_INGRESO = 18;
    final int ANULAR_INGRESO = 19;
    final int MONTAR_ARCHIVO_ENDOSO_EDU = 20;

    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";
    ContabilidadGeneralDAO dao;
    String unidad_negocio = "";

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            int opcion = Integer.parseInt(request.getParameter("opcion"));
            unidad_negocio = request.getParameter("linea") != null ? request.getParameter("linea") : "";
            dao = new ContablilidadGeneralImpl(usuario.getBd());

            switch (opcion) {
                case BUSCAR_FACTURAS_INDEMNIZACION:
                    buscarFacturasIndemnizacion();
                    break;
                case BUSCAR_FACTURAS_DESISTIMIENTO:
                    buscarFacturasDesistimiento();
                    break;
                case INDEMNIZAR_FACTURAS:
                    indemnizarFacturas();
                    break;
                case CARGAR_CARTERA_EN:
                    cargarCarteraen();
                    break;
                case DESISTIR_FACTURAS:
                    desistirFacturas();
                    break;
                case EXPORTAR_EXCEL:
                    exportarExcel();
                    break;
                case EXPORTAR_FACTURAS_INDEMNIZADAS:
                    verFacturasIndemnizadas();
                    break;
                case CARTERA_ENCUSTODIA_DE:
                    cargarCustodiaCartera();
                    break;
                case BUSCAR_FACTURAS_ENDOSAR:
                    buscarFacturasEndosar();
                    break;
                case UNIDAD_NEGOCIO:
                    cargarUnidadNegocio();
                    break;
                case ENDOSAR_AFIDUCIA:
                    cargarEndosarAfiducia();
                    break;
                case ENDOSAR_FACTURAS:
                    endosarFacturas();
                    break;
                case EXPORTAR_EXCEL_ENDOSO:
                    exportarExcelEndosados();
                    break;
                case CARGAR_TIPOS_DIFERIDOS:
                    cargarTipoDiferidos();
                    break;
                case BUSCAR_DIFERIDOS:
                    buscarDiferidos();
                    break;
                case ADELANTAR_DIFERIDOS:
                    adelantarDiferidos();
                    break;
                case ANULAR_DIFERIDOS:
                    anularDiferidos();
                    break;
                case BUSCAR_INGRESO:
                    buscarIngreso();
                    break;
                case ANULAR_INGRESO:
                    anularIngreso();
                    break;
                case MONTAR_ARCHIVO_ENDOSO_EDU:
                    MontarArchivoEndosoEdu();
                    break;
                default:
                    this.printlnResponseAjax("{\"error\":\"Peticion invalida 404 recurso no encontrado\"}", "application/json;");
                    break;

            }

        } catch (Exception ex) {
            reponseJson = "{\"error\":\"Algo salio mal al procesar su solicitud\",\"exception\":\"" + ex.getMessage() + "\"}";
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }

    private void buscarFacturasIndemnizacion() {

        String mora = (request.getParameter("mora") != null && !request.getParameter("mora").equals("")) ? request.getParameter("mora") : "90";
        String lineaNegocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
        String foto = request.getParameter("foto") != null ? request.getParameter("foto") : "";
        String cartera_en = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";
        String fecha_corte = request.getParameter("fecha_corte") != null ? request.getParameter("fecha_corte") : "";

        ParametrosBeans beans = new ParametrosBeans();
        beans.setStringParameter1(mora);
        beans.setStringParameter2(lineaNegocio);
        beans.setStringParameter3(foto);
        beans.setStringParameter4(cartera_en);
        beans.setStringParameter5(fecha_corte);

        this.reponseJson = dao.getSelectDataBaseJson("BUSCAR_FACTURAS_INDEMNIZACION", beans, usuario);

    }

    private void buscarFacturasDesistimiento() {

        String lineaNegocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
        String cartera_en = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";

        ParametrosBeans beans = new ParametrosBeans();
        beans.setStringParameter1(lineaNegocio);
        beans.setStringParameter2(cartera_en);

        this.reponseJson = dao.getSelectDataBaseJson("FACTURAS_POR_DESISTIR", beans, usuario);

    }

    private void cargarCarteraen() {
        this.reponseJson = dao.carteraEn(usuario);
    }

    private void cargarCustodiaCartera() {
        this.reponseJson = dao.CustodiaCartera(usuario);
    }

    private void cargarEndosarAfiducia() {
        String fiducia_actual = request.getParameter("FiduciaActual") != null ? request.getParameter("FiduciaActual") : "";
        this.reponseJson = dao.EndosarAfiducia(fiducia_actual, usuario);
    }

    private void cargarUnidadNegocio() {
        //castro
        this.reponseJson = dao.UnidadNegocio(unidad_negocio, usuario);
    }

    private void buscarFacturasEndosar() {

        String lineaNegocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
        String UnidadNegocio = request.getParameter("unds_negocio") != null ? request.getParameter("unds_negocio") : "";
        String cartera_en = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";
        String cedula_cliente = request.getParameter("cedula_cliente") != null ? request.getParameter("cedula_cliente") : "";
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String checkEstadoSaldo = request.getParameter("checkSaldo") == "on" ? request.getParameter("checkSaldo") : "off";

        ParametrosBeans beans = new ParametrosBeans();
        beans.setStringParameter1(lineaNegocio);
        beans.setStringParameter2(UnidadNegocio);
        beans.setStringParameter3(cartera_en); //
        beans.setStringParameter4(cedula_cliente); //
        beans.setStringParameter5(negocio); //
        beans.setStringParameter6(checkEstadoSaldo);

        this.reponseJson = dao.getSelectDataBaseJson("BUSCAR_FACTURAS_ENDOSAR", beans, usuario);

    }

    private synchronized void endosarFacturas() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);

            String lineaNegocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
            String UnidadNegocio = request.getParameter("unds_negocio") != null ? request.getParameter("unds_negocio") : "";
            String cartera_en = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";
            String endosar_a = request.getParameter("endosar_a") != null ? request.getParameter("endosar_a") : "";
            String cedula_cliente = request.getParameter("cedula_cliente") != null ? request.getParameter("cedula_cliente") : "";
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";

            String checkEstadoSaldo = request.getParameter("checkSaldo") == "on" ? request.getParameter("checkSaldo") : "off";
            String EstadoEndoso = "";

            String LoteEndoso = dao.ValidarOneValue(usuario);
            //String lote_number = (String) JsonObject.get("lote_number");

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();

            TransaccionService tserviceUpdt = new TransaccionService(usuario.getBd());
            tserviceUpdt.crearStatement();

            for (int i = 0; i < asJsonArray.size(); i++) {

                JsonObject objects = (JsonObject) asJsonArray.get(i);
                tserviceUpdt.getSt().addBatch(dao.getUpdateDataGenericJson("ACTUALIZAR_FACTURAS_ENDOSADAS", objects, usuario));

                //if ( endosar_a.equals("8020220161") ) endosar_a = ""; 
                String EndosarEnMod = endosar_a.equals("8020220161") ? EndosarEnMod = "" : endosar_a;

                tservice.getSt().addBatch(dao.getInsertDataGenericJson("GUARDAR_FACTURAS_ENDOSADAS", LoteEndoso, lineaNegocio, UnidadNegocio, cartera_en, EndosarEnMod, checkEstadoSaldo, EstadoEndoso, objects, usuario));
            }

            tserviceUpdt.execute();
            tserviceUpdt.closeAll();

            tservice.execute();
            tservice.closeAll();

            //Ejecutamos el Comprobante Diario de Endoso.
            this.reponseJson = "{\"respuesta\":\"" + dao.crearComprobanteDiarioEndoso(usuario) + "\"}";
            //this.reponseJson = "{\"respuesta\":\"Se endosaron satisfactoriamente las facturas.\"}";

        } catch (SQLException ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la inforamcion.\"}";
            ex.printStackTrace();
            Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la inforamcion.\"}";
            ex.printStackTrace();
            Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void indemnizarFacturas() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                tservice.getSt().addBatch(dao.getInsertDataBaseJson("GUARDAR_FACTURAS", objects, usuario));
            }
            tservice.execute();
            tservice.closeAll();

            //ejecutamos el proceso para crear el comprobante diario.            
            this.reponseJson = "{\"respuesta\":\"" + dao.crearComprobanteDiario(usuario) + "\"}";

        } catch (SQLException ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la inforamcion.\"}";
            ex.printStackTrace();
            Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la inforamcion.\"}";
            ex.printStackTrace();
            Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private synchronized void desistirFacturas() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                tservice.getSt().addBatch(dao.getInsertFacturaXdesistir("SQL_GUARDAR_FACTURAS_DESISTIDAS", objects, usuario));
            }
            tservice.execute();
            tservice.closeAll();

            //ejecutamos el proceso para crear el comprobante diario desistimiento.            
            this.reponseJson = "{\"respuesta\":\"" + dao.crearComprobanteDiarioDesistimiento(usuario) + "\"}";

        } catch (SQLException ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la inforamcion.\"}";
            ex.printStackTrace();
            Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la inforamcion.\"}";
            ex.printStackTrace();
            Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void exportarExcel() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_factura", titulo = "REPORTE FACTURAS";

        cabecera = new String[]{"Cod cliente", "Identificacion", "Nombre", "Foto", "Negocio", "# Factura", "Cuota", "Fecha vencimiento",
            "Dias mora", "Vlr factura", "Vlr indemnizado", "Vlr a desistir", "Fecha Indemnizacion", "# Aval", "Convenio",
            "Cuenta", "Linea Neg", "Cartera en"};

        dimensiones = new short[]{
            3000, 5000, 7000, 3000, 3000, 3000, 3000, 5000, 3000, 5000, 5000, 5000, 5000, 3000, 3000, 5000, 3000, 3000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        typeResponse = "text/plain";
    }

    private void verFacturasIndemnizadas() throws Exception {
        ParametrosBeans beans = new ParametrosBeans();
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.getSelectDataBaseJson("VER_FACTURAS_INDENIZADAS", beans, usuario));

        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_factura_indemnizadas", titulo = "REPORTE FACTURAS";

        cabecera = new String[]{"Foto", "Cod cliente", "Identificacion", "Nombre cliente", "Negocio", "# Factura", "Cuota",
            "Vlr factura", "Vlr indemnizado", "Fecha vencimiento", "Fecha indemnizacion", "# aval",
            "Comprobante indemnizacion", "Desistido", "Fecha desistimiento", "Vlr desistido",
            "Ultimos comprobante desistimiento", "# desistimientos", "Linea negocio"};

        dimensiones = new short[]{
            3000, 3000, 5000, 9000, 5000, 3000, 3000, 3000, 5000, 5000, 7000, 3000, 7000, 3000, 5000, 5000, 9000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        typeResponse = "text/plain";

    }

    private void exportarExcelEndosados() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_factura", titulo = "REPORTE FACTURAS ENDOSADAS";

        cabecera = new String[]{"Cartera En", "IdUnidad Negocio", "Unidad Negocio", "Identificacion", "Nombre", "codigo cliente", "Negocio",
            "Tipo Negocio", "# Factura", "Cuota", "Fecha vencimiento", "Dias mora", "Vlr factura", "Vlr Abono", "Vlr Saldo"};

        dimensiones = new short[]{3000, 5000, 7000, 3000, 3000, 3000, 3000, 5000, 3000, 5000, 5000, 5000, 5000, 3000, 3000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        typeResponse = "text/plain";
    }

    private void cargarTipoDiferidos() throws Exception {
        reponseJson = dao.cargarTipoDiferidos();
    }

    private void buscarDiferidos() throws Exception {
        String tipo = request.getParameter("tipodif");
        String codigo = request.getParameter("negocio");
        reponseJson = dao.buscarDiferidos(codigo, tipo);
    }

    private void adelantarDiferidos() throws Exception {
        String codigo = request.getParameter("negocio");
        String[] diferidos = request.getParameterValues("diferidos[]");
        reponseJson = dao.adelantarDiferidos(codigo, diferidos, usuario);
    }

    private void anularDiferidos() throws Exception {
        String codigo = request.getParameter("negocio");
        String[] diferidos = request.getParameterValues("diferidos[]");
        reponseJson = dao.anularDiferidos(codigo, diferidos, usuario);
    }

    private void buscarIngreso() throws Exception {
        String idIngreso = request.getParameter("idIngreso");
        reponseJson = dao.buscarIngreso(idIngreso);
    }

    private void anularIngreso() {
        String codigo = request.getParameter("idIngreso");
        reponseJson = dao.anularIngreso(codigo, usuario.getLogin());
    }
    
    private void MontarArchivoEndosoEdu() throws Exception {
        JsonObject json = new JsonObject();
        
        try {

            ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
            List fileItemsList = servletFileUpload.parseRequest(request);

            Iterator it = fileItemsList.iterator();

            if (it.hasNext()) {

                FileItem fileItem = (FileItem) it.next();

                if (!fileItem.isFormField()) {
                    if (dao.leerArchivoEndosoEdu(fileItem.getInputStream())) {
                        json.addProperty("respuesta", "Se cargo el archivo correctamente.");
                    } else {
                        json.addProperty("error", "No se pudo cargar el archivo. Comun�quese con soporte.");
                    }
                }
            } else {
                json.addProperty("error", "Debe cargar un archivo.");
            }
        } catch (IOException e) {
            System.err.println(e.getCause());
            System.err.println("Error cargando el archivo: " + e.getMessage());
            json.addProperty("error", "Error leyendo el archivo.");
        } catch (SQLException e) {
            System.err.println(e.getCause());
            System.err.println("Error generando el archivo de Pagos en: " + e.getLocalizedMessage() + " -- " + e.getMessage());
            json.addProperty("error", "Error en las consultas a la base de datos.");
        }        
        this.printlnResponseAjax(json.toString(), "application/json");
    }
}
