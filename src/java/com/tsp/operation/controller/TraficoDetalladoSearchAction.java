/*
 * TraficoSearchAction.java
 *
 * Created on 24 de marzo de 2004, 04:42 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import java.util.*;
import com.tsp.operation.model.threads.HExportarExcel_RepDetalladoTrafico;
import com.tsp.operation.model.DAOS.*;

import org.apache.log4j.Logger;

/**
 *
 * @author  Nestor Parejo
 */
public class TraficoDetalladoSearchAction extends Action
{
  static Logger logger = Logger.getLogger(TraficoDetalladoSearchAction.class);
   /**
   * Ejecuta la accion.
   * @throws ServletException Si ocurre un error durante el procesamiento
   *         de la accion.
   * @throws IOException Si no se puede abrir la vista solicitada por esta
   *         accion, la cual corresponde a una p�gina jsp.
   */
  public void run() throws ServletException, InformationException
  {
    String next = null;
    // Perform search
    String numpla  = request.getParameter("planillas");
    numpla = (numpla == null ? "" : numpla);
    
    if( numpla == null )
      next = "/jsp/sot/reports/FiltroTrafico.jsp";
    else{
      
      try
      {          
        HttpSession session = request.getSession();
        Usuario loggedUser = (Usuario)session.getAttribute("Usuario");
        String view = request.getParameter("displayInExcel");
        next = "/jsp/sot/reports/ReporteTraficoDetallado.jsp";   
        if(view!=null && view.equals("File")){
             next = "/jsp/sot/reports/FiltroUbicaEquipoRuta.jsp?comentario=Su proceso de Exportaci�n del Informe Detallado de Tr�fico para la OC No "+ numpla +", se ha iniciado... Puede consultar en [ Manejo de Archivos ]"; 
             HExportarExcel_RepDetalladoTrafico  hilo = new HExportarExcel_RepDetalladoTrafico();
             hilo.start(loggedUser, numpla);
        }
        else{        
            model.traficoService.getReporteDetalladoTraf(numpla);
            
        }
        logger.info(  loggedUser.getNombre() + " ejecuto Consulta de Trafico detallado: Planilla " + numpla 
                        );  
        
      }catch (Exception e){
          e.printStackTrace();
       // throw new ServletException(e.getMessage());
      }
      
      
    }
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }  
}
