/*
 * ProveedoresInsertAction.java
 *
 * Created on 21 de abril de 2005, 09:34 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class ClienteUpdateAction  extends Action{
    
    /** Creates a new instance of ProveedoresInsertAction */
    public ClienteUpdateAction() {
    }
    
    public void run() throws ServletException {
        String next ="/cliente/clienteUpdate.jsp?var=ok";
        String est         = request.getParameter("est");
        String cod    = request.getParameter("codigo");
        String ag    = request.getParameter("agencia");
        String nom        = request.getParameter("nombre");
        String reg      = request.getParameter("reg_status");
        String base    = request.getParameter("base");
        String fecha_cre   = request.getParameter("fecha_cre");
        String fecha_act = request.getParameter("fecha_act");
        String notas = request.getParameter("notas");
        float rent = Float.parseFloat(request.getParameter("rentabilidad"));
        String texto_oc = request.getParameter("texto_oc");        
        /*HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");*/        
        Cliente cliente = new Cliente();
        cliente.setEstado(est);
        cliente.setCodcli(cod);
        cliente.setNomcli(nom);
        cliente.setReg_status(reg);
        cliente.setBase(base);
        cliente.setAgduenia(ag);
        cliente.setCreation_date(fecha_cre);
        cliente.setLast_update(fecha_act);
        cliente.setRentabilidad(rent);
        cliente.setTexto_oc(texto_oc);        
        try{            
            model.clienteService.setCliente(cliente);
            model.clienteService.updateCliente();           
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
