/********************************************************************
 *      Nombre Clase.................   ReporteStickersUtilizadosAction.java
 *      Descripci�n..................   Genera el reporte de precintos utilizados
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   04.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteStickersUtilizadosAction extends Action{
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteStickersUtilizadosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter("FechaI");
        String fechaF = request.getParameter("FechaF");
        String agencia = request.getParameter("agencia");       
                
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/trafico/reportes/ReporteStickersUtilizados.jsp?msg=" +
                "El Reporte se ha generado exitosamente en ReporteStickersUtilizados_" +
                FechaFormated1 + ".xls";
        
        try{
            String nom_agencia = "";
            
            if( agencia.compareTo("ALL")==0 )
                agencia = "";
            else
                nom_agencia = model.agenciaService.obtenerAgencia(agencia).getNombre();
            
            model.precintosSvc.stickersUtilizados(agencia, fechaI, fechaF);
            Vector pr = model.precintosSvc.getVector();
            
            if( pr.size() == 0){
                next = "/jsp/trafico/reportes/ReporteStickersUtilizados.jsp?msg=" +
                        "La consulta no gener� ning�n resultado.";
            } else{
                
                Vector reporte = new Vector();
                
                for( int i=0; i<pr.size(); i++){
                    Vector obj = new Vector();
                    Precinto prec = (Precinto) pr.elementAt(i);
                    InfoPlanilla info = model.precintosSvc.informacionPlanilla(prec.getNumpla());
                    obj.add(prec);
                    obj.add(info);
                    
                    reporte.add(obj);
                }
                
                /**
                 * Generamos el reporte
                 */
                ReporteStickersUtilizadosTh hilo = new ReporteStickersUtilizadosTh();
                hilo.start(model, reporte, usuario.getLogin(), fechaI, fechaF, nom_agencia);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
