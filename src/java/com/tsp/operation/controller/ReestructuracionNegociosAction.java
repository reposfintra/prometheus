/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.lowagie.text.DocumentException;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ReestructurarNegociosDAO;
import com.tsp.operation.model.DAOS.impl.ReestructurarNegociosImpl;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.Negocios;
import com.tsp.operation.model.beans.SolicitudNegocio;
import com.tsp.operation.model.beans.SolicitudPersona;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.ExcelApiUtil;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpSession;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author hcuello
 */
public class ReestructuracionNegociosAction extends Action {

    Usuario usuario = null;
    private final int BUSCAR_NEGOCIOS_MICRO = 1;
    private final int BUSCAR_FACTURAS_NEGOCIOS = 2;
    private final int SIMULADOR_CREDITO_MICRO = 3;
    private final int CREAR_NEG_NUEVO_MICRO = 4;
    private final int BUSCAR_DEPARTAMENTO = 5;
    private final int BUSCAR_CIUDAD = 6;
    private final int BUSCAR_FORM = 7;
    private final int ACTUALIZAR_FORM = 8;
    private final int LISTAR_NEGOCIOS = 9;
    private final int BUSCAR_LIQUIDACION = 10;
    private final int RECHAZAR_NEGOCIO = 11;
    private final int BUSCAR_TRAZA = 12;
    private final int APROBAR_NEGOCIOS = 13;
    private final int EXPORTAR_LIQUIDACION = 14;
    private final int APROBAR_FORMALIZACION = 15;
    private final int BUSCAR_CONVENIOS_MICRO = 16;
    private final int BUSCAR_LIQUIDACION_MICRO = 17;
    private final int EXPORTAR_EXCEL_SIMULADOR_MICRO = 18;
    private final int OBTENER_INFO_NEG_LIQ_MICRO = 19;
    private final int ACTUALIZAR_LIQUIDACION_NEG_MICRO = 20;

    @Override
    public void run() throws ServletException, InformationException {

        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            int opcion = Integer.parseInt(request.getParameter("opcion"));
            switch (opcion) {
                case BUSCAR_NEGOCIOS_MICRO:
                    getNegociosMicro();
                    break;

                case BUSCAR_FACTURAS_NEGOCIOS:
                    getFacturasNegocio();
                    break;
                case SIMULADOR_CREDITO_MICRO:
                    getSimuladorCredito();
                    break;
                case CREAR_NEG_NUEVO_MICRO:
                    crearNegoMicroNuevo();
                    break;
                case BUSCAR_DEPARTAMENTO:
                    cargarDepartamento();
                    break;
                case BUSCAR_CIUDAD:
                    cargarCiudad();
                    break;
                case BUSCAR_FORM:
                    cargarFormulario();
                    break;
                case ACTUALIZAR_FORM:
                    actualizarFormulario();
                    break;
                case LISTAR_NEGOCIOS:
                    getListaNegociosRestructuracion();
                    break;
                case BUSCAR_LIQUIDACION:
                    buscarLiquidacionCredito();
                    break;
                case RECHAZAR_NEGOCIO:
                    rechazarNegocio();
                    break;
                case BUSCAR_TRAZA:
                    buscarTraza();
                    break;
                case APROBAR_NEGOCIOS:
                    aprobarNegocio();
                    break;
                case EXPORTAR_LIQUIDACION:
                    exportarLiquidacion();
                    break;
                case APROBAR_FORMALIZACION:
                    formalizarNegocioMicro();
                    break;
                case BUSCAR_CONVENIOS_MICRO:
                    cargarConveniosMicro();
                    break;
                case BUSCAR_LIQUIDACION_MICRO:
                    buscarLiquidacionMicro();
                    break;
                case EXPORTAR_EXCEL_SIMULADOR_MICRO:
                    exportarExcel();
                    break;
                case OBTENER_INFO_NEG_LIQ_MICRO:
                    getInfoNegocioLiquidarMicro();
                    break;
                case ACTUALIZAR_LIQUIDACION_NEG_MICRO:
                    actualizarLiqNegMicro();
                    break;
            }

        } catch (Exception ex) {
            throw new ServletException(ex.getMessage());
        }

    }

    private void getNegociosMicro() {
        try {
            String cedula = request.getParameter("cedula") != null ? request.getParameter("cedula") : "";
            String codNegocio = request.getParameter("codneg") != null ? request.getParameter("codneg") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listGeneral = dao.getNegocios(cedula, codNegocio);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listGeneral) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void getFacturasNegocio() {
        try {

            String codNegocio = request.getParameter("codneg") != null ? request.getParameter("codneg") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listFacturasNegocios = dao.getFacturasNegocios(codNegocio);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listFacturasNegocios) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void getSimuladorCredito() {
        try {

            String tipo_cuota = request.getParameter("tipo_cuota") != null ? request.getParameter("tipo_cuota") : "";
            int nrCuotas = Integer.parseInt(request.getParameter("cuota") != null ? request.getParameter("cuota") : "0");
            double valorNegocio = Integer.parseInt(request.getParameter("valor_negocio") != null ? request.getParameter("valor_negocio").replaceAll(",", "") : "0");
            String primeraCuota = request.getParameter("primeracuota") != null ? request.getParameter("primeracuota") : "";
            String titulo_valor = request.getParameter("titulo_valor") != null ? request.getParameter("titulo_valor") : "";

            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listLiquidacion = dao.simuladorCreditoMicro(tipo_cuota, nrCuotas, valorNegocio, primeraCuota, titulo_valor);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listLiquidacion) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void crearNegoMicroNuevo() {
        try {

            String tipo_cuota = request.getParameter("tipo_cuota") != null ? request.getParameter("tipo_cuota") : "";
            int nrCuotas = Integer.parseInt(request.getParameter("cuota") != null ? request.getParameter("cuota") : "0");
            double valorNegocio = Double.parseDouble(request.getParameter("valor_negocio") != null ? request.getParameter("valor_negocio").replaceAll(",", "") : "0");
            String primeraCuota = request.getParameter("primeracuota") != null ? request.getParameter("primeracuota") : "";
            String titulo_valor = request.getParameter("titulo_valor") != null ? request.getParameter("titulo_valor") : "";
            String negocio_padre = request.getParameter("codigo_negocio") != null ? request.getParameter("codigo_negocio") : "";

            double saldo_capital = Double.parseDouble(request.getParameter("saldo_capital") != null ? request.getParameter("saldo_capital") : "0");
            double saldo_interes = Double.parseDouble(request.getParameter("saldo_interes") != null ? request.getParameter("saldo_interes") : "0");
            double saldo_cat = Double.parseDouble(request.getParameter("saldo_cat") != null ? request.getParameter("saldo_cat") : "0");
            double saldo_seguro = Double.parseDouble(request.getParameter("saldo_seguro") != null ? request.getParameter("saldo_seguro") : "0");
            double intxmora = Double.parseDouble(request.getParameter("intxmora") != null ? request.getParameter("intxmora") : "0");
            double gac = Double.parseDouble(request.getParameter("gac") != null ? request.getParameter("gac") : "0");

            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.crearNegoMicroNuevo(negocio_padre, usuario.getLogin(), 37, valorNegocio, 
                                                  nrCuotas, primeraCuota, tipo_cuota,saldo_capital,
                                                  saldo_interes, saldo_cat ,  saldo_seguro, intxmora, gac);
            
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     */
    private void cargarDepartamento() {
        try {
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.cargarDepartamento();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     *
     */
    private void cargarCiudad() {
        try {
            String codCiu = request.getParameter("cod_ciudad") != null ? request.getParameter("cod_ciudad") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.cargarCiudad(codCiu);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cargarFormulario() {
        try {
            String numero_solicitud = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.buscarFormulario(numero_solicitud);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void actualizarFormulario() {
        try {

            String numero_solicitud = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "";
            String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
            String pr_apellido = request.getParameter("pr_apellido") != null ? request.getParameter("pr_apellido") : "";
            String sg_apellido = request.getParameter("sg_apellido") != null ? request.getParameter("sg_apellido") : "";
            String pr_nombre = request.getParameter("pr_nombre") != null ? request.getParameter("pr_nombre") : "";
            String sg_nombre = request.getParameter("sg_nombre") != null ? request.getParameter("sg_nombre") : "";
            String telefono = request.getParameter("telefono") != null ? request.getParameter("telefono") : "";
            String celular = request.getParameter("celular") != null ? request.getParameter("celular") : "";
            String direccion = request.getParameter("direccion") != null ? request.getParameter("direccion") : "";
            String departamento = request.getParameter("departamento") != null ? request.getParameter("departamento") : "";
            String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";
            String barrio = request.getParameter("barrio") != null ? request.getParameter("barrio") : "";
            String email = request.getParameter("email") != null ? request.getParameter("email") : "";
            String identificacion_cy = request.getParameter("identificacion_cy") != null ? request.getParameter("identificacion_cy") : "";
            String pr_apellido_cy = request.getParameter("pr_apellido_cy") != null ? request.getParameter("pr_apellido_cy") : "";
            String sg_apellido_cy = request.getParameter("sg_apellido_cy") != null ? request.getParameter("sg_apellido_cy") : "";
            String pr_nombre_cy = request.getParameter("pr_nombre_cy") != null ? request.getParameter("pr_nombre_cy") : "";
            String sg_nombre_cy = request.getParameter("sg_nombre_cy") != null ? request.getParameter("sg_nombre_cy") : "";
            String telefono_cy = request.getParameter("telefono_cy") != null ? request.getParameter("telefono_cy") : "";
            String celular_cy = request.getParameter("celular_cy") != null ? request.getParameter("celular_cy") : "";
            String direcion_cy = request.getParameter("direcion_cy") != null ? request.getParameter("direcion_cy") : "";
            String nombre_negocio = request.getParameter("nombre_negocio") != null ? request.getParameter("nombre_negocio") : "";
            String direccion_ng = request.getParameter("direccion_ng") != null ? request.getParameter("direccion_ng") : "";
            String departamento_ng = request.getParameter("departamento_ng") != null ? request.getParameter("departamento_ng") : "";
            String ciudad_ng = request.getParameter("ciudad_ng") != null ? request.getParameter("ciudad_ng") : "";
            String barrio_ng = request.getParameter("barrio_ng") != null ? request.getParameter("barrio_ng") : "";
            String telefono_ng = request.getParameter("telefono_ng") != null ? request.getParameter("telefono_ng") : "";
            String observacion = request.getParameter("observacion") != null ? request.getParameter("observacion").trim() : "";
            String cod_ng = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";

            //seteamos los parametros en los beans correpondientes.
            SolicitudPersona persona = new SolicitudPersona();
            persona.setNumeroSolicitud(numero_solicitud);
            persona.setIdentificacion(identificacion);
            persona.setPrimerApellido(pr_apellido);
            persona.setSegundoApellido(sg_apellido);
            persona.setPrimerNombre(pr_nombre);
            persona.setSegundoNombre(sg_nombre);
            persona.setTelefono(telefono);
            persona.setCelular(celular);
            persona.setDireccion(direccion);
            persona.setDepartamento(departamento);
            persona.setCiudad(ciudad);
            persona.setBarrio(barrio);
            persona.setEmail(email);
            persona.setIdentificacionCony(identificacion_cy);
            persona.setPrimerApellidoCony(pr_apellido_cy);
            persona.setSegundoApellidoCony(sg_apellido_cy);
            persona.setPrimerNombreCony(pr_nombre_cy);
            persona.setSegundoNombreCony(sg_nombre_cy);
            persona.setTelefonoCony(telefono_ng);
            persona.setCelularCony(celular_cy);
            persona.setDireccionEmpresaCony(direcion_cy);
            persona.setTipo(cod_ng);//se va utilizar para almacenar el codigo del negocio.

            SolicitudNegocio negocio = new SolicitudNegocio();
            negocio.setNumeroSolicitud(numero_solicitud);
            negocio.setNombre(nombre_negocio);
            negocio.setDireccion(direccion_ng);
            negocio.setDepartamento(departamento_ng);
            negocio.setCiudad(ciudad_ng);
            negocio.setBarrio(barrio_ng);
            negocio.setTelefono(telefono_ng);


            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.actualizarFormulario(persona, negocio, usuario.getLogin(), observacion);
            this.printlnResponse(json, "application/json;");

        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void getListaNegociosRestructuracion() {
        try {

            String actividad = request.getParameter("act") != null ? request.getParameter("act") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listNegocios = actividad.equals("ANA") ? dao.getListarNegociosRees(actividad, "P") : dao.getListarNegociosRees(actividad, "V");
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listNegocios) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void buscarLiquidacionCredito() throws Exception {
        String json;
        try {

            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";

            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listLiquidacion = dao.buscarLiquidacionNegocio(negocio);
            Gson gson = new Gson();

            json = "{\"page\":1,\"rows\":" + gson.toJson(listLiquidacion) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            json = ex.getMessage();
            this.printlnResponse(json, "application/json;");
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void rechazarNegocio() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.rechazarNegocio(negocio, usuario.getLogin());
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void buscarTraza() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.buscarTraza(negocio);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void aprobarNegocio() {
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            String formulario = request.getParameter("formulario") != null ? request.getParameter("formulario") : "";
            String observacion = request.getParameter("observacion") != null ? request.getParameter("observacion").trim() : "";

            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.aprobarNegocio(negocio, formulario, usuario.getLogin(), observacion);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void formalizarNegocioMicro() throws Exception {

        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String formulario = request.getParameter("formulario") != null ? request.getParameter("formulario") : "";
        String observacion = request.getParameter("observacion") != null ? request.getParameter("observacion").trim() : "";
        String json = "";

        Negocios neg = new Negocios();
        neg = model.Negociossvc.buscarNegocio(negocio);
        Convenio convenio = model.gestionConveniosSvc.buscar_convenio("fintra", neg.getId_convenio() + "");
        ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
        ArrayList listLiquidacion = dao.buscarLiquidacionNegocio(negocio);

        if (convenio.getTipo().equals("Microcredito")) {
            String generarCXCMicrocredito = model.Negociossvc.generarCXCMicrocreditoReestructuracion(neg, usuario, convenio, listLiquidacion);
            if (generarCXCMicrocredito.equals("OK")) {              
                json = dao.actualizarNegocio(negocio, usuario.getLogin(), formulario, observacion);
                String cambiarCuentasOrden = dao.cambiarCuentasOrden(negocio, usuario);
                System.out.println("cambiarCuentasOrden: "+cambiarCuentasOrden);
            }
        }

        this.printlnResponse(json, "application/json;");
    }

    private void exportarLiquidacion() throws IOException, DocumentException {

        //exporta a excel los datos de la grilla
        String pdfBuffer = request.getParameter("pdfBuffer");
        String fileName = request.getParameter("fileName");
        String fileType = request.getParameter("fileType");
        boolean isPDF = fileType.equals("pdf");
        if (isPDF) {
            /*este codigo es para exporta a pdf para el futuro
             * 
             *  *
             *
             */
            ServletOutputStream outputStream = response.getOutputStream();
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(pdfBuffer);
            renderer.layout();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "." + fileType + "\"");
            renderer.createPDF(outputStream);
            outputStream.flush();
            outputStream.close();

        } else {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "." + fileType + "\"");
            PrintWriter out = response.getWriter();
            out.print(pdfBuffer);
            out.close();
        }

    }
    
    private void cargarConveniosMicro() {
        try {
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.cargarConveniosMicro();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
       private void buscarLiquidacionMicro() throws Exception {
        String json;
        try {

            String valor_negocio = request.getParameter("valor_negocio") != null ? request.getParameter("valor_negocio") : "0";
            String num_cuotas = request.getParameter("num_cuotas") != null ? request.getParameter("num_cuotas") : "0";
            String fecha_item = request.getParameter("fecha_item") != null ? request.getParameter("fecha_item") : "";
            String tipo_cuota = request.getParameter("tipo_cuota") != null ? request.getParameter("tipo_cuota") : "";
            String id_convenio = request.getParameter("id_convenio") != null ? request.getParameter("id_convenio") : "";
            String fecha_liquidacion = request.getParameter("fecha_liquidacion") != null ? request.getParameter("fecha_liquidacion") : "";
              

            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            ArrayList listLiquidacion = dao.buscarLiquidacionMicro(Double.parseDouble(valor_negocio),Integer.parseInt(num_cuotas),fecha_item, tipo_cuota, id_convenio, fecha_liquidacion);
            Gson gson = new Gson();

            json = "{\"page\":1,\"rows\":" + gson.toJson(listLiquidacion) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            json = ex.getMessage();
            this.printlnResponse(json, "application/json;");
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
       
    private void exportarExcel() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_liquidador", titulo = "SIMULADOR MICROCREDITO";

        cabecera = new String[]{"Fecha", "Cuota", "Saldo Inicial", "Valor Cuota", "Capital", "Interes", 
                                "Capacitacion", "Cat", "Seguro", "Saldo Final"};

        dimensiones = new short[]{
            3000, 3000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        String reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        String typeResponse = "text/plain";
        this.printlnResponse(reponseJson, typeResponse);
    }
    
    private void getInfoNegocioLiquidarMicro() {
        try {           
            String codNegocio = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String json = dao.getInfoNegociosLiquidarMicro(codNegocio,usuario);
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void actualizarLiqNegMicro() {
        try {           
            String negocio =  request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
            String tipo_cuota = request.getParameter("tipo_cuota") != null ? request.getParameter("tipo_cuota") : "";
            String num_cuotas = request.getParameter("num_cuotas") != null ? request.getParameter("num_cuotas") : "0";
            String valor_negocio = request.getParameter("valor_negocio") != null ? request.getParameter("valor_negocio") : "0";
            String fecha_liquidacion = request.getParameter("fecha_liquidacion") != null ? request.getParameter("fecha_liquidacion") : "";
            String fecha_item = request.getParameter("fecha_item") != null ? request.getParameter("fecha_item") : "";
            String id_convenio = request.getParameter("id_convenio") != null ? request.getParameter("id_convenio") : "";
            
            ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
            String resp = dao.actualizarLiquidacionMicro(negocio, tipo_cuota, Integer.parseInt(num_cuotas), Double.parseDouble(valor_negocio), fecha_liquidacion, fecha_item, id_convenio);
            this.printlnResponse(resp, "application/json;");
            
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
}
