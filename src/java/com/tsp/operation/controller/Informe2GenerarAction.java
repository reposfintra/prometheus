/*
 * InformeGenerarAction.java
 *
 * Created on 1 de abril de 2005, 03:20 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class Informe2GenerarAction extends Action {
    
    /** Creates a new instance of InformeGenerarAction */
    public Informe2GenerarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/informes/InformeDespacho2.jsp";
        String fechai = request.getParameter("fechai");
        String fechaf = request.getParameter("fechaf");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            model.planillaService.llenarClientes2(fechai, fechaf, usuario.getBase());
            request.setAttribute("lista", model.planillaService.getClientes());
            Informe inf = new Informe ();
            inf.setFechai(fechai);
            inf.setFechaf(fechaf);
            model.planillaService.setInforme(inf);
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
