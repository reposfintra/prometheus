/***************************************
* Nombre Clase ............. ReportePlanillasViaAction.java
* Descripci�n  .. . . . . .  Realiza los eventos para la generacion reporte planillas por pc
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  09/08/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/




package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.HReportePlanillasVIA;





public class ReportePlanillasViaAction extends Action{
    
   
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
         try{
            
            HttpSession session  = request.getSession();
            Usuario     usuario  = (Usuario) session.getAttribute("Usuario");
      
        
            String   next       = "/jsp/trafico/reportes/ReportePlanillasPC.jsp?msj=";
            String   msj        = "";
            
            String   evento     = request.getParameter("evento");
            if( evento!=null ){
                   
                if( evento.equals("GENERAR") ){
                      String    fechaIni  =  request.getParameter("fechaIni");
                      String    fechaFin  =  request.getParameter("fechaFin");
                      String    origen    =  request.getParameter("origen");
                      String    destino   =  request.getParameter("destino");
                      String    via       =  request.getParameter("via");                      
                      String    tipo      =  request.getParameter("tipo");
                      List      clientes  =  new LinkedList(); 
                      
                      if( tipo.equals("ALL") )   
                          clientes  =  model.ReportePlanillasViaSvc.getClientes();
                      else{
                          String[] ids    =  request.getParameterValues("cliente");
                          if(ids!=null)
                              for(int i=0;i<ids.length;i++){
                                  Hashtable  ht = new Hashtable();
                                    ht.put("codigo", ids[i]);
                                  clientes.add(ht);
                              }
                      }
                      
                      if( clientes.size()>0){
                          HReportePlanillasVIA  hilo  =  new  HReportePlanillasVIA();
                          hilo.start( model, usuario, clientes, fechaIni, fechaFin, origen, destino, via );                    
                          msj = "El reporte est� siendo procesado...";
                      }else
                          msj = "No hay clientes para generar archivos...";
                      
                      
                }
                
                
                
                if( evento.equals("INIT") ){
                   model.ReportePlanillasViaSvc.LoadOrigen();
                   model.ReportePlanillasViaSvc.LoadClientes();
                }
                
                
                if( evento.equals("BUSCAR_DESTINOS") ){
                    String origen = request.getParameter("origen");
                    model.ReportePlanillasViaSvc.LoadDestino(origen);
                    next       = "/jsp/trafico/reportes/ReportePlanillasPCLoad.jsp?op=DESTINO";
                }
                
                if( evento.equals("BUSCAR_VIAS") ){
                    String origen  = request.getParameter("origen");
                    String destino = request.getParameter("destino");
                    model.ReportePlanillasViaSvc.LoadVIAS(origen, destino);
                    next       = "/jsp/trafico/reportes/ReportePlanillasPCLoad.jsp?op=VIAS";
                }
                
                
            }
            
            
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next + msj );
            if(rd == null)
               throw new Exception("No se pudo encontrar "+ next + msj );
            rd.forward(request, response); 
            
        }catch(Exception e){
            throw new ServletException( e.getMessage() );
        }
    }
    
}
