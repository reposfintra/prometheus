/******************************************************************
* Nombre ......................TblGeneralDatoModificarAction.java
* Descripci�n..................Clase Action para tabla general dato
* Autor........................Armando Oviedo
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class TblGeneralDatoModificarAction extends Action{
    
    /** Creates a new instance of TblGeneralDatoModificarAction */
    public TblGeneralDatoModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/general/TblGeneralDato/TblGeneralDatoModificar.jsp";
        try{
            String mensaje = request.getParameter("mensaje");
            HttpSession session = request.getSession();            
            String codtabla = request.getParameter("codtabla");
            String secuencia = request.getParameter("secuencia");
            String leyenda = request.getParameter("leyenda");            
            String tipo = request.getParameter("tipo");            
            String longitud = request.getParameter("longitud");            
            String modsec = request.getParameter("modsec");
            Usuario usuario = (Usuario) session.getAttribute("Usuario");                                            
            if(mensaje!=null){
                if(mensaje.equalsIgnoreCase("listar")){
                    TblGeneralDato tmp = new TblGeneralDato();
                    tmp.setCodTabla(codtabla);
                    tmp.setSecuencia(secuencia);
                    model.tblgendatosvc.setTablaGeneralDato(tmp);
                    model.tblgendatosvc.buscarTablaGeneralDato();
                }
                else if(mensaje.equalsIgnoreCase("modificar")){                    
                    TblGeneralDato tmp = new TblGeneralDato();
                    tmp.setCodTabla(codtabla);
                    tmp.setSecuencia(secuencia);
                    tmp.setLeyenda(leyenda);                    
                    tmp.setUserUpdate(usuario.getLogin());
                    tmp.setLongitud(longitud);
                    tmp.setTipo(tipo);
                    model.tblgendatosvc.setTablaGeneralDato(tmp);
                    model.tblgendatosvc.update();        
                    next+="?reload=ok&mensajemod=Elemento, modificado correctamente";
                }
                else if(mensaje.equalsIgnoreCase("eliminar")){                    
                    TblGeneralDato tmp = new TblGeneralDato();
                    tmp.setCodTabla(codtabla);
                    tmp.setSecuencia(secuencia);                    
                    tmp.setUserUpdate(usuario.getLogin());
                    model.tblgendatosvc.setTablaGeneralDato(tmp);
                    model.tblgendatosvc.delete();
                    model.tblgendatosvc.recalcularSecuenciasEliminadas(codtabla);
                    next+="?reload=ok&mensajemod=Elemento, eliminado correctamente";
                }
            }
            else if(modsec!=null){
                if(modsec.equalsIgnoreCase("listarsecuencias")){
                    next = "/jsp/general/TblGeneralDato/TblGeneralDatoModificarSecuencia.jsp?codtabla="+codtabla+"&secuencia="+secuencia;
                    model.tblgendatosvc.buscarItemsCodigoTabla(codtabla);                    
                }
                else{
                    next = "/jsp/general/TblGeneralDato/TblGeneralDatoModificarSecuencia.jsp?codtabla="+codtabla+"&secuenciasel="+secuencia+"&reload=ok";
                    String secuencias[] = request.getParameterValues("secuencia");                    
                    model.tblgendatosvc.reordenarSecuenciasModificadas(secuencias,codtabla);                    
                    model.tblgendatosvc.buscarItemsCodigoTabla(codtabla);
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
