/*
 * DespachomLplacaAction.java
 *
 * Created on 21 de noviembre de 2005, 04:05 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachomLplacaAction extends Action {
    
    /** Creates a new instance of DespachomLplacaAction */
    public DespachomLplacaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        try {
            String sw=""+request.getParameter("sw");
            String placa=""+request.getParameter("c_placa");
            model.placaService.vPlacas(placa);
            if(sw.equals("1")) {
                next="/jsp/trafico/despacho_manual/placa.jsp?accion=1";
            }
            else {
                next="/jsp/trafico/despacho_manual/dxplaca.jsp?accion=1";
            }
        }
        catch (Exception e) {
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
