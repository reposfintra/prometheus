/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class ClienteBuscarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ClienteBuscarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String Codigo = request.getParameter ("Codigo")!=null?request.getParameter ("Codigo").toUpperCase():"";
        String Nombr = request.getParameter ("Nombre")!=null?request.getParameter ("Nombre"):"";
        String Nombre = Nombr.toUpperCase();
        String Nit = request.getParameter ("Nit")!=null?request.getParameter ("Nit"):"";
        String agencia = (request.getParameter("agencia")!=null)?request.getParameter ("agencia"):"";
        String pais = (request.getParameter("Pais")!=null)?request.getParameter ("Pais"):"";
        
        String next = "/jsp/sot/body/ReporteCliente.jsp?Codigo="+Codigo+"&Nombre="+Nombre+"&Nit="+Nit+"&agencia="+agencia+"&pais="+pais;
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        agencia = agencia.equals("NADA")?"":agencia;
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        try {
              Vector datos = model.clienteService.getVector();
              
           if ( opcion.equals("1") ){
                model.clienteService.BusquedaCLiente(Codigo, Nit, Nombre, agencia,pais); 
                datos = model.clienteService.getVector();
                request.setAttribute("Codigo",Codigo);
                request.setAttribute("Nombre",Nombre);
                request.setAttribute("Nit",Nit);
                request.setAttribute("agencia",agencia);
                request.setAttribute("pais",pais);
                if ( datos.size () <= 0 ) {    
                     next = next + "&msg=Su busqueda no arrojo resultados!";
                
                }  
           } 
             
            if ( opcion.equals("2") ) {
                HReporteCliente cli = new HReporteCliente();
                cli.start(Codigo, Nit, Nombre, agencia,pais, usu);
               // next = next + "?msg=Defina un parametro de busqueda!";
            }
             
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de ClienteBuscarAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
