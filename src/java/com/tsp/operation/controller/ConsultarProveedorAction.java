/*
 * Nombre        ConsultarProveedorAction.java 
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         17 de noviembre de 2006, 10:33 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class ConsultarProveedorAction extends Action{
    
    /**
     * Crea una nueva instancia de  ConsultarProveedorAction
     */
    public ConsultarProveedorAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session   = request.getSession();
        Usuario     usuario   = (Usuario) session.getAttribute("Usuario");
        String prov = request.getParameter("provee");
        String next="/consultas/consultasRedirect.jsp?proveedor2=ok";
        
        try{
            
            model.ChequeXFacturaSvc.setUsuario(usuario);
            model.ChequeXFacturaSvc.searchProveedores(prov.toUpperCase());            
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
