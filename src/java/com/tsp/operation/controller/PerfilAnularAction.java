/*
 * Nombre        PerfilAnularAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class PerfilAnularAction extends Action {
    
    /** Creates a new instance of PerfilAnularAction */
    public PerfilAnularAction() {
    }
        
    public void run() throws ServletException, InformationException{
        String next= request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        String idp = request.getParameter("idp");        
        HttpSession session = request.getSession();     
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{ 
            model.perfilService.obtenerPerfilxPropietario(idp);
            Perfil perfil = model.perfilService.getPerfil();            
            perfil.setUser_update(usuario.getLogin());
            model.perfilService.anularPerfil(perfil);
                
            //obtengo pos del perfil 
            model.perfilService.listarPerfilOpcion(idp);
            Vector vpo = model.perfilService.getVPerfil();
            
            if ( vpo.size() != 0 ){
                for ( int i = 0; i < vpo.size(); i++){
                    //obtengo po
                    PerfilOpcion po = (PerfilOpcion) vpo.elementAt(i); 
                    model.perfilService.anularPerfilOpcion(usuario.getLogin(), idp, po.getId_opcion());
                }
            }
            request.setAttribute("perfil",null);            
            next = next + "?tipo=Anular&msg=Anulaci�n exitosa!";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }    
}//end class