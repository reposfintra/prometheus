/*************************************************************************
 * Nombre                       ClientactBusquedaAction.java               *
 * Descripci�n                  Clase Action para buscar las actividades *
 *                              de los cliente                           *
 *                              actividad                                *
 * Autor.                       Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha                        29 de agosto de 2005, 09:02 PM           *
 * Versi�n                      1.0                                      *
 * Coyright                     Transportes Sanchez Polo S.A.            *
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes
 */
public class ClientactBusquedaAction extends Action {
    
    /** Creates a new instance of ClientactBusquedaAction */
    public ClientactBusquedaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/clienteactividad/actividadesAsig.jsp";
        String op = (request.getParameter("op")!= null)?request.getParameter("op"):"";
        try{
            if(op.equals("")){
                String act = request.getParameter("actividad").toUpperCase()+"%";
                String cli = request.getParameter("cliente").toUpperCase()+"%";
                String tip = request.getParameter("tipo").toUpperCase()+"%";
           
                model.clientactSvc.listarClientActxBusq(usuario.getCia(), act, cli, tip);
                
            }else if(op.equals("BUSCAR_CLIENTE")){
                next="/jsp/trafico/clienteactividad/puente.jsp?opcion=CLIENTE";
                String cod_cli = (request.getParameter("cod_cli")!=null)?request.getParameter("cod_cli"):"";
                model.clientactSvc.buscarCliente(cod_cli);
                
            }else if(op.equals("BUSCAR_CLIENTE_LUPA")){
                String nomcli = (request.getParameter("nomcli")!=null)?request.getParameter("nomcli"):"";
                model.clientactSvc.buscarClientePorNombre(nomcli);
                next="/jsp/trafico/clienteactividad/buscarCliente.jsp?accion=1&marco=no";
            }
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
