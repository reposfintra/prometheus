/***********************************************
 * Nombre clase: CumplidoCargarAction.java
 * Descripci�n: Accion para buscar un cumplido
 * Autor: Rodrigo Salazar
 * Fecha: 20 de septiembre de 2005, 03:48 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

/**
 *
 * @author  R.SALAZAR
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
public class CumplidoCargarAction extends Action{
    /** Creates a new instance of CumplidoCargarAction */
    public CumplidoCargarAction () {
    }
    public void run () throws javax.servlet.ServletException, InformationException{
        ////System.out.println("ACTION");
        String next="/jsp/cumplidos/"+request.getParameter ("carpeta")+"/"+request.getParameter ("pagina");
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String tipodoc = request.getParameter ("c_tdoc");
        Cumplido cump;
        String cod = request.getParameter ("c_cod").toUpperCase ();
        List vec;
        try{
            if (tipodoc.equals ("001")){
                model.planillaService.buscaPlanilla (cod);
                Planilla p=model.planillaService.getPlanilla ();
                if(p!=null){
                    model.sjextrafleteService.listaFletes (p.getSj (),usuario.getId_agencia());
                    model.sjextrafleteService.listaCostos (p.getSj (),usuario.getId_agencia());
                    model.tblgensvc.buscarListaCodigo ("AUTEXFLETE", "000"+p.getSj ().substring (0,3));
                    vec = model.planillaService.buscarRemesas (cod);
                    request.setAttribute ("remesas",vec);
                    if(model.discrepanciaService.existeDiscrepanciaPlanilla (cod)){
                        next+="?discre=true";
                        if( p.getBase ().equals ("COL") ){
                            next+="&sw=1";
                        }
                        else{
                            next+="&sw=2";
                            request.setAttribute ("mensaje","No se permite cumplir o mostrar la planilla, por que no es una planilla creada en la WEB");                        
                        }
                            
                    }
                    else{
                        next+="?discre=";
                        if( p.getBase ().equals ("COL") ){
                            next+="&sw=1";
                        }
                        else{
                            next+="&sw=2";
                            request.setAttribute ("mensaje","No se permite cumplir o mostrar la planilla por que no es una planilla de la WEB");                        
                        }
                    }   
                }
                else{
                    next+="?sw=2";
                    request.setAttribute ("mensaje","No se encontro una planilla con ese codigo");
                }
            }
            else{
                vec = model.remesaService.buscarPlanillas (cod);
                request.setAttribute ("planillas", vec);
                next = "/jsp/cumplidos/cumplido/ListaPlanillas.jsp";
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
        
    }
}
