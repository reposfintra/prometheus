/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class SolicitarAutorizacionAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public SolicitarAutorizacionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next ="/colpapel/fletes.jsp?mensaje=Su solicitud ha sido enviada exitosamente.";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String base =usuario.getBase();
        try{
            float unit_cost =0;
            float viejoFlete =0;
            String monedaFlete="";
            String unit_transp="";
            model.stdjobcostoService.buscarStandardJobCostoFull(request.getParameter("sj"),request.getParameter("valorpla"));
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                unit_cost=stdjobcosto.getUnit_cost();
                viejoFlete = stdjobcosto.getUnit_cost();
                monedaFlete = stdjobcosto.getCurrency();
                if(!request.getParameter("nuevo").equals("NO")&&!request.getParameter("nuevo").equals("")){
                    unit_cost = Float.parseFloat(request.getParameter("nuevo"));
                }
                unit_transp = stdjobcosto.getUnit_transp();
            }
            
            String descripcion="";
            model.stdjobdetselService.buscaStandard(request.getParameter("sj"));
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                descripcion= stdjobdetsel.getSj_desc();
                
                
            }
            String cedula ="";
            String nit ="";
            model.placaService.buscaPlaca(request.getParameter("placa").toUpperCase());
            if(model.placaService.getPlaca()!=null){
                Placa plac = model.placaService.getPlaca();
                cedula = plac.getConductor();
                nit = plac.getPropietario();
            }
            String nombre = "No se encontro";
            model.conductorService.buscaConductor(cedula);
            if(model.conductorService.getConductor()!=null){
                Conductor conductor = model.conductorService.getConductor();
                nombre=conductor.getNombre();
            }
            String numsol = model.afleteService.getSolicitud();
            
            //BUSCO EL CLIENTE Y OBTENGO EL EMAIL DEL AGENTE DUE�O
            model.clienteService.searchCliente("000"+request.getParameter("sj").substring(0,3));
            Cliente cli = model.clienteService.getCliente();
            Date d = new Date();
            SimpleDateFormat s = new SimpleDateFormat("yyyy/MM/dd");
            String hoy = s.format(d);
            String nomorigen = model.ciudadService.obtenerNombreCiudad(request.getParameter("valorpla").substring(0,2));
            String nomdest = model.ciudadService.obtenerNombreCiudad(request.getParameter("valorpla").substring(2,4));
            String planilla = model.afleteService.getMaxPlanilla();
            //TEXTO DEL BODY DEL EMAIL
            String textoBody= "Se Solicita la Autorizacion para el siguiente ajuste:\n\n\n";
            textoBody+="DATOS DEL DESPACHO A REALIZAR\n\n"+
            "PLACA     :    "+request.getParameter("placa").toUpperCase()+"\n"+
            "CONDUCTOR : "+cedula+"-"+nombre+"\n\n"+
            "DATOS DEL CLIENTE"+
            "\nCLIENTE    : "+cli.getNomcli()+
            "\nSTANDARD   : "+request.getParameter("sj")+"-"+descripcion+
            "\nRUTA       : "+nomorigen+"-"+nomdest+"\n\n"+
            "AJUSTES EN EL FLETE"+
            "\nVALOR DEL FLETE DEL STANDARD : "+com.tsp.util.Util.customFormat(viejoFlete)+"-"+unit_transp+"\n"+
            "VALOR DEL FLETE AJUSTADO     : "+com.tsp.util.Util.customFormat(unit_cost)+"-"+unit_transp+"\n"+
            "JUSTIFICACION DEL AJUSTE     : "+request.getParameter("justificacion")+"\n"+
            "FECHA DEL CAMBIO             : "+hoy+"\n\n"+
            "DESPACHADOR                  : "+usuario.getNombre().toUpperCase()+"\n\n" +
            "NUMEROD DE LA SOLICITUD      :"+numsol+
            "\n\n\n SI DESEA AUTORIZAR ESTE CAMBIO HAGA CLICK EN ESTA DIRECCION : http://equipo8:8080/slt2/BuscarAutorizacionRedirect.jsp?planilla="+planilla;
            
            //AGREGO EL REGISTRO EN AUTORIZACION DE FLETE.
            AutorizacionFlete aflete = new AutorizacionFlete();
            aflete.setAgency_id(usuario.getId_agencia());
            aflete.setAutorizador(cli.getAgente());
            aflete.setBase(usuario.getBase());
            aflete.setCedcond(cedula+"-"+nombre);
            aflete.setDespachador(usuario.getNombre());
            aflete.setDstrct(usuario.getDstrct());
            aflete.setEmail_autorizador(cli.getEmail());
            aflete.setJustificacion(request.getParameter("justificacion"));
            aflete.setNuevoflete(unit_cost);
            aflete.setPla_owner(nit);
            aflete.setPlaca(request.getParameter("placa").toUpperCase());
            aflete.setPlanilla(planilla);
            aflete.setStandard(request.getParameter("sj")+"-"+descripcion);
            aflete.setCreation_user(usuario.getLogin());
            aflete.setUnidad(unit_transp);
            aflete.setViejoflete(viejoFlete);
            aflete.setNumsol(numsol);
            //PARA EL EMAIL
            aflete.setMyEmail("notificacionFlete@mail.tsp.com");
            aflete.setSubject("Solicitud Autorizacion Ajuste Flete");
            aflete.setBody(textoBody);
            aflete.setEmailDesp(usuario.getEmail());
            aflete.setMyName("FINV");
            model.afleteService.setAflete(aflete);
            model.tService.crearStatement();
            model.tService.getSt().addBatch(model.afleteService.insert());
            model.tService.getSt().addBatch(model.afleteService.insertSendMail());
            model.tService.execute();
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
        
    }
    
    
}
