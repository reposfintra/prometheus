/*******************************************************************
 * Nombre clase: PlacaBuscarAction.java
 * Descripci�n: Accion para eliminar una placa de la bd.
 * Autor: Ing. Luigi
 * Fecha: 16 de marzo de 2007, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  jdelarosa
 */
public class PlacaDeleteAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public PlacaDeleteAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/placas/placaDelete.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        
        String idplaca = request.getParameter("idplaca").toUpperCase();
        String tipo = request.getParameter("grupo_equipo")!=null? request.getParameter("grupo_equipo").toUpperCase() : "";
        
        try{
            
            
            model.placaService.eliminarPlaca(idplaca);
            next+="?msg=Placa Eliminada exitosamente...";
            
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}
