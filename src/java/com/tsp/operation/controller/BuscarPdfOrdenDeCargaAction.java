/******************************************************************************
 * Nombre clase :                   BuscarPdfOrdenDeCargaAction.java          *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Buscar de Orden De Carga                  *
 * Autor :                          LREALES                                   *
 * Fecha :                          11 de mayo de 2006, 12:05 PM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.io.*;

public class BuscarPdfOrdenDeCargaAction extends Action{
    
    /** Creates a new instance of BuscarPdfOrdenDeCargaAction */
    public BuscarPdfOrdenDeCargaAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
        String next="/impresiones_pdf/imprimirOrdenDeCarga/imprimirOrdenDeCarga.jsp";
           
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute( "Usuario" );
        String distrito = (String) session.getAttribute( "Distrito" );
        String user_update = usuario.getLogin().toUpperCase();
        
        String orden = ( request.getParameter( "orden" ).toUpperCase() != null )?request.getParameter( "orden" ).toUpperCase() :"";
               
        String duplicado = "";
        
        try{
                 
            if ( !orden.equals("") ){
                      
                boolean boc = false;                
                boc = model.imprimirOrdenService.existeOrdenDeCarga( distrito, orden );
                
                if ( boc == true ){
                    
                    model.imprimirOrdenService.buscarOrdenDeCarga( distrito, orden );                
                    Vector vec_buscar = model.imprimirOrdenService.getVec_buscar();
                    String bucar_orden = "";
                    String bucar_regsta = "";
                    String bucar_fecimp = "";
                    
                    if( ( vec_buscar != null ) && ( vec_buscar.size() > 0 ) ){
                        
                        HojaOrdenDeCarga hojaOrden = ( HojaOrdenDeCarga ) vec_buscar.elementAt( 0 );
                        
                        bucar_orden = hojaOrden.getOrden();
                        bucar_regsta = hojaOrden.getReg_status().toUpperCase();
                        bucar_fecimp = hojaOrden.getFecha_impresion();
                    }
                    
                    if ( bucar_regsta.equals( "A" ) ){
                        
                        next = next + "?Mensaje=La Orden Digitada ESTA ANULADA!";

                    } else if ( !bucar_orden.equals( "" ) ){
                        
                        if ( bucar_fecimp.equals( "0099-01-01 00:00:00" ) ){
                            
                            model.imprimirOrdenService.Update_Fecha_De_Impresion( user_update, distrito, orden );
                            duplicado = "no";
                            model.imprimirOrdenService.setDuplicado( duplicado );
                            
                        } else{
                            
                            duplicado = "si";
                            model.imprimirOrdenService.setDuplicado( duplicado );

                        }

                        model.imprimirOrdenService.listaOrdenDeCarga( distrito, orden );
                        Vector info_orden = model.imprimirOrdenService.getVec_lista();
                        String numorden = model.imprimirOrdenService.getHojaOrden().getOrden();
                        
                        next = next + "?Mensaje=Por Favor Ingrese La Hoja y Seleccione Imprimir!&sw=Ok&orden=" + numorden + "&distrito=" + distrito + "&duplicado=" + duplicado;

                    }   
                    
                } else{
                    
                    next = next + "?Mensaje=La Orden Digitada NO EXISTE!";
                    
                }
                                
            }         
                
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new ServletException( "Error en Buscar Pdf Orden De Carga Action.......\n" + e.getMessage() );
        
        }
        
        this.dispatchRequest( next );
        
    }  
    
}