/*
 * ConciliacionLeerAction.java
 *
 * Created on 20 de marzo de 2005, 04:42 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import java.io.BufferedReader;
import java.io.FileReader;
/**
 *
 * @author  Administrador
 */
public class ConciliacionLeerAction extends Action {
    
    /** Creates a new instance of CompaniaAtrasAction */
    public ConciliacionLeerAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "";
        String fch = request.getParameter("fecha");    
        String ruta = (String)request.getParameter("ruta");        
                
        String savePath = "/exportar/masivo/conciliacion_placas/";
        String cFilename = savePath + (String)request.getParameter("filename");
        String row="";
        
        int i=0;
        int j=0;
        int z = 0;
        String total = "";
        
        try{
            BufferedReader in = new BufferedReader(new FileReader(cFilename));
        
            int sw = 1;
        
            String resultado = "";
            while(sw == 1){
                row = in.readLine();                
                i++;                
                if( row == null ){//fin archivo
                    sw = 0;
                }else{
                    if ( i >= 15 ){
                        //comparar con tabla de placas
                        resultado = model.conciliacion.compararconPlacas(row.toUpperCase(), fch);//a modificar
                       
                        //comparar placa en la planilla
                        if (resultado.equalsIgnoreCase("vacio")){
                            z++;                            
                        }else if ((!resultado.equalsIgnoreCase("vacio")) && (!resultado.equalsIgnoreCase("")) ) {
                            j++;
                            total = total + resultado; 
                         }
                    }
                }
            }
            
            //System.out.println(" LAS SIGTES PLACAS NO SE ENCUENTRAN EL LA PLANILLA " + total + " TOTAL " + j);
            //System.out.println(" PLACAS ENCONTRADAS EN PLANILLA: " + z + " PLACAS NO ENCONTRADAS EN PLANILLA: " + j);
            
            next="/" + request.getParameter("carpeta") + "/" + request.getParameter("pagina") + "?lpne=" + total + "&npne=" + j + "&npe=" + z;
            
            //mandar mail
            if ( j != 0 ){                
                model.conciliacion.enviarCorreo(total, fch, request.getParameter("ruta"));
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }catch (IOException ex){
            throw new ServletException(ex.getMessage());
        }	                
        this.dispatchRequest(next);
    }
}