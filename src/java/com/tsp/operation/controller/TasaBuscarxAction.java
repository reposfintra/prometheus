/*
 * TasaBuscarxAction.java
 *
 * Created on 4 de julio de 2005, 01:04 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  DIOGENES
 */
public class TasaBuscarxAction extends Action {
    
    /** Creates a new instance of TasaBuscarxAction */
    public TasaBuscarxAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String moneda1 = (request.getParameter("c_mon1").toUpperCase())+"%";
        String moneda2 = (request.getParameter("c_mon2").toUpperCase())+"%";
        String fec = (request.getParameter("c_fecha").toUpperCase())+"%";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{ 
            ////System.out.println("M1 "+moneda1 +" M2 "+ moneda2+" Fe "+fec); 
            model.tasaService.BuscarTasaX(moneda1,moneda2,fec,usuario.getCia());
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
