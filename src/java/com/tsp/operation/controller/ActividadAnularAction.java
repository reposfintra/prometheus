/*************************************************************************
 * Nombre ......................ActividadAnularAction.java               *
 * Descripci�n..................Clase Action para anular actividad    *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................29 de agosto de 2005, 07:46 AM           * 
 * Versi�n......................1.0                                      * 
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/ 
  
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class ActividadAnularAction extends Action {
    
    /** Creates a new instance of ActividadAnularAction */
    public ActividadAnularAction() {
    }
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0;
        
        Actividad act = new Actividad();
        act.setCodActividad(request.getParameter("codigo").toUpperCase());
        act.setUser_update(usuario.getLogin());
        act.setDstrct(usuario.getCia());
        act.setLast_update(now);
        act.setCreation_date(now);
        try{
            model.actividadSvc.anularActividad(act);                
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
