/*******************************************************************
 * Nombre clase: Acuerdo_especialUpdateAction.java
 * Descripci�n: Accion para actualizar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 6 de diciembre de 2005, 02:51 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Acuerdo_especialUpdateAction extends Action{
    
    /** Creates a new instance of Acuerdo_especialUpdateAction */
    public Acuerdo_especialUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="/jsp/equipos/acuerdo_especial/acuerdo_especialModificar.jsp?&reload=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String)(session.getAttribute ("Distrito"));
        String standar = request.getParameter("c_standar").toUpperCase ();
        String codigo = request.getParameter("c_codigo").toUpperCase ();
        String tipo = request.getParameter("c_tipo").toUpperCase ();//tipo acuerdo
        String tip = request.getParameter("tipo");
        float porcentaje_descuento = Float.parseFloat (request.getParameter("c_porcentaje"));      
        try{
            Acuerdo_especial acuerdo = new Acuerdo_especial();
            acuerdo.setStandar (standar);
            acuerdo.setTipo(tip);
            acuerdo.setCodigo_concepto (codigo);
            acuerdo.setTipo_acuerdo (tipo);
            acuerdo.setDistrito (distrito);
            acuerdo.setPorcentaje_descuento (porcentaje_descuento);
            acuerdo.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            model.acuerdo_especialService.updateAcuerdo_especial(acuerdo);
            request.setAttribute("mensaje","La informaci�n ha sido modificada exitosamente!");
            model.acuerdo_especialService.searchAcuerdo_especial(standar, distrito, codigo,tip);

        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);        
    }
    
}
