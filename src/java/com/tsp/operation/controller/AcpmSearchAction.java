/*
 * AcpmSearchAction.java
 *
 * Created on 2 de diciembre de 2004, 09:42 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AcpmSearchAction  extends Action{
    
    static Logger logger = Logger.getLogger(SalidaValidarAction.class);
    /** Creates a new instance of AcpmSearchAction */
    public AcpmSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/acpm/acpmUpdate.jsp";
        if(request.getParameter("num").equals("2"))
            next="/acpm/acpmDelete.jsp";
        try{
            String nit= request.getParameter("nit");
            String codigo = request.getParameter("codigo");
            model.proveedoracpmService.buscaProveedor(nit,codigo);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm pacpm= model.proveedoracpmService.getProveedor();
                
                request.setAttribute("pacpm",pacpm);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
