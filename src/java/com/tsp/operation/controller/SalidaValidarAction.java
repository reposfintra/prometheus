/*
 * SalidaValidarAction.java
 *
 * Created on 25 de noviembre de 2004, 08:51 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class SalidaValidarAction extends Action {
    
    static Logger logger = Logger.getLogger(SalidaValidarAction.class);
    String grupo="ninguno";
    float valor=0;
    float costo=0;
    float maxant=0;
    String dest="";
    String origen="";
    float acpmmax=0;
    float peajeamax=0;
    float peajebmax=0;
    float peajecmax=0;
    String tipo="";
    float e_max=0;
    float vacpm=0;
    float vpeaje=0;
    Usuario usuario;
    
    
    /** Creates a new instance of SalidaValidarAction */
    public SalidaValidarAction() {
    }
    
    public String buscarStandard(String pla)throws ServletException{
        String sj="";
        try{
            model.remplaService.buscaRemPla(pla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rempla = model.remplaService.getRemPla();
                String no_rem= rempla.getRemesa();
                model.remesaService.buscaRemesa(no_rem);
                if(model.remesaService.getRemesa()!=null){
                    Remesa rem=model.remesaService.getRemesa();
                    sj=rem.getStdJobNo();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        return sj;
    }
    
    public void buscarValor(String sj, String placa, float pesoreal)throws ServletException{
        
        try{
            model.stdjobcostoService.buscaStdJobCosto(sj);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto std = model.stdjobcostoService.getStandardCosto();
                maxant=std.getporcentaje_maximo_anticipo();
                costo = std.getUnit_cost();
                valor= pesoreal * costo;
            }
            
            model.plkgruService.buscaPlkgru(placa,sj);
            if(model.plkgruService.getPlkgru()!=null){
                Plkgru plk = model.plkgruService.getPlkgru();
                grupo=plk.getGroupcode();
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    Stdjobplkcosto std = model.stdjobplkcostoService.getStd();
                    costo = std.getUnit_cost();
                    valor= pesoreal * costo;
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    public void buscarTramo(String sj)throws ServletException{
        try{
            model.stdjobdetselService.buscaStandardDetSel(sj);
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel sds= model.stdjobdetselService.getStandardDetSel();
                origen=sds.getOrigin_code();
                dest=sds.getDestination_code();
                ////System.out.println("Voy a buscar el tramo "+origen +"-"+dest);
                model.tramoService.buscarTramo(origen, dest);
                if(model.tramoService.getTramo()!=null){
                    Tramo tramo = model.tramoService.getTramo();
                    acpmmax=tramo.getAcpm();
                    peajeamax= tramo.getCantidad_maximo_ticket1();
                    peajebmax= tramo.getCantidad_maximo_ticket2();
                    peajecmax= tramo.getCantidad_maximo_ticket3();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
    }
    public void buscarAcpm(String nit)throws ServletException{
        try{
            String [] nitcod = null;
            String sucursal="";
            nitcod = nit.split("/");
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            ////System.out.println("Codigo del proveedor "+nitcod[0]+", "+sucursal);
            model.proveedoracpmService.buscaProveedor(nitcod[0], sucursal);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm acpm= model.proveedoracpmService.getProveedor();
                tipo=acpm.getTipo();
                e_max=acpm.getMax_e();
                vacpm = acpm.getValor();
                ////System.out.println("Valor del acpm "+vacpm);
                ////System.out.println("Maximo efectivo "+e_max);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
    }
    
/*public void buscarVPeajes(String nit)throws ServletException{
        try{
            model.proveedortiquetesService.buscaProveedor(nit,);
            if(model.proveedortiquetesService.getProveedor()!=null){
                Proveedor_Tiquetes pt= model.proveedortiquetesService.getProveedor();
                vpeaje= pt.getValor();
 
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
 
    }*/
    
    public int validarFecha()throws ServletException{
        
        int sw=0;
        
        ////System.out.println("VOY A VERIFICAR FECHAS ");
        
        try{
            model.tbltiempoService.buscaTiemposSal(usuario.getBase());
            Vector tiempos = model.tbltiempoService.getTiempos();
            
            for(int i =0; i<tiempos.size();i++){
                Tbltiempo tbl = (Tbltiempo) tiempos.elementAt(i);
                request.setAttribute(""+tbl.getTimeCode(),"#99cc99");
                
            }
            //Recupero las fechas
            
            Calendar fecha = Calendar.getInstance();
            
            int i=0;
            
            Calendar fecha1 = Calendar.getInstance();
            
            model.pla_tiempoService.buscarUltFecha(request.getParameter("pla").toUpperCase());
             
            if(model.pla_tiempoService.getTiempo()!=null){
             
             
                //FECHA DE LA ULTIMA FECHA INGRESADA...
                String fec = model.pla_tiempoService.getTiempo().getDate_time_traffic();
                ////System.out.println("Ultima fecha "+fec);
             
                int year=Integer.parseInt(fec.substring(0,4));
                int month=Integer.parseInt(fec.substring(5,7))-1;
                int date= Integer.parseInt(fec.substring(8,10));
                int hora=Integer.parseInt(fec.substring(11,13));
                int minuto=Integer.parseInt(fec.substring(14,16));
             
                fecha1.set(year,month,date,hora,minuto,0);
                fecha1.set(fecha1.MILLISECOND,0);
             
             
                //RECOJO LA PRIMERA FECHA QUE ME ENVIAN
                Tbltiempo tbl = (Tbltiempo) tiempos.elementAt(i);
             
                ////System.out.println("Fecha que recivo "+request.getParameter(tbl.getTimeCode()));
             
                year=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(0,4));
                ////System.out.println(""+year);
                month=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(5,7))-1;
                ////System.out.println(""+month);
                date= Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(8,10));
                ////System.out.println(""+date);
                hora=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(0,2));
                ////System.out.println(""+hora);
                minuto=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(3,5));
                ////System.out.println(""+minuto);
             
                ////System.out.println("hora "+hora);
                ////System.out.println("minuto "+minuto);
             
                fecha.set(year,month,date,hora,minuto,0);
                fecha.set(fecha.MILLISECOND,0);
             
             
                //////System.out.println("La ultima fecha del despacho es:  "+fecha1);
                ////System.out.println("La fecha siguiente es :  "+fecha.YEAR+"-"+fecha.DATE+"-"+fecha.MONTH);
             
                if(fecha1.after(fecha)){
                    request.setAttribute(""+tbl.getTimeCode(),"#cc0000");
                    sw=1;
                    ////System.out.println("LA PRIMERA FECHA ESTA DESPUES DE LA SEGUNADA ");
                }
                else if(fecha1.equals(fecha)){
                    sw=1;
                    ////System.out.println("LAS FECHAS SON IGUALES....");
                    request.setAttribute(""+tbl.getTimeCode(),"#cc0000");
             
                }
                else{
                    Calendar hoy=Calendar.getInstance();
                    if(fecha.after(hoy)){
                        request.setAttribute(""+tbl.getTimeCode(),"#cc0000");
                        sw=1;
                        ////System.out.println("LA FECHA ESTA DESPUES DE HOY ");
                    }
                }
            }
            for(i =0; i<tiempos.size();i++){
                
                Tbltiempo tbl = (Tbltiempo) tiempos.elementAt(i);
                
                int year=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(0,4));
                int month=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(5,7))-1;
                int date= Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(8,10));
                int hora=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(0,2));
                int minuto=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(3,5));
                
                fecha.set(year,month,date,hora,minuto,0);
                fecha.set(fecha.MILLISECOND,0);
                
                Calendar hoy=Calendar.getInstance();
                if(fecha.after(hoy)){
                    request.setAttribute(""+tbl.getTimeCode(),"#cc0000");
                    sw=1;
                }
                else{
                    int j= i+1;
                    if(j < tiempos.size()){
                        Tbltiempo tbl2 = (Tbltiempo) tiempos.elementAt(j);
                        
                        Calendar fecha2 = Calendar.getInstance();
                        int year2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(0,4));
                        int month2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(5,7))-1;
                        int date2= Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(8,10));
                        int hora2=Integer.parseInt(request.getParameter("h"+tbl2.getTimeCode()).substring(0,2));
                        int minuto2=Integer.parseInt(request.getParameter("h"+tbl2.getTimeCode()).substring(3,5));
                        fecha2.set(year2,month2,date2,hora2,minuto2,0);
                        fecha2.set(fecha2.MILLISECOND,0);
                        
                       
                        if(fecha.after(fecha2)){
                            request.setAttribute(""+tbl2.getTimeCode(),"#cc0000");
                            sw=1;
                        }
                        else if(fecha.equals(fecha2)){
                            sw=1;
                            ////System.out.println("LAS FECHAS SON IGUALES....");
                            request.setAttribute(""+tbl2.getTimeCode(),"#cc0000");
                            
                        }
                    }
                }
                
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        return sw;
    }
    public void run() throws ServletException, InformationException {
        
        String next;
        float full_w=0;
        String sj;
        float pesoreal=0;
        int sw=0;
        int i=0;
        float pesoMax=0;
        
        next="/despacho/InicioMasivo.jsp";
        
        HttpSession session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        try{
            
            //INICIALIZAMOS VALORES
            request.setAttribute("pesoLleno","#99cc99");
            request.setAttribute("pesoVacio","#99cc99");
            request.setAttribute("anticipo","#99cc99");
            request.setAttribute("acpm","#99cc99");
            request.setAttribute("peajea","#99cc99");
            request.setAttribute("peajeb","#99cc99");
            request.setAttribute("peajec","#99cc99");
            request.setAttribute("proveedora","#99cc99");
            request.setAttribute("planilla","#99cc99");
            request.setAttribute("placa","#99cc99");
            //INICIALIZAMOS VALORES
            List listTabla = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
            Iterator itTbla=listTabla.iterator();
            while(itTbla.hasNext()){
                Tbltiempo tbl = (Tbltiempo) itTbla.next();
                request.setAttribute(tbl.getTimeCode()+i,"");
            }
            i++;
            
            //RECOGEMOS VALORES
            float peso = Float.parseFloat(request.getParameter("pesoll"));
            float pesoVacio = Float.parseFloat(request.getParameter("pesov"));
            float anticipo = Float.parseFloat(request.getParameter("anticipo"));
            float gacpm=Float.parseFloat(request.getParameter("gacpm"));
            int peajea = Integer.parseInt(request.getParameter("peajea"));
            int peajeb = Integer.parseInt(request.getParameter("peajeb"));
            int peajec = Integer.parseInt(request.getParameter("peajec"));
            String placa = request.getParameter("placa").toUpperCase();
            String planilla = request.getParameter("pla").toUpperCase();
            String proveedorT = request.getParameter("tiquetes");
            String proveedorAcpm= request.getParameter("proveedorAcpm");
            
            //Valido el peso Vacio, que debe ser menor que el peso lleno
            sw=this.validarFecha();
            sj=this.buscarStandard(planilla);
            full_w=peso;
            this.buscarTramo(sj);
            
            ////System.out.println("El estandard es: "+sj);
            
            model.stdjobcostoService.buscaStdJobCosto(sj);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                pesoMax=stdjobcosto.getPeso_lleno_maximo();
            }
            
            if(full_w>pesoMax){
                sw=1;
                request.setAttribute("pesoLleno","#cc0000");
            }
            
            if(pesoVacio>=full_w){
                request.setAttribute("pesoVacio","#cc0000");
                sw=1;
            }
            else{
                pesoreal=full_w-pesoVacio;
                this.buscarValor(sj,placa,pesoreal);
                
            }
            
            //VALIDO EL ANTICIPO
            ////System.out.println("Voy a buscar el proveedor de acpm");
            this.buscarAcpm(proveedorAcpm);
            float totalacpm =0;
            ////System.out.println("Los galones de Maximos de Acpm "+acpmmax);
            ////System.out.println("Los galones de Acpm son "+gacpm);
            if(tipo.equals("E")){
                totalacpm = vacpm * gacpm;
                if(totalacpm>e_max){
                    request.setAttribute("acpm","#cc0000");
                    sw=1;
                }
            }
            else if(tipo.equals("G")){
                totalacpm = vacpm * gacpm;
                
                if(gacpm>acpmmax){
                    request.setAttribute("acpm","#cc0000");
                    sw=1;
                }
            }
            if(peajea>peajeamax){
                request.setAttribute("peajea","#cc0000");
                sw=1;
            }
            if(peajeb>peajebmax){
                request.setAttribute("peajeb","#cc0000");
                sw=1;
            }
            if(peajec>peajecmax){
                request.setAttribute("peajec","#cc0000");
                sw=1;
            }
            
            int nopeajes = peajec+peajeb+peajea;
            float totalpeajes = vpeaje * nopeajes;
            float antmax = valor*(maxant/100);
            float totalant = totalpeajes+totalacpm+anticipo;
            //this.buscarVPeajes(proveedorT);
            
            
            ////System.out.println("La cantidad de peaje a = "+peajea);
            ////System.out.println("La cantidad de peaje b = "+peajeb);
            ////System.out.println("La cantidad de peaje c = "+peajec);
            ////System.out.println("El valor de la planilla es : "+valor);
            ////System.out.println("El anticipo maximo es : "+maxant);
            ////System.out.println("El total de peajes es : "+totalpeajes);
            ////System.out.println("El total de acpm es : "+totalacpm);
            ////System.out.println("El anticipo  es : "+anticipo);
            
            
            if(antmax<totalant){
                request.setAttribute("anticipo","#cc0000");
                sw=1;
            }
            
            if(sw==1)
                next="/despacho/SalidaError.jsp";
            else
                next="/despacho/SalidaValida.jsp";
            
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
