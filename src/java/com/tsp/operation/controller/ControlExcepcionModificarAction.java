/*
 * PlacaInsertAction.java
 *
 * Created on 4 de noviembre de 2004, 02:48 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  FFERNANDEZ
 */
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import org.apache.log4j.Logger;

public class ControlExcepcionModificarAction extends Action{
    
    //  static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public ControlExcepcionModificarAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/trafico/planviaje/control_excepcion/ModificarControl_execpcion.jsp?reload=ok";
        String cmd = request.getParameter ("cmd");
        BeanGeneral control = new BeanGeneral ();
        Date fecha = new Date ();
        SimpleDateFormat format = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String fechacrea = format.format (fecha);
        String msg = "";        
              HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            control.setValor_01 ((request.getParameter ("codigo")!=null)?request.getParameter ("codigo"):"");
            control.setValor_02 ((request.getParameter ("origen")!=null)?request.getParameter ("origen"):"");
            control.setValor_03 ((request.getParameter ("destino")!=null)?request.getParameter ("destino"):"");
            control.setValor_04((request.getParameter ("agencia")!=null)?request.getParameter ("agencia"):"");
            control.setValor_05 ((request.getParameter ("P_Viaje")!=null)?request.getParameter ("P_Viaje"):"N");
            control.setValor_06 ((request.getParameter ("H_Reporte")!=null)?request.getParameter ("H_Reporte"):"N");
            control.setValor_07((request.getParameter ("Perno")!=null)?request.getParameter ("Perno"):"N");
            
            if (usuario != null){
		//Usuario
                control.setValor_38((usuario.getLogin ()!=null)?usuario.getLogin ():"");
            }
            try{
                model.control_excepcionService.modificarCotrolExcepcion(control);
                model.control_excepcionService.buscarControl((request.getParameter ("codigo")!=null)?request.getParameter ("codigo"):"",
                (request.getParameter ("origen")!=null)?request.getParameter ("origen"):"",
                (request.getParameter ("destino")!=null)?request.getParameter ("destino"):"",
                (request.getParameter ("agencia")!=null)?request.getParameter ("agencia"):"");
                next += "&msg=" + "Control Excepcion fue Modificado Exitosamente";
                    
            }
            catch (Exception e){
                e.printStackTrace ();
                throw new ServletException (e.getMessage ());
            }
        
        // Redireccionar a la p�gina indicada.
         this.dispatchRequest (next);
    }
}
