/***********************************************************************************
 * Nombre clase : ............... CXPBuscarDocumentoAction.java                            
 * Descripcion :................. Clase que maneja Las Acciones Para la busqueda de documentos
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     
 * Fecha :.......................  Created on 10 de octubre de 2005, 11:17 AM               
 * Version :..................... 1.0                                              
 * Copyright :................... Fintravalores S.A.                          
 ***********************************************************************************/



package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  hosorio
 */
public class CXPBuscarDocumentoAction extends Action{
    
    /** Creates a new instance of CXPBuscarFacturas */
    public CXPBuscarDocumentoAction() {
    }
     public void run() throws javax.servlet.ServletException, InformationException {
        String next="/jsp/cxpagar/facturas/";
        HttpSession session = request.getSession();
        String doc = (request.getParameter("doc")!=null)?request.getParameter("doc"):"";
        String pos = (request.getParameter("pos")!=null)?request.getParameter("pos"):"";
        Vector fact = model.cxpDocService.getVectorFacturas();
        CXP_Doc factura = null;
        try{     
            if(doc.equals("factura")) {               
                factura  = (CXP_Doc) fact.elementAt(Integer.parseInt(pos));        
                model.cxpItemDocService.buscarFacturasxUsuario(factura);
                factura.setSeleccionado(false);
                session.setAttribute("factura", factura);
                String Pagador = (request.getParameter("Pagador")!= null)?request.getParameter("Pagador"):"";
                if(Pagador.equals("si")){
                    next+="detalleFactura.jsp?Opcion=ObservacionPagador";
                }else{
                    next+="detalleFactura.jsp";
                }
            } else if(doc.equals("Observacion")) { 
                Vector vecItems = model.cxpItemDocService.getVectorItemsFactura();
                CXPItemDoc item = (CXPItemDoc) vecItems.elementAt(Integer.parseInt(pos));   
                session.setAttribute("item", item);
                model.cxpObservacionItemService.buscarObservacionItem(item);
                CXPObservacionItem observacion = model.cxpObservacionItemService.getCXPObservacionItem();
                session.setAttribute("observacion", observacion);
                
                String TextoActivo = model.cxpObservacionItemService.obtenerTextoActivo(item);
                next+="obsAutorizador.jsp?TextoActivo="+TextoActivo;
                
             } else if(doc.equals("ObservacionPagador")){
                Vector vecItems = model.cxpItemDocService.getVectorItemsFactura();
                CXPItemDoc item = (CXPItemDoc) vecItems.elementAt(Integer.parseInt(pos));   
                session.setAttribute("item", item);
                model.cxpObservacionItemService.buscarObservacionItem(item);
                CXPObservacionItem observacion = model.cxpObservacionItemService.getCXPObservacionItem();
                session.setAttribute("observacion", observacion);
                next+="RespObsPagador.jsp";
             } 
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
