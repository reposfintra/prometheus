/*
 * IdiomaCargarAction.java
 *
 * Created on 22 de febrero de 2005, 10:36 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
//import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class IdiomaCargarAction extends Action{
    
    /** Creates a new instance of IdiomaCargarAction */
    public IdiomaCargarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        HttpSession session = request.getSession();
        String lenguaje = (String)session.getAttribute("idioma");
        if (lenguaje == null){
            lenguaje = request.getParameter("idioma");    
        }    
        String pagina = request.getParameter("pagina");
        String ruta = request.getParameter("ruta");
        String next = ruta + pagina;
        
      
        this.dispatchRequest(next);        
    }
    
}
