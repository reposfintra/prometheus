 /*
 * CaravanaBusquedaAction.java
 *
 * Created on 04 de Agosto de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;

public class EscoltaAsignacionAction extends Action{
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/caravana/asignarEscolta.jsp?agregar=visible&escolta=visible";        
        String planilla = "", escolta = "";
        String error = "";
        int numC = 0;        
        try{ 
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
            //creaci�n de la caravana
            Caravana c = new Caravana();
            String plani = "";
            c.setNumcaravana(numC);
            c.setDstrct(usuario.getDstrct());
            c.setAgencia(usuario.getId_agencia());
            c.setBase(usuario.getBase());
            c.setCreation_user(usuario.getLogin());
            c.setFecinicio(Util.getFechaActual_String(4));            
            Vector datos = model.caravanaSVC.getVectorCaravanaActual();
            for (int i=0; i<1; i++){
                VistaCaravana v = (VistaCaravana)datos.elementAt(i);
                planilla = (request.getParameter("numpla"+i)!=null)?request.getParameter("numpla"+i):"";                
                //System.out.println("numpla "+i+":"+planilla);
                if (!planilla.equals("")) {
                    c.setNumpla(planilla); 
                    c.setNumrem(v.getNumrem());            
                    plani = planilla;
                    if (!model.caravanaSVC.existaPlanillaCaravana(plani)) {
                        model.caravanaSVC.insertarCaravana(c);                                        
                    } else{
                        model.escoltaVehiculoSVC.actualizarPlanillaAsignada(plani);
                    }
                        
                }
            }            
            //Creaci�n del escolta de la caravana
            EscoltaVehiculo e = new EscoltaVehiculo();            
            e.setBase(usuario.getBase());
            e.setCreation_user(usuario.getLogin());
            e.setFecasignacion(Util.getFechaActual_String(4));
            Vector esc = model.escoltaVehiculoSVC.getVectorEscoltas();
            for (int i=0; i<esc.size(); i++){
                escolta = (request.getParameter("placaesc"+i)!=null)?request.getParameter("placaesc"+i):"";                
                //System.out.println("escolta "+i+":"+escolta);
                if (!escolta.equals("")) {
                    e.setPlaca(escolta);
                    //Asociando un escolta a la planilla
                    e.setNumpla(plani);
                    model.escoltaVehiculoSVC.insertarEscoltaVehiculo(e);
                    model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plani,"S");
                }
            }            
            next = "/jsp/trafico/caravana/asignarEscolta.jsp?msg=NONE";
        }catch (Exception e){
            //throw new ServletException(e.getMessage());           
            e.printStackTrace();
        }        
        this.dispatchRequest(next);
    }    
}
