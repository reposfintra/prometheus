/*******************************************************************
 * Nombre:        UsuarioAprobacionManagerAction.java
 * Descripci�n:   Clase Action para aprobar los registros
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         16 de enero de 2006, 10:16 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UsuarioAprobacionManagerAction extends Action{
    
    /** Creates a new instance of UsuarioAprobacionManagerAction */
    public UsuarioAprobacionManagerAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/hvida/aprobacion/";
        String tabla = request.getParameter("tabla");
        Vector vec;
        try{
            if (tabla.equals("NIT")){
                vec = model.identidadService.obtVecIdentidad();
                for(int i=0; i< vec.size(); i++ ){
                    if ( request.getParameter("apro"+i)!=null ){
                        Identidad iden = (Identidad) vec.elementAt(i);
                        model.identidadService.aprobarRegistrio(usuario.getLogin(), request.getParameter("apro"+i), iden.getCedula() );
                        if (model.conductorService.existeConductor(iden.getCedula())){
                            model.conductorService.cambiarEstadoConductor(iden.getCedula(), "A");
                        }
                        model.identidadService.listarIdentidadNOAprobada(usuario.getLogin());
                    }
                }
                next=next+"aprobarIdentidades.jsp";
            }
            else if (tabla.equals("PLACA")){
                vec = model.placaService.getVPlacas();
                for(int i=0; i< vec.size(); i++ ){
                    if ( request.getParameter("apro"+i)!=null ){
                        Placa pla = (Placa) vec.elementAt(i);
                        model.placaService.aprobarRegistrio(usuario.getLogin(), request.getParameter("apro"+i), pla.getPlaca() );
                        model.placaService.cambiarEstadoPlaca(pla.getPlaca(), "A");
                        model.placaService.listarPlacaNoAprobada(usuario.getLogin());
                    }
                }
                next=next+"aprobarPlacas.jsp";
            }
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
