/*
 * FacturaMigracionCargarAction.java
 *
 * Created on 26 de noviembre de 2006, 11:04 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  dbastidas
 */
public class FormularioCargarCamposAction extends Action{
    
    /** Creates a new instance of FacturaMigracionCargarAction */
    public FormularioCargarCamposAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        HttpSession session = request.getSession();
        String opcion  = request.getParameter("opcion")==null?"":request.getParameter("opcion");
        try{
            if(opcion.equals("CFormulario")){
                model.formato_tablaService.camposFormulario(request.getParameter("formato"));
                Vector campos = model.formato_tablaService.getVecCampos();
                if(campos.size()>0){
                    Formato_tabla reg = (Formato_tabla) campos.get(0);
                    if(reg.getTabla().equals("factura_migracion")){
                        model.formato_tablaService.setVecCampos( model.formularioService.eliminarCamposFacturaMigracion( model.formato_tablaService.getVecCampos() ) );
                    }
                }
            }
            else if(opcion.equals("CBUSQUEDA")){
                model.formato_tablaService.camposFormularioBusqueda(request.getParameter("formato"));
                Vector campos = model.formato_tablaService.getVecbusqueda();
                System.out.println("Cantidad "+campos.size());
                if(campos.size()>0){
                    Formato_tabla reg = (Formato_tabla) campos.get(0);
                    if(reg.getTabla().equals("factura_migracion")){
                        model.formato_tablaService.setVecbusqueda( model.formularioService.eliminarCamposFacturaMigracion( model.formato_tablaService.getVecbusqueda() ) );
                    }
                }
                
            }
            
            System.out.println("NEXT "+ next);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
