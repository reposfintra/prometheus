/*
 * RemesaSearchAction.java
 *
 * Created on 14 de diciembre de 2004, 01:02 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class RemesaPadreInsertAction extends Action{
    
    /** Creates a new instance of RemesaSearchAction */
    public RemesaPadreInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next = "/colpapel/agregarOTpadre.jsp";
        String no_rem= request.getParameter("numrem").toUpperCase();
        String no_padre = request.getParameter("numpadre")!=null?request.getParameter("numpadre").toUpperCase():"";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        //Vector remesas = (Vector)session.getAttribute("hijas");
        Vector remesas = new Vector();
        String[] num_remesas = null;
        String asignadas = "";
        String mensaje = "";
        
        boolean tienepadre = false;
        
        try{
            
            
            if(model.remesaService.estaPadre(no_padre)){                
                if( no_rem.length()>0 ){
                    num_remesas = no_rem.split(",");
                }
                
                if( num_remesas != null ){
                    Remesa rem = new Remesa();
                    for( int i=0; i<num_remesas.length; i++){
                        model.remesaService.buscaRemesa( num_remesas[i] );
                        rem = model.remesaService.getRemesa();
                        if( rem != null ){
                            if( rem.getPadre().equals("") ){
                                remesas.add( rem );
                            }else{
                                tienepadre = true;
                                mensaje +=  rem.getNumrem()+"  ";
                            }
                        }
                    }
                }
                
                if( tienepadre == false ){
                    if( remesas != null && remesas.size()>0  ){
                        for(int i=0; i<remesas.size(); i++){
                            Remesa rem = (Remesa) remesas.get(i);
                            rem.setPadre(no_padre);
                            rem.setDistrito(usuario.getDstrct());
                            rem.setUsuario(usuario.getLogin());
                            rem.setBase(usuario.getBase());
                            rem.setDescripcion(request.getParameter("texto"));
                            model.remesaService.asignarPadre(rem);
                            asignadas += rem.getNumrem()+", ";
                        }
                        
                        
                    }
                    if( asignadas.length()>3 ){ asignadas=asignadas.substring(0, ( asignadas.length() - 2) );}
                    next = "/colpapel/agregarOTpadre.jsp?mensaje=Se Asigno la remesa padre: "+no_padre+" a la(s) remesa(s): "+asignadas;
                    no_rem="";
                }else{
                    next = "/colpapel/agregarOTpadre.jsp?mensaje=La(s) remesa(s) "+ mensaje + " ya est�(n) asociada(s) a una remesa padre";
                    no_rem="";
                }
                
            }
            else{
                next = "/colpapel/agregarOTpadre.jsp?mensaje=La remesa padre no existe.";
            }
            session.removeAttribute("hijas");

        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
