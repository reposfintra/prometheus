/********************************************************************
 *      Nombre Clase.................   AplicaciobUpdateAction.java    
 *      Descripci�n..................   Anula un registro en la tabla otros_conceptos
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   27.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OtrConceptosAnularAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public OtrConceptosAnularAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblotros_conceptos/otrconceptUpdate.jsp?mensaje=MsgModificado&estado=Anulado";
                String next = "";
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Concepto concepto = new Concepto();
                        concepto.setBase(usuario.getBase());
                        concepto.setNtabla(request.getParameter("tabla"));
                        concepto.setNdescripcion(request.getParameter("descripcion"));                        
                        concepto.setDistrito((String) session.getAttribute("Distrito"));
                        concepto.setUsuario_creacion(usuario.getLogin());
                        concepto.setUsuario_modificacion(usuario.getLogin());
                        concepto.setEstado("");
                        concepto.setC_tabla(request.getParameter("tabla"));
                        concepto.setC_descripcion(request.getParameter("descripcion"));                        
                                                                                        
                        concepto.setEstado("A");
                        model.otros_conceptosSvc.actualizarConcepto(concepto);                                              
                        
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Definici�n Otros Conceptos");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
