
package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;


public class EsquemaChequeManagerAction extends Action{
    
    public void run() throws ServletException, InformationException{
       try{
         HttpSession session = request.getSession();
         Usuario usuario     = (Usuario) session.getAttribute("Usuario");
         String login        = usuario.getLogin();
         String next         = "";
         
         
         String evento  = request.getParameter("evento"); 
         
         
        if( evento !=null ){
            
            
           String desactivo     = "true";
           String activo        = "false";
            
     //--- variables de bloqueos       
          
           String bloqueoSearch = activo;
           String bloqueoInsert = activo;
           String bloqueoUpdate = desactivo;
           String bloqueoDelete = desactivo;
           
           String comentario    = "";
           
          
       //--- ok
           
           
                  if(evento.equals("INSERT")){
                      EsquemaCheque esquema = EsquemaCheque.getObject(request);
                      EsquemaCheque auxiliar = model.EsquemaChequeSvc.searchEsquema(esquema);
                      if(auxiliar==null){
                        model.EsquemaChequeSvc.insert(esquema, login,usuario.getBase());
                        comentario="Su esquema se ha insertado satisfactotiamente...";                
                      }
                      else
                        comentario="La inserción del esquema no se pudo realizar, ya que el esquema  se encuentra registrado, si lo desea puede Modificarlo y/o Eliminarlo";
                      model.EsquemaChequeSvc.reset();  
                   }
           
           

                   if(evento.equals("SUBIR")){
                          EsquemaCheque esquema = EsquemaCheque.getObject2(request);
                          model.EsquemaChequeSvc.setEsquema(esquema);
                          List listaNula=null;
                          model.EsquemaChequeSvc.setList(listaNula);
                          bloqueoInsert = desactivo;
                          bloqueoSearch = desactivo;
                          bloqueoUpdate = activo;
                          bloqueoDelete = activo; 
                   }

           
           
                   if(evento.equals("UPDATE")){
                         EsquemaCheque esquema = EsquemaCheque.getObject(request);
                         model.EsquemaChequeSvc.update(esquema);             
                         comentario="El esquema " + esquema.getDescripcion() +" para el banco " + esquema.getBanco() + " ha sido Actualizado correctamente...";
                         model.EsquemaChequeSvc.reset(); 
                         model.EsquemaChequeSvc.setEsquema(esquema);
                         bloqueoInsert = desactivo;
                         bloqueoUpdate = activo;
                         bloqueoDelete = activo;
                   }


                   if(evento.equals("DELETE")){     
                      EsquemaCheque esquema = EsquemaCheque.getObject(request);
                      model.EsquemaChequeSvc.delete(esquema);
                      comentario="El esquema " + esquema.getDescripcion() +" para el banco " + esquema.getBanco() + " ha sido Eliminado completamente...";
                      model.EsquemaChequeSvc.reset();
                   }
           
           


                  //-- ok
                   if(evento.equals("LIST")){
                       List lista = model.EsquemaChequeSvc.search();
                       EsquemaCheque esquema =null;
                       model.EsquemaChequeSvc.setEsquema(esquema);
                       if(lista==null || lista.size()==0)
                         comentario="No hay esquema de cheque para listar...";
                       else{
                          bloqueoInsert = desactivo;
                          bloqueoSearch = desactivo;
                       }
                   }


           
                 //--- ok
                   if(evento.equals("NEW") ){
                      model.EsquemaChequeSvc.reset();
                   }
           

                   next = "/cheques/EsquemaCheque.jsp?estadoInsert="+ bloqueoInsert +"&estadoSearch="+bloqueoSearch+"&estadoDelete="+ bloqueoDelete+"&estadoUpdate="+bloqueoUpdate +"&comentario="+comentario; 
       
        
        }else
            next ="/index.jsp";
         
         
           
    //--- LLamamos la pagina JSP  
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
        }
        catch(Exception e){
          throw new ServletException(e.getMessage());
        } 
    }
}
