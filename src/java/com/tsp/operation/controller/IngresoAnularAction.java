/*************************************************************************
 * Nombre:        IngresoanularAction.java
 * Descripci�n:   Clase Action para anular el ingreso de pagos de clientes
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         24 de julio de 2006, 05:27 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class IngresoAnularAction extends Action{
    
    /** Creates a new instance of IngresoAnularAction */
    public IngresoAnularAction() {
    }
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        try{
            Ingreso ing = new Ingreso();
            ing.setDstrct(usuario.getDstrct());
            ing.setUser_update(usuario.getLogin());
            ing.setNum_ingreso(request.getParameter("num"));
            ing.setTipo_documento(request.getParameter("tipodoc"));
            
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            String [] QuerysVector=model.ingresoService.anularIngreso(ing).split(";");
            for (String QuerysVector1 : QuerysVector) {
                tService.getSt().addBatch(QuerysVector1);
            }
            tService.execute();
            
            this.dispatchRequest(next);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
