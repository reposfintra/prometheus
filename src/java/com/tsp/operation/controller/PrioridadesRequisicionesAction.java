/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.PrioridadesRequisicionesDAO;
import com.tsp.operation.model.DAOS.impl.PrioridadesRequisicionesImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author egonzalez
 */
public class PrioridadesRequisicionesAction extends Action {

    private final int BUSCAR_REQUISICIONES = 0;
    private final int MODIFICAR_PRIORIDADES = 1;
    private final int GET_FILTROS = 2;
    private final int GET_USUARIOS_PROCESO = 3;

    private JsonObject respuesta = null;
    private PrioridadesRequisicionesDAO dao;
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            dao = new PrioridadesRequisicionesImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case BUSCAR_REQUISICIONES:
                    buscar_requisiciones();
                    break;
                case MODIFICAR_PRIORIDADES:
                    modificar_prioridades();
                    break;
                case GET_FILTROS:
                    getFiltros();
                    break;
                case GET_USUARIOS_PROCESO:
                    getUsuariosProceso();
                    break;
                default:
                    throw new Exception("La opcion de orden " + opcion + " no valida");
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                response.setContentType("application/json; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println((new Gson()).toJson(respuesta));
            } catch (Exception e) {
            }
        }
    }

    private void buscar_requisiciones() {
        JsonObject info = new JsonObject();
        try {
            info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        } catch(NullPointerException exception ) {   }
        respuesta = dao.buscar_requisiciones(info);
    }

    private void modificar_prioridades() {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion")); 
        info.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        respuesta = dao.modificar_prioridades(info);
    }
    
    public void getFiltros() {
        String consulta = request.getParameter("consulta");
        respuesta = dao.getFiltro(consulta);
    }
    
    public void getUsuariosProceso() {
        String idProceso = request.getParameter("idProceso");
        respuesta = dao.getUsuariosProceso(idProceso);
    }
}
