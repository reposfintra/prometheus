/*
 * MenuArchivoTXTAction.java
 *
 * Created on 21 de diciembre de 2004, 08:57 AM
 */

package com.tsp.operation.controller;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  mcelin
 */
public class PlacaMigracionAction extends Action {
    
    /** Creates a new instance of MenuSeriesAction */
    public PlacaMigracionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            String Fecha = request.getParameter("Fecha");
            HPlaca Placa = new HPlaca();
            Placa.start(model,Fecha);
                    
            final String next = "/migracion/mostrar.jsp?Mensaje='Migración de Placas'";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException("Error en la Accion [MenuPlacaAction]...\n"+e.getMessage());
        }
    }
}

