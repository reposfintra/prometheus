/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.lang.*;
import com.tsp.operation.model.threads.*;
import java.util.Date.*;

/**
 *
 * @author  jdelarosa
 */
public class TrasladoAprobacionAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public TrasladoAprobacionAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fecI = request.getParameter ("fechai")!=null?request.getParameter ("fechai"):"";
        String fecF = request.getParameter ("fechaf")!=null?request.getParameter ("fechaf"):"";
        String bdestino = request.getParameter ("banco_destino")!=null?request.getParameter ("banco_destino"):"";
        String borigen = (request.getParameter("banco_origen")!=null)?request.getParameter ("banco_origen"):"";
       
        
        
        HttpSession session = request.getSession ();
        String next = "";
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        String []a = request.getParameterValues("aprobados");
        Date fecha = new Date ();
        SimpleDateFormat format = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String fechacrea = format.format (fecha);
        
        String a�oActual = fechacrea.substring(2,4);
        String mesActual = ""+ fechacrea.substring(5,7);
        String bancoOrigen = "";
        String bancoDestino = "";
        String sucursalOrigen = "";
        String sucursalDestino = "";
        String numeroCuenta= "";
        double valor= 0;
        String valores ="";
        boolean sw1;
        
        
        
        
        /*********************************************************/
        //nuevo traslado bancario
        /***********************************************************/
        String A�o = fechacrea.substring(0,4);
        String Mes = fechacrea.substring(5,7);
        String periodo = A�o + Mes; 
        String msg = "";        
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        
        /****************************************************************/
        
        
        try{
         if(model.trasladoService.existe_traslado_aprobado(fecI, fecF, borigen, bdestino) && a != null){
                     //System.out.println("entro al existe");
                     String sql ="";
                     //variable grupotrasacion 
                     int grupo_transaccion= modelcontab.comprobanteService.getGrupoTransaccion()+1;
                     //System.out.println("grupo transacion   =   "+grupo_transaccion);
                      //creacion de serie
                      String codigoSerie = "";
                      String numAliatorio= "";
                      String detalle = "";

                      //busco el numero de serie
                      


                      /***************************************************************/
                      //secuencia de busqueda para cambio comprobante y comprodet
                      /****************************************************************/
                      int cont=0;  

                      for(int i = 0 ; i< a.length; i++){
                          String llave = a[i];
                          model.trasladoService.Traslado_buscar_serie();
                          Vector vectoresSeries =  model.trasladoService.getVectorSerie();

                         if( vectoresSeries != null && vectoresSeries.size() > 0 ){
                            BeanGeneral serie = (BeanGeneral) vectoresSeries.elementAt(0);
                            String prefijo = serie.getValor_01();
                            String a�o = serie.getValor_02();
                            String mes = serie.getValor_03();
                            int numero = Integer.parseInt(serie.getValor_04());
                            int suma = numero + 1;

                            //incremento el codigo de tipo de serie de la tabla tipo docto
                            model.trasladoService.Incrementar_tipo_docto(suma);
                            String numeroSum= ""+suma;
                            String contador= "";
                            int tama�o = Integer.parseInt(serie.getValor_05());
                            if((a�o.equals("AA")) && (mes.equals("MM"))){
                                for(int z= 0; z < tama�o ; z++){
                                    contador += ""+0; 
                                }
                            String tama�oCero = contador.substring(0, contador.length() - numeroSum.length());

                            numAliatorio = tama�oCero + ""+numeroSum;
                        }
                        //codigo de seirie completo
                        codigoSerie = ""+prefijo + ""+a�oActual + ""+mesActual + ""+numAliatorio;
                      }

                            //realizo la busqueda del traslado bancario sus datos
                            model.trasladoService.Buscar_traslado_x_id(llave);

                            //CARGO EL VECTOR busqueda del traslado bancario sus datos POR LA LLAVE
                            Vector vectorCargar = model.trasladoService.getVectorTraslado();
                            if( vectorCargar != null && vectorCargar.size() > 0 ){
                                BeanGeneral trasladoBancario = (BeanGeneral) vectorCargar.elementAt(0);


                                bancoOrigen = trasladoBancario.getValor_01();
                                sucursalOrigen = trasladoBancario.getValor_02();
                                bancoDestino = trasladoBancario.getValor_03();
                                sucursalDestino = trasladoBancario.getValor_04();
                                valor = Double.parseDouble(trasladoBancario.getValor_05()); 
                                valores = ""+valor;
                              //ingesa el codigo de serie a la tabla traslado bancario campo comprobante
                                model.trasladoService.modificar_traslado_comprobante(bancoOrigen, bancoDestino, sucursalOrigen, sucursalDestino, valores, codigoSerie);
                                //busco el numero de  la cuenta del banco  
                                detalle = ""+bancoOrigen+" - "+sucursalOrigen+" => "+bancoDestino+"-"+sucursalDestino;
                                model.trasladoService.Buscar_cuenta_banco(bancoOrigen, sucursalOrigen);
                               
                                     BeanGeneral traslado = new BeanGeneral ();
     
                                    traslado.setValor_01(usuario.getDstrct());//distrito
                                    traslado.setValor_02("CDF");
                                    traslado.setValor_03(usuario.getId_agencia());
                                    traslado.setValor_04(periodo);
                                    traslado.setValor_05(usuario.getPerfil());
                                    traslado.setValor_06(usuario.getLogin());
                                    traslado.setValor_07(usuario.getBase());
                                    traslado.setValor_08("CDF");
                                    traslado.setValor_09(trasladoBancario.getValor_03());
                                    traslado.setValor_10(trasladoBancario.getValor_04());
                                    
                                    
                                    //creo en la tabla comprobante
                                    model.trasladoService.agregarComprobante(grupo_transaccion, codigoSerie, detalle, valor, valor,traslado);
                                    cont++;
                                 
                                //saco el numero de la cuenta 
                                Vector vecCuentaBanco = model.trasladoService.getVectorBanco();
                                if( vecCuentaBanco != null && vecCuentaBanco.size() > 0 ){
                                  Banco bancoCuenta = (Banco) vecCuentaBanco.elementAt(0);
                                  numeroCuenta= bancoCuenta.getCodigo_cuenta();
                               }   
                            }
                        sql += model.trasladoService.Traslado_cambio_aprobado(llave);

                        /*almacenar en la tabla comprobante */
                        detalle = "";
                        detalle = ""+bancoOrigen + "-"+ sucursalOrigen;
                        double valor2= 0;
                        BeanGeneral trasladoDet = new BeanGeneral ();

                        trasladoDet.setValor_01(usuario.getDstrct());//distrito
                        trasladoDet.setValor_02("CDF");
                        trasladoDet.setValor_03(usuario.getId_agencia());
                        trasladoDet.setValor_04(periodo);
                        trasladoDet.setValor_05(usuario.getPerfil());
                        trasladoDet.setValor_06(usuario.getLogin());
                        trasladoDet.setValor_07(usuario.getBase());
                        trasladoDet.setValor_08("CDF");
                        trasladoDet.setValor_09(bancoOrigen);
                        trasladoDet.setValor_10(sucursalOrigen);
                        trasladoDet.setValor_11(bancoDestino);
                        trasladoDet.setValor_12(sucursalDestino);
                        trasladoDet.setValor_13("");  
                        
                        /******************************************************/
                        //agrega origen
                        /***************************************************/
                        model.trasladoService.agregarComprodect(grupo_transaccion, codigoSerie, detalle, valor2, valor,trasladoDet);
                        /******************************************************/
                        //agerega destino
                        /***************************************************/
                        detalle = "";
                        detalle = ""+bancoDestino + "-"+ sucursalDestino;
                        trasladoDet.setValor_13("1");
                        
                        model.trasladoService.agregarComprodect(grupo_transaccion, codigoSerie, detalle, valor, valor2,trasladoDet);
                      }//cierra for
                      
                      
                      TransaccionService tService = new TransaccionService(usuario.getBd());
                      tService.crearStatement();
                      tService.getSt().addBatch(sql); 
                      tService.execute();

                      String fechaI =(String) session.getAttribute("fechaI");  
                      String fechaF =(String) session.getAttribute("fechaF");
                      String banco_Origen = (String) session.getAttribute("banco_Origen");
                      String banco_Destino = (String) session.getAttribute("banco_Destino");


                      model.trasladoService.BusquedaTraslado(fechaI, fechaF, banco_Origen, banco_Destino);
                      next="/jsp/finanzas/contab/comprobante/traslados/vertraslado.jsp?fechaI="+fechaI+"&fechaF="+fechaF+"&banco_Destino="+banco_Destino+"&banco_Origen="+banco_Origen+"&msg= se hizo el traslado satisfactoriamente";
         }               
         else {
                      String fechaI =(String) session.getAttribute("fechaI");  
                      String fechaF =(String) session.getAttribute("fechaF");
                      String banco_Origen = (String) session.getAttribute("banco_Origen");
                      String banco_Destino = (String) session.getAttribute("banco_Destino");
                      model.trasladoService.BusquedaTraslado(fechaI, fechaF, banco_Origen, banco_Destino);
                      next="/jsp/finanzas/contab/comprobante/traslados/vertraslado.jsp?fechaI="+fechaI+"&fechaF="+fechaF+"&banco_Destino="+banco_Destino+"&banco_Origen="+banco_Origen+"&msg= no hay aprobados para los traslados bancarios";
         }//cierra else
         
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en la TrasladoAprobacionAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}

//Fily 23 Marzo 2007
