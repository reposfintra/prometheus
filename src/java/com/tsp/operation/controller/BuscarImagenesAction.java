/***************************************
    * Nombre Clase ............. BuscarImagenesAction.java
    * Descripci�n  .. . . . . .  Busqueda de Imagenes
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.controller;





import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;





public class BuscarImagenesAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {
         try{
             String checkboxActividad     = request.getParameter("byActividad");
             String checkboxTipoDocumento = request.getParameter("bytipoDocumento");
             String checkboxDocumento     = request.getParameter("bydocumento");             
             String checkboxAgencia       = request.getParameter("byAgencia");
             String checkboxFecha         = request.getParameter("byFecha");
             String checkboxCantidad      = request.getParameter("byCantidad");
             
             
             String actividad             = request.getParameter("actividad");
             String tipoDoc               = request.getParameter("tipoDocumento");             
             String agencia               = request.getParameter("agencia");
             
             String user                  = request.getParameter("usuario");
             
             
             String Actividad             = actividad.split("-")[0];
             String TipoDocumento         = tipoDoc.split("-")[0]; 
             String Documento             = request.getParameter("documento");
             String Agencia               = agencia.split("-")[0];
             String fechaIni              = request.getParameter("fechaInicio");
             String fechaFin              = request.getParameter("fechaFinal");
             String cantidad              = request.getParameter("cantidad");
             
             
             String mensaje               = " ";
             
             
             model.ImagenSvc.searchImagen( checkboxActividad    , 
                                           checkboxTipoDocumento, 
                                           checkboxDocumento    ,
                                           checkboxAgencia      , 
                                           checkboxFecha        ,
                                           checkboxCantidad     ,
                                           
                                           Actividad            , 
                                           TipoDocumento        ,
                                           Documento            ,
                                           Agencia              ,
                                           fechaIni             ,
                                           fechaFin             ,
                                           cantidad             ,
                                           user 
                                         );
             
             
             
             List lista  = model.ImagenSvc.getImagenes();
             
             if(lista==null  || lista.size()==0)
                 mensaje = "No se encontraron Imagenes para Mostrar....";
             
             String next = "/imagen/Busqueda.jsp?comentario="+mensaje;
             RequestDispatcher rd = application.getRequestDispatcher(next);
             if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
             rd.forward(request, response);              
             
         }catch(Exception e){
             throw new ServletException(e.getMessage());
         }
        
    }
    
}
