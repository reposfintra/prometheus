/***********************************************
 * Nombre clase: Cumplido_docCargarAction.java
 * Descripci�n: Accion para insertar un docuemnto cumplido
 * Autor: Jose de la Rosa
 * Fecha: 27 de octubre de 2005, 06:19 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class Cumplidos_DocumentosInsertAction extends Action{
    public static String numpla = "";
    public static String numrem = "";
    public static String document_type = "";
    public static String std_job_no = "";
    public static String distrito = "";
    /** Creates a new instance of Cumplidos_DocumentosInsertAction */
    public Cumplidos_DocumentosInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        numpla = request.getParameter ("numpla");
        numrem = request.getParameter ("numrem");
        String standar = request.getParameter ("standar");
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String dstrct = (String) session.getAttribute ("Distrito");
        String []LOV = request.getParameterValues ("LOV");
        boolean cond = false;
        int sw=0;
        try{
            for(int i=0;i<LOV.length;i++){
                this.Conversion (LOV[i]);
                if(document_type.equals ("008")){ 
                    cond = true;
                }
                /*Cumplidos_documentos cumplidos_documentos = new Cumplidos_documentos ();
                cumplidos_documentos.setNumpla (numpla);
                cumplidos_documentos.setNumrem (numrem);
                cumplidos_documentos.setDocument_type (document_type);
                cumplidos_documentos.setStd_job_no (std_job_no);
                cumplidos_documentos.setDistrito (distrito);
                cumplidos_documentos.setUsuario_creacion (usuario.getLogin ().toUpperCase ());
                cumplidos_documentos.setUsuario_modificacion (usuario.getLogin ().toUpperCase ());
                cumplidos_documentos.setBase (usuario.getBase ().toUpperCase ());
                try{
                    if(!document_type.equals ("008")){
                        model.cumplidos_DocumentosService.insertCumplidos_Documentos (cumplidos_documentos);
                    }
                }catch(SQLException e){
                    sw=1;
                }
                if(sw==1){
                    if(!model.cumplidos_DocumentosService.existCumplidos_Documentos (numpla, numrem, document_type, std_job_no, distrito)){
                        model.codigo_demoraService.updateCodigo_demora(codigo, descripcion, usuario.getLogin().toUpperCase(),usuario.getBase());
                        request.setAttribute ("mensaje","El documento cumplido se ha insertado con �xito!");
                    }
                    else{
                        request.setAttribute ("mensaje","Uno de los documentos ya fue ingresado!");
                    }
                }
                else{
                    if(!document_type.equals ("008")){
                        request.setAttribute ("mensaje","El documento cumplido se ha insertado con �xito!");
                    }
                }
                request.setAttribute ("mensaje","Se agregaron documentos");
                */
            }
            if(cond == true)
                next = "/controller?estado=Cumplido_doc&accion=Cargar&rem="+numrem+"&pla="+numpla;
            else
                next = "/jsp/cumplidos/cumplido/cumplido_DocumentosInsertar.jsp?sw=1&numpla="+numpla+"&numrem="+numrem+"&standar="+standar;
        }catch(Exception e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    public static void Conversion (String f){
        String vF [] = f.split (",");
        numpla = vF[0];
        numrem = vF[1];
        document_type = vF[2];
        std_job_no = vF[3];
        distrito = vF[4];
    }
}
