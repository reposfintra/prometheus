/*************************************************************
 * Nombre      ............... SoporteClienteAction.java
 * Descripcion ............... Clase de manipulacion de
 *                             operaciones de asignacion de clientes a los soportes
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 21 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;

public class SoporteClienteAction extends Action {
    
    /** Creates a new instance of SoporteClienteAction */
    public SoporteClienteAction() {
    }
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */ 
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        try{
            
            HttpSession session = (HttpSession) request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            String Opcion = request.getParameter("Opcion");
            String []Soporte  = request.getParameterValues("Soportes");
            String []Clientes = request.getParameterValues("Clientes");
            
            if (Opcion!=null){
                if (Opcion.equals("Init")){
                    model.SoporteClienteSvc.buscarClientes();
                    model.SoporteClienteSvc.buscarSoportes();
                }
                else if (Opcion.equals("Grabar")){
                    
                    if (Soporte!=null){
                        model.SoporteClienteSvc.addRelacion(Soporte, Clientes, usuario.getLogin());
                        model.SoporteClienteSvc.buscarRelaciones();
                    }
                }
                
            }
            
            final String next = "/jsp/masivo/asignaciones/SoportesClientes.jsp?msg=Fue agregado el soporte al Cliente existosamente";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
    }
    
}
