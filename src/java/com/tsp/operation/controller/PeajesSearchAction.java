/*
 * PeajesSearchAction.java
 *
 * Created on 6 de diciembre de 2004, 08:22 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import com.tsp.exceptions.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  KREALES
 */
public class PeajesSearchAction extends Action{
    
    /** Creates a new instance of PeajesSearchAction */
    public PeajesSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/peajes/peajeUpdate.jsp";
        if(request.getParameter("num").equals("2"))
            next="/peajes/peajeDelete.jsp";
        try{
            String codigo= request.getParameter("tiket");
            model.peajeService.buscar(codigo);
            if(model.peajeService.get()!=null){
                Peajes peaje= model.peajeService.get();
                request.setAttribute("peajes",peaje);
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
