/************************************************************************************
 * Nombre clase : ............... EscoltaProveedorInsertAction.java                 *
 * Descripcion :................. Clase que maneja los eventos relacionados con el  *
 *                                ingreso de Proveedores de escolta                 * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 26 de Noviembre de 2005, 11:30 AM                 *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ***********************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.ReporteExportarFlotaUtilizadaXLS;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class FlotaUtilizadaReporteAction extends Action{    
    
    public FlotaUtilizadaReporteAction() {
    }    
    public void run() throws javax.servlet.ServletException, InformationException {      
        String next = "/jsp/masivo/reportes/utilizacionDeFlotas.jsp?msg=";
        String fechai = request.getParameter("fechai");        
        String fechaf = request.getParameter("fechaf");        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");    
        Vector flotas = null;
        try{
            //Buscamos las planillas entregadas para guardarlas en la BD
            model.flotaUtilizadaService.buscarPlanillasEntregadas(fechai, fechaf);
            flotas = model.flotaUtilizadaService.getVectorflotas();
            for (int i=0; i<flotas.size(); i++) {
                FlotaUtilizada fl = (FlotaUtilizada) flotas.elementAt(i);
                model.flotaUtilizadaService.ingresarFlotaUtilizada(fl);
            }
            flotas = null;
            //Buscamos las planillas Despachadas para guardarlas en la BD
            model.flotaUtilizadaService.buscarPlanillasSalidas(fechai, fechaf);
            flotas = model.flotaUtilizadaService.getVectorflotas();
            for (int i=0; i<flotas.size(); i++) {
                FlotaUtilizada fl = (FlotaUtilizada) flotas.elementAt(i);
                model.flotaUtilizadaService.ingresarFlotaUtilizada(fl);
            }
            //reseteamos el vector de flota y cargamos el vector para el reporte            
            model.flotaUtilizadaService.buscarVehiculosConEntradaSalida();
            model.flotaUtilizadaService.buscarVehiculosConEntradaSinSalida();
            model.flotaUtilizadaService.buscarVehiculosConSalidaSinEntrada();
            flotas = model.flotaUtilizadaService.getVectorflotas();
            Vector resumen = model.flotaUtilizadaService.getResumenFlota();
            ReporteExportarFlotaUtilizadaXLS hilo = new ReporteExportarFlotaUtilizadaXLS();
            hilo.start(usuario.getLogin(), flotas, resumen, fechai, fechaf);
            next+="Reporte Creado Exitosamente";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
