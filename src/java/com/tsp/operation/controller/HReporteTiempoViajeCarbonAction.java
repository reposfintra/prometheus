/***********************************************************************************
 * Nombre clase : ............... HReporteTiempoViajeCarbon.java                   *
 * Descripcion :................. Controlador de reporte tiempo viaje carbon       *
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 21 de noviembre de 2005, 01:33 PM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
/**
 *
 * @author  dbastidas
 */
public class HReporteTiempoViajeCarbonAction extends Action {
    
    /** Creates a new instance of HReporteTiempoViajeCarbon */
    public HReporteTiempoViajeCarbonAction() {
    }
   
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        String FechaI =request.getParameter("fechai");
        String FechaF =request.getParameter("fechaf"); 
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            //System.out.println("Usuario "+usuario.getLogin());
            next = "/jsp/masivo/informes/TiempoViajeCarbon.jsp?msg=Su reporte ha iniciado";
            
            Date fi = new Date (FechaI.replaceAll("-","/") + " 00:00:00");
            Date ff = new Date (FechaF.replaceAll("-","/") + " 00:00:00");
            
            long diferencia = ( ff.getTime() - fi.getTime() ) / (1000*60*60*24);
            //System.out.println("Diferencia "+diferencia);
            if ( diferencia >= 0 && diferencia <= 5   ){
                ReporteTiempoViajeCarbonXLS tiempoviaje = new ReporteTiempoViajeCarbonXLS();
                tiempoviaje.start(FechaI, FechaF, usuario.getLogin() );
                //System.out.println("Inicia proceso");
            }
            else
                next = "/jsp/masivo/informes/TiempoViajeCarbon.jsp?msg=El rango maximo de dias es de 5";
            
            
        }catch (Exception ex){
            throw new ServletException("Error al Generar el Reporte  .....\n"+ex.getMessage());
        }

        this.dispatchRequest(next);  

    }
}
