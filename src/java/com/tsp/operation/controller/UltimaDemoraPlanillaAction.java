 /********************************************************************
 *      Nombre Clase.................   ConsultarDemoraPlanilla.java    
 *      Descripci�n..................   Obtiene la ultima planilla de un veh�culo
 *                                      y redirecciona a la p�gina del listado de demoras de planilla
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;


public class UltimaDemoraPlanillaAction extends Action{
    
    /** Creates a new instance of UltimaDemoraPlanillaAction */
    public UltimaDemoraPlanillaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next = "/jsp/trafico/demora/consultarDemoraPlanillaListar.jsp";
        String plaveh = request.getParameter("plaveh");
        HttpSession session = request.getSession();
        
        try{
            String numpla = model.demorasSvc.ultimaPlanillaVehiculo(plaveh);                        
            next += "?numpla=" + numpla;
            
            if( numpla.length()==0 ){
                next = "/jsp/trafico/demora/consultarDemoraPlaca.jsp?msg="
                    + "No se encuentran planillas asociadas al numero de placa: " + plaveh; 
            } else {
                session.removeAttribute("numplaDemoraListar");
                session.setAttribute("numplaDemoraListar", numpla);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
