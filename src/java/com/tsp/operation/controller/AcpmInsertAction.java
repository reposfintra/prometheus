/*
 * AcpmInsertAction.java
 *
 * Created on 1 de diciembre de 2004, 03:52 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class AcpmInsertAction extends Action{
    
    /** Creates a new instance of AcpmInsertAction */
    public AcpmInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        
        String nit= request.getParameter("nit");
        request.setAttribute("nit", "ECE0D8");
        String next = "/acpm/acpmInsert.jsp?msg=";
        String codigo=request.getParameter("sucursal");
        float porcentaje = Float.parseFloat(request.getParameter("porcentaje"));
        String tipoS = request.getParameter("tipoe");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            
            float valore=0;
            float valora=0;
            if(model.proveedoracpmService.existProveedor(nit)){
                if(!model.proveedoracpmService.existSucursal(nit,codigo)){
                    valore=Float.parseFloat(request.getParameter("emax"));
                    valora=Float.parseFloat(request.getParameter("acpm"));
                    
                    Proveedor_Acpm pacpm = new Proveedor_Acpm();
                    pacpm.setNit(nit);
                    pacpm.setCodigo(codigo);
                    pacpm.setDstrct(usuario.getDstrct());
                    pacpm.setCreation_user(usuario.getLogin());
                    pacpm.setMax_e(valore);
                    pacpm.setMoneda(request.getParameter("moneda"));
                    pacpm.setTipo(tipoS);
                    pacpm.setValor(valora);
                    pacpm.setCity_code(request.getParameter("ciudad"));
                    pacpm.setCod_Migracion(request.getParameter("codigo_m").toUpperCase());
                    pacpm.setPorcentaje(porcentaje);
                    model.proveedoracpmService.insertProveedor(pacpm,usuario.getBase());
                    next+="Provedor agregado exitosamente";
                    //System.out.println("Este es el codigo de migracion "+pacpm.getCod_Migracion());
                }
                else{
                    next = "/anticipo/anticipoInsertError.jsp";
                    request.setAttribute("sucursal", "#cc0000");
                }
            }
            else{
                next = "/anticipo/anticipoInsertError.jsp";
                request.setAttribute("nit", "#cc0000");
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
