/******************************************************************
* Nombre                        ReporteDevolucionAction.java
* Descripci�n                   Clase Action para el reporte de devoluciones
* Autor                         ricardo rosero
* Fecha                         200/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  rrosero
 */
public class ReporteDevolucionAction extends Action {
    
    /** Creates a new instance of ReporteDevolucionAction */
    public ReporteDevolucionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String pag = "/jsp/masivo/repdevolucion/repdevolucion.jsp";
        String Mensaje = "";
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        String fecha = s.format(d);

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String user = usuario.getLogin();

        String fechai = request.getParameter("fechai");
        String fechaf = request.getParameter("fechaf");
        
        ReporteDevolucionTh hilo = new ReporteDevolucionTh(); 

        Vector informe = new Vector();
        
        int total = 0;
        
        try{
            ////System.out.println("Voy a ejecutar el service");
            total = model.repdevService.obtenerRegistros(fechai, fechaf);
            if(total==0) {
                Mensaje = "No hay registros relacionados en esas fechas";
            }
            else{
                informe = model.repdevService.obtenerInforme(fechai, fechaf);
                hilo.start(informe, usuario.getLogin(), fechai, fechaf); 
                Mensaje = "ReporteDevolucion_" + fecha + ".xls";
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }

        
        pag += "?Mensaje="+Mensaje;
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(pag);

    }
    
}
