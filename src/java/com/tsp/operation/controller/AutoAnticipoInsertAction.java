/*
 * AutoAnticipoInsertAction.java
 *
 * Created on 18 de junio de 2005, 01:08 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class AutoAnticipoInsertAction extends Action {
    
    /** Creates a new instance of AutoAnticipoInsertAction */
    public AutoAnticipoInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/anticipo_placas/anticipopInsert.jsp";
        
        String placa = request.getParameter("placa").toUpperCase();
        
        double efectivo = 0;
        double acpm = 0;
        int ticket_a = 0;
        int ticket_b = 0;
        int ticket_c = 0;               
        
        if (!request.getParameter("efectivo").equals("")) {
            efectivo = Double.parseDouble(request.getParameter("efectivo"));
        }
        if (!request.getParameter("acpm").equals("")){ 
             acpm = Double.parseDouble(request.getParameter("acpm"));
        }
        if (!request.getParameter("ta").equals("")) {
            ticket_a = Integer.parseInt(request.getParameter("ta"));
        }
        if  (!request.getParameter("tb").equals("")){
            ticket_b = Integer.parseInt(request.getParameter("tb"));
        }
        if (!request.getParameter("tc").equals("")){
            ticket_c = Integer.parseInt(request.getParameter("tc"));               
        }
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            Auto_Anticipo aa = new Auto_Anticipo();
            aa.setPlaca(placa);
            aa.setEfectivo(efectivo);
            aa.setacpm(acpm);
            aa.setTicket_a(ticket_a);
            aa.setTicket_b(ticket_b);
            aa.setTicket_c(ticket_c);
            aa.setCreation_user(usuario.getLogin());
            aa.setUser_update(usuario.getLogin());
            aa.setBase(usuario.getBase());
            aa.setSj(request.getParameter("standard"));
            
            model.placaService.buscaPlaca2(placa);
            if ( model.placaService.getPlaca() != null  ){
                
                model.autoAnticipoService.buscarPSj(placa,request.getParameter("standard"));
                if(model.autoAnticipoService.getAuto_Anticipo() == null){
                    model.autoAnticipoService.insert(aa);
                    next= next + "?msg=exitoR";
                }
                else{
                    next= next + "?msg=error";
                }
            }
            else {
                next = next + "?msg=noBD";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}