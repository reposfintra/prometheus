/***************************************
 * Nombre Clase ............. ArchivoMovimientoAction.java
 * Descripci�n  .. . . . . .  Maneja los eventos para generar el archivo de movimientos
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  14/03/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;


public class ArchivoMovimientoAction extends Action {
    
  
    
    public void run() throws ServletException, InformationException {
        
        
          try{
              
                HttpSession session   = request.getSession();
                Usuario     usuario   = (Usuario) session.getAttribute("Usuario");       
        
            
                String   next          = "/jsp/general/exportar/ArchivoMovimiento.jsp?msj=";
                String   msj           = "";
                String   evento        = request.getParameter("evento");
                 
                         
                if( evento !=null ){
                    
                    if ( ! model.ArchivoMovimientoSvc.isIsProceso() ){
                    
                             if ( evento.equals("BUSCAR") ){
                                  String distrito = request.getParameter("distrito").toUpperCase();
                                  String banco    = request.getParameter("banco").toUpperCase();
                                  String sucursal = request.getParameter("sucursal").toUpperCase();
                                  String corrida  = request.getParameter("corrida").toUpperCase();

                                  model.ArchivoMovimientoSvc.searchCheques(distrito, banco, sucursal, corrida);

                                  if(model.ArchivoMovimientoSvc.getListCheques().size()==0)
                                      msj="No se encontraron registros o no tienen cuentas de transferencia establecida...";
                                  else{
                                      next  = "/jsp/general/exportar/ListaArchivoMovimiento.jsp?msj=";
                                      if( model.ArchivoMovimientoSvc.existeTransferencia() ){
                                          msj = "Ya esta corrida para el banco fu� transferida anteriormente....";
                                          model.ArchivoMovimientoSvc.setActivoBtn(false);
                                      }
                                      else
                                          model.ArchivoMovimientoSvc.setActivoBtn(true);
                                  }

                             }

                             if ( evento.equals("MIGRAR") ){
                                  HArchivoMovimiento hilo = new HArchivoMovimiento();
                                  hilo.start( model, usuario.getLogin() );
                                 msj="Su proceso de generaci�n de archivo  Movimiento se ha iniciado....";

                             }
                     
                   }
                   else
                      msj="Ya existe un proceso generandose, por favor espere...."; 
                    
                }             
             
                
                
                next+= msj;
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response); 
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
        
    }
    
}
