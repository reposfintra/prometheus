/*
 * RemesaPlanillaSearchAction.java
 *
 * Created on 17 de diciembre de 2004, 03:49 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class RemesaPlanillaSearchAction extends Action{
    
    /** Creates a new instance of RemesaPlanillaSearchAction */
    public RemesaPlanillaSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        Planilla pla = new Planilla();
        Remesa rem = new Remesa();
        String pagina = request.getParameter("existe")!=null?"RemesaExistente":"relacionar";
        String next="/colpapel/"+pagina+".jsp";
        String no_rem= request.getParameter("remesa").toUpperCase();
        String no_pla = request.getParameter("planilla").toUpperCase();
        List remesas=new LinkedList();
        String mensaje ="";
        int swPla=0;
        int swRem=0;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String account_code_i="";
        String account_code_c="";
        String basePla="";
        String baseRem="";
        try{
            
            if(!model.planillaService.tieneFactura(no_pla)){
                if(!model.remplaService.estaRempla(no_pla,no_rem)){
                    model.remesaService.buscaRemesa(no_rem);
                    if(model.remesaService.getRemesa()!=null){
                        swPla=1;
                        rem=model.remesaService.getRemesa();
                        baseRem = rem.getBase();
                        String nombreR="";
                        String remitentes[]= rem.getRemitente().split(",");
                        for(int i=0; i<remitentes.length; i++){
                            nombreR=model.remidestService.getNombre(remitentes[i])+",";
                        }
                        String nombreD="";
                        String destinatarios[]= rem.getDestinatario().split(",");
                        for(int i=0; i<destinatarios.length; i++){
                            nombreD=model.remidestService.getNombre(destinatarios[i])+",";
                        }
                        
                        rem.setRemitente(nombreR);
                        rem.setDestinatario(nombreD);
                        request.setAttribute("remesa",rem);
                        
                        model.stdjobdetselService.buscaStandard(request.getParameter("standard"));
                        if(model.stdjobdetselService.getStandardDetSel()!=null){
                            Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                            account_code_i=stdjobdetsel.getAccount_code_i();
                            account_code_c=stdjobdetsel.getAccount_code_c();
                            
                        }
                    }
                    //SE BUSCA LA PLANILLA
                    model.planillaService.bucarColpapel(no_pla);
                    pla.setNumpla(no_pla);
                    if(model.planillaService.getPlanilla()!=null && swPla==1){
                        swRem=1;
                        pla = model.planillaService.getPlanilla();
                        basePla = pla.getBase();
                        String nomruta= model.planillaService.getRutas(pla.getNumpla());
                        pla.setCodruta(pla.getRuta_pla());
                        pla.setRuta_pla(nomruta);
                        request.setAttribute("planilla",pla);
                        
                        
                        model.movplaService.buscaMovpla(no_pla,"01");
                        if(model.movplaService.getMovPla()!=null){
                            Movpla movpla= model.movplaService.getMovPla();
                            request.setAttribute("movpla",movpla);
                        }
                        remesas = model.planillaService.buscarRemesas(no_pla);
                        
                    }
                    
                    if("".equals(basePla) || "".equals(baseRem)){
                        model.planillaService.setPlanilla(null);
                        model.remesaService.setRemesa(null);
                        request.setAttribute("planilla",null);
                        request.setAttribute("remesa",null);
                        
                        if("".equals(basePla)){
                            mensaje ="La planilla  selecciona no fue elaborada en la web<br>";
                        }
                        if("".equals(baseRem)){
                            mensaje = mensaje+"La remesa selecciona no fue elaborada en la web";
                        }
                        
                        next="/colpapel/"+pagina+".jsp?mensaje="+mensaje;
                    }
                }
                else{
                    swPla=1;
                    swRem=1;
                    mensaje ="La Planilla "+no_pla+" y Remesa "+no_rem+" ya estan relacionadas";
                    next="/colpapel/"+pagina+".jsp?mensaje="+mensaje;
                }
                if(swPla ==0 || swRem==0){
                    mensaje ="La Planilla "+no_pla+" o la Remesa "+no_rem+" no existen o estan anuladas";
                    next="/colpapel/"+pagina+".jsp?mensaje="+mensaje;
                }
                
                
            /*
             *Si se recive el parametro para relacionar la planilla y
             *la remesa entonces primero se valida y luego se relacionan.
             */
                if(request.getParameter("relacionar")!=null){
                    Vector consultas = new Vector();
                    int sw=0;
                    java.util.Enumeration enum1;
                    String parametro;
                    enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                    int cant = 0;
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("por")==0){
                            cant = Integer.parseInt(request.getParameter(parametro))+cant;
                            
                        }
                    }
                    if(cant!=100){
                        sw=1;
                        mensaje ="La suma total de los porcentajes es diferente al 100%";
                        next="/colpapel/"+pagina+".jsp?mensaje="+mensaje;
                    }
                    if(sw==0){
                        RemPla rempla= new RemPla();
                        rempla.setPlanilla(no_pla);
                        rempla.setRemesa(request.getParameter("remesa"));
                        rempla.setDstrct(usuario.getDstrct());
                        rempla.setPorcent(Integer.parseInt(request.getParameter("porcent")));
                        rempla.setAccount_code_c(account_code_c);
                        rempla.setAccount_code_i(account_code_i);
                        //Agrego la Remesa-Planilla
                        
                        consultas.add(model.remplaService.insertRemesa(rempla,usuario.getBase()));
                        
                        request.setAttribute("remesa" , null);
                        request.setAttribute("planilla",null);
                        
                        //SE ACTUALIZAN LOS PORCENTAJES
                        
                        enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                        
                        
                        while (enum1.hasMoreElements()) {
                            parametro = (String) enum1.nextElement();
                            if(!parametro.equals("porcent")){
                                if(parametro.indexOf("por")==0){
                                    cant = Integer.parseInt(request.getParameter(parametro))+cant;
                                    String reme = parametro.substring(3);
                                    model.remplaService.buscaRemPla(no_pla,reme);
                                    if(model.remplaService.getRemPla()!=null){
                                        RemPla rp = model.remplaService.getRemPla();
                                        rp.setPorcent(Integer.parseInt(request.getParameter(parametro)));
                                        consultas.add(model.remplaService.updateRemPla(rp));
                                    }
                                }
                            }
                        }
                        mensaje ="Se relacionaron con exito la Planilla "+no_pla+" y la Remesa "+no_rem;
                        next="/colpapel/"+pagina+".jsp?mensaje="+mensaje;
                        
                        //ESTO SE HACE CUANDO SE INTENTO ANULAR UNA REMESA RELACIONADA A UNA PLANILLA
                        // Y HAY Q REEMPLAZAR LA REMESA POR UNA NUEVA,.
                        //EN ESTE CASO YA CREAMOS LA NUEVA REMESA, AHOR HAY Q ANULARLA.
                        if(request.getParameter("anular")!=null && !request.getParameter("anular").equals("") ){
                            String remanu = request.getParameter("anular");
                            model.remesaService.buscaRemesa(remanu);
                            if(model.remesaService.getRemesa()!=null){
                                System.out.println("Encontramos la remesa "+remanu);
                                Remesa remAn =model.remesaService.getRemesa();
                                remAn.setDistrito(usuario.getDstrct());
                                remAn.setUsuario(usuario.getLogin());
                                remAn.setBase(usuario.getBase());
                                consultas.add(model.remesaService.anularRemesa(remAn));
                            }
                            else{
                                System.out.println("NO Encontramos la remesa "+remanu);
                            }
                            model.remplaService.buscaRemPla(no_pla,remanu);
                            if(model.remplaService.getRemPla()!=null){
                                
                                rempla= model.remplaService.getRemPla();
                                consultas.add(model.remplaService.anularPlanilla(rempla));
                                
                                
                                
                            }
                        }
                    }
                    model.despachoService.insertar(consultas);
                    
                }
            }
            else{
                next="/colpapel/"+pagina+".jsp?mensaje=La planilla "+no_pla+" ya tiene generada OP, no puede asociarle remesas.";
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
    
    
}
