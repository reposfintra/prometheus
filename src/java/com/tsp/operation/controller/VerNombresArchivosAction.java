/*********************************************************************************
 * Nombre clase :      VerNombresArchivosAction.java                             *
 * Descripcion :       Action del VerNombresArchivosAction.java                  *
 * Autor :             LREALES                                                   *
 * Fecha :             18 de septiembre de 2006, 10:43 AM                        *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class VerNombresArchivosAction extends Action {
    
    /** Creates a new instance of VerNombresArchivosAction */
    public VerNombresArchivosAction () { }    
    
    public void run () throws ServletException {
        
        String next = "/jsp/finanzas/contab/reporte_control/verNombresArchivos.jsp";
        
        HttpSession session = request.getSession();
        Usuario usu = (Usuario) session.getAttribute("Usuario");        
        String user = "" + usu.getLogin();
        
        String url  =  model.DirectorioSvc.getUrl() + user;
           
        try {
            
            ListadoArchivos  directorio = ListadoArchivos.loadReporteUtilidad ( url );
            model.DirectorioSvc.setDirectorio ( directorio );
        
        } catch ( Exception e ) {
                        
            e.printStackTrace ();
            throw new ServletException ( e.getMessage () );
            
        }
        
        this.dispatchRequest ( next );
                        
    }
    
}