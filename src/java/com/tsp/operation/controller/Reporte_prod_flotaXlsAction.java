/*************************************************************
 * Nombre: Reporte_prod_flotaXlsAction.java
 * Descripci�n: Accion para generar el reporte de producci�n
 *              de flota.
 * Autor: Ing. Jose de la rosa
 * Fecha: 26 de noviembre de 2005, 11:58 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import java.util.zip.*;

/**
 *
 * @author  EQUIPO13
 */
public class Reporte_prod_flotaXlsAction extends Action{
    
    /** Creates a new instance of Reporte_prod_flotaXlsAction */
    public Reporte_prod_flotaXlsAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        String ano="",anof="",mes="",Nmes="",fmes="";
        String ffin="",fechaini="",fechafin="";
        String fecha = request.getParameter ("c_fecha");
        int a�o = Integer.parseInt ( fecha.substring (0,4) );
        mes = fecha.substring (5,7);
        ano = fecha.substring (0,4);
        String diac = fecha.substring (8,10);
        fmes = String.valueOf ((Integer.parseInt (mes) + 1));
        anof = ano;
        if(mes.equals ("01")){
            ffin="31";
            Nmes="Enero";
        }
        else if(mes.equals ("02")){
            if(a�o%4==0)
                ffin="29";
            else
                ffin="28";
            Nmes="Febrero";
        }
        else if(mes.equals ("03")){
            Nmes="Marzo";
            ffin="31";
        }
        else if(mes.equals ("04")){
            Nmes="Abril";
            ffin="30";
        }
        else if(mes.equals ("05")){
            ffin="31";
            Nmes="Mayo";
        }
        else if(mes.equals ("6")){
            Nmes="Junio";
            ffin="30";
        }
        else if(mes.equals ("07")){
            Nmes="Julio";
            ffin="31";
        }
        else if(mes.equals ("08")){
            ffin="31";
            Nmes="Agosto";
        }
        else if(mes.equals ("09")){
            Nmes="Septiembre";
            ffin="30";
        }
        else if(mes.equals ("10")){
            Nmes="Octubre";
            ffin="31";
        }
        else if(mes.equals ("11")){
            Nmes="Noviembre";
            ffin="30";
        }
        else{
            anof=String.valueOf(a�o+1);
            Nmes="Diciembre";
            ffin="31";
            fmes="01";
        }
        fechaini=ano+"-"+mes+"-01";
        fechafin=anof+"-"+fmes+"-01";
        ////System.out.println(fechaini+"  "+fechafin);
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        try{
            ReporteExportarFlotaXLS hilo = new ReporteExportarFlotaXLS ();
            hilo.start (usuario.getLogin (),Nmes,fechaini,fechafin,ffin,diac);
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path =  rb.getString("ruta") + "/exportar/migracion/"+usuario.getLogin()+"/ReporteFlota";
            File file = new File(path);
            File[] files = file.listFiles();
            if (files != null){
                for( int i =0; i<files.length;i++ ) {
                    //System.out.println(files[i]);
                    files[i].delete();
                }
            }    
            file.deleteOnExit();
            
            next = "/jsp/masivo/reportes/reporteProdFlota.jsp?msg=Su reporte ha iniciado y se encuentra en el log de procesos";
        }catch (Exception ex){
            throw new ServletException ("Error en OpcionesExportarPtoAction .....\n"+ex.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
