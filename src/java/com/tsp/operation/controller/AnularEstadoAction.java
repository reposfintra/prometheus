/*
 * AnularEstadoAction.java
 *
 * Created on 4 de marzo de 2005, 07:34 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIBASMO
 */

public class AnularEstadoAction extends Action {
    
    /** Creates a new instance of AnularEstadoAction */
    public AnularEstadoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        String codigo = (request.getParameter("codigo").toUpperCase());
        String pais = (request.getParameter("pais"));
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
       // String lenguaje = (String) session.getAttribute("idioma");
     
        try{
//            model.idiomaService.cargarIdioma(lenguaje, "MsgAnulado.jsp");
            
            Estado estado = new Estado(); 
            estado.setdepartament_code(codigo);
            estado.setpais_code(pais);
            estado.setUser_update(usuario.getLogin());
            model.estadoservice.anularestado(estado); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
    
}
