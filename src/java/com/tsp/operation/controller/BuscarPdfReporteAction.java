/******************************************************************************
 * Nombre clase :                   BuscarPdfReporteAction.java               *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Buscar de Reporte Factura Destinatario    *
 * Autor :                          LREALES                                   *
 * Fecha :                          03 de abril de 2006, 07:05 PM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.io.*;

public class BuscarPdfReporteAction extends Action{
    
    /** Creates a new instance of BuscarPdfReporteAction */
    public BuscarPdfReporteAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
        String next="/impresiones_pdf/reporteFacturaDestinatario/buscarReporte.jsp";
                
        try{
            
            String tipo   = ( request.getParameter("tipo") != null )?request.getParameter("tipo") :"";
            String numero = ( request.getParameter("numero") != null )?request.getParameter("numero").toUpperCase() :"";
            String mens = "";
                        
            if ( tipo.equals("p") ){
                
                boolean bp = false;                
                bp = model.reporteFacDesService.existePlanilla( numero );
                
                if ( bp == true ){
                    
                    model.reporteFacDesService.listaPlanilla( numero );
                    Vector planilla = model.reporteFacDesService.getVectorPlanilla();
                    String num = model.reporteFacDesService.getHojaReportes().getNumero();
                    
                    next = next +"?Mensaje=Por Favor Voltee La Hoja y Seleccione Imprimir!&sw=Ok&numero="+num+"&tipo="+tipo;
                                    
                } else{
                    
                    next = next +"?Mensaje=La Planilla Ingresada NO EXISTE!";
                    
                }
            }
            
            if ( tipo.equals("r") ){
                
                boolean br = false;                
                br = model.reporteFacDesService.existeRemesa( numero );
                
                if ( br == true ){
                    
                    model.reporteFacDesService.listaRemesa( numero );
                    Vector planilla = model.reporteFacDesService.getVectorRemesa();
                    String num = model.reporteFacDesService.getHojaReportes().getNumero();
                    
                    next = next +"?Mensaje=Por Favor Voltee La Hoja y Seleccione Imprimir!&sw=Ok&numero="+num+"&tipo="+tipo;
                    
                } else{
                    
                    next = next +"?Mensaje=La Remesa Ingresada NO EXISTE!";
                    
                }
            }            
                
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new ServletException( "Error en Buscando el Reporte Factura Destinatario PDF.......\n" + e.getMessage() );
        
        }
        
        this.dispatchRequest(next);
        
    }  
    
}