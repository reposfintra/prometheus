 /***************************************
    * Nombre Clase ............. GenerarReporteAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos en la generacion del archivo
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  09/04/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/

package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;


public class CuentasPropietariosAction extends Action{
    
    
    
    public void run() throws ServletException {
        
         try{
           String next     = "/jsp/finanzas/fintra/Cuentabancos.jsp?msj=";    
           String msj      = "";
           
           String  evento  = request.getParameter("evento");
           
           if(evento!=null){
               if(evento.equals("SAVE")){
                   model.CuentaBancoSvc.loadRequest(request);
                   msj = model.CuentaBancoSvc.insert();
                   
               }
               
               if(evento.equals("UPDATE")){
                   model.CuentaBancoSvc.loadRequest(request);                   
                   msj = model.CuentaBancoSvc.update();
                   
               }
               
               
               if(evento.equals("LISTAR")){
                   next     = "/jsp/finanzas/fintra/ListadoCuentabancos.jsp?msj=";
                   model.CuentaBancoSvc.listar(); 
               }
               
               
               if(evento.equals("LISTAR_NIT")){
                   next     = "/jsp/finanzas/fintra/ListadoCuentabancos.jsp?msj=";
                   String  nit = request.getParameter("nit");
                   model.CuentaBancoSvc.listar(nit); 
               }
               
               
               
               if(evento.equals("DELETE")){
                   String[]  ids = request.getParameterValues("cuenta");
                   model.CuentaBancoSvc.delete(ids);
                   next     = "/jsp/finanzas/fintra/ListadoCuentabancos.jsp?msj=Eliminados.....";
               } 
               
               if(evento.equals("SELECT")){
                   String id = request.getParameter("id");
                   model.CuentaBancoSvc.obtenerRegistro(id);
               }               
               
               if(evento.equals("RESET")){
                   model.CuentaBancoSvc.reset(); 
               }
               
               if(evento.equals("INIT")){
                 model.CuentaBancoSvc.reset(); 
                 model.CuentaBancoSvc.loadTipoCuentas();
                 model.CuentaBancoSvc.loadBancos();
                 String mims      = reset( request.getParameter("mims")      );
                 String nit       = reset( request.getParameter("nit")       );
                 String ds        = reset( request.getParameter("distrito")  );                 
                 String banco     = reset( request.getParameter("banco")     );
                 String sucursal  = reset( request.getParameter("sucursal")  );
                 String nameCta   = reset( request.getParameter("nameCta")   );
                 String nitCta    = reset( request.getParameter("nitCta")    );                 
                 String noCta     = reset( request.getParameter("noCta")     );
                 String tipoCta   = reset( request.getParameter("tipoCta")   );
                 String e_mail    = reset( request.getParameter("e_mail")    );
                  
                 next        = "/jsp/finanzas/fintra/Cuentabancos.jsp?nit="+ nit +"&mims="+ mims +"&distrito="+ ds  +"&noCta="+noCta +"&tipoCta="+ tipoCta +"&nitCta="+ nitCta +"&banco="+ banco +"&sucursal="+ sucursal +"&nameCta="+ nameCta +"&e_mail="+e_mail+"&msj=";
              }
               
               
              if(evento.equals("SEARCH")){
                   String tipo  = request.getParameter("tipo");
                   String id    = request.getParameter("id");
                   List lista = model.CuentaBancoSvc.getInfoPropietario(tipo, id );
                   request.setAttribute("infoPropietario", lista );
                   next = "/jsp/finanzas/fintra/DatosPropietario.jsp";
               }
               
               
              
               
               
               
           }
           else{
               model.CuentaBancoSvc.reset(); 
               model.CuentaBancoSvc.loadTipoCuentas();
               model.CuentaBancoSvc.loadBancos();
           }
           
           
           RequestDispatcher rd = application.getRequestDispatcher(next + msj);
           if(rd == null)
              throw new ServletException("No se pudo encontrar "+ next);
           rd.forward(request, response);  
           
        }catch(Exception e){
          throw new ServletException(e.getMessage());
        }   
         
    }
    
    
    
    
    
  public  String reset(String val){
      if(val==null)
         val="";
      return val;
  }
    
  
}
