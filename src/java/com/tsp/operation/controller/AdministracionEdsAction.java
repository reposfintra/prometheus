/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AdministracionEdsDAO;
import com.tsp.operation.model.DAOS.InterfazLogisticaDAO;
import com.tsp.operation.model.DAOS.impl.AdministracionEdsImpl;
import com.tsp.operation.model.DAOS.impl.LogisticaAdminImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.EDSPropietarioBeans;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author mariana
 */
public class AdministracionEdsAction extends Action {

    Usuario usuario = null;
    private final int GUARDAR_PROPIETARIO = 1;
    private final int CARGAR_COMPANIA = 2;
    private final int CARGAR_EDS = 3;
    private final int CARGAR_PAIS = 4;
    private final int CARGAR_DEPARTAMENTO = 5;
    private final int GUARDAR_EDS = 6;
    private final int CARGAR_CIUDAD = 7;
    private final int CARGAR_BANDERA = 8;
    private final int CARGAR_PROPIETARIO = 9;
    private final int VERIFICAR_USUARIO = 10;
    private final int ACTUALIZAR_EDS = 11;
    private final int CAMBIAR_ESTADO_EDS = 12;
    private final int CARGAR_COMBO_EDS = 13;
    private final int CARGAR_PRODUCTOS_EDS = 14;
    private final int CARGAR_GRILLA_PRODUCTOS_EDS = 15;
    private final int CARGAR_UNIDAD_MEDIDA = 16;
    private final int GUARDAR_PRODUCTOS_EDS = 17;
    private final int CAMBIAR_ESTADO_PRODUCTO = 18;
    private final int ACTUALIZAR_PRODUCTO = 19;
    private final int CARGAR_GRILLA_BANDERA = 20;
    private final int CAMBIAR_ESTADO_BANDERA = 21;
    private final int GUARDAR_BANDERAS = 22;
    private final int ACTUALIZAR_BANDERAS = 23;
    private final int CARGAR_PRODUCTOS_RALACION_EDS = 24;
    private final int CARGAR_GRILLA_UNIDAD_MEDIDA = 25;
    private final int GUARDAR_UNIDAD_MEDIDA = 26;
    private final int ACTUALIZAR_UNIDAD_MEDIDA = 27;
    private final int CAMBIAR_ESTADO_UNIDAD_MEDIDA = 28;
    private final int CARGAR_BANCO = 29;
    private final int CARGAR_AGENCIA = 30;
    private final int CARGAR_SEDE_PADO = 31;
    private final int CARGAR_BANCO_TRANSFER = 32;
    private final int CARGAR_HC = 33;
    private final int CARGAR_SUCURSAL = 34;
    private final int CARGAR_PROPIETARIOS_EDS = 35;
    private final int EDITAR_PROPIETARIOS_EDS = 36;
    private final int CARGAR_FECHAS_CXP_EDS = 37;
    private final int CARGAR_CXP_EDS = 38;
    private final int EXPORTAR_EXCEL_CUENTAS_COBRO = 39;
    private final int VERIFICAR_USUARIO_EDS = 41;
    private final int VERIFICAR_USUARIO_PROP = 42;
    private final int CARGAR_CXP_EDS_DETALLE = 43;
    private final int EXPORTAR_EXCEL_CXP = 44;
    private final int EXPORTAR_EXCEL_CXP_EDS = 45;
    private final int AUDITORIA_VENTAS = 46;
    private final int ESTACIO_SERVCIO = 47;
    private final int EXPORTAR_EXCEL_VENTAS = 48;
    private final int VENTAS_DETALLE = 49;
    private final int EXPORTAR_PDF = 50;
    private final int CARGAR_NOTA_CREDITO = 51;
    private final int REPORTE_COLOCACION = 52;
    private final int EXPORTAR_EXCEL_REPORTE_COLOCACION = 53;

    private AdministracionEdsDAO dao;

    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new AdministracionEdsImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case GUARDAR_PROPIETARIO:
                    guardarPropietario();
                    break;
                case CARGAR_COMPANIA:
                    cargarCompania();
                    break;
                case CARGAR_EDS:
                    cargarEds();
                    break;
                case GUARDAR_EDS:
                    guardarEds();
                    break;
                case CARGAR_PAIS:
                    cargarPais();
                    break;
                case CARGAR_DEPARTAMENTO:
                    cargarDepartamento();
                    break;
                case CARGAR_CIUDAD:
                    cargarCiudad();
                    break;
                case CARGAR_BANDERA:
                    cargarBandera();
                    break;
                case CARGAR_PROPIETARIO:
                    cargarPropietario();
                    break;
                case VERIFICAR_USUARIO:
                    verificar_usuario();
                    break;
                case ACTUALIZAR_EDS:
                    actualizarEds();
                    break;
                case CAMBIAR_ESTADO_EDS:
                    cambiarEstadoEds();
                    break;
                case CARGAR_COMBO_EDS:
                    cargarComboEDS();
                    break;
                case CARGAR_PRODUCTOS_EDS:
                    cargarProductosEDS();
                    break;
                case CARGAR_GRILLA_PRODUCTOS_EDS:
                    cargarGrillaProductosEDS();
                    break;
                case CARGAR_UNIDAD_MEDIDA:
                    cargarUnidadMedidaPrp();
                    break;
                case GUARDAR_PRODUCTOS_EDS:
                    guardarProductoEds();
                    break;
                case CAMBIAR_ESTADO_PRODUCTO:
                    cambiarEstadoProducto();
                    break;
                case ACTUALIZAR_PRODUCTO:
                    actualizarProducto();
                    break;
                case CARGAR_GRILLA_BANDERA:
                    cargarGrillaBanderas();
                    break;
                case CAMBIAR_ESTADO_BANDERA:
                    cambiarEstadoBandera();
                    break;
                case GUARDAR_BANDERAS:
                    guardarBanderas();
                    break;
                case ACTUALIZAR_BANDERAS:
                    actualizarBandera();
                    break;
                case CARGAR_PRODUCTOS_RALACION_EDS:
                    cargarProductosRelEds();
                    break;
                case CARGAR_GRILLA_UNIDAD_MEDIDA:
                    cargarGrillaUnidadMedida();
                    break;
                case GUARDAR_UNIDAD_MEDIDA:
                    guardarUnidadMedida();
                    break;
                case ACTUALIZAR_UNIDAD_MEDIDA:
                    actualizarUnidadMedida();
                    break;
                case CAMBIAR_ESTADO_UNIDAD_MEDIDA:
                    cambiarUnidadMedida();
                    break;
                case CARGAR_BANCO:
                    cargarBanco();
                    break;
                case CARGAR_AGENCIA:
                    cargarAgencia();
                    break;
                case CARGAR_SEDE_PADO:
                    cargarSedePago();
                    break;
                case CARGAR_BANCO_TRANSFER:
                    cargarBancotransfer();
                    break;
                case CARGAR_HC:
                    cargarHC();
                    break;
                case CARGAR_SUCURSAL:
                    cargarSucursal();
                    break;
                case CARGAR_PROPIETARIOS_EDS:
                    cargarPropietarioEds();
                    break;
                case EDITAR_PROPIETARIOS_EDS:
                    editarPropietarioEds();
                    break;
                case CARGAR_FECHAS_CXP_EDS:
                    cargarFechaCXPEds();
                    break;
                case CARGAR_CXP_EDS:
                    cargarCXPEDS();
                    break;
                case EXPORTAR_EXCEL_CUENTAS_COBRO:
                    exportarExcelCuentasCobro();
                    break;
                case VERIFICAR_USUARIO_EDS:
                    verificarusuarioEDS();
                    break;
                case VERIFICAR_USUARIO_PROP:
                    verificarusuarioProp();
                    break;
                case CARGAR_CXP_EDS_DETALLE:
                    cargarCXPEDS_DETALLE();
                    break;
                case EXPORTAR_EXCEL_CXP:
                    exportarExcelCXP();
                    break;
                case EXPORTAR_EXCEL_CXP_EDS:
                    exportarExcelCXPEDS();
                    break;
                case AUDITORIA_VENTAS:
                    auditoriaVentas();
                    break;
                case ESTACIO_SERVCIO:
                    cargarComboSOLOEDS();
                    break;
                case EXPORTAR_EXCEL_VENTAS:
                    exportarVentas();
                    break;
                case VENTAS_DETALLE:
                    auditoriaVentasDetalle();
                    break;
                case EXPORTAR_PDF:
                    exportaPdf();
                    break;
                case CARGAR_NOTA_CREDITO:
                    cargarNotaCredito();
                    break;
                case REPORTE_COLOCACION:
                    reporteColocacion();
                    break;
                case EXPORTAR_EXCEL_REPORTE_COLOCACION:
                    exportarReporteColocacion();
                    break;

            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }

    }

    public void actualizarEds() {
        try {
            String edsciudad = request.getParameter("ciudad");
            String edsdireccion = request.getParameter("direccion");
            String edsnombre = request.getParameter("nombreds");
            String niteds = request.getParameter("niteds");
            String id = request.getParameter("id");
            String encargado = request.getParameter("encargado");
            String nit = request.getParameter("nit");
            String telefono = request.getParameter("telefono");
            String correo = request.getParameter("correo");
            String clave = Util.encript(application.getInitParameter("deskey"), request.getParameter("clave"));
            String idusuario = request.getParameter("idusuario");
            String pais = request.getParameter("pais");
            String idusuarioedit = request.getParameter("idusuarioedit");
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();

            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarUsuario(encargado, edsdireccion, correo, telefono, pais, edsciudad, clave, idusuarioedit));
//            tService.getSt().addBatch(this.dao.actualizarUsuarioProyecto(idusuario, usuario, idusuarioedit));
//            tService.getSt().addBatch(this.dao.actualizarUsuarioPerfil(idusuario, usuario, idusuarioedit));
            tService.getSt().addBatch(this.dao.actualizarEds(edsnombre, niteds, edsciudad, edsdireccion, telefono, correo, idusuario, usuario, id));
            tService.getSt().addBatch(this.dao.actualizarProveedor(niteds, edsnombre, usuario));
            tService.execute();

        } catch (Exception e) {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }

    public void actualizarProducto() {
        try {
            String nombrepro = request.getParameter("nombreproducto");
            String unidad = request.getParameter("unidadmedida");
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarProducto(nombrepro, unidad, id));
            tService.execute();

        } catch (Exception e) {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }

    public void cambiarEstadoEds() {
        try {
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String id = request.getParameter("id");

            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoEds(id, usuario));
            tService.execute();

        } catch (Exception e) {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }

    public void cambiarEstadoProducto() {
        try {
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoProducto(id));
            tService.execute();
        } catch (Exception e) {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }

    public void verificar_usuario() {
        try {
            String idusuario = request.getParameter("idusuario");
            String json = dao.verificarUsuario(idusuario);
            this.printlnResponse(json, "text/plain;");

        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void verificarusuarioEDS() {
        try {
            String nit = request.getParameter("nit");
            String json = dao.verificarusuarioEDS(nit);
            this.printlnResponse(json, "text/plain;");

        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void verificarusuarioProp() {
        try {
            Gson gson = new Gson();
            String nitem = request.getParameter("nitem");
            String tipodocumento = request.getParameter("tipodocumento");
            String json = gson.toJson(dao.verificarusuarioPrp(nitem, tipodocumento));
            //String json = dao.verificarusuarioPrp(nitem);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarEds() {
        try {
            Gson gson = new Gson();
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String tipousu = ((Usuario) request.getSession().getAttribute("Usuario")).getTipo();

            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarEDS(usuario, tipousu)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboEDS() {
        try {
            String propietario = request.getParameter("idpropietario");
            this.printlnResponse(dao.cargarComboEDS(propietario), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboSOLOEDS() {
        try {
            this.printlnResponse(dao.cargarComboEDS(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarCompania() {
        try {
            this.printlnResponse(dao.cargarCompania(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarBandera() {
        try {
            this.printlnResponse(dao.cargarBandera(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarPais() {
        try {
            this.printlnResponse(dao.cargarPais(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarPropietario() {
        try {
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String tipo = ((Usuario) request.getSession().getAttribute("Usuario")).getTipo();
            this.printlnResponse(dao.cargarPropietario(usuario, tipo, this.usuario), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarDepartamento() {
        try {
            String pais = request.getParameter("pais");
            this.printlnResponse(dao.cargarDepartamento(pais), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarCiudad() {
        try {
            String departamento = request.getParameter("departamento");
            this.printlnResponse(dao.cargarCiudad(departamento), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarProductosEDS() {
        try {
            this.printlnResponse(dao.cargarproductosEds(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarUnidadMedidaPrp() {
        try {
            this.printlnResponse(dao.cargarUnidadMedidaPrp(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGrillaProductosEDS() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaProductosEDS()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarPropietario() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String nombreprop = request.getParameter("nombreprop");
            String direccion = request.getParameter("direccion");
            String correo = request.getParameter("correo");
            String tipopersona = request.getParameter("tipopersona");
            String telefono = request.getParameter("telefono");
            String pais = request.getParameter("pais");
            String ciudad = request.getParameter("ciudad");
            String nit = request.getParameter("nit");
            String estadousu = request.getParameter("estadousu");
            String idusuario = request.getParameter("idusuario");
            String passw = Util.encript(application.getInitParameter("deskey"), request.getParameter("passw"));
            String cambioclav = request.getParameter("cambioclav");
            String perfil = "PROPIETARIO";
            String nitem = request.getParameter("nitem");
            String representante = request.getParameter("representante");
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getCia();
            String base = ((Usuario) request.getSession().getAttribute("Usuario")).getBase();
            String tipodoc = request.getParameter("tipodoc");
            String gcontribuyente = request.getParameter("gcontribuyente");
            String auiva = request.getParameter("iva");
            String auretefuente = request.getParameter("retefuente");
            String auica = request.getParameter("ica");
            String aretenedor = request.getParameter("autoretenedor");
            String banco = request.getParameter("banco");
            String sedepago = request.getParameter("sedepago");
            String banagencia = request.getParameter("banagencia");
            String tcuenta = request.getParameter("tcuenta");
            String ptransfer = request.getParameter("ptransfer");
            String bancotransfer = request.getParameter("bancotransfer");
            String numcuenta = request.getParameter("numcuenta");
            String hc = request.getParameter("hc");
            String suctransfer = request.getParameter("suctransfer");
            String regimen = request.getParameter("regimen");
            String digitover = request.getParameter("digitover"); //digito de verificacion
            /*
             Campos para la trasferencia de proveedor...
             */
            String nombre_cuenta = request.getParameter("nombre_cuenta");
            String cedula_cuenta = request.getParameter("cedula_cuenta");
            String codcli = request.getParameter("codcli");
            //
            /*
             campos que indocan si se debe editar o insetar en la tabla provedores y cliente
             */
            String existe = request.getParameter("existe");
            String operacion = request.getParameter("operacion");

            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            if (existe.equals("SI")) {
                tService.getSt().addBatch(this.dao.actualizarCliente(nombreprop, direccion, telefono, ciudad, pais, usuario, codcli));
            } else {
                tService.getSt().addBatch(this.dao.guardarCliente(nombreprop, base, nitem, cia, hc, direccion, telefono, numcuenta, telefono, correo, direccion, tipodoc, ciudad, pais, usuario));
            }
            if (operacion.equals("EDITAR")) {
                tService.getSt().addBatch(this.dao.actualizarProveedor(nitem, nombreprop, usuario));
            } else {
                tService.getSt().addBatch(this.dao.guardarProveedor(cia, nitem, nombreprop, usuario, tipodoc, tipopersona, gcontribuyente, aretenedor, auretefuente, auiva, auica, base, banco, sedepago, banagencia, tcuenta, ptransfer, bancotransfer, numcuenta, hc, suctransfer, regimen, nombre_cuenta, cedula_cuenta, digitover));
            }
            tService.getSt().addBatch(this.dao.guardarUsuario(representante, direccion, correo, telefono, pais, nitem, ciudad, estadousu, idusuario, passw, cia, cambioclav, base, perfil));
            tService.getSt().addBatch(this.dao.guardarUsuario(cia, idusuario, usuario, base));
            tService.getSt().addBatch(this.dao.guardarUsuarioPerfil(perfil, idusuario, usuario, cia));
            tService.getSt().addBatch(this.dao.gPropietarioEds(cia, tipopersona, nitem, nombreprop, direccion, correo, nit, representante, usuario));
            tService.getSt().addBatch(this.dao.actualizarProveedor(nombreprop, usuario, gcontribuyente, aretenedor, auretefuente, auiva, auica, banco, sedepago, banagencia, tcuenta, ptransfer, bancotransfer, numcuenta, hc, suctransfer, nit, regimen, nombre_cuenta, cedula_cuenta));

            //tService.getSt().addBatch(this.dao.guardarCliente(nombreprop, base, nitem, cia, hc, direccion, telefono, numcuenta, telefono, correo, direccion, tipodoc, ciudad, pais, usuario));
            tService.execute();

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                System.out.println(ex);
                ex.printStackTrace();
            }
        }
    }

    public void guardarProductoEds() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct();
            String nomproducto = request.getParameter("nomproducto");
            String unidad = request.getParameter("unidad");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarProductosEds(cia, nomproducto, unidad));

            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void guardarEds() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String idpropietario = request.getParameter("idpropietario");
            String idbandera = request.getParameter("idbandera");
            String nombreds = request.getParameter("nombreds");
            String niteds = request.getParameter("niteds");
            String direccion = request.getParameter("direccion");
            String correo = request.getParameter("correo");
            String telefono = request.getParameter("telefono");
            String pais = request.getParameter("pais");
            String ciudad = request.getParameter("ciudad");
            String nit = request.getParameter("nit");
            String estadousu = "A";
            String idusuario = request.getParameter("idusuario");
            String passw = Util.encript(application.getInitParameter("deskey"), request.getParameter("passw"));
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct();
            String base = ((Usuario) request.getSession().getAttribute("Usuario")).getBase();
            String perfil = "EDS";
            String cambioclav = request.getParameter("cambioclav");
            String nombrencargado = request.getParameter("nombrencargado");
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();

            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarUsuario(nombrencargado, direccion, correo, telefono, pais, nit, ciudad, estadousu, idusuario, passw, cia, cambioclav, base, perfil));
            tService.getSt().addBatch(this.dao.guardarUsuario(cia, idusuario, usuario, base));
            tService.getSt().addBatch(this.dao.guardarUsuarioPerfil(perfil, idusuario, usuario, cia));
            tService.getSt().addBatch(this.dao.guardarEds(idpropietario, idbandera, nombreds, niteds, ciudad, direccion, telefono, correo, estadousu, idusuario, usuario));
            tService.getSt().addBatch(this.dao.actualizarProveedor(niteds, nombreds, usuario));
            tService.getSt().addBatch(this.dao.guardarProveedor(cia, niteds, nombreds, usuario, "NIT", "PJ", "N", "N", "N", "N", "N", base, "", "", "", "", "", "", "", "", "", "C", "", "", ""));

            tService.execute();

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarGrillaBanderas() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaBanderas()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cambiarEstadoBandera() {
        try {
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoBandera(id));
            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    public void guardarBanderas() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct();
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String razonsoc = request.getParameter("razonsoc");
            String nit = request.getParameter("nit");
            String tipper = request.getParameter("tipper");
            String direccion = request.getParameter("direccion");
            String correo = request.getParameter("correo");
            String replegal = request.getParameter("replegal");
            String docrep = request.getParameter("docrep");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarBanderas(cia, razonsoc, nit, tipper, direccion, correo, replegal, docrep, usuario));
            tService.execute();

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarBandera() {
        try {
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String razonsoc = request.getParameter("razonsoc");
            String nit = request.getParameter("nit");
            String tipper = request.getParameter("tipper");
            String direccion = request.getParameter("direccion");
            String correo = request.getParameter("correo");
            String replegal = request.getParameter("replegal");
            String docrep = request.getParameter("docrep");
            String idbandera = request.getParameter("idbandera");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarBanderas(razonsoc, nit, tipper, direccion, correo, replegal, docrep, usuario, idbandera));
            tService.execute();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    public void cargarProductosRelEds() {
        try {
            Gson gson = new Gson();
            String id = request.getParameter("id");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarProductosRelEDS(id)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGrillaUnidadMedida() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaUnidadMedida()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarUnidadMedida() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct();
            String nomunmed = request.getParameter("nomunmed");
            String medicion = request.getParameter("medicion");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarUnidadMedida(cia, nomunmed, medicion));

            tService.execute();

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarUnidadMedida() {
        try {
            String nomunmed = request.getParameter("nomunmed");
            String medicion = request.getParameter("medicion");
            String id = request.getParameter("idum");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarUnidadMedida(nomunmed, medicion, id));
            tService.execute();

        } catch (Exception e) {
        }
    }

    public void cambiarUnidadMedida() {
        try {
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoUnidadMedida(id));
            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    public void cargarBanco() {
        try {
            this.printlnResponse(dao.cargarBanco(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarAgencia() {
        try {
            String banco = request.getParameter("banco");
            this.printlnResponse(dao.cargarAgencia(banco), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarSedePago() {
        try {
            this.printlnResponse(dao.cargarSedePago(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarBancotransfer() {
        try {
            this.printlnResponse(dao.cargarBancotransfer(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarHC() {
        try {
            this.printlnResponse(dao.cargarHC(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarSucursal() {
        try {
            this.printlnResponse(dao.cargarSucursal(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarPropietarioEds() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarPropietarioEds()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void editarPropietarioEds() {
        try {
            String id = request.getParameter("id");
            String ciudad = request.getParameter("ciudad");
            String direccion = request.getParameter("direccion");
            String nombreprop = request.getParameter("nombreprop");
            String nit = request.getParameter("nit");
            String tipopersona = request.getParameter("tipopersona");//
            String correo = request.getParameter("correo");
            String telefono = request.getParameter("telefono");
            String pais = request.getParameter("pais");
            String idusuario = request.getParameter("idusuario");
            String clave = Util.encript(application.getInitParameter("deskey"), request.getParameter("passw"));//
            String representante = request.getParameter("representante");
            String nitem = request.getParameter("nitem");
            String tipodoc = request.getParameter("tipodoc");
            String gcontribuyente = request.getParameter("gcontribuyente");
            String ica = request.getParameter("ica");
            String iva = request.getParameter("iva");
            String retefuente = request.getParameter("retefuente");
            String autoretenedor = request.getParameter("autoretenedor");
            String numcuenta = request.getParameter("numcuenta");
            String banco = request.getParameter("banco");
            String sedepago = request.getParameter("sedepago");
            String banagencia = request.getParameter("banagencia");
            String tcuenta = request.getParameter("tcuenta");
            String bancotransfer = request.getParameter("bancotransfer");
            String regimen = request.getParameter("regimen");
            String suctransfer = request.getParameter("suctransfer");
            String hc = request.getParameter("hc");
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String idusuarioedit = request.getParameter("idusuarioedit");
            String idedit = request.getParameter("idedit");
            String ptransfer = request.getParameter("ptransfer");
            String codcli = request.getParameter("codcli");
            /*
             Campos para la trasferencia de proveedor...
             */
            String nombre_cuenta = request.getParameter("nombre_cuenta");
            String cedula_cuenta = request.getParameter("cedula_cuenta");

            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarUsuario(nombreprop, direccion, correo, telefono, pais, ciudad, clave, idusuarioedit));
            tService.getSt().addBatch(this.dao.actualizarPropietarioEds(nombreprop, direccion, correo, nit, representante, usuario, id));
            tService.getSt().addBatch(this.dao.actualizarProveedor(nombreprop, usuario, gcontribuyente, autoretenedor, retefuente, iva, ica, banco, sedepago, banagencia, tcuenta, ptransfer, bancotransfer, numcuenta, hc, suctransfer, idedit, regimen, nombre_cuenta, cedula_cuenta));
            tService.getSt().addBatch(this.dao.actualizarCliente(nombreprop, hc, direccion, telefono, representante, telefono, correo, direccion, ciudad, pais, usuario, codcli));
            tService.execute();
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarFechaCXPEds() {
        try {
            String eds = request.getParameter("eds");
            this.printlnResponse(dao.cargarFechaCXPEds(eds), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarCXPEDS() {
        try {
            Gson gson = new Gson();
            String eds = request.getParameter("eds");
            String fechainicio = request.getParameter("fechainicio");
            String fechafin = request.getParameter("fechafin");
            String cxp = request.getParameter("cxp");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.CXPEds(eds, fechainicio, fechafin, cxp)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarCXPEDS_DETALLE() {
        try {
            Gson gson = new Gson();
            String cxp = request.getParameter("cxp");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.CXPEds_detalle(cxp)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarNotaCredito() {
        try {
            Gson gson = new Gson();
            String cxp = request.getParameter("cxp");
            String numero_nota = request.getParameter("numero_nota");
            String creation_date = request.getParameter("creation_date");
            int id_eds = request.getParameter("id_estacion") != null ? Integer.parseInt(request.getParameter("id_estacion")) : 0;

            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarNotaCredito(cxp, numero_nota, creation_date, id_eds)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void auditoriaVentas() {
        try {
            Gson gson = new Gson();
            String eds = request.getParameter("eds");
            String fechainicio = request.getParameter("fechainicio");
            String fechafin = request.getParameter("fechafin");
            String planilla = request.getParameter("planilla");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.auditoriaVentas(eds, fechainicio, fechafin, planilla)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void auditoriaVentasDetalle() {
        try {
            Gson gson = new Gson();
            String numventa = request.getParameter("numventa");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.auditoriaVentas(numventa)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void exportaPdf() {
        String json = "{\"respuesta\":\"GENERADO\"}";
        try {
            String total = request.getParameter("total");
//            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String cxp = request.getParameter("cxp");
            int edsid = Integer.parseInt(request.getParameter("edsid"));
            InterfazLogisticaDAO daolo = new LogisticaAdminImpl(usuario.getBd());
            EDSPropietarioBeans buscarPropietario = daolo.buscarPropietario((edsid), usuario);

            AdministracionLogisticaAction administracionlogistc = new AdministracionLogisticaAction();
            administracionlogistc.generarPdf(Double.valueOf(total), usuario.getLogin(), cxp, buscarPropietario);

            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }

    public void reporteColocacion() {
        try {
            Gson gson = new Gson();
            String planilla = request.getParameter("planilla");
            String fechainicio = request.getParameter("fechainicio");
            String fechafin = request.getParameter("fechafin");
            String eds = request.getParameter("eds");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.reporteColocacion(planilla, fechainicio, fechafin, eds)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void exportarExcelCuentasCobro() throws Exception {
        try{
        String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("CxcGeneradas_", "CXC GENERADAS");
        String[] cabecera = {"# Venta", "Fecha Venta", "Planilla", "Cedula", "Coductor", "Placa", "Producto", "$ Producto",
            "Cant Suministrada", "ud Medida", "Subtotal", "Comision", "Total"};

        short[] dimensiones = new short[]{
            3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
        };
        this.generaTitulos(cabecera, dimensiones);

        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject objects = (JsonObject) asJsonArray.get(i);
            fila++;
            int col = 0;
            xls.adicionarCelda(fila, col++, objects.get("num_venta").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_venta").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cedula").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("conductor").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("placa").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("producto").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("precio_producto").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cantidad_suministrada").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("unidad_medida").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("subtotal").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("comision").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("total").getAsString(), letra);

        }
//        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/CxcGeneradas_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "SE ha generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

        this.printlnResponse(resp1, "text/plain");
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void exportarExcelCXPEDS() throws Exception {
        try{
        String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("CxcGeneradas_", "CXC GENERADAS");
        String[] cabecera = {"# CXP", "# Venta", "Fecha Venta", "Planilla", "Cedula", "Coductor", "Placa", "Producto", "$ Producto",
            "Cant Suministrada", "ud Medida", "Subtotal", "Comision", "Total"};

        short[] dimensiones = new short[]{
            3000, 3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
        };
        this.generaTitulos(cabecera, dimensiones);

        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject objects = (JsonObject) asJsonArray.get(i);
            fila++;
            int col = 0;
            xls.adicionarCelda(fila, col++, objects.get("cxp_documento").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("num_venta").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_venta").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cedula").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("conductor").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("placa").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("producto").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("precio_producto").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cantidad_suministrada").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("unidad_medida").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("subtotal").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("comision").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("total").getAsString(), letra);

        }
//        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/CxcGeneradas_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

        this.printlnResponse(resp1, "text/plain");
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void exportarExcelCXP() throws Exception {
        try{
        String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("CxpGeneradas_", "CXP GENERADAS");
        String[] cabecera = {"# CXP", "Nit", "Nombre EDS", "Hc", "Valor Neto", "Valor Total Abonos", "Valor Saldo", "Fecha de Creacion", "Usuario de Creacion"};

        short[] dimensiones = new short[]{
            3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000
        };
        this.generaTitulos(cabecera, dimensiones);

        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject objects = (JsonObject) asJsonArray.get(i);
            fila++;
            int col = 0;
            xls.adicionarCelda(fila, col++, objects.get("cxp_documento").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cxp_proveedor").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("edsnombre").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("hc").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cxp_vlr_neto").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cxp_vlr_total_abonos").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cxp_vlr_saldo").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_creacion").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("usuarios_creacion").getAsString(), letra);
        }
//        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/CxpGeneradas_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

        this.printlnResponse(resp1, "text/plain");
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void exportarVentas() throws Exception {
        try{
        String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("AuditoriaVentas_", "Ventas");
        String[] cabecera = {"Periodo", "Planilla", "Fecha Envio Fintra", "Fecha Venta", "Diferencia Fechas", "Origen", "Destino", "Valor Planilla", "Valor Neto Anticipo", "Valor Descuento",
            "Valor Desembolsar", "Total Venta", "Disponible", "Reanticipo", "Fecha Pago Fintra", "Fecha Corrida",
            "Transportadoras", "Nombre Agencias", "Conductor", "Cedula Propietario", "Propietario", "Placa",
            "Sucursal", "Numero Venta", "Fecha Venta", "Nombre eds", "Kilometraje", "Cantidad Registros Ventas", "Total Venta", "Valor Comision"};

        short[] dimensiones = new short[]{
            3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000, 3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000,
            3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000, 5000
        };
        this.generaTitulos(cabecera, dimensiones);

        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject objects = (JsonObject) asJsonArray.get(i);
            fila++;
            int col = 0;
            // xls.adicionarCelda(fila, col++, objects.get("id").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("periodo").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_envio_fintra").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_vent").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("diferencia_fechas").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("origen").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("destino").getAsString(), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_planilla").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_neto_anticipo").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_descuentos_fintra").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_desembolsar").getAsString()), letra);
            xls.adicionarCelda(fila, col++,Double.parseDouble( objects.get("total_venta").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("disponible").getAsString()), letra);
            xls.adicionarCelda(fila, col++, objects.get("reanticipo").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_pago_fintra").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_corrida").getAsString(), letra);
            //xls.adicionarCelda(fila, col++, objects.get("id_transportadoras").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("transportadora").getAsString(), letra);
            // xls.adicionarCelda(fila, col++, objects.get("id_agencias").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("nombre_agencia").getAsString(), letra);
            //xls.adicionarCelda(fila, col++, objects.get("id_conductors").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("conductor").getAsString(), letra);
            //xls.adicionarCelda(fila, col++, objects.get("id_propietarios").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cedula_propietario").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("propietario").getAsString(), letra);
            //xls.adicionarCelda(fila, col++, objects.get("id_vehiculos").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("placa").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("sucursal").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("num_venta").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_vent").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("nombre_eds").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("kilometraje").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cant_reg_ventas").getAsString(), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("total_venta").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_comision_fintra").getAsString()), letra);

        }
//        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/AuditoriaVentas_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

        this.printlnResponse(resp1, "text/plain");
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void exportarReporteColocacion() throws Exception {
        try{
        String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("ReporteColocacion_", "Reporte");
        String[] cabecera = {"Periodo", "Transportadora", "Nombre Agencia", "Cedula Propietario", "Propietario", "Cedula Conductor", "Conductor", "Placa", "Sucursal", "Origen", "Destino",
            "Planilla", "Fecha Venta", "Fecha Anticipo", "Tiempo Legalizacion", "Valor Anticipo", "Descuento Fintra", "Valor Consignacion", "Reanticipo",
            "Numero Venta", "Nombre Eds", "Kilometraje", "Producto", "Precio x Unidad", "Cantidad Suministrada", "Total Ventas", "Disponible"};

        short[] dimensiones = new short[]{
            3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000, 3000, 7000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000,
            3000, 7000, 5000, 5000, 5000, 7000, 5000
        };
        this.generaTitulos(cabecera, dimensiones);

        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject objects = (JsonObject) asJsonArray.get(i);
            fila++;
            int col = 0;
            xls.adicionarCelda(fila, col++, objects.get("periodo").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("transportadora").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("nombre_agencia").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cedula_propietario").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("propietario").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("cedula_conductor").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("conductor").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("placa").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("sucursal").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("origen").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("destino").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_venta").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fecha_anticipo").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("tiempo_legalizacion").getAsString(), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_anticipo").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("descuento_fintra").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("valor_consignado").getAsString()), letra);
            xls.adicionarCelda(fila, col++, objects.get("reanticipo").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("numero_venta").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("nombre_eds").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("kilometraje").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("producto").getAsString(), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("precioxunidad").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Float.parseFloat(objects.get("cantidad_suministrada").getAsString()), letra);
            xls.adicionarCelda(fila, col++, Double.parseDouble(objects.get("total_ventas").getAsString()), letra);
            xls.adicionarCelda(fila, col++, objects.get("disponible").getAsString(), letra);
        }

        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ReporteColocacion_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

        this.printlnResponse(resp1, "text/plain");
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {
        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            // xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }
}
