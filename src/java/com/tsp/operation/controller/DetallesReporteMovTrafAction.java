/*
 * DetallesReporteMovTrafAction.java
 *
 * Created on 18 de septiembre de 2005, 12:51 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.controller;

/**
 *
 * @author Armando Oviedo
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
public class DetallesReporteMovTrafAction extends Action{
    
    /** Creates a new instance of DetallesReporteMovTrafAction */
    public DetallesReporteMovTrafAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/jsp/trafico/Movimiento_trafico/movtrafacteli.jsp" ;
        try{
            HttpSession session = request.getSession();
            String numpla = request.getParameter("numpla");
            String fecha = request.getParameter("fecha");
            //Ivan Dario Gomez 2006-04-11 
            boolean IsUltimo = (request.getParameter("ultimo").equals("true"))?true:false;
            
            String mensaje = request.getParameter("mensaje");
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String observaciones = request.getParameter("obaut")+" "+request.getParameter("observacion");
            //String []tiprep = request.getParameter("tiporep").split("/");
            String tiporeporte = request.getParameter("tiporep");
            String tipoubicacion = (request.getParameter("tubicacion")!=null)?request.getParameter("tubicacion"):"";
            String usuariocreacion = request.getParameter("creation_user");
            if(mensaje!=null){
                if(mensaje.equalsIgnoreCase("view")){
                    next+="?ultimo="+IsUltimo;
                    //System.out.println("numpla: "+numpla);
                    //System.out.println("fecha: "+fecha);
                    model.rmtService.BuscarReporteMovTraf(numpla,fecha);
                    RepMovTrafico tmp = model.rmtService.getReporteMovTraf();
                    model.rmtService.BuscarUbicaciones(tmp.getTipo_procedencia());
                    //Ivan DArio gomez 2006-04-10
                    model.tblgensvc.buscarListaSinDato("REP", "REP_TRA");
                }
                else if(mensaje.equalsIgnoreCase("listarubicaciones")){
                    //Ivan DArio gomez 2006-04-10
                    model.tblgensvc.buscarListaSinDato("REP", "REP_TRA");
                    tiporeporte=""; 
                    model.rmtService.BuscarUbicaciones(tipoubicacion);
                    next += "?tiporep="+tiporeporte+"&tubicacion="+tipoubicacion+"&observacion="+observaciones+"&mensaje=nuevainfo";
                }
                else if(mensaje.equalsIgnoreCase("update")){
                    next+="?reload=ok";
                    fecha = request.getParameter("fecha");
                    String ubicacion = request.getParameter("ubicacion");
                    String olddate = request.getParameter("olddate");
                    //String FechaActual = Util.getFechaActual_String(6);
                    if(IsUltimo && tiporeporte.equals("OBS")){
                       String[] vec = model.rmtService.BuscarRegStatus(numpla);
                       if(vec[0].equals("V")){
                         observaciones = vec[1];  
                       }
                    }
                    RepMovTrafico newrmt = new RepMovTrafico();
                    newrmt.setBase(usuario.getBase());
                    newrmt.setDstrct((String)(session.getAttribute("Distrito")));
                    newrmt.setNumpla(numpla);
                    newrmt.setFechareporte(fecha);
                    newrmt.setCreation_user(usuariocreacion);
                    newrmt.setObservacion(observaciones);
                    newrmt.setTipo_procedencia(tipoubicacion);
                   //Ivan Dario gomez 2006-04-10
                    //String []tp = tiporeporte.split("/");
                    newrmt.setTipo_reporte(tiporeporte);
                    newrmt.setUbicacion_procedencia(ubicacion);
                    newrmt.setUpdate_user(usuario.getLogin());
                    newrmt.setLast_update("now()");
                    //System.out.println("PUESTO DE CONTROL ULT-->"+ubicacion);
                    newrmt.setCodUbicacion(ubicacion);
                    newrmt.setReg_Status("");
                    
                    model.rmtService.setUltRMT ( newrmt );
                    model.rmtService.getNextPC ();
                    model.rmtService.updReport(newrmt,olddate,IsUltimo); 
                    next += "&mensaje=Reporte modificado correctamente";
                    model.rmtService.BuscarReporteMovTraf(numpla,fecha);
                }
                else if(mensaje.equalsIgnoreCase("delete")){
                    next+="?reload=ok";
                    String ubicacion = request.getParameter("ubicacion");
                    RepMovTrafico newrmt = new RepMovTrafico();
                    newrmt.setBase(usuario.getBase());
                    newrmt.setDstrct((String)(session.getAttribute("Distrito")));
                    newrmt.setNumpla(numpla);
                    newrmt.setFechareporte(fecha);
                    newrmt.setCreation_user(usuariocreacion);
                    newrmt.setObservacion(observaciones);
                    newrmt.setTipo_procedencia(tipoubicacion);
                    //Ivan Dario Gomez  2006-04-12
                   // String []tp = tiporeporte.split("/");
                    newrmt.setTipo_reporte(tiporeporte);
                    newrmt.setUbicacion_procedencia(ubicacion);
                    newrmt.setUpdate_user(usuario.getLogin());
                    model.rmtService.delReport(newrmt);
                    next += "&mensaje=Reporte eliminado correctamente";
                }
            }
        }
        catch(SQLException ex){
            throw new ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
    }
}
