/********************************************************************
 *      Nombre Clase.................   AgregarDemoraPlanillaAction.java    
 *      Descripci�n..................   Ingreso de demora por el n�mero de la planilla
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   06.10.2005
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;

/**
 *
 * @author  Tito Andr�s Maturana
 */
import org.apache.log4j.*;

public class AgregarDemoraPlanillaAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of AgregarDemoraPlanillaAction */
    public AgregarDemoraPlanillaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        //Atributos de la demora
        String numpla = request.getParameter("numpla");
        String fecha = request.getParameter("fecha1").substring(0,16);
        String sitio = request.getParameter("sitio");
        String sitio_des = request.getParameter("sitio_des");
        String observacion = request.getParameter("observacion")==null ? "" : request.getParameter("observacion");
        String cdemora = request.getParameter("cdemora");
        String cjerarquia = request.getParameter("cjerarquia").replaceAll("/","");
        String duracion = request.getParameter("duracion");
                
        observacion = "Retraso " + sitio_des + " " + fecha + " - Duraci�n: " + duracion + " min.\n" + observacion;
        
        Calendar cal = Util.crearCalendar(fecha + ":00");
        long milis = cal.getTimeInMillis() + Integer.parseInt(duracion)*60000;
        Calendar cal2  = Calendar.getInstance();
        cal2.setTimeInMillis(milis);     
        Date fec = cal2.getTime();        
        String fecha2 = Util.dateTimeFormat(fec);
        //if( fecha2.length()==0) fecha2 = "0099-01-01 00:00:00";
        
        logger.info("FECHA FIN DEMORA: " + fecha2);
        
                        
        //Instancia de Demora
        Demora demora = new Demora();
        demora.setPlanilla(numpla);
        demora.setDuracion(Integer.valueOf(duracion));
        demora.setFecha_demora(fecha);
        demora.setSitio_demora(sitio);
        demora.setObservacion(observacion);
        demora.setCodigo_demora(cdemora);
        demora.setCodigo_jerarquia(cjerarquia);
        demora.setFecha_fin_demora(fecha2);
        
        //request.setAttribute("nit", "ECE0D8");        
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        demora.setUsuario_creacion(usuario.getLogin());
        demora.setBase(usuario.getBase());
        demora.setDistrito((String) session.getAttribute("Distrito"));
        
        //Pr�xima vista
        String next = "/jsp/trafico/demora/asignarDemoraPlanilla.jsp";
        
        try{    
            Vector consultas = new Vector();
            if( !model.demorasSvc.isEjecutoProceso()){
                //model.demorasSvc.agregarDemora(demora);
                Vector reportes = model.demorasSvc.getPlanillasReporte();
                Hashtable ht = new Hashtable();
                ht.put("planilla", numpla);
                //ht.put("fecha", demora.getFecha_demora());
                reportes.add(ht);
                
                logger.info("PLANILLAS RETRASO: " + reportes.size());
                    
                /* Guardar Registro en Rep Mov Trafico*/
                
                for( int i=0; i<reportes.size(); i++){
                    ht = new Hashtable();
                    ht = (Hashtable) reportes.elementAt(i);
                    
                    
                    //model.traficoService.obtenerIngreso_Trafico(numpla.toUpperCase());
                    model.traficoService.obtenerIngreso_Trafico(ht.get("planilla").toString().toUpperCase());
                    Ingreso_Trafico tra = model.traficoService.getIngreso();
                    
                    RepMovTrafico nu = new RepMovTrafico() ;
                    nu.setDstrct(tra.getDstrct());
                    nu.setNumpla(ht.get("planilla").toString());
                    nu.setObservacion(demora.getObservacion());
                    nu.setTipo_procedencia("PC");
                    nu.setUbicacion_procedencia(tra.getPto_control_ultreporte());
                    nu.setTipo_reporte("DEM");
                    nu.setZona(tra.getZona());
                    nu.setFec_rep_pla(tra.getFecha_prox_reporte());
                    nu.setCausa("");
                    nu.setClasificacion("");
                    nu.setCreation_date("now()");
                    nu.setLast_update("now()");
                    nu.setCreation_user(usuario.getLogin());
                    nu.setUpdate_user(usuario.getLogin());
                    nu.setBase(tra.getBase());
                    
                    Calendar today = Calendar.getInstance();
                    Date fechoy = today.getTime();
                    String fecreporte = Util.dateTimeFormat(fechoy).toString().substring(0,17) + "00";//com.tsp.util.Util.fechaActualTIMESTAMP().substring(0,17) + "00"; 
                    logger.info("FECHA REPORTE EN RMT: " + fecreporte);
                    nu.setFechareporte(fecreporte);
                    
                    consultas.add(model.rmtService.addRMT(nu));
                    
                    demora.setPlanilla(ht.get("planilla").toString());
                    
                    if( ht.get("planilla").toString().compareTo(numpla)==0 ){
                        demora.setFecha_demora(fecha);     
                        demora.setFecha_fin_demora(fecha2);
                    } else {
                        demora.setFecha_demora(tra.getFecha_ult_reporte());
                        Calendar cal0 = Util.crearCalendar(fecha + ":00");
                        long milis0 = cal0.getTimeInMillis() + Integer.parseInt(duracion)*60000;
                        Calendar cal_2  = Calendar.getInstance();
                        cal_2.setTimeInMillis(milis0);
                        Date fec0 = cal_2.getTime();
                        String fecha_2 = Util.dateTimeFormat(fec0);     
                        demora.setFecha_fin_demora(fecha_2);
                    }    
                    
                    consultas.add(model.demorasSvc.agregarDemora(demora, true));
                    consultas.add(model.demorasSvc.actualizarIngresoTrafico(demora.getPlanilla(), demora.getDuracion(), true));
                    consultas.add(model.demorasSvc.actualizarIngresoTrafico(demora.getPlanilla(), demora.getObservacion()));
                }
                
                model.despachoService.insertar(consultas);                
                //model.demorasSvc.actualizarIngresoTrafico(numpla, demora.getDuracion());
                model.demorasSvc.setEjecutoProceso(true);
                
                
                next += "?msg=Demora a planilla agregada exitosamente.";
            }
        }catch (SQLException e){           
                e.printStackTrace();
                throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
