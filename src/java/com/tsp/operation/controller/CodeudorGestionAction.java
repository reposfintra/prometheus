/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * CodeudorGestionAction.java : clase encargada de gestionar los codeudores
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.operation.controller;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.http.*;

/**
 *
 * @author rhonalf
 */
public class CodeudorGestionAction extends Action {

    public CodeudorGestionAction(){
    }

    @Override
    public void run(){
        String opcion = "";
        String next = "";
        try {
            opcion = request.getParameter("opcion")!=null ? request.getParameter("opcion") : "";
            if(opcion.equals("buscar")){
                String cadenaresp = "";
                String filtro = request.getParameter("filtro")!=null ? request.getParameter("filtro") : "id";
                String cadena = request.getParameter("cadena")!=null ? request.getParameter("cadena") : "";
                cadenaresp = this.tablaResultados(filtro, cadena);
                this.escribirResponse(cadenaresp);
            }
            else if(opcion.equals("modificar")){
                String mensaje = "";
                next = "/jsp/fenalco/clientes/edit_codeudor.jsp?ced=";
                String ced = request.getParameter("id")!=null ? request.getParameter("id") : "";
                next = next + ced;
                mensaje = this.updateCodeudor();
                this.dispatchRequest(next + "&mens=" + mensaje);
            }
            else{
                System.out.println("opcion no usada ...");
            }
        }
        catch (Exception e) {
            System.out.println("Error en CodeudorGestionAction: "+e.toString());
        }
    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    public void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }

    /**
     * Busca un dato basado en un filtro y lo muestra en una tabla html
     * @param filtro el filtro a aplicar
     * @param cadena el dato a buscar
     * @return String con la tabla a mostrar
     * @throws Exception cuando hay error
     */
    public String tablaResultados(String filtro,String cadena) throws Exception{
        String cad = "";
        cad = "<table id='results' name='results' border='1' width='100%' style='border-collapse: collapse; padding: 2px;'>" +
                    "<thead><tr class='subtitulo1'>" +
                    "<th>Cedula</th>" +
                    "<th>Nombre</th>" +
                    "</tr></thead><tbody>";
        try {
            CodeudorService cserv = new CodeudorService();
            Codeudor codeudor = null;
            ArrayList lista = cserv.listaResults(filtro, cadena);
            for (int i = 0; i < lista.size(); i++) {
                codeudor = (Codeudor)lista.get(i);
                cad = cad + "<tr class='fila'><td><a href='"+request.getContextPath()+"/jsp/fenalco/clientes/edit_codeudor.jsp?ced="+
                        codeudor.getId()+"' target='_self' style='text-decoration: none; color: green;' >"+codeudor.getId()+"</a></td>";
                cad = cad + "<td><a href='"+request.getContextPath()+"/jsp/fenalco/clientes/edit_codeudor.jsp?ced="+
                        codeudor.getId()+"' target='_self' style='text-decoration: none; color: green;' >"+codeudor.getNombre()+"</a></td></tr>";
            }
            cserv = null;
        }
        catch (Exception e) {
            throw new Exception("Error al generar la tabla de resultados: "+e.toString());
        }
        cad = cad + "</tbody></table>";
        return cad;
    }

    /**
     * Actualiza los datos del codeudor
     * @return cadena con la confirmacion de la actualizacion
     * @throws Exception cuando hay error
     */
    public String updateCodeudor() throws Exception{
        String mens = "Codeudor actualizado!";
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            String ced =  request.getParameter("id")!=null ? request.getParameter("id") : "";
            String nombre =  request.getParameter("nombre")!=null ? request.getParameter("nombre") : "";
            String carrera =  request.getParameter("carrera")!=null ? request.getParameter("carrera") : "";
            String tipo_id = request.getParameter("tipo_id")!=null ? request.getParameter("tipo_id") : "";
            String celular = request.getParameter("celular")!=null ? request.getParameter("celular") : "";
            String expedicion = request.getParameter("expedicion")!=null ? request.getParameter("expedicion") : "";
            String ciudad = request.getParameter("ciudad")!=null ? request.getParameter("ciudad") : "";
            String telefono = request.getParameter("telefono")!=null ? request.getParameter("telefono") : "";
            String direccion = request.getParameter("direccion")!=null ? request.getParameter("direccion") : "";
            String universidad = request.getParameter("universidad")!=null ? request.getParameter("universidad") : "";
            String codigo = request.getParameter("codigo")!=null ? request.getParameter("codigo") : "";
            String semestre = request.getParameter("semestre")!=null ? request.getParameter("semestre") : "0";
            Codeudor cod = new Codeudor();
            cod.setCarrera(carrera);
            cod.setCel(celular);
            cod.setCiudad(ciudad);
            cod.setCod(codigo);
            cod.setDireccion(direccion);
            cod.setExpedicion_id(expedicion);
            cod.setId(ced);
            cod.setNombre(nombre);
            cod.setSemestre(semestre);
            cod.setTelefono(telefono);
            cod.setTipo_id(tipo_id);
            cod.setUniversidad(universidad);
            cod.setUser_update(usuario.getLogin());
            CodeudorService cserv = new CodeudorService();
            cserv.actualizarCodeudor(cod);
        }
        catch (Exception e) {
            mens = "No se pudo actualizar el codeudor";
            throw new Exception("No se pudo actualizar el codeudor: "+e.toString());
        }
        return mens;
    }

}