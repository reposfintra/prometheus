/*
 * Nombre        TramoModificarAction.java
 * Autor         Ing Jesus Cuestas
 * Fecha         26 de junio de 2005, 10:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class TramoModificarAction extends Action{
    
    /** Creates a new instance of TramoModificarAction */
    public TramoModificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/tramo/"+request.getParameter("pagina")+"?lista=ok&reload=ok";
                
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");        
        try{
            
            String cia = request.getParameter("c_cia");
            String origen = request.getParameter("c_origen");
            String destino = request.getParameter("c_destino");
            String consumo = (!request.getParameter("c_comb").equals(""))?request.getParameter("c_comb"):"0";
            String uconsumo = request.getParameter("c_unidadc");
            String distancia = request.getParameter("c_distancia");
            String udistancia =request.getParameter("c_unidadd");
            String duracion = request.getParameter("c_duracion");
            String uduracion = request.getParameter("c_unidaddu");
            
            if(request.getParameter("modtiket")==null){//modificar tramo
                Tramo t = new Tramo();
                t.setDstrct(cia);
                t.setOrigen(origen);
                t.setDestino(destino);
                t.setConsumo_combustible(Float.parseFloat(consumo));
                t.setUnidad_consumo(uconsumo);
                t.setDistancia(Float.parseFloat(distancia));
                t.setUnidad_distancia(udistancia);
                t.setDuracion(Float.parseFloat(duracion));
                t.setUnidad_duracion(uduracion);
                t.setUsuario(usuario.getLogin());
                
                model.tramoService.setTramo(t);
                model.tramoService.modificarTramo();
                model.tramoService.buscarTramo(cia, origen, destino);
                
                request.setAttribute("mensaje","El Tramo fue modificado con exito!");
            }else{//modificar tiquete
                String ticket_id = request.getParameter("c_tiket");
                float cantidad = Float.parseFloat(request.getParameter("c_cant"));
                if(cantidad < 9){
                    Tramo_Ticket t = new Tramo_Ticket();
                    t.setCia(cia);
                    t.setOrigen(origen);
                    t.setDestino(destino);
                    t.setTicket_id(ticket_id);
                    t.setCantidad(cantidad);
                    t.setUsuario(usuario.getLogin());
                    
                    model.tiketService.setTicket(t);
                    model.tiketService.modificarTramo_Ticket();
                    request.setAttribute("mensaje","El Tiquete fue modificado con exito!");
                }else{
                    request.setAttribute("mensaje","La cantidad maxima de tiquetes debe ser menor de 10!");
                }
                model.tiketService.listarTramos_Ticket(cia,origen,destino);
                model.tiketService.buscarTramo_Ticket(cia,origen,destino,ticket_id);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
