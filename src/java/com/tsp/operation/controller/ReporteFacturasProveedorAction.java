/******************************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   13.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

public class ReporteFacturasProveedorAction extends Action{
    
    /** Creates a new instance of DocumentoInsertAction */
    public ReporteFacturasProveedorAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String pag  = "/jsp/cxpagar/reportes/consultaFacturasProveedorRespuesta.jsp";
        String next = pag;
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            String nit = request.getParameter("nit");
            
            ////System.out.println("�������������������> nit: " + nit);
            
            //fechai += " 7:00:00.0";
            //fechaf = Util.fechaFinal(fechaf + " 6:59:00", 1);
            
            model.cxpDocService.listaDocPorProveedor(nit, "", "");
            Vector vec = model.cxpDocService.getVecCxp_doc();
            
            if ( vec.size()>0 ){
                vec = (Vector) vec.elementAt(0);
            } else {
                next = "/jsp/cxpagar/reportes/consultaFacturasProveedor.jsp?mensaje=OK";
            }
            
            model.identidadService.buscarIdentidad(nit, "");
            Identidad prov = model.identidadService.obtIdentidad();

            request.setAttribute("Vec_cxp_Docs", vec);
            request.setAttribute("idprov", nit);
            request.setAttribute("proveedor", prov);

            ////System.out.println("�������������������> cxpDocs: " + vec.size());
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
