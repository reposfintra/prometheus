/********************************************************************
 *      Nombre Clase.................   AnalisisVencCXPAction.java
 *      Descripci�n..................   Analisis de Vencimiento de Facturas
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   22.05.2003
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.operation.model.threads.RepAnalisisVencTh;

/**
 *
 * @author  Andr�s
 */
public class AnalisisVencCXPAction extends Action{
    Vector detall;
    Vector resum;
    String[] limit;
    Logger log;
    
    /** Creates a new instance of InformacionPlanillaAction */
    public AnalisisVencCXPAction() {
        log = Logger.getLogger(this.getClass());
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        detall = new Vector();
        resum = new Vector();
        limit = new String[3];
        
        String prov = request.getParameter("prove")==null ? "" : request.getParameter("prove");
        String fecha = request.getParameter("fecha");
        
        log.info("......... PROVEEDOR: " + prov);
        log.info("......... FECHA: " + fecha);
        log.info("......... EXPORTAR: " + request.getParameter("exp"));
        
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        String next = "/jsp/cxpagar/reportes/AnalisisVencRpta.jsp";
        
        try{
            model.cxpDocService.facturasNoPagadas(prov, usuario.getId_agencia());
            Vector vec = model.cxpDocService.getVectorFacturas();
            if( vec.size()>0 ){ 
                this.organizar(vec, fecha);
            }
            
            log.info("............ FINAL DETALL INFO: " + detall);
            log.info("............ FINAL RESUM INFO: " + resum);
            
            if( prov.length()==0 ){
                next = "/jsp/cxpagar/reportes/AnalisisVencRptaGen.jsp";
                request.setAttribute("detall", this.resum);
            } else {
                request.setAttribute("detall", this.detall);
            }           
            
            /* Generaci�n del reporte en MS Excel */
            if( request.getParameter("exp")!=null ) {
                RepAnalisisVencTh hilo = new RepAnalisisVencTh();
                
                if( Integer.parseInt(request.getParameter("exp"))== 0 ){
                    hilo.start(model, this.detall, usuario.getLogin(), 0, fecha, limit);
                } else {
                    hilo.start(model, this.resum, usuario.getLogin(), 1, fecha, limit);                    
                }
                
                next = "/jsp/cxpagar/reportes/AnalisisVencMsg.jsp";
            }
            
            request.setAttribute("lim01", fecha);
            request.setAttribute("lim02", limit[0]);
            request.setAttribute("lim03", limit[1]);
            request.setAttribute("lim04", limit[2]);
            request.setAttribute("prove", prov);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    public void organizar(Vector info, String fecha){
        
        this.loadLimites(fecha);
        String pro = "";
        
        double col01 = 0;
        double col02 = 0;
        double col03 = 0;
        double col04 = 0;
        double col05 = 0;
        
        
        Hashtable ht = new Hashtable();
        String nit = "";
        String no = "";
        String fec = "";
        String fecvenc = "";
        String vr = "";
        String nom = "";
        
        for( int i = 0; i < info.size(); i++){
            
            CXP_Doc fac = (CXP_Doc) info.elementAt(i);
            
            if( i==0 ) pro = fac.getProveedor();
            
            if( pro.compareTo(fac.getProveedor())!=0){
                ht = new Hashtable();
                log.info("................................ cambio de proveedor.");
                pro = fac.getProveedor();
                
                ht.put("nit", nit);
                ht.put("nomprov",  nom);
                ht.put("no",  no);
                ht.put("fec", fec);
                ht.put("fecvenc", fecvenc);
                ht.put("col01", "" + col01);
                ht.put("col02", "" + col02); 
                ht.put("col03", "" + col03); 
                ht.put("col04", "" + col04); 
                ht.put("col05", "" + col05); 
                ht.put("ttl", "ok");
                
                col01 = 0;
                col02 = 0;
                col03 = 0;
                col04 = 0;
                col05 = 0;
                
                log.info("............. " + i + ": total: " + ht);
                this.detall.add(ht);
                this.resum.add(ht); 
            }     
            
            nit = fac.getProveedor();
            no = fac.getDocumento();
            fec = fac.getFecha_documento();
            fecvenc = fac.getFecha_vencimiento().substring(0, 10);
            vr = String.valueOf(fac.getVlr_neto()); 
            nom = fac.getNomProveedor();
            
            ht = new Hashtable();            
            
            ht.put("nit", nit);
            ht.put("nomprov",  nom);
            ht.put("no",  no);
            ht.put("fec", fec);
            ht.put("fecvenc", fecvenc);
            
            
            int dias = com.tsp.util.Util.diasTranscurridos(fecha, fecvenc);
            //log.info(".......... DIAS TRANSC. " + fecha + " - " + fecvenc + " : " + dias);
            
            if( dias < 0 ){
                ht.put("col01", vr);
                col01 += fac.getVlr_neto();
            } else if( dias >= 0 && Long.parseLong(fecvenc.replaceAll("-",""))<Long.parseLong(limit[0].replaceAll("-",""))){
                ht.put("col02", vr);
                col02 += fac.getVlr_neto();
            } else if( Long.parseLong(fecvenc.replaceAll("-",""))>=Long.parseLong(limit[0].replaceAll("-",""))
                    && Long.parseLong(fecvenc.replaceAll("-",""))<Long.parseLong(limit[1].replaceAll("-",""))){
                ht.put("col03", vr);
                col03 += fac.getVlr_neto();
            } else if( Long.parseLong(fecvenc.replaceAll("-",""))>=Long.parseLong(limit[1].replaceAll("-",""))
                    && Long.parseLong(fecvenc.replaceAll("-",""))<Long.parseLong(limit[2].replaceAll("-",""))){
                ht.put("col04", vr);
                col04 += fac.getVlr_neto();
            } else {
                ht.put("col05", vr);
                col05 += fac.getVlr_neto();
            }
            
            log.info("............. " + i + " : detalle: " + ht);
            this.detall.add(ht);        
        }
        
        ht = new Hashtable();
        ht.put("nit", nit);
        ht.put("nomprov",  nom);
        ht.put("no",  no);
        ht.put("fec", fec);
        ht.put("fecvenc", fecvenc);
        ht.put("col01", "" + col01);
        ht.put("col02", "" + col02);
        ht.put("col03", "" + col03);
        ht.put("col04", "" + col04);
        ht.put("col05", "" + col05);
        ht.put("ttl", "ok");
        log.info("............. total: " + ht);
        this.detall.add(ht);
        this.resum.add(ht);
    }
    
    public void loadLimites(String fecha){
        
        Calendar cal = com.tsp.util.Util.crearCalendar(fecha + " 00:00:00");
        String vie = this.proxViernes(cal, fecha);
        limit[0] = vie;
        limit[1] = com.tsp.util.UtilFinanzas.addFecha(limit[0], 7, 2);
        limit[2] = com.tsp.util.UtilFinanzas.addFecha(limit[1], 7, 2);
        log.info("................ LIMIT 1: " + limit[0]);
        log.info("................ LIMIT 2: " + limit[1]);
        log.info("................ LIMIT 3: " + limit[2]);
    }
    
    public String proxViernes(Calendar cal, String fecha){
        
        String viernes = fecha;
        int dia = cal.get(cal.DAY_OF_WEEK);
        
        if (dia == cal.MONDAY)
            viernes = com.tsp.util.UtilFinanzas.addFecha(fecha, 4, 2);
        else if ( dia == cal.TUESDAY)
            viernes = com.tsp.util.UtilFinanzas.addFecha(fecha, 3, 2);
        else if ( dia == cal.WEDNESDAY)
            viernes = com.tsp.util.UtilFinanzas.addFecha(fecha, 2, 2);
        else if ( dia == cal.THURSDAY)
            viernes = com.tsp.util.UtilFinanzas.addFecha(fecha, 1, 2);
        else if ( dia == cal.SATURDAY)
            viernes = com.tsp.util.UtilFinanzas.addFecha(fecha, 6, 2);
        else if ( dia == cal.SUNDAY )
            viernes = com.tsp.util.UtilFinanzas.addFecha(fecha, 5, 2);
        
        return viernes;
    }
    
}
