/***************************************
 * Nombre       FormatoNacionalDatosAction.java 
 * Autor        FERNEL VILLACOB DIAZ
 * Fecha        22/10/2006
 * versi�n      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;



public class FormatoNacionalDatosAction extends Action{
    
   
    public void run() throws ServletException, InformationException {
        
         try{
            
             HttpSession session  = request.getSession(); 
             Usuario     usuario  = (Usuario) session.getAttribute("Usuario");
            
             String      user     = usuario.getLogin();
             String      distrito = usuario.getDstrct();
             
             String      CARACTER = " ";
            
             
            String next          = "/jsp/masivo/formato/captura.jsp";
            String msj           = "";  
            String BOTON         = "INSERT";
            String Remisiones ="";
            
            
            
            String   evento    =  request.getParameter("evento");

            if(evento!=null){
                
                
                 
                // INSERTA DATOS
                if(evento.equals("INSERT")  ||   evento.equals("MODIFICAR")  ){
                     
                     String  tipo_doc = request.getParameter("tipoDoc");
                     String  doc      = request.getParameter("documento");
                     String  Remision      = (request.getParameter("Remision")!=null)?request.getParameter("Remision"):"";
                     String  CentroC      = (request.getParameter("CentroC")!=null)?request.getParameter("CentroC"):"";
                     String  Compensac      = (request.getParameter("Compensac")!=null)?request.getParameter("Compensac"):"";
                      String  formato  = model.FormatoSvc.getFormato();
                   //Validamos q existe el documento registrado en la BD.
                     if( model.ImagenSvc.valido(tipo_doc, doc)){                        
                        
                      // Seteamos datos de la posiciones: 
                         int tamanoMAX   =   model.FormatoSvc.getTamanoFormato(formato);
                         String[] vector = new String[tamanoMAX];
                         for(int i=0;i<tamanoMAX;i++)
                               vector[i] = CARACTER;
                        
                      // Capturamos datos de los campos:
                         List   campos   =  model.FormatoSvc.getCampos();
                         for(int i=0;i<campos.size();i++){
                              EsquemaFormato  campo      = (EsquemaFormato)campos.get(i);
                              String          nombreCamp = campo.getCampo();
                              String          datoCampo  =  request.getParameter( nombreCamp );
                              if( datoCampo!=null   ){
                                   int         posIni     = (campo.getPInicial()==0)? campo.getPInicial() :  campo.getPInicial()-1;
                                   int         posFin     = campo.getPFinal();
                                   Remisiones = campo.getRemision();
                                      
                                   char[] dato_vec  = datoCampo.toCharArray();
                                   for(int j=0;j<dato_vec.length;j++){
                                       if( posIni< posFin ){
                                          char caracter   = dato_vec[j];
                                          vector[posIni]  = String.valueOf( caracter );
                                          posIni++; 
                                       }else
                                           break;
                                   }
                              
                              }
                              
                         }
                         
                         String dato = getStringArray(vector);   
                            if( evento.equals("INSERT")    )  msj = model.FormatoSvc.insertar(distrito , tipo_doc   , doc   , formato   , dato  , user, Remision, CentroC,Compensac  ); 
                            if( evento.equals("MODIFICAR") ){
                               msj = model.FormatoSvc.update  (distrito , tipo_doc   , doc   , formato   , dato  , user,Remision, Compensac  ); 
                               model.FormatoSvc.searchDatos   (distrito , tipo_doc   , doc   , formato );
                                  msj  +="&doc="+  doc  +"&tipoDoc="+ tipo_doc; 
                                BOTON = "MODIFICAR";
                            }
                         
                         String temp = "";
                        
                         
                         
                     }
                     else{
                        msj  = "No existe el documento " + doc +" para ese tipo["+ tipo_doc  +"] registrado en la base de datos...";  
                        msj +="&doc="+  doc  +"&tipoDoc="+ tipo_doc; 
                     }
                }
                
                model.FormatoSvc.getCampos();
                
                // BUSCA DATOS INGRESADOS PARA ACTUALIZAR
                if(evento.equals("BUSCAR")){
                     String  formato      =  request.getParameter("formato");
                     String  tipo_doc     =  request.getParameter("tipoDoc");
                     String  doc          =  request.getParameter("documento");
                     model.FormatoSvc.searchDatos(distrito , tipo_doc, doc, formato ); 
                     if( model.FormatoSvc.getCampos().size()==0)
                         msj   = "No se encontraron datos para los parametros de busquedas...";
                     msj +="&doc="+  doc  +"&tipoDoc="+ tipo_doc; 
                     
                     BOTON = "MODIFICAR";
                }
                
                
                
                // BUSCA DATOS INGRESADOS PARA ACTUALIZAR
                if(evento.equals("DELETE")){
                     String  formato      =  request.getParameter("formato");
                     String  tipo_doc     =  request.getParameter("tipoDoc");
                     String  doc          =  request.getParameter("documento");
                     msj = model.FormatoSvc.delete(distrito , tipo_doc   , doc   , formato ); 
                     model.FormatoSvc.loadCampos(formato);
                }
               
                
                
                // CARGA LA LISTA DE FORMATOS
                if(evento.equals("INIT")){
                      model.FormatoSvc.init(); 
                      model.FormatoSvc.loadTFormatos();                                              
                      if( model.FormatoSvc.getTFormatos().size()==0  ) 
                          msj = "No hay formatos definidos ";
                      next    = "/jsp/masivo/formato/Formato.jsp";
                }
                
                
                // SETEA VISTA JSP
                if(evento.equals("RESET")){
                      String  formato  = model.FormatoSvc.getFormato();
                      model.FormatoSvc.loadCampos(formato);
                }
                
                
                
                // CARGA  EL FORMATO EN LA JSP Y CARGA LOS VALORES DE TIPO Y DOCUMENTO
                if(evento.equals("LOAD")){
                      model.FormatoSvc.init();
                      String  formato    =  request.getParameter("formato");
                      String  tipo_doc   =  request.getParameter("tipodoc");
                      String  doc        =  request.getParameter("doc");
                      
                      model.FormatoSvc.loadCampos(formato);
                      model.FormatoSvc.loadTipoDocumentos();
                      List  listCampos  =  model.FormatoSvc.getCampos();                           
                      if(listCampos.size()==0  ){
                          msj   = "No hay campos definidos para el formato " + formato ;
                          next  = "/jsp/masivo/formato/Formato.jsp";
                      }else{
                         msj +="&doc="+  doc  +"&tipoDoc="+ tipo_doc;  
                      }  
                 }
                 
                 
                 // BUSCA CAMPOS DEL FORMATO Y CARGA CAMPOS VACIOS
                  if(evento.equals("CAMPOS")){
                      model.FormatoSvc.init();                      
                      String formato    = request.getParameter("formato");
                      model.FormatoSvc.loadCampos(formato);
                      model.FormatoSvc.loadTipoDocumentos();
                      List  listCampos  =  model.FormatoSvc.getCampos();                          
                      if(listCampos.size()==0  ){
                          msj = "No hay campos definidos para el formato " + formato;
                          next          = "/jsp/masivo/formato/Formato.jsp";
                      }
                  }
                
            }
            
            next += "?msj=" + msj +"&BOTON="+ BOTON;
            model.FormatoSvc.buscarCentrocostos();
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);  
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
    * M�todo que rellena los campos
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/  
    public  String rellenar(String cadena, String caracter, int tope)throws Exception{
       try{
           int lon = cadena.length();
           if(tope>lon){
             for(int i=lon;i<tope;i++){
                 cadena  =  cadena + caracter;
             }
           }
           else
               cadena = Trunc(cadena, tope );
           
       }catch(Exception e){
           throw new Exception(e.getMessage());
       }
       return cadena;
    }

    
   
   /**
    * M�todo que trunca cadena
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/  
   public static String Trunc(String Cadena, int Longitud){
       return (Cadena==null?Cadena: (Cadena.length()>=Longitud? Cadena.substring(0,Longitud):Cadena  )  );
   }
    
    
   
   
   /**
    * M�todo que devuelve el String[] en String
    * @autor.......fvillacob
    * @throws......Exception
    * @version.....1.0.
    **/ 
    public static String getStringArray (String [] array) {
        String nuevo = "";
        if (array!=null){
            for (int i = 0; i< array.length; i++)
                nuevo += array[i];
        }
        return nuevo;
    }
     
    
}
