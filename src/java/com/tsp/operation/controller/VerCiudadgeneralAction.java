/*
 * VerCiudadgeneralAction.java
 *
 * Created on 9 de marzo de 2005, 08:15 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  DIBASMO
 */
public class VerCiudadgeneralAction extends Action {
    
    /** Creates a new instance of VerCiudadgeneralAction */
    public VerCiudadgeneralAction() {
    }
    
    public void run() throws javax.servlet.ServletException {
        String next = "/jsp/trafico/ciudad/VerCiudadGeneral.jsp";
        this.dispatchRequest(next);
    }
    
}
