/*
 * CaravanaBusquedaAction.java
 *
 * Created on 04 de Agosto de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.util.Util;

public class CaravanaFinAction extends Action{
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/caravana/caravana.jsp?agregar=visible&escolta=visible";        
        String planilla = "", escolta = "";
        int numC = Integer.parseInt(request.getParameter("numC"));
        //System.out.println(model.usuario.getDstrct());
        try{ 
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
            //creaci�n de la caravana
            Caravana c = new Caravana();
            Vector vecPlani = new Vector();
            c.setNumcaravana(numC);
            c.setDstrct(usuario.getDstrct());
            c.setAgencia(usuario.getId_agencia());
            c.setBase(usuario.getBase());
            c.setCreation_user(usuario.getLogin());
            c.setFecinicio(Util.getFechaActual_String(4));            
            Vector datos = model.caravanaSVC.getVectorCaravanaActual();
            for (int i=0; i<datos.size(); i++){
                planilla = (request.getParameter("numpla"+i)!=null)?request.getParameter("numpla"+i):"";                
                //System.out.println("numpla "+i+":"+planilla);
                if (!planilla.equals("")) {
                    c.setNumpla(planilla);
                    vecPlani.addElement(planilla);
                    model.caravanaSVC.insertarCaravana(c);
                }
            }
            //Creaci�n del escolta de la caravana
            EscoltaVehiculo e = new EscoltaVehiculo();            
            e.setBase(usuario.getBase());
            e.setCreation_user(usuario.getLogin());
            e.setFecasignacion(Util.getFechaActual_String(4));
            Vector esc = model.escoltaVehiculoSVC.getVectorEscoltas();
            for (int i=0; i<esc.size(); i++){
                escolta = (request.getParameter("placaesc"+i)!=null)?request.getParameter("placaesc"+i):"";                
                //System.out.println("escolta "+i+":"+escolta);
                if (!escolta.equals("")) {
                    e.setPlaca(escolta);
                    //Asociando un escolta a las planillas de una caravana
                    for (int j=0; j<vecPlani.size(); j++){
                        e.setNumpla((String) vecPlani.elementAt(j));
                        model.escoltaVehiculoSVC.insertarEscoltaVehiculo(e);
                    }
                   // model.caravanaSVC.insertarCaravana(c);
                }
            }
            next = "/jsp/trafico/caravana/caravana.jsp";
        }catch (Exception e){
            //throw new ServletException(e.getMessage());           
            e.printStackTrace();
        }        
        this.dispatchRequest(next);
    }
    
}
