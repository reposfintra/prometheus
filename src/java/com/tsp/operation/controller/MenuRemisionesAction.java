/*
 * Nombre        MenuRemisionesAction.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         25 de julio de 2006, 11:03 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.pdf.DocumentoUnicoTransportePDF;

public class MenuRemisionesAction extends Action{
    
    /**
     * Crea una nueva instancia de  MenuRemisiones2Action
     */
    public MenuRemisionesAction() {
    }
    
    public synchronized void run() throws ServletException, InformationException {
        
        String next = "";
        
        try{
            
            String comentario="";
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String Login    = usuario.getLogin();
            String Proyecto = usuario.getProject();
            String Distrito = usuario.getDstrct();
            String usuarioName = usuario.getNombre();
            String tipo = request.getParameter("type");
            Vector v = new Vector();
            
            if(!Login.equals("") ){
                String Agencia     = usuario.getId_agencia();
                
                
                int cantidad     = 0;
                int swWindow     = 0;
                int total        = Integer.parseInt(request.getParameter("total"));
                if(model.RemisionSvc.ExistSerie() ){
                    if(model.RemisionSvc.getList()!=null && total>0){
                        
                        //--- obtenemos las remisiones seleccionadas
                        if( total>0 ){

                                for(int i =1;i<=total;i++){
                                    int parametro = Integer.parseInt(request.getParameter("parametro"+i));
                                    model.RemisionSvc.active(parametro);
                                }
                                next = "/remision/Remisiones.jsp?comentario="+comentario +"&total="+cantidad+"&ventana=0&usuario="+Login;

                            
                            
                            //--- obtenemos la lista de impresio
                            List listaImpresion = model.RemisionSvc.getListImpresion();
                            Remision remision = null;
                            
                            if(listaImpresion!=null && listaImpresion.size()>0){
                                Iterator itPrint = listaImpresion.iterator();
                                while(itPrint.hasNext()){
                                    remision = (Remision)itPrint.next();
                                    if(remision!=null){
                                        
                                        //Remisines por imprimir
                                        v.add(remision);
                                        
                                        
                                        //--- Actualizamos el campo printer_date de la oc
                                                                       model.RemisionSvc.updateRemision(remision.getRemision(),remision.getDistrito());
                                        //---  actualizamos la serie
                                                                        model.RemisionSvc.updateSeries(remision.getDistrito(), remision.getAgencia(), remision.getBanco(),remision.getAgeBanco(), remision.getCuentaBanco() , Login);
                                        Series serie = model.RemisionSvc.getSerie(remision.getDistrito(), remision.getAgencia(), remision.getBanco(), remision.getAgeBanco(), remision.getCuentaBanco());
                                        if(serie!=null){
                                            int ultimo= serie.getLast_number();
                                            int tope  = Integer.parseInt(serie.getSerial_fished_no());
                                            if(ultimo>=tope)
                                                model.RemisionSvc.finallySeries(remision.getDistrito(), remision.getAgencia(), remision.getBanco(), remision.getAgeBanco(), remision.getCuentaBanco(), Login);
                                        }
                                        
                                        double total1=0;
                                        //--- verificamos si la planilla tiene asociado otros conceptos de egreso, siendo asi,
                                        List listadoEgresos=model.RemisionSvc.searchMovimiento(remision.getRemision(),remision.getDistrito(), remision.getAgencia());
                                        if(listadoEgresos!=null){
                                            Iterator itEgresos = listadoEgresos.iterator();
                                            while(itEgresos.hasNext()){
                                                AnticipoPlanilla anticipo = (AnticipoPlanilla)itEgresos.next();
                                                total1+= anticipo.getValor();
                                                model.RemisionSvc.updateMOVOC(remision.getDistrito(), remision.getAgencia(), remision.getEgreso(), "01", remision.getRemision() , Login);
                                                if(anticipo.getMoneda().toUpperCase().equals("PES") )
                                                    model.RemisionSvc.updateDetalleEgreso(remision.getDistrito(), remision.getBanco(),remision.getAgeBanco(),remision.getEgreso(),"0"+anticipo.getConcepto(),anticipo.getConcepto(), remision.getRemision(),anticipo.getValor(),0,anticipo.getMoneda() ,Login);
                                                else
                                                    model.RemisionSvc.updateDetalleEgreso(remision.getDistrito(), remision.getBanco(),remision.getAgeBanco(),remision.getEgreso(),"0"+anticipo.getConcepto(),anticipo.getConcepto(), remision.getRemision(),0,anticipo.getValor(), anticipo.getMoneda() ,Login);
                                          }
                                        }
                                        
                                        //---  actulizamos el egreso
                                                                        model.RemisionSvc.updateEgreso(remision.getDistrito(), remision.getBanco(), remision.getAgeBanco(), remision.getEgreso(),"01", remision.getNit(), remision.getConductor(), remision.getAgencia(), total1,0, remision.getTipoMoneda(),Login);
                                        
                                        //--- actualizamos la lista
                                                                        model.RemisionSvc.delete(remision);
                                    }
                                    
                                }
                            }
                            
                            
                            
                            //--- verificamos que exista remisiones en la lista general
                            if(model.RemisionSvc.getList()==null || model.RemisionSvc.getList().size()==0){
                                comentario="(Action) No hay remisiones para ser impresas...";
                            }
                            
                            //--- cantidad de elementos para ser impresos
                            
                            cantidad =(listaImpresion!=null)?listaImpresion.size():0;
                            DocumentoUnicoTransportePDF d = new DocumentoUnicoTransportePDF();
                            d.imprimirRemisiones(v);
                            request.setAttribute("printed", "");
                            
                        }
                        
                    }else{
                        comentario= model.RemisionSvc.searchRemisiones(Proyecto,Distrito,Agencia,usuarioName,usuario.getBase());
                        next = "/remision/Remisiones.jsp?comentario="+comentario +"&total="+cantidad+"&ventana=0&usuario="+Login;
                    }
                }
                else{
                    comentario="No hay registros en la Serie banco";
                }
                
            }
            
            
            
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
