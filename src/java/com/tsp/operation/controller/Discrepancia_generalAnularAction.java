/***********************************************
 * Nombre clase: Discrepancia_generalAnularAction.java
 * Descripci�n: Accion para anular una diacrepancia a la bd.
 * Autor: Jose de la rosa
 * Fecha: 21 de noviembre de 2005, 03:24 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;

/**
 *
 * @author  EQUIPO13
 */
public class Discrepancia_generalAnularAction extends Action{
    
    /** Creates a new instance of Discrepancia_generalAnularAction */
    public Discrepancia_generalAnularAction () {
    }
    
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String fecha = request.getParameter ("c_fecha");
        String num = (request.getParameter ("c_num_discre")!=null)?request.getParameter ("c_num_discre"):"0";
        int num_discre = Integer.parseInt (num);
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String next = "";
        int sw=0;
        try {
            Discrepancia dis = new Discrepancia ();
            dis.setNro_planilla (numpla);
            dis.setNro_discrepancia (num_discre);
            dis.setUsuario_modificacion (usuario.getLogin ());
            model.discrepanciaService.setDiscrepancia (dis);
            model.discrepanciaService.anularDiscrepanciaGeneral (dis);
            dis.setNro_planilla (numpla);
            dis.setNro_discrepancia (num_discre);
            dis.setFecha_creacion (fecha);
            dis.setUsuario_modificacion (usuario.getLogin ());
            model.discrepanciaService.setDiscrepancia (dis);
            model.discrepanciaService.anularDiscrepanciaProductoGeneral(dis);
            next = "/jsp/cumplidos/discrepancia/DiscrepanciaInsertarGeneral.jsp?c_numpla=" + numpla;
            model.discrepanciaService.listDiscrepanciaGeneral(numpla, distrito);
            request.setAttribute ("msg","Discrepancia Anulada");
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);    
    }
    
}
