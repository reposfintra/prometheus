/**
 * Nombre        ConsultaTransferenciasAction.java
 * Descripci�n   Opciones de Amortizaciones transferidas
 * Autor         Mario Fontalvo Solano
 * Fecha         22 de febrero de 2006, 10:17 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Usuario;

public class ConsultaTransferenciasAction extends Action{
    
    /** Crea una nueva instancia de  AmortizacionesTransferidasAction */
    public ConsultaTransferenciasAction() {
    }
    
    
    /**
     * Metodo principal del Action
     * @autor mfontalvo
     * @throws ServletException.
     * @throws InformationException.
     */    
    public void run() throws ServletException, InformationException {
        try{
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String next = "/jsp/cxpagar/prestamos/consultas/ListadoTransferencias.jsp";
            String Opcion       = defaultString("Opcion", "");
            
            // parametros del formulario de consulta
            String distrito     = defaultString ("distrito","");
            String tercero      = defaultString ("tercero","");
            String fechaInicial = defaultString ("fechaInicial","");
            String fechaFinal   = defaultString ("fechaFinal","");
            
            if (Opcion.equalsIgnoreCase("Consultar")){
                model.PrestamoSvc.actualizarAmortizacionesTransferidas(distrito, fechaInicial, fechaFinal, tercero, usuario.getLogin());
                model.PrestamoSvc.generarTotalesAmortizaciones();
                if (model.PrestamoSvc.getListaAmortizaciones().isEmpty()){
                    next = "/jsp/cxpagar/prestamos/consultas/ConsultaTransferencias.jsp" +
                           "?Mensaje=No se encontraron amortizaciones transferidas para su criterio de busqueda.";
                }
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);   
            
        }catch (Exception ex){
             throw new ServletException(ex.getMessage());
        }
    }
    
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existir este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }    
}
