/*
 * ProductoUpdateAction.java
 *
 * Created on 17 de octubre de 2005, 12:06 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class ProductoUpdateAction extends Action{
    
    /** Creates a new instance of ProductoUpdateAction */
    public ProductoUpdateAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/cumplidos/producto/ProductoModificar.jsp?reload=ok";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String codigo = request.getParameter ("c_codigo");
        String descripcion = request.getParameter ("c_descripcion");
        String cliente = request.getParameter ("codcli");
        String unidad = request.getParameter ("c_unidad");
        try{
            Producto producto = new Producto ();
            producto.setDescripcion (descripcion);
            producto.setCodigo (codigo);
            producto.setDistrito (distrito);
            producto.setCliente (cliente);
            producto.setUnidad (unidad);
            producto.setUsuario_modificacion (usuario.getLogin ());
            if( model.clienteService.existeCliente (cliente) ){
                model.productoService.updateProducto (producto);
                request.setAttribute ("msg","Producto Modificado Exitosamente");
            }
            else
                request.setAttribute ("msg", "Error No existe el cliente");
            model.clienteService.datosCliente (cliente);
            model.productoService.serchProducto (codigo, distrito, cliente);
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
