/*
 * Codigo_demoraInsertAction.java
 *
 * Created on 26 de junio de 2005, 03:36 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class Codigo_demoraInsertAction extends Action{
    
    /** Creates a new instance of Codigo_demoraInsertAction */
    public Codigo_demoraInsertAction() {
    }
    
    public void run() throws ServletException,InformationException {
        String next = "/jsp/trafico/demora/Codigo_demoraInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        String codigo = request.getParameter("c_codigo").toUpperCase();
        String descripcion = request.getParameter("c_descripcion");        
        int sw=0;
        try{
            Codigo_demora codigo_demora = new Codigo_demora();
            codigo_demora.setDescripcion(descripcion);
            codigo_demora.setCodigo(codigo);
            codigo_demora.setCia(dstrct.toUpperCase());
            codigo_demora.setUser_update(usuario.getLogin().toUpperCase());
            codigo_demora.setCreation_user(usuario.getLogin().toUpperCase());            
            codigo_demora.setBase(usuario.getBase().toUpperCase());
            try{
                model.codigo_demoraService.insertCodigo_demora(codigo_demora);
            }catch(SQLException e){
                sw=1; 
            }
            if(sw==1){
                if(!model.codigo_demoraService.existCodigo_demora(codigo)){
                    model.codigo_demoraService.updateCodigo_demora(codigo, descripcion, usuario.getLogin().toUpperCase(),usuario.getBase());
                    request.setAttribute("mensaje","Codigo de Demora insertado con �xito!");
                }
                else{
                    request.setAttribute("mensaje","Codigo de Demora ya existe!");
                }
            }
            else{
                request.setAttribute("mensaje","Codigo de Demora insertado con �xito!");
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
