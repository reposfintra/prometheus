/*
 * AnularZonaAction.java
 *
 * Created on 13 de junio de 2005, 10:01 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

public class AnularZonaAction extends Action{
    
    /** Creates a new instance of AnularZonaAction */
    public AnularZonaAction() {
    }
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";        
        String codigo = (request.getParameter("codigo"));
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String mensaje = request.getParameter("mensaje");
        //System.out.println("zona codigo: "+codigo);
        try{
            if(!model.zonaService.existeZona(codigo) ){
                next = "jsp/trafico/zona/zona.jsp?mensaje=Error_Anular";    
            }
            else{
                next+="?mensaje=Modificar";
                Zona zona = new Zona();
                zona.setCodZona(codigo);               
                zona.setUser_update(usuario.getLogin());
                model.zonaService.eliminarZona(zona);               
            }
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }         
         // Redireccionar a la p�gina indicada.
        //next = Util.LLamarVentana(next, "Eliminar Zona");
        this.dispatchRequest(next);
    }
    
}
