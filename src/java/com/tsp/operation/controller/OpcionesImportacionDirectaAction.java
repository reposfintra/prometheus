/**
 * Nombre        OpcionesImportacionDirectaAction.java
 * Descripción   Manejo de importacion directa
 * Autor         Mario Fontalvo Solano
 * Fecha         21 de enero de 2006, 09:24 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 **/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import com.tsp.operation.model.threads.HImportaciones;
import com.tsp.util.UtilFinanzas;
import java.util.*;



/**
 * Clase de Opciones de Importacion Directa, es decir sube un archivo directamente
 *
 */

public class OpcionesImportacionDirectaAction extends Action  {
    

    /**
     * Session actual
     */
    HttpSession session = null;
    
    /**
     * Usuario en session
     */
    Usuario     usuario = null; 
    
    
    /** Crea una nueva instancia de  OpcionesImportacionDirectaAction */
    public OpcionesImportacionDirectaAction() {
    }
    
    
    
    
    
    /**
     * Procedimiento general del Action
     * @throws ServletException .
     * @throws InformationException .
     */      
    public void run() throws ServletException, InformationException  {
        
        try{
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
                    
            
            String Mensaje = "";
            String next    = "/jsp/general/importacion/importar.jsp";            

            
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
            
            
            /**
             * Carga del los archivos de importacion y datos del formulario
             */
            Upload  upload = new Upload(ruta + "/" ,request);
            upload.load();
            List lista        = upload.getFilesName();
            Dictionary fields = upload.getFields();   
            
            
            
            String Opcion     = (fields.get("Opcion")  !=null?fields.get("Opcion").toString()  :null);
            String tProceso   = (fields.get("tProceso")!=null?fields.get("tProceso").toString():"");
            
            if (Opcion!=null){
                if (Opcion.equals("Importar")){
                    
                    String filename = lista.get(0).toString();
                    
                    Mensaje = model.ImportacionSvc.usuarioValido(filename, usuario.getLogin(), tProceso );
                    if ( Mensaje.equalsIgnoreCase("OK") ) {
                        Mensaje = "Su proceso ha sido iniciado correctamente, verifique el log de Migracion para ver el estado del proceso.";
                        HImportaciones hilo = new HImportaciones();
                        hilo.start(model, usuario , filename, generarConstImportacion(), tProceso);                    
                    } 
                    
                    
                }
            }  
            
            RequestDispatcher rd = application.getRequestDispatcher( next + "?Mensaje=" + Mensaje );
            if (rd == null)
                throw new Exception ("no se pudo encontrar " + next );
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException ("Error en OpcionesImportacionDirectaAction ...\n" + ex.getMessage());            
        }
    }   
    
    
    
    /**
     * Genera la lista de contantes de importacion
     * @autor mfontalvo
     * @throws Exception .
     * @return Retorna una lista de valores contastantes de importacion basados en la session
     */      
    
    private TreeMap generarConstImportacion() throws Exception {  
        
        TreeMap constante = new TreeMap();
        try{
            
            constante.put("_DSTRCT_SESSION_", (String) session.getAttribute("Distrito"));
            constante.put("_USER_SESSION_"  , usuario.getLogin());
            constante.put("_USER_AGENCY_"   , usuario.getId_agencia());
            constante.put("_TODAY_"         , com.tsp.util.Util.getFechaActual_String(6));
            
        }catch (Exception ex){
            throw new Exception ("Error en generarConstImportacion [OpcionesImportacionDirecta] .... \n" + ex.getMessage());
        }
        return constante;
        
    }
    
    
}
