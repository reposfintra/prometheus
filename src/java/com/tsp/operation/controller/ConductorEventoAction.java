/***************************************************
 * Nombre clase:   ConductorEventoAction.java      *
 * Descripci�n:    Accion de eventos de conductor. *
 * Autor:          Ing. Diogenes Bastidas Morales  *
 * Fecha:          9 de Noviembre 2005             *
 * Copyright: Fintravalores S.A. S.A.         *
 ***************************************************/

package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;

public class ConductorEventoAction extends Action{
    
    public void run() throws ServletException, InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String login=usuario.getLogin();
            int sw=0;
            String next="";
            Identidad iden;
            
            String existeconductor="false";
            String comentario="";
            String reg="";
            String clasificacion = ""; 
            Conductor objeto=null;
            //--- obtenemos variables
            
            String evento  = request.getParameter("evento");
            String id      = request.getParameter("identificacion");
            String consulta = request.getParameter("consulta");
            //System.out.println("Consulta " +request.getParameter("consulta"));
            //System.out.println("Evento "+ evento);
            if(consulta!=null ){
                //--- creamos los objetos a partir de las variables   pantalla
                
                objeto = new Conductor();
                //obtenemos los parametros
                String estado = "I";
                String placa  = request.getParameter("placa");
                String pase       = request.getParameter("numeroPase");
                String categoria  = request.getParameter("categoriaPase");
                String fechaPase  = request.getParameter("VencePase");
                String pasaporte         = request.getParameter("pasaporte");
                String fechaPasaporte    = request.getParameter("VencePasaporte");
                String rh    = request.getParameter("grupoSanguineo")+ request.getParameter("rh");
                String lugarNacimiento    = request.getParameter("lugarNacimiento");
                String fechaNacimiento    = request.getParameter("fechaNacimiento");
                String se�a  = request.getParameter("se�aParticular");
                String referencia     = request.getParameter("nombreReferencia");
                String telReferencia  = request.getParameter("telefonoReferencia");
                String dirReferencia  = request.getParameter("direccionReferencia");
                String ciuReferencia  = request.getParameter("ciudadReferencia");
                String jefe     = request.getParameter("nombreJefe");
                String telJefe  = request.getParameter("telefonoJefe");
                String dirJefe  = request.getParameter("direccionJefe");
                String ciuJefe  = request.getParameter("ciudadJefe");
                String documento         = request.getParameter("documento");
                //nuevos campos DIOGENES 121005
                //String griesgo = request.getParameter("griesgo");
                String judicial = request.getParameter("Judicial");
                String vencejudicial = request.getParameter("VenceJudicial");
                String visa = request.getParameter("visa");
                String vecevisa = request.getParameter("VenceVisa");
                String libtripulante = request.getParameter("libTripulante");
                String vencelibTripulante = request.getParameter("VenceLibTripulante");
                String eps = request.getParameter("eps");
                String afiliaeps = request.getParameter("Afiliaeps");
                String nroeps = request.getParameter("nroeps");
                String arp = request.getParameter("arp");
                String afiliaarp = request.getParameter("Vencearp");
                String respase = request.getParameter("respase");
                String pension = request.getParameter("pension");
                String fecafipension = request.getParameter("fecpension");
                
                //formamos el objeto
                objeto.setEstado(estado);
                objeto.setCedula(id);
                objeto.setSupplierNo(placa);
                objeto.setPassNo(pase);
                objeto.setPassCat(categoria);
                objeto.setPassExpiryDate(Util.formatoFechaPostgres(fechaPase)  );
                objeto.setPassport(pasaporte);
                objeto.setPassportExpiryDate(Util.formatoFechaPostgres(fechaPasaporte));
                objeto.setRh(rh);
                objeto.setBirthPlace(lugarNacimiento);
                objeto.setDateBirth(Util.formatoFechaPostgres(fechaNacimiento) );
                objeto.setParticularSign(se�a);
                objeto.setRefName(referencia);
                objeto.setPhoneRef(telReferencia);
                objeto.setAddressRef(dirReferencia);
                objeto.setCityRef(ciuReferencia);
                objeto.setNameLastBoss(jefe);
                objeto.setPhoneBoss(telJefe);
                objeto.setAddressBoss(dirJefe);
                objeto.setCityBoss(ciuJefe);
                objeto.setDocument(documento);
                //nuevos campos
                //objeto.setGradoriesgo(griesgo);
                objeto.setNrojudicial(judicial);
                objeto.setVencejudicial(Util.formatoFechaPostgres(vencejudicial));
                objeto.setNrovisa(visa);
                objeto.setVencevisa(Util.formatoFechaPostgres(vecevisa));
                objeto.setNrolibtripulante(libtripulante);
                objeto.setVencelibtripulante(Util.formatoFechaPostgres(vencelibTripulante));
                objeto.setNomeps(eps);
                objeto.setNroeps(nroeps);
                objeto.setFecafieps(Util.formatoFechaPostgres(afiliaeps));
                objeto.setNomarp(arp);
                objeto.setFecafiarp(Util.formatoFechaPostgres(afiliaarp));
                objeto.setRes_pase(respase);
                objeto.setBase(usuario.getBase());
                objeto.setUsuario(usuario.getLogin());
                objeto.setUsuariocrea(usuario.getLogin());
                objeto.setPension(pension);
                objeto.setFecafipension(Util.formatoFechaPostgres(fecafipension));
            }
            
            //--- eventos seleccionado
            
            if(evento.equals("INSERT") ){
                try{
                    model.conductorService.insertConductorCGA(objeto);
                }
                catch (Exception e){
                    sw=1;
                }
                if( sw == 1){
                    if(model.conductorService.existerConductorAnulCGA(id)){
                        model.conductorService.updateConductorCGA(objeto);
                        try{
                            model.veridocService.AgregarDocumentos("Conductor");//Actualiza los campos de documentos
                        }catch(Exception e){}
                        comentario="Conductor ingresado exitosamente!";
                        model.identidadService.reset();
                    }
                    else{
                        comentario="Error...Conductor ya existe!";
                    }
                    
                }
                else{
                    try{
                        model.veridocService.AgregarDocumentos("Conductor");//Actualiza los campos de documentos
                    }catch(Exception e){}
                    comentario="Conductor ingresado exitosamente!";
                    model.identidadService.reset();
                }
            }
            else if(evento.equals("SEARCH") ){
                model.conductorService.buscar_ConductorCGA(id);
                model.identidadService.buscarIdentidad(id,"");
                iden = model.identidadService.obtIdentidad();
                if (iden != null){
                    model.ciudadService.buscarCiudad(iden.getCodpais(),iden.getCoddpto(),iden.getCodciu());
                    model.tablaGenService.buscarRegistros("RESPASE");
                    
                }
                
                if(model.conductorService.obtConductor()!=null) {
                    existeconductor="true";
                }
                else{
                    comentario="No se encontr� registro con dicha identificaci�n  establecida...";
                }
            }
            else if(evento.equals("UPDATE") ){
                model.conductorService.updateConductorCGA(objeto);
                comentario="Modificacion exitosa!";
                
                
                model.conductorService.buscar_ConductorCGA(id);
                model.identidadService.buscarIdentidad(id,"");
                existeconductor = "true";
            }
            else if(evento.equals("NEW") ){
                model.conductorService.reset();
                model.identidadService.reset();
            }
            else if(evento.equalsIgnoreCase("Verificar") ){
                model.conductorService.buscar_ConductorCGA(id);
                model.identidadService.buscarIdentidad(id,"");
                iden = model.identidadService.obtIdentidad();
                if(iden!=null){
                    model.ciudadService.buscarCiudad(iden.getCodpais(),iden.getCoddpto(),iden.getCodciu());
                    Ciudad ciu =  model.ciudadService.obtenerCiudad();
                    model.tablaGenService.buscarRegistros("RESPASE");
                    // 29-04-2006
                    clasificacion = iden.getClasificacion();
                    char[] tipo = clasificacion.toCharArray();
                    if(tipo[1]=='0'){
                        comentario = "La clasificacion del Nit no corresponde a Conductor";
                        model.identidadService.setIdentidad(null);
                    }
                    
                }
                else{
                    comentario = "El Conductor No Tiene registro en Nit";
                    reg="no";
                }
                
                if(model.conductorService.obtConductor()!=null) {
                    existeconductor = "true";
                }
                
            }
            next = "/jsp/trafico/conductor/Conductor.jsp?estado="+existeconductor+"&comentario="+comentario+"&reg="+reg;
            
            /**
             *  Modificaci�n: Tito Andr�s Maturana 02.02.2006
             */
            if(evento.equalsIgnoreCase("Verificar")  && request.getParameter("proveedor")!=null ){
                next += "&proveedor=OK";
            } else if((evento.equalsIgnoreCase("INSERT") || evento.equalsIgnoreCase("UPDATE"))  &&
            request.getParameter("proveedor")!=null ){
                next = "/controller?estado=Proveedor&accion=Buscar&cmd=show&nit=" + id;
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            //throw new ServletException(e.getMessage());
            e.printStackTrace();
        }
    }
}
