/*
 * Codigo_demoraUpdateAction.java
 *
 * Created on 26 de junio de 2005, 03:47 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Tipo_RecuperacionAUpdateAction extends Action{
    
    /** Creates a new instance of Codigo_demoraUpdateAction */
    public Tipo_RecuperacionAUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="/jsp/masivo/tipo_recuperacion/tipo_recuperacionModificar.jsp?lista=ok";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
    
        try{
            Tipo_Recuperacion_Anticipo tr = new Tipo_Recuperacion_Anticipo();
            tr.setCodigo(codigo);
            tr.setDescripcion(descripcion);
            tr.setUsuario(usuario.getLogin());
            tr.setDistrito(usuario.getDstrct());
            model.trecuperacionaService.settipo_recuperacion(tr);
            if(request.getParameter("modif")!=null){
                model.trecuperacionaService.update();
                request.setAttribute("mensaje","MsgModifitrdo");
            }else{
                model.trecuperacionaService.anular();
                request.setAttribute("mensaje","MsgAnulado");
                next="/jsp/trafico/mensaje/MsgAnulado.jsp";
            }
            model.trecuperacionaService.consultar("", "");
            model.trecuperacionaService.search(codigo);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }
    
}
