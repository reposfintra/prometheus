/*Created on 12 de julio de 2005, 05:10 PM*/

package com.tsp.operation.controller;

/**@author  fvillacob */


import java.io.*;
import java.io.File;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

public class VerDirectorioAction extends Action{
   
    public void run() throws ServletException, InformationException {
   
      try{
           String user =  request.getParameter("usuario");     
           String op   =  request.getParameter("evento");
           String url  =  model.DirectorioSvc.getUrl() + user;
           String comentario = "";
           if(op!=null){
               if(op.equals("LOAD")){
                 ListadoArchivos  directorio = ListadoArchivos.Load(url); 
                 model.DirectorioSvc.setDirectorio(directorio);        
               }
               if(op.equals("DELETE")){
                   int total = Integer.parseInt( (request.getParameter("total")==null)?"-1":request.getParameter("total") );  
                   for (int i=0;i<=total-1;i++){
                      int id       = Integer.parseInt( (request.getParameter("id"+i)==null)?"-1":request.getParameter("id"+i));  
                      Archivo file = model.DirectorioSvc. getArchivo(id);
                      if(file!=null){
                         model.DirectorioSvc.delete( url + "/" +file.getName() );
                         comentario= file.getName();
                      }
                   }
                   ListadoArchivos  directorio = ListadoArchivos.Load(url); 
                   model.DirectorioSvc.setDirectorio(directorio);
               }
               
           }
           
           
           String jsp           = "/directorio/Directorio.jsp?comentario="+ comentario;
           RequestDispatcher rd = application.getRequestDispatcher(jsp);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ jsp);
           rd.forward(request, response); 
       }
       catch(Exception e){
          throw new ServletException("[Accion] "+e.getMessage());
       } 
    }
    
}
