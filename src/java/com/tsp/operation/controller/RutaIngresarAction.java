/*
 * RutaIngresarAction.java
 *
 * Created on 29 de junio de 2005, 03:55 PM
 */ 

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jcuesta
 */
public class RutaIngresarAction extends Action{
    
    /** Creates a new instance of RutaIngresarAction */
    public RutaIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String Mensaje = "";
        //String next = "/jsp/trafico/ruta/ingresarRuta.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        try{
            String cia = (String) session.getAttribute("Distrito");
            String origen = request.getParameter("c_origen");
            String destino = request.getParameter("c_destino");
            String anexoDesc = request.getParameter("desc2");
            String Ruta = request.getParameter("c_via").toUpperCase();
            String secuencia ="";
            String pais_o=request.getParameter("c_paiso");
            String pais_d= request.getParameter("c_paisd");
            model.ciudadService.buscarCiudadxPais(pais_o, request.getParameter("c_origen"));             
            Ciudad co = model.ciudadService.obtenerCiudad();
            model.ciudadService.buscarCiudadxPais(pais_d, request.getParameter("c_destino")); 
            Ciudad cd = model.ciudadService.obtenerCiudad(); 
            
            String descripcion="";
            
            if (!anexoDesc.equals("")){
                descripcion = co.getnomciu() + "-" + cd.getnomciu()+"("+anexoDesc+")" ;
            }else{
                descripcion = co.getnomciu() + "-" + cd.getnomciu();
            }
            
            double duracion = 0;
            double tiempo = 0;
            
            if(model.rutaService.existeRuta(cia, origen, destino)){
                secuencia = model.rutaService.ultimaRuta(cia,origen,destino);
                if(Integer.parseInt(secuencia)< 1000){
                    secuencia = "0"+secuencia; 
                }
            }
            else {
                secuencia = "0100";
            }
            
            if (!model.rutaService.verificarRutaSecuencia(origen, destino, secuencia)){
            
                Ruta v = new Ruta();
                v.setCia(cia);
                v.setOrigen(origen);
                v.setDestino(destino);
                v.setSecuencia(secuencia);
                v.setVia(Ruta);
                v.setPais_o(pais_o);
                v.setPais_d(pais_d);
                v.setUsuario(usuario.getLogin());
                //
                v.setDescripcion(descripcion);
                v.setBase(usuario.getBase());
                v.setDuracion(duracion);
                v.setTiempo(tiempo);

                model.rutaService.setRuta(v);
                model.rutaService.agregarRuta();

                //request.setAttribute("mensaje","La Ruta fue ingresada con exito!");
                Mensaje = "La Ruta fue ingresada con exito!";
            }else{
                Mensaje = "Error! Ya se encuentra creada una Ruta con la secuencia de tramos seleccionada...";
            }
        }catch (Exception e){
            throw new ServletException(e.getMessage());
           // Mensaje = "La Ruta ya Existe!";
        }
        String next = "/jsp/trafico/ruta/ingresarRuta.jsp?Mensaje="+Mensaje;
        this.dispatchRequest(next);
    }
}
