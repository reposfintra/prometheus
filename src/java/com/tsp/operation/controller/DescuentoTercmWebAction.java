/**************************************************************************
 * Nombre clase: DescuentoTercmAction.java                                 *
 * Descripci�n: Clase Recoge los parametros de la jsp y se los envia a el  *
 * Service para procesar los datos                                         *
 * Autor: Ing. Andres MArtinez                                    *
 * Fecha: Created on 22 de Diciembre de 2005, 08:20 AM                     *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.pdf.*;

/**
 *
 * @author  Ivan Dario Gomez
 */
public class DescuentoTercmWebAction extends Action {
    
    /** Creates a new instance of DescuentoTercmAction */
    public DescuentoTercmWebAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String next="";
        String OC     = request.getParameter("OC");
        String Opcion = request.getParameter("Opcion");
        String[] boletas = new String[11];
        String CC   = "";
        String foto = "";
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");
        String NumPrefactura = request.getParameter("NumPrefactura");
        
        try{
            if(Opcion.equals("buscar")){
                
                model.DescuentoTercmWebSvc.Verificar(OC);
                List Existe = model.DescuentoTercmWebSvc.getExiste();
                Iterator Exi = Existe.iterator();
                if(!Exi.hasNext()){
                    //System.out.println("antes");
                    model.DescuentoTercmWebSvc.BuscarValor2(OC);
                    //System.out.println("despues");
                    model.DescuentoTercmWebSvc.Valor();
                    next = "/jsp/sot/body/PantallaCreacionBoletaWeb.jsp";
                }else{
                    next = "/jsp/sot/body/PantallaDescuentoTERCMWeb.jsp?IMP=Existe";
                }
            }
            if(Opcion.equals("BuscarParaImprimir")){
                String OC1 = request.getParameter("OCIMPRIMIR");
                model.DescuentoTercmWebSvc.VerificarImpresa(OC1);
                List ExisteOC = model.DescuentoTercmWebSvc.getExisteImpresa();
                Iterator iterator = ExisteOC.iterator();
                if(iterator.hasNext()){
                    model.DescuentoTercmWebSvc.BuscarValor2(OC1);
                    List val = model.DescuentoTercmWebSvc.getListado();
                    Iterator iter = val.iterator();
                    if(iter.hasNext()){
                        DescuentoTercm  te   = (DescuentoTercm)iter.next();
                        int Valor = te.getValor();
                        model.DescuentoTercmWebSvc.BuscarBoleta(OC1);
                        next = "/jsp/sot/body/PantallaCreacionBoletaWeb.jsp?IMP=OK&Total="+Valor;
                    }
                    
                }else{
                    next = "/jsp/sot/body/PantallaDescuentoTERCMWeb.jsp?IMP=ExisteImpresa";
                }
            }
            
            if(Opcion.equals("guardar")){
                boletas[0]  = (request.getParameter("Boleta1")!="")? request.getParameter("Boleta1"):"";
                boletas[1]  = (request.getParameter("Boleta2")!="")? request.getParameter("Boleta2"):"";
                boletas[2]  = (request.getParameter("Boleta3")!="")? request.getParameter("Boleta3"):"";
                boletas[3]  = (request.getParameter("Boleta4")!="")? request.getParameter("Boleta4"):"";
                boletas[4]  = (request.getParameter("Boleta5")!="")? request.getParameter("Boleta5"):"";
                boletas[5]  = (request.getParameter("Boleta6")!="")? request.getParameter("Boleta6"):"";
                boletas[6]  = (request.getParameter("Boleta7")!="")? request.getParameter("Boleta7"):"";
                boletas[7]  = (request.getParameter("Boleta8")!="")? request.getParameter("Boleta8"):"";
                boletas[8]  = (request.getParameter("Boleta9")!="")? request.getParameter("Boleta9"):"";
                boletas[9]  = (request.getParameter("Boleta10")!="")?request.getParameter("Boleta10"):"";
                
                int Total    = Integer.parseInt(request.getParameter("total"));
                
                model.DescuentoTercmWebSvc.Verificar(OC);
                List Exis = model.DescuentoTercmWebSvc.getExiste();
                Iterator Itera = Exis.iterator();
                if(!Itera.hasNext()){
                    model.DescuentoTercmWebSvc.BuscarValor2(OC);
                    List listado = model.DescuentoTercmWebSvc.getListado();
                    Iterator It = listado.iterator();
                    if(It.hasNext()){
                        DescuentoTercm  tercm   = (DescuentoTercm)It.next();
                        if(Total == tercm.getValor()){
                            model.DescuentoTercmWebSvc.GuardarBoletas(OC, tercm.getPlaca(), tercm.getCedula(), boletas, tercm.getValor(),usuario.getLogin());
                            model.DescuentoTercmWebSvc.BuscarBoleta(OC);
                            //System.out.println("antes!!!");
                            model.DescuentoTercmWebSvc.UpdateFechaImpresionMovpla(OC);
                            //System.out.println("despues!!!");
                            next = "/jsp/sot/body/PantallaCreacionBoletaWeb.jsp?IMP=OK&Total="+Total;
                        }
                    }
                }else{
                    next = "/jsp/sot/body/PantallaCreacionBoletaWeb.jsp?IMP=Existe";
                }
            }
            
            if(Opcion.equals("imprimir")){
                //System.out.println("AAAca!!!");
                model.DescuentoTercmWebSvc.BuscarValor2(OC);
                model.DescuentoTercmWebSvc.BuscarBoleta(OC);
                
                List listaImp = model.DescuentoTercmWebSvc.getListaImpresion();
                Iterator Ite = listaImp.iterator();
                if(Ite.hasNext()){
                    DescuentoTercm desc = (DescuentoTercm) Ite.next();
                    int Ced = desc.getCedula();
                    CC = String.valueOf(Ced);
                }
                //System.out.println("antes");
                model.ImagenSvc.searchImagen(null,null,"1",null,null,null,null, null,CC,null,null,null,null,usuario.getLogin());
                //System.out.println("despues");
                List lista = model.ImagenSvc.getImagenes();
                if (lista!=null){
                    Imagen img  = (Imagen) lista.get(0);
                    foto = "documentos/imagenes/"+ usuario.getLogin() +"/"+img.getNameImagen();
                    //System.out.println("foto path:"+foto);
                }
                
                //next = "/jsp/sot/body/PantallaImprimirBoleta.jsp?foto="+foto;
                
                String ruta = application.getRealPath( "/" );
                boletacombustiblePDF boletacomb = new boletacombustiblePDF();
                try{
                    
                    boletacomb.generarPDF(ruta,model,foto);
                    //System.out.println("update");
                    model.DescuentoTercmSvc.UpdateFechaImpresion(OC);
                    //System.out.println("update");
                }catch(Exception e){
                    e.printStackTrace();
                }
                next = "/pdf/boletacombustible.pdf";
                
            }
            
            if(Opcion.equals("Cambiar")){
                model.DescuentoTercmWebSvc.UpdateFechaImpresion(OC);//Actualiza la fecha de impresion
                next = "/jsp/sot/body/PantallaDescuentoTERCMWeb.jsp";
            }
            /****************************************************/
            /**************Creacion de Prefactura ***************/
            if(Opcion.equals("listar")){
                model.DescuentoTercmWebSvc.PreFactura(fechaini,fechafin);
                next = "/jsp/sot/body/PantallaPreFacturaTERCMWeb.jsp?OP=Listar";
            }
            
            if(Opcion.equals("xls")){
                model.DescuentoTercmWebSvc.Prefacturar(fechaini,fechafin);
                NumPrefactura = Util.getFechaActual_String(Util.FORMATO_YYYYMMDD);
                HListarPrefacturaTercm hilo = new HListarPrefacturaTercm();
                hilo.init(model, usuario, NumPrefactura);
                
                next = "/jsp/sot/body/PantallaPreFacturaTERCMWeb.jsp?OP=mensage";
            }
            /******************************************************/
            /****************************************************/
            /**************Creacion de FACTURA ***************/
            if(Opcion.equals("BuscarPrefactura")){
                String NunPrefactura = request.getParameter("NumPrefactura");
                model.DescuentoTercmWebSvc.BuscarPreFactura(NunPrefactura);
                next = "/jsp/sot/body/PantallaFacturaTERCMWeb.jsp?OP=Listar";
            }
            if(Opcion.equals("marcar")){
                String[] NumBoletas = request.getParameterValues("selec");
                model.DescuentoTercmWebSvc.UpdatePrefactura(NumBoletas);
                HListarPrefacturaTercm hilo = new HListarPrefacturaTercm();
                hilo.init(model, usuario, NumPrefactura);
                next = "/jsp/sot/body/PantallaFacturaTERCMWeb.jsp?OP=mensage";
            }
            
            /******************************************************/
            
           /*
            *   Anulacion De Boletas
            */
            
            if(Opcion.equals("ListarAnular")){
                model.DescuentoTercmWebSvc.BuscarBoletaAnular(OC);//busca las boletas de la oc para anularlas
                next = "/jsp/sot/body/PantallaAnulacionBoletasWeb.jsp?OP=Listar";
            }
            
            if(Opcion.equals("Anular")){
                model.DescuentoTercmWebSvc.AnularBoletas(OC, usuario.getLogin());//busca las boletas de la oc para anularlas
                next = "/jsp/sot/body/PantallaAnulacionBoletasWeb.jsp?OP=mensage";
            }
        }catch (Exception ex){
            ex.printStackTrace();
            //throw new ServletException("Error en DescuentoTercmAction[CONTROLLER] .....\n"+ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
