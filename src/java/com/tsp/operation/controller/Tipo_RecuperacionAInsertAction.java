/*
 * Codigo_discrepanciaInsertAction.java
 *
 * Created on 28 de junio de 2005, 01:41 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Tipo_RecuperacionAInsertAction extends Action{
    
    /** Creates a new instance of Codigo_discrepanciaInsertAction */
    public Tipo_RecuperacionAInsertAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "/jsp/masivo/tipo_recuperacion/tipo_recuperacionInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");   
        String dstrct = usuario.getDstrct();
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        String base = usuario.getBase();
        int sw=0;
        try{
            Tipo_Recuperacion_Anticipo t_r = new Tipo_Recuperacion_Anticipo();
            t_r.setDescripcion(descripcion);
            t_r.setCodigo(codigo);
            t_r.setDistrito(dstrct.toUpperCase());
            t_r.setUsuario(usuario.getLogin());
            t_r.setBase(base);
            model.trecuperacionaService.settipo_recuperacion(t_r);
            try{
                model.trecuperacionaService.insert();
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.trecuperacionaService.exist(codigo)){
                    model.trecuperacionaService.update();                    
                    request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                }
                else{
                    request.setAttribute("mensaje","Error el tipo de recuperacion ya existe!");
                }
            }
            else{
                request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
