/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.Auto_ScoringDAO;
import com.tsp.operation.model.DAOS.impl.Auto_ScoringImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Unidad_Negocio;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.ValorPredeterminadoUn;
import com.tsp.opav.model.beans.UnidadMedida;
import com.tsp.operation.model.beans.variable_mercado;
import com.tsp.operation.model.beans.POIWrite;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;

/**
 *
 * @author user
 */
public class Auto_ScoringAction extends Action {

    private final int CARGAR_PROCESOS_META = 1;
    private final int GUARDAR_PROCESOS_META = 2;
    private final int ACTUALIZAR_PROCESO_META = 3;
    private final int CARGAR_PROCESO_INTERNO = 4;
    private final int CARGAR_UNIDADES_NEGOCIO = 6;
    private final int ASOCIAR_UNIDADES_NEGOCIO = 7;
    private final int CARGAR_UND_NEGOCIOS_PROINTERNO = 8;
    private final int ACTUALIZAR_PROINTERNO = 9;
    private final int DESASOCIAR_UND_PROINTERNO = 10;
    private final int ANULAR_PROCESO_META = 15;
    private final int ANULAR_PROCESO_INTERNO = 16;
    private final int CARGAR_CBO_PROCESOS_META = 17;
    private final int LISTAR_USUARIOS_PROINTERNO = 18;
    private final int LISTAR_USUARIOS_REL_PROINTERNO = 19;
    private final int ASOCIAR_USUARIO_PROINTERNO = 20;
    private final int DESASOCIAR_USUARIO_PROINTERNO = 21;
    private final int EXISTE_USUARIO_REL_PROCESO = 22;
    private final int ACTUALIZAR_MODERADOR_PROINTERNO = 27;
    private final int CREAR_ESPECIFICACION = 33;
    private final int CREAR_SUBCATEGORIA = 34;
    private final int CARGAR_CATEGORIAS = 35;
    private final int CARGAR_COLUMNAS_ESPECIFICACIONES = 36;
    private final int LISTAR_VALORESPREDETERMINADOS = 38;
    private final int CARGAR_UNIDADMEDIDA = 39;
    private final int CARGAR_COMBO = 40;
    private final int guardarValorPredeterminado = 41;
    private final int GUARDARVALPREDESP = 42;
    private final int LISTAR_VALORESPREDETERMINADOS2 = 43;
    private final int LISTAR_VALORESPREDETERMINADOS_FILTRO_EDIT = 49;
    private final int CREAR_ESPECIFICACION_EDIT = 51;
    private final int CARGAR_COMBO_INSUMOS = 52;
    private final int CARGAR_NOMBRES_SUBCATEGORIAS = 55;
    private final int CARGAR_NOMBRES_ESPECIFICACIONES = 56;
    private final int CARGAR_TABLA_SCORING = 57;
    private final int GUARDAR_SCORING = 58;
    private final int CARGAR_GRILLA_SIMULADOR_SCORING = 59;
    private final int CARGAR_VALORES_PREDETERMINADOS_SELECT = 60;
    private final int GUARDAR_VALORES_VARIABLES_MERCADO = 61;
    private final int OBTENER_NEXT_SIMULADOR_NUMBER = 62;
    private final int LISTAR_VALORES_VARIABLE_MERCADO = 63;
    private final int ELIMINAR_VALOR_PREDETERMINADO = 64;

    private Auto_ScoringDAO dao;
    Usuario usuario = null;

    private JsonObject respuesta;
    private boolean ctrlRequest = true;
    String reponseJson = "{}";
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    POIWrite xls;
    private int fila = 0;
    String rutaInformes;
    String nombre;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new Auto_ScoringImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_PROCESOS_META:
                    this.cargarProcesosMeta("A");
                    break;
                case GUARDAR_PROCESOS_META:
                    this.guardarProcesosMeta();
                    break;
                case ACTUALIZAR_PROCESO_META:
                    this.actualizarProcesosMeta();
                    break;
                case CARGAR_PROCESO_INTERNO:
                    this.cargarProcesoInterno("A");
                    break;
                case CARGAR_UNIDADES_NEGOCIO:
                    this.cargarUnidadesNegocio();
                    break;
                case ASOCIAR_UNIDADES_NEGOCIO:
                    this.asociarUnidadesNegocio();
                    break;
                case CARGAR_UND_NEGOCIOS_PROINTERNO:
                    this.cargarUndNegociosProinterno();
                    break;
                case ACTUALIZAR_PROINTERNO:
                    this.actualizarProinterno();
                    break;
                case DESASOCIAR_UND_PROINTERNO:
                    this.desasociarUndProinterno();
                    break;
                case ANULAR_PROCESO_META:
                    this.anularProcesosMeta();
                    break;
                case ANULAR_PROCESO_INTERNO:
                    this.anularProcesoInterno();
                    break;
                case CARGAR_CBO_PROCESOS_META:
                    this.cargarComboProcesosMeta();
                    break;
                case LISTAR_USUARIOS_PROINTERNO:
                    this.listarUsuariosProinterno();
                    break;
                case LISTAR_USUARIOS_REL_PROINTERNO:
                    this.listarUsuariosRelProInterno();
                    break;
                case ASOCIAR_USUARIO_PROINTERNO:
                    this.asociarUsuariosProinterno();
                    break;
                case DESASOCIAR_USUARIO_PROINTERNO:
                    this.desasociarUsuariosProinterno();
                    break;
                case EXISTE_USUARIO_REL_PROCESO:
                    this.existenUsuariosRelProceso();
                    break;
                case ACTUALIZAR_MODERADOR_PROINTERNO:
                    actualizarModerador();
                    break;
                case CREAR_ESPECIFICACION:
                    this.crearEspecificacion();
                    break;
                case CREAR_SUBCATEGORIA:
                    this.guardarSubcategoriaRel();
                    break;
                case CARGAR_CATEGORIAS:
                    this.cargarCategorias();
                    break;
                case CARGAR_COLUMNAS_ESPECIFICACIONES:
                    this.obtenerEspecificacionesColumnas();
                    break;
                case LISTAR_VALORESPREDETERMINADOS:
                    this.listar_valorespredeterminados("A");
                    break;
                case CARGAR_UNIDADMEDIDA:
                    this.cargarunidadmedida("A");
                    break;
                case CARGAR_COMBO:
                    this.cargar_combo();
                    break;
                case guardarValorPredeterminado:
                    this.guardarValorPredeterminado();
                    break;
                case GUARDARVALPREDESP:
                    this.asociarValPred();
                    break;
                case LISTAR_VALORESPREDETERMINADOS_FILTRO_EDIT:
                    this.listar_valorespredeterminados_filtro_edit("A");
                    break;
                case CREAR_ESPECIFICACION_EDIT:
                    this.crearEspecificacionEdit();
                    break;
                case CARGAR_COMBO_INSUMOS:
                    this.cargarComboInsumos();
                    break;
                case CARGAR_NOMBRES_SUBCATEGORIAS:
                    this.cargarNom_subCat();
                    break;
                case CARGAR_NOMBRES_ESPECIFICACIONES:
                    this.cargarNom_Especificaciones();
                    break;
                case LISTAR_VALORESPREDETERMINADOS2:
                    this.listar_valorespredeterminados2("A");
                    break;
                case CARGAR_TABLA_SCORING:
                    this.cargarInfoScoring();
                    break;
                case GUARDAR_SCORING:
                    this.guardarInfoScoring();
                    break;
                case CARGAR_GRILLA_SIMULADOR_SCORING:
                    this.cargarGridSimuladorScoring();
                    break;
                case CARGAR_VALORES_PREDETERMINADOS_SELECT:
                    this.cargarValoresPredeterminados();
                    break;
                case GUARDAR_VALORES_VARIABLES_MERCADO:
                    this.guardarValoresVariablesMercado();
                    break;
                case OBTENER_NEXT_SIMULADOR_NUMBER:
                    this.obtenerNextSimuladorNumber();
                    break;
                case LISTAR_VALORES_VARIABLE_MERCADO:
                    this.listar_valores_VM();
                    break;
                case ELIMINAR_VALOR_PREDETERMINADO:
                    this.eliminarValPred();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarValorPredeterminado() throws IOException {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        respuesta = dao.modificar_Val(info);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void listar_valorespredeterminados(String status) {
        try {
            ArrayList<ValorPredeterminadoUn> lista = dao.listar_valorespredeterminados(usuario.getDstrct(), status);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void listar_valorespredeterminados2(String status) {
        try {
            ArrayList<ValorPredeterminadoUn> lista = dao.listar_valorespredeterminados2(usuario.getDstrct(), status);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listar_valorespredeterminados_filtro_edit(String status) {
        try {
            String id = request.getParameter("id");
            ArrayList<ValorPredeterminadoUn> lista = dao.listar_valorespredeterminados_filtro_edit(usuario.getDstrct(), status, id);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarunidadmedida(String status) {
        try {
            ArrayList<UnidadMedida> lista = dao.cargarunidadmedida(usuario.getDstrct(), status);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void obtenerEspecificacionesColumnas() throws Exception {
        Auto_ScoringDAO dao = new Auto_ScoringImpl(usuario.getBd());
        String subcategoria = request.getParameter("subcategoria");
        this.printlnResponse(dao.obtenerEspecificacionesColumnas(subcategoria), "application/json;");
    }

    public void cargarCategorias() {
        try {

            // String multiservicio = request.getParameter("multiservicio") == null ? "" : request.getParameter("multiservicio");
            String json = dao.cargarCategorias();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarProcesosMeta(String status) {
        try {
            ArrayList<Unidad_Negocio> lista = dao.cargarProcesosMeta(usuario.getDstrct(), status, request.getParameter("insumo"));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * envia respuesta al cliente
     *
     */
    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void guardarProcesosMeta() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String empresa = usuario.getDstrct();
            String idinsumo = request.getParameter("insumo");
            String resp = "{}";
            if (!dao.existeMetaProceso(empresa, nombre)) {
                resp = dao.guardarMetaProceso(idinsumo, empresa, nombre, descripcion, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se cre� el meta proceso, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void crearEspecificacion() {

        try {

            ctrlRequest = true;
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.modificar_espVal(info);
            response.setContentType("application/json; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println((new Gson()).toJson(respuesta));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void crearEspecificacionEdit() {

        try {

            ctrlRequest = true;
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.modificar_espVal_edit(info);
            response.setContentType("application/json; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println((new Gson()).toJson(respuesta));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void actualizarProcesosMeta() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String empresa = usuario.getDstrct();
            String idProceso = request.getParameter("idProceso");
            String resp = "{}";
            if (!dao.existeMetaProceso(empresa, nombre, Integer.parseInt(idProceso))) {
                resp = dao.actualizarMetaProceso(empresa, nombre, descripcion, idProceso, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se actualiz� el meta proceso, puede que este ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarProcesoInterno(String status) {
        try {
            ArrayList<Unidad_Negocio> lista;
            if (request.getParameter("procesoMeta") != null) {
                String idProMeta = request.getParameter("procesoMeta");
                lista = dao.cargarProcesoInterno(Integer.parseInt(idProMeta), status);
            } else {
                lista = dao.cargarProcesoInterno();
            }

            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarSubcategoriaRel() {
        try {
            String nombre = request.getParameter("nombre");
            String procesoMeta = request.getParameter("procesoMeta");
            String resp = "{}";
            int cod = 0;

     
            if (!dao.existeSubcategoria(nombre, usuario.getDstrct())) {
                cod = dao.guardarSubcategoria(nombre, usuario.getLogin(), usuario.getDstrct());
            } else {
                cod = dao.obtenerIdSubcategoria(nombre, usuario.getDstrct());
            }

            if (!dao.existeRelCatSubcategoria(cod, procesoMeta)) {
                dao.guardarRelCatSubcategoria(procesoMeta, cod, usuario.getLogin(), usuario.getDstrct());
            } else {
                dao.actualizarRelCatSubcategoria(procesoMeta, cod, usuario.getDstrct());
            }

            resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarUnidadesNegocio() {

        try {
            String idProceso = request.getParameter("idProceso");
            String idProMetaEdit = request.getParameter("idProMetaEdit");
            ArrayList<variable_mercado> lista = dao.cargarUnidadesNegocio(Integer.parseInt(idProceso), Integer.parseInt(idProMetaEdit));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarUnidadesNegocio() {
        String nomProceso = request.getParameter("nomProceso");
        String idProMeta = request.getParameter("procesoMeta");
        String idProcesointerno = request.getParameter("idProinterno");
        String idProMetaEdit = request.getParameter("idProMetaEdit");
        String listado[] = request.getParameter("listado").split(",");
        int idProInterno = 0;
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            if (idProcesointerno == null) {
                idProInterno = dao.obtenerIdProcesoInterno(nomProceso, idProMeta);
            } else {
                idProInterno = Integer.parseInt(idProcesointerno);
            }
            for (int i = 0; i < listado.length; i++) {

                String campos[] = listado[i].split("-");

                tService.getSt().addBatch(dao.insertarRelUnidadProInterno(idProInterno, campos[0], campos[1], usuario.getDstrct(), Integer.parseInt(idProMetaEdit)));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarValPred() {
        String idProcesointerno = request.getParameter("idProinterno");
        String listado[] = request.getParameter("listado").split(",");
        int idProInterno = 0;
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            if (idProcesointerno != null) {
                idProInterno = Integer.parseInt(idProcesointerno);
            }
            for (int i = 0; i < listado.length; i++) {

                tService.getSt().addBatch(dao.insertarRelValPred(idProInterno, listado[i], usuario.getDstrct(), usuario.getLogin()));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarUndNegociosProinterno() {
        String idProinterno = request.getParameter("idProinterno");
        String idProMetaEdit = request.getParameter("idProMetaEdit");
        try {
            ArrayList<variable_mercado> lista = dao.cargarUndNegocioProinterno(idProinterno, idProMetaEdit);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarProinterno() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            String nombre = request.getParameter("nomProinterno");
            String idProMeta = request.getParameter("idProMeta");
            int idSubcategoria = Integer.parseInt(request.getParameter("idSubcategoria"));
            String resp = "{}";
            if (!dao.existeProcesoInterno(idProMeta, nombre, idProinterno)) {
                resp = dao.actualizarProcesoInterno(idProinterno, nombre, Integer.parseInt(idProMeta), usuario.getLogin(), usuario.getDstrct(), idSubcategoria);
            } else {
                resp = "{\"error\":\" No se actualiz� el proceso interno, puede que ya exista.\"}";
            }
            this.printlnResponse(resp, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void desasociarUndProinterno() {
        try {
            String idSubcategoria = request.getParameter("idSubcategoria");
            String idProInterno = request.getParameter("idProinterno");
            String listado[] = request.getParameter("listado").split(",");
            String resp = "";
            try {

                //if (!dao.verificaExisteSub(Integer.parseInt(idProInterno))) {
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                for (int i = 0; i < listado.length; i++) {
                    tService.getSt().addBatch(dao.eliminarUndProinterno(idSubcategoria, listado[i]));
                }
                tService.execute();
                tService.closeAll();
                resp = "{\"respuesta\":\"OK\"}";

                /*} else {
                 resp = "{\"respuesta\":\"OK1\"}";
                 }*/
                this.printlnResponse(resp, "application/json;");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void anularProcesosMeta() {
        try {
            int idProceso = Integer.parseInt(request.getParameter("idProceso"));
            String resp = dao.anularMetaProceso(idProceso, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void anularProcesoInterno() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            int idProcesoMeta = Integer.parseInt(request.getParameter("idProcesoMeta"));
            String resp = dao.anularProcesoInterno(idProinterno, idProcesoMeta, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarComboProcesosMeta() {
        try {
            String json = dao.cargarComboProcesoMeta(usuario.getDstrct());
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarUsuariosProinterno() {
        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<Usuario> lista = dao.listarUsuariosProinterno(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarUsuariosRelProInterno() {
        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<Usuario> lista = dao.listarUsuariosRelProInterno(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarUsuariosProinterno() {
        String listado = request.getParameter("listado");
        String idProInterno = request.getParameter("idProInterno");

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jo = (JsonObject) jsonParser.parse(listado);
            JsonArray jsonArr = jo.getAsJsonArray("usuarios");

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < jsonArr.size(); i++) {
                String codUsuario = jsonArr.get(i).getAsJsonObject().get("cod_usuario").getAsJsonPrimitive().getAsString();
                String login = jsonArr.get(i).getAsJsonObject().get("id_usuario").getAsJsonPrimitive().getAsString();
                tService.getSt().addBatch(dao.insertarRelProInternoUser(idProInterno, Integer.parseInt(codUsuario), login, usuario.getDstrct()));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void desasociarUsuariosProinterno() {
        try {
            String idProInterno = request.getParameter("idProinterno");
            String listado[] = request.getParameter("listado").split(",");
            try {
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                for (int i = 0; i < listado.length; i++) {
                    tService.getSt().addBatch(dao.eliminarUsuarioProinterno(Integer.parseInt(idProInterno), listado[i]));
                }
                tService.execute();
                tService.closeAll();
                String resp = "{\"respuesta\":\"OK\"}";
                this.printlnResponse(resp, "application/json;");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void existenUsuariosRelProceso() {
        try {
            String id = request.getParameter("idProceso");
            String tipoProceso = request.getParameter("tipo");
            String resp = "{}";
            if (dao.existenUsuariosRelProceso(Integer.parseInt(id), tipoProceso)) {
                resp = "{\"respuesta\":\"SI\"}";
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarModerador() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            int idusuario = Integer.parseInt(request.getParameter("idusuario"));
            String moderador = request.getParameter("moderador");
            String resp = dao.actualizarModerador(idProinterno, idusuario, moderador);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_combo() throws IOException {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        respuesta = dao.cargar_combo(info);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void cargarComboInsumos() {
        try {
            String json = dao.cargarComboInsumos(usuario.getDstrct());
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarNom_subCat() {
        try {
            ArrayList<String> lista = dao.cargarNom_subCat(request.getParameter("insumo"));
            Gson gson = new Gson();
            String json = gson.toJson(lista);
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarNom_Especificaciones() {
        try {
            ArrayList<String> lista = dao.cargarNom_Especificacion(request.getParameter("categoria"));
            Gson gson = new Gson();
            String json = gson.toJson(lista);
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
        }

    }
    
     public void cargarInfoScoring() {
        try {

            String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") :  "";
            String json = dao.cargarInfoScoring(id_unidad_negocio);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Auto_ScoringAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void guardarInfoScoring() {
        try {

            String listadoScoring = request.getParameter("listadoScoring");

            JsonParser jsonParser = new JsonParser();
            JsonObject jdocs = (JsonObject) jsonParser.parse(listadoScoring);
            JsonArray jsonArr = jdocs.getAsJsonArray("data");
            JsonArray jsonArrScores = jsonArr.get(0).getAsJsonObject().get("conf_puntajes").getAsJsonArray();

            String id_unidad_negocio = jsonArr.get(0).getAsJsonObject().get("id_unidad_negocio").getAsJsonPrimitive().getAsString();
            String puntaje_maximo = jsonArr.get(0).getAsJsonObject().get("puntaje_maximo").getAsJsonPrimitive().getAsString();
            String valor_puntaje = jsonArr.get(0).getAsJsonObject().get("valor_puntaje").getAsJsonPrimitive().getAsString();
            String guardar_min_max = jsonArr.get(0).getAsJsonObject().get("guardar_min_max").getAsJsonPrimitive().getAsString();         
            
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.actualizarScoreUndNegocio(Integer.parseInt(id_unidad_negocio), puntaje_maximo, guardar_min_max, valor_puntaje));
            for (int i = 0; i < jsonArrScores.size(); i++) {
              
                int id_conf_pred = jsonArrScores.get(i).getAsJsonObject().get("id_conf_pred").getAsJsonPrimitive().getAsInt();             
                String puntaje = jsonArrScores.get(i).getAsJsonObject().get("puntaje").getAsJsonPrimitive().getAsString();
                String checkedScore = jsonArrScores.get(i).getAsJsonObject().get("checkedScore").getAsJsonPrimitive().getAsString();   
                tService.getSt().addBatch(dao.actualizarScoring(id_conf_pred, puntaje, checkedScore, guardar_min_max));                            
            }

            tService.execute();
            tService.closeAll();

            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void cargarGridSimuladorScoring() {
        try {

            String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") : "";
            String json = dao.cargarGridSimuladorScoring(id_unidad_negocio);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Auto_ScoringAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarValoresPredeterminados() {
        try {

            String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") :  "";
            String json = dao.cargarValoresPredeterminados(id_unidad_negocio);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Auto_ScoringAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void guardarValoresVariablesMercado() {
        try {
            String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") :  "";
            String num_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") :  "";
            String listadoVariables = request.getParameter("listadoVariables");

            JsonParser jsonParser = new JsonParser();
            JsonObject jdocs = (JsonObject) jsonParser.parse(listadoVariables);
            JsonArray jsonArr = jdocs.getAsJsonArray("variables_mercado");          
  
           /* TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();*/
            String fields ="", values ="";         
            for (int i = 0; i < jsonArr.size(); i++) {
                String id_variable_mercado = jsonArr.get(i).getAsJsonObject().get("id_variable_mercado").getAsJsonPrimitive().getAsString();
                String variable_mercado = jsonArr.get(i).getAsJsonObject().get("variable_mercado").getAsJsonPrimitive().getAsString().replaceAll(" ", "_").toLowerCase();
                String valor = jsonArr.get(i).getAsJsonObject().get("valor").getAsJsonPrimitive().getAsString();   
                fields = fields + variable_mercado + ",";    
                values = values + valor + ","; 
            }
            fields = fields + "id_solicitud"; 
            values = values + "'" + num_solicitud + "'";
           /* tService.getSt().addBatch(dao.guardarValoresVM(id_unidad_negocio, fields, values, usuario.getLogin()));                      
            tService.execute();*/
            dao.guardarValoresVM(id_unidad_negocio, fields, values, usuario.getLogin());        
            //Calculamos minimo
             String puntaje_minimo =  dao.simularScoring(num_solicitud,id_unidad_negocio,0);          
            //Calculamos maximo
             String puntaje_maximo = dao.simularScoring(num_solicitud,id_unidad_negocio,1);
             //Actualizamos consecutivo usado para simulacion
            dao.actualizaConsecutivoSimulador();
            //tService.getSt().addBatch(dao.simularScoring("39207",id_unidad_negocio,0)); 
           // tService.getSt().addBatch(dao.simularScoring("39207",id_unidad_negocio,1)); 
            //tService.execute();
          /*  JsonObject jResult = dao.cargarInfoSimulacion(id_unidad_negocio, num_solicitud);
            String puntaje_minimo = jResult.get("puntaje_minimo").getAsJsonPrimitive().getAsString();
            String puntaje_maximo = jResult.get("puntaje_maximo").getAsJsonPrimitive().getAsString();*/          
            String resp = (puntaje_minimo.equals("")) ?  "{\"error\":\"Ocurrio un error al obtener minimo\"}" : (puntaje_maximo.equals("")) ? "{\"error\":\"Ocurrio un error al obtener maximo\"}" : "{\"respuesta\":\"OK\",\"minimo\":\""+puntaje_minimo+"\",\"maximo\":\"" + puntaje_maximo + "\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private static String removeLastChar(String str) {
        return str.substring(0,str.length()-1);
    }
    
    private void obtenerNextSimuladorNumber() {
        try {
            String resp = "{}";
            resp =  dao.get_Next_Consecutivo_Simulador();
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void listar_valores_VM() {
        try {
            String id_variable_mercado = (request.getParameter("id_variable_mercado")) != null ? request.getParameter("id_variable_mercado"): "";
            String tipo_entrada = (request.getParameter("tipo_entrada")) != null ? request.getParameter("tipo_entrada"): "0";
            ArrayList<ValorPredeterminadoUn> lista = (tipo_entrada.equals("1")) ? dao.listar_valoresVariableMercado2(id_variable_mercado) : dao.listar_valoresVariableMercado(id_variable_mercado);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void eliminarValPred() {
        String id_variable_mercado = (request.getParameter("id_variable_mercado")) != null ? request.getParameter("id_variable_mercado"): "0";
        String id_vp = (request.getParameter("id_vp")) != null ? request.getParameter("id_vp"): "0";    
      
        try {
          
            respuesta = dao.eliminar_Val_Pred(id_variable_mercado,id_vp);
            response.setContentType("application/json; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println((new Gson()).toJson(respuesta));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
