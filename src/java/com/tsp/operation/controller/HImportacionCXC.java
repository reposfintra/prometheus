/**
 * Nombre        HImportacionCXP.java
 * Descripci�n   Hilo de control y verificacion de datos de importacion de facturas
 * Autor         Mario Fontalvo Solano
 * Fecha         16 de febrero de 2006, 10:40 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.*;
import com.tsp.util.UtilFinanzas;
import org.apache.log4j.Logger;
import com.tsp.operation.model.threads.*;

import java.io.*;
import java.util.*;
import java.text.*;


public class HImportacionCXC extends Evento {
        //2009-09-02

    
    
    // varibales generales de la facturas
    Model   model;
    TreeMap cias;
    TreeMap tbldoc;
    Logger  logger = Logger.getLogger(HImportacionCXP.class);
    
    CIA       cia       = null;
    Cliente cliente     = null;
    Banco     banco     = null;
    TreeMap   monedas   = null;
    
    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();
    
    
    
    public HImportacionCXC() {
    }
    
    /** Crea una nueva instancia de  HImportacionCXP */
    public HImportacionCXC(Usuario usuario) {
        super(usuario);
    }
    
    
    /** Procedimiento general del hilo */
    public synchronized void run(){
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/exportar/migracion/" + this.getUsuario().getLogin();
            File archivo = new File(ruta);
            archivo.mkdirs();
            fw = new FileWriter(ruta + "/MigracionFacturas_"+ fmt2.format(fechaActual) +".txt");
            pw = new PrintWriter(new BufferedWriter(fw));
            
            
            try{
                model = new Model();
                writeLog("*************************************************");
                writeLog("* Inicio del Proceso de Importacion de Facturas Cliente *");
                writeLog("*************************************************");
                model = new Model();
                Procesar();
                writeLog("*************************************************");
                writeLog("*               Proceso Finalizado              *");
                writeLog("*************************************************");
            }catch (Exception ex){
                ex.printStackTrace();
                writeLog("*************************************************");
                writeLog("*    Finalizando Proceso pero con anomalias     *");
                writeLog("*************************************************");
                writeLog(ex.getMessage());
            }
            
            
            pw.close();
        }catch (Exception ex){
            ex.printStackTrace();
            //System.out.println(ex.getMessage());
        }
    }
    
    
    
    /**
     * procedimiento general de importacion lectura y agrupacion de de datos
     * @autor mfontalvo
     * @throws Exception.
     * @see ProcesarFactura(List , TreeMap , TreeMap )
     */
    
    public void Procesar()throws Exception{
        
        try {
            int fprocesadas   = 0;
            int fnoprocesadas = 0;
            List lista = model.ImportacionCXCSvc.searchFacturaPendiente(this.getUsuario().getLogin());
            if (lista != null && lista.size()>0){
                
                CXPImportacion anterior = null;
                int ranInicial = 0;
                
                /** busqueda parametros de busqueda general */
                cias    = model.ImportacionCXPSvc.searchMonedasCias();
                tbldoc  = initDocumentos();
                monedas = initMonedas();
                
                
                /** intenta procesar la ultima factura con sus items */
                try{
                    if(!ProcesarFactura(lista)){
                        writeLog2("");
                        writeLog2("");
                        writeLog2("PROCESO TERMINADO CON ANOMALIAS");
                        writeLog2("");
                    }
                    
                }catch (Exception ex){
                    writeLog2( ex.getMessage() );
                }
                
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.error( e.getMessage() );
            writeLog2   ( e.getMessage() );
            throw new Exception(e.getMessage());
        }
    }
    
    
    /** procedimiento de una factura especifica con sus items */
    
    public boolean ProcesarFactura(List factura)throws Exception{
        String msgError = "";
        boolean estado = false;
        if (factura!=null ){
            
            
            
            factura_importacion factu =  (factura_importacion) factura.get(0);
           /*
            writeLog2("____________________________________________");
            writeLog2("Distrito   : " + pItem.getDstrct()         );
            writeLog2("Proveedor  : " + pItem.getProveedor()      );
            writeLog2("Tipo Doc   : " + pItem.getTipo_documento() );
            writeLog2("Documento  : " + pItem.getDocumento()      );
            writeLog2("Nro. Items : " + factura.size() + "\n"     );*/
            //System.out.println("ITEMS: " + factura.size());
            
            try {
                TreeMap impItem = new TreeMap();
                TreeMap impFact = new TreeMap();
                Vector  remesas   = new Vector();
                factura cab     = verificacionCabecera(factu);
                double total_fact  = 0;
                String distrito="";
                /** recorriendo los items de la factura */
                boolean sw = true;
                for (int  i = 0 ; i < factura.size() ; i++) {
                    factura_importacion item = (factura_importacion) factura.get(i);
                    distrito = item.getDstrct();
                    // cambio del numero del item a un numero valido
                    writeLog2("Verificando Item : " + item.getItem());
                    try{
                        factura_detalle factu_deta = verificacionItem(item);
                        
                        total_fact  += factu_deta.getValor_itemme();
                        if((this.getValor(factu_deta.getCantidad().doubleValue() * factu_deta.getValor_unitariome(),cab.getMoneda())) != factu_deta.getValor_itemme())
                            throw new Exception("la cantidad  por(*) el valor unitario no es igual al valor del item  ["+ item.getItem() +"],  favor verificar");
                        
                        factu_deta.setValor_item(this.getValor(factu_deta.getValor_itemme()*cab.getValor_tasa().doubleValue(),cia.getMoneda()));
                        factu_deta.setMoneda(cab.getMoneda());
                        factu_deta.setValor_tasa(cab.getValor_tasa());
                        //Faltan los demas datos de los item
                        
                        
                        remesas.add(factu_deta);
                    } catch (Exception ex){
                        writeLog2(ex.getMessage() + "\n");
                        sw = false;
                    }
                }
                model.facturaService.setRemesasFacturar(remesas);
                
                if (sw){
                    // validacion de totales
                    if (total_fact != cab.getValor_factura())
                        throw new Exception("El valor de factura ["+ cab.getValor_factura() +"], no corresponde con la sumatoria de sus items, favor verificar");
                    
                    // actualizacion de totales
                    cab.setValor_facturame(cab.getValor_factura());
                    cab.setValor_saldome(cab.getValor_factura());
                    
                    cab.setValor_saldo(this.getValor(cab.getValor_factura()*cab.getValor_tasa().doubleValue(),cia.getMoneda()));
                    cab.setValor_factura(this.getValor(cab.getValor_factura()*cab.getValor_tasa().doubleValue(),cia.getMoneda()));
                    
                    cab.setAgencia_cobro(model.facturaService.buscarAgenciaCobro(cab.getCodcli()));
                    
                    // grabacion de la factura
                    try{
                        String sql="";
                        sql = model.facturaService.ingresarFactura(cab, distrito,this.getUsuario().getLogin(), this.getUsuario().getBase());
                        
                        sql += model.facturaService.ingresarFacturaDetalle(cab, distrito,this.getUsuario().getLogin(), this.getUsuario().getBase(),model.facturaService.getNumfac());
                            
                            TransaccionService tService = new TransaccionService(this.getUsuario().getBd());  
                            tService.crearStatement();
                            tService.getSt().addBatch(sql);  // Cabecera, Detalle, Impuesto detalle
                            tService.execute();
                        
                        
                    }catch (Exception ex){
                        throw new Exception("Error al momento de grabar factura.... \n" + ex.getMessage());
                    }
                   // model.ImportacionCXPSvc.updateFacturasMigrada(cab);
                    writeLog2("");
                    writeLog2("");
                    writeLog2("");
                    writeLog2("");
                    writeLog2("FACTURA "+model.facturaService.getNumfac() + " PROCESADA CON EXITO.");
                    estado = true;
                }
            }
            catch(Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }
        }
        return estado;
    }
    
    
    private factura verificacionCabecera(factura_importacion pItem) throws Exception {
        try{
            
            String msgError = "";
            
            factura cab = new factura();
            
            // compa�ia
            cia = (CIA) cias.get( pItem.getDstrct() );
            if (cia==null){
                msgError += "\nDistrito '"+ pItem.getDstrct() +"' no esta registrado en la tabla de Cias";
            }
            
            if (cia!=null && !cia.getDistrito().equals(this.getUsuario().getDstrct())){
                msgError += "\nEl distrito del usuario que ejecuta el proceso es diferente al de la factura. deben ser iguales";
            }
            
            
            
            // proveedor
            cliente  = model.ImportacionCXCSvc.searchDatosCliente(pItem.getCodcli() );
            if (cliente == null){
                msgError += "\nCliente '"+ pItem.getCodcli() +"', no registrado en la base de datos";
            }
            
            
            // moneda
            double tasa = 0;
            if  ( monedas.get( pItem.getMoneda() )==null ) {
                msgError += "\nLa moneda de la factura no es valida.";
            } else {
                if  (!cia.getMoneda().equals( pItem.getMoneda() ) ) {
                    tasa = model.ImportacionCXPSvc.searchTasa( cia.getDistrito(), pItem.getFecha_factura(), pItem.getMoneda(), cia.getMoneda());
                    if (tasa == 0) msgError += "\nNo se pudo convertir la moneda de [" + pItem.getMoneda() + "-" + cia.getMoneda() + "]";
                } else
                    tasa = 1;
            }
            
            
            if (!this.getUsuario().getLogin().equals( pItem.getCreation_user() )){
                msgError += "\nEl usuario creacion debe archivo debe ser el mismo que ejecute el proceso";
            }
            if (!this.getUsuario().getLogin().equals( pItem.getUser_update() )){
                msgError += "\nEl usuario actualizacion debe archivo debe ser el mismo que ejecute el proceso";
            }
            
            
            if (!msgError.equals(""))
                throw new Exception(msgError.replaceFirst("\n",""));
            cab.setCodcli               ( pItem.getCodcli());
            cab.setNit                  ( pItem.getNit());
            cab.setRif                  ( cliente.getRif());
            cab.setCmc                  ( cliente.getCmc());
            cab.setAgencia_facturacion  ( cliente.getAgenciaFacturacion());
            cab.setDescripcion          ( pItem.getDescripcion() );
            cab.setObservacion          ( pItem.getObservacion());
            cab.setFecha_factura        ( pItem.getFecha_factura());
            cab.setPlazo                ( pItem.getPlazo());
            cab.setValor_factura        ( getValor(pItem.getValor_factura(),pItem.getMoneda()));
            cab.setValor_tasa           ( new Double(tasa) );
            cab.setForma_pago           ( pItem.getForma_pago());
            cab.setMoneda               ( pItem.getMoneda());
            
            
            
            return cab;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    
    private factura_detalle verificacionItem(factura_importacion item) throws Exception{
        try{
            
            String msgError = "";
            
            factura_detalle factu_deta = new factura_detalle();
            
            if(!item.getNumero_remesa().equals("")){
                if(!model.ImportacionCXCSvc.exiteRemesa(item.getNumero_remesa(),item.getCodcli())){
                    msgError += "\nEl numero de remesa  '"+ item.getNumero_remesa() +"'  no existe";
                }
            }else{
                 msgError += "\nLa remesa no puede estar vacia";
            }
            
            // validaciones de la cuenta
            if ( item.getCodigo_cuenta_contable().trim().equals("") )
                msgError += "\nDebe indicar el codigo de cuenta";
            else {
                String msg = model.ImportacionCXPSvc.searchCuenta(item.getDstrct(), item.getCodigo_cuenta_contable(), item.getTiposubledger(), item.getAuxliliar());
                if ( !msg.equals("OK" ) )  msgError += "\n" + msg;
            }
           /* 
            if(item.isExtraflete() && item.getNumero_remesa().equals("")){
                msgError += "\nDebe indicar el numero de remesa para el extraflete";
            }
            
            if(item.isExtraflete() && item.getCodigo().equals("")){
               msgError += "\nDebe indicar el codigo del concepto para el extraflete";
            }
            
            if(item.isExtraflete() && item.getStd_job_no().equals("")){
               msgError += "\nDebe indicar el Estandar  para el extraflete";
            }
            
            if(item.isExtraflete() && !item.getNumero_remesa().equals("")){
                Vector cr= model.facturaService.costoReembolsable(item.getNumero_remesa());
                
            }*/
            
            if (!msgError.equals("")){
                throw new Exception(msgError.replaceFirst("\n",""));
            }
           /* 
            if(item.isExtraflete()){
              factu_deta.setPlanilla            (item.getNumero_remesa());
              factu_deta.setConcepto            (item.getCodigo());
              factu_deta.setStd_job_no          (item.getStd_job_no());
            }
            */
            factu_deta.setNumero_remesa         (item.getNumero_remesa());
            factu_deta.setFecrem                (item.getFecrem());
            factu_deta.setDescripcion           (item.getDescripcion_item());
            factu_deta.setValor_unitariome      (this.getValor(item.getValor_unitario(),item.getMoneda()));
            factu_deta.setCantidad              (item.getCantidad());
            factu_deta.setValor_itemme          (this.getValor(item.getValor_item(),item.getMoneda()));
            factu_deta.setCodigo_cuenta_contable(item.getCodigo_cuenta_contable());
            factu_deta.setTiposubledger         (item.getTiposubledger());
            factu_deta.setAuxliliar             (item.getAuxliliar());
            String[] c={"",""};
            c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());
            factu_deta.setAux(c[1]);             
            
            
            return factu_deta;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
   
   
    public TreeMap initMonedas(){
        TreeMap mon = new TreeMap();
        mon.put("PES","PES");
        mon.put("DOL","DOL");
        mon.put("BOL","BOL");
        mon.put("SUC","SUC");
        return mon;
    }
    
    
    public TreeMap initDocumentos(){
        TreeMap mon = new TreeMap();
        mon.put("010","010");
        mon.put("035","035");
        mon.put("036","036");
        return mon;
    }
    
    
    
    public double getValor(double valor, String moneda){
        int decimales = (moneda.equals("DOL")?2:0);
        return com.tsp.util.Util.roundByDecimal(valor, decimales);
    }
    
    public void writeLog(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public void writeLog2(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length ; i++)
                    pw.println( msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }
    
    
    public static void main(String [] args) throws Exception{
/*        HImportacionCXP imp = new HImportacionCXP(null);
 */
        Usuario usuario = new Usuario();
        usuario.setLogin("HOSORIO");
        Evento event = (Evento) com.tsp.util.Util.runClass("com.tsp.operation.model.threads.HImportacionCXP");
        event.setUsuario(usuario);
        event.start();
    }
    
    
    /**
     * Metodo para obtener el numero valido del item
     * @autor mfontalvo
     * @param item, cadena a cambiar
     * @throws Exception.
     * @return Numero valido del item
     **/
    public String getItem(String item) throws Exception{
        try{
            String is = "";
            int    in = Integer.parseInt(item);
            if (in < 100)
                is = com.tsp.util.UtilFinanzas.rellenar( String.valueOf(in) , "0", 3);
            else
                is = String.valueOf(in);
            
            return is;
        } catch (Exception ex) {
            throw new Exception("Item no valido");
        }
    }
}
