/********************************************************************
 *      Nombre Clase.................   FacturaRcargarBancosAction.java
 *      Descripci�n..................   Action que se encarga de generar el reporte de documentos por pagar
 *      Autor........................   David Lamadrid
 *      Fecha........................   1 de noviembre de 2005, 05:00 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaRmostrarAction extends Action
{
        
        /** Creates a new instance of FacturaRmostrarAction */
        public FacturaRmostrarAction ()
        {
        }
        
        public void run () throws ServletException, InformationException
        {
                try
                {
                        java.util.Date utilDate = new java.util.Date (); //fecha actual
                        long lnMilisegundos = utilDate.getTime ();
                        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp (lnMilisegundos);
                        String reporte=""+request.getParameter ("c_general");
                        //System.out.println("reporte "+ reporte);
                        if(reporte.equals ("general"))
                        {
                                model.cxpDocService.listaDoc ();
                        }
                        else
                        {
                                String proveedor = request.getParameter ("c_proveedores");
                                //System.out.println("Proveedor: "+proveedor);
                                String banco = request.getParameter ("c_banco");
                                //System.out.println("banco"+banco);
                                if(banco.equals ("todos"))
                                {
                                        banco="";
                                }
                                String sucursal = request.getParameter ("c_sucursal");
                                //System.out.println("sucursal" + sucursal);
                                model.cxpDocService.listaDocPorProveedor (proveedor,banco,sucursal);
                        }
                        String criterio_t = request.getParameter ("c_criterio_t");
                        //System.out.println("CRITERIO TOTAL  " +criterio_t);
                        String criterio_d = request.getParameter ("c_criterio_d");
                        //System.out.println("CRITERIO DISPLAY "+criterio_d);
                        
                        //System.out.println("RANGO F "+request.getParameter ("c_rango_f"));
                        int rango_i=0;
                        int rango_f=0;
                        try
                        {
                                rango_i = Integer.parseInt (request.getParameter ("c_rango_i"));
                                rango_f = Integer.parseInt (request.getParameter ("c_rango_f"));
                        }
                        catch(java.lang.NumberFormatException e)
                        {
                                rango_i=1;
                                rango_f=30;
                        }
                        //System.out.println("rango inicial " + rango_i);
                        //System.out.println("rango final "+rango_f);
                        
                        Vector vDocumentos = model.cxpDocService.getVecCxp_doc ();
                        String next = "/jsp/cxpagar/facturasxpagar/reporteDXP1.jsp?criterio_t="+criterio_t+"&rango_i="+rango_i+"&rango_f="+rango_f;
                        if(criterio_d.equals ("no"))
                        {
                                HttpSession session = request.getSession ();
                                Usuario usuario = (Usuario)session.getAttribute ("Usuario");
                                String user = usuario.getLogin ();
                                String agencia = usuario.getDstrct ();
                                Calendar fechaHoy = Calendar.getInstance ();
                                java.util.Date fech = fechaHoy.getTime ();
                                SimpleDateFormat fec = new SimpleDateFormat ("yyyy-MM-dd");
                                String Fecha = fec.format (fech);
                                ReporteDXPXSL hilo = new ReporteDXPXSL ();
                                next="/jsp/cxpagar/facturasxpagar/MensajeReporteCumplido.jsp";
                                next += "?msg=Su petici�n a sido procesada"+hilo.getMensaje ();
                                hilo.start (vDocumentos,rango_i,rango_f,agencia, user, Fecha);
                        }
                        //System.out.println("NEXT "+next);
                        this.dispatchRequest (next);
                }
                catch(Exception e)
                {
                        throw new ServletException ("Accion:"+ e.getMessage ());
                }
        }
}
