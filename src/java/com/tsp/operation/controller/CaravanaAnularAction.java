/*
 * CaravanaBusquedaAction.java
 *
 * Created on 04 de Agosto de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;

import org.apache.log4j.*;

public class CaravanaAnularAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/caravana/buscarCaravana.jsp";
        String  pag = request.getParameter("pag")!=null?request.getParameter("pag"):"";
        try{
            if (pag.equals("caravana")){
                int numC = Integer.parseInt(request.getParameter("numC"));
                model.caravanaSVC.anularCaravana(numC);
                model.escoltaVehiculoSVC.getVectorEscoltasXCaravana(request.getParameter("numC"));
                Vector vecEA = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();
                for (int i=0; i<vecEA.size(); i++){
                    String plani = ((Escolta) vecEA.elementAt(i)).getNumpla();
                    String placa = ((Escolta) vecEA.elementAt(i)).getPlaca();
                    model.escoltaVehiculoSVC.liberarEscoltasCaravana(placa,numC);
                    model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plani,"");
                }
            } else {
                logger.info("ANULACION DE ESCOLTA");
                
                String placa = request.getParameter("placa");
                String opcion = request.getParameter("opcion");
                String planilla = request.getParameter("campo");
                String act      = request.getParameter("actividad")!=null ? request.getParameter("actividad"):"";
                String caravana = request.getParameter("caravana");
                int numC = 0;
                if (!caravana.equals("Ninguna"))
                    numC = Integer.parseInt(caravana);
                
                if (act.equals("liberar")) {
                    //Verificando si el escolta esta en una caravana y si es asi libera mas de una planilla en ingreso trafico
                    if (numC!=0) {
                        Vector plEscoltas = model.escoltaVehiculoSVC.getPlanillasEscolta(placa);                        
                        model.escoltaVehiculoSVC.liberarEscoltasCaravana(placa,numC);
                        for (int i=0; i<plEscoltas.size(); i++) {
                            String pl = (String) plEscoltas.elementAt(i);
                            model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(pl,"");
                        }
                    } else {
                        model.escoltaVehiculoSVC.liberarEscoltas(placa,planilla);
                        model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(planilla,"");
                    }
                        
                    next = "/jsp/trafico/caravana/buscarEscolta.jsp?msg=Escolta liberado";
                } else {
                    //Eliminar Escoltas
                    //Verificando si el escolta esta en una caravana y si es asi libera mas de una planilla asociada a el
                    if (numC!=0) {
                        Vector plEscoltas = model.escoltaVehiculoSVC.getPlanillasEscolta(placa);                        
                        model.escoltaVehiculoSVC.eliminarEscoltaCaravana(placa,numC);
                        for (int i=0; i<plEscoltas.size(); i++) {
                            String pl = (String) plEscoltas.elementAt(i);
                            model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(pl,"");
                        }
                    }else {
                        model.escoltaVehiculoSVC.eliminarEscoltas(placa,planilla);
                        model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(planilla,"");
                    }
                    next = "/jsp/trafico/caravana/buscarEscolta.jsp?msg=Escolta Eliminado";
                }
                /*Cargando datos en el vector*/
                if (opcion.equals("planilla") )
                    model.escoltaVehiculoSVC.buscarEscoltaAsignado(planilla,opcion);
                else
                    model.escoltaVehiculoSVC.buscarEscoltaAsignado(placa,opcion);
                /*Llenando nuevamente el vector de la vista*/
                Vector escoltas = model.escoltaVehiculoSVC.getVecEscoltas();
                
            }           
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
