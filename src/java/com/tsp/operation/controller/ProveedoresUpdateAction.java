/*
 * ProveedoresUpdateAction.java
 *
 * Created on 21 de abril de 2005, 02:41 PM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class ProveedoresUpdateAction extends Action{
    
    /** Creates a new instance of ProveedoresUpdateAction */
    public ProveedoresUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next         ="/proveedores/proveedorUpdate.jsp";
        String nit          = request.getParameter("nit");
        String sucursal     = request.getParameter("sucursal").toUpperCase();
        String dist         = request.getParameter("distrito");
        String ciudad       = request.getParameter("ciudad");
        String codmigracion = request.getParameter("codigo_m");
        String moneda       = request.getParameter("moneda");
        float valor         = Float.parseFloat(request.getParameter("valor"));
        float porcentaje    = Float.parseFloat(request.getParameter("porcentaje"));
        
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        
        Proveedores prov = new Proveedores();
        prov.setDstrct(dist);
        prov.setCity_code(ciudad);
        prov.setTotale(valor);
        prov.setNit(nit);
        prov.setSucursal(sucursal);
        prov.setMoneda(moneda);
        prov.setPorcentaje(porcentaje);
        prov.setmigracion(codmigracion);
        prov.setCreation_user(usuario.getLogin());
        try{
            
            model.proveedoresService.searchProveedor(nit, sucursal);
            if(model.proveedoresService.getProveedor()!=null){
                model.proveedoresService.setProveedor(prov);
                model.proveedoresService.updateProveedor();
                model.proveedoresService.vecProveedor(usuario.getBase());
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
    
}
