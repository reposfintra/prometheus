/*
 * CaravanaBusquedaAction.java
 *
 * Created on 04 de Agosto de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

public class CaravanaInsertAction extends Action{
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/caravana/caravana.jsp?agregar=visible&escolta=visible";        
        String planilla = "", escolta = "";
        int numC = Integer.parseInt(request.getParameter("numC"));
        String origen_car = request.getParameter("origen_caravana")!=null ? request.getParameter("origen_caravana"):"";
        String destino_car = request.getParameter("destino_caravana")!=null ? request.getParameter("destino_caravana"):"";        
        
        boolean sw = false;
        try{ 
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
            //creaci�n de la caravana
            Caravana c = new Caravana();
            Vector vecPlani = new Vector();
            c.setNumcaravana(numC) ;
            c.setDstrct(usuario.getDstrct());
            c.setAgencia(usuario.getId_agencia());
            c.setBase(usuario.getBase());
            c.setCreation_user(usuario.getLogin());
            c.setFecinicio(Util.getFechaActual_String(4));
            c.setOrigen (origen_car);
            c.setDestino (destino_car);
            Vector datos = model.caravanaSVC.getVectorCaravanaActual();
            if (datos.size()==1)
                next = "/jsp/trafico/caravana/caravana.jsp?agregar=visible&escolta=visible&msg=V1";
            else {
                for (int i=0; i<datos.size(); i++){
                    VistaCaravana v = (VistaCaravana)datos.elementAt(i);
                    planilla = (request.getParameter("numpla"+i)!=null)?request.getParameter("numpla"+i):"";                                   
                    if (!planilla.equals("")) {
                        c.setNumpla(planilla);
                        c.setNumrem(v.getNumrem());
                        vecPlani.addElement(planilla);
                        model.caravanaSVC.insertarCaravana(c);
                        model.caravanaSVC.actualizarCaravanaIngresoTrafico(planilla,""+numC);
                        sw = true;
                    }
                }
                //Creaci�n del escolta de la caravana
                EscoltaVehiculo e = new EscoltaVehiculo();            
                e.setBase(usuario.getBase());
                e.setCreation_user(usuario.getLogin());
                e.setFecasignacion(Util.getFechaActual_String(4));
                Vector esc = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();
                String origen = "";
                String destino = "";
                for (int i=0; i<esc.size(); i++){
                    escolta = (request.getParameter("placaesc"+i)!=null)?request.getParameter("placaesc"+i):"";                                
                    origen = request.getParameter("origen"+i)!=null ? request.getParameter("origen"+i):"";
                    destino = request.getParameter("destino"+i)!=null ? request.getParameter("destino"+i):"";       
                    if (!escolta.equals("")) {
                        e.setPlaca(escolta);
                        //Asociando un escolta a las planillas de una caravana
                        String pla = "";
                        e.setNumcaravana(numC);                        
                        e.setOrigen(origen);
                        e.setDestino(destino);
                        model.escoltaVehiculoSVC.insertarEscoltaCaravana(e);
                        for (int j=0; j<vecPlani.size(); j++){
                            pla = (String) vecPlani.elementAt(j);                            
                            model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(pla,"S");
                            sw= true;
                        }
                       // model.caravanaSVC.insertarCaravana(c);
                    }
                }
            }
            if (sw==true)
                next = "/jsp/trafico/caravana/caravana.jsp?msg=OKK";
            model.caravanaSVC.limpiarVectorActualPlanillas();
            model.escoltaVehiculoSVC.limpiarVectorEscoltaActual();
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());                       
        }        
        this.dispatchRequest(next);
    }
    
}
