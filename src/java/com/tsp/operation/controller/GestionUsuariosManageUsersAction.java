/*
* Nombre        GestionUsuariosEditUser.java
* Descripci�n   Clase para la administraci�n de usuarios
* Autor         desconociido
* Fecha         30 de marzo de 2004, 11:32 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
import com.tsp.opav.model.DAOS.ApplusDAO;
import com.tsp.operation.model.services.UsuarioService;
import com.tsp.util.Util;

/**
 * Acci�n ejecutada por el controlador para gestionar los usuarios creados y
 * permite crear nuevos, exceptuando a los destinatarios, que son gestionados
 * por los clientes administradores de transporte.
 */
public class GestionUsuariosManageUsersAction extends Action {
    
    /**
     * Metodo invocado por el servlet controlador para ejecutar la acci�n.
     * @throws ServletException si ocurre algun error al llamar a la pagina de respuesta
     * @throws InformationException ni idea
     */    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/sot/admin/GestionUsuarios.jsp";
        try {
            Usuario loggedUser = (Usuario) this.request.getSession().getAttribute("Usuario");
            String cmd = request.getParameter("cmd");
            if( cmd == null ) {
                //model.usuarioService.usuariosCreadosSearch(loggedUser);
                next = "/jsp/sot/admin/GestionUsuarios.jsp?primeraVez=true";
            }
            else if ( cmd.equals("search") ) {
                String busqueda = request.getParameter("campoBusqueda");
                model.usuarioService.buscarUsuarios(loggedUser,busqueda);
                next = "/jsp/sot/admin/GestionUsuarios.jsp";
            }
            else if( cmd.equals("edit") ){
                String idUsuario = request.getParameter("idUser");
                try {
                    model.menuService.buscarPerfiles();
                    Usuario user = model.usuarioService.obtenerUsuario( idUsuario );
                    model.agenciaService.listarOrdenadas();
                    request.setAttribute( "tsp.sot.userData", user );
                    next = "/jsp/sot/admin/EditarUsuario.jsp";
                }catch (Exception e){
                    throw new ServletException(e.getMessage());
                }
            }else if( cmd.equals("save") ){
                try {
                    String idUsuario  = request.getParameter("idUser").toUpperCase().trim();
                    String estadoUsuario = request.getParameter("estadoUsuario");
                    if( estadoUsuario.equals("R") )
                        model.usuarioService.eliminarUsuario( idUsuario );
                    else{
                        String [] data = new String[22];
                        data[0]  = this.application.getInitParameter("deskey");
                        data[1]  = request.getParameter("nombreUsuario");
                        data[2]  = request.getParameter("nitUsuario");
                        data[3]  = request.getParameter("direccionUsuario");
                        data[4]  = request.getParameter("telefonoUsuario");
                        data[5]  = request.getParameter("emailUsuario");
                        data[6]  = request.getParameter("paisUsuario");
                        data[7]  = request.getParameter("ciudadUsuario");
                        data[8]  = request.getParameter("tipoUsuario");
                        data[9]  = estadoUsuario;
                        data[10] = idUsuario.toUpperCase().trim();
                        data[11] = request.getParameter("claveUsuario");
                        data[12] = request.getParameter("claveCambio");
                        //                    String cliDest = request.getParameter("clienteDestinat");
                        String cliDest = "";
                        if(data[8].equals("CLIENTETSP") || data[8].equals("CLIENTEADM") ){
                            String[] clien = request.getParameterValues("clienteDestinat");
                            cliDest             =  arrayToString(clien);
                        } else
                            cliDest = request.getParameter("clienteDestinat");
                        
                        data[13] = (cliDest == null ? "" : cliDest);
                        data[14] = request.getParameter("agencia");
                        data[15] = request.getParameter("accesoPlanViaje");
                        //AMENDEZ 20050705
                        String cia = Arrays.deepToString(request.getParameterValues("cia"));
                        cia = cia.replace("[", ""); cia = cia.replace("]", ""); cia = cia.replace(" ", "");
                        data[16] = cia;
                        data[17] = request.getParameter("updatePass");
                        data[18] = request.getParameter("dpto");
                        data[19] = request.getParameter("base");
                        data[20] = loggedUser.getLogin();
                        
                        //JEscandon 
                        data[21] = request.getParameter("reanticipos");//Reanticipos
                        
                        String [] perfiles = request.getParameterValues("perfil");
                        model.usuarioService.actualizarUsuario( data, perfiles );
                    }
                    next = "/jsp/sot/admin/GestionUsuarios.jsp";
                    model.usuarioService.usuariosCreadosSearch(loggedUser);
                    if (cmd.equals("generar_claves_clientes_preaprobados")) {

                        Vector batch = new Vector();
                        ApplusDAO apdao = new ApplusDAO();
                        UsuarioService us = new UsuarioService();
                        ArrayList<Usuario> lista = us.buscarUsuariosPreaprobados();
                        for (int i = 0; i < lista.size(); i++) {
                            String claveEncr = Util.encript("d5C9A2bf", lista.get(i).getPassword());
                            String sql = "update usuarios set claveencr= " + "'" + claveEncr + "'" + " where idusuario=" + lista.get(i).getIdusuario();
                            batch.add(sql);
                        }
                        apdao.ejecutarSQL(batch);
                        next = "/jsp/sot/admin/generar_claves.jsp?opcion=2&mensaje=Proceso realizado con exito";
                    }
                }
                catch (SQLException e){
                    e.printStackTrace();
                    throw new ServletException("Error de base de datos: " + e.getMessage());
                }catch (EncriptionException e){
                    throw new ServletException("Error de encriptacion: " + e.getMessage());
                }
                
            }
        }
        catch(RuntimeException e){
            e.printStackTrace();
            next = "/jsp/sot/body/prohibido.jsp?mensaje="+e.getMessage();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        // Redirigir hacia la p�gina de gesti�n de usuarios.
        this.dispatchRequest(next);
    }
    
    
    /**
     * convierte un array a String con un formato desconocido
     * @param arreglo el arreglo que sera convertido a string
     * @return un string con los datos que estan en el arreglo
     */    
    public String arrayToString(String [] arreglo){
        String lista = "";
        for (int i=0;arreglo!=null && i<arreglo.length;lista+=arreglo[i]+"#",i++);
        return (lista.equals("")?"":(lista.substring(0, lista.length()-1)));
    }
    
}
