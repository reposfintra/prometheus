/*
 * Nombre        AnularCiudadAction.java
 * Autor         Ing. Diogenes Bastidas
 * Fecha         1 de abril de 2005, 02:58 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIBASMO
 */
public class AnularCiudadAction extends Action {
    
    /** Creates a new instance of AnularCiudadAction */
    public AnularCiudadAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        String codigo = (request.getParameter("codigo").toUpperCase());
        String pais = request.getParameter("pais");
        String estado = request.getParameter("dpto");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String lenguaje = (String) session.getAttribute("idioma");
     
        try{
//            model.idiomaService.cargarIdioma(lenguaje, "MsgAnulado.jsp");
            Ciudad ciudad = new Ciudad(); 
            ciudad.setcodciu(codigo);
            ciudad.setdepartament_code(estado);
            ciudad.setpais_code(pais);
            ciudad.setUser_update(usuario.getLogin());
            model.ciudadservice.anularCiudad(ciudad); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);

    }
    
}
