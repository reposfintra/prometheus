/***********************************************
 * Nombre clase: Discrepancia_generalInsertAction.java
 * Descripci�n: Accion para ingresar una discrepancia general a la bd.
 * Autor: Jose de la rosa
 * Fecha: 18 de noviembre de 2005, 04:51 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class Discrepancia_generalInsertAction extends Action{
    
    /** Creates a new instance of Discrepancia_generalInsertAction */
    public Discrepancia_generalInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String nro_rechazo = (request.getParameter ("c_nro_rechazo")!=null)?request.getParameter ("c_nro_rechazo"):"";
        String retencion = request.getParameter ("c_retencion");
        String observacion = (request.getParameter ("c_observacion")!=null)?request.getParameter ("c_observacion"):"";
        String unidad = request.getParameter ("c_unidad");
        double cantidad = Double.parseDouble (request.getParameter ("c_cantidad"));
        String cod_discrepancia = request.getParameter ("c_cod_discrepancia");
        double valor = 0;
        if( request.getParameter ("c_valor")!=null && !request.getParameter ("c_valor").equals ("") ){
            valor = Double.parseDouble (request.getParameter ("c_valor"));
        }
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String next = "";
        int sw=0;
        try {
            Vector consultas = new Vector ();
            Discrepancia dis = new Discrepancia ();
            dis.setNro_planilla (numpla);
            dis.setNro_remesa ("");
            dis.setTipo_doc_rel ("");
            dis.setDocumento_rel ("");
            dis.setTipo_doc ("");
            dis.setDocumento ("");
            dis.setUbicacion ("");
            dis.setContacto ("");
            dis.setNumero_rechazo ("");
            dis.setReportado ("");
            dis.setFecha_devolucion ("0099-01-01 00:00:00");
            dis.setRetencion (retencion);
            dis.setObservacion (observacion);
            dis.setUsuario_modificacion (usuario.getLogin ());
            dis.setUsuario_creacion (usuario.getLogin ());
            dis.setBase (usuario.getBase ());
            dis.setDistrito (distrito);
            model.discrepanciaService.setDiscrepancia (dis);
            request.setAttribute ("msg", "Discrepancia Agregada");
            consultas.add ( model.discrepanciaService.insertarDiscrepancia (dis) );
            model.discrepanciaService.searchDiscrepancia (numpla, "", "", "", "", "",distrito);
            dis.setNro_discrepancia ( model.discrepanciaService.numeroDiscrepancia() );
            dis.setCod_producto ("");
            dis.setUnidad (unidad);
            dis.setValor (valor);
            dis.setDescripcion ("");
            dis.setCantidad (cantidad);
            dis.setCausa_dev ("");
            dis.setGrupo("");
            dis.setResponsable ("");
            dis.setUsuario_modificacion (usuario.getLogin ());
            dis.setUsuario_creacion (usuario.getLogin ());
            dis.setBase (usuario.getBase ());
            dis.setDistrito (distrito);
            model.discrepanciaService.setDiscrepancia (dis);
            dis.setCod_discrepancia (cod_discrepancia);
            consultas.add ( model.discrepanciaService.insertarDiscrepanciaProducto (dis) );
            model.despachoService.insertar (consultas);
            next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertarGeneral.jsp?c_numpla"+numpla;
            model.discrepanciaService.listDiscrepanciaGeneral (numpla,distrito);
            model.planillaService.obtenerInformacionPlanilla2 (numpla);
            model.planillaService.obtenerNombrePropietario2 (numpla);
            request.setAttribute ("msg","Discrepancia Ingresada");
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
    
}
