/*
 * ProveedorAcpmConsultarAction.java
 *
 * Created on 12 de enero de 2005, 08:33 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ConsultarProveedorAcpmAction extends Action{
    
    /** Creates a new instance of ProveedorAcpmConsultarAction */
    public ConsultarProveedorAcpmAction() {
        
        
    }
    
    public void run() throws ServletException, InformationException{
        String nit = request.getParameter("nit");
        String ciudad = request.getParameter("ciudad").toUpperCase();
        String nombre = request.getParameter("nombre").toUpperCase();
        String tipo_servicio = request.getParameter("tipo_servicio");
        String next="/consultas/consultasRedirect.jsp?acpm=ok";
        try{
            if(request.getParameter("codigo")==null){
                model.proveedoracpmService.consultaProveedor(ciudad, nombre, nit, tipo_servicio);
            }
            else{
                String codigo = request.getParameter("codigo");
                model.proveedoracpmService.buscaProveedor(nit, codigo);
                next="/consultas/datosProveedorAcpm.jsp";
                
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
