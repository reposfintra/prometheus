/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.FiltroLibranzaDAO;
import com.tsp.operation.model.DAOS.LibranzaNegociosDAO;
import com.tsp.operation.model.DAOS.impl.FiltroLibranzaImpl;
import com.tsp.operation.model.DAOS.impl.LibranzaNegociosImpl;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Codigo;
import com.tsp.operation.model.beans.DocumentosNegAceptado;
import com.tsp.operation.model.beans.SolicitudAval;
import com.tsp.operation.model.beans.SolicitudBienes;
import com.tsp.operation.model.beans.SolicitudFinanzas;
import com.tsp.operation.model.beans.SolicitudLaboral;
import com.tsp.operation.model.beans.SolicitudPersona;
import com.tsp.operation.model.beans.SolicitudReferencias;
import com.tsp.operation.model.beans.SolicitudTransacciones;
import com.tsp.operation.model.beans.SolicitudVehiculo;
import com.tsp.operation.model.beans.SolicitudOblComprar;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.operation.model.services.WSHistCreditoService;

import java.util.ArrayList;
import java.util.logging.Level;
import javax.servlet.ServletException;

/**
 * @author hcuello
 */
public class FormularioLibranzaAction extends Action {

    private final int BUSCAR_PERSONA = 0;
    private final int BUSCAR_FILTROS = 1;
    private final int BUSCAR_CIUDADES = 2;
    private final int BUSCAR_OCUPACIONES = 3;
    private final int GUARDAR_FORMULARIO = 4;
    private final int NEGOCIOS_POR_LEGALIZAR = 5;
    private final int RECHAZAR_SOLICITUD = 6;
    
    private final int GENERAR_LIQUIDACION = 7;
    private final int GUARDAR_LIQUIDACION = 8;
    private final int BUSCAR_LIQUIDACION = 9;
    
    private final int BUSCAR_FECHA_PAGO = 10;
    private final int DETALLE_ENT_RECOGER = 11;
    
    private final int VALIDAR_OBLIGACIONES = 12;
    private final int CALCULAR_FECHA_PAGO = 13;
    private final int BUSCAR_OCUPACIONES_2 = 14;

    private JsonObject respuesta = null;
    private Usuario usuario;
    private boolean responceTrue=true;

    private GestionSolicitudAvalService gsaserv = null;    
    private LibranzaNegociosDAO libdao = null;

    public void init(Usuario usuario) {
        this.usuario = usuario;
        gsaserv = new GestionSolicitudAvalService(usuario.getBd());
        libdao = new LibranzaNegociosImpl(usuario.getBd());
    }
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            usuario = (Usuario) request.getSession().getAttribute("Usuario");
            gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            gsaserv.setDstrct(usuario.getDstrct() != null ? usuario.getDstrct() : "FINV");
            gsaserv.setLoginuser(usuario.getLogin() != null ? usuario.getLogin() : "ADMIN");

            libdao = new LibranzaNegociosImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case BUSCAR_PERSONA:
                    String nit = request.getParameter("dato") != null ? request.getParameter("dato") : "";
                    buscarPersonaId(nit);
                    break;
                case BUSCAR_FILTROS:
                    buscarFiltros(usuario);
                    break;
                case BUSCAR_CIUDADES:
                    String codpt = request.getParameter("dept") != null ? request.getParameter("dept") : "";
                    buscarCiudades(codpt);
                    break;
                case BUSCAR_OCUPACIONES:
                    buscarOcupaciones();
                    break;
                case BUSCAR_OCUPACIONES_2:
                    String codact = request.getParameter("act") != null ? request.getParameter("act") : "";
                    buscarOcupaciones(codact);
                    break;
                case GUARDAR_FORMULARIO:
                    guardarFormulario();
                    break;
                case NEGOCIOS_POR_LEGALIZAR:
                    negociosPorLegalizar(usuario.getLogin());
                    break;
                case RECHAZAR_SOLICITUD:
                    rechazarSolicitud();
                    break;
                case GENERAR_LIQUIDACION:
                    generarLiquidacion();
                    break;
                case GUARDAR_LIQUIDACION:
                    guardarLiquidacion(usuario.getLogin());
                    break;
                case BUSCAR_LIQUIDACION:
                    String cod_negocio = request.getParameter("codigo_negocio") != null ? request.getParameter("codigo_negocio") : "";
                    buscarLiquidacion(cod_negocio);
                    break;
                case BUSCAR_FECHA_PAGO:
                    String fnegocio = request.getParameter("fnegocio") != null ? request.getParameter("fnegocio") : "";
                    String plazo = request.getParameter("plazo") != null ? request.getParameter("plazo") : "0";
                    buscarFechaPago(fnegocio,plazo);
                    break;
                case DETALLE_ENT_RECOGER:
                    String entidad = request.getParameter("entidad") != null ? request.getParameter("entidad") : "";
                    buscarDetalleEntidades(entidad);
                    break;
                case VALIDAR_OBLIGACIONES:
                    String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
                    String ocupaciones = request.getParameter("ocup") != null ? request.getParameter("ocup") : "";
                    double vlr_sol = request.getParameter("vlr_sol") != null ? Double.parseDouble(request.getParameter("vlr_sol")) : 0;
                    double oblig = request.getParameter("oblig") != "" ? Double.parseDouble(request.getParameter("oblig")) : 0;
                    int Cantoblig = request.getParameter("Cantoblig") != "" ? Integer.parseInt(request.getParameter("Cantoblig")) : 0;
                    String cuotas = request.getParameter("plazo") != null ? request.getParameter("plazo") : "0";
                    String fianza = request.getParameter("fianza") != null ? request.getParameter("fianza") : "";
                    validarObligaciones(tipo, ocupaciones, vlr_sol, oblig, Cantoblig, cuotas, fianza);
                    break;
                case CALCULAR_FECHA_PAGO:
                    String FechaNeg = request.getParameter("FechaNeg") != null ? request.getParameter("FechaNeg") : "";
                    String Pagaduria = request.getParameter("Pagaduria") != null ? request.getParameter("Pagaduria") : "0";
                    calcularFechaPago(FechaNeg,Pagaduria);
                    break;                    
                default:
                    throw new Exception("La opcion de orden " + opcion + " no valida");
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if(responceTrue){
                    response.setContentType("application/json; charset=utf-8");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println((new Gson()).toJson(respuesta));
                }
            } catch (Exception e) {
            }
        }
    }

    public String generarNumeroSolicitud() {
        try {
            return gsaserv.numeroSolc();
        } catch (Exception e) {
            return "0000";
        }
    }

    public JsonObject negociosPorLegalizar(String usuario) {
        
        try {
            
            int enTramites = 0;
            
            ArrayList<BeanGeneral> negs = gsaserv.negociosPorLegalizar(usuario);
            BeanGeneral bean;
            
            respuesta = new JsonObject();
            JsonObject obj;
            respuesta.add("solicitudes", new JsonArray());
            
            for (BeanGeneral neg : negs) {
                bean = neg;
                obj = new JsonObject();
                obj.addProperty("estado_sol", bean.getValor_01());
                obj.addProperty("fecha_consulta", bean.getValor_02());
                obj.addProperty("fecha_creacion", bean.getValor_03());
                obj.addProperty("valor_solicitado", bean.getValor_04());
                obj.addProperty("cod_neg", bean.getValor_05());
                obj.addProperty("estado_neg", bean.getValor_06());
                obj.addProperty("numero_solicitud", bean.getValor_07());
                obj.addProperty("solicitante", bean.getValor_08());
                obj.addProperty("plazo", bean.getValor_09());
                respuesta.getAsJsonArray("solicitudes").add(obj);
                if(!bean.getValor_05().equalsIgnoreCase("")) 
                    enTramites ++;
            }
            respuesta.addProperty("mensaje", "Ud. tiene " + enTramites + " solicitud(es) en tramites.");
            respuesta.addProperty("en_tramite", enTramites);
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "Error obteniendo los negocios registrados por Ud.");
            e.printStackTrace();
        } finally {
            return respuesta;
        }
    }

    public void rechazarSolicitud() {
        try {
            String sol = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "";
            if (!sol.equalsIgnoreCase("")) {
                gsaserv.rechazarSolicitud(sol);
            }
        } catch(Exception e) {      
        }
    }
    
    public SolicitudAval buscarSolicitud(String num_solicitud) {
        try {
            return gsaserv.buscarSolicitud(Integer.parseInt(num_solicitud));
        } catch (Exception e) {
            return null;
        }
    }

    public JsonArray buscarCiudades(String codpt) {
        respuesta = new JsonObject();
        try {
            this.addElement("ciudades", gsaserv.listadoCiudades(codpt));
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "No se encontraron ciudades");
            e.printStackTrace();
        } finally {
            return respuesta.get("ciudades").getAsJsonArray();
        }
    }

    public JsonObject buscarOcupaciones() {
        respuesta = new JsonObject();
        try {
            FiltroLibranzaDAO fDao = new FiltroLibranzaImpl(usuario.getBd());
            respuesta = fDao.getOcupLaboral();
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "No se encontraron ocupaciones");
            e.printStackTrace();
        } finally {
            return respuesta;
        }
    }
    
    public JsonArray buscarOcupaciones(String act) {
        respuesta = new JsonObject();
        try {
            this.addElement("ocupaciones", gsaserv.listadoOcupaciones(act));
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "No se encontraron ocupaciones");
            e.printStackTrace();
        } finally {
            return respuesta.get("ocupaciones").getAsJsonArray();
        }
    }
    
    public JsonArray buscarEntidadesComprar(String act) {
        respuesta = new JsonObject();
        try {
            this.addElement("obligaciones_recoger", gsaserv.listadoEntidadesComprar(act));
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "No se encontraron Entidades");
            e.printStackTrace();
        } finally {
            return respuesta.get("obligaciones_recoger").getAsJsonArray();
        }
    }
    
    public JsonArray buscarDetalleEntidades(String entidad) {
        respuesta = new JsonObject();
        try {
            this.addElementMulti("entidades_recoger_info", gsaserv.listadoDetalleEntidad(entidad));
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "No se encontraron Detalle Entidad");
            e.printStackTrace();
        } finally {
            return respuesta.get("entidades_recoger_info").getAsJsonArray();
        }
    }    

    public JsonObject buscarFiltros(Usuario usuario) {
        
        respuesta = new JsonObject();
        JsonArray objR = new JsonArray();
        JsonObject obj = new JsonObject();
        ArrayList<Codigo> lista = null;
        Codigo dato = null;
        
        try {
            
            this.addElement("pagadurias", gsaserv.busquedaPagadurias(usuario.getLogin()));

            this.addElement("departamentos", gsaserv.listadoDeps());
            this.addElement("estado_civil", gsaserv.busquedaGeneral("ESTCIV"));
            this.addElement("nivel_estudio", gsaserv.busquedaGeneral("NIVEST"));
            this.addElement("actividad_economica", gsaserv.busquedaGeneral("ACTECO"));
            this.addElement("tipo_contrato", gsaserv.busquedaGeneral("TIPCON"));
            this.addElement("caracter_empresa", gsaserv.busquedaGeneral("CAREMP"));
            this.addElement("parentesco", gsaserv.busquedaGeneral("PARENT"));
            this.addElement("eps", gsaserv.busquedaGeneral("EPS"));
            this.addElement("tipo_afiliacion", gsaserv.busquedaGeneral("TIPAFIL"));
            this.addElement("tipo_id", gsaserv.busquedaGeneral("TIPID"));

            //this.addElement("plazos_cuotas", (new GestionCondicionesService()).plazoCuota());
            WSHistCreditoService wsdatacred = new WSHistCreditoService(usuario.getBd());
            lista = wsdatacred.buscarTabla("H", "vivienda");
            obj.addProperty("codigo", "");
            obj.addProperty("valor", "...");
            objR.add(obj);
            
            for (int i = 0; i < lista.size(); i++) {
                
                obj = new JsonObject();
                dato = (Codigo) lista.get(i);
                obj.addProperty("codigo", dato.getCodigo());
                obj.addProperty("valor", dato.getValor());
                objR.add(obj);
            }
            
            respuesta.add("tipo_vivienda", objR);

            objR = new JsonArray();
            obj = new JsonObject();
            obj.addProperty("codigo", "");
            obj.addProperty("valor", "...");
            objR.add(obj);
            
            for (int i = 0; i < 7; i++) {
                obj = new JsonObject();
                obj.addProperty("codigo", i);
                obj.addProperty("valor", i);
                objR.add(obj);
            }
            respuesta.add("estrato", objR);
            
            this.addElement("tipo_cuota", gsaserv.busquedaGeneral("TIPO_CUOTA"));
            
        } catch (Exception e) {
            //respuesta = new JsonObject();
            respuesta.addProperty("error", "Error cargando valores iniciales del formulario");
        } finally {
            return respuesta;
        }
    }

    private void addElement(String titulo, ArrayList lista) {
        JsonObject aux = new JsonObject();
        JsonArray auxR = new JsonArray();
        String[] dato1 = null;
        try {
            aux.addProperty("codigo", "");
            aux.addProperty("valor", "...");
            auxR.add(aux);
            for (int i = 0; i < lista.size(); i++) {
                dato1 = ((String) lista.get(i)).split(";_;");
                aux = new JsonObject();
                aux.addProperty("codigo", dato1[0]);
                aux.addProperty("valor", dato1[1]);
                auxR.add(aux);
            }
            respuesta.add(titulo, auxR);
        } catch (Exception e) {
            aux = new JsonObject();
            aux.addProperty("codigo", "");
            aux.addProperty("valor", "...");
            respuesta.add(titulo, aux);
        }
    }
    
    private void addElementMulti(String titulo, ArrayList lista) {
        JsonObject aux = new JsonObject();
        JsonArray auxR = new JsonArray();
        String[] dato1 = null;
        try {
            
            for (int i = 0; i < lista.size(); i++) {
                dato1 = ((String) lista.get(i)).split(";_;");
                aux = new JsonObject();
                aux.addProperty("nit", dato1[0]);
                aux.addProperty("tipo_cuenta", dato1[1]);
                aux.addProperty("no_cuenta", dato1[2]);
                auxR.add(aux);
            }
            respuesta.add(titulo, auxR);
            
        } catch (Exception e) {
            aux = new JsonObject();
            aux.addProperty("nit", "");
            aux.addProperty("tipo_cuenta", "");
            aux.addProperty("no_cuenta", "");
            respuesta.add(titulo, aux);
        }
    }    

    public JsonObject buscarPersonas(String num_solicitud) {
        respuesta = new JsonObject();
        String titulo = "";
        int p = 0, l = 0, cod = 1;

        ArrayList<SolicitudPersona> personas = null;
        SolicitudPersona persona = null;
        ArrayList<SolicitudLaboral> labores = null;
        SolicitudLaboral laboral = null;
        ArrayList<SolicitudReferencias> referencias = null, refs = null;
        SolicitudReferencias referencia = null;
        ArrayList<SolicitudFinanzas> finanzas = null;
        SolicitudFinanzas finanza = null;
        ArrayList<SolicitudTransacciones> transacciones = null;
        SolicitudTransacciones transaccion = null;
        try {
            personas = gsaserv.buscarPersonas(Integer.parseInt(num_solicitud));
            labores = gsaserv.buscarLabores(Integer.parseInt(num_solicitud));
            referencias = gsaserv.buscarReferencias(Integer.parseInt(num_solicitud));
            finanzas = gsaserv.buscarFinanzas(Integer.parseInt(num_solicitud));
            transacciones = gsaserv.buscarTransacciones(Integer.parseInt(num_solicitud));

            for (p = 0; p < personas.size(); p++) {
                persona = personas.get(p);
                for (l = 0; l < labores.size(); l++) {
                    laboral = labores.get(l);
                    if (laboral.getId_persona().equalsIgnoreCase(persona.getIdentificacion())) {
                        break;
                    } else {
                        laboral = null;
                    }
                }
                for (l = 0; l < finanzas.size(); l++) {
                    finanza = finanzas.get(l);
                    if (finanza.getId_persona().equalsIgnoreCase(persona.getIdentificacion())) {
                        break;
                    } else {
                        finanza = null;
                    }
                }
                for (l = 0; l < transacciones.size(); l++) {
                    transaccion = transacciones.get(l);
                    if (transaccion.getId_persona().equalsIgnoreCase(persona.getIdentificacion())) {
                        break;
                    } else {
                        transaccion = null;
                    }
                }
                refs = new ArrayList<>();
                for (l = 0; l < referencias.size(); l++) {
                    referencia = referencias.get(l);
                    if (referencia.getReferenciado().equalsIgnoreCase(persona.getIdentificacion())) {
                        refs.add(referencia);
                    }
                }
                if (persona.getTipo().equalsIgnoreCase("S")) {
                    titulo = "solicitante";
                } else if (persona.getTipo().equalsIgnoreCase("C")) {
                    titulo = "codeudor_" + cod;
                    cod++;
                }
                respuesta.add(titulo, buscar(persona, laboral, finanza, transaccion, refs));
            }

        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        } finally {
            return respuesta;
        }
    }

    public JsonObject buscarPersonaId(String nit) {
        SolicitudPersona persona = null;
        ArrayList<SolicitudLaboral> labores = null;
        SolicitudLaboral laboral = null;
        ArrayList<SolicitudFinanzas> finanzas = null;
        SolicitudFinanzas finanza = null;
        ArrayList<SolicitudTransacciones> transacciones = null;
        SolicitudTransacciones transaccion = null;
        int l;
        try {
            persona = gsaserv.buscarPersonaId(nit);
            if (persona != null) {
                if (!gsaserv.ValidarSolicitudCliente(nit)) {
                    throw new Exception("El cliente tiene solicitudes en tramite");
                } else {
                    labores = gsaserv.buscarLabores(Integer.parseInt(persona.getNumeroSolicitud()));
                    finanzas = gsaserv.buscarFinanzas(Integer.parseInt(persona.getNumeroSolicitud()));
                    transacciones = gsaserv.buscarTransacciones(Integer.parseInt(persona.getNumeroSolicitud()));
                    for (l = 0; l < labores.size(); l++) {
                        laboral = labores.get(l);
                        if (laboral.getId_persona().equalsIgnoreCase(persona.getIdentificacion())) {
                            break;
                        } else {
                            laboral = null;
                        }
                    }
                    for (l = 0; l < finanzas.size(); l++) {
                        finanza = finanzas.get(l);
                        if (finanza.getId_persona().equalsIgnoreCase(persona.getIdentificacion())) {
                            break;
                        } else {
                            finanza = null;
                        }
                    }
                    for (l = 0; l < transacciones.size(); l++) {
                        transaccion = transacciones.get(l);
                        if (transaccion.getId_persona().equalsIgnoreCase(persona.getIdentificacion())) {
                            break;
                        } else {
                            transaccion = null;
                        }
                    }
                    respuesta = buscar(persona, laboral, finanza, transaccion, null);
                }
            } else {
                throw new Exception("La informacion digitada no se encuentra registrada");
            }
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        } finally {
            return respuesta;
        }
    }

    private JsonObject buscar(SolicitudPersona persona, SolicitudLaboral laboral, SolicitudFinanzas finanza, SolicitudTransacciones transacciones, ArrayList<SolicitudReferencias> referencias) {
        JsonObject pers = new JsonObject();
        JsonObject objeto = new JsonObject();

        try {
            //INFORMACION PERSONA
            objeto.addProperty("primer_apellido", (persona != null && persona.getPrimerApellido() != null) ? persona.getPrimerApellido() : "");
            objeto.addProperty("segundo_apellido", (persona != null && persona.getSegundoApellido() != null) ? persona.getSegundoApellido() : "");
            objeto.addProperty("primer_nombre", (persona != null && persona.getPrimerNombre() != null) ? persona.getPrimerNombre() : "");
            objeto.addProperty("segundo_nombre", (persona != null && persona.getSegundoNombre() != null) ? persona.getSegundoNombre() : "");
            objeto.addProperty("nombre", (persona != null && persona.getNombre() != null) ? persona.getNombre() : "");
            objeto.addProperty("genero", (persona != null && persona.getGenero() != null) ? persona.getGenero() : "");
            objeto.addProperty("tipo_id", (persona != null && persona.getTipoId() != null) ? persona.getTipoId() : "");
            objeto.addProperty("identificacion", (persona != null && persona.getIdentificacion() != null) ? persona.getIdentificacion() : "");
            objeto.addProperty("direccion", (persona != null) ? persona.getDireccion() : "");
            objeto.addProperty("telefono", (persona != null) ? persona.getTelefono() : "");
            objeto.addProperty("telefono_2", (persona != null) ? persona.getTelefono2() : "");
            objeto.addProperty("celular", (persona != null) ? persona.getCelular() : "");
            objeto.addProperty("fax", (persona != null) ? persona.getFax() : "");
            objeto.addProperty("email", (persona != null) ? persona.getEmail() : "");
            objeto.addProperty("fecha_expedicion_id", (persona != null) ? persona.getFechaExpedicionId().substring(0, 10) : "");
            objeto.addProperty("ciudad_expedicion_id", (persona != null) ? persona.getCiudadExpedicionId() : "");
            objeto.addProperty("depto_expedicion_id", (persona != null) ? persona.getDptoExpedicionId() : "");
            objeto.addProperty("fecha_nacimiento", (persona != null) ? persona.getFechaNacimiento().substring(0, 10) : "");
            objeto.addProperty("ciudad_nacimiento", (persona != null) ? persona.getCiudadNacimiento() : "");
            objeto.addProperty("depto_nacimiento", (persona != null) ? persona.getDptoNacimiento() : "");
            objeto.addProperty("estado_civil", (persona != null) ? persona.getEstadoCivil() : "");
            objeto.addProperty("nivel_estudio", (persona != null) ? persona.getNivelEstudio() : "");
            objeto.addProperty("profesion", (persona != null) ? persona.getProfesion() : "");
            objeto.addProperty("personas_a_cargo", (persona != null) ? persona.getPersonasACargo() : "");
            objeto.addProperty("num_hijos", (persona != null) ? persona.getNumHijos() : "");
            objeto.addProperty("grupo_familia", (persona != null) ? persona.getTotalGrupoFamiliar() : "");
            objeto.addProperty("ciudad", (persona != null) ? persona.getCiudad() : "");
            objeto.addProperty("departamento", (persona != null) ? persona.getDepartamento() : "");
            objeto.addProperty("barrio", (persona != null) ? persona.getBarrio() : "");
            objeto.addProperty("estrato", (persona != null) ? persona.getEstrato() : "");
            objeto.addProperty("tipo_vivienda", (persona != null) ? persona.getTipoVivienda() : "");
            objeto.addProperty("fecha_constitucion", (persona != null) ? persona.getFechaConstitucion().substring(0, 10) : "");
            objeto.addProperty("nombre_representante", (persona != null) ? persona.getRepresentanteLegal() : "");
            objeto.addProperty("genero_representante", (persona != null) ? persona.getGeneroRepresentante() : "");
            objeto.addProperty("tipo_id_representante", (persona != null) ? persona.getTipoIdRepresentante() : "");
            objeto.addProperty("id_representante", (persona != null) ? persona.getIdRepresentante() : "");
            objeto.addProperty("nombre_firmador", (persona != null) ? persona.getFirmadorCheques() : "");
            objeto.addProperty("genero_firmador", (persona != null) ? persona.getTipoIdFirmador() : "");
            objeto.addProperty("tipo_id_firmador", (persona != null) ? persona.getIdFirmador() : "");
            objeto.addProperty("ciiu", (persona != null) ? persona.getCiiu() : "");
            objeto.addProperty("tipo_empresa", (persona != null) ? persona.getTipoEmpresa() : "");
            String[] tr = new String[]{"0", "0"};
            if (persona != null && !persona.getTiempoResidencia().equals("")) {
                tr = persona.getTiempoResidencia().split("A�os");
                if (tr.length > 1) {
                    tr[1] = tr[1].replaceAll("Meses", "").trim();
                    tr[0] = tr[0].trim();
                } else {
                    tr = new String[]{tr[0].trim(), "0"};
                }
            }
            objeto.addProperty("anos_residencia", (tr[0] != null) ? tr[0] : "");
            objeto.addProperty("meses_residencia", (tr[1] != null) ? tr[1] : "");
              
            objeto.addProperty("primer_apellido_cony", (persona != null && persona.getPrimerApellidoCony()!= null) ? persona.getPrimerApellidoCony() : "");
            objeto.addProperty("segundo_apellido_cony", (persona != null && persona.getSegundoApellidoCony() != null) ? persona.getSegundoApellidoCony() : "");
            objeto.addProperty("primer_nombre_cony", (persona != null && persona.getPrimerNombreCony() != null) ? persona.getPrimerNombreCony() : "");
            objeto.addProperty("segundo_nombre_cony", (persona != null && persona.getSegundoNombreCony() != null) ? persona.getSegundoNombreCony() : "");
            objeto.addProperty("tipo_id_cony", (persona != null && persona.getTipoIdentificacionCony()!= null) ? persona.getTipoIdentificacionCony(): "");
            objeto.addProperty("identificacion_cony", (persona != null && persona.getIdentificacionCony()!= null) ? persona.getIdentificacionCony(): "");
            objeto.addProperty("telefono_cony", (persona != null) ? persona.getTelefonoCony(): "");
            objeto.addProperty("celular_cony", (persona != null) ? persona.getCelularCony(): "");
            
            pers.add("basica", objeto);

            //INFOMACION LABORAL Y ECONOMICA
            objeto = new JsonObject();
            objeto.addProperty("actividad_economica", (laboral != null) ? laboral.getActividadEconomica() : "");
            objeto.addProperty("ocupacion", (laboral != null) ? laboral.getOcupacion() : "");
            objeto.addProperty("nombre_empresa", (laboral != null) ? laboral.getNombreEmpresa() : "");
            tr = new String[]{"", ""};
            if (laboral != null && !laboral.getNit().equals("")) {
                tr = laboral.getNit().split("-");
                if (!(tr.length > 1)) {
                    try {
                        tr = new String[]{tr[0].trim(), ""};
                    } catch(Exception e) {
                        tr = new String[]{"",""};
                    }
                }
            }
            objeto.addProperty("nit", tr[0]);
            objeto.addProperty("digito_verificacion", tr[1]);
            objeto.addProperty("direccion", (laboral != null) ? laboral.getDireccion() : "");
            objeto.addProperty("direccion_cobro", (laboral != null) ? laboral.getDireccionCobro() : "");
            objeto.addProperty("departamento", (laboral != null) ? laboral.getDepartamento() : "");
            objeto.addProperty("ciudad", (laboral != null) ? laboral.getCiudad() : "");
            objeto.addProperty("telefono", (laboral != null) ? laboral.getTelefono() : "");
            objeto.addProperty("extension", (laboral != null) ? laboral.getExtension() : "");
            objeto.addProperty("telefono_2", (laboral != null) ? laboral.getTelefono2() : "");
            objeto.addProperty("celular", (laboral != null) ? laboral.getCelular() : "");
            objeto.addProperty("email", (laboral != null) ? laboral.getEmail() : "");
            objeto.addProperty("cargo", (laboral != null) ? laboral.getCargo() : "");
            objeto.addProperty("fecha_ingreso", (laboral != null) ? laboral.getFechaIngreso().substring(0, 10) : "");
            objeto.addProperty("tipo_contrato", (laboral != null) ? laboral.getTipoContrato() : "");
            objeto.addProperty("salario", (laboral != null) ? laboral.getSalario() : "0");
            objeto.addProperty("otros_ingresos", (laboral != null) ? laboral.getOtrosIngresos() : "0");
            objeto.addProperty("conceptos_otros_ingresos", (laboral != null) ? laboral.getConceptoOtrosIng() : "");
            objeto.addProperty("gastos_manutencion", (laboral != null) ? laboral.getGastosManutencion() : "0");
            objeto.addProperty("gastos_creditos", (laboral != null) ? laboral.getGastosCreditos() : "0");
            objeto.addProperty("gastos_arriendo", (laboral != null) ? laboral.getGastosArriendo() : "0");
            objeto.addProperty("eps", (laboral != null) ? laboral.getEps() : "");
            objeto.addProperty("tipo_afiliacion", (laboral != null) ? laboral.getTipoAfiliacion() : "");
            pers.add("laboral", objeto);

            //INFORMACION FINANCIERA
            objeto = new JsonObject();
            objeto.addProperty("salario", (finanza != null) ? finanza.getSalario() : "0");
            objeto.addProperty("honorarios", (finanza != null) ? finanza.getHonorarios() : "0");
            objeto.addProperty("otros_ingresos", (finanza != null) ? finanza.getOtros_ingresos() : "0");
            objeto.addProperty("total_ingresos", (finanza != null) ? finanza.getTotal_ingresos() : "0");
            objeto.addProperty("descuento_nomina", (finanza != null) ? finanza.getDescuento_nomina() : "0");
            objeto.addProperty("gastos_arriendo", (finanza != null) ? finanza.getGastos_arriendo() : "0");
            objeto.addProperty("gastos_creditos", (finanza != null) ? finanza.getGastos_creditos() : "0");
            objeto.addProperty("otros_gastos", (finanza != null) ? finanza.getOtros_gastos() : "0");
            objeto.addProperty("total_egresos", (finanza != null) ? finanza.getTotal_egresos() : "0");
            objeto.addProperty("activos", (finanza != null) ? finanza.getActivos() : "0");
            objeto.addProperty("pasivos", (finanza != null) ? finanza.getPasivos() : "0");
            objeto.addProperty("total_patrimonio", (finanza != null) ? finanza.getTotal_patrimonio() : "0");

            pers.add("finanza", objeto);

            //INFORMACION TRANSACCIONES
            objeto = new JsonObject();

            objeto.addProperty("trans_ext", (transacciones != null) ? transacciones.getTransacciones_al_extanjero() : "N");
            objeto.addProperty("importaciones", (transacciones != null) ? transacciones.getImportaciones() : "N");
            objeto.addProperty("exportaciones", (transacciones != null) ? transacciones.getExportaciones() : "N");
            objeto.addProperty("inversiones", (transacciones != null) ? transacciones.getInversiones() : "N");
            objeto.addProperty("giros", (transacciones != null) ? transacciones.getGiros() : "N");
            objeto.addProperty("prestamos", (transacciones != null) ? transacciones.getPrestamos() : "N");
            objeto.addProperty("otras_transacciones", (transacciones != null) ? transacciones.getOtras_transacciones() : "N");
            objeto.addProperty("pago_cuenta_exterior", (transacciones != null) ? transacciones.getPago_cuenta_exterior() : "N");
            objeto.addProperty("banco", (transacciones != null) ? transacciones.getBanco() : "");
            objeto.addProperty("cuenta", (transacciones != null) ? transacciones.getCuenta() : "");
            objeto.addProperty("pais", (transacciones != null) ? transacciones.getPais() : "");
            objeto.addProperty("ciudad", (transacciones != null) ? transacciones.getCiudad() : "");
            objeto.addProperty("moneda", (transacciones != null) ? transacciones.getMoneda() : "");
            objeto.addProperty("tipo_producto", (transacciones != null) ? transacciones.getTipo_pro() : "");
            objeto.addProperty("monto", (transacciones != null) ? transacciones.getMonto() : "0");

            pers.add("transaccion", objeto);
            //INFORMACION REFERENCIAS
            if (referencias != null) {
                SolicitudReferencias r;
                JsonArray ref_per = new JsonArray(), ref_fam = new JsonArray();
                for (int i = 0; i < referencias.size(); i++) {
                    r = referencias.get(i);
                    objeto = new JsonObject();
                    objeto.addProperty("primer_apellido", r.getPrimerApellido());
                    objeto.addProperty("segundo_apellido", r.getSegundoApellido());
                    objeto.addProperty("primer_nombre", r.getPrimerNombre());
                    objeto.addProperty("segundo_nombre", r.getSegundoNombre());
                    objeto.addProperty("nombre", r.getNombre());
                    objeto.addProperty("celular", r.getCelular());
                    objeto.addProperty("telefono", r.getTelefono());
                    objeto.addProperty("telefono_2", r.getTelefono2());
                    objeto.addProperty("extension", r.getExtension());
                    objeto.addProperty("direccion", r.getDireccion());
                    objeto.addProperty("departamento", r.getDepartamento());
                    objeto.addProperty("ciudad", r.getCiudad());
                    objeto.addProperty("parentesco", r.getParentesco());
                    objeto.addProperty("secuencia", r.getSecuencia());
                    objeto.addProperty("email", r.getEmail());
                    objeto.addProperty("tiempo_conocido", r.getTiempoConocido());
                    objeto.addProperty("tipo", r.getTipo());
                    objeto.addProperty("tipo_referencia", r.getTipoReferencia());
                    if (r.getTipoReferencia().equalsIgnoreCase("P")) {
                        ref_per.add(objeto);
                    } else {
                        ref_fam.add(objeto);
                    }
                }
                pers.add("referencias_personales", ref_per);
                pers.add("referencias_familiares", ref_fam);
            }

        } catch (Exception e) {
            pers = new JsonObject();
            pers.addProperty("error", e.getMessage());
        } finally {
            return pers;
        }
    }

    private void guardarFormulario() {
        ArrayList<SolicitudPersona> personaList = new ArrayList<>();
        ArrayList<SolicitudReferencias> referenciaList = new ArrayList<>();
        ArrayList<SolicitudLaboral> laboralList = new ArrayList<>();
        ArrayList<SolicitudFinanzas> finanzasList = new ArrayList<>();
        ArrayList<SolicitudTransacciones> transaccionesList = new ArrayList<>();
        ArrayList<SolicitudBienes> bienesList = new ArrayList<>();
        ArrayList<SolicitudVehiculo> vehiculosList = new ArrayList<>();
        ArrayList<SolicitudOblComprar> obligacionesList = new ArrayList<>();
        
        SolicitudAval solicitud = new SolicitudAval();

        JsonObject formulario, objeto;

        boolean ok = true; //, crearneg = false;
        try {
            formulario = (JsonObject) (new JsonParser()).parse(request.getParameter("formulario"));
            
            objeto = formulario.getAsJsonObject("cabecera");
            String num_solicitud = objeto.get("num_solicitud").getAsString();
            int id_filtro = objeto.get("id_filtro") != null ? objeto.get("id_filtro").getAsInt(): 0;
            if (num_solicitud.equalsIgnoreCase("0000") || num_solicitud.equalsIgnoreCase("")) {
                num_solicitud = generarNumeroSolicitud();
            }

            String actividad = objeto.get("actividad") != null ? objeto.get("actividad").getAsString() : "";
            String convenio = objeto.get("convenio") != null ? objeto.get("convenio").getAsString() : "";
            boolean codeu_1 = objeto.get("codeu_1") != null && objeto.get("codeu_1").getAsString().equalsIgnoreCase("on");
            boolean codeu_2 = objeto.get("codeu_2") != null && objeto.get("codeu_2").getAsString().equalsIgnoreCase("on");

            //primero los datos basicos de la solicitud
            solicitud.setNumeroSolicitud(num_solicitud);
            solicitud.setEstadoSol(objeto.get("est_sol") != null ? objeto.get("est_sol").getAsString() : "B");
            solicitud.setActividadNegocio(actividad);
            solicitud.setIdConvenio(convenio);
            solicitud.setCodNegocio(objeto.get("negocio") != null ? objeto.get("negocio").getAsString() : "");
            solicitud.setFechaConsulta(objeto.get("fecha_cons") != null ? objeto.get("fecha_cons").getAsString() : "now()");
            solicitud.setValorSolicitado(objeto.get("valor_solicitado") != null ? objeto.get("valor_solicitado").getAsString().replaceAll(",", "") : "0");
            solicitud.setAgente(objeto.get("agente") != null ? objeto.get("agente").getAsString() : "");
            solicitud.setAfiliado(objeto.get("pagaduria") != null ? objeto.get("pagaduria").getAsString() : "");
            solicitud.setCodigo(objeto.get("codigo") != null ? objeto.get("codigo").getAsString() : "");
            solicitud.setNumeroAprobacion(objeto.get("numero_aprobacion") != null ? objeto.get("numero_aprobacion").getAsString() : "");
            solicitud.setProducto(objeto.get("producto") != null ? objeto.get("producto").getAsString() : "");
            solicitud.setServicio(objeto.get("servicio") != null ? objeto.get("servicio").getAsString() : "");
            solicitud.setValorProducto(objeto.get("valor_producto") != null ? objeto.get("valor_producto").getAsString().replaceAll(",", "") : "0");
            solicitud.setPlazo(objeto.get("plazo") != null ? objeto.get("plazo").getAsString() : "1");
            solicitud.setTipoNegocio(objeto.get("cmbTituloValor") != null ? objeto.get("cmbTituloValor").getAsString() : "");
            solicitud.setPlazoPrCuota(objeto.get("forma_pago") != null ? objeto.get("forma_pago").getAsString() : "1");
            solicitud.setSector(objeto.get("sectid") != null ? objeto.get("sectid").getAsString() : "");

            solicitud.setCiudadMatricula("");
            solicitud.setSubsector(objeto.get("subsectid") != null ? objeto.get("subsectid").getAsString() : "");
            solicitud.setBanco("");
            solicitud.setNumTipoNegocio("");
            solicitud.setRenovacion("N");
            solicitud.setAsesor(objeto.get("asesores") != null ? objeto.get("asesores").getAsString() : "");
            solicitud.setFecha_primera_cuota("0099-01-01");
            solicitud.setCodNegocioRenovado("");
            solicitud.setPreAprobadoMicro("N");
            solicitud.setTipoPersona("N");
            solicitud.setFianza(objeto.get("fianza") != null ? objeto.get("fianza").getAsString() : "N");  
            solicitud.setCuotaMaxima(0+"");
            solicitud.setNit_fondo("8110104853");
            solicitud.setProducto_fondo(1);
            solicitud.setCobertura_fondo(1);
            
            solicitud.setDestinoCredito(objeto.get("destino") != null ? objeto.get("destino").getAsString() : "");
            
            if (formulario.getAsJsonObject("solicitante") != null) {
                
                addPersonaNatural(personaList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                addFinanzas(finanzasList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                addReferencia(referenciaList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                addBienes(bienesList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                addVehiculos(vehiculosList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                addObligaciones(obligacionesList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                addLaboral(laboralList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                //addTransacciones(transaccionesList, num_solicitud, "S", formulario.getAsJsonObject("solicitante"));
                
            } else {
                throw new Exception("No hay informacion del solicitante");
            }
        
            if (codeu_1) {
                addPersonaNatural(personaList, num_solicitud, "C", formulario.getAsJsonObject("codeudor_1"));
                addLaboral(laboralList, num_solicitud, "C", formulario.getAsJsonObject("codeudor_1"));
                addReferencia(referenciaList, num_solicitud, "C", formulario.getAsJsonObject("codeudor_1"));
            }
            if (codeu_2) {
                addPersonaNatural(personaList, num_solicitud, "C", formulario.getAsJsonObject("codeudor_2"));
                addLaboral(laboralList, num_solicitud, "C", formulario.getAsJsonObject("codeudor_2"));
                addReferencia(referenciaList, num_solicitud, "C", formulario.getAsJsonObject("codeudor_2"));
            }
            
            respuesta = new JsonObject();
            respuesta.addProperty("mensaje", "");
            
            ok = gsaserv.insertarSolicitudLibranza( solicitud, personaList, laboralList, referenciaList
                                                  , finanzasList, transaccionesList, bienesList, vehiculosList, obligacionesList);
            gsaserv.actualizarSolicitudFiltroLibranza(Integer.parseInt(num_solicitud),id_filtro);
                                  
            if (ok) {
                double vlr_fianza = libdao.obtenerValorFianza(num_solicitud, solicitud.getIdConvenio(),Integer.parseInt(solicitud.getPlazo()),Double.parseDouble(solicitud.getValorSolicitado()),solicitud.getProducto_fondo(),solicitud.getCobertura_fondo(),solicitud.getNit_fondo());
                respuesta.addProperty("estado_solicitud", solicitud.getEstadoSol());
                respuesta.addProperty("codigo_negocio", solicitud.getCodNegocio());
                respuesta.addProperty("numero_solicitud", num_solicitud);
                respuesta.addProperty("fecha_negocio", solicitud.getFechaConsulta());
                respuesta.addProperty("convenio", solicitud.getIdConvenio());
                respuesta.addProperty("numero_cuotas", solicitud.getPlazo());
                respuesta.addProperty("valor_negocio", Double.parseDouble(solicitud.getValorSolicitado()));
                respuesta.addProperty("plazo_primera_cuota", solicitud.getPlazoPrCuota());
                respuesta.addProperty("titulo_valor", solicitud.getTipoNegocio());
                respuesta.addProperty("valor_fianza",vlr_fianza);
            } else {
                respuesta.addProperty("mensaje", "No se ha logrado procesar la solicitud");
                throw new Exception("No se ha logrado procesar la solicitud");
            }
            
          } catch (Exception e) {
            e.printStackTrace();
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        }
    }

    private void addReferencia(ArrayList<SolicitudReferencias> referenciaList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonArray array = (jPerSon != null) ? jPerSon.getAsJsonArray("referencia") : new JsonArray();
        JsonObject info;
        String referenciado = "";
        try {
            if (jPerSon != null) {
                referenciado = jPerSon.getAsJsonObject("basica") != null
                        ? jPerSon.getAsJsonObject("basica").get("id").getAsString()
                        : "";
            }
        } catch (Exception e) {
        }
        for (int i = 0; i < array.size(); i++) {
            info = array.get(i).getAsJsonObject();
            if (info != null) {
                SolicitudReferencias referencia = new SolicitudReferencias();
                referencia.setNumeroSolicitud(num_solicitud);
                referencia.setTipo(tipo_persona);
                referencia.setTipoReferencia(info.get("tipo_referencia").getAsString());
                referencia.setReferenciado(referenciado);
                referencia.setSecuencia("" + referenciaList.size());
                referencia.setPrimerApellido(info.get("pr_apellido") != null ? info.get("pr_apellido").getAsString() : "");
                referencia.setSegundoApellido(info.get("seg_apellido") != null ? info.get("seg_apellido").getAsString() : "");
                referencia.setPrimerNombre(info.get("pr_nombre") != null ? info.get("pr_nombre").getAsString() : "");
                referencia.setSegundoNombre(info.get("seg_nombre") != null ? info.get("seg_nombre").getAsString() : "");
                referencia.setNombre(referencia.getPrimerApellido() + " " + referencia.getSegundoApellido() + " " + referencia.getPrimerNombre() + " " + referencia.getSegundoNombre());

                if (!referencia.getNombre().trim().equalsIgnoreCase("")) {
                    referencia.setTelefono(info.get("tel1") != null ? info.get("tel1").getAsString() : "");
                    referencia.setTelefono2(info.get("tel2") != null ? info.get("tel2").getAsString() : "");
                    referencia.setExtension(info.get("ext") != null ? info.get("ext").getAsString() : "");
                    referencia.setCelular(info.get("cel") != null ? info.get("cel").getAsString() : "");
                    referencia.setCiudad(info.get("ciu") != null ? info.get("ciu").getAsString() : "");
                    referencia.setDepartamento(info.get("dep") != null ? info.get("dep").getAsString() : "");
                    referencia.setTiempoConocido(info.get("tconocido") != null ? info.get("tconocido").getAsString() : "0");
                    referencia.setParentesco(info.get("parent") != null ? info.get("parent").getAsString() : "");
                    referencia.setEmail(info.get("mail") != null ? info.get("mail").getAsString() : "");
                    referenciaList.add(referencia);
                }
            }
        }
    }

    private void addPersonaNatural(ArrayList<SolicitudPersona> personaList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonObject info = (jPerSon != null) ? jPerSon.getAsJsonObject("basica") : null;
        if (info != null) {
            SolicitudPersona persona = new SolicitudPersona();
            persona.setNumeroSolicitud(num_solicitud);
            persona.setTipoPersona("N");
            persona.setTipo(tipo_persona);
            persona.setPrimerApellido(info.get("pr_apellido") != null ? info.get("pr_apellido").getAsString() : "");
            persona.setSegundoApellido(info.get("seg_apellido") != null ? info.get("seg_apellido").getAsString() : "");
            persona.setPrimerNombre(info.get("pr_nombre") != null ? info.get("pr_nombre").getAsString() : "");
            persona.setSegundoNombre(info.get("seg_nombre") != null ? info.get("seg_nombre").getAsString() : "");
            persona.setNombre(persona.getPrimerApellido() + " " + persona.getSegundoApellido() + " " + persona.getPrimerNombre() + " " + persona.getSegundoNombre());//nombre completo
            persona.setGenero(info.get("genero") != null ? info.get("genero").getAsString() : "M");
            persona.setTipoId(info.get("tipo_id") != null ? info.get("tipo_id").getAsString() : "CED");
            persona.setIdentificacion(info.get("id") != null && !info.get("id").getAsString().equals("") ? info.get("id").getAsString() : "");
            persona.setDireccion(info.get("dir") != null ? info.get("dir").getAsString() : "");
            persona.setTelefono(info.get("tel") != null ? info.get("tel").getAsString() : "");
            persona.setFechaExpedicionId(info.get("f_exp") != null ? (info.get("f_exp").getAsString().equals("") ? "0099-01-01 00:00:00" : info.get("f_exp").getAsString()) : "0099-01-01 00:00:00");
            persona.setCiudadExpedicionId(info.get("ciu_exp") != null ? info.get("ciu_exp").getAsString() : "");
            persona.setDptoExpedicionId(info.get("dep_exp") != null ? info.get("dep_exp").getAsString() : "");
            persona.setFechaNacimiento(info.get("f_nac") != null ? (info.get("f_nac").getAsString().equals("") ? "0099-01-01 00:00:00" : info.get("f_nac").getAsString()) : "0099-01-01 00:00:00");
            persona.setCiudadNacimiento(info.get("ciu_nac") != null ? info.get("ciu_nac").getAsString() : "");
            persona.setDptoNacimiento(info.get("dep_nac") != null ? info.get("dep_nac").getAsString() : "");
            persona.setEstadoCivil(info.get("est_civil") != null ? info.get("est_civil").getAsString() : "");
            persona.setNivelEstudio(info.get("niv_est") != null ? info.get("niv_est").getAsString() : "");
            persona.setProfesion(info.get("prof") != null ? info.get("prof").getAsString() : "");
            persona.setPersonasACargo(info.get("pcargo") != null && !info.get("pcargo").getAsString().equals("") ? info.get("pcargo").getAsString() : "0");
            persona.setNumHijos(info.get("nhijos") != null && !info.get("nhijos").getAsString().equals("") ? info.get("nhijos").getAsString() : "0");
            persona.setTotalGrupoFamiliar(info.get("ngrupo") != null && !info.get("ngrupo").getAsString().equals("") ? info.get("ngrupo").getAsString() : "0");
            persona.setCiudad(info.get("ciu") != null ? info.get("ciu").getAsString() : "");
            persona.setDepartamento(info.get("dep") != null ? info.get("dep").getAsString() : "");
            persona.setBarrio(info.get("barrio") != null ? info.get("barrio").getAsString() : "");
            persona.setEstrato(info.get("estr") != null && !info.get("estr").getAsString().equals("") ? info.get("estr").getAsString() : "0");
            persona.setFechaConstitucion("0099-01-01 00:00:00");
            persona.setTipoVivienda(info.get("tipo_viv") != null ? info.get("tipo_viv").getAsString() : "");
            persona.setCelular(info.get("cel") != null ? info.get("cel").getAsString() : "");
            persona.setEmail(info.get("mail") != null ? info.get("mail").getAsString() : "");

            persona.setTipoIdentificacionCony(info.get("tipo_id_conyuge") != null ? info.get("tipo_id_conyuge").getAsString() : "");
            persona.setIdentificacionCony(info.get("id_conyuge") != null ? info.get("id_conyuge").getAsString() : "");
            persona.setPrimerApellidoCony(info.get("pr_apellido_conyuge") != null ? info.get("pr_apellido_conyuge").getAsString() : "");
            persona.setSegundoApellidoCony(info.get("seg_apellido_conyuge") != null ? info.get("seg_apellido_conyuge").getAsString() : "");
            persona.setPrimerNombreCony(info.get("pr_nombre_conyuge") != null ? info.get("pr_nombre_conyuge").getAsString() : "");
            persona.setTelefonoCony(info.get("tel_conyuge") != null ? info.get("tel_conyuge").getAsString() : "");
            persona.setCelularCony(info.get("cel_conyuge") != null ? info.get("cel_conyuge").getAsString() : "");            
            persona.setSalarioCony("0");
            String aux = "";
            try {
                aux = gsaserv.codigoCliente(info.get("id") != null ? info.get("id").getAsString() : "").split(";_;")[0];
            } catch (Exception e) {
                aux = "";
            }
            persona.setCodcli(aux);
            aux = (info.get("an_res") != null && !info.get("an_res").getAsString().equals("") ? info.get("an_res").getAsString() : "0")
                    + " A�os "
                    + (info.get("mes_res") != null && !info.get("mes_res").getAsString().equals("") ? info.get("mes_res").getAsString() : "0")
                    + " Meses";
            persona.setTiempoResidencia(aux);

            personaList.add(persona);
        }
    }

    private void addLaboral(ArrayList<SolicitudLaboral> laboralList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonObject info = (jPerSon != null) ? jPerSon.getAsJsonObject("laboral") : null;
        if (info != null) {
            String id_persona = "";
            try {
                if (jPerSon != null) {
                    id_persona = jPerSon.getAsJsonObject("basica") != null
                            ? jPerSon.getAsJsonObject("basica").get("id").getAsString()
                            : "";
                }
            } catch (Exception e) {
            }
            SolicitudLaboral laboral = new SolicitudLaboral();
            laboral.setNumeroSolicitud(num_solicitud);
            laboral.setId_persona(id_persona);
            laboral.setTipo(tipo_persona);
            laboral.setOcupacion(info.get("ocup") != null ? info.get("ocup").getAsString() : "");
            laboral.setActividadEconomica(info.get("act_econ") != null ? info.get("act_econ").getAsString() : "");
            laboral.setNombreEmpresa(info.get("nom_emp") != null ? info.get("nom_emp").getAsString() : "");
            String nit = info.get("nit_emp") != null ? info.get("nit_emp").getAsString() : "";
            nit += info.get("dig_nit_emp") != null && !info.get("dig_nit_emp").equals("") ? "-" + info.get("dig_nit_emp").getAsString() : "";
            laboral.setNit(nit);
            laboral.setDireccion(info.get("dir_emp") != null ? info.get("dir_emp").getAsString() : "");
            laboral.setDireccionCobro(info.get("dir_cob") != null ? info.get("dir_cob").getAsString() : "");
            laboral.setCiudad(info.get("ciu_emp") != null ? info.get("ciu_emp").getAsString() : "");
            laboral.setDepartamento(info.get("dep_emp") != null ? info.get("dep_emp").getAsString() : "");
            laboral.setTelefono(info.get("tel_emp") != null ? info.get("tel_emp").getAsString() : "");
            laboral.setExtension(info.get("ext_emp") != null ? info.get("ext_emp").getAsString() : "");
            laboral.setCargo(info.get("car_emp") != null ? info.get("car_emp").getAsString() : "");
            laboral.setFechaIngreso(info.get("f_ing") != null && !info.get("f_ing").getAsString().equals("") ? info.get("f_ing").getAsString() : "0099-01-01 00:00:00");
            laboral.setTipoContrato(info.get("tipo_cont") != null ? info.get("tipo_cont").getAsString() : "");
            if(tipo_persona.equalsIgnoreCase("S")) {
                JsonObject finz = (jPerSon != null) ? jPerSon.getAsJsonObject("financiera") : null;
                laboral.setConceptoOtrosIng("");
                laboral.setOcupacion(info.get("ocupations") != null ? info.get("ocupations").getAsString() : "");
                laboral.setSalario(finz != null && finz.get("salario") != null && !finz.get("salario").getAsString().equals("") ? finz.get("salario").getAsString().replaceAll(",", "") : "0");
                double otros_ingresos = 0;
                try {
                    otros_ingresos = finz != null && finz.get("honorarios") != null && !finz.get("honorarios").getAsString().equals("") ? Double.parseDouble(finz.get("honorarios").getAsString().replaceAll(",", "")) : 0;
                    otros_ingresos += finz != null && finz.get("otros_ingresos") != null && !finz.get("otros_ingresos").getAsString().equals("") ? Double.parseDouble(finz.get("otros_ingresos").getAsString().replaceAll(",", "")) : 0;
                } catch(Exception w) { otros_ingresos = 0; }
                laboral.setOtrosIngresos(String.valueOf(otros_ingresos));
                laboral.setGastosManutencion(finz != null && finz.get("otros_gastos") != null && !finz.get("otros_gastos").getAsString().equals("") ? finz.get("otros_gastos").getAsString().replaceAll(",", "") : "0");
                laboral.setGastosCreditos(finz != null && finz.get("gastos_creditos") != null && !finz.get("gastos_creditos").getAsString().equals("") ? finz.get("gastos_creditos").getAsString().replaceAll(",", "") : "0");
                laboral.setGastosArriendo(finz != null && finz.get("gastos_arriendo") != null && !finz.get("gastos_arriendo").getAsString().equals("") ? finz.get("gastos_arriendo").getAsString().replaceAll(",", "") : "0");
            } else {
                laboral.setSalario(info.get("sal") != null && !info.get("sal").getAsString().equals("") ? info.get("sal").getAsString().replaceAll(",", "") : "0");
                laboral.setOtrosIngresos(info.get("otros") != null && !info.get("otros").getAsString().equals("") ? info.get("otros").getAsString().replaceAll(",", "") : "0");
                laboral.setConceptoOtrosIng(info.get("conc_otros") != null ? info.get("conc_otros").getAsString() : "");
                laboral.setGastosManutencion(info.get("manuten") != null && !info.get("manuten").getAsString().equals("") ? info.get("manuten").getAsString().replaceAll(",", "") : "0");
                laboral.setGastosCreditos(info.get("cred") != null && !info.get("cred").getAsString().equals("") ? info.get("cred").getAsString().replaceAll(",", "") : "0");
                laboral.setGastosArriendo(info.get("arr") != null && !info.get("arr").getAsString().equals("") ? info.get("arr").getAsString().replaceAll(",", "") : "0");
            }
            laboral.setCelular(info.get("cel_emp") != null ? info.get("cel_emp").getAsString() : "");
            laboral.setEmail(info.get("mail_empr") != null ? info.get("mail_empr").getAsString() : "");
            laboral.setEps(info.get("eps") != null ? info.get("eps").getAsString() : "");
            laboral.setTipoAfiliacion(info.get("tip_afil") != null ? info.get("tip_afil").getAsString() : "");
            laboralList.add(laboral);
        }
    }

    private void addFinanzas(ArrayList<SolicitudFinanzas> finanzasList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonObject info = (jPerSon != null) ? jPerSon.getAsJsonObject("financiera") : null;
        if (info != null) {
            String id_persona = "";
            try {
                if (jPerSon != null) {
                    id_persona = jPerSon.getAsJsonObject("basica") != null
                            ? jPerSon.getAsJsonObject("basica").get("id").getAsString()
                            : "";
                }
            } catch (Exception e) {
            }
            SolicitudFinanzas sf = new SolicitudFinanzas();
            sf.setNumeroSolicitud(num_solicitud);
            sf.setTipo(tipo_persona);
            sf.setId_persona(id_persona);

            sf.setSalario(info.get("salario") != null ? info.get("salario").getAsString().replaceAll(",", "") : "0");
            sf.setHonorarios(info.get("honorarios") != null ? info.get("honorarios").getAsString().replaceAll(",", "") : "0");
            sf.setOtros_ingresos(info.get("otros_ingresos") != null ? info.get("otros_ingresos").getAsString().replaceAll(",", "") : "0");
            sf.setTotal_ingresos(info.get("total_ingresos") != null ? info.get("total_ingresos").getAsString().replaceAll(",", "") : "0");
            sf.setDescuento_nomina(info.get("descuento_nomina") != null ? info.get("descuento_nomina").getAsString().replaceAll(",", "") : "0");
            sf.setGastos_arriendo(info.get("gastos_arriendo") != null ? info.get("gastos_arriendo").getAsString().replaceAll(",", "") : "0");
            sf.setGastos_creditos(info.get("gastos_creditos") != null ? info.get("gastos_creditos").getAsString().replaceAll(",", "") : "0");
            sf.setOtros_gastos(info.get("otros_gastos") != null ? info.get("otros_gastos").getAsString().replaceAll(",", "") : "0");
            sf.setTotal_egresos(info.get("total_egresos") != null ? info.get("total_egresos").getAsString().replaceAll(",", "") : "0");
            sf.setActivos(info.get("activos") != null ? info.get("activos").getAsString().replaceAll(",", "") : "0");
            sf.setPasivos(info.get("pasivos") != null ? info.get("pasivos").getAsString().replaceAll(",", "") : "0");
            sf.setTotal_patrimonio(info.get("total_patrimonio") != null ? info.get("total_patrimonio").getAsString().replaceAll(",", "") : "0");
            finanzasList.add(sf);
        }
    }

    private void addTransacciones(ArrayList<SolicitudTransacciones> transaccionesList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonObject info = (jPerSon != null) ? jPerSon.getAsJsonObject("transacciones") : null;
        if (info != null) {
            String id_persona = "";
            try {
                if (jPerSon != null) {
                    id_persona = jPerSon.getAsJsonObject("basica") != null
                            ? jPerSon.getAsJsonObject("basica").get("id").getAsString()
                            : "";
                }
            } catch (Exception e) {
            }
            SolicitudTransacciones st = new SolicitudTransacciones();
            st.setNumeroSolicitud(num_solicitud);
            st.setTipo(tipo_persona);
            st.setId_persona(id_persona);

            st.setTransacciones_al_extanjero(info.get("trans_ext") != null && info.get("trans_ext").getAsString().equalsIgnoreCase("on") ? "S" : "N");
            st.setImportaciones(info.get("importaciones") != null && info.get("importaciones").getAsString().equalsIgnoreCase("on") && st.getTransacciones_al_extanjero().equals("S") ? "S" : "N");
            st.setExportaciones(info.get("exportaciones") != null && info.get("exportaciones").getAsString().equalsIgnoreCase("on") && st.getTransacciones_al_extanjero().equals("S") ? "S" : "N");
            st.setInversiones(info.get("inversiones") != null && info.get("inversiones").getAsString().equalsIgnoreCase("on") && st.getTransacciones_al_extanjero().equals("S") ? "S" : "N");
            st.setGiros(info.get("giros") != null && info.get("giros").getAsString().equalsIgnoreCase("on") && st.getTransacciones_al_extanjero().equals("S") ? "S" : "N");
            st.setPrestamos(info.get("prestamos") != null && info.get("prestamos").getAsString().equalsIgnoreCase("on") && st.getTransacciones_al_extanjero().equals("S") ? "S" : "N");
            st.setOtras_transacciones(info.get("otros") != null && info.get("otros").getAsString().equalsIgnoreCase("on") && st.getTransacciones_al_extanjero().equals("S") ? "S" : "N");
            st.setPago_cuenta_exterior(info.get("pago_cuenta_ext") != null && info.get("pago_cuenta_ext").getAsString().equalsIgnoreCase("on") && st.getTransacciones_al_extanjero().equals("S") ? "S" : "N");
            st.setBanco(info.get("banco") != null && st.getPago_cuenta_exterior().equals("S") ? info.get("banco").getAsString() : "");
            st.setCiudad(info.get("ciudad") != null && st.getPago_cuenta_exterior().equals("S") ? info.get("ciudad").getAsString() : "");
            st.setCuenta(info.get("cuenta") != null && st.getPago_cuenta_exterior().equals("S") ? info.get("cuenta").getAsString() : "");
            st.setPais(info.get("pais") != null && st.getPago_cuenta_exterior().equals("S") ? info.get("pais").getAsString() : "");
            st.setMoneda(info.get("moneda_ext") != null && st.getPago_cuenta_exterior().equals("S") ? info.get("moneda_ext").getAsString() : "");
            st.setTipo_pro(info.get("tipo_producto") != null && st.getTransacciones_al_extanjero().equals("S") ? info.get("tipo_producto").getAsString() : "");
            st.setMonto(info.get("monto") != null && !info.get("monto").getAsString().equalsIgnoreCase("") && st.getTransacciones_al_extanjero().equals("S") ? info.get("monto").getAsString() : "0");

            transaccionesList.add(st);
        }
    }

    private void generarLiquidacion() {
        try {
            this.responceTrue=true;
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("info"));
            respuesta = libdao.generarLiquidacion(info);
        } catch(Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        }
    }

    private void guardarLiquidacion(String usuario) {
        try {
            this.responceTrue=true;
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("info"));
            info.addProperty("usuario", usuario);
            respuesta = libdao.guardarNegocio(info);
        } catch(Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        }
    }

    public ArrayList buscarLiquidacion(String codigo_negocio) {
        try {
            respuesta = libdao.buscarLiquidacion(codigo_negocio);
            ArrayList<DocumentosNegAceptado> lista = new ArrayList<>();
            JsonArray filas = respuesta.getAsJsonArray("rows");
            DocumentosNegAceptado dna;
            JsonObject rs;
            for (int i = 0; i < filas.size(); i++) {
                dna = new DocumentosNegAceptado();
                rs = filas.get(i).getAsJsonObject();
                dna.setItem(rs.get("item").getAsString());
                dna.setFecha(rs.get("fecha").getAsString());
                dna.setDias(rs.get("dias").getAsString());
                dna.setSaldo_inicial(rs.get("saldo_inicial").getAsDouble());
                dna.setCapital(rs.get("capital").getAsDouble());
                dna.setInteres(rs.get("interes").getAsDouble());
                dna.setValor(rs.get("valor").getAsDouble());
                dna.setSaldo_final(rs.get("saldo_final").getAsDouble());
                dna.setSeguro(rs.get("seguro").getAsDouble());
                dna.setCuota_manejo(rs.get("cuota_manejo").getAsDouble());
                dna.setAval(rs.get("aval").getAsDouble());
                
                lista.add(dna);
            } return lista;
        } catch(Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        } return new ArrayList();
    }
    
    private void buscarFechaPago(String fecha_negocio, String dias_plazo) {
        try {
            respuesta = libdao.buscarFechaPago(fecha_negocio, dias_plazo);
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        }
    }
    
    private void calcularFechaPago(String fecha_negocio, String dias_plazo) {
        try {
            this.responceTrue=false;
            this.printlnResponseAjax(libdao.calcularFechaPago(fecha_negocio, dias_plazo), "application/json");
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        }
    }    
  
    private void validarObligaciones(String tipo, String ocupaciones, double vlr_sol, double oblig, int Cantoblig, String cuotas, String fianza) {
        try {           
            respuesta = libdao.validarObligaciones(tipo, ocupaciones, vlr_sol, oblig, Cantoblig, cuotas, fianza);
        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", e.getMessage());
        }
    }

    private void addBienes(ArrayList<SolicitudBienes> bienesList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonArray array = (jPerSon != null) ? jPerSon.getAsJsonArray("bienes") : new JsonArray();
        JsonObject info;
        for (int i = 0; i < array.size(); i++) {
            info = array.get(i).getAsJsonObject();
            if (info != null) {
                SolicitudBienes sb = new SolicitudBienes();
                sb.setNumeroSolicitud(num_solicitud);
                sb.setTipo(tipo_persona);
                sb.setSecuencia(""+(i+1));
                
                sb.setTipoBien(info.get("tipo_bien") != null ? info.get("tipo_bien").getAsString() : "");
                sb.setValorComercial(info.get("valor_com") != null ? info.get("valor_com").getAsString().replaceAll(",", "") : "0");
                sb.setHipoteca(info.get("hipoteca") != null ? info.get("hipoteca").getAsString() : "");
                sb.setaFavorDe(info.get("favorde") != null ? info.get("favorde").getAsString() : "");
                sb.setDireccion(info.get("dir_bien") != null ? info.get("dir_bien").getAsString() : "");
                bienesList.add(sb);
            }
        }
    }
    
    private void addVehiculos(ArrayList<SolicitudVehiculo> vehiculosList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonArray array = (jPerSon != null) ? jPerSon.getAsJsonArray("vehiculos") : new JsonArray();
        JsonObject info;
        for (int i = 0; i < array.size(); i++) {
            info = array.get(i).getAsJsonObject();
            if (info != null) {
                SolicitudVehiculo sv = new SolicitudVehiculo();
                sv.setNumeroSolicitud(num_solicitud);
                sv.setTipo(tipo_persona);
                sv.setSecuencia(""+(i+1));
                
                sv.setTipoVehiculo(info.get("tipo_veh") != null ? info.get("tipo_veh").getAsString() : "");
                sv.setMarca(info.get("marca_veh") != null ? info.get("marca_veh").getAsString() : "");
                sv.setPlaca(info.get("placa_veh") != null ? info.get("placa_veh").getAsString() : "");
                sv.setValorComercial(info.get("valor_veh") != null ? info.get("valor_veh").getAsString().replaceAll(",", "") : "0");
                sv.setModelo(info.get("modelo_veh") != null ? info.get("modelo_veh").getAsString() : "");
                sv.setCuotaMensual(info.get("cuota_veh") != null ? info.get("cuota_veh").getAsString().replaceAll(",", "") : "0");
                sv.setPignoradoAFavorDe(info.get("pign_veh") != null ? info.get("pign_veh").getAsString() : "");
                vehiculosList.add(sv);
            }
        }
    }
    
    private void addObligaciones(ArrayList<SolicitudOblComprar> obligacionesList, String num_solicitud, String tipo_persona, JsonObject jPerSon) {
        JsonArray array = (jPerSon != null) ? jPerSon.getAsJsonArray("obligaciones") : new JsonArray();
        JsonObject info;
        for (int i = 0; i < array.size(); i++) {
            info = array.get(i).getAsJsonObject();
            if (info != null) {
                SolicitudOblComprar so = new SolicitudOblComprar();
                so.setNumero_solicitud(num_solicitud);
                so.setSecuencia(""+(i+1));

                so.setEntidad(info.get("entidad") != null ? info.get("entidad").getAsString() : "");
                so.setNit_proveedor(info.get("nit_prov") != null ? info.get("nit_prov").getAsString() : "");
                so.setTipo_cuenta(info.get("tcuenta_prov") != null ? info.get("tcuenta_prov").getAsString() : "");
                so.setNumero_cuenta(info.get("cuenta_prov") != null ? info.get("cuenta_prov").getAsString() : "");
                so.setValor_comprar(info.get("valor_recoger") != null ? info.get("valor_recoger").getAsString().replaceAll(",", "") : "0");
                if (!so.getValor_comprar().equalsIgnoreCase("0")) {
                    obligacionesList.add(so);
                }                
            }
        }
    }
    
    public JsonArray buscarBarrios(String codciu) {
        respuesta = new JsonObject();
        try {
            this.addElement("barrios", gsaserv.listadoBarrios(codciu));
            

        } catch (Exception e) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "No se encontraron barrios");
            e.printStackTrace();
        }
        finally {
            return respuesta.get("barrios").getAsJsonArray();
        }
    }
    
        private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }

        
}
