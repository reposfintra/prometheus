
package com.tsp.operation.controller;
/*  @author  fvillacob */


import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;



public class ConsultaOCBuscarAction extends Action{
    
    
    public void run() throws ServletException, InformationException {
        
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String login=usuario.getLogin();
           
            if(!login.equals("") ){
                String comentario="";
                String distrito =request.getParameter("distrito");
                int tipo = Integer.parseInt(request.getParameter("tipo"));
                String next="";
                String numrem = request.getParameter("numeroOT");
                if(tipo==1){
                    
                    String oc=request.getParameter("numeroOC").toUpperCase();
                    /********************* OJO *****************/
                    comentario=model.PlanillasSvc.searchOC(distrito,oc,usuario.getBase(),numrem);
                    
                    
                    
                    
                    next= "/datosplanilla/MostrarOCs.jsp?comentario="+comentario;
                }
                else{
                    String fecha1=request.getParameter("FechaInicial");
                    fecha1=Util.formatoFechaPostgres(fecha1);
                    String fecha2=request.getParameter("FechaFinal");
                    fecha2=Util.formatoFechaPostgres(fecha2);
                    comentario=model.PlanillasSvc.searchListOC(tipo,distrito,fecha1,fecha2);
                    next= "/datosplanilla/MostrarListadoOC.jsp?comentario="+comentario+"&tipo="+String.valueOf(tipo)+"&f1="+fecha1+"&f2="+fecha2;
                }
                
                // LLamamos la pagina JSP
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response);
                
            }
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        
    }// end run
    
}// en accion
