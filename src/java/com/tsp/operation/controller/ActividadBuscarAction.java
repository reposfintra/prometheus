/*************************************************************************
 * Nombre ......................ActividadBuscarAction.java               *
 * Descripci�n..................Clase Action para anular actividad       *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................26 de agosto de 2005, 05:05 PM           * 
 * Versi�n......................1.0                                      * 
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/ 
 
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIOGENES
 */
public class ActividadBuscarAction extends Action{
    
    /** Creates a new instance of ActividadBuscarAction */
    public ActividadBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/actividad/actividad.jsp?mensaje=Modificar";
        String cod = request.getParameter("cod");
        try{
            model.actividadSvc.buscarActividad(cod,usuario.getCia());
            model.tablaGenService.buscarRegistros("EMAIL_GRUP");
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
