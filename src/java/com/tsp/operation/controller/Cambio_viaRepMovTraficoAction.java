/*********************************************************
 * Nombre clase: Cambio_viaRepMovTraficoAction.java
 * Descripci�n: Accion para cambiar una via del reporte
 *              movimeinto de trafico.
 * Autor: Jose de la rosa
 * Fecha: 9 de marzo de 2006, 11:45 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;

public class Cambio_viaRepMovTraficoAction  extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of Cambio_viaRepMovTraficoAction */
    public Cambio_viaRepMovTraficoAction () {
    }
    
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        String distrito = (String)(session.getAttribute ("Distrito"));
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String base = usuario.getBase ();
        String ruta = "";
        String next="/"+request.getParameter ("carpeta")+"/"+request.getParameter ("pagina");
        String opcion = request.getParameter ("opcion");
        String numpla = request.getParameter ("numpla").toUpperCase ();
        String planilla = numpla;
        String ruta_pla = (request.getParameter ("ruta")!=null)?request.getParameter ("origen")+request.getParameter ("destino")+request.getParameter ("ruta"):request.getParameter ("rutas");
        String rut = (request.getParameter ("ruta")!=null)?request.getParameter ("ruta"):request.getParameter ("rutas").substring (4,request.getParameter ("rutas").length ());
        
        logger.info("? Mruta: " + rut);
        
        try{
            next += "?Mruta="+ rut;
            ruta = "?Mruta="+ rut;
            if(opcion.equals ("1")){
                boolean esdm = model.rmtService.esDespachoManual (numpla);
                DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT ();
                String ubicacion = request.getParameter ("ubicacion");
                String zona = model.rmtService.getZona (ubicacion);
                String programa = request.getParameter ("c_programa")!=null?request.getParameter ("c_programa").toUpperCase ():"";
                String tipo = request.getParameter ("c_tipo")!=null?request.getParameter ("c_tipo").toUpperCase ():"";
                String descripcion = request.getParameter ("c_descripcion")!=null?request.getParameter ("c_descripcion"):"";
                String tipoubicacion = request.getParameter ("tubicacion")!=null?request.getParameter ("tubicacion"):"";
                String fechareporte = request.getParameter ("fechareporte")!=null?request.getParameter ("fechareporte"):"";
                Vector consultas = new Vector ();
                //agrego justificacion
                dp.setRutapla (request.getParameter ("origen")+request.getParameter ("destino")+rut);
                /*Justificacion jus = new Justificacion ();
                jus.setPrograma (programa);
                jus.setTipo (tipo);
                jus.setDescripcion (descripcion);
                jus.setDocumento (numpla);
                jus.setUsuario_modificacion (usuario.getLogin ());
                jus.setUsuario_creacion (usuario.getLogin ());
                jus.setBase (usuario.getBase ());
                jus.setDistrito (distrito);*/
                request.setAttribute ("msg", "Justificaci�n Agregada y Via modificada Exitosamente!");
              //  consultas.add ( model.justificacionService.ingresarJustificacion (jus) );
                // actualizo planilla
                consultas.add ( model.rmtService.cambiarViaPlanilla (esdm) );
                // valores para actualizar ultimo pesto de control
                RepMovTrafico rm = new RepMovTrafico ();
                rm.setReg_Status ("");
                rm.setNumpla (numpla);
                rm.setObservacion ( "Cambio Via "+model.rmtService.getNombreCiudad (ubicacion)+" "+fechareporte+" "+descripcion + dp.getClientes ());
                rm.setUbicacion_procedencia (ubicacion);
                rm.setTipo_procedencia (tipoubicacion);
                rm.setCreation_user (usuario.getLogin ());
                rm.setTipo_reporte ("");
                rm.setNombre_reporte ("");
                rm.setFechareporte (fechareporte);
                rm.setZona (zona);
                model.rmtService.setRMT (rm);
                consultas.add ( model.rmtService.actualizarUltPC () );
                model.rmtService.SearchLastCreatedReport ();
                RepMovTrafico rm1 = model.rmtService.getUltRMT ();
                if( rm1 != null ){
                    rm1.setCodUbicacion (ubicacion);
                    model.rmtService.getNextPC ();
                }
                consultas.add ( model.rmtService.actualizarIngresoTrafico () );
                model.despachoService.insertar (consultas);
            }
            else if(opcion.equals ("2")){
                next += "&c_programa=Reporte Movimiento Trafico&c_tipo=001";
                model.tablaGenService.buscarDatos ("TBLJUST", "001");
            }
            boolean esdm = model.rmtService.esDespachoManual (numpla);
            if(esdm){
                model.rmtService.BuscarPlanillaDM (numpla);
            }
            else{
                model.rmtService.BuscarPlanilla (numpla);
            }
            DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT ();
            if(dp!=null){
                if( opcion.equals ("0") || opcion.equals ("1") ){
                    dp.setRutapla (ruta_pla);
                    model.viaService.getViasTreeMaps (request.getParameter ("origen"), request.getParameter ("destino"));
                    
                    //******************************** AMATURANA 13.01.2006
                    logger.info("? origen: " + request.getParameter ("origen"));
                    logger.info("? destino: " + request.getParameter ("destino"));
                    logger.info("? vias: " + model.viaService.getCbxVias());
                    
                    dp = model.rmtService.getDatosPlanillaRMT ();
                    dp.setOripla( request.getParameter ("origen") );
                    dp.setDespla( request.getParameter ("destino") );
                    
                    //***********************************************************
                    
                    if(request.getParameter ("paso")==null){
                        model.ciudadService.searchTreMapCiudades ();
                        model.rmtService.BuscarPuestosControl ();
                        model.rmtService.SearchLastCreatedReport ();
                        if(model.rmtService.getUltRMT ()!=null) {
                            model.rmtService.getNextPC ();
                        }
                        model.rmtService.buscarPlanillas (dp.getNumpla (), dp.getPto_control_proxreporte ());
                    }
                    else{
                        if(model.viaService.getCbxVias ().size () > 0){
                            if(ruta_pla.length ()==4){
                                TreeMap tr = model.viaService.getCbxVias ();
                                Object key = tr.firstKey ();
                                String sec = (String)tr.get (key);
                                dp.setRutapla ( request.getParameter ("origen") + request.getParameter ("destino") + sec );
                            }
                            model.ciudadService.searchTreMapCiudades ();
                            model.rmtService.BuscarPuestosControl ();
                            model.rmtService.SearchLastCreatedReport ();
                            if(model.rmtService.getUltRMT ()!=null) {
                                model.rmtService.getNextPC ();
                            }
                            model.rmtService.buscarPlanillas (dp.getNumpla (), dp.getPto_control_proxreporte ());
                        }else{
                            model.ciudadService.searchTreMapCiudades ();
                            model.rmtService.BuscarPuestosControl ();
                        }
                    }
                }
                else if(opcion.equals ("2")){
                    
                    java.util.Date dates = new java.util.Date ();
                    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
                    String fecha_actual =s.format (dates);
                    
                    String ubicacion = request.getParameter ("ubicacion");
                    String zona = model.rmtService.getZona (ubicacion);
                    String fechareporte = request.getParameter ("c_dia")+" "+request.getParameter ("c_hora")+":00";
                    RepMovTrafico rm = new RepMovTrafico ();
                    rm.setReg_Status ("");
                    rm.setNumpla (numpla);
                    rm.setObservacion ( "" );
                    rm.setUbicacion_procedencia (ubicacion);
                    rm.setTipo_procedencia ("PC");
                    rm.setTipo_reporte ("");
                    rm.setNombre_reporte ("");
                    rm.setFechareporte (fechareporte);
                    rm.setZona (zona);
                    
                    RepMovTrafico ultimoreporte = model.rmtService.getLastCreatedReport ();
                    String fecha_antRep = ultimoreporte.getFechaReporte ();
                    int year=Integer.parseInt (fecha_antRep.substring (0,4));
                    int month=Integer.parseInt (fecha_antRep.substring (5,7))-1;
                    int date= Integer.parseInt (fecha_antRep.substring (8,10));
                    int hora=Integer.parseInt (fecha_antRep.substring (11,13));
                    int minuto=Integer.parseInt (fecha_antRep.substring (14,16));
                    
                    Calendar fecha_antRepC = Calendar.getInstance ();
                    fecha_antRepC.set (year,month,date,hora,minuto,0);
                    
                    double duracion =0;
                    RepMovTrafico ori = new RepMovTrafico ();
                    RepMovTrafico des = new RepMovTrafico ();
                    
                    if(model.rmtService.getUltRMT ()!=null) {
                        RepMovTrafico re = model.rmtService.getUltRMT ();
                        re.setCodUbicacion (ubicacion);
                        model.rmtService.getNextPC ();
                    }
                    
                    des = model.rmtService.getProxRMT ();
                    String nextpc = des.getUbicacion_procedencia ();
                    duracion = model.rmtService.getDuracionTramo (ubicacion ,nextpc);
                    String fecha_mas_tramo = Util.sumarHorasFecha (fecha_actual, duracion);
                    
                    year=Integer.parseInt (fecha_mas_tramo.substring (0,4));
                    month=Integer.parseInt (fecha_mas_tramo.substring (5,7))-1;
                    date= Integer.parseInt (fecha_mas_tramo.substring (8,10));
                    hora=Integer.parseInt (fecha_mas_tramo.substring (11,13));
                    minuto=Integer.parseInt (fecha_mas_tramo.substring (14,16));
                    
                    Calendar fecha_desRepC = Calendar.getInstance ();
                    fecha_desRepC.set (year,month,date,hora,minuto,0);
                    
                    year=Integer.parseInt (fechareporte.substring (0,4));
                    month=Integer.parseInt (fechareporte.substring (5,7))-1;
                    date= Integer.parseInt (fechareporte.substring (8,10));
                    hora=Integer.parseInt (fechareporte.substring (11,13));
                    minuto=Integer.parseInt (fechareporte.substring (14,16));
                    
                    Calendar fecha_rep= Calendar.getInstance ();
                    fecha_rep.set (year,month,date,hora,minuto,0);
                    // se compara si esta dentro del rango de una hora de la fecha de proximo reporte
                    if(fecha_rep.compareTo (fecha_antRepC) > 0 ){
                        next += "&fechareporte="+fechareporte; 
                    }
                    else{
                        request.setAttribute ("msg", "Ha ingresado una fecha menor a la fecha del �ltimo reporte");
                        String esCambioRuta = request.getParameter( "esCambioRuta" );
                        esCambioRuta = (esCambioRuta != null )? esCambioRuta : "";
                        if( esCambioRuta.equalsIgnoreCase( "si" ) ){
                            next = "/jsp/trafico/Movimiento_trafico/CambioRutaMovtraf.jsp"+ruta+"&numpla="+numpla+"&origen="+request.getParameter ("origen")+"&destino="+request.getParameter ("destino");
                        }else{
                            next = "/jsp/trafico/Movimiento_trafico/CambioViaMovtraf.jsp"+ruta+"&numpla="+numpla+"&origen="+request.getParameter ("origen")+"&destino="+request.getParameter ("destino");
                        }
                        model.viaService.getViasTreeMaps (request.getParameter ("origen"), request.getParameter ("destino"));
                        dp.setRutapla (ruta_pla);
                        model.rmtService.BuscarPuestosControl ();
                        model.rmtService.SearchLastCreatedReport ();
                        if(model.rmtService.getUltRMT ()!=null) {
                            model.rmtService.getNextPC ();
                        }
                    }
                }
                else if(opcion.equals ("3")){
                    Vector consultas = new Vector ();
                    //validamos si se debe o no agregar el reporte.
                    Vector caravana = model.rmtService.getCaravana ();
                    model.rmtService.setCaravana (new Vector ());
                    caravana.add (numpla);
                    if(model.rmtService.getPlanillasReportes ()!=null){
                        for(int i = 0; i<model.rmtService.getPlanillasReportes ().size ();i++){
                            caravana.add ((String)model.rmtService.getPlanillasReportes ().elementAt (i));
                        }
                    }
                    for(int i = 0; i<caravana.size ();i++){
                        String texto=(String) caravana.elementAt (i);
                        
                        String vecT[] = texto.split (",");
                        numpla = vecT[0];
                        
                        boolean ingresar=true;
                        String mensaje = request.getParameter ("mensaje");
                        String ubicacion = request.getParameter ("ubicacion");
                        String tipoubicacion = request.getParameter ("tubicacion");
                        String trep = request.getParameter ("tiporep");
                        String fechareporte     = request.getParameter ("c_dia")+" "+request.getParameter ("c_hora");
                        String fechaproxllegada = request.getParameter ("p_dia")+" "+request.getParameter ("p_hora"); // fecha de reinicio
                        String causas=request.getParameter ("causas")!=null?request.getParameter ("causas"):"";
                        String vecCausa[] = causas.split ("/");
                        String zona = model.rmtService.getZona (ubicacion);
                        String clasificacion = request.getParameter ("clasificacion")!=null?request.getParameter ("clasificacion"):"";
                        String vecCla[] = clasificacion.split ("/");
                        if(mensaje.equalsIgnoreCase ("insert")){
                            if(ingresar){
                                if( !model.rmtService.esFechaAnteriorAExistente (fechareporte) ){
                                    
                                    java.util.Date date = new java.util.Date ();
                                    java.text.SimpleDateFormat s = new java.text.SimpleDateFormat ("yyyy-MM-dd HH:mm");
                                    String fecha_hoy = s.format (date);
                                    fecha_hoy = fecha_hoy+":00";
                                    if( !model.rmtService.existePlanillaConFecha ( numpla, fecha_hoy ) ){
                                        RepMovTrafico rm = new RepMovTrafico ();
                                        rm.setReg_Status ("");
                                        rm.setDstrct (distrito);
                                        rm.setNumpla (numpla);
                                        rm.setTipo_reporte (trep);
                                        rm.setCreation_user (usuario.getLogin ());
                                        rm.setBase (base);
                                        rm.setUbicacion_procedencia (dp.getNom_ciudad ().equals ("")?dp.getPto_control_ultreporte ():dp.getCod_ciudad ());
                                        rm.setObservacion (request.getParameter ("obaut")+" "+request.getParameter ("observacion")+" "+dp.getClientes ());
                                        rm.setFechareporte ( fecha_hoy );
                                        rm.setFec_rep_pla ( fechareporte );
                                        rm.setZona (zona);
                                        rm.setCreation_date ( "now()" );
                                        rm.setLast_update ( "now()" );
                                        rm.setTipo_procedencia (tipoubicacion);
                                        rm.setTipo_reporte (trep);
                                        rm.setNombre_reporte (trep);
                                        rm.setCausa (vecCausa.length>0?vecCausa[0]:"");
                                        rm.setClasificacion (vecCla.length>0?vecCla[0]:"");
                                        rm.setCodUbicacion (ubicacion);
                                        consultas.add (model.rmtService.addRMT (rm));
                                        rm.setUbicacion_procedencia (ubicacion);
                                        zona = model.rmtService.getZona ( dp.getPto_control_proxreporte () );                                        
                                        consultas.add (model.rmtService.actualizacionProxFechaRep (fechareporte, numpla, request.getParameter ("obaut")+" "+request.getParameter ("observacion")+" "+dp.getClientes (), zona, trep ) );//David Pina
                                        String actual = Util.fechaActualTIMESTAMP ();
                                        if( model.tblgensvc.existeCodigo( "REP", "DETENCION", trep.toUpperCase() ) ){//David Pina
                                            model.tblgensvc.buscarLista ("REP", "DETENCION_TRAFICO");//David Pina
                                        }else{
                                            model.tblgensvc.buscarLista ("REP", "REP_TRA");
                                        }
                                        next="/jsp/trafico/Movimiento_trafico/movtrafing.jsp?tiporeporte=EVIA&tubicacion=PC&c_dia="+actual.substring (0,10)+"&c_hora="+actual.substring (11,16)+"&tubicacion="+tipoubicacion+"&observacion=&mensaje=Nuevo reporte de movimiento de tr�fico ingresado";
                                    }
                                    else{
                                        String actual = Util.fechaActualTIMESTAMP ();
                                        next="/jsp/trafico/Movimiento_trafico/movtrafing.jsp?tiporeporte=EVIA&tubicacion=PC&c_dia="+actual.substring (0,10)+"&c_hora="+actual.substring (11,16)+"&tubicacion="+tipoubicacion+"&observacion=&mensaje=Ya existe un registro para esta hora en la planilla "+numpla+".";
                                    }
                                }
                                else{
                                    String actual = Util.fechaActualTIMESTAMP ();
                                    next="/jsp/trafico/Movimiento_trafico/movtrafing.jsp?tiporeporte=EVIA&tubicacion=PC&c_dia="+actual.substring (0,10)+"&c_hora="+actual.substring (11,16)+"&tubicacion="+tipoubicacion+"&observacion=&mensaje=Imposible agregar un reporte con fecha anterior al ultimo reporte de trafico de la planilla!";
                                }
                            }
                        }
                    }
                    if(consultas.size ()>0)
                        model.despachoService.insertar (consultas);
                    boolean desp = model.rmtService.esDespachoManual (planilla);
                    if(desp){
                        model.rmtService.BuscarPlanillaDM (planilla);
                    }
                    else{
                        model.rmtService.BuscarPlanilla (planilla);
                    }
                }
                else{
                    model.rmtService.BuscarReporteMovTraf (planilla);
                    model.tblgensvc.buscarLista ("REP", "DETENCION");
                    model.rmtService.SearchLastCreatedReport ();
                    if(model.rmtService.getUltRMT ()!=null) {
                        model.rmtService.getNextPC ();
                    }
                    model.rmtService.BuscarPuestosControl();
                    model.rmtService.getNextPC();
                    model.rmtService.BuscarTodasCiudades ();
                    model.rmtService.buscarPlanillas (dp.getNumpla (), dp.getPto_control_proxreporte ());
                }
            }
            
            logger.info("? next:" + next);
        }
        
        catch(Exception ex){
            ex.printStackTrace ();
            throw new ServletException (ex.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
