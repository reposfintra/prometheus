/*******************************************************************
 * Nombre clase: PlacaBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  jdelarosa
 */
public class PlacaBuscarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public PlacaBuscarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        
        String distrito = (String) session.getAttribute("Distrito");
        
        String idplaca = request.getParameter("idplaca").toUpperCase();
        String tipo = request.getParameter("grupo_equipo")!=null? request.getParameter("grupo_equipo").toUpperCase() : "";
        
        try{
            
            
            if( request.getParameter("buscar_sin_tipo") ==  null ){
                
                if(request.getParameter("grupo_equipo").equals("T"))
                    next = "/placas/trailerUpdate.jsp";
                else if(request.getParameter("grupo_equipo").equals("C"))
                    next = "/placas/placasUpdate.jsp";
                else
                    next = "/placas/placaUpdate.jsp";
                
                model.placaService.buscaPlacaTipo(idplaca, tipo);
                Placa placa = model.placaService.getPlaca();
                model.vetoSvc.buscarCausaVeto(placa.getPlaca(), "P");
                model.placaService.setPlaca_bean( placa );
                model.tablaGenService.buscarTipo( request.getParameter("grupo_equipo") );
                model.tablaGenService.buscarClase( request.getParameter("grupo_equipo") );
                model.tablaGenService.buscarRines();
                model.tablaGenService.buscarModelo();
                model.tablaGenService.buscarPiso();
                model.tablaGenService.buscarTitulo();
                model.tablaGenService.buscarClasificacion();
                model.tablaGenService.buscarGrupos();
                model.tablaGenService.buscarGruposEq();
                if ( placa == null){
                    next+="?msg=No se encontro ningun resultado";
                }else{
                    request.setAttribute("placa", placa);
                    request.setAttribute("tenia_gps", placa.getTiene_gps());
                }
                
            }else{
                
                Placa p;
                if( idplaca != null ){
                    
                    model.placaService.buscarPlaca(idplaca);
                    p = model.placaService.getPlaca();                    
                    
                    if ( p == null){
                        next+="/placas/placaBuscarSinTipo.jsp?msg=No se encontro ningun resultado";
                    }else{
                        
                        model.placaService.setPlaca_bean( p );
                        request.setAttribute("placa", p);
                        request.setAttribute("tenia_gps", p.getTiene_gps());
                        
                        
                        model.tablaGenService.buscarGrupos();
                        if(p.getGrupo_equipo().equals("T")){
                            next = "/placas/trailerUpdate.jsp";
                            model.tablaGenService.buscarClasificacion();
                        }
                        else if(p.getGrupo_equipo().equals("C"))
                            next = "/placas/placasUpdate.jsp";
                        else
                            next = "/placas/placaUpdate.jsp";
                        
                        model.vetoSvc.buscarCausaVeto(idplaca, "P");
                        model.tablaGenService.buscarRegistrosOrdenadosByDesc("GPSOPER");
                        model.tablaGenService.setDatos2( model.tablaGenService.obtenerTablas() );
                        model.tablaGenService.buscarTipo( p.getGrupo_equipo() );
                        model.tablaGenService.buscarClase( p.getGrupo_equipo() );
                        model.tablaGenService.buscarRines();
                        model.tablaGenService.buscarModelo();
                        model.tablaGenService.buscarPiso();
                        model.tablaGenService.buscarTitulo();
                        model.tablaGenService.buscarGruposEq();
                        
                       //modificacion 20/04/2007 Enrique De Lavalle
                        Proveedor prov = model.proveedorService.obtenerProveedorPorNit(p.getPropietario());
                        prov = (prov !=null)?prov:new Proveedor();
                        session.setAttribute("nitP", p.getPropietario());
                        session.setAttribute("nombreP", (prov.getC_payment_name()!= null)?prov.getC_payment_name():"");
                        
                                          
                    }
                    
                }
                
            }
            
            if( request.getParameter("updated")!=null ){
                next+="?msg=Datos modificados exitosamente...";
            }
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}
