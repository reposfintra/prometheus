/*
 * ReporteEgresoAction.java
 *
 * Created on 13 de septiembre de 2005, 10:43 AM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.*;
/**
 *
 * @author  Administrador
 */
public class ReporteEgresoAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ReporteEgresoAction */
    public ReporteEgresoAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String opcion                 = request.getParameter("Opcion");
            String agencia                = (request.getParameter("agencia")!=null)?request.getParameter("agencia"):"";
            String fechai                 = (request.getParameter("fechai")!=null)?request.getParameter("fechai"):"";
            String fechaf                 = (request.getParameter("fechaf")!=null)?request.getParameter("fechaf"):"";
            String usuario                = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):""; //Modificacion  04/01/2006
            String fecha                  = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";
            String b                      = (request.getParameter("b")!=null)?request.getParameter("b"):"";
            String s                      = (request.getParameter("s")!=null)?request.getParameter("s"):"";
            
            String banco                  = (request.getParameter("banck")!=null)?request.getParameter("banck"):"";
            String sucuralbanco           = (request.getParameter("sucuralbanco")!=null)?request.getParameter("sucuralbanco"):"";
            
            
            String proveedor           = (request.getParameter("proveedor")!=null)?request.getParameter("proveedor"):"";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            
            String Mensaje = "";
            String next = "/RelacionEgreso/ReporteEgreso.jsp";
            
            usuario = (usuario.equals(""))? "%":usuario;
            
            logger.info("?opcion: " + opcion);
            
            if(opcion.equals("Generar")){
                ///////////////////////////////// Andr�s Maturana
                String bancosel = request.getParameter("banco")!=null ? request.getParameter("banco") : "";
                String sucursel = request.getParameter("sucursal")!=null ? request.getParameter("sucursal") : "";
                
                model.ReporteEgresoSvc.LISTREPORTEGRESO(user.getDstrct(), agencia, fechai, fechaf, usuario, bancosel, sucursel, proveedor );
                //model.ReporteEgresoSvc.LISTBANCO(agencia, fechai, fechaf, bancosel, sucursel  );
                model.ReporteEgresoSvc.listBancoPorAgencia(user.getDstrct(), agencia, fechai, fechaf, usuario, bancosel, sucursel, proveedor );//AMATURANA 1.03.2007
                
                List	ListaBancos = model.ReporteEgresoSvc.getListBancos(); 
                logger.info("?bancos  encontrados: " + ListaBancos.size());
                
                Session.setAttribute("fechaInicio", fechai );
                Session.setAttribute("fechaFin",    fechaf );
                
            }
            
            
       
            
            if(opcion.equals("BuscarS")){
                next = "/jsp/cxpagar/ModificacionCheques/Modificacion_cheques.jsp?banco="+banco;
                
            }
            
            
            if(opcion.equals("Guardar")){
                String fi = (String)Session.getAttribute("fechaInicio");
                String ff = (String)Session.getAttribute("fechaFin");
                ReporteEgresoXLS hilo = new ReporteEgresoXLS();
                hilo.start(model.ReporteEgresoSvc.getListRE(), model.ReporteEgresoSvc.getListBancos(), fi, ff, user.getLogin());
                //    model.ReporteEgresoSvc.LISTBANCO(agencia,fechai,fechaf);
                //  model.ReporteEgresoSvc.LISTREPORTEGRESO(agencia,fechai,fechaf);
            }
            
            
            if(opcion.equals("GenerarChk")){ //Modificacion  04/01/2006
                model.ReporteEgresoSvc.LISTCHK(fechai,banco,sucuralbanco);
                if( model.ReporteEgresoSvc.getListRE() == null || model.ReporteEgresoSvc.getListRE().size() == 0  )
                    Mensaje = "No hay cheques con estos parametros de busqueda";
                next = "/jsp/cxpagar/ModificacionCheques/Modificacion_cheques.jsp?Mensaje="+Mensaje+"&fecha="+fecha+"&banco="+banco+"&sucursal="+sucuralbanco;
                
                Session.setAttribute("fechaInicio", fechai );
                Session.setAttribute("fechaFin",    fechaf );
                
            }
            
            
            if(opcion.equals("Actualizar")){
                java.util.Enumeration enum1;
                String parametro = "";
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                List listaChk = model.ReporteEgresoSvc.getListRE();
                
                /*Armo lista de valores de lista original*/
                Iterator it2 = listaChk.iterator();
                String listaO = "";
                while(it2.hasNext()){
                    ReporteEgreso re = (ReporteEgreso) it2.next();
                    listaO+= "'"+re.getNumeroChk()+"'"+",";
                }
                listaO = listaO.substring(0,listaO.length() - 1);
                
                
                String list = ""; //Lista de valores temporales
                String cheques = "";
                String fechas = "";
                boolean aux = false;
                
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    if( parametro.length() > 5 ){
                        if(parametro.substring(0,5).equals("campo")){
                            if(!request.getParameter(parametro).equals("")){
                                String fin = (String)Session.getAttribute("fechaInicio");
                                int numero =Integer.parseInt( parametro.substring(5));
                                ReporteEgreso chk = (ReporteEgreso)listaChk.get(numero-1);
                                String valor = request.getParameter(parametro);
                                if( !chk.getNumeroChk().equals(valor) ){
                                    if( !model.ReporteEgresoSvc.SEARCH(valor, listaO,b, s )){
                                        
                                        
                                        
                                        model.ReporteEgresoSvc.UPDATE(valor, user.getLogin(), chk.getNumeroChk(), b, s, (String)Session.getAttribute("Distrito"));
                                        
                                        ////System.out.println("NDOC  " + valor + "DOC  " + chk.getNumeroChk());
                                        
                                        
                                        //    model.ReporteEgresoSvc.UPDATEREGA(valor, user.getLogin(), chk.getNumeroChk(), b, s, (String)Session.getAttribute("Distrito"));
                                        Mensaje = "Los Cheques se han modificado";
                                        model.ReporteEgresoSvc.LISTCHK(fecha,b,s);
                                        
                                        
                                        
                                    }
                                    else{
                                        aux = true;
                                        list += valor+"/"+chk.getNumeroChk()+",";
                                        chk.setNuevoNChk(valor);
                                        ReporteEgreso re = model.ReporteEgresoSvc.BUSCAR(valor);
                                        cheques += valor+",";
                                        fechas += re.getFechaChk()+",";
                                        
                                    }
                                }
                                
                            }
                        }
                    }
                }
                
                
                
                
                /* SEGUNDA ACTUALIZACION */
                if(  !aux  ){
                    enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                    parametro = "";
                    model.ReporteEgresoSvc.LISTCHK(fecha,b,s);
                    List listaCH = model.ReporteEgresoSvc.getListRE();
                    
                    while (enum1.hasMoreElements()) {
                        
                        parametro = (String) enum1.nextElement();
                        
                        if( parametro.length() > 5 ){
                            
                            if(parametro.substring(0,5).equals("campo")){
                                
                                if(!request.getParameter(parametro).equals("")){
                                    
                                    
                                    int numero =Integer.parseInt( parametro.substring(5));
                                    
                                    
                                    ReporteEgreso chk = (ReporteEgreso)listaCH.get(numero-1);
                                    
                                    
                                    
                                    String valor = request.getParameter(parametro);
                                    
                                    
                                    model.ReporteEgresoSvc.UPDATEREGA(valor, user.getLogin(), chk.getNumeroChk(), b, s, (String)Session.getAttribute("Distrito"));
                                    
                                    model.ReporteEgresoSvc.LISTCHK(fecha,b,s);
                                    
                                    
                                    
                                    
                                }
                            }
                        }
                    }
                    
                    
                    
                }
                
                
                
                
                
                /*FIN SEGUNDA ACTUALIZACION */
                
                
                
                list = (list.length()>0)?list.substring(0,list.length() - 1 ):"";
                if( cheques.length() > 0 && fechas.length() > 0  ){
                    cheques = cheques.substring(0,cheques.length() - 1 );
                    fechas = fechas.substring(0, fechas.length() - 1 );
                    if( cheques.split(",").length > 1 )
                        Mensaje = "Listado de cheques ya registrados";
                    else
                        Mensaje = "El cheque " + cheques + " ya se encuentra registrado con fecha de " + fechas;
                }
                next = "/jsp/cxpagar/ModificacionCheques/Modificacion_cheques.jsp?Mensaje="+Mensaje+"&fecha="+fecha+"&list="+list+"&cheques="+cheques+"&fechas="+fechas+"&banco="+b+"&sucursal="+s;
                
            }
            
            
            
            
            
                                    
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
        }
    }
    
}
