/*********************************************************************************
 * Nombre clase :      VerPlanillasManualesAction.java                        *
 * Descripcion :       Action del VerPlanillasManualesAction.java            *
 * Autor :             LREALES                                                   *
 * Fecha :             31 de marzo de 2006, 08:43 AM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class VerPlanillasManualesAction  extends Action {
    
    /** Creates a new instance of VerPlanillasManualesAction */
    public VerPlanillasManualesAction() {
    }    
    
    public void run() throws ServletException {
        //String next=request.getParameter("carpeta")+"/"+request.getParameter("pagina");                
        //this.dispatchRequest(next);
        
        String next = "/jsp/trafico/convertirPlanillaManual/verPlanillasManuales.jsp";
        
        try{
            
            model.convertirPlaManService.listaPlanillasManuales();

            int cont = model.convertirPlaManService.getVectorPlanilla().size();

            //if( cont == 0 )                
               // next = next + "?Mensaje=No Existen Planillas Manuales!";

            if ( cont > 0 )
                next = "/jsp/trafico/convertirPlanillaManual/verPlanillasManuales.jsp";
        
        } catch ( SQLException e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
                        
    }
    
}
