/********************************************************************
 *      Nombre Clase.................   ReporteVehiculosVaradosRefreshAction.java
 *      Descripci�n..................   Genera el reporte de oportunidad
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   07.01.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class RepDespachoManualAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of InformacionPlanillaAction */
    public RepDespachoManualAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        logger.info("REPORTE DE VIAJES MANUALES");
        
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String fechai = request.getParameter("FechaI");        
        String fechaf = request.getParameter("FechaF");        
        
        logger.info("FECHA INICIAL: " + fechai);
        logger.info("FECHA FINAL: " + fechaf);
        
        //Pr�xima vista
        String next = "/jsp/trafico/reportes/ReporteDespachoManual.jsp?msg=Se ha iniciado la generaci�n del reporte exitosamente.";
        
        try{
            RepDespachoManualTh hilo =  new RepDespachoManualTh();
            hilo.start(model, usuario, (String) session.getAttribute("Distrito"), fechai, fechaf);            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
