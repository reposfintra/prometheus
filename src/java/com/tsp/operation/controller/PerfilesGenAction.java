/*
 * Nombre        PerfilBuscarAction.java
 * Autor         Ing. Luis Eduardo Frieri
 * Fecha         23 enero 2007
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class PerfilesGenAction extends Action {
    
    /** Creates a new instance of PerfilBuscarAction */
    public PerfilesGenAction() {
    }
        
    public void run() throws javax.servlet.ServletException, InformationException{                  
        String next = "/jsp/masivo/PerfilesGen/PerfilesGen.jsp?mensaje=ok";
        
        try{ 
            
            String []Perfiles  = request.getParameterValues("Soportes");
            String []Tablas = request.getParameterValues("Clientes");
            
            if(request.getParameter("Opcion").equals("Grabar")) {
                if(Tablas==null){
                    model.PerfilesGenService.eliminarRelaciones(Perfiles);
                }else{
                     model.PerfilesGenService.addRelacionPerfilesGen(Perfiles, Tablas);
            
                }
           }  
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);                   
    }
}