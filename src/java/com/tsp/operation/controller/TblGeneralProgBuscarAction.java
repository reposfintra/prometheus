/******************************************************************
* Nombre ......................TblGeneralProgBuscarAction.java
* Descripci�n..................Clase Action para descuento de equipos
* Autor........................Ricardo Rosero
* Fecha........................23/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Ricardo Rosero
 */
public class TblGeneralProgBuscarAction extends Action{
    
    /** Creates a new instance of TblGeneralProgBuscarAction */
    public TblGeneralProgBuscarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/general/tablagen_prog/TblGeneralProgListar.jsp";
        try{
            String mensaje = request.getParameter("mensaje");
            if(mensaje!=null){

                if(mensaje.equalsIgnoreCase("listar")){
                    String codt = request.getParameter("table_type")!=null?request.getParameter("table_type"):"";
                    String cod = request.getParameter("table_code")!=null?request.getParameter("table_code"):"";
                    String program = request.getParameter ("program")!=null?request.getParameter ("program"):"";
                    model.tbl_GeneralProgService.listarTodos(codt,cod, program);
                    next = "/jsp/general/tablagen_prog/TblGeneralProgListar.jsp";
                }
                
                else if(mensaje.equalsIgnoreCase("listar2")){
                    String table_type = request.getParameter("table_type")!=null?request.getParameter("table_type"):"";
                    String table_code = request.getParameter("table_code")!=null?request.getParameter("table_code"):"";
                    String program = request.getParameter ("program")!=null?request.getParameter ("program"):"";
                    TblGeneralProg tmp = new TblGeneralProg ();
                    tmp.settable_code (table_code);
                    tmp.settable_type (table_type);
                    tmp.setprogram (program);
                    model.tbl_GeneralProgService.setTblGeneralProg (tmp);
                    next = "/jsp/general/tablagen_prog/item.jsp";
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
