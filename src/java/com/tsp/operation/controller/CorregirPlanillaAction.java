/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.CorregirPlanillasDAO;
import com.tsp.operation.model.DAOS.impl.CorregirPlanillaImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.AnticiposTerceros;
import com.tsp.operation.model.beans.ExtractoDetalleBeans;
import com.tsp.operation.model.beans.Usuario;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author egonzalez
 */
public class CorregirPlanillaAction extends Action {

    Usuario usuario = null;
    private final int BUSCAR_PLANILLAS = 1;
    private final int VER_EXTRACTO_DETALLE = 2;
    private final int UPDATE_EXTRACTO_DETALLE = 3;
    private final int BUSCAR_DIFERENCIAS_TABLAS = 4;
    private final int UPDATE_TABLAS_APT = 5;
    private final int EDIT_EXTRACTO = 6;
    private final int VER_ANTICIPOS = 7;
    private final int UPDATE_TABLAS_EXT = 8;
    private final int CARGAR_PLANILLAS_ANTICIPO = 9;
    private final int DETALLE_PLANILLA_ANTICIPO = 10;
    private final int REVERSAR_PLANILLAS_ANTICIPO = 11;
    private final int CARGAR_USUARIOS = 12;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            int opcion = request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : 0;

            switch (opcion) {
                case BUSCAR_PLANILLAS:
                    buscarPlanillas();
                    break;
                case VER_EXTRACTO_DETALLE:
                    buscarExtractoDetalle();
                    break;
                case UPDATE_EXTRACTO_DETALLE:
                    actualizarExtractoDetalle();
                    break;
                case BUSCAR_DIFERENCIAS_TABLAS:
                    buscarDiferenciaTablas();
                    break;
                case UPDATE_TABLAS_APT:
                    actualizarTablaAnticipo();
                    break;
                case EDIT_EXTRACTO:
                    cacularValorTotalExtracto();
                    break;                
                case VER_ANTICIPOS:
                    buscarAnticipos();
                    break;
                case UPDATE_TABLAS_EXT:
                    actualizarTablaExtracto();
                    break;
                case CARGAR_PLANILLAS_ANTICIPO:
                    cargarPlanillasAnticipo();
                    break;    
                case DETALLE_PLANILLA_ANTICIPO:
                    detallePlanillaAnticipo();
                    break;  
                case REVERSAR_PLANILLAS_ANTICIPO:
                    reversarPlanillasAnticipo();
                    break;
                case CARGAR_USUARIOS:
                    cargarUserSelect();
                    break;
            }
        } catch (Exception ex) {
            throw new ServletException(ex.getMessage());
        }
    }

    private void buscarPlanillas() {

        try {
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());
            ArrayList listPlanilla = dao.buscarPlanilla();
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listPlanilla) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CorregirPlanillaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void buscarExtractoDetalle() {

        try {
            int numero_operacion = request.getParameter("numero_operacion") != null ? Integer.parseInt(request.getParameter("numero_operacion")) : 0;
            String documento = request.getParameter("documento") != null ? request.getParameter("documento") : "";
            String clase = request.getParameter("clase") != null ? request.getParameter("clase") : "";

            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());
            ArrayList listPlanilla = dao.buscarExtractoDetalle(numero_operacion, documento, clase);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listPlanilla) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CorregirPlanillaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cacularValorTotalExtracto() throws Exception {

        double valor = Double.parseDouble(request.getParameter("valor"));
        double retefuente = Double.parseDouble(request.getParameter("retefuente"));
        double reteica = Double.parseDouble(request.getParameter("reteica"));
        double impuestos = Double.parseDouble(request.getParameter("impuestos"));

        ExtractoDetalleBeans detalleBeans = new ExtractoDetalleBeans();
        detalleBeans.setValor(valor);
        detalleBeans.setRetefuente(retefuente);
        detalleBeans.setReteica(reteica);
        detalleBeans.setImpuestos(reteica);

        Gson gson = new Gson();
        String json = gson.toJson(detalleBeans);
        this.printlnResponse(json, "application/json;");

    }
    

    private void actualizarExtractoDetalle() throws Exception {

        String json = request.getParameter("lista") != null ? request.getParameter("lista") : "";
        String clase = request.getParameter("clase") != null ? request.getParameter("clase") : "";
        if (!json.equals("")) {
            String respuestaJson = "{}";
            Gson gson = new Gson();
            Type tipoListaEmpleados;
            tipoListaEmpleados = new TypeToken<ArrayList<ExtractoDetalleBeans>>() {
            }.getType();

            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());
            ArrayList<ExtractoDetalleBeans> listExtracto = gson.fromJson(json, tipoListaEmpleados);
            TransaccionService tservice = new TransaccionService(usuario.getBd());
            try {

                tservice.crearStatement();
                for (ExtractoDetalleBeans detalleBeans : listExtracto) {
                    tservice.getSt().addBatch(dao.actualizarExtractoDetalle(detalleBeans, clase, usuario.getLogin()));
                }

                tservice.execute();
                respuestaJson = "{\"respuesta\":\"OK\"}";

            } catch (SQLException ex) {
                respuestaJson = "{\"error\":\"" + ex.getMessage() + "\"}";
                Logger.getLogger(CorregirPlanillaAction.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.printlnResponse(respuestaJson, "application/json;");
                tservice.closeAll();
            }
        }
    }
    
     private void buscarDiferenciaTablas() {

        try {
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());           
            String json=dao.buscarDiferenciaTablas();        
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CorregirPlanillaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
     
    private void buscarAnticipos() {

        try {
            String json="{}";
            int secuencia = request.getParameter("secuencia") != null ? Integer.parseInt(request.getParameter("secuencia")) : 0;
            if(secuencia!=0){
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());
            json= dao.buscarAnticipos(secuencia);
            }else{
              json="{\"error\":\"El numero de secuencia no es validad\"}";
            }
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CorregirPlanillaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private void actualizarTablaAnticipo() throws Exception {
        String json = "";
        try {
            String id = request.getParameter("identificador");
            double valor = Double.parseDouble(request.getParameter("valor"));
            double valor_for = Double.parseDouble(request.getParameter("valor_for"));

            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            tservice.getSt().addBatch(dao.actualizarAnticipo(id, valor, valor_for,usuario.getLogin()));
            tservice.execute();
            json = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(json, "application/json;");
            
        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
        }

    }
    
     private void actualizarTablaExtracto() throws Exception {
        String json = "";
        try {
            int id = Integer.parseInt(request.getParameter("secuencia"));
            double valor = Double.parseDouble(request.getParameter("extracto"));
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            tservice.getSt().addBatch(dao.actualizarExtracto(id, valor,usuario.getLogin()));
            tservice.execute();
            json = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(json, "application/json;");
            
        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
        }

    }

    private void cargarPlanillasAnticipo() {
        String user = request.getParameter("user");
        String fecha = request.getParameter("fecha");
        try {
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());
            ArrayList<AnticiposTerceros> listPlanilla = dao.buscarPlanillasAnticipo(user,fecha);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listPlanilla) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CorregirPlanillaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void detallePlanillaAnticipo() {
        String user = request.getParameter("user");
        String fecha_transf = request.getParameter("fecha_transf");
        try {
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());
            ArrayList<AnticiposTerceros> listPlanilla = dao.detallePlanillaAnticipo(user,fecha_transf);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listPlanilla) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CorregirPlanillaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void reversarPlanillasAnticipo() {
        String json = "";
        try {
            String user = request.getParameter("user");
            String fecha = request.getParameter("fecha");
            
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            tservice.getSt().addBatch(dao.reversarPlanillasAnticipo(user,fecha,usuario.getLogin()));
            tservice.execute();
            json = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(json, "application/json;");
            
        } catch (Exception e) {
            json = "{\"error\":\"" + e.getMessage() + "\"}";
        }
    }
    
     /**
     *
     */
    private void cargarUserSelect() {
        try {
            CorregirPlanillasDAO dao = new CorregirPlanillaImpl(usuario.getBd());
            String json = dao.cargarComboUsuarios();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

}
