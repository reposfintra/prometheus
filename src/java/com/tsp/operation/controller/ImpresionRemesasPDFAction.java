/*****************************************************************************
 * Nombre clase :                   ImpresionRemesasPDFAction.java           *
 * Descripcion :                    Clase que maneja los eventos             *
 *                                  relacionados con el programa de          *
 *                                  Impresion de Remesas                     *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          6 de enero de 2006, 04:32 PM             *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.*;
import java.util.*;

public class ImpresionRemesasPDFAction extends Action {
    
    /** Creates a new instance of ImpresionRemesasPDFAction */
    public ImpresionRemesasPDFAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try{
            
            String next = "";
            HttpSession session = request.getSession();
            DatosRemesaImpPDF datos;
            DatosPlanillaImpPDF aux;
            String Mensaje = "";
            String numrem = (request.getParameter("numrem")!=null)?request.getParameter("numrem").toUpperCase():"";
            String numpla = (request.getParameter("numpla")!=null)?request.getParameter("numpla").toUpperCase():"";
            String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
            String []LOV  = request.getParameterValues("Impresa");
            
            
            model.remesaService.buscaRemesa(numrem);
            
            List lista = new LinkedList();
            List listaaux = new LinkedList();
            
            
            if(opcion.equals("multiple")){
                for( int i = 0 ; i < LOV.length ; i++ ){
                    String temp = LOV[i];
                    
                    numrem = temp.split("/")[0];
                    
                    if( !model.remesaImpresionSvc.SearchPlanilla_Pendiente(numrem)){
                        
                        model.remesaImpresionSvc.DatosRemesa(numrem);
                        
                        datos = model.remesaImpresionSvc.getDato();
                        model.RemesaSvc.Actualizar_Remesa_Impresa(numrem);
                        
                        Vector copias = new Vector();
                        copias.add("FACTURA ORIGINAL/FACTURA COPIA");
                        copias.add("CLIENTE/CONTABILIDAD");
                        
                        
                        datos.setCopias(copias);
                        
                        lista.add(datos);
                        
                        //next = "/impresiones_pdf/ImpresionRemesaPDF.jsp";
                        next = "/impresiones_pdf/PDFJoin.jsp";
                        // next="/controller?estado=Menu&accion=ImprimirCarbon&total=0&opcion=no&planilla="+numpla;
                        
                    }
                    else{
                        Mensaje =   " Esta Remesa tiene asociada una planilla que requiere \n   "+
                        " una autorización de la administración de riesgos, \n      "+
                        " consulte el correo para su obtener su autorización        ";
                        next = "/impresiones_pdf/buscarremesaPDF.jsp?Mensaje="+Mensaje;
                    }
                }
                /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                String ruta = application.getRealPath("/");
                File xslt = new File(ruta + "Templates/p1.xsl");
                File pdf = new File(ruta + "pdf/p1.pdf");
                
                session.setAttribute("ArchivoXSLT", xslt );
                session.setAttribute("ArchivoPDF", pdf );
                session.setAttribute("datosRemesa", lista);
                
                
                
            }//Fin if ( multiple )
            
            if( opcion.equals("sencillo") ){
                for( int j = 0 ; j < LOV.length ; j++ ){
                    
                    if( LOV[j].indexOf("/") != -1 ){
                        String temp = LOV[j].split("/")[0];
                        numrem = temp;
                    }
                    else
                        numrem = LOV[j];
                    
                    
                    model.remesaService.buscaRemesa(numrem);
                    if( model.remesaService.getRemesa() != null ){
                        
                        if( !model.remesaImpresionSvc.SearchPlanilla_Pendiente(numrem)){
                            model.remesaImpresionSvc.DatosRemesa(numrem);
                            
                            datos = model.remesaImpresionSvc.getDato();
                            
                            model.RemesaSvc.Actualizar_Remesa_Impresa(numrem);
                            Vector copias = new Vector();
                            copias.add("FACTURA ORIGINAL/FACTURA COPIA");
                            copias.add("CLIENTE/CONTABILIDAD");
                            
                            datos.setCopias(copias);
                            
                            lista.add(datos);
                            
                            /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                            String ruta = application.getRealPath("/");
                            File xslt = new File(ruta + "Templates/p1.xsl");
                            File pdf = new File(ruta + "pdf/p1.pdf");
                            
                            session.setAttribute("ArchivoXSLT", xslt );
                            session.setAttribute("ArchivoPDF", pdf );
                            session.setAttribute("datosRemesa", lista);
                            
                            
                            next = "/impresiones_pdf/ImpresionRemesaPDF.jsp";
                        }
                        else{
                            Mensaje =   " Esta Remesa tiene asociada una planilla que requiere \n   "+
                            " una autorización de la administración de riesgos, \n      "+
                            " consulte el correo para su obtener su autorización        ";
                            next = "/impresiones_pdf/buscarremesaPDF.jsp?Mensaje="+Mensaje;
                        }
                    }
                    
                    else{
                        Mensaje = "La Remesa no existe";
                        next = "/impresiones_pdf/buscarremesaPDF.jsp?Mensaje="+Mensaje;
                    }
                }
            }//Fin if ( Sencillo )
            
            /*Implementacion Codigo*/
            if( opcion.equals("Buscar") ){
                
                if( model.remesaService.getRemesa() != null ){
                    
                    if( !model.remesaImpresionSvc.SearchPlanilla_Pendiente(numrem)){
                        
                        /*Impresion Varias*/
                        model.remesaImpresionSvc.ListaPlanillaAsociadas(numrem);
                        if( model.remesaImpresionSvc.getList().size() <= 1 ){
                            //model.remesaImpresionSvc.DatosRemesa(numrem);
                            List Listado = model.remesaImpresionSvc.getList();
                            
                            datos = (DatosRemesaImpPDF)Listado.get(0);//Primer Objeto
                            
                            
                            Vector copias = new Vector();
                            copias.add("FACTURA ORIGINAL/FACTURA COPIA");
                            copias.add("CLIENTE/CONTABILIDAD");
                            
                            
                            datos.setCopias(copias);
                            
                            lista.add(datos);
                            
                            model.RemesaSvc.Actualizar_Remesa_Impresa(numrem);
                            
                            /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                            String ruta = application.getRealPath("/");
                            File xslt = new File(ruta + "Templates/p1.xsl");
                            File pdf = new File(ruta + "pdf/p1.pdf");
                            
                            session.setAttribute("ArchivoXSLT", xslt );
                            session.setAttribute("ArchivoPDF", pdf );
                            session.setAttribute("datosRemesa", lista);
                            
                            
                            next = "/impresiones_pdf/ImpresionRemesaPDF.jsp";
                        }
                        /*Presentar en pantalla lista de planillas asociadas*/
                        else{
                            /*Busqueda de Planillas asociadas*/
                            if( numpla.equals("") ){
                                next = "/impresiones_pdf/buscarremesaPDF.jsp?numrem="+numrem;
                            }
                            /*Impresion de Remesas con planilla referenciada*/
                            else{
                                model.remesaImpresionSvc.DatosRemesaAsociadas(numpla, numrem);
                                datos = model.remesaImpresionSvc.getDato();
                                
                                Vector copias = new Vector();
                                copias.add("FACTURA ORIGINAL/FACTURA COPIA");
                                copias.add("CLIENTE/CONTABILIDAD");
                                
                                
                                datos.setCopias(copias);
                                
                                lista.add(datos);
                                
                                model.RemesaSvc.Actualizar_Remesa_Impresa(numrem);
                                
                                /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                                String ruta = application.getRealPath("/");
                                File xslt = new File(ruta + "Templates/p1.xsl");
                                File pdf = new File(ruta + "pdf/p1.pdf");
                                
                                session.setAttribute("ArchivoXSLT", xslt );
                                session.setAttribute("ArchivoPDF", pdf );
                                session.setAttribute("datosRemesa", lista);
                                
                                
                                next = "/impresiones_pdf/ImpresionRemesaPDF.jsp";
                                
                                model.remesaImpresionSvc.ReiniciarLista();
                            }
                        }
                        
                    }
                    else{
                        Mensaje =   " Esta Remesa tiene asociada una planilla que requiere \n   "+
                        " una autorización de la administración de riesgos, \n      "+
                        " consulte el correo para su obtener su autorización        ";
                        next = "/impresiones_pdf/buscarremesaPDF.jsp?Mensaje="+Mensaje;
                    }
                }
                
                else{
                    Mensaje = "La Remesa no existe";
                    next = "/impresiones_pdf/buscarremesaPDF.jsp?Mensaje="+Mensaje;
                }
            }//Fin Buscar
            
            
            
            if( opcion.equals("conremesas") ){
                listaaux = (List)session.getAttribute("datosPlanilla");
                Iterator it2 = listaaux.iterator();
                while(it2.hasNext()){
                    aux = (DatosPlanillaImpPDF)it2.next();
                    List listaRemesa  = aux.getRemesas();
                    if( listaRemesa.size() > 0  ){
                        DatosRemesaImp d = (DatosRemesaImp)listaRemesa.get(0);
                        
                        numrem = d.getNumeroRemesa();
                        
                        model.remesaImpresionSvc.DatosRemesa(numrem);
                        
                        datos = model.remesaImpresionSvc.getDato();
                        
                        model.RemesaSvc.Actualizar_Remesa_Impresa(numrem);
                        
                        Vector copias = new Vector();
                        copias.add("FACTURA ORIGINAL/FACTURA COPIA");
                        copias.add("CLIENTE/CONTABILIDAD");
                        
                        datos.setCopias(copias);
                        
                        lista.add(datos);
                    }
                }
                
                /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                String ruta = application.getRealPath("/");
                File xslt = new File(ruta + "Templates/p1.xsl");
                File pdf = new File(ruta + "pdf/p1.pdf");
                
                session.setAttribute("ArchivoXSLT", xslt );
                session.setAttribute("ArchivoPDF", pdf );
                session.setAttribute("datosRemesa", lista);
                
                next = "/impresiones_pdf/ImpresionRemesaPDF.jsp";
                
                
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
        
    }
    
}
