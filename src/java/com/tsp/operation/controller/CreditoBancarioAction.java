package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.exceptions.InformationException;
import com.tsp.finanzas.contab.model.beans.ComprobanteFacturas;
import com.tsp.finanzas.contab.model.beans.Cuentas;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Banco;
import com.tsp.operation.model.beans.Bancos;
import com.tsp.operation.model.beans.CXPItemDoc;
import com.tsp.operation.model.beans.CXP_Doc;
import com.tsp.operation.model.beans.Cheque;
import com.tsp.operation.model.beans.CreditoBancario;
import com.tsp.operation.model.beans.CreditoBancarioDetalle;
import com.tsp.operation.model.beans.CupoBanco;
import com.tsp.operation.model.beans.CupoCreditoBancario;
import com.tsp.operation.model.beans.FacturasCheques;
import com.tsp.operation.model.beans.LineaCredito;
import com.tsp.operation.model.beans.PuntosBasicosBanco;
import com.tsp.operation.model.beans.TablaGen;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.BancosService;
import com.tsp.operation.model.services.ChequeXFacturaService;
import com.tsp.operation.model.services.CreditosBancariosService;
import com.tsp.util.Util;
import com.tsp.util.UtilFinanzas;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 * Controlador para el programa de manejo de creditos bancarios
 * @author darrieta - geotech
 */
public class CreditoBancarioAction extends Action {

    HttpSession session;
    Usuario usuario;
    String next = "/jsp/creditoBancario/crearCreditoBancario.jsp";
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String opcion = request.getParameter("opcion");
            boolean redirect = true;
            String strRespuesta = "";
            
            if(opcion.equals("CARGAR_SUCURSALES")){
                strRespuesta = cargarSucursales();
                redirect = false;
            }else if(opcion.equals("CARGAR_PUNTOS_BASICOS")){
                strRespuesta = cargarPuntosBasicos();
                redirect = false;
            }else if(opcion.equals("CREAR")){
                crearCreditoBancario();
            }else if(opcion.equals("INSERTAR_PUNTOS_BASICOS")){
                strRespuesta = insertarPuntosBasicos();
                redirect = false;
            }else if(opcion.equals("EDITAR_PUNTOS_BASICOS")){
                editarPuntosBasicos();
                strRespuesta = "ok";
                redirect = false;
            }else if(opcion.equals("INSERTAR_CUPO")){
                strRespuesta = insertarCupo();
                redirect = false;
            }else if(opcion.equals("EDITAR_CUPO")){
                editarCupo();
                strRespuesta = "ok";
                redirect = false;
            }else if(opcion.equals("CAUSACION")){
                causarIntereses();
            }else if(opcion.equals("CARGAR_BANCOS_NACIONALIDAD")){
                strRespuesta = cargarBancosNacionalidad();
                redirect = false;
            }else if(opcion.equals("FILTRO_CREDITOS")){
                consultarCreditosFiltro();
            }else if(opcion.equals("CARGAR_DTF")){
                strRespuesta = cargarDTF();
                redirect = false;
            } else if (opcion.equals("CARGAR_DTF_CUOTA")) {
                String ultimaFecha="";
                String fecha = request.getParameter("fechaDTF");
                String periodo = request.getParameter("periodo");
                if (periodo.equals("0")) {
                    ultimaFecha = fecha;
                } else if (periodo.equals("12")) {
                    ultimaFecha = Util.fechaMasDias(fecha, -30);
                } else if (periodo.equals("6")) {
                    ultimaFecha = Util.fechaMasDias(fecha, -180);
                } else if (periodo.equals("4")) {
                    ultimaFecha = Util.fechaMasDias(fecha, -90);
                }
                
                strRespuesta = cargarDTFCupo(ultimaFecha);
                redirect = false;
            }else if(opcion.equals("GUARDAR_DETALLE_CREDITO")){
                try {
                    guardarMovimientoCredito();
                } catch (Exception exception) {
                    next = "/jsp/creditoBancario/consultaCreditoBancario.jsp?mensaje=Ha ocurrido un error. "+exception.getMessage();
                }
            }else if(opcion.equals("INSERTAR_LINEA_CREDITO")){
                strRespuesta = insertarLineaCredito();
                redirect = false;
            }else if(opcion.equals("EDITAR_LINEA_CREDITO")){
                editarLineaCredito();
                strRespuesta = "ok";
                redirect = false;
            }else if(opcion.equals("INSERTAR_CUPO_CREDITO")){
                strRespuesta = insertarCupoCredito();
                redirect = false;
            }else if(opcion.equals("EDITAR_CUPO_CREDITO")){
                editarCupoCredito();
                strRespuesta = "ok";
                redirect = false;
            }else if(opcion.equals("CONSULTAR_CUENTAS")){
                String cuenta;
                cuenta = (String) request.getParameter("cuenta") != null ? request.getParameter("cuenta") : "";
                cargarCuentas(cuenta);
                strRespuesta = "ok";
                redirect = false;
                
            }
            
            if(redirect){
                this.dispatchRequest(next);
            }else{
                if (!opcion.equals("CONSULTAR_CUENTAS")) {
                    response.setContentType("text/plain");
                    response.setHeader("Cache-Control", "no-store");
                    response.setDateHeader("Expires", 0);
                    response.getWriter().print(strRespuesta);
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error en CreditoBancarioAction: "+e.getMessage());
        }
               
    }
    
    /**
     * Carga las sucursales del banco seleccionado
     * @return html con las sucursales a agregar al select
     * @throws SQLException 
     */
    public String cargarSucursales() throws SQLException{
        String banco = request.getParameter("banco");
        model.servicioBanco.loadSusursales(banco);
        TreeMap<String,String> sucursales = model.servicioBanco.getSucursal();
        String options = "";
        
        for (Map.Entry<String,String> entry : sucursales.entrySet()) {
            options += "<option value='"+entry.getKey()+"'>"+entry.getValue()+"</option>";
        }
        
        return options;
    }
    
    
    /**
     * Carga las sucursales del banco seleccionado
     * @return html con las sucursales a agregar al select
     * @throws SQLException 
     */
    public String cargarPuntosBasicos() throws Exception{
        String banco = request.getParameter("banco");
        String linea = request.getParameter("linea");
        
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        double puntos_basicos = cbs.obtenerPuntosBasicos(banco, linea);
        
        return String.valueOf(puntos_basicos);
    }
    
    /**
     * Inserta un credito bancario y genera la cxp
     * @throws Exception 
     */
    public void crearCreditoBancario() throws Exception{
        next = "/jsp/creditoBancario/crearCreditoBancario.jsp";
        
        if(!model.cxpDocService.existeDoc(usuario.getDstrct(), request.getParameter("banco"), "FAP", request.getParameter("documento"))){
        
            double vlrCredito = Double.parseDouble(request.getParameter("vlrCredito").replaceAll(",", ""));
            double vlrCXP = vlrCredito;   
            String cp[] = request.getParameter("cupo").split(";");
            String linea[] = request.getParameter("linea").split(";");
            CreditoBancario credito = new CreditoBancario();
            credito.setRef_credito(linea[1]);
            credito.setNit_banco(request.getParameter("banco"));
            credito.setLinea_credito(linea[1]);
            credito.setDocumento(request.getParameter("documento"));
            credito.setTipo_dtf(request.getParameter("tipoDTF"));
            credito.setDtf(Double.parseDouble(request.getParameter("dtf")));
            credito.setPuntos_basicos(Double.parseDouble(request.getParameter("ptoBasicos")));
            credito.setVlr_credito(vlrCredito);
            credito.setFecha_inicial(request.getParameter("fechaInicial"));
            credito.setFecha_vencimiento(request.getParameter("fechaVencimiento"));
            credito.setCupo(cp[1]);
            //credito.setCupo(request.getParameter("cupo"));            
            credito.setCreation_user(usuario.getLogin());
            credito.setDstrct(usuario.getDstrct());
            credito.setHc(request.getParameter("hc"));
            credito.setCuenta(request.getParameter("cuenta"));
            if(request.getParameter("periodicidad").isEmpty()){//Si es factoring
                double vlrIntereses = Double.parseDouble(request.getParameter("vlrIntereses").replaceAll(",", ""));
                double tasaCobrada = Double.parseDouble(request.getParameter("tasaCobrada").replaceAll(",", "").replaceAll("%", ""));
                credito.setVlr_intereses(vlrIntereses);
                credito.setTasa_cobrada(tasaCobrada);
                vlrCXP = vlrCredito + vlrIntereses;
            }else{
                credito.setPeriodicidad(Integer.parseInt(request.getParameter("periodicidad")));
            }

            //Parametros para la CXP

            //Se ingresa el credito
            TransaccionService tService = new TransaccionService(usuario.getBd());
            CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(cbs.crearCreditoBancario(credito));

            //GENERAR LA CXP

            CXP_Doc factura = new CXP_Doc();
            String monedaUsuario = (String)session.getAttribute("Moneda");
            double valor_tasa = 0;

            //Se consulta la informacion de los bancos
            Banco bancoIngreso = model.servicioBanco.obtenerBanco(usuario.getDstrct(), request.getParameter("bancoIngreso"), request.getParameter("sucursalIngreso"));
            Banco bancoEgreso = model.servicioBanco.obtenerBanco(usuario.getDstrct(), request.getParameter("bancoEgreso"), request.getParameter("sucursalEgreso"));

            //Se consulta la tasa
            Tasa tasa = model.tasaService.buscarValorTasa(monedaUsuario, monedaUsuario, monedaUsuario, request.getParameter("fechaInicial"));
            if (tasa != null) {
                valor_tasa = tasa.getValor_tasa();
            }

            factura.setDstrct(usuario.getDstrct());
            factura.setProveedor(request.getParameter("banco"));
            factura.setTipo_documento("FAP");
            factura.setDocumento(request.getParameter("documento"));
            factura.setDescripcion(request.getParameter("descripcion"));
            factura.setAgencia(bancoEgreso.getCodigo_Agencia());
            factura.setId_mims("");
            factura.setFecha_documento(request.getParameter("fechaInicial"));
            factura.setTipo_documento_rel("");
            factura.setDocumento_relacionado("");
            factura.setFecha_aprobacion(new Date().toString());
            factura.setUsuario_aprobacion(request.getParameter("autorizador"));
            factura.setClase_documento_rel("4");
            factura.setAprobador(request.getParameter("autorizador"));
            factura.setFecha_vencimiento(request.getParameter("fechaVencimiento"));
            factura.setUltima_fecha_pago("0099-01-01 00:00:00");
            factura.setBanco(request.getParameter("bancoEgreso"));
            factura.setSucursal(request.getParameter("sucursalEgreso"));
            factura.setMoneda(monedaUsuario);
            factura.setMoneda_banco(bancoEgreso.getMoneda());
            factura.setHandle_code(request.getParameter("hc"));
            factura.setTasa(valor_tasa);
            factura.setUsuario_contabilizo("");
            factura.setFecha_contabilizacion("0099-01-01 00:00:00");
            factura.setUsuario_anulo("");
            factura.setFecha_anulacion("0099-01-01 00:00:00");
            factura.setFecha_contabilizacion_anulacion("0099-01-01 00:00:00");
            factura.setObservacion("");
            factura.setNum_obs_autorizador(0);
            factura.setNum_obs_pagador(0);
            factura.setNum_obs_registra(0);
            factura.setCreation_user(usuario.getLogin());
            factura.setUser_update(usuario.getLogin());
            factura.setBase(usuario.getBase());
            factura.setVlr_neto(vlrCXP);
            factura.setVlr_total_abonos(0);
            factura.setVlr_saldo(vlrCXP);
            factura.setVlr_neto_me(vlrCXP);
            factura.setVlr_total_abonos_me(0);
            factura.setVlr_saldo_me(vlrCXP);

            Vector vItems = new Vector();
            int numItem = 1;
            CXPItemDoc item = crearDetalleCXP(credito.getNit_banco(), "FAP", credito.getDocumento(), request.getParameter("descripcion"), credito.getVlr_credito(), bancoIngreso.getCodigo_cuenta(), numItem++);
            vItems.add(item);
            if(credito.getPeriodicidad()==0){//Si es factoring
                model.tablaGenService.buscarDatos("CB_CUENTA", "CXP_INTERES");
                TablaGen tCuenta = model.tablaGenService.getTblgen();
                item = crearDetalleCXP(credito.getNit_banco(), "FAP", credito.getDocumento(), "INTERESES", credito.getVlr_intereses(), tCuenta.getReferencia(), numItem++);
                vItems.add(item);
            }
            
           // tService.getSt().addBatch(
            ArrayList<String> listQuery = model.cxpDocService.insertarCXPDoc_SQL(factura, vItems, new Vector(), bancoEgreso.getCodigo_Agencia(),"");
            
            for (String sql : listQuery) {
                tService.getSt().addBatch(sql);
            }
            tService.execute();

            next += "?mensaje=Se han guardado los cambios";
        }else{
            next += "?mensaje=ERROR: el documento ingresado ya existe en el sistema";
        }
    }
    
    private CXPItemDoc crearDetalleCXP(String proveedor, String tipoDocumento, String documento, String descripcion, double valor, String cuenta, int numItem){
        CXPItemDoc item = new CXPItemDoc();
        item.setDstrct(usuario.getDstrct());
        item.setProveedor(proveedor);
        item.setTipo_documento(tipoDocumento);
        item.setDocumento(documento);
        item.setItem("00"+numItem);
        item.setDescripcion(descripcion);
        item.setConcepto("");
        item.setVlr(valor);
        item.setVlr_me(valor);
        item.setCodigo_cuenta(cuenta);
        item.setCodigo_abc("");
        item.setPlanilla("");
        item.setCodcliarea("");
        item.setTipcliarea("");
        item.setCreation_user(usuario.getLogin());
        item.setUser_update(usuario.getLogin());
        item.setBase(usuario.getBase());
        item.setAuxiliar("");
        item.setTipoSubledger("");
        Vector vImpuestosPorItem = new Vector();
        item.setVItems(vImpuestosPorItem);
        return item;
    }
    
    /**
     * Inserta los puntos basicos para un banco y linea
     */
    public String insertarPuntosBasicos() throws Exception{
        PuntosBasicosBanco pb = new PuntosBasicosBanco();
        pb.setBanco(request.getParameter("banco"));
        pb.setLineaCredito(request.getParameter("linea"));
        pb.setPuntosBasicos(Double.parseDouble(request.getParameter("puntosBasicos").replaceAll(",", "")));
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        if(cbs.existePuntosBasicos(pb.getBanco(), pb.getLineaCredito())){
            return "ERROR: Ya existe un registro para el banco y linea seleccionado. No se guardaron los cambios";
        }else{
            cbs.insertarPuntosBasicos(pb, usuario);
            return "ok";
        }
    }
    
    /**
     * Edita el valor de los puntos basicos para un banco y linea
     */
    public void editarPuntosBasicos() throws Exception{
        PuntosBasicosBanco pb = new PuntosBasicosBanco();
        pb.setBanco(request.getParameter("banco"));
        pb.setLineaCredito(request.getParameter("linea"));
        pb.setPuntosBasicos(Double.parseDouble(request.getParameter("puntosBasicos").replaceAll(",", "")));
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        cbs.editarPuntosBasicos(pb, usuario);
    }
        
    /**
     * Inserta los puntos basicos para un banco y linea
     */
    public String insertarCupo() throws Exception{
        CupoBanco cupo = new CupoBanco();
        cupo.setBanco(request.getParameter("banco"));
        cupo.setLineaCupo(request.getParameter("linea"));
        cupo.setCupo(Double.parseDouble(request.getParameter("cupo").replaceAll(",", "")));
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        if(cbs.existeCupo(cupo.getBanco(), cupo.getLineaCupo())){
            return "ERROR: Ya existe un registro para el banco y linea seleccionado. No se guardaron los cambios";
        }else{
            cbs.insertarCupo(cupo, usuario);
            return "ok";
        }
    }
    
    /**
     * Edita el valor de los puntos basicos para un banco y linea
     */
    public void editarCupo() throws Exception{
        CupoBanco cupo = new CupoBanco();
        cupo.setBanco(request.getParameter("banco"));
        cupo.setLineaCupo(request.getParameter("linea"));
        cupo.setCupo(Double.parseDouble(request.getParameter("cupo").replaceAll(",", "")));
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        cbs.editarCupo(cupo, usuario);
    }
    
    /**
     * Realiza la causacion de los intereses de los creditos seleccionados de la interfaz
     */
    public void causarIntereses() throws Exception{
        String mensaje = "Se han guardado los cambios";
        try {
            int numCreditos = Integer.parseInt(request.getParameter("numCreditos"));
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            
            for (int i = 0; i < numCreditos; i++) {
                if (request.getParameter("chkCredito" + i) != null) {
                    double interesesCalculados = Double.parseDouble(request.getParameter("interesesCalculados" + i).replaceAll(",", ""));
                    double interesesInterfaz = Double.parseDouble(request.getParameter("intereses" + i).replaceAll(",", "")); //Estos pueden haber sido modificados en la interfaz
                    CreditoBancarioDetalle credito = new CreditoBancarioDetalle();
                    credito.setNit_banco(request.getParameter("nit_banco" + i));
                    credito.setDocumento(request.getParameter("documento" + i));
                    credito.setFecha_inicial(request.getParameter("fecha_inicial" + i));
                    credito.setFecha_final(request.getParameter("fecha_final" + i));
                    credito.setDtf(Double.parseDouble(request.getParameter("dtf" + i)));
                    credito.setSaldo_inicial(Double.parseDouble(request.getParameter("saldo_inicial" + i)));
                    credito.setCapital_inicial(Double.parseDouble(request.getParameter("capital_inicial" + i)));
                    credito.setIntereses(interesesCalculados);
                    credito.setAjuste_intereses(interesesInterfaz - interesesCalculados);
                    credito.setCreation_user(usuario.getLogin());
                    credito.setDstrct(usuario.getDstrct());
                    double interes_acumulado_anterior = Double.parseDouble(request.getParameter("interesAcumulado" + i)) - Double.parseDouble(request.getParameter("pagoIntereses" + i));
                    credito.setInteres_acumulado(interes_acumulado_anterior + interesesInterfaz);
                    causarInteresCredito(String.valueOf(i), credito, tService,"");
                }
            }
            tService.execute();
        } catch (Exception exception) {
            exception.printStackTrace();
            mensaje = exception.getMessage();
        }

        next = "/jsp/creditoBancario/causacionIntereses.jsp?mensaje="+mensaje;
    }
    
    /**
     * Realiza la causacion de los intereses de un credito
     * @param i sufijo de las variables de la interfaz para el credito
     * @param tService service para el manejo de transacciones
     * @throws Exception 
     */
    public void causarInteresCredito(String i, CreditoBancarioDetalle credito, TransaccionService tService,String tipo) throws Exception{
        double intereses = credito.getIntereses() + credito.getAjuste_intereses();

        if (request.getParameter("periodicidad" + i).equals("0")) { // Si es factoring
            //Generar comprobante de intereses factoring
            ArrayList<String> listaQuery = crearComprobante(credito);
            for (String sql : listaQuery) {
                tService.getSt().addBatch(sql);
            }

        } else {
            //GENERAR LA ND a la CXP
            //primero consultar la cxp
            CXP_Doc cxp = model.cxpDocService.ConsultarCXP_Doc(usuario.getDstrct(), credito.getNit_banco(), "FAP", credito.getDocumento());
            ArrayList<CXP_Doc> notas = model.cxpDocService.buscarDocsRelacionados(usuario.getDstrct(), credito.getNit_banco(), "ND", cxp.getDocumento(), cxp.getTipo_documento());

            String documento = credito.getDocumento() + "-" + "ND" + (notas.size() + 1);
            String descripcion = "INTERESES A " + credito.getFecha_final();
            credito.setDoc_intereses(documento);

            CXP_Doc factura = crearND(credito, cxp, documento, descripcion, intereses);
            
            //aqui validaremos la cuanta dependiendo del hc de la cxp
            
            //primera validacion...
            //si el hc del la cxp es CT o CO toma la cuenta G010080035331
          /*  if(cxp.getHandle_code().equals("CT") || cxp.getHandle_code().equals("CO") ){
                
              model.tablaGenService.buscarDatos("CB_CUENTA", "ND");
            
            }else {
                
               /***************************************************************
                * Obtenemos la cuenta de gastos dependiendo del hc            *   
                * GT: cuenta de gastos intereses tsp: 16252150                *
                * GB: cuenta de gastos intereses fenalco: 16252151            *    
                * GR: cuenta de gastos intereses metrotel: 16252152           *
                * GS: cuenta de gastos intereses multiservicios: 16252153     * 
                **************************************************************/
                
           /*    model.tablaGenService.buscarDatos("CB_CUENTA", cxp.getHandle_code());
            }*/
          //  String cuenta = model.servicioBanco.obtenerCuentaHC(cxp.getHandle_code());
            String cuentaGasto = model.servicioBanco.obtenerCuentaGasto(credito.getDocumento());
              //TablaGen tCuenta = model.tablaGenService.getTblgen();
            Vector vItems = new Vector();
            CXPItemDoc item = crearDetalleCXP(credito.getNit_banco(), "ND", documento, descripcion, intereses, cuentaGasto, 1);
            vItems.add(item);

           // tService.getSt().addBatch(
            ArrayList<String> listaQuery = model.cxpDocService.insertarCXPDoc_SQL(factura, vItems, new Vector(), cxp.getAgencia(),tipo);
            
            for (String sql : listaQuery) {
                System.out.println(sql);
                tService.getSt().addBatch(sql);
            }
        }
        
        //Se ingresa el detalle del credito
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        tService.getSt().addBatch(cbs.insertarDetalleCredito(credito));
    }
    
    /**
     * Genera el sql para un comprobante
     * @param credito
     * @throws SQLException
     * @throws Exception 
     */
    public ArrayList<String> crearComprobante(CreditoBancarioDetalle credito) throws SQLException, Exception{
        ArrayList<String> listSql = new ArrayList<String>();
        try {
            //Se instancia el model de contabilidad
            com.tsp.finanzas.contab.model.Model modelContab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            
            String tipodoc = "IFA";            
            
            String[] monedaBase = modelContab.GrabarComprobanteSVC.getMoneda(usuario.getCia()).split("-");
            Hashtable DatosTipoDoc = modelContab.GrabarComprobanteSVC.buscarDatosTipoDoc(usuario.getDstrct(), tipodoc);
            java.sql.Timestamp ffinal = Util.ConvertiraTimestamp(credito.getFecha_final() + " 00:00:00");
            
            String periodo = Util.formatoTimestamp(ffinal, 4);
            String numdoc = DatosTipoDoc.get("prefijo") + periodo.substring(2) + UtilFinanzas.rellenar((String)DatosTipoDoc.get("serie_act"),"0",4);
            String concepto = "INTERESES CREDITO FACTORING " + credito.getDocumento();
            double valor = credito.getAjuste_intereses() + credito.getIntereses();
            credito.setDoc_intereses(numdoc);
            
            ComprobanteFacturas comprobante = new ComprobanteFacturas();
            
            comprobante.setDstrct(usuario.getDstrct());
            comprobante.setTipodoc(tipodoc);
            comprobante.setNumdoc(numdoc);
            comprobante.setSucursal(usuario.getId_agencia());
            comprobante.setPeriodo(periodo);
            comprobante.setFechadoc(credito.getFecha_final());
            comprobante.setDetalle(concepto);
            comprobante.setTercero("");
            comprobante.setTipo("C");
            comprobante.setTotal_debito(valor);
            comprobante.setTotal_credito(valor);
            comprobante.setMoneda(monedaBase[0]);
            comprobante.setAprobador(usuario.getLogin());
            comprobante.setBase(monedaBase[1]);
            comprobante.setDocumento_interno((String) DatosTipoDoc.get("codigo_interno"));
            comprobante.setOrigen("COMPROBANTE");
            comprobante.setTipo_operacion("009");
            
            ArrayList items = new ArrayList();

            //Generar los items del comprobante
            model.tablaGenService.buscarDatos("CB_CUENTA", "COMPRODET_CRE");
            TablaGen tCuenta = model.tablaGenService.getTblgen();
            ComprobanteFacturas comprodet = this.crearComprodet(credito, comprobante, tCuenta.getReferencia(), valor, 0, comprobante.getDetalle());
            items.add(comprodet);
            model.tablaGenService.buscarDatos("CB_CUENTA", "COMPRODET_DEB");
            tCuenta = model.tablaGenService.getTblgen();
            comprodet = this.crearComprodet(credito, comprobante, tCuenta.getReferencia(), 0, valor, comprobante.getDetalle());
            items.add(comprodet);

            comprobante.setItems(items);
            comprobante.setTotal_items(2);
            modelContab.GrabarComprobanteSVC.setComprobante(comprobante);

            ArrayList listaContable = new ArrayList();
            listaContable.add(comprobante);
            if (modelContab.GrabarComprobanteSVC.existeDocumento(usuario.getDstrct(), tipodoc, numdoc)) {
                throw new Exception("Este numero de documento generado ya existe dentro de la base de datos");
            } else {
                listSql = modelContab.ContabilizacionFacSvc.generarSQLComprobante(comprobante, usuario.getLogin());
                String msgError = modelContab.ContabilizacionFacSvc.getESTADO();
                
                if (!msgError.equals("")) {
                    throw new Exception(msgError);
                } else {
                    modelContab.GrabarComprobanteSVC.UpdateSerie(usuario.getDstrct(), tipodoc);
                }
            }
        } catch (Exception exception) {
            throw exception;
        }
        return listSql;
    }

    
    /**
     * Genera un detalle de comprobante
     * @param cbDetalle
     * @param comprobante
     * @param codContable
     * @param debito
     * @param credito
     * @return bean ComprobanteFacturas con los datos del comprobante
     * @throws SQLException
     * @throws Exception 
     */
    public ComprobanteFacturas crearComprodet(CreditoBancarioDetalle cbDetalle, ComprobanteFacturas comprobante, String codContable, double debito, double credito, String detalle) throws SQLException, Exception{
        //Se instancia el model de contabilidad
        com.tsp.finanzas.contab.model.Model modelContab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        
        modelContab.GrabarComprobanteSVC.buscarCuenta(usuario.getDstrct(), codContable);
        boolean Existe = modelContab.GrabarComprobanteSVC.isExisteCuenta();
        ComprobanteFacturas comprodet = new ComprobanteFacturas();
        comprodet.setDstrct(usuario.getDstrct());
        comprodet.setTipodoc(comprobante.getTipodoc());
        comprodet.setNumdoc(comprobante.getNumdoc());
        comprodet.setPeriodo(comprobante.getPeriodo());
        comprodet.setCuenta(codContable);
        comprodet.setAuxiliar("");
        comprodet.setDetalle(detalle);
        comprodet.setAbc("");
        comprodet.setTdoc_rel("FAP");
        comprodet.setNumdoc_rel(cbDetalle.getDocumento());
        comprodet.setTotal_debito(debito);
        comprodet.setTotal_credito(credito);
        comprodet.setTercero(cbDetalle.getNit_banco());
        comprodet.setBase(usuario.getBase());
        comprodet.setDocumento_interno(comprobante.getDocumento_interno());
        comprodet.setExisteCuenta(Existe);
        comprodet.setTipo("D");
        comprodet.setOrigen("COMPROBANTE");
        Vector tipSubledger = null;
        if (!Existe) {
            throw new Exception("Los numeros de cuentas parametrizados para el comprobante no existen en la base de datos o no pueden ser usadas dentro de este modulo");
        } else {
            Hashtable cuenta = modelContab.GrabarComprobanteSVC.getCuenta();
            if (cuenta.get("modulo1").toString().equalsIgnoreCase("N")) {
                comprodet.setExisteCuenta(false);
                throw new Exception("Los numeros de cuentas parametrizados para el comprobante no existen en la base de datos o no pueden ser usadas dentro de este modulo");
            }
            modelContab.subledgerService.buscarCodigosCuenta(codContable);
            tipSubledger = modelContab.subledgerService.getCodigos();
        }
        comprodet.setCodSubledger(tipSubledger);
        
        return comprodet;
    }
    
    public CXP_Doc crearND(CreditoBancarioDetalle credito, CXP_Doc cxp, String documento, String descripcion, double interesesInterfaz) throws SQLException, Exception{

        CXP_Doc factura = new CXP_Doc();
        String fecha_documento =credito.getFecha_final();
        String monedaUsuario = (String)session.getAttribute("Moneda");                
        double valor_tasa = 0;

        //Se consulta la tasa
        Tasa tasa = model.tasaService.buscarValorTasa(monedaUsuario, monedaUsuario, monedaUsuario, credito.getFecha_inicial());
        if (tasa != null) {
            valor_tasa = tasa.getValor_tasa();
        }

        //Se calcula la fecha de vencimiento de la ND de acuerdo al plazo de la factura         
        int plazo_cxp = Util.diasTranscurridos(cxp.getFecha_documento(), cxp.getFecha_vencimiento());
        String fecha_vencimiento = Util.fechaFinalYYYYMMDD(fecha_documento,plazo_cxp);

        factura.setDstrct(usuario.getDstrct());
        factura.setProveedor(credito.getNit_banco());
        factura.setTipo_documento("ND");
        factura.setDocumento(documento);
        factura.setDescripcion(descripcion);
        factura.setAgencia(cxp.getAgencia());
        factura.setId_mims("");
        factura.setFecha_documento(fecha_documento);
        factura.setTipo_documento_rel(cxp.getTipo_documento());
        factura.setDocumento_relacionado(cxp.getDocumento());
        factura.setFecha_aprobacion("0099-01-01 00:00:00");
        factura.setUsuario_aprobacion("");
        factura.setClase_documento_rel("4");
        factura.setAprobador(cxp.getAprobador());
        factura.setFecha_vencimiento(fecha_vencimiento);
        factura.setUltima_fecha_pago("0099-01-01 00:00:00");
        factura.setBanco(cxp.getBanco());
        factura.setSucursal(cxp.getSucursal());
        factura.setMoneda(cxp.getMoneda());
        factura.setMoneda_banco(cxp.getMoneda_banco());
        factura.setHandle_code(cxp.getHandle_code());
        factura.setTasa(valor_tasa);
        factura.setUsuario_contabilizo("");
        factura.setFecha_contabilizacion("0099-01-01 00:00:00");
        factura.setUsuario_anulo("");
        factura.setFecha_anulacion("0099-01-01 00:00:00");
        factura.setFecha_contabilizacion_anulacion("0099-01-01 00:00:00");
        factura.setObservacion("");
        factura.setNum_obs_autorizador(0);
        factura.setNum_obs_pagador(0);
        factura.setNum_obs_registra(0);
        factura.setCreation_user(usuario.getLogin());
        factura.setUser_update(usuario.getLogin());
        factura.setBase(usuario.getBase());
        factura.setVlr_neto(interesesInterfaz);
        factura.setVlr_total_abonos(0);
        factura.setVlr_saldo(interesesInterfaz);
        factura.setVlr_neto_me(interesesInterfaz);
        factura.setVlr_total_abonos_me(0);
        factura.setVlr_saldo_me(interesesInterfaz);
        
        return factura;
    }
     
    /**
     * Carga los bancos de acuerdo a la nacionalidad
     * @return html con los bancos a agregar al select
     * @throws SQLException 
     */
    public String cargarBancosNacionalidad() throws SQLException, Exception{
        boolean nacionales = Boolean.parseBoolean(request.getParameter("nacionales"));
        
        BancosService bs = new BancosService(usuario.getBd());
        ArrayList<Bancos> bancos = bs.obtenerBancosNacionalidad(nacionales);
        String options = "<option value='' selected='selected'>Todos</option>";
        
        for (int i = 0; i < bancos.size(); i++) {
            Bancos b = bancos.get(i);
            options += "<option value='"+b.getNit()+"'>"+b.getNombre()+"</option>";
        }
        
        return options;
    }
    
    public void consultarCreditosFiltro() throws Exception{
        String tipo = request.getParameter("tipo");
        String banco = request.getParameter("banco");
        boolean vigente = request.getParameter("chkVigente")==null?false:true;
        String fechaInicial = request.getParameter("fechaInicial");
        String fechaFinal = request.getParameter("fechaFinal");
        
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        ArrayList<CreditoBancario> creditos = cbs.consultarSaldosBancos(tipo, banco, vigente, fechaInicial, fechaFinal);
        request.setAttribute("saldosBancos", creditos);
        next = "/jsp/creditoBancario/saldosBancos.jsp?vigente="+vigente;
    }
    
    /**
     * Obtiene el DTF de la semana
     * @return String con el DTF de la semana
     * @throws SQLException
     * @throws Exception 
     */
    public String cargarDTF() throws SQLException, Exception{
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        return String.valueOf(cbs.obtenerDTFActual());
    }
    
    /**
     * Guarda un detalle de un credito (causacion y/o egreso)
     * @throws Exception 
     */
    public void guardarMovimientoCredito() throws Exception{
        
        TransaccionService tService = new TransaccionService(usuario.getBd());
        tService.crearStatement();
        
        //Se realiza la causacion de los intereses
        CreditoBancarioDetalle credito = new CreditoBancarioDetalle();
        credito.setNit_banco(request.getParameter("nit"));
        credito.setDocumento(request.getParameter("documento"));
        credito.setFecha_inicial(request.getParameter("fecha_inicial"));
        credito.setFecha_final(request.getParameter("fecha_final"));
        credito.setDtf(Double.parseDouble(request.getParameter("dtf")));
        credito.setSaldo_inicial(Double.parseDouble(request.getParameter("saldo_inicial").replaceAll(",", "")));
        credito.setCapital_inicial(Double.parseDouble(request.getParameter("capital_inicial").replaceAll(",", "")));
        credito.setIntereses(Double.parseDouble(request.getParameter("interesesCalculados").replaceAll(",", "")));
        credito.setAjuste_intereses(Double.parseDouble(request.getParameter("ajuste").replaceAll(",", "")));
        credito.setCreation_user(usuario.getLogin());
        credito.setDstrct(usuario.getDstrct());
        credito.setInteres_acumulado(Double.parseDouble(request.getParameter("interesAcumulado").replaceAll(",", "")));        
        
        if(request.getParameter("tipo").equals("TODO")){
            credito.setPago_capital(Double.parseDouble(request.getParameter("pagoCapital").replaceAll(",", "")));
            credito.setPago_intereses(Double.parseDouble(request.getParameter("pagoIntereses").replaceAll(",", "")));
            
            /*
             * Se genera el precheque y el egreso
             * Tomado de ChequeXFacturaAction.java
             */
            ChequeXFacturaService ChequeXFacturaSvc = new ChequeXFacturaService(usuario.getBd());
            ChequeXFacturaSvc.setProveedor( credito.getNit_banco() );
            ChequeXFacturaSvc.setTipoFactura("FAP");
            ChequeXFacturaSvc.setUsuario(usuario);
            ChequeXFacturaSvc.searchFacturasProveedor();
            List lista = ChequeXFacturaSvc.getListFacturasPro();                    
            if(lista.isEmpty())
                throw new Exception("El proveedor no tiene facturas disponibles");
            String[] vecFacturas = new String[1];
            vecFacturas[0] = credito.getDocumento();
            ChequeXFacturaSvc.loadFacturasCheque(vecFacturas);
            if(!ChequeXFacturaSvc.getFacturasCheque().isEmpty()){
                FacturasCheques facCheque = (FacturasCheques)ChequeXFacturaSvc.getFacturasCheque().get(0);
                facCheque.setBanco(request.getParameter("bancoEgreso"));
                facCheque.setSucursal(request.getParameter("sucursalEgreso"));
                ChequeXFacturaSvc.formarPrecheque();
                ChequeXFacturaSvc.loadNextCheque();
                ChequeXFacturaSvc.getCheque().setBanco(request.getParameter("bancoEgreso"));
                ChequeXFacturaSvc.getCheque().setSucursal(request.getParameter("sucursalEgreso"));
                //Agencias para impresion
                model.agenciaService.listarOrdenadas();


                /*
                 * Se genera el precheque
                 */
                double vlrPagar  = Double.parseDouble( request.getParameter("valorPago").replaceAll(",", ""));
                double intereses = credito.getAjuste_intereses()+credito.getIntereses();
                // Redondemos valor a pagar de acuerdo a la moneda:
                String  monedaBanco  = ChequeXFacturaSvc.getCheque().getMoneda();
                vlrPagar = (monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")) ? (double)Math.round(vlrPagar) : Util.roundByDecimal(vlrPagar, 2);

                ChequeXFacturaSvc.getCheque().setVlrPagar( vlrPagar );
                Banco b = model.servicioBanco.obtenerBanco( ChequeXFacturaSvc.getCheque().getDistrito(), ChequeXFacturaSvc.getCheque().getBanco(), ChequeXFacturaSvc.getCheque().getSucursal() ); 
                ChequeXFacturaSvc.setAgencia_impresion(b.getCodigo_Agencia());
                ArrayList<String> ListSql = ChequeXFacturaSvc.guardarPreCheque(vlrPagar);
                Vector items = new Vector();
                items.add(ChequeXFacturaSvc.getPrecheque());
                ChequeXFacturaSvc.getPrecheque().setItems(items);
                ChequeXFacturaSvc.getPrecheque().setFacturas(credito.getDocumento()+",");
                for (String sql : ListSql) {
                    System.out.println(sql);
                    tService.getSt().addBatch(sql);
                }

                boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                String msg = "";
                if( hayTasa ){
                    Vector precheques = new Vector();
                    precheques.add(ChequeXFacturaSvc.getPrecheque());
                    ChequeXFacturaSvc.setPrecheques(precheques);
                    Vector seleccionados = new Vector();
                    seleccionados.add(ChequeXFacturaSvc.getPrecheque().getId());
                    //msg = ChequeXFacturaSvc.formarPrechequesToCheque(seleccionados);
                    msg = ChequeXFacturaSvc.prechequeToCheque(ChequeXFacturaSvc.getPrecheque(), 0 );
                    ChequeXFacturaSvc.setListFacturasPro(lista);
                    if(msg.isEmpty()){
                        /*
                         * Se genera el egreso
                         */
                        ChequeXFacturaSvc.loadNextCheque();
                        Cheque cheque = ChequeXFacturaSvc.getCheque();
                        cheque.setVlrPagar(vlrPagar);

                        ArrayList<String> ListSql2 = ChequeXFacturaSvc.updateChequePreCheque(vlrPagar,intereses);
                        ChequeXFacturaSvc.removePrecheque( cheque.getId_precheque() );
                        for (String sql : ListSql2) {
                            System.out.println(sql);
                            tService.getSt().addBatch(sql);
                        }
                        credito.setDoc_pago(cheque.getNumero());
                    }else{
                        throw new Exception(msg);
                    }
                }else{
                    throw new Exception("No hay tasa");
                }
            }else{
                throw new Exception("No se encontro la factura o la factura no ha sido aprobada");
            }           
        }
        causarInteresCredito("", credito, tService,"pago");
        tService.execute();
        
        next = "/jsp/creditoBancario/consultaCreditoBancario.jsp?mensaje=Se han guardado los cambios";
    }

    private String insertarLineaCredito() throws Exception {
        LineaCredito linea = new LineaCredito();
        linea.setLinea(request.getParameter("linea"));
        linea.setHc(request.getParameter("hc"));
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        if(cbs.existeLineaCredito(linea.getLinea())){
            return "ERROR: Ya existe esta linea de credito. No se guardaron los cambios";
        }else{
            cbs.insertarLineaCredito(linea, usuario);
            return "ok";
        }
    }

    private void editarLineaCredito() throws Exception {
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        LineaCredito linea = new LineaCredito();
        linea.setLinea(request.getParameter("linea"));
        linea.setHc(request.getParameter("hc"));
        linea.setId(Integer.parseInt(request.getParameter("idLinea")));
        cbs.editarLineaCredito(linea, usuario);
       
    }

    private String insertarCupoCredito() throws Exception {
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        CupoCreditoBancario cupo = new CupoCreditoBancario();
        cupo.setNombre(request.getParameter("cupo"));
        cupo.setCuenta(request.getParameter("cuenta"));
        if(cbs.existeCupoCredito(cupo.getNombre())){
            return "ERROR: Este nombre de cupo ya existe. No se guardaron los cambios";
        }else{
            cbs.insertarCupoCredito(cupo, usuario);
            return "ok";
        }
    }

    private void editarCupoCredito() throws Exception {
        CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        CupoCreditoBancario cupo = new CupoCreditoBancario();
        cupo.setNombre(request.getParameter("cupo"));
        cupo.setCuenta(request.getParameter("cuenta"));
        cupo.setId(Integer.parseInt(request.getParameter("idCupo")));
        cbs.editarCupoCredito(cupo, usuario);
    }

    private String cargarDTFCupo(String fecha) throws Exception {
       CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
       return String.valueOf(cbs.obtenerDTFCuota(fecha));
    }

    private void cargarCuentas(String cuenta) throws Exception {
       CreditosBancariosService cbs = new CreditosBancariosService(usuario.getBd());
        ArrayList<Cuentas> listaCuentas = cbs.listarCuentas(cuenta);

        Gson gson = new Gson();
        String json = gson.toJson(listaCuentas);
        this.printlnResponse(json, "application/json;");
    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
}
