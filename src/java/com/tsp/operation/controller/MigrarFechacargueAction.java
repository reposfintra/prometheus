/*
 * MigrarFechacargueAction.java
 *
 * Created on 18 de septiembre de 2006, 07:31 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.HMigrarFechaCargue;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import com.tsp.util.UtilFinanzas;

public class MigrarFechacargueAction extends Action {
    
    /** Creates a new instance of MigrarFechacargueAction */
    public MigrarFechacargueAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/general/proceso/pla_fechacargue/MigrarExcel.jsp";
        try{
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String ruta = UtilFinanzas.obtenerRuta("ruta","/exportar/migracion/" + usuario.getLogin());
            Upload  upload = new Upload(ruta ,request );
            upload.load();
            List       lista    = upload.getFilesName();
            List       archivo  = upload.getFiles();
            Dictionary fields   = upload.getFields();            
            String cliente     = fields.get("codcli").toString();
            String filename = lista.get(0).toString();
            File file = (File)archivo.get(0);
            if (model.clienteService.existeCliente( cliente )){
                int pos = file.getName().lastIndexOf(".");
                String ext = "";
                if (pos!=-1) {
                    ext = file.getName().substring(pos+1, file.getName().length());
                }
                if( !ext.equalsIgnoreCase("xls") ) {
                    file.delete();
                    next+="?mensaje=No se puede procesar el archivo debido a que no es un xls";
                }else{
                    next+="?mensaje=El Proceso ha iniciado";
                    //System.out.println("llama el hilo "+cliente);
                    HMigrarFechaCargue hilo = new HMigrarFechaCargue(model,usuario,file,cliente);
                }
            }else{
                next+="?mensaje=El codigo del cliente no existe";
            }
            this.dispatchRequest(next);
        }catch (Exception e){
            e.printStackTrace();
        }
        
    }
    
}
