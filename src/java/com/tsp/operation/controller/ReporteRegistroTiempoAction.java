/*
 * ReporteRegistroTiempoAction.java
 *
 * Created on 3 de diciembre de 2004, 12:19
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;


/**
 *
 * @author  MARIO FONTALVO
 */
public class ReporteRegistroTiempoAction extends Action {
    
    /** Creates a new instance of ReporteRegistroTiempoAction */
    public ReporteRegistroTiempoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try{
            String Mensaje = "";
            String Opcion = request.getParameter("Opcion");
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            String Distrito     = request.getParameter("Distrito");
            String FechaInicial = request.getParameter("FInicial");
            String FechaFinal   = request.getParameter("FFinal");
            String Titulo       = "Reporte Tiempos de Planilla [" + FechaInicial + "]-["+ FechaFinal  + "]";
            String Directorio   = path + "/exportar/migracion/" + usuario.getLogin() + "/";
            String PathFile     = Directorio + Titulo + ".xls";////System.out.println("RUTA " + PathFile);
            String web = "exportar/migracion/"+ usuario.getLogin() + "/"+ Titulo + ".xls";
            if (Opcion.equals("Exportar")){
                ReporteRegistroTiempoXLS Report = new ReporteRegistroTiempoXLS();
                Report.start(model,FechaInicial, FechaFinal ,Directorio, Titulo);
                Mensaje = "Reporte Terminado.";
            }

            final String next = "/registrotiempo/reporteregistrotiempo.jsp?Mensaje="+Mensaje+"&PathFile="+web;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
            
        }catch(Exception e){
            new ServletException("Error en ReporteRegistroTiempoAction ...\n"+e.getMessage());
        }
    }
    
}
