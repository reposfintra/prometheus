/*
 * MigracionRemesasAnuladasAction.java
 *
 * Created on 16 de julio de 2005, 02:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

/**
 *
 * @author  Hager
 */
public class MigracionPlanillaAnuladaAnticipoAction extends Action{
    
    /** Creates a new instance of MigracionRemesasAnuladasAction */
    public MigracionPlanillaAnuladaAnticipoAction() {
    }
    
    public void run() throws ServletException, InformationException {        
        String next="/migracion/migracionPlanillaAnulada.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try {
            String fechaActual = Util.fechaActualTIMESTAMP();
            model.migracionPlanillaAnuladaService.cargarArchivoPropiedades();
            String fechaIni = model.migracionPlanillaAnuladaService.getUltimaFechaProceso();
            ////System.out.println("Fecha last Process :"+fechaIni);
            Vector conAnticipos = model.migracionPlanillaAnuladaService.obtenerPlanillasAnuladasConAnticipo(fechaIni, fechaActual);
            Vector sinAnticipos = model.migracionPlanillaAnuladaService.obtenerPlanillasAnuladasSinAnticipo(fechaIni, fechaActual);
            HMigracionPlanillaAnuladaAnticipo hilo = new HMigracionPlanillaAnuladaAnticipo();
            hilo.start(conAnticipos,sinAnticipos, usuario.getLogin());
            model.migracionPlanillaAnuladaService.setUltimaFechaProceso(fechaActual);
            next = "/migracion/mostrarOT.jsp";
            
        }catch (Exception ex) {
            ex.printStackTrace();
        }    
                        
        this.dispatchRequest(next);
    }
    
}
