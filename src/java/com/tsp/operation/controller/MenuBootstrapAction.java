/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.MenuBootstrapDAO;
import com.tsp.operation.model.DAOS.impl.MenuBootstrapImpl;
import com.tsp.operation.model.beans.Usuario;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class MenuBootstrapAction extends Action {

    private final int CARGAR_MENU_OPCIONES_MODULO = 0;

    private MenuBootstrapDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";
    String modulo="";
      
    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new MenuBootstrapImpl(usuario.getBd());
            modulo = request.getParameter("modulo") != null ? request.getParameter("modulo") : "";

            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_MENU_OPCIONES_MODULO:
                    cargarMenuOpciones();
                    break;
                default:
                    this.printlnResponseAjax("{\"error\":\"Peticion invalida 404 recurso no encontrado\"}", "application/json;");
                    break;
            }

        } catch (Exception ex) {
            reponseJson = "{\"error\":\"Algo salio mal al cargar el menu del modulo"+this.modulo+" \",\"exception\":\"" + ex.getMessage() + "\"}";
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                java.util.logging.Logger.getLogger(MenuBootstrapAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }

    private void cargarMenuOpciones() throws SQLException {        
        reponseJson = new Gson().toJson(dao.cargarMenuOpciones(usuario,modulo));
    }

}
