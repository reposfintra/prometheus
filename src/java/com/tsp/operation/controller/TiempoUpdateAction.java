/*
 * TiempoUpdateAction.java
 *
 * Created on 31 de enero de 2005, 08:54 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class TiempoUpdateAction extends Action{
    
    /** Creates a new instance of TiempoUpdateAction */
    public TiempoUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next                 =   "/diferenciatiempos/tiempoUpdate.jsp";
        String dstrct               =   request.getParameter("dstrct").toUpperCase();
        String stdjob               =   request.getParameter("sj").toUpperCase();
        String cf                   =   request.getParameter("cf").toUpperCase();
        String fec1                 =   request.getParameter("fec1").toUpperCase();
        String fec2                 =   request.getParameter("fec2").toUpperCase();
        HttpSession session         =   request.getSession();
        Usuario usuario             =   (Usuario) session.getAttribute("Usuario");
        String user_update          =   usuario.getLogin();
        try{
            request.setAttribute("error","");
            if(request.getParameter("buscar")!=null){
                next = "/diferenciatiempos/tiempoUpdate.jsp";
                if(request.getParameter("anular")!=null){
                    next = "/diferenciatiempos/tiempoAnular.jsp";
                }
                model.tiempoService.setTiempo(null);
                model.tiempoService.buscar(dstrct, stdjob, cf, fec1,fec2);
                if(model.tiempoService.getTiempo()!=null){
                    request.setAttribute("tiempo", model.tiempoService.getTiempo());
                }
            }
            else{
                
                String sec                  =   request.getParameter("sec").toUpperCase();
                String reporte              =   request.getParameter("reporte").toUpperCase();
                String fec1a                 =   request.getParameter("fec1a").toUpperCase();
                String fec2a                 =   request.getParameter("fec2a").toUpperCase();
        
                Tiempo tiempo = new Tiempo();
                tiempo.setcf_code(cf);
                tiempo.setcreation_user(user_update);
                tiempo.setdstrct(dstrct);
                tiempo.setreporte(reporte);
                tiempo.setsecuence(sec);
                tiempo.setsj(stdjob);
                tiempo.settime_code_1(fec1);
                tiempo.settime_code_2(fec2);
                tiempo.settime_codeUpdate1(fec1a);
                tiempo.settime_Update2(fec2a);
                request.setAttribute("tiempo", null);
                
                if(model.tiempoService.estaFechas(stdjob,fec1)&&model.tiempoService.estaFechas(stdjob,fec2)){
                    model.tiempoService.setTiempo(tiempo);
                    model.tiempoService.update();
                }
                else{
                    request.setAttribute("error","#cc0000");
                    next="/diferenciatiempos/tiempoUpdateError.jsp";
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
