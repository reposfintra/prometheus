/*
 * AnticiposSearchAction.java
 *
 * Created on 20 de abril de 2005, 04:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class AnticiposSearchAction extends Action{
    
    /** Creates a new instance of AnticiposSearchAction */
    public AnticiposSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next         ="/anticipos/anticipoUpdate.jsp";
        String codant       = request.getParameter("codant").toUpperCase();
        String dist         = request.getParameter("distrito");
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        String sj           = request.getParameter("sj");
        try{
            model.anticiposService.searchAnticipos(dist, codant,sj);
            if(model.anticiposService.getAnticipo()!=null){
                request.setAttribute("ant", model.anticiposService.getAnticipo());
            }
            if(request.getParameter("anular")!=null){
                next ="/anticipos/anticipoDelete.jsp";
            }
            if(request.getParameter("proveedores")!=null){
                model.proveedoresService.vecProveedor(usuario.getBase());
                request.setAttribute("pro","");
                next = "/anticipos/proveedorAnticipoInsert.jsp";
            
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
    
}
