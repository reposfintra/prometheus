/*
 * BuscarStandardColpapelAction.java
 *
 * Created on 23 de abril de 2005, 02:29 PM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class BuscarStandardOCargueAction extends Action{
    
    static Logger logger = Logger.getLogger(BuscarStandardColpapelAction.class);
    /** Creates a new instance of BuscarStandardColpapelAction */
    public BuscarStandardOCargueAction() {
        
        
    }
    public void run() throws ServletException, InformationException {
        
        String next="/jsp/masivo/ordencargue/preliminar.jsp";
        String op = request.getParameter("op")==null?"":request.getParameter("op");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String base =usuario.getBase();
        model.RemDocSvc.setDocumentos(null);//Juan 12.11.05
        String stdjob = "";
        String cmd = request.getParameter("cmd");
        try{
            model.imprimirOrdenService.setHoc(null);
            if(cmd==null ){
                
                String cliente = request.getParameter("cliente");
                model.stdjobdetselService.searchCliente(cliente);
                if(model.stdjobdetselService.getStandardDetSel()!=null){
                    Stdjobdetsel sj = model.stdjobdetselService.getStandardDetSel();
                    request.setAttribute("cliente",sj.getCodCliente()+"-"+sj.getCliente());
                    request.setAttribute("agency",sj.getAg_cliente());
                }
                if(op.equals("1")){
                    
                    if(cliente.substring(0,3).equals("000")){
                        String std = cliente;
                        TreeMap t = new TreeMap();
                        model.stdjobdetselService.setCiudadesDest(t);
                        model.stdjobdetselService.setCiudadesOri(t);
                        model.stdjobdetselService.setStdjobTree(t);
                        model.stdjobdetselService.searchRutasCliente(std);
                        //System.out.println(""+std);
                    }
                    else{
                        model.stdjobdetselService.searchStdJob(cliente);
                        if(model.stdjobdetselService.getStandardDetSel()!=null){
                            if(model.stdjobdetselService.getStandardDetSel()!=null){
                                Stdjobdetsel sj = model.stdjobdetselService.getStandardDetSel();
                                request.setAttribute("std", sj.getSj()+"-"+sj.getSj_desc());
                                request.setAttribute("sj", sj.getSj());
                                stdjob =sj.getSj();
                                
                            }
                        }
                    }
                }
                else if(op.equals("2")){
                    String std = cliente;
                    String origen = request.getParameter("origen");
                    model.stdjobdetselService.searchDestinosOrigen(std, origen);
                }
                else if(op.equals("3")){
                    
                    String std = cliente;
                    String origen = request.getParameter("origen");
                    String destino =request.getParameter("destino");
                    model.stdjobdetselService.searchStandaresOrDest(std, origen, destino);
                    
                    
                    request.setAttribute("ok", "ok");
                }
                
                else{
                    stdjob = request.getParameter("standard");
                    //System.out.println("Busco la ruta");
                    model.stdjobcostoService.searchRutas(stdjob);
                    //System.out.println("Termine la ruta");
                    //System.out.println("Busco Standard");
                    model.stdjobdetselService.searchStdJob(stdjob);
                    model.remidestService.searchCiudades(stdjob.substring(0,3));
                   
                    model.productoService.setProductos(null);
                    //System.out.println("Termine Standard");
                    next = "/jsp/masivo/ordencargue/OrdenCargue.jsp";
                    
                }
            }
            else if(cmd.equalsIgnoreCase("remesa")){
                String numrem = request.getParameter("numrem");
                model.remesaService.buscaRemesa(numrem.toUpperCase());
                Remesa r = model.remesaService.getRemesa();
                if(r!=null){
                    try{
                        model.RemDocSvc.LISTVC(r.getNumrem(), "");
                        model.RemDocSvc.setDocumentos(model.RemDocSvc.getVec());
                    }catch(Exception ex){
                        throw new ServletException(ex.getMessage());
                    }
                    logger.info("Encontre la remesa "+numrem);
                    logger.info("Standard "+r.getStdJobNo());
                    logger.info("Remitente "+r.getRemitente());
                    
                    HojaOrdenDeCarga oc = new HojaOrdenDeCarga();
                    oc.setStd_job_no(r.getStdJobNo());
                    oc.setRemitente(r.getRemitente());
                    oc.setDestinatarios(r.getDestinatario());
                    model.imprimirOrdenService.setHoc(oc);
                    
                    model.stdjobdetselService.searchStdJob(r.getStdJobNo());
                    model.remidestService.searchCiudades(r.getStdJobNo().substring(0,3));
                    model.productoService.setProductos(null);
                    next = "/jsp/masivo/ordencargue/OrdenCargue.jsp";
                }
            }
            
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
