/*
 * ConfigurarStdJobAction.java
 *
 * Created on 19 de enero de 2005, 01:41 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ConfigurarStdJobAction extends Action{
    
    /** Creates a new instance of ConfigurarStdJobAction */
    public ConfigurarStdJobAction() {
    }
    
    public boolean estaStd(String std){
        
        boolean sw=false;
        java.util.Enumeration enum1;
        String parametro;
        enum1 = request.getParameterNames(); // Leemos todos los atributos del request
        
        while (enum1.hasMoreElements()) {
            parametro = (String) enum1.nextElement();
            if(parametro.equalsIgnoreCase(std)){
                sw=true;
            }
        }
        
        return sw;
    }
    
    public void run() throws ServletException, InformationException {
        String next="/configuracion/consultasRedirect.jsp?stdjob=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            if(request.getParameter("cliente")!=null){
                String cliente = request.getParameter("cliente").toUpperCase();
                model.stdjobdetselService.consultaStandardCli(cliente);
            }
            else{
                next="/configuracion/consultasStdjob.jsp";
                float peso =0;
                java.util.Enumeration enum1;
                String parametro;
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    model.stdjobdetselService.buscarStandardJob(parametro);
                    if(model.stdjobdetselService.getStandardDetSel()!=null){
                        Stdjobdetsel sd = model.stdjobdetselService.getStandardDetSel();
                        //BUSCO EL PESO DE EL DISTRITO
                        model.ciaService.buscarCia(sd.getDstrct());
                        if(model.ciaService.getCompania()!=null){
                            peso = model.ciaService.getCompania().getpeso_lleno_max();
                        }
                        if(request.getParameter("peso"+sd.getSj())!=null){
                            if(Float.parseFloat(request.getParameter("peso"+sd.getSj()))!=0){
                                peso = Float.parseFloat(request.getParameter("peso"+sd.getSj()));
                            }
                        }
                        sd.setCreation_user(usuario.getLogin());
                        sd.setPeso(peso);
                        model.stdjobdetselService.setStandard(sd);
                        model.stdjobdetselService.updateStandard();
                        
                        if(!model.stdjobdetselService.existStandards(sd.getSj())){
                            model.stdjobdetselService.insertStandard(usuario.getBase());
                        }
                    }
                }
                model.stdjobdetselService.buscaStdSel();
                List stds= model.stdjobdetselService.getStandards();
                Iterator it=stds.iterator();
                while (it.hasNext()){
                    Stdjobdetsel std = (Stdjobdetsel) it.next();
                    if(!this.estaStd(std.getSj())){
                        model.stdjobdetselService.deleteStandard(std);
                    }
                }
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
