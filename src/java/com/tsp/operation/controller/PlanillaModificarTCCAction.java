/*
 * PlanillaModificarTCCAction.java
 *
 * Created on 26 de septiembre de 2005, 03:25 PM
 */

package com.tsp.operation.controller;

import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;


/**
 *
 * @author  sescalante
 */
public class PlanillaModificarTCCAction extends Action {
    
    /** Creates a new instance of PlanillaModificarTCCAction */
    public PlanillaModificarTCCAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next= "/jsp/trafico/planilla/PlanillaModificarTCC.jsp";
        String planilla = request.getParameter("planilla").toUpperCase();
        
        try{
            if (request.getParameter("tipo").equals("buscar")){//busqueda de la planilla
                model.planillaService.getInfoPlanilla(planilla);
                Planilla pla = model.planillaService.getPlanilla();////System.out.println("PLA " +model.planillaService.getPlanilla());
                //verifico si la placa de la planilla es trailer o tractomula
                next = next + "?tipo=validar";
                //planilla no existe
                if (pla == null){ 
                    next = "/jsp/trafico/planilla/PlanillaxNumero.jsp?mensaje=Planilla no existe!!";
                }else{
                    if(!model.placaService.isTractomula(pla.getPlaveh())){
                        next = next + "&tr=ok";
                    }
                }
            }else if (request.getParameter("tipo").equalsIgnoreCase("validar")) {//modificar planilla                   
                    String msg  = "";
                    String trailer = request.getParameter("c_pltrailer");
                    //validaciones de campos 
                    if(model.placaService.isTractomula(request.getParameter("placa"))){//placa es TM?//si
                        if(!trailer.equalsIgnoreCase("NA")){
                            ////System.out.println("El trailer no es NA");
                            if(model.placaService.placaExist(trailer)){//buscar placa trailer//si
                                model.placaService.buscaPlaca(trailer);
                                Placa vehiculo = model.placaService.getPlaca();
                                int swTra = 0;//0= propietario TSP
                                if(!vehiculo.getPropietario().equals("8901031611")){//placa trailer propiedad de TSP?
                                    swTra=1;//1=no
                                }
                                if(!model.placaService.isTrailer(trailer)){//placa trailes es trailer?no
                                    ////System.out.println("LA PLACA NO ES TRAILER");
                                    msg="-ERROR: La placa que usted ha escrito como trailer NO es un trailer.";
                                    //request.setAttribute("mensaje", msg);                                    
                                    ////System.out.println("ERROR: La placa que usted ha escrito como trailer NO es un trailer.");
                                    
                                }
                                else{//ok//verificacion de propiedad
                                    if(request.getParameter("proptr").equals("FINV")){
                                        //BUSCO EL PROPIETARIO A VER SI ES TSP
                                        ////System.out.println("SELECCIONARON EL TRAILER COMO TSP");
                                        if(swTra==1){
                                            ////System.out.println("EL TRAILER NO ES DE TSP");
                                            msg="-ERROR: El trailer no es de TSP.";
                                            //request.setAttribute("mensaje", msg);                                            
                                            //request.setAttribute("trailer","#cc0000");
                                            ////System.out.println("ERROR: El trailer no es de TSP!");
                                        }
                                    }
                                    else{
                                        if(swTra==0){
                                            ////System.out.println("El trailer es de sanchez polo y el usuario dijo q no. ");                                                                                        
                                            //request.setAttribute("trailer","#cc0000");
                                            msg=msg +"-ADVERTENCIA: El trailer SI es Propiedad de TSP, Ud. marco NO. Marque correctamente.";
                                            //request.setAttribute("mensaje", msg);
                                        }
                                        
                                    }
                                }
                            }else{//no existe placa trailer
                                msg = msg +"-ERROR: La placa de trailer digitada no existe.";
                                //request.setAttribute("trailer","#cc0000");
                            }
                        }
                        else{                            
                            msg = "-ERROR: Debe digitar una placa trailer.";
                        }
                    }
                    else{//placa no es TM...trailer no aplica
                        trailer = "NA";
                    }
                 
                    String tipo_cont =request.getParameter("propcont");
                    if("FINV".equals(tipo_cont)){
                        if(!request.getParameter("cont1").equals("")){
                            if(!model.placaService.placaExist(request.getParameter("cont1"))){
                                //request.setAttribute("c1","#cc0000");
                                ////System.out.println("ERROR EN EL CONTENEDOR 1....");
                                msg = msg +"-ERROR: Contenedor 1 no registrado.";
                                //tipo_cont = "NAVIERA";
                            }
                            if (!model.placaService.placaExist(request.getParameter("cont2"))){
                                //request.setAttribute("c2","#cc0000");
                                ////System.out.println("ERROR EN EL CONTENEDOR 2....");
                                msg = msg +"-ERROR: Contenedor 2 no registrado.";
                                //tipo_cont = "NAVIERA";
                            }                                
                        }
                    }
                    if(!request.getParameter("cont2").equals("")){
                        if (!request.getParameter("cont2").equals("NA")){
                            if(request.getParameter("cont2").equals(request.getParameter("cont1"))){
                                ////System.out.println("ERROR... EL CONTENEDOR 2 IGUAL A CONT1....");
                                msg = msg +"-ERROR: Contenedor 2 igual al Contenedor 1.";
                            }
                        }
                    }
                //retorno
                model.planillaService.getInfoPlanilla(planilla);                
                next = (msg == "")?next + "?tipo=modificar":next + "?tipo=validar&msg="+msg;               
            }else if (request.getParameter("tipo").equalsIgnoreCase("modificar")) {//modificar planilla
                    String co1 = (request.getParameter("cont1").equals(""))?"NA":request.getParameter("cont1");
                    String co2 = (request.getParameter("cont2").equals(""))?"NA":request.getParameter("cont2");
                    String pco1 = (co1.equals("NA"))?"NA":request.getParameter("propcont");
                    Planilla p = new Planilla();
                    p.setNumpla(planilla);
                    p.setPlatlr(request.getParameter("c_pltrailer").toUpperCase());
                    p.setTipotrailer(request.getParameter("proptr").toUpperCase());
                    p.setContenedores(co1.toUpperCase()+","+co2.toUpperCase());
                    p.setTipocont(pco1.toUpperCase());
                    //modificacion
                    model.planillaService.confirmacionTrailer(p);
                    //retorno
                    model.planillaService.getInfoPlanilla(planilla);
                    next = next + "?msg=Proceso exitoso!!&tipo=validar";
            }
            ////System.out.println("NEXT " + next);
        }catch (SQLException e ){
            throw new ServletException (e.getMessage());
        }
        this.dispatchRequest(next); 
    }    
}
