 /********************************************************************
 *      Nombre Clase.................   FinalizarPlanillaAction.java 
 *      Descripci�n..................   Finaliza una demora
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;


public class FinalizarDemoraPlanillaAction extends Action{
    
    /** Creates a new instance of FinalizarPlanillaAction */
    public FinalizarDemoraPlanillaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        HttpSession session = request.getSession();
        String numpla = request.getParameter("numpla");
        String next = "/jsp/trafico/demora/consultarDemoraPlanillaParam.jsp";
        
        //Usuario en sesi�n
        Usuario usuario = (Usuario) session.getAttribute("Usuario");    
        
        try{
            session.removeAttribute("Demora");
            Demora dem = model.demorasSvc.obtenerDemora(numpla);  
            dem.setUsuario_finalizacion(usuario.getLogin());
            dem.setUsuario_modificacion(usuario.getLogin());            
            model.demorasSvc.finalizarDemora(dem); 
            dem.setFinalizada("si");
            request.setAttribute("Demora", dem);
            next += "?msg=Demora registrada finalizada.";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
