/*
 * SjdelayInsertAction.java
 *
 * Created on 3 de diciembre de 2004, 04:55 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  KREALES
 */
public class SjdelayInsertAction extends Action {
    
    /** Creates a new instance of SjdelayInsertAction */
    public SjdelayInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String sj= request.getParameter("sj").toUpperCase();
        request.setAttribute("cf", "ECE0D8");
        request.setAttribute("sj", "ECE0D8");
        String cf= request.getParameter("cf").toUpperCase();
        String next = "/sjdelay/sjdelayInsert.jsp";
        int sw=0;
        String cf2="ninguno";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            if(!model.sdelayService.exist(sj,cf)){
                Sjdelay sd= new Sjdelay();
                sd.setCf(cf);
                sd.setDstrct(usuario.getDstrct());
                sd.setSj(sj);
                sd.setUser(usuario.getLogin());
                model.sdelayService.insert(sd);
                
            }
            else{
                sw=1;
                request.setAttribute("sj", "#cc0000");
                request.setAttribute("cf", "#cc0000");
            }
            if(sw==1)
                next = "/sjdelay/sjdelayInsertError.jsp";
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
