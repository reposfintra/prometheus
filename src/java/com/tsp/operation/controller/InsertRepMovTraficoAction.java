/*
 * InsertRepMovTraficoAction.java
 *
 * Created on 14 de septiembre de 2005, 08:31 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;
/**
 *
 * @author Armando Oviedo
 */
public class InsertRepMovTraficoAction extends Action{
    static Logger logger = Logger.getLogger(InsertRepMovTraficoAction.class);
    /** Creates a new instance of InsertRepMovTraficoAction */
    public InsertRepMovTraficoAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        String actuales = Util.fechaActualTIMESTAMP();
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String pag_actual = next;
        String actual = request.getParameter("c_dia")+" "+request.getParameter("c_hora");
        String numpla = request.getParameter("numplanilla");
        String planilla = numpla;
        String vec [] = request.getParameter("tiporep").split("/");
        String tiporeporte = vec[0];
        String TIPO_REP =  (vec.length>1)?vec[1]:"";
        boolean termino = false;
        boolean saveDetencion = false;
        //  boolean error   = (request.getParameter("error")!=null?true:false);
        String tipoubicacion = request.getParameter("tubicacion");
        String observaciones = request.getParameter("observacion");
        DatosPlanillaRMT dp = null;
        //VECTOR PARA GUARDAR TODOS LOS QUERYS A EJECUTAR
        Vector consultas = new Vector();
        try{
            HttpSession session = request.getSession();
            String mensaje = request.getParameter("mensaje");
            Calendar fecha=null;
            String fechareporte = request.getParameter("c_dia")+" "+request.getParameter("c_hora");
            String trep = tiporeporte;
            String ubicacion = "";
            String vecu[] = request.getParameter("ubicacion").split("/");
            if(vecu.length>1)
                ubicacion = vecu[0];
            else
                ubicacion = request.getParameter("ubicacion");
            String distrito = (String)(session.getAttribute("Distrito"));
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String base = usuario.getBase();
            dp = model.rmtService.getDatosPlanillaRMT();
            model.rmtService.BuscarPuestosControl();
            
            
            if(mensaje.equalsIgnoreCase("listarub")){
                
                //si el tipo de ubicacion es ciudad buscamos la zona, ult_rep, y prox reporte.
                if(tipoubicacion.equalsIgnoreCase("CIU")){
                    model.rmtService.BuscarReporteMovTraf(numpla);
                }
                if( model.tblgensvc.existeCodigo( "REP", "DETENCION", dp.getTipo_reporte().toUpperCase() ) ){//David Pina
                    model.tblgensvc.buscarLista("REP", "DETENCION_TRAFICO");//David Pina
                }else{
                    model.tblgensvc.buscarLista("REP", "REP_TRA");
                }
                //model.tblgensvc.buscarLista("REP", "REP_TRA");
                model.rmtService.BuscarUbicaciones(tipoubicacion);
                next+="?tiporeporte="+tiporeporte+"&tubicacion="+tipoubicacion+"&observacion="+observaciones+"";
            }
            else if(mensaje.equalsIgnoreCase("Sancion")){
                boolean ingresar=true;
                if(request.getParameter("causas")==null){
                    model.rmtService.searchListaCausas("REP_SAN");
                    next+="?c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tiporeporte="+tiporeporte+"&causa=ok&mensaje=Usted ha ingresado un reporte en un puesto de control que no es el proximo debe seleccionar una causa.";
                    ingresar=false;
                }else{
                    next+="?c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tiporeporte="+tiporeporte+"&causa=ok&mensaje=Usted ha ingresado un reporte en un puesto de control que no es el proximo debe seleccionar una causa.";
                    ingresar=false;
                }
            }
            else if(mensaje.equalsIgnoreCase("observacion")){
                if( !model.rmtService.existePlanillaConFecha(numpla,fechareporte) ){
                    //validamos si se debe o no agregar el reporte.
                    Vector caravana = model.rmtService.getCaravana();
                    model.rmtService.setCaravana(new Vector());
                    caravana.add(numpla);
                    if(model.rmtService.getPlanillasReportes()!=null){
                        for(int i = 0; i<model.rmtService.getPlanillasReportes().size();i++){
                            caravana.add((String)model.rmtService.getPlanillasReportes().elementAt(i));
                        }
                    }
                    for(int i = 0; i<caravana.size();i++){
                        String texto=(String) caravana.elementAt(i);
                        String vecT[] = texto.split(",");
                        numpla = vecT[0];
                        String causa_vector = vecT.length>1?vecT[1]:"";
                        fechareporte = vecT.length>2?vecT[2]:request.getParameter("c_dia")+" "+request.getParameter("c_hora");
                        boolean sw=true;
                        String causas=request.getParameter("causas")!=null?request.getParameter("causas"):causa_vector;
                        String vecCausa[] = causas.split("/");
                        String zona = model.rmtService.getZona(ubicacion);
                        String clasificacion = request.getParameter("clasificacion")!=null?request.getParameter("clasificacion"):"";
                        String vecCla[] = clasificacion.split("/");
                        
                        RepMovTrafico rm = new RepMovTrafico();
                        rm.setReg_Status("");
                        rm.setNumpla(numpla);
                        rm.setDstrct(distrito);
                        rm.setBase(base);
                        rm.setCreation_date("now()");
                        rm.setTipo_reporte( "OBS" );
                        rm.setTipo_procedencia(tipoubicacion);
                        rm.setUbicacion_procedencia(ubicacion);
                        rm.setObservacion(request.getParameter("obaut")+" "+observaciones+"  "+dp.getClientes());
                        rm.setCreation_user(usuario.getLogin());
                        rm.setUpdate_user("");
                        rm.setLast_update("now()");
                        rm.setFechareporte(fechareporte);
                        rm.setZona(zona);
                        rm.setCodUbicacion(ubicacion);
                        rm.setFec_rep_pla(dp.getFecha_prox_rep());
                        rm.setCausa("");
                        rm.setClasificacion(vecCla.length>0?vecCla[0]:"");
                        consultas.add(model.rmtService.addRMT(rm));
                        //if(!dp.getReg_status().equals("D"))
                        consultas.add(model.rmtService.actualizacionObservacion( request.getParameter("obaut")+" "+observaciones+"  "+dp.getClientes(), numpla ) );
                    }
                    model.despachoService.insertar(consultas);
                    next+="?c_dia="+actuales.substring(0,10)+"&c_hora="+actuales.substring(11,16)+"&observacion=&mensaje=Se ingreso la observacion con exito.";
                }else{
                    next+="?c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&observacion="+observaciones+"&mensaje=Ya existe un registro para esta hora en la planilla "+numpla+".";
                }
            }
            else if(mensaje.equalsIgnoreCase("insert")){
                boolean ingresar=true;
                if ( vec != null && vec.length > 1  && vec[1].equals("BOD") ){
                    model.zonaService.buscarZona(ubicacion);
                    
                    if( model.zonaService.obtenerZona() == null ){
                        ingresar=false;
                        next+="?tiporeporte="+tiporeporte+"&c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tubicacion="+tipoubicacion+"&observacion="+request.getParameter("observacion")+"&mensaje=No se puede realizar el reporte por que la ciudad no pertenece a una zona de frontera.";
                    }
                    else{
                        ingresar=true;
                    }
                }
                else{
                    if(request.getParameter("causas")==null && tiporeporte.equals("EIN")){
                        next+="?tiporeporte="+tiporeporte+"&c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tubicacion="+tipoubicacion+"&causa=ok"+"&observacion="+request.getParameter("observacion")+"&mensaje=Para las entregas intermedias es necesario seleccionar una causa.";
                        model.rmtService.searchListaCausas("MEINT");
                        ingresar=false;
                        
                    }
                    else if(request.getParameter("causas")==null && dp.getFecha_prox_rep()!=null){
                        //VALIDAMOS SI LA FECHA DEL REPORTE A INGRESAR ES MAYOR O MENOR UNA HORA AL
                        //REPORTE QUE DEBERIA GENERARSE.
                        String fecha_antRep = dp.getFecha_prox_rep();
                        int year=Integer.parseInt(fecha_antRep.substring(0,4));
                        int month=Integer.parseInt(fecha_antRep.substring(5,7))-1;
                        int date= Integer.parseInt(fecha_antRep.substring(8,10));
                        int hora=Integer.parseInt(fecha_antRep.substring(11,13));
                        int minuto=Integer.parseInt(fecha_antRep.substring(14,16));
                        
                        Calendar fecha_antRepC = Calendar.getInstance();
                        fecha_antRepC.set(year,month,date,hora,minuto,0);
                        fecha_antRepC.add(fecha_antRepC.HOUR, -1);
                        
                        Calendar fecha_desRepC = Calendar.getInstance();
                        fecha_desRepC.set(year,month,date,hora,minuto,0);
                        fecha_desRepC.add(fecha_desRepC.HOUR, +1);
                        
                        year=Integer.parseInt(fechareporte.substring(0,4));
                        month=Integer.parseInt(fechareporte.substring(5,7))-1;
                        date= Integer.parseInt(fechareporte.substring(8,10));
                        hora=Integer.parseInt(fechareporte.substring(11,13));
                        minuto=Integer.parseInt(fechareporte.substring(14,16));
                        
                        Calendar fecha_rep= Calendar.getInstance();
                        fecha_rep.set(year,month,date,hora,minuto,0);
                        // se compara si esta dentro del rango de una hora de la fecha de proximo reporte
                        if(fecha_rep.compareTo(fecha_antRepC) > 0 && fecha_rep.compareTo(fecha_desRepC) < 0 ){
                            ingresar=true;
                        }
                        else{
                            next+="?tiporeporte="+tiporeporte+"&c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tubicacion="+tipoubicacion+"&causa=ok"+"&observacion="+request.getParameter("observacion")+"&mensaje=Usted ha ingresado un reporte con una hora de diferencia al proximo reporte  debe seleccionar una causa.";
                            model.rmtService.searchListaCausas("REP_TRA");
                            ingresar=false;
                        }
                    }
                }
                if(ingresar){
                    
                    RepMovTrafico rm = new RepMovTrafico();
                    if( !model.rmtService.esFechaAnteriorAExistente(fechareporte) ){
                        //validamos si se debe o no agregar el reporte.
                        Vector caravana = model.rmtService.getCaravana();
                        model.rmtService.setCaravana(new Vector());
                        caravana.add(numpla);
                        if(model.rmtService.getPlanillasReportes()!=null){
                            for(int i = 0; i<model.rmtService.getPlanillasReportes().size();i++){
                                caravana.add((String)model.rmtService.getPlanillasReportes().elementAt(i));
                            }
                        }
                        for(int i = 0; i<caravana.size();i++){
                            next = pag_actual;
                            String texto=(String) caravana.elementAt(i);
                            String vecT[] = texto.split(",");
                            numpla = vecT[0];
                            String causa_vector = vecT.length>1?vecT[1]:"";
                            fechareporte = vecT.length>2?vecT[2]:request.getParameter("c_dia")+" "+request.getParameter("c_hora");
                            boolean sw=true;
                            String causas=request.getParameter("causas")!=null?request.getParameter("causas"):causa_vector;
                            String vecCausa[] = causas.split("/");
                            String zona = model.rmtService.getZona(ubicacion);
                            String clasificacion = request.getParameter("clasificacion")!=null?request.getParameter("clasificacion"):"";
                            String vecCla[] = clasificacion.split("/");
                            if( !model.rmtService.existePlanillaConFecha(numpla,fechareporte) ){
                                //varifico si la planilla es por lote y si es puesto de control
                                if( tipoubicacion.equals("PC") && !numpla.equals( planilla ) ){
                                    boolean esdm = model.rmtService.esDespachoManual(numpla);
                                    if(esdm){
                                        model.rmtService.BuscarPlanillaDM(numpla);
                                    }
                                    else{
                                        model.rmtService.BuscarPlanilla(numpla);
                                    }
                                    dp = model.rmtService.getDatosPlanillaRMT();
                                    model.rmtService.BuscarUbicaciones("PC");
                                }
                                
                                if(trep.equals("EFR") || trep.equals("BOD")){
                                    zona = dp.getFrontera();
                                }
                                if( vec.length > 1  && vec[1].equals("DET"))
                                    rm.setReg_Status("D");
                                else
                                    if( vec.length > 1  && vec[1].equals("BOD")){
                                        rm.setReg_Status("D");
                                        model.zonaService.buscarZona(ubicacion);
                                        Zona z = model.zonaService.obtenerZona();
                                        zona = z.getCodZona();
                                    }
                                    else
                                        rm.setReg_Status("");
                                rm.setNumpla(numpla);
                                rm.setDstrct(distrito);
                                rm.setBase(base);
                                rm.setCreation_date("now()");
                                rm.setTipo_reporte(trep);
                                rm.setTipo_procedencia(tipoubicacion);
                                String vecObser[] = request.getParameter("obaut").split(" ");
                                rm.setNombre_reporte(vecObser[0]);//debe colocar el nombre
                                rm.setUbicacion_procedencia(ubicacion);
                                rm.setObservacion(request.getParameter("obaut")+" "+observaciones+" "+dp.getClientes());
                                rm.setCreation_user(usuario.getLogin());
                                rm.setUpdate_user("");
                                rm.setTiempo(0);
                                rm.setLast_update("now()");
                                rm.setFechareporte(fechareporte);
                                rm.setZona(zona);
                                rm.setCodUbicacion(ubicacion);
                                rm.setFec_rep_pla(dp.getFecha_prox_rep());
                                rm.setCausa(vecCausa.length>0?vecCausa[0]:"");
                                rm.setClasificacion(vecCla.length>0?vecCla[0]:"");
                                //String zonaAnt = ""; //David Pina Lopez
                                //Vector datosdes = new Vector(); //David Pina Lopez
                                //if( tiporeporte.equalsIgnoreCase("EFR") || tiporeporte.equalsIgnoreCase("ECL") || tiporeporte.equalsIgnoreCase("EIN") || (vec.length > 1  && vec[1].equals("L")) ){
                                if( tiporeporte.equalsIgnoreCase("EFR") || tiporeporte.equalsIgnoreCase("ECL") || tiporeporte.equalsIgnoreCase("EIN") ){
                                    consultas = new Vector();
                                    consultas.add(model.rmtService.addRMT(rm));
                                    termino = true;
                                    model.planillaService.anulaTrafico(numpla);
                                    next = "/jsp/trafico/Movimiento_trafico/MsgMovtrafing.jsp?cerrar=0k&alert=Se finalizo el viaje.";
                                    // }
                                    model.despachoService.insertar(consultas);
                                }
                                else{
                                    //Esto se hace para cuando el tipo de ubicacion es Puesto de control
                                    if( !dp.getDespla().equals(ubicacion) ){
                                        
                                        double duracion =0;
                                        //boolean swPC = true;
                                        RepMovTrafico ori = new RepMovTrafico();
                                        RepMovTrafico des = new RepMovTrafico();
                                        
                                        model.rmtService.setUltRMT( rm );
                                        if(tipoubicacion.equals("PC")){
                                            model.rmtService.getNextPC();
                                        }else{
                                            RepMovTrafico proxrmt = new RepMovTrafico();
                                            proxrmt.setNumpla(dp.getNumpla());
                                            proxrmt.setUbicacion_procedencia(dp.getPto_control_proxreporte());
                                            proxrmt.setTipo_procedencia("PC");
                                            model.rmtService.setProxRMT(proxrmt);
                                        }
                                        
                                        ori = model.rmtService.getUltRMT();
                                        des = model.rmtService.getProxRMT();
                                        
                                        duracion=model.rmtService.getDuracionTramo(ori.getUbicacion_procedencia(), des.getUbicacion_procedencia());
                                        if(duracion<=0) {
                                            sw=false;
                                            next+="?c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tiporeporte="+tiporeporte+"&tubicacion="+tipoubicacion+"&observacion=&mensaje=Imposible agregar. El tiempo entre los tramos "+model.rmtService.getNombreCiudad(ori.getUbicacion_procedencia())+" - "+model.rmtService.getNombreCiudad(des.getUbicacion_procedencia())+" no es v�lido: "+duracion;
                                            
                                           
                                            Ciudad ciudad_o = model.ciudadService.obtenerCiudad(ori.getUbicacion_procedencia());
                                            model.ciudadservice.listarCiudadesxpais(ciudad_o.getPais());
                                            
                                            model.ciudadService.setCiudadO(model.ciudadservice.obtenerCiudades());
                                            //request.setAttribute("ciudadO", model.ciudadservice.obtenerCiudades());
                                            List co = model.ciudadService.obtenerCiudades();
                                            
                                            Ciudad ciudad_d = model.ciudadService.obtenerCiudad(des.getUbicacion_procedencia());
                                            model.ciudadservice.listarCiudadesxpais(ciudad_d.getPais());
                                            model.ciudadService.setCiudadD(model.ciudadservice.obtenerCiudades());
                                            //request.setAttribute("ciudadD", model.ciudadservice.obtenerCiudades());
                                           
                                            String open = "<script>window.open('"+request.getContextPath()+"/controller?estado=Menu&accion=Cargar&pagina=ingresarTramo.jsp&carpeta=/jsp/trafico/tramo&marco=no&opcion=26&sw=ok&c_paiso="+ciudad_o.getPais()+"&c_origen="+ciudad_o.getCodCiu()+"&c_paisd="+ciudad_d.getPais()+"&c_destino="+ciudad_d.getCodCiu()+"','','status=no,scrollbars=no,width=800,height=450,resizable=yes') </script>";
                                            request.setAttribute("open_tramo", open);
                                            
                                        }
                                        zona = model.rmtService.getZona(des.getUbicacion_procedencia());
                                        rm.setTiempo( duracion );
                                        
                                    }
                                    
                                    if(sw){
                                        consultas = new Vector();
                                        rm.setTipo_procedencia(tipoubicacion);
                                        rm.setCodUbicacion(ubicacion);
                                        rm.setUbicacion_procedencia(ubicacion);
                                        
                                        Vector datosor = model.rmtService.getDatosCiudad( dp.getPto_control_ultreporte() );//David Pina Lopez
                                        Vector datosdes = model.rmtService.getDatosCiudad( ubicacion );
                                        
                                        //System.out.println( "Datos Ultimo reporte: " + dp.getPto_control_ultreporte() );
                                        
                                        String tiene_rep = ""; //David Pina Lopez
                                        if( datosor != null && datosor.size() > 0 ){ //David Pina Lopez
                                            tiene_rep = (String)datosor.get( 4 ); //David Pina Lopez
                                        }
                                        //System.out.println( "Tiene reporte: " + tiene_rep );
                                        
                                        String zona_urb = ""; //David Pina Lopez
                                        if( datosdes != null && datosdes.size() > 0 ){ //David Pina Lopez
                                            zona_urb = (String)datosdes.get( 3 ); //David Pina Lopez
                                            //System.out.println( "NOMBRE DESTINO: " + datosdes.get( 0 ) );
                                        }
                                        //System.out.println( "ZONA URBANA PC: " + zona_urb );
                                        
                                        if( tiene_rep.equalsIgnoreCase( "S" ) && zona_urb.equalsIgnoreCase( "U" ) ){//David Pina Lopez
                                            rm.setZona( (String)datosor.get( 1 ) );//David Pina Lopez
                                            //System.out.println( "ZONA ANTERIOR: " + (String)datosor.get( 1 ) );
                                        }else{
                                            //                                            //System.out.println("Entro a esta zona");
                                            rm.setZona( zona );//David Pina Lopez
                                            //System.out.println( "ZONA NORMAL: " + zona );
                                        }
                                        
                                        consultas.add(model.rmtService.addRMT(rm));
                                        
                                        //if(!tipoubicacion.equalsIgnoreCase("CIU")){
                                        if( !dp.getDespla().equals(ubicacion)){
                                            model.rmtService.registrarReinicio();
                                            consultas.add(model.rmtService.actualizarTablaIngresoTrafico());
                                            //obtiene las remesas q su aduana es 'S'
                                            Vector vect = model.rmtService.planillaRemesasConAduana(numpla);
                                            for(int m = 0 ; m < vect.size(); m++){
                                                Remesa re = (Remesa) vect.elementAt(m);
                                                //condiciones si la remesa es internacional y q la ubicacion del pc no es frontera
                                                if( model.rmtService.remesasInternacionales( re.getOriRem(), ubicacion ) && model.rmtService.pcNoEsFrontera(ubicacion) )
                                                    consultas.add( model.rmtService.actualizacionAduana( re.getNumrem() ) );
                                            }
                                            
                                        }else if(tiporeporte.equals("LLE") || TIPO_REP.equals("DET") || tiporeporte.equals("ENT")){
                                            consultas.add(model.rmtService.actualizarTablaIngresoTrafico());
                                        }
                                        
                                        
                                        if( vec != null && vec.length > 1 && vec[1].equals("BOD") )
                                            consultas.add( model.rmtService.actualizarZonaTrafico() );
                                        else{
                                            if( request.getParameter("reinicio")!=null ){
                                                rm.setZona( model.rmtService.getZona( ubicacion ) );
                                                consultas.add( model.rmtService.actualizarZonaTrafico() );
                                            }
                                        }
                                        
                                        if(request.getParameter("pagina").equals("DetencionMovtraf.jsp")) {
                                            next="/"+request.getParameter("carpeta")+"/movtrafing.jsp";
                                            saveDetencion = true;
                                        }
                                        
                                        next+="?tiporeporte="+tiporeporte+"&c_dia="+actuales.substring(0,10)+"&c_hora="+actuales.substring(11,16)+"&tubicacion="+tipoubicacion+"&observacion=&mensaje=Nuevo reporte de movimiento de tr�fico ingresado ";
                                        
                                        
                                        
                                    }
                                    Vector destinos = dp.getDestinatarios();
                                    if(destinos!=null){
                                        for(int k=0; k<destinos.size();k++){
                                            Remesa r = (Remesa) destinos.elementAt(k);
                                            r.setDistrito(distrito);
                                            r.setUsuario(usuario.getLogin());
                                            consultas.add(model.rmtService.registrarDestinatarios(r));
                                        }
                                    }
                                    model.despachoService.insertar(consultas);
                                    if(trep.equals("VAR")){
                                        model.rmtService.setRMT(rm);
                                        model.rmtService.registrarVarado();
                                    }
                                }
                            }else{
                                next+="?c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tiporeporte="+tiporeporte+"&tubicacion="+tipoubicacion+"&observacion="+observaciones+"&mensaje=Ya existe un registro para esta hora en la planilla "+numpla+".";
                            }
                        }
                        
                    }
                    else{
                        next+="?c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tiporeporte="+tiporeporte+"&tubicacion="+tipoubicacion+"&observacion="+observaciones+"&mensaje=Imposible agregar un reporte con fecha anterior al ultimo reporte de trafico de la planilla!";
                        if(termino){
                            next+="&cerrar=0k&alert=Se finalizo el viaje.";
                        }
                    }
                    model.rmtService.setPlanillasReportes(null);
                    
                }
            }
            if(termino==false){
                if(mensaje.equalsIgnoreCase("vector")){
                    model.rmtService.setCaravana(null);
                    Vector caravana = new Vector();
                    java.util.Enumeration enum1;
                    String parametro;
                    enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(model.rmtService.estaPlanilla(parametro)){
                            caravana.add(parametro);
                        }
                    }
                    model.rmtService.setCaravana(caravana);
                }
                
                
                //jose 2006-03-09
                boolean esdm = model.rmtService.esDespachoManual(planilla);
                if(esdm)
                    model.rmtService.BuscarPlanillaDM(planilla);
                else
                    model.rmtService.BuscarPlanilla(planilla);
                dp = model.rmtService.getDatosPlanillaRMT();
                
                
                model.rmtService.SearchLastCreatedReport();
                if(model.rmtService.getUltRMT()!=null) {
                    model.rmtService.getNextPC();
                }
                model.rmtService.buscarPlanillas(dp.getNumpla(), dp.getPto_control_proxreporte());
                
            }
            
            
            
            
            if (!tipoubicacion.equals("PC"))
                model.rmtService.BuscarUbicaciones(tipoubicacion);
            
            
            if (request.getParameter("pagina").equals("DetencionMovtraf.jsp")){
                if (!saveDetencion){
                    model.rmtService.BuscarPuestosControl();
                    model.rmtService.BuscarTodasCiudades();
                    next += "&ubicacion="+request.getParameter("ubicacion");
                }
                else {
                    model.tblgensvc.buscarLista("REP", "DETENCION_TRAFICO");
                }
            }
            
        }catch(SQLException ex){
            if(ex.getErrorCode()==0){
                next=pag_actual+"?c_dia="+actual.substring(0,10)+"&c_hora="+actual.substring(11,16)+"&tiporeporte="+tiporeporte+"&tubicacion="+tipoubicacion+"&observacion="+observaciones+"&mensaje="+ex.getMessage();
                if(termino)
                    next+="&cerrar=0k&alert=Se finalizo el viaje.";
                ex.printStackTrace();
            }
            else{
                throw new ServletException(ex.getMessage());
            }
        }
        this.dispatchRequest(next);
    }
    
    
    
    
}
