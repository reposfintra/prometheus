    /***************************************
    * Nombre Clase ............. ReporteContableAnticiposAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para generar reporte de producci�n
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  22/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.HReporteContableAnticipos;





public class ReporteContableAnticiposAction  extends Action{
    
  
    
    public void run() throws ServletException, InformationException {
        
         try{
                HttpSession session     = request.getSession();
                Usuario     usuario     = (Usuario)session.getAttribute("Usuario");
                String      user        = usuario.getLogin(); 
                String      distrito    = usuario.getDstrct();
                String      proveedor   =  model.AnticiposPagosTercerosSvc.getProveedorUser(user);
                 
               
                String      next        = "/jsp/finanzas/fintra/ListaProduccion.jsp?x=1";
                String      msj         = "";
                
                String     evento       =  request.getParameter("evento");
                if(evento!=null){
                    
                    if(evento.equals("BUSCAR")){
                         String fechaIni  =  request.getParameter("fechaInicio");
                         String fechaFin  =  request.getParameter("fechaFinal");
                         model.ReporteContableAnticiposSvc.searchProduccion(distrito, proveedor,fechaIni, fechaFin);
                         List lista =  model.ReporteContableAnticiposSvc.getListReporte();
                         if(lista.size()==0)
                             msj   = "No se encontraron registros de producci�n";
                         else
                             next  = "/jsp/finanzas/fintra/ListaProduccionDetalle.jsp?x=1";
                    }
                    
                    
                    if(evento.equals("EXCEL")){
                        HReporteContableAnticipos  hilo = new HReporteContableAnticipos();
                        hilo.start(model, usuario);
                        msj   = "Su reporte est� siendo generado...";
                    }
                    
                    
                    
                    
                }
                
                
                next += "&msj="+ msj;
                 
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                   throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response);     
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
         
         
    }
    
}
