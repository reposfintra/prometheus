/*
 * Nombre        AnularContactoAction.java
 * Autor         Ing. Rodrigo Salazar
 * Fecha         18 de julio de 2005, 20:22
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class AnularContactoAction extends Action{
    
    /** Creates a new instance of AnularContactoAction */
    public AnularContactoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String next = "/jsp/trafico/contacto/contacto.jsp?mensaje=Eliminar";
        String codigo = request.getParameter("codigo");
        String ccia = request.getParameter("ccia");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");               
        
        try{            
            
            Contacto cont = new Contacto();
            cont.setCod_contacto(codigo);
            cont.setCod_cia(ccia);
            cont.setUser_update(usuario.getLogin());            
        
            model.contactoService.anularcontacto(cont); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
}
