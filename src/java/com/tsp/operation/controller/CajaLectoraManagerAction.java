/***************************************************************************
 * Nombre clase : ............... RegistroTarjetaManagerAction.java        *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                RegistroTarjeta                          *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 6 de diciembre de 2005, 08:56 AM        *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class CajaLectoraManagerAction extends Action{
    
    
    public CajaLectoraManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            /*
             *Declaracion de variables
             */
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String num_caja               = (request.getParameter("numcaja")!=null)?request.getParameter("numcaja").toUpperCase():"";
            String cliente                = (request.getParameter("cliente")!=null)?request.getParameter("cliente"):"";
            String descripcion            = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):"";
            String fechainicial           = (request.getParameter("fechaini")!=null)?request.getParameter("fechaini"):"";
            String fechafinal             = (request.getParameter("fechafin")!=null)?request.getParameter("fechafin"):"";
            String dato                   = (request.getParameter("tipo_caja")!=null)?request.getParameter("tipo_caja"):"";
            String nnum_caja               = (request.getParameter("nnumcaja")!=null)?request.getParameter("nnumcaja").toUpperCase():"";
            String ncliente                = (request.getParameter("ncliente")!=null)?request.getParameter("ncliente"):"";
            String ndescripcion            = (request.getParameter("ndescripcion")!=null)?request.getParameter("ndescripcion"):"";
            String nfechainicial           = (request.getParameter("nfechaini")!=null)?request.getParameter("nfechaini"):"";
            String nfechafinal             = (request.getParameter("nfechafin")!=null)?request.getParameter("nfechafin"):"";
            String ndato                   = (request.getParameter("tipo_caja")!=null)?request.getParameter("tipo_caja"):"";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String dstrct                 = user.getDstrct();
            String base                   = user.getBase();
            
            String next         = "";
            String Mensaje      = "";
            String secuencia    = "";
            
            /*
             *Manejo de eventos
             */
            
            
            if (Opcion.equals("Guardar")){
                //secuencia = model.cajaLectoraSvc.secuencia(num_caja);
                //dato = fechainicial+fechafinal;
                if( model.clienteService.existeCliente(cliente)){
                    try{
                        model.cajaLectoraSvc.Insert(num_caja, cliente, descripcion, dato, usuario, usuario);
                        Mensaje = "El Registro ha sido agregado";
                    }catch(Exception e){
                        model.cajaLectoraSvc.Update(num_caja, num_caja, cliente, descripcion, dato, usuario);
                        model.cajaLectoraSvc.UpdateEstado("", num_caja, usuario);
                        Mensaje = "El Registro ha sido agregado";
                    }
                }
                else{
                    Mensaje = "El Cliente no existe";
                }
                next ="/jsp/masivo/caja_lectora/CajaLectoraInsert.jsp?Mensaje="+Mensaje;
            }
            
            if (Opcion.equals("Anular")){
                model.cajaLectoraSvc.UpdateEstado("A", num_caja, usuario);
                model.cajaLectoraSvc.Search(num_caja);
                Mensaje = "El Registro ha sido anulado";
                next ="/jsp/masivo/caja_lectora/CajaLectoraModificar.jsp?Mensaje="+Mensaje+"&reload=ok";
            }
            
            
            if (Opcion.equals("Eliminar")){
                model.cajaLectoraSvc.Delete(num_caja);
                Mensaje = "El Registro ha sido eliminado";
                next ="/jsp/masivo/caja_lectora/CajaLectoraModificar.jsp?Mensaje="+Mensaje+"&reload=ok";
            }
            
            
            if (Opcion.equals("Modificar")){
                next ="/jsp/masivo/caja_lectora/CajaLectoraModificar.jsp?Mensaje="+Mensaje;
                //dato = nfechainicial+nfechafinal;
                if(model.clienteService.existeCliente(ncliente)) {
                    model.cajaLectoraSvc.Update(num_caja, num_caja, ncliente, ndescripcion, dato, usuario);
                    model.cajaLectoraSvc.Search(num_caja);
                    Mensaje = "El Registro ha sido modificado";
                    next ="/jsp/masivo/caja_lectora/CajaLectoraModificar.jsp?Mensaje="+Mensaje+"&reload=ok";
                }
                else{
                    Mensaje = "El Cliente no existe";
                    next ="/jsp/masivo/caja_lectora/CajaLectoraModificar.jsp?Mensaje="+Mensaje;
                }
            }
            
            
            if (Opcion.equals("Listado")){
                model.cajaLectoraSvc.List();
                next ="/jsp/masivo/caja_lectora/CajaLectoraListado.jsp";
                
            }
            
            if (Opcion.equals("ListadoReg")){
                model.cajaLectoraSvc.ListBus(num_caja+"%", cliente+"%", descripcion+"%", dato+"%");
                next ="/jsp/masivo/caja_lectora/CajaLectoraListado.jsp";
                
            }
            
            if (Opcion.equals("Buscar")){
                model.cajaLectoraSvc.Search(num_caja);
                next ="/jsp/masivo/caja_lectora/CajaLectoraModificar.jsp";
            }
            
            if (Opcion.equals("Activar")){
                model.cajaLectoraSvc.UpdateEstado("", num_caja, usuario);
                model.cajaLectoraSvc.Search(num_caja);
                Mensaje = "El Registro ha sido activado";
                next ="/jsp/masivo/caja_lectora/CajaLectoraModificar.jsp?Mensaje="+Mensaje+"&reload=ok";
            }
            
            
            if ("Eliminar".indexOf(Opcion) != -1) {
                model.cajaLectoraSvc.ReiniciarDato();
                model.cajaLectoraSvc.List();
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en RegistroTarjetaManagerAction .....\n"+e.getMessage());
        }
        
    }
    
}
