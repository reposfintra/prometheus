/******************************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   19.20.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import com.tsp.finanzas.contab.model.threads.*;




import org.apache.log4j.*;

public class ReporteFacturasAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    private CXP_Doc facturas = new CXP_Doc();
    /** Creates a new instance of DocumentoInsertAction */
    public ReporteFacturasAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String opc = request.getParameter( "opc" );
        //Pr�xima vista
        
        String next  = "/jsp/cxpagar/reportes/ReporteFacturasR.jsp";
        String factura = request.getParameter("factura");
        String tipo_doc = request.getParameter("tipo_doc");
        String nit = request.getParameter("proveedor");
        String numpla = request.getParameter("planilla");
        String agencia = request.getParameter("agencia");
        String banco = request.getParameter("banco");
        String sucursal = request.getParameter("sucursal");
        String fechai = request.getParameter("FechaI");
        String fechaf = request.getParameter("FechaF");
        String pag = request.getParameter("pagada");
        String Trans = request.getParameter("transaccion");
        String hc = request.getParameter("hc");
        //System.out.println("trans"+Trans);
        String transaccion_anulada = request.getParameter("transaccion_anualada");
        String ret_pago = request.getParameter("ret_pago");
        
         logger.info("FACTURA: " + factura);
        logger.info("TIPO DOC: " + tipo_doc);
        logger.info("PROVEEDOR: " + nit);
        logger.info("AGENCIA: " + agencia);
        logger.info("BANCO: " + banco);
        logger.info("SUCURSAL: " + sucursal);
        logger.info("FECHAI: " + fechai);
        logger.info("FECHAF: " + fechaf);
        logger.info("TRANSACCION: " +Trans);
        logger.info("TRANSACCION ANULADA: "+transaccion_anulada);
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        
        try{
                        
            if( request.getParameter("placa") != null && !request.getParameter("placa").equals("") ){
                String comentario = "";
            }else{
                List ListaAgencias = model.ciudadService.ListarAgencias();
                TreeMap tm = new TreeMap();
                if(ListaAgencias.size()>0) {
                    Iterator It3 = ListaAgencias.iterator();
                    while(It3.hasNext()) {
                        Ciudad  datos2 =  (Ciudad) It3.next();
                        tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                    }
                }
                request.setAttribute("agencias", ListaAgencias);
                request.setAttribute("agencias_tm", tm);
                String[]   placaFiltro           =  request.getParameterValues("placasFiltro");
                String placa = "";
                if( placaFiltro!=null ){
                    placa = model.ExtractosSvc.getToString( placaFiltro );
                }
                else
                    placa= "";
                
                String iniciox= request.getParameter("iniciox");
                model.cxpDocService.consultarDocumento(dstrct, factura, tipo_doc, numpla, agencia, banco, sucursal, nit, fechai, fechaf, pag, placa, ret_pago,iniciox,hc,usuario.getLogin());//, grupo, transaccion_anulada2);
   
                //model.cxpDocService.consultarDocumento(dstrct, factura, tipo_doc, numpla, agencia, banco, sucursal, nit, fechai, fechaf, pag, placa, ret_pago);//, grupo, transaccion_anulada2);            
                
            }
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}

//Tmaturana 10 MArzo 2007
