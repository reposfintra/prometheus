/*
 * DespachomBXPlacaAction.java
 *
 * Created on 22 de noviembre de 2005, 12:47 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  David A
 */
public class DespachomBXPlacaAction extends Action{
    
    /** Creates a new instance of DespachomBXPlacaAction */
    public DespachomBXPlacaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        try {
            
            
            String placa=""+request.getParameter("placa");
            model.despachoManualService.despachosPorPlaca(placa);
            next="/jsp/trafico/despacho_manual/despacho.jsp?reload=1";
            
        }
        catch (Exception e){
            throw new ServletException(e.getMessage());
            // e.printStackTrace();
        }
        this.dispatchRequest(next);
    }
}
