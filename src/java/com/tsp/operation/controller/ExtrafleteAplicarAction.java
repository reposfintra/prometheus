/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ExtrafleteAplicarAction extends Action{
    static Logger logger = Logger.getLogger(ExtrafleteAplicarAction.class);
    /** Creates a new instance of AcpmDeleteAction */
    public ExtrafleteAplicarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        logger.info("*********************INSERTAR COSTO REEMBOLSABLE*************************");
        
        String next="/colpapel/extrafletes.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        logger.info("Entre a la accion");
        String sj_no = request.getParameter("sj");
        String codcli ="000"+sj_no;
        //Se leen todos los parametros que envia la pagina
        if(request.getParameter("costos")==null){
            next="/colpapel/extrafletes.jsp?cerrar=ok";
            Vector efletes= model.sjextrafleteService.getFletes();
            Vector efletesN = new Vector();
            for(int i=0; i<efletes.size();i++){
                SJExtraflete sj = (SJExtraflete)efletes.elementAt(i);
                if(request.getParameter("chek"+sj.getCod_extraflete())!=null){
                    logger.info("extraflete: "+sj.getCod_extraflete());
                    sj.setSelec(true);
                    String cod = sj.getCod_extraflete();
                    sj.setAprobador(request.getParameter("aprobador"+cod));
                    if(request.getParameter("cant"+cod+"o")!=null){
                        logger.info("CANTIDAD: "+request.getParameter("cant"+cod+"o"));
                        sj.setCantidad(Float.parseFloat(request.getParameter("cant"+cod+"o")));
                    }
                    if(request.getParameter("liq"+cod+"o")!=null){
                        logger.info("VALOR LIQUIDADO: "+request.getParameter("liq"+cod+"o"));
                        sj.setVlrtotal(Float.parseFloat(request.getParameter("liq"+cod+"o")));
                    }
                    
                    if(request.getParameter("ingreso"+cod+"o")!=null){
                        logger.info("VALOR INGRESO: "+request.getParameter("ingreso"+cod+"o"));
                        sj.setValor_ingreso(Float.parseFloat(request.getParameter("ingreso"+cod+"o")));
                        
                    }
                    if(request.getParameter("liqIng"+cod+"o")!=null){
                        logger.info("VALOR TOTAL INGRESO: "+request.getParameter("liqIng"+cod+"o"));
                        sj.setVlrtotalIng(Float.parseFloat(request.getParameter("liqIng"+cod+"o")));
                        
                    }
                    if(request.getParameter("cost"+cod+"o")!=null){
                        logger.info("VALOR COSTO: "+request.getParameter("cost"+cod+"o"));
                        sj.setValor_costo(Float.parseFloat(request.getParameter("cost"+cod+"o")));
                        
                    }
                }
                else{
                    sj.setSelec(false);
                }
                efletesN.add(sj);
            }
            model.sjextrafleteService.setFletes(efletesN);
        }
        else{
            try{
                next="/colpapel/costosRembolsables.jsp?cerrar=ok";
                Vector efletes= model.sjextrafleteService.getC_reembolsables();
                Vector efletesN = new Vector();
                
                for(int i=0; i<efletes.size();i++){
                    
                    SJExtraflete sj = (SJExtraflete)efletes.elementAt(i);
                    
                    if(request.getParameter("chek"+sj.getCod_extraflete())!=null){
                        logger.info("AGREGANDO COSTO NO. "+i);
                        sj.setDistrito(usuario.getDstrct());
                        String cod = sj.getCod_extraflete();
                        
                        if(request.getParameter("cant"+cod+"o")!=null){
                            sj.setCantidad(Float.parseFloat(request.getParameter("cant"+cod+"o")));
                        }
                        if(request.getParameter("liq"+cod+"o")!=null){
                            sj.setVlrtotal(Float.parseFloat(request.getParameter("liq"+cod+"o")));
                        }
                        if(request.getParameter("liqIng"+cod+"o")!=null){
                            logger.info("VALOR TOTAL INGRESO: "+request.getParameter("liqIng"+cod+"o"));
                            sj.setVlrtotalIng(Float.parseFloat(request.getParameter("liqIng"+cod+"o")));
                            
                        }
                        if(request.getParameter("ingreso"+cod+"o")!=null){
                            logger.info("VALOR INGRESO: "+request.getParameter("ingreso"+cod+"o"));
                            sj.setValor_ingreso(Float.parseFloat(request.getParameter("ingreso"+cod+"o")));
                            
                        }
                        if(request.getParameter("cost"+cod+"o")!=null){
                            logger.info("VALOR COSTO: "+request.getParameter("cost"+cod+"o"));
                            sj.setValor_costo(Float.parseFloat(request.getParameter("cost"+cod+"o")));
                            
                        }
                        logger.info("SI EXISTE LA CEDULA "+request.getParameter("provee"+cod+"o"));
                        sj.setProveedor(request.getParameter("provee"+cod));
                        sj.setSelec(true);
                        
                        if(!model.conductorService.conductorExist(request.getParameter("provee"+cod))){
                            logger.info("NO EXISTE LA CEDULA "+request.getParameter("provee"+cod));
                            next="/colpapel/costosRembolsables.jsp?mensaje=El proveedor "+request.getParameter("provee"+cod)+" no existe en la base de datos";
                            sj.setSelec(false);
                        }
                        
                    }
                    else{
                        logger.info("NO SE MARCO EL CODIGO");
                        sj.setSelec(false);
                    }
                    logger.info("SE AGREGO EN EL VECTOR Y EL PROVEEDOR ES "+sj.getProveedor());
                    efletesN.add(sj);
                }
                
                model.sjextrafleteService.setC_reembolsables(efletesN);
                
            }catch (SQLException e){
                throw new ServletException(e.getMessage());
            }
        }
        
        this.dispatchRequest(next);
        
        
    }
}
