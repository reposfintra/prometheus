/********************************************************************
 *      Nombre Clase.................   FacturaBuscarproveedoresAction.java
 *      Descripci�n..................   Action que se encarga de cargar los bancos en la pagina siguiente
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException; 
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
//logger
import org.apache.log4j.Logger;

public class FactRecurrCargarbancosAction extends Action {
    
    Logger log = Logger.getLogger(this.getClass());
    
    
    
    
    /** Creates a new instance of FactursCargarbancosAction */
    public FactRecurrCargarbancosAction() {
    }
    
    public void run () throws ServletException, InformationException {
        try {
            
            //ivan 21 julio 2006
            HttpSession session = request.getSession();
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            //String banco =""+ request.getParameter ("c_banco");
            String Modificar =  (request.getParameter("Modificar")!=null)?request.getParameter("Modificar"):"";
            String maxfila          =   request.getParameter("maxfila");
            log.info("MODIFICAR  :  "+Modificar);
            String X=(request.getParameter ("x")==null?"":request.getParameter ("x"));
            String subtotal=(request.getParameter ("total")==null?"":request.getParameter ("total"));
            String tipo_documento =""+ request.getParameter ("tipo_documento");
            String documento =""+ request.getParameter ("documento");
            String proveedor =""+ request.getParameter ("proveedor");
            String tipo_documento_rel =""+ request.getParameter ("tipo_documento_rel");
            String documento_relacionado =""+ request.getParameter ("documento_relacionado");
            String fecha_documento=""+request.getParameter ("fecha_documento");
            String banco =""+ request.getParameter ("c_banco");
            String validar = (request.getParameter("validar") != null )?request.getParameter("validar"):"";
            String CabIva           =   (request.getParameter("CabIva")!=null?request.getParameter("CabIva"):"");
            String CabRiva          =   (request.getParameter("CabRiva")!=null?request.getParameter("CabRiva"):"");
            String CabRica          =   (request.getParameter("CabRica")!=null?request.getParameter("CabRica"):"");
            String CabRfte          =   (request.getParameter("CabRfte")!=null?request.getParameter("CabRfte"):"");
            
            /**************************************************************************************************************/
            int num_cuotas = (request.getParameter("ncuotas")!=null && request.getParameter("ncuotas").length()!=0? Integer.parseInt(request.getParameter("ncuotas")) : 0);
            String fecha_inicio_transfer = (request.getParameter("fecha_inicio")!=null?request.getParameter("fecha_inicio"):"");
            String frecuencia = (request.getParameter("CabRfte")!=null?request.getParameter("frecuencia"):"");
            String fecha_inicio_transferO = (request.getParameter("fecha_inicio2")!=null?request.getParameter("fecha_inicio2"):"");
            
            log.info("NCUOTAS: " + num_cuotas);
            log.info("FECHA INICIO TRANSFER: " + fecha_inicio_transfer);
            log.info("FRECUENCIA: " + frecuencia);
            /**************************************************************************************************************/
            
            
            String beneficiario = (request.getParameter("beneficiario")!=null)?request.getParameter("beneficiario"):"";
            String hc = (request.getParameter("hc")!=null)?request.getParameter("hc"):"";
            
            log.info("VALIDAR " + validar );
            model.servicioBanco.loadSusursales (banco);
            double vlr_neto=model.factrecurrService.getNumero (""+ request.getParameter ("vlr_neto"));
            double total    =   model.factrecurrService.getNumero (""+ request.getParameter ("total"));
            int plazo=0;
            int num_items=0;
            try{
                num_items  = Integer.parseInt (""+ request.getParameter ("num_items"));
                log.info ("Numero de Items "+num_items);
                plazo = Integer.parseInt (""+ request.getParameter ("plazo"));
            }
            catch(java.lang.NumberFormatException e){
                vlr_neto  = 0;
                total = 0;
                plazo=0;
                num_items=1;
            }
            
            Vector vItems = new Vector ();
            log.info ("Numero de Items " + num_items);
            int MaxFi = Integer.parseInt(maxfila);
            for (int i=1;i <= MaxFi; i++){
                CXPItemDoc item = new CXPItemDoc ();
                String filaTabla = request.getParameter("valor1"+i);  
                if ( filaTabla!=null  ){
                    
                    String descripcion = ""+request.getParameter("desc"+i);
                    String concepto=""+ request.getParameter ("descripcion_i"+i);
                    String codigo_cuenta=""+ request.getParameter ("codigo_cuenta"+i);
                    String codigo_abc=""+ request.getParameter ("codigo_abc"+i).toUpperCase();
                    String planilla=""+ request.getParameter ("planilla"+i);
                    //Ivan 26 julio 2006
                    String ree  = ""+ request.getParameter("REE"+i).toUpperCase();
                    String Ref3 = ""+ request.getParameter("Ref3"+i);
                    String Ref4 = ""+ request.getParameter("Ref4"+i);
                    String agenciaItem = ""+ request.getParameter("agencia"+i);
                    ////////////////////////////////////////////////////////////
                    String cod1 = request.getParameter("cod1"+i); 
                    String cod2 = request.getParameter("cod2"+i);
                    String cod3 = request.getParameter("cod3"+i);
                    String cod4 = request.getParameter("cod4"+i);
                    String cod5 = request.getParameter("cod5"+i);
                    String [] codigos = {cod1,cod2,cod3,cod4,cod5};
                    // Modificacion 21 julio 2006
                    LinkedList tbltipo = null;
                    String auxiliar      = request.getParameter("auxiliar"+i);
                    String tipoSubledger = request.getParameter("tipo"+i);
                    if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),codigo_cuenta)){
                        modelcontab.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),codigo_cuenta);
                        tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                    }
                    //////////////////////////////////////////////////////////////////////
                    
                    double valor =0;
                    double vlr_total = 0;

                    String tipcliarea=""+ request.getParameter ("cod_oc"+i);
                    String codcliarea= ""+ request.getParameter ("oc"+i);
                    String descliarea= ""+ request.getParameter ("doc"+i);
                    String iva       = ""+ request.getParameter ("iva"+i);


                    valor=model.factrecurrService.getNumero (""+ request.getParameter ("valor1"+i));
                    vlr_total = model.factrecurrService.getNumero(""+ request.getParameter("valorNeto"+i));
                
                    //Ivan 26 julio 2006
                    item.setRef3(Ref3); 
                    item.setRef4(Ref4);  
                    item.setRee(ree); 
                    item.setAgencia(agenciaItem); 
                    ///////////////////////////////

                    item.setCodigos(codigos); 
                    item.setConcepto (concepto);
                    item.setDescripcion(descripcion);
                    item.setCodigo_cuenta (codigo_cuenta);
                    item.setCodigo_abc (codigo_abc);
                    item.setPlanilla (planilla);
                    item.setVlr_me(valor);
                    item.setVlr_total(vlr_total);
                    item.setTipcliarea (tipcliarea);
                    item.setCodcliarea (codcliarea);
                    item.setDescliarea (descliarea);
                    item.setIva(iva);
                    
                    // ivan 21 julio 2006
                    item.setAuxiliar(auxiliar);
                    item.setTipo(tbltipo);
                    item.setTipoSubledger(tipoSubledger);
                    /////////////////////////////////


                    Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos ();
                    Vector vImpuestosPorItem= new Vector ();
                    for(int x=0;x<vTipoImp.size ();x++){
                        CXPImpItem impuestoItem = new CXPImpItem ();
                        String cod_impuesto = ""+ request.getParameter ("impuesto"+x+""+i);
                        impuestoItem.setCod_impuesto (cod_impuesto);
                        log.info ("impuesto"+cod_impuesto);
                        vImpuestosPorItem.add (impuestoItem);
                    }
                    
                    item.setVItems ( vImpuestosPorItem);
                    item.setVCopia(vImpuestosPorItem);
                    vItems.add (item);
                }else{
                    vItems.add (item);
                }
            }
            
            model.factrecurrService.setVecRxpItemsDoc (vItems);
            
            String sucursal ="";
            String moneda=""+ request.getParameter ("moneda");
            String descripcion =""+ request.getParameter ("descripcion");
            String observacion =""+ request.getParameter ("observacion");
            String usuario_aprobacion=""+request.getParameter ("usuario_aprobacion");
            
            
            Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit (proveedor);
            int b =0;
            if(o_proveedor != null) {
                log.info (" Id Mins "+o_proveedor.getC_idMims ());
                log.info (" Beneficiario "+o_proveedor.getC_branch_code ());
                log.info (" Sucursal "+o_proveedor.getC_bank_account ());
                log.info (" Id Mins "+o_proveedor.getC_payment_name ());
                b=1;
            }
            
            if (b!=1) {
                proveedor="";
            }
            
            CXP_Doc factura = new CXP_Doc ();
            factura.setTipo_documento (tipo_documento);
            factura.setDocumento (documento);
            factura.setProveedor (proveedor);
            factura.setTipo_documento_rel (tipo_documento_rel);
            factura.setDocumento_relacionado (documento_relacionado);
            factura.setFecha_documento (fecha_documento);
            factura.setBanco (banco);
            factura.setVlr_neto(total);
            factura.setVlr_total(vlr_neto);
            factura.setMoneda (moneda);
            factura.setSucursal (sucursal);
            factura.setDescripcion (descripcion);
            factura.setObservacion (observacion);
            factura.setUsuario_aprobacion (usuario_aprobacion);
            factura.setPlazo (plazo);
            factura.setIva(CabIva);
            factura.setRiva(CabRiva);
            factura.setRica(CabRica);
            factura.setRfte(CabRfte);
            
            /**************************************************************/
            factura.setNum_cuotas(num_cuotas);
            factura.setFecha_inicio_transfer(fecha_inicio_transfer);
            factura.setFrecuencia(frecuencia);
            factura.setFecha_inicio_transferO(fecha_inicio_transferO);
            /**************************************************************/
            
            factura.setBeneficiario(beneficiario);
            factura.setHandle_code(hc);
            
            model.factrecurrService.setFactura (factura);
            
            String next = "/jsp/cxpagar/facturasrec/facturaP.jsp?op=cargarB&num_items="+num_items+"&x="+X+"&subtotal="+subtotal+"&ag="+validar+"&maxfila="+maxfila+"&Modificar="+Modificar;
            log.info ("next en Filtro"+next);
            
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null)
                throw new Exception ("No se pudo encontrar "+ next);
            rd.forward (request, response); 
        }
        catch(Exception e) {
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
}
