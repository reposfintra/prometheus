/*
 * BancoUsuarioAction.java
 *
 * Created on 8 de julio de 2005, 10:57
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  INTEL
 */
public class BancoUsuarioAction extends Action {
    
    /** Creates a new instance of BancoUsuarioAction */
    public BancoUsuarioAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        
        String next ="/bancoUsuario/BancoUsuario.jsp";
        String opcion    = request.getParameter("opcion");
        String usuario    = request.getParameter("id_usuario");
        HttpSession session = request.getSession();
        
        //Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        //String []LOV         = request.getParameterValues("LOV");
        
        try {
            
            if ( opcion.equals("llenarTabla") ){
                Usuario us = model.usuarioService.obtenerUsuario(usuario);
                if ( us != null ){
                    llenarTabla(usuario,session);
                    session.setAttribute("nombreUsuario", us.getNombre());
                }
                else {
                    next+="?mensaje=El usuario "+usuario+" no fue encontrado!";
                    session.removeAttribute("bancos");
                }
            }
            else if ( opcion.equals("guardarTabla") ){
                String []bancos = request.getParameterValues("CHK");
                //Vector todosLosBancos = (Vector) session.getAttribute("bancos");
                //Vector bancosAEliminar = new Vector();
                model.buService.eliminarBancosDeUsuario(session.getAttribute("idUsuario").toString());
                if ( bancos != null ){
                    /*for( int i=0; i<todosLosBancos.size(); i++ ){
                        Banco b = (Banco) todosLosBancos.elementAt(i);
                        //System.out.println("El banco "+b+" es de usuario? "+b.esDeUsuario());
                        if ( b.esDeUsuario() && !buscarBanco(b,bancos) ){
                            //System.out.println("Eliminando banco: "+b);
                            model.buService.eliminarBancoDeUsuario(b,session.getAttribute("idUsuario").toString());
                        }                    
                    }
                    Vector nuevos = buscarNuevos(todosLosBancos,bancos);*/
                    model.buService.agregarBancosDeUsuario(bancos, session.getAttribute("idUsuario").toString());
                }
                next+="?mensaje=Los cambios fueron actualizados correctamente!";
                llenarTabla(session.getAttribute("idUsuario").toString(),session);
            }
            this.dispatchRequest(next);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en BancoUsuarioAction .....\n"+e.getMessage());
        }
        
    }
    
    private boolean buscarBanco(Banco b, String [] bancos){
        if( bancos == null ){
            return false;
        }
        for( int i=0; i<bancos.length; i++ ){
            //System.out.println("Comaparando "+b+" con "+bancos[i]);
            if ( b.equals(bancos[i]) ){
                //System.out.println("Banco "+b+" encontrado!");
                return true;
            }
        }
        return false;
    }
    
    private Vector buscarNuevos(Vector todos, String [] seleccionados){
        Vector nuevos = new Vector();
        for(int i=0; i<todos.size(); i++ ){
            Banco b = (Banco) todos.elementAt(i);
            for( int j=0; j<seleccionados.length; j++){
                if ( !b.esDeUsuario() && b.equals(seleccionados[j]) ){
                    nuevos.addElement(b);
                }
            }
        }
        return nuevos;
    }
    
    private void llenarTabla(String usuario, HttpSession session) throws Exception{
        Vector bancosUsuario = model.buService.obtenerBancosDeUsuario(usuario);
        Vector bancos = model.servicioBanco.obtenerBancos();
        for(int i=0; i<bancos.size(); i++ ){
            Banco b = (Banco) bancos.elementAt(i);
            if ( bancosUsuario.contains(b) ){
                b.setEsDeUsuario(true);
            }
        }
        
        session.setAttribute("idUsuario", usuario);
        session.setAttribute("bancos", bancos);
    }
    
}
