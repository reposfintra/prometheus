/*
 * PlanillaImpresionOpciones.java
 *
 * Created on 17 de diciembre de 2004, 9:44
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  Mario Fontalvo
 */
public class PlanillaImpresionOpcionesAction extends Action{
    
    /** Creates a new instance of PlanillaImpresionOpciones */
    public PlanillaImpresionOpcionesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        try {
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            String      Login   = usuario.getLogin();
            String      Agencia = usuario.getId_agencia();
            
            
            
            //String Login          = "Mario";
            //String Agencia        = "A01";
            String Opcion         = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String Origen         = (request.getParameter("Origen")!=null)?request.getParameter("Origen"):"";
            String NumeroPlanilla = request.getParameter("NumeroPlanilla").toUpperCase();
            String Mensaje        = "";
            String next           = "";
            
            
            if (Opcion.equals("Buscar")){
                model.PlanillaImpresionSvc.ReiniciarPlanillas();
                model.PlanillaImpresionSvc.ReiniciarCIA();
                model.PlanillaImpresionSvc.BuscarDatosCIA();
                model.PlanillaImpresionSvc.BuscarPlanilla(Agencia, NumeroPlanilla,usuario.getBase(),usuario.getLogin());
                next = "/impresion/planillasnoimp.jsp?Origen="+Origen;
                
                if (model.PlanillaImpresionSvc.getPlanillas()==null || model.PlanillaImpresionSvc.getPlanillas().size()<1){
                    Mensaje = "No se pudo encontrar la planilla";
                    next    = "/impresion/buscarplanilla.jsp?Mensaje="+Mensaje;
                }
                
            }
            else if (Opcion.equals("Imprimir")){
                ////System.out.println("Entro a la opcion de imprimir ....");
                List Planillas    = model.PlanillaImpresionSvc.getPlanillas();
                Iterator it       = Planillas.iterator();
                String[] ListPlan = NumeroPlanilla.split("~");
                while (it.hasNext()){
                    ////System.out.println("Entro al while ....");
                    DatosPlanillaImp datos = (DatosPlanillaImp) it.next();
                    for (int i=0;i<ListPlan.length;i++) {
                        ////System.out.println("Entro al for ....");
                        if (datos.getNumeroPlanilla().equals(ListPlan[i])){
                            //if (com.tsp.util.Util.ImprimirPlanilla(datos, model.PlanillaImpresionSvc.getCIA(), Login)){
                            model.PlanillaImpresionSvc.ActualizarFechaImpresion(ListPlan[i]);
                            if (Origen.equals("FListado")) it.remove();
                            //}
                            break;
                        }
                    }
                    model.PlanillaImpresionSvc.setPlanillas(Planillas);
                }
                next = "/impresion/planillasnoimp.jsp?Origen="+Origen;
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
    }
    
}
