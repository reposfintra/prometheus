/******************************************************************
 * Nombre ......................IndicadoresEntrarAction.java
 * Descripci�n..................Manejador de las funcionalidades de los indicadores
 * Autor........................Ing. Tito Andr�s Maturana D.
 * Fecha........................2 de marzo del 1006
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class IndicadoresEntrarAction extends Action{        
    public void run() throws javax.servlet.ServletException, InformationException{        
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");   
            String cia = (String) session.getAttribute("Distrito");
            
            String opcion = request.getParameter("Opcion");
            String codigos[] = request.getParameterValues("LOV");
            String idFolder = request.getParameter("idFolder");
            String ref1 = request.getParameter("ref1");
            String ref2 = request.getParameter("ref2");
            String nivel = request.getParameter("nivel");                                    
            String codigo = request.getParameter("codigo"); 
            String index = request.getParameter("index"); 
            String idpadre = request.getParameter("idpadre");
            
            /* Datos del indicador */
            String descrip = request.getParameter("descripcion");
            String vlr_min = request.getParameter("vlr_min");
            String vlr_max = request.getParameter("vlr_max");
            String valor = request.getParameter("valor");
            String porcent = request.getParameter("porcent");
            String metodo = request.getParameter("metodo");
            String ejec = request.getParameter("ejecucion");
            String formula = request.getParameter("formula");
            
            //////System.out.println(".................... OPCION: " + opcion);
                                   
            if(opcion.equalsIgnoreCase("Ver")){
                String iter = request.getParameter("Index");
                for(int i=0;i<model.indicadoresSvc.getLista().size();i++){
                    Indicador indc = (Indicador)(model.indicadoresSvc.getLista().get(i));
                    ////System.out.println("codigo: "+indc.getCodigo()+" Nombre: "+indc.getDescripcion());                    
                }
                Indicador indc = (Indicador)(model.indicadoresSvc.getLista().get(Integer.parseInt(iter)));                
                model.indicadoresSvc.addTraza(indc);
                //model.indicadoresSvc.actualizarContenido();                
                //model.indicadoresSvc.mostrarContenido(cia, indc.getCodigo());
                model.indicadoresSvc.buscarHijos(cia, indc.getCodigo());                
                //////System.out.println("..................... INDICADOR: " + indc.getCodigo() + " - LISTA: " + model.indicadoresSvc.getLista().size());
            } else if ( opcion.equalsIgnoreCase("Reg")){
                Indicador indc = model.indicadoresSvc.getUTraza();
                model.indicadoresSvc.removeTraza();
                if(model.indicadoresSvc.getListaTraza()!=null && model.indicadoresSvc.getListaTraza().size()>0){
                    //model.indicadoresSvc.mostrarContenido(cia, indc.getCodigo());
                    //model.indicadoresSvc.actualizarContenido();
                    Indicador tmp = model.indicadoresSvc.getUTraza();
                    String code = tmp == null ? indc.getCodigo() : model.indicadoresSvc.getRaiz() ;
                    model.indicadoresSvc.buscarHijos(cia, code);
                } else {
                    //model.ConceptoPagosvc.mostrarContenido(model.ConceptoPagosvc.getRaiz());
                    model.indicadoresSvc.buscarHijos(cia, model.indicadoresSvc.getRaiz());
                }
            } else if (opcion.equalsIgnoreCase("modificar")){
                String mover = request.getParameter("mover");
                String moveradestino = request.getParameter("moveradestino");                
                
                Indicador padre = model.indicadoresSvc.getUTraza();
                Indicador hijo = new Indicador();
                
                if( padre!=null ){
                    hijo.setCodpadre(padre.getCodigo());
                } else {
                    hijo.setCodpadre("0");
                }                
                
                hijo.setCodigo(request.getParameter("Id"));
                hijo.setDescripcion(descrip);
                hijo.setValor(Long.parseLong(valor));
                hijo.setVlr_max(Long.parseLong(vlr_max));
                hijo.setVlr_min(Long.parseLong(vlr_min));
                hijo.setPorcentaje(Integer.parseInt(porcent));
                hijo.setMetodo(metodo);
                hijo.setEjecucion(ejec);
                hijo.setFormula(formula);
                hijo.setBase(usuario.getBase());
                hijo.setUsuario(usuario.getLogin());
                hijo.setDistrict(cia);
                hijo.setId_folder((idFolder!=null? idFolder : "Y"));
                hijo.setNivel(Integer.parseInt(nivel));
                
                if(mover!=null && mover.equalsIgnoreCase("true")){
                    hijo.setCodpadre(moveradestino);
                }
                
                model.indicadoresSvc.setIndicador(hijo);
                model.indicadoresSvc.actualizar();
                
                ////System.out.println("mover: "+mover);
                                
                model.indicadoresSvc.load(cia);
                model.indicadoresSvc.buscarHijos(cia, hijo.getCodpadre());
                next+="?reload=ok&index=";               
            } else if(opcion.equalsIgnoreCase("agregar")){
                Indicador padre = model.indicadoresSvc.getUTraza();
                Indicador hijo = new Indicador();
                
                if( padre!=null ){
                    hijo.setCodpadre(padre.getCodigo());
                } else {
                    hijo.setCodpadre("0");
                }
                
                hijo.setDescripcion(descrip);
                hijo.setValor(Long.parseLong(valor));
                hijo.setVlr_max(Long.parseLong(vlr_max));
                hijo.setVlr_min(Long.parseLong(vlr_min));
                hijo.setPorcentaje(Integer.parseInt(porcent));
                hijo.setMetodo(metodo);
                hijo.setEjecucion(ejec);
                hijo.setFormula(formula);
                hijo.setBase(usuario.getBase());
                hijo.setUsuario(usuario.getLogin());
                hijo.setDistrict(cia);
                hijo.setId_folder(idFolder);
                
                if( model.indicadoresSvc.getListaTraza()!=null ){
                    hijo.setNivel(model.indicadoresSvc.getListaTraza().size()+1);
                }
                else{
                    hijo.setNivel(1);
                }
                
                if( model.indicadoresSvc.getLista()!=null ){
                    hijo.setOrden(model.indicadoresSvc.getLista().size());
                } else {
                    hijo.setOrden(0);
                }
                
                model.indicadoresSvc.setIndicador(hijo);
                model.indicadoresSvc.insertar();
                //model.indicadoresSvc.removeTraza();
                model.indicadoresSvc.load(cia);
                model.indicadoresSvc.buscarHijos(cia, hijo.getCodpadre());
                next += "?reload=ok";
            } else if(opcion.equalsIgnoreCase("Eliminar")){
                String padre = model.indicadoresSvc.getUTraza() != null ? 
                        model.indicadoresSvc.getUTraza().getCodigo() : model.indicadoresSvc.getRaiz();
                
                if (codigos!=null){
                    for (int i=0; i<codigos.length;i++){
                        Indicador idc = new Indicador();
                        idc.setDistrict(cia);
                        idc.setCodigo(codigos[i]);
                        
                        /*model.indicadoresSvc.buscarHijos(idc.getDistrict(), idc.getCodigo());
                        
                        List lst = model.indicadoresSvc.getLista();
                        
                        for( int j=0; j<lst.size(); j++){
                            Indicador idc2 = new Indicador();
                            idc2 = (Indicador) lst.get(i);

                            model.indicadoresSvc.setIndicador(idc2);
                            model.indicadoresSvc.delete();
                        }*/
                                                
                        model.indicadoresSvc.setIndicador(idc);
                        model.indicadoresSvc.delete();
                    }
                }      
                        
                next += "?reload=ok";
                model.indicadoresSvc.load(cia);
                model.indicadoresSvc.buscarHijos(cia, padre);
            } else if(opcion.equalsIgnoreCase("cargarvistafolder")){                
                model.indicadoresSvc.buscarHijos(cia, idpadre);
                next += "?index=" + index;   
                ////System.out.println(".............. HIJOS ENCONTRADOS: " + model.indicadoresSvc.getLista().size());
            }  
            
            model.indicadoresSvc.soloPadres(cia);
            model.tblgensvc.searchDatosTablaMetodo(); 
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new ServletException(ex.getMessage());            
        }
        this.dispatchRequest(next);
    }    
}
