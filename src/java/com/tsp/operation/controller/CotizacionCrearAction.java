/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.http.*;
/**
 *
 * @author Rhonalf
 */
public class CotizacionCrearAction extends Action{

    public CotizacionCrearAction(){

    }

     public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        CotizacionService cserv = new CotizacionService();
        String parametro ="";
        String ms="";
        String nota="";
        String fecha="";
        //String tipUnid="";
        double cant=0;
        int k=0;
        double subt=0.0;
        ArrayList listado = new ArrayList();
        Cotizacion cot = null;
        HttpSession session   = request.getSession();//090922
        Usuario     usuario   = (Usuario) session.getAttribute("Usuario"); //090922
        String loginx=usuario.getLogin();
        String next  = "/jsp/delectricaribe/cotizacion/cotizacion_contratista.jsp";
        //request.setAttribute("msg", "Orden(es) creada(s)(Etapa de prueba...)!");
        request.setAttribute("msg", "");
        try{
            ms = request.getParameter("multiservicios")!=null? request.getParameter("multiservicios"):"";
            fecha = request.getParameter("fecha")!=null? request.getParameter("fecha"):"";
            k = request.getParameter("filas")!=null? Integer.parseInt(request.getParameter("filas")):0;
            System.out.println("Filas listas para insertar: "+k);
            MaterialesService prs = new MaterialesService();
            ArrayList ars = null;
            Material mat = null;
            if(k>0){
                int contadorx=1;//091028
                //for(int i=1;i<=k;i++){//091028
                int i=1;//091028
                while (contadorx<=k){//091028
                    //se debe adicionar a listado y aumentar i cuando realmente hayan datos (volviendo el for un while y teniendo otro contador para recorrer realmente las filas)//091028
                    if (request.getParameter("codp"+i)!=null && !(request.getParameter("codp"+i).equals(""))){//091028
                        parametro = request.getParameter("codp"+i)!=null? request.getParameter("codp"+i):"";
                        ars = prs.buscarPor(1, parametro);
                        mat = (Material)ars.get(0);
                        cant = request.getParameter("cantidad"+i)!=null? Double.parseDouble(request.getParameter("cantidad"+i)):0;//100226
                        //subt = request.getParameter("subtotal_2"+i)!=null? Double.parseDouble(request.getParameter("subtotal_2"+i)):0.0;
                        subt = mat.getValor();//090929
                        subt = subt * cant;//090929
                        nota = request.getParameter("nota"+i)!=null? request.getParameter("nota"+i):"";
                        //tipUnid = request.getParameter("unid"+i)!=null? request.getParameter("unid"+i):"";
                        //System.out.println("codigo "+parametro+" cantidad "+cant+" subtotal "+subt);
                        cot = new Cotizacion();
                        cot.setAccion(ms);
                        cot.setCantidad(cant);
                        cot.setCodigo("");
                        cot.setFecha(fecha);
                        cot.setMaterial(parametro);
                        cot.setObservacion(nota);
                        listado.add(cot);
                        contadorx=contadorx+1;//091028
                        System.out.println("__request.getParameter(codp_"+i+")="+request.getParameter("codp"+i)+" en el if ");//091028
                    }else{//091028
                        System.out.println("request.getParameter(codp_"+i+")="+request.getParameter("codp"+i)+" en el else ");//091028
                    }//091028
                    i=i+1;//091028
                }
                if(request.getParameter("opcion")!=null && !(request.getParameter("opcion").equals(""))) {//2010-01-14
                     if(request.getParameter("opcion").equals("save")){ //2010-01-14
                        System.out.println("pasando a borrador...");//2010-01-14
                        cserv.borrarDetalles(ms);//2010-01-14
                        cserv.guardarBorrador(listado,fecha,loginx); //2010-01-14
                    }
                }
                else { //2010-01-14
                    System.out.println("pasando a insercion...");//2010-01-14
                    cserv.borrarDetalles(ms);//2010-01-22
                    cserv.insertDetsPlusOrden(listado, fecha,loginx);//2010-01-14
                }
                next=next+"?idaccion="+ms;
            }
            else {
                System.out.println("Error: no se enviaron filas para insertar...");
                request.setAttribute("msg", "Error: no se enviaron filas para insertar...");
            }

        }
        catch(Exception ec){
            System.out.println("Error en el run del action: "+ec.toString());
            request.setAttribute("msg", "Ha ocurrido un error!");
        }
        this.dispatchRequest(next);
    }
}