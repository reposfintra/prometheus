/********************************************************************
 *      Nombre Clase.................   PosBancariaObtenerAction.java
 *      Descripci�n..................   Traslada saldos en el archivo posicion_bancaria
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   25.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Andres
 */
public class PosBancariaTrasladarSaldosAction extends Action{
       
    /** Creates a new instance of PosBancariaRefreshAction */
    public PosBancariaTrasladarSaldosAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String banco = request.getParameter("banco");
        String agency_id = request.getParameter("agency_id");
        String fecha = request.getParameter("fecha");
        
        String fechatr = request.getParameter("fechatr");/*Fecha de traslado*/
        
        //String next = "/jsp/cxpagar/posbancaria/PosBancariaTraslado.jsp";
        String next = "/controller?estado=PosBancaria&accion=Refresh&cmd=show&listar=OK&cargar=OK&agenciaSelec=" + agency_id + "&bancoSelec=" + banco + "&fecha=" + fecha + "&traslado=OK";
        //////System.out.println("................ NEXT TRASLADO: " + next);        
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        String cia = (String) session.getAttribute("Distrito");
        
        try{
                        
            Vector bancos = new Vector();
            
            if( banco.length()!=0 ){
                model.servicioBanco.obtenerSucursales(agency_id, banco, cia);
                bancos = model.servicioBanco.getBancos();
            } else {
                model.servicioBanco.obtenerBancosAgenciaCia(agency_id, cia);
                bancos = model.servicioBanco.getBancos();
            }
            
            for( int i = 0; i<bancos.size(); i++){
                
                Banco b = (Banco) bancos.elementAt(i);
                
                PosBancaria posb = new PosBancaria();
                posb.setCreation_user(user.getLogin());
                posb.setDstrct(cia);
                posb.setBase(user.getBase());
                posb.setFecha(fecha);
                posb.setBranch_code(b.getBanco());
                posb.setBank_account_no(b.getBank_account_no());
                posb.setAgency_id(b.getCodigo_Agencia());
                posb.setAnticipos(0);
                posb.setProveedores(0);
                posb.setCupo(0);
                posb.setSaldo_inicial(0);
                posb.setSaldo_anterior(0);
                posb.setNuevo_saldo(0);
                posb.setReg_status("");
                
//                String reg_status = model.posbancariaSvc.existe(cia, posb.getAgency_id(), posb.getBranch_code(), posb.getBank_account_no(), posb.getFecha());
//                if( reg_status==null || reg_status.compareTo("A")==0 ){
//                    
//                    /* Traslado del saldo anterior */
//                    model.posbancariaSvc.posicionAnterior(posb.getDstrct(), posb.getAgency_id(), posb.getBranch_code(), posb.getBank_account_no());
//                    PosBancaria aux = model.posbancariaSvc.getPosbanc();
//                    if( aux!=null ){
//                        posb.setSaldo_anterior(aux.getNuevo_saldo());
//                        posb.setSaldo_inicial(aux.getNuevo_saldo());
//                    }
//                    
//                    /* Traslado de Corridas Generadas*/
//                    String fechaAnt = Util.FechaMenosDias(posb.getFecha(), 1);
//                    Calendar ante = Util.crearCalendar(fechaAnt + " 00:00:00");
//                    boolean domingo = (ante.get(ante.DAY_OF_WEEK)==Calendar.SUNDAY);
//                    boolean feriado = model.tablaGenService.isFeriado(fechaAnt);
//                    Vector corridas = new Vector();
//                    if( domingo || feriado ){
//                        String fechai = this.diaApropiado(fechaAnt);                        
//                        //////System.out.println(".......................... SE MIGRO DESDE: " + fechai + " HASTA: " + fechaAnt);
//                        model.posbancariaSvc.corridaPeriodo(posb.getDstrct(), posb.getBranch_code(), posb.getBank_account_no(), fechai.replaceAll("-",""), fechaAnt.replaceAll("-",""));
//                    } else {
//                        //////System.out.println(".......................... SE MIGRO DE: " + fechaAnt);
//                        model.posbancariaSvc.corridaAnterior(posb.getDstrct(), posb.getBranch_code(), posb.getBank_account_no(), fechaAnt.replaceAll("-",""));
//                    }                    
//                    corridas = model.posbancariaSvc.getCorridas();
//                    for( int j=0; j<corridas.size(); j++){
//                        Hashtable ht = (Hashtable) corridas.elementAt(j);
//                        String tipo = ht.get("tipo").toString();
//                        
//                        if( tipo.compareTo("0")==0 || tipo.compareTo("1")==0 || tipo.compareTo("A")==0 ){
//                            posb.setAnticipos(posb.getAnticipos() + Double.parseDouble(ht.get("total").toString()));
//                        } else if ( tipo.compareTo("4")==0 ){
//                            posb.setProveedores(posb.getProveedores() + Double.parseDouble(ht.get("total").toString()));
//                        }
//                    }
//                    
//                    //////System.out.println("...................... Anticipos: " + posb.getAnticipos());
//                    //////System.out.println("...................... Proveedores: " + posb.getProveedores());
//                    
//                    double pendiente = posb.getSaldo_anterior() - posb.getSaldo_anterior();
//                    double nsaldo = posb.getSaldo_inicial() - pendiente - ( posb.getAnticipos() + posb.getProveedores() );
//                    posb.setNuevo_saldo(nsaldo);
//                    
//                    if( reg_status==null ){
//                        model.posbancariaSvc.setPosbanc(posb);
//                        model.posbancariaSvc.ingresar();
//                    } else if ( reg_status.compareTo("A")==0 ){
//                        model.posbancariaSvc.setPosbanc(posb);
//                        model.posbancariaSvc.actualizar();
//                    }
//                }               
            }

        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }    
    
    /**
     * Busca el d�a apropiado para realizar el traslado de las corridas generadas, 
     * teniendo en cuenta los d�as S�bado, Domingo y Feriados.
     * @autor Ing. Andr�s Maturana D.
     * @param dia D�a a evaluar si es el apropiado.
     * @return D�a apropiado desde donde se realizar� el traslado.
     * @version 1.0
     */ 
    private String diaApropiado(String dia) throws SQLException{
        String fechaAnt = Util.FechaMenosDias(dia, 1);
        Calendar ante = Util.crearCalendar(fechaAnt + " 00:00:00");
        boolean domingo = (ante.get(ante.DAY_OF_WEEK)==Calendar.SUNDAY);
        boolean sabado = (ante.get(ante.DAY_OF_WEEK)==Calendar.SATURDAY);
        boolean feriado = model.tablaGenService.isFeriado(fechaAnt);;
        
        if( domingo || sabado ){
            fechaAnt = this.diaApropiado(fechaAnt);
        } else if( feriado ){
            fechaAnt = this.diaApropiado(fechaAnt);
        } 
        
        return fechaAnt;
    }
    
}
