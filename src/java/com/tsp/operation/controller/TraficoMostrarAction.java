/********************************************************************
 *      Nombre Clase.................   TraficoMostrarAction.java
 *      Descripci�n..................   Action para Acceder al modulo de control de Trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import org.apache.log4j.Logger;
/**
 *
 * @author  Administrador
 */
public class TraficoMostrarAction extends Action {
    
    /** Creates a new instance of TraficoVerTodos */
    public TraficoMostrarAction () {
    }
    
    static Logger logger = Logger.getLogger(TraficoMostrarAction.class);
    
    public void run () throws ServletException, InformationException {
        
        logger.info("********************************************************************************");
        logger.info("*********************N U E V O  T R A F I C O*************************");
        String next="";
        String filtroP ="Ningun filtro ha sido aplicado";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String user=""+usuario.getLogin ();
        //logger.info ("Usuario:"+user);
        try {
            // String f = model.traficoService.zonasDeTurnos(user);
            // Vector mkl=model.traficoService.vDeTurnos (user);
            model.traficoService.obtenerZonasPorUsuario (user);
            model.traficoService.gennerarVZonasTurno (user);
            //model.traficoService.generarVZonas (user);
            //logger.info ("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas ();
            String validacion=" zona='-1'";
            //logger.info ("genera zonas 11 ");
            if(zonas.size ()>0){
                if ((zonas != null)||zonas.size ()>0) {
                    validacion = model.traficoService.validacion (zonas);
                }
                //logger.info ("termina zonas "+validacion );
                java.util.Date utilDate = new java.util.Date (); //fecha actual
                long lnMilisegundos = utilDate.getTime ();
                java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp (lnMilisegundos);
                //logger.info ("entra en actualizar zonas");
                model.traficoService.actualizarZonas (validacion, sqlTimestamp);
                //logger.info ("SALIO DE ACTUALIZAR ZONAS................................");
                
                String var2="";
                String var1="";
                String var3="";
                String configuracion="";
                String clas="";
                String linea="";
                String filtro="";
                Vector colum= new Vector ();
                String consulta="";
                ConfTraficoU vista = new ConfTraficoU ();
                
                if(model.traficoService.existeConfiguracion (user)==false) {
                    
                    
                    
                    var2=model.traficoService.obtVar1 ();
                    var2=model.traficoService.codificar (var2);
                    var3= model.traficoService.obtVar3 ();
                    filtro=" where "+validacion;
                    configuracion=model.traficoService.obtConf ();
                    linea=model.traficoService.obtlinea ();
                    
                    model.traficoService.listarColTrafico (var3);
                    Vector col = model.traficoService.obtColTrafico ();
                    model.traficoService.listarColtrafico2 (col);
                    
                    
                    consulta= model.traficoService.obtConf ()+  ",reg_status,ult_observacion,color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias   from ingreso_trafico where "+validacion;
                   // //System.out.println("Consulta--------->>>>>>>>>  : "+consulta);
                    model.traficoService.listarCamposTrafico (linea);
                    colum = model.traficoService.obtenerCampos (linea);
                    model.traficoService.generarTabla (consulta, colum);
                    logger.info ("genera consulta");
                    
                    vista.setConfiguracion (configuracion);
                    vista.setFiltro (filtro);
                    vista.setLinea (linea);
                    vista.setClas (clas);
                    vista.setVar1 (var1);
                    vista.setVar2 (var2);
                    vista.setVar3 (var3);
                    vista.setCreation_user (user);
                    vista.setDstrct (""+usuario.getDstrct ());
                    model.traficoService.insertarConfiguracion (vista);
                    logger.info("Linea rescatada del usuario "+linea);
                    logger.info("Tamano del vector de campos trafico "+model.traficoService.obtCamposTrafico().size());
                    
                }
                else {
                    //logger.info("YA tiene conf");
                    vista = model.traficoService.listarConfiguracionPorUsuario (user);
                    configuracion=vista.getConfiguracion ()+", color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias  ";
                    ////System.out.println("CONFIGURACIONN--->"+ configuracion);
                    filtro=vista.getFiltro ();
                    linea=vista.getLinea ();
                    clas=vista.getClas ();
                    var1=vista.getVar1 ();
                    var2=vista.getVar2 ();
                    var3=vista.getVar3 ();
                    logger.info("Linea rescatada del usuario "+linea);
                    filtroP =filtro.replaceAll("where","");
                    filtroP =filtroP.replaceAll("\\(","");
                    filtroP =filtroP.replaceAll("\\)","");
                    filtroP =filtroP.replaceAll(" and ",",");
                    filtroP =filtroP.replaceAll(" or ",",");
                    
                    model.traficoService.listarColTrafico (var3);
                    Vector col = model.traficoService.obtColTrafico ();
                    model.traficoService.listarColtrafico2 (col);
                    
                    consulta = model.traficoService.getConsulta (configuracion, filtro, clas);
                   // //System.out.println("consulta---->"+consulta);
                    model.traficoService.listarCamposTrafico (linea);
                    logger.info("Tamano del vector de campos trafico "+model.traficoService.obtCamposTrafico().size());
                    colum = model.traficoService.obtenerCampos (linea);
                    model.traficoService.generarTabla (consulta, colum);
                }
                logger.info ("RUTA"+next);
                next= "/jsp/trafico/controltrafico/vertrafico2.jsp?marco=no&linea="+linea+"&conf="+configuracion+"&clas="+clas+"&var1="+var1+"&var2="+var2+"&var3="+var3+"&filtroP="+filtroP;
            }
            else{
                next = "/jsp/trafico/controltrafico/notfound.jsp";
            }
        }
        catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
