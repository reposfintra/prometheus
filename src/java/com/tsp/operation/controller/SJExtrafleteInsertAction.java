/*
 * ProveedoresInsertAction.java
 *
 * Created on 19 de Julio de 2005, 01:34 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import com.tsp.exceptions.*;
/**
 *
 * @author  Henry
 */
public class SJExtrafleteInsertAction extends Action{
    
    /** Creates a new instance of ProveedoresInsertAction */
    public SJExtrafleteInsertAction() {
    }
    
    public void run() throws ServletException {
        String next ="/extraflete/extraflete.jsp?";
        //String ruta          = request.getParameter("ruta");        
        String std_job       = request.getParameter("standard");
        String codcli        = "000"+std_job.substring(0,3);
        String codextra      = request.getParameter("extraflete").substring(2);
        String vlr_costo     = request.getParameter("vlr_costo");
        String moneda_costo  = request.getParameter("moneda_costo");
        String vf_costo      = request.getParameter("vf_costo");
        String reemb         = request.getParameter("reembolsable");
        String vlr_ingreso   = request.getParameter("vlr_ingreso");
        String moneda_ingre  = request.getParameter("moneda_ingreso");
        String vf_ingreso    = request.getParameter("vf_ingreso");
        String clase         = request.getParameter("clase_valor");
        String porcentaje    = request.getParameter("porcentaje");
        /*HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");*/            
        SJExtraflete s = new SJExtraflete();
        s.setCod_extraflete(codextra);
        s.setCodcli(codcli);
        s.setStd_job_no(std_job);
        if(vlr_costo.equals(""))
            vlr_costo="0.00";
        s.setValor_costo(Double.parseDouble(vlr_costo));
        s.setMoneda_costo(moneda_costo);
        s.setVf_costo(vf_costo);
        s.setReembolsable(reemb);
        if(vlr_ingreso.equals(""))
            vlr_ingreso="0.00";
        s.setValor_ingreso(Double.parseDouble(vlr_ingreso));
        s.setMoneda_ingreso(moneda_ingre);
        s.setVf_ingreso(vf_ingreso);
        s.setClase_valor(clase);
        if(porcentaje.equals(""))
            porcentaje="0";
        s.setPorcentaje(Float.parseFloat(porcentaje));
        //other        
        String resp = "opcion=CL"+codcli+"ST000000RT000-000EF"+codextra+"&ruta=&men=";
        try{ 
            if(!model.sjextrafleteService.existeSJExtraflete(codcli,std_job,codextra)){
                if( model.tablaGenService.obtenerInfoTablaGen ("AUTEXFLETE", codcli ) != null && model.tablaGenService.obtenerInfoTablaGen ("AUTEXFLETE", codcli ).size ()>0 ){
                    model.sjextrafleteService.insertarSJExtraflete(s);
                    next = next+resp+"OK";
                }
                else
                    next = next+resp+"AUTO";
            } else {
                next = next+resp+"ERROR";
            }           
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
