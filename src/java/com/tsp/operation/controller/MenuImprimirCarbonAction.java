package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;

import com.tsp.pdf.DocumentoUnicoTransportePDF;

public class MenuImprimirCarbonAction extends Action{
    
    public synchronized void run() throws ServletException, InformationException {
        
        try{
            String comentario="";
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String Login    = usuario.getLogin();
            String Proyecto = usuario.getProject();
            String Distrito = usuario.getDstrct();
            String usuarioName = usuario.getNombre();
            int entro = 0;
            if(!Login.equals("") ){
                String Agencia     = usuario.getId_agencia();
                
                request.setAttribute("Printer",null);
                String opcionPrint= request.getParameter("opcion");
                int cantidad     = 0;
                int swWindow     = 0;
                int total        = Integer.parseInt(request.getParameter("total"));
                String planilla    = request.getParameter("planilla");
                //if(model.RemisionOCSvc.ExistSerie() ){
                if(model.RemisionOCSvc.getList()!=null && total>0){
                    
                    //--- obtenemos las remisiones seleccionadas
                    if(total>0 && opcionPrint.equals("not")){
                        for(int i =1;i<=total;i++){
                            int parametro = Integer.parseInt(request.getParameter("parametro"+i));
                            model.RemisionOCSvc.active(parametro);
                        }
                    }
                    
                    //--- Si es opcion de imprimir
                    if(opcionPrint.equals("yes")){
                        int id = Integer.parseInt( request.getParameter("id") );
                        Remision  remision = model.RemisionOCSvc.getRemisionPrinter(id);
                        if(remision!=null){
                            request.setAttribute("Printer", remision);
                            
                            if( total==1 ){
                                Vector v = new Vector();
                                v.add(remision);
                                DocumentoUnicoTransportePDF d = new DocumentoUnicoTransportePDF();
                                d.imprimirRemisiones(v);
                            }
                            
                            //--- Actualizamos el campo printer_date de la oc
                            model.RemisionOCSvc.updateRemision(remision.getRemision(),remision.getDistrito());
                            //---  actualizamos la serie
                            model.RemisionOCSvc.updateSeries(remision.getDistrito(), remision.getAgencia(), remision.getBanco(),remision.getAgeBanco(), remision.getCuentaBanco() , Login);
                            Series serie = model.RemisionOCSvc.getSerie(remision.getDistrito(), remision.getAgencia(), remision.getBanco(), remision.getAgeBanco(), remision.getCuentaBanco());
                            if(serie!=null){
                                int ultimo= serie.getLast_number();
                                int tope  = Integer.parseInt(serie.getSerial_fished_no());
                                if(ultimo>=tope)
                                    model.RemisionOCSvc.finallySeries(remision.getDistrito(), remision.getAgencia(), remision.getBanco(), remision.getAgeBanco(), remision.getCuentaBanco(), Login);
                            }
                            
                            double total1=0;
                            //--- verificamos si la planilla tiene asociado otros conceptos de egreso, siendo asi,
                            List listadoEgresos=model.RemisionOCSvc.searchMovimiento(remision.getRemision(),remision.getDistrito(), remision.getAgencia());
                            if(listadoEgresos!=null){
                                Iterator itEgresos = listadoEgresos.iterator();
                                while(itEgresos.hasNext()){
                                    AnticipoPlanilla anticipo = (AnticipoPlanilla)itEgresos.next();
                                    total1+= anticipo.getValor();
                                    model.RemisionOCSvc.updateMOVOC(remision.getDistrito(), remision.getAgencia(), remision.getEgreso(), anticipo.getConcepto(), remision.getRemision() , Login);
                                    if(anticipo.getMoneda().toUpperCase().equals("PES") )
                                        model.RemisionOCSvc.updateDetalleEgreso(remision.getDistrito(), remision.getBanco(),remision.getAgeBanco(),remision.getEgreso(),"0"+anticipo.getConcepto(),anticipo.getConcepto(), remision.getRemision(),anticipo.getValor(),0,anticipo.getMoneda() ,Login);
                                    else
                                        model.RemisionOCSvc.updateDetalleEgreso(remision.getDistrito(), remision.getBanco(),remision.getAgeBanco(),remision.getEgreso(),"0"+anticipo.getConcepto(),anticipo.getConcepto(), remision.getRemision(),0,anticipo.getValor(), anticipo.getMoneda() ,Login);
                                }
                            }
                            
                            //---  actulizamos el egreso
                            model.RemisionOCSvc.updateEgreso(remision.getDistrito(), remision.getBanco(), remision.getAgeBanco(), remision.getEgreso(),"01", remision.getNit(), remision.getConductor(), remision.getAgencia(), total1,0, remision.getTipoMoneda(),Login);
                            
                            //--- actualizamos la lista
                            model.RemisionOCSvc.delete(remision);
                        }
                        entro = 1;
                        
                    }
                    
                    
                    //--- la accion de cancelar  la impresion
                    if(opcionPrint.equals("cancel")){
                        int id = Integer.parseInt( request.getParameter("id") );
                        model.RemisionOCSvc.cancel(id);
                    }
                    
                    
                    //--- obtenemos la lista de impresio
                    List listaImpresion = model.RemisionOCSvc.getListImpresion();
                    Remision remision = null;
                    if(listaImpresion!=null){
                        if(listaImpresion!=null && listaImpresion.size()>0){
                            Iterator itPrint = listaImpresion.iterator();
                            while(itPrint.hasNext()){
                                remision = (Remision)itPrint.next();
                                swWindow=1;
                                break;
                            }
                        }
                    }
                    
                    //--- verificamos que exista remisiones en la lista general
                    if(model.RemisionOCSvc.getList()==null || model.RemisionOCSvc.getList().size()==0)
                        comentario="(Action) No hay remisiones para ser impresas...";
                    
                    
                    //--- cantidad de elementos para ser impresos
                    cantidad =(listaImpresion!=null)?listaImpresion.size():0;
                    
                    //--- Resetiamos el objeto remision del Servis para luego ser capturado en la jsp para ser impreso
                    if(swWindow==1){
                        String egreso=remision.getEgreso();
                        Series serie = model.RemisionOCSvc.getSerie(remision.getDistrito(), remision.getAgencia(), remision.getBanco(), remision.getAgeBanco(), remision.getCuentaBanco());
                        if(serie!=null)
                            egreso = serie.getPrefijo() + String.valueOf(serie.getLast_number()+1);
                        remision.setEgreso(egreso);
                        model.RemisionOCSvc.setRemision(remision);
                        request.setAttribute("remisionPrint",remision);
                    }
                    
                }
                else
                    comentario= model.RemisionOCSvc.searchRemisiones(Proyecto,Distrito,Agencia,usuarioName,planilla);
                //}
                //else
                //  comentario="No hay registros en la Serie banco";
                
                
                // LLamamos la pagina JSP
                String next = "/remision/RemisionesPlanilla.jsp?comentario="+comentario +"&total="+cantidad+"&ventana="+String.valueOf(swWindow)+"&usuario="+Login;;
                /*if(entro ==1){
                    next = "/despacho/InicioMasivo.jsp";
                }*/
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response);
                
            }
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
    }// fin run
    
    
}//fin clase