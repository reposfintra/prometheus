/********************************************************************
 *      Nombre Clase.................   FacturaCargarPAction.java
 *      Descripci�n..................   Action que se encarga de abrir la busqueda de proveedores
 *      Autor........................   David Lamadrid
 *      Fecha........................   12 de octubre de 2005, 06:36 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
//logger
import org.apache.log4j.Logger;

public class FactRecurrCargarPAction extends Action{
    
    Logger log = Logger.getLogger(this.getClass());
    
    
    /** Creates a new instance of FacturaCargarP */
    public FactRecurrCargarPAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            
             //ivan 21 julio 2006
            HttpSession session = request.getSession();
            Usuario usuario     = (Usuario) session.getAttribute("Usuario");
            
            String maxfila = request.getParameter("maxfila");
            String x=(request.getParameter("x")==null?"":request.getParameter("x"));
            String proveedor=(request.getParameter("proveedor")==null?"":request.getParameter("proveedor"));
            String banco=(request.getParameter("banco")==null?"":request.getParameter("banco"));
            String sucursal=(request.getParameter("sucursal")==null?"":request.getParameter("sucursal"));
            String subtotal=(request.getParameter("subtotal")==null?"":request.getParameter("subtotal"));
            int plazo = Integer.parseInt((request.getParameter("plazo")==null?"0":request.getParameter("plazo")));
            String moneda_banco = (request.getParameter("moneda_banco")!=null)?request.getParameter("moneda_banco"):"";
            String agenciaBanco = (request.getParameter("agenciaBanco")!=null)?request.getParameter("agenciaBanco"):"";
            String beneficiario = (request.getParameter("beneficiario")!=null)?request.getParameter("beneficiario"):"";
            String hc = (request.getParameter("hc")!=null)?request.getParameter("hc"):"";
            
            /**************************************************************************************************************/
            int num_cuotas = (request.getParameter("ncuotas")!=null && request.getParameter("ncuotas").length()!=0? Integer.parseInt(request.getParameter("ncuotas")) : 0);
            String fecha_inicio_transfer = (request.getParameter("fecha_inicio")!=null?request.getParameter("fecha_inicio"):"");
            String fecha_inicio_transferO = (request.getParameter("fecha_inicio2")!=null?request.getParameter("fecha_inicio2"):"");
            String frecuencia = (request.getParameter("CabRfte")!=null?request.getParameter("frecuencia"):"");
            
            log.info("NCUOTAS: " + num_cuotas);
            log.info("FECHA INICIO TRANSFER: " + fecha_inicio_transfer);
            log.info("FRECUENCIA: " + frecuencia);
            /**************************************************************************************************************/
            
            String agenteRet = "";
            String rica      = "";
            String rfte      = "";
            String riva      = "";
            
            model.tblgensvc.searchDatosTablaArea();
            model.factrecurrService.obtenerNumeroFXP(proveedor);
            model.servicioBanco.loadSusursales(banco);
            
            CXP_Doc factura = new CXP_Doc();
            factura = model.factrecurrService.getFactura();
            factura.setProveedor(proveedor);
            factura.setBanco(banco);
            factura.setSucursal(sucursal);
            factura.setPlazo(plazo);
            factura.setMoneda_banco(moneda_banco);
            factura.setAgenciaBanco(agenciaBanco);
            factura.setBeneficiario(beneficiario);
            factura.setHandle_code(hc);
            
            /**************************************************************/
            factura.setNum_cuotas(num_cuotas);
            factura.setFecha_inicio_transfer(fecha_inicio_transfer);
            factura.setFrecuencia(frecuencia);
            factura.setFecha_inicio_transferO(fecha_inicio_transferO);
            /**************************************************************/
            
            
            Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
            if( o_proveedor!=null ){
                agenteRet = o_proveedor.getC_agente_retenedor();
                rica      = o_proveedor.getC_autoretenedor_ica();
                rfte      = o_proveedor.getC_autoretenedor_rfte();
                riva      = o_proveedor.getC_autoretenedor_iva();
            }
             if(!agenteRet.equals("S")){
                
                if(rica.equals("N")){
                    factura.setRica("");
                }
                if( rfte.equals("N")){
                    factura.setRfte("");
                }
                if( riva.equals("N")){
                    factura.setRiva("");
                }
            }else{
                factura.setRica("");
                factura.setRfte("");
                factura.setRiva("");
            }
            
            model.factrecurrService.setFactura(factura);
            Vector vItems = model.factrecurrService.getVecRxpItemsDoc();
            Vector vitemsModif = new Vector();
            for(int i=0;i<vItems.size();i++){
                CXPItemDoc item = (CXPItemDoc)vItems.elementAt(i);
                if(item.getDescripcion()!=null){
                    
                    //System.out.println(item.getVlr()+"---"+item.getDescripcion());
                    Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos();
                    Vector vImpuestosPorItem = item.getVItems();
                    Vector vImpPorItem= new Vector();
                    //Ivan 24 julio 2006.
                    String codIva ="";
                    for(int a=0;a<vTipoImp.size();a++){
                        String Impuesto = (String)vTipoImp.elementAt(a);
                        CXPImpItem impuestoItem = (CXPImpItem)vImpuestosPorItem.elementAt(a);
                        //impuestoItem.setCod_impuesto("");//Agrege esto
                        //item.setIva("");
                        
                        
                      if(Impuesto.equals("IVA"))
                            codIva = (impuestoItem.getCod_impuesto()!=null)?impuestoItem.getCod_impuesto():"";
                            
                            if(!agenteRet.equals("S")){
                                
                                if(Impuesto.equals("RICA") && rica.equals("N")){
                                    impuestoItem.setCod_impuesto("");
                                }else if(Impuesto.equals("RFTE") && rfte.equals("N")){
                                    impuestoItem.setCod_impuesto("");
                                }else if(Impuesto.equals("RIVA") && riva.equals("N")){
                                    impuestoItem.setCod_impuesto("");
                                }else if(Impuesto.equals("RIVA") && riva.equals("S") && !codIva.equals("")){
                                    Tipo_impuesto datos =  model.TimpuestoSvc.buscarImpuestoPorCodigos( "IVA", codIva, usuario.getDstrct(),"");
                                    if(datos!= null){
                                       impuestoItem.setCod_impuesto(datos.getCod_cuenta_contable());
                                       item.setIva(String.valueOf(datos.getPorcentaje1()));
                                    }
                                }
                                
                                
                                
                            }else if(!Impuesto.equals("IVA")){
                                impuestoItem.setCod_impuesto("");
                            }
                        vImpPorItem.add(impuestoItem);
                        
                    }
                    item.setVItems( vImpPorItem);
                    item.setVCopia(vImpPorItem);
                    vitemsModif.add(item);
                }else{
                    vitemsModif.add(item);
                }
            }
            model.factrecurrService.setVecRxpItemsDoc(vitemsModif);
            int num_items=0;
            
            try {
                num_items  = Integer.parseInt(""+ request.getParameter("num_items"));
            }
            catch(java.lang.NumberFormatException e) {
                plazo=0;
                num_items=1;
            }
            
            String next = "/jsp/cxpagar/facturasrec/facturaP.jsp?op=cargarB&num_items="+num_items+"&x="+x+"&subtotal="+subtotal+"&maxfila"+maxfila;
            log.info("NEXT: "+next);
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
    }
    
}
