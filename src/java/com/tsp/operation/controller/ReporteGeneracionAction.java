/********************************************************************
 *      Nombre Clase.................   ReporteGeneracionAction.java
 *      Descripci�n..................   Genera el reporte de oportunidad
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   27.12.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteGeneracionAction extends Action{
    
    /** Creates a new instance of InformacionPlanillaAction */
    public ReporteGeneracionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter("FechaI");
        String fechaF = request.getParameter("FechaF");      
                
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
        String FechaFormated1 = s1.format(d);
        
        String next = "/jsp/trafico/reportes/ReporteGeneracion.jsp?msg=" +
                "El Reporte se ha generado exitosamente en ReporteDeGeneracion_" +
                FechaFormated1 + ".xls";
        
        try{
            
            model.rmtService.iniciarReporteGeneracion(usuario.getLogin(), fechaI, fechaF);            
            
            Vector rep = model.rmtService.obtenerReporteGeneracion();
            Vector ttls = model.rmtService.totalesReporteDeGeneracion();
            
            if( rep.size() == 0){
                next = "/jsp/trafico/reportes/ReporteGeneracion.jsp?msg=" +
                        "La consulta no gener� ning�n resultado.";
            } else{                
                ReporteGeneracionTh hilo = new ReporteGeneracionTh();
                hilo.start(model, rep, ttls, usuario.getLogin(), fechaI, fechaF);
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        //////System.out.println("next---------- "+ next);
        this.dispatchRequest(next);
    }
    
}
