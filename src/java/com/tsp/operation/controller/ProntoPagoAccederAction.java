/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

/**
 *
 * @author Alvaro
 */

import java.sql.SQLException;
import java.util.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;

import com.tsp.operation.model.Model;
import com.tsp.util.Util;

import com.tsp.operation.model.beans.POIWrite;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


import java.io.*;
import com.tsp.util.LogWriter;



/**
 *
 * @author Alvaro
 */
public class ProntoPagoAccederAction extends Action {


    public ProntoPagoAccederAction() {
    }

    public void run() throws javax.servlet.ServletException {

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();

        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        String aceptarDisable = "";

        try {

            //  EVENTO  :  CREAR_ORDEN_SERVICIO de ordenServicio.jsp

            if (evento.equalsIgnoreCase("CREAR_ORDEN_SERVICIO")) {

                // HOrdenServicio hilo = new HOrdenServicio();
                // hilo.start(model, usuario,distrito);

                // Fase1(model,usuario,distrito);
                OrdenServicio(model,usuario,distrito) ;


                aceptarDisable = "S";

                next  = "/jsp/finanzas/prontoPago/ordenServicio.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de crear orden de servicio. Ver log de proceso para ver su finalizacion";

            } // Fin de CREAR_ORDEN_SERVICIO




            //  EVENTO  :  CREAR_EGRESO egresoGeneracion.jsp

            if (evento.equalsIgnoreCase("CREAR_EGRESO")) {

                //HEgresoGenerar hilo = new HEgresoGenerar();
                //hilo.start(model, usuario,distrito);

                crearEgreso(model,usuario,distrito);

                aceptarDisable = "S";

                next  = "/jsp/finanzas/prontoPago/egresoGeneracion.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de crear egresos y cxp a TSP. Ver log de proceso para ver su finalizacion";


            } // Fin de CREAR_EGRESO

            if (evento.equalsIgnoreCase("CREAR_INGRESO")) {

                HIngresoEgresoTSPGenerar hilo = new HIngresoEgresoTSPGenerar();
                hilo.start(model, usuario,distrito);

                // crearIngreso(model,usuario,distrito); // Colocado en el hilo

                aceptarDisable = "S";

                next  = "/jsp/finanzas/prontoPago/ingresoGeneracion.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de crear ingresos de egresos de TSP. Ver log de proceso para ver su finalizacion";


            } // Fin de CREAR_INGRESO

            next += msj ;
            this.dispatchRequest(next);

        }catch (Exception e) {
            e.printStackTrace();
        }

    }

public void OrdenServicio(Model model,Usuario usuario,String distrito){

        try {

            Vector comandos_sql =new Vector();

            String login = usuario.getLogin();

            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();

            DateFormat formato_periodo;
            formato_periodo = new SimpleDateFormat("yyyyMM");
            String periodo  = formato_periodo.format(fechaActual);

            DateFormat formato_fecha;
            formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
            String  fecha = formato_fecha.format(fechaActual);

            // ANTICIPOS
            // Localizar los registros de anticipos pronto pago relacionados a anticipos de planilla

            model.prontoPagoService.buscaProntoPagoEfectivo();
            List listaProntoPago = model.prontoPagoService.getProntoPago();
            ordenServicioAnticipo (listaProntoPago,comandos_sql, distrito,login, creation_date, fecha, periodo);

            // EXTRACTOS
            // Localizar los registros de anticipos pronto pago relacionados a pagos por extracto

            model.prontoPagoService.buscaProntoPago();
            listaProntoPago = model.prontoPagoService.getProntoPago();
            ordenServicioAnticipo (listaProntoPago,comandos_sql, distrito,login, creation_date, fecha, periodo);


            // ABONO A PRESTAMOS EN SU TOTALIDAD
            // Localizar los registros de anticipos pronto pago relacionados a pagos por extracto y abononados en su totalidad a prestamos
            model.prontoPagoService.buscaProntoPagoPrestamo();
            listaProntoPago = model.prontoPagoService.getProntoPago();
            ordenServicioAnticipo (listaProntoPago,comandos_sql, distrito,login, creation_date, fecha, periodo);

        }catch (Exception ex){
            ex.printStackTrace();
            try{

                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error en el proceso OrdenServicio ...\n"  + e.getMessage());
            }
        }
}

public List creaProntoPagoDetalle(ProntoPagoDetalle prontoPagoDetalle, ProntoPago prontoPago){


    List listaProntoPagoDetalle = null;
    listaProntoPagoDetalle = new LinkedList();
    prontoPagoDetalle.setNit( prontoPago.getNit() );
    prontoPagoDetalle.setNumero_operacion (prontoPago.getNumero_operacion());
    prontoPagoDetalle.setTipo_documento("PL");
    prontoPagoDetalle.setClase(prontoPago.getTipo_operacion());



    prontoPagoDetalle.setDocumento( prontoPago.getNumero_operacion()  ) ;
    prontoPagoDetalle.setValor_calculado(prontoPago.getVlr_extracto());
    prontoPagoDetalle.setValor_registrado (prontoPago.getVlr_extracto());
    prontoPagoDetalle.setDiferencia_interna (0.0);
    prontoPagoDetalle.setPlaca (prontoPago.getSupplier());

    listaProntoPagoDetalle.add(prontoPagoDetalle);

    return listaProntoPagoDetalle;


}

public void defineTipo_operacion (ProntoPago prontoPago,String distrito)throws SQLException {


    // Pago de extractos
    System.out.println("CONCEPT_CODE_"+prontoPago.getConcept_code());
    if (prontoPago.getConcept_code().equalsIgnoreCase("50") ) {
        prontoPago.setTipo_operacion("EXT");
        prontoPago.setCuenta_anticipo("23050201");
    }
    // Pago de anticipos
    else if (prontoPago.getConcept_code().equalsIgnoreCase("01")) {


       System.out.println("getCon_ant_tercero_"+prontoPago.getCon_ant_tercero());
        // prontoPago.getTipo_cuenta().equalsIgnoreCase("EF")
        if  (prontoPago.getCon_ant_tercero().equalsIgnoreCase("01") ) {
            // Pago de anticipos por efectivo
            prontoPago.setTipo_cuenta("EF");
            prontoPago.setTipo_operacion("AEF");
            prontoPago.setCuenta_anticipo("23050302");
        }
        else if (prontoPago.getCon_ant_tercero().equalsIgnoreCase("02")) {
            // Pago de anticipos por efectivo
            prontoPago.setTipo_operacion("AET");
            prontoPago.setCuenta_anticipo("23050301");
        }
    }

    else if (prontoPago.getConcept_code().equalsIgnoreCase("10")) {
        // Pago de anticipos por gasolina
        prontoPago.setTipo_operacion("AGA");
        prontoPago.setCuenta_anticipo("23050303");
    }

    // Define el numero de la factura que se le pagara al propietario
    String proveedor = prontoPago.getNit();
    String tipo_documento = "FAP";

    String secuencia = prontoPago.getNumero_operacion();
    String documento = "";

    try {

        if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")){
            documento = "E"+secuencia;
        }
        else {
            documento = model.prontoPagoService.buscarDocumento(distrito,proveedor,tipo_documento, secuencia);
        }
        // Actualiza el pronto pago con el numero de la factura con la que se le paga al proveedor
        prontoPago.setFactura_cxp(documento);

    }catch(Exception e){
        e.printStackTrace();
        throw new SQLException("ERROR DURANTE LA DETERMINACION DEL NUMERO DE FACTURA AL PROPIETARIO. \n " + e.getMessage());
    }


}

public void defineTotales_extractoDetalle(ProntoPago prontoPago) throws SQLException {

    try {
        // Define totales por default para anticipos por efectivo, transferencia o gasolina
        prontoPago.setVlr_ext_detalle_calculado(prontoPago.getVlr_extracto());
        prontoPago.setVlr_ext_detalle_registrado(prontoPago.getVlr_extracto());
        prontoPago.setVlr_ext_detalle_diferencia(0.00);
        prontoPago.setVlr_diferencia_ext_ant(0.00);


        if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")) {

            String secuencia = prontoPago.getNumero_operacion();
            TotalExtracto totalExtracto = (TotalExtracto) model.prontoPagoService.getTotalExtracto(secuencia);
            prontoPago.setVlr_ext_detalle_calculado(totalExtracto.getVlr_ext_detalle_calculado());
            prontoPago.setVlr_ext_detalle_registrado(totalExtracto.getVlr_ext_detalle_registrado());
            prontoPago.setVlr_ext_detalle_diferencia(totalExtracto.getVlr_ext_detalle_diferencia());
            prontoPago.setVlr_diferencia_ext_ant(totalExtracto.getVlr_ext_detalle_calculado() - prontoPago.getVlr_extracto());
        }
    }
    catch (Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA REGISTROS DETALLADOS DEL EXTRACTO. \n " + e.getMessage());
    }


}

public boolean validaError(ProntoPago prontoPago,List listaProntoPagoDetalle){

     // Validar si todo el documento es consistente

     ProntoPagoDetalle prontoPagoDetalle = new ProntoPagoDetalle();

     boolean error = false;

     String operacion = "";
     if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")) {
         operacion = "Extracto " ;}
     else {
         operacion = "Anticipo ";
     }


     String descripcionError = operacion + prontoPago.getNumero_operacion() + "\n";
     // Valida si vlr + retefuente + reteica + impuestos = vlr_ppa_item para cada documento incluido en el extracto
     if (prontoPago.getVlr_ext_detalle_diferencia() != 0 ) {
         error = true;
         descripcionError += "Items de extracto detalle no suman igual a la columna de total por item (vlr_ppa_item) \n";
     }
     // Valida si valor total del extracto detalle = a valor del extracto en anticipos pagos terceros
     if (prontoPago.getVlr_diferencia_ext_ant()!= 0) {
         error = true;
         descripcionError += "Valor total del extracto detalle no suma igual al valor del extracto registrado en anticipos pagos terceros \n";
     }
     // Valida cual de los documentos en el extracto detalle no suma a su total
     if (listaProntoPagoDetalle.size() != 0){

        Iterator it1 = listaProntoPagoDetalle.iterator();
         while (it1.hasNext()) {
             prontoPagoDetalle = (ProntoPagoDetalle)it1.next();
             if (prontoPagoDetalle.getDiferencia_interna() != 0) {
                 error = true;
                 descripcionError += "Detalle diferencia \n";
                 descripcionError += prontoPagoDetalle.getTipo_documento() + " ";
                 descripcionError += prontoPagoDetalle.getClase() + " ";
                 descripcionError += prontoPagoDetalle.getDocumento() + " ";
                 descripcionError += prontoPagoDetalle.getValor_calculado() + " ";
                 descripcionError += prontoPagoDetalle.getValor_registrado() + " ";
                 descripcionError += prontoPagoDetalle.getDiferencia_interna() + "\n";
             }
         }
     }
     else {
         error = true;
         descripcionError += "No existe detalle para el extracto \n";
     }
     descripcionError += "\n";
     System.out.println(descripcionError);
     return error;

}

public Vector localizarBanco(ProntoPago prontoPago) throws SQLException {

    Vector banco = new Vector();         // Elementos del vector:  0=branch_code  ; 1=bank_account_no  ; 2=codigo_cuenta
                                                 //                        3=currency


    try {

         // Localiza informacion de banco para cuando se trata de extractos

         String tipo_operacion =  prontoPago.getTipo_operacion();
         System.out.println("TIPO OPERACION_"+prontoPago.getTipo_operacion()+"_");
         System.out.println("ID_"+prontoPago.getId());
         System.out.println("cuenta transferencia==>"+prontoPago.getCuenta_transferencia()+"banco transfer_"+prontoPago.getBanco_transferencia()+"tipo cuenta=>"+prontoPago.getTcta_transferencia() );
         if ( (tipo_operacion.equalsIgnoreCase("EXT"))  |
              (tipo_operacion.equalsIgnoreCase("AET"))   )  {
                  //System.out.println("cuenta transferencia==>"+prontoPago.getCuenta_transferencia()+"banco transfer_"+prontoPago.getBanco_transferencia()+"tipo cuenta=>"+prontoPago.getTcta_transferencia() );
             banco = model.prontoPagoService.getBanco(prontoPago.getCuenta_transferencia(),
                                                      prontoPago.getBanco_transferencia(),
                                                      prontoPago.getTcta_transferencia() );
         }

         // Localiza informacion de banco para cuando se trata de anticipos en efectivo

         if (prontoPago.getTipo_operacion().equalsIgnoreCase("AEF")) {
             System.out.println("agencia==>"+prontoPago.getAgency_id());
             banco = model.prontoPagoService.getBancoAnticipo(prontoPago.getAgency_id());
         }

         // Localiza informacion de banco para cuando se trata de anticipos en gasolina
         if (prontoPago.getTipo_operacion().equalsIgnoreCase("AGA")) {
             banco.add("ESTACION");
             banco.add("GASOLINA");
             banco.add("23050118");
             banco.add("PES");
         }

         String cuenta_contable = (String) banco.get(2);
         prontoPago.setCuenta_contable_banco(cuenta_contable ) ;

         String branch_code  = (String) banco.get(0);
         prontoPago.setBanco(branch_code);

         String bank_account_no  = (String) banco.get(1);
         prontoPago.setSucursal(bank_account_no);


    }
    catch (Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CODIGO DE CUENTA DEL BANCO POR DONDE SE TRANSFIEREN LOS EFECTIVOS. \n " + e.getMessage());
    }


     return banco;
}

public void localizarCmc(ProntoPago prontoPago,String distrito) throws SQLException {

    Cmc cmc = null;
    cmc = new Cmc();
    try {

         // Localiza informacion de cmc
        String tipoCmc = "";
         if (prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")) {
             tipoCmc = "EX";
         }
         else if(prontoPago.getTipo_operacion().equalsIgnoreCase("AEF")) {
             tipoCmc = "AE";
         }
         else if(prontoPago.getTipo_operacion().equalsIgnoreCase("AET")) {
             tipoCmc = "AT";
         }
         else if(prontoPago.getTipo_operacion().equalsIgnoreCase("AGA")) {
             tipoCmc = "AG";
         }

         cmc = model.prontoPagoService.getCmc(distrito, prontoPago.getTipo_operacion(),tipoCmc);
         prontoPago.setCmc(tipoCmc);
         prontoPago.setCuenta_os(cmc.getCuenta());
         prontoPago.setDbcr_os(cmc.getDbcr());
         prontoPago.setSigla_comprobante_os(cmc.getSigla_comprobante());
    }
    catch (Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE INFORMACION DEL CMC. \n " + e.getMessage());
    }


}

public void creaComprobante(ProntoPago prontoPago,Vector comandos_sql,
                            ProntoPagoDetalle  prontoPagoDetalle,
                            String distrito, String creation_date,
                            String fecha) throws SQLException{
    try {

        String comandoSQL = "";

         // Crear la cabecera del comprobante contable de la Orden de servicio

         Comprobante comprobante = new Comprobante();

         comprobante.setReg_status("");
         comprobante.setDstrct(distrito);
         comprobante.setTipodoc(prontoPago.getSigla_comprobante_os());
         comprobante.setNumdoc(prontoPago.getNumero_operacion()  );
         comprobante.setGrupo_transaccion(prontoPago.getGrupo_transaccion());
         comprobante.setSucursal("OP");
         comprobante.setPeriodo(prontoPago.getPeriodo_os());
         comprobante.setFechadoc(fecha);
         comprobante.setDetalle("Contabiliza " + prontoPago.getTipo_operacion()+ " " + prontoPago.getNumero_operacion() );
         comprobante.setTercero(prontoPago.getNit());
         comprobante.setTotal_debito(prontoPago.getVlr_extracto());
         comprobante.setTotal_credito(prontoPago.getVlr_extracto()) ;
         comprobante.setTotal_items(3);
         comprobante.setMoneda("PES");
         comprobante.setFecha_aplicacion("0099-01-01 00:00:00");
         comprobante.setAprobador("ADMIN");
         comprobante.setLast_update(creation_date);
         comprobante.setUser_update("ADMIN");
         comprobante.setCreation_date(creation_date);
         comprobante.setCreation_user("ADMIN");
         comprobante.setBase("COL");
         comprobante.setUsuario_aplicacion("");
         comprobante.setTipo_operacion("");
         comprobante.setMoneda_foranea("");
         comprobante.setValor_for(0.00);
         comprobante.setRef_1(prontoPago.getTipo_operacion());
         comprobante.setRef_2(prontoPago.getNumero_operacion()  );

         comandoSQL = model.prontoPagoService.setComprobante(comprobante);
         comandos_sql.add(comandoSQL);

         // Crear el detalle del comprobante

         crearComprobanteDetalle(prontoPago, prontoPagoDetalle, comandos_sql, distrito,creation_date);
    }
    catch (Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA GRABACION DEL COMPROBANTE CONTABLE. \n " + e.getMessage());
    }
}

public void ordenServicioAnticipo (List listaProntoPago,Vector comandos_sql, String distrito, String login,
                                   String creation_date, String fecha, String periodo)throws SQLException {

    try {

        if (listaProntoPago.size() != 0){

            ProntoPago prontoPago =  new ProntoPago();
            ProntoPagoDetalle prontoPagoDetalle =  new ProntoPagoDetalle();

            Iterator it = listaProntoPago.iterator();
            while (it.hasNext()) {
                 // Un registro de orden de servicio para un anticipo pago tercero
                 prontoPago = (ProntoPago)it.next();
                 defineTipo_operacion (prontoPago,distrito);

                 // Crear los registros detallados del pronto pago

                 List listaProntoPagoDetalle = null;

                 if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT") ) {
                     // Detalle de orden de servicio  para un anticipo pago tercero proveniente de extracto detalle
                     model.prontoPagoService.buscaProntoPagoDetalle(prontoPago.getNumero_operacion());
                     listaProntoPagoDetalle = model.prontoPagoService.getProntoPagoDetalle();
                 }
                 else {
                     // Detalle de orden de servicio  para un anticipo pago tercero proveniente de anticipos efectivo, transferencia, gasolina
                     listaProntoPagoDetalle = creaProntoPagoDetalle(prontoPagoDetalle, prontoPago );
                 }


                 defineTotales_extractoDetalle(prontoPago);
                 if(!validaError(prontoPago,listaProntoPagoDetalle) ) {

                     creaOrdenServicio(prontoPago,distrito,periodo,comandos_sql,
                                       login, creation_date, listaProntoPagoDetalle, fecha );
                 }
             }
        }

    }catch (Exception ex){
        ex.printStackTrace();
        try{

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
        }catch (Exception e){
            System.out.println("Error al elaborar la orden de servicio basado en pagos por anticipos ...\n"  + e.getMessage());
        }
    }

}

public void creaOrdenServicio(ProntoPago prontoPago,String distrito, String periodo,
                              Vector comandos_sql, String login, String creation_date,
                              List listaProntoPagoDetalle ,
                              String fecha) throws SQLException {

    try {

         String comandoSQL = "";
         ProntoPagoDetalle prontoPagoDetalle = null;
         prontoPagoDetalle = new ProntoPagoDetalle();

         Vector banco = new Vector();         // Elementos del vector:  0=branch_code  ; 1=bank_account_no  ; 2=codigo_cuenta
                                              //                        3=currency

         String cuentaTransferencia = prontoPago.getCuenta_transferencia();
         banco = localizarBanco(prontoPago);
         localizarCmc(prontoPago,distrito);
         prontoPago.setGrupo_transaccion(model.prontoPagoService.getGrupoTransaccion());
         prontoPago.setPeriodo_os(periodo);

         // Procesa transaccion

         // Grabar un registro en orden de servicio
         String numeroOS = model.prontoPagoService.buscarDocumentoOS(distrito, prontoPago.getTipo_operacion(), prontoPago.getNumero_operacion());
         prontoPago.setNumero_operacion(numeroOS);
         System.out.println("[numeroOS_"+numeroOS+"]");
         comandoSQL = model.prontoPagoService.setOrdenServicio(prontoPago, distrito, login, creation_date);
         comandos_sql.add(comandoSQL);
         // Actualiza anticipos pagos terceros para desactivar en proximo querys
         comandoSQL = model.prontoPagoService.setAnticipo(prontoPago, "FINV", creation_date);
         comandos_sql.add(comandoSQL);

         // Grabar registros en orden de servicio detallado
         Iterator it1 = listaProntoPagoDetalle.iterator();
         while (it1.hasNext()) {
             prontoPagoDetalle = (ProntoPagoDetalle)it1.next();
             comandoSQL = model.prontoPagoService.setOrdenServicioDetalle(prontoPago, prontoPagoDetalle, distrito, login, creation_date);
             comandos_sql.add(comandoSQL);
         }

         // Crear el comprobante contable
         creaComprobante(prontoPago, comandos_sql, prontoPagoDetalle,
                         distrito, creation_date, fecha);
         // Crear la factura CxP al Propietario
         crearCxPPropietario( prontoPago, prontoPagoDetalle, comandos_sql, distrito,banco);

         // Grabando todo a la base de datos.
         model.applusService.ejecutarSQL(comandos_sql);
         comandos_sql.removeAllElements();




    }
    catch (Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL PROCESO DE GRABACION DE LA ORDEN DE SERVICIO. \n " + e.getMessage());
    }



}


public void Fase1(Model model, Usuario usuario, String distrito){

        try {

            Vector comandos_sql =new Vector();
            Vector banco = new Vector();         // Elementos del vector:  0=branch_code  ; 1=bank_account_no  ; 2=codigo_cuenta
                                                 //                        3=currency

            String comandoSQL = "";
            String login = usuario.getLogin();

            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();

            DateFormat formato_periodo;
            formato_periodo = new SimpleDateFormat("yyyyMM");
            String periodo  = formato_periodo.format(fechaActual);

            DateFormat formato_fecha;
            formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
            String  fecha = formato_fecha.format(fechaActual);

            model.prontoPagoService.buscaProntoPago();
            List listaProntoPago = model.prontoPagoService.getProntoPago();

            if (listaProntoPago.size() != 0){

                ProntoPago prontoPago =  new ProntoPago();
                ProntoPagoDetalle prontoPagoDetalle =  new ProntoPagoDetalle();

                Iterator it = listaProntoPago.iterator();
                 while (it.hasNext()) {


                     // Un registro de orden de servicio para un anticipo pago tercero
                     prontoPago = (ProntoPago)it.next();

                     // Detalle de orden de servicio  para un anticipo pago tercero proveniente de extracto detalle

                     model.prontoPagoService.buscaProntoPagoDetalle(prontoPago.getNumero_operacion());
                     List listaProntoPagoDetalle = model.prontoPagoService.getProntoPagoDetalle();

                     // Totales del extracto

                     prontoPago.setTipo_operacion("EXT");
                     String secuencia = prontoPago.getNumero_operacion();
                     TotalExtracto totalExtracto = (TotalExtracto) model.prontoPagoService.getTotalExtracto(secuencia);
                     prontoPago.setVlr_ext_detalle_calculado(totalExtracto.getVlr_ext_detalle_calculado());
                     prontoPago.setVlr_ext_detalle_registrado(totalExtracto.getVlr_ext_detalle_registrado());
                     prontoPago.setVlr_ext_detalle_diferencia(totalExtracto.getVlr_ext_detalle_diferencia());
                     prontoPago.setVlr_diferencia_ext_ant(totalExtracto.getVlr_ext_detalle_calculado() - prontoPago.getVlr_extracto());

                     // Validar si todo el documento es consistente

                     boolean error = false;
                     String descripcionError = "Extracto "+ secuencia + "\n";
                     // Valida si vlr + retefuente + reteica + impuestos = vlr_ppa_item para cada documento incluido en el extracto
                     if (prontoPago.getVlr_ext_detalle_diferencia() != 0 ) {
                         error = true;
                         descripcionError += "Items de extracto detalle no suman igual a la columna de total por item (vlr_ppa_item) \n";
                     }
                     // Valida si valor total del extracto detalle = a valor del extracto en anticipos pagos terceros
                     if (prontoPago.getVlr_diferencia_ext_ant()!= 0) {
                         error = true;
                         descripcionError += "Valor total del extracto detalle no suma igual al valor del extracto registrado en anticipos pagos terceros \n";
                     }
                     // Valida cual de los documentos en el extracto detalle no suma a su total
                     if (listaProntoPagoDetalle.size() != 0){

                        Iterator it1 = listaProntoPagoDetalle.iterator();
                         while (it1.hasNext()) {
                             prontoPagoDetalle = (ProntoPagoDetalle)it1.next();
                             if (prontoPagoDetalle.getDiferencia_interna() != 0) {
                                 error = true;
                                 descripcionError += "Detalle diferencia \n";
                                 descripcionError += prontoPagoDetalle.getTipo_documento() + " ";
                                 descripcionError += prontoPagoDetalle.getClase() + " ";
                                 descripcionError += prontoPagoDetalle.getDocumento() + " ";
                                 descripcionError += prontoPagoDetalle.getValor_calculado() + " ";
                                 descripcionError += prontoPagoDetalle.getValor_registrado() + " ";
                                 descripcionError += prontoPagoDetalle.getDiferencia_interna() + "\n";
                             }
                         }
                     }
                     else {
                         error = true;
                         descripcionError += "No existe detalle para el extracto \n";
                     }
                     descripcionError += "\n";

                     if (!error) {

                         System.out.println("Paso 1");
                         String cuentaTransferencia = prontoPago.getCuenta_transferencia();
                         System.out.println("cuentaTransferencia impresa");


                         banco = model.prontoPagoService.getBanco(prontoPago.getCuenta_transferencia(),
                                                                  prontoPago.getBanco_transferencia(),
                                                                  prontoPago.getTcta_transferencia() );


                         String cuenta_contable = (String) banco.get(2);
                         prontoPago.setCuenta_contable_banco(cuenta_contable ) ;

                         String branch_code  = (String) banco.get(0);
                         prontoPago.setBanco(branch_code);

                         String bank_account_no  = (String) banco.get(1);
                         prontoPago.setSucursal(bank_account_no);

                         Cmc cmc = model.prontoPagoService.getCmc(distrito, "EXT","EX");
                         prontoPago.setCmc("EX");
                         prontoPago.setCuenta_os(cmc.getCuenta());
                         prontoPago.setDbcr_os(cmc.getDbcr());
                         prontoPago.setSigla_comprobante_os(cmc.getSigla_comprobante());
                         prontoPago.setGrupo_transaccion(model.prontoPagoService.getGrupoTransaccion());


                         prontoPago.setPeriodo_os(periodo);

                         // Procesa transaccion

                         // Grabar un registro en orden de servicio
                         comandoSQL = model.prontoPagoService.setOrdenServicio(prontoPago, distrito, login, creation_date);
                         comandos_sql.add(comandoSQL);

                         comandoSQL = model.prontoPagoService.setAnticipo(prontoPago, "FINV", creation_date);
                         comandos_sql.add(comandoSQL);

                         // Grabar registros en orden de servicio detallado
                         Iterator it1 = listaProntoPagoDetalle.iterator();
                         while (it1.hasNext()) {
                             prontoPagoDetalle = (ProntoPagoDetalle)it1.next();
                             comandoSQL = model.prontoPagoService.setOrdenServicioDetalle(prontoPago, prontoPagoDetalle, distrito, login, creation_date);
                             comandos_sql.add(comandoSQL);
                         }


                         // Crear la cabecera del comprobante contable de la Orden de servicio

                         Comprobante comprobante = new Comprobante();

                         comprobante.setReg_status("");
                         comprobante.setDstrct(distrito);
                         comprobante.setTipodoc(prontoPago.getSigla_comprobante_os());
                         comprobante.setNumdoc(prontoPago.getNumero_operacion()  );
                         comprobante.setGrupo_transaccion(prontoPago.getGrupo_transaccion());
                         comprobante.setSucursal("OP");
                         comprobante.setPeriodo(prontoPago.getPeriodo_os());
                         comprobante.setFechadoc(fecha);
                         comprobante.setDetalle("Contabiliza " + prontoPago.getTipo_operacion()+ " " + prontoPago.getNumero_operacion()  );
                         comprobante.setTercero(prontoPago.getNit());
                         comprobante.setTotal_debito(prontoPago.getVlr_extracto());
                         comprobante.setTotal_credito(prontoPago.getVlr_extracto()) ;
                         comprobante.setTotal_items(3);
                         comprobante.setMoneda("PES");
                         comprobante.setFecha_aplicacion("0099-01-01 00:00:00");
                         comprobante.setAprobador("ADMIN");
                         comprobante.setLast_update(creation_date);
                         comprobante.setUser_update("ADMIN");
                         comprobante.setCreation_date(creation_date);
                         comprobante.setCreation_user("ADMIN");
                         comprobante.setBase("COL");
                         comprobante.setUsuario_aplicacion("");
                         comprobante.setTipo_operacion("");
                         comprobante.setMoneda_foranea("");
                         comprobante.setValor_for(0.00);
                         comprobante.setRef_1(prontoPago.getTipo_operacion());
                         comprobante.setRef_2(prontoPago.getNumero_operacion()  );

                         comandoSQL = model.prontoPagoService.setComprobante(comprobante);
                         comandos_sql.add(comandoSQL);

                         // Crear el detalle del comprobante

                         crearComprobanteDetalle(prontoPago, prontoPagoDetalle, comandos_sql, distrito,creation_date);

                         // Crear la factura CxP al Propietario

                         crearCxPPropietario( prontoPago, prontoPagoDetalle, comandos_sql, distrito,banco);


                         // Grabando todo a la base de datos.
                         model.applusService.ejecutarSQL(comandos_sql);
                         comandos_sql.removeAllElements();

                     }  // final del if de la orden de servicio sin error


                 } // final del while

            } // final del if

        }catch (Exception ex){
            ex.printStackTrace();
            try{

                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HOrdenServicio ...\n"  + e.getMessage());
            }
        }
    }

public void crearCxPPropietario(ProntoPago prontoPago, ProntoPagoDetalle prontoPagoDetalle,
                                Vector comandos_sql, String distrito, Vector banco) throws SQLException {

    try {

        String comandoSQL = "";

        // Datos comunes a la cabecera y a los items

        String proveedor = prontoPago.getNit();
        String auxiliar  = proveedor;
        String tipo_documento = "FAP";


        String documento = prontoPago.getFactura_cxp();


        // String user_update = usuario.getLogin();

        String user_update = "ADMIN";

        String creation_user = user_update;
        String base = "COL";

        // CREACION DE LA CABECERA EN CXP_DOC

        // Datos de la cabecera

        String tipo_operacion     = prontoPago.getTipo_operacion();
        String descripcionAdicional1 = "";
        String descripcionAdicional2 = "";
        String descripcionAdicional3 = "  Propietario: " + prontoPago.getNit() +
                                       "  Fecha transferencia: " + prontoPago.getFecha_transferencia().substring(0, 10) +
                                       "  Numero transferencia: " + prontoPago.getNumero_transferencia();


        if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")){
            descripcionAdicional1 =  "   SECUENCIA EXTRACTO: "  ;
            descripcionAdicional2 =  "  Valor extracto: "  ;
        }
        else {
            descripcionAdicional1 =  "   PLANILLA: " ;
            descripcionAdicional2 =  "  Valor anticipo: " ;
            descripcionAdicional3 =  descripcionAdicional3 + "  Placa: " + prontoPago.getSupplier();

        }
        descripcionAdicional1 = descripcionAdicional1 + documento ;
        descripcionAdicional2 = descripcionAdicional2 + Util.FormatoMiles(prontoPago.getVlr_extracto() )  ;
        String descripcion_factura  = "TIPO OPERACION : " + tipo_operacion + descripcionAdicional1  + descripcionAdicional2 + descripcionAdicional3  ;


        String agencia        = "OP";

        String handle_code = prontoPago.getCmc();
        String aprobador   = "JGOMEZ";
        String usuario_aprobacion = "JGOMEZ";

        try{


            String branch_code     = (String) banco.get(0);  // Banco
            String bank_account_no = (String) banco.get(1);  // Sucursal

            String moneda = "PES";

            double vlr_factura = Util.redondear2(prontoPago.getVlr_neto(), 2);

            double vlr_neto  =  vlr_factura;
            double vlr_total_abonos = 0;
            double vlr_saldo = vlr_neto;
            double vlr_neto_me = vlr_neto;
            double vlr_total_abonos_me = 0;
            double vlr_saldo_me = vlr_neto;
            double tasa = 1;

            String observacion = "DETALLE :";
            observacion = observacion + descripcionAdicional2  ;
            if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")){
                observacion = observacion + "  Porcentaje : " + Double.toString(prontoPago.getPorcentaje()) ;
                observacion = observacion + "  Ingreso anticipado : " + Util.FormatoMiles(prontoPago.getVlr_descuento()) ;
            }
            observacion = observacion + descripcionAdicional3;

            String clase_documento = "4";

            String moneda_banco = (String) banco.get(3);  // Currency
            String clase_documento_rel = "4";

            // Calculo de la fecha de vencimiento de la factura//


            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();

            String fechaTransferencia = prontoPago.getFecha_transferencia();


            // DateFormat formato_fecha_factura;
            // formato_fecha_factura = new SimpleDateFormat("yyyy-MM-dd");
            String fecha_factura = fechaTransferencia.substring(0, 10);
            // String fecha_factura  = formato_fecha_factura.format(fechaActual);
            String FechaFacturaVencimiento = fecha_factura ;

            comandoSQL = model.prontoPagoService.setCxp_doc(distrito, proveedor, tipo_documento, documento, descripcion_factura,
                    agencia, handle_code, aprobador, usuario_aprobacion, branch_code, bank_account_no,
                    moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                    vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                    creation_user, base, clase_documento, moneda_banco,
                    fecha_factura, FechaFacturaVencimiento, clase_documento_rel,
                    creation_date, creation_date, creation_date);

            comandos_sql.add(comandoSQL);


            // CREACION DE LOS ITEMS EN CXP_ITEMS_DOC

            // Datos comunes a todos los items

            int item = 0;

            // Item de valor del extracto menos el ingreso anticipado


            comandoSQL  = model.applusService.setCxp_items_doc(distrito, proveedor,
                                 tipo_documento, documento,Integer.toString(++item),  descripcion_factura,
                                 vlr_neto, vlr_neto, prontoPago.getCuenta_anticipo(),
                                 user_update, creation_user, base, "098", auxiliar,
                                 creation_date, creation_date);
            comandos_sql.add(comandoSQL);





        }catch (Exception ex){
            ex.printStackTrace();
            try{

                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HOrdenServicio, procedimiento crearCxPPropietario2  ...\n"  + e.getMessage());
            }
        }


    }catch (Exception e){
        System.out.println("Error HOrdenServicio, procedimiento crearCxPPropietario1  ...\n"  + e.getMessage());
    }




}

public String buscarDocumento(String distrito,String proveedor,String tipo_documento, String secuencia) throws SQLException {

    return model.prontoPagoService.buscarDocumento( distrito, proveedor, tipo_documento,  secuencia);


}

public void crearComprobanteDetalle(ProntoPago prontoPago, ProntoPagoDetalle prontoPagoDetalle, Vector comandos_sql,
                                    String distrito, String creation_date) {


    try{

         String comandoSQL = "";
         ComprobanteDetalle comprobanteDetalle = new ComprobanteDetalle();


         // Item 1
         comprobanteDetalle.setReg_status("");
         comprobanteDetalle.setDstrct(distrito);
         comprobanteDetalle.setTipodoc(prontoPago.getSigla_comprobante_os());
         comprobanteDetalle.setNumdoc(prontoPago.getNumero_operacion()  );
         comprobanteDetalle.setGrupo_transaccion(prontoPago.getGrupo_transaccion());
         comprobanteDetalle.setTransaccion(model.prontoPagoService.getTransaccion());
         comprobanteDetalle.setPeriodo(prontoPago.getPeriodo_os());
         if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")){
             comprobanteDetalle.setCuenta("13802601");
         }
         else {
             comprobanteDetalle.setCuenta("13802701");
         }

         comprobanteDetalle.setAuxiliar(prontoPago.getNit());
         comprobanteDetalle.setDetalle("Contabiliza " + prontoPago.getTipo_operacion() + " "+ prontoPago.getNumero_operacion() );
         comprobanteDetalle.setValor_debito(prontoPago.getVlr_extracto());
         comprobanteDetalle.setValor_credito(0.00) ;
         comprobanteDetalle.setTercero(prontoPago.getNit());
         comprobanteDetalle.setDocumento_interno(prontoPago.getSigla_comprobante_os());
         comprobanteDetalle.setLast_update(creation_date);
         comprobanteDetalle.setUser_update("ADMIN");
         comprobanteDetalle.setCreation_date(creation_date);
         comprobanteDetalle.setCreation_user("ADMIN");
         comprobanteDetalle.setBase("COL");
         comprobanteDetalle.setAbc("");
         comprobanteDetalle.setVlr_for(0.00);
         comprobanteDetalle.setTipodoc_rel(prontoPago.getTipo_operacion());
         comprobanteDetalle.setDocumento_rel(prontoPago.getNumero_operacion()  );

         comandoSQL = model.prontoPagoService.setComprobanteDetalle(comprobanteDetalle);
         comandos_sql.add(comandoSQL);

         // Item 2
         comprobanteDetalle.setTransaccion(model.prontoPagoService.getTransaccion());
         if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")){
             comprobanteDetalle.setCuenta("23050201");
         }
         else if (prontoPago.getTipo_operacion().equalsIgnoreCase("AET")) {
             comprobanteDetalle.setCuenta("23050301");
         }
         else if (prontoPago.getTipo_operacion().equalsIgnoreCase("AEF")) {
             comprobanteDetalle.setCuenta("23050302");
         }
         else if (prontoPago.getTipo_operacion().equalsIgnoreCase("AGA")) {
             comprobanteDetalle.setCuenta("23050303");
         }

         comprobanteDetalle.setValor_debito(0.00);
         comprobanteDetalle.setValor_credito(prontoPago.getVlr_neto()) ;

         comandoSQL = model.prontoPagoService.setComprobanteDetalle(comprobanteDetalle);
         comandos_sql.add(comandoSQL);


         // Item 3  20101005
         comprobanteDetalle.setTransaccion(model.prontoPagoService.getTransaccion());
         if(prontoPago.getTipo_operacion().equalsIgnoreCase("EXT")){
             comprobanteDetalle.setCuenta("I010020024163");
         }
         else if (prontoPago.getTipo_operacion().equalsIgnoreCase("AET")) {
               comprobanteDetalle.setCuenta("I010030024153");
//             if(prontoPago.getAgency_id().equalsIgnoreCase("YO00")) {           // 20101025
//                 comprobanteDetalle.setCuenta("I360030024153");                 // 20101025
//             }                                                                  // 20101025
//             else {                                                             // 20101025
//                 comprobanteDetalle.setCuenta("I010030024153");
//             }
         }
         else if (prontoPago.getTipo_operacion().equalsIgnoreCase("AEF")) {
             if( prontoPago.getAgency_id().equalsIgnoreCase("DU00")) {
                 comprobanteDetalle.setCuenta("I160030024155");
             }
             else if(prontoPago.getAgency_id().equalsIgnoreCase("CU00")) {
                 comprobanteDetalle.setCuenta("I340030024155");
             }
             else if(prontoPago.getAgency_id().equalsIgnoreCase("YO00")) {      // 20101025
                 comprobanteDetalle.setCuenta("I360030024155");                 // 20101025
             }                                                                  // 20101025
             else {
                  comprobanteDetalle.setCuenta("I010030024155");
             }
         }
         else if (prontoPago.getTipo_operacion().equalsIgnoreCase("AGA")) {
             comprobanteDetalle.setCuenta("I010030024154");
         }
         else {
             comprobanteDetalle.setCuenta("I010020024163");
         }



         double ingreso_anticipado = prontoPago.getVlr_extracto() - prontoPago.getVlr_neto();
         comprobanteDetalle.setValor_debito(0.00);
         comprobanteDetalle.setValor_credito(ingreso_anticipado) ;

         comandoSQL = model.prontoPagoService.setComprobanteDetalle(comprobanteDetalle);
         comandos_sql.add(comandoSQL);




    }catch (Exception ex){
        ex.printStackTrace();
        try{

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
        }catch (Exception e){
            System.out.println("Error HOrdenServicio, procedimiento crear Comprobante detallado  ...\n"  + e.getMessage());
        }
    }

}


public void crearEgreso(Model model,Usuario usuario,String distrito) throws SQLException{

    try {


                List listaOS = model.prontoPagoService.getOrdenServicio();


                if (listaOS.size() > 0) {

                    SerieGeneral serie = null;
                    serie = new SerieGeneral();

                    OrdenServicio ordenServicio = new OrdenServicio();
                    OrdenServicioDetalle ordenServicioDetalle = new OrdenServicioDetalle();
                    EgresoCabecera egreso = new EgresoCabecera();
                    EgresoDetalle  egresoDetalle = new EgresoDetalle();


                    Vector comandos_sql =new Vector();

                    String comandoSQL = "";
                    String login = usuario.getLogin();

                    java.util.Date fechaActual = new Date();
                    String creation_date = fechaActual.toString();

                    DateFormat formato_fecha;
                    formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
                    String  fecha = formato_fecha.format(fechaActual);

                    Iterator it = listaOS.iterator();
                    while (it.hasNext()) {

                        // Un registro de orden de servicio para un egreso
                        ordenServicio = (OrdenServicio)it.next();

                        //determina numero de egreso para anticipos gasolina ya que no tienen
                        //serie = model.serieGeneralService.getSerie("FINV","OP","EGRAG");
                        //model.serieGeneralService.setSerie("FINV","OP","EGRAG");
                        //String numeroTransferencia = serie.getUltimo_prefijo_numero();
                        //System.out.println("numeroTransferencia_"+numeroTransferencia);
                        if(ordenServicio.getTipo_operacion().equalsIgnoreCase("AGA")){
                            serie = model.serieGeneralService.getSerie("FINV","OP","EGRAG");
                            model.serieGeneralService.setSerie("FINV","OP","EGRAG");
                            String numeroTransferencia = serie.getUltimo_prefijo_numero();
                            System.out.println("numeroTransferencia_"+numeroTransferencia+"_");
                            System.out.println(" ");
                            ordenServicio.setNumero_transferencia(numeroTransferencia);
                        }else{
                            if(ordenServicio.getBanco().equals("ABONO PRESTAMOS")){
                                serie = model.serieGeneralService.getSerie("FINV","OP","EGRAP");
                                model.serieGeneralService.setSerie("FINV","OP","EGRAP");
                                String numeroTransferencia = serie.getUltimo_prefijo_numero();
                                System.out.println("numeroTransferencia_"+numeroTransferencia+"_");
                                System.out.println(" ");
                                ordenServicio.setNumero_transferencia(numeroTransferencia);
                            }else{
                                String numeroEgreso = model.prontoPagoService.buscarDocumentoEgreso(distrito, ordenServicio.getBanco(), ordenServicio.getSucursal(), ordenServicio.getNumero_transferencia());
                                ordenServicio.setNumero_transferencia(numeroEgreso);
                                System.out.println("[numeroEgreso_"+numeroEgreso+"]");
                            }
                        }

                        String fecha_transferencia = ordenServicio.getFecha_transferencia();
                        String fecha_cheque = fecha_transferencia.substring(0,10);

                        egreso.setReg_status("" ) ;
                        egreso.setDstrct(distrito ) ;
                        egreso.setBranch_code(ordenServicio.getBanco() ) ;
                        egreso.setBank_account_no(ordenServicio.getSucursal() ) ;
                        egreso.setDocument_no(ordenServicio.getNumero_transferencia() ) ;
                        egreso.setNit(ordenServicio.getProveedor() ) ;
                        egreso.setPayment_name(ordenServicio.getNombre_propietario() ) ;
                        egreso.setAgency_id("OP" ) ;
                        egreso.setPmt_date(fecha ) ;
                        egreso.setPrinter_date (fecha ) ;
                        egreso.setConcept_code("FAC" ) ;
                        egreso.setVlr (ordenServicio.getVlr_consignacion() );
                        egreso.setVlr_for(ordenServicio.getVlr_consignacion() ) ;
                        egreso.setCurrency("PES" ) ;
                        egreso.setLast_update(creation_date ) ;
                        egreso.setUser_update(login ) ;
                        egreso.setCreation_date( creation_date) ;
                        egreso.setCreation_user(login ) ;
                        egreso.setBase("COL" ) ;
                        egreso.setTipo_documento("004" ) ;
                        egreso.setTasa (1.00 ) ;
                        egreso.setFecha_cheque(fecha_cheque) ;
                        egreso.setUsuario_impresion("ADMIN" ) ;
                        egreso.setUsuario_contabilizacion("" ) ;
                        egreso.setFecha_contabilizacion("0099-01-01 00:00:00" ) ;
                        egreso.setFecha_entrega("0099-01-01 00:00:00" ) ;
                        egreso.setUsuario_envio("" ) ;
                        egreso.setFecha_envio("0099-01-01 00:00:00" ) ;
                        egreso.setUsuario_recibido("" ) ;
                        egreso.setFecha_recibido("0099-01-01 00:00:00" ) ;
                        egreso.setUsuario_entrega("" ) ;
                        egreso.setFecha_registro_entrega("0099-01-01 00:00:00" ) ;
                        egreso.setFecha_registro_envio("0099-01-01 00:00:00" ) ;
                        egreso.setFecha_registro_recibido("0099-01-01 00:00:00" ) ;
                        egreso.setTransaccion(0);
                        egreso.setNit_beneficiario(ordenServicio.getProveedor());
                        egreso.setFecha_reporte("0099-01-01 00:00:00" ) ;
                        egreso.setNit_proveedor(ordenServicio.getProveedor() ) ;
                        egreso.setUsuario_generacion("ADMIN" ) ;
                        egreso.setPeriodo( "") ;
                        egreso.setReimpresion("N" ) ;
                        egreso.setContabilizable("S" ) ;
                        System.out.println("[getDocument_no]"+egreso.getDocument_no()+"[]");
                        // Grabar un registro en la cabecera del egreso
                        comandoSQL = model.prontoPagoService.setEgreso(egreso);
                        comandos_sql.add(comandoSQL);

                        // Crear detalle del egreso

                        int item = 0;

                        // ITEM 1
                        egresoDetalle.setReg_status("");
                        egresoDetalle.setDstrct (distrito ) ;
                        egresoDetalle.setBranch_code(ordenServicio.getBanco() ) ;
                        egresoDetalle.setBank_account_no(ordenServicio.getSucursal() ) ;
                        egresoDetalle.setDocument_no(ordenServicio.getNumero_transferencia() ) ;
                        item++;
                        String sItem = "000" + Integer.toString(item);
                        sItem = sItem.substring(sItem.length() - 3);
                        egresoDetalle.setItem_no (sItem ) ;
                        egresoDetalle.setConcept_code ("FAC" ) ;
                        egresoDetalle.setVlr(ordenServicio.getVlr_neto() ) ;
                        egresoDetalle.setVlr_for( ordenServicio.getVlr_neto()) ;
                        egresoDetalle.setCurrency ("PES" ) ;
                        egresoDetalle.setOc ("" ) ;
                        egresoDetalle.setLast_update ( creation_date) ;
                        egresoDetalle.setUser_update(login ) ;
                        egresoDetalle.setCreation_date (creation_date ) ;
                        egresoDetalle.setCreation_user(login ) ;
                        egresoDetalle.setDescription("" ) ;
                        egresoDetalle.setBase ("COL" ) ;
                        egresoDetalle.setTasa(1.00 ) ;
                        egresoDetalle.setTransaccion(0 ) ;
                        egresoDetalle.setTipo_documento("FAP" ) ;

                        egresoDetalle.setDocumento(ordenServicio.getFactura_cxp() ) ;
                        egresoDetalle.setTipo_pago("C" ) ;

                        egresoDetalle.setCuenta("");
                        egresoDetalle.setAuxiliar("");

                        comandoSQL = model.prontoPagoService.setEgresoDetalle(egresoDetalle);
                        comandos_sql.add(comandoSQL);

                        // ITEM 2


                        if(ordenServicio.getVlr_combancaria() != 0) {
                            item++;
                            sItem = "000" + Integer.toString(item);
                            sItem = sItem.substring(sItem.length() - 3);
                            egresoDetalle.setItem_no (sItem ) ;
                            egresoDetalle.setVlr(-ordenServicio.getVlr_combancaria() ) ;
                            egresoDetalle.setVlr_for(-ordenServicio.getVlr_combancaria()) ;

                            // 20101005
                            if (ordenServicio.getCmc().equalsIgnoreCase("AT") ) {
                                egresoDetalle.setCuenta("I010030024208");
                            }

                            else if(ordenServicio.getCmc().equalsIgnoreCase("EX") ) {
                                    egresoDetalle.setCuenta("I010020024208");
                            }
                            else {
                                egresoDetalle.setCuenta("I010020024208");
                            }
                            // 20101005

                            egresoDetalle.setAuxiliar(egreso.getNit());

                            comandoSQL = model.prontoPagoService.setEgresoDetalle(egresoDetalle);
                            comandos_sql.add(comandoSQL);

                        }






                        // ITEM 3

                        if (ordenServicio.getVlr_ajuste() != 0 ) {

                            item++;
                            sItem = "000" + Integer.toString(item);
                            sItem = sItem.substring(sItem.length() - 3);
                            egresoDetalle.setItem_no (sItem ) ;
                            egresoDetalle.setVlr(ordenServicio.getVlr_ajuste() ) ;
                            egresoDetalle.setVlr_for( ordenServicio.getVlr_ajuste()) ;
                            egresoDetalle.setCuenta("I010030024211");//I010020024211
                            egresoDetalle.setAuxiliar(egreso.getNit());

                            comandoSQL = model.prontoPagoService.setEgresoDetalle(egresoDetalle);
                            comandos_sql.add(comandoSQL);
                        }

                        // Actualiza saldos en la factura
                        comandoSQL = model.prontoPagoService.setSaldos(ordenServicio.getVlr_neto(), ordenServicio.getNumero_transferencia(),
                                                                       distrito, ordenServicio.getProveedor(),
                                                                       "FAP", ordenServicio.getFactura_cxp());
                        comandos_sql.add(comandoSQL);


                        // Crear la CxC a TSP
                        crearCxC(ordenServicio,usuario,distrito,comandos_sql);


                        // Actualizar la fecha de egreso en la orden de servicio
                        comandoSQL = model.prontoPagoService.setFechaEgreso(distrito, ordenServicio.getTipo_operacion(), ordenServicio.getNumero_operacion(),
                                                                             ordenServicio.getNumero_transferencia(), creation_date);
                        comandos_sql.add(comandoSQL);

                        // Grabando todo a la base de datos.
                        model.applusService.ejecutarSQL(comandos_sql);
                        comandos_sql.removeAllElements();

                    }  // fin del While
                }

    }catch (Exception e){
        System.out.println("Error HOrdenServicio, procedimiento crearEgreso  ...\n"  + e.getMessage());
    }



}



public void crearCxC(OrdenServicio ordenServicio,Usuario usuario, String distrito, Vector comandos_sql)  throws SQLException{

    OrdenServicioDetalle ordenServicioDetalle = new OrdenServicioDetalle();

    try {

        String comandoSQL = "";

        List listaOSDetalle = null;

        listaOSDetalle      =  model.prontoPagoService.getOrdenServicioDetalle(distrito,
                                           ordenServicio.getTipo_operacion(),
                                           ordenServicio.getNumero_operacion() );
        int numero_items = listaOSDetalle.size();


        String login = usuario.getLogin();

        String nit   = "8901031611";
        String codigo = "CL00749";
        String descripcion_factura = "";
        String tipo_ref1 = "";
        String ref1 = "";
        String tipo_ref2 = "";
        String ref2 = "";

        // Calculo de la fecha de vencimiento de la factura

        java.util.Date fechaActual = new Date();


        DateFormat formato_fecha;
        formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String fecha_transferencia = ordenServicio.getFecha_transferencia();


        // En caso de anticipos de gasolina, no se efectua transferencia por parte de Fintra, por tanto esta fecha esta con 0099-01-01. Se debe utilizar entonces la fecha del egreso

        if(fecha_transferencia.equals("0099-01-01 00:00:00")) {                 // 20100916
            fecha_transferencia = sdf.format(fechaActual);                      // 20101023
        }                                                                       // 20100916

        String fecha_factura       = fecha_transferencia.substring(0,10);







        Calendar calendarFechaVencimiento = Calendar.getInstance();
        String ano = fecha_transferencia.substring(0,4);
        String mes = fecha_transferencia.substring(5,7);
        String dia = fecha_transferencia.substring(8,10);

        calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes)-1,Integer.parseInt(dia),8,0,0);


        String FechaFacturaVencimiento=formato_fecha.format(calendarFechaVencimiento.getTime()) ;

        Iterator it = listaOSDetalle.iterator();
        String documento = "";
        while (it.hasNext()) {

            ordenServicioDetalle = (OrdenServicioDetalle)it.next();


            int k = 0;
            SerieGeneral serie = null;
            serie = new SerieGeneral();
            serie = model.serieGeneralService.getSerie("FINV","OP","FATSP");
            model.serieGeneralService.setSerie("FINV","OP","FATSP");

            documento = serie.getUltimo_prefijo_numero();

            descripcion_factura = "Tipo operacion: "      + ordenServicio.getTipo_operacion();
            descripcion_factura += "  Numero Operacion: " + ordenServicio.getNumero_operacion();
            descripcion_factura += "  Tipo Documento: "   + ordenServicioDetalle.getTipo_documento();
            descripcion_factura += "  Documento: "        + ordenServicioDetalle.getDocumento();
            descripcion_factura += "  Proveedor: "        + ordenServicioDetalle.getProveedor();
            descripcion_factura += "  Clase: "            + ordenServicioDetalle.getClase();
            descripcion_factura += "  Placa: "            + ordenServicioDetalle.getPlaca();

            tipo_ref1 = "VE";
            ref1  = ordenServicio.getTipo_operacion()+",";
            ref1 += ordenServicio.getNumero_operacion()+",";
            ref1 += ordenServicioDetalle.getTipo_documento()+",";
            ref1 += ordenServicioDetalle.getDocumento()+",";
            ref1 += ordenServicioDetalle.getProveedor()+",";
            ref1 += ordenServicioDetalle.getClase()+",";
            ref1 += ordenServicioDetalle.getPlaca();

            double valor_factura = ordenServicioDetalle.getValor_calculado();

            // Cabecera de la factura

            comandoSQL = model.applusService.setFactura("FINV", "FAC", documento,
                         nit , codigo,ordenServicio.getTipo_operacion(), fecha_factura, FechaFacturaVencimiento, fecha_factura,
                         descripcion_factura,
                         valor_factura, 0.0, valor_factura, valor_factura,
                         0.0, valor_factura, 1.0, "PES", numero_items,
                         "CREDITO", "OP", "OP",
                         "", "COL", fecha_transferencia, login, fecha_transferencia,
                         login, ordenServicio.getCmc(), "", "OP",
                         tipo_ref1,ref1,tipo_ref2,ref2);

            comandos_sql.add(comandoSQL);


            // Item de la factura
            // Determina cuenta contable del item, parte credito
            String cuentaItem = "";
            if(ordenServicio.getTipo_operacion().equalsIgnoreCase("EXT")) {
                cuentaItem = "13802601";
            }
            else {
                cuentaItem = "13802701";
            }
            k++;
            comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                             k,  nit,  "097", descripcion_factura ,
                                             cuentaItem, 1.0,
                                             valor_factura , valor_factura , valor_factura,
                                              valor_factura , 1.0, "PES",
                                             fecha_transferencia, login, fecha_transferencia,
                                             login, "COL", nit);
            comandos_sql.add(comandoSQL);

            // Actualizar cada registro de orden de servicio detalle con el numero de factura realizado a TSP.
            comandoSQL = model.prontoPagoService.setFacturaCxC(documento, distrito, ordenServicio.getTipo_operacion(),
                                                               ordenServicio.getNumero_operacion(), ordenServicioDetalle.getTipo_documento(), ordenServicioDetalle.getDocumento());
            comandos_sql.add(comandoSQL);


        } // fin del while


    }catch (Exception e){
        System.out.println("Error HOrdenServicio, procedimiento crearCxC  ...\n"  + e.getMessage());
    }


}




}