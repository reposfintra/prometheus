/*
 * AnticiposDeleteAction.java
 *
 * Created on 20 de abril de 2005, 05:58 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class AnticiposDeleteAction extends Action{
    
    /** Creates a new instance of AnticiposDeleteAction */
    public AnticiposDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next ="/anticipos/anticipoDelete.jsp";
        String codant       = request.getParameter("codant").toUpperCase();
        String dist         = request.getParameter("distrito");
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        String sj =request.getParameter("sj");
        try{
            model.anticiposService.searchAnticipos(dist, codant,sj);
            if(model.anticiposService.getAnticipo()!=null){
                model.anticiposService.setAnticipos(model.anticiposService.getAnticipo());
                model.anticiposService.anularAnticipo();
                model.anticiposService.vecAnticipos(usuario.getBase());
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
    
}
