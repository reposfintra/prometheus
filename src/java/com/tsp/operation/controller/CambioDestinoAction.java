/*
 * CambioDestinoAction.java
 *
 * Created on 23 de abril de 2007, 02:35 PM
 */

package com.tsp.operation.controller;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Remesa2;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Ciudad;
import com.tsp.operation.model.beans.RemiDest;


public class CambioDestinoAction extends Action {
    
    
    
    public final String CONCEPTO_CAMBIO_DESTINO = "53";
    HttpSession session;
    Usuario     usuario;    
    
    
    
    
    /** Creates a new instance of CambioDestinoAction */
    public CambioDestinoAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String opcion  = coalesce( request.getParameter("opcion"), "");
            String next    = "/jsp/despacho/cambio_destino/consulta.jsp";
            
            //this.parametros();
            
            if ( opcion.equalsIgnoreCase("BUSCAR")){
                next = this.buscarRemesa(next);
            } else if ( opcion.equalsIgnoreCase("REMPLAZO")){
                next = this.buscarRemesaRemplazo(next);                
            } else if ( opcion.equalsIgnoreCase("LOAD_ORIGENES")){
                next = this.loadOrigenes(next);
            } else if ( opcion.equalsIgnoreCase("LOAD_DESTINOS")){
                next = this.loadDestinos(next);
            } else if ( opcion.equalsIgnoreCase("LOAD_ESTANDARES")){
                next = this.loadEstandares(next);
            } else if ( opcion.equalsIgnoreCase("CAMBIO_DESTINO")){
                next = this.cambioDestino(next);
            } else if ( opcion.equalsIgnoreCase("LOAD_REMITENTES")){
                next = this.loadRemitentes(next);
            }  else if ( opcion.equalsIgnoreCase("LOAD_DESTINATARIOS")){
                next = this.loadDestinatarios(next);
            }
            
            this.dispatchRequest(next);
        } catch (Exception ex){
            ex.printStackTrace();
            throw new ServletException (ex.getMessage());
        }
    }
    
    
    public String  buscarRemesa(String next) throws Exception {
        try{
            
            String dstrct = coalesce( request.getParameter("dstrct"), "");
            String numrem = coalesce( request.getParameter("numrem"), "");
            model.CambioDestinoSvc.obtenerRemesa(dstrct, numrem);
            Remesa2 r = model.CambioDestinoSvc.getRemesa();
            if (r==null){
                request.setAttribute("msg","No se pudo encontrar la remesa, <br>verifique que exista o no este anulada.");
            } else {
                model.CambioDestinoSvc.setRemesaRemplazo( (Remesa2) model.CambioDestinoSvc.getRemesa().clone()  );
                next = "/jsp/despacho/cambio_destino/nuevo.jsp";
            }
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }
    
    
    public String  buscarRemesaRemplazo(String next) throws Exception {
        try{
            
            Remesa2 ranterior = model.CambioDestinoSvc.getRemesaRemplazo();
            
            
            String dstrct   = coalesce( request.getParameter("dstrct")  , "");
            String estandar = coalesce( request.getParameter("estandar"), "");
            String codcli   = coalesce( request.getParameter("codcli")  , "");
            model.CambioDestinoSvc.obtenerRemesaRemplazo(dstrct, estandar, codcli);
            Remesa2 r = model.CambioDestinoSvc.getRemesaRemplazo();
            next = "/jsp/despacho/cambio_destino/nuevo.jsp";
            if (r==null){
                request.setAttribute("msg","Estandar no valido para el cambio de destino.");
                if (ranterior!=null)
                    model.CambioDestinoSvc.setRemesaRemplazo(ranterior);
            }
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }
    
    
    public String loadOrigenes(String next) throws Exception {
        try{
            
            String dstrct = coalesce( request.getParameter("dstrct"), "");
            String codcli = coalesce( request.getParameter("codcli"), "");
            model.CambioDestinoSvc.obtenerOrigenes(dstrct, codcli);            
            model.CambioDestinoSvc.setDestinos    (null);
            model.CambioDestinoSvc.setEstandares  (null);
            next = "/jsp/despacho/cambio_destino/standar.jsp";
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }
    
    public String loadDestinos(String next) throws Exception {
        try{
            
            String dstrct = coalesce( request.getParameter("dstrct"), "");
            String codcli = coalesce( request.getParameter("codcli"), "");
            String origen = coalesce( request.getParameter("origen"), "");
            model.CambioDestinoSvc.obtenerDestinos(dstrct, codcli,  origen); 
            model.CambioDestinoSvc.setEstandares  (null);
            next = "/jsp/despacho/cambio_destino/standar.jsp";
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }    
    
    
    public String loadEstandares(String next) throws Exception {
        try{
            
            String dstrct  = coalesce( request.getParameter("dstrct"), "");
            String codcli  = coalesce( request.getParameter("codcli"), "");
            String origen  = coalesce( request.getParameter("origen"), "");
            String destino = coalesce( request.getParameter("destino"), "");
            model.CambioDestinoSvc.obtenerEstandares(dstrct, codcli,  origen, destino);            
            next = "/jsp/despacho/cambio_destino/standar.jsp";
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }    
    
    
    public String cambioDestino(String next) throws Exception {
        try{

            
            Remesa2 remesa_original = model.CambioDestinoSvc.getRemesa();
            Remesa2 remesa_remplazo = model.CambioDestinoSvc.getRemesaRemplazo();
            
            
            double vlrrem   = Double.parseDouble(coalesce( request.getParameter("vlrrem")  , "0").replaceAll(",","") );
            double vlrrem2  = Double.parseDouble(coalesce( request.getParameter("vlrrem2") , "0").replaceAll(",","") );
            double pesoreal = Double.parseDouble(coalesce( request.getParameter("pesoreal"), "0").replaceAll(",","") );            
            remesa_remplazo.setVlrrem        ( vlrrem   );
            remesa_remplazo.setVlrrem2       ( vlrrem2  );
            remesa_remplazo.setPesoreal      ( pesoreal );
            remesa_remplazo.setVlrrem_aj     ( remesa_remplazo.getVlrrem()    - remesa_original.getVlrrem()    );
            remesa_remplazo.setVlrrem2_aj    ( remesa_remplazo.getVlrrem2()   - remesa_original.getVlrrem2()   );
            remesa_remplazo.setPesoreal_aj   ( remesa_remplazo.getPesoreal()  - remesa_original.getPesoreal()  );
            remesa_remplazo.setQty_value_aj  ( remesa_remplazo.getQty_value() - remesa_original.getQty_value() );
            remesa_remplazo.setTasa_aj       ( remesa_remplazo.getTasa()      - remesa_original.getTasa()      );
            
            
            String crossdocking = coalesce( request.getParameter("crossdocking") , "");
            String cadena       = coalesce( request.getParameter("cadena")       , "");
            String n_facturable = coalesce( request.getParameter("n_facturable") , "");
            remesa_remplazo.setCrossdocking  ( !crossdocking.equals("")?"S":"N" );
            remesa_remplazo.setCadena        ( !cadena.equals("")      ?"S":"N" );
            remesa_remplazo.setN_facturable  ( !n_facturable.equals("")?"S":"N" );
            remesa_remplazo.setBase          ( usuario.getBase()                );
            remesa_remplazo.setUsuario       ( usuario.getLogin()               );
            remesa_remplazo.setAgcrem        ( usuario.getId_agencia()          ); // agencia del movimiento mas no es la agencia de la remesa
            remesa_remplazo.setCreation_date ( "now()"                          );
            remesa_remplazo.setConcepto      ( CONCEPTO_CAMBIO_DESTINO );
            
            if ( !cantidadValida( remesa_remplazo ) )
                request.setAttribute("msg","Cantidad ha facturar no valida, ha sobrepasado limites maximos.");
            else if (remesa_remplazo.getRemitentes2()==null || remesa_remplazo.getRemitentes2().isEmpty()   )
                request.setAttribute("msg","Aun no ha ingresado los remitentes.");
            else if (remesa_remplazo.getDestinatarios2()==null || remesa_remplazo.getDestinatarios2().isEmpty()  )                
                request.setAttribute("msg","Aun no ha ingresado los destinatarios.");
            else {
                model.CambioDestinoSvc.saveCambioDestino(remesa_original, remesa_remplazo);            
                request.setAttribute("msg","Cambio de destino realizado con exito.");
                model.CambioDestinoSvc.obtenerRemesa(remesa_remplazo.getCia(), remesa_remplazo.getNumrem());
            }
            
            
            next = "/jsp/despacho/cambio_destino/nuevo.jsp";
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }    

    
    /**
     * Metodo para validar campos nulos
     * @autor mfontalvo
     * @param variable variable a procesar
     * @param strdefault campos default a retornar
     */
    public static String coalesce(String variable, String strdefault) {
        return (variable!=null?!variable.equals("")?variable:strdefault:strdefault);
    }    
    
    
    
    
    
    public String loadRemitentes(String next) throws Exception {
        try{
            
            String dstrct    = coalesce( request.getParameter("dstrct")   , "");
            String codcli    = coalesce( request.getParameter("codcli")   , "");
            String ciudad    = coalesce( request.getParameter("ciudad")   , "");
            String ciudadA   = coalesce( request.getParameter("ciudadA")   , "");
            String nomciu    = coalesce( request.getParameter("nomciu")   , "");
            String operacion = coalesce( request.getParameter("operacion"), "");
            String modo      = coalesce( request.getParameter("modo")     , "");
            String [] LOV = request.getParameterValues("LOV");
            Remesa2 rr = (modo.equals("CONSULTA")?model.CambioDestinoSvc.getRemesa():model.CambioDestinoSvc.getRemesaRemplazo());
            Vector  v  = model.CambioDestinoSvc.getRemitentes();
            
            
            if ( operacion.equals("ASIGNAR") && modo.equals("MODIFICACION") ){
                
                if ( v!=null && !v.isEmpty() ){
                    
                    // seteo de los nuevos remitentes
                    Ciudad c = new Ciudad();
                    c.setCodigo ( ciudadA );
                    c.setNombre ( nomciu  );
                    c.setDetalle( new Vector() );
                    for (int i = 0; LOV!=null && i<LOV.length; i++){                        
                        RemiDest rd = (RemiDest) v.get( Integer.parseInt( LOV[i] ) );
                        c.getDetalle().add( rd );
                    }
                    
                    // asignacion de remitentes por ciudad
                    if (rr.getRemitentes2()==null)
                        rr.setRemitentes2( new TreeMap() );
                    if ( !c.getDetalle().isEmpty() )
                        rr.getRemitentes2().put(c.getCodigo(), c );
                    else
                        rr.getRemitentes2().remove( c.getCodigo() );
                }
            } else  if ( operacion.equals("LOAD_CIUDAD") ){
                model.CambioDestinoSvc.obtenerCiudades(); 
            }
            
                       
            model.CambioDestinoSvc.obtenerRemitentes( ciudad, codcli );
            v  = model.CambioDestinoSvc.getRemitentes();
            
            if ( rr.getRemitentes2()!=null ){
                Ciudad cs = (Ciudad) rr.getRemitentes2().get( ciudad );            
                if (cs!=null){
                    Vector re = cs.getDetalle();
                    for ( int i = 0; v!=null && i < v.size() ; i++ ){
                        RemiDest r1 = (RemiDest) v.get(i);
                        for ( int j = 0; re!=null && j < re.size() ; j++ ){
                            RemiDest r2 = (RemiDest) re.get(j);
                            if ( r1.getCodigo().equals( r2.getCodigo() )  ){
                                r1.setTipo(" checked ");
                                break;
                            }
                        }
                    }
                }
            }
            
            
            
            next = "/jsp/despacho/cambio_destino/remidest.jsp";
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }
    
    
   
    public String loadDestinatarios(String next) throws Exception {
        try{
            
            String dstrct    = coalesce( request.getParameter("dstrct")   , "");
            String codcli    = coalesce( request.getParameter("codcli")   , "");
            String ciudad    = coalesce( request.getParameter("ciudad")   , "");
            String ciudadA   = coalesce( request.getParameter("ciudadA")   , "");
            String nomciu    = coalesce( request.getParameter("nomciu")   , "");
            String operacion = coalesce( request.getParameter("operacion"), "");
            String modo      = coalesce( request.getParameter("modo")     , "");
            String [] LOV = request.getParameterValues("LOV");
            Remesa2 rr = (modo.equals("CONSULTA")?model.CambioDestinoSvc.getRemesa():model.CambioDestinoSvc.getRemesaRemplazo());
            Vector  v  = model.CambioDestinoSvc.getDestinatarios();
            
            
            if ( operacion.equals("ASIGNAR") && modo.equals("MODIFICACION") ){
                
                if ( v!=null && !v.isEmpty() ){
                    
                    // seteo de los nuevos remitentes
                    Ciudad c = new Ciudad();
                    c.setCodigo ( ciudadA );
                    c.setNombre ( nomciu  );
                    c.setDetalle( new Vector() );
                    for (int i = 0; LOV!=null && i<LOV.length; i++){                        
                        RemiDest rd = (RemiDest) v.get( Integer.parseInt( LOV[i] ) );
                        c.getDetalle().add( rd );
                    }
                    
                    // asignacion de remitentes por ciudad
                    if (rr.getDestinatarios2()==null)
                        rr.setDestinatarios2( new TreeMap() );
                    if ( !c.getDetalle().isEmpty() )
                        rr.getDestinatarios2().put(c.getCodigo(), c );
                    else
                        rr.getDestinatarios2().remove( c.getCodigo() );
                }
            } else  if ( operacion.equals("LOAD_CIUDAD") ){
                model.CambioDestinoSvc.obtenerCiudades(); 
            }
            
                       
            model.CambioDestinoSvc.obtenerDestinatarios( ciudad, codcli );
            v  = model.CambioDestinoSvc.getDestinatarios();
            
            if ( rr.getDestinatarios2()!=null ){
                Ciudad cs = (Ciudad) rr.getDestinatarios2().get( ciudad );            
                if (cs!=null){
                    Vector re = cs.getDetalle();
                    for ( int i = 0; v!=null && i < v.size() ; i++ ){
                        RemiDest r1 = (RemiDest) v.get(i);
                        for ( int j = 0; re!=null && j < re.size() ; j++ ){
                            RemiDest r2 = (RemiDest) re.get(j);
                            if ( r1.getCodigo().equals( r2.getCodigo() )  ){
                                r1.setTipo(" checked ");
                                break;
                            }
                        }
                    }
                }
            }
            
            
            
            next = "/jsp/despacho/cambio_destino/remidest.jsp";
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
        return next;
    }
    
    
    public void parametros() throws Exception{
        System.out.println("*******************************************");
        Enumeration en = request.getParameterNames();
        while (en.hasMoreElements()){
            String ele = en.nextElement().toString();
            String [] valores = request.getParameterValues(ele);
            for (int i = 0 ; i < valores.length ; i++)
                System.out.println(  ele + (valores.length>1?"["+i+"]":"") + " : " + valores[i]);
        }
        
    }
    
    
    public boolean cantidadValida (Remesa2 r ) throws Exception  {
        double maximo_valor =0;
        model.tablaGenService.obtenerRegistro("MAXCANSJ",r.getStd_job_no(), "");
        if(model.tablaGenService.getTblgen()!=null){
            try{
                maximo_valor =Double.parseDouble(model.tablaGenService.getTblgen().getReferencia());
            }catch(NumberFormatException ne){
                maximo_valor = 0;
            }
        }
        double maxton = maximo_valor==0?    50:maximo_valor;
        double maxmt3 = maximo_valor==0?   100:maximo_valor;
        double maxkl  = maximo_valor==0? 55000:maximo_valor;
        double maxgl  = maximo_valor==0?500000:maximo_valor;
        double maxvi  = maximo_valor==0?     1:maximo_valor;


        String  uw        = r.getUnit_of_work();
        double  cfacturar = r.getPesoreal();
        boolean sw        = true; 

        if(uw.indexOf("T")==0){//toneladas
            if(cfacturar>maxton){
                sw = false;
            }
        }
        if(uw.indexOf("M")==0){//MT3
            if(cfacturar>maxmt3){
                sw = false;
            }
        }
        if(uw.indexOf("L")==0){//KILO
            if(cfacturar>maxkl){
                sw = false;
            }
        }
        if(uw.indexOf("G")==0){//GALONES
            if(cfacturar>maxgl){
                sw = false;
            }
        }
        if(uw.indexOf("V")==0 || uw.indexOf("W")==0){//VIAJES
            if(cfacturar>maxvi){
                sw = false;
            }
        }                   
        return sw;
    }
}
