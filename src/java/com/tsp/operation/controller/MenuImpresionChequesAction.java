
/***************************************
 * Nombre Clase ............. MenuImpresionChequesAction.java
 * Descripci�n  .. . . . . .  Permite manejar los eventos para la impresion de cheques
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  26/01/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import java.sql.SQLException;




public class MenuImpresionChequesAction extends Action {
    
    
    private String PREFIJO_LASER = "LASER ";
    
    
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session   = request.getSession();
        Usuario usuario       = (Usuario) session.getAttribute("Usuario");
        String Login          = usuario.getLogin();
        String Distrito       = usuario.getDstrct();
        String usuarioName    = usuario.getNombre();
        String agencia        = usuario.getId_agencia();
        String next           = "/cheques/ImpresionCheques.jsp?marco=no&comentario=Error..";
        String  Verificacion1 = request.getParameter("Verificacion")!=null?request.getParameter("Verificacion"):"true";
        boolean Verificacion2 = Verificacion1.equals("true")?true:false;
        String comentario     = "";
        String prueba         = "";
        String opcionPrint    = request.getParameter("opcion");
        int cantidad          = 0;
        int swWindow          = 0;
        int total=Integer.parseInt(request.getParameter("total"));
        ImpresionCheque chequeToImprimir = null;
        
        try{
            //CUANDO MANDAN A IMPRIMIR
            if(model.ImprimirChequeSvc.getCheques()!=null && total>0){
                
                // ACTIVAMOS CHEQUES A IMPRIMIR
                if(opcionPrint.equals("not")){
                    for(int i =1;i<=total;i++){
                        int    parametro = Integer.parseInt(request.getParameter( "parametro" + i ));
                        String laser     = ( request.getParameter("laser" + i)==null )?"N": request.getParameter("laser" + i) ;
                        model.ImprimirChequeSvc.active( parametro, 1, laser ); // Activamos el cheque, indicamos si es por Laser.
                        model.ImprimirChequeSvc.setVerificacion(Verificacion2);
                    }
                }
                
                // ACEPTA LA IMPRESION
                else if(opcionPrint.equals("yes")){
                    try{
                        int id = Integer.parseInt( request.getParameter("id") );
                        ImpresionCheque cheque = model.ImprimirChequeSvc.getCheque(id);
                        if(cheque!=null){
                         
                                //if(cheque.getTope() >=(cheque.getNumber()) ){
                            
                            
                         //--- cargamos el cheque a imprimir
                                chequeToImprimir=cheque;
                                
                        //---  actualizamos el registro en la BD
                                
                                String sql = model.ImprimirChequeSvc.updateRecord(cheque,  Login);
                                model.tService.crearStatement();
                                model.tService.getSt().addBatch(sql);
                                model.tService.execute();
                                
                                
                                
                          //--- Registramos anticipos a terceros
                                try{
                                    model.AnticiposPagosTercerosSvc.pasarAnticipos(cheque,agencia);
                                }catch(Exception e){   e.printStackTrace();   }
                                
                                
                         //---  actualizamos la lista (borra el cheque de la lista general)
                                model.ImprimirChequeSvc.update(cheque);
                                
                            
                         //       if( cheque.getTope() == cheque.getNumber() )
                         //           model.ImprimirChequeSvc.finallySerie(cheque,  Login);
                            
                                
                             //}else
                             //     model.ImprimirChequeSvc.finallySerie(cheque,  Login);
                            
                                 
                        } else {
                            comentario="El cheque  ha sido impreso...";
                        }
                        
                        
                    }catch(Exception e){
                        throw new ServletException(e.getMessage());
                    }
                    
                }
                
                // CANCELA LA IMPRESION
                else if(opcionPrint.equals("cancel")){
                    int id = Integer.parseInt( request.getParameter("id") );
                    model.ImprimirChequeSvc.cancel(id);
                }
                
                
                
                
                
                try{
                    
                    
                    //BUSCO EL CHEQUE A IMPRIMIR: SACAMOS  DE LA LISTA
                    List listaImpresion = model.ImprimirChequeSvc.getListImpresion();
                    ImpresionCheque cheque=null;
                    try{
                        if(listaImpresion!=null && listaImpresion.size()>0){
                            Iterator itPrint = listaImpresion.iterator();
                            while(itPrint.hasNext()){
                                cheque   = (ImpresionCheque)itPrint.next();
                                swWindow = 1;
                                break;
                            }
                        }
                    }catch(Exception  e){
                        throw new ServletException("Error buscando el cheque a imprimir: "+e.getMessage());
                    }
                    
                    
                    
                    if(model.ImprimirChequeSvc.getCheques()==null || model.ImprimirChequeSvc.getCheques().size()==0)
                        comentario="No hay registros de cheques para ser impresos...";
                    
                    cantidad =(listaImpresion!=null)?listaImpresion.size():0;
                    
                    
                    
                    //CHEQUE A IMPRIMIR: VALIDAMOS Y FORMATEAMOS DATOS
                    try{
                        if(swWindow==1){
                            String msj = (cantidad>1)?",  por favor seleccione nuevamente cheques a imprimir":"";
                            cheque = model.ImprimirChequeSvc.format(cheque, Login );
                            if(!cheque.isNoSerie()){
                                
                                String bancoEsquema = ( cheque.getLaser().equals("S")  )?  PREFIJO_LASER + cheque.getBanco()  :  cheque.getBanco();
                                
                                //cheque.setPositionColumn( model.ImprimirChequeSvc.getEsquema( cheque.getDistrito(), bancoEsquema  ) );
                                cheque.setPositionColumn( model.ImprimirChequeSvc.getEsquemaCMS( bancoEsquema  ) );
                                
                                List listaEsquema = cheque.getPositionColumn();
                                if(listaEsquema!=null && listaEsquema.size()>0){
                                    model.ImprimirChequeSvc.setActivo(cheque.getId());
                                    request.setAttribute("chequePrint",cheque);
                                }
                                else{
                                    comentario = "No existe esquema de Impresi�n definido para el banco [ " + bancoEsquema +" ]" + msj;
                                    model.ImprimirChequeSvc.resetListImpresion();
                                }
                            }
                            else{
                                comentario = "No existe serie definida para el usuario " + Login + " en el banco [ " + cheque.getBanco() +" "+  cheque.getAgenciaBanco() + " ] en Anticipos " + msj;
                                model.ImprimirChequeSvc.resetListImpresion();
                            }
                        }
                    }catch(Exception  e){
                        throw new ServletException("Error formateando los cheques: "+e.getMessage());
                    }
                    
                    
                    
                }catch(ServletException  e){
                    throw new ServletException("Error: "+e.getMessage());
                }
                
                
            }
            else{
                //--- Realizamos la Consulta
                List lista = model.ImprimirChequeSvc.searchCheques(Distrito, agencia, usuario.getBase(),usuario.getLogin());
                comentario = (lista==null || lista.size()==0)?"No hay cheque para imprimir":"";
            }
            
            //--- LLamamos la pagina JSP
            request.setAttribute("chequeToImprimir",chequeToImprimir);
            model.ImprimirChequeSvc.setChequeToPrint( chequeToImprimir );
            next = "/cheques/ImpresionCheques.jsp?comentario="+comentario+"&total="+cantidad+"&ventana="+String.valueOf(swWindow)+"&prueba="+prueba+"&usuario="+Login;
            
        }
        catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
    
}
