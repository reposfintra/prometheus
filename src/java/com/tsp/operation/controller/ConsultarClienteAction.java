/*
 * ConsultarConductor.java
 *
 * Created on 6 de enero de 2005, 09:00 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class ConsultarClienteAction extends Action{
    
    /** Creates a new instance of ConsultarConductor */
    public ConsultarClienteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        Usuario usuario = null;
        String a=request.getParameter ("var");
        String nombre = request.getParameter("nomcli");
        String liquidador = request.getParameter("liquidador");
        String next="";
        HttpSession session  = request.getSession();        
        usuario = (Usuario) session.getAttribute("Usuario"); 
        if (a==null)
        {
            next="/consultas/consultasRedirect.jsp?cliente=ok";
        }
        else
        {
            next="/consultas/consultasRedirect.jsp?var=ok";
        }
        try{
            if (a==null)
            {
                model.clienteService.consultarCliente(nombre+"%");
                if(liquidador!=null){
                    next="/consultas/consultasRedirect.jsp?var=ok";
                }
            }
            else
            {
                model.clienteService.consultarClienteFen(nombre+"%",usuario.getCedula()); 
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
