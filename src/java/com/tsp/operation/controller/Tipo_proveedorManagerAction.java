/*
 * Tipo_proveedorManagerAction.java
 *
 * Created on 22 de septiembre de 2005, 09:22 PM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  Jm
 */
public class Tipo_proveedorManagerAction extends Action {
    private static String cod;
    private static String desc;
    /** Creates a new instance of Tipo_proveedorManagerAction */
    public Tipo_proveedorManagerAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String codigo_proveedor       = (request.getParameter("codigo")!=null)?request.getParameter("codigo"):"";
            String descripcion            = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):"";
            String original               = (request.getParameter("original")!=null)?request.getParameter("original"):"";
            String original2              = (request.getParameter("original2")!=null)?request.getParameter("original2"):"";
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String dstrct                 = user.getDstrct();
            
            String []LOV                  = request.getParameterValues("LOV");
            String Mensaje                = "";
            
            
            
            if (Opcion.equals("Guardar")){
                if(!model.TproveedorSvc.EXISTE_TP(codigo_proveedor,descripcion)){
                    model.TproveedorSvc.INSERT(dstrct, codigo_proveedor, descripcion, usuario);
                    Mensaje = "El Registro ha sido agregado";
                }
                else
                    Mensaje = "El Registro ya existe";
            }
            
            
            if (Opcion.equals("Modificar")){
                if(!model.TproveedorSvc.EXISTE_TP(codigo_proveedor,descripcion)){
                    model.TproveedorSvc.UPDATE_TP(codigo_proveedor, descripcion, usuario,original.split(",")[0] ,original.split(",")[1]);
                    model.TproveedorSvc.ReiniciarDato();
                    Mensaje = "El Registro ha sido modificado";
                }
                else{
                    Mensaje = "El Registro ya se encuentra registrado";
                }
            }
            
            if (Opcion.equals("Nuevo"))
                model.TproveedorSvc.ReiniciarDato();
            
            if (Opcion.equals("Ocultar Lista")){
                model.TproveedorSvc.ReiniciarLista();
                ////System.out.println("ENTRO OCULTAR");
            }
            
            if (Opcion.equals("Anular")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.TproveedorSvc.UPDATEESTADO_TP("A", cod, desc, usuario);
                }
                Mensaje = "El Registro ha sido anulado";
            }
            if (Opcion.equals("Activar")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.TproveedorSvc.UPDATEESTADO_TP("", cod, desc, usuario);
                }
                Mensaje = "El Registro ha sido activado ";
            }
            if (Opcion.equals("Eliminar")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.TproveedorSvc.DELETE_TP(cod, desc);
                }
                Mensaje = "El Registro ha sido eliminado ";
            }
            
            
            if(Opcion.equals("Seleccionar")){
                this.Conversion(original2);
                model.TproveedorSvc.SEARCH_TP(cod, desc);
            }
            
            if (Opcion.equals("Listado")){
                model.TproveedorSvc.LIST();
            }
            
            
            
            if ("Listado|Modificar|Guardar|Eliminar|Anular|Activar".indexOf(Opcion) != -1) {
                model.TproveedorSvc.LIST();
            }
            String next ="/jsp/cxpagar/Tipo_proveedor/Tipo_proveedor.jsp?Mensaje="+Mensaje;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en Tipo_proveedorManagerAction .....\n"+e.getMessage());
        }
    }
    
     public static void Conversion(String f){
        String vF [] = f.split(",");
        cod = vF[0];
        desc = vF[1];
    }
    
}
