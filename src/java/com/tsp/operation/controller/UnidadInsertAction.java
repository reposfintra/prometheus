/*
 * Nombre        UnidadInsertAction.java
 * Autor         Ing Jose de la Rosa
 * Modificado    Ing Sandra Escalante
 * Fecha         26 de junio de 2005, 11:15 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UnidadInsertAction extends Action{
    
    /** Creates a new instance of UnidadInsertAction */
    public UnidadInsertAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException {
        
        String next = "/jsp/trafico/unidad/UnidadInsertar.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String cia = (String) session.getAttribute("Distrito");
        String codigo = request.getParameter("c_codigo").toUpperCase();
        String descripcion = request.getParameter("c_descripcion").toUpperCase();
        String tipo = request.getParameter("c_tipo");

        try{
        
            Unidad unidad = new Unidad();
            unidad.setDescripcion(descripcion);
            unidad.setCodigo(codigo);
            unidad.setUser_update(usuario.getLogin());
            unidad.setCreation_user(usuario.getLogin());
            unidad.setTipo(tipo);
            
            try{
                model.unidadTrfService.insertUnidad(unidad);
                request.setAttribute("mensaje","Unidad ingresada exitosamente!");
            }catch(Exception ex){
                request.setAttribute("mensaje","El codigo de Unidad digitado ya existe!");
            }
        }catch(Exception e){
             e.printStackTrace();   
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
