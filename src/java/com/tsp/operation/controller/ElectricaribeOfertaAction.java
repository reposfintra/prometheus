package com.tsp.operation.controller;

import java.io.IOException;
import javax.servlet.http.HttpSession;
import com.tsp.operation.model.beans.*;

public class ElectricaribeOfertaAction extends Action{

    public ElectricaribeOfertaAction(){
    }

    public void run(){
        HttpSession session     = request.getSession();
        Usuario     usuario     = (Usuario) session.getAttribute("Usuario");
        String      msjGenerar  = "";
        String      msjPdf      = "PDF creado exitosamente";
        String      msjAnular   = "";
        String      num         = "";
        OfertaElca  oelca;

        try {
            if(request.getParameter("opcion").equals("1")){
                try {
                    num = request.getParameter("num_oferta");

                    if(num!=null){
                        model.ElectricaribeOfertaSvc.iniciar(request.getParameter("num_oferta"), usuario.getLogin());

                        String tipo = model.ElectricaribeOfertaSvc.getOfferDao().getOferta().getTipo_solicitud();

                        if(tipo.equals("Emergencia")){
                            model.ElectricaribeOfertaSvc.doEmergencyPDF();
                        }
                        else{
                            model.ElectricaribeOfertaSvc.doOfferPDF();
                        }

                        model.ElectricaribeOfertaSvc.cerrar();
                    }
                }
                catch (Exception e){
                    msjPdf = model.ElectricaribeOfertaSvc.returnNext();
                }
            }

            if(request.getParameter("opcion").equals("2")){
                try {
                    if ((request.getParameter("id")!=null) &&
                        (request.getParameter("nom_oferta")!=null) &&
                        (request.getParameter("text_consi")!=null) &&
                        (request.getParameter("consideraciones")!=null) ){

                        oelca = new OfertaElca();
                        oelca.setId_solicitud(          request.getParameter("id"));
                        oelca.setNombre_solicitud(      request.getParameter("nom_oferta"));
                        oelca.setOtras_consideraciones( request.getParameter("text_consi"));
                        oelca.setConsideraciones(       request.getParameter("consideraciones"));
                        oelca.setOficial(               request.getParameter("meses_mora"));

                        if(request.getParameter("cambiarcons").equals("true")){
                            model.ElectricaribeOfertaSvc.actualizarOferta(usuario, oelca, false);
                        }
                        else{
                            model.ElectricaribeOfertaSvc.actualizarOferta(usuario, oelca, true);
                        }

                        msjGenerar = model.ElectricaribeOfertaSvc.returnNext();

                        oelca = null;
                    }
                }
                catch (Exception e){
                    msjGenerar = "Error generando la oferta";
                }
            }

            if(request.getParameter("opcion").equals("3")){
                try {
                    this.dispatchRequest("/jsp/delectricaribe/pdf_electricaribe.jsp");
                }
                catch (Exception Exception){
                }
            }

            if(request.getParameter("opcion").equals("4")){
                try {
                    if ((request.getParameter("id")!=null) &&
                        (request.getParameter("text_consi")!=null) ){
                        oelca = new OfertaElca();
                        oelca.setId_solicitud(          request.getParameter("id"));
                        oelca.setOtras_consideraciones( request.getParameter("text_consi"));
                        oelca.setConsideraciones(       request.getParameter("anulaciones"));

                        msjAnular=model.ElectricaribeOfertaSvc.actualizarAnulacionOferta(oelca, false,usuario.getLogin());

                        oelca = null;
                    }
                } catch (Exception e){
                    System.out.println("Error anulando oferta"+e.toString()+"_"+e.getMessage());
                    msjAnular = "Error denegando la oferta";
                }
            }
        }
        catch (Exception ex){
            msjGenerar  = "Error, hay una excepcion";
            msjPdf      = "Error, hay una excepcion";
            msjAnular   = "Error, hay una excepcion";
        }



        /*
         * Lo siguiente es para mandar las respuestas
         * al metodo Ajax.Request que esta en la pagina.
         */

        if(request.getParameter("opcion").equals("1")){
            try {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(msjPdf);
            }
            catch (IOException ex) {
            }
        }

        if(request.getParameter("opcion").equals("2")){
            try {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(msjGenerar);
            }
            catch (IOException ex) {
            }
        }

         if(request.getParameter("opcion").equals("4")){
            try {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(msjAnular);
            }
            catch (IOException ex) {
                System.out.println("error raro: "+ex.toString()+"_"+ex.getMessage());
            }
        }
    }
}