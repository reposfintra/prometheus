/*
 * VExternosManagerAction.java
 *
 * Created on 30 de julio de 2005, 02:41 PM
 */

package com.tsp.operation.controller;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  jescandon
 */
public class VExternosManagerAction extends Action{
    private static String pla;
    private static String fd;
    /** Creates a new instance of VExternosManagerAction */
    public VExternosManagerAction() {
    }
    
    public void run() throws javax.servlet.ServletException {
        try{
            String sel                    = (request.getParameter("Sel")!=null)?request.getParameter("Sel"):"";
            String Opcion                 = request.getParameter("Opcion");
            String placa                  = (request.getParameter("placa")!=null)?request.getParameter("placa").toUpperCase():"";
            String fecha_d                = request.getParameter("fechadisp");
            String tv                     = request.getParameter("tiempovigencia");
            String agencia_disp           = request.getParameter("agencia");
            String agencia                = request.getParameter("filtroagencias");
            
            String vplaca                 = (request.getParameter("vplaca")!=null)?request.getParameter("vplaca").toUpperCase():"";
            String vfecha_d               = request.getParameter("vfechadisp");
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String base                   = user.getBase();
            String UserName               = user.getLogin();
            String AgenciaCentral         = user.getId_agencia();
            String distrito               = user.getCia();
            
         
            String []LOV                  = request.getParameterValues("LOV");
            String Mensaje                = "";
            String listar                 = "";
            
            String next                   = "";
            //Validacion Opcion
            if(Opcion == null )
                Opcion = "Mostrar";
            java.sql.Timestamp fecha_disp = new java.sql.Timestamp(3555);
            int tiempo_vigencia = 0;
            
            Util u = new Util();
            
            vfecha_d    +=  " 08:00:00";
        
            if( fecha_d != null ){
                if( fecha_d.length() > 0 ){
                    fecha_d = fecha_d + " 08:00:00";
                    fecha_disp = u.ConvertiraTimestamp(fecha_d);
                    tiempo_vigencia = Integer.parseInt(tv);
                }
            }
            
            
            if (Opcion.equals("Guardar")){
                if(model.placaService.placaExist(placa)){
                    if(!model.VExternosSvc.EXISTE(placa, fecha_disp)){
                        model.VExternosSvc.INSERT(placa, fecha_d, tiempo_vigencia, agencia_disp, base,UserName, distrito);
                        Mensaje = "El Registro ha sido agregado";
                    }
                    else
                        Mensaje = "El Registro ya existe";
                }
                else{
                    Mensaje = "La placa del vehiculo no existe";
                }
                
                next ="/VehExternos/VExternos.jsp?Mensaje="+Mensaje;
                
            }
            
            
            if (Opcion.equals("Modificar")){
                if(model.placaService.placaExist(placa)){
                    if(!model.VExternosSvc.EXISTE(placa, fecha_disp)){
                        model.VExternosSvc.UPDATE(tiempo_vigencia, agencia_disp, base, UserName, placa, fecha_disp, vplaca, vfecha_d);
                        model.VExternosSvc.SEARCH(placa, fecha_disp);
                        model.VExternosSvc.LIST(agencia_disp);
                        Mensaje = "El Registro ha sido modificado";
                    }
                    else
                        Mensaje = "El Registro ya existe";
                }
                else{
                    Mensaje = "La nueva placa no existe";
                }
                next ="/VehExternos/VExternosModificar.jsp?Mensaje="+Mensaje;
            }
            
                                                        
            
            if (Opcion.equals("Anular")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.VExternosSvc.UPDATEESTADO("A", UserName , pla, u.ConvertiraTimestamp(fd) );
                }
                Mensaje = "El Registro ha sido anulado";
            }
            
            if (Opcion.equals("Activar")){
                model.VExternosSvc.ReiniciarDato();
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.VExternosSvc.UPDATEESTADO("", UserName , pla, u.ConvertiraTimestamp(fd) );
                }
                Mensaje = "El Registro ha sido activado ";
            }
            if (Opcion.equals("Eliminar")){
                model.VExternosSvc.ReiniciarDato();
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.VExternosSvc.DELETE(pla, u.ConvertiraTimestamp(fd));
                }
                Mensaje = "El Registro ha sido eliminado ";
            }
            
            
            if (Opcion.equals("Listado")){                
                model.VExternosSvc.LIST(AgenciaCentral);
                
            }
            
            
            if (Opcion.equals("Mostrar")){
                model.VExternosSvc.LIST(agencia);
                if( model.VExternosSvc.VEXAGENCIA(agencia) == 0 && !agencia.equalsIgnoreCase("ALL")){
                    Mensaje = "No hay vehiculos externos relacionados a la Agencia " + model.ciudadService.obtenerNombreCiudad(agencia);
                    next ="/VehExternos/ListadoVE.jsp?Mensaje="+Mensaje;
                }
                else{
                    next ="/VehExternos/ListadoVE.jsp?Mensaje="+Mensaje;
                }
            }
            
            if(Opcion.equals("Seleccionar2")){
                this.Conversion(sel);
                model.VExternosSvc.SEARCH(pla, u.ConvertiraTimestamp(fd));
                next ="/VehExternos/VExternosModificar.jsp";
            }
            
           
            
            if ("Anular|Activar|Eliminar".indexOf(Opcion) != -1) {
                model.VExternosSvc.LIST(agencia);
                next ="/VehExternos/ListadoVE.jsp?Mensaje="+Mensaje;
            }
            
             
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en VExternosManagerAction .....\n"+e.getMessage());
        }
        
    }
    
    public static void Conversion(String f){
        String vF [] = f.split("/");
        pla = vF[0];
        fd = vF[1];
    }
    
    
}
