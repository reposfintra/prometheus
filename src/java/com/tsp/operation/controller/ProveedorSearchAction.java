/***********************************************
 * Nombre clase: ProveedorSearchAction.java
 * Descripci�n: Accion para buscar un proveedor.
 * Autor: Jose de la rosa
 * Fecha: 23 de enero de 2006, 03:47 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class ProveedorSearchAction extends Action{
    
    /** Creates a new instance of ProveedorSearchAction */
    public ProveedorSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        String nit = "";
        String nombre = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            next="/jsp/cxpagar/proveedor/ProveedorListar.jsp";
            String sw = (String) request.getParameter("sw");
            if(sw.equals("True")){
                nit = (String) request.getParameter("c_nit").toUpperCase();
                nombre = (String) request.getParameter("c_nombre");
            }
            else{
                nit = "";
                nombre = "";
            }
            session.setAttribute("nit", nit);
            session.setAttribute("nombre", nombre);
            if(model.tablaGenService.obtenerInformacionDato( "USRPROV",usuario.getLogin()) !=null){
                
                session.setAttribute("Control","S");
            }
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
