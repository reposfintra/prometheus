/*
 * CompaniaUpdateAction.java
 *
 * Created on 21 de enero de 2005, 08:59 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class CompaniaUpdateAction extends Action{
    
    /** Creates a new instance of CompaniaUpdateAction */
    public CompaniaUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException{
        
        String next="/compania/companiaUpdate.jsp";
        String dstrct               =   request.getParameter("dstrct").toUpperCase();
        try{
            
            model.ciaService.buscarCia(dstrct);
            
            if(request.getParameter("description")!=null){
                
                String description          =   request.getParameter("description").toUpperCase();
                String default_project      =   request.getParameter("default_project").toUpperCase();
                String nit                  =   request.getParameter("nit").toUpperCase();
                String nombre_aseguradora   =   request.getParameter("nombre_aseguradora").toUpperCase();
                String poliza_aseguradora   =   request.getParameter("poliza_aseguradora").toUpperCase();
                String fecha_ven_poliza     =   request.getParameter("fecha_ven_poliza").toUpperCase();
                String codigo_regional      =   request.getParameter("codigo_regional").toUpperCase();
                String codigo_empresa       =   request.getParameter("codigo_empresa").toUpperCase();
                String resolucion           =   request.getParameter("resolucion").toUpperCase();
                float peso_lleno_max        =   Float.parseFloat(request.getParameter("pmax"));
                float anticipo_max          =   Float.parseFloat(request.getParameter("pormax"));
                String hora                 =   request.getParameter("hora").toUpperCase();
                
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                
                String user_update          =   usuario.getLogin();
                
                Compania cia = new Compania();
                cia.setcodigo_empresa(codigo_empresa);
                cia.setcodigo_regional(codigo_regional);
                cia.setdefault_project(default_project);
                cia.setdescription(description);
                cia.setdstrct(dstrct);
                cia.setfecha_ven_poliza(fecha_ven_poliza);
                cia.setnit(nit);
                cia.setnombre_aseguradora(nombre_aseguradora);
                cia.setpoliza_aseguradora(poliza_aseguradora);
                cia.setresolucion(resolucion);
                cia.setuser_update(user_update);
                cia.setpeso_lleno_max(peso_lleno_max);
                cia.setanticipo_max(anticipo_max);
                cia.sethora(hora);
                model.ciaService.updateCia(cia);
                
                model.ciaService.setCia(null);
                model.ciaService.anularCia(cia);
                next="/compania/companiaUpdate.jsp?mensaje=El distrito "+dstrct+" ha sido modificado correctamente!";
                
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
    
    
}
