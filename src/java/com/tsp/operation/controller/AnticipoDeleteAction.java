/*
 * AnticipoDeleteAction.java
 *
 * Created on 3 de diciembre de 2004, 09:06 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AnticipoDeleteAction  extends Action{
    
    /** Creates a new instance of AnticipoDeleteAction */
    public AnticipoDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/anticipo/anticipoDelete.jsp?mensaje=ok";
        String nit = request.getParameter("nit");
        String sucursal = request.getParameter("codigo");
        try{
            model.proveedoranticipoService.buscaProveedor(nit,sucursal);
            if(model.proveedoranticipoService.getProveedor()!=null){
                Proveedor_Anticipo pa= model.proveedoranticipoService.getProveedor();
                model.proveedoranticipoService.anularProveedor(pa);
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
