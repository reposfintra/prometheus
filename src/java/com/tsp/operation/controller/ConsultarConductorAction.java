/*
 * ConsultarConductor.java
 *
 * Created on 6 de enero de 2005, 09:00 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class ConsultarConductorAction extends Action{
    
    /** Creates a new instance of ConsultarConductor */
    public ConsultarConductorAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String nit = request.getParameter("nit");
        String next="/consultas/consultasRedirect.jsp?conductor=ok";
        try{
            model.conductorService.consultaConductor(nit);
            if(request.getParameter("despacho")!=null){
                next="/consultas/consultasRedirect.jsp?despacho=ok";
            }
            if(request.getParameter("propietario")!=null){
                next="/consultas/consultasRedirect.jsp?propietario=ok";
            }
            if(request.getParameter("hvida")!=null){
                next="/consultas/consultasRedirect.jsp?hvida=ok";
            }
            if(request.getParameter("registroT")!=null){
                next="/consultas/consultasRedirect.jsp?registroT=ok";
            }

            if(request.getParameter("registroTI")!=null){
                next="/consultas/consultasRedirect.jsp?registroTI=ok";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
