/*
 * TiempoAnularAction.java
 *
 * Created on 1 de febrero de 2005, 10:41 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class TiempoAnularAction extends Action{
    
    /** Creates a new instance of TiempoAnularAction */
    public TiempoAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next                 =   "/diferenciatiempos/tiempoAnular.jsp";
        String dstrct               =   request.getParameter("dstrct").toUpperCase();
        String stdjob               =   request.getParameter("sj").toUpperCase();
        String cf                   =   request.getParameter("cf").toUpperCase();
        String fec1                 =   request.getParameter("fec1").toUpperCase();
        String fec2                 =   request.getParameter("fec2").toUpperCase();
        HttpSession session         =   request.getSession();
        Usuario usuario             =   (Usuario) session.getAttribute("Usuario");
        String user_update          =   usuario.getLogin();
        try{
            Tiempo tiempo = new Tiempo();
            tiempo.setcf_code(cf);
            tiempo.setcreation_user(user_update);
            tiempo.setdstrct(dstrct);
            tiempo.setsj(stdjob);
            tiempo.settime_code_1(fec1);
            tiempo.settime_code_2(fec2);
            request.setAttribute("tiempo", null);
            
            model.tiempoService.setTiempo(tiempo);
            model.tiempoService.anular();
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
