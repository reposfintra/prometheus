/***************************************************************************
 * Nombre clase :                 EstadisticasViajesAction.javaa           *
 * Descripcion :                  Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Estadisticas mensuales de viajes         *
 * Autor :                         Ing. Juan Manuel Escandon Perez         *
 * Fecha :                         11 de enero de 2006, 09:50 AM           *
 * Version :                       1.0                                     *
 * Copyright :                     Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
public class EstadisticasViajesAction extends Action{
    
    /** Creates a new instance of EstadisticasViajesAction */
    public EstadisticasViajesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            String next = "";
            String Mensaje = "El proceso se ha iniciado";
            EstadisticasViajesXLS hilo = new EstadisticasViajesXLS();
            hilo.start();
            next = "/jsp/masivo/reportes/EstadisticasViajes/EstadisticasViajes.jsp?Mensaje="+Mensaje;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en EstadisticasViajesAction .....\n"+e.getMessage());
        }
    }
    
}
