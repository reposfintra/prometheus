/*
 * SalidaInsertAction.java
 *
 * Created on 26 de noviembre de 2004, 11:10 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  KREALES
 */
public class SalidaInsertAction extends Action{
  //2009-09-02
    
    String grupo="ninguno";
    float valor=0;
    float valorT=0;
    float valorA=0;
    float costo=0;
    float maxant=0;
    String dest="";
    String origen="";
    float acpmmax=0;
    float peajeamax=0;
    float peajebmax=0;
    float peajecmax=0;
    float aacpm=0;
    float apeaje=0;
    float aanticipo=0;
    Remesa rem;
    Stdjobcosto std;
    Usuario usuario;
    String no_rem="";
    double totalAjuste =0;
    boolean efectivo=false;
    
    /** Creates a new instance of SalidaInsertAction */
    public SalidaInsertAction() {
    }
    
    public String buscarStandard(String pla)throws ServletException{
        String sj="";
        try{
            model.remplaService.buscaRemPla(pla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rempla = model.remplaService.getRemPla();
                no_rem= rempla.getRemesa();
                model.remesaService.buscaRemesa(no_rem);
                if(model.remesaService.getRemesa()!=null){
                    rem=model.remesaService.getRemesa();
                    sj=rem.getStdJobNo();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        return sj;
    }
    
    
    public float buscarFull_W(String planilla)throws ServletException{
        float full_w=0;
        try{
            model.planillaService.bucaPlaaux(planilla);
            if(model.planillaService.getPlaaux()!=null){
                Plaaux plaaux= model.planillaService.getPlaaux();
                full_w=plaaux.getFull_weight();
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        return full_w;
        
    }
    
    
    public void buscarValores(String proveedorA, float cantA, String proveedorT, float peajea,float peajeb,float peajec )throws ServletException{
        
        try{
            String [] nitcod = null;
            nitcod = proveedorA.split("/");
            String sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            float valora=0, valorb=0, valorc=0;
            efectivo=false;
            model.proveedoracpmService.buscaProveedor(nitcod[0],sucursal);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm pacpm = model.proveedoracpmService.getProveedor();
                float vacpm= pacpm.getValor();
                valorA= vacpm * cantA;
                ////System.out.println("--------SERVICIO DE LA BOMBA...."+pacpm.getTipo());
                if(pacpm.getTipo().equals("E")){
                    efectivo=true;
                }
                
            }
            
            model.peajeService.buscar("A");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valora = vt * peajea;
            }
            model.peajeService.buscar("B");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valorb = vt * peajeb;
            }
            
            model.peajeService.buscar("C");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valorc = vt * peajec;
            }
            
            valorT = valora+valorb+valorc;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    
    public void buscarValor(String sj, String placa, float pesoreal)throws ServletException{
        
        try{
            model.stdjobcostoService.buscaStdJobCosto(sj);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                this.std = model.stdjobcostoService.getStandardCosto();
                maxant=std.getporcentaje_maximo_anticipo();
                costo = std.getUnit_cost();
                valor= pesoreal * costo;
            }
            
            model.plkgruService.buscaPlkgru(placa,sj);
            if(model.plkgruService.getPlkgru()!=null){
                Plkgru plk = model.plkgruService.getPlkgru();
                grupo=plk.getGroupcode();
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    Stdjobplkcosto std = model.stdjobplkcostoService.getStd();
                    costo = std.getUnit_cost();
                    valor= pesoreal * costo;
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    
    public String[] insertTiempo(String planilla )throws ServletException{
        
        try{
            String vector[]= null;
            String sql="";
            List listTabla = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
            Iterator itTbla=listTabla.iterator();
            
            while(itTbla.hasNext()){
                
                Tbltiempo tbl = (Tbltiempo) itTbla.next();
                String tblcode = tbl.getTimeCode();
                int secuence = tbl.getSecuence();
                
                String fecha = request.getParameter(tbl.getTimeCode()).substring(0,10);
                int hora=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(0,2));
                String minuto=request.getParameter("h"+tbl.getTimeCode()).substring(3,5);
                
                
                fecha= fecha + " "+hora+":"+minuto+":00";
                Planilla_Tiempo pla_tiempo= new Planilla_Tiempo();
                
                pla_tiempo.setCf_code(std.getCf_code());
                pla_tiempo.setCreation_user(usuario.getLogin());
                pla_tiempo.setDate_time_traffic(fecha);
                pla_tiempo.setDstrct(usuario.getDstrct());
                pla_tiempo.setPla(planilla);
                pla_tiempo.setSecuence(secuence);
                pla_tiempo.setSj(std.getSj());
                pla_tiempo.setTime_code(tblcode);
                
                sql+=model.pla_tiempoService.insertTiempo(pla_tiempo,usuario.getBase())+"::";
            }
            vector = sql.split("::");
            return vector;
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    
    public void buscarAjuste(String pacpm, String pant, String ppeaje,float valorA,float anticipo,float valorT)throws ServletException{
        
        try{
            ////System.out.println("-------------------BUSCAR AJUSTE------------");
            float porcentaje = 0;
            String [] nitcod = null;
            nitcod = pacpm.split("/");
            String sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            
            //SE BUSCA EL AJUSTE DEL ACPM
            ////System.out.println("-------------------BUSCAR AJUSTE DEL ACPM------------");
            ////System.out.println("Codigo proveedor ACPM "+nitcod[0]+sucursal);
            model.proveedoracpmService.buscaProveedor(nitcod[0], sucursal);
            if(model.proveedoracpmService.getProveedor()!=null){
                porcentaje = model.proveedoracpmService.getProveedor().getPorcentaje()/100;
                ////System.out.println("Porcentaje del ACPM  "+porcentaje);
                aacpm= valorA *porcentaje;
            }
            
            //SE BUSCA EL AJUSTE DEL PEAJE
            ////System.out.println("-------------------BUSCAR AJUSTE DEL PEAJE------------");
            nitcod = ppeaje.split("/");
            sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            ////System.out.println("Codigo proveedor peaje "+nitcod[0]+sucursal);
            model.proveedortiquetesService.buscaProveedor(nitcod[0] ,sucursal);
            if(model.proveedortiquetesService.getProveedor()!=null){
                ////System.out.println("Encontre el proveedor de Peajes");
                porcentaje = model.proveedortiquetesService.getProveedor().getPorcentaje()/100;
                ////System.out.println("Porcentaje del peaje  "+porcentaje);
                apeaje = anticipo*porcentaje;
            }
            //SE BUSCA EL AJUSTE DEL ANTICIPO
            nitcod = pant.split("/");
            sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            model.proveedoranticipoService.buscaProveedor(nitcod[0],sucursal);
            ////System.out.println("Codigo proveedor anticipo "+nitcod[0]+sucursal);
            if(model.proveedoranticipoService.getProveedor()!=null){
                ////System.out.println("Encontre el proveedor de Anticipo");
                porcentaje = model.proveedoranticipoService.getProveedor().getPorcentaje()/100;
                ////System.out.println("Porcentaje del anticipo  "+porcentaje);
                aanticipo = valorT*porcentaje;
            }
            
            totalAjuste = aacpm+apeaje+aanticipo;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    public void run() throws ServletException, InformationException {
        
        int i=0;
        String sj;
        float full_w=0;
        float pesoreal=0;
        String next="";
        Planilla pla=new Planilla();
        String nit="";
        
        HttpSession session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            model.tService.crearStatement();
            
            String placa = request.getParameter("placa").toUpperCase();
            String planilla = request.getParameter("pla").toUpperCase();
            float pesoVacio = Float.parseFloat(request.getParameter("pesov"));
            float anticipo = Float.parseFloat(request.getParameter("anticipo"));
            String proveedorAVec[]= request.getParameter("proveedora").split("/");
            String proveedorA= proveedorAVec[0];
            float gacpm=Float.parseFloat(request.getParameter("gacpm"));
            String proveedorAcpm= request.getParameter("proveedorAcpm");
            int peajea = Integer.parseInt(request.getParameter("peajea"));
            int peajeb = Integer.parseInt(request.getParameter("peajeb"));
            int peajec = Integer.parseInt(request.getParameter("peajec"));
            String proveedorTVec[] = request.getParameter("tiquetes").split("/");
            String proveedorT = proveedorTVec[0];
            
            String sucursalA="";
            if(proveedorAVec.length>1){
                sucursalA = proveedorAVec[1];
            }
            String sucursalT="";
            if(proveedorTVec.length>1){
                sucursalT = proveedorTVec[1];
            }
            
            next="/controller?estado=Menu&accion=ImprimirCarbon&total=0&opcion=no&planilla="+planilla;
            
            ////System.out.println("Sucursales  Anticipo: "+sucursalA+" Tiquetes: "+sucursalT);
            sj=this.buscarStandard(planilla);
            full_w=Float.parseFloat(request.getParameter("pesoll"));
            pesoreal=full_w-pesoVacio;
            
            //Hago el update en la remesa.
            rem.setPesoReal(pesoreal);
            rem.setVlrRem(rem.getVlrRem()*pesoreal);
            model.tService.getSt().addBatch(model.remesaService.updateRemesa(rem));
            
            this.buscarValor(sj,placa, pesoreal);
            
            model.planillaService.buscaPlanilla(planilla);
            ////System.out.println("planilla no " + planilla);
            if(model.planillaService.getPlanilla()!=null){
                ////System.out.println("Actualizo la planilla " + planilla);
                pla= model.planillaService.getPlanilla();
                ////System.out.println("Valor de la pla " +valor);
                ////System.out.println("Peso real " +pesoreal);
                pla.setNumpla(planilla);
                pla.setVlrpla(valor);
                pla.setPesoreal(pesoreal);
                pla.setGroup_code(grupo);
                pla.setUnit_cost(costo);
                pla.setFeccum(request.getParameter("fecdsp"));
                pla.setUnit_transp("TON");
                nit=pla.getNitpro();
                //Hago el update de la planilla.
                ////System.out.println("Update planilla: "+model.planillaService.updatePlanilla(pla));
                model.tService.getSt().addBatch(model.planillaService.updatePlanilla(pla));
                
            }
            
            //PODEMOS ACTUALIZAR EL STANDARD SI LO CAMBIARON
            String descripcion="";
            model.stdjobdetselService.buscaStandardDetSel(request.getParameter("standard"));
            ////System.out.println("El standard seleccionado es: "+request.getParameter("standard"));
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                descripcion = stdjobdetsel.getSj_desc();
            }
            
            String numrem ="";
            model.remplaService.buscaRemPla(planilla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rp = model.remplaService.getRemPla();
                numrem = rp.getRemesa();
                rp.setFecha("'now()'");
                rp.setQuvrcm(pesoreal);
                rp.setObservacion("");
                rp.setPlanilla(planilla);
                rp.setRemesa(this.no_rem);
                
                model.tService.getSt().addBatch(model.remplaService.cumplirRemPla(rp));
            }
            ////System.out.println("El numero de la remesa es : "+numrem);
            model.remesaService.buscaRemesa(numrem);
            if(model.remesaService.getRemesa()!=null){
                ////System.out.println("OK la remesa no es nula");
                Remesa r = model.remesaService.getRemesa();
                r.setStdJobNo(request.getParameter("standard"));
                r.setDescripcion(descripcion);
                model.tService.getSt().addBatch(model.remesaService.updateRemesa(r));
            }
            String [] nitcod = null;
            nitcod = proveedorAcpm.split("/");
            
            float totalPeajes = peajea+peajeb+peajec;
            this.buscarValores(proveedorAcpm, gacpm, proveedorT,peajea,peajeb,peajec);
            //Inserto el movpla del Anticipo.
            
            ////System.out.println("Efectivo es "+efectivo);
            if(efectivo == true){
                anticipo = anticipo + valorA;
                gacpm=0;
                valorA=0;
            }
            
            //Hago el update de la planilla aux.
            String sucursal="";
            if(nitcod.length>1){
                sucursal = nitcod[1];
            }
            
            Plaaux plaaux= new Plaaux();
            plaaux.setPlanilla(planilla);
            plaaux.setAcpm_gln(gacpm);
            plaaux.setAcpm_supplier(nitcod[0]);
            plaaux.setAcpm_code(sucursal);
            plaaux.setCreation_user(usuario.getLogin());
            plaaux.setEmpty_weight(pesoVacio);
            plaaux.setTicket_a((float)peajea);
            plaaux.setTicket_b((float)peajeb);
            plaaux.setTicket_c((float)peajec);
            plaaux.setTicket_supplier(proveedorT);
            plaaux.setFull_weight(full_w);
            plaaux.setTiket_code(sucursalT);
            model.tService.getSt().addBatch(model.plaauxService.updatePlaaux(plaaux));
            
            Movpla movpla= new Movpla();
            if(anticipo!=0){
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(planilla);
                movpla.setSupplier(placa);
                movpla.setVlr((int)anticipo);
                movpla.setDocument_type("001");
                movpla.setConcept_code("01");
                movpla.setAp_ind("S");
                movpla.setProveedor(proveedorA);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setVlr_for(0);
                movpla.setSucursal(sucursalA);
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+1));
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                
            }
            
            
            //Inserto el movpla del acpm
            
            sucursal ="";
            if(nitcod.length>1){
                sucursal = nitcod[1];
            }
            
            if(valorA!=0){
                movpla= new Movpla();
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(planilla);
                movpla.setSupplier(placa);
                movpla.setVlr(valorA);
                movpla.setDocument_type("001");
                movpla.setConcept_code("02");
                movpla.setAp_ind("S");
                movpla.setProveedor(nitcod[0]);
                movpla.setSucursal(sucursal);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+10));
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
            }
            //inserto el movpla de los tiquetes
            if(valorT!=0){
                movpla= new Movpla();
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(planilla);
                movpla.setSupplier(placa);
                movpla.setVlr(valorT);
                movpla.setDocument_type("001");
                movpla.setConcept_code("03");
                movpla.setAp_ind("S");
                movpla.setProveedor(proveedorT);
                movpla.setSucursal(sucursalT);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
            }
            this.buscarAjuste(proveedorAcpm,proveedorA+"/"+sucursalA,proveedorT+"/"+sucursalT,valorA,anticipo,valorT);
            
            if(totalAjuste!=0){
                
                //inserto el movpla del ajuste del acpm
                movpla= new Movpla();
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(planilla);
                movpla.setSupplier(placa);
                movpla.setVlr((float)com.tsp.util.Util.redondear(totalAjuste,0));
                movpla.setDocument_type("001");
                movpla.setConcept_code("09");
                movpla.setAp_ind("S");
                movpla.setProveedor(proveedorA);
                movpla.setSucursal(sucursalA);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+12));
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                
            }
            
            String a []=this.insertTiempo(planilla);
            for(int j=0; j<=a.length-1; j++){
                model.tService.getSt().addBatch(a[j]);
                ////System.out.println(a[j]);
            }
            
            
            //AGREGAMOS LOS ANTICIPOS DINAMICOS..
            model.anticiposService.vecAnticipos(usuario.getBase(),sj);
            Vector ants = model.anticiposService.getAnticipos();
            for (int k=0 ; k<ants.size();k++){
                Anticipos ant = (Anticipos) ants.elementAt(k);
                model.anticiposService.searchAnticipos(usuario.getDstrct(), ant.getAnticipo_code(), sj);
                if(model.anticiposService.getAnticipo()!=null){
                    ant = model.anticiposService.getAnticipo();
                    float value = 0;
                    if(request.getParameter(ant.getAnticipo_code())!=null){
                        value = Float.parseFloat(request.getParameter(ant.getAnticipo_code()));
                    }
                    String prov="";
                    String nitPA="";
                    String sucursalPA="";
                    if(request.getParameter("provee"+ant.getAnticipo_code())!=null){
                        prov = request.getParameter("provee"+ant.getAnticipo_code());
                        String vec[]=prov.split("/");
                        nitPA = vec[0];
                        if(vec.length>1){
                            sucursalPA = vec[1];
                        }
                    }
                    movpla= new Movpla();
                    movpla.setAgency_id(usuario.getId_agencia());
                    movpla.setCurrency(std.getCurrency());
                    movpla.setDstrct(usuario.getDstrct());
                    movpla.setPla_owner(nit);
                    movpla.setPlanilla(planilla);
                    movpla.setSupplier(placa);
                    movpla.setVlr(value);
                    movpla.setDocument_type("001");
                    movpla.setConcept_code(ant.getAnticipo_code());
                    movpla.setAp_ind(ant.getIndicador());
                    movpla.setProveedor(nitPA);
                    movpla.setSucursal(sucursalPA);
                    movpla.setCreation_user(usuario.getLogin());
                    movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+12+k));
//                    model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                }
            }
            
            
            model.tService.execute();
            
            List plaAux = model.planillaService.getPlasIncompletas(usuario.getLogin());
            session.setAttribute("PlaIncompletas", plaAux);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
