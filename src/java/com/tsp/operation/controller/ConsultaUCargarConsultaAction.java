/******************************************************************
* Nombre ......................ConsultaUCargarConsultaAction.java
* Descripci�n..................Clase Action para generar el reporte de consultas
* Autor........................David lamadrid
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUCargarConsultaAction  extends Action
{
    
    /** Creates a new instance of ConsultaUCargarConsultaAction */
    public ConsultaUCargarConsultaAction ()
    {
    }
    
     public void run () throws ServletException, InformationException
    {
        
        String next="";
        try
        {
            String codigo=""+request.getParameter ("tConsultas");
            
            model.consultaUsuarioService.listarPorCodigo (codigo);
            
            ConsultaUsuarios consulta=new  ConsultaUsuarios();
            consulta = model.consultaUsuarioService.getConsulta ();
            
            //String tabla=""+request.getParameter ("c_tabla");
            String select =""+consulta.getCselect ();
            String otros =""+consulta.getCotros ();
            String from =""+consulta.getCfrom ();
            String where =""+consulta.getCwhere ();
            //System.out.println("where "+where);
          
            
            //setear el vSelect
            
            String selectaux=select.substring (7,select.length ());
            //System.out.println("selectaux"+selectaux);
            String[] cadena= selectaux.split (",");
            Vector vSelect =new Vector ();
            for(int i=0;i<cadena.length;i++){
                vSelect.add (""+cadena[i]);  
            }
            model.consultaUsuarioService.setVSelect (vSelect);
           

            
            ///setear el from
            Vector vFrom =new Vector ();
            String fromaux=from.substring (5,from.length ());
            //System.out.println("fromaux"+fromaux);
            String[] cadena2= fromaux.split (",");
            for(int i=0;i<cadena2.length;i++){
                String nombre = ""+cadena2[i];
                //System.out.println("CADENA "+nombre);
                String letra=nombre.substring (nombre.length ()-2,nombre.length ());
                nombre= nombre.substring (0,nombre.indexOf ("as")-1);
                //System.out.println("NOMBRE "+nombre+" LETRA"+letra);
                Vector fila =new Vector();
                fila.add (nombre);
                fila.add (letra);
                vFrom.add (fila);  
            }
            model.consultaUsuarioService.setVFrom (vFrom);

            
            next="/jsp/general/consultas/insertarconsultas.jsp?select="+select+"&from="+from+"&where="+where+"&otros="+otros+"&accion=";
        }
        catch (Exception e)
        {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
