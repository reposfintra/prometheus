/*********************************************************************************************
 *      Nombre Clase.................   ReporteVerifNitpropAction.java
 *      Descripci�n..................   Action para la generaci�n del reporte de anticipos
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   20.09.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ********************************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;
import org.apache.log4j.*;


public class ReporteVerifNitpropAction extends Action {
    
    Logger logger =  Logger.getLogger(this.getClass());
    
    public ReporteVerifNitpropAction() {
        
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        
        String next = "/jsp/general/reportes/RepVerifNitProp.jsp?msg=Se ha iniciado la generaci�n del reporte exitosamente.";
        
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        String fecha = s.format(d);
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String user = usuario.getLogin();
        
        String fechai = request.getParameter("FechaI");
        String fechaf = request.getParameter("FechaF");
        String cia = (String) session.getAttribute("Distrito");
        
        logger.info("FECHAI: " + fechai);
        logger.info("FECHAF: " + fechaf);
        logger.info("CIA: " + cia);
        
        try{
            RepVerifNitproTh hilo = new RepVerifNitproTh();
            hilo.start(model, user, fechai, fechaf, cia);
        }catch (Exception e){
           e.printStackTrace();
           throw new ServletException(e.getMessage());
        }        
        
        this.dispatchRequest(next);
        
    }
    
}
