/********************************************************************
 *      Nombre Clase.................   Campos_jspAnularAction.java    
 *      Descripci�n..................   Anula un campo en el archivo de campos_jsp.
 *      Autor........................   Ing. Rodrigo Salazar
 *      Fecha........................   18.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author  Rodrigo
 */
public class Campos_jspAnularAction extends Action{
    
    /** Creates a new instance of Campos_jspAnularAction */
    public Campos_jspAnularAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="/jsp/trafico/permisos/campos_jsp/Campos_jspModificar.jsp?reload=ok&mensaje=Campo Eliminado";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_pagina");
        String descripcion = request.getParameter("c_campo");
        //System.out.println(descripcion+" action "+codigo);
        try{
            
            campos_jsp cj = new campos_jsp();
            cj.setPagina(codigo);
            cj.setCampo(descripcion);
            cj.setUser_update(usuario.getLogin().toUpperCase());
            cj.setCia(usuario.getCia().toUpperCase());
            model.camposjsp.anularcampos_jsp(codigo,descripcion);
            request.setAttribute("mensaje","MsgAnulado");            
            model.tipo_ubicacionService.serchTipo_ubicacion(codigo);
            campos_jsp cj2 = model.camposjsp.getcampos_jsp();
            request.setAttribute("campos_jsp",cj2);
            //System.out.println(descripcion+" action "+codigo);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
