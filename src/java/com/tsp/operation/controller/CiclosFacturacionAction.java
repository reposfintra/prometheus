/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.CiclosFacturacionService;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Ing. Rhonalf Martinez (rhonalf) <rhonaldomaster@gmail.com>
 * @version 0.1
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class CiclosFacturacionAction extends Action{
    private int reg_malos;
    private String filas_malas;
    public CiclosFacturacionAction(){
    }

    @Override
    public void run() throws ServletException, InformationException {
        reg_malos = 0;
        filas_malas="";
        String opcion = "";
        String cadena = "";
        String next = "/jsp/fenalco/fenalco_metrotel/importacion_ciclos.jsp";
        boolean redirect = false;
        HttpSession session = request.getSession();
        try {
            opcion = request.getParameter("opcion")!= null ? request.getParameter("opcion"):"";
            if(opcion.equals("buscar_ciclos")){
                cadena = this.buscarCiclos();
            }
            else if(opcion.equals("verificar_anio")){
                cadena = this.verificarDato();
            }
            else if(opcion.equals("importar")){
                redirect = true;
                String anio = request.getParameter("anio")!= null ? request.getParameter("anio"):"0099";
                String actividad = request.getParameter("actividad")!= null ? request.getParameter("actividad"):"insercion";
                String filename = this.procesaFicheros();
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                String ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
                String nomfile = ruta + "/" + filename;
                CiclosFacturacionService cfserv = new CiclosFacturacionService();
                cadena = cfserv.eliminar(anio);
                cadena += "\n" + this.leerXls(nomfile);
                if(this.reg_malos==0){
                    int reg = 0;
                    reg = cfserv.ejecutarSQL(cadena);
                    if(reg>0){
                        cadena = "Se hizo "+actividad+" de registros en la base de datos";
                    }
                    else{
                        cadena = "No se hizo "+actividad+" de registros en la base de datos";
                    }
                }
                else{
                    cadena = "No se puede procesar la(s) fila(s) "+this.filas_malas+".\nHay datos que no cumplen con el formato.";
                            //+(this.filas_malas.substring(0, this.filas_malas.lastIndexOf(",")));
                }
                session.setAttribute("msj", cadena);
            }
        }
        catch (Exception e) {
            cadena = "Error al procesar el archivo";
            session.setAttribute("msj", cadena);
            System.out.println("Error en CiclosFacturacionAction.java: "+e.toString());
            e.printStackTrace();
        }
        finally {
            try {
                if(redirect==false){
                    this.escribirResponse(cadena);
                }
                else{
                    this.dispatchRequest(next);
                }
            }
            catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: "+e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }

    /**
     * Busca si existen datos para el a&ntilde;o seleccionado
     * , en caso de existir, avisa que se har&aacute; actualizaci&oacute;n, sino, que se hara inserci&oacute;n
     * @return cadena con la actividad a realizar
     */
    private String verificarDato(){
        String cadena = "insercion";
        try {
            String anio = request.getParameter("dato")!= null ? request.getParameter("dato"):"0099";
            CiclosFacturacionService cfserv = new CiclosFacturacionService();
            boolean existe = cfserv.existeDatosAnio(anio);
            if(existe==true) cadena = "actualizacion";
        }
        catch (Exception e) {
            System.out.println("Error verificando la informacion del a\u00f1o: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }

    /**
     * Busca los datos de ciclos de facturaci&oacute;n para un a&ntilde;o
     * @return Codigo de la tabla a mostrar
     */
    private String buscarCiclos(){
        String cadena = "";
        try {
            cadena = "<table width='100%' border='0' style='border-collapse: collapse;'>"
                            + "<thead>"
                                + "<tr class='subtitulo1'>"
                                    + "<th style='border: 0.1em solid black;'>Ciclo</th>"
                                    + "<th style='border: 0.1em solid black;'>Mes</th>"
                                    + "<th style='border: 0.1em solid black;'>Lectura</th>"
                                    + "<th style='border: 0.1em solid black;'>Liquidacion</th>"
                                    + "<th style='border: 0.1em solid black;'>Facturacion</th>"
                                    + "<th style='border: 0.1em solid black;'>Impresion</th>"
                                    + "<th style='border: 0.1em solid black;'>Distribucion</th>"
                                    + "<th style='border: 0.1em solid black;'>1 Venc</th>"
                                    + "<th style='border: 0.1em solid black;'>Suspension</th>"
                                    + "<th style='border: 0.1em solid black;'>2 Venc</th>"
                                    + "<th style='border: 0.1em solid black;'>Periodo</th>"
                                    + "<th style='border: 0.1em solid black;'>Dias</th>"
                                + "</tr>"
                            + "</thead>";
            CiclosFacturacionService cfserv = new CiclosFacturacionService();
            ArrayList<BeanGeneral> lista = null;
            String anio = request.getParameter("dato")!= null ? request.getParameter("dato"):"0099";
            try {
                lista = cfserv.buscarCiclosAnio(anio);
            }
            catch (Exception e) {
                System.out.println("Error: "+e.toString());
            }
            cadena += "<tbody>";
            if(lista!=null && lista.isEmpty()==false){
                for (int i = 0; i < lista.size(); i++) {
                    BeanGeneral b = lista.get(i);
                    cadena += "<tr class='filaazul'>"
                                    + "<td>"+(b.getValor_02())+"</td>"
                                    + "<td>"+(b.getValor_03())+"</td>"
                                    + "<td>"+(b.getValor_04())+"</td>"
                                    + "<td>"+(b.getValor_05())+"</td>"
                                    + "<td>"+(b.getValor_06())+"</td>"
                                    + "<td>"+(b.getValor_07())+"</td>"
                                    + "<td>"+(b.getValor_08())+"</td>"
                                    + "<td>"+(b.getValor_09())+"</td>"
                                    + "<td>"+(b.getValor_10())+"</td>"
                                    + "<td>"+(b.getValor_11())+"</td>"
                                    + "<td>"+(b.getValor_12())+"</td>"
                                    + "<td>"+(b.getValor_13())+"</td>"
                                + "</tr>";
                }
            }
            else{
                cadena += "<tr class='fila'><td colspan='12' align='center'>No se encontraron registros para el a\u00f1o "+anio+"</td></tr>";
            }
            cadena += "</tbody>";
        }
        catch (Exception e) {
            System.out.println("Error generando la tabla de resultados: "+e.toString());
            e.printStackTrace();
        }
        cadena += "</table>";
        return cadena;
    }

    /**
     * Metodo para subir un archivo al directorio del usuario en sesi&oacute;n
     * @return nombre del archivo subido
     */
    protected String procesaFicheros() {
        String nombre = "";
        try {
            // construimos el objeto que es capaz de parsear la peticion
            DiskFileUpload fu = new DiskFileUpload();
            // maximo numero de bytes
            fu.setSizeMax(1024*1024); // 1024 K
            // tama�o por encima del cual los ficheros son escritos directamente en disco
            fu.setSizeThreshold(4096);
            // directorio en el que se escribiran los ficheros con tama�o superior al soportado en memoria
            fu.setRepositoryPath("/tmp");
            // ordenamos procesar los ficheros
            List fileItems = fu.parseRequest(request);
            if(fileItems == null){
                depura("La lista de archivos es nula");
                nombre = "";
            }
            else{
                // Iteramos por cada fichero
                Iterator i = fileItems.iterator();
                FileItem actual = null;
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                String ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
                while (i.hasNext()){
                    actual = (FileItem)i.next();
                    String fileName = actual.getName();
                    // construimos un objeto file para recuperar el trayecto completo
                    File fichero = new File(fileName);
                    nombre = fichero.getName();
                    // nos quedamos solo con el nombre y descartamos el path
                    fichero = new  File(ruta + "/" + ( nombre.substring(nombre.lastIndexOf("\\") + 1, nombre.length()) ));
                    //nombre = nombre.substring(nombre.lastIndexOf("\\") + 1, nombre.length());
                    nombre = "importacion.xls";
                    // escribimos el fichero colgando del nuevo path
                    actual.write(fichero);
                    fichero.renameTo(new File(ruta+"/"+nombre));
                }
            }
        }
        catch(Exception e) {
            depura("Error de Aplicacion " + e.getMessage());
            e.printStackTrace();
            nombre = "";
        }
        return nombre;
    }

    /**
     * Metodo para escribir error
     * @param cadena error a mostrar
     */
    public void depura(String cadena){
        System.out.println("El error es: " + cadena);
    }

    /**
     * Dado un objeto tipo <i>Calendar</i> , devuelve la fecha en formato YYYY-MM-DD
     * @param c Objeto tipo <i>Calendar</i> con informacion de la fecha
     * @return <i>String</i> con la fecha formateada
     */
    public String devolverFecha(Calendar c){
        String cad = "";
        cad = c.get(Calendar.YEAR)+"-"+((c.get(Calendar.MONTH) + 1)<10? "0"+(c.get(Calendar.MONTH) + 1):(c.get(Calendar.MONTH) + 1))+"-"+c.get(Calendar.DAY_OF_MONTH);
        return cad;
    }

    /**
     * Procesa un archivo xls con la informacion de los ciclos de un a&ntilde;o
     * @param filename Ruta del archivo a procesar
     * @return Cadena con codigo sql a ejecutar para insertar los registros en la base de datos
     */
    protected String leerXls(String filename){
        String cadena = "";
        try {
            FileInputStream inputfile=new FileInputStream(filename);
            HSSFWorkbook xls= new HSSFWorkbook(inputfile);
            HSSFSheet sheet=xls.getSheetAt(0);
            Calendar c = Calendar.getInstance();
            String cad = "";
            CiclosFacturacionService cfserv = new CiclosFacturacionService();
            String anio = request.getParameter("anio")!= null ? request.getParameter("anio"):"0099";
            for (int i=1; i<sheet.getLastRowNum() + 1;i++){
                try {
                    BeanGeneral bean = new BeanGeneral();
                    HSSFRow row=sheet.getRow(i);
                    HSSFCell cell=row.getCell((short)0);
                    bean.setValor_01(""+(int)cell.getNumericCellValue());
                    cell=row.getCell((short)1);
                    bean.setValor_02(""+(int)cell.getNumericCellValue());
                    cell=row.getCell((short)2);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_03(cad);
                    cell=row.getCell((short)3);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_04(cad);
                    cell=row.getCell((short)4);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_05(cad);
                    cell=row.getCell((short)5);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_06(cad);
                    cell=row.getCell((short)6);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_07(cad);
                    cell=row.getCell((short)7);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_08(cad);
                    cell=row.getCell((short)8);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_09(cad);
                    cell=row.getCell((short)9);
                    c.setTime(cell.getDateCellValue());
                    cad = this.devolverFecha(c);
                    bean.setValor_10(cad);
                    cell=row.getCell((short)10);
                    bean.setValor_11(cell.getStringCellValue());
                    cell=row.getCell((short)11);
                    bean.setValor_12(""+(int)cell.getNumericCellValue());
                    cadena += "\n" + cfserv.insertar(anio, bean);
                }
                catch (Exception e) {
                    this.reg_malos ++;
                    filas_malas = ( reg_malos==1 ? ""+(i+1): filas_malas+","+(i+1));
                    System.out.println("Error: "+e.toString());
                }
            }
            inputfile.close();
            File file = new File(filename);
            boolean borrado = file.delete();
            if(borrado==true) System.out.println("se ha eliminado el archivo "+filename);
            else System.out.println("no se pudo eliminar el archivo "+filename);
        }
        catch (Exception e) {
            cadena = "nooooo";
            System.out.println("Error: "+e.toString());
            e.printStackTrace();
        }
        return cadena;
    }
}
