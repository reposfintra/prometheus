/***********************************************
 * Nombre clase: CumplidoInsertAction.java
 * Descripci�n: Accion para ingresar un cumplido
 * Autor: Rodrigo Salazar
 * Fecha: 21 de diciembre de 2004, 11:46 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Rodrigo
 */
public class CumplidoInsertAction extends Action{
    
    /** Creates a new instance of CumplidoInsertAction */
    public CumplidoInsertAction() {
    }
    
    public void run() throws ServletException, InformationException{
        
        String next="/jsp/cumplidos/cumplido/Cumplidos.jsp?sw=1&discre="+request.getParameter("discre");
        String no_pla = request.getParameter("c_numpla").toUpperCase();
        String tipo = request.getParameter("tdoc");
        String observacion = request.getParameter("observacion");
        HttpSession session = request.getSession();
        String num = "";
        String remesa="";
        String mensaje = "";
        String cantdesp = request.getParameter("c_cantidad_desp");
        String canti = request.getParameter("c_cantidad");
        if(cantdesp.equals("") || cantdesp.equals("0")){
            cantdesp = "0";
        }
        if(canti.equals("") || canti.equals("0")){
            canti = "0";
        }
        double ca =0;
        double caM =0;
        
        Vector consultas = new Vector();
        String unid = request.getParameter("c_unidad_ton");
        double cant = Double.parseDouble(canti);
        double cant_desp = Double.parseDouble(cantdesp);
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        boolean sw = false;
        String standar = "";
        int cantRem = 0;
        int cantRemCum = 0;
        boolean sw1 = true;
        try{
            Planilla p = model.planillaService.getPlanilla();
            List lst = model.planillaService.buscarRemesas(p.getNumpla());
            boolean swRem=true;
            cantRem = lst.size();
            
            for(int i =0;i<lst.size();i++){
                
                Remesa rem = (Remesa)lst.get(i);
                String unit = rem.getUnit()!=null?rem.getUnit():"";
                
                if( !p.getUnit_vlr().equals( unit ) ){
                    swRem = false;
                }
                if(rem.getReg_status().equals("C")){
                    cantRemCum++;
                }
                
            }
            Cumplido cumplido = new Cumplido();
            cumplido.setTipo_doc(tipo);
            
            if (tipo.equals("001")){
                
                
                ca= p.getPesoreal();
                
                
                cumplido.setCod_doc(request.getParameter("c_numpla"));
                cumplido.setCantidad(Float.parseFloat(canti));
                cumplido.setComent(request.getParameter("c_coment"));
                cumplido.setUnidad(request.getParameter("c_unidad"));
                //Costos Reembolsables
                Vector costos = model.sjextrafleteService.getC_reembolsables();
                for(int i=0; i<costos.size();i++){
                    SJExtraflete sjE = (SJExtraflete)costos.elementAt(i);
                    if(sjE.isSelec()){
                        sjE.setNumpla(request.getParameter("c_numpla"));
                        sjE.setNumrem("");
                        sjE.setFecdsp(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
                        model.sjextrafleteService.setSj(sjE);
                        consultas.add( model.sjextrafleteService.insertarCostoReembolsable() );
                    }
                }
                
                
                if(cant!=cant_desp){
                    mensaje+="La cantidad de la planilla despachada tiene una diferencia con la cantidad entregada de :"+Util.customFormat(cant-cant_desp)+".";
                }
                mensaje+=" La Planilla fu� Cumplida.";
                standar = p.getSj();
                
                if(swRem){
                    ca = model.cumplidoService.maxCantidadRemesa( no_pla, distrito);
                }
                caM = model.cumplidoService.maxCantidadCumplida( no_pla, distrito);
                
                if(cantRemCum==cantRem ){
                    if(unid.indexOf("V")<0 && unid.indexOf("W")<0 ){
                        if(cumplido.getCantidad()!=caM && swRem){
                            mensaje ="La planilla debe ser cumplida por un valor igual a la suma total de las remesas cumplidas";
                            sw1=false;
                        }
                    }
                }
            }
            else{
                num = request.getParameter("rem");
                double cant_rem = Double.parseDouble(request.getParameter("c_cantidad_rem"+num));
                String carrem = request.getParameter("c_cargrem"+num);
                if(carrem.equals("") || carrem.equals(".")){
                    carrem = "0";
                }
                double cant_rec_rem = Double.parseDouble(carrem);
                if(cant_rem!=cant_rec_rem){
                    mensaje+="La cantidad de la remesa tiene una diferencia con la cantidad recibida de :"+Util.customFormat(cant_rem-cant_rec_rem)+".";
                }
                num = request.getParameter("rem");
                cumplido.setCod_doc(request.getParameter("c_numrem"+num));
                cumplido.setCantidad(Float.parseFloat(!request.getParameter("c_cargrem"+num).equals("") || !request.getParameter("c_cargrem"+num).equals(".")?request.getParameter("c_cargrem"+num):"0"));
                cumplido.setComent(request.getParameter("c_coment"+num));
                cumplido.setUnidad(request.getParameter("c_unidad"+num));
                mensaje+=" La Remesa fu� Cumplida.";
                model.remesaService.buscaRemesa(request.getParameter("c_numrem"+num));
                standar = model.remesaService.getRemesa().getStdJobNo();
                
                ca = model.remesaService.getRemesa().getPesoReal();
                double sumRem = 0;
                
                if(swRem){
                    sumRem = model.cumplidoService.maxCantidadPlanilla(  distrito, no_pla, request.getParameter("c_numrem"+num) );
                    
                }
                
                
                double maximo_valor =0;
                boolean sw2 = false;
                
                model.tablaGenService.obtenerRegistro("MAXCANSJ",standar, "");
                if(model.tablaGenService.getTblgen()!=null){
                    try{
                        maximo_valor =Double.parseDouble(model.tablaGenService.getTblgen().getReferencia());
                        sw2=true;
                    }catch(NumberFormatException ne){
                        sw1=false;
                    }
                }
                if(cantRem - cantRemCum <= 1 && swRem){
                    float cantPla = p.getSaldo()!=0? p.getSaldo(): p.getPesoreal() ;
                    if(!sw2){
                        System.out.println("Inicial Total remesas "+sumRem);
                        sumRem = sumRem + cumplido.getCantidad();
                        System.out.println("Total remesas "+sumRem);
                        System.out.println("Total planilla "+cantPla);
                        if(sumRem <  cantPla){
                            
                            sw=true;
                            mensaje = "La remesa a cumplir debe ser cumplida por un valor diferente, la sumatoria de las cantidades de las remesas son menores a las cantidades de la planilla.";
                            
                        }
                    }
                    
                }
            }
            
            if(!sw){
                //MAXIMOS VALORES EN TABLAGEN
                double maximo_valor =0;
                
                model.tablaGenService.obtenerRegistro("MAXCANSJ",standar, "");
                if(model.tablaGenService.getTblgen()!=null){
                    try{
                        maximo_valor =Double.parseDouble(model.tablaGenService.getTblgen().getReferencia());
                        
                    }catch(NumberFormatException ne){
                        
                    }
                }
                
                
                double maxton=maximo_valor==0?50:maximo_valor;
                double maxmt3=maximo_valor==0?100:maximo_valor;
                double maxkl=maximo_valor==0?55000:maximo_valor;
                double maxgl=maximo_valor==0?500000:maximo_valor;
                double maxvi=maximo_valor==0?1:maximo_valor;
                String uw=cumplido.getUnidad();
                double cfacturar =cumplido.getCantidad();
                
                
                if(uw.indexOf("T")==0){//toneladas
                    if(cfacturar>maxton){
                        sw1 = false;
                        mensaje="No se puede ingresar por que la maxima cantidad de ton es "+maxton;
                    }
                }
                if(uw.indexOf("M")==0){//MT3
                    if(cfacturar>maxmt3){
                        sw1 = false;
                        mensaje="No se puede ingresar por que la maxima cantidad de mt3 es "+maxmt3;
                    }
                }
                if(uw.indexOf("L")==0 || uw.indexOf("K")==0){//KILO
                    if(cfacturar>maxkl){
                        sw1 = false;
                        mensaje="No se puede ingresar por que la maxima cantidad de kilos es "+maxkl;
                    }
                }
                if(uw.indexOf("G")==0){//GALONES
                    if(cfacturar>maxgl){
                        sw1 = false;
                        mensaje="No se puede ingresar por que la maxima cantidad de galones es "+maxgl;
                    }
                }
                if(uw.indexOf("V")==0 || uw.indexOf("W")==0){//VIAJES
                    if(cfacturar>maxvi){
                        sw1 = false;
                        mensaje="No se puede ingresar por que la maxima cantidad de viajes es "+maxvi;
                    }
                }
                
                if( sw1){
                    cumplido.setCrea_user(usuario.getLogin());
                    cumplido.setMod_user(usuario.getLogin());
                    cumplido.setBase(usuario.getBase());
                    cumplido.setDistrict((String)session.getAttribute("Distrito"));
                    cumplido.setAgencia(usuario.getId_agencia());
                    if(request.getParameter("actualizar")==null){
                        consultas.add( model.cumplidoService.insertCumplido(cumplido) );
                    }
                    else{
                        consultas.add( model.cumplidoService.updateCumplidos(cumplido) );
                    }
                    model.despachoService.insertar( consultas );
                }
                
                
            }
            String PLA = request.getParameter("c_numpla");
            model.planillaService.buscaPlanilla(PLA);
            request.setAttribute("planilla", model.planillaService.getPlanilla());
            request.setAttribute("remesas", model.planillaService.buscarRemesas(PLA) );
            request.setAttribute("mensaje", mensaje);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
