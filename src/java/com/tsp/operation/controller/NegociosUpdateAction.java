/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
**/
package com.tsp.operation.controller;

import com.google.gson.JsonObject;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;

import com.tsp.operation.model.TransaccionService;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NegociosUpdateAction  extends Action{

   //protected HttpServletRequest request;
    public NegociosUpdateAction () {
    }
    Usuario usuario = null;
    public void run() throws ServletException, InformationException
    {
        int op=Integer.parseInt(request.getParameter("op"));
        String obs=(String)request.getParameter("textarea");
        String codn=(String)request.getParameter("codneg");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String next="";
        String m="";
        boolean redirect = true;
        String strRespuesta = "";
        
        // Se valida si el negocio ha sido devuelto de transferencia para no generar docs
        String conceptoTraza="";
        try {
            String negocio = request.getParameter("negocio");
            conceptoTraza = model.Negociossvc.obtenerUltimoConceptoTraza(negocio);
            if(conceptoTraza.equals("DEVOLVER")){   
                op = 21;
        }
        } catch (SQLException ex) {
            Logger.getLogger(NegociosUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        switch(op)
        {
            case 1:
                next="/jsp/fenalco/negocios/resultado.jsp";
                //System.out.println("Entro en la opcion de negocios"+obs+"*****"+codn);
                try{
                    model.Negociossvc.denny(obs,codn,"R");
                    next=next+"?res=OK";
                }
                catch (Exception e){
                e.printStackTrace();
                next=next+"?res=NO";
                throw new ServletException(e.getMessage());
                }
            break;
           case 2:
                redirect = false;
                String codneg = request.getParameter("negocio");
                String form = request.getParameter("form");
                String act = request.getParameter("act") != null ? request.getParameter("act") : "";
                String coment = request.getParameter("coment") != null ? request.getParameter("coment") : "";
                String causa = request.getParameter("causa") != null ? request.getParameter("causa") : "";
                String concep = request.getParameter("concep") != null ? request.getParameter("concep") : "";
                String tipoconv=request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "";
                Negocios negocio = new Negocios();
                Convenio convenio =new Convenio();
                GestionSolicitudAvalService solcService = new GestionSolicitudAvalService(usuario.getBd());                 
                SolicitudAval solicitud=new SolicitudAval();
                    
                try {
                    negocio = model.Negociossvc.buscarNegocio(codneg);  
                    convenio = model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), String.valueOf(negocio.getId_convenio()));
                    solicitud = solcService.buscarSolicitud(Integer.parseInt(solcService.buscarNumSolicitud(negocio.getCod_negocio())));
                    
                    AvalFianzaBeans avalFianza= model.gsaserv.getConfiguracionFianza(Integer.parseInt(convenio.getId_convenio()), negocio.getNodocs(),solicitud.getProducto_fondo(),solicitud.getCobertura_fondo(),solicitud.getNit_fondo());
                    negocio.setPorc_dto_aval(avalFianza.getPorc_dto_fianza());
                    negocio.setPorc_fin_aval(avalFianza.getPorc_fin_fianza());
                    
                } catch (Exception ex) {
                    Logger.getLogger(NegociosUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                }
                String tn = "";
                if (negocio.getTneg().equals("01")) {
                    tn = "CH";
                } else {
                    if (negocio.getTneg().equals("02")) {
                        tn = "LT";
                    } else {
                        tn = "PG";
                    }
                }

                String fp[] = new String[negocio.getNodocs() + 1];
                String fv[] = new String[negocio.getNodocs() + 1];
                double[][] inter = (double[][]) session.getAttribute("inter");
                String[] cod_cheques = new String[negocio.getNodocs() + 1];
                String[] item = new String[negocio.getNodocs() + 1];
                String cuenta_cheque = (String) session.getAttribute("cuenta_cheque");
                if (!tipoconv.equals("Microcredito")) {
                    fp = (String[]) session.getAttribute("fpdoc");
                    fv = (String[]) session.getAttribute("fvdoc");
                    cod_cheques = (String[]) session.getAttribute("numche");
                    item = cod_cheques.clone();
                    for (int iii = 0; iii < negocio.getNodocs(); iii++) {
                        cod_cheques[iii] = item[iii + 1];
                    }
                }
                
                String cod_banco = tn.equals("CH") ? negocio.getCodigoBanco() : "170";

                //System.out.println("Fecha neg "+fechan);
                BeanGeneral bg = new BeanGeneral();
                bg.setValor_01(negocio.getCod_cli()); //codcli - nit del cliente
                bg.setValor_02(negocio.getNodocs() + ""); // numero de cuotas
                bg.setValor_03((negocio.getTotpagado() / negocio.getNodocs()) + ""); //Valor de las cuotas
                bg.setValor_04(usuario.getLogin()); //usuario en sesion
                bg.setV1(fp); //Vector con las fechas de cada cuota
                bg.setV2(fv); //Vector con la fecha del negiocio(todos las posiciones del vector tienen el mismo valor)
                bg.setValor_05(tn); //tipo de negocio: cheque, letra, etc
                bg.setValor_06(codneg); //codigo del negocio
                bg.setValor_07(usuario.getBase()); //base
                bg.setValor_08("RD-" + negocio.getCod_cli());
                bg.setValor_09(negocio.getNit_tercero()); //nit tercero (nit del afiliado)
                bg.setValor_10(negocio.getTotpagado() + ""); //total pagado(valor del campo tot_pagado en la tabla negocios)
                bg.setValor_11(negocio.getVr_desem() + ""); //valor del desembolso
                bg.setValor_12(negocio.getFecha_neg()); //fecha del negocio
                bg.setValor_13(negocio.getNumaval()); //numero de aval
                bg.setV3(inter); //Matriz de intereses
                bg.setV4(cod_cheques); //consecutivos de los titulo valor de cada cuota (se captura de la interfaz)
                //  bg.setValor_14(ajuste);
                bg.setValor_15(negocio.getvalor_aval()); //valor del aval
                bg.setValor_16(negocio.getTneg()); //Codigo del titulo valor
                bg.setValor_17(negocio.getId_convenio() + ""); //codigo del convenio
                bg.setValor_18(usuario.getDstrct()); //distrito del usuario en sesion
                bg.setValor_27(negocio.getValor_capacitacion()+"");//valor capacitacion
                bg.setValor_28(negocio.getValor_seguro()+"");//valor seguro
                bg.setValor_29(negocio.getVr_negocio()+"");// valor negocio

                try {
                    ArrayList<Aval> avales = model.Negociossvc.searchAvales(Util.lpad(negocio.getNumaval(), '0', 8));
                    boolean sw = true;
                    double vd = (negocio.getTotpagado() / negocio.getNodocs());
                    int vi = (int) vd;
                    String valor = vi + "";
                    String errores = "";
                    String[] interaval = model.Negociossvc.IntermediarioAval(codneg).split(";");
                    boolean boolinteraval = Boolean.parseBoolean(interaval[0]);
                    if (!interaval[2].equals("S")) {
                        if (avales.isEmpty()) {
                            errores += "El aval no fue encontrado";
                            sw = false;
                        }
                        for (int i = 0; i < avales.size(); i++) {
                            if (boolinteraval) {
                                if (!Util.lpad(interaval[1], '0', 11).equals(avales.get(i).getNitAfiliado())) {
                                    errores += "El nit del afiliado no coicide\n";
                                }
                            } else {
                                if (!Util.lpad(negocio.getNit_tercero(), '0', 11).equals(avales.get(i).getNitAfiliado())) {
                                    errores += "El nit del afiliado no coicide\n";
                                }
                            }
                            if (!Util.lpad(negocio.getCod_cli(), '0', 11).equals(avales.get(i).getNumDoc())) {
                                errores += "El documento del cliente no coincide\n";
                            }
                            if (!(cod_banco.equals("170") ? "99" : cod_banco).equals(avales.get(i).getCodigoBanco())) {
                                errores += "El Codigo del banco no coicide \n";
                            }
                            if (!Util.lpad(cuenta_cheque, '0', 13).equals(avales.get(i).getNumeroCuenta())) {
                                errores += "El numero de cuenta no coincide\n";
                            }
                            if (!Util.lpad(item[i + 1], '0', 10).equals(avales.get(i).getTituloValor())) {
                                errores += "El numero del titulo no coicide en la cuota " + i + 1 + "\n";
                            }
                            if (!fp[i + 1].substring(0, 10).replaceAll("-", "").equals(avales.get(i).getFechaConsignacion())) {
                                errores += "La fecha del titulo no coicide en la cuota " + i + 1 + "\n";
                            }
                            if (!Util.lpad(valor, '0', 9).equals(avales.get(i).getValorTitulo())) {
                                errores += "El valor del titulo no coicide en la cuota " + i + 1 + "\n";
                            }
                            if (!(vi <= Integer.parseInt(avales.get(i).getCoberturaConsulta()))) {
                                errores += "La cobertura es menor al valor de la cuota\n";
                            }
                            if (!errores.trim().equals("")) {
                                sw = false;
                                break;
                            }
                        }
                    }
                    if (sw) {
                    
                        NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService();

                        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                        negtraza.setActividad(act);
                        negtraza.setNumeroSolicitud(form);
                        negtraza.setUsuario(usuario.getLogin());
                        negtraza.setCausal(causa);
                        negtraza.setComentarios(coment);
                        negtraza.setDstrct(usuario.getDstrct());
                        negtraza.setConcepto(concep);
                        negtraza.setCodNeg(codneg);

                        //Si esta es la opcion para las letras, entonces no aplica numero de chequera
                        //se manda vacio
                        ArrayList<String> listSql = new ArrayList<String>();
                        int sw2 = 0;
                        TransaccionService scv = new TransaccionService(usuario.getBd());
                        listSql.add(model.Negociossvc.admit(usuario.getLogin(), obs, codneg, negocio.getNumaval(), cod_banco, cuenta_cheque, "A"));//se modifico esta linea
                        //inserta registros en factura, factura_detalle, cxp_doc, cxp_items_doc y ing_fenalco
                        if(convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre()){
                            // Se valida si es negocio de seguro y no se transfiere
                            if((!model.Negociossvc.esTransferido(negocio.getNit_tercero())) && (convenio.getId_convenio().equals("35"))){
                                listSql.addAll(model.Negociossvc.generarDocumentosNegocioSeguro(bg, usuario,negocio)); //Genera los documentos de un negocio de seguro que no se transfiere
                                sw2 = 1;
                            }else{
                                listSql.addAll(model.Negociossvc.generarDocumentosFenalco(bg, usuario,negocio)); //Genera los documentos de los negocios de Fenalco
                            }
                           //sql = sql + model.Negociossvc.generarDocumentos(bg, usuario,negocio); // Metodo anterior
                        }
                        else
                        {                            
                            if (convenio.getTipo().equals("Microcredito")){
                                
                                
                                    negtraza.setActividad("");
                                    if (!negocio.getFecha_liquidacion().equals(negocio.getFechatran().substring(0, 10))) {
                                        Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
                                        if (impuesto == null) {
                                            throw new Exception("Error: el convenio " + convenio.getId_convenio() + " no tiene impuesto vigente");
                                        }
                                        String fechaInicial = Util.fechaMasDias(negocio.getFecha_liquidacion(), Integer.parseInt(negocio.getFpago()));
                                        negocio.setCodigoBanco(cod_banco);
                                        negocio.setFecha_liquidacion(Util.getFechaActual_String(4));
                                        negocio.setFechatran(Util.getFechaActual_String(4));                                        
                                        negocio.setFpago(Util.fechasDiferenciaEnDias(Util.convertiraDate(negocio.getFecha_liquidacion()), Util.convertiraDate(fechaInicial)) + "");
                                        //buscar numero informacion del aval...                           
//                                      model.Negociossvc.calcularLiquidacionMicrocredito(negocio, usuario, impuesto, solicitud.getRenovacion(),solicitud.getNumeroSolicitud());
                                      bg.setValor_30(solicitud.getNumeroSolicitud());
                                      model.Negociossvc.liquidarNegocioMicrocredito(negocio,solicitud,convenio);
                                      model.Negociossvc.updateNegocioMicrocredito(negocio, solcService.buscarNumSolicitud(negocio.getCod_negocio()), negtraza);
                                    }
                       
                            }
                            negtraza.setActividad(act);
                            bg.setValor_31(negocio.getPolitica());
                            bg.setValor_32(negocio.getValor_renovacion()+"");
                            bg.setValor_33(solicitud.getNit_fondo());
                            bg.setValor_34(solicitud.getProducto_fondo()+"");
                            bg.setValor_35(solicitud.getCobertura_fondo()+"");
                          listSql.addAll(model.Negociossvc.generarDocumentosNegocio(bg, usuario)); //Genera los documentos para microcredito
                        }
                        //inserta en con.subledger
                        listSql.addAll(model.Negociossvc.subled(bg));
                        
                        scv.crearStatement();
                        for (String sql : listSql) {
                            scv.getSt().addBatch(sql);
                        }
                        scv.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                        scv.getSt().addBatch(trazaService.updateActividad(codneg, act));
                        if(sw2 == 1){ //Actualiza a estado transferido si es negocio de seguro que no se transfiere
                            scv.getSt().addBatch(trazaService.updateEstadoForm(form, "T"));
                            scv.getSt().addBatch(trazaService.updateEstadoNeg(codneg, "T"));
                        }else{
                            scv.getSt().addBatch(trazaService.updateEstadoForm(form, "A"));
                        }                      
                        
                        scv.execute();
                        
                       if(convenio.getTipo().equals("Microcredito") && model.Negociossvc.validarcompracartera(form)){  
                        model.Negociossvc.generarDocumentosCompraCartera(bg, usuario, negocio.getId_convenio());
                       }
//                        if(convenio.getTipo().equals("Microcredito")){
//                         model.Negociossvc.generar_Nc_Y_Cxps_Polizas(negocio,usuario);
//                        }
                       
                        if(negocio.getId_convenio()==17 || negocio.getId_convenio()==31){
                            //Envio de email al beneficiario del credito para negocios de educativo
                            JsonObject jobj = model.Negociossvc.getInfoCuentaEnvioCorreoAprob();
                            String mailsToSend = model.Negociossvc.obtenerMailEnvioCorreoAprobCredito(form);//Aca se establece el destinatario del correo                           
                            String asunto = "Aprobaci�n cr�dito educativo en aplicativo de Credi100";
                            Thread hiloEmail = new Thread(new EmailSenderService(jobj, "APROB_CRED", "", mailsToSend, asunto, usuario), "hilo");
                            hiloEmail.start();
                        }
                        strRespuesta = "/controller?estado=Negocios&accion=Ver&op=5&vista=9";
                    } else {
                        strRespuesta = "<span class='fila'>Error :" + errores + "</span>";
                    }
                } catch (Exception e) {
                    NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService();
                    TransaccionService scv = new TransaccionService(usuario.getBd());
                    try {
                        //Se realiza update para devolverle el negocio a formalizacion en caso de error al generar documentos fuera de la transaccion
                        scv.crearStatement();
                        scv.getSt().addBatch(trazaService.updateActividad(codneg, "DEC"));
                        scv.getSt().addBatch(trazaService.updateEstadoNeg(codneg, "V"));
                        scv.getSt().addBatch(trazaService.updateEstadoForm(form, "V"));
                        scv.execute();
                    } catch (SQLException ex) {
                        Logger.getLogger(NegociosUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    e.printStackTrace();
                    strRespuesta = "<span class='fila'>Error :" + e.getMessage() + "</span>";
                    throw new ServletException(e.getMessage());
                }
                cod_cheques = null;
                bg = null;
                fp = null;
                fv = null;
                inter = null;
            break;
            case 3:
                String cli=(String)request.getParameter("clientes");
                String prov=(String)request.getParameter("proveedor");
                BeanGeneral bgen=new BeanGeneral();
                bgen.setValor_01(prov);
                bgen.setValor_02(cli);
                bgen.setValor_03(usuario.getLogin());
                bgen.setValor_04(usuario.getBase());
                try
                {
                    if(model.Negociossvc.existe(bgen,"A"))//comprueba que exista el registro
                    {
                        try
                        {
                            model.Negociossvc.deleteU(bgen);
                            next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=Operacion Completada con Exito!!";
                        }
                        catch (Exception e)
                        {
                            next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=No se pudo completar la operacion";
                            throw new ServletException(e.getMessage());
                        }
                    }
                    else
                    {
                        if(!model.Negociossvc.existe(bgen,""))//comprueba que exista el registro
                        {
                            try
                            {
                                model.Negociossvc.relsave(bgen);
                                next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=Operacion Completada con Exito!o!";
                            }
                            catch (Exception e)
                            {
                                next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=No se pudo completar la operacion, datos repetidos";
                                throw new Exception(e.getMessage());
                            }
                        }
                        else
                        {
                            next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=No se pudo completar la operacion, datos repetidos";
                        }
                    }
                }
                catch(Exception e){
                     next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=No se pudo completar la operacion";
                    throw new ServletException(e.getMessage());
                }
                bgen=null;
                break;
            case 4:
                String clid=(String)request.getParameter("clientes");
                String provd=(String)request.getParameter("proveedor");
                BeanGeneral bgn=new BeanGeneral();
                bgn.setValor_01(provd);
                bgn.setValor_02(clid);
                bgn.setValor_03(usuario.getLogin());
                try
                {
                    if(model.Negociossvc.existe(bgn,""))//comprueba que exista el registro
                    {
                        try
                        {
                            model.Negociossvc.delete(bgn);
                            next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=Operacion Completada con Exito";
                        }
                        catch (Exception e)
                        {
                            next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=No se pudo completar la operacion";
                            throw new ServletException(e.getMessage());
                        }
                    }
                    else
                    {
                        next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=Este registro no existe por tanto no se puede anular";
                    }
                }
                catch(Exception e){
                     next="/jsp/fenalco/avales/inicio_asignar.jsp?msg=No se pudo completar la operacion";
                    throw new ServletException(e.getMessage());
                }
                break;

                case 6:

                try {
                    String[] ingresos;
                    ingresos = request.getParameterValues("ingresos");
                    next = "/jsp/fenalco/contabilizacion/devoluciones.jsp?msg=El proceso de devolucion a iniciado";
                    HDevoluciones hilo = new HDevoluciones();
                    hilo.start(model, ingresos, usuario);

                }  catch (Exception e) {
                    e.printStackTrace();
                    next = "/jsp/fenalco/contabilizacion/devoluciones.jsp?msg=Error al iniciar e proceso de devolucion";
                    throw new ServletException(e.getMessage());
                }
                break;


                	case 7: //ivargas 11-07-11
 		next = "/jsp/fenalco/contabilizacion/Mensaje.jsp";
                String[] facturas;
                String mensaje = "";
                facturas = request.getParameterValues("facturasC");
                String cliente = (String) request.getParameter("cliente");
                String fi = (String) request.getParameter("fecini");
                String ff = (String) request.getParameter("fecfin");
                String tercero= cliente ;

                        if (cliente.equals("1")) { //fenalco Atlantico
                            cliente = "8901009858";
                        } else if (cliente.equals("3")) { //fenalco Bolivar
                            cliente = "8904800244";
                        } else if (cliente.equals("2")) { //fenalco bogota
                            cliente = "8600091619";
                        } else {
                            cliente = "";
                        }
                   
                    for (int i = 0; i < facturas.length; i++) {
                        String[] fact = facturas[i].split(";");
                        try {
                            m = "";
                            m = model.Negociossvc.newfact2(fact[0],fact[1], fi, ff, cliente, usuario); //codigo_factura, fecha_inicio, fecha_fi, cliente

                            if (m.equals("")) {

                                BeanGeneral beanGeneral = null;//Defino un objeto BeanGeneral

                                beanGeneral = model.Negociossvc.getReporteFenalcoService(fact[0]);
                                //Obtengo los datos del cheque a devolver

                                model.PdfImprimirSvc.setBeanGeneral(beanGeneral);


                                model.PdfImprimirSvc.setVariablesReporteFenalco(beanGeneral);//Instancio las variables con los datos a imprimir
                                model.PdfImprimirSvc.reporteChequesDevueltosFenalco(model, fact[0], usuario.getLogin());
                                //funcion que genera todos lo documentos faltantes por cada factura 
                                String me = " Genero el siguiente resultado  : ";
                                if (!tercero.equals("2")) {//si es diferente de fenalco bogota entra
                                    System.out.println("entra a : runEndosoTercero");
                                    me += model.Negociossvc.runEndosoTercero(fact[0], fact[1]);
                                } else {
                                    me = "Gener� el proceso correctamente!";
                                }
                                mensaje += "La factura " + fact[0] + " " + me + "\n";

                            } else {
                                mensaje+="error: factura "+fact[0]+" "+m+"\n";
                            }

                        } catch (Exception e) {
                            System.out.println(e.toString());//Usado para control
                            mensaje+="error: factura "+fact[0]+" "+e.getMessage()+"\n";
                        }

                    }
                    facturas= null;
                    next = next + "?msg="+ mensaje;

                break;

           case 8:
                    next="/jsp/fenalco/Afiliados/inicio_consecutivos.jsp";
                    String nitpro=(String)request.getParameter("proveedor");
                    BeanGeneral bgen1=new BeanGeneral();
                    bgen1.setValor_01(nitpro);
                    String m2="";
                    // Prefix
                    int l=(nitpro.length())-1;
                    int x=l-6;
                    String prefix=nitpro.substring(x,l);
                    if (prefix.startsWith("0"))
                    {
                        prefix=nitpro.substring(x+1,l);
                    }
                    //
                    bgen1.setValor_02(prefix);
                    bgen1.setValor_03(usuario.getLogin());
                    bgen1.setValor_04(usuario.getBase());
                    bgen1.setValor_05("FINV");
                    try{
                        m2=model.Negociossvc.Conse(bgen1);

                    }
                    catch (Exception e){
                    e.printStackTrace();
                    next=next+"?msg=NO";
                    throw new ServletException(e.getMessage());
                    }
                    if (m2.equals(""))
                    {
                        next=next+"?msg=Consecutivo Almacenado con Exito";
                    }
                    else
                    {
                        next=next+"?msg="+m2;
                    }
                break;
            case 9:

                next="/jsp/fenalco/letras/form_letras.jsp";
                String codnego=(String)request.getParameter("codn");
                next=next+"?var="+codnego;
                break;
           case 10:
                BeanGeneral datos=new BeanGeneral();
                next="/jsp/fenalco/letras/inicio_letras.jsp";
                datos.setValor_01((String)request.getParameter("NomCood"));
                datos.setValor_02((String)request.getParameter("CedCliente"));
                datos.setValor_03((String)request.getParameter("Celular"));
                datos.setValor_04((String)request.getParameter("aprob"));
                datos.setValor_05((String)request.getParameter("c_dir"));
                datos.setValor_06((String)request.getParameter("c_otor"));
                datos.setValor_07((String)request.getParameter("negocio"));
                datos.setValor_08((String)request.getParameter("ccaut"));
                try{
                        model.Negociossvc.letras(datos);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                break;
             case 12:
                String ccnit=(String)request.getParameter("nit");
                String ccnodoc=(String)request.getParameter("nodocs");//...CANTIDAD DE DOCUMENTOS
                String cctn=(String)request.getParameter("tneg");
                String ccaux=(String)request.getParameter("aux");
                String ccvrneg=(String)request.getParameter("vrnego");
                String ccvrdes=(String)request.getParameter("vrdesembolso");
                String ccfechan=(String)request.getParameter("fechaneg");
                String[] cod_cheques1=request.getParameterValues("numche");//Array con los numeros de los cheques
                double fval[]=new double[Integer.parseInt(ccnodoc)+1];
                String fven[]=new String[Integer.parseInt(ccnodoc)+1];
                String fdoc[]=new String[Integer.parseInt(ccnodoc)+1];
                double fint[]=new double[50];
                String finte[]= new String[50];
                fval=(double[])session.getAttribute("fval");
                fven=(String[])session.getAttribute("fven");
                fdoc=(String[])session.getAttribute("fdoc");
                fint=(double[])session.getAttribute("intec");
                finte=(String[])session.getAttribute("finte");

                String numeritoAval=(String)request.getParameter("aval");//Numero aprobacion fenalco
                String banquitoCheque=(String)request.getParameter("bcofid");//numero del banco

                //jemartinez
                //Obtiene el numero de la cuenta de la chequera
                String numeroCuentaCheque = (String)request.getParameter("cuenta_cheque");

                BeanGeneral ccbg=new BeanGeneral();
                ccbg.setValor_01(ccaux);
                ccbg.setValor_02(ccnodoc);
                ccbg.setValor_04(usuario.getLogin());
                ccbg.setValor_05(cctn);
                ccbg.setValor_07(usuario.getBase());
                ccbg.setValor_08("RD-"+ccaux);
                ccbg.setValor_09(ccnit);
                ccbg.setValor_10(ccvrneg);
                ccbg.setValor_11(ccvrdes);
                ccbg.setValor_12(ccfechan);
                ccbg.setValor_13(numeroCuentaCheque);//Almancena el valor de la numero de cuenta
                ccbg.setArr_1(fval);
                ccbg.setArr_2(fint);
                ccbg.setValor_06(codn);
                ccbg.setV1(fven);
                ccbg.setV5(fdoc);
                ccbg.setV2(finte);
                ccbg.setV4(cod_cheques1);//Numeros de cheques

                next="/jsp/fenalco/negocios/resultado.jsp";

                try{
                    TransaccionService  scv = new TransaccionService(usuario.getBd());
                    ArrayList<String> listQuery = new ArrayList<String>();
                    listQuery.addAll(model.Negociossvc.CCfactsave(ccbg));

                    //model.Negociossvc.admit(usuario.getLogin(),obs,codn,"0","0");
                    listQuery.add(model.Negociossvc.admit(usuario.getLogin(),obs,codn,numeritoAval,banquitoCheque,numeroCuentaCheque,"A"));


                    //modificar metodo admit y consecuentemente *Service y *DAO
                    //para insertar el numero de la cuenta...mas bn actualizarlo

                    listQuery.addAll(model.Negociossvc.subled(ccbg));
                    scv.crearStatement();
                    for (String sql : listQuery) {
                        scv.getSt().addBatch(sql);
                    }
                    scv.execute();

                    next=next+"?res=OK";
                }
                catch (Exception e){
                e.printStackTrace();
                next=next+"?res=NO";
                throw new ServletException(e.getMessage());
                }
                bg=null;
                ccbg=null;
                fval=null;
                fven=null;
                fdoc=null;
                fint=null;
                session.removeAttribute("fval");
		session.removeAttribute("fven");
                session.removeAttribute("fdoc");
		session.removeAttribute("mat");
                session.removeAttribute("finte");
            break;
            ////creado por Miguel Altamiranda////////////
             case 19:
                String nit5=(String)request.getParameter("nit");
                String nodoc5=(String)request.getParameter("nodocs");
                String vr5=(String)request.getParameter("vrdocs");
                String tn5=(String)request.getParameter("tneg");
                String fp5[]=new String[Integer.parseInt(nodoc5)+1];
                String fv5[]=new String[Integer.parseInt(nodoc5)+1];
                fp5=(String[])session.getAttribute("fpdoc");
                fv5=(String[])session.getAttribute("fvdoc");
                String aux5=(String)request.getParameter("aux");
                String vrneg5=(String)request.getParameter("vrnego");
                String vrdes5=(String)request.getParameter("vrdesembolso");
                String fechan5=(String)request.getParameter("fechaneg");
                double[][] inter5= (double[][])session.getAttribute("inter");
                String num_aval5=(String)request.getParameter("aval");
                String cod_banco5=(String)request.getParameter("bcofid");
                String[] cod_cheques5=request.getParameterValues("numche");

                //System.out.println("Fecha neg "+fechan);
                BeanGeneral bg5=new BeanGeneral();
                bg5.setValor_01(nit5);
                bg5.setValor_02(nodoc5);
                bg5.setValor_03(vr5);
                bg5.setValor_04(usuario.getLogin());
                bg5.setV1(fp5);
                bg5.setV2(fv5);
                bg5.setValor_05(tn5);
                bg5.setValor_06(codn);
                bg5.setValor_07(usuario.getBase());
                bg5.setValor_08("RD-"+nit5);
                bg5.setValor_09(aux5);
                bg5.setValor_10(vrneg5);
                bg5.setValor_11(vrdes5);
                bg5.setValor_12(fechan5);
                bg5.setV3(inter5);
                bg5.setV4(cod_cheques5);
                next="/jsp/fenalco/negocios/resultado.jsp";

                try{
                    //Si esta es la opcion para las letras, entonces no aplica numero de chequera
                    //se manda vacio
                    model.Negociossvc.admit(usuario.getLogin(),obs,codn,"A");
                    model.Negociossvc.factsave(bg5);
                    model.Negociossvc.subled(bg5);
                    next=next+"?res=OK";
                }
                catch (Exception e){
                    e.printStackTrace();
                    next=next+"?res=NO";
                    throw new ServletException(e.getMessage());
                }

                bg=null;
                fp=null;
                fv=null;
                inter=null;
            break;
//////////////////case 20:

            case 20://Agregado por Miguel Altamiranda
                String nit20=(String)request.getParameter("nit");
                String nodoc20=(String)request.getParameter("nodocs");
                String vr20=(String)request.getParameter("vrdocs");
                String tn20=(String)request.getParameter("tneg");
                String fp20[]=new String[Integer.parseInt(nodoc20)+1];
                String fv20[]=new String[Integer.parseInt(nodoc20)+1];
                fp20=(String[])session.getAttribute("fpdoc");
                fv20=(String[])session.getAttribute("fvdoc");
                String aux20=(String)request.getParameter("aux");
                String vrneg20=(String)request.getParameter("vrnego");
                String vrdes20=(String)request.getParameter("vrdesembolso");
                String fechan20=(String)request.getParameter("fechaneg");
                double[][] inter20= (double[][])session.getAttribute("inter");
                String num_aval20=(String)request.getParameter("aval");
                String cod_banco20=(String)request.getParameter("bcofid");
                String[] cod_cheques20=request.getParameterValues("numche");
                String plaza20=request.getParameter("plaza");
                Calendar[] fechas_cheques20=(Calendar[])session.getAttribute("fechas_cheques");
                String[] valores_cheques20=(String[])session.getAttribute("valores_cheques");
                String no_cta20=(String)request.getParameter("no_cta");
                String c_fintra20=request.getParameter("c_fintra");
                ArrayList afiliado20=(ArrayList)session.getAttribute("afiliado");
                String c_custodia20=(String)request.getParameter("c_custodia");
                String nom_cli20=(String)session.getAttribute("nom_cli");
                String cod_cli20=(String)session.getAttribute("cod_cli");
                String nombanc20="";
                String codbanc20="";
                String dirbanco20="";
                String ex_numaval=(String)session.getAttribute("ex_numaval");
                String ex_no_orden=(String)session.getAttribute("ex_no_orden");
                next="/jsp/fenalco/negocios/resultado.jsp";
                int sw=0;
                for(int j=0;j<c_fintra20.length();j++)
                {   if(!c_fintra20.substring(j,j+1).equals(",")&&sw==0)
                    {   codbanc20=codbanc20+c_fintra20.substring(j,j+1);
                    }
                    else
                    {   if(c_fintra20.substring(j,j+1).equals(","))
                        {   sw++;
                        }
                        else
                        {   if(sw==1)
                            {   nombanc20=nombanc20+c_fintra20.substring(j,j+1);
                            }
                            else
                            {   if(sw==2)
                                {   dirbanco20=dirbanco20+c_fintra20.substring(j,j+1);
                                }
                            }
                        }
                    }
                }
                if(nombanc20.equals(""))
                {   nombanc20="FINTRAVALORES";
                }
                if(dirbanco20.equals(""))
                {   dirbanco20="Carrera 53 No 79 - 01 Local 205";
                }
                try{
                    ArrayList afiliad20=(ArrayList)afiliado20.get(0);
                    for(int k=0;k<cod_cheques20.length;k++)
                    {
                        String ch=cod_cheques20[k];
                        java.text.SimpleDateFormat f = new java.text.SimpleDateFormat("ddMMyy");
                        Calendar hoy=Calendar.getInstance();
                        if(hoy.get(Calendar.HOUR_OF_DAY)>=16)
                        {   hoy.add(Calendar.DATE,1);
                        }
                        String dts=f.format(hoy.getTime());
                        String no_orden="";
                        if(!no_cta20.equals("1"))
                        {   no_orden="990"+dts+(String)afiliad20.get(3);
                        }
                        else
                        {   no_orden="991"+dts+(String)afiliad20.get(3);
                        }
                        String valor=valores_cheques20[k];
                        java.text.SimpleDateFormat f2= new java.text.SimpleDateFormat("yyyy-MM-dd");
                        model.Negociossvc.ingresar_ordenes2(ex_no_orden,ex_numaval,no_orden,(String)afiliad20.get(0), (String)afiliad20.get(2), nom_cli20, cod_cli20, ch, no_cta20, cod_banco20, valor, plaza20, f2.format(fechas_cheques20[k].getTime()), codbanc20, nombanc20, num_aval20,c_custodia20,dirbanco20,f2.format(hoy.getTime()));
                    }
                    model.Negociossvc.admit(usuario.getLogin(),obs,codn,num_aval20,cod_banco20,"","PD");
                    next=next+"?res=OK";
                }
                catch (Exception e){
                    e.printStackTrace();
                    next=next+"?res=NO";
                    throw new ServletException(e.getMessage());
                }
                fp20=null;
                fv20=null;
                inter20=null;
            break;
            case 13://Agregado por Miguel Altamiranda
                String nit2=(String)request.getParameter("nit");
                String nodoc2=(String)request.getParameter("nodocs");
                String vr2=(String)request.getParameter("vrdocs");
                String tn2=(String)request.getParameter("tneg");
                String fp2[]=new String[Integer.parseInt(nodoc2)+1];
                String fv2[]=new String[Integer.parseInt(nodoc2)+1];
                fp2=(String[])session.getAttribute("fpdoc");
                fv2=(String[])session.getAttribute("fvdoc");
                String aux2=(String)request.getParameter("aux");
                String vrneg2=(String)request.getParameter("vrnego");
                String vrdes2=(String)request.getParameter("vrdesembolso");
                String fechan2=(String)request.getParameter("fechaneg");
                double[][] inter2= (double[][])session.getAttribute("inter");
                String num_aval2=(String)request.getParameter("aval");
                String cod_banco2=(String)request.getParameter("bcofid");
                String[] cod_cheques2=request.getParameterValues("numche");
                String plaza=request.getParameter("plaza");
                Calendar[] fechas_cheques=(Calendar[])session.getAttribute("fechas_cheques");
                String[] valores_cheques=(String[])session.getAttribute("valores_cheques");
                String no_cta=(String)request.getParameter("no_cta");
                String c_fintra=request.getParameter("c_fintra");
                ArrayList afiliado=(ArrayList)session.getAttribute("afiliado");
                String c_custodia=(String)request.getParameter("c_custodia");
                String nom_cli=(String)session.getAttribute("nom_cli");
                String cod_cli=(String)session.getAttribute("cod_cli");

                //System.out.println("Fecha neg "+fechan);
                BeanGeneral bg2=new BeanGeneral();
                bg2.setValor_01(nit2);
                bg2.setValor_02(nodoc2);
                bg2.setValor_03(vr2);
                bg2.setValor_04(usuario.getLogin());
                bg2.setV1(fp2);
                bg2.setV2(fv2);
                bg2.setValor_05(tn2);
                bg2.setValor_06(codn);
                bg2.setValor_07(usuario.getBase());
                bg2.setValor_08("RD-"+nit2);
                bg2.setValor_09(aux2);
                bg2.setValor_10(vrneg2);
                bg2.setValor_11(vrdes2);
                bg2.setValor_12(fechan2);
                bg2.setV3(inter2);
                bg2.setV4(cod_cheques2);
                String nombanc="";
                String codbanc="";
                String dirbanco="";
                next="/jsp/fenalco/negocios/resultado.jsp";
                int sw2=0;
                for(int j=0;j<c_fintra.length();j++)
                {   if(!c_fintra.substring(j,j+1).equals(",")&&sw2==0)
                    {   codbanc=codbanc+c_fintra.substring(j,j+1);
                    }
                    else
                    {   if(c_fintra.substring(j,j+1).equals(","))
                        {   sw2++;
                        }
                        else
                        {   if(sw2==1)
                            {   nombanc=nombanc+c_fintra.substring(j,j+1);
                            }
                            else
                            {   if(sw2==2)
                                {   dirbanco=dirbanco+c_fintra.substring(j,j+1);
                                }
                            }
                        }
                    }
                }
                if(nombanc.equals(""))
                {   nombanc="FINTRAVALORES";
                }
                if(dirbanco.equals(""))
                {   dirbanco="Carrera 53 No 79 - 01 Local 205";
                }
                try{
                    ArrayList afiliad=(ArrayList)afiliado.get(0);
                    for(int k=0;k<cod_cheques2.length;k++)
                    {   String ch=cod_cheques2[k];
                        java.text.SimpleDateFormat f = new java.text.SimpleDateFormat("ddMMyy");
                        Calendar hoy=Calendar.getInstance();
                        if(hoy.get(Calendar.HOUR_OF_DAY)>=16)
                        {   hoy.add(Calendar.DATE,1);
                        }
                        String dts=f.format(hoy.getTime());
                        String no_orden="";
                        if(!no_cta.equals("1"))
                        {   no_orden="990"+dts+(String)afiliad.get(3);
                        }
                        else
                        {   no_orden="991"+dts+(String)afiliad.get(3);
                        }
                        String valor=valores_cheques[k];
                        java.text.SimpleDateFormat f2= new java.text.SimpleDateFormat("yyyy-MM-dd");
                        System.out.println(no_orden+","+afiliad.get(0)+","+afiliad.get(2)+","+nom_cli+","+cod_cli+","+ch+","+no_cta+","+cod_banco2+","+valor+","+plaza+","+c_fintra+","+f2.format(fechas_cheques[k].getTime())+","+num_aval2+","+c_custodia);
                        model.Negociossvc.ingresar_ordenes(no_orden,(String)afiliad.get(0), (String)afiliad.get(2), nom_cli, cod_cli, ch, no_cta, cod_banco2, valor, plaza, f2.format(fechas_cheques[k].getTime()), codbanc, nombanc, num_aval2,c_custodia,dirbanco,codn,f2.format(hoy.getTime()));
                    }
                    model.Negociossvc.admit(usuario.getLogin(),obs,codn,num_aval2,cod_banco2,"","PD");
                    next=next+"?res=OK";
                }
                catch (Exception e){
                    e.printStackTrace();
                    next=next+"?res=NO";
                    throw new ServletException(e.getMessage());
                }

                bg2=null;
                fp2=null;
                fv2=null;
                inter2=null;
            break;
	    case 14://Agregado por Miguel Altamiranda
                String nit3=(String)request.getParameter("nit");
                String nodoc3=(String)request.getParameter("nodocs");
                String vr3=(String)request.getParameter("vrdocs");
                String tn3=(String)request.getParameter("tneg");
                String fp3[]=new String[Integer.parseInt(nodoc3)+1];
                String fv3[]=new String[Integer.parseInt(nodoc3)+1];
                fp3=(String[])session.getAttribute("fpdoc");
                fv3=(String[])session.getAttribute("fvdoc");
                String aux3=(String)request.getParameter("aux");
                String vrneg3=(String)request.getParameter("vrnego");
                String vrdes3=(String)request.getParameter("vrdesembolso");
                String fechan3=(String)request.getParameter("fechaneg");
                double[][] inter3= (double[][])session.getAttribute("inter");
                String num_aval3=(String)request.getParameter("aval");
                String cod_banco3=(String)request.getParameter("bcofid");
                String[] cod_cheques3=request.getParameterValues("numche");

                //System.out.println("Fecha neg "+fechan);
                BeanGeneral bg3=new BeanGeneral();
                bg3.setValor_01(nit3);
                bg3.setValor_02(nodoc3);
                bg3.setValor_03(vr3);
                bg3.setValor_04(usuario.getLogin());
                bg3.setV1(fp3);
                bg3.setV2(fv3);
                bg3.setValor_05(tn3);
                bg3.setValor_06(codn);
                bg3.setValor_07(usuario.getBase());
                bg3.setValor_08("RD-"+nit3);
                bg3.setValor_09(aux3);
                bg3.setValor_10(vrneg3);
                bg3.setValor_11(vrdes3);
                bg3.setValor_12(fechan3);
                bg3.setV3(inter3);
                bg3.setV4(cod_cheques3);
                next="/jsp/fenalco/negocios/resultado.jsp";

                try{
                    model.Negociossvc.admit(codn,"AD");
                    next=next+"?res=OK";
                }
                catch (Exception e){
                    e.printStackTrace();
                    next=next+"?res=NO";
                    throw new ServletException(e.getMessage());
                }

                bg3=null;
                fp3=null;
                fv3=null;
                inter3=null;
            break;
 	    case 15://Agregado por Miguel Altamiranda
                String nit4=(String)request.getParameter("nit");
                String nodoc4=(String)request.getParameter("nodocs");
                String vr4=(String)request.getParameter("vrdocs");
                String tn4=(String)request.getParameter("tneg");
                String fp4[]=new String[Integer.parseInt(nodoc4)+1];
                String fv4[]=new String[Integer.parseInt(nodoc4)+1];
                fp4=(String[])session.getAttribute("fpdoc");
                fv4=(String[])session.getAttribute("fvdoc");
                String aux4=(String)request.getParameter("aux");
                String vrneg4=(String)request.getParameter("vrnego");
                String vrdes4=(String)request.getParameter("vrdesembolso");
                String fechan4=(String)request.getParameter("fechaneg");
                double[][] inter4= (double[][])session.getAttribute("inter");
                String num_aval4=(String)request.getParameter("aval");
                String cod_banco4=(String)request.getParameter("bcofid");
                String[] cod_cheques4=request.getParameterValues("numche");

                //System.out.println("Fecha neg "+fechan);
                BeanGeneral bg4=new BeanGeneral();
                bg4.setValor_01(nit4);
                bg4.setValor_02(nodoc4);
                bg4.setValor_03(vr4);
                bg4.setValor_04(usuario.getLogin());
                bg4.setV1(fp4);
                bg4.setV2(fv4);
                bg4.setValor_05(tn4);
                bg4.setValor_06(codn);
                bg4.setValor_07(usuario.getBase());
                bg4.setValor_08("RD-"+nit4);
                bg4.setValor_09(aux4);
                bg4.setValor_10(vrneg4);
                bg4.setValor_11(vrdes4);
                bg4.setValor_12(fechan4);
                bg4.setV3(inter4);
                bg4.setV4(cod_cheques4);
                next="/jsp/fenalco/negocios/resultado.jsp";

                try{
                    model.Negociossvc.admit(codn,"RD");
                    next=next+"?res=OK";
                }
                catch (Exception e){
                    e.printStackTrace();
                    next=next+"?res=NO";
                    throw new ServletException(e.getMessage());
                }

                bg3=null;
                fp3=null;
                fv3=null;
                inter3=null;
            break;
            ////FIN  creado por Miguel Altamiranda////////////
            case 16:
                next="/jsp/fenalco/negocios/resultado.jsp?vista=3";
                String tasa=request.getParameter("tasa");
                String cod_neg=request.getParameter("cod");
                try{
                    model.Negociossvc.habReliquidar(cod_neg,tasa);
                    next=next+"&res=OK";
                }
                catch (Exception e){
                e.printStackTrace();
                next=next+"&res=NO";
                throw new ServletException(e.getMessage());
                }
            break;

            case 17:
                next="/jsp/fenalco/negocios/resultado.jsp?vista=4";
                String aval=request.getParameter("aval");
                cod_neg=request.getParameter("cod");
                try{
                    model.Negociossvc.avalarNegocio(cod_neg, aval, usuario.getLogin());
                    next=next+"&res=OK";
                }
                catch (Exception e){
                e.printStackTrace();
                next=next+"&res=NO";
                throw new ServletException(e.getMessage());
                }
            break; 
            case 18:
                try {
                    String num = (request.getParameter("numero") != null) ? request.getParameter("numero").toUpperCase() : "";
                  
                    String fecini = request.getParameter("fechaInicio");
                    String fecfin = request.getParameter("fechaFinal");
                    String tipo = request.getParameter("tipodoc");
                    String banco = request.getParameter("banco_docs");
                    String factura = (request.getParameter("factura") != null)? request.getParameter("factura").toUpperCase() : "";


                    model.ingresoService.buscarIngresos(usuario.getDstrct(), tipo, banco, fecini, fecfin, num, factura);
                    if (model.ingresoService.getListadoingreso().size() > 0) {
                        next = "/jsp/fenalco/contabilizacion/devolucionesDet.jsp";
                    } else {
                        next = "/jsp/fenalco/contabilizacion/devoluciones.jsp?msg=La busqueda no arrojo resultados";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    next = next + "&res=NO";
                    throw new ServletException(e.getMessage());
                }
                break;
            case 21:
                redirect = false;
                codneg = request.getParameter("negocio");
                form = request.getParameter("form");
                act = request.getParameter("act") != null ? request.getParameter("act") : "";
                coment = request.getParameter("coment") != null ? request.getParameter("coment") : "";
                causa = request.getParameter("causa") != null ? request.getParameter("causa") : "";
                concep = request.getParameter("concep") != null ? request.getParameter("concep") : "";

                try {
                    negocio = model.Negociossvc.buscarNegocio(codneg);
                    tn = "";
                    if (negocio.getTneg().equals("01")) {
                        tn = "CH";
                    } else {
                        if (negocio.getTneg().equals("02")) {
                            tn = "LT";
                        } else {
                            tn = "PG";
                        }
                    }
                    cod_banco = tn.equals("CH") ? negocio.getCodigoBanco() : "170";
                    cuenta_cheque = (String) session.getAttribute("cuenta_cheque");

                    NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService();
                    TransaccionService scv = new TransaccionService(usuario.getBd());
                    String sql = "";
                    sql = model.Negociossvc.admit(usuario.getLogin(), obs, codneg, negocio.getNumaval(), cod_banco, cuenta_cheque, "A");
                    
                    NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                    negtraza.setActividad(act);
                    negtraza.setNumeroSolicitud(form);
                    negtraza.setUsuario(usuario.getLogin());
                    negtraza.setCausal(causa);
                    negtraza.setComentarios(coment);
                    negtraza.setDstrct(usuario.getDstrct());
                    negtraza.setConcepto(concep);
                    negtraza.setCodNeg(codneg);

                    scv.crearStatement();
                    scv.getSt().addBatch(sql);
                    scv.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                    scv.getSt().addBatch(trazaService.updateActividad(codneg, act));
                    scv.getSt().addBatch(trazaService.updateEstadoForm(form, "A"));
                    scv.execute();

                    strRespuesta = "/controller?estado=Negocios&accion=Ver&op=5&vista=9";

                } catch (Exception e) {
                    e.printStackTrace();
                    next = next + "&res=NO";
                    throw new ServletException(e.getMessage());
                }
                break;
            case 99:
                next="/jsp/fenalco/letras/form_pagare.jsp";
                String codnegocio=(String)request.getParameter("codigo_negocio");
                next=next+"?var="+codnegocio;
                break;
        }
      if (redirect) {
            this.dispatchRequest(next);
        } else {
            response.setContentType("text/plain");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            try {
                response.getWriter().print(strRespuesta);
            } catch (IOException ex) {
                Logger.getLogger(NegociosUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
