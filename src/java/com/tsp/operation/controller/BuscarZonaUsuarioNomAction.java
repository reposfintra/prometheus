/*
 * BuscarZonanomAction.java
 *
 * Created on 3 de marzo de 2005, 02:07 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class BuscarZonaUsuarioNomAction extends Action {
    
    /** Creates a new instance of BuscarZonanomAction */
    public BuscarZonaUsuarioNomAction() {
    }
    
    public void run() throws ServletException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String nombre = (request.getParameter("nombre"))+"%";        
        try{ 
            model.zonaUsuarioService.buscarZonaNombre(nombre);
            Vector VecZonas = model.zonaUsuarioService.obtZonas();
          /*  if (VecZonas.size()==0) {
                next = "jsp/trafico/mensaje/ErrorBusq.jsp?ruta=/jsp/trafico/zona/BuscarZonaUsuario.jsp";
            }*/
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
   
}
