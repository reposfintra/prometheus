/********************************************************************
 *      Nombre Clase.................   AplicacionObtenertAction.java    
 *      Descripci�n..................   Obtiene un registro de la tabla tblapl
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class AplicacionObtenerAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public AplicacionObtenerAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblapl/AppUpdate.jsp?mensaje=";
                String next = "";
                
                String cia = request.getParameter("cia");
                String code = request.getParameter("code");
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        model.appSvc.obtenerAplicacion(cia, code);
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Aplicaci�n");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
