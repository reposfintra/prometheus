/*
 * CargarEstadobusqAction.java
 *
 * Created on 10 de marzo de 2005, 12:32 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


/**
 *
 * @author  DIBASMO
 */
public class CargarEstadobusqAction extends Action{
    
    /** Creates a new instance of CargarEstadobusqAction */
    public void run() throws javax.servlet.ServletException, InformationException{
        String next = "/ciudad/SeleccionarEstado.jsp?mensaje=cargado";
        String pais = request.getParameter("pais");
        HttpSession session = request.getSession();
        String lenguaje = (String) session.getAttribute("idioma");
        
        this.dispatchRequest(next);
    }
    
}
