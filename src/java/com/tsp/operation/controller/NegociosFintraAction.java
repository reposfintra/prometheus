/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.NegociosFintraDAO;
import com.tsp.operation.model.DAOS.impl.NegociosFintraImpl;
import com.tsp.operation.model.beans.ConceptoFactura;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.VariablesRefinanciacion;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author mcamargo
 */
public class NegociosFintraAction extends Action {

    private final int CARGAR_NEGOCIOS_A_RELIQUIDAR = 0;
    private final int CARGAR_LIQUIDACION = 1;
    private final int INSERT_DOCUMENT_LIQUIDACION = 2;
    private final int VALIDAR_NEGOCIO_ACTIVO = 3;
    private final int CARGAR_REPORTE_ACT_SALDOS = 4;
    private final int EXPORTAR_REPORTE_ACT_SALDOS = 5;
    private final int CARGAR_REPORTE_NEGOCIOS_NUEVOS = 6;
    private final int EXPORTAR_REPORTE_NEGOCIOS_NUEVOS = 7;
    private final int CARGAR_COMBO_ASEGURADORAS = 8;
    private final int CARGAR_COMBO_POLIZA = 9;
    private final int CARGAR_CXP_ASEGURADORAS = 10;
    private final int GENERAR_CXP_CONSOLIDADA_ASEGURADORA = 11;
    private final int CARGAR_COMBO_CAJAS = 12;
    private final int CARGAR_DETALLE_CXC_CAJA = 13;
    private final int GENERAR_CXC_CAJA_RECAUDOS = 14;
    private final int EXPORTAR_CXP_DEFINITIVA_ASEGURADORAS = 15;
    private final int CARGAR_SOLICITUTES_REASIGNAR = 16;
    private final int CARGAR_COMBO_ASESORES = 17;
    private final int UPDATE_ASESORES = 18;
    private final int CARGAR_NEGOCIOS_REFINANCIAR = 19;
    private final int CARGAR_DETALLE_CARTERA_REFINANCIAR = 20;
    private final int VER_CARTERA_A_REFINANCIAR = 21;
    private final int PROYECCION_REFINANCIACION_NEGOCIOS = 22;
    private final int BUSCAR_FECHA_REFI = 23;
    private final int BUSCAR_NEGOCIOS_APROBAR_REFI = 24;
    private final int BUSCAR_LIQUIDACION_NEGOCIOS_APROBAR_REFI = 25;
    private final int APROBAR_REFINANCIACION = 26;
    private final int CARGAR_COMBO_UNIDAD_NEGOCIO = 27;
    private final int CARGAR_REPORTE_NEGOCIOS_CARTERA = 28;
    private final int EXPORTAR_REPORTE_NEGOCIOS_CARTERA = 29;
    private final int BUSCAR_CARTERA_POR_ASIGNAR = 30;
    private final int ASIGNAR_CARTERA = 31;
    private final int GENERAR_ARCHIVO_ASOBANCARIA = 32;
    private final int CARGAR_COMBO_ENTIDAD_RECAUDO = 33;
    private final int CARGAR_RECAUDOS = 34;
    private final int CARGAR_RECAUDO_DETALLE = 35;
    private final int CARGAR_COMBO_RECAUDADOR_TERCERO = 36;
    private final int CARGAR_CARTERA_POR_ASIGNAR = 37;
    private final int APROBAR_CARTERA_POR_ASIGNAR = 38;
    private final int CARGAR_COMBO_RESPONSABLE_CUENTA = 39;
    private final int CARGAR_COMBO_CIUDAD = 40;
    private final int CARGAR_COMBO_BARRIO = 41;
    private final int CARGAR_CASAS_COBRANZA = 42;
    private final int CAMBIAR_ESTADO_CASA_COBRANZA = 43;
    private final int UPDATE_CASA_COBRANZA = 44;
    private final int GENERAR_CARTAS_COBRO = 45;
    private final int CARGAR_COMBO_MORA = 46;
    private final int CALCULAR_CUOTA_INICAL_A_PAGAR = 47;
    private final int IMPRIMIR_EXTRACTO = 48;
    private final int COMBO_ESTRATEGIA = 49;
    private final int ANULAR_REFINANCIACION = 50;
    private final int CARGAR_OBSERVACION_SIMULACION = 51;

    NegociosFintraDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";
    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;
    String dbName;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            int opcion = Integer.parseInt(request.getParameter("opcion"));
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            reponseJson = "{}";
            
            dao = new NegociosFintraImpl(usuario.getBd());
            switch (opcion) {

                case CARGAR_NEGOCIOS_A_RELIQUIDAR:
                    cargarNegociosReliquidar();
                    break;
                case CARGAR_LIQUIDACION:
                    cargarLiquidacion();
                    break;
                case INSERT_DOCUMENT_LIQUIDACION:
                    insert_document_neg_aceptados();
                    break;
                case VALIDAR_NEGOCIO_ACTIVO:
                    validar_negocio_activo();
                    break;
                case CARGAR_REPORTE_ACT_SALDOS:
                    CargarReportesSaldoCartera();
                    break;
                case EXPORTAR_REPORTE_ACT_SALDOS:
                    Exportar_Reporte_Act_Saldos();
                    break;
                case CARGAR_REPORTE_NEGOCIOS_NUEVOS:
                    CargarReporteNegociosNuevos();
                    break;
                case EXPORTAR_REPORTE_NEGOCIOS_NUEVOS:
                    ExportarReporteNegociosNuevos();
                    break;
                case CARGAR_COMBO_ASEGURADORAS:
                    Cargar_Combo_Aseguradoras();
                    break;
                case CARGAR_COMBO_POLIZA:
                    Cargar_Combo_Polizas();
                    break;
                case CARGAR_CXP_ASEGURADORAS:
                    Cargar_Cxp_Aseguradoras();
                    break;
                case GENERAR_CXP_CONSOLIDADA_ASEGURADORA:
                    Generar_Cxp_consolidada_Aseguradora();
                    break;
                case CARGAR_COMBO_CAJAS:
                    Cargar_combo_cajas();
                    break;
                case CARGAR_DETALLE_CXC_CAJA:
                    Cargar_detalle_cxc_caja();
                    break;
                case GENERAR_CXC_CAJA_RECAUDOS:
                    Generar_cxc_caja_recaudos();
                    break;
                case EXPORTAR_CXP_DEFINITIVA_ASEGURADORAS:
                    Exportar_cxp_definitiva_aseguradoras();
                    break;
                case CARGAR_SOLICITUTES_REASIGNAR:
                    cargar_solicitudes_reasignar();
                    break;
                case CARGAR_COMBO_ASESORES:
                    cargar_combo_asesores();
                    break;
                case UPDATE_ASESORES:
                    update_asesores();
                    break;
                case CARGAR_NEGOCIOS_REFINANCIAR:
                    cargar_negocios_refinanciar();
                    break;
                case CARGAR_DETALLE_CARTERA_REFINANCIAR:
                    cargar_detalle_cartera_refinanciar();
                    break;
                case VER_CARTERA_A_REFINANCIAR:
                    ver_cartera_refinanciar();
                    break;
                case PROYECCION_REFINANCIACION_NEGOCIOS:
                    proyeccion_refinanciacion_negocios();
                    break;
                case BUSCAR_FECHA_REFI:
                    buscar_fecha_refi();
                    break;
                case BUSCAR_NEGOCIOS_APROBAR_REFI:
                    buscar_negocios_aprobar_refi();
                    break;
                case BUSCAR_LIQUIDACION_NEGOCIOS_APROBAR_REFI:
                    buscar_liquidacion_negocios_aprobar_refi();
                    break;
                case APROBAR_REFINANCIACION:
                    aprobar_refinanciacion();
                    break;
                case CARGAR_COMBO_UNIDAD_NEGOCIO:
                    cargarComboUnidadNegocio();
                    break;
                case CARGAR_REPORTE_NEGOCIOS_CARTERA:
                    cargarReporteNegociosCartera();
                    break;
                case EXPORTAR_REPORTE_NEGOCIOS_CARTERA:
                    exportarNegociosReporteCartera();
                    break;
                case BUSCAR_CARTERA_POR_ASIGNAR:
                    buscarCarteraPorAsignar();
                    break;
                case ASIGNAR_CARTERA:
                    asignarCartera();
                    break;
                case GENERAR_ARCHIVO_ASOBANCARIA:
                    generarArchivoAsobancaria();
                    break;
                case CARGAR_COMBO_ENTIDAD_RECAUDO:
                    cargarComboEntidadRecaudo();
                    break;
                case CARGAR_RECAUDOS:
                    cargarRecaudos();
                    break;
                case CARGAR_RECAUDO_DETALLE:
                    cargarRecaudoDetalle();
                    break;
                case CARGAR_COMBO_RECAUDADOR_TERCERO:
                    cargarRecaudadorTercero();
                    break;
                case CARGAR_CARTERA_POR_ASIGNAR:
                    cargarCarteraPorAsignar();
                    break;
                case APROBAR_CARTERA_POR_ASIGNAR:
                    aprobarCarteraPorAsignar();
                    break;
                case CARGAR_COMBO_RESPONSABLE_CUENTA:
                    cargarComboResponsableCuenta();
                    break;
                case CARGAR_COMBO_CIUDAD:
                    cargarComboCiudad();
                    break;
                case CARGAR_COMBO_BARRIO:
                    cargarComboBarrio();
                    break;
                case CARGAR_CASAS_COBRANZA:
                    cargarCasasCobranza();
                    break;
                case CAMBIAR_ESTADO_CASA_COBRANZA:
                    cambiarEstadoCasaCobranza();
                case UPDATE_CASA_COBRANZA:
                    updateCasaCobranza();
                    break;
                case GENERAR_CARTAS_COBRO:
                    generarCartasCobro();
                    break;
                case CARGAR_COMBO_MORA:
                    cargarComboMora();
                    break;
                case CALCULAR_CUOTA_INICAL_A_PAGAR:
                    getCalcularCuotaInicialApagar();
                    break;
                case IMPRIMIR_EXTRACTO:
                    imprimirExtrato();
                    break;
                case COMBO_ESTRATEGIA:
                    cargarComboEstrategiaCartera();
                    break;
                case ANULAR_REFINANCIACION:
                    anularRefinanciacion();
                    break;
                case CARGAR_OBSERVACION_SIMULACION:
                    cargarObservacionSimulacion();
                    break;
                default:
                    this.printlnResponseAjax("{\"error\":\"Peticion invalida 404 recurso no encontrado\"}", "application/json;");
                    break;
            }
        } catch (Exception ex) {
            reponseJson = "{\"error\":\"Algo salio mal al procesar su solicitud\",\"exception\":\"" + ex.getMessage() + "\"}";
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(NegociosFintraAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void cargarNegociosReliquidar() {
        try {
            this.reponseJson = dao.cargarNegociosReliquidar();
        } catch (Exception e) {

        }
    }

    private void cargarLiquidacion() {

        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String valor = request.getParameter("valor") != null ? request.getParameter("valor") : "";
        String cuota = request.getParameter("cuota") != null ? request.getParameter("cuota") : "";
        String convenio = request.getParameter("convenio") != null ? request.getParameter("convenio") : "";
        String fechacuota = request.getParameter("fechacuota") != null ? request.getParameter("fechacuota") : "";
        String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";

        try {
            this.reponseJson = dao.cargarLiquidacion(negocio, valor, cuota, convenio, fechacuota, identificacion);
        } catch (Exception e) {

        }
    }

    private void insert_document_neg_aceptados() {

        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String valor = request.getParameter("valor") != null ? request.getParameter("valor") : "";
        String cuota = request.getParameter("cuota") != null ? request.getParameter("cuota") : "";
        String convenio = request.getParameter("convenio") != null ? request.getParameter("convenio") : "";
        String fechacuota = request.getParameter("fechacuota") != null ? request.getParameter("fechacuota") : "";
        String usuario = this.usuario.getLogin();
        String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
        try {

            dao.insert_document_neg_aceptados(negocio, valor, cuota, convenio, fechacuota, usuario, identificacion);

        } catch (Exception e) {

        }

    }

    private void validar_negocio_activo() {

        String numero_solicitud = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "";
        try {
            reponseJson = dao.validar_negocio_activo(numero_solicitud);
        } catch (Exception e) {

        }
    }

    private void CargarReportesSaldoCartera() {

        try {
            String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
            String unidad_negocio = request.getParameter("unidad_negocio") != null ? request.getParameter("unidad_negocio") : "";

            reponseJson = dao.CargarReportesSaldoCartera(periodo, unidad_negocio);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Exportar_Reporte_Act_Saldos() throws Exception {
        try {

            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("ActualizacionSaldoCartera", "Reporte");
            String[] cabecera = {"INTERMEDIARIO", "NIT", "COD_NEG", "SALDO_CAPITAL_CORTE", "TOTAL_OBLICACION", "FECHA_CORTE", "CUOTAS_PENDIENTES", "INICIO_MORA", "FECHA_DE_CANCELACION", "ESTADO", "NUM_PAGARE"};

            short[] dimensiones = new short[]{
                5000, 4000, 5000, 8000, 4000, 4000, 5000, 5000, 4000, 12000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("INTERMEDIARIO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("NIT").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("COD_NEG").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("SALDO_CAPITAL_CORTE").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("TOTAL_OBLICACION").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("FECHA_CORTE").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("CUOTAS_PENDIENTES").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("INICIO_MORA").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("FECHA_DE_CANCELACION").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("ESTADO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("NUM_PAGARE").getAsString(), letra);

            }

            this.cerrarArchivo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 11);
            xls.adicionarCelda(0, 0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.BLUE.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.BLUE.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 2;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void CargarReporteNegociosNuevos() {

        try {
            String fechaini = request.getParameter("fechaini") != null ? request.getParameter("fechaini") : "";
            String fechafin = request.getParameter("fechafin") != null ? request.getParameter("fechafin") : "";
            String unidad_negocio = request.getParameter("unidad_negocio") != null ? request.getParameter("unidad_negocio") : "";

            reponseJson = dao.CargarReporteNegociosNuevos(fechaini, fechafin, unidad_negocio);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ExportarReporteNegociosNuevos() throws Exception {
        try {

            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("NegociosNuevos", "Reporte Negocios Nuevos Fintra");
            String[] cabecera = {"INTERMEDIARIO", "COD_SUCURSAL", "TITULAR", "TIPO_IDENTIFICACION", "CEDULA_TITULAR", "GENERO_TITULAR", "DIRECCION_TITULAR", "COD_MUNICIPIO", "TEL_TITULAR",
                "TELEFONO2_TITULAR", "FAX_TITULAR", "COD_CI", "NUM_PAGARE", "COD_NEG", "MONEDA", "VALOR_MONTO", "FECHA_DESEMBOLSO", "NRO_DOCS", "FECHA_VENCIMIENTO", "PERIODO_GRACIA",
                "TIPO_CARTERA", "TIPO_INVERSION", "TIPO_RECURSO", "VALOR_REDESCUENTO", "PORC_REDESCUENTO", "NIT_ENTIDAD_REDESCUENTO", "CONVENIO", "PRODUCTO_GARANTIA", "PORCENTAJE_AVAL",
                "RESPONSABLE", "DESCRIP_INTERMEDIARIO", "IDENTIFICACION", "TIPO_IDENTIFICACION_CODEUDOR", "CODEUDOR", "DIRECCION", "COD_MUNICIPIO", "CELULAR", "TELEFONO2",
                "CAMPO_RESERVADO", "CAMPO_RESERVADO2", "ACTIVIDAD_ECONOMICA", "COD_MUNICIPIO", "PAYMENT_NAME", "TIENDA_SUCURSAL", "ESTRATO", "FECHA_NACIMIENTO", "ESTADO_CIVIL"};

            short[] dimensiones = new short[]{
                5000, 4000, 5000, 8000, 4000, 4000, 5000, 5000, 4000, 12000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("INTERMEDIARIO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("COD_SUCURSAL").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TITULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TIPO_IDENTIFICACION").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("CEDULA_TITULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("GENERO_TITULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("DIRECCION_TITULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("COD_MUNICIPIO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TEL_TITULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TELEFONO2_TITULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("FAX_TITULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("COD_CI").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("NUM_PAGARE").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("COD_NEG").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("MONEDA").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("VALOR_MONTO").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("FECHA_DESEMBOLSO").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("NRO_DOCS").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("FECHA_VENCIMIENTO").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("PERIODO_GRACIA").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TIPO_CARTERA").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TIPO_INVERSION").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TIPO_RECURSO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("VALOR_REDESCUENTO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("PORC_REDESCUENTO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("NIT_ENTIDAD_REDESCUENTO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("CONVENIO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("PRODUCTO_GARANTIA").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("PORCENTAJE_AVAL").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("RESPONSABLE").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("DESCRIP_INTERMEDIARIO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("IDENTIFICACION").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TIPO_IDENTIFICACION_CODEUDOR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("CODEUDOR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("DIRECCION").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("COD_MUNICIPIO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("CELULAR").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TELEFONO2").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("CAMPO_RESERVADO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("CAMPO_RESERVADO2").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("ACTIVIDAD_ECONOMICA").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("COD_MUNICIPIO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("PAYMENT_NAME").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("TIENDA_SUCURSAL").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("ESTRATO").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("FECHA_NACIMIENTO").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("ESTADO_CIVIL").getAsString(), letra);

            }

            this.cerrarArchivo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void Cargar_Combo_Aseguradoras() {

        try {

            this.reponseJson = dao.Cargar_Combo_Aseguradoras();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Cargar_Combo_Polizas() {

        try {

            this.reponseJson = dao.Cargar_Combo_Polizas();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Cargar_Cxp_Aseguradoras() {
        String nit_aseguradora = request.getParameter("aseguradora") != null ? request.getParameter("aseguradora") : "";
        String id_poliza = request.getParameter("poliza") != null ? request.getParameter("poliza") : "";
        String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";

        try {

            this.reponseJson = dao.Cargar_Cxp_Aseguradoras(nit_aseguradora, id_poliza, periodo);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Generar_Cxp_consolidada_Aseguradora() {
        String listadoCxps = request.getParameter("listadoCxps");
        String aseguradora = request.getParameter("aseguradora");
        String poliza = request.getParameter("poliza");
        JsonParser jsonParser = new JsonParser();
        JsonObject jneg = (JsonObject) jsonParser.parse(listadoCxps);
        JsonArray jsonArrCxp = jneg.getAsJsonArray("documentos");
        List<String> x = new ArrayList<String>();
        for (int i = 0; i < jsonArrCxp.size(); i++) {
            String id = jsonArrCxp.get(i).getAsJsonObject().get("documento").getAsJsonPrimitive().getAsString();
            x.add(id);
        }
        String[] ArrayCxp = x.toArray(new String[0]);
        String[] split = dao.Generar_Cxp_consolidada_Aseguradora(ArrayCxp, usuario.getLogin(), aseguradora, poliza).split(";");
        this.reponseJson = "{\"respuesta\":\"" + split[0] + "\",\"numCxP\":\"" + split[1] + "\",\"numCxpClie\":\"" + split[2] + "\"}";
    }

    private void Cargar_combo_cajas() {
        try {

            this.reponseJson = dao.Cargar_combo_cajas();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Cargar_detalle_cxc_caja() {
        String caja = request.getParameter("caja") != null ? request.getParameter("caja") : "";
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";

        try {

            this.reponseJson = dao.Cargar_detalle_cxc_caja(caja, fecha);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Generar_cxc_caja_recaudos() {
        String listadodetalle = request.getParameter("listadoDetalle");
        String caja = request.getParameter("caja");
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";
        JsonParser jsonParser = new JsonParser();
        JsonObject jneg = (JsonObject) jsonParser.parse(listadodetalle);
        JsonArray jsonArrCxp = jneg.getAsJsonArray("num_ingresos");
        List<String> x = new ArrayList<String>();
        for (int i = 0; i < jsonArrCxp.size(); i++) {
            String id = jsonArrCxp.get(i).getAsJsonObject().get("num_ingreso").getAsJsonPrimitive().getAsString();
            x.add(id);
        }
        String[] ArrayCxp = x.toArray(new String[0]);
        String[] split = dao.Generar_cxc_caja_recaudo(ArrayCxp, usuario.getLogin(), caja, fecha).split(";");
        this.reponseJson = "{\"respuesta\":\"" + split[0] + "\",\"numCxC\":\"" + split[1] + "\"}";
    }

    private void Exportar_cxp_definitiva_aseguradoras() throws Exception {
        try {

            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            String poliza = request.getParameter("poliza");
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("CXP ASEGURADORA " + poliza, "Reporte Cxp Aseguradora");
            String[] cabecera = {"NIT ASEGURADORA", "ASEGURADORA", "POLIZA", "NO DOCUMENTO", "FECHA VENCIMIENTO", "NEGOCIO", "VALOR NETO POLIZA", "VALOR IVA POLIZA", "VALOR TOTAL POLIZA", "SALDO CARTERA", "CUOTA", "CAPITAL"};

            short[] dimensiones = new short[]{
                5000, 4000, 5000, 8000, 4000, 4000, 5000, 5000, 5000, 5000, 5000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("nit").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("aseguradora").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("poliza").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("documento").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_vencimiento").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("documento_relacionado").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor").getAsDouble(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("valor_iva").getAsDouble(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("vlr_neto").getAsDouble(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("saldo_cartera").getAsDouble(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("cuota").getAsDouble(), numero);
                xls.adicionarCelda(fila, col++, objects.get("capital").getAsDouble(), dinero);
            }

            this.cerrarArchivo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void cargar_solicitudes_reasignar() {

        try {
            this.reponseJson = dao.cargar_solicitudes_reasignar(usuario.getLogin());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_combo_asesores() {
        try {
            this.reponseJson = dao.cargar_combo_asesores();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void update_asesores() {
        String listadosolicitudes = request.getParameter("listadosolicitudes") != null ? request.getParameter("listadosolicitudes") : "";
        String asesor = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
        JsonParser jsonParser = new JsonParser();
        JsonObject jneg = (JsonObject) jsonParser.parse(listadosolicitudes);
        JsonArray jsonArrSol = jneg.getAsJsonArray("solicitudes");
        List<String> x = new ArrayList<String>();

        for (int i = 0; i < jsonArrSol.size(); i++) {
            String id = jsonArrSol.get(i).getAsJsonObject().get("numero_solicitud").getAsJsonPrimitive().getAsString();
            x.add(id);
        }
        String[] ArraySolicitudes = x.toArray(new String[0]);
        String Result = dao.update_asesores(ArraySolicitudes, asesor, usuario.getLogin());
        this.reponseJson = "{\"respuesta\":\"" + Result + "\"}";

    }

    private void cargar_negocios_refinanciar() {
        String tipo_busqueda = request.getParameter("tipo_busqueda") != null ? request.getParameter("tipo_busqueda") : "";
        String documento = request.getParameter("documento") != null ? request.getParameter("documento") : "";
        String tipo_refi = request.getParameter("tipo_refi") != null ? request.getParameter("tipo_refi") : "";
        try {

            this.reponseJson = dao.cargar_negocios_refinanciar(tipo_busqueda, documento, tipo_refi);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_detalle_cartera_refinanciar() {
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String tipo_refi = request.getParameter("tipo_refi") != null ? request.getParameter("tipo_refi") : "";
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";

        try {
            this.reponseJson = dao.cargar_detalle_cartera_refinanciar(negocio, tipo_refi, fecha);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ver_cartera_refinanciar() {
        String valor_capital = request.getParameter("saldo_capital") != null ? request.getParameter("saldo_capital") : "0";
        String no_cuotas = request.getParameter("cuota") != null ? request.getParameter("cuota") : "0";
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";
        String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";
        String compra_cartera = request.getParameter("compra_cartera") != null ? request.getParameter("compra_cartera") : "";
        String cuota_inicio = request.getParameter("cuota_inicio") != null ? request.getParameter("cuota_inicio") : "0";
        String tasa = request.getParameter("tasa") != null ? request.getParameter("tasa") : "0";
        String saldo_cat = request.getParameter("saldo_cat") != null ? request.getParameter("saldo_cat") : "0";
        String tipo_refi = request.getParameter("tipo_refi") != null ? request.getParameter("tipo_refi") : "0";

        this.reponseJson = dao.verCarteraRefinanciar(valor_capital, no_cuotas,
                fecha, ciudad, compra_cartera, cuota_inicio, tasa, saldo_cat, tipo_refi);

    }

    private void proyeccion_refinanciacion_negocios() {

        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String tipoRefi = request.getParameter("tipo_refi") != null ? request.getParameter("tipo_refi") : "";
        String fechaPrimeraCuota = request.getParameter("fecha_primera_cuota") != null ? request.getParameter("fecha_primera_cuota") : "";
        int plazo = request.getParameter("cuota") != null ? Integer.parseInt(request.getParameter("cuota")) : 0;
        String fechaProyeccion = request.getParameter("fecha_proyeccion") != null ? request.getParameter("fecha_proyeccion") : "";
        int cuota_inicio = request.getParameter("cuota_inicio") != null ? Integer.parseInt(request.getParameter("cuota_inicio")) : 0;
        String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";
        String compraCartera = request.getParameter("compra_cartera") != null ? request.getParameter("compra_cartera") : "";
        double porce = request.getParameter("porcentaje") != null ? Double.parseDouble(request.getParameter("porcentaje")) : 0;
        double interes = request.getParameter("interes") != null ? Double.parseDouble(request.getParameter("interes")) : 0;
        double catVencido = request.getParameter("cat_vencido") != null ? Double.parseDouble(request.getParameter("cat_vencido")) : 0;
        double cuotaAdmon = request.getParameter("cuota_admin") != null ? Double.parseDouble(request.getParameter("cuota_admin")) : 0;
        double intMora = request.getParameter("intxmora") != null ? Double.parseDouble(request.getParameter("intxmora")) : 0;
        double gastoCobranza = request.getParameter("gasto_cobranza") != null ? Double.parseDouble(request.getParameter("gasto_cobranza")) : 0;
        double pagoInicial = request.getParameter("pago_inicial") != null ? Double.parseDouble(request.getParameter("pago_inicial")) : 0;
        double capitalRefinanciacion = request.getParameter("capital_refin") != null ? Double.parseDouble(request.getParameter("capital_refin")) : 0;
        double saldoCat = request.getParameter("saldo_cat") != null ? Double.parseDouble(request.getParameter("saldo_cat")) : 0;
        boolean isPago =(pagoInicial > 0);
        double total_pagar = request.getParameter("tot_a_pagar") != null ? Double.parseDouble(request.getParameter("tot_a_pagar")) : 0;
        int periodo_pago_inicial = request.getParameter("periodo_pago") != null ? Integer.parseInt(request.getParameter("periodo_pago")) : 0;        
        String observacion = request.getParameter("observacion") != null ? request.getParameter("observacion"): "";        
        long celular = request.getParameter("celular") != null ? Long.parseLong(request.getParameter("celular")): 0;        
                
        VariablesRefinanciacion refinanciacion = new VariablesRefinanciacion(negocio, tipoRefi, fechaPrimeraCuota, plazo,
                fechaProyeccion, cuota_inicio, ciudad, compraCartera, porce, isPago,
                interes, catVencido, cuotaAdmon, intMora, gastoCobranza, pagoInicial,
                capitalRefinanciacion, saldoCat,total_pagar,periodo_pago_inicial,observacion,celular);

        try {
            this.reponseJson = dao.proyeccion_refinanciacion_negocios(refinanciacion, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buscar_fecha_refi() {

        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String tipo_refi = request.getParameter("tipo_refi") != null ? request.getParameter("tipo_refi") : "";
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";
        try {
            this.reponseJson = dao.buscar_fecha_refi(negocio, tipo_refi, fecha);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void buscar_negocios_aprobar_refi() {
        String tipo_busqueda = request.getParameter("tipo_busqueda") != null ? request.getParameter("tipo_busqueda") : "";
        String documento = request.getParameter("documento") != null ? request.getParameter("documento") : "";
        String estado = request.getParameter("estado_refi") != null ? request.getParameter("estado_refi") : "";

        try {
            this.reponseJson = dao.buscar_negocios_aprobar_refi(documento, tipo_busqueda,estado,usuario);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buscar_liquidacion_negocios_aprobar_refi() {
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        try {

            this.reponseJson = dao.buscar_liquidacion_negocios_aprobar_refi(negocio);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void aprobar_refinanciacion() {
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String key_ref = request.getParameter("key_ref") != null ? request.getParameter("key_ref") : "";
        String tipo_refi = request.getParameter("tipo_refi") != null ? request.getParameter("tipo_refi") : "";
        String fecha_vencimiento_acuerdo = request.getParameter("fecha_vencimiento_acuerdo") != null ? request.getParameter("fecha_vencimiento_acuerdo") : "";
        String observacion = request.getParameter("observacion") != null ? request.getParameter("observacion") : "";

        try {
            this.reponseJson = dao.aprobar_refinanciacion(usuario, negocio, tipo_refi, key_ref, fecha_vencimiento_acuerdo,observacion);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void cargarComboUnidadNegocio() {
        this.reponseJson = dao.cargarComboUnidadNegocio(usuario.getLogin());
    }

    private void cargarReporteNegociosCartera() {
        String id_entidad_recaudo = request.getParameter("id_recaudador_tercero") != null ? request.getParameter("id_recaudador_tercero") : "";
        String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") : "";
        try {
            this.reponseJson = dao.cargarReporteNegociosCartera(id_unidad_negocio, id_entidad_recaudo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportarNegociosReporteCartera() throws Exception {

        try {
            String id_entidad_recaudo = request.getParameter("id_recaudador_tercero") != null ? request.getParameter("id_recaudador_tercero") : "";
            String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") : "";

            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.exportarNegociosReporteCartera(id_unidad_negocio, id_entidad_recaudo));
            this.generarRUTA();
            this.crearLibro("ReporteCarteraNegocios", "Reporte");
            String[] cabecera = {"Empresa Responsable", "Perido Carga", "Estado", "Cedula", "Nombre Cliente", "Direccion Domicilio", "Ciudad Domicilio", "Barrio Domicilio",
                "Telefono", "Celular", "Telefono Direccion Negocio", "Barrio Negocio", "Telefono Negocio", "Negocio", "Unidad Negocio", "Agencia", "Altura Mora", "Dia pago",
                "Saldo Actual", "Saldo Vencido", "Interes Mora", "Gac", "Total A Pagar", "Juridica", "Reestructuracion", "Fecha Ultimo Compromiso", "Responsable Cuenta"};

            short[] dimensiones = new short[]{
                5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;

                xls.adicionarCelda(fila, col++, objects.get("empresa_responsable").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("periodo_carga").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("estado").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cedula").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_cliente").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("direccion_domicilio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("ciudad_domicilio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("barrio_domicilio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("telefono").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("celular").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("direccion_negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("barrio_negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("telefon_negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("unidad_negocio").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("agencia").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("altura_mora_actual").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("dia_pago").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("saldo_actual").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("valor_saldo_vencido").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("interes_mora").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("gac").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("total_a_pagar").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("juridica").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("reestructuracion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_ult_compromiso").getAsString(), ftofecha);
                xls.adicionarCelda(fila, col++, objects.get("responsable_cuenta").getAsString(), letra);

            }

            this.cerrarArchivo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void buscarCarteraPorAsignar() {
        String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") : "";
        String id_agencia = request.getParameter("id_agencia") != null ? request.getParameter("id_agencia") : "";
        String id_altura_mora = request.getParameter("id_altura_mora") != null ? request.getParameter("id_altura_mora") : "";
        String id_responsable_cuenta = request.getParameter("id_responsable_cuenta") != null ? request.getParameter("id_responsable_cuenta") : "";
        String id_ciudad = request.getParameter("id_ciudad") != null ? request.getParameter("id_ciudad") : "";
        String id_barrio = request.getParameter("id_barrio") != null ? request.getParameter("id_barrio") : "";
        try {
            this.reponseJson = dao.buscarCarteraPorAsignar(id_unidad_negocio, id_agencia, id_altura_mora, id_responsable_cuenta, id_ciudad, id_barrio);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asignarCartera() {

        try {
            String empresa_responsable = request.getParameter("empresa_responsable") != null ? request.getParameter("empresa_responsable") : "";
            String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") : "";
            String id_agencia = request.getParameter("id_agencia") != null ? request.getParameter("id_agencia") : "";
            String id_altura_mora = request.getParameter("id_altura_mora") != null ? request.getParameter("id_altura_mora") : "";

            String listnegocios = request.getParameter("listnegocios") != null ? request.getParameter("listnegocios") : "";
            JsonParser jsonParser = new JsonParser();
            JsonObject jneg = (JsonObject) jsonParser.parse(listnegocios);
            JsonArray jsonArrNeg = jneg.getAsJsonArray("negocio");
            List<String> x = new ArrayList<>();
            for (int i = 0; i < jsonArrNeg.size(); i++) {
                String id = jsonArrNeg.get(i).getAsJsonObject().get("negocio").getAsJsonPrimitive().getAsString();
                x.add(id);
            }
            String[] ArrayNeg = x.toArray(new String[0]);

            String result = dao.asignarCartera(empresa_responsable, id_unidad_negocio, id_agencia, id_altura_mora, usuario.getLogin(), ArrayNeg);
            this.reponseJson = "{\"respuesta\":\"" + result + "\"}";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generarArchivoAsobancaria() {
        String id_archivo = request.getParameter("id_archivo") != null ? request.getParameter("id_archivo") : "";
        try {
            this.reponseJson = dao.generarArchivoAsobancaria(id_archivo, usuario.getLogin());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarComboEntidadRecaudo() {
        this.reponseJson = dao.cargarComboEntidadRecaudo();
    }

    private void cargarRecaudos() {
        String id_entidad_recaudo = request.getParameter("id_entidad_recaudo") != null ? request.getParameter("id_entidad_recaudo") : "";
        String select_fecha = request.getParameter("select_fecha") != null ? request.getParameter("select_fecha") : "";
        try {
            this.reponseJson = dao.cargarRecaudos(id_entidad_recaudo, select_fecha);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarRecaudoDetalle() {
        String id_archivo = request.getParameter("id_archivo") != null ? request.getParameter("id_archivo") : "";
        try {
            this.reponseJson = dao.cargarRecaudoDetalle(id_archivo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarRecaudadorTercero() {
        this.reponseJson = dao.cargarRecaudadorTercero();
    }

    private void cargarCarteraPorAsignar() {
        String id_unidad_negocio = request.getParameter("id_unidad_negocio") != null ? request.getParameter("id_unidad_negocio") : "";
        String id_entidad_recaudo = request.getParameter("id_entidad_recaudo") != null ? request.getParameter("id_entidad_recaudo") : "";
        this.reponseJson = dao.cargarCarteraPorAsignar(id_unidad_negocio, id_entidad_recaudo);

    }

    private void aprobarCarteraPorAsignar() {
        String resp = "";
        String data = request.getParameter("data");
        String id_accion = request.getParameter("id_accion") != null ? request.getParameter("id_accion") : "";

        JsonParser jsonParser = new JsonParser();
        JsonObject jneg = (JsonObject) jsonParser.parse(data);
        JsonArray jsonArrNeg = jneg.getAsJsonArray("data");
        for (int i = 0; i < jsonArrNeg.size(); i++) {
            String altura_mora_actual = jsonArrNeg.get(i).getAsJsonObject().get("altura_mora_actual").getAsJsonPrimitive().getAsString();
            String lote = jsonArrNeg.get(i).getAsJsonObject().get("lote").getAsJsonPrimitive().getAsString();
            if (id_accion.equals("A")) {

                resp = dao.aprobarCarteraPorAsignar(altura_mora_actual, lote, usuario.getLogin());
            } else {
                resp = dao.rechazarCarteraPorAsignar(altura_mora_actual, lote, usuario.getLogin());
            }
        }

        this.reponseJson = "{\"respuesta\":\"" + resp + "\"}";
    }

    private void cargarComboResponsableCuenta() {
        String sucursal = request.getParameter("sucursal") != null ? request.getParameter("sucursal") : "";
        this.reponseJson = dao.cargarComboResponsableCuenta(sucursal);
    }

    private void cargarComboCiudad() {
        String sucursal = request.getParameter("sucursal") != null ? request.getParameter("sucursal") : "";
        this.reponseJson = dao.cargarComboCiudad(sucursal);
    }

    private void cargarComboBarrio() {
        String id_ciudad = request.getParameter("id_ciudad") != null ? request.getParameter("id_ciudad") : "";
        this.reponseJson = dao.cargarComboBarrio(id_ciudad);
    }

    private void cargarCasasCobranza() {
        this.reponseJson = dao.cargarCasasCobranza();
    }

    private void cambiarEstadoCasaCobranza() {
        String idCasaCobranza = request.getParameter("id") != null ? request.getParameter("id") : "";
        String resp = dao.cambiarEstadoCasaCobranza(idCasaCobranza, usuario);
        this.reponseJson = "{\"respuesta\":\"" + resp + "\"}";
    }

    private void updateCasaCobranza() {
        String resp = "";
        String action = request.getParameter("action") != null ? request.getParameter("action") : "";
        String id_casa = request.getParameter("id_casa") != null ? request.getParameter("id_casa") : "";
        String nit = request.getParameter("nit") != null ? request.getParameter("nit") : "";
        String casa_cobranza = request.getParameter("nombre") != null ? request.getParameter("nombre") : "";
        String direccion = request.getParameter("direccion") != null ? request.getParameter("direccion") : "";
        String telefono = request.getParameter("telefono") != null ? request.getParameter("telefono") : "";
        String nombre_contacto1 = request.getParameter("nombre_contacto1") != null ? request.getParameter("nombre_contacto1") : "";
        String telefono_contacto1 = request.getParameter("telefono_contacto1") != null ? request.getParameter("telefono_contacto1") : "";
        String cargo_contacto1 = request.getParameter("cargo_contacto1") != null ? request.getParameter("cargo_contacto1") : "";
        String email_contacto1 = request.getParameter("email_contacto1") != null ? request.getParameter("email_contacto1") : "";
        String nombre_contacto2 = request.getParameter("nombre_contacto2") != null ? request.getParameter("nombre_contacto2") : "";
        String telefono_contacto2 = request.getParameter("telefono_contacto2") != null ? request.getParameter("telefono_contacto2") : "";
        String cargo_contacto2 = request.getParameter("cargo_contacto2") != null ? request.getParameter("cargo_contacto2") : "";
        String email_contacto2 = request.getParameter("email_contacto2") != null ? request.getParameter("email_contacto2") : "";
        String nombre_contacto3 = request.getParameter("nombre_contacto3") != null ? request.getParameter("nombre_contacto3") : "";
        String telefono_contacto3 = request.getParameter("telefono_contacto3") != null ? request.getParameter("telefono_contacto3") : "";
        String cargo_contacto3 = request.getParameter("cargo_contacto3") != null ? request.getParameter("cargo_contacto3") : "";
        String email_contacto3 = request.getParameter("email_contacto3") != null ? request.getParameter("email_contacto3") : "";

        if (action.equals("update")) {

            resp = dao.updateCasaCobranza(id_casa, nit, casa_cobranza, direccion, telefono, nombre_contacto1, telefono_contacto1, cargo_contacto1,
                    email_contacto1, nombre_contacto2, telefono_contacto2, cargo_contacto2, email_contacto2, nombre_contacto3, telefono_contacto3,
                    cargo_contacto3, email_contacto3, usuario);

        } else if (action.equals("insert")) {

            resp = dao.insertCasaCobranza(id_casa, nit, casa_cobranza, direccion, telefono, nombre_contacto1, telefono_contacto1, cargo_contacto1,
                    email_contacto1, nombre_contacto2, telefono_contacto2, cargo_contacto2, email_contacto2, nombre_contacto3, telefono_contacto3,
                    cargo_contacto3, email_contacto3, usuario);

        }
        this.reponseJson = "{\"respuesta\":\"" + resp + "\"}";
    }

    private void generarCartasCobro() {
       String id_unidad_negocio = request.getParameter("id_unidad_negocio")!= null ? request.getParameter("id_unidad_negocio") : "";
       String id_agencia = request.getParameter("id_agencia")!= null ? request.getParameter("id_agencia") : "";
       String id_altura_mora = request.getParameter("id_altura_mora")!= null ? request.getParameter("id_altura_mora") : "";
       String id_dia = request.getParameter("id_dia")!= null ? request.getParameter("id_dia") : "";
       
       String resp=dao.generarCartasCobro(id_unidad_negocio,id_agencia,id_altura_mora, usuario,id_dia);
       this.reponseJson =  "{\"respuesta\":\"" + resp + "\"}"; 
    }

    private void cargarComboMora() {
        this.reponseJson = dao.cargarComboMora();
    }

    private void getCalcularCuotaInicialApagar() {
        this.reponseJson = "{}";
        String tipo_ref = request.getParameter("id_tipo_refi") != null ? request.getParameter("id_tipo_refi") : "";
        int dias = request.getParameter("id_dias_mora") != null ? Integer.parseInt(request.getParameter("id_dias_mora")) : 0;
        double saldoCapital = request.getParameter("id_saldo_capital") != null ? Double.parseDouble(request.getParameter("id_saldo_capital")) : 0;
        double saldoCapitalAval = request.getParameter("id_saldo_capital_aval") != null ? Double.parseDouble(request.getParameter("id_saldo_capital_aval")) : 0;
        double saldoInteres = request.getParameter("id_saldo_interes") != null ? Double.parseDouble(request.getParameter("id_saldo_interes")) : 0;
        double saldoCat = request.getParameter("id_saldo_cat") != null ? Double.parseDouble(request.getParameter("id_saldo_cat")) : 0;
        double saldoCatVencido = request.getParameter("id_saldo_cat_vencido") != null ? Double.parseDouble(request.getParameter("id_saldo_cat_vencido")) : 0;
        double saldoCuotaAdmin = request.getParameter("id_cuota_admin") != null ? Double.parseDouble(request.getParameter("id_cuota_admin")) : 0;
        double saldoInteresAval = request.getParameter("id_saldo_interes_aval") != null ? Double.parseDouble(request.getParameter("id_saldo_interes_aval")) : 0;
        double saldoIntXmora = request.getParameter("id_saldo_intxmora") != null ? Double.parseDouble(request.getParameter("id_saldo_intxmora")) : 0;
        double saldoGac = request.getParameter("id_saldo_gac") != null ? Double.parseDouble(request.getParameter("id_saldo_gac")) : 0;

        ConceptoFactura conceptoFactura;
        conceptoFactura = new ConceptoFactura(saldoCapital, saldoCapitalAval, saldoInteres, saldoCatVencido, saldoCat, saldoCuotaAdmin, saldoInteresAval, saldoIntXmora, saldoGac, dias);
        this.reponseJson = dao.getCuotaInicialApagar(conceptoFactura, tipo_ref);
    }

    private void imprimirExtrato() {
        double valorInicial = request.getParameter("valor_inicial") != null ? Double.parseDouble(request.getParameter("valor_inicial")) : 0;
        double intXmora = request.getParameter("intxmora") != null ? Double.parseDouble(request.getParameter("intxmora")) : 0;
        double gac = request.getParameter("gac") != null ? Double.parseDouble(request.getParameter("gac")) : 0;
        String key_ref = request.getParameter("key_ref") != null ? request.getParameter("key_ref") : "";

        this.reponseJson = dao.imprimirExtracto(valorInicial, gac, gac, key_ref, usuario);
    }

    private void cargarComboEstrategiaCartera() {
        this.reponseJson = dao.cargarComboEstrategiaCartera();
    }

    private void anularRefinanciacion() {
        String key_ref = request.getParameter("key_ref") != null ? request.getParameter("key_ref") : "";
        String observacion = request.getParameter("observacion") != null ? request.getParameter("observacion") : "";
        this.reponseJson = dao.anularRefinanciacion(key_ref, usuario,observacion);
    }

    private void cargarObservacionSimulacion() {
        String key_ref = request.getParameter("key_ref") != null ? request.getParameter("key_ref") : "";
        this.reponseJson = dao.cargarObservacionSimulacion(key_ref);
    }

}
