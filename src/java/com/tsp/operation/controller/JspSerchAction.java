/********************************************************************
 *      Nombre Clase.................   JspSerchAction.java    
 *      Descripci�n..................   Obtiene un campo en el archivo de campos_jsp.
 *      Autor........................   Ing. Rodrigo Salazar
 *      Fecha........................   18.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Rodrigo
 */
public class JspSerchAction extends Action{
    
    /** Creates a new instance of JspSerchAction */
    public JspSerchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession();        
        String listar = (String) request.getParameter("listar");
        String codigo = (String) request.getParameter("c_codigo");
        String pagina = (String) request.getParameter("c_pagina"); 
        try{                
            if (listar.equals("True")){                
                next="/jsp/trafico/permisos/jsp/JspListar.jsp";                    
                Vector jsps = model.jspService.searchDetalleJsps(codigo, pagina);               
                request.setAttribute("jsps",jsps);
            }
            else{
                model.jspService.serchJsp(codigo);
                Jsp jsp = model.jspService.getJsp();
                request.setAttribute("jsp",jsp);                
                next="/jsp/trafico/permisos/jsp/JspModificar.jsp";
                               
            }                        
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
