/*************************************************************
 * Nombre: ReportePlaneacionViajesAction.java
 * Descripci�n: Accion para generar el reporte de planeaci�n
 *              Viaje-
 * Autor: Ing. Jose de la rosa
 * Fecha: 26 de enero de 2006, 06:16 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;


public class ReportePlaneacionViajesAction extends Action{
    
    /** Creates a new instance of ReportePlaneacionViajesAction */
    public ReportePlaneacionViajesAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        String ano="",anof="",mes="",Nmes="",fmes="";
        String ffin="",fechaini="",fechafin="";
        String fecha = request.getParameter ("c_fecha");
        int a�o = Integer.parseInt ( fecha.substring (0,4) );
        mes = fecha.substring (5,7);
        ano = fecha.substring (0,4);
        String diac = fecha.substring (8,10);
        fmes = String.valueOf ((Integer.parseInt (mes) + 1));
        anof = ano;
        if(mes.equals ("01")){
            ffin="31";
            Nmes="Enero";
        }
        else if(mes.equals ("02")){
            if(a�o%4==0)
                ffin="29";
            else
                ffin="28";
            Nmes="Febrero";
        }
        else if(mes.equals ("03")){
            Nmes="Marzo";
            ffin="31";
        }
        else if(mes.equals ("04")){
            Nmes="Abril";
            ffin="30";
        }
        else if(mes.equals ("05")){
            ffin="31";
            Nmes="Mayo";
        }
        else if(mes.equals ("6")){
            Nmes="Junio";
            ffin="30";
        }
        else if(mes.equals ("07")){
            Nmes="Julio";
            ffin="31";
        }
        else if(mes.equals ("08")){
            ffin="31";
            Nmes="Agosto";
        }
        else if(mes.equals ("09")){
            Nmes="Septiembre";
            ffin="30";
        }
        else if(mes.equals ("10")){
            Nmes="Octubre";
            ffin="31";
        }
        else if(mes.equals ("11")){
            Nmes="Noviembre";
            ffin="30";
        }
        else{
            anof=String.valueOf(a�o+1);
            Nmes="Diciembre";
            ffin="31";
            fmes="01";
        }
        fechaini=ano+"-"+mes+"-01";
        fechafin=anof+"-"+ fmes +"-01";
        ////System.out.println(fechaini+"  "+fechafin);
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        try{
            ReportePlaneacionViajesXLS hilo = new ReportePlaneacionViajesXLS ();
            hilo.start (usuario,Nmes,fechaini,fechafin,ffin,diac,dstrct);
            next = "/jsp/masivo/reportes/reportePlaneacionViajes.jsp?msg=Su reporte ha iniciado y se encuentra en el log de procesos";
        }catch (Exception ex){
            throw new ServletException ("Error en OpcionesExportarPtoAction .....\n"+ex.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
