package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.WSHistCreditoDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.CXP_Doc;
import com.tsp.operation.model.beans.ExtractoDetalleBeans;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.CXPDocService;
import com.tsp.util.Utility;
import java.lang.reflect.Type;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;

/**
 *
 * @author hosorio
 */
public class AdministradorCxpAction extends Action {

    private final int OBTENER_FACTURAS = 1;
    private final int APROBAR_FACTURAS = 2;
    private final int OBTENER_FACTURAS_AUTORIZADAS = 4;
    private final int CONSULTA = 3;
    boolean redirect = true;
    HttpSession session;
    Usuario usuario;
    
    /**
     * Creates a new instance of CXPBuscarFacturas
     */
    public AdministradorCxpAction() {
    }

    @Override
    public void run() throws javax.servlet.ServletException, InformationException {

        session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        try {

            int op = (request.getParameter("op") != null) ? Integer.parseInt(request.getParameter("op").toString()) : -1;
            switch (op) {
                case OBTENER_FACTURAS:
                    this.obtenerFacturas();
                    break;
                case APROBAR_FACTURAS:
                    this.aprobarFacturas();
                    break;
                case OBTENER_FACTURAS_AUTORIZADAS:
                    this.obtenerFacturasAutorizadas();
                    break;
                case CONSULTA:
                    this.realizarConsultas();
                    break;
            }


        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void realizarConsultas() {
        try {
            WSHistCreditoDAO o = new WSHistCreditoDAO(usuario.getBd());
            o.formulario((Usuario) session.getAttribute("Usuario"), "8020220161");
        } catch (Exception e) {
            System.out.println(e.getMessage() + " - " + e.getCause());
        }

    }

    public void obtenerFacturas() throws Exception {

        String fechaInicial = request.getParameter("fechaInicial");
        String fechaFinal = request.getParameter("fechaFinal");
        String agencia = request.getParameter("agencia");

        CXPDocService cxpservice = new CXPDocService(usuario.getBd());
        ArrayList<CXP_Doc> buscar_facturas_no_aprobadas = cxpservice.buscar_facturas_no_aprobadas(usuario.getLogin(), fechaInicial, fechaFinal, agencia);

        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(buscar_facturas_no_aprobadas) + "}";
        this.printlnResponse(json, "application/json;");

    }

    public void aprobarFacturas() throws Exception {
        String resp = "";
        try {
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            int tam = Integer.parseInt(request.getParameter("tam"));
            if (!json.equals("")) {
                Gson gson = new Gson();
                Type tipoListaFacturas;
                tipoListaFacturas = new TypeToken<ArrayList<CXP_Doc>>() {
                }.getType();
                ArrayList<CXP_Doc> listFacturas = gson.fromJson(json, tipoListaFacturas);
                TransaccionService tService = new TransaccionService(this.usuario.getBd());
                tService.crearStatement();

                for (int i = 0; i < listFacturas.size(); i++) {
                    tService.getSt().addBatch(model.cxpDocService.autorizarFacturas(usuario.getLogin(), usuario.getDstrct(), listFacturas.get(i).getProveedor(), listFacturas.get(i).getTipo_documento(), listFacturas.get(i).getDocumento()));
                }

                tService.execute();
                resp = Utility.getIcono(request.getContextPath(), 5) + "Autorizacion de facturas realizado. Se han aprobaron " + tam + " facturas exitosamente.";
            }
        } catch (Exception exception) {
            throw exception;
        }

        this.printlnResponse(resp, "application/text;");
    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
    
    public void  obtenerFacturasAutorizadas() throws Exception{
        String fechaInicial = request.getParameter("fechaInicial");
        String fechaFinal = request.getParameter("fechaFinal");
        String agencia = request.getParameter("agencia");

        CXPDocService cxpservice = new CXPDocService(usuario.getBd());
        ArrayList<CXP_Doc> facturas_autorizadas = cxpservice.buscarFacturasAutorizadas(usuario.getLogin(), fechaInicial, fechaFinal, agencia);

        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(facturas_autorizadas) + "}";
        this.printlnResponse(json, "application/json;");
    }
}
