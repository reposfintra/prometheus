/***************************************************************************
 * Nombre clase : ............... ReporteViajeAction.java                  *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de viajes                        *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 16 de noviembre de 2005, 05:57 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

public class ReporteViajeAction extends Action{
    
    /** Creates a new instance of ReporteViajeAction */
    public ReporteViajeAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String next = "";
            
            String opcion                 = request.getParameter("Opcion");
            String placa                  = (request.getParameter("placa")!=null) ? request.getParameter("placa").toUpperCase() : "";
            String fechai                 = (request.getParameter("finicial")!=null) ? request.getParameter("finicial") : "";
            String fechaf                 = (request.getParameter("ffinal")!=null) ? request.getParameter("ffinal") : "";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            
            String Mensaje = "";
            
            if(opcion.equals("Generar")){
                model.ReporteViajeSvc.listReporteViaje(placa, fechai, fechaf);
                next = "/jsp/masivo/reportes/ReporteViaje.jsp?fechai="+fechai+"&fechaf="+fechaf;
            }
            
            if(opcion.equals("Excel")){
                ReporteViajeThread hilo = new ReporteViajeThread();
                hilo.start(model.ReporteViajeSvc.getListRV(), fechai, fechaf, user.getLogin());
                Mensaje = "El proceso se ha iniciado exitosamente";
                next = "/jsp/masivo/reportes/ReporteViaje.jsp?Mensaje="+Mensaje;
                
            }
                        
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
        }
    }
}
