//Ing. Osvaldo P�rez Ferrer
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.*;
import java.util.*;
import com.tsp.operation.model.threads.*;

public class BalancePruebaPDFAction extends Action{
    
    public BalancePruebaPDFAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try{
            HttpSession session = request.getSession();
        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String Distrito = usuario.getDstrct();
            String anio     = (request.getParameter("anio")!=null)?request.getParameter("anio"):"";
            String auxmes   = (request.getParameter("mes")!=null)?request.getParameter("mes"):"";
            String Mensaje = "El proceso de generacion del balance se ha iniciado, puede consultar su estado en el Log de procesos";
            String next = "";
            int mes = Integer.parseInt(auxmes);
            
            ReporteBalancePrueba hilo   = new ReporteBalancePrueba(usuario.getBd());
            
            hilo.start(usuario, Distrito, mes, anio);
                        
            next = "/jsp/finanzas/reportes/BalancePrueba.jsp?Mensaje="+Mensaje;
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
}
