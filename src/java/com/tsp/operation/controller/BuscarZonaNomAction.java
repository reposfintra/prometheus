/*
 * BuscarZonanomAction.java
 *
 * Created on 3 de marzo de 2005, 02:07 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class BuscarZonaNomAction extends Action {
    
    /** Creates a new instance of BuscarZonanomAction */
    public BuscarZonaNomAction() {
    }
    
    public void run() throws ServletException {
        String next= request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String nombre = (request.getParameter("nombre").toUpperCase())+"%";
        try{ 
            model.zonaService.buscarZonaNombre(nombre);
            Vector VecZonas = model.zonaService.obtZonas();
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }                
        this.dispatchRequest(next);
    }
   
}
