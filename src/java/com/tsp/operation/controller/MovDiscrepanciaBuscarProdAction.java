/***********************************************
 * Nombre clase: MovDiscrepanciaBuscarProd.java
 * Descripci�n: Accion para buscar productos de discrepancia.
 * Autor: Diogenes Bastidas Morales
 * Fecha: 27 de diciembre de 2005, 03:00 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class MovDiscrepanciaBuscarProdAction extends Action {
    
    /** Creates a new instance of MovDiscrepanciaBuscarProd */
    public MovDiscrepanciaBuscarProdAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String cod = request.getParameter("cliente");
        String ubi = request.getParameter("ubicacion");
        String next = "/jsp/cumplidos/movimiento_discrepancia/productosclienteubicacion.jsp?cliente="+cod+"&ubicacion="+ubi;
        try{
            model.discrepanciaService.listProductosCliente(cod, ubi,distrito);
            ////System.out.println(next);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
