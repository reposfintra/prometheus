/*
 * LeerArchivoAction.java
 *
 * Created on 17 de febrero de 2005, 11:30 AM
 */

package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class LeerArchivoAction extends Action{
    
    /** Creates a new instance of LeerArchivoAction */
    public LeerArchivoAction() {
    }
    
    public void run() throws ServletException,InformationException {
        
        String next="/Sincronizacion/Consultas.jsp";
        java.util.Enumeration lista;
        Properties dbProps = new Properties();
        String nombre = request.getParameter("nombre");
       
        File f = new File("/exportar/masivo/Sincronizacion/servidor/");
        Vector list = new Vector();
        File[] vec=f.listFiles();
        for(int i=0; i<vec.length; i++){
            if(vec[i].isFile()){
                list.add(vec[i].getName());
            }
        }
        request.setAttribute("lista", list);
        
        f = new File("/exportar/masivo/Sincronizacion/servidor/recibido/");
        list = new Vector();
        vec=f.listFiles();
        for(int i=0; i<vec.length; i++){
            if(vec[i].isFile())
                list.add(vec[i].getName());
        }
        request.setAttribute("journal", list);
        
        request.setAttribute("archivo",null);
        try{
            
            if(request.getParameter("nombre")!=null){
                
                
                File archivo = new File("/exportar/masivo/Sincronizacion/"+nombre);
                FileInputStream input = new FileInputStream(archivo);
                if (archivo.exists()){
                    dbProps.load(input);
                }
                
                Vector leer = new Vector();
                lista =dbProps.keys();
                
                while (lista.hasMoreElements()) {
                    String key =(String) lista.nextElement();
                    leer.add(key+";"+dbProps.getProperty(key));
                }
                request.setAttribute("archivo",leer);
                
            }
            else if(request.getParameter("archivo")==null){
                
                next="/Sincronizacion/LeerLogRedirect.jsp";
                model.logservice.leerArchivo();
            }
            else{
                String journal = request.getParameter("archivo");
                next="/Sincronizacion/LeerLogRedirect.jsp?journal="+journal;
                model.logservice.leerArchivo(journal);
            
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
