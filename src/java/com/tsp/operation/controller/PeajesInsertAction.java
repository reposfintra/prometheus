/*
 * PeajesInsertAction.java
 *
 * Created on 4 de diciembre de 2004, 10:42 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  KREALES
 */
public class PeajesInsertAction  extends Action {
    
    /** Creates a new instance of PeajesInsertAction */
    public PeajesInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
    
        String tiket= request.getParameter("tiket").toUpperCase();
        request.setAttribute("tiket", "ECE0D8");
        String Mensaje = "El registro ha sido agregado";
        String next = "/peajes/peajeInsert.jsp?Mensaje="+Mensaje;
        int sw=0;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            if(!model.peajeService.exist(tiket)){
                Peajes p= new Peajes();
                p.setDecripcion(request.getParameter("descripcion"));
                p.setDstrct(request.getParameter("distrito"));
                p.setMoneda(request.getParameter("moneda"));
                p.setTiket_id(tiket);
                p.setUser(usuario.getLogin());
                p.setValue(Float.parseFloat(request.getParameter("valor")));
                model.peajeService.insert(p,usuario.getBase());
            }
            else{
                sw=1;
                Mensaje = "El registro ya se encuentra registrado";
                request.setAttribute("tiket", "#cc0000");
            }
            if(sw==1)
                next = "/peajes/peajeInsertError.jsp?Mensaje="+Mensaje;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
