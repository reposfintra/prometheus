/***********************************************
 * Nombre BancoAnularAction.java
 * Descripci�n: Accion para anular un banco.
 * Autor: Ing. Jose de la rosa
 * Fecha: 28 de septiembre de 2005, 11:55 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ***********************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Jose
 */
public class BancoAnularAction extends Action {
    
    /** Creates a new instance of BancoAnularAction */
    public BancoAnularAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession ();
        String distrito = request.getParameter ("c_distrito").toUpperCase ();
        String banco = (request.getParameter ("c_banco").toUpperCase ());
        String sucursal = (request.getParameter ("c_sucursal").toUpperCase ());
        String cuenta = (request.getParameter ("c_cuenta").toUpperCase ());
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        try {
            Banco b = new Banco ();
            b.setBanco (banco);
            b.setDstrct (distrito);
            b.setCuenta (cuenta);
            b.setBank_account_no (sucursal);
            b.setUsuario_modificacion (usuario.getLogin ());
            model.servicioBanco.anularBanco (b);
            request.setAttribute ("msg","banco Anulado");
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
