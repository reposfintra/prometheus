/***************************************
 * Nombre Clase ............. TransferenciaCorridaAction.java
 * Descripci�n  .. . . . . .  maneja eventos para realizar pago por transferencia
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  02/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;



public class TransferenciaCorridaAction extends Action{
    
    
    
    private  FileOutputStream       fw;
    private  BufferedWriter   bf;
    private  PrintWriter      linea;
    private  Usuario          usuario;
    
    
    // Soporte:
    private  FileOutputStream       fwSoporte;
    private  BufferedWriter   bfSoporte;
    private  PrintWriter      lineaSoporte;
    
    
    

    
    
    
    
    public void run() throws ServletException, InformationException {
        
        try{
            
            HttpSession session = request.getSession();
            this.usuario        = (Usuario) session.getAttribute("Usuario");
            
            
            String   next          = "/jsp/cxpagar/transferencias/seleccionFacturas.jsp";
            String   msj           = "";
            String   evento        = request.getParameter("evento");    
            
            
            
            if(evento!=null){        
                
                
            
                   if(evento.equals("BuscarS")) {
                        String banco   = request.getParameter("banco").toUpperCase() ;
                        model.servicioBanco.obtenerSucursalesBanco(banco);
                        next = "/jsp/cxpagar/transferencias/FiltroCorridaTransferencia.jsp";
                   }
                    
                   if(evento.equals("BuscarSTR")) {
                        String banco   = request.getParameter("bancoTR").toUpperCase() ;
                        model.TransferenciaSvc.setBancoTR   ( banco   );
                        model.servicioBanco.obtenerSucursalesBanco(banco);
                   }
                   
                   
                   else if( evento.equals("FILTER") ){
                        
                        String banco    = request.getParameter("banco").toUpperCase() ;
                        String suc      = request.getParameter("sucursal").toUpperCase() ;            
                        String corrida  = request.getParameter("corrida");
                        
                        model.TransferenciaSvc.reset();
                        model.TransferenciaSvc.setCorrida   ( corrida );
                        model.TransferenciaSvc.setBanco     ( banco   );
                        model.TransferenciaSvc.setSucursal  ( suc     );                        
                        model.TransferenciaSvc.setBancoTR   ( banco   );
                        model.TransferenciaSvc.setSucursalTR( suc     );
                        
                        String monedaBanco =  model.TransferenciaSvc.getMonedaBanco(usuario.getDstrct(), banco, suc);
                        model.TransferenciaSvc.setMonedaBanco(  monedaBanco  );
                        
                        model.TransferenciaSvc.buscarFacturasAprobadasFiltroMod(usuario.getDstrct(), usuario.getId_agencia(), banco, suc);
                        
                    }
                   
                   
                    else if ( evento.equals("FORMAR") ){
                        String   bancoTR     = request.getParameter("bancoTR").toUpperCase() ;
                        String   sucursalTR  = request.getParameter("sucursalTR").toUpperCase() ;

                     // Verificamos que tenga formato
                        if( model.TransferenciaSvc.existeFormato(bancoTR)  ){
                            
                            
                            // Serie del banco
                               model.TransferenciaSvc.getSeries(usuario.getDstrct(), bancoTR, sucursalTR, usuario.getId_agencia(), usuario.getLogin() );
                               if(  model.TransferenciaSvc.getSerie() != null  ){                               

                                    if( model.TransferenciaSvc.existeRutinaFormato( bancoTR ) ) {
                                   
                                         // Bancos por transferir
                                            model.TransferenciaSvc.setBancoTR   (  bancoTR    );
                                            model.TransferenciaSvc.setSucursalTR(  sucursalTR );

                                            String monedaTR =  model.TransferenciaSvc.getMonedaBanco(usuario.getDstrct(), bancoTR , sucursalTR );
                                            model.TransferenciaSvc.setMonedaBancoTR( monedaTR );                       

                                            model.TransferenciaSvc.loadSeleccion();
                                            
                                         // Vista Previa:
                                            msj =  model.TransferenciaSvc.formarGrupos(bancoTR);   

                                            next          = "/jsp/cxpagar/transferencias/viewPrevia.jsp";
                                            
                                            
                                    }else
                                         msj = "No hay rutina definida para el formato del banco " + bancoTR ; 
                                  
                               }else
                                   msj = "El banco " + bancoTR  + " - " + sucursalTR + " no presenta serie de cheque definida para pago proveedor"; 
                               
                               

                        }else
                            msj = "El banco " + bancoTR + " no presenta formato definido para pagos por transferencias";


                    }
                   
                   
                    else if ( evento.equals("RESET") ){
                        model.TransferenciaSvc.buscarFacturasAprobadasFiltroMod(usuario.getDstrct(), usuario.getId_agencia(), model.TransferenciaSvc.getBanco(), model.TransferenciaSvc.getSucursal());                    
                    }

                   
                    else if ( evento.equals("MIGRAR") ){//Mod TMolina 2008-08-28

                        try{
                            List listaMigracion   = model.TransferenciaSvc.getListMigracion();

                            if(listaMigracion.size()>0){
                                
                             // Nombre de los archivos:
                                String nameFile = model.TransferenciaSvc.getBancoTR() + model.TransferenciaSvc.getSucursalTR() + model.TransferenciaSvc.getCorrida()+"_"+this.usuario.getBd() ;
                                
                                 if(model.TransferenciaSvc.existeFormato(model.TransferenciaSvc.getBancoTR())){
                                     
                                     List listaMigrar = model.TransferenciaSvc.getListMigracion();
                                     int  contError   = 0;
                                     
                                     initTxt(nameFile); //Mod TMOLINA 2008-08-30
                                     if( model.TransferenciaSvc.getBancoTR().equals("BANCOLOMBIA" )){
                                        model.TransferenciaSvc.formatearLongitud();          
                                       
                                        double sum=0;
                                            for(int i=0;i<listaMigrar.size();i++){
                                                    Hashtable  tran = (Hashtable)listaMigrar.get(i);
                                                    ArchivoMovimiento am = (ArchivoMovimiento)tran.get("Registro"); 
                                                    sum=sum+Double.valueOf(am.getValor()).doubleValue();
                                        }

                                        ArchivoMovimiento amov = new ArchivoMovimiento();
                                   
                                        amov.setTipoCuentaOrigen(model.TransferenciaSvc.getSucursalTR());
                                        String secuenciaPAB = model.AnticiposPagosTercerosSvc.obtenerSecuenciaBanco("7B", usuario.getLogin());
                                        linea.println(amov.head(listaMigrar.size(),"0",sum, secuenciaPAB,model).replaceAll("&nbsp", " ")); 
                                     
                                     }else if (model.TransferenciaSvc.getBancoTR().equals("BBVA" )){    
                                         model.TransferenciaSvc.formatearLongitudBBVA();
                                     }
                                    
                                     //initTxt(nameFile);

                                    // Recorremos el listado:

                                        for(int i=0;i<listaMigrar.size();i++){
                                                Hashtable  tran = (Hashtable)listaMigrar.get(i);
                                                try{

                                                // Actualizamos la BD:
                                                        String[] sql      = model.TransferenciaSvc.updateRegistros( tran, this.usuario.getLogin() ).split(";");
                                                        TransaccionService  tService = new TransaccionService(this.usuario.getBd());
                                                        tService.crearStatement();
                                                        for (int j = 0; j < sql.length; j++) {
                                                          tService.getSt().addBatch(sql[j]);                                                        
                                                        }                                                       
                                                        tService.execute();     

                                                if( model.TransferenciaSvc.getBancoTR().equals("BANCOLOMBIA" )){
                                                   // Archivo transferencia:
                                                   ArchivoMovimiento am = (ArchivoMovimiento)tran.get("Registro");                                         
                                                   String   dato        = am.getColumnas().replaceAll("&nbsp", " ");                                           
                                                   linea.println( dato );
                                                }else if (model.TransferenciaSvc.getBancoTR().equals("BBVA" )){
                                                    ArchivoMovimiento am = (ArchivoMovimiento)tran.get("Registro");
                                                    String   dato        = am.getColumnasBBVA().replaceAll("&nbsp", " ");
                                                    linea.println( dato );
                                                }


                                                }catch(Exception kk){
                                                        kk.printStackTrace();
                                                        contError ++;
                                                }                                            
                                        }
                                                                           
                                        // Archivo Soporte:                                       

                                            lineaSoporte.println( this.usuario.getBd().toUpperCase() );
                                            lineaSoporte.println( "DETALLE TRANSFERENCIA : " + model.TransferenciaSvc.getTransferencia()  );
                                            lineaSoporte.println( "BANCO SUCURSAL        : " + model.TransferenciaSvc.getBancoTR() + " " + model.TransferenciaSvc.getSucursalTR() + " " + model.TransferenciaSvc.getCorrida()  );                                        
                                            lineaSoporte.println( "-------------------------------------------------------------------------------------------------------------"  );
                                            lineaSoporte.println( " CHEQUE       BANCO               CUENTA      TIPO    NOMBRE CTA            NIT CTA              VALOR       "  );

                                            List  ListTransf     =  model.TransferenciaSvc.registrosTransferencias( usuario.getDstrct(), model.TransferenciaSvc.getBancoTR(), model.TransferenciaSvc.getSucursalTR(), model.TransferenciaSvc.getTransferencia() );                                       
                                            for(int i=0;i<ListTransf.size();i++){
                                                    Hashtable  detalle = (Hashtable) ListTransf.get(i);
                                                    writeSoporte(detalle);
                                            }

                                            if( contError > 0 )
                                               lineaSoporte.println( " ERRORES AL GRABAR CHEQUE: " +  contError  );

                                            save();

                                            msj = "El archivo de Migraci�n para el banco "+  model.TransferenciaSvc.getBancoTR() +" - "+ model.TransferenciaSvc.getSucursalTR()   +" ha sido generado....";
                                            model.TransferenciaSvc.buscarFacturasAprobadasFiltroMod(usuario.getDstrct(), usuario.getId_agencia(), model.TransferenciaSvc.getBanco(), model.TransferenciaSvc.getSucursal());                    
   
                                      
                                        
                                 }else
                                     msj = "No hay formato para generar archivo txt del banco " +  model.TransferenciaSvc.getBancoTR()  ;
                                
                                
                                
                            }else
                                msj = "No hay registros para migrar....";


                        }catch(Exception x){
                            x.printStackTrace();
                            msj    = "No se pudo generar el archivo de migraci�n :" + x.getMessage() ;
                            next   = "/jsp/cxpagar/transferencias/viewPrevia.jsp";
                        }

                    }//END Migrar

                    
                    

                    String puede_seleccionar = usuario.getId_agencia().equals("OP")? "S":"N";
                    request.setAttribute("puede_seleccionar" , puede_seleccionar );
            
                    
                    
            }  
            
            
            next+= "?msj="+ msj;
            this.dispatchRequest(next);
            
            
            
        } catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * M�todo que crea el archivo de soporte
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void writeSoporte( Hashtable  tran  )throws Exception{
        try{
            
            String  SPACE = " ";
            String  IZQ   = "I";
            String  DER   = "R";
            
            String  cheque            = rellenar( (String)tran.get("cheque")        , SPACE ,  8, DER );
            String  banco             = rellenar( (String)tran.get("banco_cuenta")  , SPACE , 20, DER );
            String  noCta             = rellenar( (String)tran.get("no_cuenta")     , SPACE , 15, IZQ );
            String  tipoCta           = rellenar( (String)tran.get("tipo_cuenta")   , SPACE , 3,  IZQ );
            
            String  nombreCta         = rellenar( (String)tran.get("nombre_cuenta") , SPACE , 22, DER );
            String  nitCta            = rellenar( (String)tran.get("cedula_cuenta") , SPACE , 15, IZQ );            
            String  valor             = rellenar(  Util.customFormat(Double.parseDouble((String)tran.get("valor")) )    , SPACE , 20, IZQ );
            
            
            lineaSoporte.println( cheque + SPACE + banco + SPACE + noCta + SPACE + tipoCta + SPACE + nombreCta + SPACE + nitCta + SPACE +  valor );
            
            
            
        }catch(Exception e){
            throw new Exception( " writeSoporte " +e.getMessage() );
        }
    }
    
    
    
    
    
    
  /**
     * M�todo que inicializa los archivos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void initTxt(String nameFile)throws Exception{
        try{
            String ext = ".txt";
            model.DirectorioSvc.create( this.usuario.getLogin() );
            String directorio    =  model.DirectorioSvc.getUrl() + this.usuario.getLogin() + "/";
            String fecha         =  Util.getFechaActual_String(6).replaceAll("/|:| ","");
            
            String filename = "TR_"      + nameFile + ext;
            String soporte  = "SOPORTE_" + nameFile + ext;
            
            
            // Archivo Migraci�n banco:
            String ruta     = directorio + filename;
            this.fw         = new FileOutputStream    (ruta   );
            this.bf         = new BufferedWriter( new OutputStreamWriter(fw,"ISO-8859-1" ) );
            this.linea      = new PrintWriter   (this.bf);
            
            
            // Archivo soporte:
            ruta               = directorio + soporte;
            this.fwSoporte     = new FileOutputStream    (ruta   );
            this.bfSoporte     = new BufferedWriter( new OutputStreamWriter(fwSoporte,"ISO-8859-1" ) );
            this.lineaSoporte  = new PrintWriter   (this.bfSoporte);
            
            
        }catch(Exception e){
            throw new Exception(e.getMessage() );
        }
    }

    
    
    
    
    /**
     * M�todo que cierra los archivos generados
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void save(){
        this.linea.close();
        this.lineaSoporte.close();
    }
    
    
    
    
    
    
    
    /**
     * M�todo que rellena los campos
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public static String rellenar(String cadena, String caracter, int tope, String posicion)throws Exception{
        try{
            int lon = cadena.length();
            if(tope>lon){
                for(int i=lon;i<tope;i++){
                    if(posicion.equals("R"))    cadena += caracter;
                    else                        cadena  = caracter + cadena;
                }
            }
            else
                cadena = Utility.Trunc(cadena, tope );
            
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return cadena;
    }
    
}