/********************************************************************
 *      Nombre Clase.................   CumplidoDocScanAction.java    
 *      Descripci�n..................   Reporte de documentos escaneados de cumplido
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   06.05.2006
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;

/**
 *
 * @author  Andr�s Maturana
 */
public class CumplidoDocScanAction extends Action{
    
    /** Creates a new instance of AgregarDemoraPlanillaAction */
    public CumplidoDocScanAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String codcli = request.getParameter("clientes");
        String fechai = request.getParameter("FechaI");
        String fechaf = request.getParameter("FechaF");
        
        String next = "/jsp/cumplidos/reportes/RepDocScanLista.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            model.cumplido_docService.documentosCumplidos(fechai, fechaf);
            Vector info =  model.cumplido_docService.getCumplido_docs();
            Vector info2 = new Vector();
            
            ////System.out.println("................. SE ECONCOTRARON: " + info.size());
            if( codcli.length()>1 ){
                for (int i = 0; i < info.size(); i++){
                    Hashtable ht = (Hashtable) info.elementAt(i);
                    String cli = ht.get("codcli").toString();
                    
                    if( codcli.compareTo(cli)==0 ){
                        info2.add(ht);
                    }
                }
            } else {
                String usu_dest = model.cumplido_docService.clientesAsignados(usuario.getLogin());
                StringTokenizer st = new StringTokenizer(usu_dest,"#");
                Vector clis = new Vector();
                
                while( st.hasMoreTokens()){
                    String codcl = st.nextToken();
                    clis.add(codcl);
                }
                
                for (int i = 0; i < info.size(); i++){
                    Hashtable ht = (Hashtable) info.elementAt(i);
                    String cli = ht.get("codcli").toString();
                    
                    for( int j=0; j<clis.size(); j++){
                        String cc = (String) clis.elementAt(j);
                        if( cc.compareTo(cli)==0 ){
                            info2.add(ht);
                        }
                    }
                }
                
            }
 
            model.cumplido_docService.setCumplido_docs(info2);
           
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
