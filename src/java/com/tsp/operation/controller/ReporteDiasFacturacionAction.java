/*
 * ReporteDiasFacturacionAction.java
 *
 * Created on 23 de mayo de 2007, 04:16 PM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;


public class ReporteDiasFacturacionAction extends Action {
    //2009-09-02
    /** Creates a new instance of ReporteDiasFacturacionAction */
    public ReporteDiasFacturacionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            
            String next = "/jsp/masivo/reportes/DiasFacturacion/formulario.jsp";
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            
            String dstrct = (String)  session.getAttribute("Distrito");
            String fechai = request.getParameter("fechaini");
            String fechaf = request.getParameter("fechafin");
            String opcion = request.getParameter("opcion");
            
            
            if (opcion!=null && opcion.equalsIgnoreCase("Generar")){
               /* HReportesDiasFacturacion h= new HReportesDiasFacturacion();
                h.start(model, dstrct, fechai, fechaf ,usuario);*/
                request.setAttribute("msg","El proceso de generacion del reporte ha iniciado con exito.");
            }
            
            this.dispatchRequest(next);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        
    }
    
}
