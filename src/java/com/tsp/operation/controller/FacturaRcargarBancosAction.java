
/********************************************************************
 *      Nombre Clase.................   FacturaRcargarBancosAction.java
 *      Descripci�n..................   Action que se encarga de generar un vector con los resgitros de los bancos
 *      Autor........................   David Lamadrid
 *      Fecha........................   31 de octubre de 2005, 06:36 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaRcargarBancosAction extends Action
{
        
        /** Creates a new instance of FacturaRCargarBancos */
        public FacturaRcargarBancosAction ()
        {
        }
        
        public void run () throws ServletException, InformationException
        {
                try
                {
                        String proveedores =request.getParameter ("c_proveedores");
                        String nombre=request.getParameter ("c_nombre");
                        String proveedor=proveedores;
                        if(proveedores != null)
                        {
                        }
                        else
                        {
                                //System.out.println("proveedores es null");
                        }
                        String banco =""+ request.getParameter ("c_banco");
                        String next = "/jsp/cxpagar/facturasxpagar/reporteDXP11.jsp?c_banco="+banco+"&proveedores="+proveedor+"&c_nombre="+nombre;
                        //System.out.println("NEXT "+next);
                        this.dispatchRequest (next);
                        
                }
                catch(Exception e)
                {
                        throw new ServletException ("Accion:"+ e.getMessage ());
                }
        }
        
}
