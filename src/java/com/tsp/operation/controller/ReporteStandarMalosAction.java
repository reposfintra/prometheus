/*************************************************************
 * Nombre: ReporteStandarMalosAction.java
 * Descripci�n: Accion para generar el reporte de standares inconcistentes
 * Autor: Ing. Jose de la rosa
 * Fecha: 5 de abril de 2006, 05:34 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;

public class ReporteStandarMalosAction extends Action{
    
    /** Creates a new instance of ReporteStandarMalosAction */
    public ReporteStandarMalosAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        
        try{
            ReporteStandarMalosXLS hilo = new ReporteStandarMalosXLS ();
            hilo.start (usuario.getLogin ().toUpperCase ());
            next = "/jsp/masivo/reportes/reporteStandaresInconsistentes.jsp?msg=Su reporte ha iniciado y se encuentra en el log de procesos";
        }catch (Exception ex){
            throw new ServletException ("Error en OpcionesExportarPtoAction .....\n"+ex.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
