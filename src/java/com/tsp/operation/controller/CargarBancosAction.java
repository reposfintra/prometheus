/******************************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Ing. fily steven fernandez
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

import org.apache.log4j.*;
/**
 *
 * @author  Administrador
 */
public class CargarBancosAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ReporteEgresoAction */
    public CargarBancosAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String agencia = request.getParameter("agenciaFact");
            String banco = request.getParameter("BanConsignacion");
            String sucursal = request.getParameter("SucBanco");
            String op = request.getParameter("op");
            String ops = request.getParameter("ops");
            String agDue�a = request.getParameter("agencia");
            String Pais = request.getParameter("Pais");
            String PaisEnv = request.getParameter("PaisEnv");
            
            
            
            //funcion para cargar los soportes
          
          
            //System.out.println("agencia  "+ agencia);
            String next= "";
            logger.info("AGENCIA: " + agencia);
            logger.info("BANCO: " + banco);
            logger.info("SUCURSAL: " + sucursal);
            
            request.setAttribute("agencia", agencia );
            request.setAttribute("banco", banco );
            request.setAttribute("sucursal", sucursal );
            if (op.equals("2")) {
                next = "/jsp/sot/body/ModificarCliente.jsp";
            }
            if (op.equals("1")) {
                next = "/jsp/sot/body/AdicionarCliente.jsp";
                 
                String [] soportes  = request.getParameterValues("Soporte");
                if(soportes == null){
                    System.out.println("el soporte es null");
                }else{
                    model.clienteService.cargarSoporte(soportes);
                }    
            }
                
                HttpSession session = request.getSession();
                
                String target =  request.getParameter("target")!=null ? request.getParameter("target") : "";
                
                logger.info("TARGET: " + target);
                //String agc = request.getParameter("agc")!=null ? request.getParameter("agc") : "";
                
                //logger.info("AGC: " + agc);
                
                List ListaAgencias = model.clienteService.listarAgecniaFacturacion();
                TreeMap tm = new TreeMap();
                if(ListaAgencias.size()>0) {
                    Iterator It3 = ListaAgencias.iterator();
                    while(It3.hasNext()) {
                        Ciudad  datos2 =  (Ciudad) It3.next();
                        tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                    }
                }
                request.setAttribute("agencias", ListaAgencias);
                request.setAttribute("agencias_tm", tm);
                if(agencia.equals("OP")){
                    agencia = "";
                }
                
                if( target.length()!=0 && target.compareTo("bancos")==0 ){
                    model.servicioBanco.loadBancos(agencia, (String) session.getAttribute("Distrito"));
                    model.servicioBanco.setSucursal(new TreeMap());
                } else if( target.length()!=0 && target.compareTo("sucursales")==0 ){
                    model.servicioBanco.loadSucursalesAgencia(agencia, banco, (String) session.getAttribute("Distrito"));
                    TreeMap SucBanco = model.servicioBanco.getSucursal();
                }
            String cedula = "";
             model.clienteService.BusquedaCedCLiente(agDue�a);
             model.ciudadService.loadCiudadesC(Pais);
             model.ciudadService.loadCiudadesCE(PaisEnv);
    
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en CargarDaTosCombo .....\n"+e.getMessage());
        }
    }
    
}
