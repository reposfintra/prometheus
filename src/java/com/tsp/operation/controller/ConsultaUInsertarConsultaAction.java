/******************************************************************
 * Nombre ......................ConsultaUConsultaAction.java
 * Descripci�n..................Clase Action para insertar una consulta
 * Autor........................David lamadrid
 * Fecha........................21/12/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUInsertarConsultaAction extends Action {
    
    /** Creates a new instance of ConsultaUInsertarConsultaAction */
    public ConsultaUInsertarConsultaAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        try {
            
            String tabla=""+request.getParameter ("c_tabla");
            String nombre_tabla=""+request.getParameter ("c_tabla");
            String from =""+request.getParameter ("from");
            String select =""+request.getParameter ("select");
            String otros =""+request.getParameter ("otros");
            String where =""+request.getParameter ("where");
            String descripcion=""+request.getParameter ("descripcion");
            //System.out.println("where "+where);
            //Vector vSelect =new Vector ();
            
            String[] campos = request.getParameterValues ("cselect");
            String[] cfrom = request.getParameterValues ("cfrom");

            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String userlogin=""+usuario.getLogin ();
            String distrito=""+ usuario.getDstrct ();
            String base =""+usuario.getBase ();
            
            ConsultaUsuarios consulta = new ConsultaUsuarios ();
            consulta.setCfrom (from);
            consulta.setCotros (otros);
            consulta.setCreation_user (userlogin);
            consulta.setCselect (select);
            consulta.setCwhere (where);
            consulta.setDescripcion (descripcion);
            consulta.setDstrct (distrito);
            consulta.setBase (base);
            model.consultaUsuarioService.setConsulta (consulta);
            
            //System.out.println("WHERE ANTES DE LA VALIDACION 1"+where+"2 "+where.indexOf ("where")+" 3 "+ where.indexOf ("WHERE") );
            if(!((where.indexOf ("where")>=0 ||where.indexOf ("WHERE")>=0)) && where.length ()>0) {
                where= "where "+where;
            }
            //System.out.println("WHERE DESPUUESES DE LA VALIDACION 1"+where);
            
            String cadena =select +" " +  from +" "+ where + " " +otros;
            //System.out.println("consulta "+consulta);
            String ms="";
            //System.out.println("CADENA 1"+cadena);
            boolean valido= model.consultaUsuarioService.existeConsulta (cadena);
            
            if(valido == true) {
                model.consultaUsuarioService.insertar ();
                ms ="Consulta Insertada";
            }
            else{
                ms="La Consulta no esta bien Construida";
            }
        /*    
            if(campos!=null) {
                for( int i=0 ;i<campos.length;i++) {
                    vSelect.add (""+campos[i]);
                }
                model.consultaUsuarioService.setVSelect (vSelect);
            }
            else{
                model.consultaUsuarioService.setVSelect (null);
            }
            
            Vector vFrom =new Vector ();*/
/*
            for(int i=1;i<=numeroFrom;i++) {
                String fromC= ""+request.getParameter ("from"+i);
                vFrom.add (fromC);
            }*/
            /*
            if(model.consultaUsuarioService.existeFromI (tabla)==false) {
                vFrom.add (tabla);
            }
            /*
            if(numeroFrom==0 && model.consultaUsuarioService.existeFromI (tabla)==true) {
                vFrom.add (tabla);
            }*/
         /*   
            model.consultaUsuarioService.setVFrom (vFrom);
            
            if(from.equals ("")) {
                from = "from "+tabla+" as "+tabla;
            }
            else {
                if(from.indexOf (tabla)<0) {
                    from =from+","+tabla+" as "+tabla;
                }
            }*/
          /*  
            //System.out.println("bandera 1");
            boolean sw=false;
            sw= model.consultaUsuarioService.verificarCampos (tabla);
            if(sw==false) {
                model.consultaUsuarioService.obtenerCampos (tabla);
                //System.out.println("entra en obtener campo");
            }
            //System.out.println("bandera 2");*/
            
             model.consultaUsuarioService.tConfiguracionesPorUsuario(userlogin);
            if(cfrom != null){
                model.consultaUsuarioService.validarFrom (cfrom,from);
            }
            
              //seteo un Vector vFrom con los Campos de la Tabla
            model.consultaUsuarioService.obtenerCampos (tabla);
            model.consultaUsuarioService.adicionarTabla (tabla);
            from = model.consultaUsuarioService.getFrom ();
            
            //System.out.println("bandera 3");
            //System.out.println("CAMPOSSSSS "+campos);
            if(campos!=null){
                Vector vSelect=new Vector ();
                for( int i=0 ;i<campos.length;i++){
                    vSelect.add (""+campos[i]);
                }
                model.consultaUsuarioService.setVSelect (vSelect);
            }
            else{
                model.consultaUsuarioService.setVSelect (null);
            }
            
            next="/jsp/general/consultas/insertarconsultas.jsp?select="+select+"&from="+from+"&where="+where+"&otros="+otros+"&accion=&ms="+ms;
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
