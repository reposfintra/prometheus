/*******************************************************************
 * Nombre clase: ColpapelInsertAction.java
 * Descripci�n: Accion para ingresar un despacho
 * Autor: Ing. Karen reales
 * Fecha: Marzo del 2005
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ColpapelInsertAction extends Action{
    static Logger logger = Logger.getLogger(ColpapelInsertAction.class);
        //2009-09-02
    /** Creates a new instance of ColpapelInsertAction */
    public ColpapelInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        logger.info("********************************************************************************");
        logger.info("*********************N U E V O   D E S P A C H O*************************");
        
        String next="/colpapel/preliminar.jsp";
        float pesoMax=0;
        double valorU=0;
        float pmax=0;
        float anticipo=!request.getParameter("anticipo").equals("")?Float.parseFloat(request.getParameter("anticipo")):0;
        float pesoreal=!request.getParameter("toneladas").equals("")?Float.parseFloat(request.getParameter("toneladas")):0;
        String placa = request.getParameter("placa").toUpperCase();
        String cedula= request.getParameter("conductor");
        String remitentes[]= request.getParameter("remitentes").split(",");
        String destinatarios[]= request.getParameter("destinatarios").split(",");
        String destinatario = request.getParameter("destinatarios").length()>40? request.getParameter("destinatarios").substring(0,40):request.getParameter("destinatarios");
        String remitente=request.getParameter("remitentes");
        String origen="";
        String destino="";
        String unit_of_work="";
        String moneda2=request.getParameter("moneda");
        String nit="";
        String numpla="";
        String numre=request.getParameter("remesa");
        String nombre="";
        String cf_code="";
        String descripcion="";
        String docinterno=request.getParameter("docinterno")!=null?request.getParameter("docinterno"):"";
        float costoRem=0;
        float unit_cost=0;
        String grupo="";
        String unit_transp="";
        String sj = request.getParameter("standard");
        String faccial=request.getParameter("fcial");
        String banco_cuenta =request.getParameter("banco");
        float cantreal = 0;
        String nfacturable =request.getParameter("facturable")!=null?"S":"N";
        float cfacturar =0;
        String cdocking = request.getParameter("cdock")!=null?"Y":"N";
        String facturable = request.getParameter("facturable")!=null?"N":"S";
        String monedaCia="PES";
        String fecha =request.getParameter("fechadesp").length()>10? request.getParameter("fechadesp").substring(0,10):"2006-01-01";
        String contenedores= request.getParameter("c1");
        contenedores = !contenedores.equals("")?contenedores+",":contenedores;
        contenedores = contenedores+ request.getParameter("c2");
        String solicitud = request.getParameter("solicitud");
        String cp1=" ";
        String cp2=" ";
        cp1 = request.getParameter("c1precinto")==null?"":request.getParameter("c1precinto");
        cp2 = request.getParameter("c2precinto")==null?"": request.getParameter("c2precinto");
        float vlrMercancia =!request.getParameter("vlrmercan").equals("")?Float.parseFloat(request.getParameter("vlrmercan")):0;
        String proveedor="no encontro";
        boolean errorUnit = false;
        String causa ="";
        int sw = 0;
        String moneda ="";
        float tarifa = 0;
        boolean vacio = false;
        boolean justificar=false;
        boolean mayorLimite=false;
        float viejoFlete = 0;
        double fletePes = 0;
        float valAlquiler=0;
        float ajusteAlquiler=0;
        String mondedaFlete="";
        String celular = "";
        String precintos =cp1+","+cp2+",";
        String banvec[] = banco_cuenta.split("/");
        String banco=banvec[0];
        String cuenta=banvec[1];
        String sticker = request.getParameter("sticker")!=null?request.getParameter("sticker"):"";
        logger.info("Fecha de despacho "+request.getParameter("fechadesp"));
        int hora=request.getParameter("fechadesp").length()>=13?Integer.parseInt(request.getParameter("fechadesp").substring(11,13)):23;
        String minuto=request.getParameter("fechadesp").substring(14,16);
        String  account_code_i="";
        String  account_code_c="";
        String inventario = "a".equals(request.getParameter("inventarios"))?"":request.getParameter("inventarios");
        String wo_type = "NA";
        String aduana = "N";
        String ocarga="";
        String pagador="";
        String tipoCarga = "";
        /*if(request.getParameter("fechadesp").substring(16,18).equalsIgnoreCase("pm")){
            if(hora<12)
                hora=hora+12;
        }*/
        fecha= fecha + " "+hora+":"+minuto+":00";
        
        if(request.getParameter("cfacturar")!=null){
            cfacturar =!request.getParameter("cfacturar").equals("")? Float.parseFloat(request.getParameter("cfacturar")):0;
        }
        if(request.getParameter("cantreal")!=null){
            cantreal = !request.getParameter("cantreal").equals("")?Float.parseFloat(request.getParameter("cantreal")):0;
        }
        String cadena = request.getParameter("cadena")!=null?"S":"N";
        
        Planilla pla= new Planilla();
        Remesa rem = new Remesa();
        RemPla rempla= new RemPla();
        Precinto p = new Precinto();
        AutorizacionFlete aflete = new AutorizacionFlete();
        
        
        //VECTORES PARA INSERTAR EN DESPACHOS.
        Vector prencintos = new Vector();
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        
        p.setNumpla(numpla);
        p.setAgencia(usuario.getId_agencia());
        p.setTipo_Documento("Precinto");
        
        
        
        try{
            
            
            float limite = model.ciaService.maxValorMercancia(usuario.getDstrct());
            logger.info("Valor mercancia "+vlrMercancia);
            logger.info("Valor limite "+limite);
            if(vlrMercancia>limite){
                mayorLimite = true;
            }
            
            //SE GENERA EL NUMERO DE LA REMESA..
            if(numre==null){
                sw=1;
                model.seriesService.obtenerSerie( "002", usuario);
                logger.info("ENCONTRE LA SERIE DE REMESAS");
                if(model.seriesService.getSerie()!=null){
                    Series serie = model.seriesService.getSerie();
                    model.seriesService.asignarSeries(serie,usuario);
                    numre=serie.getPrefix()+serie.getLast_number();
                    
                }
                
            }
            
            //SE GENERA EL NUMERO DE LA PLANILLA..
            model.seriesService.obtenerSerie( "001", usuario);
            logger.info("ENCONTRE LA SERIE DE PLANILLAS");
            if(model.seriesService.getSerie()!=null){
                Series serie = model.seriesService.getSerie();
                model.seriesService.asignarSeries(serie,usuario);
                numpla=serie.getPrefix()+serie.getLast_number();
            }
            if(numpla==null){
                numpla="";
            }
            if(numre==null){
                numre="";
            }
            int sw1=0;
            
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames();
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")){
                    String nombrep  = parametro;
                    String valor = request.getParameter(parametro);
                    java.util.Enumeration enum2;
                    enum2 = request.getParameterNames();
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")
                        && !nombrep.equals(parametro) && request.getParameter(parametro).equals(valor)){
                            String numero = parametro.substring(9);
                            String numero1 = nombre.substring(9);
                            request.setAttribute("p"+numero,"filaroja");
                            request.setAttribute("p"+numero1,"filaroja");
                            sw1=1;
                            next="/colpapel/DespachoError.jsp";
                            if(sw==0){
                                model.remesaService.buscaRemesa(numre);
                                next ="/colpapel/agregarPlanillaError.jsp";
                                // next ="/controller?estado=Remesa&accion=Search&remesa="+numre+"&mensaje=El tramo seleccionado no existe";
                            }
                        }
                    }
                    
                    
                }
            }
            if(request.getParameter("c2precinto")!=null && request.getParameter("c1precinto")!=null){
                if(!request.getParameter("c2precinto").equals("") || !request.getParameter("c1precinto").equals("")){
                    if(request.getParameter("c2precinto").equals(request.getParameter("c1precinto"))){
                        sw1=1;
                        request.setAttribute("cp2","filaroja");
                        request.setAttribute("cp1","filaroja");
                        next="/colpapel/DespachoError.jsp";
                    }
                }
            }
            
            
            if(!"".equals(numpla)&&!"".equals(numre) && sw1==0){
                /*******************************************************************/
                //DATOS DEL CONDUCTOR
                if(model.conductorService.getConductor()!=null){
                    Conductor conductor = model.conductorService.getConductor();
                    nombre=conductor.getNombre();
                    celular = conductor.getCelular();
                }
                
                //DATOS PLACA
                //model.placaService.buscaPlaca(request.getParameter("placa").toUpperCase());
                model.placaService.buscaPlaca(placa.toUpperCase());
                if(model.placaService.getPlaca()!=null){
                    Placa placa1 = model.placaService.getPlaca();
                    nit=placa1.getPropietario();
                    proveedor = placa1.getProveedor();
                }
                else{
                    errorUnit=true;
                    causa = "Retorno null la placa "+request.getParameter("placa");
                }
                if(nit.equals("")){
                    errorUnit=true;
                    causa = "El nit esta vacio "+request.getParameter("placa");
                }
                if(errorUnit){
                    request.setAttribute("mensaje","Error en el Propietario..");
                    next="/colpapel/DespachoError.jsp";
                    sw1=1;
                    if(sw==0){
                        model.remesaService.buscaRemesa(numre);
                        next ="/colpapel/agregarPlanillaError.jsp";
                        // next ="/controller?estado=Remesa&accion=Search&remesa="+numre+"&mensaje=El tramo seleccionado no existe";
                    }
                }
                else{
                    //DATOS RELACIONADOS AL STANDARD.
                    boolean esUrbano = model.stdjobService.esUrbano(request.getParameter("valorpla"));
                    if(model.stdjobdetselService.getStandardDetSel()!=null){
                        logger.info("Standard Job Det Set enconreado");
                        Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                        destino=stdjobdetsel.getDestination_code();
                        descripcion= stdjobdetsel.getSj_desc();
                        origen= stdjobdetsel.getOrigin_code();
                        unit_of_work= stdjobdetsel.getUnit_of_work();
                        moneda=stdjobdetsel.getCurrency();
                        costoRem=stdjobdetsel.getVlr_freight()*cfacturar;
                        tarifa = stdjobdetsel.getVlr_freight();
                        
                        account_code_i=stdjobdetsel.getAccount_code_i();
                        account_code_c=stdjobdetsel.getAccount_code_c();
                        wo_type = stdjobdetsel.getWoType();
                        tipoCarga = stdjobdetsel.getTipoCarga();
                        if(!vacio && wo_type!=null){
                            if(wo_type.equals("RM")||wo_type.equals("RC")||wo_type.equals("RE")||wo_type.equals("DC")||wo_type.equals("DP")){
                                aduana = "S";
                            }
                        }
                        logger.info("Cuenta de ingreso "+account_code_i);
                        logger.info("Cuenta de costo "+account_code_c);
                        pagador = stdjobdetsel.getPagador()!=null?stdjobdetsel.getPagador():"";
                        System.out.println("Pagador "+stdjobdetsel.getPagador());
                         /*
                          * Verificamos si la placa y el standar son de alquiler.
                          */
                        try{
                            if(model.VAlquilerSvc.EXISTE(placa.toUpperCase(), request.getParameter("standard"))){
                                
                                Calendar mesActual = Calendar.getInstance();
                                mesActual.set(mesActual.DATE,1);
                                
                                Date fecha_mes = mesActual.getTime();
                                java.text.SimpleDateFormat s =  new java.text.SimpleDateFormat("yyyy-MM-dd");
                                String fecha_mes_actual = s.format(fecha_mes);
                                valAlquiler =tarifa/30;
                                int cant = model.planillaService.cantPlanillasPlaca(fecha_mes_actual, placa.toUpperCase());
                                ajusteAlquiler = valAlquiler * cant;
                            }
                        }catch (Exception e){
                            throw new ServletException(e.getMessage());
                        }
                        //vlrMercancia = stdjobdetsel.getVlr_mercancia()*pesoreal;
                    }else{
                        logger.info("Standard Job Det Set NO encontrado");
                        request.setAttribute("mensaje","No se encontro el standard job seleccionado.");
                        sw1=1;
                        next="/colpapel/DespachoError.jsp";
                        if(sw==0){
                            model.remesaService.buscaRemesa(numre);
                            next ="/colpapel/agregarPlanillaError.jsp";
                            // next ="/controller?estado=Remesa&accion=Search&remesa="+numre+"&mensaje=El tramo seleccionado no existe";
                        }
                    }
                    logger.info("numero del standard "+request.getParameter("standard"));
                    logger.info("ft "+request.getParameter("valorpla"));
                    logger.info("Busco el urbano por : "+request.getParameter("valorpla"));
                    
                    
                    //DATOS COSTOS.
                    
                    model.stdjobcostoService.buscarStandardJobCostoFull(request.getParameter("standard"),request.getParameter("valorpla"));
                    if(model.stdjobcostoService.getStandardCosto()!=null){
                        logger.info("encontro el standard");
                        Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                        unit_cost=stdjobcosto.getUnit_cost();
                        viejoFlete = stdjobcosto.getUnit_cost();
                        mondedaFlete = stdjobcosto.getCurrency();
                        //SI LA PERSONA SELECCIONA OTRO VALOR DE FLETE SE CAMBIA EL UNIT COST
                        if(!request.getParameter("otro").equals("NO")&&!request.getParameter("otro").equals("")){
                            unit_cost = Float.parseFloat(request.getParameter("otro"));
                            if(unit_cost>stdjobcosto.getUnit_cost() && request.getParameter("autorizado").equals("no")){
                                justificar=true;
                            }
                        }
                        valorU=unit_cost*pesoreal;
                        valorU = com.tsp.util.Util.redondear(valorU, 0);
                        cf_code=stdjobcosto.getCf_code();
                        vacio =cf_code.substring(6,7).equals("V");
                        unit_transp = stdjobcosto.getUnit_transp();
                        fletePes=valorU;
                        //si la moneda del flete es diferente a la moneda de la compania entonces buscamos el valor del
                        //flete en pesos
                        
                        if(!mondedaFlete.equals(monedaCia)){
                            try{
                                model.tasaService.buscarValorTasa(monedaCia,mondedaFlete,monedaCia,fecha);
                            }catch(Exception et){
                                throw new ServletException(et.getMessage());
                            }
                            Tasa tasa = model.tasaService.obtenerTasa();
                            if(tasa!=null){
                                
                                double vtasa = tasa.getValor_tasa();
                                fletePes = vtasa*valorU;
                                fletePes = com.tsp.util.Util.redondear(fletePes,0);
                                logger.info("Moneda cia "+monedaCia);
                                logger.info("Moneda Flete "+mondedaFlete);
                                logger.info("Valor a cambiar "+valorU);
                                logger.info("Flete en pesos "+fletePes);
                            }else{
                                logger.info("NO EXISTE TASA ");
                                request.setAttribute("mensaje","No se encontro tasa de cambio.");
                                sw1=1;
                                next="/colpapel/DespachoError.jsp";
                                if(sw==0){
                                    model.remesaService.buscaRemesa(numre);
                                    next ="/colpapel/agregarPlanillaError.jsp";
                                    // next ="/controller?estado=Remesa&accion=Search&remesa="+numre+"&mensaje=El tramo seleccionado no existe";
                                }
                            }
                        }
                        
                        
                        
                        //ESTAS LINEAS SON PARA DEPURAR CUALES SON LAS PLANILLAS QUE ESTAN BAJANDO EN 0
                        if(unit_cost==0){
                            errorUnit=true;
                            causa = "Unit cost cero en stdjob cost full standard:"+request.getParameter("standard")+" Ft: "+request.getParameter("valorpla");;
                        }
                        
                        
                    }
                    else{
                        errorUnit=true;
                        causa = "No se encontro el standard: "+request.getParameter("standard")+" Ft: "+request.getParameter("valorpla");
                        request.setAttribute("mensaje","No se encontro la +cf seleccionada.");
                        sw1=1;
                        next="/colpapel/DespachoError.jsp";
                        if(sw==0){
                            model.remesaService.buscaRemesa(numre);
                            next ="/colpapel/agregarPlanillaError.jsp";
                            // next ="/controller?estado=Remesa&accion=Search&remesa="+numre+"&mensaje=El tramo seleccionado no existe";
                        }
                    }
                    //SE VERIFICA SI LA PLACA PERTENECE A UN GRUPO-
                    // model.plkgruService.buscaPlkgru(placa,request.getParameter("standard"));
                    if(model.plkgruService.getPlkgru()!=null){
                        Plkgru plk = model.plkgruService.getPlkgru();
                        grupo=plk.getGroupcode();
                        model.stdjobplkcostoService.buscaStd(grupo);
                        if(model.stdjobplkcostoService.getStd()!=null){
                            Stdjobplkcosto std = model.stdjobplkcostoService.getStd();
                            unit_cost = std.getUnit_cost();
                            valorU= pesoreal * unit_cost ;
                        }
                    }
                    //SI LA MONEDA DE LA REMESA ES DISTINTA A LA MONEDA DE LA CIA SE BUSCA EL VALOR DE LA REMESA EN PESOS
                    rem.setVlr_pesos((float)com.tsp.util.Util.redondear(costoRem-ajusteAlquiler,0));
                    if(!moneda.equals(monedaCia)){
                        
                        try{
                            model.tasaService.buscarValorTasa(monedaCia,moneda,monedaCia,fecha);
                        }catch(Exception et){
                            throw new ServletException(et.getMessage());
                        }
                        Tasa tasa = model.tasaService.obtenerTasa();
                        if(tasa!=null){
                            
                            double vtasa = tasa.getValor_tasa();
                            double vlrrem2= com.tsp.util.Util.redondear(vtasa*costoRem,0);
                            logger.info("Moneda cia "+monedaCia);
                            logger.info("Moneda standard "+mondedaFlete);
                            logger.info("Valor a cambiar "+costoRem);
                            logger.info("Valor rem en pesos "+vlrrem2);
                            rem.setVlr_pesos((float)com.tsp.util.Util.redondear(vlrrem2-ajusteAlquiler,0));
                            
                        }else{
                            logger.info("NO EXISTE TASA ");
                            request.setAttribute("mensaje","No se encontro tasa de cambio.");
                            sw1=1;
                            next="/colpapel/DespachoError.jsp";
                            if(sw==0){
                                model.remesaService.buscaRemesa(numre);
                                next ="/colpapel/agregarPlanillaError.jsp";
                                // next ="/controller?estado=Remesa&accion=Search&remesa="+numre+"&mensaje=El tramo seleccionado no existe";
                            }
                        }
                        
                        
                    }
                    
                    Vector consultas =new Vector();
                    
                    //AGREGO DOCUMENTOS RELACIONADOS CON LA REMESA
                    Vector vecR = model.RemDocSvc.getDocs();
                    if(vecR!=null){
                        for(int i=0; i<vecR.size();i++){
                            remesa_docto remdoc = (remesa_docto) vecR.elementAt(i);
                            if(!remdoc.getDocumento().equals("")){
                                logger.info("Documento que viene por documentos sin destinatario: "+ remdoc.getDocumento());
                                docinterno = remdoc.getDocumento();
                                remdoc.setCreation_user( usuario.getLogin());
                                remdoc.setNumrem(numre);
                                remdoc.setDstrct(usuario.getDstrct());
                                remdoc.setCodcli("000"+request.getParameter("standard").substring(0,3));
                                consultas.add(model.RemDocSvc.INSERTDOC(remdoc));
                                
                                boolean importacion = remdoc.getImportacion()!=null?remdoc.getImportacion().equals("S")?true:false:false;
                                if(importacion && !vacio){
                                    logger.info("ES IMPORTACION");
                                    remesa_docto remImp=model.RemDocSvc.buscarImpoExpo(usuario.getDstrct(),remdoc.getTipo_doc(),remdoc.getDocumento());
                                    if(remImp==null){
                                        consultas.add(model.RemDocSvc.insertImpoExpo(remdoc.getDstrct(), remdoc.getTipo_doc(),remdoc.getDocumento(),  remdoc.getCreation_user(),remdoc.getFecha_sia(),remdoc.getFecha_eta(),remdoc.getDescripcion_cga() ,remdoc.getCodcli()));
                                    }
                                    consultas.add(model.RemDocSvc.relacionImpoExpo(remdoc.getDstrct(),numre,remdoc.getTipo_doc(),remdoc.getDocumento(),remdoc.getCreation_user()));
                                }
                                
                                boolean exportacion = remdoc.getExportacion()!=null?remdoc.getExportacion().equals("S")?true:false:false;
                                if(exportacion && !vacio){
                                    logger.info("ES EXPORTACION");
                                    remesa_docto remImp=model.RemDocSvc.buscarImpoExpo(usuario.getDstrct(),remdoc.getTipo_doc(),remdoc.getDocumento());
                                    if(remImp==null){
                                        consultas.add(model.RemDocSvc.insertImpoExpo(remdoc.getDstrct(), remdoc.getTipo_doc(),remdoc.getDocumento(),  remdoc.getCreation_user(),"0099-01-01 00:00:00","0099-01-01 00:00:00","" ,remdoc.getCodcli()));
                                    }
                                    consultas.add(model.RemDocSvc.relacionImpoExpo(remdoc.getDstrct(),numre,remdoc.getTipo_doc(),remdoc.getDocumento(),remdoc.getCreation_user()));
                                }
                                
                            }
                            
                        }
                    }
                    String tienedoc = docinterno.equals("")?"N":"S";
                    p.setNumpla(numpla);
                    p.setUser_update(usuario.getLogin());
                    //SE MARCAN UTILIZADO LOS PRECINTOS
                    if(!sticker.equals("")){
                        p.setTipo_Documento("Sticker");
                        p.setPrecinto(sticker);
                        prencintos.add(p);
                        //logger.info(model.precintosSvc.utilizar(p));
                        consultas.add(model.precintosSvc.utilizar(p));
                        //consultas.add(model.precintosSvc.utilizar(p));
                    }
                    if(model.imprimirOrdenService.getHojaOrden()!=null){
                        HojaOrdenDeCarga orden =model.imprimirOrdenService.getHojaOrden();
                        ocarga =orden.getOrden();
                        orden.setNumpla(numpla);
                        model.imprimirOrdenService.setHoc(orden);
                        consultas.add(model.imprimirOrdenService.marcar());
                        
                    }
                    p.setTipo_Documento("Precinto");
                    
                    if(!cp1.equals("")){
                        p.setPrecinto(cp1);
                        
                        // logger.info(model.precintosSvc.utilizar(p));
                        consultas.add(model.precintosSvc.utilizar(p));
                        //consultas.add(model.precintosSvc.utilizar(p));
                        prencintos.add(p);
                    }
                    if(!cp2.equals("")){
                        p.setPrecinto(cp2);
                        logger.info(model.precintosSvc.utilizar(p));
                        consultas.add(model.precintosSvc.utilizar(p));
                    }
                    String prent = request.getParameter("precintos")!=null?request.getParameter("precintos"):"";
                    if(!prent.equals("")){
                        p.setPrecinto(prent);
                        logger.info(model.precintosSvc.utilizar(p));
                        consultas.add(model.precintosSvc.utilizar(p));
                    }
                    precintos =precintos +prent;
                    int k =2;
                    while(k<=5){
                        if(request.getParameter("precintos"+k)!=null){
                            if(!request.getParameter("precintos"+k).equals("")){
                                precintos = precintos+","+request.getParameter("precintos"+k);
                                p.setPrecinto(request.getParameter("precintos"+k));
                                logger.info(model.precintosSvc.utilizar(p));
                                consultas.add(model.precintosSvc.utilizar(p));
                            }
                        }
                        k++;
                    }
                    
                    //MANEJO DE INVENTARIO
                    logger.info("Inventario: "+inventario);
                    
                    if(inventario!=null && !"".equals(inventario)){
                        String productos[]=inventario.split(",");
                        for(int i =0; i<productos.length;i++){
                            
                            String valores[] = productos[i].split("/");
                            String codigo_prod = valores[0];
                            String discrepancia = valores[1];
                            String ubicacion = valores[2];
                            
                            Producto prod = new Producto();
                            prod.setNumpla(numpla);
                            prod.setNumrem(numre);
                            prod.setUsuario_modificacion(usuario.getLogin());
                            prod.setDistrito(usuario.getDstrct());
                            prod.setDiscrepancia(discrepancia);
                            prod.setCodigo(codigo_prod);
                            prod.setUbicacion(ubicacion);
                            
                            logger.info("Producto:"+codigo_prod);
                            logger.info("Discrepancia:"+discrepancia);
                            logger.info("Ubicacion:"+ubicacion);
                            
                            model.discrepanciaService.setProducto(prod);
                            consultas.add(model.discrepanciaService.utilizarProducto());
                        }
                        
                    }
                    
                    //AGREGAMOS LA PLANILLA
                    //SE BUSCA EL CMC DEL PROVEEDOR
                    String cmc = "";
                    model.proveedorService.obtenerProveedor(nit, usuario.getDstrct());
                    if(model.proveedorService.getProveedor()!=null){
                        Proveedor prov  = model.proveedorService.getProveedor();
                        cmc = prov.getCmc();
                        
                    }
                    // SE BUSCA EL CMC DEL CLIENTE
                    model.clienteService.searchCliente("000"+request.getParameter("standard").substring(0,3));
                    Cliente cli =null;
                    String cmcCli="";
                    if(model.clienteService.getCliente()!=null){
                        cli = model.clienteService.getCliente();
                        cmcCli = cli.getCmc();
                    }
                    
                    
                    pla.setCmc(cmc);
                    pla.setAgcpla(usuario.getId_agencia());
                    pla.setCia(usuario.getDstrct());
                    pla.setDespachador(usuario.getLogin());
                    pla.setDespla(request.getParameter("valorpla").substring(2,4));
                    pla.setNitpro(nit);
                    pla.setNumpla(numpla);
                    pla.setOripla(request.getParameter("valorpla").substring(0,2));
                    pla.setFecdsp(fecha);
                    pla.setPlatlr(request.getParameter("trailer").toUpperCase());
                    pla.setPlaveh(request.getParameter("placa").toUpperCase());
                    pla.setCedcon(request.getParameter("conductor"));
                    pla.setTipoviaje(vacio?"VAC":"NOR");
                    pla.setMoneda(mondedaFlete);
                    pla.setOrden_carga("");
                    pla.setNomCond(nombre);
                    pla.setPesoreal(pesoreal);
                    pla.setVlrpla((float)valorU);
                    pla.setRuta_pla(request.getParameter("via")!=null?request.getParameter("via"):request.getParameter("viaSel"));
                    pla.setPrecinto(precintos);
                    pla.setGroup_code("");
                    pla.setUnit_transp(unit_transp);
                    pla.setUnit_cost(unit_cost);
                    pla.setContenedores(contenedores);
                    pla.setTipocont(request.getParameter("tipo_cont").substring(0,3));
                    pla.setTipotrailer(request.getParameter("tipo_tra"));
                    pla.setProveedor(proveedor);
                    pla.setCf_code(cf_code);
                    pla.setVlrpla2((float)fletePes);
                    pla.setOrden_carga(ocarga);
                    pla.setFecha_salida("0099-01-01 00:00");
                    
                    logger.info("Unidad de transporte "+unit_transp );
                    logger.info("Unidad de Costo "+unit_cost );
                    
                    double vlrrem = costoRem-ajusteAlquiler;
                    if(request.getParameter("remesa")==null){
                        rem.setAduana(aduana);
                        rem.setAgcRem(usuario.getId_agencia());
                        rem.setCia(usuario.getDstrct());
                        rem.setCliente(request.getParameter("standard").substring(0,3));
                        rem.setDesRem(destino);
                        rem.setRemitente(remitente);
                        rem.setNumRem(numre);
                        rem.setOriRem(origen);
                        rem.setStdJobNo(request.getParameter("standard"));
                        rem.setTipoViaje(wo_type);
                        rem.setUnidcam(moneda);
                        rem.setUnitOfWork(unit_of_work);
                        rem.setUsuario(usuario.getLogin());
                        rem.setVlrRem((float)vlrrem);
                        rem.setPesoReal(Float.parseFloat(request.getParameter("cfacturar")));
                        rem.setDocInterno("");
                        rem.setRemitente(remitente);
                        rem.setDestinatario(destinatario!=null?destinatario:"");
                        rem.setObservacion(request.getParameter("observacion"));
                        rem.setDescripcion(descripcion);
                        rem.setRemision(numre);
                        rem.setFaccial(faccial);
                        rem.setCantreal(cantreal);
                        rem.setUnidad(request.getParameter("unidad"));
                        rem.setFacturable(facturable);
                        rem.setQty_value(tarifa);
                        rem.setCurrency(moneda);
                        rem.setNfacturable(nfacturable);
                        rem.setCrossdocking(cdocking);
                        rem.setTipoCarga(tipoCarga);
                        rem.setCadena(cadena);
                        rem.setPagador(pagador.equals("")?"000"+request.getParameter("standard").substring(0,3):pagador);
                        rem.setCmc(cmcCli);
                        
                        consultas.add(model.remesaService.insertRemesa(rem,usuario.getBase()));
                        
                        Vector doc = model.RemDocSvc.getDocumentos();
                        if( doc != null ){
                            if(destinatario!=null){
                                
                                String insertDocRel=model.RemDocSvc.INSERTDOCREL2(doc, usuario.getDstrct(), numre, destinatario, usuario.getLogin());
                                String  inserts2[]=insertDocRel.split(";");
                                for(int j = 0; j<inserts2.length;j++){
                                    tienedoc="S";
                                    logger.info("Documento que viene por documentos con destinatario: "+ inserts2[j]);
                                    consultas.add(inserts2[j]);
                                }
                            }
                        }
                        
                        RemesaDest rd= new RemesaDest();
                        rd.setCodigo(remitente);
                        rd.setDstrct(usuario.getDstrct());
                        rd.setNumrem(numre);
                        rd.setTipo("RE");
                        rd.setUsuario(usuario.getLogin());
                        consultas.add(model.rdService.insertRemesa(rd,usuario.getBase()));
                        
                        for(int i=0; i<destinatarios.length; i++){
                            rd= new RemesaDest();
                            if(destinatarios[i]!=null){
                                rd.setCodigo(destinatarios[i]);
                                rd.setDstrct(usuario.getDstrct());
                                rd.setNumrem(numre);
                                rd.setTipo("DE");
                                rd.setUsuario(usuario.getLogin());
                                if(!model.rdService.estaDest(numre,destinatarios[i])){
                                    consultas.add(model.rdService.insertRemesa(rd,usuario.getBase()));
                                }
                            }
                        }
                        
                    }
                    pla.setTiene_doc(tienedoc);
                    //Agrego la planilla
                    consultas.add(model.planillaService.insertPlanilla(pla,usuario.getBase()));
                    
                    //INSERTO LA REMPLA
                    rempla.setPlanilla(numpla);
                    rempla.setRemesa(numre);
                    rempla.setDstrct(usuario.getDstrct());
                    rempla.setAccount_code_c(account_code_c);
                    rempla.setAccount_code_i(account_code_i);
                    consultas.add(model.remplaService.insertRemesa(rempla,usuario.getBase()));
                    
                    
                    
                    //AGREGAMOS EL FITMEN
                    if(!request.getParameter("trailer").toUpperCase().equalsIgnoreCase("NA")){
                        //model.fitmenService.buscarFitmen(request.getParameter("trailer").toUpperCase());
                        if(model.fitmenService.getFitmen()!=null){
                            Fitmen fit = model.fitmenService.getFitmen();
                            logger.info("Fitem encontrado "+fit.getPlatlr()+" Numpla ant "+fit.getNumpla_ant());
                            fit.setNumpla(numpla);
                            fit.setUser_update(usuario.getLogin());
                            fit.setFeccum("0099-01-01 00:00:00");
                            model.fitmenService.setFitmen(fit);
                            logger.info(model.fitmenService.updateFitmen());
                            consultas.add(model.fitmenService.updateFitmen());
                        }
                        else{
                            Fitmen fit = new Fitmen();
                            fit.setPlatlr(request.getParameter("trailer").toUpperCase());
                            fit.setNumpla(numpla);
                            fit.setCreation_user(usuario.getLogin());
                            fit.setBase(usuario.getBase());
                            fit.setDstrct(usuario.getDstrct());
                            model.fitmenService.setFitmen(fit);
                            consultas.add(model.fitmenService.addFitmen());
                        }
                    }
                    //AGREGAMOS LAS FECHAS.
                    List listTabla = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
                    Iterator itTbla=listTabla.iterator();
                    while(itTbla.hasNext()){
                        
                        Tbltiempo tbl = (Tbltiempo) itTbla.next();
                        String tblcode = tbl.getTimeCode();
                        int secuence = tbl.getSecuence();
                        
                        if(request.getParameter(tbl.getTimeCode()).length()>15){
                            fecha = request.getParameter(tbl.getTimeCode()).substring(0,10);
                            hora=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(11,13));
                            minuto=request.getParameter(tbl.getTimeCode()).substring(14,16);
                            
                            fecha= fecha + " "+hora+":"+minuto+":00";
                            
                            Planilla_Tiempo pla_tiempo= new Planilla_Tiempo();
                            
                            pla_tiempo.setCf_code(cf_code);
                            pla_tiempo.setCreation_user(usuario.getLogin());
                            pla_tiempo.setDate_time_traffic(fecha);
                            pla_tiempo.setDstrct(usuario.getDstrct());
                            pla_tiempo.setPla(numpla);
                            pla_tiempo.setSecuence(secuence);
                            pla_tiempo.setSj(request.getParameter("standard"));
                            pla_tiempo.setTime_code(tblcode);
                            
                            consultas.add(model.pla_tiempoService.insertTiempo(pla_tiempo,usuario.getBase()));
                        }
                    }
                    
                    Banco bancoOj = model.servicioBanco.obtenerBanco(usuario.getDstrct(),banco ,cuenta);
                    moneda2 = bancoOj.getMoneda();
                    double vlrtasa = 1;
                    if(!moneda2.equals(monedaCia)){
                        
                        try{
                            model.tasaService.buscarValorTasa(monedaCia,moneda2,monedaCia,fecha);
                        }catch(Exception et){
                            throw new ServletException(et.getMessage());
                        }
                        Tasa tasa = model.tasaService.obtenerTasa();
                        if(tasa!=null){
                            
                            vlrtasa= tasa.getValor_tasa();
                            logger.info("Moneda cia "+monedaCia);
                            logger.info("Moneda Banco "+moneda2);
                            
                        }else{
                            logger.info("NO EXISTE TASA ");
                        }
                        
                        
                    }
                    
                    /*
                     *Si se aplicaron anticipos de proveedores
                     */
                    //SE BUSCA EL NIT DE LA CIA
                    String proveedorAnt="";
                    model.ciaService.buscarCia(usuario.getDstrct());
                    if(model.ciaService.getCompania()!=null){
                        proveedorAnt = model.ciaService.getCompania().getnit();
                    }
                    
                    float ant2 = anticipo;
                    Movpla movpla= new Movpla();
                    String beneficiario="";
                    //BUSCAMOS EL NIT DEL BENEFICIARIO
                    if(request.getParameter("beneficiario")!=null){
                        if(request.getParameter("beneficiario").equals("C")){
                            beneficiario = request.getParameter("conductor");
                        }
                        else{
                            beneficiario = model.movplaService.buscarBeneficiarioCheque(request.getParameter("beneficiario"),placa);
                        }
                    }
                    
                    
                    
                    if(beneficiario.equals("")){
                        //SE OBTIENE LA AGENCIA ASOCIADA AL ORIGEN DEL STANDARD
                        model.ciudadService.buscarCiudad(request.getParameter("valorpla").substring(0,2));
                        Ciudad c = model.ciudadService.obtenerCiudad();
                        beneficiario = request.getParameter("conductor");
                        if(c!=null){
                            String agencia = c.getAgAsoc();
                            
                            //SE BUSCA EL PAIS DE LA AGENCIA ASOCIADA
                            model.ciudadService.buscarCiudad(agencia);
                            c = model.ciudadService.obtenerCiudad();
                            if(c!=null){
                                String pais = c.getPais()!=null?c.getPais():"";
                                if(pais.equals("VE")){
                                    beneficiario=nit;
                                }
                            }
                        }
                        
                    }
                    
                    
                    Vector anticipoProv=model.anticiposService.getAnticiposProv();
                    boolean antProve=false;
                    if(anticipoProv!=null){
                        anticipo = 0;
                        for(int i=0; i<anticipoProv.size(); i++){
                            Anticipos ant = (Anticipos) anticipoProv.elementAt(i);
                            if(ant.getValor()>0){
                                antProve=true;
                                String provee[] = ant.getProveedor().split("/");
                                movpla.setAgency_id(usuario.getId_agencia());
                                movpla.setCurrency(moneda2);
                                movpla.setDstrct(usuario.getDstrct());
                                movpla.setPla_owner(nit);
                                movpla.setPlanilla(numpla);
                                movpla.setSupplier(placa);
                                movpla.setVlr((float)com.tsp.util.Util.redondear(ant.getValor()*vlrtasa,0));
                                movpla.setVlr_for(ant.getValor());
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(ant.getAnticipo_code());
                                movpla.setAp_ind("S");
                                movpla.setProveedor(provee.length>0?provee[0]:"");
                                movpla.setCreation_user(usuario.getLogin());
                                movpla.setSucursal(provee.length>1?provee[1]:"");
                                movpla.setBanco(banco);
                                movpla.setCuenta(cuenta);
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
                                movpla.setFecha_migracion("0099-01-01");
                                movpla.setBeneficiario(beneficiario);
//                                consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            }
                        }
                        
                    }
                    //AGREGAMOS LOS MOVIMIENTOS DE PLANILLA
                    
                    if(ant2>0 && !antProve){
                        movpla.setAgency_id(usuario.getId_agencia());
                        movpla.setCurrency(moneda2);
                        movpla.setDstrct(usuario.getDstrct());
                        movpla.setPla_owner(nit);
                        movpla.setPlanilla(numpla);
                        movpla.setSupplier(placa);
                        movpla.setDocument_type("001");
                        movpla.setConcept_code("01");
                        movpla.setAp_ind("S");
                        movpla.setProveedor(proveedorAnt);
                        movpla.setCreation_user(usuario.getLogin());
                        //movpla.setVlr(ant2);
                        //movpla.setVlr_for((float)com.tsp.util.Util.redondear(ant2*vlrtasa,0));
                        movpla.setVlr((float)com.tsp.util.Util.redondear(ant2*vlrtasa,0));
                        movpla.setVlr_for(ant2);
                        movpla.setSucursal("");
                        movpla.setBanco(banco);
                        movpla.setCuenta(cuenta);
                        movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
                        movpla.setFecha_migracion("0099-01-01");
                        movpla.setBeneficiario(beneficiario);
//                        consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                        
                    }
                    //AGREGAMOS EL AJUSTE DE ALQUILER
                    
                    //AGREGAMOS LOS MOVIMIENTOS DE PLANILLA
                    movpla= new Movpla();
                    if(ajusteAlquiler>0){
                        movpla.setAgency_id(usuario.getId_agencia());
                        movpla.setCurrency(moneda2);
                        movpla.setDstrct(usuario.getDstrct());
                        movpla.setPla_owner(nit);
                        movpla.setPlanilla(numpla);
                        movpla.setSupplier(placa);
                        movpla.setVlr(ajusteAlquiler*-1);
                        movpla.setDocument_type("001");
                        movpla.setConcept_code("09");
                        movpla.setAp_ind("V");
                        movpla.setProveedor(proveedorAnt);
                        movpla.setCreation_user(usuario.getLogin());
                        movpla.setVlr_for((float)com.tsp.util.Util.redondear(ant2*vlrtasa,0));
                        movpla.setSucursal("");
                        movpla.setBanco("");
                        movpla.setCuenta("");
                        movpla.setBeneficiario(beneficiario);
                        movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
//                        consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                    }
                    //AGREGAMOS LOS ANTICIPOS DINAMICOS..
                    model.anticiposService.vecAnticipos(usuario.getBase(),sj);
                    Vector ants = model.anticiposService.getAnticipos();
                    for (k=0 ; k<ants.size();k++){
                        Anticipos ant = (Anticipos) ants.elementAt(k);
                        String proveedorDes = ant.getProveedor();
                        model.anticiposService.searchAnticipos(usuario.getDstrct(), ant.getAnticipo_code(), sj);
                        if(model.anticiposService.getAnticipo()!=null){
                            ant = model.anticiposService.getAnticipo();
                            float value = 0;
                            String tipoDesc ="V";
                            if(request.getParameter(ant.getAnticipo_code())!=null){
                                if(ant.getTipo_s().equals("P"))
                                    tipoDesc = "P";
                                value = Float.parseFloat(request.getParameter(ant.getAnticipo_code()).equals("")?"0.0":request.getParameter(ant.getAnticipo_code()));
                            }
                            String prov="";
                            String nitPA="";
                            String sucursalPA="";
                            if(request.getParameter("provee"+ant.getAnticipo_code())!=null){
                                prov = request.getParameter("provee"+ant.getAnticipo_code());
                                String vec[]=prov.split("/");
                                nitPA = vec[0];
                                if(vec.length>1){
                                    sucursalPA = vec[1];
                                }
                                movpla= new Movpla();
                                movpla.setAgency_id(usuario.getId_agencia());
                                movpla.setCurrency(moneda2);
                                movpla.setDstrct(usuario.getDstrct());
                                movpla.setPla_owner(nit);
                                movpla.setPlanilla(numpla);
                                movpla.setSupplier(placa);
                                movpla.setVlr(value);
                                movpla.setVlr_for(value);
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(ant.getAnticipo_code());
                                movpla.setAp_ind(ant.getIndicador());
                                movpla.setProveedor(nitPA);
                                movpla.setSucursal(sucursalPA);
                                movpla.setCreation_user(usuario.getLogin());
                                movpla.setFecha_migracion("1900-01-01");
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100+k));
                                movpla.setBeneficiario(beneficiario);
//                                consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            }
                            else{
                                if(request.getParameter("check"+ant.getAnticipo_code())!=null && value>0){
                                    double vtasa = 1;
                                    if(!ant.getMoneda().equals(mondedaFlete)){
                                        
                                        try{
                                            model.tasaService.buscarValorTasa(monedaCia,ant.getMoneda(),mondedaFlete,fecha);
                                        }catch(Exception et){
                                            throw new ServletException(et.getMessage());
                                        }
                                        Tasa tasa = model.tasaService.obtenerTasa();
                                        
                                        if(tasa!=null){
                                            
                                            vtasa = tasa.getValor_tasa();
                                            
                                            logger.info("Moneda cia "+monedaCia);
                                            logger.info("Moneda standard "+mondedaFlete);
                                            logger.info("Valor a cambiar "+value);
                                            logger.info("Valor en moneda del standard "+value*vtasa);
                                            
                                        }else{
                                            logger.info("NO EXISTE TASA ");
                                        }
                                        
                                        
                                    }
                                    movpla= new Movpla();
                                    movpla.setAgency_id(usuario.getId_agencia());
                                    movpla.setCurrency(mondedaFlete);
                                    movpla.setDstrct(usuario.getDstrct());
                                    movpla.setPla_owner(nit);
                                    movpla.setPlanilla(numpla);
                                    movpla.setSupplier(placa);
                                    movpla.setVlr(value);
                                    movpla.setVlr_for(ant.getTipo_s().equals("P")?value:(float)com.tsp.util.Util.redondear(value*vtasa,0));
                                    movpla.setInd_vlr(tipoDesc);
                                    movpla.setDocument_type("001");
                                    movpla.setConcept_code(ant.getAnticipo_code());
                                    movpla.setAp_ind(ant.getIndicador());
                                    movpla.setProveedor(proveedorDes);
                                    movpla.setSucursal(sucursalPA);
                                    movpla.setCreation_user(usuario.getLogin());
                                    movpla.setFecha_migracion("1900-01-01");
                                    movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100+k));
                                    movpla.setBeneficiario(beneficiario);
//                                    consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    //AGREGAMOS LOS EXTRAFLETES
                    Vector efletes= model.sjextrafleteService.getFletes();
                    if(efletes!=null){
                        for(int i=0; i<efletes.size();i++){
                            SJExtraflete sjE = (SJExtraflete)efletes.elementAt(i);
                            sjE.setNumpla(numpla);
                            sjE.setNumrem(numre);
                            sjE.setFecdsp(fecha);
                            sjE.setDistrito(usuario.getDstrct());
                            
                            if(sjE.isSelec()){
                                movpla= new Movpla();
                                movpla.setAgency_id(usuario.getId_agencia());
                                movpla.setCurrency(moneda2);
                                movpla.setDstrct(usuario.getDstrct());
                                movpla.setPla_owner(nit);
                                movpla.setPlanilla(numpla);
                                movpla.setSupplier(placa);
                                movpla.setVlr(sjE.getVlrtotal());
                                movpla.setVlr_for(sjE.getVlrtotal());
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(sjE.getCod_extraflete());
                                movpla.setAp_ind("V");
                                movpla.setProveedor("");
                                movpla.setSucursal("");
                                movpla.setCreation_user(usuario.getLogin());
                                movpla.setCantidad(sjE.getCantidad());
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
                                movpla.setBeneficiario(beneficiario);
//                                consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                
                                model.sjextrafleteService.setSj(sjE);
                                consultas.add(model.sjextrafleteService.insertarCostoReembolsable());
                            }
                        }
                    }
                    //COSTOS REEMBOLSABLES
                    Vector costos= model.sjextrafleteService.getC_reembolsables();
                    model.sjextrafleteService.getC_reembolsables();
                    logger.info("Cantidad de costos para aplicar "+costos.size());
                    
                    for(int i=0; i<costos.size();i++){
                        SJExtraflete sjE = (SJExtraflete)costos.elementAt(i);
                        if(sjE.isSelec()){
                            sjE.setNumpla(numpla);
                            sjE.setNumrem(numre);
                            sjE.setFecdsp(fecha);
                            model.sjextrafleteService.setSj(sjE);
                            logger.info(model.sjextrafleteService.insertarCostoReembolsable());
                            consultas.add(model.sjextrafleteService.insertarCostoReembolsable());
                            
                        }
                        else{
                            logger.info("No esta seleccionado");
                        }
                        
                    }
                    Date d = new Date();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy/MM/dd");
                    String hoy = s.format(d);
                    
                    //SE INSERTA UN REGISTRO EN LA TABLA DE AUTORIZACIONES, Y SE ENVIA UN EMAIL NOTIFICANDO EL CAMBIO
                    
                    String nomorigen = model.ciudadService.obtenerNombreCiudad(request.getParameter("valorpla").substring(0,2));
                    String nomdest = model.ciudadService.obtenerNombreCiudad(request.getParameter("valorpla").substring(2,4));
                    
                    String textoBody= "Ocurrio un error:" +
                    "\n\n Cliente:  "+cli.getNomcli()+" Codigo: "+"000"+request.getParameter("standard").substring(0,3)+
                    "\n Agente duenio: "+cli.getAgente()+
                    "\n No se encontro el email de el agente duenio.";
                    
                    pla.setClientes(cli.getNomcli());
                    if(cli.getEmail()!=null){
                        if(justificar){
                            //BUSCO EL CLIENTE Y OBTENGO EL EMAIL DEL AGENTE DUE�O
                            //TEXTO DEL BODY DEL EMAIL
                            textoBody= "Se realiz� el siguiente ajuste en la planilla:\n\n\n";
                            textoBody+="DATOS DE LA PLANILLA\n\n"+
                            "NUMERO    :   "+numpla+"\n" +
                            "PLACA     :    "+request.getParameter("placa").toUpperCase()+"\n"+
                            "CONDUCTOR : "+request.getParameter("conductor")+"-"+nombre+"\n\n"+
                            "DATOS DEL CLIENTE"+
                            "\nCLIENTE    : "+cli.getNomcli()+
                            "\nSTANDARD   : "+request.getParameter("standard")+"-"+descripcion+
                            "\nRUTA       : "+nomorigen+"-"+nomdest+"\n\n"+
                            "AJUSTES EN EL FLETE"+
                            "\nVALOR DEL FLETE DEL STANDARD : "+com.tsp.util.Util.customFormat(viejoFlete)+"-"+unit_transp+"\n"+
                            "VALOR DEL FLETE AJUSTADO     : "+com.tsp.util.Util.customFormat(unit_cost)+"-"+unit_transp+"\n"+
                            "JUSTIFICACION DEL AJUSTE     : "+request.getParameter("justificacion")+"\n"+
                            "FECHA DEL CAMBIO             : "+hoy+"\n\n"+
                            "DESPACHADOR                  : "+usuario.getNombre().toUpperCase()+"\n\n";
                            
                            //AGREGO EL REGISTRO EN AUTORIZACION DE FLETE.
                            
                            aflete.setAgency_id(usuario.getId_agencia());
                            aflete.setAutorizador(cli.getAgente());
                            aflete.setBase(usuario.getBase());
                            aflete.setCedcond(request.getParameter("conductor")+"-"+nombre);
                            aflete.setDespachador(usuario.getNombre());
                            aflete.setDstrct(usuario.getDstrct());
                            aflete.setEmail_autorizador(cli.getEmail());
                            aflete.setJustificacion(request.getParameter("justificacion"));
                            aflete.setNuevoflete(unit_cost);
                            aflete.setPla_owner(nit);
                            aflete.setPlaca(request.getParameter("placa").toUpperCase());
                            aflete.setPlanilla(numpla);
                            aflete.setStandard(request.getParameter("standard")+"-"+descripcion);
                            aflete.setCreation_user(usuario.getLogin());
                            aflete.setUnidad(unit_transp);
                            aflete.setViejoflete(viejoFlete);
                            //PARA EL EMAIL
                            aflete.setMyEmail("procesos@mail.tsp.com");
                            aflete.setSubject("Notificacion ajuste flete a la planilla No. "+numpla);
                            aflete.setBody(textoBody);
                            aflete.setMyName("SLT");
                            
                        }
                        
                        if(vacio){
                            
                            textoBody= "Se realiz� el siguiente despacho vacio:\n\n\n";
                            textoBody+="DATOS DE LA PLANILLA\n\n"+
                            "NUMERO    :   "+numpla+"\n" +
                            "PLACA     :    "+request.getParameter("placa").toUpperCase()+"\n"+
                            "CONDUCTOR : "+request.getParameter("conductor")+"-"+nombre+"\n\n"+
                            "DATOS DEL CLIENTE"+
                            "\nCLIENTE    : "+cli.getNomcli()+
                            "\nSTANDARD   : "+request.getParameter("standard")+"-"+descripcion+
                            "\nRUTA       : "+nomorigen+"-"+nomdest+"\n\n"+
                            "DESPACHADOR                  : "+usuario.getNombre().toUpperCase()+"\n\n";
                            
                            aflete.setMyEmail("procesos@mail.tsp.com");
                            aflete.setSubject("Notificacion despacho vacio Planilla No. "+numpla);
                            aflete.setBody(textoBody);
                            aflete.setMyName("SLT");
                            aflete.setEmail_autorizador(cli.getEmail());
                            model.afleteService.setAflete(aflete);
                            consultas.add(model.afleteService.insertSendMail());
                        }
                        
                    }
                    else{
                        
                        aflete.setEmail_autorizador("mrolong@mail.tsp.com;lmerlano@mail.tsp.com;kreales@mail.tsp.com");
                        //aflete.setEmail_autorizador("kreales@mail.tsp.com");
                        //PARA EL EMAIL
                        aflete.setMyEmail("procesos@mail.tsp.com");
                        aflete.setSubject("ERROR AL TRATAR DE ENVIAR EMAIL PLANILLA No. "+numpla);
                        aflete.setBody(textoBody);
                        aflete.setMyName("SLT");
                        
                    }
                    if(mayorLimite){
                        textoBody= "Se realiz� el siguiente despacho con valor superior a "+com.tsp.util.Util.customFormat(limite)+":\n\n\n";
                        textoBody+="DATOS DE LA PLANILLA\n\n"+
                        "VALOR ESTIMADO DE LA MERCANCIA " +com.tsp.util.Util.customFormat(vlrMercancia)+"\n\n"+
                        "NUMERO    :   "+numpla+"\n" +
                        "PLACA     :    "+request.getParameter("placa").toUpperCase()+"\n"+
                        "CONDUCTOR : "+request.getParameter("conductor")+"-"+nombre+"\n\n"+
                        "DATOS DEL CLIENTE"+
                        "\nCLIENTE    : "+cli.getNomcli()+
                        "\nSTANDARD   : "+request.getParameter("standard")+"-"+descripcion+
                        "\nRUTA       : "+nomorigen+"-"+nomdest+"\n\n"+
                        "DESPACHADOR                  : "+usuario.getNombre().toUpperCase()+"\n\n" +
                        "Se necesita de su autorizacion para poder imprimir la planilla, remesa y cheque.";
                        aflete.setMyEmail("procesos@mail.tsp.com");
                        aflete.setSubject("Notificacion despacho con carga riesgosa Planilla No. "+numpla);
                        aflete.setBody(textoBody);
                        aflete.setMyName("SLT");
                        aflete.setEmail_autorizador("kreales@mail.tsp.com;lmerlano@mail.tsp.com");
                        model.afleteService.setAflete(aflete);
                    }
                    
                    if(aflete.getMyEmail()!=null){
                        model.afleteService.setAflete(aflete);
                        consultas.add(model.afleteService.insertSendMail());
                    }
                    if(aflete.getJustificacion()!=null) {
                        consultas.add(model.afleteService.insert());
                    }
                    if(!solicitud.equals(""))
                        model.afleteService.cumplirSolicitud(solicitud);
                    
                    
                    if(errorUnit==true){
                        try{
                            File file = new File("/exportar/migracion/log");
                            file.mkdirs();
                            String NombreArchivo = "/exportar/migracion/log/log_unitcero_"+numpla+".txt";
                            PrintStream archivo = new PrintStream(NombreArchivo);
                            archivo.println("Numero de la planilla "+numpla);
                            archivo.println("Causa :"+causa);
                        }catch(IOException e){
                            logger.info("Error tratando de escribir el archivo.");
                        }
                    }
                    model.placaService.actualizarCond(request.getParameter("conductor"),request.getParameter("placa").toUpperCase());
                    
                    /****************DATOS PARA TRAFICO */
                    
                    //BUSCAMOS EL NOMBRE DEL PROPIETARIO
                    String nomprop =model.nitService.obtenerNombre(nit);
                    pla.setNomprop(nomprop);
                    
                    //BUSCAMOS EL NOMBRE DEL ORIGEN
                    model.ciudadService.buscarCiudad(pla.getOripla());
                    String nomori = "";
                    String zona = "";
                    String agasoc ="";
                    String zona_ori="";
                    if(model.ciudadService.obtenerCiudad()!=null){
                        nomori = model.ciudadService.obtenerCiudad().getNomCiu();
                        zona = model.ciudadService.obtenerCiudad().getZona();
                        zona_ori = model.ciudadService.obtenerCiudad().getZona();
                        agasoc = model.ciudadService.obtenerCiudad().getAgAsoc();
                    }
                    pla.setNomori(nomori);
                    pla.setBase(usuario.getBase());
                    
                    //BUSCAMOS EL NOMBRE DEL ORIGEN
                    model.ciudadService.buscarCiudad(pla.getDespla());
                    String nomdes = model.ciudadService.obtenerCiudad().getNomCiu();
                    pla.setNomdest(nomdes);
                    
                    if(esUrbano && model.ciudadService.obtenerCiudad().isReporteUrbano()){
                        zona = agasoc;
                    }
                    //BUSCAMOS EL NOMBRE DE LA ZONA
                    model.zonaService.buscarZona(zona);
                    String nomzona ="";
                    if( model.zonaService.obtenerZona()!=null){
                        nomzona = model.zonaService.obtenerZona().getNomZona();
                    }else{
                        model.zonaService.buscarZona(zona_ori);
                        if( model.zonaService.obtenerZona()!=null){
                            zona = zona_ori;
                            nomzona = model.zonaService.obtenerZona().getNomZona();
                        }else{
                            zona = "";
                            nomzona = "";
                        }
                    }
                    pla.setZona("000");
                    pla.setNomzona("SALIDA");
                    pla.setCelularcon(celular);
                    
                    //BUSCO EL PROX PUESTO DE CONTROL
                    String via =pla.getOripla();
                    
                    /*String via = model.viaService.getRuta(pla.getRuta_pla());
                    if(via!=null){
                        via = via.substring(2,4);
                    }*/
                    
                    model.ciudadService.buscarCiudad(via);
                    String nompxpto = model.ciudadService.obtenerCiudad().getNomCiu();
                    logger.info("Via :"+via);
                    
                    pla.setProx_pto_control("");
                    pla.setNom_prox_pto_control("");
                    pla.setEintermedias(request.getParameter("eintermed"));
                    pla.setObservacion("");
                    String ciudades = "";
                    if(!"".equals(request.getParameter("eintermed"))){
                        
                        String vec[] = request.getParameter("eintermed").split(",");
                        for(int m =0; m<vec.length; m++){
                            model.ciudadService.buscarCiudad(vec[m]);
                            String nomciu = model.ciudadService.obtenerCiudad().getNomCiu();
                            ciudades  = ciudades +nomciu+",";
                        }
                        pla.setObservacion("Despacho realizado con entrega intermedias en "+ciudades);
                        
                    }
                    pla.setNeintermedias(ciudades);
                    double duracion =model.rmtService.getDuracionTramo(pla.getOripla(),via);
                    logger.info("Duracion :"+duracion);
                    if(model.tramoService.getTr()!=null){
                        duracion=model.tramoService.getTr().getDuracion();
                    }
                    
                    //SE SUMA A LA FECHA DEL DESPACHO LA DURACION DEL TRAMO DEL ORIGEN Y EL PROX PTO DE CONTROL
                    logger.info("Suma:"+fecha+" + " +duracion);
                    String fec_prox_rep= com.tsp.util.Util.sumarHorasFecha(fecha,duracion);
                    
                    logger.info("Fecha Prox reporte: "+fec_prox_rep);
                    pla.setFec_prox_pto_control("0099-01-01 00:00:00");
                    
                    model.tablaGenService.obtenerRegistro("SJSINTRAF",request.getParameter("standard"), "");
                    if(model.tablaGenService.getTblgen()==null){
                        //METODOS QUE AGREGAN A LAS TABLAS DE TRAFICO E INGRESO TRAFICO
                        model.planillaService.setPlanilla(pla);
                        consultas.add(model.planillaService.insertTrafico());
                    }
                    
                    try{
                        logger.info("Se ingresa la imagen de la planilla");
                        model.ImagenSvc.addDocument("004","001",numpla);
                        logger.info("Se ingresa la imagen de la remesa");
                        model.ImagenSvc.addDocument("004","002",numre);
                        consultas.add(model.ImagenSvc.getSQL("004","001",numpla));
                        consultas.add(model.ImagenSvc.getSQL("004","002",numre));
                    }catch(Exception ex){
                        throw new ServletException("ERROR AGREGANDO IMAGENES :"+ex.getMessage());
                    }
                    
                    if(cadena.equals("S")){
                        consultas.add(model.rmtService.marcarCadena(numpla));
                    }
                    
                    
                    
                    logger.info("Consultas para correr en la transaccion");
                    //SE EJECUTA TODO EL BATCH DE INSERTS.
                    for(int i =0; i<consultas.size();i++){
                        String sql = (String)consultas.elementAt(i);
                        logger.info(sql);
                        System.out.println(sql);
                    }
                    if(cf_code.equals("")){
                        request.setAttribute("mensaje", "El tramo seleccionado no existe.");
                        sw1=1;
                        next ="/colpapel/DespachoError.jsp";
                        if(sw==0){
                            model.remesaService.buscaRemesa(numre);
                            rem = model.remesaService.getRemesa();
                            model.stdjobcostoService.searchRutas(rem.getStdJobNo());
                            next ="/colpapel/agregarPlanillaError.jsp";
                            // next ="/controller?estado=Remesa&accion=Search&remesa="+numre+"&mensaje=El tramo seleccionado no existe";
                        }
                        
                        
                    }
                    if(sw1==0){
                        model.despachoService.insertar(consultas);
                        request.setAttribute("mensaje", "Se generaron la REMESA No. "+numre+" y la PLANILLA No. "+numpla);
                        request.setAttribute("numpla", numpla);
                        request.setAttribute("numrem", numre);
                        
                    }
                    
                    //se coloca de otro color la letra de los despachos vacios
                    //en ingresotrafico
                    if(vacio){
                        model.traficoService.cambiarColor(numpla, "#FF0000");
                    }
                    if(!"".equals(request.getParameter("eintermed"))){
                        model.traficoService.cambiarColor(numpla, "#9900CC");
                    }
                    TreeMap t = new TreeMap();
                    model.stdjobdetselService.setCiudadesDest(t);
                    model.stdjobdetselService.setCiudadesOri(t);
                    model.stdjobdetselService.setStdjobTree(t);
                    
                    model.imprimirOrdenService.listar(usuario);
                }
                
                model.tablaGenService.obtenerRegistro("CLIECARGUE","000"+request.getParameter("standard").substring(0,3), "");
                if(model.tablaGenService.getTblgen()!=null){
                    request.setAttribute("LINK","ok");
                }
                
                
                model.RemDocSvc.setDocs(null);
                model.tablaGenService.setTblgen(null);
                model.tablaGenService.obtenerRegistro("STDFORMNAL",request.getParameter("standard"), "");
                
            }
            else{
                request.setAttribute("mensaje", "NO EXISTE SERIE PARA REALIZAR PLANILLA Y REMESA");
                request.setAttribute("numpla", "");
                request.setAttribute("numrem", "");
            }
            System.out.println("NEXT "+next);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
