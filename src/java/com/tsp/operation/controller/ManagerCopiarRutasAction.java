/*
 * ManagerCopiarRutasAction.java
 *
 * Created on 27 de agosto de 2006, 03:57 PM
 */
/********************************************************************
 *  Nombre Clase.................   ManagerCopiarRutasAction.java
 *  Descripci�n..................   Accion que permite administrar los eventos de Copiar Rutas en 
 *                                  V�as
 *  Autor........................   David Pi�a L�pez
 *  Fecha........................   27 de agosto de 2006, 03:57 PM
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  David
 */
public class ManagerCopiarRutasAction extends Action{
    
    /** Creates a new instance of ManagerCopiarRutasAction */
    public ManagerCopiarRutasAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/ruta/trazarRuta.jsp";
        HttpSession session = request.getSession();
        try{
            String opcion = request.getParameter( "opc" );
            String origen = request.getParameter( "origen" );
            String rutas = request.getParameter( "rutas" );
            String distrito = (String)session.getAttribute( "Distrito" );
            if( opcion.equals( "1" ) ){
                next = "/jsp/trafico/ruta/trazarRuta.jsp";
                model.rutaService.getRutaOrigen( origen );
            }else if( opcion.equals( "2" ) ){
                next = "/jsp/trafico/ruta/result.jsp?opc=2";
                model.rutaService.armarVias( rutas );
            }else if( opcion.equals( "3" ) ){
                String valores[] = request.getParameterValues( "pcontrol" );
                String via = "";
                Vector errores = new Vector();
                Vector selects = new Vector();
                if( valores != null ){
                    for( int i = 0; i < valores.length; i++ ){
                        via = via + valores[ i ];
                    }
                    for (int i=0; i < via.length() -2 ; i += 2 ){
                        String tOrigen  = via.substring( i, (i + 2) );
                        String tDestino = via.substring( (i + 2), (i + 4) );
                        boolean existe = model.tramoService.existeTramo( distrito, tOrigen, tDestino );
                        if( !existe ){
                            String nombreO = model.ciudadService.obtenerNombreCiudad( tOrigen );
                            String nombreD = model.ciudadService.obtenerNombreCiudad( tDestino );
                            errores.add( nombreO + " - " + nombreD );
                        }
                    }
                    if( errores.size() == 0 ){
                        for ( int i=0; i < via.length(); i += 2 ){
                            Ciudad c = new Ciudad();
                            String codciu  = via.substring( i, (i + 2) );
                            String nombreciu = model.ciudadService.obtenerNombreCiudad( codciu );
                            c.setCodCiu( codciu );
                            c.setNombre( nombreciu );
                            selects.add( c );
                        }
                    }
                }
                request.setAttribute( "errores", errores );
                request.setAttribute( "selects", selects );
                next = "/jsp/trafico/ruta/result.jsp?opc=3";
                //model.tramoService.
            }
        }catch ( Exception e ){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest( next );
    }
    
}
