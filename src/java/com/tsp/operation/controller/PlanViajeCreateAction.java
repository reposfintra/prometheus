/**
 *
 */
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.util.rhymbox.Rhymbox;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class PlanViajeCreateAction extends Action {
  static Logger logger = Logger.getLogger(PlanViajeCreateAction.class);
  
  public void run() throws ServletException, InformationException {
      String next = "";
      Rhymbox rb = null;
      String zona = null;
      String retorno = null;
   
      HttpSession session = request.getSession();
      Usuario usuario = (Usuario)session.getAttribute("Usuario");
      Calendar fecha = Calendar.getInstance();
      SimpleDateFormat FFormat = new SimpleDateFormat("yyyyMMdd");
      SimpleDateFormat TFormat = new SimpleDateFormat("HHmm");
      String last_mod_date = FFormat.format(fecha.getTime());
      String last_mod_time = TFormat.format(fecha.getTime());
      PlanViaje planVj = new PlanViaje();
      NitSot conductor = null;
      NitSot propietario = null;

      planVj.setPlanilla(request.getParameter("planilla"));
      planVj.setPlaca(request.getParameter("placa"));
      planVj.setProducto(request.getParameter("producto"));
      planVj.setTrailer(request.getParameter("trailer"));
      planVj.setContenedor(request.getParameter("contenedor"));
      planVj.setCodtipocarga(request.getParameter("codtipocarga"));
      planVj.setFecha(request.getParameter("fecha"));
      planVj.setDestinatario(request.getParameter("destinatario"));
      planVj.setRuta(request.getParameter("ruta"));
      planVj.setCedcon(request.getParameter("cedcon"));
      planVj.setAl1(request.getParameter("al1"));
      planVj.setAt1(request.getParameter("at1"));
      planVj.setAl2(request.getParameter("al2"));
      planVj.setAt2(request.getParameter("at2"));
      planVj.setAl3(request.getParameter("al3"));
      planVj.setAt3(request.getParameter("at3"));
      planVj.setPl1(request.getParameter("pl1"));
      planVj.setPt1(request.getParameter("pt1"));
      planVj.setPl2(request.getParameter("pl2"));
      planVj.setPt2(request.getParameter("pt2"));
      planVj.setPl3(request.getParameter("pl3"));
      planVj.setPt3(request.getParameter("pt3"));
      planVj.setRadio(request.getParameter("radio"));
      planVj.setCelular(request.getParameter("celular"));
      planVj.setAvantel(request.getParameter("avantel"));
      planVj.setTelefono(request.getParameter("telefono"));
      planVj.setCazador(request.getParameter("cazador"));
      planVj.setMovil(request.getParameter("movil"));
      planVj.setOtro(request.getParameter("otro"));
      planVj.setQlo(request.getParameter("qlo"));
      planVj.setQto(request.getParameter("qto"));
      planVj.setQld(request.getParameter("qld"));
      planVj.setQtd(request.getParameter("qtd"));
      planVj.setQl1(request.getParameter("ql1"));
      planVj.setQt1(request.getParameter("qt1"));
      planVj.setQl2(request.getParameter("ql2"));
      planVj.setQt2(request.getParameter("qt2"));
      planVj.setQl3(request.getParameter("ql3"));
      planVj.setQt3(request.getParameter("qt3"));
      planVj.setTl1(request.getParameter("tl1"));
      planVj.setTt1(request.getParameter("tt1"));
      planVj.setTl2(request.getParameter("tl2"));
      planVj.setTt2(request.getParameter("tt2"));
      planVj.setTl3(request.getParameter("tl3"));
      planVj.setTt3(request.getParameter("tt3"));
      planVj.setNomfam(request.getParameter("nomfam"));
      planVj.setPhonefam(request.getParameter("phonefam"));
      planVj.setNitpro(request.getParameter("nitpro"));
      planVj.setComentario1(request.getParameter("comentario1"));
      planVj.setComentario2(request.getParameter("comentario2"));
      planVj.setComentario3(request.getParameter("comentario3"));
      planVj.setLast_mod_date(last_mod_date);
      planVj.setLast_mod_time(last_mod_time);
      planVj.setLast_mod_user(usuario.getLogin());
      planVj.setUsuario(usuario.getLogin());
      planVj.setCia(request.getParameter("cia"));

      retorno = request.getParameter("retorno");
      if (retorno  == null)
        retorno = "N";
      planVj.setRetorno(retorno);


      try{
            //busca el nit del conductor para crearlo o actualizarlo
            if (model.nitService.searchNit(planVj.getCedcon())){
              conductor = model.nitService.getNit();
              conductor.setNombre(request.getParameter("nomcon"));
              conductor.setDireccion(request.getParameter("dircon"));
              conductor.setTelefono(request.getParameter("phonecon"));
              conductor.setCodciu(request.getParameter("ciucon"));
              model.nitService.updateNit(conductor);
            }
            else{
              conductor = new NitSot();
              conductor.setCedula(request.getParameter("cedcon"));
              conductor.setNombre(request.getParameter("nomcon"));
              conductor.setDireccion(request.getParameter("dircon"));
              conductor.setTelefono(request.getParameter("phonecon"));
              conductor.setCodciu(request.getParameter("ciucon"));
              conductor.setCellular("");
              conductor.setCoddpto("");
              conductor.setCodpais("");
              conductor.setE_mail("");
              conductor.setFechanac("0099-01-01");
              conductor.setId_mims("");
              conductor.setSexo("");
              conductor.setUsuariocrea("");
              conductor.setUsuario("");
              model.nitService.insertNit(conductor);
            }

            //busca el nit del propetrio para crearlo o actualizarlo
            if (model.nitService.searchNit(planVj.getNitpro())){
              propietario = model.nitService.getNit();
              propietario.setNombre(request.getParameter("nompro"));
              propietario.setDireccion(request.getParameter("dirpro"));
              propietario.setTelefono(request.getParameter("phonepro"));
              propietario.setCodciu(request.getParameter("ciupro"));
            }
            else{
              propietario = new NitSot();
              propietario.setCedula(request.getParameter("nitpro"));
              propietario.setNombre(request.getParameter("nompro"));
              propietario.setDireccion(request.getParameter("dirpro"));
              propietario.setTelefono(request.getParameter("phonepro"));
              propietario.setCodciu(request.getParameter("ciupro"));
              propietario.setCellular("");
              propietario.setCoddpto("");
              propietario.setCodpais("");
              propietario.setE_mail("");
              propietario.setFechanac("0099-01-01");
              propietario.setId_mims("");
              propietario.setSexo("");
              propietario.setUsuariocrea("");
              propietario.setUsuario("");
              model.nitService.insertNit(propietario);
            }

            model.planViajeService.createPlanVj(planVj);
            
            //ENVIA EL MENSAJE DE NOTIFICACION POR RHYMBOX
            rb = new Rhymbox();
            rb.setBody("SE HA CREADO EL PLAN DE VIAJE NO: " + planVj.getPlanilla() + "\n ESTE ES UN MENSAJE AUTOMATICO ABSTENGASE DE RESPONDERLO.");
            //rb.sendMessage();
            zona = model.planViajeService.getZona(planVj.getCia(), planVj.getPlanilla());
            if ((zona == null) || zona.trim().equals("")){
                throw new InformationException("LA ZONA DE LA PLANILLA "+planVj.getPlanilla()+ " NO SE PUEDE DETERMINAR");
            }
            rb.sendMessageZona(zona);

            logger.info(usuario.getNombre() + " Creo planViaje con: Planilla = " + planVj.getPlanilla());

            request.setAttribute("mensaje", "� PLAN VIAJE " + planVj.getPlanilla() + " CREADO EXITOSAMENTE !");
            next = "/controller?estado=PlanViaje&accion=QryAg";
      }
      catch (Exception e){
        throw new ServletException(e.getMessage());
      }
      // Redireccionar a la p�gina indicada.
      this.dispatchRequest(next);
  }
}
