/********************************************************************
 *      Nombre Clase.................   TraficoEConfAction.java
 *      Descripci�n..................   Action que se encarga de eliminar una configuracion creada por el usuario
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class TraficoEConfAction extends Action {
    
    /** Creates a new instance of TraficoEConfAction */
    public TraficoEConfAction () {
    }
    
    public void run () throws ServletException, InformationException {
        try{
            String  []camposS  = request.getParameterValues ("camposS");
            String campoS      = ""+request.getParameter ("cConfiguraciones");
            String user        = request.getParameter ("usuario");
            String clas        = ""+ request.getParameter ("clas");
            String filtro      = ""+request.getParameter ("filtro");
            String var1        = ""+request.getParameter ("var1");
            String var2        = ""+request.getParameter ("var2");
            String var3        = ""+request.getParameter ("var3");
            String conf        = ""+request.getParameter ("conf");
            String nombre      = ""+request.getParameter ("nombre");
            String linea       = ""+request.getParameter ("linea");
            String nombreC     = ""+request.getParameter ("nombreC");
            String filtroP = "" + request.getParameter("filtroP");
            
            filtro=model.traficoService.decodificar (filtro);
            
            model.traficoService.eliminarConfiguracion (campoS);
            
            String configuracion ="";
            
            /*if(camposS != null){
                for(int i=0;i<=camposS.length-1;i++){
                    String campo   = camposS[i];
                    //////System.out.println ("............... EN e configuracion - Campo "+i+": "+campo);
                }
                configuracion = model.traficoService.getConfiguracion (camposS);
                
                linea="";
                for(int i=0;i<=camposS.length-1;i++){
                    linea  = linea+"-"+camposS[i];
                }
                
                //////System.out.println ("............... EN e configuracion  - linea: "+ linea);
                linea="-tipo_despacho-planilla-placa"+linea;
            }*/
            
            linea = "-tipo_despacho-planilla-placa"+linea;//Modif. Andr�s Maturana
            
            filtro=model.traficoService.codificar (filtro);
            String next ="";
            
            //////System.out.println(".................. LINEA: " + linea);
            next= "/jsp/trafico/controltrafico/configuracion3.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion+"&clas="+clas+"&filtroP="+filtroP;
            
            ////System.out.println ("next "+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
    
}
