/*
 * MonedaAnularAction.java
 *
 * Created on 26 de octubre de 2005, 03:47 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class MonedaAnularAction  extends Action{
        
        
        /** Creates a new instance of MonedaAnularAction */
        public MonedaAnularAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
                String codigo = request.getParameter("c_codigo");
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                try{
                        Moneda moneda = new Moneda();
                        moneda.setCodMoneda(codigo);
                        moneda.setUser_update(usuario.getLogin());
                        model.monedaSvc.anularMoneda(moneda);           
                }catch (Exception e){
                        throw new ServletException(e.getMessage());
                }
         
                // Redireccionar a la p�gina indicada.
                this.dispatchRequest(next);
        }
        
}
