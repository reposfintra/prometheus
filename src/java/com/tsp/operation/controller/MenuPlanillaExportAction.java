/*
 * MenuExportPlanillaAction.java
 *
 * Created on 22 de diciembre de 2004, 18:17
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;


/**
 *
 * @author  Administrador
 */
public class MenuPlanillaExportAction extends Action {
    
    /** Creates a new instance of MenuExportPlanillaAction */
    public MenuPlanillaExportAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
       try
        {   
            final String next = "/migracion/remesas.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e)
        {
            throw new ServletException(e.getMessage());
        }        
    }    
}
