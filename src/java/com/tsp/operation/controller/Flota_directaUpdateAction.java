/*******************************************************************
 * Nombre clase: Flota_directaUpdateAction.java
 * Descripci�n: Accion para actualizar una flota directa a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 2 de diciembre de 2005, 03:14 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Flota_directaUpdateAction extends Action{
    
    /** Creates a new instance of Flota_directaUpdateAction */
    public Flota_directaUpdateAction () {
    }
    public void run() throws ServletException, InformationException {
        String next="/jsp/masivo/flota_directa/flota_directaModificar.jsp?&reload=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String nnit = request.getParameter("c_nnit");
        String nplaca = request.getParameter("c_nplaca");
        String nit = request.getParameter("c_nit");
        String placa = request.getParameter("c_placa");        
        try{
            Responsable resp = new Responsable();
            Flota_directa flota = new Flota_directa();
            flota.setNit (nit);
            flota.setPlaca (placa);
            if(!model.flota_directaService.existFlota_directa (nnit, nplaca)){
                if(model.identidadService.existeIdentidad(nit)){
                    if(model.placaService.existePlaca (placa)){
                        model.flota_directaService.updateFlota_directa (nnit, nplaca, usuario.getLogin().toUpperCase(), nit, placa);
                        request.setAttribute("mensaje","Flota Directa Modificada");
                        model.flota_directaService.searchFlota_directa(nnit, nplaca);
                    }
                    else{
                        request.setAttribute("mensaje","Error la placa no existe!");
                        model.flota_directaService.searchFlota_directa(nit, placa);
                    }
                }
                else{
                    request.setAttribute("mensaje","Error el nit no existe!");
                    model.flota_directaService.searchFlota_directa(nit, placa);
                }
            }
            else
                request.setAttribute("mensaje","Error el registro ya existe!");
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
