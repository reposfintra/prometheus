/******
 * Nombre:        Jerarquia_tiempoInsertAction.java              
 * Descripci�n:   Clase Action para insertar jerarquia tiempo   
 * Autor:         Ing. Jose de la Rosa 
 * Fecha:         26 de junio de 2005, 03:56 PM                             
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 *******/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class Jerarquia_tiempoInsertAction extends Action{
    
    /** Creates a new instance of Jerarquia_tiempoInsertAction */
    public Jerarquia_tiempoInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/jerarquia/Jerarquia_tiempoInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        String distrito = (String) session.getAttribute("Distrito");
        String actividad = request.getParameter("c_actividad");
        String responsable = request.getParameter("c_responsable");
        String demora = request.getParameter("c_demora");
        String nactividad = actividad;
        String nresponsable = responsable;
        String ndemora = demora;
        int sw=0;
        try{
            Jerarquia_tiempo jerarquia_tiempo = new Jerarquia_tiempo();
            jerarquia_tiempo.setActividad(actividad);
            jerarquia_tiempo.setResponsable(responsable);
            jerarquia_tiempo.setDemora(demora);
            jerarquia_tiempo.setCia(distrito.toUpperCase());
            jerarquia_tiempo.setUser_update(usuario.getLogin().toUpperCase());
            jerarquia_tiempo.setCreation_user(usuario.getLogin().toUpperCase());
            jerarquia_tiempo.setBase(usuario.getBase());
            try{
                model.jerarquia_tiempoService.insertJerarquia_tiempo(jerarquia_tiempo);
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.jerarquia_tiempoService.existJerarquia_tiempo(actividad,responsable,demora)){
                    model.jerarquia_tiempoService.updateJerarquia_tiempo(actividad,responsable,demora, usuario.getLogin().toUpperCase(),nactividad,nresponsable,ndemora,usuario.getBase());
                    request.setAttribute("mensaje","Jerarquia Tiempo ingresada exitosamente!");
                }
                else{
                    request.setAttribute("mensaje","Jerarquia Tiempo ya existe!!");
                }
            }
            else{
                request.setAttribute("mensaje","Jerarquia Tiempo ingresada exitosamente!");
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
