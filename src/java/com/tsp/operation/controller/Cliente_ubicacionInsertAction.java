/********************************************************************
 * Nombre clase: Cliente_ubicacionInsertAction.java
 * Descripci�n: Accion para ingresar un cliente_ubicacion a la bd.
 * Autor: Jose de la rosa
 * Fecha: 20 de enero de 2006, 09:33 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;


public class Cliente_ubicacionInsertAction extends Action {
    
    /** Creates a new instance of Cliente_ubicacionInsertAction */
    public Cliente_ubicacionInsertAction () {
    }
    public void run () throws ServletException, InformationException{
        String next = "/jsp/trafico/cliente_ubicacion/Cliente_ubicacionInsertar.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String dstrct = (String) session.getAttribute ("Distrito");
        String cliente = request.getParameter ("codcli");
        String ubicacion = request.getParameter ("c_ubicacion");
        boolean sw1 = false;
        int sw=0;
        try{
            if(! model.ubService.existeUbicacion (ubicacion, dstrct.toUpperCase ()) ){
                sw1=true;
                request.setAttribute ("mensaje","Error la ubicaci�n no existe!");
            }
            if(! model.clienteService.existeCliente (cliente) ){
                request.setAttribute ("mensaje","Error el cliente no existe!");
                sw1=true;
            }
            Cliente_ubicacion cli = new Cliente_ubicacion ();
            cli.setCliente (cliente);
            cli.setUbicacion (ubicacion);
            cli.setDstrct (dstrct.toUpperCase ());
            cli.setUser_update (usuario.getLogin ().toUpperCase ());
            cli.setCreation_user (usuario.getLogin ().toUpperCase ());
            cli.setBase (usuario.getBase ().toUpperCase ());
            if(sw1==false){
                try{
                    if( model.ubService.existeUbicacion (ubicacion, dstrct.toUpperCase ()) )
                        if( model.clienteService.existeCliente (cliente) )
                            model.cliente_ubicacionService.insertCliente_ubicacion (cli);
                        else
                            request.setAttribute ("mensaje","Error el cliente no existe!");
                    else
                        request.setAttribute ("mensaje","Error la ubicaci�n no existe!");
                }catch(SQLException e){
                    sw=1;
                }
                if(sw==1){
                    if(!model.cliente_ubicacionService.existCliente_ubicacion (cliente,ubicacion)){
                        model.cliente_ubicacionService.updateCliente_ubicacion (cli);
                        request.setAttribute ("mensaje","La informaci�n ha sido ingresada exitosamente!");
                    }
                    else{
                        request.setAttribute ("mensaje","Error la ubicacion del cliente ya existe!");
                    }
                }
                else{
                    request.setAttribute ("mensaje","La informaci�n ha sido ingresada exitosamente!");
                }
            }
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
