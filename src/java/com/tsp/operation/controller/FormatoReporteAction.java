/*
 * Nombre        FormatoReporteAction.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         27 de octubre de 2006, 10:40 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.HCuadroAzul;

public class FormatoReporteAction extends Action{
    
    /**
     * Crea una nueva instancia de  FormatoReporteAction
     */
    public FormatoReporteAction(){
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next="/jsp/masivo/formato/filtro.jsp";
        String opc = (request.getParameter("opcion") != null)? request.getParameter("opcion") : "";
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            if( opc.equals("load") ){
                
                model.FormatoSvc.loadTipoDocumentos();
                model.FormatoSvc.loadTFormatos();
                
            }
          
            else if( opc.equals("web") ){
                
                String anio = request.getParameter("anio");
                String[] mes = request.getParameter("mes").split("-");
                
                String nombremes = mes[0];
                String nummes    = mes[1]; 
                HCuadroAzul h = new HCuadroAzul();
                h.start(u, nummes, nombremes, anio, "web", model);
                //model.FormatoSvc.cuadroAzul( anio, nombremes, nummes, u.getDstrct() );
                h.join();
                next="/jsp/masivo/formato/reporte.jsp";
                
            }else if( opc.equals("export") ){
                
                HCuadroAzul h = new HCuadroAzul();
                h.start(u, "", "", "", "export", model);
                //ReporteFormatoXLS r = new ReporteFormatoXLS();
                //r.start( u, model.FormatoSvc.getVector() );                
                next="/jsp/masivo/formato/reporte.jsp";
                
            }else if( opc.equals("excel") ){
                
                String anio = request.getParameter("anio");
                String[] mes = request.getParameter("mes").split("-");
                
                String nombremes = mes[0];
                String nummes    = mes[1]; 
                HCuadroAzul h = new HCuadroAzul();
                h.start(u, nummes, nombremes, anio, "excel", model);
                
                next="/jsp/masivo/formato/filtro.jsp?mensaje=El proceso de generaci�n del reporte ha iniciado, consulte sus archivos";
            }
            
        }catch (Exception ex){
            throw new ServletException(ex.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
  
    
}
