/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.extractoDuplicadoDAO;
import com.tsp.operation.model.DAOS.impl.extractoDuplicadoImpl;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.extractoDuplicadoBeans;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author mariana
 */
public class extractoDuplicadoAction extends Action {

    Usuario usuario = null;
    private final int BUSCAR_NEGOCIOS_CLIENTE = 1;
    private final int DETALLE_NEGOCIOS = 2;
    private final int APLICAR_GENERAR = 3;
    private final int BUSCAR_IDROP = 4;
    private final int GUARDAR_DOCUMENTOS_TEMPORAL = 5;
    private final int TIPO_CARTERA = 6;
    private extractoDuplicadoDAO dao;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new extractoDuplicadoImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case BUSCAR_NEGOCIOS_CLIENTE:
                    buscarNegociosCliente();
                    break;
                case DETALLE_NEGOCIOS:
                    detalleNegocios();
                    break;
                case APLICAR_GENERAR:
                    aplicarGenerar();
                    break;              
                case GUARDAR_DOCUMENTOS_TEMPORAL:
                    guardardocumentosExt();
                    break;
                case TIPO_CARTERA:
                    getLineaNegocio();
                    break;
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
            e.printStackTrace();
        }
    }

    public void buscarNegociosCliente() {
        try {
            String query = "";
//            String unidad = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";
//            if (unidad.equals("FENALCO_ATL") || unidad.equals("FENALCO_BOL")) {
//                query = "SQL_BUSCAR_NEGOCIOS_CLIENTE";
//            } else {
//                query = "SQL_BUSCAR_NEGOCIOS_CLIENTE_MICRO";
//            }
            //
            query = "SQL_BUSCAR_NEGOCIOS_CLIENTE";
            Gson gson = new Gson();
            String cedula = request.getParameter("cedula");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.buscarNegociosCliente(cedula, query, usuario)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void detalleNegocios() {
        try {
            Gson gson = new Gson();
            String negocio = request.getParameter("negocio");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.detalleNegocios(negocio,usuario)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void aplicarGenerar() throws Exception {
        try {
            Gson gson = new Gson();
            String negocio = request.getParameter("negocio");
            String accion = request.getParameter("accionf");
            // String info = "{\"page\":1,\"rows\":" + gson.toJson(dao.detalleNegocios(negocio, accion, usuario)) + "}";
            String json = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            String json = "{\"respuesta\":\"Error\"}";
            this.printlnResponse(json, "application/json;");
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardardocumentosExt() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String porct_ixmora = request.getParameter("ixmora") == null ? "0" : request.getParameter("ixmora");
            String porct_gacobranza = request.getParameter("gacobranza") == null || request.getParameter("gacobranza").equals("") ? "0" : request.getParameter("gacobranza");
            String valorsaldo = request.getParameter("valorsaldo") == null ? "0" : request.getParameter("valorsaldo");
            String valorMora = request.getParameter("valormora") == null ? "0" : request.getParameter("valormora");
            String valorGac = request.getParameter("valorgac") == null ? "0" : request.getParameter("valorgac");
            String unidad_negocio = request.getParameter("unidad_negocio") == null ? "" : request.getParameter("unidad_negocio");
            String totalExtracto = request.getParameter("totalExtracto") == null ? "0" : request.getParameter("totalExtracto");

            extractoDuplicadoBeans duplicadoBeans = new extractoDuplicadoBeans();
            duplicadoBeans.setEstado_obligacion("VENCIDO"); //CAMBIAR POR EL PARAMETRO
            duplicadoBeans.setPorct_capital(0);//en cero por ahora
            duplicadoBeans.setPorct_interes(0);//en cero por ahora
            duplicadoBeans.setPorct_ixmora(Double.parseDouble(porct_ixmora));
            duplicadoBeans.setPorct_gacobranza(Double.parseDouble(porct_gacobranza));
            duplicadoBeans.setValor_saldo(valorsaldo);
            duplicadoBeans.setValor_saldo_capital("0");//en cero por ahora
            duplicadoBeans.setValor_saldo_interes("0");//en cero por ahora
            duplicadoBeans.setInt_mora(valorMora);
            duplicadoBeans.setGasto_cobranza(valorGac);
            duplicadoBeans.setTotalExtracto(Double.parseDouble(totalExtracto));
            duplicadoBeans.setUnidad_negocio(unidad_negocio);
            
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(request.getParameter("listaJson"));
            //JsonObject obj = (JsonObject) (new JsonParser()).parse();
            
            json = this.dao.guardardocumentosExt(asJsonArray, duplicadoBeans, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void getLineaNegocio() throws Exception {
        String reponseJson = dao.getLineaNegocio(usuario);
        this.printlnResponse(reponseJson, "application/json;");
    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }
}
