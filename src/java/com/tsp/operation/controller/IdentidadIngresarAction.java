/*************************************************************************
 * Nombre:        IdentidadIngresarAction.java
 * Descripci�n:   Clase Action para ingresar identidad
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         19 de julio de 2005, 11:38 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIOGENES
 */
public class IdentidadIngresarAction extends Action{

    /** Creates a new instance of IdentidadIngresarAction */
    public IdentidadIngresarAction() {
    }

    public void run() throws ServletException, InformationException {
        String em = (request.getParameter("empleado")!=null)?request.getParameter("empleado"):"0";
        String c = (request.getParameter("conductor")!=null)?request.getParameter("conductor"):"0";
        String p = (request.getParameter("propietario")!=null)?request.getParameter("propietario"):"0";
        String v = (request.getParameter("proveedor")!=null)?request.getParameter("proveedor"):"0";
        String a = (request.getParameter("procarbon")!=null)?request.getParameter("procarbon"):"0";
        String clas = em+c+p+v+a;

        String tipodoc = request.getParameter("c_tipdoc");
        String doc = (request.getParameter("c_doc")!=null)?request.getParameter("c_doc").trim():"";
        String pag=(request.getParameter("pag")!=null)?request.getParameter("pag").trim():"";
        String idmims = "";
        String nom = (request.getParameter("c_nom")!=null)?request.getParameter("c_nom").toUpperCase():"";
        String nom1 = (request.getParameter("c_nom1")!=null)?request.getParameter("c_nom1").toUpperCase():"";
        String nom2 = (request.getParameter("c_nom2")!=null)?request.getParameter("c_nom2").toUpperCase():"";
        String ape1 = (request.getParameter("c_ape1")!=null)?request.getParameter("c_ape1").toUpperCase():"";
        String ape2 = (request.getParameter("c_ape2")!=null)?request.getParameter("c_ape2").toUpperCase():"";
        String genero = (request.getParameter("c_genero")!=null)?request.getParameter("c_genero"):"";
        String fecha = (request.getParameter("c_fecha")!=null && !request.getParameter("c_fecha").equals(""))?request.getParameter("c_fecha"):"0099-01-01";
        String pais = request.getParameter("pais");
        String est = request.getParameter("est");
        String ciudad = request.getParameter("ciudad");
        String dir = (request.getParameter("c_dir").toUpperCase());
        String tel1 = request.getParameter("c_tel11")+" "+request.getParameter("c_tel12")+" "+request.getParameter("c_tel13");
        String tel2 = request.getParameter("c_tel21")+" "+request.getParameter("c_tel22")+" "+request.getParameter("c_tel23");
        String cel = request.getParameter("c_cel");
        String email = request.getParameter("c_email");

        String cargo = (request.getParameter("c_cargo")!=null)?request.getParameter("c_cargo").toUpperCase():"";
        String ref = (request.getParameter("c_ref").toUpperCase());
        String obser = (request.getParameter("c_obse")!=null)?request.getParameter("c_obse").toUpperCase():"";
        //Nuevos Campos
        String libreta = (request.getParameter("c_libreta")!=null)?request.getParameter("c_libreta").toUpperCase():"";
        String expe = (request.getParameter("c_expe")!=null)?request.getParameter("c_expe").toUpperCase():"";
        String lugar = (request.getParameter("c_lugar")!=null)?request.getParameter("c_lugar").toUpperCase():"";
        String barrio = (request.getParameter("c_barrio")!=null)?request.getParameter("c_barrio").toUpperCase():"";
        String estcivil = (request.getParameter("c_estcivil")!=null)?request.getParameter("c_estcivil").toUpperCase():"";
        String sen = (request.getParameter("c_senal")!=null)?request.getParameter("c_senal").toUpperCase():"";
        String nomnemo = (request.getParameter("c_nomnemo")!=null)?request.getParameter("c_nomnemo").toUpperCase():"";
        String reg =  request.getParameter("reg");
        String veto = (request.getParameter("veto")!=null)?request.getParameter("veto").toUpperCase():"S";
        String next = "/jsp/hvida/identidad/identidad.jsp?mensaje=MsgAgregado";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        //veto
        String ob = "";
        String causa = "IRN";
        String fuente = "";
        ///
        int sw=0;
        if ( nom2.trim().equals("") ){
            nom2 = " ";
        }
        else{
            nom2+=" ";
        }
        try{
            Identidad ident = new Identidad();
            ident.setTipo_iden(tipodoc);
            ident.setCedula(doc);
            ident.setId_mims(idmims);
            ident.setNombre((nom.equals(""))?nom1+" "+nom2+ape1+" "+ape2:nom);
            ident.setNom1(nom1);
            ident.setNom2(nom2.trim());
            ident.setApe1(ape1);
            ident.setApe2(ape2);
            ident.setSexo(genero);
            ident.setFechanac(fecha);
            ident.setCodpais(pais);
            ident.setCoddpto(est);
            ident.setCodciu(ciudad);
            ident.setDireccion(dir);
            try{
                ident.setTelefono(tel1);
            }catch (Exception e){}
            ident.setTelefono1(tel2);
            ident.setCelular(cel);
            ident.setE_mail(email);
            ident.setCargo(cargo); ident.getnum1();
            ident.setRef1(ref);
            ident.setObservacion(obser);
            ident.setUsuariocrea(usuario.getLogin());
            ident.setUsuario(usuario.getLogin());
            ident.setBase(usuario.getBase());
            ident.setCia(distrito);
            //nuevos campos
            ident.setLibmilitar(libreta);
            ident.setExpced(expe);
            ident.setLugarnac(lugar);
            ident.setBarrio(barrio);
            ident.setEst_civil(estcivil);
            ident.setSenalParticular(sen);
            ident.setNomnemo(nomnemo);
            ident.setClasificacion(clas);
            ident.setVeto(veto);
            if(usuario.getBase().equalsIgnoreCase("pco") || usuario.getBase().equals("spo")) {
                next = next+"&base=carbon&clas=0000A";
            }
            try{
                //request.setAttribute("msg", "MsgAgregado");
                if (model.identidadService.existeNomnemo(nomnemo)){
                    next = "/jsp/hvida/identidad/identidad.jsp?mensaje=MsgNomNemo&clas="+clas;
                    //request.setAttribute("msg", "MsgNomNemo");
                }
                else{
                    model.identidadService.insertarIdentidad(ident);
                    if(veto.equals("S")){
                        model.vetoSvc.insertarVetoNuevo(usuario.getDstrct(), usuario.getLogin(), ident.getCedula(), usuario.getBase(), causa, ob, veto,fuente,"N");
                    }
                    if(c.equals("C")){
                        next = "/controller?estado=Conductor&accion=Evento&evento=Verificar&identificacion="+doc;
                    }

                    /**
                     *  Modificaci�n: Tito Andr�s Maturana 02.02.2006
                     */
                    if( c.compareTo("0")!=0 && p.compareTo("0")!=0 ){
                        next = "/controller?estado=Conductor&accion=Evento&evento=Verificar&identificacion="+ doc + "&proveedor=OK";
                    } else if( v.compareTo("0")!=0 || p.compareTo("0")!= 0 ){
                        next = "/controller?estado=Proveedor&accion=Buscar&cmd=show&nit=" + ident.getCedula();
                        next += "&sede="+(request.getParameter("sede")!=null ? request.getParameter("sede") : "N");//<!-- 20101111 -->
                    }
                    //*******************autor Leonardo Parody*******************************************************************
                    model.identidadService.archivoTxt("/rmi/Sincronizacion" , ident.getCedula(), usuario);
                    //***********************************************************************************************************
                }
            }
            catch (SQLException e){
                ////System.out.println("Error " + e.getMessage()+ " Codigo "+ e.getErrorCode() );
                sw=1;
            }
            if ( sw == 1 ){
                if ( model.identidadService.existeIdentidadAnulado(doc,usuario.getCia()) ){
                    //*******************autor Leonardo Parody*******************************************************************
                    model.identidadService.archivoTxt("/rmi/Sincronizacion" , ident.getCedula(), usuario);
                    //***********************************************************************************************************
                    model.identidadService.activarIdentidad(ident);
                    //Modificado el 27 - Ene - 2006
                    if(c.equals("C")){
                        next = "/controller?estado=Conductor&accion=Evento&evento=Verificar&identificacion="+doc;
                    }
                    /**
                     *  Modificaci�n: Tito Andr�s Maturana 02.02.2006
                     */
                    if( c.compareTo("0")!=0 && p.compareTo("0")!=0 ){
                        next = "/controller?estado=Conductor&accion=Evento&evento=Verificar&identificacion="+ doc + "&proveedor=OK";
                    } else if( v.compareTo("0")!=0 || p.compareTo("0")!= 0 ){
                        next = "/controller?estado=Proveedor&accion=Buscar&cmd=show&nit=" + ident.getCedula();
                        next += "&sede="+(request.getParameter("sede")!=null ? request.getParameter("sede") : "N");
                    }
                    //*******************autor Leonardo Parody*******************************************************************
                    model.identidadService.archivoTxt("/rmi/Sincronizacion" , ident.getCedula(), usuario);
                    //***********************************************************************************************************
                }
                else{
                    next = "/jsp/hvida/identidad/identidad.jsp?mensaje=MsgErrorIng&clas="+clas;
                    request.setAttribute("sw","0");
                    request.setAttribute("msg", "MsgErrorIng");
                }
            }


            if(reg.equals("no")){
                model.identidadService.buscarPersonaNatural(doc);
                next="/jsp/trafico/conductor/Conductor.jsp?reg=";

            }

            if(pag.equals("S")){
                next=next+"&pag=I";
            }
            ////System.out.println("...................... INGRESAR IDENTIDAD: " + next);


        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }

}
