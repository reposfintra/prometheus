/*
 * NitInsertAction.java
 *
 * Created on 26 de mayo de 2005, 04:11 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class NitInsertAction extends Action{
    
    /** Creates a new instance of NitInsertAction */
    public NitInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/propietarios/propietarioInsert.jsp.jsp";
        
        String cedula = request.getParameter("nit");
        String pnombre = request.getParameter("nit");
        String snombre = request.getParameter("nit");
        String papellido = request.getParameter("nit");
        String sapellido= request.getParameter("nit");
        String dstrct = request.getParameter("nit");
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String base =usuario.getBase();
        
        try{
            Nit nit  = new Nit();
            nit.setNit(cedula);
            nit.setPnombre(pnombre);
            nit.setSnombre(snombre);
            nit.setPapellido(papellido);
            nit.setSapellido(sapellido);
            nit.setDstrct(dstrct);
            nit.setBase(base);
            
            model.nitService.setProp(nit);
            
            if(model.nitService.estaProp()){
                next = "/propietarios/propietarioInsertError.jsp";
                request.setAttribute("error", "#cc0000");
                
            }
            else{
                model.nitService.insertProp();
            }
                
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
