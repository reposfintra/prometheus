/*
 * PlanillaDeleteAction.java
 *
 * Created on 1 de diciembre de 2004, 08:15 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class PlanillaDeleteAction extends Action{
    //2009-09-02
    static Logger logger = Logger.getLogger(PlanillaDeleteAction.class);
    
    /** Creates a new instance of PlanillaDeleteAction */
    public PlanillaDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String planilla=request.getParameter("planilla");
        String mensaje="";
        String fecha="";
        String next="";
        String remesa="";
        float vacpm=0;
        float vtiket=0;
        float vpla=0;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        boolean anular = true;
        Vector consultas =new Vector();
        try{
            
            model.remplaService.buscaRemPla(planilla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rempla= model.remplaService.getRemPla();
                remesa = rempla.getRemesa();
            }
            
            /*
             *PRIMERO SE BUSCAN TODAS LAS REMESAS RELACIONADAS CON LA PLANILLA QUE
             *NO ESTEN RELACIONADAS CON NINGUNA OTRA PLANILLA
             *SI EXISTEN, SE ENVIA UN ERROR, ESPECIFICANDO QUE PRIMERO SE DEBE HACER ALGO
             *CON ESTA REMESA, ESTO SI Y SOLO SI LA OPCION ES LA DE ANULAR PLANILLA
             */
            
            
            model.remesaService.buscarRemesasAlone(planilla);
            int mayor= 1;
            if(request.getParameter("aplanilla")!=null){
                mayor= 0;
            }
            if(model.remesaService.getRemesas().size()>mayor){
                mensaje ="No se puede anular el despacho, Existe mas de una remesa relacionada a la planilla "+planilla+"<br>";
                anular = false;
                
            }
            if(request.getParameter("aplanilla")==null){
                model.remesaService.buscarPlanillaError(remesa);
                if(model.remesaService.getPlasError()!=null){
                    if(model.remesaService.getPlasError().size()>1){
                        anular = false;
                        mensaje= mensaje + "No se puede anular el despacho, Existe mas de una planilla relacionada a la remesa "+remesa;
                    }
                }
            }
            /*else{
                if(model.remesaService.getRemesas().size()>1){
                    anular = false;
                }
            }*/
            if(!anular){
                model.planillaService.setPlanilla(null);
                model.movplaService.setLista(null);
                model.remesaService.setRemesa(null);
                model.remesaService.setRm(null);
                
                if(request.getParameter("colpapel")!=null){
                    next="/colpapel/anularDespacho.jsp?mensaje="+mensaje;
                }
                
                if(request.getParameter("aplanilla")!=null){
                    next = "/colpapel/anularPlaError.jsp";
                }
                
            }
            else{
                logger.info("la planilla es : "+planilla);
                
                if(request.getParameter("colpapel")!=null){
                    model.planillaService.bucarColpapel(planilla);
                }
                else{
                    model.planillaService.buscaPlanilla(planilla);
                }
                
                if(model.planillaService.getPlanilla()!=null){
                    
                    model.movplaService.buscaMovpla(planilla,"01");
                    //PRIMERO BUSCO TODOS LOS CHEQUES IMPRESOS
                    String t_recup ="";
                    Vector cheques= model.movplaService.getLista();
                    if(cheques!=null){
                        for(int i = 0; i<cheques.size(); i++){
                            
                            
                            Movpla cheq = (Movpla) cheques.elementAt(i);
                            logger.info("Se va a anular el cheque No "+cheq.getDocument());
                            
                            
                            String banco [] = cheq.getBanco().split("/");
                            
                            model.movplaService.searchChequePlanilla(cheq.getDocument(),banco.length>0?banco[0]:"",banco.length>1?banco[1]:"",planilla);
                            
                            float valor= 0;
                            if(model.movplaService.getMovPla()!=null){
                                valor = model.movplaService.getMovPla().getVlr();
                                logger.info("Valor del cheque "+valor);
                            }
                            
                            t_recup = request.getParameter(cheq.getDocument())!=null?request.getParameter(cheq.getDocument()):"";
                            logger.info("Tipo de Recuperacion "+t_recup);
                            
                            if(model.cumplidoService.estaCumplida(planilla)){
                                t_recup ="DE";
                            }
                            
                            //SI EL TIPO DE RECUPERACION ES CA SE DEBE ANULAR EL CHEQUE
                            if(t_recup.equals("CA") || t_recup.equals("RC") ){
                                
                                logger.info("CHEQUE ANULADO DE TSP");
                                
                                banco = cheq.getBanco().split("/");
                                try{
                                    model.movplaService.BuscarChequeImpreso(cheq.getDocument(),banco.length>0?banco[0]:"",banco.length>1?banco[1]:"");
                                }catch (SQLException e){
                                    throw new ServletException(e.getMessage());
                                }
                                consultas.add(model.movplaService.anularChequeImpreso());
                                
                                
                                
                            }
                            else if(t_recup.equals("XP")){
                                logger.info("RECUPERACION PARCIAL POR EXTRACTO...");
                                float can =Float.parseFloat(request.getParameter("cant_devol"+cheq.getDocument()));
                                float ajuste =  can *-1;
                              //  model.movplaService.buscaMovpla(planilla,"01");
                                if(model.movplaService.getMovPla()!=null){
                                    
                                    Movpla movpla = model.movplaService.getMovPla();
                                    banco= movpla.getBanco().split("/");
                                    movpla.setAgency_id(movpla.getAgency_id());
                                    movpla.setCurrency(movpla.getCurrency());
                                    movpla.setDstrct(movpla.getDstrct());
                                    movpla.setPla_owner(movpla.getPla_owner());
                                    movpla.setPlanilla(planilla);
                                    movpla.setSupplier(movpla.getSupplier());
                                    movpla.setVlr(ajuste);
                                    movpla.setVlr_for(ajuste);
                                    movpla.setDocument_type("001");
                                    movpla.setConcept_code("15");
                                    movpla.setAp_ind("S");
                                    movpla.setBanco(banco.length>0?banco[0]:"");
                                    movpla.setCuenta(banco.length>1?banco[1]:"");
                                    movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100000));
                                    movpla.setBranch_code(banco.length>0?banco[0]:"");
                                    movpla.setBank_account_no(banco.length>1?banco[1]:"");
                                    
  //                                  consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                    logger.info("Reversion del cheque "+model.movplaService.registrarReversionCheque());
                                    consultas.add(model.movplaService.registrarReversionCheque());
                                }
                                
                            }else if(t_recup.equals("DE")){
                              //  model.movplaService.buscaMovpla(planilla,"01");
                                logger.info("RECUPERACION TOTAL POR EXTRACTO...");
                                if(model.movplaService.getMovPla()!=null){
                                    Movpla movpla = model.movplaService.getMovPla();
                                    banco = movpla.getBanco().split("/");
                                    movpla.setAgency_id(movpla.getAgency_id());
                                    movpla.setCurrency(movpla.getCurrency());
                                    movpla.setDstrct(movpla.getDstrct());
                                    movpla.setPla_owner(movpla.getPla_owner());
                                    movpla.setPlanilla(planilla);
                                    movpla.setSupplier(movpla.getSupplier());
                                    movpla.setVlr(valor*-1);
                                    movpla.setVlr_for(valor*-1);
                                    movpla.setDocument_type("001");
                                    movpla.setConcept_code("15");
                                    movpla.setAp_ind("S");
                                    movpla.setBanco(banco.length>0?banco[0]:"");
                                    movpla.setCuenta(banco.length>1?banco[1]:"");
                                    movpla.setBranch_code(banco.length>0?banco[0]:"");
                                    movpla.setBank_account_no(banco.length>1?banco[1]:"");
                                    movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100000));
//                                    consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                    logger.info("Reversion del cheque "+model.movplaService.registrarReversionCheque());
                                    consultas.add(model.movplaService.registrarReversionCheque());
                                }
                                
                            }
                            else if(t_recup.equals("CO")){
                                Movpla movpla = model.movplaService.getMovPla();
                                banco= movpla.getBanco().split("/");
                                movpla.setBanco(banco.length>0?banco[0]:"");
                                movpla.setCuenta(banco.length>1?banco[1]:"");
                                model.movplaService.setMovPla(movpla);    
                                consultas.add(model.movplaService.registrarReversionCheque());
                            }
                            
                            
                            
                        }
                    }
                    if(cheques==null || cheques.size()==0){
                        logger.info("NO HAY CHEQUES...");
                        Movpla movpla = model.movplaService.getMovPla();
                        if(movpla!=null){
                            String banco [] = movpla.getBanco().split("/");
                            
                            movpla.setAgency_id(movpla.getAgency_id());
                            movpla.setCurrency(movpla.getCurrency());
                            movpla.setDstrct(movpla.getDstrct());
                            movpla.setPla_owner(movpla.getPla_owner());
                            movpla.setPlanilla(planilla);
                            movpla.setSupplier(movpla.getSupplier());
                            movpla.setVlr(movpla.getVlr()*-1);
                            movpla.setVlr_for(movpla.getVlr_for()*-1);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code("01");
                            movpla.setAp_ind("S");
                            movpla.setReanticipo("Y");
                            movpla.setBanco(banco.length>0?banco[0]:"");
                            movpla.setCuenta(banco.length>1?banco[1]:"");
                            movpla.setFecha_migracion("0099-01-01");
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100000));
                            movpla.setBeneficiario(movpla.getBeneficiario());
//                            consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            
                            
                        }
                        
                    }
                  /*  model.movplaService.searchReanticipos(planilla, "01");
                    logger.info("SE ANULAN REANTICIPOS...");
                    Vector lista = model.movplaService.getLista();
                    for(int j=0;j<lista.size();j++){
                        
                        Movpla movpla = (Movpla) lista.elementAt(j);
                        if(movpla!=null){
                            if(!movpla.isImpresa()){
                                String banco []= movpla.getBanco().split("/");
                                
                                movpla.setAgency_id(movpla.getAgency_id());
                                movpla.setCurrency(movpla.getCurrency());
                                movpla.setDstrct(movpla.getDstrct());
                                movpla.setPla_owner(movpla.getPla_owner());
                                movpla.setPlanilla(planilla);
                                movpla.setSupplier(movpla.getSupplier());
                                movpla.setVlr(movpla.getVlr()*-1);
                                movpla.setVlr_for(movpla.getVlr_for()*-1);
                                movpla.setDocument_type("001");
                                movpla.setConcept_code("01");
                                movpla.setAp_ind("S");
                                movpla.setBanco(banco.length>0?banco[0]:"");
                                movpla.setCuenta(banco.length>1?banco[1]:"");
                                movpla.setReanticipo("Y");
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+110000));
                                movpla.setBeneficiario(movpla.getBeneficiario());
                                consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            }
                        }
                    }
                    */
                    Planilla pla= model.planillaService.getPlanilla();
                    pla.setBase(usuario.getBase());
                    pla.setDespachador(usuario.getLogin());
                    pla.setDistrito(usuario.getDstrct());
                    pla.setCausa(request.getParameter("causas")!=null?request.getParameter("causas"):"");
                    pla.setT_recuperacion(t_recup!=null?t_recup:"");
                    pla.setObservacion(request.getParameter("observacion"));
                    pla.setNumpla(planilla);
                    vpla=pla.getVlrpla()*-1;
                    
                    //Anulo la planilla
                    consultas.add(model.planillaService.anularPlanilla(pla));
                    
                    //Anulo la planilla auxiliar
                    Plaaux plaaux = new Plaaux();
                    plaaux.setPlanilla(planilla);
                    consultas.add(model.plaauxService.anularPlanilla(plaaux));
                    
                    // model.remplaService.buscaRemPla(planilla);
                    if(model.remplaService.getRemPla()!=null){
                        
                        //Anulo la rempla
                        RemPla rempla= model.remplaService.getRemPla();
                        remesa = rempla.getRemesa();
                        consultas.add(model.remplaService.anularListaPlanilla(planilla));
                        if(request.getParameter("aplanilla")==null){
                            if(request.getParameter("remesaRel")!=null){
                                remesa = request.getParameter("remesaRel").equals("") || request.getParameter("remesaRel").equals("null") ?remesa:request.getParameter("remesaRel");
                            }
                            //Anulo la remesa
                            logger.info("Remesa a anular: "+remesa);
                            Remesa rem = new Remesa();
                            rem.setNumRem(remesa);
                            rem.setDistrito(usuario.getDstrct());
                            rem.setUsuario(usuario.getLogin());
                            rem.setBase(usuario.getBase());
                            consultas.add(model.remesaService.anularRemesa(rem));
                            consultas.add(model.remesaService.anularMovrem(remesa,usuario));
                            
                            /***************************************************************************
                             * ESTO SE HACE POR SI ACA LA REMESA TIENE MAS DE UNA PLANILLA RELACIONADA *
                             * SE DEBE ANULAR LA RELACION.                                             *
                             ***************************************************************************/
                            
                            List planillas= model.remesaService.buscarPlanillas(remesa);
                            Iterator it=planillas.iterator();
                            while (it.hasNext()){
                                
                                Planilla plan = (Planilla) it.next();
                                String numpla = plan.getNumpla();
                                logger.info("Encontre relacionada la planilla "+numpla);
                                
                                
                                //AHORA BUSCO LA LISTA DE REMESAS Q VOY A ANULAR
                                //PARA REASIGNARLE LOS PORCENTAJES
                                float valortotalR = 0;
                                List list = model.planillaService.buscarRemesas(numpla,remesa);
                                Iterator itRem = list.iterator();
                                while(itRem.hasNext()){
                                    rem =  (Remesa) itRem.next();
                                    if(!rem.getNumrem().equals(remesa)){
                                        valortotalR  =valortotalR+ rem.getVlrRem();
                                    }
                                    
                                }
                                System.out.println("Valor total de remesas "+valortotalR);
                                
                                //AHORA LE ASIGNO A CADA UNA EL PROCENTAJE
                                
                                int totalPorcent =0;
                                System.out.println("Lista "+list.size());
                                for(int k=0; k<list.size()-1; k++){
                                    rem = (Remesa) list.get(k);
                                    model.remesaService.buscaRemesa(rem.getNumrem());
                                    Remesa r = model.remesaService.getRemesa();
                                    if(!rem.getNumrem().equals(remesa)){
                                        
                                        double vlrrem = r.getVlrRem();
                                        System.out.println("Valor remesa "+rem.getNumrem()+" - "+vlrrem);
                                        double division = (vlrrem/valortotalR)*100;
                                        int porcentaje = (int)division;
                                        System.out.println("Procentaje "+rem.getNumrem()+" - "+porcentaje);
                                        
                                        totalPorcent = totalPorcent + porcentaje;
                                        
                                        model.remplaService.buscaRemPla(numpla,r.getNumrem());
                                        if(model.remplaService.getRemPla()!=null){
                                            RemPla rp = model.remplaService.getRemPla();
                                            rp.setPorcent(porcentaje);
                                            consultas.add(model.remplaService.updateRemPla(rp));
                                            
                                        }
                                    }
                                }
                                System.out.println("Total porcent "+totalPorcent);
                                //ULTIMO ITEM
                                if(list.size()>0){
                                    rem = (Remesa) list.get(list.size()-1);
                                    model.remesaService.buscaRemesa(rem.getNumrem());
                                    Remesa r = model.remesaService.getRemesa();
                                    if(!rem.getNumrem().equals(remesa)){
                                        int porcentaje = 100-totalPorcent;
                                        model.remplaService.buscaRemPla(numpla,r.getNumrem());
                                        if(model.remplaService.getRemPla()!=null){
                                            RemPla rp = model.remplaService.getRemPla();
                                            rp.setPorcent(porcentaje);
                                            consultas.add(model.remplaService.updateRemPla(rp));
                                            
                                        }
                                    }
                                }
                                
                                
                                model.remplaService.buscaRemPla(numpla,remesa);
                                if(model.remplaService.getRemPla()!=null){
                                    
                                    rempla= model.remplaService.getRemPla();
                                    consultas.add(model.remplaService.anularPlanilla(rempla));
                                }
                            }
                        }
                    }
                    
                    
                    //ANULO LAS RELACIONES DE REMPLA
                    model.remplaService.buscaRemPla(planilla,remesa);
                    if(model.remplaService.getRemPla()!=null){
                        
                        RemPla rempla= model.remplaService.getRemPla();
                        model.remplaService.anularPlanilla(rempla);
                    }
                    
                    
                    
                    
                    
                    //Anulo los planilla_tiempos
                    Planilla_Tiempo pt = new Planilla_Tiempo();
                    pt.setPla(planilla);
                    consultas.add(model.pla_tiempoService.anularPlanilla(pt));
                    
                    
                    
                    model.sjextrafleteService.listaCostosPlanilla(planilla,"E");
                    Vector extrafletes = model.sjextrafleteService.getC_reembolsables();
                    for(int i=0; i<extrafletes.size();i++){
                        
                        SJExtraflete sjE = (SJExtraflete)extrafletes.elementAt(i);
                        sjE.setValor_costo(sjE.getValor_costo()*-1);
                        sjE.setValor_ingreso(sjE.getValor_ingreso()*-1);
                        model.movplaService.buscaMovpla(planilla,sjE.getCod_extraflete());
                        if(model.movplaService.getMovPla()!=null){
                            logger.info("Anulamos en extraflete "+sjE.getCod_extraflete());
                            Movpla movpla = model.movplaService.getMovPla();
                            
                            Movpla movplaP= new Movpla();
                            movpla.setAgency_id(movpla.getAgency_id());
                            movpla.setCurrency(movpla.getCurrency());
                            movpla.setDstrct(movpla.getDstrct());
                            movpla.setPla_owner(movpla.getPla_owner());
                            movpla.setPlanilla(planilla);
                            movpla.setSupplier(movpla.getSupplier());
                            movpla.setVlr(movpla.getVlr()*-1);
                            movpla.setVlr_for(movpla.getVlr_for()*-1);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code(sjE.getCod_extraflete());
                            movpla.setAp_ind("V");
                            movpla.setCantidad(sjE.getCantidad());
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100000000+i));
//                            consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            model.sjextrafleteService.setSj(sjE);
                            consultas.add(model.sjextrafleteService.insertarCostoReembolsable());
                            
                        }
                    }
                    model.sjextrafleteService.listaCostosPlanilla(planilla,"R");
                    extrafletes = model.sjextrafleteService.getC_reembolsables();
                    for(int i=0; i<extrafletes.size();i++){
                        
                        SJExtraflete sjE = (SJExtraflete)extrafletes.elementAt(i);
                        sjE.setValor_costo(sjE.getValor_costo()*-1);
                        sjE.setValor_ingreso(sjE.getValor_ingreso()*-1);
                        sjE.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11+i));
                        model.sjextrafleteService.setSj(sjE);
                        
                        consultas.add(model.sjextrafleteService.insertarCostoReembolsable());
                        
                    }
                    
                    // consultas.add(model.sjextrafleteService.anularCosto(planilla));
                    //ANULO LOS ANTICIPOS DINAMICOS..
                    
                    model.anticiposService.vecAnticipos(usuario.getBase(),pla.getSj());
                    Vector ants = model.anticiposService.getAnticipos();
                    logger.info("SE ENCONTRARON  "+ants.size()+" descuentos por anular");
                    for (int k=0 ; k<ants.size();k++){
                        Anticipos ant = (Anticipos) ants.elementAt(k);
                        model.anticiposService.searchAnticipos(usuario.getDstrct(), ant.getAnticipo_code(), pla.getSj());
                        if(model.anticiposService.getAnticipo()!=null){
                            ant = model.anticiposService.getAnticipo();
                            
                            model.movplaService.buscaMovpla(planilla,ant.getAnticipo_code());
                            if(model.movplaService.getMovPla()!=null){
                                logger.info("Anulamos el descuento "+ant.getAnticipo_code());
                                Movpla movpla = model.movplaService.getMovPla();
                                
                                Movpla movplaP= new Movpla();
                                movpla.setAgency_id(movpla.getAgency_id());
                                movpla.setCurrency(movpla.getCurrency());
                                movpla.setDstrct(movpla.getDstrct());
                                movpla.setPla_owner(movpla.getPla_owner());
                                movpla.setPlanilla(planilla);
                                movpla.setSupplier(movpla.getSupplier());
                                movpla.setVlr(movpla.getVlr()*-1);
                                movpla.setVlr_for(movpla.getVlr_for()*-1);
                                movpla.setDocument_type("001");
                                movpla.setConcept_code(ant.getAnticipo_code());
                                movpla.setAp_ind("S");
                                movpla.setBanco("");
                                movpla.setCuenta("");
                                movpla.setFecha_migracion("1900-01-01");
                                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+123425));
//                                consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                
                                
                            }
                            
                            
                            
                        }
                        
                    }
                    Precinto p = new Precinto();
                    p.setNumpla(planilla);
                    p.setUser_update(usuario.getLogin());
                    p.setTipo_Documento("Precinto");
                    p.setAgencia(usuario.getId_agencia());
                    consultas.add(model.precintosSvc.desmarcar(p));
                    pla = model.planillaService.getPlanilla();
                    if(pla.getPrinter_date2()!=null){
                        if(pla.getPrinter_date2().equals("0099-01-01 00:00:00")){
                            p = new Precinto();
                            p.setNumpla(planilla);
                            p.setUser_update(usuario.getLogin());
                            p.setTipo_Documento("Sticker");
                            p.setAgencia(usuario.getId_agencia());
                            consultas.add(model.precintosSvc.desmarcar(p));
                        }
                    }
                    
                    //ESTO SE HACE SOLO SI ES CARBON
                    if(request.getParameter("CGA")==null){
                        logger.info("ES CARBON...");
                        //Creo lo movplanilla Negativos.
                        //Primero creo el movplanilla con el valor de la planilla negativo.
                        model.movplaService.buscaMovpla(planilla,"01");
                        if(model.movplaService.getMovPla()!=null){
                            Movpla movpla = model.movplaService.getMovPla();
                            String branch_code="";
                            String bank_account="";
                            String vec []= movpla.getBanco().split("/");
                            if(vec.length>0)
                                branch_code=vec[0];
                            if(vec.length>1)
                                bank_account=vec[1];
                            Movpla movplaP= new Movpla();
                            movpla.setAgency_id(movpla.getAgency_id());
                            movpla.setCurrency(movpla.getCurrency());
                            movpla.setDstrct(movpla.getDstrct());
                            movpla.setPla_owner(movpla.getPla_owner());
                            movpla.setPlanilla(planilla);
                            movpla.setSupplier(movpla.getSupplier());
                            movpla.setVlr(vpla);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code("06");
                            movpla.setAp_ind("V");
                            movpla.setBranch_code(branch_code);
                            movpla.setBank_account_no("");
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+1000));
//                            consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            
                            
                        }
                        //si no esta impresa creo los mov de planilla del acpm y los tiket
                        fecha=""+pla.getPrinter_date();
                        
                        //Creo el movpla del Anticipo
                        model.movplaService.buscaMovpla(planilla,"01");
                        if(model.movplaService.getMovPla()!=null){
                            Movpla movpla = model.movplaService.getMovPla();
                            
                            Movpla movplaP= new Movpla();
                            movpla.setAgency_id(movpla.getAgency_id());
                            movpla.setCurrency(movpla.getCurrency());
                            movpla.setDstrct(movpla.getDstrct());
                            movpla.setPla_owner(movpla.getPla_owner());
                            movpla.setPlanilla(planilla);
                            movpla.setSupplier(movpla.getSupplier());
                            movpla.setVlr(movpla.getVlr()*-1);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code("01");
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+1000000));
                            movpla.setAp_ind("S");
//                            consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            
                            
                        }
                        
                        //Creo el movpla del Acpm
                        model.movplaService.buscaMovpla(planilla,"02");
                        if(model.movplaService.getMovPla()!=null){
                            Movpla movpla = model.movplaService.getMovPla();
                            
                            Movpla movplaP= new Movpla();
                            movpla.setAgency_id(movpla.getAgency_id());
                            movpla.setCurrency(movpla.getCurrency());
                            movpla.setDstrct(movpla.getDstrct());
                            movpla.setPla_owner(movpla.getPla_owner());
                            movpla.setPlanilla(planilla);
                            movpla.setSupplier(movpla.getSupplier());
                            movpla.setVlr(movpla.getVlr()*-1);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code("02");
                            movpla.setAp_ind("S");
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+10000000));
//                            consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            
                            
                        }
                        
                        //Creo el movpla de los tiquetes
                        model.movplaService.buscaMovpla(planilla,"03");
                        if(model.movplaService.getMovPla()!=null){
                            Movpla movpla = model.movplaService.getMovPla();
                            
                            Movpla movplaP= new Movpla();
                            movpla.setAgency_id(movpla.getAgency_id());
                            movpla.setCurrency(movpla.getCurrency());
                            movpla.setDstrct(movpla.getDstrct());
                            movpla.setPla_owner(movpla.getPla_owner());
                            movpla.setPlanilla(planilla);
                            movpla.setSupplier(movpla.getSupplier());
                            movpla.setVlr(movpla.getVlr()*-1);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code("03");
                            movpla.setAp_ind("S");
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100000000));
//                            consultas.add(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            
                            
                            
                        }
                    }
                    
                    consultas.add(model.discrepanciaService.desmarcarProductos(planilla));
                    //SE EJECUTA TODO EL BATCH DE INSERTS.
                    for(int i =0; i<consultas.size();i++){
                        String sql = (String)consultas.elementAt(i);
                        logger.info(sql);
                    }
                    model.despachoService.insertar(consultas);
                }
                
                model.planillaService.setPlanilla(null);
                model.movplaService.setLista(null);
                model.remesaService.setRemesa(null);
                model.remesaService.setRm(null);
                next="/anularDespacho/buscarPlanilla.jsp";
                
                if(request.getParameter("colpapel")!=null){
                    mensaje="Se anulo con exito la planilla "+planilla+" y remesa "+remesa;
                    next="/colpapel/anularDespacho.jsp?mensaje="+mensaje;
                }
                if(request.getParameter("aplanilla")!=null){
                    mensaje="Se anulo con exito la planilla "+planilla;
                    next="/colpapel/anularPlanilla.jsp?mensaje="+mensaje;
                }
                System.out.println("Next "+next);
                //ANULO LA PLANILLA EN TRAFICO Y EN INGRESO TRAFICO
                model.planillaService.anulaTrafico(planilla);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
