/***************************************
* Nombre Clase ............. CuotasManagerAction.java
* Descripci�n  .. . . . . .  Realiza los eventos de la consulta de cuotas
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  16/02/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/




package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.HMsf265Cuotas;



public class CuotasManagerAction extends Action{
    
   
    public void run() throws ServletException, InformationException {
        
         try{
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
      
        
            String   next       = "/jsp/cxpagar/prestamos/consultas/ListadoCuotas.jsp?msj=";
            String   msj        = "";
            
            String   distrito   = usuario.getDstrct(); 
            String   user       = usuario.getLogin();
            String   nitUser    = usuario.getCedula();
            
            String   opcion     = request.getParameter("evento");
            
            String   ADD        = "ADD";
            String   MIGRAR     = "MIGRAR";
            
            if(opcion!=null){        
                
                
                
               
             // BUSCAR CUOTAS :
                if(opcion.equals("SEARCH") ){
                    model.CuotasSvc.setEventoBtn( MIGRAR );
                    String   tercero    = request.getParameter("tercero");
                    String   fecha      = request.getParameter("fecha");
                    String   cantidad   = request.getParameter("cantidad");
                    model.CuotasSvc.searchCuotas(distrito, tercero, fecha,  Integer.parseInt(cantidad));
                    List lista = model.CuotasSvc.getListCuotas();
                    if(lista==null || lista.size()==0)
                        msj = " No se encontraron registros de cuotas seg�n parametros dados...";
                }
                
                
                
                if(opcion.equals("SEARCH_PROVEEDOR") ){
                    model.CuotasSvc.setEventoBtn( ADD );
                    String   tercero       = request.getParameter      ("tercero");
                    String[] proveedores   = request.getParameterValues("proveedoresFiltro");
                    model.CuotasSvc.searchPrestamosProveedor(distrito, tercero, proveedores);
                    List lista = model.CuotasSvc.getListPrestamos();
                    if(lista==null || lista.size()==0)
                        msj    = " No se encontraron registros de prestamos seg�n parametros dados...";
                    else
                        next   = "/jsp/cxpagar/prestamos/consultas/ListadoMigracionCuotasProveedor.jsp";
                }
                
                if(opcion.equals("VIEW_AMORT") ){
                    model.CuotasSvc.setEventoBtn( ADD );
                    int   id    = Integer.parseInt( request.getParameter      ("id") );
                    ResumenPrestamo  ptm = model.CuotasSvc.getPrestamoPropietario(id);
                    model.CuotasSvc.setListCuotas( ptm.getAmortizaciones() );
                }
                
                if(opcion.equals("ADD") ){
                      int total    =  Integer.parseInt( request.getParameter("total") );
                      for(int i =1;i<=total;i++){
                           int parametro = Integer.parseInt(request.getParameter("parametro"+i));
                           model.CuotasSvc.active(parametro);
                      } 
                      model.CuotasSvc.addMigracion();
                 }
                
                
                if(opcion.equals("VIEW_CUOTAS_SELECT") ){
                     List listaSelect = model.CuotasSvc.getListMigracion();
                     if ( listaSelect.size()>0){
                          model.CuotasSvc.setEventoBtn( MIGRAR );
                          model.CuotasSvc.setListCuotas( listaSelect );
                          msj  = " Cuotas seleccionadas para ser migradas a Mims.";
                     }
                     else{
                           next   = "/jsp/cxpagar/prestamos/consultas/ListadoMigracionCuotasProveedor.jsp";
                           next  += "?msj=Deber� seleccionar las cuotas para migrar por cada prestamo....";
                     }
                }
                
                
                
                
                
                
             // MIGRAR CUOTAS A MIMS
                if(opcion.equals("MIGRAR") ){                    
                      int total    =  Integer.parseInt( request.getParameter("total") );
                      for(int i =1;i<=total;i++){
                          //System.out.println("paso en "+i); 
                           int parametro = Integer.parseInt(request.getParameter("parametro"+i));
                           model.CuotasSvc.active(parametro);
                      }
                      model.CuotasSvc.formarMigracion();  
                      HMsf265Cuotas hilo = new HMsf265Cuotas();
                      hilo.start(model, usuario, nitUser);
                      msj = "Proceso de Migraci�n de cuotas se ha iniciado...";
                }
                
                if(opcion.equals("MIGRAR_PROPIETARIO") ){                    
                      HMsf265Cuotas hilo = new HMsf265Cuotas();
                      hilo.start(model, usuario, nitUser);
                      msj = "Proceso de Migraci�n de cuotas se ha iniciado...";
                }
                
                
                
                
                
                
              
             // ACTUALIZAR FECHA PAGO A UNA CUOTA
                if(opcion.equals("UPDATE_FECHA") ){
                    int id             =  Integer.parseInt( request.getParameter("idPrestamo") );
                    String newfecha    =  request.getParameter("newFecha");
                    Amortizacion amort =  model.CuotasSvc.getAmortizacion(id);
                    amort.setFechaPago(newfecha);   
                    model.CuotasSvc.updateFechaPago(amort, user);
                    msj =  "Se ha actualizado la fecha de pago para la cuota " + amort.getItem() + " del prestamo " + amort.getPrestamo();
                }
                
                
                
            // ACTUALIZAR FECHA A TODA LA LISTA
                if(opcion.equals("UPDATE_ALL") ){
                    String newfecha    =  request.getParameter("newfecha");
                    List lista = model.CuotasSvc.getListCuotas();
                    for(int i=0;i<lista.size();i++){
                        Amortizacion  amort = (Amortizacion) lista.get(i);
                        amort.setFechaPago(newfecha); 
                        model.CuotasSvc.updateFechaPago(amort, user);
                    }
                    msj = "Se han actualizado las fechas de pago para las cuotas";
                }
                
                
                
            }
            
            
             RequestDispatcher rd = application.getRequestDispatcher(next + msj );
             if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next + msj );
             rd.forward(request, response);            
            
        }catch(Exception e){
            throw new ServletException( e.getMessage() );
        }
        
    }
    
}
