/*************************************************************************
 * Nombre                       ClientactBuscarAction.java               *
 * Descripci�n                  Clase Action para buscar las actividades *
 *                              de los cliente                           *
 *                              actividad                                *
 * Autor.                       Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha                        29 de agosto de 2005, 11:23 PM           * 
 * Versi�n                      1.0                                      * 
 * Coyright                     Transportes Sanchez Polo S.A.            *
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes
 */
public class ClientactBuscarAction extends Action {
    
    /** Creates a new instance of ClientactBuscarAction */
    public ClientactBuscarAction() {
    }
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/clienteactividad/clienteactividadMod.jsp";
        String codact = request.getParameter("codact");
        String codcli = request.getParameter("codcli");
        String tipo = request.getParameter("tipo");
        try{
            model.clientactSvc.buscarClientAct(codcli, codact, tipo, usuario.getCia());
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
