
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mcastillo
 */

package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.VentasEdsDAO;
import com.tsp.operation.model.DAOS.impl.VentasEdsImpl;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.Usuario;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;


public class VentasEdsAction extends Action{
    
    Usuario usuario = null;
    private final int CARGAR_OPCIONES_VENTA_EDS = 0;    
    private final int CARGAR_JSON_GUARDAR_VENTA_EDS = 1;    

    private VentasEdsDAO dao;

   
    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new VentasEdsImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_OPCIONES_VENTA_EDS: 
                    cargarMenuRecaudo();
                    break;        
                case CARGAR_JSON_GUARDAR_VENTA_EDS: 
                    cargarJsonGuardarVentaEDS();
                    break;        
            }
        } catch (Exception e) {
        }

    }
    
    private void cargarMenuRecaudo() {
        try {
            Usuario us = (Usuario) request.getSession().getAttribute("Usuario");
            String usuario = (us.getLogin() != null) ? us.getLogin() : "";

            ArrayList<MenuOpcionesModulos> listOpcionesVentaEds = dao.cargarMenuVentasEds(usuario, us.getDstrct());
            Gson gson = new Gson();
            String json;
            json = gson.toJson(listOpcionesVentaEds);
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
      private void cargarJsonGuardarVentaEDS() {
                 
        try{
            String listado = request.getParameter("listado");
            String idEstacion = request.getParameter("id_estacion");
            String nombre_eds = request.getParameter("nombre_eds");
            String idmanifiesto = request.getParameter("id_manifiesto");
            String planilla = request.getParameter("planilla");
            String kilometraje = request.getParameter("kilometraje");
            
            String json = dao.generarJsonGuardarVentaEds(Integer.parseInt(idEstacion), nombre_eds, Integer.parseInt(idmanifiesto), planilla, Integer.parseInt(kilometraje),listado);          
            this.printlnResponse(json, "application/json;"); 
           
        }catch(Exception e){
            e.printStackTrace();
        }
    }      
    
    
    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
}
