/*
 * ModificarInsetAction.java
 *
 * Created on 30 de noviembre de 2004, 08:15 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ModificarInsertAction extends Action {
    
    static Logger logger = Logger.getLogger(SalidaValidarAction.class);
    String grupo="ninguno";
    float valor=0;
    float costo=0;
    float maxant=0;
    String dest="";
    String origen="";
    float acpmmax=0;
    float peajeamax=0;
    float peajebmax=0;
    float peajecmax=0;
    float valorT=0;
    float valorA=50;
    Remesa rem;
    Stdjobcosto std;
    Usuario usuario;
    float peajea;
    float peajeb;
    float peajec;
    /** Creates a new instance of ModificarInsetAction */
    public ModificarInsertAction() {
    }
    
    public void buscarRemesa(String pla)throws ServletException{
        try{
            model.remplaService.buscaRemPla(pla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rempla = model.remplaService.getRemPla();
                String no_rem= rempla.getRemesa();
                model.remesaService.buscaRemesa(no_rem);
                if(model.remesaService.getRemesa()!=null){
                    rem=model.remesaService.getRemesa();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    
    public void buscarValor(String sj, String placa, float pesoreal)throws ServletException{
        
        try{
            model.stdjobcostoService.buscaStdJobCosto(sj);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto std = model.stdjobcostoService.getStandardCosto();
                maxant=std.getporcentaje_maximo_anticipo();
                costo = std.getUnit_cost();
                valor= pesoreal * costo;
            }
            
            model.plkgruService.buscaPlkgru(placa,sj);
            if(model.plkgruService.getPlkgru()!=null){
                Plkgru plk = model.plkgruService.getPlkgru();
                grupo=plk.getGroupcode();
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    Stdjobplkcosto std = model.stdjobplkcostoService.getStd();
                    costo = std.getUnit_cost();
                    valor= pesoreal * costo;
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    public void buscarValores(String proveedorA, float cantA, String proveedorT, float cantT)throws ServletException{
        
        try{
            String [] nitcod = null;
            nitcod = proveedorA.split("/");
            String sucursal = "";
            if(nitcod.length>1){
                sucursal = nitcod[1];
            }
            model.proveedoracpmService.buscaProveedor(nitcod[0],sucursal);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm pacpm = model.proveedoracpmService.getProveedor();
                valorA= cantA;
                if(pacpm.getTipo().equals("G")){
                    float vacpm= pacpm.getValor();
                    valorA= vacpm * cantA;
                }
                
            }
            float valora=0, valorb=0, valorc=0;
            model.peajeService.buscar("A");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valora = vt * peajea;
            }
            model.peajeService.buscar("B");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valorb = vt * peajeb;
            }
            
            model.peajeService.buscar("C");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valorc = vt * peajec;
            }
            
            valorT = valora+valorb+valorc;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    public void run() throws ServletException, InformationException {
        
        
        
        String next="/ModificarDespacho/InicioModificar.jsp";
        
        String placa = request.getParameter("placa").toUpperCase();
        String standar = request.getParameter("standar");
        String cedula = request.getParameter("conductor");
        String trailer = request.getParameter("trailer");
        String orden = request.getParameter("orden");
        float peso = Float.parseFloat(request.getParameter("peso"));
        String planilla = request.getParameter("planilla").toUpperCase();
        float pesoVacio = Float.parseFloat(request.getParameter("pesov"));
        float anticipo = Float.parseFloat(request.getParameter("anticipo"));
        String proveedorA= request.getParameter("proveedorA");
        String vecA[] = proveedorA.split("/");
        String sucursalA = "";
        if(vecA.length>1){
            sucursalA = vecA[1];
        }
        
        float gacpm=Float.parseFloat(request.getParameter("acpm"));
        String proveedorAcpm= request.getParameter("proveedorAcpm");
        String vecACPM[] = proveedorAcpm.split("/");
        String sucursalACPM = "";
        if(vecACPM.length>1){
            sucursalACPM = vecACPM[1];
        }
        peajea = Float.parseFloat(request.getParameter("peajea"));
        peajeb = Float.parseFloat(request.getParameter("peajeb"));
        peajec = Float.parseFloat(request.getParameter("peajec"));
        String proveedorT = request.getParameter("tiquetes");
        String vecT[] = proveedorT.split("/");
        String sucursalT = "";
        if(vecT.length>1){
            sucursalT = vecT[1];
        }
        
        String nombre="";
        float pesoMax=0;
        float pesoreal=0;
        float full_w=0;
        int sw=0;
        String nit="";
        
        ////System.out.println("Vamos a modificar la planilla.."+planilla);
        
        HttpSession session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        try{
            
            model.tService.crearStatement();
            model.placaService.buscaPlaca(request.getParameter("placa"));
            if(model.placaService.getPlaca()!=null){
                Placa placaS = model.placaService.getPlaca();
                nit=placaS.getPropietario();
                
            }
            //Modifico la planilla
            pesoreal=peso-pesoVacio;
            this.buscarValor(standar, placa, pesoreal);
            model.planillaService.buscaPlanilla(planilla);
            
            Planilla pla= model.planillaService.getPlanilla();
            pla.setNitpro(nit);
            pla.setNumpla(planilla);
            pla.setPlatlr(trailer.toUpperCase());
            pla.setPlaveh(placa.toUpperCase());
            pla.setCedcon(cedula);
            pla.setOrden_carga(orden);
            pla.setPesoreal(pesoreal);
            pla.setVlrpla(valor);
            pla.setGroup_code("");
            model.tService.getSt().addBatch(model.planillaService.updatePlanilla(pla));
            
            this.buscarRemesa(planilla);
            //Hago el update en la remesa.
            rem.setPesoReal(pesoreal);
            model.tService.getSt().addBatch(model.remesaService.updateRemesa(rem));
            
            String [] nitcod = null;
            nitcod = proveedorAcpm.split("/");
            String sucursal = "";
            if(nitcod.length>1){
                sucursal = nitcod[1];
            }
            //Hago el update de la planilla aux.
            Plaaux plaaux= new Plaaux();
            plaaux.setPlanilla(planilla);
            plaaux.setAcpm_gln(gacpm);
            plaaux.setAcpm_supplier(nitcod[0]);
            plaaux.setAcpm_code(sucursal);
            plaaux.setCreation_user(usuario.getLogin());
            plaaux.setEmpty_weight(pesoVacio);
            plaaux.setTicket_a((float)peajea);
            plaaux.setTicket_b((float)peajeb);
            plaaux.setTicket_c((float)peajec);
            plaaux.setTicket_supplier(vecT[0]);
            plaaux.setFull_weight(peso);
            plaaux.setTiket_code(vecT[1]);
            
            model.tService.getSt().addBatch(model.plaauxService.updatePlaaux(plaaux));
            
            float totalPeajes = peajea+peajeb+peajec;
            this.buscarValores(proveedorAcpm, gacpm, proveedorT, totalPeajes);
            
            //Modifico la movpla
            Movpla movpla= new Movpla();
            movpla.setPla_owner(pla.getNitpro());
            movpla.setPlanilla(planilla);
            movpla.setSupplier(pla.getPlaveh());
            movpla.setVlr(anticipo);
            movpla.setConcept_code("01");
            movpla.setProveedor(vecA[0]);
            movpla.setSucursal(sucursalA);
            model.tService.getSt().addBatch(model.movplaService.updateMovPla(movpla));
            
            movpla= new Movpla();
            movpla.setPla_owner(pla.getNitpro());
            movpla.setPlanilla(planilla);
            movpla.setSupplier(pla.getPlaveh());
            movpla.setVlr(valorA);
            movpla.setConcept_code("02");
            movpla.setProveedor(vecACPM[0]);
            movpla.setSucursal(sucursalACPM);
            model.tService.getSt().addBatch(model.movplaService.updateMovPla(movpla));
            
            movpla= new Movpla();
            movpla.setPla_owner(pla.getNitpro());
            movpla.setPlanilla(planilla);
            movpla.setSupplier(pla.getPlaveh());
            movpla.setVlr(valorT);
            movpla.setConcept_code("03");
            movpla.setProveedor(vecT[0]);
            movpla.setSucursal(sucursalT);
            model.tService.getSt().addBatch(model.movplaService.updateMovPla(movpla));
            
            model.tService.execute();
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
