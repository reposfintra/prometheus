/*
 * ReportePPPAction.java
 *
 * Created on 12 de marzo de 2007, 03:47 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  equipo
 */
import javax.servlet.http.*;
import javax.servlet.*;
import com.tsp.util.Util;
import com.tsp.operation.model.threads.HReportePPP;
import com.tsp.operation.model.beans.Usuario;

public class ReportePPPAction extends  Action {
    
    /** Creates a new instance of ReportePPPAction */
    public ReportePPPAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            String      opcion  = Util.coalesce( request.getParameter("opcion"), "");
            
            String fiRNF  = Util.coalesce( request.getParameter("fecIniRNF") , "");
            String ffRNF  = Util.coalesce( request.getParameter("fecFinRNF") , "");
            String fiRFNP = Util.coalesce( request.getParameter("fecIniRFNP"), "");
            String ffRFNP = Util.coalesce( request.getParameter("fecFinRFNP"), "");
            String fiRP   = Util.coalesce( request.getParameter("fecIniRFP")  , "");
            String ffRP   = Util.coalesce( request.getParameter("fecFinRFP")  , "");
            String next   = "/jsp/general/reportes/ppp/generar.jsp";
            
            if (opcion.equalsIgnoreCase("Generar")){
                HReportePPP h = new HReportePPP();
                h.start(model, fiRNF, ffRNF, fiRFNP, ffRFNP, fiRP, ffRP, usuario);
                request.setAttribute("msg","Su proceso ha iniciado con exito, verfique el log de proceso para verifcar su estado.");
            }
            
            this.dispatchRequest(next);
        } catch (Exception ex){
            throw new ServletException(ex.getMessage());
        }
    }
    
}
