/*
 * ChequeBuscarAction.java
 *
 * Created on 17 de junio de 2005, 05:07 PM
 */
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import java.util.regex.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  kreales
 */
public class ChequeBuscarAction  extends Action{
    static Logger logger = Logger.getLogger(ChequeBuscarAction.class);
    
    /** Creates a new instance of ChequeBuscarAction */
    public ChequeBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/cheques/mostrarCheque.jsp";
        String cheque = request.getParameter("cheque");
        
        logger.info("ANULAR CHEQUE Y REEMPLAZAR "+cheque);
        /*JuanM 25-11-05*/
        String opcion                 = request.getParameter("Opcion")!=null?request.getParameter("Opcion"):"";
        String banc                   = (request.getParameter("banco")!=null)?request.getParameter("banco"):"";
        String banck                  = (request.getParameter("banck")!=null)?request.getParameter("banck"):"";
        String sucursalbanco          = (request.getParameter("sucursalbanco")!=null)?request.getParameter("sucursalbanco"):"";
        
        logger.info("OPCION= "+opcion);
        logger.info("banc= "+banc);
        logger.info("banck= "+banck);
        logger.info("sucursalbanco= "+sucursalbanco);
        /*fin*/
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            /*Juan M. 25-11-05*/
            if(opcion.equals("BuscarS")){
                next = "/cheques/buscarCheque.jsp?banco="+banc;
            }
            /*fin*/
            else{
                if(request.getParameter("noexiste")==null){
                    logger.info("Anulamos un cheque y lo reemplazamos.");
                    model.movplaService.searchCheque(cheque,banck,sucursalbanco);
                    if(model.movplaService.getMovPla()==null){
                        next = "/cheques/buscarCheque.jsp?mensaje=El Cheque "+cheque+" esta anulado o no existe.";
                    }
                    else{
                        if(request.getParameter("anular")!=null){
                            Movpla mp = model.movplaService.getMovPla();
                            mp.setCreation_user(usuario.getLogin());
                            model.movplaService.setMovPla(mp);
                            model.tService.crearStatement();
                            model.tService.getSt().addBatch(model.movplaService.anularCheque());
                            model.tService.execute();
                            
                            next = "/controller?estado=Menu&accion=ImpresionCheques&opcion=no&total=0";
                        }
                    }
                }
                else if(request.getParameter("noexiste")!=null){
                    next = "/cheques/buscarChequeNoExist.jsp";
                    String cheque1=request.getParameter("cheque1").toUpperCase();
                    String cheque2=request.getParameter("cheque2").toUpperCase();
                    String bancos = request.getParameter("banco");
                    
                    if(cheque2.equals("")){
                        cheque2=cheque1;
                    }
                    if(model.movplaService.existenCheques(cheque1,cheque2,banck,sucursalbanco)){
                        next = "/cheques/buscarChequeNoExist.jsp";
                        request.setAttribute("Mensaje", "ERROR: Existe un cheque impreso entre los que desea anular");
                    }
                    else{
                        Pattern p = Pattern.compile("[0-9]+");
                        Matcher m = p.matcher(cheque1);
                        boolean b = m.matches();
                        if(b){
                            
                            m = p.matcher(cheque2);
                            boolean ch2 = m.matches();
                            if(ch2){
                                logger.info( "Ok el patron esta bien");
                                //String prefix=cheque1.substring(0,2);
                                String base=usuario.getBase();
                                String vecBanco[] = bancos.split("/");
                                String banco=vecBanco[0];
                                String cuenta=vecBanco[1];
                                int ultnum = Integer.parseInt(cheque1);
                                if(model.movplaService.existeSerie(base,banco,cuenta,ultnum)){
                                    logger.info( "Ok existe la serie de cheques..");
                                    int ultnum2 = Integer.parseInt(cheque2);
                                    if(ultnum2<ultnum){
                                        next = "/cheques/buscarChequeNoExist.jsp";
                                        request.setAttribute("Mensaje", "ERROR: El primer numero de la serie no puede ser mayor al primero");
                                    }
                                    else{
                                        int cant = ultnum2-ultnum;
                                        model.tService.crearStatement();
                                        for(int i = 0; i<=cant;i++){
                                            String document = ""+ultnum+i;
                                            Movpla movpla= new Movpla();
                                            movpla.setDstrct(usuario.getDstrct());
                                            movpla.setAgency_id(usuario.getId_agencia());
                                            movpla.setDocument_type("001");
                                            movpla.setConcept_code("14");
                                            movpla.setVlr(0);
                                            movpla.setCurrency("PES");
                                            movpla.setCreation_user(usuario.getLogin());
                                            movpla.setBanco(banco);
                                            movpla.setCuenta(cuenta);
                                            movpla.setDocument(document);
                                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+100));
                                            model.tService.getSt().addBatch(model.movplaService.insertMovPlaChequeNull(movpla,usuario.getBase()));
                                        }
                                        model.seriesService.buscaSerie(banco,cuenta,ultnum2);
                                        if(model.seriesService.getSerie()!=null){
                                            Series serie = model.seriesService.getSerie();
                                            //serie.setLast_number(serie.getLast_number());
                                            model.tService.getSt().addBatch(model.seriesService.updateSerieCheque(serie,usuario));
                                            
                                        }
                                        model.tService.execute();
                                        next = "/cheques/buscarChequeNoExist.jsp";
                                        request.setAttribute("Mensaje", "Los cheques fueron anulados con exito");
                                        
                                    }
                                }else{
                                    next = "/cheques/buscarChequeNoExist.jsp";
                                    request.setAttribute("Mensaje", "ERROR: La serie no es correcta");
                                }
                            }
                            else{
                                next = "/cheques/buscarChequeNoExist.jsp";
                                request.setAttribute("Mensaje", "ERROR: La serie no es correcta");
                            }
                            
                        }
                        else{
                            next = "/cheques/buscarChequeNoExist.jsp";
                            request.setAttribute("Mensaje", "ERROR: La serie no es correcta");
                        }
                        
                    }
                }
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
