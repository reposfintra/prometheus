/******************************************************************
* Nombre ......................TblGeneralDatoInsertAction.java
* Descripci�n..................Clase Action para descuento de equipos
* Autor........................Armando Oviedo
* Fecha........................15/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class TblGeneralDatoInsertAction extends Action{
    
    /** Creates a new instance of TblGeneralDatoInsertAction */
    public TblGeneralDatoInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/general/TblGeneralDato/TblGeneralDatoInsert.jsp" ;
        try{
            HttpSession session = request.getSession();            
            String codtabla = request.getParameter("codtabla");            
            String leyenda = request.getParameter("leyenda");
            String tipo = request.getParameter("tipo");
            String longitud = request.getParameter("longitud");
            String mensaje = request.getParameter("mensaje");            
            if(codtabla!=null){
                Usuario usuario = (Usuario) session.getAttribute("Usuario");                        
                String base = usuario.getBase();                
                String distrito = (String)(session.getAttribute("Distrito"));                    
                TblGeneralDato tmp = new TblGeneralDato();
                tmp.setBase(base);
                tmp.setCodTabla(codtabla);                    
                tmp.setSecuencia(String.valueOf(model.tblgendatosvc.getSequencyNumber(codtabla)));
                tmp.setLeyenda(leyenda);                
                tmp.setTipo(tipo);
                tmp.setLongitud(longitud);
                tmp.setCreationUser(usuario.getLogin());
                tmp.setUserUpdate(usuario.getLogin());                
                tmp.setDstrct(distrito);                                                
                model.tblgendatosvc.setTablaGeneralDato(tmp);
                boolean existe = model.tblgendatosvc.existe();
                if(!existe){
                    model.tblgendatosvc.addTablaGeneralDato();
                    next += "?mensaje=Elemento ingresado correctamente";
                }
                else{
                    next += "?mensaje=Ya existe un elemento con este c�digo y secuencia";
                }                
            }            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
