/*
 * Nombre        InsertarContactoAction.java
 * Autor         Ing. Rodrigo Salazar
 * Fecha         23 de junio de 2005, 10:45 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;



public class InsertarContactoAction extends Action {
    
    /** Creates a new instance of InsertarContactoAction */
    public InsertarContactoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String codigo = (request.getParameter("c_codigo").toUpperCase());
        String cod_cia = (request.getParameter("c_cia").toUpperCase());
        String tipo = (request.getParameter("c_tipo").toUpperCase());
        String next = "/jsp/trafico/contacto/contacto.jsp?mensaje=Contacto agregado exitosamente!";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");  
                
        try{ 
            int sw = 0;//1 = error
            //buscar si contacto y cia existe
            if (!model.identidadService.buscarIdentidadxTid(codigo, "CED") ){
               sw=1;
            }
            if ( !model.identidadService.buscarIdentidadxTid(cod_cia, "NIT") ){
                sw=1;
            }
            
            if (sw==0){
                Contacto contacto = new Contacto();
                contacto.setCod_contacto(codigo);
                contacto.setCod_cia(cod_cia);
                contacto.setTipo(tipo);
                contacto.setUser_update(usuario.getLogin());
                contacto.setCreation_user(usuario.getLogin());                
                try{
                    model.contactoService.insertarContacto(contacto); 
                }catch(SQLException ex){
                    next = "/jsp/trafico/contacto/contacto.jsp?mensaje=Error...Contacto ya existe!";
                }
            }
            else {
                next = "/jsp/trafico/contacto/contacto.jsp?mensaje=Error...Codigo de Contacto y/o Compa�ia no existe!";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }    
}
